/* ********************************************** */
/* Fix the 3D geometry of elem_2d_nodes using     */
/* the Z coordinate from terrain                  */
/* ********************************************** */

update $model.elem_2d_node set
geom=ST_SnapToGrid(
    ST_SetSRID(
        ST_MakePoint(
            ST_X(ST_Centroid(contour)),
            ST_Y(ST_Centroid(contour)),
            project.altitude(ST_Centroid(contour))
            ),
        $srid
        ),
    (select precision from hydra.metadata)
    )
;;

