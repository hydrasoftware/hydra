/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

create function $model.closed_parametric_geometry_before_del_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config record;
    begin
        for config in select name from $model.configuration loop
            update $model._link
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', cp_geom}')::text[], 'null'::jsonb))::json
            where link_type='pipe' and (configuration->config.name->'cp_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', up_cp_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'up_cp_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', down_cp_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'down_cp_geom')::text=old.id::text;
        end loop;
        return old;
    end;
$$$$
;;

create trigger ${model}_closed_parametric_geometry_trig
    before delete on $model.closed_parametric_geometry
       for each row execute procedure ${model}.closed_parametric_geometry_before_del_fct()
;;

create function $model.open_parametric_geometry_before_del_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config record;
    begin
        for config in select name from $model.configuration loop
            update $model._link
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', op_geom}')::text[], 'null'::jsonb))::json
            where link_type='pipe' and (configuration->config.name->'op_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', up_op_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'up_op_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', down_op_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'down_op_geom')::text=old.id::text;
        end loop;
        return old;
    end;
$$$$
;;

create trigger ${model}_open_parametric_geometry_trig
    before delete on $model.open_parametric_geometry
       for each row execute procedure ${model}.open_parametric_geometry_before_del_fct()
;;


create function $model.valley_cross_section_geometry_before_del_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config record;
    begin
        for config in select name from $model.configuration loop
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', up_vcs_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'up_vcs_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', down_vcs_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'down_vcs_geom')::text=old.id::text;
        end loop;
        return old;
    end;
$$$$
;;

create trigger ${model}_valley_cross_section_geometry_trig
    before delete on $model.valley_cross_section_geometry
       for each row execute procedure ${model}.valley_cross_section_geometry_before_del_fct()
;;

create function $model.valley_cross_section_topo_geometry_before_del_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config record;
    begin
        for config in select name from $model.configuration loop
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', up_vcs_topo_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'up_vcs_topo_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', down_vcs_topo_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'down_vcs_topo_geom')::text=old.id::text;
        end loop;
        return old;
    end;
$$$$
;;

create trigger ${model}_valley_cross_section_topo_geometry_trig
    before delete on $model.valley_cross_section_topo_geometry
       for each row execute procedure ${model}.valley_cross_section_topo_geometry_before_del_fct()
;;
