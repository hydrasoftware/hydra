from builtins import next
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
import requests
import tempfile
import pathlib
from datetime import datetime
from hydra.database import database as dbhydra
from hydra.utility.settings_properties import SettingsProperties
from hydra.utility.process import run_cmd

__tempdir = tempfile.gettempdir()

def __replace_str(file, dic):
    lines = []
    with open(file) as infile:
        for line in infile:
            for src, target in dic.items():
                line = line.replace(src, target)
            lines.append(line)
    with open(file, 'w') as outfile:
        for line in lines:
            outfile.write(line)

def __replace_hydra_version(file, version):
    replacements = {"""CREATE EXTENSION IF NOT EXISTS hydra WITH SCHEMA public;""":
                    """CREATE EXTENSION IF NOT EXISTS hydra WITH SCHEMA public VERSION '{}';""".format(version)}
    __replace_str(file, replacements)

def __move_postgis_sfcgal(file):
    has_postgis_sfcgal = False
    with open(file, 'r') as fil:
        for line in fil:
            match = re.search(r"CREATE EXTENSION IF NOT EXISTS postgis_sfcgal WITH SCHEMA public;", line)
            if match:
                has_postgis_sfcgal = True
                break
    if has_postgis_sfcgal:
        # remonte la création de l'extension "postgis_sfcgal" avant l'extension hydra (car créée en cours de route, apres hydra)
        replacements_1 = {"""CREATE EXTENSION IF NOT EXISTS postgis_sfcgal WITH SCHEMA public;""":
                            """--CREATE EXTENSION IF NOT EXISTS postgis_sfcgal WITH SCHEMA public;""",
                          """COMMENT ON EXTENSION postgis_sfcgal IS 'PostGIS SFCGAL functions';""":
                            """--COMMENT ON EXTENSION postgis_sfcgal IS 'PostGIS SFCGAL functions';"""}
        __replace_str(file, replacements_1)

        replacements_2 = {"""COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';""":
                             """COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';"""+
                             """\n\nCREATE EXTENSION IF NOT EXISTS postgis_sfcgal WITH SCHEMA public;"""+
                             """\n\nCOMMENT ON EXTENSION postgis_sfcgal IS 'PostGIS SFCGAL functions';""",
                          """COMMENT ON EXTENSION postgis IS 'PostGIS geometry and geography spatial types and functions';""":
                             """COMMENT ON EXTENSION postgis IS 'PostGIS geometry and geography spatial types and functions';"""+
                             """\n\nCREATE EXTENSION IF NOT EXISTS postgis_sfcgal WITH SCHEMA public;"""+
                             """\n\nCOMMENT ON EXTENSION postgis_sfcgal IS 'PostGIS SFCGAL functions';"""}
        __replace_str(file, replacements_2)

def version_from_dump(file_name):
    """Extracts project version from a hydra sql dump file"""
    with open(file_name, 'r') as fil:
        for line in fil:
            # try with hydra schema version
            match1 = re.search(r"CREATE EXTENSION IF NOT EXISTS hydra WITH SCHEMA public VERSION '(\w+(\.\w+)*)'.*;", line)
            if match1:
                return match1.group(1)
            # try with metadata table content
            match2 = re.search(r"COPY (.+\.)*metadata \(", line)
            if match2:
                return next(fil, '').strip().split('\t')[1]
    return ''

def srid_from_dump(file_name):
    """Extracts SRID from a hydra sql dump file"""
    with open(file_name, 'r') as fil:
        for line in fil:
            match0 = re.search(r"SRID=(\d+)", line)
            if match0:
                return int(match0.group(1))
            match1 = re.search(r"geometry\('POINTZ',(\d+)\)", line)
            if match1:
                return int(match1.group(1))
            match2 = re.search(r"geometry\(PointZ,(\d+)\)", line)
            if match2:
                return int(match2.group(1))
    return ''

def dump_project(log, file_name, project_name, model_name=None):
    """Exports project to .sql file
    file_name: target file
    project_name: project to export
    model_name: if specified, exports only schema of model"""
    log.notice("Dumping project {} as {}".format(project_name, file_name))

    server = dbhydra.public_server()
    if server:
        res = requests.get(f"{server['url']}/dump_project?name={project_name}",
            headers={'authorization': '{token_type} {access_token}'.format(**server['token'])})
        if res.status_code != 200:
            raise RuntimeError('code: {} msg:{}'.format(res.status_code, res.text))
        open(file_name, 'wb').write(res.content)
        return

    args = [os.path.join(SettingsProperties.get_path('postgre'), 'bin', 'pg_dump'),
        '-O', # no ownership of objects
        '-x', # no grant/revoke in dump
        '-f', file_name
    ]
    if file_name.endswith('.dump'):
        # Custom format, compressed
        args.append("-Fc")

        # Model ?
    if model_name:
        args+=['-n', model_name]
    # service connection info must be last
    args+=[f"service={SettingsProperties.get_service()} dbname={project_name}"]

    run_cmd(args, log=log)

    # Version continuity !
    version = dbhydra.get_project_metadata(project_name, 'version')

    if not file_name.endswith('.dump'):
        # only valid for SQL files ??
        __replace_hydra_version(file_name, version)
        __move_postgis_sfcgal(file_name)

def temp_dump_project(log, project_name):
    log.notice("Dumping project {} into {}".format(project_name, __tempdir))
    temp_file = os.path.join(__tempdir, "_"+project_name+".save.sql")
    dump_project(log, temp_file, project_name)
    return temp_file

def autosave_project(log, project_name, autosave_directory, version):
    log.notice("Autosaving project {} into {}".format(project_name, autosave_directory))
    autosave_file = os.path.join(autosave_directory, project_name+"_"+version+"_"+datetime.now().strftime('%d%m%Y')+".save.sql")
    dump_project(log, autosave_file, project_name)
    return autosave_file

def run_psql(log, file_name, project_name, strict=False):
    """Run .sql file on database
    file_name: source file
    project_name: target project database"""
    args = ["psql"] + (['-v', 'ON_ERROR_STOP=on'] if strict else [])+['-f', file_name, f"service={SettingsProperties.get_service()} dbname={project_name}"]
    run_cmd(args, log=log)

def run_pg_restore(log, file_name, project_name):
    """Run .dump file on database and create it
    file_name: source file
    project_name: target project database"""
    # en remote on conserve les droits et on escalade les privilèges via hydra_writer
    #TODO en 3.11
    #args = ["pg_restore", '--role=hydra_writer', file_name, "-d", f"service={SettingsProperties.get_service()} dbname={project_name}"]
    args = ["pg_restore", file_name, "-d", f"service={SettingsProperties.get_service()} dbname={project_name}"]
    run_cmd(args, log=log)

def create_db(log, db_name):
    with tempfile.NamedTemporaryFile('wt', delete=False) as fp:
        #TODO en 3.11
        #fp.write(f"""set role hydra_writer;
        #         create database {db_name} owner hydra;""")
        fp.write(f"""create database {db_name} owner hydra;""")
    args = ["psql", "-f", fp.name, f"service={SettingsProperties.get_service()} dbname=postgres"] \
        if os.name == 'nt' else ["psql", f'service={SettingsProperties.get_service()} dbname=postgres', "-f", fp.name]
    run_cmd(args, log=log)
    pathlib.Path(fp.name).unlink()

def copy_project(log, project_name, new_project_name):
    temp_file = os.path.join(__tempdir, "_"+project_name+".save.sql")
    dump_project(log, temp_file, project_name)
    create_db(log, new_project_name)
    run_psql(log, temp_file, new_project_name)

def import_db(log, file_name, project_name):

    file_version = version_from_dump(file_name)
    # Check file version vs. current database version
    if file_version not in dbhydra.get_available_hydra_versions():
        raise Exception(f"'{file_version}' version not supported")
    create_db(log, project_name)
    run_psql(log, file_name, project_name)
    srid = srid_from_dump(file_name)
    dbhydra.insert_project_metadata(project_name, srid)



if __name__=='__main__':
    import sys
    from hydra.utility.log import LogManager, ConsoleLogger
    log = LogManager(ConsoleLogger(), "Hydra")
    name = sys.argv[1]
    file = sys.argv[2]
    import_db(log, file, name)
