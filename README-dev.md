# Installation

Retrieve Hydra source code using git:

```bash
git clone git@gitlab.com:hydrasoftware/hydra.git
```

Installing Hydra PostgreSQL extensions (replace PG_VERSION with your PostgreSQL version):

```bash
cd hydra/server/
sudo python3 install.py --all-versions /usr/share/postgresql/PG_VERSION/extension/
sudo python3 setup.py install
```

# Testing

## Linux

Add QGIS plugin directory to PYTHONPATH because `db_manager` is not available from qgis package.

```bash
export PYTHONPATH=/usr/share/qgis/python/plugins
```

Launch tests.

```bash
python3 -m test
```

# Developing

In order to push some changes, some pre-commit hooks are used to ensure code quality.

```
pip install --user pre-commit
pre-commit install
```

ou

```
sudo apt install pre-commit 
pre-commit install
```

Next, when you make a commit, Git will run the pre-commit hook.

Manually check your changed files:
```
pre-commit run
```

Manually check all files on the repository:
```
pre-commit run --all-files
```
