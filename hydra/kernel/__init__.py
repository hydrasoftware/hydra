# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run hydra calculation

USAGE

   python -m hydra.kernel_runner <project name>, <scenario name>

OPTIONS

   -h, --help
        outputs this help
"""

import os
import sys
import getopt
import datetime
from shutil import which
from subprocess import Popen, PIPE, STDOUT
if os.name == 'nt':
    from subprocess import STARTUPINFO, STARTF_USESHOWWINDOW
from qgis.PyQt.QtCore import QObject, pyqtSignal
from hydra.project import Project
from hydra.utility.licence import licence_file
from hydra.utility.empty_ui_manager import EmptyUIManager
from hydra.utility.log import LogManager, ConsoleLogger
from pathlib import Path

def find_exe(program):
    program_path = which(program)
    if program_path is None:
        kernel_exe = Path(__file__).parent / '..' / 'kernel' / f"{program + ('_dos' if program=='whydram24' else '')}.exe"
        if kernel_exe.exists():
            program_path = str(kernel_exe)
        else:
            raise RuntimeError(f"error: {kernel_exe} not found")
    return program_path


class KernelError(Exception):
    pass

class Kernel(QObject):
    log = pyqtSignal(str)
    output = pyqtSignal(str)

    @staticmethod
    def get_error(fname):
        if not os.path.isfile(fname):
            return -1
        else:
            err = 0
            with open(fname) as f:
                content = f.readlines()
                err = int(content[0])
            return err

    @staticmethod
    def readfile(fname):
        if not os.path.isfile(fname):
            return ""
        else:
            content=""
            with open(fname, 'rb') as f:
                content = f.readlines()
            content = [s.decode('Latin-1') for s in content]
            return content

    @staticmethod
    def get_filedate(file_name):
        try:
            mtime = os.path.getmtime(file_name)
        except OSError:
            mtime = 0
        return datetime.datetime.fromtimestamp(mtime).strftime("%d/%m/%Y %H:%M:%S")

    def __init__(self, project, scenario, parent=None, mode_serie=False, output=None):
        '''creates an object to start computations of a given project's scenario
            - project: object project
            - scenario: scenario's id'''
        QObject.__init__(self, parent)

        self.parent = parent
        self.__process = None

        self.__project = project
        self.__mode_serie = mode_serie
        self.__scn_table = 'scenario' if not mode_serie else 'serie_scenario'
        self.__scenario_id = scenario
        self.__scenario_name, self.__calc_mode, self.__flag_gfx_control, self.__model_connect_settings = self.__project.execute("""
            select name, comput_mode, flag_gfx_control, model_connect_settings from project.{} as scn where scn.id={}""".format(self.__scn_table, self.__scenario_id)).fetchone()
        self.__model_list = self.list_models()

        self.__cur_dir = os.getcwd()
        self.__kernel_dir = os.path.abspath(os.path.dirname(__file__))
        self.__calc_dir = os.path.join(self.__project.get_senario_path_from_name(self.__scenario_name), "travail")
        self.__hyd_dir = os.path.join(self.__project.get_senario_path_from_name(self.__scenario_name), "hydraulique")

        if output:
            self.output.connect(output)

    def get_status(self):
        '''returns status from computation: i, "error message", date'''
        if not os.path.isdir(self.__calc_dir) or not os.path.exists(os.path.join(self.__calc_dir, "hydrol.err")):
            return -1, "Not calculated", "Never"

        if self.__calc_mode!="hydraulics":
            i=Kernel.get_error(os.path.join(self.__calc_dir, "hydrol.err"))
            if (i!=0):
                return i, "hydrol error", Kernel.get_filedate(os.path.join(self.__calc_dir, "hydrol.err"))

        if self.__calc_mode!="hydrology":
            i=Kernel.get_error(os.path.join(self.__calc_dir, "hydragen.err"))
            if (i!=0):
                return i, "hydragen error", Kernel.get_filedate(os.path.join(self.__calc_dir, "hydragen.err"))

        i=Kernel.get_error(os.path.join(self.__hyd_dir, "whydram24.err"))

        if (i!=0):
            return i, "whydram24 error", Kernel.get_filedate(os.path.join(self.__hyd_dir, "whydram24.err"))
        return 0, "Calculated", Kernel.get_filedate(os.path.join(self.__hyd_dir, "whydram24.err"))

    def list_models(self):
        '''returns list of models used in scenario'''
        if self.__model_connect_settings=="mixed":
            lstmodel = []
            groups = self.__project.execute("""
                    select id, name, ord
                    from project.mixed as mixed, project.grp as grp
                    where mixed.grp=grp.id and mixed.scenario={}
                    order by mixed.ord""".format(self.__scenario_id)).fetchall()
            for group in groups:
                lstmodel = [name for name, in self.__project.execute("""
                    select name from project.model_config as model
                    where model.id in (select model from project.grp_model where grp = %i)
                    """%(int(group[0]))).fetchall()]
        else:
            lstmodel = [name for name, in self.__project.execute("""select name from project.model""").fetchall()]

        return lstmodel

    def models_validity(self):
        '''returns a list of models presenting invalidities in scenario'''
        invalid_models = []
        for model in self.__model_list:
            if self.__project.execute(""" select * from {}.invalid""".format(model)).fetchall():
                invalid_models.append(model)
        return invalid_models

    def run(self):
        '''runs the HYDRA kernel processes'''
        self.log.emit("Calculation ({}) starting: {}".format(self.__calc_mode, self.__scenario_name))
        i=0

        if self.__calc_mode in ["hydrology", "hydrology_and_hydraulics"]:
            i = self.execute("hydrol")
            self.log.emit("Hydrol ok" if i==0 else f"Hydrol error: {i}")

        if self.__calc_mode in ["hydraulics", "hydrology_and_hydraulics"]:
            if (i==0):
                j = self.execute("hydragen")
                if j==0:
                    self.log.emit("Hydragen ok")
                    k = self.execute("whydram24")
                    self.log.emit("Whydram24 ok" if k==0 else f"Whydram24 error: {k}")
                else:
                    self.log.emit(f"Hydragen error: {j}")

    def execute(self, program):
        '''executes a given HYDRA exec (hydrol / hydragen / whydram24)
           emits output and logs
           returns error code'''
        assert(program in ["hydrol", "hydragen", "whydram24"])

        program_name = f"{program}{'_dos' if program == 'whydram24' else ''}"

        # Find path for exe
        program_path = which(program_name)
        if program_path is None:
            kernel_exe = os.path.join(self.__kernel_dir, f"{program_name}.exe")
            if os.path.isfile(kernel_exe):
                program_path = kernel_exe
            else:
                self.log.emit(f"Error: {program} not found in {self.__kernel_dir}")
                raise KernelError(f"{program} not found in {self.__kernel_dir}")

        # GFX control
        if program == "whydram24":
            if self.__flag_gfx_control:
                self.log.emit("Graphic option for whydram24.exe is deprecated. Using whydram24_dos.exe instead.")

        # Choose directory for cwd
        if program in ["hydrol", "hydragen"]:
            dir_ = self.__calc_dir
        elif program == "whydram24":
            dir_ = self.__hyd_dir

        # Licence
        environment = os.environ.copy()
        if program in ["hydrol", "whydram24"]:
            environment['HYDRA_LICENSE'] = licence_file

        # Hide shell
        if os.name == 'nt':
            si = STARTUPINFO()
            si.dwFlags |= STARTF_USESHOWWINDOW # default : si.wShowWindow = subprocess.SW_HIDE
        else:
            si = None

        self.__process = Popen(program_path, cwd=dir_, stdin=PIPE, stdout=PIPE, stderr=STDOUT, env=environment, universal_newlines=True, startupinfo=si)

        # Emitting STDOUT of execution through "output" signal
        self.output.emit("{} output:".format(os.path.basename(program)))
        for stdout_line in iter(self.__process.stdout.readline, ""):
            self.output.emit(stdout_line)

        self.__process.stdin.close()
        self.__process.stdout.close()
        self.__process.wait()

        # Emitting logs of execution through "log" signal
        log_file = os.path.join(dir_, program + ".log")
        logs = Kernel.readfile(log_file)
        self.log.emit('\n'.join([str(line) for line in logs]))

        # Returning error code
        err_file = os.path.join(dir_, program + ".err")
        return Kernel.get_error(err_file)

    def kill_process(self):
        '''terminates process if any is running'''
        if self.__process:
            self.__process.terminate()

if __name__ == "__main__":
    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    try:
        optlist, args = getopt.getopt(sys.argv[1:],
                "h",
                ["help"])
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        exit(1)

    optlist = dict(optlist)

    if "-h" in optlist or "--help" in optlist:
        help(sys.modules[__name__])
        exit(0)

    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    log = LogManager(ConsoleLogger(), "Hydra")
    project = Project(args[0], log, EmptyUIManager())

    scn, = project.execute("""select id, from project.scenario where name = '{}'""".format(args[1])).fetchone()

    assert scn is not None

    runner = Kernel(project, scn)
    runner.run()
