/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update $model.metadata set version = '1.9';
;;

/* ********************************************** */
/*  new table config_serie                        */
/* ********************************************** */

create or replace function ${model}.config_before_delete_fct()
returns trigger
language plpgsql
as $$$$
    begin
        update $model._node set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._node set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;
        update $model._link set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._link set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;
        update $model._singularity set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._singularity set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;
        update $model._river_cross_section_profile set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._river_cross_section_profile set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;

        delete from project.config_scenario where model='$model' and configuration=old.id;
        delete from project.config_serie where model='$model' and configuration=old.id;

        return old;
    end;
$$$$
;;

/* ********************************************** */
/*  Results update (no street / mesh 2D)          */
/* ********************************************** */

drop view if exists ${model}.results_link;
;;

create or replace view ${model}.results_link as
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.gate_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.regul_gate_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.weir_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.borda_headloss_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.pump_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.deriv_pump_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.fuse_spillway_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.connector_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.strickler_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.porous_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.overflow_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.network_overflow_link
  --    union
  --select id, UPPER(name)::varchar(24) as name, geom from ${model}.mesh_2d_link
  --    union
  --select id, UPPER(name)::varchar(24) as name, geom from ${model}.street_link
;;

