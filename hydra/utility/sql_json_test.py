# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if layers loaded actually exist on the database

USAGE

   python -m hydra.utility.sql_json_test

"""

assert __name__ == "__main__"
import hydra.utility.sql_json as sql_json
import json

params = dict()
params['gameson'] = dict()
params['r15'] = dict()
params['gameson']['pollution_coef'] = "10.52"
params['gameson']['waterfall_coef'] = "0.0"
params['gameson']['adjust_coef'] = 1.23
params['r15']['reoxygen_coef'] = 'TEST'
params['r15']['temperature_correct_coef'] = "15"
params['r15']['temp_coef'] = 22

json_obj = sql_json.pack(params)
assert(json.loads(json_obj) == json.loads("""{"r15": {"temp_coef": 22, "reoxygen_coef": null, "temperature_correct_coef": 15}, "gameson": {"waterfall_coef": 0.0, "adjust_coef": 1.23, "pollution_coef": 10.52}}"""))
unpacked_obj = sql_json.unpack(json_obj)
assert(unpacked_obj['r15']['reoxygen_coef'] == '')
assert(unpacked_obj['r15']['temp_coef'] == '22')
assert(unpacked_obj['r15']['temperature_correct_coef'] == '15')
assert(unpacked_obj['gameson']['pollution_coef'] == "10.52")

del params

params = dict()
params['gameson'] = dict()
params['r15'] = dict()
params['gameson']['pollution_coef'] = "10.52"
params['gameson']['waterfall_coef'] = "0.0"
params['gameson']['adjust_coef'] = 1.23
params['r15']['reoxygen_coef'] = 'TEST'
params['r15']['temperature_correct_coef'] = "15"
params['r15']['temp_coef'] = 22

json_obj_with_str = sql_json.pack(params, keep_text=True)
assert(json.loads(json_obj_with_str) == json.loads("""{"r15": {"temp_coef": 22, "reoxygen_coef": "TEST", "temperature_correct_coef": 15}, "gameson": {"waterfall_coef": 0.0, "adjust_coef": 1.23, "pollution_coef": 10.52}}"""))
unpacked_obj = sql_json.unpack(json_obj_with_str)
assert(unpacked_obj['r15']['reoxygen_coef'] == 'TEST')
assert(unpacked_obj['r15']['temp_coef'] == '22')
assert(unpacked_obj['r15']['temperature_correct_coef'] == '15')
assert(unpacked_obj['gameson']['pollution_coef'] == '10.52')

print("ok")
