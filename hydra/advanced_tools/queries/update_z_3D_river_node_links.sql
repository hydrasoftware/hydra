/* ******************************************* */
/* Update 3D geom of river_nodes node and      */
/* links (overflow, strickler, mesh_2d)        */
/* ******************************************* */

update $model.river_node
set geom = (ST_SetSrid(
                ST_MakePoint(
                    ST_X(geom),
                    ST_Y(geom),
                    project.altitude(geom)
                    ),
                $srid
                )
            ),
    z_ground = project.altitude(geom)
;;

select $model.set_link_altitude(ol.id)
from $model.overflow_link as ol
where ol.z_crest1<=0
   or ol.z_crest1>999.
;;

select $model.set_link_altitude(sl.id)
from $model.strickler_link as sl
where sl.z_crest1<=0
   or sl.z_crest1>999.
;;

select $model.set_link_altitude(ml.id)
from $model.mesh_2d_link as ml
;;


