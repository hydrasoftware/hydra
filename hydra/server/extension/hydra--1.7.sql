create schema hydra;
create schema work;
create type hydra_valve_mode as enum ('no_valve', 'one_way_downstream', 'one_way_upstream');
create table hydra.valve_mode(
    name hydra_valve_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.valve_mode(name,description,id) values ('no_valve','no valve',1);
insert into hydra.valve_mode(name,description,id) values ('one_way_downstream','one way valve (toward downstream)',2);
insert into hydra.valve_mode(name,description,id) values ('one_way_upstream','one way valve (toward upstream)',3);
create type hydra_dry_inflow_period as enum ('weekday', 'weekend');
create table hydra.dry_inflow_period(
    name hydra_dry_inflow_period primary key,
    id integer unique,
    description varchar
    );
insert into hydra.dry_inflow_period(name) values ('weekday');
insert into hydra.dry_inflow_period(name) values ('weekend');
create type hydra_borda_headloss_singularity_type as enum ('enlargment_with_transition', 'contraction_with_transition', 'sharp_angle_bend', 'parametric', 'v', 'sharp_transition_geometry', 'screen_oblique_to_flow', 'q', 'circular_bend', 'screen_normal_to_flow', 'sharp_bend_rectangular', 'friction');
create table hydra.borda_headloss_singularity_type(
    name hydra_borda_headloss_singularity_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('enlargment_with_transition','Enlargment with transition',11);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('contraction_with_transition','Contraction with transition',10);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('sharp_angle_bend','Sharp angle bend',13);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('parametric','Parametric',17);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('v','Law dE=(K/2g).(Q2/S2)',2);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('sharp_transition_geometry','Sharp geometry transition',9);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('screen_oblique_to_flow','Screen oblique to flow',15);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('q','Law dz=KQ2',1);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('circular_bend','Circular bend',12);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('screen_normal_to_flow','Screen normal to flow',14);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('sharp_bend_rectangular','Sharp bend rectangular',18);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('friction','Friction',16);
create type hydra_transport_type as enum ('quality', 'no_transport', 'pollution', 'sediment');
create table hydra.transport_type(
    name hydra_transport_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.transport_type(name,description,id) values ('quality','Quality',3);
insert into hydra.transport_type(name,description,id) values ('no_transport','None',1);
insert into hydra.transport_type(name,description,id) values ('pollution','Pollution: Pollution generation and network transport',2);
insert into hydra.transport_type(name,description,id) values ('sediment','Hydromorphology: Bedload sediment transport',4);
create type hydra_borda_headloss_link_type as enum ('transition_edge_inlet', 'junction', 'enlargment_with_transition', 'parametric', 'contraction_with_transition', 'sharp_angle_bend', 'diffuser', 'v', 't_shape_bifurcation', 'lateral_inlet', 'sharp_transition_geometry', 't_shape_junction', 'lateral_outlet', 'screen_oblique_to_flow', 'q', 'circular_bend', 'screen_normal_to_flow', 'bifurcation', 'sharp_bend_rectangular', 'friction');
create table hydra.borda_headloss_link_type(
    name hydra_borda_headloss_link_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.borda_headloss_link_type(name,description,id) values ('transition_edge_inlet','Transition edge inlet',7);
insert into hydra.borda_headloss_link_type(name,description,id) values ('junction','Junction',6);
insert into hydra.borda_headloss_link_type(name,description,id) values ('enlargment_with_transition','Enlargment with transition',11);
insert into hydra.borda_headloss_link_type(name,description,id) values ('parametric','Parametric',17);
insert into hydra.borda_headloss_link_type(name,description,id) values ('contraction_with_transition','Contraction with transition',10);
insert into hydra.borda_headloss_link_type(name,description,id) values ('sharp_angle_bend','Sharp angle bend',13);
insert into hydra.borda_headloss_link_type(name,description,id) values ('diffuser','Diffuser',8);
insert into hydra.borda_headloss_link_type(name,description,id) values ('v','Law dE=(K/2g).(Q2/S2)',2);
insert into hydra.borda_headloss_link_type(name,description,id) values ('t_shape_bifurcation','T shape bifurcation',19);
insert into hydra.borda_headloss_link_type(name,description,id) values ('lateral_inlet','Lateral inlet',4);
insert into hydra.borda_headloss_link_type(name,description,id) values ('sharp_transition_geometry','Sharp geometry transition',9);
insert into hydra.borda_headloss_link_type(name,description,id) values ('t_shape_junction','T shape junction',20);
insert into hydra.borda_headloss_link_type(name,description,id) values ('lateral_outlet','Lateral outlet',3);
insert into hydra.borda_headloss_link_type(name,description,id) values ('screen_oblique_to_flow','Screen oblique to flow',15);
insert into hydra.borda_headloss_link_type(name,description,id) values ('q','Law dz=KQ2',1);
insert into hydra.borda_headloss_link_type(name,description,id) values ('circular_bend','Circular bend',12);
insert into hydra.borda_headloss_link_type(name,description,id) values ('screen_normal_to_flow','Screen normal to flow',14);
insert into hydra.borda_headloss_link_type(name,description,id) values ('bifurcation','Bifurcation',5);
insert into hydra.borda_headloss_link_type(name,description,id) values ('sharp_bend_rectangular','Sharp rectangular bend',18);
insert into hydra.borda_headloss_link_type(name,description,id) values ('friction','Friction',16);
create type hydra_computation_mode as enum ('hydraulics', 'hydrology', 'hydrology_and_hydraulics');
create table hydra.computation_mode(
    name hydra_computation_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.computation_mode(name,id) values ('hydraulics',2);
insert into hydra.computation_mode(name,id) values ('hydrology',1);
insert into hydra.computation_mode(name,id) values ('hydrology_and_hydraulics',3);
create type hydra_manhole_connection_type as enum ('manhole_cover', 'drainage_inlet');
create table hydra.manhole_connection_type(
    name hydra_manhole_connection_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.manhole_connection_type(name,description,id) values ('manhole_cover','Manhole cover',1);
insert into hydra.manhole_connection_type(name,description,id) values ('drainage_inlet','Drainage Inlet',2);
create type hydra_netflow_type as enum ('scs', 'permeable_soil', 'holtan', 'constant_runoff', 'horner');
create table hydra.netflow_type(
    name hydra_netflow_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.netflow_type(name,description,id) values ('scs','SCS',4);
insert into hydra.netflow_type(name,description,id) values ('permeable_soil','Permeable soil',5);
insert into hydra.netflow_type(name,description,id) values ('holtan','Holtan',3);
insert into hydra.netflow_type(name,description,id) values ('constant_runoff','Constant runoff',1);
insert into hydra.netflow_type(name,description,id) values ('horner','Horner',2);
create type hydra_rainfall_type as enum ('radar', 'single_triangular', 'mages', 'caquot', 'intensity_curve', 'double_triangular', 'gage');
create table hydra.rainfall_type(
    name hydra_rainfall_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.rainfall_type(name,id) values ('radar',6);
insert into hydra.rainfall_type(name,id) values ('single_triangular',2);
insert into hydra.rainfall_type(name,id) values ('mages',7);
insert into hydra.rainfall_type(name,id) values ('caquot',1);
insert into hydra.rainfall_type(name,id) values ('intensity_curve',4);
insert into hydra.rainfall_type(name,id) values ('double_triangular',3);
insert into hydra.rainfall_type(name,id) values ('gage',5);
create type hydra_rerouting_option as enum ('fraction', 'qlimit');
create table hydra.rerouting_option(
    name hydra_rerouting_option primary key,
    id integer unique,
    description varchar
    );
insert into hydra.rerouting_option(name,description,id) values ('fraction','Fraction (between 0 et 1)',1);
insert into hydra.rerouting_option(name,description,id) values ('qlimit','Q limit (m3/s)',2);
create type hydra_link_type as enum ('borda_headloss', 'porous', 'regul_gate', 'gate', 'deriv_pump', 'strickler', 'pipe', 'fuse_spillway', 'connector_hydrology', 'pump', 'routing_hydrology', 'connector', 'network_overflow', 'weir', 'overflow', 'mesh_2d');
create table hydra.link_type(
    name hydra_link_type primary key,
    id integer unique,
    description varchar,
    abbreviation varchar unique
    );
insert into hydra.link_type(name,abbreviation,id) values ('borda_headloss','QMK',10);
insert into hydra.link_type(name,abbreviation,id) values ('porous','LPOR',13);
insert into hydra.link_type(name,abbreviation,id) values ('regul_gate','RG',7);
insert into hydra.link_type(name,abbreviation,id) values ('gate','LORF',5);
insert into hydra.link_type(name,abbreviation,id) values ('deriv_pump','QDP',3);
insert into hydra.link_type(name,abbreviation,id) values ('strickler','LSTK',12);
insert into hydra.link_type(name,abbreviation,id) values ('pipe','TRC',20);
insert into hydra.link_type(name,abbreviation,id) values ('fuse_spillway','LDVF',15);
insert into hydra.link_type(name,abbreviation,id) values ('connector_hydrology','TRH',19);
insert into hydra.link_type(name,abbreviation,id) values ('pump','QMP',6);
insert into hydra.link_type(name,abbreviation,id) values ('routing_hydrology','ROUT',18);
insert into hydra.link_type(name,abbreviation,id) values ('connector','QMTR',9);
insert into hydra.link_type(name,abbreviation,id) values ('network_overflow','NOV',22);
insert into hydra.link_type(name,abbreviation,id) values ('weir','LDEV',4);
insert into hydra.link_type(name,abbreviation,id) values ('overflow','OFL',21);
insert into hydra.link_type(name,abbreviation,id) values ('mesh_2d','LPAV',11);
create type hydra_pollution_treatment_mode as enum ('residual_concentration', 'efficiency');
create table hydra.pollution_treatment_mode(
    name hydra_pollution_treatment_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.pollution_treatment_mode(name,description,id) values ('residual_concentration','Residual concentration',1);
insert into hydra.pollution_treatment_mode(name,description,id) values ('efficiency','Efficiency',2);
create type hydra_abutment_type as enum ('angle_90', 'angle_45', 'rounded');
create table hydra.abutment_type(
    name hydra_abutment_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.abutment_type(name,description,id) values ('angle_90','Angle 90',1);
insert into hydra.abutment_type(name,description,id) values ('angle_45','Angle 45',2);
insert into hydra.abutment_type(name,description,id) values ('rounded','Rounded edge',3);
create type hydra_network_type as enum ('combined', 'separative');
create table hydra.network_type(
    name hydra_network_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.network_type(name,description,id) values ('combined','Combined',2);
insert into hydra.network_type(name,description,id) values ('separative','Separative',1);
create type hydra_weir_mode_regul as enum ('elevation', 'width');
create table hydra.weir_mode_regul(
    name hydra_weir_mode_regul primary key,
    id integer unique,
    description varchar
    );
insert into hydra.weir_mode_regul(name,description,id) values ('elevation','Elevation',1);
insert into hydra.weir_mode_regul(name,description,id) values ('width','Width',2);
create type hydra_action_gate_type as enum ('downward_opening', 'upward_opening');
create table hydra.action_gate_type(
    name hydra_action_gate_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.action_gate_type(name,description,id) values ('downward_opening','downward opening',1);
insert into hydra.action_gate_type(name,description,id) values ('upward_opening','upward opening',2);
create type hydra_coverage_type as enum ('street', 'reach', 'storage', '2d');
create table hydra.coverage_type(
    name hydra_coverage_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.coverage_type(name,id) values ('street',3);
insert into hydra.coverage_type(name,id) values ('reach',2);
insert into hydra.coverage_type(name,id) values ('storage',7);
insert into hydra.coverage_type(name,id) values ('2d',1);
create type hydra_split_law_type as enum ('zq', 'weir', 'gate', 'stickler');
create table hydra.split_law_type(
    name hydra_split_law_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.split_law_type(name,id) values ('zq',4);
insert into hydra.split_law_type(name,id) values ('weir',1);
insert into hydra.split_law_type(name,id) values ('gate',2);
insert into hydra.split_law_type(name,id) values ('stickler',3);
create type hydra_cross_section_type as enum ('ovoid', 'pipe', 'valley', 'circular', 'channel');
create table hydra.cross_section_type(
    name hydra_cross_section_type primary key,
    id integer unique,
    description varchar,
    abbreviation varchar unique
    );
insert into hydra.cross_section_type(name,abbreviation,id) values ('ovoid','OV',5);
insert into hydra.cross_section_type(name,abbreviation,id) values ('pipe','PF',3);
insert into hydra.cross_section_type(name,abbreviation,id) values ('valley','VL',2);
insert into hydra.cross_section_type(name,abbreviation,id) values ('circular','CI',6);
insert into hydra.cross_section_type(name,abbreviation,id) values ('channel','PO',4);
create type hydra_param_sor1_type as enum ('upstream_singularity', 'downstream_upstream_singularity', 'hydrologic_element', 'link', 'upstream_downstream_singularity', 'downstream_singularity');
create table hydra.param_sor1_type(
    name hydra_param_sor1_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.param_sor1_type(name) values ('upstream_singularity');
insert into hydra.param_sor1_type(name) values ('downstream_upstream_singularity');
insert into hydra.param_sor1_type(name) values ('hydrologic_element');
insert into hydra.param_sor1_type(name) values ('link');
insert into hydra.param_sor1_type(name) values ('upstream_downstream_singularity');
insert into hydra.param_sor1_type(name) values ('downstream_singularity');
create type hydra_gate_mode_regul as enum ('discharge', 'elevation', 'no_regulation');
create table hydra.gate_mode_regul(
    name hydra_gate_mode_regul primary key,
    id integer unique,
    description varchar
    );
insert into hydra.gate_mode_regul(name,description,id) values ('discharge','Discharge',2);
insert into hydra.gate_mode_regul(name,description,id) values ('elevation','Elevation',1);
insert into hydra.gate_mode_regul(name,description,id) values ('no_regulation','No regulation',0);
create type hydra_type_domain as enum ('river', 'network', '2d_domain');
create table hydra.type_domain(
    name hydra_type_domain primary key,
    id integer unique,
    description varchar
    );
insert into hydra.type_domain(name,id) values ('river',2);
insert into hydra.type_domain(name,id) values ('network',1);
insert into hydra.type_domain(name,id) values ('2d_domain',3);
create type hydra_rainfall_interpolation_type as enum ('distance_ponderation', 'thyssen');
create table hydra.rainfall_interpolation_type(
    name hydra_rainfall_interpolation_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.rainfall_interpolation_type(name) values ('distance_ponderation');
insert into hydra.rainfall_interpolation_type(name) values ('thyssen');
create type hydra_singularity_type as enum ('pipe_branch_marker', 'weir_bc', 'param_headloss', 'bridge_headloss', 'zq_split_hydrology', 'tz_bc', 'froude_bc', 'reservoir_rs_hydrology', 'regul_sluice_gate', 'zregul_weir', 'strickler_bc', 'hydrograph_bc', 'bradley_headloss', 'borda_headloss', 'hydraulic_cut', 'zq_bc', 'qq_split_hydrology', 'marker', 'model_connect_bc', 'reservoir_rsp_hydrology', 'constant_inflow_bc', 'hydrology_bc', 'tank_bc', 'gate');
create table hydra.singularity_type(
    name hydra_singularity_type primary key,
    id integer unique,
    description varchar,
    abbreviation varchar unique
    );
insert into hydra.singularity_type(name,abbreviation,id) values ('pipe_branch_marker','MRKB',9);
insert into hydra.singularity_type(name,abbreviation,id) values ('weir_bc','DL',11);
insert into hydra.singularity_type(name,abbreviation,id) values ('param_headloss','DH',4);
insert into hydra.singularity_type(name,abbreviation,id) values ('bridge_headloss','BRDG',25);
insert into hydra.singularity_type(name,abbreviation,id) values ('zq_split_hydrology','DZH',21);
insert into hydra.singularity_type(name,abbreviation,id) values ('tz_bc','ZT',14);
insert into hydra.singularity_type(name,abbreviation,id) values ('froude_bc','ZF',15);
insert into hydra.singularity_type(name,abbreviation,id) values ('reservoir_rs_hydrology','RSH',22);
insert into hydra.singularity_type(name,abbreviation,id) values ('regul_sluice_gate','ACTA',6);
insert into hydra.singularity_type(name,abbreviation,id) values ('zregul_weir','DE',5);
insert into hydra.singularity_type(name,abbreviation,id) values ('strickler_bc','RK',12);
insert into hydra.singularity_type(name,abbreviation,id) values ('hydrograph_bc','HY',17);
insert into hydra.singularity_type(name,abbreviation,id) values ('bradley_headloss','BRD',8);
insert into hydra.singularity_type(name,abbreviation,id) values ('borda_headloss','SMK',3);
insert into hydra.singularity_type(name,abbreviation,id) values ('hydraulic_cut','CPR',2);
insert into hydra.singularity_type(name,abbreviation,id) values ('zq_bc','ZQ',13);
insert into hydra.singularity_type(name,abbreviation,id) values ('qq_split_hydrology','DQH',20);
insert into hydra.singularity_type(name,abbreviation,id) values ('marker','MRKP',7);
insert into hydra.singularity_type(name,abbreviation,id) values ('model_connect_bc','RACC',19);
insert into hydra.singularity_type(name,abbreviation,id) values ('reservoir_rsp_hydrology','RSPH',23);
insert into hydra.singularity_type(name,abbreviation,id) values ('constant_inflow_bc','HC',18);
insert into hydra.singularity_type(name,abbreviation,id) values ('hydrology_bc','CLH',24);
insert into hydra.singularity_type(name,abbreviation,id) values ('tank_bc','BO',16);
insert into hydra.singularity_type(name,abbreviation,id) values ('gate','VA',1);
create type hydra_runoff_type as enum ('define_k', 'socose', 'k_desbordes', 'passini', 'giandotti');
create table hydra.runoff_type(
    name hydra_runoff_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.runoff_type(name,description,id) values ('define_k','Defined K and linear reservoir',5);
insert into hydra.runoff_type(name,description,id) values ('socose','Tc imposed and Socose',4);
insert into hydra.runoff_type(name,description,id) values ('k_desbordes','k-Desbordes  and linear reservoir',1);
insert into hydra.runoff_type(name,description,id) values ('passini','k-Passini  and linear reservoir',2);
insert into hydra.runoff_type(name,description,id) values ('giandotti','k-Giandotti and linear reservoir',3);
create type hydra_model_connection_settings as enum ('global', 'mixed', 'cascade');
create table hydra.model_connection_settings(
    name hydra_model_connection_settings primary key,
    id integer unique,
    description varchar
    );
insert into hydra.model_connection_settings(name,id) values ('global',2);
insert into hydra.model_connection_settings(name,id) values ('mixed',3);
insert into hydra.model_connection_settings(name,id) values ('cascade',1);
create type hydra_fuse_spillway_break_mode as enum ('zw_critical', 'time_critical', 'none');
create table hydra.fuse_spillway_break_mode(
    name hydra_fuse_spillway_break_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.fuse_spillway_break_mode(name,description,id) values ('zw_critical','Break on water level',1);
insert into hydra.fuse_spillway_break_mode(name,description,id) values ('time_critical','Break on time',2);
insert into hydra.fuse_spillway_break_mode(name,description,id) values ('none','No break',0);
create type hydra_constrain_type as enum ('flood_plain_transect', 'porous', 'connector', 'boundary', 'strickler', 'overflow');
create table hydra.constrain_type(
    name hydra_constrain_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.constrain_type(name,id) values ('flood_plain_transect',7);
insert into hydra.constrain_type(name,id) values ('porous',2);
insert into hydra.constrain_type(name,id) values ('connector',6);
insert into hydra.constrain_type(name,id) values ('boundary',8);
insert into hydra.constrain_type(name,id) values ('strickler',5);
insert into hydra.constrain_type(name,id) values ('overflow',1);
create type hydra_model_connect_mode as enum ('hydrograph', 'zq_downstream_condition');
create table hydra.model_connect_mode(
    name hydra_model_connect_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.model_connect_mode(name,description,id) values ('hydrograph','Hydrograph',1);
insert into hydra.model_connect_mode(name,description,id) values ('zq_downstream_condition','Downstream condition: Q(Z)',2);
create type hydra_quality_type as enum ('bacteriological_quality', 'physico_chemical', 'suspended_sediment_transport');
create table hydra.quality_type(
    name hydra_quality_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.quality_type(name,description,id) values ('bacteriological_quality','Quality 2: Bacteriological quality',2);
insert into hydra.quality_type(name,description,id) values ('physico_chemical','Quality 1: Physico chemical',1);
insert into hydra.quality_type(name,description,id) values ('suspended_sediment_transport','Quality 3: Suspended sediment transport',3);
create type hydra_type_sor1 as enum ('hys_file', 'deb_or_lim_file');
create table hydra.type_sor1(
    name hydra_type_sor1 primary key,
    id integer unique,
    description varchar
    );
insert into hydra.type_sor1(name,description,id) values ('hys_file','hys file',1);
insert into hydra.type_sor1(name,description,id) values ('deb_or_lim_file','deb/lim file',2);
create type hydra_weir_mode_reoxy as enum ('gameson', 'r15');
create table hydra.weir_mode_reoxy(
    name hydra_weir_mode_reoxy primary key,
    id integer unique,
    description varchar
    );
insert into hydra.weir_mode_reoxy(name,description,id) values ('gameson','Gameson law',1);
insert into hydra.weir_mode_reoxy(name,description,id) values ('r15','R15 law',2);
create type hydra_node_type as enum ('elem_2d', 'manhole_hydrology', 'catchment', 'crossroad', 'manhole', 'storage', 'station', 'river');
create table hydra.node_type(
    name hydra_node_type primary key,
    id integer unique,
    description varchar,
    abbreviation varchar unique
    );
insert into hydra.node_type(name,abbreviation,id) values ('elem_2d','PAV',5);
insert into hydra.node_type(name,abbreviation,id) values ('manhole_hydrology','NODH',8);
insert into hydra.node_type(name,abbreviation,id) values ('catchment','BC',9);
insert into hydra.node_type(name,abbreviation,id) values ('crossroad','CAR',6);
insert into hydra.node_type(name,abbreviation,id) values ('manhole','NODA',1);
insert into hydra.node_type(name,abbreviation,id) values ('storage','CAS',4);
insert into hydra.node_type(name,abbreviation,id) values ('station','NODS',3);
insert into hydra.node_type(name,abbreviation,id) values ('river','NODR',2);
create table hydra.metadata(
    creation_date timestamp not null default current_date,
    srid integer references spatial_ref_sys(srid),
    precision real not null default .01,
    workspace varchar(256) not null default '',
    version varchar(24) not null default '1.7',
    id integer unique not null default(1) check (id=1) -- only one row
    );
create function hydra.metadata_after_insert_fct()
        returns trigger
        language plpython3u
        as $$
            from hydra_1_7 import create_project
            import plpy
            for statement in create_project(TD['new']['srid'], '1.7'):
                plpy.execute(statement)
        $$;
create trigger hydra_metadata_after_insert_trig
        after insert on hydra.metadata
        for each row execute procedure hydra.metadata_after_insert_fct()
        ;
/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

create or replace function notice(msg text, param anyelement)
returns anyelement
language plpgsql
immutable
as
$$
    begin
        raise notice '% %', msg, param;
        return param;
    end;
$$
;


/* ********************************************** */
/*  Linear geometry tools                         */
/* ********************************************** */

create type vector2 as (x double precision, y double precision)
;


-- difference of two points yielding a vector
create or replace function difference(a geometry, b geometry)
returns vector2
language plpgsql immutable
as
$$
    begin
        return row(st_x(a)-st_x(b), st_y(a)-st_y(b))::vector2;
    end;
$$
;


-- dot product of two points considered to be vectors
create or replace function dot(u vector2, v vector2)
returns float
language plpgsql immutable
as
$$
    begin
        return u.x*v.x + u.y*v.y;
    end;
$$
;


-- normalized vector
create or replace function normalized(u vector2)
returns vector2
language plpgsql immutable
as
$$
    declare
        norm double precision;
    begin
        norm := sqrt(dot(u, u));
        if norm < 1e-9 then
            return u;
        end if;
        return row(u.x/norm, u.y/norm)::vector2;
    end;
$$
;


-- angular interpolation of direction of two lines at a point
-- either points to intersection or // to both
create or replace function interpolate_direction(a geometry, b geometry, point geometry)
returns vector2
language plpgsql immutable
as
$$
    declare
        det double precision;
        u vector2;
        v vector2;
        d vector2;
        alpha double precision;
        res vector2;
    begin
    -- let a and b be the start points of the segments and u and v their directions,
    -- intesection point satisfies:

    -- a + alpha*u = b + beta*v

    -- ax + alpha*ux = bx + beta*vx
    -- ay + alpha*uy = by + beta*vy

    -- or

    -- alpha*ux - beta*vx = bx - ax
    -- alpha*uy - beta*uy = by - ay

    -- we define: d = b - a

    -- [ ux   -vx ][ alpha ] = [ d.x ]
    -- [ uy   -vy ][ beta  ] = [ d.y ]

    -- det = -ux*vy + uy*vx
    -- comT = [ -vy  vx ]
    --        [ -uy  ux ]


    -- [ alpha ] = 1/det comT * [ bx-ax ] = 1/det  [ -vy  vx ] * [ d.x ]
    -- [ beta  ]                [ by-ay ]          [ -uy  ux ]   [ d.y ]

        u := difference( st_endpoint(a), st_startpoint(a) );
        v := difference( st_endpoint(b), st_startpoint(b) );
        det = u.y*v.x - u.x*v.y;
        if abs(det) < 1.e-12 then -- lines are parallel
            --raise notice 'parallel %', det;
            return u;
        end if;
        d := difference( st_startpoint(b), st_startpoint(a) );
        alpha := (1./det)*(-v.y*d.x + v.x*d.y);
        --raise notice 'intersection % alpha % % % %', alpha, u, st_translate(st_startpoint(a), alpha*u.x, alpha*u.y), a, b;
        res := normalized(difference(st_translate(st_startpoint(a), alpha*u.x, alpha*u.y), point));
        if dot(res, u) > 0 then
            return res;
        else
            return row(-res.x, -res.y)::vector2;
        end if;
    end;
$$
;


create or replace function check_is_point(geom geometry)
returns geometry
language plpgsql immutable
as
$$
    begin
        if st_geometrytype(geom) = 'ST_Point' or geom is null then
            return geom;
        else
            raise '% is not a point but a %', st_astext(geom), st_geometrytype(geom);
            return geom;
        end if;
    end;
$$
;


-- linear interpolation of array points (2d), t is in [0,1]
create or replace function interpolate_array(u real[], v real[], t double precision)
returns real[]
language plpgsql
as
$$
    begin
        return (
            select array_agg(array[u[i][1] * (1. - t) + v[i][1] * t, u[i][2] * (1. - t) + v[i][2] * t])
            from generate_subscripts(u, 1) as i
        );
    end;
$$
;


create or replace function zb_cat(zb_maj_l real[], zb_min real[], zb_maj_r real[])
returns real[]
language plpgsql
immutable
as
$$
    begin
        --raise notice 'array % % % %', zb_maj_l, array_length(zb_maj_l ,1 ), zb_maj_l[1][:], array(select array[ zb_maj_l[array_length(zb_maj_l ,1 )][1], zb_maj_l[array_length(zb_maj_l ,1 )][2]] from generate_series(array_length(zb_maj_l, 1)+1, 4));
        return array_cat(
            array_cat(
                zb_maj_l,
                array(select array[zb_maj_l[array_length(zb_maj_l ,1 )][1], zb_maj_l[array_length(zb_maj_l ,1 )][2]] from generate_series(array_length(zb_maj_l, 1)+1, 4))
            ),
            array_cat(
                array_cat(
                    zb_min,
                    array(select array[zb_min[array_length(zb_min ,1 )][1], zb_min[array_length(zb_min ,1 )][2]] from generate_series(array_length(zb_min , 1)+1, 6))
                ),
                array_cat(
                    zb_maj_r,
                    array(select array[zb_maj_r[array_length(zb_maj_r ,1 )][1], zb_maj_r[array_length(zb_maj_r ,1 )][2]]  from generate_series(array_length(zb_maj_r , 1)+1, 4))
                )
            )
        );
    end;
$$
;
