# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_link_dialog import SimpleLinkDialog, tr
import hydra.utility.string as string
from ..widgets.graph_widget import GraphWidget
from ..widgets.array_widget import ArrayWidget
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QTableWidgetItem
import os

class DerivPumpLinkEditor(SimpleLinkDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleLinkDialog.__init__(self, project, geom, "deriv_pump_link", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "deriv_pump_link.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        name, q_pump, qz_array, comment = self.project.execute("""
            select name, q_pump, qz_array, comment
            from {}.deriv_pump_link
            where id={}""".format(self.model.name, self.id)).fetchone()
        self.name.setText(name)
        self.q_pump.setText(string.get_str(q_pump))

        self.graph = GraphWidget(self.graph_placeholder)
        self.array = ArrayWidget(["Maximum discharge ratio", "Upstream elevation (m)"], [10,2], qz_array,
                draw_graph_func=self.__draw_graph, help_func=None, parent=self.array_placeholder)
        self.notes.setText(comment)

    def __draw_graph(self, datas):
        if len(datas)>0:
            self.graph.clear()
            self.graph.add_line(
                [r[0] for r in datas],
                [r[1] for r in datas],
                "r",
                self.name.text(),
                tr("Q/Qmax"), tr("Z (m)"))
        else:
            self.graph.canvas.setVisible(False)

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.model.update_qdp_link(self.id, self.geom, string.get_sql_float(self.q_pump.text()), self.array.get_table_items(), self.name.text(), comment=self.notes.text())
        SimpleLinkDialog.save(self)
        self.close()
