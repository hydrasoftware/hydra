# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run all tests

USAGE
    python -m hydra.test [-h, -e test, -c, -x regex, -j nbproc, -l]

OPTIONS
    -h
        print this help

    -e test1,tes2
        exclude test1 and test2

    -c
        do not recompile fortan code (posix only)

    -x regex
        runs only tests that match regex

    -j nbproc
        runs nbproc tests in parallel

    -d
        run test in debug

    -l
        list tests instead of running them
"""
from __future__ import print_function
from builtins import str

import os
import re
import sys
import getopt
from time import time, sleep
from subprocess import Popen, PIPE
from hydra.database.database import TestProject
import atexit
import tempfile

# kill reunning processes at exit
@atexit.register
def goodbye():
    for p, t, s, l in RUNNING_PROCESSES:
        if p.poll() is None:
            p.kill()
RUNNING_PROCESSES = []

def list_tests(excludes, regex):
    "return module names for tests"
    tests = []
    hydra_dir = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'hydra')
    top_dir = os.path.dirname(hydra_dir)
    for root, dirs, files in os.walk(hydra_dir):
        for file_ in files:
            if re.match(r".*_test.py$", file_):
                # remove the trailing '.py' (3 characters)
                # replace \ or / by dots
                test = '.'.join(
                            os.path.abspath(
                                os.path.join(root, file_)
                            ).replace(hydra_dir, "hydra").split(os.sep))[:-3]
                if test not in excludes and (regex is None or re.match(regex, test)):
                    tests.append(test)
    return tests

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hj:e:c:x:ld",
            ["help"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

excludes = optlist['-e'].split(',') if '-e' in optlist else []
regex = optlist['-x'] if '-x' in optlist else None
tests = list_tests(excludes, regex)

if '-l' in optlist:
    print('\n'.join(tests))
    exit(0)

if len(excludes):
    print("excluded " + ' '.join(excludes))

print("template database creation... ")
debug = "-d" in optlist or "--debug" in optlist
start = time()
TestProject.reset_template()
print("created template database")

nb_proc = int(optlist['-j']) if '-j' in optlist else len(tests)

tmpdir = tempfile.mkdtemp()

if nb_proc > 1:
    print("start %d processes..."%(nb_proc))

i = 0

for test_group in [tests[i*nb_proc:i*nb_proc+nb_proc] for i in range(len(tests)//nb_proc)]:
    RUNNING_PROCESSES = []
    for test in test_group:
        with open(os.path.join(tmpdir, test+'.log'), 'w') as log:
            RUNNING_PROCESSES.append((Popen([sys.executable, "-m", test], stderr=log, stdout=log), test, time(), log))

        while len(RUNNING_PROCESSES):
            sleep(.1)
            for p, (process, test, strt, log) in enumerate(RUNNING_PROCESSES):
                if process.poll() is not None:
                    process.communicate()
                    exit_code = process.returncode
                    if exit_code!=0:
                        print("error in",test)
                        with open(os.path.join(tmpdir, test+'.log'), 'r') as log:
                            print(log.read())
                        exit(1)
                    print("%2d/%d %s after %.2fsec"%(i+1, len(tests), test, time() - strt))
                    i += 1
                    del RUNNING_PROCESSES[p]
                    break


print("everything is fine %d sec"%(int(time() - start)))