# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
run radar_rain tests

USAGE

    radar_rain_test [-hdk] [files]

    if files is specified, create terrain and hilshade from them

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode

   -k, --keep
        keep test database


"""

import os
import sys
import numpy
from shutil import rmtree
from tempfile import mkdtemp
from getopt import getopt
from hydra.project import Project
from hydra.utility.log import LogManager
from hydra.utility.timer import Timer
from hydra.database.radar_rain import RadarRain, RainVrt
from hydra.database.database import TestProject, project_exists, remove_project

hydra_dir = os.path.join(os.path.expanduser("~"), ".hydra")

try:
    optlist, args = getopt(sys.argv[1:],
            "hdk",
            ["help", "debug", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)
debug = "-d" in optlist or "--debug" in optlist
keep = "-k" in optlist or "--keep" in optlist

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

project_name = "radar_rain_test"

if project_exists(project_name):
    remove_project(project_name)
if os.path.exists(os.path.join(hydra_dir, project_name)):
    rmtree(os.path.join(hydra_dir, project_name))

test_project = TestProject(project_name, keep, with_model=True)
project = Project.load_project(project_name)

data_dir = os.path.join(os.path.dirname(__file__), 'test_data')

radar_rain = RadarRain(os.path.join(project.directory, 'rain'), project)

assert not radar_rain.has_data()

radar_rain.build_vrt('test',
                     args or [os.path.join(data_dir, 'LAME_EAU.LEPU.200811011300.tif'),
                              os.path.join(data_dir, 'LAME_EAU.LEPU.200811011305.tif'),
                              os.path.join(data_dir, 'LAME_EAU.LEPU.200811011310.tif')],
                     'LAME_EAU.LEPU.????????????')

assert radar_rain.has_data()

rain_file = project.execute("""select file from project.radar_rainfall""").fetchone()[0]
catchment_id = project.execute("""insert into model.catchment(geom) values('SRID=2154; POLYGON((809637.022242 6442441.47053,811102.50724 6450194.3589,820462.701742 6447547.03116,815498.962233 6434688.58215,809637.022242 6442441.47053))'::geometry) returning id""").fetchone()[0]
#catchment_id = project.execute("""insert into model.catchment(geom) values('SRID=2154; POLYGON((809637.022242 6442441.47053, 809638.022242 6442442.47053, 809637.022242 6442442.47053, 809637.022242 6442441.47053))::geometry') returning id""").fetchone()[0]
project.commit()

rain = RainVrt(rain_file, project)

assert rain.band_number==3

hyeto = rain.hyetogram(catchment_id, 'model', 1)
project.log.notice('Unit correction coefficient=1:')
project.log.notice('    time | cumul')
for [date,cumul] in hyeto:
    project.log.notice("  {date} | {cumul}".format(date=date, cumul=cumul))

# hyeto = rain.hyetogram(catchment_id, 'model', 100)
# project.log.notice('Unit correction coefficient=100:')
# project.log.notice('    time | cumul')
# for [t,i] in hyeto:
    # project.log.notice("    %4d | %5.2f" % (t, i))

if keep:
    project.commit()

print('ok')
