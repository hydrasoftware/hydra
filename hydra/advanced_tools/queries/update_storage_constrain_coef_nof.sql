/* ************************************************************************************  */
/* Will set contraction coeff for storage area,                                          */
/* you will need to select a "bati" table in                                             */
/* work schema and chose a geom column.                                                  */
/* this query will update the contraction coefficient                                    */
/* for all coverage with storage domain type                                             */
/*                                                                                       */
/* ************************************************************************************* */



DO $$$$DECLARE
    coverage_array integer[];
    cov integer;
    begin
    select  ARRAY_AGG(id) into coverage_array from $model.coverage where domain_type = 'storage';
    foreach cov in array coverage_array
    loop
        with bati_union as (
            select St_union(b.geom) as geom_union
            from work.$work_table as b, $model.coverage as contour
            where St_intersects(b.geom, contour.geom)
            and contour.id in (cov)
        ),
        intersection as(
            select St_intersection(b.geom_union, contour.geom) as inter_geom,
            contour.geom as contour_geom,
            contour.id as id
            from bati_union as b, $model.coverage as contour
            where contour.id in (cov)
        )
        update $model.storage_node
            set contraction_coef =
                case
                    when St_area(intersection.inter_geom) is not null then round(((St_area(contour_geom) - St_area(intersection.inter_geom))/St_area(contour_geom))::numeric,2)
                    else  1
                end
            from intersection
            where contour = intersection.id;
    end loop;
end$$$$;
;;