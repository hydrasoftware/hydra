# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from .cross_section_canvas import CrossSectionCanvas
from qgis.PyQt.QtWidgets import QWidget, QHBoxLayout, QLabel, QVBoxLayout
from qgis.PyQt.QtCore import pyqtSignal
from .zb_arrays_widget import ZbArraysWidget

class SectionWidget(QWidget):
    transect_changed = pyqtSignal(list)
    section_changed = pyqtSignal(dict)
    geometry_changed = pyqtSignal(dict)
    new_geometry = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.__layout = QHBoxLayout(self)
        self.__layout.setContentsMargins(0,0,0,0)
        self.__canvas = CrossSectionCanvas(parent=self)
        self.__zb_widget = None
        self.__vlayout = QVBoxLayout()
        self.__vlayout.addWidget(self.__canvas)
        self.__layout.addLayout(self.__vlayout)
        self.__section = None
        w = QWidget()
        w.setMaximumWidth(270)
        self.__layout.addWidget(w)
        self.__section_geom = None

    def set_section(self, section):
        self.__section = section
        w = self.__layout.takeAt(1)
        w and w.widget().setParent(None)

        if self.__zb_widget is not None:
            self.__zb_widget.setParent(None)
            self.__zb_widget = None

        if section is not None:
            self.__layout.addWidget(section)

            self.__section_changed(self.__section.param)

            self.__section.transect_changed.connect(self.__transect_changed)
            self.__section.section_changed.connect(self.__section_changed)
            self.__section.geometry_changed.connect(self.__geometry_changed)
            self.__section.new_geometry.connect(self.new_geometry.emit)
        else:
            w = QWidget()
            w.setMaximumWidth(270)
            self.__layout.addWidget(w)
            self.__section_geom = None
            self.__canvas.set_profile(None)

    def __geometry_changed(self, param):
        self.geometry_changed.emit(param)
        self.__canvas.draw()

    def __transect_changed(self, l):
        self.transect_changed.emit(l)
        # if transect changed, geometry changes, no need to draw
        self.__canvas.draw()

    def __section_changed(self, param):
        self.section_changed.emit(param)
        if self.__section.geom is not None:
            if self.__section.geom.param is not None:
                self.__geometry_changed(self.__section.geom.param)
            if self.__section.geom.transect is not None:
                self.__transect_changed(self.__section.geom.transect)
            if self.__section.geom != self.__section_geom:
                self.__section_geom = self.__section.geom
                if hasattr(self.__section_geom, 'set_zb_widget'):
                    self.__zb_widget = ZbArraysWidget()
                    self.__vlayout.addWidget(self.__zb_widget)
                    self.__section_geom.set_zb_widget(self.__zb_widget)
                self.__canvas.set_profile(self.__section.geom)
        else:
            self.__canvas.set_profile(None)

        self.__section_geom = self.__section.geom

        #self.__canvas.draw()
