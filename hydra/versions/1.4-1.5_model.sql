/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update $model.metadata set version = '1.5';
;;

/* ********************************************** */
/*  Configurations fix                            */
/* ********************************************** */

create or replace function ${model}.config_before_delete_fct()
returns trigger
language plpgsql
as $$$$
    begin
        update $model._node set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._node set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;
        update $model._link set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._link set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;
        update $model._singularity set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._singularity set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;
        update $model._river_cross_section_profile set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._river_cross_section_profile set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;

        delete from project.config_scenario where model='$model' and configuration=old.id;

        return old;
    end;
$$$$
;;

create or replace function ${model}.config_rename_fct()
returns trigger
language plpgsql
as $$$$
    begin
        update $model._node set configuration=(configuration::jsonb - old.name || jsonb_build_object(new.name, (configuration->old.name)))::json where configuration is not null and configuration::jsonb ? old.name;
        update $model._link set configuration=(configuration::jsonb - old.name || jsonb_build_object(new.name, (configuration->old.name)))::json where configuration is not null and configuration::jsonb ? old.name;
        update $model._singularity set configuration=(configuration::jsonb - old.name || jsonb_build_object(new.name, (configuration->old.name)))::json where configuration is not null and configuration::jsonb ? old.name;
        update $model._river_cross_section_profile set configuration=(configuration::jsonb - old.name || jsonb_build_object(new.name, (configuration->old.name)))::json where configuration is not null and configuration::jsonb ? old.name;
        return new;
    end;
$$$$
;;

drop trigger if exists ${model}_config_rename_trig on $model.configuration;
;;

create trigger ${model}_config_rename_trig
    before update of name on $model.configuration
       for each row execute procedure ${model}.config_rename_fct();
;;

/* ********************************************** */
/*  Fix missing SRID substitution on HY BC        */
/* ********************************************** */

create or replace function ${model}.hydrograph_bc_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'hydrograph_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'hydrograph_bc') where name = 'define_later' and id = id_;

            insert into $model._hydrograph_bc_singularity(id, singularity_type, storage_area, tq_array, constant_dry_flow, distrib_coef, lag_time_hr, sector, hourly_modulation, pollution_dryweather_runoff, quality_dryweather_runoff, external_file_data)
                values (id_, 'hydrograph_bc', coalesce(new.storage_area, 1), new.tq_array, coalesce(new.constant_dry_flow, 0), coalesce(new.distrib_coef, 1), coalesce(new.lag_time_hr, 0), new.sector, new.hourly_modulation, coalesce(new.pollution_dryweather_runoff, '{{0, 0, 0, 0},{0, 0, 0, 0}}'::real[]), coalesce(new.quality_dryweather_runoff, '{{0, 0, 0, 0, 0, 0, 0, 0, 0},{0, 0, 0, 0, 0, 0, 0, 0, 0}}'::real[]), coalesce(new.external_file_data, 'f'));
            if 'hydrograph_bc' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            if 'hydrograph_bc' = 'hydrograph_bc' then
                update $model.routing_hydrology_link set hydrograph=id_ where down=nid_;
                update $model.connector_hydrology_link set hydrograph=id_ where down=nid_;
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'hydrograph_bc_singularity');
            update $model._singularity set validity = (select (storage_area is not null) and (storage_area>=0) and (constant_dry_flow is not null) and (constant_dry_flow>=0) and (distrib_coef is null or distrib_coef>=0) and (lag_time_hr is null or (lag_time_hr>=0)) and (sector is null or (distrib_coef is not null and lag_time_hr is not null)) and (array_length(pollution_dryweather_runoff, 1)=2) and (array_length(pollution_dryweather_runoff, 2)=4) and (array_length(quality_dryweather_runoff, 1)=2) and (array_length(quality_dryweather_runoff, 2)=9) and (external_file_data or tq_array is not null) and (external_file_data or array_length(tq_array, 1)<=10) and (external_file_data or array_length(tq_array, 1)>=1) and (external_file_data or array_length(tq_array, 2)=2) from  $model._hydrograph_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.storage_area, new.tq_array, new.constant_dry_flow, new.distrib_coef, new.lag_time_hr, new.hourly_modulation, new.pollution_dryweather_runoff, new.quality_dryweather_runoff, new.external_file_data) is distinct from (old.storage_area, old.tq_array, old.constant_dry_flow, old.distrib_coef, old.lag_time_hr, old.hourly_modulation, old.pollution_dryweather_runoff, old.quality_dryweather_runoff, old.external_file_data)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.storage_area, old.tq_array, old.constant_dry_flow, old.distrib_coef, old.lag_time_hr, old.hourly_modulation, old.pollution_dryweather_runoff, old.quality_dryweather_runoff, old.external_file_data) as o, (select new.storage_area, new.tq_array, new.constant_dry_flow, new.distrib_coef, new.lag_time_hr, new.hourly_modulation, new.pollution_dryweather_runoff, new.quality_dryweather_runoff, new.external_file_data) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.storage_area, new.tq_array, new.constant_dry_flow, new.distrib_coef, new.lag_time_hr, new.hourly_modulation, new.pollution_dryweather_runoff, new.quality_dryweather_runoff, new.external_file_data) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._hydrograph_bc_singularity set storage_area=new.storage_area, tq_array=new.tq_array, constant_dry_flow=new.constant_dry_flow, distrib_coef=new.distrib_coef, lag_time_hr=new.lag_time_hr, sector=new.sector, hourly_modulation=new.hourly_modulation, pollution_dryweather_runoff=new.pollution_dryweather_runoff, quality_dryweather_runoff=new.quality_dryweather_runoff, external_file_data=new.external_file_data where id=old.id;
            if 'hydrograph_bc' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            if 'hydrograph_bc' = 'hydrograph_bc' then
                update $model.routing_hydrology_link set hydrograph=null where down=old.node;
                update $model.connector_hydrology_link set hydrograph=null where down=old.node;
                update $model.routing_hydrology_link set hydrograph=new.id where down=new.node;
                update $model.connector_hydrology_link set hydrograph=new.id where down=new.node;
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'hydrograph_bc_singularity');
            update $model._singularity set validity = (select (storage_area is not null) and (storage_area>=0) and (constant_dry_flow is not null) and (constant_dry_flow>=0) and (distrib_coef is null or distrib_coef>=0) and (lag_time_hr is null or (lag_time_hr>=0)) and (sector is null or (distrib_coef is not null and lag_time_hr is not null)) and (array_length(pollution_dryweather_runoff, 1)=2) and (array_length(pollution_dryweather_runoff, 2)=4) and (array_length(quality_dryweather_runoff, 1)=2) and (array_length(quality_dryweather_runoff, 2)=9) and (external_file_data or tq_array is not null) and (external_file_data or array_length(tq_array, 1)<=10) and (external_file_data or array_length(tq_array, 1)>=1) and (external_file_data or array_length(tq_array, 2)=2) from  $model._hydrograph_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            if 'hydrograph_bc' = 'hydrograph_bc' then
                update $model.routing_hydrology_link set hydrograph=null where down=old.node;
                update $model.connector_hydrology_link set hydrograph=null where down=old.node;
            end if;
            delete from $model._hydrograph_bc_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'hydrograph_bc' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$$$
;;

/* ********************************************** */
/*  Fix validity on catchment                     */
/* ********************************************** */
create or replace function ${model}.catchment_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('catchment', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'catchment') where name = 'define_later' and id = id_;
            insert into $model._catchment_node(id, node_type, area_ha, rl, slope, c_imp, netflow_type, constant_runoff, horner_ini_loss_coef, horner_recharge_coef, holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm, scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm, permeable_soil_j_mm, permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange, runoff_type, socose_tc_mn, socose_shape_param_beta, define_k_mn, q_limit, q0, contour, network_type, rural_land_use, industrial_land_use, suburban_housing_land_use, dense_housing_land_use)
                values (id_, 'catchment', new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, coalesce(new.q_limit, 9999), coalesce(new.q0, 0), coalesce(new.contour, (select id from $model.catchment where st_intersects(geom, new.geom))), coalesce(new.network_type, 'separative'), new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'catchment_node');

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'catchment' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_type!='constant_runoff' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)) and (netflow_type!='horner' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)) and (netflow_type!='horner' or (horner_recharge_coef is not null and horner_recharge_coef>=0)) and (netflow_type!='holtan' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)) and (netflow_type!='scs' or (scs_j_mm is not null and scs_j_mm>=0)) and (netflow_type!='scs' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)) and (netflow_type!='scs' or (scs_rfu_mm is not null and scs_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_j_mm is not null and permeable_soil_j_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_rfu_mm is not null and permeable_soil_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_max_inf_rate is not null and permeable_soil_ground_max_inf_rate>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_lag_time_day is not null and permeable_soil_ground_lag_time_day>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_coef_river_exchange is not null and permeable_soil_coef_river_exchange>=0 and permeable_soil_coef_river_exchange<=1)) and (runoff_type is not null) and (runoff_type!='socose' or (socose_tc_mn is not null and socose_tc_mn>0)) and (runoff_type!='socose' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6)) and (runoff_type!='define_k' or (define_k_mn is not null and define_k_mn>0)) and (q_limit is not null) and (q0 is not null) and (network_type is not null) and (rural_land_use is null or rural_land_use>=0) and (industrial_land_use is null or industrial_land_use>=0) and (suburban_housing_land_use is null or suburban_housing_land_use>=0) and (dense_housing_land_use is null or dense_housing_land_use>=0) from  $model._catchment_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.network_type, new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use) is distinct from (old.area_ha, old.rl, old.slope, old.c_imp, old.netflow_type, old.constant_runoff, old.horner_ini_loss_coef, old.horner_recharge_coef, old.holtan_sat_inf_rate_mmh, old.holtan_dry_inf_rate_mmh, old.holtan_soil_storage_cap_mm, old.scs_j_mm, old.scs_soil_drainage_time_day, old.scs_rfu_mm, old.permeable_soil_j_mm, old.permeable_soil_rfu_mm, old.permeable_soil_ground_max_inf_rate, old.permeable_soil_ground_lag_time_day, old.permeable_soil_coef_river_exchange, old.runoff_type, old.socose_tc_mn, old.socose_shape_param_beta, old.define_k_mn, old.q_limit, old.q0, old.contour, old.network_type, old.rural_land_use, old.industrial_land_use, old.suburban_housing_land_use, old.dense_housing_land_use)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area_ha, old.rl, old.slope, old.c_imp, old.netflow_type, old.constant_runoff, old.horner_ini_loss_coef, old.horner_recharge_coef, old.holtan_sat_inf_rate_mmh, old.holtan_dry_inf_rate_mmh, old.holtan_soil_storage_cap_mm, old.scs_j_mm, old.scs_soil_drainage_time_day, old.scs_rfu_mm, old.permeable_soil_j_mm, old.permeable_soil_rfu_mm, old.permeable_soil_ground_max_inf_rate, old.permeable_soil_ground_lag_time_day, old.permeable_soil_coef_river_exchange, old.runoff_type, old.socose_tc_mn, old.socose_shape_param_beta, old.define_k_mn, old.q_limit, old.q0, old.contour, old.network_type, old.rural_land_use, old.industrial_land_use, old.suburban_housing_land_use, old.dense_housing_land_use) as o, (select new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.network_type, new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.network_type, new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._catchment_node set area_ha=new.area_ha, rl=new.rl, slope=new.slope, c_imp=new.c_imp, netflow_type=new.netflow_type, constant_runoff=new.constant_runoff, horner_ini_loss_coef=new.horner_ini_loss_coef, horner_recharge_coef=new.horner_recharge_coef, holtan_sat_inf_rate_mmh=new.holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh=new.holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm=new.holtan_soil_storage_cap_mm, scs_j_mm=new.scs_j_mm, scs_soil_drainage_time_day=new.scs_soil_drainage_time_day, scs_rfu_mm=new.scs_rfu_mm, permeable_soil_j_mm=new.permeable_soil_j_mm, permeable_soil_rfu_mm=new.permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate=new.permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day=new.permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange=new.permeable_soil_coef_river_exchange, runoff_type=new.runoff_type, socose_tc_mn=new.socose_tc_mn, socose_shape_param_beta=new.socose_shape_param_beta, define_k_mn=new.define_k_mn, q_limit=new.q_limit, q0=new.q0, contour=new.contour, network_type=new.network_type, rural_land_use=new.rural_land_use, industrial_land_use=new.industrial_land_use, suburban_housing_land_use=new.suburban_housing_land_use, dense_housing_land_use=new.dense_housing_land_use where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'catchment_node');

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'catchment' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'catchment' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_type!='constant_runoff' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)) and (netflow_type!='horner' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)) and (netflow_type!='horner' or (horner_recharge_coef is not null and horner_recharge_coef>=0)) and (netflow_type!='holtan' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)) and (netflow_type!='scs' or (scs_j_mm is not null and scs_j_mm>=0)) and (netflow_type!='scs' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)) and (netflow_type!='scs' or (scs_rfu_mm is not null and scs_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_j_mm is not null and permeable_soil_j_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_rfu_mm is not null and permeable_soil_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_max_inf_rate is not null and permeable_soil_ground_max_inf_rate>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_lag_time_day is not null and permeable_soil_ground_lag_time_day>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_coef_river_exchange is not null and permeable_soil_coef_river_exchange>=0 and permeable_soil_coef_river_exchange<=1)) and (runoff_type is not null) and (runoff_type!='socose' or (socose_tc_mn is not null and socose_tc_mn>0)) and (runoff_type!='socose' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6)) and (runoff_type!='define_k' or (define_k_mn is not null and define_k_mn>0)) and (q_limit is not null) and (q0 is not null) and (network_type is not null) and (rural_land_use is null or rural_land_use>=0) and (industrial_land_use is null or industrial_land_use>=0) and (suburban_housing_land_use is not null or suburban_housing_land_use>=0) and (dense_housing_land_use is not null or dense_housing_land_use>=0) from  $model._catchment_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'storage' and (select trigger_coverage from $model.metadata) then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'catchment' = 'crossroad' and (select trigger_street_link from $model.metadata) then
                perform $model.update_street_links();
            end if;

            delete from $model._catchment_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

/* ********************************************** */
/*            View for results display            */
/* ********************************************** */

-- Pipes
create or replace view ${model}.results_pipe as
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.pipe_link
    where up_type='manhole' and down_type='manhole'
;;

-- Hydrol pipes
create or replace view ${model}.results_pipe_hydrol as
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.pipe_link
    where up_type='manhole_hydrology' and down_type='manhole_hydrology'
;;

-- Catchments
create or replace view ${model}.results_catchment as
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.catchment_node
;;

-- Links
create or replace view ${model}.results_link as
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.gate_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.regul_gate_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.weir_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.borda_headloss_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.pump_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.deriv_pump_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.fuse_spillway_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.connector_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.strickler_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.porous_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.overflow_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.network_overflow_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.mesh_2d_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.street_link
;;

-- Surface elements
create or replace view ${model}.results_surface as
    with
        dump as (
            select st_collect(n.geom) as points, s.geom as envelope, s.id
            from ${model}.coverage as s, ${model}.crossroad_node as n
            where s.domain_type='street' and st_intersects(s.geom, n.geom)
            group by s.id
        ),
        voronoi as (
            select st_intersection(st_setsrid((st_dump(st_VoronoiPolygons(dump.points, 0, dump.envelope))).geom, ${srid}), dump.envelope) as geom
            from dump
        )
    select e.id as id, UPPER(e.name)::varchar(24) as name, e.contour as geom from ${model}.elem_2d_node as e
        union
    select n.id as id, UPPER(n.name)::varchar(24) as name, c.geom as geom from ${model}.storage_node as n join ${model}.coverage as c on n.contour=c.id
        union
    select n.id as id, UPPER(n.name)::varchar(24) as name, voronoi.geom as geom from voronoi, ${model}.crossroad_node as n where st_intersects(voronoi.geom, n.geom)
        union
    select n.id as id, UPPER(n.name)::varchar(24) as name, c.geom as geom from ${model}.crossroad_node as n, ${model}.coverage as c where c.domain_type='crossroad' and st_intersects(n.geom, c.geom)
    order by id asc;
;;
