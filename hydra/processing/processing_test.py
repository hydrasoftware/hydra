from qgis.core import QgsApplication
from qgis.PyQt.QtWidgets import QApplication


from .provider import HydraProvider
from .taudem.taudemProvider import TauDemProvider

app = QApplication([])
QgsApplication.setPrefixPath("/usr", True)
QgsApplication.initQgis()

# Adds hydra processes to processing module
processing_provider = HydraProvider()

QgsApplication.processingRegistry().addProvider(processing_provider)
for hydra_alg in ['Bake', 'Depth velocity vectorisation', 'Blurring raster']:
    assert hydra_alg in [alg.displayName() for alg in QgsApplication.processingRegistry().algorithms()]

taudem_provider = TauDemProvider()
assert taudem_provider.getAlgs()

print('ok')

## to od test calling of processes
# # Snipet code to test calling of processes
# inLayer = QgsVectorLayer('/home/user/data/in.shp', 'input', 'ogr')
# outLayer = '/home/user/data/out.shp'
# alg.setParameterValue('INPUT', inLayer)
# alg.setOutputValue('OUTPUT', outLayer)

# # Run the algorithm
# from processing.core.SilentProgress import SilentProgress
# progress = SilentProgress()
# alg.processAlgorithm(progress)
