# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.forms.zregul_weir_singularity_guitest [-dhs] [-g project_name model_name x y]

OPTIONS
   -s  --solo
        run test in solo mode (create database)

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name model_name x y
        run in gui mode

"""

from hydra.database.database import TestProject, remove_project, project_exists
from hydra.utility.string import normalized_name, normalized_model_name
import sys
import getopt

from hydra.project import Project

def gui_mode(project_name, model_name, geom, id):

    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.zregul_weir_singularity import ZregulWeirSingularityEditor
    from hydra.utility.string import normalized_name, normalized_model_name

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)

    test_dialog = ZregulWeirSingularityEditor(obj_project, geom, id)
    test_dialog.exec_()


def test():
    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.zregul_weir_singularity import ZregulWeirSingularityEditor
    from hydra.database.database import TestProject

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)
    model = obj_project.get_current_model()

    model.add_manhole([-4, -12])
    obj_project.commit()

    # check cancellation and creation on the same point
    test_dialog = ZregulWeirSingularityEditor(obj_project, [-4, -12])
    test_dialog.reject()

    test_dialog = ZregulWeirSingularityEditor(obj_project, [-4, -12])
    test_dialog.name.setText('test_zregul_weir')
    test_dialog.radioBtn_regulated_weir.setChecked(True)
    test_dialog.z_invert.setText('15')
    test_dialog.width.setText('12')
    test_dialog.z_regul.setText('0.5')
    test_dialog.cc.setText('0.9')
    test_dialog.mode_regul.setCurrentIndex(test_dialog.mode_regul.findText('Width'))
    test_dialog.reoxy_law.setCurrentIndex(test_dialog.reoxy_law.findText('R15 law'))
    test_dialog.reoxygen_coef.setText('TEST')
    test_dialog.temperature_correct_coef.setText('10')
    id_weir_singularity = test_dialog.get_id()
    test_dialog.save()

    test_dialog = ZregulWeirSingularityEditor(obj_project, None, id_weir_singularity)
    assert test_dialog.name.text() =='test_zregul_weir'
    assert float(test_dialog.z_invert.text()) == 15
    assert float(test_dialog.width.text()) == 12
    assert float(test_dialog.z_regul.text()) == 0.5
    assert float(test_dialog.cc.text()) == 0.9
    assert test_dialog.mode_regul.currentText() == 'Width'
    assert test_dialog.reoxy_law.currentText() == 'R15 law'
    assert test_dialog.reoxygen_coef.text() == ''
    assert float(test_dialog.temperature_correct_coef.text()) == 10
    test_dialog.reoxy_law.setCurrentIndex(test_dialog.reoxy_law.findText('Gameson law'))
    test_dialog.adjust_coef.setText('0.8')
    test_dialog.waterfall_coef.setText('None')
    test_dialog.pollution_coef.setText('null')
    test_dialog.radioBtn_fixed_weir.setChecked(True)
    test_dialog.z_weir.setText('10')
    test_dialog.save()

    test_dialog = ZregulWeirSingularityEditor(obj_project, None, id_weir_singularity)
    assert test_dialog.name.text() =='test_zregul_weir'
    assert float(test_dialog.z_weir.text()) == 10
    assert float(test_dialog.width.text()) == 12
    assert float(test_dialog.cc.text()) == 0.9
    assert test_dialog.radioBtn_fixed_weir.isChecked()
    assert test_dialog.reoxy_law.currentText() == 'Gameson law'
    assert float(test_dialog.adjust_coef.text()) ==  0.8
    assert test_dialog.waterfall_coef.text() == ''
    assert test_dialog.pollution_coef.text() == ''
    test_dialog.reject()


try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])

except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name, with_model=True)
    test()
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    model_name = args[1]
    geom = [float(a) for a in args[2:]]
    id =geom[2] if len(geom)==3 else None
    project_name = normalized_name(project_name)
    model_name = normalized_model_name(model_name)
    gui_mode(project_name, model_name, geom, id)
    exit(0)

test()
# fix_print_with_import
print("ok")


