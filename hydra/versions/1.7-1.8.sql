/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update hydra.metadata set version = '1.8';
;;

create or replace function project.model_instead_fct()
returns trigger
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_model
    for statement in create_model(TD['new']['name'], $srid, '$version'):
        plpy.execute(statement)
$$$$
;;

/* ********************************************** */
/*  Catchment evolution                           */
/* ********************************************** */

--isolated--
alter type hydra_netflow_type add value if not exists 'gr4';
;;
insert into hydra.netflow_type(id, name, description) values (6, 'gr4', 'GR4');
;;

--isolated--
alter type hydra_netflow_type add value if not exists 'hydra';
;;
update hydra.netflow_type set name='hydra', description='Hydra' where id=5;
;;

/* ********************************************** */
/*  Rerouting add model_to                        */
/* ********************************************** */

alter table project.inflow_rerouting rename column model to model_from;
alter table project.inflow_rerouting add column model_to varchar(24);
update project.inflow_rerouting set model_to=model_from;
;;

/* ********************************************** */
/*  Output SOR1                                   */
/* ********************************************** */

create type public.hydra_sor1_mode as enum('two_files_extended', 'two_files_averaged', 'one_file');
;;

create table hydra.sor1_mode(
    name hydra_sor1_mode primary key,
    id integer unique,
    description varchar
);

insert into hydra.sor1_mode(name, id, description)
values ('two_files_extended', 1, '2 files - extended Hydra format'),
       ('two_files_averaged', 2, '2 files -averaged'),
       ('one_file', 3, '1 file/item');
;;

alter table project.scenario add column sor1_mode hydra_sor1_mode not null default 'two_files_extended';
alter table project.scenario add column dt_sor1_hr interval default '00:05:00' check(extract(seconds from dt_sor1_hr)=0);
update project.scenario set dt_sor1_hr = dt_output_hr;
;;




