/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update $model.metadata set version = '1.6';
;;

/* ********************************************** */
/*  Street removal                                */
/* ********************************************** */

alter table $model.street disable trigger ${model}_street_after_trig;
;;

alter table $model.street enable trigger ${model}_street_after_trig;
;;

drop table if exists $model._street_link cascade;
;;

delete from $model._link where link_type='street';
;;

alter table $model._link drop constraint ${model}_link_connectivity_check_cstr;
alter table $model._link add constraint ${model}_link_connectivity_check_cstr check (
    ((link_type = 'deriv_pump'::hydra_link_type) and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))) or
    ((link_type = 'connector_hydrology'::hydra_link_type) and (up_type = 'manhole_hydrology'::hydra_node_type) and (down_type = any (array['manhole_hydrology'::hydra_node_type, 'manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'storage'::hydra_node_type, 'crossroad'::hydra_node_type]))) or
    ((link_type = 'weir'::hydra_link_type) and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))) or
    ((link_type = 'porous'::hydra_link_type) and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))) or
    ((link_type = 'borda_headloss'::hydra_link_type) and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))) or
    ((link_type = 'strickler'::hydra_link_type) and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))) or
    ((link_type = 'fuse_spillway'::hydra_link_type) and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))) or
    ((link_type = 'routing_hydrology'::hydra_link_type) and (up_type = 'catchment'::hydra_node_type) and (down_type = any (array['manhole_hydrology'::hydra_node_type, 'manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'storage'::hydra_node_type, 'crossroad'::hydra_node_type]))) or
    ((link_type = 'gate'::hydra_link_type) and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))) or
    ((link_type = 'regul_gate'::hydra_link_type) and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))) or
    ((link_type = 'overflow'::hydra_link_type) and (up_type = any (array['river'::hydra_node_type, 'storage'::hydra_node_type, 'elem_2d'::hydra_node_type, 'crossroad'::hydra_node_type])) and (down_type = any (array['river'::hydra_node_type, 'storage'::hydra_node_type, 'elem_2d'::hydra_node_type, 'crossroad'::hydra_node_type]))) or
    ((link_type = 'connector'::hydra_link_type) and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))) or
    ((link_type = 'pipe'::hydra_link_type) and (((up_type = 'manhole_hydrology'::hydra_node_type) and (down_type = 'manhole_hydrology'::hydra_node_type)) or ((up_type = 'manhole_hydrology'::hydra_node_type) and (down_type = 'station'::hydra_node_type)) or ((up_type = 'manhole'::hydra_node_type) and (down_type = 'manhole'::hydra_node_type)) or ((up_type = 'manhole'::hydra_node_type) and (down_type = 'station'::hydra_node_type)))) or
    ((link_type = 'pump'::hydra_link_type) and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))) or
    ((link_type = 'network_overflow'::hydra_link_type) and (up_type = 'manhole'::hydra_node_type) and (down_type = any (array['crossroad'::hydra_node_type, 'elem_2d'::hydra_node_type, 'storage'::hydra_node_type, 'river'::hydra_node_type]))) or
    ((link_type = 'mesh_2d'::hydra_link_type) and (up_type = 'elem_2d'::hydra_node_type) and (down_type = 'elem_2d'::hydra_node_type))
)
;;


alter table $model.metadata drop column if exists trigger_street_link;
;;

drop function if exists ${model}.update_street_links();
;;

create or replace function ${model}._link_after_delete_fct()
returns trigger
language plpgsql
as $$$$
    begin
        delete from $model._node
        where id=old.down
        and generated is not null
        and (select count(1) from $model._link where up=old.down) = 0
        and (select count(1) from $model._link where down=old.down) = 0
        and (select count(1) from $model._singularity as s where s.id=id) = 0;
        delete from $model._node
        where id=old.up
        and generated is not null
        and (select count(1) from $model._link where up=old.up) = 0
        and (select count(1) from $model._link where down=old.up) = 0
        and (select count(1) from $model._singularity as s where s.id=id) = 0;
        return old;
    end;
$$$$
;;

create or replace view $model.street_link as (
    with p as (select precision as prec from hydra.metadata),
         links as (
                select s.rk,
                    s.width,
                    ST_MakeLine(
                        lag(n.geom) over (partition by s.id order by ST_LineLocatePoint(s.geom, n.geom)),
                        n.geom
                        ) as geom
                from $model.street as s, $model.crossroad_node as n, p
                where ST_DWithin(s.geom, n.geom, p.prec)
                )
    select
        (row_number() OVER ())::integer + (( SELECT max(_link.id) FROM $model._link)) AS id,
        'LRUE'::text || ((row_number() OVER ())::integer + ( SELECT max(_link.id) FROM $model._link))::character varying::text AS name,
        u.id as up,
        d.id as down,
        width,
        ST_Length(l.geom)::real as length,
        rk,
        l.geom
    from links as l
        join $model.crossroad_node as u on ST_Equals(ST_StartPoint(l.geom), u.geom)
        join $model.crossroad_node as d on ST_Equals(ST_EndPoint(l.geom), d.geom)
    where l.geom is not null and ST_IsValid(l.geom)
    order by id asc
    );
;;

-- Recreates nodes fct (deletion of reference to street_link_update()
create or replace function $model.crossroad_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('crossroad', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'crossroad') where name = 'define_later' and id = id_;
            insert into $model._crossroad_node(id, node_type, area, z_ground, h)
                values (id_, 'crossroad', coalesce(new.area, 1), coalesce(new.z_ground, (select project.altitude(new.geom))), coalesce(new.h, 0));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'crossroad_node');

            update $model._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) and (h is not null) and (h>=0) from  $model._crossroad_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area, new.z_ground, new.h) is distinct from (old.area, old.z_ground, old.h)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.z_ground, old.h) as o, (select new.area, new.z_ground, new.h) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.z_ground, new.h) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._crossroad_node set area=new.area, z_ground=new.z_ground, h=new.h where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'crossroad_node');

            update $model._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) and (h is not null) and (h>=0) from  $model._crossroad_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            delete from $model._crossroad_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;


create or replace function $model.elem_2d_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('elem_2d', coalesce(new.name, 'define_later'), ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), $srid), (select precision from hydra.metadata)), new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'elem_2d') where name = 'define_later' and id = id_;
            insert into $model._elem_2d_node(id, node_type, area, zb, rk, domain_2d, contour)
                values (id_, 'elem_2d', coalesce(new.area, ST_Area(new.contour)), coalesce(new.zb, (select ST_Z(ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), $srid), (select precision from hydra.metadata))))), coalesce(new.rk, 12), new.domain_2d, new.contour);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'elem_2d_node');

            update $model._node set validity = (select (area is not null) and (area>0) and (zb is not null) and (rk is not null) and (rk>0) from  $model._elem_2d_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area, new.zb, new.rk) is distinct from (old.area, old.zb, old.rk)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.zb, old.rk) as o, (select new.area, new.zb, new.rk) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.zb, new.rk) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), $srid), (select precision from hydra.metadata)), generated=new.generated where id=old.id;
            update $model._elem_2d_node set area=new.area, zb=new.zb, rk=new.rk, domain_2d=new.domain_2d, contour=new.contour where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'elem_2d_node');

            update $model._node set validity = (select (area is not null) and (area>0) and (zb is not null) and (rk is not null) and (rk>0) from  $model._elem_2d_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            delete from $model._elem_2d_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;


create or replace function $model.manhole_hydrology_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('manhole_hydrology', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'manhole_hydrology') where name = 'define_later' and id = id_;
            insert into $model._manhole_hydrology_node(id, node_type, area, z_ground)
                values (id_, 'manhole_hydrology', coalesce(new.area, 1), coalesce(new.z_ground, (select project.altitude(new.geom))));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'manhole_hydrology_node');

            update $model._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) from  $model._manhole_hydrology_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area, new.z_ground) is distinct from (old.area, old.z_ground)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.z_ground) as o, (select new.area, new.z_ground) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.z_ground) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._manhole_hydrology_node set area=new.area, z_ground=new.z_ground where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'manhole_hydrology_node');

            update $model._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) from  $model._manhole_hydrology_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            delete from $model._manhole_hydrology_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;


create or replace function $model.river_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('river', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'river') where name = 'define_later' and id = id_;
            insert into $model._river_node(id, node_type, reach, z_ground, area)
                values (id_, 'river', coalesce(new.reach, (select id from $model.reach where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) asc limit 1)), coalesce(new.z_ground, (select project.altitude(new.geom))), coalesce(new.area, 1));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'river_node');

            update $model._node set validity = (select (z_ground is not null) and (area is not null) and (area>0) and (reach is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) from  $model._river_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.z_ground, new.area) is distinct from (old.z_ground, old.area)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_ground, old.area) as o, (select new.z_ground, new.area) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_ground, new.area) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._river_node set reach=new.reach, z_ground=new.z_ground, area=new.area where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'river_node');

            update $model._node set validity = (select (z_ground is not null) and (area is not null) and (area>0) and (reach is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) from  $model._river_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            delete from $model._river_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;


create or replace function $model.station_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('station', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'station') where name = 'define_later' and id = id_;
            insert into $model._station_node(id, node_type, area, z_invert, station)
                values (id_, 'station', coalesce(new.area, 1), coalesce(new.z_invert, (select project.altitude(new.geom))), coalesce(new.station, (select id from $model.station where ST_Intersects(new.geom, geom))));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'station_node');

            update $model._node set validity = (select (area is not null) and (area>0) and (z_invert is not null) and (station is not null ) and ((select st_intersects(new.geom, geom) from $model.station where id=new.station)) from  $model._station_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area, new.z_invert) is distinct from (old.area, old.z_invert)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.z_invert) as o, (select new.area, new.z_invert) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.z_invert) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._station_node set area=new.area, z_invert=new.z_invert, station=new.station where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'station_node');

            update $model._node set validity = (select (area is not null) and (area>0) and (z_invert is not null) and (station is not null ) and ((select st_intersects(new.geom, geom) from $model.station where id=new.station)) from  $model._station_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            delete from $model._station_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;


create or replace function $model.catchment_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('catchment', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'catchment') where name = 'define_later' and id = id_;
            insert into $model._catchment_node(id, node_type, area_ha, rl, slope, c_imp, netflow_type, constant_runoff, horner_ini_loss_coef, horner_recharge_coef, holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm, scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm, permeable_soil_j_mm, permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange, runoff_type, socose_tc_mn, socose_shape_param_beta, define_k_mn, q_limit, q0, contour, network_type, rural_land_use, industrial_land_use, suburban_housing_land_use, dense_housing_land_use)
                values (id_, 'catchment', new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, coalesce(new.q_limit, 9999), coalesce(new.q0, 0), coalesce(new.contour, (select id from $model.catchment where st_intersects(geom, new.geom))), coalesce(new.network_type, 'separative'), new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'catchment_node');

            update $model._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_type!='constant_runoff' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)) and (netflow_type!='horner' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)) and (netflow_type!='horner' or (horner_recharge_coef is not null and horner_recharge_coef>=0)) and (netflow_type!='holtan' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)) and (netflow_type!='scs' or (scs_j_mm is not null and scs_j_mm>=0)) and (netflow_type!='scs' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)) and (netflow_type!='scs' or (scs_rfu_mm is not null and scs_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_j_mm is not null and permeable_soil_j_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_rfu_mm is not null and permeable_soil_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_max_inf_rate is not null and permeable_soil_ground_max_inf_rate>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_lag_time_day is not null and permeable_soil_ground_lag_time_day>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_coef_river_exchange is not null and permeable_soil_coef_river_exchange>=0 and permeable_soil_coef_river_exchange<=1)) and (runoff_type is not null) and (runoff_type!='socose' or (socose_tc_mn is not null and socose_tc_mn>0)) and (runoff_type!='socose' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6)) and (runoff_type!='define_k' or (define_k_mn is not null and define_k_mn>0)) and (q_limit is not null) and (q0 is not null) and (network_type is not null) and (rural_land_use is null or rural_land_use>=0) and (industrial_land_use is null or industrial_land_use>=0) and (suburban_housing_land_use is null or suburban_housing_land_use>=0) and (dense_housing_land_use is null or dense_housing_land_use>=0) from  $model._catchment_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.network_type, new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use) is distinct from (old.area_ha, old.rl, old.slope, old.c_imp, old.netflow_type, old.constant_runoff, old.horner_ini_loss_coef, old.horner_recharge_coef, old.holtan_sat_inf_rate_mmh, old.holtan_dry_inf_rate_mmh, old.holtan_soil_storage_cap_mm, old.scs_j_mm, old.scs_soil_drainage_time_day, old.scs_rfu_mm, old.permeable_soil_j_mm, old.permeable_soil_rfu_mm, old.permeable_soil_ground_max_inf_rate, old.permeable_soil_ground_lag_time_day, old.permeable_soil_coef_river_exchange, old.runoff_type, old.socose_tc_mn, old.socose_shape_param_beta, old.define_k_mn, old.q_limit, old.q0, old.contour, old.network_type, old.rural_land_use, old.industrial_land_use, old.suburban_housing_land_use, old.dense_housing_land_use)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area_ha, old.rl, old.slope, old.c_imp, old.netflow_type, old.constant_runoff, old.horner_ini_loss_coef, old.horner_recharge_coef, old.holtan_sat_inf_rate_mmh, old.holtan_dry_inf_rate_mmh, old.holtan_soil_storage_cap_mm, old.scs_j_mm, old.scs_soil_drainage_time_day, old.scs_rfu_mm, old.permeable_soil_j_mm, old.permeable_soil_rfu_mm, old.permeable_soil_ground_max_inf_rate, old.permeable_soil_ground_lag_time_day, old.permeable_soil_coef_river_exchange, old.runoff_type, old.socose_tc_mn, old.socose_shape_param_beta, old.define_k_mn, old.q_limit, old.q0, old.contour, old.network_type, old.rural_land_use, old.industrial_land_use, old.suburban_housing_land_use, old.dense_housing_land_use) as o, (select new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.network_type, new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.network_type, new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._catchment_node set area_ha=new.area_ha, rl=new.rl, slope=new.slope, c_imp=new.c_imp, netflow_type=new.netflow_type, constant_runoff=new.constant_runoff, horner_ini_loss_coef=new.horner_ini_loss_coef, horner_recharge_coef=new.horner_recharge_coef, holtan_sat_inf_rate_mmh=new.holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh=new.holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm=new.holtan_soil_storage_cap_mm, scs_j_mm=new.scs_j_mm, scs_soil_drainage_time_day=new.scs_soil_drainage_time_day, scs_rfu_mm=new.scs_rfu_mm, permeable_soil_j_mm=new.permeable_soil_j_mm, permeable_soil_rfu_mm=new.permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate=new.permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day=new.permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange=new.permeable_soil_coef_river_exchange, runoff_type=new.runoff_type, socose_tc_mn=new.socose_tc_mn, socose_shape_param_beta=new.socose_shape_param_beta, define_k_mn=new.define_k_mn, q_limit=new.q_limit, q0=new.q0, contour=new.contour, network_type=new.network_type, rural_land_use=new.rural_land_use, industrial_land_use=new.industrial_land_use, suburban_housing_land_use=new.suburban_housing_land_use, dense_housing_land_use=new.dense_housing_land_use where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'catchment_node');

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;

            update $model._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_type!='constant_runoff' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)) and (netflow_type!='horner' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)) and (netflow_type!='horner' or (horner_recharge_coef is not null and horner_recharge_coef>=0)) and (netflow_type!='holtan' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)) and (netflow_type!='scs' or (scs_j_mm is not null and scs_j_mm>=0)) and (netflow_type!='scs' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)) and (netflow_type!='scs' or (scs_rfu_mm is not null and scs_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_j_mm is not null and permeable_soil_j_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_rfu_mm is not null and permeable_soil_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_max_inf_rate is not null and permeable_soil_ground_max_inf_rate>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_lag_time_day is not null and permeable_soil_ground_lag_time_day>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_coef_river_exchange is not null and permeable_soil_coef_river_exchange>=0 and permeable_soil_coef_river_exchange<=1)) and (runoff_type is not null) and (runoff_type!='socose' or (socose_tc_mn is not null and socose_tc_mn>0)) and (runoff_type!='socose' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6)) and (runoff_type!='define_k' or (define_k_mn is not null and define_k_mn>0)) and (q_limit is not null) and (q0 is not null) and (network_type is not null) and (rural_land_use is null or rural_land_use>=0) and (industrial_land_use is null or industrial_land_use>=0) and (suburban_housing_land_use is null or suburban_housing_land_use>=0) and (dense_housing_land_use is null or dense_housing_land_use>=0) from  $model._catchment_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._catchment_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;


create or replace function $model.storage_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('storage', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'storage') where name = 'define_later' and id = id_;
            insert into $model._storage_node(id, node_type, zs_array, zini, contour)
                values (id_, 'storage', new.zs_array, new.zini, coalesce(new.contour, (select id from $model.coverage where st_intersects(geom, new.geom))));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'storage_node');

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            update $model._node set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1)<=10 ) and (array_length(zs_array, 1) >=1) and (array_length(zs_array, 2)=2) from  $model._storage_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.zs_array, new.zini) is distinct from (old.zs_array, old.zini)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.zs_array, old.zini) as o, (select new.zs_array, new.zini) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.zs_array, new.zini) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._storage_node set zs_array=new.zs_array, zini=new.zini, contour=new.contour where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'storage_node');

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            update $model._node set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1)<=10 ) and (array_length(zs_array, 1) >=1) and (array_length(zs_array, 2)=2) from  $model._storage_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' and (select trigger_coverage from $model.metadata) then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            delete from $model._storage_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;


create or replace function $model.manhole_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('manhole', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'manhole') where name = 'define_later' and id = id_;
            insert into $model._manhole_node(id, node_type, area, z_ground, cover_diameter, cover_critical_pressure, inlet_width, inlet_height, connection_law)
                values (id_, 'manhole', coalesce(new.area, 1), coalesce(new.z_ground, (select project.altitude(new.geom))), coalesce(new.cover_diameter, 0.1), coalesce(new.cover_critical_pressure, 2.0), coalesce(new.inlet_width, 1.0), coalesce(new.inlet_height, 0.25), coalesce(new.connection_law, 'manhole_cover'));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'manhole_node');

            update $model._node set validity = (select (connection_law is not null) and (connection_law != 'manhole_cover' or cover_diameter is not null) and (connection_law != 'manhole_cover' or cover_diameter > 0) and (connection_law != 'manhole_cover' or cover_critical_pressure is not null) and (connection_law != 'manhole_cover' or cover_critical_pressure >0) and (connection_law != 'drainage_inlet' or inlet_width is not null) and (connection_law != 'drainage_inlet' or inlet_width >0) and (connection_law != 'drainage_inlet' or inlet_height is not null) and (connection_law != 'drainage_inlet' or inlet_height >0) and (area is not null) and (area>0) and (z_ground is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) and ((select exists (select 1 from $model.pipe_link where up=new.id or down=new.id))) from  $model._manhole_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area, new.z_ground, new.cover_diameter, new.cover_critical_pressure, new.inlet_width, new.inlet_height, new.connection_law) is distinct from (old.area, old.z_ground, old.cover_diameter, old.cover_critical_pressure, old.inlet_width, old.inlet_height, old.connection_law)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.z_ground, old.cover_diameter, old.cover_critical_pressure, old.inlet_width, old.inlet_height, old.connection_law) as o, (select new.area, new.z_ground, new.cover_diameter, new.cover_critical_pressure, new.inlet_width, new.inlet_height, new.connection_law) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.z_ground, new.cover_diameter, new.cover_critical_pressure, new.inlet_width, new.inlet_height, new.connection_law) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._manhole_node set area=new.area, z_ground=new.z_ground, cover_diameter=new.cover_diameter, cover_critical_pressure=new.cover_critical_pressure, inlet_width=new.inlet_width, inlet_height=new.inlet_height, connection_law=new.connection_law where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'manhole_node');

            update $model._node set validity = (select (connection_law is not null) and (connection_law != 'manhole_cover' or cover_diameter is not null) and (connection_law != 'manhole_cover' or cover_diameter > 0) and (connection_law != 'manhole_cover' or cover_critical_pressure is not null) and (connection_law != 'manhole_cover' or cover_critical_pressure >0) and (connection_law != 'drainage_inlet' or inlet_width is not null) and (connection_law != 'drainage_inlet' or inlet_width >0) and (connection_law != 'drainage_inlet' or inlet_height is not null) and (connection_law != 'drainage_inlet' or inlet_height >0) and (area is not null) and (area>0) and (z_ground is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) and ((select exists (select 1 from $model.pipe_link where up=new.id or down=new.id))) from  $model._manhole_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            delete from $model._manhole_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

-- recreate invalid view (cascade-deleted)
create or replace view $model.invalid as
    with node_invalidity_reason AS (
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   (area > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h IS NOT NULL THEN ''::text
                    ELSE '   h is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h >= 0::double precision THEN ''::text
                    ELSE '   h>=0   '::text
                END AS reason
           FROM $model.crossroad_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((
                CASE
                    WHEN new_1.connection_law IS NOT NULL THEN ''::text
                    ELSE '   connection_law is not null   '::text
                END ||
                CASE
                    WHEN new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type OR new_1.cover_diameter IS NOT NULL THEN ''::text
                    ELSE '   connection_law != ''manhole_cover'' or cover_diameter is not null   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type OR new_1.cover_diameter > 0::double precision THEN ''::text
                    ELSE '   connection_law != ''manhole_cover'' or cover_diameter > 0   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type OR new_1.cover_critical_pressure IS NOT NULL THEN ''::text
                    ELSE '   connection_law != ''manhole_cover'' or cover_critical_pressure is not null   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type OR new_1.cover_critical_pressure > 0::double precision THEN ''::text
                    ELSE '   connection_law != ''manhole_cover'' or cover_critical_pressure >0   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type OR new_1.inlet_width IS NOT NULL THEN ''::text
                    ELSE '   connection_law != ''drainage_inlet'' or inlet_width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type OR new_1.inlet_width > 0::double precision THEN ''::text
                    ELSE '   connection_law != ''drainage_inlet'' or inlet_width >0   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type OR new_1.inlet_height IS NOT NULL THEN ''::text
                    ELSE '   connection_law != ''drainage_inlet'' or inlet_height is not null   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type OR new_1.inlet_height > 0::double precision THEN ''::text
                    ELSE '   connection_law != ''drainage_inlet'' or inlet_height >0   '::text
                END) ||
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END) ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END) ||
                CASE
                    WHEN ( SELECT NOT (EXISTS ( SELECT 1
                               FROM $model.station
                              WHERE st_intersects(station.geom, new_1.geom)))) THEN ''::text
                    ELSE '   (select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))   '::text
                END) ||
                CASE
                    WHEN ( SELECT (EXISTS ( SELECT 1
                               FROM $model.pipe_link
                              WHERE pipe_link.up = new_1.id OR pipe_link.down = new_1.id)) AS "exists") THEN ''::text
                    ELSE '   (select exists (select 1 from $model.pipe_link where up=new.id or down=new.id))   '::text
                END AS reason
           FROM $model.manhole_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END ||
                CASE
                    WHEN new_1.zini IS NOT NULL THEN ''::text
                    ELSE '   zini is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10    '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1) >=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END AS reason
           FROM $model.storage_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   (area > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END AS reason
           FROM $model.manhole_hydrology_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.zb IS NOT NULL THEN ''::text
                    ELSE '   zb is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk > 0::double precision THEN ''::text
                    ELSE '   rk>0   '::text
                END AS reason
           FROM $model.elem_2d_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((((((((((((((((((
                CASE
                    WHEN new_1.area_ha IS NOT NULL THEN ''::text
                    ELSE '   area_ha is not null   '::text
                END ||
                CASE
                    WHEN new_1.area_ha > 0::double precision THEN ''::text
                    ELSE '   area_ha>0   '::text
                END) ||
                CASE
                    WHEN new_1.rl IS NOT NULL THEN ''::text
                    ELSE '   rl is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rl > 0::double precision THEN ''::text
                    ELSE '   rl>0   '::text
                END) ||
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp IS NOT NULL THEN ''::text
                    ELSE '   c_imp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp >= 0::double precision THEN ''::text
                    ELSE '   c_imp>=0   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp <= 1::double precision THEN ''::text
                    ELSE '   c_imp<=1   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type IS NOT NULL THEN ''::text
                    ELSE '   netflow_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'constant_runoff'::hydra_netflow_type OR new_1.constant_runoff IS NOT NULL AND new_1.constant_runoff >= 0::double precision AND new_1.constant_runoff <= 1::double precision THEN ''::text
                    ELSE '   netflow_type!=''constant_runoff'' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'horner'::hydra_netflow_type OR new_1.horner_ini_loss_coef IS NOT NULL AND new_1.horner_ini_loss_coef >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''horner'' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'horner'::hydra_netflow_type OR new_1.horner_recharge_coef IS NOT NULL AND new_1.horner_recharge_coef >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''horner'' or (horner_recharge_coef is not null and horner_recharge_coef>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'holtan'::hydra_netflow_type OR new_1.holtan_sat_inf_rate_mmh IS NOT NULL AND new_1.holtan_sat_inf_rate_mmh >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''holtan'' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'holtan'::hydra_netflow_type OR new_1.holtan_dry_inf_rate_mmh IS NOT NULL AND new_1.holtan_dry_inf_rate_mmh >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''holtan'' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'holtan'::hydra_netflow_type OR new_1.holtan_soil_storage_cap_mm IS NOT NULL AND new_1.holtan_soil_storage_cap_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''holtan'' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'scs'::hydra_netflow_type OR new_1.scs_j_mm IS NOT NULL AND new_1.scs_j_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''scs'' or (scs_j_mm is not null and scs_j_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'scs'::hydra_netflow_type OR new_1.scs_soil_drainage_time_day IS NOT NULL AND new_1.scs_soil_drainage_time_day >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''scs'' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'scs'::hydra_netflow_type OR new_1.scs_rfu_mm IS NOT NULL AND new_1.scs_rfu_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''scs'' or (scs_rfu_mm is not null and scs_rfu_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'permeable_soil'::hydra_netflow_type OR new_1.permeable_soil_j_mm IS NOT NULL AND new_1.permeable_soil_j_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''permeable_soil'' or (permeable_soil_j_mm is not null and permeable_soil_j_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'permeable_soil'::hydra_netflow_type OR new_1.permeable_soil_rfu_mm IS NOT NULL AND new_1.permeable_soil_rfu_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''permeable_soil'' or (permeable_soil_rfu_mm is not null and permeable_soil_rfu_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'permeable_soil'::hydra_netflow_type OR new_1.permeable_soil_ground_max_inf_rate IS NOT NULL AND new_1.permeable_soil_ground_max_inf_rate >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''permeable_soil'' or (permeable_soil_ground_max_inf_rate is not null and permeable_soil_ground_max_inf_rate>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'permeable_soil'::hydra_netflow_type OR new_1.permeable_soil_ground_lag_time_day IS NOT NULL AND new_1.permeable_soil_ground_lag_time_day >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''permeable_soil'' or (permeable_soil_ground_lag_time_day is not null and permeable_soil_ground_lag_time_day>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'permeable_soil'::hydra_netflow_type OR new_1.permeable_soil_coef_river_exchange IS NOT NULL AND new_1.permeable_soil_coef_river_exchange >= 0::double precision AND new_1.permeable_soil_coef_river_exchange <= 1::double precision THEN ''::text
                    ELSE '   netflow_type!=''permeable_soil'' or (permeable_soil_coef_river_exchange is not null and permeable_soil_coef_river_exchange>=0 and permeable_soil_coef_river_exchange<=1)   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type IS NOT NULL THEN ''::text
                    ELSE '   runoff_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type <> 'socose'::hydra_runoff_type OR new_1.socose_tc_mn IS NOT NULL AND new_1.socose_tc_mn > 0::double precision THEN ''::text
                    ELSE '   runoff_type!=''socose'' or (socose_tc_mn is not null and socose_tc_mn>0)   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type <> 'socose'::hydra_runoff_type OR new_1.socose_shape_param_beta IS NOT NULL AND new_1.socose_shape_param_beta >= 1::double precision AND new_1.socose_shape_param_beta <= 6::double precision THEN ''::text
                    ELSE '   runoff_type!=''socose'' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6)   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type <> 'define_k'::hydra_runoff_type OR new_1.define_k_mn IS NOT NULL AND new_1.define_k_mn > 0::double precision THEN ''::text
                    ELSE '   runoff_type!=''define_k'' or (define_k_mn is not null and define_k_mn>0)   '::text
                END) ||
                CASE
                    WHEN new_1.q_limit IS NOT NULL THEN ''::text
                    ELSE '   q_limit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q0 IS NOT NULL THEN ''::text
                    ELSE '   q0 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.network_type IS NOT NULL THEN ''::text
                    ELSE '   network_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rural_land_use IS NULL OR new_1.rural_land_use >= 0::double precision THEN ''::text
                    ELSE '   rural_land_use is null or rural_land_use>=0   '::text
                END) ||
                CASE
                    WHEN new_1.industrial_land_use IS NULL OR new_1.industrial_land_use >= 0::double precision THEN ''::text
                    ELSE '   industrial_land_use is null or industrial_land_use>=0   '::text
                END) ||
                CASE
                    WHEN new_1.suburban_housing_land_use IS NULL OR new_1.suburban_housing_land_use >= 0::double precision THEN ''::text
                    ELSE '   suburban_housing_land_use is null or suburban_housing_land_use>=0   '::text
                END) ||
                CASE
                    WHEN new_1.dense_housing_land_use IS NULL OR new_1.dense_housing_land_use >= 0::double precision THEN ''::text
                    ELSE '   dense_housing_land_use is null or dense_housing_land_use>=0   '::text
                END AS reason
           FROM $model.catchment_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END ||
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END) ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.reach IS NOT NULL THEN ''::text
                    ELSE '   reach is not null   '::text
                END) ||
                CASE
                    WHEN ( SELECT NOT (EXISTS ( SELECT 1
                               FROM $model.station
                              WHERE st_intersects(station.geom, new_1.geom)))) THEN ''::text
                    ELSE '   (select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))   '::text
                END AS reason
           FROM $model.river_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END) ||
                CASE
                    WHEN new_1.station IS NOT NULL THEN ''::text
                    ELSE '   station is not null    '::text
                END) ||
                CASE
                    WHEN ( SELECT st_intersects(new_1.geom, station.geom) AS st_intersects
                       FROM $model.station
                      WHERE station.id = new_1.station) THEN ''::text
                    ELSE '   (select st_intersects(new.geom, geom) from $model.station where id=new.station)   '::text
                END AS reason
           FROM $model.station_node new_1
          WHERE NOT new_1.validity
        ), link_invalidity_reason AS (
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.lateral_contraction_coef IS NOT NULL THEN ''::text
                    ELSE '   lateral_contraction_coef is not null   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef <= 1::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef<=1   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef >= 0::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef>=0   '::text
                END) ||
                CASE
                    WHEN st_npoints(new_1.border) = 2 THEN ''::text
                    ELSE '   ST_NPoints(border)=2   '::text
                END) ||
                CASE
                    WHEN st_isvalid(new_1.border) THEN ''::text
                    ELSE '   ST_IsValid(border)   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.mesh_2d_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((
                CASE
                    WHEN new_1.z_overflow IS NOT NULL THEN ''::text
                    ELSE '   z_overflow is not null   '::text
                END ||
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END) ||
                CASE
                    WHEN new_1.area >= 0::double precision THEN ''::text
                    ELSE '   area>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.network_overflow_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null    '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.weir_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.law_type IS NOT NULL THEN ''::text
                    ELSE '   law_type is not null   '::text
                END ||
                CASE
                    WHEN new_1.param IS NOT NULL THEN ''::text
                    ELSE '   param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.borda_headloss_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((
                CASE
                    WHEN new_1.z_invert_up IS NOT NULL THEN ''::text
                    ELSE '   z_invert_up is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_invert_down IS NOT NULL THEN ''::text
                    ELSE '   z_invert_down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h_sable IS NOT NULL THEN ''::text
                    ELSE '   h_sable is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'valley'::hydra_cross_section_type THEN ''::text
                    ELSE '   cross_section_type not in (''valley'')   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk > 0::double precision THEN ''::text
                    ELSE '   rk >0   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'circular'::hydra_cross_section_type OR new_1.circular_diameter IS NOT NULL AND new_1.circular_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''circular'' or (circular_diameter is not null and circular_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_top_diameter IS NOT NULL AND new_1.ovoid_top_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_invert_diameter IS NOT NULL AND new_1.ovoid_invert_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_height IS NOT NULL AND new_1.ovoid_height > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_height is not null and ovoid_height >0 )   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_height > ((new_1.ovoid_top_diameter + new_1.ovoid_invert_diameter) / 2::double precision) THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'pipe'::hydra_cross_section_type OR new_1.cp_geom IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type!=''pipe'' or cp_geom is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'channel'::hydra_cross_section_type OR new_1.op_geom IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type!=''channel'' or op_geom is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.pipe_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN new_1.z_crest1 IS NOT NULL THEN ''::text
                    ELSE '   z_crest1 is not null   '::text
                END ||
                CASE
                    WHEN new_1.width1 IS NOT NULL THEN ''::text
                    ELSE '   width1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width1 > 0::double precision THEN ''::text
                    ELSE '   width1>0   '::text
                END) ||
                CASE
                    WHEN new_1.length IS NOT NULL THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN new_1.length > 0::double precision THEN ''::text
                    ELSE '   length>0   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk >= 0::double precision THEN ''::text
                    ELSE '   rk>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 IS NOT NULL THEN ''::text
                    ELSE '   z_crest2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 > new_1.z_crest1 THEN ''::text
                    ELSE '   z_crest2>z_crest1   '::text
                END) ||
                CASE
                    WHEN new_1.width2 IS NOT NULL THEN ''::text
                    ELSE '   width2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width2 > new_1.width1 THEN ''::text
                    ELSE '   width2>width1   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.strickler_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert_stop IS NOT NULL THEN ''::text
                    ELSE '   z_invert_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling_stop IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.dt_regul_hr IS NOT NULL THEN ''::text
                    ELSE '   dt_regul_hr is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_control_node IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_pid_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_tz_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR new_1.q_z_crit IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul OR new_1.nr_z_gate IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.regul_gate_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling > new_1.z_invert THEN ''::text
                    ELSE '   z_ceiling>z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc <=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc >=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_valve IS NOT NULL THEN ''::text
                    ELSE '   mode_valve is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate IS NOT NULL THEN ''::text
                    ELSE '   z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate >= new_1.z_invert THEN ''::text
                    ELSE '   z_gate>=z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate <= new_1.z_ceiling THEN ''::text
                    ELSE '   z_gate<=z_ceiling   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.gate_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.transmitivity IS NOT NULL THEN ''::text
                    ELSE '   transmitivity is not null   '::text
                END) ||
                CASE
                    WHEN new_1.transmitivity >= 0::double precision THEN ''::text
                    ELSE '   transmitivity>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.porous_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((
                CASE
                    WHEN new_1.q_pump IS NOT NULL THEN ''::text
                    ELSE '   q_pump is not null   '::text
                END ||
                CASE
                    WHEN new_1.q_pump >= 0::double precision THEN ''::text
                    ELSE '   q_pump>= 0   '::text
                END) ||
                CASE
                    WHEN new_1.qz_array IS NOT NULL THEN ''::text
                    ELSE '   qz_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(qz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN ( SELECT bool_and(data_set.bool) AS bool_and
                       FROM ( SELECT 0::double precision <= unnest(new_1.qz_array[:][1:1]) AS bool) data_set) THEN ''::text
                    ELSE '   (/*   qz_array: 0<=q   */ select bool_and(bool) from (select 0<=unnest(new.qz_array[:][1]) as bool) as data_set)   '::text
                END) ||
                CASE
                    WHEN ( SELECT bool_and(data_set.bool) AS bool_and
                       FROM ( SELECT unnest(new_1.qz_array[:][1:1]) <= 1::double precision AS bool) data_set) THEN ''::text
                    ELSE '   (/*   qz_array: q<=1   */ select bool_and(bool) from (select unnest(new.qz_array[:][1])<=1 as bool) as data_set)   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.deriv_pump_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((((((((((((((((
                CASE
                    WHEN new_1.z_crest1 IS NOT NULL THEN ''::text
                    ELSE '   z_crest1 is not null   '::text
                END ||
                CASE
                    WHEN new_1.width1 IS NOT NULL THEN ''::text
                    ELSE '   width1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width1 >= 0::double precision THEN ''::text
                    ELSE '   width1>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 IS NOT NULL THEN ''::text
                    ELSE '   z_crest2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 >= new_1.z_crest1 THEN ''::text
                    ELSE '   z_crest2>=z_crest1   '::text
                END) ||
                CASE
                    WHEN new_1.width2 IS NOT NULL THEN ''::text
                    ELSE '   width2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width2 >= 0::double precision THEN ''::text
                    ELSE '   width2>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef IS NOT NULL THEN ''::text
                    ELSE '   lateral_contraction_coef is not null   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef <= 1::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef<=1   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef >= 0::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef>=0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode IS NOT NULL THEN ''::text
                    ELSE '   break_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode OR new_1.z_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''zw_critical'' or z_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode OR new_1.t_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''time_critical'' or t_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.width_breach IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or width_breach is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or z_invert is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or grp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp > 0 THEN ''::text
                    ELSE '   break_mode=''none'' or grp>0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp < 100 THEN ''::text
                    ELSE '   break_mode=''none'' or grp<100   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.dt_fracw_array IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or dt_fracw_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) <= 10 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) >= 1 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 2) = 2 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.overflow_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.down_type = 'manhole_hydrology'::hydra_node_type OR (new_1.down_type = ANY (ARRAY['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'storage'::hydra_node_type, 'crossroad'::hydra_node_type])) AND new_1.hydrograph IS NOT NULL THEN ''::text
                    ELSE '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'', ''storage'', ''crossroad'') and (hydrograph is not null))   '::text
                END ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.connector_hydrology_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN new_1.npump IS NOT NULL THEN ''::text
                    ELSE '   npump is not null   '::text
                END ||
                CASE
                    WHEN new_1.npump <= 10 THEN ''::text
                    ELSE '   npump<=10   '::text
                END) ||
                CASE
                    WHEN new_1.npump >= 1 THEN ''::text
                    ELSE '   npump>=1   '::text
                END) ||
                CASE
                    WHEN new_1.zregul_array IS NOT NULL THEN ''::text
                    ELSE '   zregul_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.hq_array IS NOT NULL THEN ''::text
                    ELSE '   hq_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zregul_array, 1) = new_1.npump THEN ''::text
                    ELSE '   array_length(zregul_array, 1)=npump   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zregul_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zregul_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 1) = new_1.npump THEN ''::text
                    ELSE '   array_length(hq_array, 1)=npump   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 2) <= 10 THEN ''::text
                    ELSE '   array_length(hq_array, 2)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 2) >= 1 THEN ''::text
                    ELSE '   array_length(hq_array, 2)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 3) = 2 THEN ''::text
                    ELSE '   array_length(hq_array, 3)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.pump_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= '0'::numeric::double precision THEN ''::text
                    ELSE '   width>=0.   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= '0'::numeric::double precision THEN ''::text
                    ELSE '   cc>=0.   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode IS NOT NULL THEN ''::text
                    ELSE '   break_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode OR new_1.z_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''zw_critical'' or z_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode OR new_1.t_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''time_critical'' or t_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or grp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp > 0 THEN ''::text
                    ELSE '   break_mode=''none'' or grp>0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp < 100 THEN ''::text
                    ELSE '   break_mode=''none'' or grp<100   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.dt_fracw_array IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or dt_fracw_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) <= 10 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) >= 1 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 2) = 2 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.fuse_spillway_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.cross_section IS NOT NULL THEN ''::text
                    ELSE '   cross_section is not null   '::text
                END ||
                CASE
                    WHEN new_1.cross_section >= 0::double precision THEN ''::text
                    ELSE '   cross_section>=0   '::text
                END) ||
                CASE
                    WHEN new_1.length IS NOT NULL THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN new_1.length >= 0::double precision THEN ''::text
                    ELSE '   length>=0   '::text
                END) ||
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type = 'manhole_hydrology'::hydra_node_type OR (new_1.down_type = ANY (ARRAY['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'crossroad'::hydra_node_type, 'storage'::hydra_node_type])) AND new_1.hydrograph IS NOT NULL THEN ''::text
                    ELSE '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'', ''crossroad'', ''storage'') and (hydrograph is not null))   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.routing_hydrology_link new_1
          WHERE NOT new_1.validity
        ), singularity_invalidity_reason AS (
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.z_weir IS NOT NULL THEN ''::text
                    ELSE '   z_weir is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= '0'::numeric::double precision THEN ''::text
                    ELSE '   cc>=0.   '::text
                END AS reason
           FROM $model.weir_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN new_1.storage_area IS NOT NULL THEN ''::text
                    ELSE '   storage_area is not null   '::text
                END ||
                CASE
                    WHEN new_1.storage_area >= 0::double precision THEN ''::text
                    ELSE '   storage_area>=0   '::text
                END) ||
                CASE
                    WHEN new_1.constant_dry_flow IS NOT NULL THEN ''::text
                    ELSE '   constant_dry_flow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.constant_dry_flow >= 0::double precision THEN ''::text
                    ELSE '   constant_dry_flow>=0   '::text
                END) ||
                CASE
                    WHEN new_1.distrib_coef IS NULL OR new_1.distrib_coef >= 0::double precision THEN ''::text
                    ELSE '   distrib_coef is null or distrib_coef>=0   '::text
                END) ||
                CASE
                    WHEN new_1.lag_time_hr IS NULL OR new_1.lag_time_hr >= 0::double precision THEN ''::text
                    ELSE '   lag_time_hr is null or (lag_time_hr>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.sector IS NULL OR new_1.distrib_coef IS NOT NULL AND new_1.lag_time_hr IS NOT NULL THEN ''::text
                    ELSE '   sector is null or (distrib_coef is not null and lag_time_hr is not null)   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.pollution_dryweather_runoff, 1) = 2 THEN ''::text
                    ELSE '   array_length(pollution_dryweather_runoff, 1)=2   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.pollution_dryweather_runoff, 2) = 4 THEN ''::text
                    ELSE '   array_length(pollution_dryweather_runoff, 2)=4   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.quality_dryweather_runoff, 1) = 2 THEN ''::text
                    ELSE '   array_length(quality_dryweather_runoff, 1)=2   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.quality_dryweather_runoff, 2) = 9 THEN ''::text
                    ELSE '   array_length(quality_dryweather_runoff, 2)=9   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR new_1.tq_array IS NOT NULL THEN ''::text
                    ELSE '   external_file_data or tq_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 1) <= 10 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 1) >= 1 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 2) = 2 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 2)=2   '::text
                END AS reason
           FROM $model.hydrograph_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((
                CASE
                    WHEN new_1.zq_array IS NOT NULL THEN ''::text
                    ELSE '   zq_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zq_array, 2)=2   '::text
                END AS reason
           FROM $model.zq_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.drainage IS NOT NULL THEN ''::text
                    ELSE '   drainage is not null   '::text
                END ||
                CASE
                    WHEN new_1.overflow IS NOT NULL THEN ''::text
                    ELSE '   overflow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q_drainage IS NOT NULL THEN ''::text
                    ELSE '   q_drainage is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q_drainage >= 0::double precision THEN ''::text
                    ELSE '   q_drainage>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_ini IS NOT NULL THEN ''::text
                    ELSE '   z_ini is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.treatment_mode IS NOT NULL THEN ''::text
                    ELSE '   treatment_mode is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END AS reason
           FROM $model.reservoir_rs_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
                CASE
                    WHEN new_1.q0 IS NOT NULL THEN ''::text
                    ELSE '   q0 is not null   '::text
                END AS reason
           FROM $model.constant_inflow_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END ||
                CASE
                    WHEN new_1.k IS NOT NULL THEN ''::text
                    ELSE '   k is not null   '::text
                END) ||
                CASE
                    WHEN new_1.k > 0::double precision THEN ''::text
                    ELSE '   k>0   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END AS reason
           FROM $model.strickler_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.pk0_km IS NOT NULL THEN ''::text
                    ELSE '   pk0_km is not null   '::text
                END ||
                CASE
                    WHEN new_1.dx IS NOT NULL THEN ''::text
                    ELSE '   dx is not null   '::text
                END) ||
                CASE
                    WHEN new_1.dx >= 0.1::double precision THEN ''::text
                    ELSE '   dx>=0.1   '::text
                END AS reason
           FROM $model.pipe_branch_marker_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.q_dz_array IS NOT NULL THEN ''::text
                    ELSE '   q_dz_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(q_dz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(q_dz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(q_dz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.param_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null    '::text
                END ||
                CASE
                    WHEN new_1.z_regul IS NOT NULL THEN ''::text
                    ELSE '   z_regul is not null    '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul IS NOT NULL THEN ''::text
                    ELSE '   mode_regul is not null   '::text
                END) ||
                CASE
                    WHEN new_1.reoxy_law IS NOT NULL THEN ''::text
                    ELSE '   reoxy_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.reoxy_param IS NOT NULL THEN ''::text
                    ELSE '   reoxy_param is not null   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.zregul_weir_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.d_abutment_l IS NOT NULL THEN ''::text
                    ELSE '   d_abutment_l is not null   '::text
                END ||
                CASE
                    WHEN new_1.d_abutment_r IS NOT NULL THEN ''::text
                    ELSE '   d_abutment_r is not null   '::text
                END) ||
                CASE
                    WHEN new_1.abutment_type IS NOT NULL THEN ''::text
                    ELSE '   abutment_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zw_array IS NOT NULL THEN ''::text
                    ELSE '   zw_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.bradley_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert_stop IS NOT NULL THEN ''::text
                    ELSE '   z_invert_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling_stop IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.dt_regul_hr IS NOT NULL THEN ''::text
                    ELSE '   dt_regul_hr is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_control_node IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_pid_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_tz_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR new_1.q_z_crit IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul OR new_1.nr_z_gate IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                END AS reason
           FROM $model.regul_sluice_gate_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.law_type IS NOT NULL THEN ''::text
                    ELSE '   law_type is not null   '::text
                END ||
                CASE
                    WHEN new_1.param IS NOT NULL THEN ''::text
                    ELSE '   param is not null   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.borda_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.drainage IS NOT NULL THEN ''::text
                    ELSE '   drainage is not null   '::text
                END ||
                CASE
                    WHEN new_1.overflow IS NOT NULL THEN ''::text
                    ELSE '   overflow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ini IS NOT NULL THEN ''::text
                    ELSE '   z_ini is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zr_sr_qf_qs_array IS NOT NULL THEN ''::text
                    ELSE '   zr_sr_qf_qs_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.treatment_mode IS NOT NULL THEN ''::text
                    ELSE '   treatment_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.treatment_param IS NOT NULL THEN ''::text
                    ELSE '   treatment_param is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 2) = 4 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 2)=4   '::text
                END AS reason
           FROM $model.reservoir_rsp_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling > new_1.z_invert THEN ''::text
                    ELSE '   z_ceiling>z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc <=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc >=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_valve IS NOT NULL THEN ''::text
                    ELSE '   mode_valve is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate IS NOT NULL THEN ''::text
                    ELSE '   z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate >= new_1.z_invert THEN ''::text
                    ELSE '   z_gate>=z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate <= new_1.z_ceiling THEN ''::text
                    ELSE '   z_gate<=z_ceiling   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.gate_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END ||
                CASE
                    WHEN new_1.zini IS NOT NULL THEN ''::text
                    ELSE '   zini is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.treatment_mode IS NOT NULL THEN ''::text
                    ELSE '   treatment_mode is not null   '::text
                END AS reason
           FROM $model.tank_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((
                CASE
                    WHEN new_1.downstream IS NOT NULL THEN ''::text
                    ELSE '   downstream is not null   '::text
                END ||
                CASE
                    WHEN new_1.split1 IS NOT NULL THEN ''::text
                    ELSE '   split1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.downstream_param IS NOT NULL THEN ''::text
                    ELSE '   downstream_param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1_law IS NOT NULL THEN ''::text
                    ELSE '   split1_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1_param IS NOT NULL THEN ''::text
                    ELSE '   split1_param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NULL OR new_1.split2_law IS NOT NULL THEN ''::text
                    ELSE '   split2 is null or split2_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NULL OR new_1.split2_param IS NOT NULL THEN ''::text
                    ELSE '   split2 is null or split2_param is not null   '::text
                END AS reason
           FROM $model.zq_split_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((
                CASE
                    WHEN new_1.external_file_data OR new_1.tz_array IS NOT NULL THEN ''::text
                    ELSE '   external_file_data or tz_array is not null   '::text
                END ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 1) <= 10 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 1) >= 1 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 2) = 2 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 2)=2   '::text
                END AS reason
           FROM $model.tz_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((
                CASE
                    WHEN new_1.l_road IS NOT NULL THEN ''::text
                    ELSE '   l_road is not null   '::text
                END ||
                CASE
                    WHEN new_1.l_road >= 0::double precision THEN ''::text
                    ELSE '   l_road>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_road IS NOT NULL THEN ''::text
                    ELSE '   z_road is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zw_array IS NOT NULL THEN ''::text
                    ELSE '   zw_array is not null    '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.bridge_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.qz_array IS NOT NULL THEN ''::text
                    ELSE '   qz_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(qz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.hydraulic_cut_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.cascade_mode IS NOT NULL THEN ''::text
                    ELSE '   cascade_mode is not null   '::text
                END ||
                CASE
                    WHEN new_1.cascade_mode = 'hydrograph'::hydra_model_connect_mode OR new_1.zq_array IS NOT NULL THEN ''::text
                    ELSE '   cascade_mode=''hydrograph'' or zq_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) <= 100 THEN ''::text
                    ELSE '   array_length(zq_array, 1)<=100   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zq_array, 2)=2   '::text
                END AS reason
           FROM $model.model_connect_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.qq_array IS NOT NULL THEN ''::text
                    ELSE '   qq_array is not null   '::text
                END ||
                CASE
                    WHEN new_1.downstream IS NOT NULL THEN ''::text
                    ELSE '   downstream is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1 IS NOT NULL THEN ''::text
                    ELSE '   split1 is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qq_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NOT NULL AND array_length(new_1.qq_array, 2) = 3 OR new_1.split2 IS NULL AND array_length(new_1.qq_array, 2) = 2 THEN ''::text
                    ELSE '   (split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2)   '::text
                END AS reason
           FROM $model.qq_split_hydrology_singularity new_1
          WHERE NOT new_1.validity
        )
    select CONCAT('node:', n.id) as
        id,
        validity,
        name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom,
        reason from $model._node as n join node_invalidity_reason as r on r.id=n.id
        where validity=FALSE
    union
    select CONCAT('link:', l.id) as
        id,
        validity,
        name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom,
        reason from $model._link as l join link_invalidity_reason as r on  r.id=l.id
        where validity=FALSE
    union
    select CONCAT('singularity:', s.id) as
        id,
        s.validity,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        reason from $model._singularity as s join $model._node as n on s.node = n.id join singularity_invalidity_reason as r on r.id=s.id
        where s.validity=FALSE
    union
    select CONCAT('profile:', p.id) as
        id,
        p.validity,
        p.name,
        'profile'::varchar as type,
        ''::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        ''::varchar as reason from $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where p.validity=FALSE
;;

-- recreate results_links view (cascade-deleted)
create or replace view ${model}.results_link as
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.gate_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.regul_gate_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.weir_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.borda_headloss_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.pump_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.deriv_pump_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.fuse_spillway_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.connector_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.strickler_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.porous_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.overflow_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.network_overflow_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.mesh_2d_link
        union
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.street_link
;;

-- Surface elements
create or replace view ${model}.results_surface as
    with
        dump as (
            select st_collect(n.geom) as points, s.geom as envelope, s.id
            from ${model}.coverage as s, ${model}.crossroad_node as n
            where s.domain_type='street' and st_intersects(s.geom, n.geom)
            group by s.id
        ),
        voronoi as (
            select st_intersection(st_setsrid((st_dump(st_VoronoiPolygons(dump.points, 0, dump.envelope))).geom, ${srid}), dump.envelope) as geom
            from dump
        )
    select e.id as id, UPPER(e.name)::varchar(24) as name, e.contour as geom from ${model}.elem_2d_node as e
        union
    select n.id as id, UPPER(n.name)::varchar(24) as name, c.geom as geom from ${model}.storage_node as n join ${model}.coverage as c on n.contour=c.id
        union
    select n.id as id, UPPER(n.name)::varchar(24) as name, voronoi.geom as geom from voronoi, ${model}.crossroad_node as n where st_intersects(voronoi.geom, n.geom)
    order by id asc;
;;

/* ********************************************** */
/*  Coverages changes                             */
/* ********************************************** */

create or replace function $model.coverage_update()
returns integer
language plpgsql
as $$$$
    declare
        res_ integer;
    begin
        delete from $model.coverage ; -- where domain_2d is null;
        alter sequence $model.coverage_id_seq restart;

        with cst as (
            select (ST_Dump(coalesce(ST_Split(a.discretized,
                    (select ST_Collect(discretized) from $model.constrain as b where a.id!=b.id and ST_Intersects(a.discretized, b.discretized))
                    ),
                a.discretized))).geom as geom
            from $model.constrain as a
            )
        insert into $model.coverage(geom, domain_type) select (st_dump(st_polygonize(geom))).geom, '2d' from cst;


        with oned as (
            select c.id
            from $model.coverage as c, $model.street as d
            where ST_Intersects(c.geom, d.geom)
            group by c.id
        )
        update $model.coverage set domain_type='street' where id in (select oned.id from oned);

        update $model.coverage as c set domain_type='reach'
        where exists (select 1 from $model.open_reach as r
        where ST_Intersects(c.geom, r.geom)
        and ST_Length(ST_CollectionExtract(ST_Intersection(c.geom, r.geom), 2)) > 0);

        with oned as (
            select distinct c.id
            from $model.coverage as c, $model.storage_node as d
            where ST_Intersects(c.geom, d.geom)
        )
        update $model.coverage set domain_type='storage' where id in (select oned.id from oned);

        with oned as (
            select c.id, count(1) as ct
            from $model.coverage as c, $model.coverage_marker as mkc
            where ST_Intersects(c.geom, mkc.geom)
            group by c.id
        )
        update $model.coverage set domain_type=null where id in (select oned.id from oned where ct=1);

        select count(*) from $model.coverage into res_;
        return res_;
    end;
$$$$
;;

create or replace function ${model}.crossroad_update()
returns integer
language plpgsql
as $$$$
    declare
        res_ integer;
    begin
        with nodes as (
            insert into $model.crossroad_node(geom)
                (select distinct s.geom
                    from (
                        select (st_dumppoints(geom)).geom as geom
                        from $model.street
                            union
                        select (st_dumppoints(st_intersection(s1.geom, s2.geom))).geom as geom
                        from $model.street as s1, $model.street as s2
                        where ST_intersects(s1.geom,s2.geom)
                        and s1.id<s2.id
                    ) as s
                    left join (select geom from $model.crossroad_node) as c
                    on s.geom=c.geom
                    where c.geom is null)
            returning id
        )
        select count(1) from nodes into res_;
        return res_;
    end;
$$$$
;;

create or replace function ${model}.gen_cst_street(street_id integer)
returns integer
language plpgsql
as $$$$
    declare
        _street_geom geometry;
        _street_width real;
        _street_el real;
        _trigger_state boolean;
    begin
        select geom, width, elem_length
        from $model.street
        where id=street_id
        into _street_geom, _street_width, _street_el;

        select trigger_coverage from $model.metadata into _trigger_state;
        update $model.metadata set trigger_coverage = 'f';

        with buf as (
                select ST_Buffer(ST_Force2d(_street_geom), _street_width/2, 'quad_segs=1 join=mitre mitre_limit='||(_street_width*2)::varchar) as geom
            ),
            cst as (
                select ST_ExteriorRing(geom) as geom from buf
            ),
            snp as (
                update $model.constrain set geom = ST_Snap(geom, _street_geom, 1)
                where ST_DWithin(geom, _street_geom, 1)
            ),
            spl as (
                select coalesce(ST_Split(cst.geom, (select ST_Collect(ST_Force2d(c.geom))
                                                    from $model.constrain as c
                                                    where ST_Intersects(c.geom, cst.geom))
                                                    ), cst.geom) as geom
                from cst
            ),
            spld as (
                select ST_Force3d((st_dump(geom)).geom) as geom from spl
            ),
            diff as (
                select id, (ST_Dump(ST_CollectionExtract(ST_Split(c.geom, b.geom), 2))).geom as geom
                from $model.constrain as c, cst as b
                where ST_Intersects(b.geom, c.geom)
            ),
            ins as (
                insert into $model.constrain(geom, elem_length, constrain_type)
                select d.geom, c.elem_length, c.constrain_type
                from $model.constrain as c, diff as d
                where c.id = d.id
                union
                select geom, _street_el, 'overflow'
                from spld
                returning id
            )
        delete from $model.constrain
        where id in (select id from diff);

        with cst_brut as (
                select a.id, (ST_Dump(coalesce(ST_Split(a.discretized,
                        (select ST_Collect(discretized) from $model.constrain as b where a.id!=b.id and ST_Intersects(a.discretized, b.discretized))
                        ),
                    a.discretized))).geom as geom
                from $model.constrain as a
            ),
            cst as (
                select id, ST_Union(geom) over (partition by id) as geom from cst_brut
            ),
            polyg as (
                select (ST_Dump(ST_Polygonize(cst_brut.geom))).geom as geom from cst_brut
            ),
            inter as (
                select p.geom from polyg as p, $model.street as s
                where ST_Intersects(p.geom, s.geom)
            ),
            un as (
                select ST_Union(i.geom) as geom from inter as i
            ),
            boundary as (
                select ST_Boundary(u.geom) as geom from un as u
            ),
            to_del as (
                select id from cst as c
                where (select ST_Dimension(ST_Intersection(c.geom, b.geom))=0 from boundary as b)
                and (select ST_Contains(u.geom, c.geom) from un as u)
            )
            delete from $model.constrain as c
            where exists (select 1 from to_del as d where d.id=c.id limit 1)
            and not exists (select 1 from $model.valley_cross_section_geometry where transect=c.id limit 1);

        if _trigger_state then
            perform $model.coverage_update();
        end if;

        update $model.metadata set trigger_coverage = _trigger_state;

        return 1;
    end;
$$$$
;;

-- regen coverages to remove crossroads/reach_end
do
$$$$
    declare
        _trigger_state boolean;
    begin
        select trigger_coverage from $model.metadata into _trigger_state;
        update $model.metadata set trigger_coverage = 'f';

        with cst_brut as (
                select a.id, (ST_Dump(coalesce(ST_Split(a.discretized,
                        (select ST_Collect(discretized) from $model.constrain as b where a.id!=b.id and ST_Intersects(a.discretized, b.discretized))
                        ),
                    a.discretized))).geom as geom
                from $model.constrain as a
            ),
            cst as (
                select id, ST_Union(geom) over (partition by id) as geom from cst_brut
            ),
            polyg as (
                select (ST_Dump(ST_Polygonize(cst_brut.geom))).geom as geom from cst_brut
            ),
            inter as (
                select p.geom from polyg as p, $model.street as s
                where ST_Intersects(p.geom, s.geom)
            ),
            un as (
                select ST_Union(i.geom) as geom from inter as i
            ),
            boundary as (
                select ST_Boundary(u.geom) as geom from un as u
            ),
            to_del as (
                select id from cst as c
                where (select ST_Dimension(ST_Intersection(c.geom, b.geom))=0 from boundary as b)
                and (select ST_Contains(u.geom, c.geom) from un as u)
            )
            delete from $model.constrain as c
            where exists (select 1 from to_del as d where d.id=c.id limit 1)
            and not exists (select 1 from $model.valley_cross_section_geometry where transect=c.id limit 1);


        if _trigger_state then
            perform $model.coverage_update();
        end if;

        update $model.metadata set trigger_coverage = _trigger_state;
    end;
$$$$
;;

/* ********************************************** */
/*                River transects                 */
/* ********************************************** */

-- for any point close to riverbank, give the transect interpolated at this point
-- for points on the border bewteen two reach coverage, you can specify the coverage id
create or replace function ${model}.interpolate_transect_at(point geometry, coverage_id integer default null)
returns geometry('LINESTRINGZ', $srid)
language plpgsql
immutable
as $$$$
    declare
        l_ real;
        coverage geometry;
        reach geometry;
        res geometry;
        left_transects geometry;
        right_transects geometry;
        closest geometry;
        direction vector2;
        other_direction vector2;
        other_side geometry;
        side varchar;
    begin
        l_ := 10000;

        --raise notice 'point %', point;
        select ST_CollectionHomogenize(ST_Collect(ST_MakeLine(array[ST_StartPoint(discretized), ST_ClosestPoint(discretized, reach), ST_EndPoint(discretized)])))
        from ${model}.constrain
        where constrain_type='flood_plain_transect'
        and ST_Intersects(discretized, point)
        into res;

        if ST_GeometryType(res) = 'ST_LineString' then
            return (
                select ST_MakeLine(array[ST_StartPoint(res), ST_ClosestPoint(r.geom, res), ST_EndPoint(res)])
                from ${model}.reach r
                order by r.geom <-> res
                limit 1
            );
        end if;

        -- closest coverage or specified one
        select coalesce(
            (select geom from ${model}.coverage where id=coverage_id),
            (select geom from ${model}.coverage where domain_type='reach' order by geom <-> point limit 1)
        ) into coverage;

        if not ST_DWithin(coverage, point, .1) then
            raise 'point % is not close enought to nearest coverage %', point, coverage ;
        end if;

        -- closest reach in coverage
        select geom from ${model}.reach where ST_Intersects(geom, coverage) order by geom <-> point limit 1 into reach;

        if reach is null then
            raise 'no reach in reach coverage %', coverage ;
        end if;

        with pt as (
            select ST_StartPoint(t.geom) as s, ST_ClosestPoint(t.geom, reach) as m, ST_EndPoint(t.geom) as e
            from ${model}.constrain as t
            where ST_Intersects(coverage, t.discretized)
            and t.constrain_type='flood_plain_transect'
            and ST_Length(ST_CollectionExtract(ST_Intersection(t.discretized, coverage), 2)) > 0
            order by ST_Distance(geom, reach)
            limit 2
        )
        select ST_Collect(ST_MakeLine(s, m)), ST_Collect(ST_MakeLine(e, m))
        from pt
        into left_transects, right_transects;

        if ST_NumGeometries(left_transects) = 0 then
            return ST_MakeLine(point, ST_ClosestPoint(reach, point));
        end if;

        select ST_ClosestPoint(reach, point) into closest;
        if dot(difference(ST_EndPoint(ST_GeometryN(right_transects,1)), ST_StartPoint(ST_GeometryN(right_transects,1))), difference(closest, point)) > 0 then
            side := 'right';
        else
            side := 'left';
        end if;

        if ST_NumGeometries(left_transects) = 1 then
            if side = 'right' then
                direction := difference(ST_EndPoint(ST_GeometryN(right_transects,1)), ST_StartPoint(ST_GeometryN(right_transects,1)));
            else
                direction := difference(ST_EndPoint(ST_GeometryN(left_transects,1)), ST_StartPoint(ST_GeometryN(left_transects,1)));
            end if;
            other_direction := direction;
        else
            if side = 'right' then
                direction := interpolate_direction( ST_GeometryN(right_transects,1), ST_GeometryN(right_transects,2), point);
            else
                direction := interpolate_direction( ST_GeometryN(left_transects,1), ST_GeometryN(left_transects,2), point);
            end if;
        end if;

        closest := coalesce(ST_ClosestPoint(ST_Intersection(ST_MakeLine(point, ST_Translate(point, l_*direction.x, l_*direction.y)), reach), point), closest);
        --raise notice 'point % closest %', point, closest;

        if ST_NumGeometries(left_transects) = 1 then
            other_direction := row(-direction.x, -direction.y)::vector2;
        else
            if side = 'right' then
                other_direction := interpolate_direction( ST_GeometryN(left_transects,1), ST_GeometryN(left_transects,2), closest);
            else
                other_direction := interpolate_direction( ST_GeometryN(right_transects,1), ST_GeometryN(right_transects,2), closest);
            end if;
        end if;

        other_side := ST_ClosestPoint(ST_Intersection(ST_MakeLine(closest, ST_Translate(closest, -l_*other_direction.x, -l_*other_direction.y)), ST_ExteriorRing(coverage)), closest);

        if side = 'right' then
            return ST_MakeLine(array[other_side, closest, point]);
        else
            return ST_MakeLine(array[point, closest, other_side]);
        end if;

    end;
$$$$
;;

create or replace function ${model}.valley_section_at(point geometry)
returns real[]
language plpgsql
as
$$$$
declare
    res real[];
begin
    with reach as (
        select id, geom, ST_LineLocatePoint(geom, point) t from ${model}.reach where ST_DWithin(geom, point, .1)
    ),
    up_pt as (
        select zb_cat(u.zbmaj_lbank_array, u.zbmin_array, u.zbmaj_rbank_array) as zb, ST_LineLocatePoint(r.geom, p.geom) as t
        from ${model}.river_cross_section_profile as p
        join ${model}.river_node as n on n.id=p.id
        join ${model}.valley_cross_section_geometry as u on coalesce(p.down_vcs_geom, p.up_vcs_geom) = u.id,
        reach r
        where r.id=n.reach
        and ST_LineLocatePoint(r.geom, p.geom) <= ST_LineLocatePoint(r.geom, point)
        order by ST_LineLocatePoint(r.geom, p.geom) desc limit 1
    ),
    down_pt as (
        select zb_cat(u.zbmaj_lbank_array, u.zbmin_array, u.zbmaj_rbank_array) as zb, ST_LineLocatePoint(r.geom, p.geom) as t
        from ${model}.river_cross_section_profile as p
        join ${model}.river_node as n on n.id=p.id
        join ${model}.valley_cross_section_geometry as u on coalesce(p.up_vcs_geom, p.down_vcs_geom) = u.id,
        reach r
        where r.id=n.reach
        and ST_LineLocatePoint(r.geom, p.geom) >= ST_LineLocatePoint(r.geom, point)
        order by ST_LineLocatePoint(r.geom, p.geom) asc limit 1
    )
    select coalesce(
        (select interpolate_array( u.zb, d.zb, case when d.t-u.t != 0 then (r.t-u.t)/(d.t-u.t) else 0 end) from reach r, up_pt u, down_pt d),
        (select u.zb from reach r, up_pt u),
        (select d.zb from reach r, down_pt d)
    ) into res;

    if res is null then
        select zb_cat(u.zbmaj_lbank_array, u.zbmin_array, u.zbmaj_rbank_array)
        from ${model}.river_cross_section_profile as p
        join ${model}.valley_cross_section_geometry as u on coalesce(p.up_vcs_geom, p.down_vcs_geom) = u.id
        order by point <-> p.geom
        limit 1
        into res;
        if res is null then
            raise 'unable to compute valey section at %', point;
        end if;
    end if;
    return res;
end
$$$$
;;

-- if side is specified, line is only 3 points with the middle point on the reach, the fraction is a fraction of this half transect
-- if side is not specified the fraction is a fraction of the transect
create or replace function ${model}.section_interpolate_point(line geometry, section real[], fraction real, side varchar default null)
returns geometry
language plpgsql
immutable
as
$$$$
declare
    l_ real;
    s_ real[];
    z_ real;
    p_ geometry;
    idx integer;
    alp real;
begin
    if side = 'left' then
        l_ := section[4][2] + section[10][2]/2.;
        s_ = array[array[0                                     , section[4][1]],
                   array[l_ - section[3][2] - section[10][2]/2., section[3][1]],
                   array[l_ - section[2][2] - section[10][2]/2., section[2][1]],
                   array[l_ - section[1][2] - section[10][2]/2., section[1][1]],
                   array[l_ - .5*section[10][2], section[10][1]],
                   array[l_ -  .5*section[9][2],  section[9][1]],
                   array[l_ -  .5*section[8][2],  section[8][1]],
                   array[l_ -  .5*section[7][2],  section[7][1]],
                   array[l_ -  .5*section[6][2],  section[6][1]],
                   array[l_ -  .5*section[5][2],  section[5][1]],
                   array[l_ , section[5][1]]
                ];
        p_ := ST_LineInterpolatePoint(ST_MakeLine(ST_StartPoint(line), st_pointn(line, 2)), fraction);
    elsif side = 'right' then
        l_ := section[14][2] + section[10][2]/2.;
        s_ = array[array[0                                      , section[14][1]],
                   array[l_ - section[13][2] - section[10][2]/2., section[13][1]],
                   array[l_ - section[12][2] - section[10][2]/2., section[12][1]],
                   array[l_ - section[11][2] - section[10][2]/2., section[11][1]],
                   array[l_ - .5*section[10][2], section[10][1]],
                   array[l_ -  .5*section[9][2],  section[9][1]],
                   array[l_ -  .5*section[8][2],  section[8][1]],
                   array[l_ -  .5*section[7][2],  section[7][1]],
                   array[l_ -  .5*section[6][2],  section[6][1]],
                   array[l_ -  .5*section[5][2],  section[5][1]],
                   array[l_ , section[5][1]]
                ];
        p_ := ST_LineInterpolatePoint(ST_MakeLine(ST_EndPoint(line), st_pointn(line, 2)), fraction);
    else
        l_ := section[4][2] + section[14][2] + section[10][2];
        s_ = array[array[0                                                   , section[4][1]],
                   array[l_ - section[3][2] - section[10][2] - section[14][2], section[3][1]],
                   array[l_ - section[2][2] - section[10][2] - section[14][2], section[2][1]],
                   array[l_ - section[1][2] - section[10][2] - section[14][2], section[1][1]],
                   array[l_ - section[10][2] - section[14][2], section[10][1]],
                   array[l_ - section[9][2] - section[14][2],  section[9][1]],
                   array[l_ - section[8][2] - section[14][2],  section[8][1]],
                   array[l_ - section[7][2] - section[14][2],  section[7][1]],
                   array[l_ - section[6][2] - section[14][2],  section[6][1]],
                   array[l_ - section[5][2] - section[14][2],  section[5][1]],
                   array[l_ - .5*section[5][2] - section[14][2], section[5][1]],
                   array[l_ - .5*section[6][2] - section[14][2], section[6][1]],
                   array[l_ - .5*section[7][2] - section[14][2], section[7][1]],
                   array[l_ - .5*section[8][2] - section[14][2], section[8][1]],
                   array[l_ - .5*section[9][2] - section[14][2], section[9][1]],
                   array[l_ - .5*section[10][2] - section[14][2], section[10][1]],
                   array[l_ - section[14][2],  section[11][1]],
                   array[l_ - section[14][2] + section[12][2],  section[12][1]],
                   array[l_ - section[14][2] + section[13][2],  section[13][1]],
                   array[l_ ,  section[14][1]]
                ];
        p_ := ST_LineInterpolatePoint(line, fraction);
    end if;

    if l_*fraction = s_[array_length(s_,1)][1] then
        return ST_SetSRID(ST_MakePoint(ST_X(p_), ST_Y(p_), s_[array_length(s_,1)][2]), ST_SRID(line));
    end if;

    select s_[i-1][2]*(1. - (l_*fraction - s_[i-1][1])/(s_[i][1] - s_[i-1][1])) + s_[i][2]*(l_*fraction - s_[i-1][1])/(s_[i][1] - s_[i-1][1]), i, (l_*fraction - s_[i-1][1])/(s_[i][1] - s_[i-1][1])
    from generate_subscripts(s_, 1) as i
    where i > 1
    and s_[i-1][1] <= l_*fraction
    and s_[i][1] > l_*fraction
    into z_, idx, alp;

    if z_ is null then
        raise 'we have a problem % %/% % % % % %', side, l_*fraction, l_, idx, alp, z_, s_, line;
    end if;
    --raise notice 's_ % %', l_*fraction, s_;
    --raise notice 'z_ % % %', z_, idx, alp;
    return ST_SetSRID(ST_MakePoint(ST_X(p_), ST_Y(p_), z_) ,ST_SRID(line));
end
$$$$
;;

create or replace function ${model}.set_reach_point_altitude(point geometry)
returns geometry
language plpgsql
stable
as
$$$$
    declare
        transect geometry;
        section real[];
        reach geometry;
    begin
        transect :=  ${model}.interpolate_transect_at(point);
        select geom from ${model}.reach order by ST_Distance(transect, geom) limit 1 into reach;
        section := ${model}.valley_section_at(ST_ClosestPoint(reach, transect));
        return ST_SetSRID(
            ST_MakePoint(
                ST_X(point),
                ST_Y(point),
                ST_Z(${model}.section_interpolate_point(transect, section, ST_LineLocatePoint(transect, point)::real))
            ),
            ST_SRID(point)
        );
    end;
$$$$
;;

/* ************************************************************** */
/*  Reach types changes: ST_DWithin instead of ST_LineLocatePoint */
/* ************************************************************** */

create or replace view ${model}.open_reach
as with
    ends as (
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'valley' and p.type_cross_section_down != 'valley' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up != 'valley' and p.type_cross_section_down = 'valley' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'valley' and st_dwithin(p.geom, r.geom, .1) and ST_DWithin(p.geom, ST_EndPoint(r.geom), 1)
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_down = 'valley' and st_dwithin(p.geom, r.geom, .1) and ST_DWithin(p.geom, ST_StartPoint(r.geom), 1)
    )
    ,
    bounds as (
        select FIRST_VALUE(reach) over (partition by reach order by alpha), typ, LAG(alpha) over (partition by reach order by alpha) as start, alpha as end, id, reach from ends
    )
    select row_number() over() as id, st_linesubstring(r.geom, b.start, b.end) as geom, r.id as reach from bounds as b join ${model}.reach as r on r.id=b.reach where b.typ = 'down'
;;

create or replace view ${model}.channel_reach
as with
    ends as (
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'channel' and p.type_cross_section_down != 'channel' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up != 'channel' and p.type_cross_section_down = 'channel' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'channel' and st_dwithin(p.geom, r.geom, .1) and ST_DWithin(p.geom, ST_EndPoint(r.geom), 1)
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_down = 'channel' and st_dwithin(p.geom, r.geom, .1) and ST_DWithin(p.geom, ST_StartPoint(r.geom), 1)
    )
    ,
    bounds as (
        select FIRST_VALUE(reach) over (partition by reach order by alpha), typ, LAG(alpha) over (partition by reach order by alpha) as start, alpha as end, id, reach from ends
    )
    select row_number() over() as id, st_linesubstring(r.geom, b.start, b.end) as geom, r.id as reach from bounds as b join ${model}.reach as r on r.id=b.reach where b.typ = 'down'
;;