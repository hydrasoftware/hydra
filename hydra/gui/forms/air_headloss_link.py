# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_link_dialog import SimpleLinkDialog, tr
from qgis.PyQt import uic
import os

class AirHeadlossLinkEditor(SimpleLinkDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleLinkDialog.__init__(self, project, geom, "air_headloss_link", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "air_headloss_link.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        area, up_to_down_headloss_coefficient, down_to_up_headloss_coefficient, comment = self.project.execute("""
            select area, up_to_down_headloss_coefficient, down_to_up_headloss_coefficient, comment
            from {}.air_headloss_link
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.area.setText(f"{area:.2f}" if area is not None else '')
        self.up_to_down_headloss_coefficient.setText(f"{up_to_down_headloss_coefficient:.3f}" if up_to_down_headloss_coefficient is not None else '')
        self.down_to_up_headloss_coefficient.setText(f"{down_to_up_headloss_coefficient:.3f}" if down_to_up_headloss_coefficient is not None else '')
        self.notes.setText(comment)

    def get_id(self):
        return self.id

    def save(self):
        float_or_none = lambda s: float(s) if s else None
        self.project.log.notice(tr("Saved"))
        self.model.update_air_headloss_link(self.id, self.geom,
            float_or_none(self.area.text()),
            float_or_none(self.up_to_down_headloss_coefficient.text()),
            float_or_none(self.down_to_up_headloss_coefficient.text()),
            comment=self.notes.text())
        SimpleLinkDialog.save(self)
        self.close()
