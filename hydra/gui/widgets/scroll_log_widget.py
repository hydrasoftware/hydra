################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import re
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QApplication, QWidget, QScrollArea, QVBoxLayout, QLabel

class ScrollLogger(QWidget):
    def __init__(self, parent=None, night_mode=False):
        QWidget.__init__(self, parent)

        #Container Widget
        self.widget = QWidget()
        p = self.widget.palette()
        if night_mode:
            p.setColor(self.widget.backgroundRole(), Qt.black)
        else:
            p.setColor(self.widget.backgroundRole(), Qt.white)
        self.widget.setAutoFillBackground(True)
        self.widget.setPalette(p)
        #Layout of Container Widget
        lay = QVBoxLayout(self)
        lay.setSpacing(0)
        lay.setMargin(0)
        lay.setAlignment(Qt.AlignTop)
        self.widget.setLayout(lay)

        #Scroll Area Properties
        self.scroll = QScrollArea()
        self.scroll.setWidgetResizable(True)
        self.scroll.setWidget(self.widget)

        #Scroll Area Layer add
        vLayout = QVBoxLayout(self)
        vLayout.setSpacing(0)
        vLayout.setMargin(0)
        vLayout.addWidget(self.scroll)
        self.setLayout(vLayout)

        self.current_label = None

    def add_line(self, text, weight=None, color=None, centered=False):
        ''' add basic line for scrollArea
            use rich text and HTML to format text
            new label can be used to declare a new Qlabel'''
        if weight == "bold":
            text = "<b>" + text + "</b>"
        if color :
            text = """<font color="{}">""".format(color) + text + "</font>"
        if centered :
            text = "<center>" + text + "</center>"

        lines = re.split('\n|<br>', text)

        if self.current_label is None:
            self.new_label()

        self.current_label.append(lines)

        # Below lines are used to have a dynamic logger widget
        old_max = self.scroll.verticalScrollBar().maximum()
        QApplication.processEvents()
        new_max = self.scroll.verticalScrollBar().maximum()
        if new_max > old_max:
            # Scroll to bottom if needed
            self.scroll.verticalScrollBar().setValue(self.scroll.verticalScrollBar().maximum())

    def new_label(self, max_lines=0):
        '''creates a new label to write to.
            Only if no current label to write to or
            if current label has text inside,
            else will not create'''
        if self.current_label is None or len(self.current_label.text())>0:
            self.current_label=CustomLabel(max_lines)
            self.current_label.setTextInteractionFlags(Qt.TextSelectableByMouse)
            self.current_label.setTextFormat(Qt.RichText)
            self.widget.layout().addWidget(self.current_label)

    def clear(self):
        '''clear all labels from widget'''
        for i in reversed(list(range(self.widget.layout().count()))):
            self.widget.layout().itemAt(i).widget().setParent(None)
        self.current_label = None
        QApplication.processEvents()

    def reset_current_label(self):
        self.current_label.reset()

class CustomLabel(QLabel):
    def __init__(self, max_lines=0):
        QLabel.__init__(self)

        self.__max_lines = max_lines
        self.__lines=[]

    def append(self, lines):
        self.__lines += lines
        if self.__max_lines>0:
            self.__lines=self.__lines[self.__max_lines*-1:]
        self.setText('<br>'.join(self.__lines))

    def reset(self):
        self.__lines = []
        self.setText('<br>'.join(self.__lines))
