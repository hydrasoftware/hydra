# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import re
import string
from qgis.PyQt.QtCore import QCoreApplication, Qt
from qgis.PyQt.QtWidgets import QFileDialog, QTableWidget, QLabel, QLineEdit, QVBoxLayout, QHBoxLayout, QComboBox, QTableWidgetItem, QAbstractItemView, QDialogButtonBox, QMessageBox, QWidget, QSpacerItem
from hydra.gui.base_dialog import BaseDialog

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

current_dir = os.path.dirname(__file__)

substitutions = ['model', 'work_table', 'work_table_geom_column']

presentation_dico = { 'model':tr("Target model: "),
                      'work_table':tr("Table in work: "),
                      'work_table_geom_column':tr("Geometry column of work table: "),
                      'list':tr("Liste d'identifiants"),
                    }


class PreComposedQueriesManager(BaseDialog):
    def __init__ (self, project, parent=None):
        BaseDialog.__init__(self,project,parent)

        self.project=project
        self.selected_file=None
        self.selected_queries=[]

        self.init_gui()

        self.models = self.project.get_models()
        if len(self.models) == 1 :
            self.combo_model.addItem(self.models[0])
        elif len(self.models) == 0:
            pass
        else:
            self.models.append("All models")
            self.combo_model.addItems(self.models)

        if self.models and self.project.get_current_model() is not None  :
            self.combo_model.setCurrentIndex(self.combo_model.findText(self.project.get_current_model().name))

        list_work_table = [item for (item,) in self.project.execute("SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = 'work'")]
        self.combo_work_table.addItems(list_work_table)

        self.reload_work_table_geom_combo()

        self.buttonBox.rejected.connect(self.close)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.buttons()[2].clicked.connect(self.run)
        self.list_queries.itemSelectionChanged.connect(self.file_changed) # init in init_gui fct
        self.combo_work_table.currentIndexChanged.connect(self.reload_work_table_geom_combo) # init in init_gui fct

        self.reload_list_queries()

    def init_gui(self):
        self.setWindowTitle('Pre-composed SQL queries')
        self.resize(650,450)

        self.list_queries = QTableWidget()
        self.list_queries.setSelectionMode(QAbstractItemView.SingleSelection)
        self.list_queries.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.label_title = QLabel(tr("Notice : You must have at least one model to perform queries.<br/><br/>Selected SQL query effect:"))
        self.label_info_query = QLabel()

        #create generique combo/label for each substitution_type in substitutions

        for substitution_type in substitutions :

            setattr(self, 'combo_'+substitution_type, QComboBox())
            setattr(self, 'label_'+substitution_type, QLabel(presentation_dico[substitution_type]))

            getattr(self,'combo_'+substitution_type).setVisible(False)
            getattr(self,'label_'+substitution_type).setVisible(False)

            setattr(self,'Hlayout_'+substitution_type, QHBoxLayout())

            getattr(self,'Hlayout_'+substitution_type).addWidget(getattr(self,'label_'+substitution_type))
            getattr(self,'Hlayout_'+substitution_type).addWidget(getattr(self,'combo_'+substitution_type))


        # right side of widget 'label_tittle' and so on
        labelbox = QVBoxLayout()
        labelbox.addWidget(self.label_title)
        labelbox.addWidget(self.label_info_query)

        for substitution_type in substitutions :
            labelbox.addLayout(vars(self)['Hlayout_'+substitution_type])

        self.qedit_list_int = QLineEdit()
        self.label_list_int = QLabel(presentation_dico["list"])

        self.qedit_list_int.setVisible(False)
        self.label_list_int.setVisible(False)

        self.Hlayout_list_int = QHBoxLayout()

        self.Hlayout_list_int.addWidget(self.label_list_int)
        self.Hlayout_list_int.addWidget(self.qedit_list_int)

        labelbox.addLayout(self.Hlayout_list_int)

        labelbox.setAlignment(Qt.AlignTop)

        self.buttonBox = QDialogButtonBox()
        self.buttonBox.addButton("Cancel",QDialogButtonBox.RejectRole)
        self.buttonBox.addButton("Ok",QDialogButtonBox.AcceptRole)
        self.buttonBox.addButton("Run",QDialogButtonBox.ActionRole)

        MainHbox = QHBoxLayout()
        MainHbox.addWidget(self.list_queries)
        MainHbox.addStretch(1)
        MainHbox.addLayout(labelbox)
        MainHbox.addStretch(1)

        MainVbox = QVBoxLayout()
        MainVbox.addLayout(MainHbox)
        MainVbox.addWidget(self.buttonBox)

        self.setLayout(MainVbox)

        self.list_sql_file = [f for f in os.listdir(os.path.join(current_dir,'queries')) if os.path.isfile(os.path.join(current_dir,'queries',f))]

        self.list_queries.setColumnCount(1)
        self.list_queries.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.list_queries.horizontalHeader().hide()
        self.list_queries.verticalHeader().hide()
        self.list_queries.horizontalHeader().setStretchLastSection(True)

    def reload_work_table_geom_combo(self):
        self.combo_work_table_geom_column.clear()
        list_geom_in_work_table = [item for (item,) in self.project.execute("SELECT f_geometry_column from geometry_columns where f_table_name = '{work_table}'".format(work_table=self.combo_work_table.currentText()))]
        self.combo_work_table_geom_column.addItems(list_geom_in_work_table)

    def reload_list_queries(self):
        file_number = len(self.list_sql_file)
        self.list_queries.clear()
        self.list_queries.setRowCount(file_number)
        for i in range (file_number) :
            self.list_queries.setItem(i,0,QTableWidgetItem(self.list_sql_file[i]))

    def file_changed(self):
        self.selected_file = str(self.list_queries.currentItem().text())
        self.selected_queries = self.parse_query(self.selected_file)

        for substitution_type in substitutions :
            keyword_exists = any('$'+substitution_type in query for query in self.selected_queries)
            getattr(self, 'combo_'+substitution_type).setVisible(keyword_exists)
            getattr(self, 'label_'+substitution_type).setVisible(keyword_exists)

        list_in_queries =  any('$list' in query for query in self.selected_queries)
        self.qedit_list_int.setVisible(list_in_queries)
        self.label_list_int.setVisible(list_in_queries)

        self.reload_labels()

    def reload_labels(self):
        with open(os.path.join(current_dir,'queries',self.selected_file),'r') as sqlfile:
            data = sqlfile.read().replace('\n', '')
            match = re.search(r'\/\*\s(.+)\s\*\/', data)
            information = re.sub(' +', ' ', match.group(0).replace(' *//* ', '\n').replace('*','').replace('/','').strip())

        self.label_info_query.setText(information)

    def parse_query(self,fileName):
        return open(os.path.join(current_dir, 'queries', fileName)).read().split(";;")[:-1]

    def run_query_on_model(self,model):
        ''' appelé si le fichier .sql choisi ne contient aucune requête de type select,
        effectue les substitutions et lance les requêtes'''

        subs = {"model":model,
                 "srid":self.project.srid,
                 "work_table":self.combo_work_table.currentText(),
                 "work_table_geom_column":self.combo_work_table_geom_column.currentText(),
                 "list":self.qedit_list_int.text()
                 }

        for query in self.selected_queries:
            self.project.execute(string.Template(query).substitute(subs))

    def run_returning_query_on_model(self,model,filename):
        ''' appelé pour les requêtes de type --returning query-- ,
        écrit le résultat de la requête select et execute les autres'''

        subs = {"model":model,
                 "srid":self.project.srid,
                 "work_table":self.combo_work_table.currentText(),
                 "work_table_geom_column":self.combo_work_table_geom_column.currentText()
                 }

        for query in self.selected_queries:
            if '--returning query--' in query :
                with open(filename,'a') as file :
                    res = self.project.execute(string.Template(query).substitute(subs)).fetchall()
                    for line in res :
                        for elem in line :
                            file.write(str(elem or "-")+";")
                        file.write("\n")
            else :
                self.project.execute(string.Template(query).substitute(subs))

    def run(self):
        '''on regarde la requête choisie, si elle contient des requêtes de type select balisé par --returning query--
           alors on demande un fichier cible à l'utilisateur, toutes les requêtes seront écrites dans ce fichier
           les requêtes de type update sont lancés et applique les changements.'''

        if self.selected_file:
            if self.combo_model.currentText() == "All models" and any('$model' in query for query in self.selected_queries) :
                if any('--returning query--' in query for query in self.selected_queries) :
                    fileName, __ = QFileDialog.getSaveFileName(self, tr('Create result file'), self.project.data_dir, tr("CSV (*.csv)"))
                    if fileName :
                        f = open(fileName, "w").close()
                        for model in self.project.get_models() :
                            self.run_returning_query_on_model(model, fileName)
                else :
                    for model in self.project.get_models() :
                        self.run_query_on_model(model)
            else :
                if any('--returning query--' in query for query in self.selected_queries) :
                    fileName, __ = QFileDialog.getSaveFileName(self, tr('Create result file'), self.project.data_dir, tr("CSV (*.csv)"))
                    if fileName :
                        f = open(fileName, "w").close()
                        self.run_returning_query_on_model(self.combo_model.currentText(), fileName)
                else :
                    self.run_query_on_model(self.combo_model.currentText())
            QMessageBox.information(self, tr('SQL query successful'), tr('Queries from file {} succesfully run.'.format(self.selected_file)), QMessageBox.Ok)

    def save(self):
        BaseDialog.save(self)
        self.close()
