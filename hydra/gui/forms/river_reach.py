# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..base_dialog import BaseDialog, tr
import hydra.utility.string as string
from qgis.PyQt import uic
import os

class RiverReachEditor(BaseDialog):
    def __init__(self, project, geom, id=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "river_reach.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.__model = self.project.get_current_model()
        assert not self.__model == None
        self.__geom = geom
        if id==None:
            self.__id = self.__model.add_reachpath(self.__geom)
            self.__update_river_node(geom[0])
            self.__update_river_node(geom[-1])
        else:
            self.__id = id
            if self.__geom is None:
                geom,= self.project.execute("""
                select geom
                from {}.reach
                where id={}""".format(self.__model.name, self.__id)).fetchone()

        name, pk0_km, dx, comment = self.project.execute("""
            select name, pk0_km, dx, comment
            from {}.reach
            where id={}""".format(self.__model.name, self.__id)).fetchone()
        self.name.setText(name)
        self.pk0_km.setText(string.get_str(pk0_km))
        self.dx.setText(string.get_str(dx))
        self.notes.setText(comment)

    def __update_river_node(self, geom_node):
        self.project.execute("""
                update {}.river_node set z_ground={}
                where st_force2d(geom)='srid={}; point({} {})'::geometry""".format(self.__model.name,
                str(geom_node[2]),self.project.srid, str(geom_node[0]),
                str(geom_node[1])))

    def get_id(self):
        return self.__id

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.__model.update_reachpath(self.__id, self.__geom, string.get_sql_float(self.dx.text()), string.get_sql_float(self.pk0_km.text()), self.name.text(), comment=self.notes.text())
        BaseDialog.save(self)
        self.close()
