# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from __future__ import unicode_literals
import os
import re
import unicodedata
import webbrowser
from qgis.PyQt import uic, QtGui, QtCore
from qgis.PyQt.QtWidgets import QTableWidgetItem, QFileDialog, QMessageBox, QApplication
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from hydra.gui.base_dialog import BaseDialog, tr
from hydra.utility.process import ExecRunner, HydraExecError
from hydra.gui.widgets.scroll_log_widget import ScrollLogger
import hydra.utility.string as string
from hydra.utility.system import open_external


class ExtractError(Exception):
    pass

class Extract(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        self.current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(self.current_dir, "extract.ui"), self)

        self.table_control_files = HydraTableWidget(project,
            ("id", "ctrl_file"),
            (tr("Id"), tr("Control File")),
            "project", "extract",
            (self.new_control_file, self.delete_control_file, self.edit_control_file, self.help_control_file), "", "id",
            self.table_control_files_placeholder, showEdit=True, showHelp=True, file_columns=[1])
        self.table_control_files.set_editable(False)
        self.table_control_files.table.itemSelectionChanged.connect(self.__selection_changed)

        self.logger = ScrollLogger(self.logger_placeholder, night_mode=True)
        self.logger.setFixedSize(400, 500)
        self.__selection_changed()

        self.go_extract.clicked.connect(self.run_extract)
        self.go_wexpdess.clicked.connect(self.run_wexpdess)
        self.buttonBox.accepted.connect(self.save)
        self.log_btn.clicked.connect(self.open_log_file)

    def __selection_changed(self):
        self.log_btn.setEnabled(False)
        self.go_wexpdess.setEnabled(False)
        items = self.table_control_files.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_control_files.table.row(items[0])
            file = self.table_control_files.table.item(selected_row,1).text()
            directory=os.path.dirname(file)

            log_file = os.path.abspath(os.path.join(directory, 'extract.log'))
            self.log_btn.setEnabled(os.path.isfile(log_file))
            extract_res_file = os.path.splitext(file)[0]+'.mst'

            self.log_btn.setEnabled(os.path.isfile(log_file))
            self.go_wexpdess.setEnabled(os.path.isfile(extract_res_file))

    def new_control_file(self):
        file_url, __ = QFileDialog.getSaveFileName(self, tr('Select extract control file'), self.project.data_dir, options=QFileDialog.DontConfirmOverwrite)
        if file_url:
            # creates file empty if not exists
            open(file_url, 'a').close()
            self.project.execute("""insert into project.extract(ctrl_file) values('{}');""".format(self.project.pack_path(file_url)))
            self.table_control_files.update_data()

    def delete_control_file(self):
        items = self.table_control_files.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_control_files.table.row(items[0])
            id = int(self.table_control_files.table.item(selected_row,0).text())
            self.project.execute("""delete from project.extract where id={};""".format(id))
            self.table_control_files.update_data()

    def edit_control_file(self):
        items = self.table_control_files.table.selectedItems()
        if len(items)>0:
            file = items[1].text()
            open_external(file)

    def help_control_file(self):
        webbrowser.open('http://hydra-software.net/docs/notes_techniques/NT31.pdf', new=0)

    def __output_log(self, text):
        ''' add line for extract output logging'''
        text = '\n'.join(re.split('\n+', text.strip()))
        keep_words = ['extract.exe','*', '*fin']
        if text:
            if "output:" in text:
                self.logger.new_label(20)
                self.logger.add_line(text, None, color='#ffffff')
            elif any(words in text for words in keep_words):
                self.logger.reset_current_label()
                self.logger.new_label()
                self.logger.add_line(text, 'bold', color='#ffffff')
            else:
                self.logger.add_line(text, None, color='#ffffff')

    def run_extract(self):
        exe = os.path.abspath(os.path.join(self.current_dir, "extract.exe"))

        if not os.path.isfile(exe):
            raise ExtractError("Executable not found: {}".format(exe))

        items = self.table_control_files.table.selectedItems()
        if len(items)>0:
            self.logger.clear()
            selected_row = self.table_control_files.table.row(items[0])
            file = self.table_control_files.table.item(selected_row,1).text()
            outfile=os.path.join(os.path.dirname(file), 'extract')
            runner = ExecRunner([exe], log=self.project.log)
            runner.output.connect(self.__output_log)

            try:
                runner.run_cmd_outfile(arguments=[file], file=outfile, directory=os.path.dirname(file))
                QMessageBox.information(self, tr('Extract successful'), tr("""Extract run successfully with control file {}.""").format(file), QMessageBox.Ok)
            except HydraExecError as error: # do not try to catch everything, otherwise debugging is a pain in the neck !
                QMessageBox.critical(self, tr('Extract error'), tr("""An error occured during extract with file {}.\nSee hydra.log file for extract logs.\n""").format(file), QMessageBox.Ok)

    def run_wexpdess(self):
        exe = os.path.abspath(os.path.join(self.current_dir, "wexpdess.exe"))

        if not os.path.isfile(exe):
            raise ExtractError("Executable not found: {}".format(exe))

        items = self.table_control_files.table.selectedItems()
        if len(items)>0:
            self.logger.clear()
            selected_row = self.table_control_files.table.row(items[0])
            file = self.table_control_files.table.item(selected_row,1).text()
            infile = os.path.splitext(file)[0]+'.mst'
            outfile=os.path.join(os.path.dirname(file), 'expdess')
            runner = ExecRunner([exe, infile], log=self.project.log)
            runner.output.connect(self.__output_log)
            runner.run_cmd_outfile(arguments=[file], file=outfile, directory=os.path.dirname(file))


    def open_log_file(self):
        items = self.table_control_files.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_control_files.table.row(items[0])
            file = self.table_control_files.table.item(selected_row,1).text()
            directory=os.path.dirname(file)
            log_file = os.path.abspath(os.path.join(directory, 'extract.log'))
            if os.path.isfile(log_file):
                open_external(log_file)

    def save(self):
        BaseDialog.save(self)
        self.close()