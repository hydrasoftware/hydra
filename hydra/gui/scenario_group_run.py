# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import re
from qgis.PyQt import uic
from qgis.PyQt.QtGui import QColor
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QDialog, QApplication, QWidget, QVBoxLayout, QTableWidgetItem, QPushButton, QMessageBox, QAbstractItemView
from hydra.gui.base_dialog import tr
from hydra.gui.widgets.scroll_log_widget import ScrollLogger
from hydra.database.export_calcul import ExportCalcul
from hydra.database.export_carto_data import ExportCartoData
from hydra.kernel import Kernel

class ScenarioGroupRun(QDialog):
    def __init__(self, project, group_id, parent=None):
        QDialog.__init__(self, parent)

        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "scenario_group_run.ui"), self)

        self.project = project
        self.group_id = group_id

        group_name, = self.project.execute("""
            select name
            from project.scenario_group
            where id={id}""".format(id=self.group_id)).fetchone()
        self.lbl_group_name.setText(group_name)

        self.btnRunAll.clicked.connect(self.run_all)
        self.btnRunNotCalculated.clicked.connect(self.run_not_calculated)

        self.table.setSelectionMode(QAbstractItemView.SingleSelection)
        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.table.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.update_scenarios()

        self.log_scroll = ScrollLogger()
        self.log_lay = QVBoxLayout()
        self.log_lay.addWidget(self.log_scroll)
        self.log_placeholder.setLayout(self.log_lay)

        self.output_scroll = ScrollLogger(night_mode=True)
        self.output_lay = QVBoxLayout()
        self.output_lay.addWidget(self.output_scroll)
        self.output_placeholder.setLayout(self.output_lay)

        self.__runner = None

        self.show()

    def closeEvent(self, evnt):
        '''stops computation process at close event'''
        if self.__runner:
            self.__runner.kill_process()
        QApplication.restoreOverrideCursor()
        QDialog.closeEvent(self, evnt)

    def run_all(self):
        '''starts computing all scenarios from group sequentially'''
        self.log_scroll.clear()
        self.output_scroll.clear()

        scenarios = self.project.execute("""
                select id, name
                from project.scenario as scn
                where scn.id in
                    (select scenario
                    from project.param_scenario_group
                    where grp=%i)
                order by scn.name asc"""%(self.group_id)).fetchall()

        if scenarios:
            self.project.unload_csv_results_layers()
            self.project.save()

            for scn in scenarios:
                self.run_scenario(scn)
                self.__log_separator()

    def run_not_calculated(self):
        '''starts computing only not properly runned scenarios from group sequentially'''
        self.log_scroll.clear()
        self.output_scroll.clear()

        if self.table.rowCount():
            self.project.unload_csv_results_layers()
            self.project.save()

            for i in range(0, self.table.rowCount()):
                scn = self.project.execute("""
                    select id, name
                    from project.scenario as scn
                    where scn.name='{name}'""".format(name =self.table.item(i, 0).text())).fetchone()

                runner = Kernel(self.project, scn[0], parent=self)
                err, message, date = runner.get_status()
                if err!=0:
                    self.run_scenario(scn)
                    self.__log_separator()

    def run_scenario(self, scn):
        '''runs specified scenario_group
            - scn=(id, name)'''
        self.output_scroll.clear()
        self.__log("Starting {}".format(scn[1]))
        self.__log("{} processing ok".format(scn[1]))
        self.__log("Exporting {}".format(scn[1]))

        # export topological data for calculation
        exporter = ExportCalcul(self.project)
        exporter.export(scn[0])
        # export cartograpic data for triangulation
        exporter = ExportCartoData(self.project)
        exporter.export(scn[0])
        self.__log("Export ok".format(scn[1]))

        # run calculations
        try:
            self.__runner = Kernel(self.project, scn[0], parent=self)
            self.__output_log("{} output:".format(scn[1]))
            self.__runner.log.connect(self.__log)
            self.__runner.output.connect(self.__output_log)
            self.__runner.run()
            QApplication.processEvents()
        except Exception as e:
            QMessageBox.critical(self, tr('Error during computation'), tr("An error occured during {}:\n    {}").format(scn[1], str(e)), QMessageBox.Ok)

        self.update_scenarios()

    def make_run_scenario(self, id, name):

        def run():
            self.log_scroll.clear()
            self.output_scroll.clear()

            self.run_scenario((id, name))

        return run

    def __log(self, text):
        ''' add line for macro logging (big steps in group scenario run)'''
        text = '\n'.join(re.split('\n+', text.strip()))
        if text:
            weight = None
            color = None
            if "error" in text :
                color = "red"
                weight = "bold"
                self.project.log.error(text)
                raise KernelError(text)
            elif "ok" in text:
                weight = "bold"
                self.project.log.notice(text)
            self.log_scroll.add_line(text, weight, color, centered="- - - " in text)

    def __log_separator(self):
        ''' add separator in log'''
        self.log_scroll.new_label()
        self.__log("- - - "*12)
        self.log_scroll.new_label()

    def __output_log(self, text):
        ''' add line for kernel output logging'''
        text = '\n'.join(re.split('\n+', text.strip()))
        if text:
            weight = None
            if "output" in text :
                self.output_scroll.new_label()
                self.output_scroll.add_line(text, weight='bold', color='#ffffff')
            else:
                self.output_scroll.add_line(text, color='#ffffff')
            if "PHASE CALCUL TRANSITOIRE" in text:
                self.output_scroll.new_label(10)

    def update_scenarios(self):
        scenarios = self.project.execute("""
            select id, name
            from project.scenario as scn
            where scn.id in
                (select scenario
                from project.param_scenario_group
                where grp=%i)
			order by scn.name asc"""%(self.group_id)).fetchall()

        self.table.setRowCount(0)
        for scn in scenarios:
            self.add_scenario(scn)

    def add_scenario(self, scn):
        rowcount = self.table.rowCount() + 1
        self.table.setRowCount(rowcount)
        row = rowcount - 1

        item = QTableWidgetItem(scn[1])
        self.table.setItem(row, 0, item)

        runner = Kernel(self.project, scn[0], None)
        err, message, date = runner.get_status()

        item = QTableWidgetItem(message)
        self.table.setItem(row, 1, item)
        if err==-1: #not calculated
            self.table.item(row, 1).setForeground(QColor(255,0,0))
        elif err!=0: #error
            self.table.item(row, 1).setBackground(QColor(255,0,0))

        item = QTableWidgetItem(date)
        self.table.setItem(row, 2, item)

        btn = QPushButton(self.table)
        btn.setText(tr("Run") + " " + scn[1])
        btn.clicked.connect(self.make_run_scenario(int(scn[0]), scn[1]))
        self.table.setCellWidget(row, 3, btn)

class KernelError(Exception):
    pass

if __name__ == '__main__':
    from hydra.project import Project
    import sys
    app = QApplication(sys.argv)
    obj_project = Project.load_project("scngrp")
    run_dlg = ScenarioGroupRun(obj_project, 2)
    run_dlg.exec_()
