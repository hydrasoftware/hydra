/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update hydra.metadata set version = '3.7';
;;

alter table project.param_external_hydrograph add column rank_ integer not null default 1
;;

alter table project.param_regulation add column rank_ integer not null default 1
;;

alter table project.param_measure add column rank_ integer not null default 1
;;

create type hydra_soil_infiltration_type as enum ('rate', 'split')
;;

create table hydra.soil_infiltration_type(
    name hydra_soil_infiltration_type primary key,
    id integer,
    description varchar)
;;

insert into hydra.soil_infiltration_type(name, id, description)
values
('rate', 1, 'Soil infiltration rate'),
('split', 2, 'Split infiltration/runoff')
;;

create type hydra_aquifer_infiltration_type as enum ('split', 'rate')
;;

create table hydra.aquifer_infiltration_type(
    name hydra_aquifer_infiltration_type primary key,
    id integer,
    description varchar)
;;

insert into hydra.aquifer_infiltration_type(name, id, description)
values
('split', 1, 'Aquifer/river split coefficient'),
('rate', 2, 'Aquifer infiltration rate')
;;

update hydra.netflow_type set description='Hydra' where name='hydra'
;;

update hydra.netflow_type set description='Obsolete' where name='hydra_permeable'
;;

alter type hydra_runoff_type rename value 'k_desbordes' to 'Desbordes 1 Cr'
;;
alter type hydra_runoff_type rename value 'passini' to 'Passini'
;;
alter type hydra_runoff_type rename value 'giandotti' to 'Giandotti'
;;
alter type hydra_runoff_type rename value 'socose' to 'Define Tc'
;;
alter type hydra_runoff_type rename value 'define_k' to 'Define K'
;;
--isolated--
alter type hydra_runoff_type add value 'Turraza'
;;
--isolated--
alter type hydra_runoff_type add value 'Ventura'
;;
--isolated--
alter type hydra_runoff_type add value 'Kirpich'
;;
--isolated--
alter type hydra_runoff_type add value 'Cemagref'
;;
--isolated--
alter type hydra_runoff_type add value 'Mockus'
;;
--isolated--
alter type hydra_runoff_type add value 'Desbordes 1 Cimp'
;;
--isolated--
alter type hydra_runoff_type add value 'Desbordes 2 Cimp'
;;
--isolated--
alter type hydra_runoff_type add value 'Krajewski'
;;

update hydra.runoff_type set description='0.4ha < S < 5000ha, 0.2 < C < 1, 100m < L < 18000m, 0.2% < I < 15%' where name='Desbordes 1 Cr'
;;
update hydra.runoff_type set description='4000ha < S < 7000ha' where name='Passini'
;;
update hydra.runoff_type set description='S > 4000ha' where name='Giandotti'
;;
update hydra.runoff_type set description='Concentration time Tc for socose unit hydrograph' where name='Define Tc'
;;
update hydra.runoff_type set description='Lag time K for linear reservoir' where name='Define K'
;;
insert into hydra.runoff_type(name, id, description) values
('Turraza', 6, ''),
('Ventura', 7, 'S > 1000ha'),
('Kirpich', 8, '4ha < S < 80ha, 3% < pente < 10%, sol argileux'),
('Cemagref', 9, 'Petits bassins ruraux'),
('Mockus', 10, '4ha < S < 1000ha, pente<1%, sols limoneux/argileux'),
('Desbordes 1 Cimp', 11, '0.4ha < S < 5000ha, 0.2 < C < 1, 100m < L < 18000m, 0.2% < I < 15%'),
('Desbordes 2 Cimp', 12, '0.4ha < S < 5000ha, 0.2 < C < 1, 100m < L < 18000m, 0.2% < I < 15%'),
('Krajewski', 13, '0.4ha < S < 5000ha, 0.2 < C < 1, 100m < L < 18000m, 0.2% < I < 15%')
;;

