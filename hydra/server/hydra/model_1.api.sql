/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*              Schema model                      */
/* ********************************************** */


-- Trigger to remove properly config
create function ${model}.config_before_delete_fct()
returns trigger
language plpgsql
as $$$$
    begin
        update $model._node set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._node set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;
        update $model._link set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._link set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;
        update $model._singularity set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._singularity set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;
        update $model._river_cross_section_profile set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._river_cross_section_profile set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;

        delete from project.config_scenario where model='$model' and configuration=old.id;
        delete from project.config_serie where model='$model' and configuration=old.id;

        return old;
    end;
$$$$
;;

create trigger ${model}_config_before_delete_trig
    before delete on $model.configuration
       for each row execute procedure ${model}.config_before_delete_fct()
;;

-- Trigger to rename properly config
create function ${model}.config_rename_fct()
returns trigger
language plpgsql
as $$$$
    begin
        update $model._node set configuration=(configuration::jsonb - old.name || jsonb_build_object(new.name, (configuration->old.name)))::json where configuration is not null and configuration::jsonb ? old.name;
        update $model._link set configuration=(configuration::jsonb - old.name || jsonb_build_object(new.name, (configuration->old.name)))::json where configuration is not null and configuration::jsonb ? old.name;
        update $model._singularity set configuration=(configuration::jsonb - old.name || jsonb_build_object(new.name, (configuration->old.name)))::json where configuration is not null and configuration::jsonb ? old.name;
        update $model._river_cross_section_profile set configuration=(configuration::jsonb - old.name || jsonb_build_object(new.name, (configuration->old.name)))::json where configuration is not null and configuration::jsonb ? old.name;
        return new;
    end;
$$$$
;;

create trigger ${model}_config_rename_trig
    before update of name on $model.configuration
       for each row execute procedure ${model}.config_rename_fct()
;;


/* ********************************************** */
/*         Abstract entities: NODES               */
/* ********************************************** */

-- Trigger to update lien when the end points change
create function ${model}.node_update_fct()
returns trigger
language plpgsql
as $$$$
    begin
        update $model._link set geom=ST_SetPoint(geom, 0, new.geom) where up=new.id;
        update $model._link set geom=ST_SetPoint(geom, ST_NumPoints(geom)-1, new.geom) where down=new.id;
        return new;
    end;
$$$$
;;

create trigger ${model}_node_update_trig
    after update of geom on $model._node
       for each row execute procedure ${model}.node_update_fct()
;;

/* ********************************************** */
/*         Abstract entities: LINKS               */
/* ********************************************** */

-- Trigger to remove generated nodes if no link and/or singularity on it
create function ${model}._link_after_delete_fct()
returns trigger
language plpgsql
as $$$$
    begin
        delete from $model._node
        where id=old.down
        and generated is not null
        and (select count(1) from $model._link where up=old.down) = 0
        and (select count(1) from $model._link where down=old.down) = 0
        and (select count(1) from $model._singularity as s where s.id=id) = 0;
        delete from $model._node
        where id=old.up
        and generated is not null
        and (select count(1) from $model._link where up=old.up) = 0
        and (select count(1) from $model._link where down=old.up) = 0
        and (select count(1) from $model._singularity as s where s.id=id) = 0;
        return old;
    end;
$$$$
;;

create trigger ${model}_link_after_delete_trig
    after delete on $model._link
       for each row execute procedure ${model}._link_after_delete_fct()
;;

-- Common function to get endpoints from geometry raise error if endpoints cannot be found
create function ${model}.find_link_fct(
    in geom_ geometry('LINESTRINGZ',$srid),
    out up_ integer,
    out up_type_ hydra_node_type,
    out down_ integer,
    out down_type_ hydra_node_type)
language plpgsql
as $$$$
    begin
        select id, node_type from $model._node
        where ST_DWithin(ST_StartPoint(geom_), geom, .1)
        order by ST_Distance(ST_StartPoint(geom_), geom) asc
        limit 1 into up_, up_type_
        ;
        select id, node_type from $model._node
        where ST_DWithin(ST_EndPoint(geom_), geom, .1)
        order by ST_Distance(ST_EndPoint(geom_), geom) asc
        limit 1 into down_, down_type_
        ;
        if (up_ is null) then
            -- find link from contours
            with node as (
                select s.id, 'storage' as node_type, c.geom as contour from $model._storage_node as s, $model.coverage as c where c.id=s.contour
                union
                select id, 'elem_2d' as node_type, contour from $model._elem_2d_node
            )
            select id, node_type from node
            where ST_Intersects(ST_StartPoint(geom_), contour)
            order by ST_Distance(ST_StartPoint(geom_), ST_Centroid(contour)) asc
            limit 1 into up_, up_type_
            ;
        end if;
        if (down_ is null) then
            -- find link from contours
            with node as (
                select s.id, 'storage' as node_type, c.geom as contour from $model._storage_node as s, $model.coverage as c where c.id=s.contour
                union
                select id, 'elem_2d' as node_type, contour from $model._elem_2d_node
            )
            select id, node_type from node
            where ST_Intersects(ST_EndPoint(geom_), contour)
            order by ST_Distance(ST_EndPoint(geom_), ST_Centroid(contour)) asc
            limit 1 into down_, down_type_
            ;
        end if;

        if (up_ is null or down_ is null) then
            raise exception 'link % not touching nodes (distance from start % distance from end %)', ST_AsText(geom_), (select ST_Distance(ST_StartPoint(geom_), geom) from $model._node order by ST_Distance(ST_StartPoint(geom_), geom) limit 1), (select ST_Distance(ST_EndPoint(geom_), geom) from $model._node order by ST_Distance(ST_EndPoint(geom_), geom) limit 1);
        end if;
    end;
$$$$
;;

/* ********************************************** */
/*       Abstract entities: SINGULARITIES         */
/* ********************************************** */

-- Update geometry of inter_paves sylbols after update
create function ${model}.inter_pave_symbol_fct(
    in up integer,
    in down integer,
    out out_ geometry('LINESTRINGZ',$srid)
)
language plpgsql
as $$$$
    declare
       pstart_ geometry('POINTZ',$srid);
       pend_ geometry('POINTZ',$srid);
       tx_ real;
       ty_ real;
    begin
       select geom from $model._node where id=up into pstart_;
       select geom from $model._node where id=down into pend_;
       select .1*(ST_X(pend_)-ST_X(pstart_)), .1*(ST_Y(pend_)-ST_Y(pstart_)) into tx_, ty_;
       with p1 as (select (ST_DumpPoints(contour)).geom::geometry as geom from $model._elem_2d_node where id=up),
       p2 as (select (ST_DumpPoints(contour)).geom::geometry as geom from $model._elem_2d_node where id=down),
       p1_ as (select distinct geom from p1),
       p2_ as (select distinct geom from p2),
       pairs as (select ST_Collect(p1_.geom, p2_.geom) as geom from p1_, p2_ order by ST_Distance(p1_.geom, p2_.geom) limit 2),
       line as (select ST_Collect(geom) as geom from pairs),
       symb as (select ST_Centroid(geom) as geom from line)
       select ST_SetSRID(ST_MakeLine(ST_MakePoint(ST_X(geom)-tx_, ST_Y(geom)-ty_, ST_Z(pstart_)), ST_MakePoint(ST_X(geom)+tx_, ST_Y(geom)+ty_, ST_Z(pend_))), $srid) from symb into out_;
    end;
$$$$
;;

create function ${model}.inter_pave_border_fct(
    in up integer,
    in down integer,
    out out_ geometry('LINESTRINGZ',$srid)
)
language plpgsql
as $$$$
    begin
        with border as (
            select ST_CollectionHomogenize(
                ST_Intersection(n1.contour, n2.contour)) as geom, 0.5*(ST_Z(n1.geom) + ST_Z(n2.geom)) as z
            from ${model}.elem_2d_node as n1, ${model}.elem_2d_node as n2
            where n1.id=up and n2.id=down
        )
        select ST_SetSRID(ST_MakeLine(
                ST_MakePoint(ST_X(ST_StartPoint(geom)), ST_Y(ST_StartPoint(geom)), z),
                ST_MakePoint(ST_X(ST_EndPoint(geom)), ST_Y(ST_EndPoint(geom)), z)), $srid)
        from border into out_;
    end;
$$$$
;;

/* ********************************************** */
/*         Configuration update                   */
/* ********************************************** */

create function ${model}.add_configuration_fct(in configuration json, in id integer, in table_name varchar) returns void
language plpgsql
as $$$$
    declare
        config varchar;
    begin
        if configuration is not null then
            -- @todo complete configuration with default if needed
            if 'default' not in (select k from json_object_keys(configuration) as k) then
                raise exception 'trying to add a configuration with no default values';
            end if;
            insert into $model.configuration(name)
                select k
                from json_object_keys(configuration) as k
                where k not in (select name from $model.configuration);
        end if;
    end;
$$$$
;;

create function ${model}.metadata_configuration_after_update_fct()
returns trigger
language plpgsql
as $$$$
    begin
        -- Unpack all configured items in default config
        perform $model.unpack_config(1);
        if new.configuration != 1 then
            -- Unpack configured items for new config only (on top of all items in default)
            perform $model.unpack_config(new.configuration);
        end if;
        return new;
    end;
$$$$
;;

create function ${model}.unpack_config(config_id integer)
returns integer
language plpgsql
as $$$$
    declare
        _res integer := 0;
        node record;
        singularity record;
        link record;
        profile record;
        update_fields varchar;
        update_arrays varchar;
        json_fields varchar;
        config_name varchar;
    begin
        update $model.config_switch set is_switching=true;

        select name from $model.configuration where id=config_id into config_name;

        for node in select * from $model._node where configuration is not null loop
            if config_name in (select k from json_object_keys(node.configuration) as k) then
                -- json switch must be done first to handle trigger on other fields with correct values
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||node.node_type::varchar||'_node'
                and data_type = 'json'
                and column_name not in ('id', 'node_type', 'name', 'geom', 'reach', 'contour', 'station', 'domain_2d', 'configuration')
                into json_fields;

                if json_fields is not null then
                    execute 'update $model.'||node.node_type::varchar||'_node '||
                                 'set '||json_fields||' where id='||node.id::varchar||';';
                end if;

                -- user defined types
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||udt_schema||'.'||udt_name, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||node.node_type::varchar||'_node'
                and data_type = 'USER-DEFINED'
                and column_name not in ('id', 'node_type', 'name', 'geom', 'reach', 'contour', 'station', 'domain_2d', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model.'||node.node_type::varchar||'_node '||
                                 'set '||update_fields||' where id='||node.id::varchar||';';
                end if;

                -- common fields
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||node.node_type::varchar||'_node'
                and data_type != 'USER-DEFINED'
                and data_type != 'ARRAY'
                and data_type != 'json'
                and column_name not in ('id', 'node_type', 'name', 'geom', 'reach', 'contour', 'station', 'domain_2d', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model.'||node.node_type::varchar||'_node '||
                                 'set '||update_fields||' where id='||node.id::varchar||';';
                end if;

                -- assume that all arrays are real[] (it is the case in v1.0.0)
                select string_agg(column_name||'=replace(replace(configuration_json->'''||config_name||'''->>'''||column_name||''', ''['' , ''{''), '']'', ''}'')::real[]', ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||node.node_type::varchar||'_node'
                and data_type = 'ARRAY'
                and column_name not in ('id', 'node_type', 'name', 'geom', 'reach', 'contour', 'station', 'domain_2d', 'configuration')
                into update_arrays;

                if update_arrays is not null then
                    execute 'update $model.'||node.node_type::varchar||'_node '||
                                 'set '||update_arrays||' where id='||node.id::varchar||';';
                end if;
                _res := _res +1;
            end if;
        end loop;

        -- Update singularities that have a configuration
        for singularity in select * from $model._singularity where configuration is not null loop
            if config_name in (select k from json_object_keys(singularity.configuration) as k) then
                -- json switch must be done first to handle trigger on other fields with correct values
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||singularity.singularity_type::varchar||'_singularity'
                and data_type = 'json'
                and column_name not in ('id', 'singularity_type', 'name', 'geom', 'sector', 'configuration')
                into json_fields;

                if json_fields is not null then
                    execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                                 'set '||json_fields||' where id='||singularity.id::varchar||';';
                end if;

                -- booleans
                select string_agg(column_name||'=coalesce((configuration_json->'''||config_name||'''->>'''||column_name||''')::'||udt_schema||'.'||udt_name||', false)', ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||singularity.singularity_type::varchar||'_singularity'
                and data_type = 'boolean'
                and column_name not in ('id', 'singularity_type', 'name', 'geom', 'sector', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                                 'set '||update_fields||' where id='||singularity.id::varchar||';';
                end if;

                -- user defined types
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||udt_schema||'.'||udt_name, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||singularity.singularity_type::varchar||'_singularity'
                and data_type = 'USER-DEFINED'
                and column_name not in ('id', 'singularity_type', 'name', 'geom', 'sector', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                                 'set '||update_fields||' where id='||singularity.id::varchar||';';
                end if;

                -- common fields
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||singularity.singularity_type::varchar||'_singularity'
                and data_type != 'USER-DEFINED'
                and data_type != 'ARRAY'
                and data_type != 'json'
                and data_type != 'boolean'
                and column_name not in ('id', 'singularity_type', 'name', 'geom', 'sector', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                                 'set '||update_fields||' where id='||singularity.id::varchar||';';
                end if;

                -- assume that all arrays are real[] (it is the case in v1.0.0)
                select string_agg(column_name||'=replace(replace(configuration_json->'''||config_name||'''->>'''||column_name||''', ''['' , ''{''), '']'', ''}'')::real[]', ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||singularity.singularity_type::varchar||'_singularity'
                and data_type = 'ARRAY'
                and column_name not in ('id', 'singularity_type', 'name', 'geom', 'sector', 'configuration')
                into update_arrays;

                if update_arrays is not null then
                    execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                                 'set '||update_arrays||' where id='||singularity.id::varchar||';';
                end if;
                _res := _res +1;
            end if;
        end loop;

        -- Update links that have a configuration
        for link in select * from $model._link where configuration is not null loop
            if config_name in (select k from json_object_keys(link.configuration) as k) then
                -- json switch must be done first to handle trigger on other fields with correct values
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||link.link_type::varchar||'_link'
                and data_type = 'json'
                and column_name not in ('id', 'link_type', 'name', 'geom', 'branch', 'border', 'hydrograph', 'configuration')
                into json_fields;

                if json_fields is not null then
                    execute 'update $model.'||link.link_type::varchar||'_link '||
                                 'set '||json_fields||' where id='||link.id::varchar||';';
                end if;

                -- user defined types
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||udt_schema||'.'||udt_name, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||link.link_type::varchar||'_link'
                and data_type = 'USER-DEFINED'
                and column_name not in ('id', 'link_type', 'name', 'geom', 'branch', 'border', 'hydrograph', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model.'||link.link_type::varchar||'_link '||
                                 'set '||update_fields||' where id='||link.id::varchar||';';
                end if;

                -- common fields
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||link.link_type::varchar||'_link'
                and data_type != 'USER-DEFINED'
                and data_type != 'ARRAY'
                and data_type != 'json'
                and column_name not in ('id', 'link_type', 'name', 'geom', 'branch', 'border', 'hydrograph', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model.'||link.link_type::varchar||'_link '||
                                 'set '||update_fields||' where id='||link.id::varchar||';';
                end if;

                -- assume that all arrays are real[] (it is the case in v1.0.0)
                select string_agg(column_name||'=replace(replace(configuration_json->'''||config_name||'''->>'''||column_name||''', ''['' , ''{''), '']'', ''}'')::real[]', ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||link.link_type::varchar||'_link'
                and data_type = 'ARRAY'
                and column_name not in ('id', 'link_type', 'name', 'geom', 'branch', 'border', 'hydrograph', 'configuration')
                into update_arrays;

                if update_arrays is not null then
                    execute 'update $model.'||link.link_type::varchar||'_link '||
                                 'set '||update_arrays||' where id='||link.id::varchar||';';
                end if;
                _res := _res +1;
            end if;
        end loop;

        for profile in select * from $model._river_cross_section_profile where configuration is not null loop
            if config_name in (select k from json_object_keys(profile.configuration) as k) then
                -- foreign keys
                select string_agg(column_name||'=(configuration->'''||config_name||'''->>'''||column_name||''')::'||udt_schema||'.'||udt_name, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_river_cross_section_profile'
                and column_name  in ('up_cp_geom', 'down_cp_geom', 'up_op_geom', 'down_op_geom', 'up_vcs_geom', 'up_vcs_topo_geom', 'down_vcs_geom', 'down_vcs_topo_geom')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model._river_cross_section_profile set '||update_fields||' where id='||profile.id::varchar||';';
                end if;

                -- user defined types
                select string_agg(column_name||'=(configuration->'''||config_name||'''->>'''||column_name||''')::'||udt_schema||'.'||udt_name, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_river_cross_section_profile'
                and data_type = 'USER-DEFINED'
                and column_name not in ('id', 'name', 'geom', 'up_cp_geom', 'down_cp_geom', 'up_op_geom', 'down_op_geom', 'up_vcs_geom', 'up_vcs_topo_geom', 'down_vcs_geom', 'down_vcs_topo_geom', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model._river_cross_section_profile set '||update_fields||' where id='||profile.id::varchar||';';
                end if;

                -- common fields
                select string_agg(column_name||'=(configuration->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_river_cross_section_profile'
                and data_type != 'USER-DEFINED'
                and data_type != 'ARRAY'
                and data_type != 'json'
                and column_name not in ('id', 'name', 'geom', 'up_cp_geom', 'down_cp_geom', 'up_op_geom', 'down_op_geom', 'up_vcs_geom', 'up_vcs_topo_geom', 'down_vcs_geom', 'down_vcs_topo_geom', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model._river_cross_section_profile set '||update_fields||' where id='||profile.id::varchar||';';
                end if;

                -- assume that all arrays are real[] (it is the case in v1.0.0)
                select string_agg(column_name||'=replace(replace(configuration->'''||config_name||'''->>'''||column_name||''', ''['' , ''{''), '']'', ''}'')::real[]', ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_river_cross_section_profile'
                and data_type = 'ARRAY'
                and column_name not in ('id', 'name', 'geom', 'up_cp_geom', 'down_cp_geom', 'up_op_geom', 'down_op_geom', 'up_vcs_geom', 'up_vcs_topo_geom', 'down_vcs_geom', 'down_vcs_topo_geom', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model._river_cross_section_profile set '||update_fields||' where id='||profile.id::varchar||';';
                end if;
                _res := _res +1;
            end if;
        end loop;

        update $model.config_switch set is_switching=false;

        return _res;
    end;
$$$$
;;

create trigger ${model}_metadata_configuration_after_update_trig
    before update of configuration on $model.metadata
       for each row execute procedure ${model}.metadata_configuration_after_update_fct()
;;
