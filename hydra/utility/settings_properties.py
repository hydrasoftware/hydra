# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import json
import os
from configparser import ConfigParser

_hydra_dir = os.path.join(os.path.expanduser('~'), ".hydra")
_settings_file = os.path.join(_hydra_dir, "hydra.config")

class SettingsProperties(object):
    # memory of last directory visited
    __memorized_dir = None

    @staticmethod
    def get_memorized_dir():
        return SettingsProperties.__memorized_dir

    @staticmethod
    def set_memorized_dir(directory):
        SettingsProperties.__memorized_dir = directory

    # generic settings saved in hydra.config file
    __settings_dic = None

    @staticmethod
    def get_snap():
        if SettingsProperties.__settings_dic is None:
            SettingsProperties.check_settings_file()
            with open (_settings_file) as fi:
                SettingsProperties.__settings_dic = json.load(fi)
        return SettingsProperties.__settings_dic['snap']

    @staticmethod
    def get_service():
        if SettingsProperties.__settings_dic is None:
            SettingsProperties.check_settings_file()
            with open (_settings_file) as fi:
                SettingsProperties.__settings_dic = json.load(fi)
        return SettingsProperties.__settings_dic['service']

    @staticmethod
    def local():
        services = ConfigParser()
        services.read(os.environ['PGSERVICEFILE'] if 'PGSERVICEFILE' in os.environ else os.path.join(os.path.expanduser('~'), '.pg_service.conf'))
        current_service = SettingsProperties.get_service()
        if current_service in services and services[current_service]['host'] in ('127.0.0.1', 'localhost'):
            return True
        else:
            return False

    @staticmethod
    def get_path(app=None):
        if SettingsProperties.__settings_dic is None:
            SettingsProperties.check_settings_file()
            with open (_settings_file) as fi:
                SettingsProperties.__settings_dic = json.load(fi)
        if app:
            return SettingsProperties.__settings_dic['path'][app]
        else:
            return SettingsProperties.__settings_dic['path']

    @staticmethod
    def write_settings_file(snap, service, path_postgre, path_python):
        dic = SettingsProperties.__write_settings_dic(snap, service, path_postgre, path_python)
        SettingsProperties.check_settings_file()
        with open (_settings_file, "w") as fo:
            json.dump(dic, fo, indent=4)
        SettingsProperties.__reset_settings()

    @staticmethod
    def check_settings_file():
        '''
        complete settings file with default values or creates it if needed
        to be called at the start of plugin only (no need to reset)
        '''
        need_update = False
        default_settings = SettingsProperties.__write_settings_dic()
        if not os.path.isfile(_settings_file):
            file_settings = default_settings
            need_update = True
        else:
            with open(_settings_file) as fi:
                file_settings = json.load(fi)
            for key1 in default_settings.keys():
                if key1 not in file_settings.keys():
                    file_settings[key1] = default_settings[key1]
                    need_update = True
                elif type(default_settings[key1])==type({}):
                    for key2 in default_settings[key1].keys():
                        if key2 not in file_settings[key1].keys():
                            file_settings[key1][key2] = default_settings[key1][key2]
                            need_update = True
        if need_update:
            if not os.path.isdir(os.path.dirname(_settings_file)):
                os.mkdir(os.path.dirname(_settings_file))
            with open (_settings_file, "w") as fo:
                json.dump(file_settings, fo, indent=4)


    @staticmethod
    def __write_settings_dic(snap=20, service='hydra',
            path_postgre='C:\\Program Files\\Hydra\\PostgreSQL\\13' if os.name == 'nt' else '/usr/bin',
            path_python='C:\\Program Files\\Hydra\\languagepack\\v1\\Python-3.7' if os.name == 'nt' else '/usr'):
        '''generated a settings dict with possibly default values'''

        # default service is 'hydra' except if not in PGSERVICEFILE then it's hydra_legacy'
        if service == 'hydra':
            services = ConfigParser()
            services.read(os.environ['PGSERVICEFILE'] if 'PGSERVICEFILE' in os.environ else os.path.join(os.path.expanduser('~'), '.pg_service.conf'))
            if 'hydra' not in services and 'hydra_legacy' in services:
                service = 'hydra_legacy'

        dict ={'service': service,
                'path': {
                    'postgre': path_postgre,
                    'python': path_python
                    },
                'snap': snap,
                }
        return dict

    @staticmethod
    def __reset_settings():
        SettingsProperties.__settings_dic = None

if __name__ == "__main__":
    if not os.path.isdir(_hydra_dir):
        os.makedirs(_hydra_dir)

    # check settings file
    SettingsProperties.check_settings_file()
