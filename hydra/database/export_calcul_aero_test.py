# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import sys
import os
import shutil
from hydra.project import Project
from hydra.database.database import TestProject, project_exists, remove_project
from hydra.database.export_calcul import ExportCalcul

if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

keep = True
hydra_dir = os.path.join(os.path.expanduser("~"), ".hydra")
tesdb = "export_calcul_aero_test"

if project_exists(tesdb):
    remove_project(tesdb)
if os.path.exists(os.path.join(hydra_dir, tesdb)):
    shutil.rmtree(os.path.join(hydra_dir, tesdb))

test_project = TestProject(tesdb, keep)
project = Project.load_project(tesdb)
project.add_new_model('station')
project.set_current_model('station')
model = project.get_current_model()

model.add_station([[[0,0],[1000,0],[1000,1000],[0,1000],[0,0]]])
model.add_station_node([10, 10])
model.add_station_node([10, 90])
model.add_station_node([90, 90])
model.add_station_node([90, 10])

model.add_air_flow_bc_singularity([10, 10], 'flow_bc', 1, False)
model.add_air_pressure_bc_singularity([10, 90], 'pressure_bc', 2)
model.add_air_duct_link([[10, 10], [90, 10]], 3, 4, 5, 6, 7, 8)
model.add_air_headloss_link([[10, 90], [90, 90]], 9, 10, 10.5)
model.add_jet_fan_link([[10, 10], [10, 90]], 11., 12, .13, 14, 15, True)
model.add_ventilator_link([[90, 10], [90, 90]], [[10, 0], [8, 6], [0, 7]], False)

scenario_id, = project.execute(f"""
    insert into project.scenario(name, has_aeraulics)
    values ('scenario', true)
    returning id""").fetchone()

project.commit()

exporter = ExportCalcul(project)
exporter.export(scenario_id)

ref = """$ Air Duct
   41         1
1 1 4 'AD1'
3.0 4.0 8.0 5.0 6.0 7.0
$ Air Headloss
   42         1
1 2 3 'HL2'
9.0 10.0 10.5
$ Jet Fan
   44         1
1 1 2 'JF3'
1 12 11.0 0.13 14.0 15.0
$ Ventilator
   43         1
1 4 3 'V4'
2 3
10.0 0.0
8.0 6.0
0.0 7.0
$ Air Flow BC
   45         1
1 1 'flow_bc'
2 1.0
$ Air Pressure BC
   46         1
1 2 'pressure_bc'
2.0
"""




with open(os.path.join(hydra_dir, tesdb,
        'SCENARIO', 'travail', 'SCENARIO_STATION.li')) as f:
    for i, (a, b) in enumerate(zip(f.read().split('\n'), ref.split('\n'))):
        if a.strip() != b.strip():
           print(f'diffence at {f.name}:{i+1}', a, '|', b)
        assert(a.strip()==b.strip())


