# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run mesh tests

USAGE

   python -m hydra.mesh_test [-hk]

OPTIONS

   -h, --help
        print this help

   -k, --keep
        keep database after test

"""

assert __name__ == "__main__"

import os, sys
import getopt
from hydra.project import Project
from hydra.database.import_model import import_model_csv
from hydra.database.database import TestProject, project_exists, remove_project
from hydra.utility.string import read_file

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hkd",
            ["help", "keep", "debug"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

keep =  "-k" in optlist or "--keep" in optlist
debug =  "-d" in optlist or "--debug" in optlist


if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

project_name='mesh_test'

if project_exists(project_name):
    remove_project(project_name)

test_project = TestProject(project_name, keep)
project = Project.load_project(project_name)
project.debug = debug
project.add_new_model('model')
data_dir = os.path.join(os.path.dirname(__file__), 'test_data')

id_type_points = project.execute("""
    insert into project.points_type (name, comment)
    values ('point_type_1', 'comment_1')
    returning id
    """).fetchone()[0]

project.execute("""
    create view datal as
    with jdata as (
        select '{}'::json as data
    )
    select
        row_number() over() as id,
        st_setsrid(st_geomfromgeojson((d->'geometry')::varchar), 2154) as geom,
        (d->'properties'->'type')::varchar as type
    from (select json_array_elements(data->'features') as d from jdata) as t
    """.format(read_file(os.path.join(data_dir, 'mesh_test_line.geojson'))))

project.execute("""
    create view datap as
    with jdata as (
        select '{}'::json as data
    )
    select
        row_number() over() as id,
        st_setsrid(st_geomfromgeojson((d->'geometry')::varchar), 2154) as geom,
        (d->'properties'->'type')::varchar as type
    from (select json_array_elements(data->'features') as d from jdata) as t
    """.format(read_file(os.path.join(data_dir, 'mesh_test_point.geojson'))))

project.execute("""
    create view datap_terrain as
    with jdata as (
        select '{}'::json as data
    )
    select
        row_number() over() as id,
        st_setsrid(st_geomfromgeojson((d->'geometry')::varchar), 2154) as geom,
        (d->'properties'->'name')::varchar as name,
        (d->'properties'->>'points_id')::float as points_id,
        (d->'properties'->>'z_ground')::float as z_ground
    from (select json_array_elements(data->'features') as d from jdata) as t
    """.format(read_file(os.path.join(data_dir, 'topo_test_points_xyz.geojson'))))


data_statements = """
insert into project.points_xyz(id, name, z_ground, points_id, geom) select id, name, z_ground, points_id, geom from datap_terrain where points_id=1
;;
insert into model.reach(geom) select geom from datal where type='"reach"'
;;
insert into model.river_cross_section_profile(geom) select n.geom from model.river_node as n
;;
update model.river_cross_section_profile as p set type_cross_section_down='valley', z_invert_down=102.4 where exists (select 1 from model.reach as r where st_intersects(p.geom, st_startpoint(r.geom)))
;;
update model.river_cross_section_profile as p set type_cross_section_up='valley', z_invert_up=97.6 where exists (select 1 from model.reach as r where st_intersects(p.geom, st_endpoint(r.geom)))
;;
update model.river_cross_section_profile as p set type_cross_section_down='channel' where ST_DWithin(geom, (select ST_StartPoint(geom) from model.reach where name='REACH_4'), 1)
;;
update model.river_cross_section_profile as p set type_cross_section_up='channel' where ST_DWithin(geom, (select ST_EndPoint(geom) from model.reach where name='REACH_4'), 1)
;;

insert into model.street(geom) select geom from datal where type='"street"'
;;
update model.street set width=60 where name='STREET_5'
;;

insert into model.constrain(geom, constrain_type, elem_length) select geom, 'flood_plain_transect', 300 from datal where type='"transect"'
;;
insert into model.constrain(geom, elem_length) select st_force3d(geom),  300 from datal where type='"constrain"'
;;
insert into model.constrain(geom, elem_length, constrain_type) select st_force3d(geom),  100, 'overflow' from datal where type='"overflow"'
;;
insert into model.constrain(geom, elem_length, constrain_type, link_attributes) select st_force3d(geom),  300, 'porous', '{"transmitivity": 0.3}'::json from datal where type='"porous"'
;;
insert into model.constrain(geom, elem_length, constrain_type, points_xyz, points_xyz_proximity) select st_force3d(geom),  300, 'strickler', 1, 100 from datal where type='"strickler"'
;;
insert into model.constrain(geom, elem_length, constrain_type) select st_force3d(geom),  100, 'connector' from datal where type='"connector"'
;;
insert into model.constrain(geom, elem_length, constrain_type) select st_force3d(geom),  300, 'boundary' from datal where type='"boundary"'
;;
insert into model.constrain(geom, constrain_type) select geom, 'ignored_for_coverages' from datal where type='"ignored_for_coverages"'
;;

insert into model.storage_node(geom, zs_array)
select st_setsrid(st_makepoint(st_x(st_centroid(c.geom)), st_y(st_centroid(c.geom)), 999), 2154), '{{0, 0}, {10,1000}}'
from model.coverage as c, datap as p where st_intersects(c.geom, p.geom) and p.type='"storage"'
;;

insert into model.coverage_marker(geom)
select st_setsrid(st_makepoint(st_x(p.geom), st_y(p.geom), 999), 2154)
from datap as p where p.type='"null"'
;;

insert into model.manhole_node(geom)
select st_setsrid(st_makepoint(st_x(p.geom), st_y(p.geom), 999), 2154)
from datap as p where p.type='"manhole"'
;;

select model.coverage_update();
;;

create view debug as
with l as (
select model.interpolate_transect_at(geom) as geom
from (select (st_dumppoints(geom)).geom as geom from model.coverage where domain_type='reach' ) as t
)
select row_number() over() as id, geom, st_azimuth(st_startpoint(geom), st_endpoint(geom)) from l
;;
"""

for statement in data_statements.split(';;')[:-1]:
    project.execute(statement)

project.commit()

mesh_statements = """
select model.mesh(id) from model.coverage where domain_type='2d'
;;

select model.create_links(c1.id, c2.id)
from model.coverage as c1, model.coverage as c2, model.constrain as c
where st_intersects(c1.geom, c2.geom)
and st_length(st_collectionextract(st_intersection(c1.geom, c2.geom), 2)) > 0
and st_length(st_collectionextract(st_intersection(c.discretized, st_intersection(c1.geom, c2.geom)), 2)) > 0
and st_length(st_collectionextract(st_intersection(c.discretized, st_intersection(c1.geom, c2.geom)), 2)) > 0
and (c.constrain_type is null or c.constrain_type!='flood_plain_transect')
and c1.domain_type in ('reach', 'street', 'storage')
and c2.domain_type in ('reach', 'street', 'storage')
and c1.id <> c2.id
;;

-- Links over flood_plain_transects
select model.create_links(c1.id, c2.id)
from model.coverage as c1, model.coverage as c2, model.constrain as c
where st_intersects(c1.geom, c2.geom)
and st_length(st_collectionextract(st_intersection(c1.geom, c2.geom), 2)) > 0
and st_length(st_collectionextract(st_intersection(c.discretized, st_intersection(c1.geom, c2.geom)), 2)) > 0
and st_length(st_collectionextract(st_intersection(c.discretized, st_intersection(c1.geom, c2.geom)), 2)) > 0
and c.constrain_type='flood_plain_transect'
and c1.domain_type in ('reach', 'street', 'storage')
and c2.domain_type in ('reach', 'street', 'storage')
and c1.id <> c2.id
;;

select model.create_network_overflow_links(c.id)
from model.coverage as c
where c.domain_type in ('2d', 'street', 'storage')
;;

"""

for statement in mesh_statements.split(';;')[:-1]:
    project.execute(statement)

project.commit()

# Connector generation from boundary constain to station node

connector_statements="""
insert into model.station(geom) values (ST_Force3D(ST_MakeEnvelope(1050488, 6818266, 1050910, 6818655, 2154)))
;;

insert into model.station_node(geom)
select st_setsrid(st_makepoint(st_x(p.geom), st_y(p.geom), 999), 2154)
from datap as p where p.type='"station"'
;;

select model.create_boundary_links('constrain', c.id, s.id) from model.constrain as c, model.station_node as s where c.constrain_type = 'boundary'
;;

select model.create_boundary_links('street', 2, s.id) from model.station_node as s
;;

"""

for statement in connector_statements.split(';;')[:-1]:
    project.execute(statement)

project.commit()

assert(project.execute("select count(1) from model.invalid").fetchone()[0] == 39)
assert(abs(project.execute("select count(1) from model.elem_2d_node").fetchone()[0] -  383) < 383*.1)
assert(project.execute("select count(1) from model.strickler_link").fetchone()[0] == 14)
assert(abs(project.execute("select count(1) from model.overflow_link").fetchone()[0]-300) < 300*.02)
assert(project.execute("select count(1) from model.network_overflow_link").fetchone()[0] == 18)
assert(abs(project.execute("select count(1) from model.mesh_2d_link").fetchone()[0] - 664) < 664*.1)
assert(project.execute("select count(1) from model.porous_link").fetchone()[0] == 14)
assert(project.execute("select count(1) from model.connector_link").fetchone()[0] == 32)

project.execute("""select model.set_link_altitude_topo(l.id) from model.strickler_link as l where ST_Intersects(l.geom, (select geom from model.constrain where id=41))""")

z_crests = project.execute("select z_crest1, z_crest2 from model.strickler_link as l where ST_Intersects(l.geom, (select geom from model.constrain where id=41))").fetchall()

assert(all([110<z1 and z1<130 for z1, z2 in z_crests]))
assert(all([110<z2 and z2<130 for z1, z2 in z_crests]))

print('ok')
