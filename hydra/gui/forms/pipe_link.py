# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QMessageBox, QLineEdit
from hydra.gui.geom_dialog import GeomDialog
from hydra.gui.simple_link_dialog import SimpleLinkDialog, tr
from hydra.gui.widgets.combo_with_values import ComboWithValues
import hydra.utility.string as string

_table_info = {'pipe':    {'index': 0, 'table': 'closed_parametric_geometry'},
                'channel': {'index': 1, 'table': 'open_parametric_geometry'}}

class PipeLinkEditor(SimpleLinkDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleLinkDialog.__init__(self, project, geom, "pipe_link", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "pipe_link.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.z_invert_down.textChanged.connect(self.slope_refresh)
        self.z_invert_up.textChanged.connect(self.slope_refresh)
        self.length.textChanged.connect(self.slope_refresh)
        self.filter_line_edit = self.findChild(QLineEdit, 'filter')
        self.filter_line_edit.textChanged.connect(self.filter_geom_combo)

        #get pipe info
        name, z_invert_up, z_invert_down, h_sable, type_cs, exclude_from_branch, rk, circular_diameter, ovoid_height, ovoid_top_diameter, ovoid_invert_diameter, cp_geom, op_geom, length, custom_length, comment, rk_maj = self.project.execute("""
            select name, z_invert_up, z_invert_down, h_sable, cross_section_type, exclude_from_branch, rk, circular_diameter, ovoid_height, ovoid_top_diameter, ovoid_invert_diameter, cp_geom, op_geom, ST_Length(geom), custom_length, comment, rk_maj
            from {}.pipe_link where id={} """.format(self.model.name, str(self.id))).fetchone()

        self.name.setText(name)
        self.z_invert_up.setText(string.get_str(z_invert_up))
        self.z_invert_down.setText(string.get_str(z_invert_down))
        self.h_sable.setText(string.get_str(h_sable))
        self.__type_cs = type_cs

        self.exclude_from_branch.setChecked(exclude_from_branch)
        self.exclude_from_branch.toggled.connect(self.excludechecked)
        self.geom_ids = [cp_geom, op_geom]
        self.comment.setText(comment)


        self.custom_length.toggled.connect(self.customchecked)
        if custom_length is not None:
            self.length.setText(str(custom_length))
            self.custom_length.setChecked(True)
        else:
            self.length.setText("{0:.3f}".format(length))

        # cross section combo box building
        cs_id_name_abb = self.project.execute("""
                                        select id, name, abbreviation
                                        from hydra.cross_section_type
                                        where id<>1 and id<>2 """).fetchall()
        for i in range(0, len(cs_id_name_abb)):
            self.combo_cross_section.addItem(str(cs_id_name_abb[i][1]))
            if self.__type_cs is not None and cs_id_name_abb[i][1]==self.__type_cs:
                self.combo_cross_section.setCurrentIndex(i)
        if self.__type_cs is None:
            self.combo_cross_section.setCurrentIndex(-1)


        self.rk.setText(string.get_str(rk))
        self.circular_diameter.setText(string.get_str(circular_diameter))
        self.ovoid_height.setText(string.get_str(ovoid_height))
        self.ovoid_top_diameter.setText(string.get_str(ovoid_top_diameter))
        self.ovoid_invert_diameter.setText(string.get_str(ovoid_invert_diameter))
        self.rk_maj.setText(string.get_str(rk_maj))

        #tools for geometry selection
        self.create_new.clicked.connect(self.new_geom)
        self.edit_selected.clicked.connect(self.edit_geom)
        self.geom_combo = ComboWithValues(self.geom_combo_placeholder)
        self.geom_combo.activated.connect(self.geom_changed)

        self.cross_section_changed()
        self.slope_refresh()

    def excludechecked(self):
        if self.exclude_from_branch.isChecked():
            confirm = QMessageBox(QMessageBox.Question, tr('Confirm'), tr('This will exclude pipe {} from branch generation. It can create errors in results. Are you sure?').format(self.name.text()), QMessageBox.Ok | QMessageBox.Cancel).exec_()
            if confirm == QMessageBox.Cancel:
                self.exclude_from_branch.setChecked(False)

    def customchecked(self):
        if self.custom_length.isChecked():
            self.length.setEnabled(True)
        else:
            self.length.setEnabled(False)
            leng, = self.project.execute("""select ST_Length(geom) from {}.pipe_link where id={} """.format(self.model.name, str(self.id))).fetchone()
            self.length.setText("{0:.3f}".format(leng))

    def cross_section_changed(self):
        self.label_circular_diameter.hide()
        self.circular_diameter.hide()
        self.label_ovoid_height.hide()
        self.ovoid_height.hide()
        self.label_ovoid_top_diameter.hide()
        self.ovoid_top_diameter.hide()
        self.label_ovoid_invert_diameter.hide()
        self.ovoid_invert_diameter.hide()
        self.rk_maj.hide()
        self.label_rk_maj.hide()

        if self.combo_cross_section.currentIndex() != -1:
            self.selected_cs = self.combo_cross_section.currentText()
            if self.selected_cs == 'circular':
                self.label_circular_diameter.show()
                self.circular_diameter.show()
                self.disable_geom()
            elif self.selected_cs == 'ovoid':
                self.label_ovoid_height.show()
                self.ovoid_height.show()
                self.label_ovoid_top_diameter.show()
                self.ovoid_top_diameter.show()
                self.label_ovoid_invert_diameter.show()
                self.ovoid_invert_diameter.show()
                self.disable_geom()
            elif self.selected_cs == 'channel':
                self.rk_maj.show()
                self.label_rk_maj.show()
                self.geom_refresh()
            else:
                self.geom_refresh()
        else:
            self.selected_cs = None

    def geom_changed(self):
        self.geom_ids[_table_info[self.selected_cs]['index']] = self.geom_combo.get_selected_value()

    def new_geom(self):
        if _table_info[self.selected_cs]['table'] == 'open_parametric_geometry':
            id = self.model.add_pipe_channel_open_section_geometry(string.list_to_sql_array([[0,0]]))

        elif _table_info[self.selected_cs]['table'] == 'closed_parametric_geometry':
            id = self.model.add_pipe_channel_closed_section_geometry(string.list_to_sql_array([[0,0]]))
        geom_dialog = GeomDialog(self.project, id, self.selected_cs)
        geom_dialog.exec_()
        if geom_dialog.canceled:
            self.project.execute("""delete from {}.{} where id={}""".format(self.model.name, _table_info[self.selected_cs]['table'], id))
        else:
            self.geom_ids[_table_info[self.selected_cs]['index']] = id
        self.geom_refresh()

    def edit_geom(self):
        if self.geom_combo.get_selected_value():
            GeomDialog(self.project, self.geom_combo.get_selected_value(), self.selected_cs).exec_()

    def select_geom(self):
        if self.table_existing_geometry.currentRow()>-1:
            if self.selected_cs == 'pipe':
                self.cp_geom_id = int(self.table_existing_geometry.item(self.table_existing_geometry.currentRow(), 0).text())
                self.cp_geom_name = self.table_existing_geometry.item(self.table_existing_geometry.currentRow(), 1).text()
            elif self.selected_cs == 'channel':
                self.op_geom_id = int(self.table_existing_geometry.item(self.table_existing_geometry.currentRow(), 0).text())
                self.op_geom_name = self.table_existing_geometry.item(self.table_existing_geometry.currentRow(), 1).text()

    def geom_refresh(self):
        self.geom_combo.setEnabled(True)
        self.geom_combo.clear()
        geoms = self.project.execute(""" select id, name from {}.{} order by name""".format(self.model.name, _table_info[self.selected_cs]['table'])).fetchall()
        for geom in geoms:
            self.geom_combo.addItem(geom[1], geom[0])
        self.geom_combo.set_selected_value(self.geom_ids[_table_info[self.selected_cs]['index']])
        self.geom_combo.setEditable(True)

        self.create_new.setEnabled(True)
        self.edit_selected.setEnabled(self.geom_combo.count()>0)

    def disable_geom(self):
        self.geom_combo.setEnabled(False)
        self.geom_combo.clear()
        self.create_new.setEnabled(False)
        self.edit_selected.setEnabled(False)

    def filter_geom_combo(self):
        filter_text = self.filter_line_edit.text().lower()
        self.geom_combo.clear()
        geoms = self.project.execute(f"SELECT id, name FROM {self.model.name}.{_table_info[self.selected_cs]['table']} ORDER BY name").fetchall()
        matching_geoms = [geom for geom in geoms if filter_text in geom[1].lower()]
        for geom in matching_geoms:
            self.geom_combo.addItem(geom[1], geom[0])
        if matching_geoms:
            self.geom_combo.set_selected_value(matching_geoms[0][0])
        else:
            self.geom_combo.set_selected_value(None)
        self.geom_combo.setEditable(True)

    def slope_refresh(self):
        slope = string.calc_slope(self.z_invert_up.text(), self.z_invert_down.text(), self.length.text())
        if slope:
            self.slope.setText(str(round(slope, 4)))
        else:
            self.slope.setText("###")

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))

        self.model.update_pipe_link(self.id, self.geom,
                            string.get_sql_float(self.z_invert_up.text()), string.get_sql_float(self.z_invert_down.text()),
                            self.combo_cross_section.currentText(), string.get_sql_float(self.h_sable.text()),
                            self.exclude_from_branch.isChecked(), string.get_sql_float(self.rk.text()), string.get_sql_float(self.circular_diameter.text()),
                            string.get_sql_float(self.ovoid_height.text()), string.get_sql_float(self.ovoid_top_diameter.text()), string.get_sql_float(self.ovoid_invert_diameter.text()),
                            self.geom_ids[0], self.geom_ids[1], self.name.text(), self.comment.toPlainText(), string.get_sql_float(self.rk_maj.text()))

        # update custom_length
        if self.custom_length.isChecked():
            custom_length = self.length.text()
        else:
            custom_length = 'null'

        self.project.execute("""update {}.pipe_link
                                set custom_length = {}
                                where id={}""".format(self.model.name, custom_length, self.id))


        SimpleLinkDialog.save(self)
        self.close()
