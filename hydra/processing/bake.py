from qgis.core import (QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingMultiStepFeedback,
    QgsProcessingParameterVectorLayer,
    QgsProcessingParameterString,
    QgsProcessingParameterDistance,
    QgsProcessingParameterMultipleLayers,
    QgsProcessingParameterVectorDestination,
    QgsProcessingParameterRasterDestination,
    QgsProcessingParameterCrs,
    QgsProcessingParameterField,
    QgsMessageLog,
    Qgis,
    QgsProject)
from osgeo import ogr
import os
from qgis import processing
import subprocess
#from . import style

class Bake(QgsProcessingAlgorithm):
    def initAlgorithm(self, config=None):
        proj = QgsProject.instance()

        self.addParameter(QgsProcessingParameterVectorLayer('polygon_to_bake', 'Polygons'))
        self.addParameter(QgsProcessingParameterField('value_field',
                                                      'Field to use for a burn-in value',
                                                      None,
                                                      'polygon_to_bake',
                                                      QgsProcessingParameterField.Numeric))
        self.addParameter(QgsProcessingParameterMultipleLayers('rasters', 'Rasters', QgsProcessing.TypeRaster))
        self.addParameter(QgsProcessingParameterCrs('crs', 'Projection for the output file', optional=True))
        self.addParameter(QgsProcessingParameterRasterDestination('baked_raster', 'Output file name (vrt)'))

    def processAlgorithm(self, parameters, context, model_feedback):
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(len(parameters['rasters']), model_feedback)
        results = {}
        outputs = {}

        parameters['polygon_to_bake'] = self.parameterAsVectorLayer(parameters, 'polygon_to_bake', context).dataProvider().dataSourceUri()
        baked_raster = parameters['baked_raster']

        assert(baked_raster)

        feedback.pushInfo('Converts raster tiles to tif and burn layer value in each tif')
        tiles = []
        step = 0
        for raster in parameters['rasters']:
            tile = f"{raster.rsplit('.')[0]}_tile.tif"
            tiles.append(tile)
            subprocess.run(['gdal_translate', '-a_srs', parameters['crs'].toWkt(), raster, tile], stdout=subprocess.DEVNULL)
            subprocess.run(['gdal_rasterize', '-a', parameters['value_field'], parameters['polygon_to_bake'], tile], stdout=subprocess.DEVNULL)
            feedback.setCurrentStep(step)
            step+=1

        feedback.pushInfo('Built vrt from tiles')
        subprocess.run(['gdalbuildvrt', baked_raster] + tiles, stdout=subprocess.DEVNULL)

        results = {'baked_raster': baked_raster}

        return results

    def name(self):
        return 'bake'

    def displayName(self):
        return 'Bake'

    def group(self):
        return "Raster tools"

    def groupId(self):
        return "rastertools"

    def createInstance(self):
        return Bake()

    def __Run(self, fused_command, feedback, working_directory = None):
        QgsMessageLog.logMessage(fused_command, 'Processing', Qgis.Info)
        feedback.pushInfo(self.name()+' command:')
        feedback.pushCommandInfo(fused_command)
        feedback.pushInfo(self.name()+' command output:')
        success = False
        retry_count = 0
        while not success:
            loglines = []
            loglines.append(self.name()+' execution console output')
            try:
                with subprocess.Popen(
                    fused_command,
                    shell=True,
                    stdout=subprocess.PIPE,
                    stdin=subprocess.DEVNULL,
                    stderr=subprocess.STDOUT,
                    universal_newlines=True,
                    cwd=working_directory,
                ) as proc:
                    for line in proc.stdout:
                        feedback.pushConsoleInfo(line)
                        loglines.append(line)
                    success = True
            except IOError as e:
                if retry_count < 5:
                    retry_count += 1
                else:
                    raise IOError(
                        str(e) + u'\nTried 5 times without success. Last iteration stopped after reading {} line(s).\nLast line(s):\n{}'.format(
                            len(loglines), u'\n'.join(loglines[-10:])))

            QgsMessageLog.logMessage('\n'.join(loglines), 'Processing', Qgis.Info)
