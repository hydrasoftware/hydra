# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.rain_manager_guitest [-dhs] [-g project_name]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name
        run in gui mode

   -s  --solo
        run test in solo mode (create database)
"""

from hydra.utility.string import normalized_name, normalized_model_name
import sys
import getopt
from hydra.project import Project
from qgis.PyQt.QtWidgets import QApplication
from qgis.PyQt.QtCore import QCoreApplication, QTranslator
from hydra.gui.rain_manager import RainManager
from ..database.database import TestProject, remove_project, project_exists

def gui_mode(project_name):
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    obj_project = Project.load_project(project_name)

    test_dialog = RainManager(obj_project)
    test_dialog.exec_()


def test():
    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)

    test_dialog = RainManager(obj_project)
    test_dialog.new_hyetograph()

    last_index = test_dialog.table_hyetograph.table.rowCount() -1
    test_dialog.table_hyetograph.table.setCurrentCell(last_index,0)
    test_dialog.table_hyetograph.table.item(last_index,1).setText("test_name")
    id_hyetograph = test_dialog.table_hyetograph.table.item(last_index,0).text()
    test_dialog.hyetograph_values.set_table_items([[1,2], [3,4], [5,6]])
    test_dialog.save()

    test_dialog = RainManager(obj_project)
    line_index = None

    for i in range(test_dialog.table_hyetograph.table.rowCount()):
        if test_dialog.table_hyetograph.table.item(i,0).text() == id_hyetograph:
            line_index = i
            break
    assert line_index is not None

    test_dialog.table_hyetograph.table.setCurrentCell(line_index,0)
    assert test_dialog.table_hyetograph.table.item(line_index,1).text()=="test_name"
    assert float(test_dialog.hyetograph_values.table.item(0,0).text())==1
    assert float(test_dialog.hyetograph_values.table.item(0,1).text())==2
    assert float(test_dialog.hyetograph_values.table.item(1,0).text())==3
    assert float(test_dialog.hyetograph_values.table.item(1,1).text())==4
    assert float(test_dialog.hyetograph_values.table.item(2,0).text())==5
    assert float(test_dialog.hyetograph_values.table.item(2,1).text())==6
    test_dialog.delete_hyetograph()

    test_dialog.new_montana()
    last_index = test_dialog.table_montana.table.rowCount() -1
    test_dialog.table_montana.table.selectRow(last_index)
    id_montana = test_dialog.table_montana.table.item(last_index,1).text()
    test_dialog.table_montana.table.item(last_index,1).setText("paris_5ans")
    test_dialog.table_montana.table.item(last_index,2).setText("5.2")
    test_dialog.table_montana.table.item(last_index,3).setText("1")
    test_dialog.table_montana.table.item(last_index,4).setText("-1")
    test_dialog.table_montana.table.item(last_index,5).setText("comment 5ans 6min-1h")

    test_dialog.new_montana()
    last_index = test_dialog.table_montana.table.rowCount() -1
    test_dialog.table_montana.table.selectRow(last_index)
    test_dialog.delete_montana()

    test_dialog.new_caquot()
    last_index = test_dialog.table_caquot.table.rowCount() -1
    test_dialog.table_caquot.table.selectRow(last_index)
    test_dialog.delete_caquot()

    test_dialog.new_simple_tri()
    last_index = test_dialog.table_simple_tri.table.rowCount() -1
    test_dialog.table_simple_tri.table.selectRow(last_index)
    test_dialog.delete_simple_tri()

    test_dialog.new_double_tri()
    last_index = test_dialog.table_double_tri.table.rowCount() -1
    test_dialog.table_double_tri.table.selectRow(last_index)
    test_dialog.delete_double_tri()

    test_dialog.new_gage_rainfall()
    last_index = test_dialog.table_gage_rainfall.table.rowCount() -1
    test_dialog.table_gage_rainfall.table.selectRow(last_index)
    test_dialog.delete_gage_rainfall()

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name)
    test()
    # fix_print_with_import
    print("ok")
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    project_name = normalized_name(project_name)
    gui_mode(project_name)
    exit(0)

test()
# fix_print_with_import

# fix_print_with_import
print("ok")
exit(0)


