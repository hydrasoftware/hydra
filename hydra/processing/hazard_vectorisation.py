import os
from pathlib import Path

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingMultiStepFeedback,
    QgsProcessingParameterRasterLayer,
    QgsProcessingParameterString,
    QgsProcessingParameterDistance,
    QgsProcessingParameterVectorDestination,
    QgsProject,
    QgsVectorLayer,
    QgsLayerTreeGroup,
    QgsLayerTreeLayer,
    edit,
    QgsField,
    QgsProcessingException,
    QgsProcessingParameterBand,
)
from qgis import processing
from qgis.PyQt.QtCore import QVariant
from osgeo import gdal
from osgeo import ogr
from shapely.ops import polygonize


class HazardVectorisation(QgsProcessingAlgorithm):
    #VELOCITY = "VELOCITY"
    #HAZARD = "HAZARD"
    INPUT_VELOCITY = "INPUT_VELOCITY"
    INPUT_VELOCITY_BAND = "INPUT_VELOCITY_BAND"
    INPUT_DEPTH = "INPUT_DEPTH"
    INPUT_DEPTH_BAND = "INPUT_DEPTH_BAND"
    OUTPUT_VELOCITY = "OUTPUT_VELOCITY"
    OUTPUT_DEPTH = "OUTPUT_DEPTH"
    OUTPUT_HAZARD = "OUTPUT_HAZARD"
    DEPTH_CLASSES = "DEPTH_CLASSES"
    VELOCITY_CLASSES = "VELOCITY_CLASSES"
    HAZARD_CLASSES = "HAZARD_CLASSES"
    MINIMUM_CONTOUR_LENGTH = "MINIMUM_CONTOUR_LENGTH"

    def tr(self, string):
        return QCoreApplication.translate("Processing", string)

    def name(self):
        return "hazard vectorisation"

    def displayName(self):
        return self.tr("Depth velocity vectorisation")

    def group(self):
        return self.tr("River and free surface flow mapping")

    def groupId(self):
        return "surfaceflowmapping"

    def createInstance(self):
        return HazardVectorisation()

    def shortHelpString(self):
        return self.tr("This algorithm generates vector layers of contour polygons from the specified water level and flow velocity input rasters.\n\nThe algorithm also gives the possibity to produce a vector map layer combining the previous depth and velocity results into a user-defined hazard classification (see depth classes, velocity classes and hazard classes parameters).\n\nThe minimal contour length parameter specifies the minimum contour polyline length (m) for polygon generation.\n\n The hazard classes parameter sepcifies the hazard level of the various classes. The lines correspond to the depth classes and the columns to the velocity classes. On top: lowest depth class; on left side: lowest velocity class. By default, the values are set from the french reglementation: Arrêté du 5 juillet 2019 Art. 2.\n\nThe output vector file format is shape or geopackage.\n\n\nAuthor : hydra software")

    def initAlgorithm(self, config=None):
        proj = QgsProject.instance()
                
        # depth is mandatory
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT_DEPTH,
                "Depth raster source",
                defaultValue=proj.readEntry("hazardmapping", self.INPUT_DEPTH, None)[0],
            )
        )
        self.addParameter(
            QgsProcessingParameterBand(
                self.INPUT_DEPTH_BAND,
                "Depth band",
                parentLayerParameterName=self.INPUT_DEPTH, optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterString(
                self.DEPTH_CLASSES,
                "Depth classes (space separated list)",
                defaultValue=proj.readEntry(
                    "hazardmapping", self.DEPTH_CLASSES, "0 0.5 1 2"
                )[0],
            )
        )

        self.addParameter(
            QgsProcessingParameterDistance(
                self.MINIMUM_CONTOUR_LENGTH,
                "Minimum contour length (m)",
                defaultValue=proj.readNumEntry(
                    "hazardmapping", self.MINIMUM_CONTOUR_LENGTH, 100
                )[0],
            )
        )

        self.addParameter(
            QgsProcessingParameterVectorDestination(
                self.OUTPUT_DEPTH,
                "Iso-depth output layer",
                QgsProcessing.TypeVectorPolygon,
                optional = False,
            )
        )

        # velocity is optional
        #QgsProject.instance().removeEntry("hazardmapping", "VELOCITY")
        #self.addParameter(
        #    QgsProcessingParameterBoolean(
        #        self.VELOCITY,
        #        "Produce velocity vectorisation",
        #        defaultValue=proj.readEntry("hazardmapping", self.VELOCITY, "False")[0],
        #    )
        #)
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT_VELOCITY,
                "Velocity raster source",
                defaultValue=proj.readEntry("hazardmapping", self.INPUT_VELOCITY, None)[0],
            )
        )
        self.addParameter(
            QgsProcessingParameterBand(
                self.INPUT_VELOCITY_BAND,
                "Velocity band", parentLayerParameterName=self.INPUT_VELOCITY, optional=True
            )
        )

        self.addParameter(
            QgsProcessingParameterString(
                self.VELOCITY_CLASSES,
                "Velocity classes (space separated list)",
                defaultValue=proj.readEntry(
                    "hazardmapping", self.VELOCITY_CLASSES, "0 0.2 0.5"
                )[0],
            )
        )

        self.addParameter(
            QgsProcessingParameterVectorDestination(
                self.OUTPUT_VELOCITY,
                "Iso-velocity output layer",
                QgsProcessing.TypeVectorPolygon,
                optional = True,
                createByDefault = False,
            )
        )

        #QgsProject.instance().removeEntry("hazardmapping", "HAZARD")
        #self.addParameter(
        #    QgsProcessingParameterBoolean(
        #        self.HAZARD,
        #        "Produce hazard map",
        #        defaultValue=proj.readEntry("hazardmapping", "hazard", "False")[0],
        #    )
        #)

        self.addParameter(
            QgsProcessingParameterString(
                self.HAZARD_CLASSES,
                "Hazard classes (space separated columns)",
                defaultValue=proj.readEntry(
                    "hazardmapping",
                    self.HAZARD_CLASSES,
                    "faible modéré fort\n"
                    "modéré modéré fort\n"
                    "fort fort très_fort\n"
                    "très_fort très_fort très_fort",
                )[0],
                multiLine=True,
            )
        )

        self.addParameter(
            QgsProcessingParameterVectorDestination(
                self.OUTPUT_HAZARD,
                "Depth velocity intersection output layer",
                QgsProcessing.TypeVectorPolygon,
                optional = True,
                createByDefault = False,
                )
        )

    def processAlgorithm(self, parameters, context, model_feedback):
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(15, model_feedback)

        #self.produce_velocity = self.parameterAsBoolean(
        #    parameters, self.VELOCITY, context
        #)
        #self.produce_hazard = self.parameterAsBoolean(
        #    parameters, self.HAZARD, context
        #)
        results = {}
        feedback.setProgress(1)

        minimum_contour_length = self.parameterAsDouble(
            parameters, self.MINIMUM_CONTOUR_LENGTH, context
        )

        # use to store temp files to be removed after processing
        self.tmpfiles = []

        # Save parameters
        proj = QgsProject.instance()
        for param in [
            self.INPUT_DEPTH,
            self.INPUT_VELOCITY,
            self.DEPTH_CLASSES,
            self.VELOCITY_CLASSES,
            self.HAZARD_CLASSES,
        ]:
            proj.writeEntry("hazardmapping", param, parameters[param])

        proj.writeEntryDouble(
            "hazardmapping",
            self.MINIMUM_CONTOUR_LENGTH,
            parameters[self.MINIMUM_CONTOUR_LENGTH],
        )

        # depth parameters
        self.input_depth_layer = self.parameterAsRasterLayer(
            parameters, self.INPUT_DEPTH, context
        )
        input_depth = Path(self.input_depth_layer.dataProvider().dataSourceUri())
        input_depth_band = self.parameterAsInt(
            parameters, self.INPUT_DEPTH_BAND, context
        )
        if not input_depth_band:
            if hasattr(self.input_depth_layer.renderer(), 'band'):
                input_depth_band = self.input_depth_layer.renderer().band()
            elif hasattr(self.input_depth_layer.renderer(), 'inputBand'):
                input_depth_band = self.input_depth_layer.renderer().inputBand()
            else:
                input_depth_band = 1
        depth_classes = self.parameterAsString(
            parameters, self.DEPTH_CLASSES, context
        ).strip().split()
        self.isodepth = self.parameterAsOutputLayer(
            parameters, self.OUTPUT_DEPTH, context
        )
        #output_depth = parameters.get(self.OUTPUT_DEPTH)
        #self.depth_sink, _ = output_depth.sink.value(context.expressionContext(), None)
        depth_blur = Path(self.isodepth).with_suffix(".depth_blur.vrt")
        depth_contour_lines = Path(self.isodepth).with_suffix(".depth_contour_lines"+Path(self.isodepth).suffix)
        depth_contour_poly = Path(self.isodepth).with_suffix(".depth_contour_poly"+Path(self.isodepth).suffix)

        # velocity parameters
        self.input_velocity_layer = self.parameterAsRasterLayer(
            parameters, self.INPUT_VELOCITY, context
        )
        input_velocity = Path(self.input_velocity_layer.dataProvider().dataSourceUri())
        input_velocity_band = self.parameterAsInt(
            parameters, self.INPUT_VELOCITY_BAND, context
        )
        if not input_velocity_band:
            if hasattr(self.input_velocity_layer.renderer(), 'band'):
                input_velocity_band = self.input_velocity_layer.renderer().band()
            elif hasattr(self.input_velocity_layer.renderer(), 'inputBand'):
                input_velocity_band = self.input_velocity_layer.renderer().inputBand()
            else:
                input_velocity_band = 1
        velocity_classes = self.parameterAsString(
            parameters, self.VELOCITY_CLASSES, context
        ).strip().split()
        self.isovelocity = self.parameterAsOutputLayer(
            parameters, self.OUTPUT_VELOCITY, context
        )
        output_velocity = parameters.get(self.OUTPUT_VELOCITY)
        self.produce_velocity = False if output_velocity is None else True
        #self.velocity_sink, _ = output_velocity.sink.value(context.expressionContext(), None)

        # hazard parameters
        hazard_classes = self.parameterAsString(parameters, self.HAZARD_CLASSES, context)
        self.hazard_output = self.parameterAsOutputLayer(parameters, self.OUTPUT_HAZARD, context)
        output_hazard = parameters.get(self.OUTPUT_HAZARD)
        self.produce_hazard = False if output_hazard is None else True
        #self.hazard_sink, _ = output_hazard.sink.value(context.expressionContext(), None)

        # check consistency for hazard classes
        if self.produce_hazard:
            # force velocity processing
            self.produce_velocity = True
            nd = len(depth_classes)
            nv = len(velocity_classes)
            hc_row = hazard_classes.strip().split('\n')
            if len(hc_row) != nd:
                raise QgsProcessingException(f'hazard classes must be a {nd}x{nv} matrix, {nd} rows, {nv} columns')
            for row in hc_row:
                if nv != len(row.split()):
                    raise QgsProcessingException(f'hazard classes must be a {nd}x{nv} matrix, {nd} rows, {nv} columns')

        # DEPTH ##############
        processing.run(
            "hydra:blurred",
            {
                "INPUT": str(input_depth),
                "OUTPUT": str(depth_blur),
                "IGNORE_NODATA": True,
                "BAND": input_depth_band
            },
            feedback=feedback,
        )
        feedback.setCurrentStep(1)
        self.tmpfiles.append(depth_blur)

        processing.run(
            "gdal:contour",
            {
                "INPUT": str(depth_blur),
                "OUTPUT": str(depth_contour_lines),
                "BAND": 1,
                "IGNORE_NODATA": True,
                "FIELD_NAME": "min",
                "EXTRA": "-fl " + ' '.join(depth_classes),
            },
            feedback=feedback,
        )
        feedback.setCurrentStep(2)
        self.tmpfiles.append(depth_contour_lines)

        feedback.pushInfo("Polygonize contours")
        self.__Polygonize(
            str(depth_contour_lines),
            str(depth_contour_poly),
            minimum_contour_length,
            extent=self.__RasterExtent(str(depth_blur)),
        )
        self.tmpfiles.append(depth_contour_poly)
        feedback.setCurrentStep(3)

        # blurring with nodata
        processing.run(
            "hydra:blurred",
            {
                "INPUT": str(input_depth),
                "OUTPUT": str(depth_blur),
                "IGNORE_NODATA": False,
                "BAND": input_depth_band
            },
            feedback=feedback,
        )

        # Zonal statistics
        feedback.pushInfo("Zonal statistics")
        processing.run(
            "native:zonalstatisticsfb",
            {
                "COLUMN_PREFIX": "_",
                "INPUT_RASTER": str(depth_blur),
                "INPUT": str(depth_contour_poly),
                "OUTPUT": self.isodepth,
                "RASTER_BAND": 1,
                "STATISTICS": [0, 2, 3, 5, 6],  # count mean median min max
            },
            feedback=feedback,
        )
        feedback.setCurrentStep(4)

        feedback.pushInfo("Classify isodepth")
        self.__Classify(self.isodepth, ' '.join(depth_classes))
        feedback.setCurrentStep(5)

        feedback.pushInfo("Writing qml style for depth")
        style(self.isodepth, depth_classes, "min", "m")
        results[self.OUTPUT_DEPTH] = self.isodepth
        feedback.setCurrentStep(6)

        # VELOCITY ##############
        if self.produce_velocity:

            # prepare Path for reuse
            isovelocity_path = Path(self.isovelocity)

            # blurring
            velocity_blur = Path(self.isovelocity).with_suffix('.velocity_blur.vrt')
            processing.run(
                "hydra:blurred",
                {
                    "INPUT": str(input_velocity),
                    "OUTPUT": str(velocity_blur),
                    "IGNORE_NODATA": True,
                    "BAND": input_velocity_band
                },
                feedback=feedback,
            )
            self.tmpfiles.append(velocity_blur)

            # contour lines
            velocity_contour_lines = isovelocity_path.with_suffix(
                ".velocity_contour_lines"+isovelocity_path.suffix
            )
            assert velocity_classes[0] == '0'
            processing.run(
                "gdal:contour",
                {
                    "INPUT": str(velocity_blur),
                    "OUTPUT": str(velocity_contour_lines),
                    "BAND": 1,
                    "IGNORE_NODATA": True,
                    "FIELD_NAME": "min",
                    "EXTRA": "-fl " + ' '.join(velocity_classes[1:]),
                },
                feedback=feedback,
            )
            self.tmpfiles.append(velocity_contour_lines)
            feedback.setCurrentStep(7)

            feedback.pushInfo("Polygonize velocity contours using shapely")
            velocity_contour_poly = isovelocity_path.with_suffix(".velocity_contour_poly"+isovelocity_path.suffix)
            self.__Polygonize(
                str(velocity_contour_lines),
                str(velocity_contour_poly),
                minimum_contour_length,
                extent=self.__RasterExtent(str(velocity_blur)),
            )
            self.tmpfiles.append(velocity_contour_poly)
            feedback.setCurrentStep(8)

            # blurring with nodata
            processing.run(
                "hydra:blurred",
                {
                    "INPUT": str(input_velocity),
                    "OUTPUT": str(velocity_blur),
                    "IGNORE_NODATA": False,
                    "BAND": input_velocity_band
                },
                feedback=feedback,
            )

            # Zonal statistics
            velocity_contour_zonal = isovelocity_path.with_suffix(
                ".velocity_contour_zonal"+isovelocity_path.suffix
            )
            feedback.pushInfo("Zonal statistics for velocity")
            processing.run(
                "native:zonalstatisticsfb",
                {
                    "COLUMN_PREFIX": "_",
                    "INPUT_RASTER": str(velocity_blur),
                    "INPUT": str(velocity_contour_poly),
                    "OUTPUT": str(velocity_contour_zonal),
                    "RASTER_BAND": 1,
                    "STATISTICS": [0, 2, 3, 5, 6],  # count mean median min max
                },
                feedback=feedback,
            )
            self.tmpfiles.append(velocity_contour_zonal)
            feedback.setCurrentStep(9)

            feedback.pushInfo("Classify velocity")
            self.__Classify(str(velocity_contour_zonal), ' '.join(velocity_classes[1:]))
            feedback.setCurrentStep(10)

            # create zero velocity zone where there is water but no speed
            # first dissolve depth classes
            wet_areas = isovelocity_path.with_suffix(".wet_areas"+isovelocity_path.suffix)
            processing.run(
                "native:dissolve",
                {
                    "FIELD": [],
                    "INPUT": self.isodepth,
                    "OUTPUT": str(wet_areas),
                },
                feedback=feedback,
            )
            self.tmpfiles.append(wet_areas)
            feedback.setCurrentStep(11)

            processing.run(
                "native:intersection",
                {
                    "INPUT": str(velocity_contour_zonal),
                    "INPUT_FIELDS": ['fid', 'min', 'max'],
                    "OVERLAY": str(wet_areas),
                    "OVERLAY_FIELDS": None,
                    "OUTPUT": self.isovelocity,
                },
                feedback=feedback,
            )

            velocity_nonzero_source = QgsVectorLayer(
                self.isovelocity, Path(self.isovelocity).name
            )
            with edit(velocity_nonzero_source):
                velocity_nonzero_source.dataProvider().deleteAttributes([3,4,5])

            # Difference velocity > 0 and wet area to define velocity = 0
            velocity_first_class = isovelocity_path.with_suffix(".velocity_first_class"+isovelocity_path.suffix)
            velocity_first_class_multi = isovelocity_path.with_suffix(".velocity_first_class_multi"+isovelocity_path.suffix)
            processing.run(
                "native:difference",
                {
                    "INPUT": str(wet_areas),
                    "OVERLAY": self.isovelocity,
                    "OUTPUT": str(velocity_first_class_multi),
                },
                feedback=feedback,
            )
            self.tmpfiles.append(velocity_first_class_multi)
            feedback.setCurrentStep(12)

            # explode multi polygons to simple polygons
            processing.run(
                "native:multiparttosingleparts",
                {
                    "INPUT": str(velocity_first_class_multi),
                    "OUTPUT": str(velocity_first_class)
                },
                feedback=feedback
            )
            self.tmpfiles.append(velocity_first_class)

            # adding first class to output layer
            vfirst_class_source = QgsVectorLayer(
                str(velocity_first_class), velocity_first_class.name
            )

            with edit(velocity_nonzero_source):
                idx = velocity_nonzero_source.fields().indexFromName("fid")
                with edit(vfirst_class_source):
                    for feature in vfirst_class_source.getFeatures():
                        # overwrite firstclass min/max
                        feature["min"] = velocity_classes[0]
                        feature["max"] = velocity_classes[1]
                        vfirst_class_source.updateFeature(feature)

                        if idx is not None:  # check if there is an "fid" attribute
                            feature[idx] = None  # clear attribute
                        #res = velocity_nonzero_source.addFeature(feature)

            feedback.setCurrentStep(13)

            feedback.pushInfo("Styling velocity")
            style(
                self.isovelocity,
                velocity_classes,
                "min",
                "m/s",
            )
            feedback.setCurrentStep(14)

            results[self.OUTPUT_VELOCITY] = self.isovelocity
        # end velocity

        # HAZARD
        if self.produce_hazard:
            datasource = ogr.Open(str(self.isodepth))
            layer = datasource.GetLayer(0)
            srs = layer.GetSpatialRef()

            # intersections des combinaisons de classe de v et de h
            # -> 15 operation de 10 sec ~3min
            # union des polygones par classe d'alea
            # identification des petits polygones d'alea
            # retrouver les petits morceaux de compbinaison qui composent ces petits polygon
            # pour chaque morceau trouver le polygon de vitesse dont la vitesse est différente et qui le touche et l'unir à ce morceau et le soustraire au polygone de même vitesse

            feedback.pushInfo("Create depth classes intersections with velocity classes")
            feedback.setProgress(1)

            step = 0
            nstep = len(depth_classes) * len(
                velocity_classes
            )
            for dc in depth_classes:
                for vc in velocity_classes:
                    union_dc_vc = Path(self.hazard_output).with_suffix(f".union_{dc}m_{vc}ms"+Path(self.hazard_output).suffix)
                    self.tmpfiles.append(union_dc_vc)

                    feedback.pushInfo(f"create {union_dc_vc}")
                    param = {
                        "INPUT": f"{self.isodepth}|subset=min='{dc}'",
                        "INPUT_FIELDS": ['min', 'max'],
                        "OUTPUT": str(union_dc_vc),
                        "OVERLAY": f"{self.isovelocity}|subset=min='{vc}'",
                        "OVERLAY_FIELDS": ['min', 'max'],
                        "OVERLAY_FIELDS_PREFIX": "v",
                    }
                    processing.run("qgis:intersection", param)
                    step += 1
                    feedback.setProgress(100 * (step / nstep))
                    if feedback.isCanceled():
                        return results
            feedback.setCurrentStep(1)

            feedback.pushInfo("Assemble similar classes into a layer per class")

            risk_classes = [
                row.strip().split()
                for row in hazard_classes.strip().split("\n")
            ]

            if Path(self.hazard_output).suffix == ".gpkg":
                outDriver = ogr.GetDriverByName("GPKG")
            elif Path(self.hazard_output).suffix == ".shp":
                outDriver = ogr.GetDriverByName("ESRI Shapefile")

            # Remove output if it already exists and create one file per risk class
            outDataSource = {}
            outLayer = {}
            for c in {cl for row in risk_classes for cl in row}:
                output_polygon_file = Path(self.hazard_output).with_suffix(f'.{c}'+Path(self.hazard_output).suffix)
                self.tmpfiles.append(output_polygon_file)

                if output_polygon_file.exists():
                    outDriver.DeleteDataSource(str(output_polygon_file))
                outDataSource[c] = outDriver.CreateDataSource(
                    str(output_polygon_file)
                )  #  file creation
                outLayer[c] = outDataSource[c].CreateLayer(output_polygon_file.stem, srs, ogr.wkbMultiPolygon)

            for i, dc in enumerate(depth_classes):
                for j, vc in enumerate(velocity_classes):
                    c = risk_classes[i][j]
                    source = ogr.Open(str(Path(self.hazard_output).with_suffix(f".union_{dc}m_{vc}ms"+Path(self.hazard_output).suffix)))
                    slayer = source.GetLayer(0)
                    for feature in slayer:
                        featureDefn = outLayer[c].GetLayerDefn()
                        f = ogr.Feature(featureDefn)
                        f.SetGeometry(feature.geometry())
                        outLayer[c].CreateFeature(f)
                        f = None
            feedback.setCurrentStep(2)

            feedback.pushInfo("Dissolve class layer")
            step = 0
            nstep = len({cl for row in risk_classes for cl in row})
            for c in {cl for row in risk_classes for cl in row}:
                del outLayer[c]
                del outDataSource[c]

                param = {
                    "FIELD": [],
                    "INPUT": str(Path(self.hazard_output).with_suffix(f'.{c}'+Path(self.hazard_output).suffix)),
                    "OUTPUT": str(Path(self.hazard_output).with_suffix(f'.{c}_dissolved'+Path(self.hazard_output).suffix)),
                }
                processing.run("qgis:dissolve", param)
                param = {
                    "FIELD": [],
                    "INPUT": str(Path(self.hazard_output).with_suffix(f'.{c}_dissolved'+Path(self.hazard_output).suffix)),
                    "OUTPUT": str(Path(self.hazard_output).with_suffix(f'.{c}'+Path(self.hazard_output).suffix)),
                }
                processing.run("qgis:multiparttosingleparts", param)

                # remove intermediate step
                outDriver.DeleteDataSource(str(Path(self.hazard_output).with_suffix(f'.{c}_dissolved'+Path(self.hazard_output).suffix)))
                step += 1
                feedback.setProgress(100 * (step / nstep))
            feedback.setCurrentStep(3)

            feedback.pushInfo("Recompose output layer from class layers")
            outDataSource = outDriver.CreateDataSource(self.hazard_output)  #  file creation
            outLayer = outDataSource.CreateLayer(Path(self.hazard_output).stem, srs, ogr.wkbPolygon)
            field_class = ogr.FieldDefn("class", ogr.OFTString)
            field_class.SetWidth(max([len(c) for row in risk_classes for c in row]))
            outLayer.CreateField(field_class)
            for c in {cl for row in risk_classes for cl in row}:
                source = ogr.Open(str(Path(self.hazard_output).with_suffix(f'.{c}'+Path(self.hazard_output).suffix)))
                slayer = source.GetLayer(0)
                for feature in slayer:
                    featureDefn = outLayer.GetLayerDefn()
                    f = ogr.Feature(featureDefn)
                    f["class"] = c
                    f.SetGeometry(feature.geometry())
                    outLayer.CreateFeature(f)
                    f = None

            results[self.OUTPUT_HAZARD] = self.hazard_output
            feedback.setCurrentStep(4)

            feedback.pushInfo("Adds symbology")
            seen = set()  # to help buil a unique list of classes conserving order
            style(
                self.hazard_output,
                [
                    x
                    for row in risk_classes
                    for x in row
                    if x not in seen and not seen.add(x)
                ],
                "class",
                "",
            )
            feedback.setCurrentStep(5)

        return results

    def postProcessAlgorithm(self, context, feedback):
        """
        https://github.com/qgis/QGIS/blob/5f2326ac2f53cbb116b8fdce055e91cf000c8ed2/python/core/auto_generated/processing/qgsprocessingalgorithm.sip.in#L627
        """
    
        def find_group_by_id(layer_id, racine, group=None):
            if group is None:
                group = racine
            for child in group.children():
                if isinstance(child, QgsLayerTreeLayer) and child.layerId() == layer_id:
                    return group
                elif isinstance(child, QgsLayerTreeGroup):
                    result = find_group_by_id(layer_id, racine, child)
                    if result:
                        return result
            return None

        root = QgsProject.instance().layerTreeRoot()
        depth_id = self.input_depth_layer.id()
        group = find_group_by_id(depth_id, root)
        subgrp = 'raster vectorisation'
        layer = QgsVectorLayer(self.isodepth, Path(self.isodepth).name)
        if group:
            subgroup = group.findGroup(subgrp) or group.insertGroup(0, subgrp)
        else:
            subgroup = root.findGroup(subgrp) or root.insertGroup(0, subgrp)
        QgsProject.instance().addMapLayer(layer, False)
        subgroup.addLayer(layer)
        res = {self.OUTPUT_DEPTH: self.isodepth}

        if self.produce_velocity:
            layer = QgsVectorLayer(self.isovelocity, Path(self.isovelocity).name)
            QgsProject.instance().addMapLayer(layer, False)
            subgroup.addLayer(layer)
            res[self.OUTPUT_VELOCITY] = self.isovelocity

        if self.produce_hazard:
            layer = QgsVectorLayer(self.hazard_output, Path(self.hazard_output).name)
            QgsProject.instance().addMapLayer(layer, False)
            subgroup.addLayer(layer)
            res[self.OUTPUT_HAZARD] = self.hazard_output

        # cleanup temp files
        for tmpfile in self.tmpfiles:
            try:
                tmpfile.unlink()
            except PermissionError as err:
                feedback.pushWarning(f'temporary file not deleted, reason: {err}')

        return res

    def __Classify(self, gpkg, classes):
        classes = [999] + [float(v) for v in reversed(classes.strip().split())]
        datasource = QgsVectorLayer(gpkg, Path(gpkg).name)

        todelete = []
        with edit(datasource):
            datasource.addAttribute(QgsField("min", QVariant.String))
            datasource.addAttribute(QgsField("max", QVariant.String))
            datasource.updateFields()
            for feat in datasource.getFeatures():
                if feat["_median"] <= classes[-1]:
                    todelete.append(feat.id())
                for cmin, cmax in zip(classes[1:], classes[:-1]):
                    if feat["_median"] > cmin:
                        feat["min"] = cmin
                        feat["max"] = cmax
                        datasource.updateFeature(feat)
                        break
            for featid in todelete:
                datasource.deleteFeature(featid)

            for attname in ["_count", "_mean", "_median", "_min", "_max"]:
                datasource.deleteAttribute(datasource.fields().indexFromName(attname))
            datasource.updateFields()

    def __RasterExtent(self, raster_file):
        datasource = gdal.Open(raster_file.strip(), gdal.GA_ReadOnly)
        geoTransform = datasource.GetGeoTransform()
        minx = geoTransform[0]
        maxy = geoTransform[3]
        maxx = minx + geoTransform[1] * datasource.RasterXSize
        miny = maxy + geoTransform[5] * datasource.RasterYSize
        extent = [minx, miny, maxx, maxy]
        del datasource
        return extent

    def __Polygonize(
        self,
        input_linestring_file,
        output_polygon_file,
        too_short_a_length=0,
        extent=None,
    ):
        datasource = ogr.Open(input_linestring_file)
        layer = datasource.GetLayer(0)
        assert layer.GetGeomType() == ogr.wkbLineString

        # snapping is not a good idea
        # linework = [[(round(p[0],1), round(p[1],1)) for p in feature.geometry().GetPoints()]
        #    for feature in layer if feature.geometry().Length()>too_short_a_length]


        linework = [
            feature.geometry().GetPoints()
            for feature in layer
            if feature.geometry().Length() > too_short_a_length
        ]
        if extent:
            left, bottom, right, top = [], [], [], []
            # adds lines on the border
            for line in linework:
                if line[0] != line[-1]:
                    for x, y in (line[0], line[-1]):
                        if x - extent[0] == 0:
                            left.append((x, y))
                        elif y - extent[1] == 0:
                            bottom.append((x, y))
                        elif x - extent[2] == 0:
                            right.append((x, y))
                        elif y - extent[3] == 0:
                            top.append((x, y))
            # create contour linework starting bootom left trigo direction
            contour = (
                [(extent[0], extent[1])]
                + list(sorted(bottom, key=lambda p: p[0]))
                + [(extent[2], extent[1])]
                + list(sorted(right, key=lambda p: p[1]))
                + [(extent[2], extent[3])]
                + list(reversed(sorted(top, key=lambda p: p[0])))
                + [(extent[0], extent[3])]
                + list(reversed(sorted(left, key=lambda p: p[1])))
                + [(extent[0], extent[1])]
            )

            linework += [[s, e] for s, e in zip(contour[:-1], contour[1:])]

        polygons = polygonize(linework)

        if output_polygon_file.endswith(".gpkg"):
            outDriver = ogr.GetDriverByName("GPKG")
        elif output_polygon_file.endswith(".shp"):
            outDriver = ogr.GetDriverByName("ESRI Shapefile")

        # Remove output if it already exists
        if os.path.exists(output_polygon_file):
            outDriver.DeleteDataSource(output_polygon_file)

        outDataSource = outDriver.CreateDataSource(output_polygon_file)  # file creation
        outLayer = outDataSource.CreateLayer(
            os.path.basename(output_polygon_file)[: output_polygon_file.rfind(".")],
            layer.GetSpatialRef(),
            ogr.wkbPolygon,
        )

        assert outLayer

        # Create the feature and set values
        featureDefn = outLayer.GetLayerDefn()
        for i, p in enumerate(polygons):
            feature = ogr.Feature(featureDefn)
            feature.SetGeometry(ogr.CreateGeometryFromWkb(p.wkb))
            outLayer.CreateFeature(feature)
            feature = None


def style(gpkg, classes, field, units):
    if units == "m":
        col_min = [216, 231, 245]
        col_max = [8, 48, 107]
    else:
        col_min = [255, 200, 200]
        col_max = [255, 0, 0]
    with open(gpkg.rsplit(".", 1)[0] + ".qml", "w") as qml:
        qml.write(
            "\n".join(
                (
                    '<!DOCTYPE qgis PUBLIC "http://mrcc.com/qgis.dtd" "SYSTEM">',
                    '<qgis styleCategories="Symbology">',
                    f'  <renderer-v2 attr="{field}" type="categorizedSymbol" symbollevels="0" enableorderby="0" forceraster="0">',
                    "    <categories>\n",
                )
            )
        )
        i = 0
        for i, c in enumerate(classes):
            qml.write(
                f"""      <category symbol="{i}" label="{c} {units}" value="{c}" render="true"/>\n"""
            )
        qml.write("    </categories>\n")
        qml.write("    <symbols>\n")
        i = 0
        for i, c in enumerate(classes):
            qml.write(
                "\n".join(
                    (
                        '     <symbol type="fill" force_rhr="0" name="{}" clip_to_extent="1" alpha="1">',
                        '       <layer locked="0" enabled="1" class="SimpleFill" pass="0">',
                        '         <prop k="color" v="{},{},{},255"/>',
                        '         <prop k="outline_style" v="no"/>',
                        "           <data_defined_properties>",
                        '             <Option type="Map">',
                        '               <Option type="QString" value="" name="name"/>',
                        '               <Option name="properties"/>',
                        '               <Option type="QString" value="collection" name="type"/>',
                        "             </Option>",
                        "           </data_defined_properties>",
                        "       </layer>",
                        "     </symbol>\n",
                    )
                ).format(
                    i,
                    int(col_min[0] - (col_min[0] - col_max[0]) * (i) / len(classes)),
                    int(col_min[1] - (col_min[1] - col_max[1]) * (i) / len(classes)),
                    int(col_min[2] - (col_min[2] - col_max[2]) * (i) / len(classes)),
                )
            )
        qml.write(
            "\n".join(
                (
                    "    </symbols>",
                    "  <rotation/>",
                    "  <sizescale/>",
                    " </renderer-v2>",
                    " <blendMode>0</blendMode>",
                    " <featureBlendMode>0</featureBlendMode>",
                    " <layerGeometryType>2</layerGeometryType>",
                    "</qgis>\n",
                )
            )
        )
