create schema hydra;
create schema work;
create type hydra_param_sor1_type as enum ('link', 'upstream_singularity', 'hydrologic_element', 'downstream_upstream_singularity', 'upstream_downstream_singularity', 'downstream_singularity');
create table hydra.param_sor1_type(
    name hydra_param_sor1_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.param_sor1_type(name) values ('link');
insert into hydra.param_sor1_type(name) values ('upstream_singularity');
insert into hydra.param_sor1_type(name) values ('hydrologic_element');
insert into hydra.param_sor1_type(name) values ('downstream_upstream_singularity');
insert into hydra.param_sor1_type(name) values ('upstream_downstream_singularity');
insert into hydra.param_sor1_type(name) values ('downstream_singularity');
create type hydra_fuse_spillway_break_mode as enum ('none', 'time_critical', 'zw_critical');
create table hydra.fuse_spillway_break_mode(
    name hydra_fuse_spillway_break_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.fuse_spillway_break_mode(name,id,description) values ('none',0,'No break');
insert into hydra.fuse_spillway_break_mode(name,id,description) values ('time_critical',2,'Break on time');
insert into hydra.fuse_spillway_break_mode(name,id,description) values ('zw_critical',1,'Break on water level');
create type hydra_model_connect_mode as enum ('zq_downstream_condition', 'hydrograph');
create table hydra.model_connect_mode(
    name hydra_model_connect_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.model_connect_mode(name,id,description) values ('zq_downstream_condition',2,'Downstream condition: Q(Z)');
insert into hydra.model_connect_mode(name,id,description) values ('hydrograph',1,'Hydrograph');
create type hydra_valve_mode as enum ('no_valve', 'one_way_downstream', 'one_way_upstream');
create table hydra.valve_mode(
    name hydra_valve_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.valve_mode(name,id,description) values ('no_valve',1,'no valve');
insert into hydra.valve_mode(name,id,description) values ('one_way_downstream',2,'one way valve (toward downstream)');
insert into hydra.valve_mode(name,id,description) values ('one_way_upstream',3,'one way valve (toward upstream)');
create type hydra_netflow_type as enum ('holtan', 'permeable_soil', 'horner', 'scs', 'constant_runoff');
create table hydra.netflow_type(
    name hydra_netflow_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.netflow_type(name,id,description) values ('holtan',3,'Holtan');
insert into hydra.netflow_type(name,id,description) values ('permeable_soil',5,'Permeable soil');
insert into hydra.netflow_type(name,id,description) values ('horner',2,'Horner');
insert into hydra.netflow_type(name,id,description) values ('scs',4,'SCS');
insert into hydra.netflow_type(name,id,description) values ('constant_runoff',1,'Constant runoff');
create type hydra_rainfall_interpolation_type as enum ('distance_ponderation', 'thyssen');
create table hydra.rainfall_interpolation_type(
    name hydra_rainfall_interpolation_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.rainfall_interpolation_type(name) values ('distance_ponderation');
insert into hydra.rainfall_interpolation_type(name) values ('thyssen');
create type hydra_weir_mode_reoxy as enum ('gameson', 'r15');
create table hydra.weir_mode_reoxy(
    name hydra_weir_mode_reoxy primary key,
    id integer unique,
    description varchar
    );
insert into hydra.weir_mode_reoxy(name,id,description) values ('gameson',1,'Gameson law');
insert into hydra.weir_mode_reoxy(name,id,description) values ('r15',2,'R15 law');
create type hydra_rerouting_option as enum ('qlimit', 'fraction');
create table hydra.rerouting_option(
    name hydra_rerouting_option primary key,
    id integer unique,
    description varchar
    );
insert into hydra.rerouting_option(name,id,description) values ('qlimit',2,'Q limit (m3/s)');
insert into hydra.rerouting_option(name,id,description) values ('fraction',1,'Fraction (between 0 et 1)');
create type hydra_runoff_type as enum ('socose', 'giandotti', 'define_k', 'passini', 'k_desbordes');
create table hydra.runoff_type(
    name hydra_runoff_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.runoff_type(name,id,description) values ('socose',4,'Tc imposed and Socose');
insert into hydra.runoff_type(name,id,description) values ('giandotti',3,'k-Giandotti and linear reservoir');
insert into hydra.runoff_type(name,id,description) values ('define_k',5,'Defined K and linear reservoir');
insert into hydra.runoff_type(name,id,description) values ('passini',2,'k-Passini  and linear reservoir');
insert into hydra.runoff_type(name,id,description) values ('k_desbordes',1,'k-Desbordes  and linear reservoir');
create type hydra_rainfall_type as enum ('intensity_curve', 'single_triangular', 'double_triangular', 'radar', 'caquot', 'gage');
create table hydra.rainfall_type(
    name hydra_rainfall_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.rainfall_type(name,id) values ('intensity_curve',4);
insert into hydra.rainfall_type(name,id) values ('single_triangular',2);
insert into hydra.rainfall_type(name,id) values ('double_triangular',3);
insert into hydra.rainfall_type(name,id) values ('radar',6);
insert into hydra.rainfall_type(name,id) values ('caquot',1);
insert into hydra.rainfall_type(name,id) values ('gage',5);
create type hydra_cross_section_type as enum ('valley', 'circular', 'pipe', 'ovoid', 'channel');
create table hydra.cross_section_type(
    name hydra_cross_section_type primary key,
    id integer unique,
    description varchar,
    abbreviation varchar unique
    );
insert into hydra.cross_section_type(name,id,abbreviation) values ('valley',2,'VL');
insert into hydra.cross_section_type(name,id,abbreviation) values ('circular',6,'CI');
insert into hydra.cross_section_type(name,id,abbreviation) values ('pipe',3,'PF');
insert into hydra.cross_section_type(name,id,abbreviation) values ('ovoid',5,'OV');
insert into hydra.cross_section_type(name,id,abbreviation) values ('channel',4,'PO');
create type hydra_type_sor1 as enum ('hys_file', 'deb_or_lim_file');
create table hydra.type_sor1(
    name hydra_type_sor1 primary key,
    id integer unique,
    description varchar
    );
insert into hydra.type_sor1(name,id,description) values ('hys_file',1,'hys file');
insert into hydra.type_sor1(name,id,description) values ('deb_or_lim_file',2,'deb/lim file');
create type hydra_borda_headloss_singularity_type as enum ('sharp_bend_rectangular', 'friction', 'sharp_transition_geometry', 'screen_normal_to_flow', 'enlargment_with_transition', 'sharp_angle_bend', 'v', 'circular_bend', 'contraction_with_transition', 'parametric', 'q', 'screen_oblique_to_flow');
create table hydra.borda_headloss_singularity_type(
    name hydra_borda_headloss_singularity_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.borda_headloss_singularity_type(name,id,description) values ('sharp_bend_rectangular',18,'Sharp bend rectangular');
insert into hydra.borda_headloss_singularity_type(name,id,description) values ('friction',16,'Friction');
insert into hydra.borda_headloss_singularity_type(name,id,description) values ('sharp_transition_geometry',9,'Sharp geometry transition');
insert into hydra.borda_headloss_singularity_type(name,id,description) values ('screen_normal_to_flow',14,'Screen normal to flow');
insert into hydra.borda_headloss_singularity_type(name,id,description) values ('enlargment_with_transition',11,'Enlargment with transition');
insert into hydra.borda_headloss_singularity_type(name,id,description) values ('sharp_angle_bend',13,'Sharp angle bend');
insert into hydra.borda_headloss_singularity_type(name,id,description) values ('v',2,'Law dE=(K/2g).(Q2/S2)');
insert into hydra.borda_headloss_singularity_type(name,id,description) values ('circular_bend',12,'Circular bend');
insert into hydra.borda_headloss_singularity_type(name,id,description) values ('contraction_with_transition',10,'Contraction with transition');
insert into hydra.borda_headloss_singularity_type(name,id,description) values ('parametric',17,'Parametric');
insert into hydra.borda_headloss_singularity_type(name,id,description) values ('q',1,'Law dz=KQ2');
insert into hydra.borda_headloss_singularity_type(name,id,description) values ('screen_oblique_to_flow',15,'Screen oblique to flow');
create type hydra_link_type as enum ('street', 'mesh_2d', 'regul_gate', 'strickler', 'fuse_spillway', 'routing_hydrology', 'connector_hydrology', 'network_overflow', 'gate', 'borda_headloss', 'deriv_pump', 'pump', 'porous', 'connector', 'pipe', 'weir', 'overflow');
create table hydra.link_type(
    name hydra_link_type primary key,
    id integer unique,
    description varchar,
    abbreviation varchar unique
    );
insert into hydra.link_type(name,id,abbreviation) values ('street',16,'LRUE');
insert into hydra.link_type(name,abbreviation,id) values ('mesh_2d','LPAV',11);
insert into hydra.link_type(name,id,abbreviation) values ('regul_gate',7,'RG');
insert into hydra.link_type(name,abbreviation,id) values ('strickler','LSTK',12);
insert into hydra.link_type(name,id,abbreviation) values ('fuse_spillway',15,'LDVF');
insert into hydra.link_type(name,abbreviation,id) values ('routing_hydrology','ROUT',18);
insert into hydra.link_type(name,abbreviation,id) values ('connector_hydrology','TRH',19);
insert into hydra.link_type(name,id,abbreviation) values ('network_overflow',22,'NOV');
insert into hydra.link_type(name,id,abbreviation) values ('gate',5,'LORF');
insert into hydra.link_type(name,id,abbreviation) values ('borda_headloss',10,'QMK');
insert into hydra.link_type(name,id,abbreviation) values ('deriv_pump',3,'QDP');
insert into hydra.link_type(name,id,abbreviation) values ('pump',6,'QMP');
insert into hydra.link_type(name,abbreviation,id) values ('porous','LPOR',13);
insert into hydra.link_type(name,id,abbreviation) values ('connector',9,'QMTR');
insert into hydra.link_type(name,abbreviation,id) values ('pipe','TRC',20);
insert into hydra.link_type(name,id,abbreviation) values ('weir',4,'LDEV');
insert into hydra.link_type(name,abbreviation,id) values ('overflow','OFL',21);
create type hydra_land_use_type as enum ('individual_housing', 'rural', 'industrial', 'group_housing');
create table hydra.land_use_type(
    name hydra_land_use_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.land_use_type(name,id) values ('individual_housing',3);
insert into hydra.land_use_type(name,id) values ('rural',1);
insert into hydra.land_use_type(name,id) values ('industrial',2);
insert into hydra.land_use_type(name,id) values ('group_housing',4);
create type hydra_model_connection_settings as enum ('mixed', 'global', 'cascade');
create table hydra.model_connection_settings(
    name hydra_model_connection_settings primary key,
    id integer unique,
    description varchar
    );
insert into hydra.model_connection_settings(name,id) values ('mixed',3);
insert into hydra.model_connection_settings(name,id) values ('global',2);
insert into hydra.model_connection_settings(name,id) values ('cascade',1);
create type hydra_node_type as enum ('manhole_hydrology', 'elem_2d', 'crossroad', 'catchment', 'river', 'manhole', 'storage', 'station');
create table hydra.node_type(
    name hydra_node_type primary key,
    id integer unique,
    description varchar,
    abbreviation varchar unique
    );
insert into hydra.node_type(name,id,abbreviation) values ('manhole_hydrology',8,'NODH');
insert into hydra.node_type(name,abbreviation,id) values ('elem_2d','PAV',5);
insert into hydra.node_type(name,id,abbreviation) values ('crossroad',6,'CAR');
insert into hydra.node_type(name,id,abbreviation) values ('catchment',9,'BC');
insert into hydra.node_type(name,abbreviation,id) values ('river','NODR',2);
insert into hydra.node_type(name,id,abbreviation) values ('manhole',1,'NODA');
insert into hydra.node_type(name,abbreviation,id) values ('storage','CAS',4);
insert into hydra.node_type(name,abbreviation,id) values ('station','NODS',3);
create type hydra_weir_mode_regul as enum ('width', 'elevation');
create table hydra.weir_mode_regul(
    name hydra_weir_mode_regul primary key,
    id integer unique,
    description varchar
    );
insert into hydra.weir_mode_regul(name,id,description) values ('width',2,'Width');
insert into hydra.weir_mode_regul(name,id,description) values ('elevation',1,'Elevation');
create type hydra_type_domain as enum ('river', 'network', '2d_domain');
create table hydra.type_domain(
    name hydra_type_domain primary key,
    id integer unique,
    description varchar
    );
insert into hydra.type_domain(name,id) values ('river',2);
insert into hydra.type_domain(name,id) values ('network',1);
insert into hydra.type_domain(name,id) values ('2d_domain',3);
create type hydra_pollution_treatment_mode as enum ('residual_concentration', 'efficiency');
create table hydra.pollution_treatment_mode(
    name hydra_pollution_treatment_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.pollution_treatment_mode(name,id,description) values ('residual_concentration',1,'Residual concentration');
insert into hydra.pollution_treatment_mode(name,id,description) values ('efficiency',2,'Efficiency');
create type hydra_split_law_type as enum ('weir', 'zq', 'stickler', 'gate');
create table hydra.split_law_type(
    name hydra_split_law_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.split_law_type(name,id) values ('weir',1);
insert into hydra.split_law_type(name,id) values ('zq',4);
insert into hydra.split_law_type(name,id) values ('stickler',3);
insert into hydra.split_law_type(name,id) values ('gate',2);
create type hydra_computation_mode as enum ('hydraulics', 'hydrology', 'hydrology_and_hydraulics');
create table hydra.computation_mode(
    name hydra_computation_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.computation_mode(name,id) values ('hydraulics',2);
insert into hydra.computation_mode(name,id) values ('hydrology',1);
insert into hydra.computation_mode(name,id) values ('hydrology_and_hydraulics',3);
create type hydra_singularity_type as enum ('param_headloss', 'model_connect_bc', 'gate', 'reservoir_rsp_hydrology', 'bradley_headloss', 'marker', 'pipe_branch_marker', 'tz_bc', 'hydrology_bc', 'zregul_weir', 'reservoir_rs_hydrology', 'froude_bc', 'zq_split_hydrology', 'qq_split_hydrology', 'tank_bc', 'hydrograph_bc', 'bridge_headloss', 'constant_inflow_bc', 'strickler_bc', 'hydraulic_cut', 'zq_bc', 'regul_sluice_gate', 'borda_headloss', 'weir_bc');
create table hydra.singularity_type(
    name hydra_singularity_type primary key,
    id integer unique,
    description varchar,
    abbreviation varchar unique
    );
insert into hydra.singularity_type(name,id,abbreviation) values ('param_headloss',4,'DH');
insert into hydra.singularity_type(name,id,abbreviation) values ('model_connect_bc',19,'RACC');
insert into hydra.singularity_type(name,id,abbreviation) values ('gate',1,'VA');
insert into hydra.singularity_type(name,id,abbreviation) values ('reservoir_rsp_hydrology',23,'RSPH');
insert into hydra.singularity_type(name,id,abbreviation) values ('bradley_headloss',8,'BRD');
insert into hydra.singularity_type(name,id,abbreviation) values ('marker',7,'MRKP');
insert into hydra.singularity_type(name,id,abbreviation) values ('pipe_branch_marker',9,'MRKB');
insert into hydra.singularity_type(name,id,abbreviation) values ('tz_bc',14,'ZT');
insert into hydra.singularity_type(name,id,abbreviation) values ('hydrology_bc',24,'CLH');
insert into hydra.singularity_type(name,id,abbreviation) values ('zregul_weir',5,'DE');
insert into hydra.singularity_type(name,id,abbreviation) values ('reservoir_rs_hydrology',22,'RSH');
insert into hydra.singularity_type(name,id,abbreviation) values ('froude_bc',15,'ZF');
insert into hydra.singularity_type(name,id,abbreviation) values ('zq_split_hydrology',21,'DZH');
insert into hydra.singularity_type(name,id,abbreviation) values ('qq_split_hydrology',20,'DQH');
insert into hydra.singularity_type(name,id,abbreviation) values ('tank_bc',16,'BO');
insert into hydra.singularity_type(name,abbreviation,id) values ('hydrograph_bc','HY',17);
insert into hydra.singularity_type(name,id,abbreviation) values ('bridge_headloss',25,'BRDG');
insert into hydra.singularity_type(name,id,abbreviation) values ('constant_inflow_bc',18,'HC');
insert into hydra.singularity_type(name,id,abbreviation) values ('strickler_bc',12,'RK');
insert into hydra.singularity_type(name,id,abbreviation) values ('hydraulic_cut',2,'CPR');
insert into hydra.singularity_type(name,id,abbreviation) values ('zq_bc',13,'ZQ');
insert into hydra.singularity_type(name,id,abbreviation) values ('regul_sluice_gate',6,'ACTA');
insert into hydra.singularity_type(name,id,abbreviation) values ('borda_headloss',3,'SMK');
insert into hydra.singularity_type(name,id,abbreviation) values ('weir_bc',11,'DL');
create type hydra_constrain_type as enum ('boundary', 'flood_plain_transect', 'overflow', 'connector', 'porous', 'strickler');
create table hydra.constrain_type(
    name hydra_constrain_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.constrain_type(name,id) values ('boundary',8);
insert into hydra.constrain_type(name,id) values ('flood_plain_transect',7);
insert into hydra.constrain_type(name,id) values ('overflow',1);
insert into hydra.constrain_type(name,id) values ('connector',6);
insert into hydra.constrain_type(name,id) values ('porous',2);
insert into hydra.constrain_type(name,id) values ('strickler',5);
create type hydra_borda_headloss_link_type as enum ('sharp_bend_rectangular', 't_shape_bifurcation', 'lateral_inlet', 'friction', 'lateral_outlet', 'junction', 'q', 'bifurcation', 'enlargment_with_transition', 'sharp_angle_bend', 'v', 'circular_bend', 'sharp_transition_geometry', 'diffuser', 'screen_normal_to_flow', 'transition_edge_inlet', 't_shape_junction', 'contraction_with_transition', 'parametric', 'screen_oblique_to_flow');
create table hydra.borda_headloss_link_type(
    name hydra_borda_headloss_link_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.borda_headloss_link_type(name,id,description) values ('sharp_bend_rectangular',18,'Sharp rectangular bend');
insert into hydra.borda_headloss_link_type(name,id,description) values ('t_shape_bifurcation',19,'T shape bifurcation');
insert into hydra.borda_headloss_link_type(name,id,description) values ('lateral_inlet',4,'Lateral inlet');
insert into hydra.borda_headloss_link_type(name,id,description) values ('friction',16,'Friction');
insert into hydra.borda_headloss_link_type(name,id,description) values ('lateral_outlet',3,'Lateral outlet');
insert into hydra.borda_headloss_link_type(name,id,description) values ('junction',6,'Junction');
insert into hydra.borda_headloss_link_type(name,id,description) values ('q',1,'Law dz=KQ2');
insert into hydra.borda_headloss_link_type(name,id,description) values ('bifurcation',5,'Bifurcation');
insert into hydra.borda_headloss_link_type(name,id,description) values ('enlargment_with_transition',11,'Enlargment with transition');
insert into hydra.borda_headloss_link_type(name,id,description) values ('sharp_angle_bend',13,'Sharp angle bend');
insert into hydra.borda_headloss_link_type(name,id,description) values ('v',2,'Law dE=(K/2g).(Q2/S2)');
insert into hydra.borda_headloss_link_type(name,id,description) values ('circular_bend',12,'Circular bend');
insert into hydra.borda_headloss_link_type(name,id,description) values ('sharp_transition_geometry',9,'Sharp geometry transition');
insert into hydra.borda_headloss_link_type(name,id,description) values ('diffuser',8,'Diffuser');
insert into hydra.borda_headloss_link_type(name,id,description) values ('screen_normal_to_flow',14,'Screen normal to flow');
insert into hydra.borda_headloss_link_type(name,id,description) values ('transition_edge_inlet',7,'Transition edge inlet');
insert into hydra.borda_headloss_link_type(name,id,description) values ('t_shape_junction',20,'T shape junction');
insert into hydra.borda_headloss_link_type(name,id,description) values ('contraction_with_transition',10,'Contraction with transition');
insert into hydra.borda_headloss_link_type(name,id,description) values ('parametric',17,'Parametric');
insert into hydra.borda_headloss_link_type(name,id,description) values ('screen_oblique_to_flow',15,'Screen oblique to flow');
create type hydra_transport_type as enum ('no_transport', 'quality', 'sediment', 'pollution');
create table hydra.transport_type(
    name hydra_transport_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.transport_type(name,id,description) values ('no_transport',1,'None');
insert into hydra.transport_type(name,id,description) values ('quality',3,'Quality');
insert into hydra.transport_type(name,id,description) values ('sediment',4,'Sediment transport');
insert into hydra.transport_type(name,id,description) values ('pollution',2,'Pollution generation and network transport');
create type hydra_coverage_type as enum ('street', 'right_flood_plain', 'left_flood_plain', 'reach', 'reach_end', 'storage', 'crossroad', '2d');
create table hydra.coverage_type(
    name hydra_coverage_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.coverage_type(name,id) values ('street',3);
insert into hydra.coverage_type(name,id) values ('right_flood_plain',6);
insert into hydra.coverage_type(name,id) values ('left_flood_plain',5);
insert into hydra.coverage_type(name,id) values ('reach',2);
insert into hydra.coverage_type(name,id) values ('reach_end',8);
insert into hydra.coverage_type(name,id) values ('storage',7);
insert into hydra.coverage_type(name,id) values ('crossroad',4);
insert into hydra.coverage_type(name,id) values ('2d',1);
create type hydra_pollution_quality_mode as enum ('quality2', 'quality3', 'quality1', 'pollution');
create table hydra.pollution_quality_mode(
    name hydra_pollution_quality_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.pollution_quality_mode(name,id) values ('quality2',3);
insert into hydra.pollution_quality_mode(name,id) values ('quality3',4);
insert into hydra.pollution_quality_mode(name,id) values ('quality1',2);
insert into hydra.pollution_quality_mode(name,id) values ('pollution',1);
create type hydra_action_gate_type as enum ('upward_opening', 'downward_opening');
create table hydra.action_gate_type(
    name hydra_action_gate_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.action_gate_type(name,id,description) values ('upward_opening',2,'upward opening');
insert into hydra.action_gate_type(name,id,description) values ('downward_opening',1,'downward opening');
create type hydra_gate_mode_regul as enum ('discharge', 'no_regulation', 'elevation');
create table hydra.gate_mode_regul(
    name hydra_gate_mode_regul primary key,
    id integer unique,
    description varchar
    );
insert into hydra.gate_mode_regul(name,id,description) values ('discharge',2,'Discharge');
insert into hydra.gate_mode_regul(name,id,description) values ('no_regulation',0,'No regulation');
insert into hydra.gate_mode_regul(name,id,description) values ('elevation',1,'Elevation');
create type hydra_quality_type as enum ('suspended sediment transport', 'physico_chemical', 'quality_parameter_selection');
create table hydra.quality_type(
    name hydra_quality_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.quality_type(name,id,description) values ('suspended sediment transport',3,'Suspended sediment transport');
insert into hydra.quality_type(name,id,description) values ('physico_chemical',1,'Physico chemical: 4 inter related parameters: DBO, NH4, NO3, O2');
insert into hydra.quality_type(name,id,description) values ('quality_parameter_selection',2,'Quality parameter selection: no chemical interaction betwwen parameters');
create type hydra_catchment_pollution_mode as enum ('variable_concentration', 'fixed_concentration');
create table hydra.catchment_pollution_mode(
    name hydra_catchment_pollution_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.catchment_pollution_mode(name,id) values ('variable_concentration',2);
insert into hydra.catchment_pollution_mode(name,id) values ('fixed_concentration',1);
create type hydra_abutment_type as enum ('angle_90', 'rounded', 'angle_45');
create table hydra.abutment_type(
    name hydra_abutment_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.abutment_type(name,id,description) values ('angle_90',1,'Angle 90');
insert into hydra.abutment_type(name,id,description) values ('rounded',3,'Rounded edge');
insert into hydra.abutment_type(name,id,description) values ('angle_45',2,'Angle 45');
create table hydra.metadata(
    creation_date timestamp not null default current_date,
    srid integer references spatial_ref_sys(srid),
    precision real not null default .01,
    workspace varchar(256) not null default '',
    version varchar(24) not null default '1.3',
    id integer unique not null default(1) check (id=1) -- only one row
    );
create function hydra.metadata_after_insert_fct()
        returns trigger
        language plpython3u
        as $$
            from hydra_1_3 import create_project
            import plpy
            for statement in create_project(TD['new']['srid'], '1.3'):
                plpy.execute(statement)
        $$;
create trigger hydra_metadata_after_insert_trig
        after insert on hydra.metadata
        for each row execute procedure hydra.metadata_after_insert_fct()
        ;
