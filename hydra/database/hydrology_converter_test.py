
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
run all tests

USAGE

    python -m hydra.database.hydrology_converter_test.py [-h]

    run converter test from hydrology to hydraulics


OPTIONS

    -h, --help
        print this help

"""
from __future__ import absolute_import  # important to read the doc !

import sys
import os
import getopt
import json
import string

from subprocess import Popen, PIPE
from qgis.PyQt.QtWidgets import QApplication
from qgis.PyQt.QtCore import QCoreApplication, QTranslator
from hydra.project import Project
from hydra.gui.work_manager import WorkManager
from hydra.gui.forms.street import StreetEditor
from hydra.database.database import TestProject, remove_project, project_exists
from hydra.database.hydrology_converter import HydrologyConverter
from hydra.utility.log import LogManager, ConsoleLogger
from hydra.utility.settings_properties import SettingsProperties
from hydra.utility.string import normalized_name, normalized_model_name

__currendir = os.path.dirname(__file__)
__hydradir = os.path.dirname(__currendir)

try:
    optlist, args = getopt.getopt(sys.argv[1:],
                    "hdk",
                    ["help", "debug", "keep"])
except Exception as e:
    sys.stderr.write(str(e) + "\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

keep =  "-k" in optlist or "--keep" in optlist

if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

log_manager = LogManager(ConsoleLogger(), "Hydra")

current_dir = os.path.abspath(os.path.dirname(__file__))
app = QApplication(sys.argv)
translator = QTranslator()
translator.load("i18n/fr", os.path.join(os.path.dirname(__file__), "../i18n"))
QCoreApplication.installTranslator(translator)
guitranslator = QTranslator()
guitranslator.load("/usr/share/qt4/translations/qt_fr")
QCoreApplication.installTranslator(guitranslator)

project_name = "hydrol_converter_test"
model_name = "model"

if project_exists(project_name):
    remove_project(project_name)

test_project = TestProject(project_name, keep)

project = Project.load_project(project_name)
project.add_new_model(model_name)


for filename, layer in [
        ("catchment.shp", "work.catchment_node"),
        ("connector.shp", "work.connector"),
        ("hydrology_connector.shp", "work.hydrology_connector"),
        ("hydrology_node.shp", "work.hydrology_node"),
        ("hydrology_routing.shp", "work.hydrology_routing"),
        ("manhole_node.shp", "work.manhole_node"),
        ("pipe_link.shp", "work.pipe_link")]:
    filepath = os.path.join(current_dir, 'test_data/raw_shape_h2h', filename)
    cmd = ['ogr2ogr', f'PG:service=hydra dbname={project_name}', '-lco', 'GEOMETRY_NAME=geom', '-a_srs', f'EPSG:{project.srid}', '-nln', layer, filepath] 
    print(' '.join(cmd))
    env = dict(os.environ)
    env['PGCLIENTENCODING'] = 'latin1'
    p = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, env=env)
    output, error = p.communicate()


    if error:
        raise RuntimeError(error.decode('utf8'))


test_dialog = WorkManager(project, source='manhole_node', dest='model.manhole')
test_dialog.import_from_work()
test_dialog.save()

test_dialog = WorkManager(project, source='hydrology_node', dest='model.manhole_hydrology')
test_dialog.import_from_work()
test_dialog.save()

test_dialog = WorkManager(project, source='catchment_node', dest='model.catchment_node')
test_dialog.import_from_work()
test_dialog.save()

test_dialog = WorkManager(project, source='pipe_link', dest='model.pipe')
test_dialog.import_from_work()
test_dialog.save()

test_dialog = WorkManager(project, source='connector', dest='model.connector')
test_dialog.import_from_work()
test_dialog.save()

test_dialog = WorkManager(project, source='hydrology_connector', dest='model.connector_hydrology')
test_dialog.import_from_work()
test_dialog.save()

worktabletest = 'hydrology_routing'
test_dialog = WorkManager(project, source='hydrology_routing', dest='model.routing_hydrology')
test_dialog.import_from_work()
test_dialog.save()

id_root_node = project.execute("select id from model.manhole_hydrology_node where z_ground in (54)").fetchone()

ids = project.execute('''select {model}.find_minimal_hydrology_network('{points}')'''.format(
            model=model_name, points="{"+",".join([str(pt) for pt in id_root_node])+"}")).fetchone()[0]

hydro_converter = HydrologyConverter(project, 'model')
hydro_converter.convert_to_hydraulic_network(ids)
hydro_converter.update_data_hydro()
hydro_converter.add_hydrograph_to_hydraulics_routing()

project.commit()

count_manhole = project.execute("select count(*) from model.manhole_node").fetchone()[0]
count_hydro_manhole = project.execute("select count(*) from model.manhole_hydrology_node").fetchone()[0]
count_hydrograph = project.execute("select count(*) from model.hydrograph_bc_singularity").fetchone()[0]

assert count_manhole == 10
assert count_hydro_manhole == 8
assert count_hydrograph == 3

id_root_node = project.execute("select id from model.manhole_hydrology_node where z_ground in (63)").fetchone()

ids = project.execute('''select {model}.find_minimal_hydrology_network('{points}')'''.format(
            model=model_name, points="{"+",".join([str(pt) for pt in id_root_node])+"}")).fetchone()[0]

hydro_converter = HydrologyConverter(project, 'model')
hydro_converter.convert_to_hydraulic_network(ids)
hydro_converter.update_data_hydro()
hydro_converter.add_hydrograph_to_hydraulics_routing()

project.commit()

count_manhole = project.execute("select count(*) from model.manhole_node").fetchone()[0]
count_hydro_manhole = project.execute("select count(*) from model.manhole_hydrology_node").fetchone()[0]
count_hydrograph = project.execute("select count(*) from model.hydrograph_bc_singularity").fetchone()[0]

assert count_manhole == 13
assert count_hydro_manhole == 5
assert count_hydrograph == 4

print("ok")
