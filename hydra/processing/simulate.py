from pathlib import Path
import subprocess
import os
from qgis.utils import iface

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterString,
    QgsProcessingException,
    QgsProject
)
from ..project import Project
from ..utility.licence import licence_file
from ..database.export_calcul import ExportCalcul, ExportError
from ..database.export_carto_data import ExportCartoData
from .utility import ProjectWidget, ScenarioWidget
from ..kernel import find_exe as kernel_find_exe

def find_exe(program):
    try:
        return kernel_find_exe(program)
    except RuntimeError as e:
        raise QgsProcessingException(str(e))

class Logger:
    def __init__(self, feedback):
        self.feedback = feedback

    def cleanup(self):
        pass

    def log(self, msg):
        self.feedback.pushInfo(msg)
        #self.feedback.setProgressText(msg)

    def notice(self, msg, item_name=None):
        #self.feedback.pushConsoleInfo(msg + (f" - {item_name}" if item_name else ""))
        #self.feedback.setProgressText(msg + (f" - {item_name}" if item_name else ""))
        pass

    def warning(self, msg):
        self.feedback.pushWarning(msg)

    def error(self, msg):
        self.feedback.reportError(msg)

    def classic_notice(self, msg):
        self.feedback.pushInfo(msg)

    def continuous_notice(self, msg):
        #self.feedback.pushConsoleInfo(msg)
        #self.feedback.setProgressText(msg)
        pass

    def classic_error(self, msg):
        self.feedback.reportError(msg)

class Progress:
    total = 0
    current = 0

class Simulate(QgsProcessingAlgorithm):
    """
    Algorithm running hydra
    """

    PROJECT = "project"
    SCENARIO = "scenario"
    POSTPROCESS = "postprocess"
    NB_PROCESS = "nb_process"

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return Simulate()

    def name(self):
        return "simulate"

    def displayName(self):
        return self.tr("Simulate")

    def group(self):
        return ""

    def groupId(self):
        return ""

    def shortHelpString(self):
        return self.tr("Run hydra simulation.")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        param = QgsProcessingParameterString(self.PROJECT, "Project")
        param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.SCENARIO, "Scenario")
        param.setMetadata({ 'widget_wrapper': {'class': ScenarioWidget}})
        self.addParameter(param)

    def prepareAlgorithm(self, parameters, context, feedback):
        self.success = False
        logger = Logger(feedback)
        self.project = self.parameterAsString(parameters, self.PROJECT, context)
        project = Project(self.project, logger)
        self.scenario = self.parameterAsString(parameters, self.SCENARIO, context)

        if iface and project.qgs == QgsProject.instance().fileName():
            from ..utility.qgis_utilities import QGisUIManager
            Project(self.project, logger, QGisUIManager(iface)).unload_csv_results_layers(self.scenario)

        feedback.setProgress(0)
        feedback.pushInfo('testing for invalidities')

        self.scn_id, self.calc_mode, self.flag_gfx_control,\
            self.model_connect_settings, self.tini, self.duration = project.fetchone(f"""
            select
                id,
                comput_mode,
                flag_gfx_control,
                model_connect_settings,
                t.tini,
                t.tfin - t.tini
            from project.scenario as scn
            cross join lateral (
                select case when flag_rstart then
                    (extract(epoch from trstart_hr)/3600)::float
                    else
                    -(extract(epoch from tini_hydrau_hr)/3600)::float
                    end as tini,
                    (extract(epoch from tfin_hr)/3600)::float as tfin
                    ) as t
            where scn.name ilike '{self.scenario}'
            """)

        invalids = []
        if self.model_connect_settings=="mixed":
            for group, in project.fetchall(f"""
                    select id
                    from project.mixed as mixed
                    join project.grp as grp on mixed.grp=grp.id
                    where mixed.scenario={self.scn_id}
                    order by mixed.ord"""):
                for model, in project.fetchall("""
                    select name from project.model_config as model
                    where model.id in (select model
                    from project.grp_model where grp = %s)
                    """, (group,)):
                    if project.fetchone(
                            f"select count(1) from {model}.invalid")[0]:
                        invalids.append(model)
        else:
            for model, in project.fetchall("""
                    select name
                    from project.model_config
                    order by cascade_order asc
                    """):
                if project.fetchone(
                        f"select count(1) from {model}.invalid")[0]:
                    invalids.append(model)

        if len(invalids):
            raise QgsProcessingException(self.tr('invalidities remain in ')+' '.join(invalids))
            return False


        feedback.setProgress(100)

        return True

    def processAlgorithm(self, parameters, context, feedback):
        feedback.setProgress(0)
        feedback.pushInfo('Exporting scenario')
        logger = Logger(feedback)
        project = Project(self.project, logger)

        # export topological data for calculation
        try:
            exporter = ExportCalcul(project, logger)
            exporter.export(self.scn_id)
        except ExportError as e:
            raise QgsProcessingException(str(e))

        # export cartograpic data for triangulation
        exporter = ExportCartoData(project)
        exporter.export(self.scn_id)
        feedback.pushInfo('export OK')

        if feedback.isCanceled():
            raise QgsProcessingException('cancelled')

        workdir = Path(project.directory) / self.scenario.upper() / "travail"
        hydrdir = Path(project.directory) / self.scenario.upper() / "hydraulique"

        if self.calc_mode in ["hydrology", "hydrology_and_hydraulics"]:
            self._run_external("hydrol", feedback, workdir=workdir)

        if self.calc_mode in ["hydraulics", "hydrology_and_hydraulics"]:
            self._run_external("hydragen", feedback, workdir=workdir)
            self._run_external("whydram24", feedback, workdir=hydrdir)

        feedback.setProgress(100)

        return {}

    def _run_external(self, command, feedback, workdir=None):
        feedback.setProgressText(command)
        success = False
        retry_count = 0
        while not success:
            loglines = []
            loglines.append(self.name() + " execution console output")
            try:
                with subprocess.Popen(
                    find_exe(command),
                    shell=True,
                    stdout=subprocess.PIPE,
                    stdin=subprocess.DEVNULL,
                    stderr=subprocess.STDOUT,
                    env= {**os.environ, **{'HYDRA_LICENSE': licence_file}},
                    errors='replace',
                    cwd=workdir,
                ) as proc:
                    err = False
                    for line in proc.stdout:
                        ll = line.strip()
                        if ll.startswith('hydraul time_hr:'):
                            splt = ll.split()
                            t = float(splt[2])
                            feedback.setProgress(100 * (t - self.tini)/self.duration)

                        if ll.find('attention') != -1:
                            feedback.pushWarning(ll)
                        elif feedback.isCanceled():
                            feedback.reportError("cancelled: terminate process")
                            proc.terminate()
                            proc.stdout.close()
                            proc.wait()
                            raise QgsProcessingException("cancelled")
                        else:
                            err |= ll.find('forrtl: severe') != -1
                            if not err:
                                feedback.pushConsoleInfo(ll.strip())
                            else:
                                feedback.reportError(ll)

                        loglines.append(line.strip())
                    success = True
            except IOError as e:
                if retry_count < 5:
                    retry_count += 1
                else:
                    raise IOError(
                        str(e)
                        + "\nTried 5 times without success. Last iteration stopped after reading {} line(s).\nLast line(s):\n{}".format(
                            len(loglines), "\n".join(loglines[-10:])
                        )
                    )
        with open(os.path.join(workdir, f"{command}.err")) as e, open(os.path.join(workdir, f"{command}.log")) as ll:
            if int(e.readlines()[0]) != 0:
                feedback.reportError(f"Contenu de {ll.name}:")
                for line in ll.readlines():
                    feedback.pushConsoleInfo(line.strip())
                raise QgsProcessingException("error in "+command)
                return False
            else:
                feedback.pushInfo(command + " OK")
                return True
