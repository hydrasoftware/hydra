"""
create _fr.qml files from the english language .qml file located in the plugin/ressources/qml directory,

The file plugin/i18n/fr.ts is updated if needed, the translation can be made opening this file in Qt linguist utility.

One the translations are complete, re-run translate_qml.py to udate the _fr.qml files and generate the plugin/i18n/fr.qm file
"""

from lxml import etree
import os
import subprocess
import sys

_plugin_dir = os.path.join(os.path.dirname(__file__), 'hydra')
_qml_dir = os.path.join(_plugin_dir, 'ressources', 'qml')
_ts_file = os.path.join(_plugin_dir, 'i18n', 'fr.ts')
_qml_ts_file = os.path.join(_plugin_dir, 'i18n', 'qml_fr.ts')

def get_or_create_context(ts, qml):
    "find context or create it"
    for context in ts.iter('context'):
        for name in context.iter('name'):
            if name.text == qml :
                return context

    context = etree.SubElement(ts, 'context')
    name = etree.SubElement(context, 'name')
    name.text = qml
    return context

def translate_or_create_message(context, name, filename):
    if not name:
        return ''
    for message in context.iter('message'):
        for source in message.iter('source'):
            if source.text == name:
                for translation in message.iter('translation'):
                    if 'type' in translation.attrib:
                        sys.stdout.write(f"{name} needs translation\n")
                    return translation.text or name
    message = etree.SubElement(context, 'message')
    location = etree.SubElement(message, 'location')
    location.set('filename', filename)
    location.set('line', '1')
    source = etree.SubElement(message, 'source')
    source.text = name
    translation = etree.SubElement(message, 'translation')
    translation.set('type', 'unfinished')
    translation.text = ''
    sys.stdout.write(f"new translation: {name} from {filename}\n")
    return name
# translate .qml files

if os.path.exists(_qml_ts_file):
    ts = etree.parse(_qml_ts_file).getroot()
else:
    ts = etree.Element("TS")
    ts.set('version', '2.1')
    ts.set('language', 'fr_FR')
    ts.set('sourcelanguage', 'en_US')

for qml in os.listdir(_qml_dir):
    if qml.endswith('.qml') and not qml.endswith('_fr.qml'):
        context = get_or_create_context(ts, qml)
        root = etree.parse(os.path.join(_qml_dir, qml)).getroot()
        reloc = '../ressources/qml/'+qml

        # update translation if needed
        for aliases in root.iter('aliases'):
            for alias in aliases:
                alias.set('name', translate_or_create_message(context, alias.attrib['name'], reloc))

        for referencedLayers in root.iter('referencedLayers'):
            for relation in referencedLayers.iter('relation'):
                relation.set('layerName', translate_or_create_message(context, relation.attrib['layerName'], reloc))

        for fieldConfiguration in root.iter('fieldConfiguration'):
            for field in fieldConfiguration.iter('field'):
                for editWidget in field.iter('editWidget'):
                    if editWidget.attrib['type'] == 'RelationReference':
                        for config in editWidget.iter('config'):
                            for Option in config.iter('Option'):
                                for option in Option:
                                    if option.attrib['name'] == 'ReferencedLayerName':
                                        option.set('value', translate_or_create_message(context, option.attrib['value'], reloc))

        for attributeEditorForm in root.iter('attributeEditorForm'):
            for attributeEditorContainer in attributeEditorForm.iter('attributeEditorContainer'):
                attributeEditorContainer.set('name', translate_or_create_message(context, attributeEditorContainer.attrib['name'], reloc))

        # create french qml file
        with open(os.path.join(_qml_dir, qml[:-4]+'_fr.qml'), 'wb') as o:
            o.write(b"<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>\n")
            o.write(etree.tostring(root, pretty_print=True, encoding='utf-8'))

with open(_qml_ts_file, 'w') as o:
    o.write('<?xml version="1.0" encoding="utf-8"?>\n')
    o.write('<!DOCTYPE TS>\n')
    #etree.indent(ts, ' '*4)
    o.write(etree.tostring(ts, pretty_print=True, encoding='utf-8').decode('utf-8'))

if len(sys.argv)>1 and sys.argv[1] == 'qml':
    exit(0)

# translate .py and .ui files
sources = []
for root, dirs, files in os.walk(_plugin_dir):
    for file in files:
        if file.endswith('.py') or file.endswith('.ui'):
            sources.append(os.path.join(root, file))

subprocess.run(['pylupdate5']+sources+['-ts', _ts_file])
