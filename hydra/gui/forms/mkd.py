# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..base_dialog import BaseDialog, tr
import hydra.utility.string as string
from qgis.PyQt import uic
import os

class Marker2DDomainEditor(BaseDialog):
    def __init__(self, project, geom, id=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "mkd.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.__model = self.project.get_current_model()
        assert not self.__model == None
        self.__geom = geom
        if id==None:
            self.__id = self.__model.add_2d_domain_marker(self.__geom)
        else:
            self.__id = id
            if self.__geom is None:
                geom,= self.project.execute("""
                select geom
                from {}.domain_2d_marker
                where id={}""".format(self.__model.name, self.__id)).fetchone()

        name, domain_2d, comment = self.project.execute("""
            select name, domain_2d, comment
            from {}.domain_2d_marker
            where id={}""".format(self.__model.name, self.__id)).fetchone()

        contour = self.project.execute("""
            with g as (select geom from {m}.domain_2d_marker where id={i})
            select id from {m}.coverage as c, g where st_intersects(c.geom, g.geom)""".format(m=self.__model.name, i=self.__id)).fetchone()

        self.name.setText(name)

        domains = self.project.execute("""
            select id, name from {}.domain_2d""".format(self.__model.name)).fetchall()
        self.combo_domain.addItems([name for id,name in domains])
        if domain_2d is not None :
            self.combo_domain.setCurrentIndex(self.combo_domain.findText([name for id,name in domains if id==domain_2d][0]))
        else :
            self.combo_domain.setCurrentIndex(-1)

        self.comment.setText(comment)

        if contour is not None:
            self.contour.setText(str(contour[0]))
        else:
            self.contour.setText('None')


    def get_id(self):
        return self.__id

    def save(self):
        self.project.log.notice(tr("Saved"))

        domain_id, = self.project.execute("""
            select id from {}.domain_2d where name='{}'""".format(self.__model.name, self.combo_domain.currentText())).fetchone()

        self.__model.update_2d_domain_marker(self.__id, self.__geom, self.name.text(), domain_id, self.comment.toPlainText())
        BaseDialog.save(self)
        self.close()
