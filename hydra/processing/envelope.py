import itertools
import shutil
from collections import namedtuple
from pathlib import Path
from multiprocessing import cpu_count

from qgis.PyQt.QtCore import QCoreApplication
from qgis.utils import plugins as qgis_plugins
from qgis import processing
from qgis.core import (
    QgsProcessing,
    QgsProcessingException,
    QgsProcessingAlgorithm,
    QgsProcessingParameterRasterDestination,
    QgsProcessingParameterRasterLayer,
    QgsProcessingParameterNumber,
    QgsProcessingParameterString,
    QgsProcessingMultiStepFeedback,
    QgsRasterLayer,
    QgsCoordinateReferenceSystem,
    QgsProject,
)
from .crgeng import run_crgeng
from .utility import ProjectWidget, ScenarioGroupWidget


QML_DIR = Path(__file__).parent.parent / "ressources" / "qml"

ENVELOP_CTL_CONTENT = """*WRKSPACE
{workspace}

*GRID_VRT
{terrain}

*MODE ENVELOP
1
{project_name} {model}
{scenarios}
{output} {time_step}
5
0.0101
0.2501
0.5001
1.0001
2.0001

*OPTION_RES
LAMB93
VIT_ASC

*OPTION_ZF2

"""

ENVELOPZ_CTL_CONTENT = """*WRKSPACE
{workspace}

*GRID_VRT
{terrain}

*MODE ENVELOPZ
1
{project_name} {model}
{scenarios}
{output} {time_step}
1 0 1

*OPTION_RES
LAMB93
NVZ_ASC

*OPTION_ZF2

"""

SQL_GROUP_SCENARIOS = """
select s.name, c.name as mode,
case
    when c.name = 'mixed' then array_agg(distinct mixed.name)
    else array_agg(distinct models.name) end as models
from project.scenario_group g
join project.param_scenario_group psc on g.id = psc.grp
join project.scenario s on s.id = psc.scenario
left join hydra.model_connection_settings c on c.name = s.model_connect_settings
left join lateral (
    select mc.name
    from project.mixed m
    join project.grp_model gm on gm.grp = m.grp
    join project.model_config mc on mc.id = gm.model
    where m.scenario = s.id
) mixed on true
left join lateral (select name::varchar from project.model) as models on true
where g.name = %s
group by s.name, c.name;
"""


class EnvelopeProcessing(QgsProcessingAlgorithm):
    """
    Algorithm creating an Envelope merging max of a list of scenarios
    """

    INPUT = "TERRAIN"
    GROUP = "GROUPS"
    TIME_STEP = "TIME_STEP"
    PROJECT = "PROJECT"

    def createInstance(self):
        return self.__class__()

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def group(self):
        return self.tr("River and free surface flow mapping")

    def groupId(self):
        return "surfaceflowmapping"

    def initAlgorithm(self, config=None):

        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT,
                self.tr("Terrain Layer"),
                [QgsProcessing.TypeRaster],
            )
        )

        param = QgsProcessingParameterString(self.PROJECT, "Project")
        param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.GROUP, " Group Name")
        param.setMetadata({ 'widget_wrapper': {'class': ScenarioGroupWidget}})
        self.addParameter(param)

        self.addParameter(
            QgsProcessingParameterNumber(
                self.TIME_STEP,
                "Time step",
                QgsProcessingParameterNumber.Double,
                defaultValue=-999.0,
            )
        )


class VelocityEnvelopeRaster(EnvelopeProcessing):
    """max envelope of velocity"""

    OUTPUT = "velocity"

    def name(self):
        return "velocity_raster_envelope"

    def displayName(self):
        return self.tr("crgeng Velocity raster envelope")

    def shortHelpString(self):
        return self.tr("Maximum velocity of a group of scenarios")

    def initAlgorithm(self, config=None):
        super().initAlgorithm()
        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.OUTPUT, "Velocity raster envelope"
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        self.project = Projet(self.parameterAsString(parameters, self.PROJECT, context))
        self.srid = self.project.srid
        self.project_name = self.project.name
        self.workspace_dir = Path(self.project.directory).parent
        self.carto_dir = self.project.carto_dir
        group = self.parameterAsString(parameters, self.GROUP, context)

        self.output = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        input = self.parameterAsRasterLayer(parameters, self.INPUT, context)
        time_step = self.parameterAsDouble(parameters, self.TIME_STEP, context)

        Scenario = namedtuple("scenarios", "name mode models")
        scenarios = [
            Scenario(row[0], row[1], row[2])
            for row in self.project.execute(SQL_GROUP_SCENARIOS, [group]).fetchall()
        ]

        nmod = len(scenarios[0].models)
        if not all([len(s.models) == nmod for s in scenarios]):
            raise QgsProcessingException(
                "Inconsistency in scenario/model association: {scenarios}"
            )

        velocity_folder = Path(self.carto_dir) / "velocity"
        velocity_folder.mkdir(parents=True, exist_ok=True)

        ctl_file_path = velocity_folder / "envelope_vit_asc.ctl"
        feedback.pushInfo(f"Writing control file to {ctl_file_path}")

        # get a unique list of models
        models = set(itertools.chain.from_iterable(s.models for s in scenarios))

        # two steps per model
        multifb = QgsProcessingMultiStepFeedback(2 * len(models), feedback)

        feedback.pushInfo(f"Scenarios: {', '.join([s.name for s in scenarios])}")
        feedback.pushInfo(f"Models: {', '.join([m for m in models])}")

        # use to store temp files to be removed after processing
        self.tmpfiles = []
        # velocity_merged = tif output files for each model
        velocity_merged = []

        for idx, model in enumerate(models, start=1):
            multifb.setCurrentStep(idx)
            with ctl_file_path.open("wt", encoding="utf-8") as ctl_file:
                ctl_file.write(
                    ENVELOP_CTL_CONTENT.format(
                        workspace=self.workspace_dir,
                        terrain=str(Path(input.source())),
                        project_name=self.project_name,
                        model=model.upper(),
                        scenarios=" ".join(s.name.upper() for s in scenarios),
                        time_step=time_step,
                        output=f"{group}_envelope",
                        # output: carto/GROUP_1_modele1_envelope_MODELE1_-999.0_vit002.asc
                    )
                )

            run_crgeng([str(ctl_file_path)], multifb, velocity_folder)

            asc_files = list(
                Path(self.carto_dir).glob(
                    f"{group}_envelope_{model.upper()}_{time_step}_vit*.asc"
                )
            )
            self.tmpfiles.extend(asc_files)

            feedback.pushInfo("merging source files")
            velocity_merged.append(
                Path(self.carto_dir)
                / f"velocity_{group}_envelope_{model.upper()}_{time_step}.tif"
            )

            multifb.setCurrentStep(idx + 1)
            processing.run(
                "gdal:merge",
                {
                    "INPUT": [str(p) for p in asc_files],
                    "OUTPUT": str(velocity_merged[-1]),
                    "NODATA_INPUT": -999,  # don't treat this bad nodata value
                    "NODATA_OUTPUT": -9999,
                    "OPTIONS": f"PREDICTOR=3|COMPRESS=DEFLATE|NUM_THREADS={cpu_count()}",
                },
                feedback=multifb,
            )

        processing.run(
            "gdal:buildvirtualraster",
            {
                "INPUT": [str(v) for v in velocity_merged],
                "OUTPUT": self.output,
                "SEPARATE": False,
            },
        )

        # prepare style for output layer
        shutil.copy(
            QML_DIR / "raster_vitesses.qml", Path(self.output).with_suffix(".qml")
        )

        return {self.OUTPUT: self.output}

    def postProcessAlgorithm(self, context, feedback):

        layer = QgsRasterLayer(self.output, Path(self.output).name)
        layer.setCrs(QgsCoordinateReferenceSystem.fromEpsgId(self.srid))
        QgsProject.instance().addMapLayer(layer)

        # cleanup temp files
        for raster in self.tmpfiles:
            try:
                raster.unlink()
            except PermissionError as err:
                feedback.pushWarning(f"source raster not deleted, reason: {err}")

        # cleanup gdal metadata if exists
        for xml_metadata in Path(self.carto_dir).glob("*vit*.asc.aux.xml"):
            xml_metadata.unlink()

        return {self.OUTPUT: self.output}


class DepthEnvelopeRaster(EnvelopeProcessing):
    """max envelope of depth"""

    OUTPUT = "Depth"

    def name(self):
        return "depth_raster_envelope"

    def displayName(self):
        return self.tr("crgeng Depth raster envelope")

    def shortHelpString(self):
        return self.tr("Maximum Depth of a group of scenarios")

    def initAlgorithm(self, config=None):
        super().initAlgorithm()
        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.OUTPUT, "Depth raster envelope"
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """

        self.output = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        input = self.parameterAsRasterLayer(parameters, self.INPUT, context)
        time_step = self.parameterAsDouble(parameters, self.TIME_STEP, context)
        group = self.groups[self.parameterAsEnum(parameters, self.GROUP, context)]

        Scenario = namedtuple("scenarios", "name mode models")
        scenarios = [
            Scenario(row[0], row[1], row[2])
            for row in self.project.execute(SQL_GROUP_SCENARIOS, [group]).fetchall()
        ]

        nmod = len(scenarios[0].models)
        if not all([len(s.models) == nmod for s in scenarios]):
            raise QgsProcessingException(
                "Inconsistency in scenario/model association: {scenarios}"
            )

        depth_folder = Path(self.carto_dir) / "depth"
        depth_folder.mkdir(parents=True, exist_ok=True)

        ctl_file_path = depth_folder / "envelope_depth_asc.ctl"
        feedback.pushInfo(f"Writing control file to {ctl_file_path}")

        # get a unique list of models
        models = set(itertools.chain.from_iterable(s.models for s in scenarios))

        # four steps per model
        multifb = QgsProcessingMultiStepFeedback(4 * len(models), feedback)

        feedback.pushInfo(f"Scenarios: {', '.join([s.name for s in scenarios])}")
        feedback.pushInfo(f"Models: {', '.join([m for m in models])}")

        # use to store temp files to be removed after processing
        self.tmpfiles = []
        # depth_merged = tif output files for each model
        depth_merged = []

        for idx, model in enumerate(models, start=1):
            multifb.setCurrentStep(idx)
            with ctl_file_path.open("wt", encoding="utf-8") as ctl_file:
                ctl_file.write(
                    ENVELOPZ_CTL_CONTENT.format(
                        workspace=self.workspace_dir,
                        terrain=str(Path(input.source())),
                        project_name=self.project_name,
                        model=model.upper(),
                        scenarios=" ".join(s.name.upper() for s in scenarios),
                        time_step=time_step,
                        output=f"{group}_envelope",
                        # output: carto/GROUP_1_modele1_envelope_MODELE1_-999.0_nvz*.asc
                    )
                )
            # compute depth values
            run_crgeng([str(ctl_file_path)], multifb, depth_folder)

            asc_files = list(
                Path(self.carto_dir).glob(
                    f"{group}_envelope_{model.upper()}_{time_step}_nvz*.asc"
                )
            )
            self.tmpfiles.extend(asc_files)

            feedback.pushInfo(f"merging source files {asc_files} to compressed geotiff")
            current_merged = (
                Path(self.carto_dir)
                / f"depth_{group}_envelope_{model.upper()}_{time_step}.tif"
            )
            depth_merged.append(current_merged)

            multifb.setCurrentStep(idx + 1)
            processing.run(
                "gdal:merge",
                {
                    "INPUT": [str(p) for p in asc_files],
                    "OUTPUT": str(current_merged),
                    "NODATA_INPUT": -999,  # don't treat this bad nodata value
                    "NODATA_OUTPUT": -9999,
                    "OPTIONS": f"PREDICTOR=3|COMPRESS=DEFLATE|NUM_THREADS={cpu_count()}",
                },
                feedback=multifb,
            )

            merged_layer = QgsRasterLayer(str(current_merged), current_merged.name)

            feedback.pushInfo(f"clipping DEM to: {merged_layer.extent()}")

            multifb.setCurrentStep(idx + 2)
            mnt_clipped = str(depth_folder / "mnt_clipped.tif")

            processing.run(
                "gdal:cliprasterbyextent",
                {
                    "INPUT": input,
                    "PROJWIN": merged_layer.extent(),
                    "OUTPUT": mnt_clipped,
                    "NODATA": -9999,
                    "EXTRA": f"-projwin_srs EPSG:{self.srid}",
                },
                feedback=multifb,
            )

            feedback.pushInfo("Computing water depth using absolute value and DEM")

            multifb.setCurrentStep(idx + 3)
            processing.run(
                "gdal:rastercalculator",
                {
                    "INPUT_A": str(current_merged),
                    "INPUT_B": mnt_clipped,
                    "BAND_A": 1,
                    "BAND_B": 1,
                    "NO_DATA": 0,
                    # https://docs.qgis.org/3.22/en/docs/user_manual/processing_algs/gdal/rastermiscellaneous.html?highlight=buildvirtualraster#raster-calculator
                    "RTYPE": 5,  # float 32
                    "ASSIGN_CRS": f"EPSG:{self.srid}",
                    "OUTPUT": str(current_merged.with_suffix(".norm.tif")),
                    "FORMULA": "B*less(A,0)+A*greater(A,0)-B",
                    "OPTIONS": f"PREDICTOR=3|COMPRESS=DEFLATE|NUM_THREADS={cpu_count()}",
                },
                feedback=multifb,
            )

        # assembling all depth raster to a VRT
        processing.run(
            "gdal:buildvirtualraster",
            {
                "INPUT": [str(v.with_suffix(".norm.tif")) for v in depth_merged],
                "OUTPUT": self.output,
                "SEPARATE": False,
            },
        )

        self.tmpfiles.extend(depth_merged)

        # prepare style for output layer
        shutil.copy(
            QML_DIR / "raster_hauteurs.qml", Path(self.output).with_suffix(".qml")
        )

        return {self.OUTPUT: self.output}

    def postProcessAlgorithm(self, context, feedback):

        layer = QgsRasterLayer(self.output, Path(self.output).name)
        layer.setCrs(QgsCoordinateReferenceSystem.fromEpsgId(self.srid))
        QgsProject.instance().addMapLayer(layer)

        # cleanup temp files
        for raster in self.tmpfiles:
            try:
                raster.unlink()
            except PermissionError as err:
                feedback.pushWarning(f"source raster not deleted, reason: {err}")

        # cleanup gdal metadata if exists
        for xml_metadata in Path(self.carto_dir).glob("*nvz*.asc.aux.xml"):
            xml_metadata.unlink()

        return {self.OUTPUT: self.output}
