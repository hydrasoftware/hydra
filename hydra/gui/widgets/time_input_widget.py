################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import datetime
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QWidget, QGridLayout
from qgis.PyQt.QtCore import QMargins

class TimeInputWidget(QWidget):
    def __init__(self, parent=None, no_zero=False):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "time_input_widget.ui"), self)
        if parent:
            tmplayout = QGridLayout()
            tmplayout.setContentsMargins(QMargins(0,0,0,0))
            tmplayout.addWidget(self)
            parent.setLayout(tmplayout)

        self.no_zero = no_zero
        self.hours.valueChanged.connect(self.change_time)
        self.minutes.valueChanged.connect(self.change_time)
        self.seconds.valueChanged.connect(self.change_time)

    def change_time(self):
        if self.no_zero and self.get_time() == datetime.timedelta(seconds=0):
            spinbox = self.sender()
            spinbox.setValue(1)

    def set_time(self, time):
        tot_secs = time.total_seconds()

        self.hours.setValue(int(tot_secs // 3600))
        self.minutes.setValue(int((tot_secs % 3600) // 60))
        self.seconds.setValue(int(tot_secs % 60))

    def get_time(self):
        total_secs = 3600*self.hours.value() + 60*self.minutes.value() + self.seconds.value()
        return datetime.timedelta(seconds=total_secs)

    def set_format(self, hr=99999, min=59, sec=59):
        if hr > 0:
            self.hours.setMaximum(hr)
        else:
            self.hours.setValue(0)
            self.hours.hide()
            self.label_hours.hide()
        if min > 0:
            self.minutes.setMaximum(min)
        else:
            self.minutes.setValue(0)
            self.minutes.hide()
            self.label_minutes.hide()
            self.label_hours.setText('h')
        if sec > 0:
            self.seconds.setMaximum(sec)
        else:
            self.seconds.setValue(0)
            self.seconds.hide()
            self.label_seconds.hide()
            self.label_minutes.setText('m')
