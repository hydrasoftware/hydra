# -*- coding: utf-8 -*-

"""
***************************************************************************
    peukerdouglasstreamdef.py
    ---------------------
    Date                 : April 2019
    Copyright            : (C) 2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'April 2019'
__copyright__ = '(C) 2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.core import (QgsProcessing,
                       QgsProcessingException,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterBoolean,
                       QgsProcessingParameterEnum,
                       QgsProcessingParameterRasterDestination,
                       QgsProcessingParameterFileDestination,
                      )

from .taudemAlgorithm import TauDemAlgorithm
from . import taudemUtils


class PeukerDouglasStreamDef(TauDemAlgorithm):

    D8_FLOWDIR = 'D8_FLOWDIR'
    PIT_FILLED = 'PIT_FILLED'
    OUTLETS = 'OUTLETS'
    MASK_GRID = 'MASK_GRID'
    D8_CONTRIB_AREA = 'D8_CONTRIB_AREA'

    CENTER_WEIGHT = 'CENTER_WEIGHT'
    SIDE_WEIGHT = 'SIDE_WEIGHT'
    DIAGONAL_WEIGHT = 'DIAGONAL_WEIGHT'
    THRESHOLD = 'THRESHOLD'
    MIN_TRESHOLD = 'MIN_TRESHOLD'
    MAX_THRESHOLD = 'MAX_THRESHOLD'
    DROP_TRESHOLDS = 'DROP_TRESHOLDS'
    STEP = 'STEP'
    EDGE_CONTAMINATION = 'EDGE_CONTAMINATION'
    THRESHOLD_BY_DROP = 'THRESHOLD_BY_DROP'

    STREAM_SOURCE = 'STREAM_SOURCE'
    ACCUM_STREAM_SOURCE = 'ACCUM_STREAM_SOURCE'
    STREAM_RASTER = 'STREAM_RASTER'
    DROP_ANALYSIS = 'DROP_ANALYSIS'

    def name(self):
        return 'peukerdouglasstreamdef'

    def displayName(self):
        return self.tr('Peuker Douglas stream definition')

    def group(self):
        return self.tr('Stream network analysis')

    def groupId(self):
        return 'streamanalysis'

    def tags(self):
        return self.tr('dem,hydrology,stream,indicator,d8,threshold,drop').split(',')

    def shortHelpString(self):
        return self.tr('Generates a stream indicator grid (1,0) where '
                       'the streams are located using a DEM curvature-based method.')

    def helpUrl(self):
        return 'http://hydrology.usu.edu/taudem/taudem5/help53/PeukerDouglasStreamDefinition.html'

    def __init__(self):
        super().__init__()

    def initAlgorithm(self, config=None):
        self.steps = [self.tr('Logarithmic'),
                      self.tr('Arithmetic')
                     ]

        self.addParameter(QgsProcessingParameterRasterLayer(self.PIT_FILLED,
                                                            self.tr('Pit filled elevation')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.D8_FLOWDIR,
                                                            self.tr('D8 flow directions')))
        self.addParameter(QgsProcessingParameterFeatureSource(self.OUTLETS,
                                                              self.tr('Outlets'),
                                                              types=[QgsProcessing.TypeVectorPoint],
                                                              defaultValue=None,
                                                              optional=True))
        self.addParameter(QgsProcessingParameterRasterLayer(self.MASK_GRID,
                                                            self.tr('Mask grid'),
                                                            defaultValue=None,
                                                            optional=True))
        self.addParameter(QgsProcessingParameterRasterLayer(self.D8_CONTRIB_AREA,
                                                            self.tr('D8 contributing area for drop analysis'),
                                                            defaultValue=None,
                                                            optional=True))
        self.addParameter(QgsProcessingParameterNumber(self.CENTER_WEIGHT,
                                                       self.tr('Center smoothing weight'),
                                                       QgsProcessingParameterNumber.Double,
                                                       minValue=0,
                                                       defaultValue=0.4))
        self.addParameter(QgsProcessingParameterNumber(self.SIDE_WEIGHT,
                                                       self.tr('Side smoothing weight'),
                                                       QgsProcessingParameterNumber.Double,
                                                       minValue=0,
                                                       defaultValue=0.1))
        self.addParameter(QgsProcessingParameterNumber(self.DIAGONAL_WEIGHT,
                                                       self.tr('Diagonal smoothing weight'),
                                                       QgsProcessingParameterNumber.Double,
                                                       minValue=0,
                                                       defaultValue=0.05))
        self.addParameter(QgsProcessingParameterNumber(self.THRESHOLD,
                                                       self.tr('Accumulation threshold'),
                                                       QgsProcessingParameterNumber.Double,
                                                       defaultValue=100.0))
        self.addParameter(QgsProcessingParameterNumber(self.MIN_TRESHOLD,
                                                       self.tr('Minimum threshold'),
                                                       QgsProcessingParameterNumber.Double,
                                                       defaultValue=5))
        self.addParameter(QgsProcessingParameterNumber(self.MAX_THRESHOLD,
                                                       self.tr('Maximum threshold'),
                                                       QgsProcessingParameterNumber.Double,
                                                       defaultValue=500))
        self.addParameter(QgsProcessingParameterNumber(self.DROP_TRESHOLDS,
                                                       self.tr('Number of drop thresholds'),
                                                       QgsProcessingParameterNumber.Double,
                                                       defaultValue=10))
        self.addParameter(QgsProcessingParameterEnum(self.STEP,
                                                     self.tr('Type of threshold step'),
                                                     options=self.steps,
                                                     defaultValue=0))
        self.addParameter(QgsProcessingParameterBoolean(self.EDGE_CONTAMINATION,
                                                        self.tr('Check for edge contamination'),
                                                        defaultValue=True))
        self.addParameter(QgsProcessingParameterBoolean(self.THRESHOLD_BY_DROP,
                                                        self.tr('Select threshold by drop analysis'),
                                                        defaultValue=False))

        self.addParameter(QgsProcessingParameterRasterDestination(self.STREAM_SOURCE,
                                                                  self.tr('Stream source')))
        self.addParameter(QgsProcessingParameterRasterDestination(self.ACCUM_STREAM_SOURCE,
                                                                  self.tr('Accumulated stream source')))
        self.addParameter(QgsProcessingParameterRasterDestination(self.STREAM_RASTER,
                                                                  self.tr('Stream raster')))
        self.addParameter(QgsProcessingParameterFileDestination(self.DROP_ANALYSIS,
                                                                self.tr('Drop analysis'),
                                                                self.tr('Text files (*.txt)'),
                                                                optional=True,
                                                                createByDefault=False))


    def processAlgorithm(self, parameters, context, alg_feedback):
        demLayer = self.parameterAsRasterLayer(parameters, self.PIT_FILLED, context)
        if demLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.PIT_FILLED))

        d8Layer = self.parameterAsRasterLayer(parameters, self.D8_FLOWDIR, context)
        if d8Layer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.D8_FLOWDIR))

        hasDropAnalysis = self.parameterAsBool(parameters, self.THRESHOLD_BY_DROP, context)
        if hasDropAnalysis:
            if self.DROP_ANALYSIS not in parameters:
                raise QgsProcessingException(self.tr('Specify drop analysis output file or uncheck "Select threshold by drop analysis".'))

        steps = 4 if hasDropAnalysis else 3
        feedback = QgsProcessingMultiStepFeedback(steps, alg_feedback)

        outputs = {}
        results = {}

        # PeukerDouglas
        params = {'PIT_FILLED': parameters[self.PIT_FILLED],
                  'CENTER_WEIGHT': parameters[self.CENTER_WEIGHT],
                  'SIDE_WEIGHT': parameters[self.SIDE_WEIGHT],
                  'DIAGONAL_WEIGHT': parameters[self.DIAGONAL_WEIGHT],
                  'STREAM_SOURCE': parameters[self.STREAM_SOURCE]
                 }
        outputs['PEUKER_DOUGLAS'] = processing.run('taudem:peukerdouglas', params, context=context, feedback=feedback, is_child_algorithm=True)
        results[self.STREAM_SOURCE] = outputs['PEUKER_DOUGLAS']['STREAM_SOURCE']

        feedback.setCurrentStep(1)
        if feedback.isCanceled():
            return {}

        # AreaD8
        params = {'D8_FLOWDIR': parameters[self.D8_FLOWDIR],
                  'WEIGHT_GRID': outputs['PEUKER_DOUGLAS']['STREAM_SOURCE'],
                  'OUTLETS': parameters[self.OUTLETS],
                  'EDGE_CONTAMINATION': parameters[self.EDGE_CONTAMINATION],
                  'D8_CONTRIB_AREA': parameters[self.ACCUM_STREAM_SOURCE],
                 }
        outputs['AREA_D8'] = processing.run('taudem:aread8', params, context=context, feedback=feedback, is_child_algorithm=True)
        results[self.ACCUM_STREAM_SOURCE] = outputs['AREA_D8']['D8_CONTRIB_AREA']

        feedback.setCurrentStep(2)
        if feedback.isCanceled():
            return {}

        threshold = parameters[self.THRESHOLD]

        # optional DropAnalysis
        if hasDropAnalysis:
            params = {'PIT_FILLED': parameters[self.PIT_FILLED],
                      'D8_FLOWDIR': parameters[self.D8_FLOWDIR],
                      'D8_CONTRIB_AREA': parameters[self.D8_CONTRIB_AREA],
                      'ACCUM_STREAM_SOURCE': outputs['AREA_D8']['D8_CONTRIB_AREA'],
                      'OUTLETS': parameters[self.OUTLETS],
                      'MIN_THRESHOLD': parameters[self.MIN_THRESHOLD],
                      'MAX_THRESHOLD': parameters[self.MAX_THRESHOLD],
                      'DROP_TRESHOLDS': parameters[self.DROP_TRESHOLDS],
                      'STEP': parameters[self.STEP],
                      'DROP_ANALYSIS': parameters[self.DROP_ANALYSIS],
                     }
            outputs['DROP_ANALYSIS'] = processing.run('taudem:dropanalysis', params, context=context, feedback=feedback, is_child_algorithm=True)
            dropFile = self.self.parameterAsFileOutput(parameters, self.DROP_ANALYSIS, context)
            if dropFile is not None:
                results[self.DROP_ANALYSIS] = outputs['DROP_ANALYSIS']['DROP_ANALYSIS']

            # read threshold from the drop analysis text file
            with open(dropFile, 'r') as f:
                for line in f:
                    pass
                threshold = float(line.split(':')[1].strip())

            feedback.setCurrentStep(3)
            if feedback.isCanceled():
                return {}

        # Threshold
        params = {'ACCUM_STREAM_SOURCE': outputs['AREA_D8']['D8_CONTRIB_AREA'],
                  'MASK_GRID': parameters[self.MASK_GRID],
                  'THRESHOLD': threshold,
                  'STREAM_RASTER': parameters[self.STREAM_RASTER],
                 }
        outputs['THRESHOLD'] = processing.run('taudem:threshold', params, context=context, feedback=feedback, is_child_algorithm=True)
        results[self.STREAM_RASTER] = outputs['THRESHOLD']['STREAM_RASTER']

        feedback.setCurrentStep(steps)
        if feedback.isCanceled():
            return {}

        return results
