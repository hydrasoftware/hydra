/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update hydra.metadata set version = '1.4';
;;

create or replace function project.model_instead_fct()
returns trigger
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_model
    for statement in create_model(TD['new']['name'], $srid, '$version'):
        plpy.execute(statement)
$$$$
;;

/* ********************************************** */
/*  scenario ref                                  */
/* ********************************************** */

alter table project.scenario add column if not exists scenario_hydrol integer references project.scenario(id);
;;

/* ********************************************** */
/*  Scenario transport and quality                */
/* ********************************************** */

do
$$$$
    begin
        if exists (select 1 FROM information_schema.columns WHERE table_schema='project' and table_name='scenario' and column_name='iflag_ad4') then
            alter table project.scenario rename column iflag_ad1 to iflag_ecoli;
            alter table project.scenario rename column iflag_ad2 to iflag_enti;
            alter table project.scenario rename column iflag_ad3 to iflag_ad1;
            alter table project.scenario rename column iflag_ad4 to iflag_ad2;
        end if;
    end
$$$$;
;;

do
$$$$
    begin
        if exists (select 1 FROM information_schema.columns WHERE table_schema='project' and table_name='scenario' and column_name='ad4_decay_rate_jminus_one') then
            alter table project.scenario rename column ad1_decay_rate_jminus_one to ecoli_decay_rate_jminus_one;
            alter table project.scenario rename column ad2_decay_rate_jminus_one to enti_decay_rate_jminus_one;
            alter table project.scenario rename column ad3_decay_rate_jminus_one to ad1_decay_rate_jminus_one;
            alter table project.scenario rename column ad4_decay_rate_jminus_one to ad2_decay_rate_jminus_one;
        end if;
    end
$$$$;
;;

alter table project.scenario add column if not exists suspended_sediment_fall_velocity_mhr real not null default 0;
;;

--isolated--
alter type hydra_quality_type add value if not exists 'bacteriological_quality';
;;

--isolated--
alter type hydra_quality_type add value if not exists 'suspended_sediment_transport';
;;

update hydra.transport_type set description='Pollution: Pollution generation and network transport' where id=2;
update hydra.transport_type set description='Hydromorphology: Bedload sediment transport' where id=4;
;;

update hydra.quality_type set description='Quality 1: Physico chemical' where id=1;
update hydra.quality_type set description='Quality 2: Bacteriological quality', name='bacteriological_quality' where id=2;
update hydra.quality_type set description='Quality 3: Suspended sediment transport', name='suspended_sediment_transport' where id=3;
;;

do
$$$$
    begin
        if not exists (select 1 from pg_type where typname = 'hydra_network_type') then
            create type public.hydra_network_type as enum ('separative', 'combined');
        end if;
    end
$$$$;
;;

create table if not exists hydra.network_type(
    name hydra_network_type primary key,
    id integer unique,
    description varchar
)
;;

insert into hydra.network_type(name, id, description) values ('separative', 1, 'Separative'), ('combined', 2, 'Combined') on conflict do nothing;
;;

drop table if exists project.hydrograph_variable_dry_flow;
drop table if exists project.hydrograph_pollution;
drop table if exists project.hydrograph_quality;
;;

/* ********************************************** */
/*              Terrain                           */
/* ********************************************** */

create or replace function project.check_source(source varchar)
returns bool
language plpython3u immutable
as
$$$$
    import gdal
    src_ds = gdal.Open(source)
    if src_ds is None:
        return False
    else:
        return True
$$$$
;;

create or replace function project.unpack_project_directory()
returns varchar
language plpgsql immutable
as
$$$$
    declare
        _workspace varchar;
    begin
        select workspace||'\\'||current_database() from hydra.metadata into _workspace;
        return _workspace;
    end;
$$$$
;;

create or replace function project.altitude(x real, y real)
returns real
language plpython3u immutable
as
$$$$
    from $hydra import altitude
    import plpy
    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select replace(source, '$workspace', project.unpack_project_directory()) as source from project.dem order by priority asc")]
    return altitude(sources, x, y)
$$$$
;;

create or replace function project.set_altitude(geom geometry)
returns geometry
language plpython3u immutable
as
$$$$
    from $hydra import set_altitude
    import plpy
    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select replace(source, '$workspace', project.unpack_project_directory()) as source from project.dem order by priority asc")]
    return set_altitude(sources, geom, $srid)
$$$$
;;

create or replace function project._str_filling_curve(polygon geometry)
returns varchar
language plpython3u immutable
as
$$$$
    from $hydra import filling_curve
    import plpy
    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select replace(source, '$workspace', project.unpack_project_directory()) as source from project.dem order by priority asc")]
    return filling_curve(sources, polygon)
$$$$
;;

create or replace function project.crest_line(line geometry)
returns geometry
language plpython3u immutable
as
$$$$
    from $hydra import crest_line
    import plpy
    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select replace(source, '$workspace', project.unpack_project_directory()) as source from project.dem order by priority asc")]
    return crest_line(sources, line, $srid)
$$$$
;;

/* ********************************************** */
/*              Land use                          */
/* ********************************************** */

drop table if exists project.pollution_land_accumulation;
drop table if exists project.land_use;
;;

create table if not exists project.pollution_land_accumulation(
    id integer not null default 1 unique check (id=1),
    sewage_storage_kgha real[4][4] not null
        check(array_length(sewage_storage_kgha, 1)=4 and array_length(sewage_storage_kgha, 2)=4)
        default '{{0,0,0,0},{175,17.5,87.5,3.25},{50,5,25,1},{112.5,11,56,2.2}}'::real[],
    unitary_storage_kgha real[4][4] not null
        check(array_length(unitary_storage_kgha, 1)=4 and array_length(unitary_storage_kgha, 2)=4)
        default '{{0,0,0,0},{280,35,210,6.2},{80,10,50,2},{180,22,135,4.5}}'::real[],
    regeneration_time_days real not null default 5,
    a_extraction_coef real not null default 0.06,
    b_extraction_coef real not null default 1.5
)
;;

insert into project.pollution_land_accumulation default values on conflict do nothing;
;;

/* ********************************************** */
/*              Results                           */
/* ********************************************** */

drop function if exists project.create_result(scenario, model);
;;

/* ********************************************** */
/*              Manhole new Data                  */
/* ********************************************** */

do
$$$$
    begin
        if not exists (select 1 from pg_type where typname = 'hydra_manhole_connection_type') then
            create type public.hydra_manhole_connection_type as enum ('manhole_cover', 'drainage_inlet');
        end if;
    end
$$$$;
;;

create table if not exists hydra.manhole_connection_type(
    id integer not null primary key,
    name hydra_manhole_connection_type unique not null,
    description character varying
    );
;;

insert into hydra.manhole_connection_type(name,id,description) values ('manhole_cover' ,1 ,'Manhole cover') on conflict do nothing;
;;

insert into hydra.manhole_connection_type(name,id,description) values ('drainage_inlet' ,2 ,'Drainage Inlet') on conflict do nothing;
;;
