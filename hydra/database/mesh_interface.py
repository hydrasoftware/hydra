# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import json
import os
from qgis.core import QgsDataSourceUri
from qgis.PyQt.QtCore import QObject, QCoreApplication, pyqtSignal
from qgis.PyQt.QtWidgets import QApplication, QMessageBox
from hydra.gui.forms.model_constrain import ModelConstrainWidget
from hydra.utility.map_point_tool import create_line, create_point, Snapper
from hydra.utility.settings_properties import SettingsProperties

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

def log_info(info, info_form):
    if info_form is not None:
        info_form.textInfos.append(info)
        QApplication.processEvents()

_current_dir = os.path.dirname(__file__)
_server_dir = os.path.join(_current_dir, '..', 'server', 'hydra')
_error_codes_file = os.path.join(_server_dir, 'mesh_error.json')

class MeshInterface(QObject):
    '''Handles interaction between project, meshing module and qgis interface'''

    layers_changed_signal = pyqtSignal(list)

    def __init__(self, canvas, project, parent=None):
        QObject.__init__(self, parent)
        self.__canvas = canvas
        self.__project = project
        with open(_error_codes_file, 'r') as f:
            self.__error_codes = json.load(f)

    def create_constrain(self):
        '''init a tool to create a line (snapping on other constrains), then inserts a constrain_line'''
        map_per_pix = self.__canvas.getCoordinateTransform().mapUnitsPerPixel()
        snapper = Snapper(10*map_per_pix)
        for x, y in self.__project.execute("""
                with points as (
                    select (st_dumppoints(geom)).geom as geom
                    from {model}.constrain
                    )
                select st_x(geom), st_y(geom) from points
                """.format(model=self.__project.get_current_model().name)).fetchall():
            snapper.add_point((x,y))

        line = create_line(self.__canvas, self.__project.log, self.__project.terrain, snapper)
        if line:
            ModelConstrainWidget(self.__project, line).exec_()
            self.layers_changed_signal.emit(['coverage', 'constrain'])

    def __mesh_one(self, coverage_id):
        '''Mesh one coverage (input as id)
           if query returns error code of 6 or higher (mesh elements properly created),
           allows user to choose to keep mesh without generated links'''
        res = self.__project.execute("""
            select {model}.mesh({coverage_id})
            """.format(model=self.__project.get_current_model().name, coverage_id=coverage_id)).fetchone()
        if res:
            error_code = res[0]
            if error_code==0:
                self.__project.log.notice(tr('Meshed coverage {}'.format(coverage_id)))
                small_mesh_elements = self.__project.execute(f"""select exists(
                                        select 1
                                        from {self.__project.get_current_model().name}.elem_2d_node as e
                                        join {self.__project.get_current_model().name}.coverage as c on c.id={coverage_id} and ST_Intersects(c.geom, e.geom)
                                        where ST_Area(contour)<4 and generated is not null limit 1);
                                        """).fetchone()[0]
                if small_mesh_elements:
                    self.__project.log.warning("""elem_2d with a surface smaller than 4m² were generated, """+
                                               """but the minimum area is set at 4m² by default during the computation. """+
                                               """This can lead to erroneous computation results. """+
                                               """See <a href="https://wiki.hydra-software.net/fr/model_building/river_free_surface_flow/mesh_element">elem_2d documentation page</a>.""")

                self.__project.execute("""
                    select {model}.set_link_altitude_topo(_link.id)
                    from {model}._link
                    join {model}.constrain on st_intersects(_link.geom, constrain.geom)
                    join {model}.coverage on st_intersects(coverage.geom, _link.geom)
                    where constrain.points_xyz is not null and coverage.id={coverage_id}
                """.format(model=self.__project.get_current_model().name, coverage_id=coverage_id))

                self.__project.commit()
            else:
                if error_code>5:
                    keep = QMessageBox(QMessageBox.Question, tr("Mesh error"), "{}. Keep mesh anyway ?".format(self.__error_codes[str(error_code)]), QMessageBox.Ok | QMessageBox.Cancel).exec_()
                    if keep == QMessageBox.Ok:
                        self.__project.log.notice('Kept mesh on coverage {} with error: {}'.format(coverage_id, self.__error_codes[str(error_code)]))
                        self.__project.commit()
                        return
                self.__project.log.warning("Error meshing coverage {}: {}".format(coverage_id, self.__error_codes[str(error_code)]))
                self.__project.rollback()
        else:
            self.__project.rollback()
            self.__project.log.warning("No coverage here")

    def __mesh_list(self, coverage_ids):
        '''Mesh a list of coverage (input id list)
           a config switch is in place (back to default at start, back to active config at endswith
           to avoid doing it while meshing each coverage'''
        # Saves current config and switches to default config
        cfg = self.__project.execute("""select configuration from {model}.metadata""".format(model=self.__project.get_current_model().name)).fetchone()[0]
        if cfg != 1:
            self.__project.execute("""update {model}.metadata set configuration=1""".format(model=self.__project.get_current_model().name))
            self.__project.commit()

        for id in coverage_ids:
            res = self.__project.execute("""
                select {model}.mesh({coverage_id})
                """.format(model=self.__project.get_current_model().name, coverage_id=id)).fetchone()
            if res[0]==0:
                self.__project.log.notice(tr('Meshed coverage {}'.format(id)))
                small_mesh_elements = self.__project.execute(f"""select exists(
                                        select 1
                                        from {self.__project.get_current_model().name}.elem_2d_node as e
                                        join {self.__project.get_current_model().name}.coverage as c on c.id={id} and ST_Intersects(c.geom, e.geom)
                                        where ST_Area(contour)<4 and generated is not null limit 1);
                                        """).fetchone()[0]
                if small_mesh_elements:
                    self.__project.log.warning(f"""elem_2d with a surface smaller than 4m² were generated in coverage {id}, """+
                                                """but the minimum area is set at 4m² by default during the computation. """+
                                                """This can lead to erroneous computation results. """+
                                                """See <a href="https://wiki.hydra-software.net/fr/model_building/river_free_surface_flow/mesh_element">elem_2d documentation page</a>.""")

                self.__project.execute("""
                    select {model}.set_link_altitude_topo(_link.id)
                    from {model}._link
                    join {model}.constrain on st_intersects(_link.geom, constrain.geom)
                    join {model}.coverage on st_intersects(coverage.geom, _link.geom)
                    where constrain.points_xyz is not null and coverage.id={coverage_id}
                """.format(model=self.__project.get_current_model().name, coverage_id=id))

                self.__project.commit()
            else:
                self.__project.log.warning("Error meshing coverage {}: {}".format(id, self.__error_codes[str(res[0])]))
                self.__project.rollback()

        # Restores initial configuration
        if cfg != 1:
            self.__project.execute("""update {m}.metadata set configuration={c}""".format(m=self.__project.get_current_model().name,c=cfg))
            self.__project.commit()

    def create_mesh(self):
        '''Mesh a coverage. First check if one or more 2D coverage is selected in active layer. Else select one coverage by tool'''
        active_layer = self.__canvas.currentLayer()
        if active_layer and QgsDataSourceUri(active_layer.source()).table() == 'coverage' and active_layer.selectedFeatureCount() > 0:
            coverage_ids = [str(f['id']) for f in active_layer.selectedFeatures()]
        else:
            point = create_point(self.__canvas, self.__project.log)
            if point:
                id, = self.__project.execute("""
                    select c.id
                    from {model}.coverage as c
                    where st_intersects(c.geom, 'SRID={srid}; POINT({x} {y})'::geometry)
                    and c.domain_type='2d'
                    """.format(model=self.__project.get_current_model().name, srid=str(self.__project.srid), x=point[0], y=point[1])).fetchone()
                coverage_ids = [id]

        if len(coverage_ids)==1:
            self.__mesh_one(coverage_ids[0])
        elif len(coverage_ids) > 1:
            self.__mesh_list(coverage_ids)
        self.layers_changed_signal.emit([])

    def mesh_regen(self):
        '''Delete mesh on meshed coverages, then mesh all coverages again.
           Commits those that go to the end (error_codes 0)'''
        confirm = QMessageBox(QMessageBox.Warning, tr('Regenerate all meshes'), tr('This will unmesh all 2D coverages tehn regenerate mesh in all of them. Proceed?'), QMessageBox.Ok | QMessageBox.Cancel).exec_()
        if confirm == QMessageBox.Ok:
            self.__project.log.notice("starting complete mesh regeneration")

            meshed_coverages = [id for (id,) in self.__project.execute("""
                with elem_2d as (select st_union(geom) as geom from {model}.elem_2d_node)
                select c.id from {model}.coverage as c, elem_2d as e where st_intersects(e.geom, c.geom) and c.domain_type='2d' order by c.id
                """.format(model=self.__project.get_current_model().name)).fetchall()]
            all_coverages = [id for (id,) in self.__project.execute("""
                select c.id from {model}.coverage as c where c.domain_type='2d' order by c.id
                """.format(model=self.__project.get_current_model().name)).fetchall()]

            for mc in meshed_coverages:
                self.__delete_mesh(mc)

            self.__mesh_list(all_coverages)
            self.layers_changed_signal.emit([])

    def mesh_unmeshed(self):
        '''Mesh unmeshed coverages. Commits those that go to the end (error_codes 0)'''
        confirm = QMessageBox(QMessageBox.Warning, tr('Mesh all unmeshed coverages'), tr('This will mesh all 2D coverages not currently meshed. Proceed?'), QMessageBox.Ok | QMessageBox.Cancel).exec_()
        if confirm == QMessageBox.Ok:
            self.__project.log.notice("starting mesh on unmeshed coverages")

            model = self.__project.get_current_model().name
            unmeshed_coverages = [id for (id,) in self.__project.execute("""
                with elem_2d as (select st_union(geom) as geom from {model}.elem_2d_node)
                select c.id from {model}.coverage as c, elem_2d as e where not st_intersects(e.geom, c.geom) and c.domain_type='2d' order by c.id
                """.format(model=model)).fetchall()]

            self.__mesh_list(unmeshed_coverages)
            self.layers_changed_signal.emit([])

    def __delete_mesh(self, coverage_id):
        '''Delete mesh by supporting coverage id'''
        model = self.__project.get_current_model().name
        self.__project.execute("""
            delete from {model}._link
            where id in (
                select distinct l.id
                from {model}._link as l, {model}.coverage as c
                where st_intersects(l.geom, c.geom)
                and c.id={coverage_id}
                and l.generated is not null
                )
            """.format(model=model, coverage_id=coverage_id))
        self.__project.execute("""
            delete from {model}.elem_2d_node
            where id in (
                select distinct e.id
                from {model}.elem_2d_node as e, {model}.coverage as c
                where st_intersects(e.geom, c.geom)
                and c.id={coverage_id}
                and e.generated is not null
                )
            """.format(model=model, coverage_id=coverage_id))
        self.__project.execute("""
            delete from {model}.river_node
            where id in (
                select distinct rn.id
                from {model}.river_node as rn, {model}.coverage as c
                where st_intersects(rn.geom, c.geom)
                and c.id={coverage_id}
                and rn.generated is not null
                and rn.id not in (select node from {model}._singularity)
                and rn.id not in (select id from {model}.river_cross_section_profile)
                and rn.id not in (select up from {model}._link where up is not null)
                and rn.id not in (select down from {model}._link where down is not null)
                )
            """.format(model=model, coverage_id=coverage_id))
        self.__project.commit()
        self.__project.log.notice('unmeshed coverage: {}'.format(coverage_id))

    def delete_mesh(self):
        '''Creates a point tool to select a coverage, then delete mesh there'''
        point = create_point(self.__canvas, self.__project.log)
        if point:
            coverage_id, = self.__project.execute("""
                    select c.id from {model}.coverage as c
                    where st_intersects(c.geom, 'SRID={srid}; POINT({x} {y})'::geometry)
                    """.format(model=self.__project.get_current_model().name, srid=str(self.__project.srid), x=point[0], y=point[1])).fetchone()
            self.__delete_mesh(coverage_id)
            self.layers_changed_signal.emit([])

    def create_links(self):
        '''Generates links between Boundary constrain and a coverage or 2 coverages'''
        active_layer = self.__canvas.currentLayer()

        coverage_ids = []
        # Try to process from coverage layers and selected items inside
        if active_layer and QgsDataSourceUri(active_layer.source()).table() == 'coverage' and active_layer.selectedFeatureCount() > 1:
            coverage_ids = [str(f['id']) for f in active_layer.selectedFeatures()]

        # Else goes with 2 clics in canvas
        else:
            pu = create_point(self.__canvas, self.__project.log)
            pd = create_point(self.__canvas, self.__project.log)

            # First case to check: BOUNDARY CONSTRAIN or STREET + STATION NODE:
            boundary_result = self.create_boundary_links(pu, pd)
            if boundary_result is True:
                # Links generated on boundary condition : EXIT
                return

            # Else assume we try to create LINKS BETWEEN 2 COVERAGES
            else:
                for p in [pu, pd]:
                    res = self.__project.execute("""
                    select id from {model}.coverage
                    where st_intersects(geom, 'SRID={srid}; POINT({x} {y})'::geometry)
                    """.format(model=self.__project.get_current_model().name, srid=self.__project.srid, x=p[0], y=p[1])).fetchall()
                    if res:
                        coverage_ids.append(str(res[0][0]))

        if len(coverage_ids) < 2:
            self.__project.log.warning(tr("you must select two domains"))
            return

        # Generate links between all domains adjacent
        self.__project.execute("""select {model}.create_links(c1.id, c2.id)
                                from {model}.coverage as c1, {model}.coverage as c2
                                where c1.id < c2.id and c1.id in ({ids}) and c2.id in ({ids}) and
                                ST_Length(ST_CollectionExtract(ST_Intersection(c1.geom, c2.geom), 2)) > 0
                                """.format(model=self.__project.get_current_model().name, ids=', '.join(coverage_ids)))

        self.__project.commit()
        self.layers_changed_signal.emit([])

    def create_boundary_links(self, point_up, point_down):
        assert point_up is not None and point_down is not None

        precision = SettingsProperties.get_snap()

        boundary_constrain = self.__project.execute("""
            select id from {model}.constrain
            where st_dwithin(geom, 'SRID={srid}; POINT({x} {y})'::geometry, {prec}) and constrain_type = 'boundary'
            """.format(model=self.__project.get_current_model().name, srid=self.__project.srid, x=point_up[0], y=point_up[1], prec=precision )).fetchone()

        street = self.__project.execute("""
            select id from {model}.street
            where st_dwithin(geom, 'SRID={srid}; POINT({x} {y})'::geometry, {prec})
            """.format(model=self.__project.get_current_model().name, srid=self.__project.srid, x=point_up[0], y=point_up[1], prec=precision )).fetchone()

        station_node = self.__project.execute("""
            select id from {model}.station_node
            where st_dwithin(geom, 'SRID={srid}; POINT({x} {y})'::geometry, {prec})
            """.format(model=self.__project.get_current_model().name, srid=self.__project.srid, x=point_down[0], y=point_down[1], prec=precision )).fetchone()

        # BOUNDARY CONSTRAIN + STATION NODE:
        if boundary_constrain is not None and station_node is not None:
            self.__project.execute("""
                select {model}.create_boundary_links('constrain', {cid}, {nid})
                """.format(model=self.__project.get_current_model().name, cid=boundary_constrain[0], nid=station_node[0]))
            self.__project.commit()
            self.layers_changed_signal.emit([])
            return True

        # STREET + STATION NODE:
        elif street is not None and station_node is not None:
            self.__project.execute("""
                select {model}.create_boundary_links('street', {sid}, {nid})
                """.format(model=self.__project.get_current_model().name, sid=street[0], nid=station_node[0]))
            self.__project.commit()
            self.layers_changed_signal.emit([])
            return True

        return False

    def create_network_overflow_links(self):
        '''Generate network overflow links in a coverage'''
        active_layer = self.__canvas.currentLayer()

        coverage_ids = []
        if not active_layer or QgsDataSourceUri(active_layer.source()).table() != 'coverage' or active_layer.selectedFeatureCount() == 0:
            point = create_point(self.__canvas, self.__project.log)

            res = self.__project.execute("""
                select id from {model}.coverage
                where st_intersects(geom, 'SRID={srid}; POINT({x} {y})'::geometry)
                """.format(model=self.__project.get_current_model().name, srid=self.__project.srid, x=point[0], y=point[1])).fetchone()
            if res:
                coverage_ids = [str(res[0])]
        else:
            coverage_ids = [str(f['id']) for f in active_layer.selectedFeatures()]

        for id in coverage_ids:
            self.__project.execute("""
                select {model}.create_network_overflow_links({id})
                """.format(model=self.__project.get_current_model().name, id=id))

        self.__project.commit()
        self.layers_changed_signal.emit([])
