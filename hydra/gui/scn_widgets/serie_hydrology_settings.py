# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
from builtins import str
from builtins import range
import os
import webbrowser
from operator import itemgetter
from functools import partial
from qgis.PyQt import uic, QtCore
from qgis.PyQt.QtWidgets import QFileDialog, QWidget, QComboBox, QTableWidgetItem, QGridLayout
from hydra.gui.base_dialog import tr
from hydra.gui.rain_manager import RainManager
from hydra.gui.widgets.combo_with_values import ComboWithValues
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from hydra.utility.string import get_sql_float
from hydra.utility.settings_properties import SettingsProperties
from hydra.utility.system import open_external


class SerieHydrologySettingsWidget(QWidget):

    def __init__(self, current_project, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "hydrology_settings.ui"), self)
        tmplayout = QGridLayout()
        tmplayout.addWidget(self,0,0)
        parent.setLayout(tmplayout)
        self.project = current_project
        self.active_bloc_id = None

        self.rainfall_combo = ComboWithValues(self.rainfall_combo_placeholder)
        self.dry_combo = ComboWithValues(self.dry_inflow_combo_placeholder)
        self.scenario_hydrology_rstart = ComboWithValues(self.scenario_hydrology_rstart_placeholder)
        self.flag_hydrology_rstart.stateChanged.connect(self.flag_hydrology_rstart_changed)
        self.hydro_from_other_serie = ComboWithValues(self.hydro_from_other_scn_placeholder)

        self.selected_rainfall = None
        self.selected_dry_inflow_scenario = None

        models = self.project.get_models()
        for model in models:
            self.combo_model_from.addItem(model)
            self.combo_model_to.addItem(model)

        self.combo_hy_from = ComboWithValues(self.placeholder_hy_from)
        self.combo_hy_to = ComboWithValues(self.placeholder_hy_to)

        self.btnAdd.clicked.connect(self.__rerouting_add)
        self.btnRemove.clicked.connect(self.__rerouting_remove)

        self.flag_hydrology_auto_dimensioning.setVisible(False)

        self.table_hyd_files = HydraTableWidget(current_project,
            ("id", "external_file"),
            (tr("Id"), tr("Path")),
            "project", "param_external_hydrograph_bloc",
            (self.__ask_and_add_hyd_file, self.del_hyd_file, self.edit_hyd_file, self.help_hyd_file), "", "id",
            self.table_hyd_files_placeholder, autoInit=False, showEdit=True, showHelp=True, file_columns=[1])
        self.table_hyd_files.set_editable(True)

        self.btn_rain_manager.clicked.connect(self.__rain_scenario)
        self.combo_model_from.activated.connect(partial(self.refresh_rerouting_combos, 'from'))
        self.combo_model_to.activated.connect(partial(self.refresh_rerouting_combos, 'to'))

        self.show()

    def set_bloc_serie(self, id_bloc, id_serie):
        self.active_serie_id = id_serie
        if id_bloc:
            self.active_bloc_id = id_bloc
            self.init_combo_rainfall()
            self.init_combo_dry_inflow()
            self.init_combo_hydrology_rstart_scenario()
            self.init_combo_hydrology_other_scenario()

            rainfall, dry_inflow, flag_hydrology_rstart, scenario_hydrology_rstart, soil_cr_alp, soil_horner_sat, soil_holtan_sat, soil_scs_sat, \
            soil_hydra_psat, soil_hydra_rsat, soil_gr4_psat, soil_gr4_rsat, scenario_hydrol = self.project.execute("""
            select rainfall, dry_inflow, flag_hydrology_rstart, scenario_hydrology_rstart, soil_cr_alp, soil_horner_sat, soil_holtan_sat, soil_scs_sat,
            soil_hydra_psat, soil_hydra_rsat, soil_gr4_psat, soil_gr4_rsat, scenario_hydrol
            from project.serie_bloc
            where id={} limit 1
            """.format(self.active_bloc_id)).fetchone()

            self.soil_cr_alp.setText(str(soil_cr_alp))
            self.soil_horner_sat.setText(str(soil_horner_sat))
            self.soil_holtan_sat.setText(str(soil_holtan_sat))
            self.soil_scs_sat.setText(str(soil_scs_sat))
            self.soil_hydra_psat.setText(str(soil_hydra_psat))
            self.soil_hydra_rsat.setText(str(soil_hydra_rsat))
            self.soil_gr4_psat.setText(str(soil_gr4_psat))
            self.soil_gr4_rsat.setText(str(soil_gr4_rsat))

            self.rainfall_combo.set_selected_value(rainfall)
            self.dry_combo.set_selected_value(dry_inflow)

            self.flag_hydrology_rstart.setChecked(flag_hydrology_rstart)
            self.scenario_hydrology_rstart.set_selected_value(scenario_hydrology_rstart)
            self.flag_hydrology_rstart_changed()

            if self.hydro_from_other_serie.isEnabled():
                self.hydro_from_other_serie.set_selected_value(scenario_hydrol)
            else:
                self.hydro_from_other_serie.set_selected_value(None)

            self.table_hyd_files.set_filter("where param_bloc={}".format(self.active_bloc_id))

            self.refresh_rerouting_combos()
            self.refresh_rerouting_table()

    def save(self):
        if self.active_bloc_id:
            self.project.execute("""
            update project.serie_bloc
            set rainfall={}, dry_inflow={}, soil_cr_alp='{}', soil_horner_sat='{}', soil_holtan_sat='{}', soil_scs_sat='{}',
            soil_hydra_psat='{}', soil_hydra_rsat='{}', soil_gr4_psat='{}', soil_gr4_rsat='{}', flag_hydrology_rstart='{}',
            scenario_hydrology_rstart={}, scenario_hydrol={}
            where id={}
            """.format(self.rainfall_combo.get_selected_value() if self.rainfall_combo.get_selected_value() is not None else 'null',
                self.dry_combo.get_selected_value() if self.dry_combo.get_selected_value() is not None else 'null',
                str(self.soil_cr_alp.text()), str(self.soil_horner_sat.text()), str(self.soil_holtan_sat.text()), str(self.soil_scs_sat.text()),
                str(self.soil_hydra_psat.text()), str(self.soil_hydra_rsat.text()), str(self.soil_gr4_psat.text()), str(self.soil_gr4_rsat.text()),
                self.flag_hydrology_rstart.isChecked(),
                self.scenario_hydrology_rstart.get_selected_value() if self.scenario_hydrology_rstart.get_selected_value() is not None else 'null',
                self.hydro_from_other_serie.get_selected_value() if self.hydro_from_other_serie.get_selected_value() is not None else 'null',
                str(self.active_bloc_id)))
            self.__save_rerouting()

    def init_combo_dry_inflow(self):
        if self.active_bloc_id is not None:
            dry_scenarios = self.project.execute("select name, id from project.dry_inflow_scenario order by name").fetchall()
            self.dry_combo.clear()
            self.dry_combo.addItem(tr("None"), None)
            for dry_scenario in dry_scenarios:
                self.dry_combo.addItem(dry_scenario[0], dry_scenario[1])

    def init_combo_rainfall(self):
        if self.active_bloc_id is not None:
            rains = self.project.execute("select name, id from project._rainfall order by name").fetchall()
            self.rainfall_combo.clear()
            self.rainfall_combo.addItem(tr("None"), None)
            for rain in rains:
                self.rainfall_combo.addItem(rain[0], rain[1])

    def init_combo_hydrology_rstart_scenario(self):
        if self.active_bloc_id is not None:
            serie_id_name = [[id,name] for (id,name) in self.project.execute("""select id,name from project.serie""").fetchall()]
            self.scenario_hydrology_rstart.clear()
            self.scenario_hydrology_rstart.addItem("None", None)
            for serie in sorted(serie_id_name, key=itemgetter(1)):
                if not float(serie[0])==float(self.active_serie_id):
                    self.scenario_hydrology_rstart.addItem(serie[1], int(serie[0]))

    def init_combo_hydrology_other_scenario(self):
        if self.active_bloc_id is not None:
            serie_id_name = [[id,name] for (id,name) in self.project.execute("""select id,name from project.serie""").fetchall()]
            self.hydro_from_other_serie.clear()
            for serie in sorted(serie_id_name, key=itemgetter(1)):
                if not float(serie[0])==float(self.active_serie_id):
                    self.hydro_from_other_serie.addItem(serie[1], int(serie[0]))

    def flag_hydrology_rstart_changed(self):
        self.scenario_hydrology_rstart.setEnabled(self.flag_hydrology_rstart.isChecked())
        if not self.flag_hydrology_rstart.isChecked():
            self.scenario_hydrology_rstart.set_selected_value(None)

    def __ask_and_add_hyd_file(self):
        if self.active_bloc_id is not None:
            fileName, __ = QFileDialog.getSaveFileName(self, tr('Add file'), SettingsProperties.get_memorized_dir() or self.project.data_dir, options=QFileDialog.DontConfirmOverwrite)
            if fileName is not None and fileName!='':
                # creates file empty if not exists
                open(fileName, 'a').close()
                self.add_hyd_file(fileName)
                # saves directory in settings for other files openings (for current session)
                SettingsProperties.set_memorized_dir(os.path.dirname(fileName))

    def add_hyd_file(self, filePath):
        if self.active_bloc_id is not None:
            self.table_hyd_files.add_row((filePath, self.active_bloc_id), ["param_bloc"])

    def del_hyd_file(self):
        if self.active_bloc_id is not None:
            self.table_hyd_files.del_selected_row()

    def edit_hyd_file(self):
        items = self.table_hyd_files.table.selectedItems()
        if len(items)>0:
            file = items[1].text()
            open_external(file)

    def help_hyd_file(self):
        webbrowser.open('http://hydra-software.net/docs/notes_techniques/NT24.pdf', new=0)

    def __rain_scenario(self):
        current_rain = self.rainfall_combo.get_selected_value()
        rain = RainManager(self.project)
        rain.exec_()
        self.init_combo_rainfall()
        self.rainfall_combo.set_selected_value(current_rain)

    def refresh_rerouting_combos(self, combo=None):
        if self.active_bloc_id is not None:
            if (combo=='from' or combo is None) and self.combo_model_from.currentIndex()!=-1:
                self.combo_hy_from.clear()
                hydrographs_from = self.project.execute("""select id, name from {}.hydrograph_bc_singularity order by name;""".format(self.combo_model_from.currentText())).fetchall()
                self.combo_hy_from.addItem("None", None)
                for hydrograph in hydrographs_from:
                    self.combo_hy_from.addItem(hydrograph[1],hydrograph[0])
            if (combo=='to' or combo is None) and self.combo_model_to.currentIndex()!=-1:
                self.combo_hy_to.clear()
                hydrographs_to = self.project.execute("""select id, name from {}.hydrograph_bc_singularity order by name;""".format(self.combo_model_to.currentText())).fetchall()
                self.combo_hy_to.addItem("None", None)
                for hydrograph in hydrographs_to:
                    self.combo_hy_to.addItem(hydrograph[1],hydrograph[0])

    def refresh_rerouting_table(self):
        if self.active_bloc_id is not None:
            rerouting_list = self.project.execute("""
                select model_from, hydrograph_from, model_to, hydrograph_to, option, parameter
                from project.inflow_rerouting_bloc
                where bloc={}
                order by model_from, hydrograph_from, model_to, hydrograph_to;""".format(self.active_bloc_id)).fetchall()

            rerouting_options = self.project.execute("""
                select id, name, description
                from hydra.rerouting_option
                order by name;""").fetchall()

            self.rerouting_table.setRowCount(0)
            for item in rerouting_list:
                rowcount = self.rerouting_table.rowCount() + 1
                self.rerouting_table.setRowCount(rowcount)
                row = rowcount - 1
                self.rerouting_table.setItem(row, 0, QTableWidgetItem(str(item[0])))
                self.rerouting_table.setItem(row, 1, QTableWidgetItem(str(self.__get_hydrograph_name(item[0],item[1]))))
                self.rerouting_table.setItem(row, 2, QTableWidgetItem(str(item[2])))
                self.rerouting_table.setItem(row, 3, QTableWidgetItem(str(self.__get_hydrograph_name(item[2],item[3]))))

                cbox = QComboBox()
                for i in range(0, len(rerouting_options)):
                    cbox.addItem(rerouting_options[i][1])
                    if (item[3] == rerouting_options[i][1]):
                        cbox.setCurrentIndex(i)
                self.rerouting_table.setCellWidget(row,3, cbox)

                self.rerouting_table.setItem(row, 4, QTableWidgetItem(str(item[4])))

                for i in range(3):
                    self.rerouting_table.item(row,i).setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)

            self.rerouting_table.resizeColumnsToContents()
            self.rerouting_table.horizontalHeader().setStretchLastSection(True)

    def __save_rerouting(self):
        for irow in range(0,self.rerouting_table.rowCount()):
            model_from=self.rerouting_table.item(irow,0).text()
            hydrograph_from=self.__get_hydrograph_id(model_from, self.rerouting_table.item(irow,1).text())
            model_to=self.rerouting_table.item(irow,2).text()
            hydrograph_to=self.__get_hydrograph_id(model_to, self.rerouting_table.item(irow,2).text())
            option=self.rerouting_table.cellWidget(irow,4).currentText()
            parameter=get_sql_float(self.rerouting_table.item(irow,5).text())

            str_hy_to= """model_to='{}' and hydrograph_to={}""".format(model_to, hydrograph_to) if hydrograph_to is not None else """model_to='' and hydrograph_to=null"""

            self.project.execute("""update project.inflow_rerouting_bloc set option='{}', parameter={}
                                        where scenario={} and model_from='{}' and hydrograph_from={} and {};
                                        """.format(option, parameter, self.active_scn_id, model_from, hydrograph_from, str_hy_to))

    def __rerouting_add(self):
        if self.active_bloc_id is not None:
            self.__save_rerouting()

            existing_rerouting = self.project.execute("""
                select model_from, hydrograph_from, model_to, hydrograph_to
                from project.inflow_rerouting_bloc
                where bloc={};""".format(self.active_bloc_id)).fetchall()

            model_from=str(self.combo_model.currentText())
            hydrograph_from=self.combo_hy_from.get_selected_value()
            model_to=str(self.combo_model_to.currentText())
            hydrograph_to=self.combo_hy_to.get_selected_value()

            if self.combo_model_from.currentIndex()==-1 or self.combo_hy_from.currentIndex()==-1 or self.combo_model_to.currentIndex()==-1 or self.combo_hy_to.currentIndex()==-1:
                return
            elif model_from==model_to and hydrograph_from==hydrograph_to:
                return
            elif (model_from ,hydrograph_from, model_to, hydrograph_to) in existing_rerouting:
                return

            model_to, hydrograph_to = (model_to, hydrograph_to) if hydrograph_to is not None else ('', 'null')
            self.project.execute("""insert into project.inflow_rerouting_bloc(bloc, model_from, hydrograph_from, model_to, hydrograph_to) values({},'{}', {}, '{}', {})
                                    """.format(self.active_bloc_id, model_from, hydrograph_from, model_to, hydrograph_to))
            self.refresh_rerouting_table()

    def __rerouting_remove(self):
        if self.active_bloc_id is not None:
            self.__save_rerouting()

            items = self.rerouting_table.selectedItems()
            if len(items)>0:
                selected_row = self.rerouting_table.row(items[0])
                model_from=self.rerouting_table.item(selected_row,0).text()
                hydrograph_from=self.__get_hydrograph_id(model_from, self.rerouting_table.item(selected_row,1).text())
                model_to=self.rerouting_table.item(selected_row,2).text()
                hydrograph_to=self.__get_hydrograph_id(model_to, self.rerouting_table.item(selected_row,3).text())
                str_hy_to= """model_to='{}' and hydrograph_to={}""".format(model_to, hydrograph_to) if hydrograph_to is not None else """model_to='' and hydrograph_to is null"""
                self.project.execute("""delete from project.inflow_rerouting_bloc where bloc={} and model_from='{}' and hydrograph_from={} and {};
                                        """.format(self.active_bloc_id, model_from, hydrograph_from, str_hy_to))
                self.refresh_rerouting_table()

    def __get_hydrograph_id(self, model, hydrograph_name):
        if model=='' or hydrograph_name is None:
            return None
        id = self.project.execute("""select id from {}.hydrograph_bc_singularity where name='{}';""".format(model, hydrograph_name)).fetchone()
        if id is not None:
            return id[0]
        else:
            return None

    def __get_hydrograph_name(self, model, hydrograph_id):
        if model=='' or hydrograph_id is None:
            return None
        name = self.project.execute("""select name from {}.hydrograph_bc_singularity where id={};""".format(model, hydrograph_id)).fetchone()
        if name is not None:
            return name[0]
        else:
            return None
