# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
import project (scenarios, rains,...) from old hydra models
"""

from __future__ import absolute_import # important to read the doc !
from inspect import getargvalues, currentframe
import traceback
import re
import sys, os, math
import codecs
import datetime
from qgis.PyQt.QtWidgets import QApplication
from hydra.utility.log import LogManager
from hydra.utility.timer import Timer
from hydra.database import database as dbhydra
from hydra.project import Project

def _get_nbRow(table):
    return (int(project.execute("""select count(*) nbrow from project.{}""".format(table)).fetchone()[0]))

def _readline_to_array(csv_streamFile):
    return [v[1:-1] if len(v) and v[0]=="'" and v[-1]=="'" else v
            for v in csv_streamFile.readline().strip().split(';')]

def _parse_array(lineArr, num_start_col, nbcol, csv_streamFile, bStr=False, bClosed=False):
    zzArr=[]
    if bStr:
        nbpt = int(str(lineArr[num_start_col]).split(' ')[0])
    else:
        nbpt=int(float(str(lineArr[num_start_col]).split(' ')[0]))
    if nbpt > 0:
        if bStr:
            zzArr = [[str(lineArr[num_start_col+icol+1]) for icol in range(nbcol)]]
        else:
            zzArr = [[float(lineArr[num_start_col+icol+1]) for icol in range(nbcol)]]
        for i in range(nbpt-1):
            lineArr = _readline_to_array(csv_streamFile)
            if lineArr[num_start_col+1]:
                if bStr:
                    zzArr.append([str(lineArr[num_start_col+icol+1]) for icol in range(nbcol)])
                else:
                    zzArr.append([float(lineArr[num_start_col+icol+1]) for icol in range(nbcol)])
        if bClosed==True and zzArr[0] != zzArr[-1]: zzArr.append(zzArr[0])
        return zzArr
    else:
        raise Exception("no element for array "+str(nbpt))

def _quote(value):
    return "'"+value+"'" \
            if (isinstance(value, str) or isinstance(value, str))\
            and ( value == "" or value[0] != "'") and value != "null" else value

def _get_attrib_from_table_by_name(table, name):
    return project.execute('select * from project.{} where name={}'.format(table, _quote(str(name)))).fetchall()

def _insert(table, arg_map):
    argmap = {key: value for key, value in arg_map.items()
        if value is not None and key != 'table'}
    if len(argmap):
        sql = ("""insert into project.{tab}({col}) values({val}) returning id""".format(
                tab=table,
                col=",".join(argmap.keys()),
                val=",".join(_quote(str(value)) for value in argmap.values())))
        new_id, = project.execute(sql).fetchone()
    else:
        new_id, = project.execute(""" insert into project.{} default values returning id""".format(table)).fetchone()
    return new_id

def _update(table, id, arg_map):
    for key, value in arg_map.items():
        if value is not None and key not in ['table','id']:
            project.execute("""update project.{tab} set {col}={val} where id={id}""".format(tab=table,col=key,val=_quote(str(value)),id=id))

def _make_point(point):
    return "'srid={}; POINTZ({} {} {})'::geometry".format(
            str(project.srid),
            str(point[0]),
            str(point[1]),
            str(point[2]) if len(point) == 3 else 9999
            ) if point else None

def _make_polygon(contour):
    return "'srid={}; POLYGON(({}))'::geometry".format(
            str(project.srid),
            ",".join([str(p[0])+" "+str(p[1]) for p in contour[0]])) if contour else None

def _make_real_array(array):
    return "'{}'::real[]".format(
            str(array).replace(']', '}'
                     ).replace('[', '{'
                     ).replace('(', '{'
                     ).replace(')', '}')) if array else None

def _add_singularity(table, geom, name=None):
    """ used to insert default singularities into any tables """
    geom = _make_point( geom)
    return _insert(table, getargvalues(currentframe()).locals)

def _update_singularity(table, id, geom, name=None):
    """ used to update default singularities into any tables """
    geom = _make_point( geom)
    _update(table, id, getargvalues(currentframe()).locals)

class LineCounter(object):
    """to count lines read in functions with readline"""
    def __init__(self, f):
        self.__f = f
        self.count = 0
    def readline(self):
        self.count += 1
        return self.__f.readline()

def _delete_scenario():
    """remove all data from scenario table and linked tables"""
    project.execute("delete from project.scenario")
    project.execute("delete from project.output_option")

class Properties(object):
    def __init__(self, index_of, lineArr):
        self.__index_of = index_of
        self.__lineArr = lineArr

    def __prop(self, kwd):
        idx = self.__index_of[kwd]
        return self.__lineArr[idx] if idx < len(self.__lineArr) else None

    def s(self, kwd):
        s = self.__prop(kwd)
        return str(s) if s else None

    def f(self, kwd):
        s = self.__prop(kwd)
        return float(s) if s else None

    def i(self, kwd):
        s = self.__prop(kwd)
        return int(s) if s else None

    def b(self, kwd):
        s = self.__prop(kwd)
        return False if int(s)==0 else True

def multi_parse_scn_param(lineArr, csv_streamFile, line_number):
    """create scnenario table and its linked tables"""
    _delete_scenario()

    index_of = {
        u"Element":                      1,
        u"Identifiant":                  2,
        u"Commentaire":                  3,
        u"comput_mode":                  4,
        u"dry_inflow":                   5,
        u"rainfall":                     6,
        u"soil_cr_alp":                  8,
        u"soil_horner_sat":              8,
        u"soil_holtan_sat":              7,
        u"soil_scs_sat":                 7,
        u"soil_hydra_psat":              7,
        u"soil_hydra_rsat":              7,
        u"soil_gr4_psat":                7,
        u"soil_gr4_rsat":                7,
        u"dt_hydrol_mn":                 9,
        u"option_dim_hydrol_network":   10,
        u"date0":                       11,
        u"tfin_hr":                     12,
        u"tini_hydrau_hr":              13,
        u"dtmin_hydrau_hr":             14,
        u"dtmax_hydrau_hr":             15,
        u"dzmin_hydrau":                16,
        u"dzmax_hydrau":                17,
        u"flag_save":                   18,
        u"tsave_hr":                    19,
        u"flag_rstart":                 20,
        u"scenario_rstart":             21,
        u"flag_hydrology_rstart":       22,
        u"scenario_hydrology_rstart":   23,
        u"trstart_hr":                  24,
        u"graphic_control":             25,
        u"model_connect_settings":      26,
        u"mixed":                       27,
        u"tbegin_output_hr":            28,
        u"tend_output_hr":              29,
        u"dt_output_hr":                30,
        u"comput_option":               31,
        u"tronc_vl":                    32,
        u"nofente":                     33,
        u"zini":                        34,
        u"c_affin":                     35,
        u"t_affin_start_hr":            36,
        u"strickl":                     37,
        u"option_file":                 38,
        u"fisor":                       39,
        u"sor1":                        40,
        u"sor2":                        41,
        u"sor3":                        42,
        u"sor7":                        43,
        u"sor10":                       44,
        u"sor13":                       45,
        u"sor14":                       46,
        u"sor15":                       47,
        u"sor16":                       48,
        u"transport_type":              49,
        u"iflag_mes":                   50,
        u"iflag_dbo5":                  51,
        u"iflag_dco":                   52,
        u"iflag_ntk":                   53,
        u"diffusion":                   54,
        u"dispersion_coef":             55,
        u"longit_diffusion_coef":       56,
        u"lateral_diff_coef":           57,
        u"class":                       58,
        u"water_temperature_c":         59,
        u"min_o2_mgl":                  60,
        u"kdbo5_j_minus_1":             61,
        u"koxygen_j_minus_1":           62,
        u"knr_j_minus_1":               63,
        u"kn_j_minus_1":                64,
        u"bdf_mgl":                     65,
        u"o2sat_mgl":                   66,
        u"iflag_ecoli":                 67,
        u"iflag_enti":                  68,
        u"iflag_ad1":                   69,
        u"iflag_ad2":                   70,
        u"ad1_decay_rate_jminus_one":   71,
        u"ad2_decay_rate_jminus_one":   72,
        u"ad3_decay_rate_jminus_one":   73,
        u"ad4_decay_rate_jminus_one":   74,
        u"sst_mgl":                     75,
        u"sediment_file":               76}

    scnLineArr=[lineArr]
    while line_number < number_of_lines+1:
        id_scn = project.add_new_scenario()
        name, comment =  str(lineArr[2]), str(lineArr[3])
        _update('scenario',id_scn, {'name':name,'comment':comment})
        if line_number < number_of_lines:
            lineArr = _readline_to_array(csv_streamFile)
            scnLineArr.append(lineArr)
        line_number += 1

    for lineArr in scnLineArr:
        #scenario table
        prop = Properties(index_of, lineArr)

        name =  str(lineArr[2])
        id_scn = int(_get_attrib_from_table_by_name('scenario', name)[0][0])
        id_dry_inflow = project.execute("""select id from project.dry_inflow_scenario
            where name='{}'""".format(prop.s(u"dry_inflow"))).fetchone()[0] \
            if prop.s(u"dry_inflow")!=None else None
        id_rainfall = project.execute("""select id from project._rainfall
            where name='{}'""".format(prop.s(u"rainfall"))).fetchone()[0] if prop.s(u"rainfall")!=None else None

        scnrstart_name=prop.s(u"scenario_rstart").strip() if prop.s(u"scenario_rstart") is not None else None
        if _get_nbRow( 'scenario')==0 or (scnrstart_name=='Aucun' or None):
            id_scnrestart=None
        else:
            id_scnrestart = int(project.execute("""select id from project.scenario
                where name='{}'""".format(scnrstart_name)).fetchone()[0]) \
                if _get_attrib_from_table_by_name('scenario', scnrstart_name) else None

        dicoval = {
            "comput_mode": prop.s(u"comput_mode"),
            "dry_inflow": id_dry_inflow,
            "rainfall": id_rainfall,
            "dt_hydrol_mn": datetime.timedelta(minutes=prop.f(u"dt_hydrol_mn") if prop.f(u"dt_hydrol_mn") else 5),
            "soil_cr_alp": prop.f(u"soil_cr_alp"),
            "soil_horner_sat": prop.f(u"soil_horner_sat"),
            "soil_holtan_sat": prop.f(u"soil_holtan_sat"),
            "soil_scs_sat": prop.f(u"soil_scs_sat"),
            "soil_hydra_psat": prop.f(u"soil_hydra_psat"),
            "soil_hydra_rsat": prop.f(u"soil_hydra_rsat"),
            "soil_gr4_psat": prop.f(u"soil_gr4_psat"),
            "soil_gr4_rsat": prop.f(u"soil_gr4_rsat"),
            "option_dim_hydrol_network": prop.b(u"option_dim_hydrol_network"),
            "date0": prop.s(u"date0"),
            "tfin_hr": datetime.timedelta(hours=prop.f(u"tfin_hr") if prop.f(u"tfin_hr") else 0),
            "tini_hydrau_hr": datetime.timedelta(hours=prop.f(u"tini_hydrau_hr") if prop.f(u"tini_hydrau_hr") else 12),
            "dtmin_hydrau_hr": datetime.timedelta(hours=prop.f(u"dtmin_hydrau_hr") if prop.f(u"dtmin_hydrau_hr") else 0.004),
            "dtmax_hydrau_hr": datetime.timedelta(hours=prop.f(u"dtmax_hydrau_hr") if prop.f(u"dtmax_hydrau_hr") else 0.083),
            "dzmin_hydrau": prop.f(u"dzmin_hydrau"),
            "dzmax_hydrau": prop.f(u"dzmax_hydrau"),
            "flag_save": prop.b(u"flag_save"),
            "tsave_hr": datetime.timedelta(hours=prop.f(u"tsave_hr") if prop.f(u"tsave_hr") else 0),
            "flag_rstart": prop.b(u"flag_rstart"),
            "scenario_rstart": id_scnrestart,
            "flag_hydrology_rstart": prop.b(u"flag_rstart"),
            "scenario_hydrology_rstart": id_scnrestart,
            "trstart_hr": datetime.timedelta(hours=prop.f(u"trstart_hr") if prop.f(u"trstart_hr") else 0),
            "graphic_control": prop.b(u"graphic_control"),
            "model_connect_settings": prop.s(u"model_connect_settings"),
            "tbegin_output_hr": datetime.timedelta(hours=prop.f(u"tbegin_output_hr") if prop.f(u"tbegin_output_hr") else 0),
            "tend_output_hr": datetime.timedelta(hours=prop.f(u"tend_output_hr") if prop.f(u"tend_output_hr") else 12),
            "dt_output_hr": datetime.timedelta(hours=prop.f(u"dt_output_hr") if prop.f(u"dt_output_hr") else 0)}

        _update('scenario', id_scn, dicoval)

def parse_plv(lineArr):
    """create table rain_gage and gage_rainfall"""
    table, name = 'rain_gage', str(lineArr[2])
    xyArr=lineArr[4].split('-')
    retline = _get_attrib_from_table_by_name(table, name)
    if len(retline)==0:
        id_plv=_add_singularity(table, xyArr, name)
    else:
        id_plv = retline[0][0]
        _update_singularity(table, id_plv, xyArr, name)

def parse_pluvio_external(lineArr):
    """create 'gage_rainfall'"""
    table = 'gage_rainfall'
    name = str(lineArr[2])
    interpolation_type = {'thyssen': 'shortest_distance', 'distance_ponderation': 'distance_ponderation'}[lineArr[3]]
    cbv_grid_file = lineArr[4]
    retline = _get_attrib_from_table_by_name(table, name)

    dicoval={'name':name,'cbv_grid_connect_file':cbv_grid_file,'interpolation':interpolation_type}
    if len(retline)==0:
        id_plv=_insert(table, dicoval)
    else:
        _update(table,int(retline[0][0]),dicoval)

def parse_grid_radar(lineArr):
    pass
    # """create 'radar_grid'"""
    # table, name = 'radar_grid', str(lineArr[2])
    # x0,y0,dx,dy,nx,ny=lineArr[3],lineArr[4],lineArr[5],lineArr[6],lineArr[7],lineArr[8]
    # retline = _get_attrib_from_table_by_name(table, name)

    # dicoval={'name':name,'x0':x0,'y0':y0,'dx':dx,'dy':dy,'nx':nx,'ny':ny}
    # if len(retline)==0:
        # id_radar=_insert(table,dicoval)
    # else:
        # _update(table,int(retline[0][0]),dicoval)

def parse_hyeto_radar(lineArr):
    """create 'radar_rainfall', table radar_grid must exist"""
    table, name = 'radar_rainfall', str(lineArr[2])
    prf_file = lineArr[3]
    retline = _get_attrib_from_table_by_name(table, name)

    dicoval={'name':name,'file':prf_file}
    if len(retline)==0:
        id_radar=_insert(table,dicoval)
    else:
        _update(table,int(retline[0][0]),dicoval)

def multi_parse_saptsec( lineArr, csv_streamFile, line=None):
    table, name, comment, nbvtx = 'dry_inflow_sector', lineArr[2], lineArr[3], lineArr[4]
    if int(nbvtx)==0:
        logger.warning(" contour must exist for dry inflow sector "+str(name))
        return
    if "'" in comment: comment=str(comment).replace("'", "?")  #if No, error in sql
    contour = [_parse_array(lineArr, 4, 2, csv_streamFile, False, True)] if int(nbvtx)>0 else None
    retline = _get_attrib_from_table_by_name(table, name)
    geom=_make_polygon(contour)

    dicoval={'name':name,'comment':comment,'geom':geom}
    if len(retline)==0:
        #geom=_make_polygon(contour)
        id=_insert(table,dicoval)
    else:
        id=int(retline[0][0])
        #geom=_make_polygon(contour)
        _update(table,id,dicoval)

def multi_parse_crbtsec(lineArr, csv_streamFile, line=None):
    table, name = 'dry_inflow_hourly_modulation', lineArr[2]
    crvArr = _parse_array(lineArr, 3, 2, csv_streamFile)
    hvArr = _make_real_array([[h,v,v] for [h,v] in crvArr])
    retline = _get_attrib_from_table_by_name(table, name)
    if len(retline)==0:
        id=_insert(table, {'name':name,'hv_array':hvArr})
    else:
        _update(table, int(retline[0][0]), {'name':name,'hv_array':hvArr})

def multi_parse_scntsec(lineArr, csv_streamFile, line=None):
    '''create table dry_inflow_scenario and dry_inflow_scenario_sector_setting, the following tables must exist:
       dry_inflow_hourly_modulation and dry_inflow_sector'''
    name, comment, crv = lineArr[2], lineArr[3], lineArr[4]
    if "'" in comment: comment=str(comment).replace("'", "?")  #if No, error in sql
    retline = _get_attrib_from_table_by_name('dry_inflow_scenario', name)
    if len(retline)==0:
        id_scenario=_insert('dry_inflow_scenario', {'name':name,'comment':comment})
    else:
        id_scenario=int(retline[0][0])
        _update('dry_inflow_scenario', id_scenario, {'name':name,'comment':comment})

    # curve=_get_attrib_from_table_by_name('dry_inflow_hourly_modulation', crv)
    # if len(curve)==0:
        # logger.warning("\n{f}".format(f=str(crv)+": this hourly modulation curve must exist"))
        # return
    # id_curve = curve[0][0]

    project.execute("""delete from project.dry_inflow_scenario_sector_setting where dry_inflow_scenario={}""".format(id_scenario))

    sectorArr = _parse_array(lineArr, 5, 5, csv_streamFile, True)
    for ii in range(len(sectorArr)):
        sector=sectorArr[ii][0]
        id_sector = _get_attrib_from_table_by_name('dry_inflow_sector', sector)[0][0]
        vol_wasted, coef_vol_wasted = float(sectorArr[ii][1]), float(sectorArr[ii][2])
        vol_clear, coef_vol_clear = float(sectorArr[ii][3]), float(sectorArr[ii][4])

        project.execute("""insert into project.dry_inflow_scenario_sector_setting
                        (dry_inflow_scenario,sector,volume_sewage_m3day,coef_volume_sewage_m3day,volume_clear_water_m3day,coef_volume_clear_water_m3day)
                        values({},{},{},{},{},{})""".format(id_scenario,id_sector,vol_wasted,coef_vol_wasted,vol_clear,coef_vol_clear))

def parse_montana(lineArr):
    """create table 'coef_montana'"""
    table, name, comment = 'coef_montana', lineArr[2], lineArr[3]
    if "'" in comment: comment=str(comment).replace("'", "?")  #if No, error in sql
    retline = _get_attrib_from_table_by_name(table, name)

    dicoval={'name':name,'comment':comment,'return_period_yr':float(lineArr[4]),
             'coef_a':float(lineArr[5]),'coef_b':float(lineArr[6])}
    if len(retline)==0:
        id=_insert(table,dicoval)
    else:
        _update(table, int(retline[0][0]),dicoval)

def parse_caquot(lineArr):
    """create table 'caquot_rainfall', table 'coef_montana' must exist"""
    table, name, montana_name = 'caquot_rainfall', lineArr[2], lineArr[4]
    id_montana = _get_attrib_from_table_by_name('coef_montana', montana_name)[0][0]
    retline = _get_attrib_from_table_by_name(table, name)

    dicoval={'name':name,'montana':id_montana}
    if len(retline)==0:
        id=_insert(table,dicoval)
    else:
        _update(table, int(retline[0][0]),dicoval)

def parse_plu_st(lineArr):
    """create table 'single_triangular_rainfall', , table 'coef_montana' must exist"""
    table, name, montana_name = 'single_triangular_rainfall', lineArr[2], lineArr[4]
    id_montana = _get_attrib_from_table_by_name('coef_montana', montana_name)[0][0] if _get_attrib_from_table_by_name('coef_montana', montana_name) else None
    retline = _get_attrib_from_table_by_name(table, name)

    total_duration_mn = lineArr[5] if len(lineArr)>5 and lineArr[5] else 0
    peak_time_mn = lineArr[6] if len(lineArr)>6 and lineArr[6] else 0

    dicoval={'name':name,'montana':id_montana,'total_duration_mn':total_duration_mn,'peak_time_mn':peak_time_mn}
    if len(retline)==0:
        id=_insert(table,dicoval)
    else:
        _update(table, int(retline[0][0]),dicoval)

def parse_plu_dt(lineArr):
    """create table 'double_triangular_rainfall', table 'coef_montana' must exist"""
    table, name, montana_tot_name, montana_peak_name = 'double_triangular_rainfall', lineArr[2], lineArr[4], lineArr[5]
    id_montana_tot = _get_attrib_from_table_by_name('coef_montana', montana_tot_name)[0][0]
    id_montana_peak = _get_attrib_from_table_by_name('coef_montana', montana_peak_name)[0][0]
    retline = _get_attrib_from_table_by_name(table, name)

    dicoval={'name':name,'montana_total':id_montana_tot,
             'montana_peak':id_montana_peak,'total_duration_mn':lineArr[6],
             'peak_duration_mn':lineArr[7],'peak_time_mn':lineArr[8]}
    if len(retline)==0:
        id=_insert(table,dicoval)
    else:
        _update(table, int(retline[0][0]),dicoval)

def multi_parse_hyeto_internal(lineArr, csv_streamFile, line=None):
    """create table 'intensity_curve_rainfall' with time(mn), intensity(mm/h)"""
    table, name = 'intensity_curve_rainfall', lineArr[2]
    crvArr = _parse_array(lineArr, 3, 2, csv_streamFile)
    tiArr = _make_real_array(crvArr)
    retline = _get_attrib_from_table_by_name(table, name)

    dicoval={'name':name,'t_mn_intensity_mmh':tiArr}
    if len(retline)==0:
        id=_insert(table,dicoval)
    else:
        _update(table, int(retline[0][0]),dicoval)

def log_info(info, info_form):
    if info_form is not None:
        info_form.textInfos.append(info)
        QApplication.processEvents()

def import_project_csv(filename, pj, logger=LogManager(), infoform=None):
    global number_of_lines,line_number, csv_line, project
    project=pj
    progress = logger.progress("loading "+filename+"\n")
    log_info("loading "+filename, infoform)
    number_of_lines = len(codecs.open(filename, 'r', 'latin-1').readlines())
    with codecs.open(filename, 'r', 'latin-1') as csv_streamFile:
        line_number = 0
        for csv_line in csv_streamFile:
            line_number += 1
            csv_streamFile_counted = LineCounter(csv_streamFile)
            csv_line = csv_line.strip()
            progress.set_ratio(float(line_number)/number_of_lines)

            lineArr = [v[1:-1] if len(v) and v[0]=="'" and v[-1]=="'" else v
                    for v in csv_line.strip().split(';')]
            if lineArr[0].lower() == 'project':
                csv_kword = str(lineArr[1].lower())
                log_info("import "+csv_line, infoform)
                try:
                    parser = "parse_" + csv_kword
                    multi_parser = "multi_parse_" + csv_kword
                    if parser in globals():
                        globals()[parser](lineArr)
                    elif multi_parser in globals():
                        globals()[multi_parser](lineArr, csv_streamFile_counted,line_number)
                    else:
                        logger.warning("\n{f}:{n} {l} unknown keyword: {k}".format(f=filename, n=line_number, l=csv_line, k=csv_kword))
                    pj.commit()
                except Exception as error:
                    logger.error("\n{f}:{n}\n{l}".format(f=filename, n=line_number, l=u''.join(csv_line).strip()))
                    project.rollback()
                    raise
            elif lineArr[0].lower() == 'modele':
                pass
            elif csv_line[0:2] == ';;':
                logger.warning("\n{f}:{n} {l} useless line".format(f=filename, n=line_number, l=csv_line))
            elif csv_line == '':
                logger.warning("\n{f}:{n} {l} empty line".format(f=filename, n=line_number, l=csv_line))
            else:
                project.rollback()
                raise Exception(u"unrecognised line {f}:{n} {l}".format(f=filename, n=line_number, l=csv_line))
            line_number += csv_streamFile_counted.count
        log_info("saving changes", infoform)
        pj.commit()
    progress.set_ratio(1)
    project.log.clear_progress()
