# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QApplication, QTreeWidgetItem, QTableWidgetItem, QItemDelegate, QLineEdit
from qgis.PyQt.QtCore import Qt, pyqtSignal
from hydra.gui.base_dialog import BaseDialog, tr
from hydra.gui.serie_run import SerieRun
from hydra.gui.scn_widgets.serie_comput_options import SerieComputOptionsWidget
from hydra.gui.scn_widgets.serie_settings import SerieSettingsWidget
from hydra.gui.scn_widgets.serie_hydrology_settings import SerieHydrologySettingsWidget
from hydra.gui.scn_widgets.serie_model_ordering import SerieModelOrderingWidget
from hydra.gui.scn_widgets.serie_transport import SerieTransportWidget
from hydra.gui.scn_widgets.serie_regulation_and_configuration import SerieRegulationConfigurationWidget
from hydra.utility.string import normalized_name


class SerieManager(BaseDialog):

    serie_changed_signal = pyqtSignal()

    class DelegateLineEdit(QItemDelegate):
        def __init__(self, parent=None, maxlen=24):
            QItemDelegate.__init__(self, parent)
            self.maxlen = maxlen

        def createEditor(self, parent, option, index):
            line_edit = QLineEdit(parent)
            line_edit.setMaxLength(self.maxlen)
            return line_edit

    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "serie_manager.ui"), self)
        self.__project = project
        self.setWindowFlags(self.windowFlags() |
                              Qt.WindowSystemMenuHint |
                              Qt.WindowMinMaxButtonsHint)
        self.setWindowState(Qt.WindowMaximized)

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.btn_add_serie.clicked.connect(self.add_serie)
        self.btn_add_bloc.clicked.connect(self.__add_bloc)
        self.btn_remove.clicked.connect(self.delete_serie)
        self.buttonRun.clicked.connect(self.run)

        self.__reloading = False
        self.active_serie = None
        self.active_bloc = None

        self.serie_tree.itemSelectionChanged.connect(self.selection_changed)
        self.serie_tree.itemDoubleClicked.connect(self.edit_serie)
        self.serie_tree.itemChanged.connect(self.save_serie_item)
        self.setDelegate("LineEdit", {'maxlen': 7})

        self.comput_options = SerieComputOptionsWidget(project, self.comput_options_placeholder)
        self.comput_settings = SerieSettingsWidget(project, self.serie_settings_placeholder)
        self.hydrology_settings = SerieHydrologySettingsWidget(project, self.hydrology_settings_placeholder)
        self.model_ordering = SerieModelOrderingWidget(project, self.model_ordering_placeholder)
        self.transport = SerieTransportWidget(project, self.transport_placeholder)
        self.regulation_and_configuration = SerieRegulationConfigurationWidget(project, self.regulation_and_configuration_placeholder)

        self.serie_changed_signal.connect(self.changed_serie)
        self.btn_copy_data_scn.clicked.connect(self.copy_data_scn)

        self.refresh_serie_tree()
        self.serie_tree.setCurrentItem(self.serie_tree.topLevelItem(0))

    def setDelegate(self, delegate, argmap={}):
        if delegate == "LineEdit":
            self.serie_tree.setItemDelegate(SerieManager.DelegateLineEdit(parent=None, **argmap))

    def selection_changed(self):
        if not self.__reloading :
            current_item = self.serie_tree.selectedItems()[0]
            if current_item.parent() is None :
                self.active_serie = [current_item.text(0), current_item.text(1)]
                self.active_bloc = None
            else :
                self.active_serie = [current_item.parent().text(0), current_item.parent().text(1)]
                self.active_bloc = [current_item.text(0), current_item.text(1)]
            self.serie_changed_signal.emit()


    def edit_serie(self, item, column):
        if column == 1:
            self.serie_tree.editItem(item, column)

    def save_serie_item(self, item, column):
        if column == 1:
            id, name = item.text(0), item.text(1)
            if item.parent() is None:
                # editing a serie
                self.__project.execute("""update project.serie set name='{}' where id={};""".format(normalized_name(name, max_length=8, no_caps=False), id))
            else:
                # editing a bloc
                self.__project.execute("""update project.serie_bloc set name='{}' where id={};""".format(normalized_name(name, max_length=7, no_caps=False), id))

    def refresh_scn_table(self):
        self.scn_bloc_table.clear()
        self.scn_bloc_table.setColumnCount(3)
        self.scn_bloc_table.setHorizontalHeaderLabels(["Scenario", "Start date", "Duration(days)"])
        if self.active_bloc :
            self.active_scn = [[name, date0, tfin] for(name, date0 ,tfin) in self.__project.execute("""select name, date0, tfin_hr from project.serie_scenario where id_b = {id_b} order by date0""".format(id_b=self.active_bloc[0])).fetchall()]
            nb_scn = len(self.active_scn)
            self.active_scn_label.setText("Current bloc scenarios: " + str(nb_scn))
        elif self.active_serie :
            self.active_scn = [[name, date0, tfin] for(name, date0, tfin) in self.__project.execute("""select name, date0, tfin_hr from project.serie_scenario where id_cs = {id_cs} order by date0""".format(id_cs=self.active_serie[0])).fetchall()]
            nb_scn = len(self.active_scn)
            self.active_scn_label.setText("Current time serie scenarios: " + str(nb_scn))

        self.scn_bloc_table.setRowCount(nb_scn)
        for i in range(nb_scn) :
            self.scn_bloc_table.setItem(i,0,QTableWidgetItem(str(self.active_scn[i][0])))
            self.scn_bloc_table.setItem(i,1,QTableWidgetItem(str(self.active_scn[i][1])))
            self.scn_bloc_table.setItem(i,2,QTableWidgetItem(str(self.active_scn[i][2].days)))

    def refresh_serie_tree(self):
        self.__reloading = True

        self.serie_tree.clear()

        for serie in self.__project.execute("""select id, name from project.serie order by id asc;""").fetchall():
            serie_node = QTreeWidgetItem(self.serie_tree)
            serie_node.setFlags(serie_node.flags() | Qt.ItemIsEditable)
            serie_node.setExpanded(True)
            serie_node.setText(0, str(serie[0]))
            serie_node.setText(1, serie[1])
            for bloc in self.__project.execute("""select id, id_cs, name from project.serie_bloc order by t_till_start asc;""").fetchall():
                if bloc[1] == serie[0] :
                    children_node = QTreeWidgetItem(serie_node)
                    children_node.setFlags(children_node.flags() | Qt.ItemIsEditable)
                    children_node.setExpanded(True)
                    children_node.setText(0,tr(str(bloc[0])))
                    children_node.setText(1,tr(bloc[2]))

        self.__reloading = False

    def find_bloc_time(self, id_serie) :
        t_bloc_max = self.__project.execute("""select max(t_till_start) from project.serie_bloc where id_cs = {id_cs}""".format(id_cs = id_serie)).fetchone()

        t_fin_serie = self.__project.execute("""select tfin from project.serie where id = {id}""".format(id = id_serie)).fetchone()[0]

        t_bloc_max = t_bloc_max[0] if t_bloc_max is not None else None

        if t_bloc_max == None :
            return("'0 days'")
        else :
            return("'"+str(((t_fin_serie+t_bloc_max)/2).days)+" days'")

    def add_serie(self):
        new_serie_name, = self.__project.execute("""insert into project.serie default values returning name""").fetchone()
        self.refresh_serie_tree()
        self.select_serie_by_name(new_serie_name)
        self.__add_bloc()
        self.select_serie_by_name(new_serie_name)

    def __add_bloc(self):
        if self.active_serie is not None :
            t_till_start = self.find_bloc_time(self.active_serie[0])
            new_bloc_name,  = self.__project.execute("""insert into project.serie_bloc (id_cs,t_till_start) values ({id_cs},{time}) returning name""".format(id_cs = self.active_serie[0], time = t_till_start)).fetchone()
            self.refresh_serie_tree()
            self.__select_bloc_by_name(new_bloc_name)

    def delete_serie(self) :
        if self.active_bloc is not None :
            self.__project.execute("""delete from project.serie_bloc where id = {id} """.format(id = self.active_bloc[0]))
        elif self.active_serie is not None :
            self.__project.execute("""delete from project.serie where id = {id} """.format(id = self.active_serie[0]))
        self.refresh_serie_tree()

    def changed_serie(self) :
        self.comput_options.save()
        self.comput_settings.save()
        self.hydrology_settings.save()
        self.transport.save()
        self.model_ordering.save()
        self.regulation_and_configuration.save()

        self.labelSelectedSerie.setText(self.active_serie[1])
        self.comput_settings.set_serie(self.active_serie[0])
        if self.active_bloc :
            self.comput_options.set_bloc_serie(self.active_bloc[0], self.active_serie[0])
            self.comput_settings.set_bloc(self.active_bloc[0])
            self.hydrology_settings.set_bloc_serie(self.active_bloc[0], self.active_serie[0])
            self.regulation_and_configuration.set_bloc_serie(self.active_bloc[0], self.active_serie[0])

            self.comput_settings.setEnabled(True)
            self.hydrology_settings.setEnabled(True)
            self.regulation_and_configuration.setEnabled(True)
            self.transport.setEnabled(False)
            self.comput_options.setEnabled(False)
            self.model_ordering.setEnabled(False)

        elif self.active_serie :
            self.comput_options.set_bloc_serie(self.active_bloc, self.active_serie[0])
            self.comput_settings.set_bloc(self.active_bloc)
            self.hydrology_settings.set_bloc_serie(self.active_bloc, self.active_serie[0])
            self.transport.set_serie(self.active_serie[0])
            self.model_ordering.set_serie(self.active_serie[0])
            self.regulation_and_configuration.set_bloc_serie(self.active_bloc, self.active_serie[0])

            self.comput_options.setEnabled(False)
            self.comput_settings.setEnabled(True)
            self.hydrology_settings.setEnabled(False)
            self.model_ordering.setEnabled(True)
            self.transport.setEnabled(True)
            self.comput_options.setEnabled(True)
        else :
            self.comput_options.set_bloc_serie(self.active_bloc, self.active_serie)
            self.comput_settings.set_serie(self.active_serie)
            self.comput_settings.set_bloc(self.active_bloc)
            self.hydrology_settings.set_bloc(self.active_bloc, self.active_serie)
            self.transport.set_serie(self.active_serie)
            self.model_ordering.set_serie(self.active_serie)
            self.regulation_and_configuration.set_bloc_serie(self.active_bloc, self.active_serie)

            self.comput_options.setEnabled(True)
            self.comput_settings.setEnabled(False)
            self.hydrology_settings.setEnabled(False)
            self.model_ordering.setEnabled(False)
            self.transport.setEnabled(False)

        self.refresh_scn_table()

    def run(self, mute = False):
        # run the serie
        if not self.active_serie is None:
            self.comput_options.save()
            self.comput_settings.save()
            self.hydrology_settings.save()
            self.transport.save()
            self.model_ordering.save()
            self.regulation_and_configuration.save()
            if self.active_serie is None:
                return

            self.serie_run = SerieRun(self.__project, self.active_serie[0], parent=self)
            self.serie_run.exec_()

    def select_serie_by_name(self, serie_name):
        for i in range(0, self.serie_tree.topLevelItemCount()):
            if self.serie_tree.topLevelItem(i).text(1)==serie_name:
                self.serie_tree.setCurrentItem(self.serie_tree.topLevelItem(i))

    def __select_bloc_by_name(self, bloc_name):
        for i in range(0, self.serie_tree.topLevelItemCount()):
            serie_tree_item = self.serie_tree.topLevelItem(i)
            for j in range(0, serie_tree_item.childCount()):
                bloc_tree_item = serie_tree_item.child(j)
                if bloc_tree_item.text(1) == bloc_name:
                    self.serie_tree.setCurrentItem(bloc_tree_item)

    def copy_data_scn(self):
        ''' fonction to write and add in clipboard data for extract scn in serie '''

        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard)

        dt_time = str(round(self.comput_settings.dt_output_hr.get_time().total_seconds() / 3600., 4))

        text = "$SET \nSCN ; AAAA ; MM ; JJ ; DUREE ; PASDETEMPS;\n"
        for i in range(self.scn_bloc_table.rowCount()):
            date = self.scn_bloc_table.item(i,1).text()
            year = date[0:4]
            month = date[5:7]
            days = date[8:10]
            time = str(int(self.scn_bloc_table.item(i,2).text()) * 24)
            row = " ; ".join([self.scn_bloc_table.item(i,0).text(), year, month, days, time, dt_time, "\n"])
            text += row

        cb.setText(text, mode=cb.Clipboard)

    def save(self):
        self.comput_options.save()
        self.comput_settings.save()
        self.hydrology_settings.save()
        self.transport.save()
        self.model_ordering.save()
        self.regulation_and_configuration.save()
        BaseDialog.save(self)
        self.close()