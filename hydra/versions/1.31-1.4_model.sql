/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update $model.metadata set version = '1.4';
;;

/* ********************************************** */
/*  Config delete on project.config_scenario      */
/* ********************************************** */

create or replace function ${model}.config_before_delete_fct()
returns trigger
language plpgsql
as $$$$
    begin
        update $model._node set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._node set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;
        update $model._link set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._link set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;
        update $model._singularity set configuration=null where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)=1;
        update $model._singularity set configuration=configuration::jsonb-old.name where configuration is not null and array_length(array((select json_object_keys((configuration::jsonb-old.name)::json))),1)>1;

        delete from project.config_scenario where model='$model' and configuration=old.id;

        return old;
    end;
$$$$
;;

/* ********************************************** */
/*  River cross section view update               */
/* ********************************************** */

drop view if exists $model.river_cross_section_profile cascade;
;;

create view $model.river_cross_section_profile as
    select g.id,
        c.name,
        c.z_invert_up,
        c.z_invert_down,
        c.type_cross_section_up,
        c.type_cross_section_down,
        c.up_rk,
        c.up_rk_maj,
        c.up_sinuosity,
        c.up_circular_diameter,
        c.up_ovoid_height,
        c.up_ovoid_top_diameter,
        c.up_ovoid_invert_diameter,
        c.up_cp_geom,
        c.up_op_geom,
        c.up_vcs_geom,
        c.up_vcs_topo_geom,
        c.down_rk,
        c.down_rk_maj,
        c.down_sinuosity,
        c.down_circular_diameter,
        c.down_ovoid_height,
        c.down_ovoid_top_diameter,
        c.down_ovoid_invert_diameter,
        c.down_cp_geom,
        c.down_op_geom,
        c.down_vcs_geom,
        c.down_vcs_topo_geom,
        c.configuration,
        (select case when (c.type_cross_section_up='channel' or c.type_cross_section_up='valley') then
            (select (c.z_invert_up))
        else (select st_z(st_startpoint(g.geom)))
        end) as z_tn_up,
        (select case when (c.type_cross_section_down='channel' or c.type_cross_section_down='valley') then
            (select (c.z_invert_down))
        else (select st_z(st_endpoint(g.geom)))
        end) as z_tn_down,
        (select case when c.type_cross_section_up='circular' then
            (select (c.z_invert_up+c.up_circular_diameter))
        when c.type_cross_section_up='ovoid' then
            (select (c.z_invert_up+c.up_ovoid_height))
        when c.type_cross_section_up='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.up_cp_geom = cpg.id)
            (select (c.z_invert_up + n.height) from n))
        when c.type_cross_section_up='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.up_op_geom = opg.id)
            (select (c.z_invert_up + n.height) from n))
        when c.type_cross_section_up='valley' then
            (with n as (select (zbmaj_lbank_array[1][1] - zbmin_array[1][1]) as height
                from ${model}.valley_cross_section_geometry opg
                where c.up_vcs_geom = opg.id)
            (select (c.z_invert_up + n.height) from n))
        else null
        end) as z_lbank_up,
        (select case when c.type_cross_section_down='circular' then
            (select (c.z_invert_down+c.down_circular_diameter))
        when c.type_cross_section_down='ovoid' then
            (select (c.z_invert_down+c.down_ovoid_height))
        when c.type_cross_section_down='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.down_cp_geom = cpg.id)
            (select (c.z_invert_down + n.height) from n))
        when c.type_cross_section_down='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.down_op_geom = opg.id)
            (select (c.z_invert_down + n.height) from n))
        when c.type_cross_section_down='valley' then
            (with n as (select (zbmaj_lbank_array[1][1] - zbmin_array[1][1]) as height
                from ${model}.valley_cross_section_geometry opg
                where c.down_vcs_geom = opg.id)
            (select (c.z_invert_down + n.height) from n))
        else null
        end) as z_lbank_down,
        (select case when c.type_cross_section_up='circular' then
            (select (c.z_invert_up+c.up_circular_diameter))
        when c.type_cross_section_up='ovoid' then
            (select (c.z_invert_up+c.up_ovoid_height))
        when c.type_cross_section_up='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.up_cp_geom = cpg.id)
            (select (c.z_invert_up + n.height) from n))
        when c.type_cross_section_up='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.up_op_geom = opg.id)
            (select (c.z_invert_up + n.height) from n))
        when c.type_cross_section_up='valley' then
            (with n as (select (zbmaj_rbank_array[1][1] - zbmin_array[1][1]) as height
                from ${model}.valley_cross_section_geometry opg
                where c.up_vcs_geom = opg.id)
            (select (c.z_invert_up + n.height) from n))
        else null
        end) as z_rbank_up,
        (select case when c.type_cross_section_down='circular' then
            (select (c.z_invert_down+c.down_circular_diameter))
        when c.type_cross_section_down='ovoid' then
            (select (c.z_invert_down+c.down_ovoid_height))
        when c.type_cross_section_down='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.down_cp_geom = cpg.id)
            (select (c.z_invert_down + n.height) from n))
        when c.type_cross_section_down='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.down_op_geom = opg.id)
            (select (c.z_invert_down + n.height) from n))
        when c.type_cross_section_down='valley' then
            (with n as (select (zbmaj_rbank_array[1][1] - zbmin_array[1][1]) as height
                from ${model}.valley_cross_section_geometry opg
                where c.down_vcs_geom = opg.id)
            (select (c.z_invert_down + n.height) from n))
        else null
        end) as z_rbank_down,
        (select case when c.type_cross_section_down='valley' then
            null
        when c.type_cross_section_down='channel' then
            null
        else
            (with n as (select zbmin_array[array_length(zbmin_array, 1)][1] as height
                from ${model}.closed_parametric_geometry opg
                where c.down_op_geom = opg.id)
            select (c.z_invert_up + n.height) from n)
        end) as z_ceiling_up,
        (select case when c.type_cross_section_down='valley' then
            null
        when c.type_cross_section_down='channel' then
            null
        else
            (with n as (select zbmin_array[array_length(zbmin_array, 1)][1] as height
                from ${model}.closed_parametric_geometry opg
                where c.down_op_geom = opg.id)
            select (c.z_invert_down + n.height) from n)
        end) as z_ceiling_down,
        c.validity,
        g.geom
    from $model._river_cross_section_profile as c, $model._node as g
    where g.id = c.id
;;

-- TRIGGERS

-- Insert & Update

create or replace function ${model}.river_cross_section_profile_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        select (
            (new.type_cross_section_up is null or (
                new.z_invert_up is not null
                and new.up_rk is not null
                and (new.type_cross_section_up!='circular' or (new.up_circular_diameter is not null and new.up_circular_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_top_diameter is not null and new.up_ovoid_top_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_invert_diameter is not null and new.up_ovoid_invert_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_height is not null and new.up_ovoid_height >0 ))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_height > (new.up_ovoid_top_diameter+new.up_ovoid_invert_diameter)/2))
                and (new.type_cross_section_up!='pipe' or new.up_cp_geom is not null)
                and (new.type_cross_section_up!='channel' or new.up_op_geom is not null)
                and (new.type_cross_section_up!='valley'
                        or (new.up_rk = 0 and new.up_rk_maj = 0 and new.up_sinuosity = 0)
                        or (new.up_rk >0 and new.up_rk_maj is not null and new.up_rk_maj > 0 and new.up_sinuosity is not null and new.up_sinuosity > 0))
                and (new.type_cross_section_up!='valley' or new.up_vcs_geom is not null)
                )
            )
            and
            (new.type_cross_section_down is null or (
                new.z_invert_down is not null
                and new.down_rk is not null
                and (new.type_cross_section_down!='circular' or (new.down_circular_diameter is not null and new.down_circular_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_top_diameter is not null and new.down_ovoid_top_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_invert_diameter is not null and new.down_ovoid_invert_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_height is not null and new.down_ovoid_height >0 ))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_height > (new.down_ovoid_top_diameter+new.down_ovoid_invert_diameter)/2))
                and (new.type_cross_section_down!='pipe' or new.down_cp_geom is not null)
                and (new.type_cross_section_down!='channel' or new.down_op_geom is not null)
                and (new.type_cross_section_down!='valley'
                        or (new.down_rk = 0 and new.down_rk_maj = 0 and new.down_sinuosity = 0)
                        or (new.down_rk >0 and new.down_rk_maj is not null and new.down_rk_maj > 0 and new.down_sinuosity is not null and new.down_sinuosity > 0))
                and (new.type_cross_section_down!='valley' or new.down_vcs_geom is not null)
                )
            )
            and
            (new.type_cross_section_up is null or new.type_cross_section_down is null or (
                new.z_invert_up is not null and new.z_invert_down is not null
                )
            )
        ) into new.validity;

        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id from $model.river_node where ST_DWithin(new.geom, geom, .1) into new.id;
            select coalesce(new.name, 'CP'||new.id::varchar) into new.name;
            if new.id is null then
                raise exception 'no river node nearby river_cross_section_profile %', st_astext(new.geom);
            end if;

            insert into $model._river_cross_section_profile (
                    id,
                    name,
                    z_invert_up,
                    z_invert_down,
                    type_cross_section_up,
                    type_cross_section_down,
                    up_rk,
                    up_rk_maj,
                    up_sinuosity,
                    up_circular_diameter,
                    up_ovoid_height,
                    up_ovoid_top_diameter,
                    up_ovoid_invert_diameter,
                    up_cp_geom,
                    up_op_geom,
                    up_vcs_geom,
                    up_vcs_topo_geom,
                    down_rk,
                    down_rk_maj,
                    down_sinuosity,
                    down_circular_diameter,
                    down_ovoid_height,
                    down_ovoid_top_diameter,
                    down_ovoid_invert_diameter,
                    down_cp_geom,
                    down_op_geom,
                    down_vcs_geom,
                    down_vcs_topo_geom,
                    configuration,
                    validity
                )
                values (
                    new.id,
                    new.name,
                    new.z_invert_up,
                    new.z_invert_down,
                    new.type_cross_section_up,
                    new.type_cross_section_down,
                    new.up_rk,
                    new.up_rk_maj,
                    new.up_sinuosity,
                    new.up_circular_diameter,
                    new.up_ovoid_height,
                    new.up_ovoid_top_diameter,
                    new.up_ovoid_invert_diameter,
                    new.up_cp_geom,
                    new.up_op_geom,
                    new.up_vcs_geom,
                    new.up_vcs_topo_geom,
                    new.down_rk,
                    new.down_rk_maj,
                    new.down_sinuosity,
                    new.down_circular_diameter,
                    new.down_ovoid_height,
                    new.down_ovoid_top_diameter,
                    new.down_ovoid_invert_diameter,
                    new.down_cp_geom,
                    new.down_op_geom,
                    new.down_vcs_geom,
                    new.down_vcs_topo_geom,
                    new.configuration::json,
                    new.validity
                );

            perform $model.add_configuration_fct(new.configuration::json, new.id, 'river_cross_section_profile');

            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id from $model.river_node where ST_DWithin(new.geom, geom, .1) into new.id;
            select coalesce(new.name, 'CP'||new.id::varchar) into new.name;
            if new.id is null then
                raise exception 'no river node nearby river_cross_section_profile %', st_astext(new.geom);
            end if;

            -- Handle configurations
            if ((new.z_invert_up, new.z_invert_down,
                    new.type_cross_section_up, new.type_cross_section_down,
                    new.up_rk, new.up_rk_maj, new.up_sinuosity, new.up_circular_diameter,
                    new.up_ovoid_height, new.up_ovoid_top_diameter, new.up_ovoid_invert_diameter,
                    new.up_cp_geom, new.up_op_geom, new.up_vcs_geom, new.up_vcs_topo_geom,
                    new.down_rk, new.down_rk_maj, new.down_sinuosity, new.down_circular_diameter,
                    new.down_ovoid_height, new.down_ovoid_top_diameter, new.down_ovoid_invert_diameter,
                    new.down_cp_geom, new.down_op_geom, new.down_vcs_geom, new.down_vcs_topo_geom)
            is distinct from (old.z_invert_up, old.z_invert_down,
                    old.type_cross_section_up, old.type_cross_section_down,
                    old.up_rk, old.up_rk_maj, old.up_sinuosity, old.up_circular_diameter,
                    old.up_ovoid_height, old.up_ovoid_top_diameter, old.up_ovoid_invert_diameter,
                    old.up_cp_geom, old.up_op_geom, old.up_vcs_geom, old.up_vcs_topo_geom,
                    old.down_rk, old.down_rk_maj, old.down_sinuosity, old.down_circular_diameter,
                    old.down_ovoid_height, old.down_ovoid_top_diameter, old.down_ovoid_invert_diameter,
                    old.down_cp_geom, old.down_op_geom, old.down_vcs_geom, old.down_vcs_topo_geom))
            then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}'
                            from (select old.z_invert_up, old.z_invert_down,
                                old.type_cross_section_up, old.type_cross_section_down,
                                old.up_rk, old.up_rk_maj, old.up_sinuosity, old.up_circular_diameter,
                                old.up_ovoid_height, old.up_ovoid_top_diameter, old.up_ovoid_invert_diameter,
                                old.up_cp_geom, old.up_op_geom, old.up_vcs_geom, old.up_vcs_topo_geom,
                                old.down_rk, old.down_rk_maj, old.down_sinuosity, old.down_circular_diameter,
                                old.down_ovoid_height, old.down_ovoid_top_diameter, old.down_ovoid_invert_diameter,
                                old.down_cp_geom, old.down_op_geom, old.down_vcs_geom, old.down_vcs_topo_geom) as o,
                                (select new.z_invert_up, new.z_invert_down,
                                new.type_cross_section_up, new.type_cross_section_down,
                                new.up_rk, new.up_rk_maj, new.up_sinuosity, new.up_circular_diameter,
                                new.up_ovoid_height, new.up_ovoid_top_diameter, new.up_ovoid_invert_diameter,
                                new.up_cp_geom, new.up_op_geom, new.up_vcs_geom, new.up_vcs_topo_geom,
                                new.down_rk, new.down_rk_maj, new.down_sinuosity, new.down_circular_diameter,
                                new.down_ovoid_height, new.down_ovoid_top_diameter, new.down_ovoid_invert_diameter,
                                new.down_cp_geom, new.down_op_geom, new.down_vcs_geom, new.down_vcs_topo_geom) as n
                            into new_config;
                        update $model._river_cross_section_profile set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}'
                            from (select new.z_invert_up, new.z_invert_down,
                                new.type_cross_section_up, new.type_cross_section_down,
                                new.up_rk, new.up_rk_maj, new.up_sinuosity, new.up_circular_diameter,
                                new.up_ovoid_height, new.up_ovoid_top_diameter, new.up_ovoid_invert_diameter,
                                new.up_cp_geom, new.up_op_geom, new.up_vcs_geom, new.up_vcs_topo_geom,
                                new.down_rk, new.down_rk_maj, new.down_sinuosity, new.down_circular_diameter,
                                new.down_ovoid_height, new.down_ovoid_top_diameter, new.down_ovoid_invert_diameter,
                                new.down_cp_geom, new.down_op_geom, new.down_vcs_geom, new.down_vcs_topo_geom) n
                            into new_config;
                        update $model._river_cross_section_profile set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._river_cross_section_profile set
                id=new.id,
                name=new.name,
                z_invert_up=new.z_invert_up,
                z_invert_down=new.z_invert_down,
                type_cross_section_up=new.type_cross_section_up,
                type_cross_section_down=new.type_cross_section_down,
                up_rk=new.up_rk,
                up_rk_maj=new.up_rk_maj,
                up_sinuosity=new.up_sinuosity,
                up_circular_diameter=new.up_circular_diameter,
                up_ovoid_height=new.up_ovoid_height,
                up_ovoid_top_diameter=new.up_ovoid_top_diameter,
                up_ovoid_invert_diameter=new.up_ovoid_invert_diameter,
                up_cp_geom=new.up_cp_geom,
                up_op_geom=new.up_op_geom,
                up_vcs_geom=new.up_vcs_geom,
                up_vcs_topo_geom=new.up_vcs_topo_geom,
                down_rk=new.down_rk,
                down_rk_maj=new.down_rk_maj,
                down_sinuosity=new.down_sinuosity,
                down_circular_diameter=new.down_circular_diameter,
                down_ovoid_height=new.down_ovoid_height,
                down_ovoid_top_diameter=new.down_ovoid_top_diameter,
                down_ovoid_invert_diameter=new.down_ovoid_invert_diameter,
                down_cp_geom=new.down_cp_geom,
                down_op_geom=new.down_op_geom,
                down_vcs_geom=new.down_vcs_geom,
                down_vcs_topo_geom=new.down_vcs_topo_geom,
                validity=new.validity
            where id=old.id;

            perform $model.add_configuration_fct(new.configuration::json, new.id, 'river_cross_section_profile');

            return new;
        end if;
    end;
$$$$
;;

drop trigger if exists ${model}_river_cross_section_profile_trig on $model.river_cross_section_profile;
;;

create trigger ${model}_river_cross_section_profile_trig
    instead of insert or update on $model.river_cross_section_profile
       for each row execute procedure ${model}.river_cross_section_profile_fct()
;;

-- Delete

create or replace function ${model}.river_cross_section_profile_del_fct()
returns trigger
language plpgsql
as $$$$
    begin
        delete from $model._river_cross_section_profile where id=old.id;
        return old;
    end;
$$$$
;;

drop trigger if exists ${model}_river_cross_section_profile_del_trig on $model.river_cross_section_profile;
;;

create trigger ${model}_river_cross_section_profile_del_trig
    instead of delete on $model.river_cross_section_profile
       for each row execute procedure ${model}.river_cross_section_profile_del_fct()
;;

-- re-creates views dropped at cascade

create or replace view ${model}.open_reach
as with
    ends as (
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'valley' and p.type_cross_section_down != 'valley' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up != 'valley' and p.type_cross_section_down = 'valley' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'valley' and st_dwithin(p.geom, r.geom, .1) and st_linelocatepoint(r.geom, p.geom) > .99
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_down = 'valley' and st_dwithin(p.geom, r.geom, .1) and st_linelocatepoint(r.geom, p.geom) < .01
    )
    ,
    bounds as (
        select FIRST_VALUE(reach) over (partition by reach order by alpha), typ, LAG(alpha) over (partition by reach order by alpha) as start, alpha as end, id, reach from ends
    )
    select row_number() over() as id, st_linesubstring(r.geom, b.start, b.end) as geom, r.id as reach from bounds as b join ${model}.reach as r on r.id=b.reach where b.typ = 'down'
;;

create or replace view ${model}.channel_reach
as with
    ends as (
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'channel' and p.type_cross_section_down != 'channel' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up != 'channel' and p.type_cross_section_down = 'channel' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'channel' and st_dwithin(p.geom, r.geom, .1) and st_linelocatepoint(r.geom, p.geom) > .99
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_down = 'channel' and st_dwithin(p.geom, r.geom, .1) and st_linelocatepoint(r.geom, p.geom) < .01
    )
    ,
    bounds as (
        select FIRST_VALUE(reach) over (partition by reach order by alpha), typ, LAG(alpha) over (partition by reach order by alpha) as start, alpha as end, id, reach from ends
    )
    select row_number() over() as id, st_linesubstring(r.geom, b.start, b.end) as geom, r.id as reach from bounds as b join ${model}.reach as r on r.id=b.reach where b.typ = 'down'
;;

/* ********************************************** */
/*  Manhole node connectivity updt                */
/* ********************************************** */


alter table $model._manhole_node add column if not exists cover_diameter real default 0.1;
alter table $model._manhole_node add column if not exists cover_critical_pressure real default 2.0;
alter table $model._manhole_node add column if not exists inlet_width real default 1.0;
alter table $model._manhole_node add column if not exists inlet_height real default 0.25;
alter table $model._manhole_node add column if not exists connection_law hydra_manhole_connection_type default 'manhole_cover';
;;

drop view if exists $model.manhole_node cascade;
;;

create or replace view $model.manhole_node as
    select p.id,
        p.name,
        c.area,
        c.z_ground,
        c.cover_diameter,
        c.cover_critical_pressure,
        c.inlet_width,
        c.inlet_height,
        c.connection_law,
        p.geom,
        p.configuration::character varying as configuration,
        p.generated,
        p.validity,
        p.configuration as configuration_json
    from $model._manhole_node c,
        $model._node p
  where p.id = c.id;
;;

create or replace function ${model}.manhole_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('manhole', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'manhole') where name = 'define_later' and id = id_;
            insert into $model._manhole_node
                values (id_, 'manhole', coalesce(new.area, 1), coalesce(new.z_ground, (select project.altitude(new.geom))), coalesce(new.cover_diameter, 0.1), coalesce(new.cover_critical_pressure, 2.0), coalesce(new.inlet_width, 1.0), coalesce(new.inlet_height, 0.25), coalesce(new.connection_law, 'manhole_cover'));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'manhole_node');

            -- Lines to update specific nodes that works with associated contours
            if 'manhole' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'manhole' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (connection_law is not null) and (connection_law != 'manhole_cover' or cover_diameter is not null) and (connection_law != 'manhole_cover' or cover_diameter > 0) and (connection_law != 'manhole_cover' or cover_critical_pressure is not null) and (connection_law != 'manhole_cover' or cover_critical_pressure >0) and (connection_law != 'drainage_inlet' or inlet_width is not null) and (connection_law != 'drainage_inlet' or inlet_width >0) and (connection_law != 'drainage_inlet' or inlet_height is not null) and (connection_law != 'drainage_inlet' or inlet_height >0) and (area is not null) and (area>0) and (z_ground is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) and ((select exists (select 1 from $model.pipe_link where up=new.id or down=new.id))) from  $model._manhole_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area, new.z_ground, new.cover_diameter, new.cover_critical_pressure, new.inlet_width, new.inlet_height, new.connection_law) is distinct from (old.area, old.z_ground, old.cover_diameter, old.cover_critical_pressure, old.inlet_width, old.inlet_height, old.connection_law)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.z_ground, old.cover_diameter, old.cover_critical_pressure, old.inlet_width, old.inlet_height, old.connection_law) as o, (select new.area, new.z_ground, new.cover_diameter, new.cover_critical_pressure, new.inlet_width, new.inlet_height, new.connection_law) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.z_ground, new.cover_diameter, new.cover_critical_pressure, new.inlet_width, new.inlet_height, new.connection_law) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._manhole_node set area=new.area, z_ground=new.z_ground, cover_diameter=new.cover_diameter, cover_critical_pressure=new.cover_critical_pressure, inlet_width=new.inlet_width, inlet_height=new.inlet_height, connection_law=new.connection_law where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'manhole_node');

            -- Lines to update specific nodes that works with associated contours
            if 'manhole' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'manhole' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'manhole' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (connection_law is not null) and (connection_law != 'manhole_cover' or cover_diameter is not null) and (connection_law != 'manhole_cover' or cover_diameter > 0) and (connection_law != 'manhole_cover' or cover_critical_pressure is not null) and (connection_law != 'manhole_cover' or cover_critical_pressure >0) and (connection_law != 'drainage_inlet' or inlet_width is not null) and (connection_law != 'drainage_inlet' or inlet_width >0) and (connection_law != 'drainage_inlet' or inlet_height is not null) and (connection_law != 'drainage_inlet' or inlet_height >0) and (area is not null) and (area>0) and (z_ground is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) and ((select exists (select 1 from $model.pipe_link where up=new.id or down=new.id))) from  $model._manhole_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'manhole' = 'storage' and (select trigger_coverage from $model.metadata) then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'manhole' = 'crossroad' and (select trigger_street_link from $model.metadata) then
                perform $model.update_street_links();
            end if;

            delete from $model._manhole_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

drop trigger if exists ${model}_manhole_node_trig on ${model}.manhole_node;
;;

create trigger ${model}_manhole_node_trig
    instead of insert or update or delete on ${model}.manhole_node
    for each row execute procedure ${model}.manhole_node_fct();
;;

drop function if exists ${model}.check_connected_pipes(id);
;;

drop view if exists $model.connectivity_manhole;
;;

drop view if exists $model.connectivity_manhole_hydrology;
;;

drop function if exists ${model}.check_extremities_nodes(geom);
;;

drop function if exists ${model}.check_hydrogramme_apport_on_down_route_fct();
;;

drop function if exists ${model}.check_catchment_has_one_route_fct();
;;

/* ********************************************** */
/*  Routing/connector hydrology down on crossroad */
/* ********************************************** */

create or replace function ${model}.routing_hydrology_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('routing_hydrology', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'routing_hydrology') where name = 'define_later' and id = id_;
            insert into $model._routing_hydrology_link
                values (id_, 'routing_hydrology', coalesce(new.cross_section, 1), coalesce(new.length, 0.1), coalesce(new.slope, .01), coalesce(new.hydrograph, (select hbc.id from $model.hydrograph_bc_singularity as hbc where ST_DWithin(ST_EndPoint(new.geom), hbc.geom, .1))));
            if 'routing_hydrology' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'routing_hydrology_link');
            update $model._link set validity = (select (cross_section is not null) and (cross_section>=0) and (length is not null) and (length>=0) and (slope is not null) and (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station', 'crossroad', 'storage') and (hydrograph is not null))) from  $model._routing_hydrology_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'routing_hydrology' = 'pipe' then
                update $model.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.cross_section, new.length, new.slope) is distinct from (old.cross_section, old.length, old.slope)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.cross_section, old.length, old.slope) as o, (select new.cross_section, new.length, new.slope) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.cross_section, new.length, new.slope) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._routing_hydrology_link set cross_section=new.cross_section, length=new.length, slope=new.slope, hydrograph=new.hydrograph where id=old.id;

            if 'routing_hydrology' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) and (select trigger_branch from $model.metadata) then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'routing_hydrology_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'routing_hydrology' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update $model.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (cross_section is not null) and (cross_section>=0) and (length is not null) and (length>=0) and (slope is not null) and (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station', 'crossroad', 'storage') and (hydrograph is not null))) from  $model._routing_hydrology_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._routing_hydrology_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'routing_hydrology' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'routing_hydrology' = 'pipe' then
                update $model.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;

create or replace function ${model}.connector_hydrology_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('connector_hydrology', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'connector_hydrology') where name = 'define_later' and id = id_;
            insert into $model._connector_hydrology_link
                values (id_, 'connector_hydrology', coalesce(new.hydrograph, (select hbc.id from $model.hydrograph_bc_singularity as hbc where ST_DWithin(ST_EndPoint(new.geom), hbc.geom, .1))));
            if 'connector_hydrology' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'connector_hydrology_link');
            update $model._link set validity = (select (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station', 'storage', 'crossroad') and (hydrograph is not null))) from  $model._connector_hydrology_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'connector_hydrology' = 'pipe' then
                update $model.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if (('no_column') is distinct from ('no_column')) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select 'no_column') as o, (select 'no_column') as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select 'no_column') n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._connector_hydrology_link set hydrograph=new.hydrograph where id=old.id;

            if 'connector_hydrology' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) and (select trigger_branch from $model.metadata) then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'connector_hydrology_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'connector_hydrology' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update $model.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station', 'storage', 'crossroad') and (hydrograph is not null))) from  $model._connector_hydrology_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._connector_hydrology_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'connector_hydrology' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'connector_hydrology' = 'pipe' then
                update $model.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;

alter table $model._link
  drop constraint if exists ${model}_link_connectivity_check_cstr ;

alter table $model._link
  add constraint ${model}_link_connectivity_check_cstr
  check (
     link_type = 'deriv_pump'::hydra_link_type and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
  or link_type = 'connector_hydrology'::hydra_link_type and up_type = 'manhole_hydrology'::hydra_node_type and (down_type = any (array['manhole_hydrology'::hydra_node_type, 'manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'storage'::hydra_node_type, 'crossroad'::hydra_node_type]))
  or link_type = 'weir'::hydra_link_type and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
  or link_type = 'porous'::hydra_link_type and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
  or link_type = 'borda_headloss'::hydra_link_type and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
  or link_type = 'strickler'::hydra_link_type and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
  or link_type = 'fuse_spillway'::hydra_link_type and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
  or link_type = 'routing_hydrology'::hydra_link_type and up_type = 'catchment'::hydra_node_type and (down_type = any (array['manhole_hydrology'::hydra_node_type, 'manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'storage'::hydra_node_type, 'crossroad'::hydra_node_type]))
  or link_type = 'gate'::hydra_link_type and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
  or link_type = 'regul_gate'::hydra_link_type and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
  or link_type = 'overflow'::hydra_link_type and (up_type = any (array['river'::hydra_node_type, 'storage'::hydra_node_type, 'elem_2d'::hydra_node_type, 'crossroad'::hydra_node_type])) and (down_type = any (array['river'::hydra_node_type, 'storage'::hydra_node_type, 'elem_2d'::hydra_node_type, 'crossroad'::hydra_node_type]))
  or link_type = 'connector'::hydra_link_type and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
  or link_type = 'pipe'::hydra_link_type and (up_type = 'manhole_hydrology'::hydra_node_type and down_type = 'manhole_hydrology'::hydra_node_type or up_type = 'manhole_hydrology'::hydra_node_type and down_type = 'station'::hydra_node_type or up_type = 'manhole'::hydra_node_type and down_type = 'manhole'::hydra_node_type or up_type = 'manhole'::hydra_node_type and down_type = 'station'::hydra_node_type)
  or link_type = 'street'::hydra_link_type and up_type = 'crossroad'::hydra_node_type and down_type = 'crossroad'::hydra_node_type
  or link_type = 'pump'::hydra_link_type and (up_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) and (down_type <> all (array['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
  or link_type = 'network_overflow'::hydra_link_type and up_type = 'manhole'::hydra_node_type and (down_type = any (array['crossroad'::hydra_node_type, 'elem_2d'::hydra_node_type, 'storage'::hydra_node_type, 'river'::hydra_node_type]))
  or link_type = 'mesh_2d'::hydra_link_type and up_type = 'elem_2d'::hydra_node_type and down_type = 'elem_2d'::hydra_node_type
  );
;;

/* ********************************************** */
/*  Tables changed for quality/pollution          */
/* ********************************************** */

-- Catchment
drop view if exists $model.catchment_node cascade;
;;

alter table $model._catchment_node drop column if exists catchment_pollution_mode;
alter table $model._catchment_node drop column if exists catchment_pollution_param;
alter table $model._catchment_node add column if not exists network_type hydra_network_type default 'separative';
alter table $model._catchment_node add column if not exists rural_land_use real;
alter table $model._catchment_node add column if not exists industrial_land_use real;
alter table $model._catchment_node add column if not exists suburban_housing_land_use real;
alter table $model._catchment_node add column if not exists dense_housing_land_use real;
;;

create or replace view $model.catchment_node as
    select p.id,
        p.name,
        c.area_ha,
        c.rl,
        c.slope,
        c.c_imp,
        c.netflow_type,
        c.constant_runoff,
        c.horner_ini_loss_coef,
        c.horner_recharge_coef,
        c.holtan_sat_inf_rate_mmh,
        c.holtan_dry_inf_rate_mmh,
        c.holtan_soil_storage_cap_mm,
        c.scs_j_mm,
        c.scs_soil_drainage_time_day,
        c.scs_rfu_mm,
        c.permeable_soil_j_mm,
        c.permeable_soil_rfu_mm,
        c.permeable_soil_ground_max_inf_rate,
        c.permeable_soil_ground_lag_time_day,
        c.permeable_soil_coef_river_exchange,
        c.runoff_type,
        c.socose_tc_mn,
        c.socose_shape_param_beta,
        c.define_k_mn,
        c.q_limit,
        c.q0,
        c.contour,
        c.network_type,
        c.rural_land_use,
        c.industrial_land_use,
        c.suburban_housing_land_use,
        c.dense_housing_land_use,
        p.geom,
        p.configuration::character varying as configuration,
        p.generated,
        p.validity,
        p.configuration as configuration_json
    from $model._catchment_node c,
    $model._node p
    where p.id = c.id;
;;

create or replace function ${model}.catchment_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('catchment', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'catchment') where name = 'define_later' and id = id_;
            insert into $model._catchment_node(id, node_type, area_ha, rl, slope, c_imp, netflow_type, constant_runoff, horner_ini_loss_coef, horner_recharge_coef, holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm, scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm, permeable_soil_j_mm, permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange, runoff_type, socose_tc_mn, socose_shape_param_beta, define_k_mn, q_limit, q0, contour, network_type, rural_land_use, industrial_land_use, suburban_housing_land_use, dense_housing_land_use)
                values (id_, 'catchment', new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, coalesce(new.q_limit, 9999), coalesce(new.q0, 0), coalesce(new.contour, (select id from $model.catchment where st_intersects(geom, new.geom))), coalesce(new.network_type, 'separative'), new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'catchment_node');

            update $model._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_type!='constant_runoff' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)) and (netflow_type!='horner' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)) and (netflow_type!='horner' or (horner_recharge_coef is not null and horner_recharge_coef>=0)) and (netflow_type!='holtan' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)) and (netflow_type!='scs' or (scs_j_mm is not null and scs_j_mm>=0)) and (netflow_type!='scs' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)) and (netflow_type!='scs' or (scs_rfu_mm is not null and scs_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_j_mm is not null and permeable_soil_j_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_rfu_mm is not null and permeable_soil_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_max_inf_rate is not null and permeable_soil_ground_max_inf_rate>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_lag_time_day is not null and permeable_soil_ground_lag_time_day>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_coef_river_exchange is not null and permeable_soil_coef_river_exchange>=0 and permeable_soil_coef_river_exchange<=1)) and (runoff_type is not null) and (runoff_type!='socose' or (socose_tc_mn is not null and socose_tc_mn>0)) and (runoff_type!='socose' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6)) and (runoff_type!='define_k' or (define_k_mn is not null and define_k_mn>0)) and (q_limit is not null) and (q0 is not null) and (network_type is not null) and (rural_land_use>=0) and (industrial_land_use>=0) and (suburban_housing_land_use>=0) and (dense_housing_land_use>=0) from  $model._catchment_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.network_type, new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use) is distinct from (old.area_ha, old.rl, old.slope, old.c_imp, old.netflow_type, old.constant_runoff, old.horner_ini_loss_coef, old.horner_recharge_coef, old.holtan_sat_inf_rate_mmh, old.holtan_dry_inf_rate_mmh, old.holtan_soil_storage_cap_mm, old.scs_j_mm, old.scs_soil_drainage_time_day, old.scs_rfu_mm, old.permeable_soil_j_mm, old.permeable_soil_rfu_mm, old.permeable_soil_ground_max_inf_rate, old.permeable_soil_ground_lag_time_day, old.permeable_soil_coef_river_exchange, old.runoff_type, old.socose_tc_mn, old.socose_shape_param_beta, old.define_k_mn, old.q_limit, old.q0, old.contour, old.network_type, old.rural_land_use, old.industrial_land_use, old.suburban_housing_land_use, old.dense_housing_land_use)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area_ha, old.rl, old.slope, old.c_imp, old.netflow_type, old.constant_runoff, old.horner_ini_loss_coef, old.horner_recharge_coef, old.holtan_sat_inf_rate_mmh, old.holtan_dry_inf_rate_mmh, old.holtan_soil_storage_cap_mm, old.scs_j_mm, old.scs_soil_drainage_time_day, old.scs_rfu_mm, old.permeable_soil_j_mm, old.permeable_soil_rfu_mm, old.permeable_soil_ground_max_inf_rate, old.permeable_soil_ground_lag_time_day, old.permeable_soil_coef_river_exchange, old.runoff_type, old.socose_tc_mn, old.socose_shape_param_beta, old.define_k_mn, old.q_limit, old.q0, old.contour, old.network_type, old.rural_land_use, old.industrial_land_use, old.suburban_housing_land_use, old.dense_housing_land_use) as o, (select new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.network_type, new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.network_type, new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._catchment_node set area_ha=new.area_ha, rl=new.rl, slope=new.slope, c_imp=new.c_imp, netflow_type=new.netflow_type, constant_runoff=new.constant_runoff, horner_ini_loss_coef=new.horner_ini_loss_coef, horner_recharge_coef=new.horner_recharge_coef, holtan_sat_inf_rate_mmh=new.holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh=new.holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm=new.holtan_soil_storage_cap_mm, scs_j_mm=new.scs_j_mm, scs_soil_drainage_time_day=new.scs_soil_drainage_time_day, scs_rfu_mm=new.scs_rfu_mm, permeable_soil_j_mm=new.permeable_soil_j_mm, permeable_soil_rfu_mm=new.permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate=new.permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day=new.permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange=new.permeable_soil_coef_river_exchange, runoff_type=new.runoff_type, socose_tc_mn=new.socose_tc_mn, socose_shape_param_beta=new.socose_shape_param_beta, define_k_mn=new.define_k_mn, q_limit=new.q_limit, q0=new.q0, contour=new.contour, network_type=new.network_type, rural_land_use=new.rural_land_use, industrial_land_use=new.industrial_land_use, suburban_housing_land_use=new.suburban_housing_land_use, dense_housing_land_use=new.dense_housing_land_use where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'catchment_node');

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;

            update $model._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_type!='constant_runoff' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)) and (netflow_type!='horner' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)) and (netflow_type!='horner' or (horner_recharge_coef is not null and horner_recharge_coef>=0)) and (netflow_type!='holtan' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)) and (netflow_type!='scs' or (scs_j_mm is not null and scs_j_mm>=0)) and (netflow_type!='scs' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)) and (netflow_type!='scs' or (scs_rfu_mm is not null and scs_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_j_mm is not null and permeable_soil_j_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_rfu_mm is not null and permeable_soil_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_max_inf_rate is not null and permeable_soil_ground_max_inf_rate>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_lag_time_day is not null and permeable_soil_ground_lag_time_day>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_coef_river_exchange is not null and permeable_soil_coef_river_exchange>=0 and permeable_soil_coef_river_exchange<=1)) and (runoff_type is not null) and (runoff_type!='socose' or (socose_tc_mn is not null and socose_tc_mn>0)) and (runoff_type!='socose' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6)) and (runoff_type!='define_k' or (define_k_mn is not null and define_k_mn>0)) and (q_limit is not null) and (q0 is not null) and (network_type is not null) and (rural_land_use>=0) and (industrial_land_use>=0) and (suburban_housing_land_use>=0) and (dense_housing_land_use>=0) from  $model._catchment_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            delete from $model._catchment_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

drop trigger if exists ${model}_catchment_node on $model.catchment_node;
;;

create trigger ${model}_catchment_node_trig
    instead of insert or update or delete on $model.catchment_node
       for each row execute procedure ${model}.catchment_node_fct()
;;

-- Hydrograph BC
drop view if exists $model.hydrograph_bc_singularity cascade;
;;

alter table $model._hydrograph_bc_singularity drop column if exists pollution_quality_mode ;
alter table $model._hydrograph_bc_singularity drop column if exists pollution_quality_param ;
alter table $model._hydrograph_bc_singularity add column if not exists pollution_dryweather_runoff real[2][4] default '{{0, 0, 0, 0},{0, 0, 0, 0}}'::real[];
alter table $model._hydrograph_bc_singularity add column if not exists quality_dryweather_runoff  real[2][9] default '{{0, 0, 0, 0, 0, 0, 0, 0, 0},{0, 0, 0, 0, 0, 0, 0, 0, 0}}'::real[];
;;

update $model._hydrograph_bc_singularity set pollution_dryweather_runoff='{{0, 0, 0, 0},{0, 0, 0, 0}}'::real[], quality_dryweather_runoff='{{0, 0, 0, 0, 0, 0, 0, 0, 0},{0, 0, 0, 0, 0, 0, 0, 0, 0}}'::real[];
;;

create or replace view $model.hydrograph_bc_singularity as
    select p.id,
        p.name,
        n.id as node,
        c.storage_area,
        c.tq_array,
        c.constant_dry_flow,
        c.distrib_coef,
        c.lag_time_hr,
        c.sector,
        c.hourly_modulation,
        c.pollution_dryweather_runoff,
        c.quality_dryweather_runoff,
        c.external_file_data,
        n.geom,
        p.configuration::character varying as configuration,
        p.validity,
        p.configuration as configuration_json
    from $model._hydrograph_bc_singularity c,
        $model._singularity p,
        $model._node n
    where p.id = c.id and n.id = p.node;
;;

create or replace function ${model}.hydrograph_bc_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'hydrograph_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'hydrograph_bc') where name = 'define_later' and id = id_;

            insert into $model._hydrograph_bc_singularity
                values (id_, 'hydrograph_bc', coalesce(new.storage_area, 1), new.tq_array, coalesce(new.constant_dry_flow, 0), coalesce(new.distrib_coef, 1), coalesce(new.lag_time_hr, 0), new.sector, new.hourly_modulation, coalesce(new.pollution_dryweather_runoff, '{{0, 0, 0, 0},{0, 0, 0, 0}}'::real[]), coalesce(new.quality_dryweather_runoff, '{{0, 0, 0, 0, 0, 0, 0, 0, 0},{0, 0, 0, 0, 0, 0, 0, 0, 0}}'::real[]), coalesce(new.external_file_data, 'f'));
            if 'hydrograph_bc' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            if 'hydrograph_bc' = 'hydrograph_bc' then
                update $model.routing_hydrology_link set hydrograph=id_ where down=nid_;
                update $model.connector_hydrology_link set hydrograph=id_ where down=nid_;
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'hydrograph_bc_singularity');
            update $model._singularity set validity = (select (storage_area is not null) and (storage_area>=0) and (constant_dry_flow is not null) and (constant_dry_flow>=0) and (distrib_coef is null or distrib_coef>=0) and (lag_time_hr is null or (lag_time_hr>=0)) and (sector is null or (distrib_coef is not null and lag_time_hr is not null)) and (array_length(pollution_dryweather_runoff, 1)=2) and (array_length(pollution_dryweather_runoff, 2)=4) and (array_length(quality_dryweather_runoff, 1)=2) and (array_length(quality_dryweather_runoff, 2)=9) and (external_file_data or tq_array is not null) and (external_file_data or array_length(tq_array, 1)<=10) and (external_file_data or array_length(tq_array, 1)>=1) and (external_file_data or array_length(tq_array, 2)=2) from  $model._hydrograph_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.storage_area, new.tq_array, new.constant_dry_flow, new.distrib_coef, new.lag_time_hr, new.hourly_modulation, new.pollution_dryweather_runoff, new.quality_dryweather_runoff, new.external_file_data) is distinct from (old.storage_area, old.tq_array, old.constant_dry_flow, old.distrib_coef, old.lag_time_hr, old.hourly_modulation, old.pollution_dryweather_runoff, old.quality_dryweather_runoff, old.external_file_data)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.storage_area, old.tq_array, old.constant_dry_flow, old.distrib_coef, old.lag_time_hr, old.hourly_modulation, old.pollution_dryweather_runoff, old.quality_dryweather_runoff, old.external_file_data) as o, (select new.storage_area, new.tq_array, new.constant_dry_flow, new.distrib_coef, new.lag_time_hr, new.hourly_modulation, new.pollution_dryweather_runoff, new.quality_dryweather_runoff, new.external_file_data) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.storage_area, new.tq_array, new.constant_dry_flow, new.distrib_coef, new.lag_time_hr, new.hourly_modulation, new.pollution_dryweather_runoff, new.quality_dryweather_runoff, new.external_file_data) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._hydrograph_bc_singularity set storage_area=new.storage_area, tq_array=new.tq_array, constant_dry_flow=new.constant_dry_flow, distrib_coef=new.distrib_coef, lag_time_hr=new.lag_time_hr, sector=new.sector, hourly_modulation=new.hourly_modulation, pollution_dryweather_runoff=new.pollution_dryweather_runoff, quality_dryweather_runoff=new.quality_dryweather_runoff, external_file_data=new.external_file_data where id=old.id;
            if 'hydrograph_bc' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            if 'hydrograph_bc' = 'hydrograph_bc' then
                update $model.routing_hydrology_link set hydrograph=null where down=old.node;
                update $model.connector_hydrology_link set hydrograph=null where down=old.node;
                update $model.routing_hydrology_link set hydrograph=new.id where down=new.node;
                update $model.connector_hydrology_link set hydrograph=new.id where down=new.node;
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'hydrograph_bc_singularity');
            update $model._singularity set validity = (select (storage_area is not null) and (storage_area>=0) and (constant_dry_flow is not null) and (constant_dry_flow>=0) and (distrib_coef is null or distrib_coef>=0) and (lag_time_hr is null or (lag_time_hr>=0)) and (sector is null or (distrib_coef is not null and lag_time_hr is not null)) and (array_length(pollution_dryweather_runoff, 1)=2) and (array_length(pollution_dryweather_runoff, 2)=4) and (array_length(quality_dryweather_runoff, 1)=2) and (array_length(quality_dryweather_runoff, 2)=9) and (external_file_data or tq_array is not null) and (external_file_data or array_length(tq_array, 1)<=10) and (external_file_data or array_length(tq_array, 1)>=1) and (external_file_data or array_length(tq_array, 2)=2) from  $model._hydrograph_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            if 'hydrograph_bc' = 'hydrograph_bc' then
                update $model.routing_hydrology_link set hydrograph=null where down=old.node;
                update $model.connector_hydrology_link set hydrograph=null where down=old.node;
            end if;
            delete from $model._hydrograph_bc_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'hydrograph_bc' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$$$
;;

drop trigger if exists ${model}_hydrograph_bc_singularity on $model.hydrograph_bc_singularity;
;;

create trigger ${model}_hydrograph_bc_singularity_trig
    instead of insert or update or delete on $model.hydrograph_bc_singularity
       for each row execute procedure ${model}.hydrograph_bc_singularity_fct()
;;

-- Tank BC
drop view if exists $model.tank_bc_singularity cascade;
;;

alter table $model._tank_bc_singularity add column if not exists treatment_mode hydra_pollution_treatment_mode default 'residual_concentration';
alter table $model._tank_bc_singularity add column if not exists treatment_param json;
;;

create or replace view $model.tank_bc_singularity as
    select p.id,
        p.name,
        n.id as node,
        c.zs_array,
        c.zini,
        c.treatment_mode,
        c.treatment_param::character varying as treatment_param,
        c.treatment_param as treatment_param_json,
        n.geom,
        p.configuration::character varying as configuration,
        p.validity,
        p.configuration as configuration_json
    from $model._tank_bc_singularity c,
        $model._singularity p,
        $model._node n
    where p.id = c.id and n.id = p.node;
;;

create or replace function ${model}.tank_bc_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'tank_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'tank_bc') where name = 'define_later' and id = id_;

            insert into $model._tank_bc_singularity(id, singularity_type, zs_array, zini, treatment_mode, treatment_param)
                values (id_, 'tank_bc', new.zs_array, new.zini, coalesce(new.treatment_mode, 'residual_concentration'), new.treatment_param::json);

            perform $model.add_configuration_fct(new.configuration::json, id_, 'tank_bc_singularity');
            update $model._singularity set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1)<=10) and (array_length(zs_array, 1)>=1) and (array_length(zs_array, 2)=2) and (treatment_mode is not null) from  $model._tank_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.zs_array, new.zini, new.treatment_mode, new.treatment_param) is distinct from (old.zs_array, old.zini, old.treatment_mode, old.treatment_param)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.zs_array, old.zini, old.treatment_mode, old.treatment_param) as o, (select new.zs_array, new.zini, new.treatment_mode, new.treatment_param) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.zs_array, new.zini, new.treatment_mode, new.treatment_param) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._tank_bc_singularity set zs_array=new.zs_array, zini=new.zini, treatment_mode=new.treatment_mode, treatment_param=new.treatment_param::json where id=old.id;

            perform $model.add_configuration_fct(new.configuration::json, old.id, 'tank_bc_singularity');
            update $model._singularity set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1)<=10) and (array_length(zs_array, 1)>=1) and (array_length(zs_array, 2)=2) and (treatment_mode is not null) from  $model._tank_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._tank_bc_singularity where id=old.id;
            delete from $model._singularity where id=old.id;

            return old;
        end if;
    end;
$$$$
;;

drop trigger if exists ${model}_tank_bc_singularity on $model.tank_bc_singularity;
;;

create trigger ${model}_tank_bc_singularity_trig
    instead of insert or update or delete on $model.tank_bc_singularity
       for each row execute procedure ${model}.tank_bc_singularity_fct()
;;

/* ********************************************** */
/*  Dervivation pump link validity update         */
/* ********************************************** */

create or replace function ${model}.deriv_pump_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('deriv_pump', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'deriv_pump') where name = 'define_later' and id = id_;
            insert into $model._deriv_pump_link
                values (id_, 'deriv_pump', new.q_pump, new.qz_array);
            if 'deriv_pump' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'deriv_pump_link');
            update $model._link set validity = (select (q_pump is not null) and (q_pump>= 0) and (qz_array is not null) and (array_length(qz_array, 1)<=10) and (array_length(qz_array, 1)>=1) and (array_length(qz_array, 2)=2) and ((/*   qz_array: 0<=q   */ select bool_and(bool) from (select 0<=unnest(new.qz_array[:][1]) as bool) as data_set)) and ((/*   qz_array: q<=1   */ select bool_and(bool) from (select unnest(new.qz_array[:][1])<=1 as bool) as data_set)) from  $model._deriv_pump_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'deriv_pump' = 'pipe' then
                update $model.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.q_pump, new.qz_array) is distinct from (old.q_pump, old.qz_array)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.q_pump, old.qz_array) as o, (select new.q_pump, new.qz_array) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.q_pump, new.qz_array) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._deriv_pump_link set q_pump=new.q_pump, qz_array=new.qz_array where id=old.id;

            if 'deriv_pump' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) and (select trigger_branch from $model.metadata) then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'deriv_pump_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'deriv_pump' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update $model.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (q_pump is not null) and (q_pump>= 0) and (qz_array is not null) and (array_length(qz_array, 1)<=10) and (array_length(qz_array, 1)>=1) and (array_length(qz_array, 2)=2) and ((/*   qz_array: 0<=q   */ select bool_and(bool) from (select 0<=unnest(new.qz_array[:][1]) as bool) as data_set)) and ((/*   qz_array: q<=1   */ select bool_and(bool) from (select unnest(new.qz_array[:][1])<=1 as bool) as data_set)) from  $model._deriv_pump_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._deriv_pump_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'deriv_pump' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'deriv_pump' = 'pipe' then
                update $model.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;

/* ********************************************** */
/*  Views droppped in cascades                    */
/* ********************************************** */

create or replace view ${model}.pipe_link as
    select p.id,
        p.name,
        p.up,
        p.down,
        c.comment,
        c.z_invert_up,
        c.z_invert_down,
        c.cross_section_type,
        c.h_sable,
        c.branch,
        c.rk,
        c.custom_length,
        c.circular_diameter,
        c.ovoid_height,
        c.ovoid_top_diameter,
        c.ovoid_invert_diameter,
        c.cp_geom,
        c.op_geom,
        p.geom,
        p.configuration::character varying as configuration,
        p.generated,
        p.validity,
        p.up_type,
        p.down_type,
        p.configuration as configuration_json,
        (select case
        when (c.z_invert_up-c.z_invert_down)*st_length(p.geom)>0
        and st_length(p.geom)<>0
        then
            (select case when c.cross_section_type='circular' then
                (select case when c.circular_diameter>0 then
                    (with n as (select
                        (pi()/4*pow(c.circular_diameter, 2.0)) as s,
                        (pi()*c.circular_diameter) as p,
                        c.rk as k)
                    (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n))
                else -999
                end)
            when c.cross_section_type='ovoid' then
                (with n as (select
                    (pi()/8*(c.ovoid_top_diameter*c.ovoid_top_diameter+c.ovoid_invert_diameter*c.ovoid_invert_diameter)+0.5*(c.ovoid_height-0.5*(c.ovoid_top_diameter+c.ovoid_invert_diameter))*(c.ovoid_top_diameter+c.ovoid_invert_diameter)) as s,
                    (pi()/2*(c.ovoid_top_diameter+c.ovoid_invert_diameter)+2*(c.ovoid_height-0.5*(c.ovoid_top_diameter+c.ovoid_invert_diameter))) as p,
                    c.rk as k)
                (select case when n.s>0 and n.p>0 then
                    (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
                else -999
                end from n))
            when c.cross_section_type='pipe' then
                (with n as (select ${model}.geometric_calc_s_fct(cpg.zbmin_array) as s,
                        ${model}.cp_geometric_calc_p_fct(cpg.zbmin_array) as p,
                        c.rk as k
                    from ${model}.closed_parametric_geometry cpg
                    where c.cp_geom = cpg.id)
                (select case when n.s>0 and n.p>0 then
                    (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
                else -999
                end from n))
            when c.cross_section_type='channel' then
                (with n as (select ${model}.geometric_calc_s_fct(opg.zbmin_array) as s,
                        ${model}.op_geometric_calc_p_fct(opg.zbmin_array) as p,
                        c.rk as k
                    from ${model}.open_parametric_geometry opg
                    where c.op_geom = opg.id)
                (select case when n.s>0 and n.p>0 then
                    (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
                else -999
                end from n))
            else 0.0
            end)
        else -999
        end) as qcap,
        (select case when c.cross_section_type='circular' then
            (select (c.z_invert_up+c.circular_diameter))
        when c.cross_section_type='ovoid' then
            (select (c.z_invert_up+c.ovoid_height))
        when c.cross_section_type='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.cp_geom = cpg.id)
            (select (c.z_invert_up + n.height) from n))
        when c.cross_section_type='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.op_geom = opg.id)
            (select (c.z_invert_up + n.height) from n))
        else null
        end) as z_vault_up,
        (select case when c.cross_section_type='circular' then
            (select (c.z_invert_down+c.circular_diameter))
        when c.cross_section_type='ovoid' then
            (select (c.z_invert_down+c.ovoid_height))
        when c.cross_section_type='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.cp_geom = cpg.id)
            (select (c.z_invert_down + n.height) from n))
        when c.cross_section_type='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.op_geom = opg.id)
            (select (c.z_invert_down + n.height) from n))
        else null
        end) as z_vault_down,
        (select case when c.cross_section_type='channel' then
            (select (c.z_invert_up))::double precision
        else (select z_ground from ${model}.manhole_node where p.up = id)::double precision
        end) as z_tn_up,
        (select case when c.cross_section_type='channel' then
            (select (c.z_invert_down))::double precision
        else (select z_ground from ${model}.manhole_node where p.down = id)::double precision
        end) as z_tn_down

    from ${model}._pipe_link c,
    ${model}._link p
    where p.id = c.id;
;;

create or replace function ${model}.pipe_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('pipe', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'pipe') where name = 'define_later' and id = id_;
            insert into $model._pipe_link
                values (id_, 'pipe', new.comment, new.z_invert_up, new.z_invert_down, coalesce(new.cross_section_type, 'circular'), coalesce(new.h_sable, 0), new.branch, new.rk, coalesce(new.custom_length, null), new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom);
            if 'pipe' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'pipe_link');
            update $model._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (h_sable is not null) and (cross_section_type is not null) and (cross_section_type not in ('valley')) and (rk is not null) and (rk >0) and (cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 )) and (cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)) and (cross_section_type!='pipe' or cp_geom is not null) and (cross_section_type!='channel' or op_geom is not null) from  $model._pipe_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' then
                update $model.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.comment, new.z_invert_up, new.z_invert_down, new.cross_section_type, new.h_sable, new.rk, new.custom_length, new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom) is distinct from (old.comment, old.z_invert_up, old.z_invert_down, old.cross_section_type, old.h_sable, old.rk, old.custom_length, old.circular_diameter, old.ovoid_height, old.ovoid_top_diameter, old.ovoid_invert_diameter, old.cp_geom, old.op_geom)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.comment, old.z_invert_up, old.z_invert_down, old.cross_section_type, old.h_sable, old.rk, old.custom_length, old.circular_diameter, old.ovoid_height, old.ovoid_top_diameter, old.ovoid_invert_diameter, old.cp_geom, old.op_geom) as o, (select new.comment, new.z_invert_up, new.z_invert_down, new.cross_section_type, new.h_sable, new.rk, new.custom_length, new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.comment, new.z_invert_up, new.z_invert_down, new.cross_section_type, new.h_sable, new.rk, new.custom_length, new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._pipe_link set comment=new.comment, z_invert_up=new.z_invert_up, z_invert_down=new.z_invert_down, cross_section_type=new.cross_section_type, h_sable=new.h_sable, branch=new.branch, rk=new.rk, custom_length=new.custom_length, circular_diameter=new.circular_diameter, ovoid_height=new.ovoid_height, ovoid_top_diameter=new.ovoid_top_diameter, ovoid_invert_diameter=new.ovoid_invert_diameter, cp_geom=new.cp_geom, op_geom=new.op_geom where id=old.id;

            if 'pipe' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) and (select trigger_branch from $model.metadata) then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'pipe_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update $model.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (h_sable is not null) and (cross_section_type is not null) and (cross_section_type not in ('valley')) and (rk is not null) and (rk >0) and (cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 )) and (cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)) and (cross_section_type!='pipe' or cp_geom is not null) and (cross_section_type!='channel' or op_geom is not null) from  $model._pipe_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._pipe_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'pipe' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' then
                update $model.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;

drop trigger if exists ${model}_pipe_link_trig  on ${model}.pipe_link;
;;

create trigger ${model}_pipe_link_trig
    instead of insert or update or delete on ${model}.pipe_link
    for each row execute procedure ${model}.pipe_link_fct();
;;

-- Pipes
create or replace view ${model}.results_pipe as
    select id, name, geom from ${model}.pipe_link
    where up_type='manhole' and down_type='manhole'
;;

-- Hydrol pipes
create or replace view ${model}.results_pipe_hydrol as
    select id, name, geom from ${model}.pipe_link
    where up_type='manhole_hydrology' and down_type='manhole_hydrology'
;;

-- Catchments
create or replace view ${model}.results_catchment as
    select id, name, geom from ${model}.catchment_node
;;

-- Links
create or replace view ${model}.results_link as
    select id, name, geom from ${model}.gate_link
        union
    select id, name, geom from ${model}.regul_gate_link
        union
    select id, name, geom from ${model}.weir_link
        union
    select id, name, geom from ${model}.borda_headloss_link
        union
    select id, name, geom from ${model}.pump_link
        union
    select id, name, geom from ${model}.deriv_pump_link
        union
    select id, name, geom from ${model}.fuse_spillway_link
        union
    select id, name, geom from ${model}.connector_link
        union
    select id, name, geom from ${model}.strickler_link
        union
    select id, name, geom from ${model}.porous_link
        union
    select id, name, geom from ${model}.overflow_link
        union
    select id, name, geom from ${model}.network_overflow_link
        union
    select id, name, geom from ${model}.mesh_2d_link
        union
    select id, name, geom from ${model}.street_link
;;

-- Surface elements
create or replace view ${model}.results_surface as
    with
        dump as (
            select st_collect(n.geom) as points, s.geom as envelope, s.id
            from ${model}.coverage as s, ${model}.crossroad_node as n
            where s.domain_type='street' and st_intersects(s.geom, n.geom)
            group by s.id
        ),
        voronoi as (
            select st_intersection(st_setsrid((st_dump(st_VoronoiPolygons(dump.points, 0, dump.envelope))).geom, ${srid}), dump.envelope) as geom
            from dump
        )
    select e.id as id, e.name as name, e.contour as geom from ${model}.elem_2d_node as e
        union
    select n.id as id, n.name as name, c.geom as geom from ${model}.storage_node as n join ${model}.coverage as c on n.contour=c.id
        union
    select n.id as id, n.name as name, voronoi.geom as geom from voronoi, ${model}.crossroad_node as n where st_intersects(voronoi.geom, n.geom)
        union
    select n.id as id, n.name as name, c.geom as geom from ${model}.crossroad_node as n, ${model}.coverage as c where c.domain_type='crossroad' and st_intersects(n.geom, c.geom)
    order by id asc;
;;

create or replace view $model.invalid as
    with node_invalidity_reason AS (
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   (area > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END AS reason
           FROM $model.manhole_hydrology_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END) ||
                CASE
                    WHEN ( SELECT NOT (EXISTS ( SELECT 1
                               FROM $model.station
                              WHERE st_intersects(station.geom, new_1.geom)))) THEN ''::text
                    ELSE '   (select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))   '::text
                END) ||
                CASE
                    WHEN $model.check_connected_pipes(new_1.id) THEN ''::text
                    ELSE '   $model.check_connected_pipes(new.id)   '::text
                END AS reason
           FROM $model.manhole_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END) ||
                CASE
                    WHEN new_1.station IS NOT NULL THEN ''::text
                    ELSE '   station is not null    '::text
                END) ||
                CASE
                    WHEN ( SELECT st_intersects(new_1.geom, station.geom) AS st_intersects
                       FROM $model.station
                      WHERE station.id = new_1.station) THEN ''::text
                    ELSE '   (select st_intersects(new.geom, geom) from $model.station where id=new.station)   '::text
                END AS reason
           FROM $model.station_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((((((((((((((((
                CASE
                    WHEN new_1.area_ha IS NOT NULL THEN ''::text
                    ELSE '   area_ha is not null   '::text
                END ||
                CASE
                    WHEN new_1.area_ha > 0::double precision THEN ''::text
                    ELSE '   area_ha>0   '::text
                END) ||
                CASE
                    WHEN new_1.rl IS NOT NULL THEN ''::text
                    ELSE '   rl is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rl > 0::double precision THEN ''::text
                    ELSE '   rl>0   '::text
                END) ||
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp IS NOT NULL THEN ''::text
                    ELSE '   c_imp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp >= 0::double precision THEN ''::text
                    ELSE '   c_imp>=0   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp <= 1::double precision THEN ''::text
                    ELSE '   c_imp<=1   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type IS NOT NULL THEN ''::text
                    ELSE '   netflow_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'constant_runoff'::hydra_netflow_type OR new_1.constant_runoff IS NOT NULL AND new_1.constant_runoff >= 0::double precision AND new_1.constant_runoff <= 1::double precision THEN ''::text
                    ELSE '   netflow_type!=''constant_runoff'' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'horner'::hydra_netflow_type OR new_1.horner_ini_loss_coef IS NOT NULL AND new_1.horner_ini_loss_coef >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''horner'' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'horner'::hydra_netflow_type OR new_1.horner_recharge_coef IS NOT NULL AND new_1.horner_recharge_coef >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''horner'' or (horner_recharge_coef is not null and horner_recharge_coef>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'holtan'::hydra_netflow_type OR new_1.holtan_sat_inf_rate_mmh IS NOT NULL AND new_1.holtan_sat_inf_rate_mmh >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''holtan'' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'holtan'::hydra_netflow_type OR new_1.holtan_dry_inf_rate_mmh IS NOT NULL AND new_1.holtan_dry_inf_rate_mmh >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''holtan'' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'holtan'::hydra_netflow_type OR new_1.holtan_soil_storage_cap_mm IS NOT NULL AND new_1.holtan_soil_storage_cap_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''holtan'' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'scs'::hydra_netflow_type OR new_1.scs_j_mm IS NOT NULL AND new_1.scs_j_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''scs'' or (scs_j_mm is not null and scs_j_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'scs'::hydra_netflow_type OR new_1.scs_soil_drainage_time_day IS NOT NULL AND new_1.scs_soil_drainage_time_day >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''scs'' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'scs'::hydra_netflow_type OR new_1.scs_rfu_mm IS NOT NULL AND new_1.scs_rfu_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''scs'' or (scs_rfu_mm is not null and scs_rfu_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'permeable_soil'::hydra_netflow_type OR new_1.permeable_soil_j_mm IS NOT NULL AND new_1.permeable_soil_j_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''permeable_soil'' or (permeable_soil_j_mm is not null and permeable_soil_j_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'permeable_soil'::hydra_netflow_type OR new_1.permeable_soil_rfu_mm IS NOT NULL AND new_1.permeable_soil_rfu_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''permeable_soil'' or (permeable_soil_rfu_mm is not null and permeable_soil_rfu_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'permeable_soil'::hydra_netflow_type OR new_1.permeable_soil_ground_max_inf_rate IS NOT NULL AND new_1.permeable_soil_ground_max_inf_rate >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''permeable_soil'' or (permeable_soil_ground_max_inf_rate is not null and permeable_soil_ground_max_inf_rate>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'permeable_soil'::hydra_netflow_type OR new_1.permeable_soil_ground_lag_time_day IS NOT NULL AND new_1.permeable_soil_ground_lag_time_day >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''permeable_soil'' or (permeable_soil_ground_lag_time_day is not null and permeable_soil_ground_lag_time_day>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'permeable_soil'::hydra_netflow_type OR new_1.permeable_soil_coef_river_exchange IS NOT NULL AND new_1.permeable_soil_coef_river_exchange >= 0::double precision AND new_1.permeable_soil_coef_river_exchange <= 1::double precision THEN ''::text
                    ELSE '   netflow_type!=''permeable_soil'' or (permeable_soil_coef_river_exchange is not null and permeable_soil_coef_river_exchange>=0 and permeable_soil_coef_river_exchange<=1)   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type IS NOT NULL THEN ''::text
                    ELSE '   runoff_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type <> 'socose'::hydra_runoff_type OR new_1.socose_tc_mn IS NOT NULL AND new_1.socose_tc_mn > 0::double precision THEN ''::text
                    ELSE '   runoff_type!=''socose'' or (socose_tc_mn is not null and socose_tc_mn>0)   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type <> 'socose'::hydra_runoff_type OR new_1.socose_shape_param_beta IS NOT NULL AND new_1.socose_shape_param_beta >= 1::double precision AND new_1.socose_shape_param_beta <= 6::double precision THEN ''::text
                    ELSE '   runoff_type!=''socose'' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6)   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type <> 'define_k'::hydra_runoff_type OR new_1.define_k_mn IS NOT NULL AND new_1.define_k_mn > 0::double precision THEN ''::text
                    ELSE '   runoff_type!=''define_k'' or (define_k_mn is not null and define_k_mn>0)   '::text
                END) ||
                CASE
                    WHEN new_1.q_limit IS NOT NULL THEN ''::text
                    ELSE '   q_limit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q0 IS NOT NULL THEN ''::text
                    ELSE '   q0 is not null   '::text
                END AS reason
           FROM $model.catchment_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END ||
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END) ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.reach IS NOT NULL THEN ''::text
                    ELSE '   reach is not null   '::text
                END) ||
                CASE
                    WHEN ( SELECT NOT (EXISTS ( SELECT 1
                               FROM $model.station
                              WHERE st_intersects(station.geom, new_1.geom)))) THEN ''::text
                    ELSE '   (select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))   '::text
                END AS reason
           FROM $model.river_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END ||
                CASE
                    WHEN new_1.zini IS NOT NULL THEN ''::text
                    ELSE '   zini is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10    '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1) >=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END AS reason
           FROM $model.storage_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   (area > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h IS NOT NULL THEN ''::text
                    ELSE '   h is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h >= 0::double precision THEN ''::text
                    ELSE '   h>=0   '::text
                END AS reason
           FROM $model.crossroad_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.zb IS NOT NULL THEN ''::text
                    ELSE '   zb is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk > 0::double precision THEN ''::text
                    ELSE '   rk>0   '::text
                END AS reason
           FROM $model.elem_2d_node new_1
          WHERE NOT new_1.validity
        ), link_invalidity_reason AS (
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.cross_section IS NOT NULL THEN ''::text
                    ELSE '   cross_section is not null   '::text
                END ||
                CASE
                    WHEN new_1.cross_section >= 0::double precision THEN ''::text
                    ELSE '   cross_section>=0   '::text
                END) ||
                CASE
                    WHEN new_1.length IS NOT NULL THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN new_1.length >= 0::double precision THEN ''::text
                    ELSE '   length>=0   '::text
                END) ||
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type = 'manhole_hydrology'::hydra_node_type OR (new_1.down_type = ANY (ARRAY['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type])) AND new_1.hydrograph IS NOT NULL THEN ''::text
                    ELSE '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'') and (hydrograph is not null))   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.routing_hydrology_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.lateral_contraction_coef IS NOT NULL THEN ''::text
                    ELSE '   lateral_contraction_coef is not null   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef <= 1::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef<=1   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef >= 0::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef>=0   '::text
                END) ||
                CASE
                    WHEN st_npoints(new_1.border) = 2 THEN ''::text
                    ELSE '   ST_NPoints(border)=2   '::text
                END) ||
                CASE
                    WHEN st_isvalid(new_1.border) THEN ''::text
                    ELSE '   ST_IsValid(border)   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.mesh_2d_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= '0'::numeric::double precision THEN ''::text
                    ELSE '   width>=0.   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= '0'::numeric::double precision THEN ''::text
                    ELSE '   cc>=0.   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode IS NOT NULL THEN ''::text
                    ELSE '   break_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode OR new_1.z_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''zw_critical'' or z_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode OR new_1.t_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''time_critical'' or t_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or grp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp > 0 THEN ''::text
                    ELSE '   break_mode=''none'' or grp>0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp < 100 THEN ''::text
                    ELSE '   break_mode=''none'' or grp<100   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.dt_fracw_array IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or dt_fracw_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) <= 10 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) >= 1 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 2) = 2 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.fuse_spillway_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.length IS NOT NULL THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN new_1.length >= 0::double precision THEN ''::text
                    ELSE '   length>=0   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk > 0::double precision THEN ''::text
                    ELSE '   rk>0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.street_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.down_type = 'manhole_hydrology'::hydra_node_type OR (new_1.down_type = ANY (ARRAY['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type])) AND new_1.hydrograph IS NOT NULL THEN ''::text
                    ELSE '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'') and (hydrograph is not null))   '::text
                END ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.connector_hydrology_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert_stop IS NOT NULL THEN ''::text
                    ELSE '   z_invert_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling_stop IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.dt_regul_hr IS NOT NULL THEN ''::text
                    ELSE '   dt_regul_hr is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_control_node IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_pid_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_tz_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR new_1.q_z_crit IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul OR new_1.nr_z_gate IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.regul_gate_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.transmitivity IS NOT NULL THEN ''::text
                    ELSE '   transmitivity is not null   '::text
                END) ||
                CASE
                    WHEN new_1.transmitivity >= 0::double precision THEN ''::text
                    ELSE '   transmitivity>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.porous_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.q_pump IS NOT NULL THEN ''::text
                    ELSE '   q_pump is not null   '::text
                END ||
                CASE
                    WHEN new_1.q_pump >= 0::double precision THEN ''::text
                    ELSE '   q_pump>= 0   '::text
                END) ||
                CASE
                    WHEN new_1.qz_array IS NOT NULL THEN ''::text
                    ELSE '   qz_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(qz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.deriv_pump_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN new_1.z_crest1 IS NOT NULL THEN ''::text
                    ELSE '   z_crest1 is not null   '::text
                END ||
                CASE
                    WHEN new_1.width1 IS NOT NULL THEN ''::text
                    ELSE '   width1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width1 > 0::double precision THEN ''::text
                    ELSE '   width1>0   '::text
                END) ||
                CASE
                    WHEN new_1.length IS NOT NULL THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN new_1.length > 0::double precision THEN ''::text
                    ELSE '   length>0   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk >= 0::double precision THEN ''::text
                    ELSE '   rk>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 IS NOT NULL THEN ''::text
                    ELSE '   z_crest2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 > new_1.z_crest1 THEN ''::text
                    ELSE '   z_crest2>z_crest1   '::text
                END) ||
                CASE
                    WHEN new_1.width2 IS NOT NULL THEN ''::text
                    ELSE '   width2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width2 > new_1.width1 THEN ''::text
                    ELSE '   width2>width1   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.strickler_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((
                CASE
                    WHEN new_1.z_overflow IS NOT NULL THEN ''::text
                    ELSE '   z_overflow is not null   '::text
                END ||
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END) ||
                CASE
                    WHEN new_1.area >= 0::double precision THEN ''::text
                    ELSE '   area>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.network_overflow_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN new_1.npump IS NOT NULL THEN ''::text
                    ELSE '   npump is not null   '::text
                END ||
                CASE
                    WHEN new_1.npump <= 10 THEN ''::text
                    ELSE '   npump<=10   '::text
                END) ||
                CASE
                    WHEN new_1.npump >= 1 THEN ''::text
                    ELSE '   npump>=1   '::text
                END) ||
                CASE
                    WHEN new_1.zregul_array IS NOT NULL THEN ''::text
                    ELSE '   zregul_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.hq_array IS NOT NULL THEN ''::text
                    ELSE '   hq_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zregul_array, 1) = new_1.npump THEN ''::text
                    ELSE '   array_length(zregul_array, 1)=npump   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zregul_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zregul_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 1) = new_1.npump THEN ''::text
                    ELSE '   array_length(hq_array, 1)=npump   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 2) <= 10 THEN ''::text
                    ELSE '   array_length(hq_array, 2)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 2) >= 1 THEN ''::text
                    ELSE '   array_length(hq_array, 2)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 3) = 2 THEN ''::text
                    ELSE '   array_length(hq_array, 3)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.pump_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((
                CASE
                    WHEN new_1.z_invert_up IS NOT NULL THEN ''::text
                    ELSE '   z_invert_up is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_invert_down IS NOT NULL THEN ''::text
                    ELSE '   z_invert_down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h_sable IS NOT NULL THEN ''::text
                    ELSE '   h_sable is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'valley'::hydra_cross_section_type THEN ''::text
                    ELSE '   cross_section_type not in (''valley'')   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk > 0::double precision THEN ''::text
                    ELSE '   rk >0   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'circular'::hydra_cross_section_type OR new_1.circular_diameter IS NOT NULL AND new_1.circular_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''circular'' or (circular_diameter is not null and circular_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_top_diameter IS NOT NULL AND new_1.ovoid_top_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_invert_diameter IS NOT NULL AND new_1.ovoid_invert_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_height IS NOT NULL AND new_1.ovoid_height > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_height is not null and ovoid_height >0 )   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_height > ((new_1.ovoid_top_diameter + new_1.ovoid_invert_diameter) / 2::double precision) THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'pipe'::hydra_cross_section_type OR new_1.cp_geom IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type!=''pipe'' or cp_geom is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'channel'::hydra_cross_section_type OR new_1.op_geom IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type!=''channel'' or op_geom is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.pipe_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null    '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.weir_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((((((((((((((((
                CASE
                    WHEN new_1.z_crest1 IS NOT NULL THEN ''::text
                    ELSE '   z_crest1 is not null   '::text
                END ||
                CASE
                    WHEN new_1.width1 IS NOT NULL THEN ''::text
                    ELSE '   width1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width1 >= 0::double precision THEN ''::text
                    ELSE '   width1>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 IS NOT NULL THEN ''::text
                    ELSE '   z_crest2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 >= new_1.z_crest1 THEN ''::text
                    ELSE '   z_crest2>=z_crest1   '::text
                END) ||
                CASE
                    WHEN new_1.width2 IS NOT NULL THEN ''::text
                    ELSE '   width2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width2 >= 0::double precision THEN ''::text
                    ELSE '   width2>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef IS NOT NULL THEN ''::text
                    ELSE '   lateral_contraction_coef is not null   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef <= 1::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef<=1   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef >= 0::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef>=0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode IS NOT NULL THEN ''::text
                    ELSE '   break_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode OR new_1.z_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''zw_critical'' or z_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode OR new_1.t_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''time_critical'' or t_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.width_breach IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or width_breach is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or z_invert is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or grp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp > 0 THEN ''::text
                    ELSE '   break_mode=''none'' or grp>0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp < 100 THEN ''::text
                    ELSE '   break_mode=''none'' or grp<100   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.dt_fracw_array IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or dt_fracw_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) <= 10 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) >= 1 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 2) = 2 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.overflow_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling > new_1.z_invert THEN ''::text
                    ELSE '   z_ceiling>z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc <=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc >=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_valve IS NOT NULL THEN ''::text
                    ELSE '   mode_valve is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate IS NOT NULL THEN ''::text
                    ELSE '   z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate >= new_1.z_invert THEN ''::text
                    ELSE '   z_gate>=z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate <= new_1.z_ceiling THEN ''::text
                    ELSE '   z_gate<=z_ceiling   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.gate_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.law_type IS NOT NULL THEN ''::text
                    ELSE '   law_type is not null   '::text
                END ||
                CASE
                    WHEN new_1.param IS NOT NULL THEN ''::text
                    ELSE '   param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.borda_headloss_link new_1
          WHERE NOT new_1.validity
        ), singularity_invalidity_reason AS (
         SELECT new_1.id,
            (((((((((
                CASE
                    WHEN new_1.storage_area IS NOT NULL THEN ''::text
                    ELSE '   storage_area is not null   '::text
                END ||
                CASE
                    WHEN new_1.storage_area >= 0::double precision THEN ''::text
                    ELSE '   storage_area>=0   '::text
                END) ||
                CASE
                    WHEN new_1.constant_dry_flow IS NOT NULL THEN ''::text
                    ELSE '   constant_dry_flow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.constant_dry_flow >= 0::double precision THEN ''::text
                    ELSE '   constant_dry_flow>=0   '::text
                END) ||
                CASE
                    WHEN new_1.distrib_coef IS NULL OR new_1.distrib_coef >= 0::double precision THEN ''::text
                    ELSE '   distrib_coef is null or distrib_coef>=0   '::text
                END) ||
                CASE
                    WHEN new_1.lag_time_hr IS NULL OR new_1.lag_time_hr >= 0::double precision THEN ''::text
                    ELSE '   lag_time_hr is null or (lag_time_hr>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.sector IS NULL OR new_1.distrib_coef IS NOT NULL AND new_1.lag_time_hr IS NOT NULL THEN ''::text
                    ELSE '   sector is null or (distrib_coef is not null and lag_time_hr is not null)   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR new_1.tq_array IS NOT NULL THEN ''::text
                    ELSE '   external_file_data or tq_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 1) <= 10 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 1) >= 1 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 2) = 2 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 2)=2   '::text
                END AS reason
           FROM $model.hydrograph_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.drainage IS NOT NULL THEN ''::text
                    ELSE '   drainage is not null   '::text
                END ||
                CASE
                    WHEN new_1.overflow IS NOT NULL THEN ''::text
                    ELSE '   overflow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ini IS NOT NULL THEN ''::text
                    ELSE '   z_ini is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zr_sr_qf_qs_array IS NOT NULL THEN ''::text
                    ELSE '   zr_sr_qf_qs_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.treatment_mode IS NOT NULL THEN ''::text
                    ELSE '   treatment_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.treatment_param IS NOT NULL THEN ''::text
                    ELSE '   treatment_param is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 2) = 4 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 2)=4   '::text
                END AS reason
           FROM $model.reservoir_rsp_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null    '::text
                END ||
                CASE
                    WHEN new_1.z_regul IS NOT NULL THEN ''::text
                    ELSE '   z_regul is not null    '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul IS NOT NULL THEN ''::text
                    ELSE '   mode_regul is not null   '::text
                END) ||
                CASE
                    WHEN new_1.reoxy_law IS NOT NULL THEN ''::text
                    ELSE '   reoxy_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.reoxy_param IS NOT NULL THEN ''::text
                    ELSE '   reoxy_param is not null   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.zregul_weir_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.qz_array IS NOT NULL THEN ''::text
                    ELSE '   qz_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(qz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.hydraulic_cut_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((
                CASE
                    WHEN new_1.l_road IS NOT NULL THEN ''::text
                    ELSE '   l_road is not null   '::text
                END ||
                CASE
                    WHEN new_1.l_road >= 0::double precision THEN ''::text
                    ELSE '   l_road>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_road IS NOT NULL THEN ''::text
                    ELSE '   z_road is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zw_array IS NOT NULL THEN ''::text
                    ELSE '   zw_array is not null    '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.bridge_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.d_abutment_l IS NOT NULL THEN ''::text
                    ELSE '   d_abutment_l is not null   '::text
                END ||
                CASE
                    WHEN new_1.d_abutment_r IS NOT NULL THEN ''::text
                    ELSE '   d_abutment_r is not null   '::text
                END) ||
                CASE
                    WHEN new_1.abutment_type IS NOT NULL THEN ''::text
                    ELSE '   abutment_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zw_array IS NOT NULL THEN ''::text
                    ELSE '   zw_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.bradley_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.pk0_km IS NOT NULL THEN ''::text
                    ELSE '   pk0_km is not null   '::text
                END ||
                CASE
                    WHEN new_1.dx IS NOT NULL THEN ''::text
                    ELSE '   dx is not null   '::text
                END) ||
                CASE
                    WHEN new_1.dx >= 0.1::double precision THEN ''::text
                    ELSE '   dx>=0.1   '::text
                END AS reason
           FROM $model.pipe_branch_marker_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.q_dz_array IS NOT NULL THEN ''::text
                    ELSE '   q_dz_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(q_dz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(q_dz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(q_dz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.param_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((
                CASE
                    WHEN new_1.downstream IS NOT NULL THEN ''::text
                    ELSE '   downstream is not null   '::text
                END ||
                CASE
                    WHEN new_1.split1 IS NOT NULL THEN ''::text
                    ELSE '   split1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.downstream_param IS NOT NULL THEN ''::text
                    ELSE '   downstream_param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1_law IS NOT NULL THEN ''::text
                    ELSE '   split1_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1_param IS NOT NULL THEN ''::text
                    ELSE '   split1_param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NULL OR new_1.split2_law IS NOT NULL THEN ''::text
                    ELSE '   split2 is null or split2_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NULL OR new_1.split2_param IS NOT NULL THEN ''::text
                    ELSE '   split2 is null or split2_param is not null   '::text
                END AS reason
           FROM $model.zq_split_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((
                CASE
                    WHEN new_1.zq_array IS NOT NULL THEN ''::text
                    ELSE '   zq_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zq_array, 2)=2   '::text
                END AS reason
           FROM $model.zq_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling > new_1.z_invert THEN ''::text
                    ELSE '   z_ceiling>z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc <=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc >=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_valve IS NOT NULL THEN ''::text
                    ELSE '   mode_valve is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate IS NOT NULL THEN ''::text
                    ELSE '   z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate >= new_1.z_invert THEN ''::text
                    ELSE '   z_gate>=z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate <= new_1.z_ceiling THEN ''::text
                    ELSE '   z_gate<=z_ceiling   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.gate_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert_stop IS NOT NULL THEN ''::text
                    ELSE '   z_invert_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling_stop IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.dt_regul_hr IS NOT NULL THEN ''::text
                    ELSE '   dt_regul_hr is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_control_node IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_pid_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_tz_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR new_1.q_z_crit IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul OR new_1.nr_z_gate IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                END AS reason
           FROM $model.regul_sluice_gate_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.qq_array IS NOT NULL THEN ''::text
                    ELSE '   qq_array is not null   '::text
                END ||
                CASE
                    WHEN new_1.downstream IS NOT NULL THEN ''::text
                    ELSE '   downstream is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1 IS NOT NULL THEN ''::text
                    ELSE '   split1 is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qq_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NOT NULL AND array_length(new_1.qq_array, 2) = 3 OR new_1.split2 IS NULL AND array_length(new_1.qq_array, 2) = 2 THEN ''::text
                    ELSE '   (split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2)   '::text
                END AS reason
           FROM $model.qq_split_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.law_type IS NOT NULL THEN ''::text
                    ELSE '   law_type is not null   '::text
                END ||
                CASE
                    WHEN new_1.param IS NOT NULL THEN ''::text
                    ELSE '   param is not null   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.borda_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
                CASE
                    WHEN new_1.q0 IS NOT NULL THEN ''::text
                    ELSE '   q0 is not null   '::text
                END AS reason
           FROM $model.constant_inflow_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END ||
                CASE
                    WHEN new_1.k IS NOT NULL THEN ''::text
                    ELSE '   k is not null   '::text
                END) ||
                CASE
                    WHEN new_1.k > 0::double precision THEN ''::text
                    ELSE '   k>0   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END AS reason
           FROM $model.strickler_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END ||
                CASE
                    WHEN new_1.zini IS NOT NULL THEN ''::text
                    ELSE '   zini is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END AS reason
           FROM $model.tank_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.z_weir IS NOT NULL THEN ''::text
                    ELSE '   z_weir is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= '0'::numeric::double precision THEN ''::text
                    ELSE '   cc>=0.   '::text
                END AS reason
           FROM $model.weir_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.drainage IS NOT NULL THEN ''::text
                    ELSE '   drainage is not null   '::text
                END ||
                CASE
                    WHEN new_1.overflow IS NOT NULL THEN ''::text
                    ELSE '   overflow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q_drainage IS NOT NULL THEN ''::text
                    ELSE '   q_drainage is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q_drainage >= 0::double precision THEN ''::text
                    ELSE '   q_drainage>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_ini IS NOT NULL THEN ''::text
                    ELSE '   z_ini is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.treatment_mode IS NOT NULL THEN ''::text
                    ELSE '   treatment_mode is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END AS reason
           FROM $model.reservoir_rs_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.cascade_mode IS NOT NULL THEN ''::text
                    ELSE '   cascade_mode is not null   '::text
                END ||
                CASE
                    WHEN new_1.cascade_mode = 'hydrograph'::hydra_model_connect_mode OR new_1.zq_array IS NOT NULL THEN ''::text
                    ELSE '   cascade_mode=''hydrograph'' or zq_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) <= 100 THEN ''::text
                    ELSE '   array_length(zq_array, 1)<=100   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zq_array, 2)=2   '::text
                END AS reason
           FROM $model.model_connect_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((
                CASE
                    WHEN new_1.external_file_data OR new_1.tz_array IS NOT NULL THEN ''::text
                    ELSE '   external_file_data or tz_array is not null   '::text
                END ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 1) <= 10 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 1) >= 1 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 2) = 2 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 2)=2   '::text
                END AS reason
           FROM $model.tz_bc_singularity new_1
          WHERE NOT new_1.validity
        )
    select CONCAT('node:', n.id) as
        id,
        validity,
        name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom,
        reason from $model._node as n join node_invalidity_reason as r on r.id=n.id
        where validity=FALSE
    union
    select CONCAT('link:', l.id) as
        id,
        validity,
        name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom,
        reason from $model._link as l join link_invalidity_reason as r on  r.id=l.id
        where validity=FALSE
    union
    select CONCAT('singularity:', s.id) as
        id,
        s.validity,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        reason from $model._singularity as s join $model._node as n on s.node = n.id join singularity_invalidity_reason as r on r.id=s.id
        where s.validity=FALSE
    union
    select CONCAT('profile:', p.id) as
        id,
        p.validity,
        p.name,
        'profile'::varchar as type,
        ''::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        ''::varchar as reason from $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where p.validity=FALSE
;;

