/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

create function notice(msg text, param anyelement)
returns anyelement
language plpgsql
immutable
as
$$$$
    begin
        raise notice '% %', msg, param;
        return param;
    end;
$$$$
;;

/* ********************************************** */
/*  Linear geometry tools                         */
/* ********************************************** */

create type vector2 as (x double precision, y double precision)
;;

-- difference of two points yielding a vector
create function difference(a geometry, b geometry)
returns vector2
language plpgsql immutable set search_path to public
as
$$$$
    begin
        return row(st_x(a)-st_x(b), st_y(a)-st_y(b))::vector2;
    end;
$$$$
;;

-- dot product of two points considered to be vectors
create function dot(u vector2, v vector2)
returns float
language plpgsql immutable
as
$$$$
    begin
        return u.x*v.x + u.y*v.y;
    end;
$$$$
;;

-- normalized vector
create function normalized(u vector2)
returns vector2
language plpgsql immutable set search_path to public
as
$$$$
    declare
        norm double precision;
    begin
        norm := sqrt(dot(u, u));
        if norm < 1e-9 then
            return u;
        end if;
        return row(u.x/norm, u.y/norm)::vector2;
    end;
$$$$
;;

-- angular interpolation of direction of two lines at a point
-- either points to intersection or // to both
create function interpolate_direction(a geometry, b geometry, point geometry)
returns vector2
language plpgsql immutable set search_path to public
as
$$$$
    declare
        det double precision;
        u vector2;
        v vector2;
        d vector2;
        alpha double precision;
        res vector2;
    begin
    -- let a and b be the start points of the segments and u and v their directions,
    -- intesection point satisfies:

    -- a + alpha*u = b + beta*v

    -- ax + alpha*ux = bx + beta*vx
    -- ay + alpha*uy = by + beta*vy

    -- or

    -- alpha*ux - beta*vx = bx - ax
    -- alpha*uy - beta*uy = by - ay

    -- we define: d = b - a

    -- [ ux   -vx ][ alpha ] = [ d.x ]
    -- [ uy   -vy ][ beta  ] = [ d.y ]

    -- det = -ux*vy + uy*vx
    -- comT = [ -vy  vx ]
    --        [ -uy  ux ]


    -- [ alpha ] = 1/det comT * [ bx-ax ] = 1/det  [ -vy  vx ] * [ d.x ]
    -- [ beta  ]                [ by-ay ]          [ -uy  ux ]   [ d.y ]

        u := difference( st_endpoint(a), st_startpoint(a) );
        v := difference( st_endpoint(b), st_startpoint(b) );
        det = u.y*v.x - u.x*v.y;
        if abs(det) < 1.e-12 then -- lines are parallel
            --raise notice 'parallel %', det;
            return u;
        end if;
        d := difference( st_startpoint(b), st_startpoint(a) );
        alpha := (1./det)*(-v.y*d.x + v.x*d.y);
        --raise notice 'intersection % alpha % % % %', alpha, u, st_translate(st_startpoint(a), alpha*u.x, alpha*u.y), a, b;
        res := normalized(difference(st_translate(st_startpoint(a), alpha*u.x, alpha*u.y), point));
        if dot(res, u) > 0 then
            return res;
        else
            return row(-res.x, -res.y)::vector2;
        end if;
    end;
$$$$
;;

create function check_is_point(geom geometry)
returns geometry
language plpgsql immutable set search_path to public
as
$$$$
    begin
        if st_geometrytype(geom) = 'ST_Point' or geom is null then
            return geom;
        else
            raise '% is not a point but a %', st_astext(geom), st_geometrytype(geom);
            return geom;
        end if;
    end;
$$$$
;;

-- linear interpolation of array points (2d), t is in [0,1]
create function interpolate_array(u real[], v real[], t double precision)
returns real[]
language plpgsql
as
$$$$
    begin
        return (
            select array_agg(array[u[i][1] * (1. - t) + v[i][1] * t, u[i][2] * (1. - t) + v[i][2] * t])
            from generate_subscripts(u, 1) as i
        );
    end;
$$$$
;;

create function zb_cat(zb_maj_l real[], zb_min real[], zb_maj_r real[])
returns real[]
language plpgsql
immutable
as
$$$$
    begin
        --raise notice 'array % % % %', zb_maj_l, array_length(zb_maj_l ,1 ), zb_maj_l[1][:], array(select array[ zb_maj_l[array_length(zb_maj_l ,1 )][1], zb_maj_l[array_length(zb_maj_l ,1 )][2]] from generate_series(array_length(zb_maj_l, 1)+1, 4));
        return array_cat(
            array_cat(
                zb_maj_l,
                array(select array[zb_maj_l[array_length(zb_maj_l ,1 )][1], zb_maj_l[array_length(zb_maj_l ,1 )][2]] from generate_series(array_length(zb_maj_l, 1)+1, 4))
            ),
            array_cat(
                array_cat(
                    zb_min,
                    array(select array[zb_min[array_length(zb_min ,1 )][1], zb_min[array_length(zb_min ,1 )][2]] from generate_series(array_length(zb_min , 1)+1, 6))
                ),
                array_cat(
                    zb_maj_r,
                    array(select array[zb_maj_r[array_length(zb_maj_r ,1 )][1], zb_maj_r[array_length(zb_maj_r ,1 )][2]]  from generate_series(array_length(zb_maj_r , 1)+1, 4))
                )
            )
        );
    end;
$$$$
;;
