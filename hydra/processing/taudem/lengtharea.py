# -*- coding: utf-8 -*-

"""
***************************************************************************
    lengtharea.py
    ---------------------
    Date                 : June 2012
    Copyright            : (C) 2012-2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'June 2012'
__copyright__ = '(C) 2012-2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.core import (QgsProcessingException,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterRasterDestination
                      )

from .taudemAlgorithm import TauDemAlgorithm
from . import taudemUtils


class LengthArea(TauDemAlgorithm):

    LENGTH = 'LENGTH'
    CONTRIB_AREA = 'CONTRIB_AREA'
    THRESHOLD = 'THRESHOLD'
    EXPONENT = 'EXPONENT'
    STREAM_SOURCE = 'STREAM_SOURCE'

    def name(self):
        return 'lengtharea'

    def displayName(self):
        return self.tr('Length area stream source')

    def group(self):
        return self.tr('Stream network analysis')

    def groupId(self):
        return 'streamanalysis'

    def tags(self):
        return self.tr('dem,hydrology,dem,threshold,compare').split(',')

    def shortHelpString(self):
        return self.tr('Creates an indicator grid (1, 0) that evaluates '
                       'A >= M·(L^y) based on upslope path length, D8 '
                       'contributing area grid inputs, and parameters M and y.')

    def helpUrl(self):
        return 'http://hydrology.usu.edu/taudem/taudem5/help53/LengthAreaStreamSource.html'

    def __init__(self):
        super().__init__()

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterRasterLayer(self.LENGTH,
                                                            self.tr('Maximum upslope length')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.CONTRIB_AREA,
                                                            self.tr('Contributing area')))
        self.addParameter(QgsProcessingParameterNumber(self.THRESHOLD,
                                                       self.tr('Multiplier (M)'),
                                                       QgsProcessingParameterNumber.Double,
                                                       defaultValue=0.03))
        self.addParameter(QgsProcessingParameterNumber(self.EXPONENT,
                                                       self.tr('Exponent (y)'),
                                                       QgsProcessingParameterNumber.Double,
                                                       defaultValue=1.3))

        self.addParameter(QgsProcessingParameterRasterDestination(self.STREAM_SOURCE,
                                                                  self.tr('Stream source')))

    def processAlgorithm(self, parameters, context, feedback):
        lengthLayer = self.parameterAsRasterLayer(parameters, self.LENGTH, context)
        if slopeLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.LENGTH))

        areaLayer = self.parameterAsRasterLayer(parameters, self.CONTRIB_AREA, context)
        if areaLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.CONTRIB_AREA))

        arguments = []
        arguments.append(self.command())

        arguments.append('-plen')
        arguments.append(lengthLayer.source())
        arguments.append('-ad8')
        arguments.append(areaLayer.source())

        arguments.append('-par')
        arguments.append('{}'.format(self.parameterAsDouble(parameters, self.THRESHOLD, context)))
        arguments.append('{}'.format(self.parameterAsDouble(parameters, self.EXPONENT, context)))

        arguments.append('-ss')
        arguments.append(self.parameterAsOutputLayer(parameters, self.STREAM_SOURCE, context))

        taudemUtils.execute(arguments, feedback)

        return self.algorithmResults(parameters)
