# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
run terrain tests

USAGE

    terrain_test [-hdk] [files]

    if files is specified, create terrain and hilshade from them

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode

   -k, --keep
        keep test database


"""

import os
import sys
import numpy
from shutil import rmtree
from tempfile import mkdtemp
from getopt import getopt
from hydra.project import Project
from hydra.utility.log import LogManager
from hydra.utility.timer import Timer
from hydra.utility.log import LogManager, ConsoleLogger
from hydra.database.terrain import Terrain
from hydra.database.database import TestProject, project_exists, remove_project, project_dir

try:
    optlist, args = getopt(sys.argv[1:],
            "hdk",
            ["help", "debug", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)
debug = "-d" in optlist or "--debug" in optlist
keep = "-k" in optlist or "--keep" in optlist

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

project_name = "terrain_test"

if project_exists(project_name):
    remove_project(project_name)
if os.path.exists(project_dir(project_name)):
    rmtree(project_dir(project_name))

test_project = TestProject(project_name, keep)
project = Project.load_project(project_name)

project.debug = debug

data_dir = os.path.join(os.path.dirname(__file__), 'test_data')

terrain = Terrain(os.path.join(project.directory, 'terrain'), project)

assert(not terrain.has_data())

terrain.build_vrt(args or [os.path.join(data_dir, '2013_012.asc'),
                          os.path.join(data_dir, '2013_013.asc')])

project.commit()

assert(not len(terrain.error_files()))
assert(terrain.has_data())

assert(abs(project.execute("""select project.altitude(997159.2,6559643.7)""").fetchone()[0] - 1049) < 1)
assert(project.execute("""select project.altitude(0, 0)""").fetchone()[0] == 9999)

if debug:
    # fix_print_with_import
    print("testing rotation")
    import matplotlib.pyplot as plt

    l1 = ((997259.236097043, 6559019.59459126), (997166.65145224, 6559196.08407041))
    l2 = ((997108.786049238, 6559291.56198537), (997069.726902212, 6559463.7115593))
    l3 = ((997046.580741011, 6559592.46208098), (997217.283679867, 6559541.82985335))

    for l in [l1, l2, l3]:
        timer = Timer()
        img = terrain.altitudes(l, 50)
        if img is None:
            continue
        print(numpy.mean(numpy.amax(img, 0)))
        print(timer.reset('total'))
        #img = terrain.altitudes(l2, 50)
        #img = terrain.altitudes(l3, 50)
        plt.figure(1)
        ax = plt.subplot(221)
        #ax.imshow(orig)
        #ax.plot(corners[:,1], corners[:,0])
        ax = plt.subplot(222)
        ax.imshow(img)
        ax = plt.subplot(223)
        ax.plot(numpy.sort(numpy.amax(img, 1)))
        ax = plt.subplot(224)
        ax.plot(numpy.amax(img, 0))
        plt.show()

#import matplotlib.pyplot as plt
#
#terrain = Terrain('/home/vmo/.hydra/mesh/terrain', 2154, LogManager())
#img, orig, corners =terrain.altitudes(((998474.561575991, 2402692.80549499), (998489.64507541, 2402692.88504641)), 1.50837091974)
#plt.figure(1)
#ax = plt.subplot(121)
#ax.imshow(orig)
#ax.plot(corners[:,1], corners[:,0])
#ax = plt.subplot(122)
#ax.imshow(img)
#plt.show()

# test line_elevation
l = [ [ 997011.646373958908953, 6559797.931034421548247 ], [ 997068.246161273564212, 6559857.266763117164373 ] ]
#l = [ [ 997028.57501125219278, 6559817.638362178578973 ], [ 997028.57501125219278, 6559830.676832822151482 ] ]
#l = [ [ 997028.209410561714321, 6559817.321083893999457 ], [ 997028.230785102699883, 6559818.368436404503882 ] ]
#l = [ [ 997027.598878311226144, 6559817.586521125398576 ], [ 997028.462964231730439, 6559817.605723034590483 ], [ 997028.411759140202776, 6559816.741637113504112 ], [ 997027.650083402870223, 6559816.754438387230039 ] ]
lz = terrain.line_elevation(l)
assert(l[0][0] == lz[0][0])
assert(l[0][1] == lz[0][1])

s = []
z = []
zp = []
for p in lz:
    s.append(numpy.linalg.norm(p[:2] - lz[0,:2]))
    z .append(p[2])
    zp.append(terrain.altitude(p))


# import matplotlib.pyplot as plt
# from shapely.geometry import MultiPoint
# print(MultiPoint(lz).wkt)

# ax = plt.subplot(111)
# ax.plot(s, z)
# ax.plot(s, zp)
# plt.show()


if keep:
    project.commit()

print('ok')

