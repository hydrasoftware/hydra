# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.database.d_domain_test [-dh]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -k, --keep
        does not delete DB at the end
"""

from __future__ import absolute_import # important to read the doc !
from __future__ import print_function
from builtins import str
from builtins import range
import os, json
import sys
import shutil
import getopt
from hydra.project import Project
from hydra.utility.log import LogManager, ConsoleLogger
from hydra.utility import empty_ui_manager as ui_manager
from hydra.database.database import TestProject, project_exists, remove_project

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdk",
            ["help", "debug", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

keep = True if "-k" in optlist or "--keep" in optlist else False

if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

project_name = "model_test"

if project_exists(project_name):
    remove_project(project_name)

model_name = "model1"
srid = 2154

test_project = TestProject(project_name, keep)

obj_project = Project.load_project(project_name)
if model_name in obj_project.get_models():
    obj_project.delete_model(model_name)

obj_project.add_new_model(model_name)
obj_project.set_current_model(model_name)

md = obj_project.get_current_model()

#clean model
def delete_all_from_table(table):
    obj_project.execute("delete from {}.{}".format(model_name, table))

delete_all_from_table("pipe_link")
delete_all_from_table("closed_parametric_geometry")
delete_all_from_table("open_parametric_geometry")
delete_all_from_table("marker_singularity")

delete_all_from_table("river_cross_section_profile")
delete_all_from_table("hydrograph_bc_singularity")
delete_all_from_table("froude_bc_singularity")
delete_all_from_table("gate_singularity")
delete_all_from_table("regul_sluice_gate_singularity")
delete_all_from_table("manhole_node")
delete_all_from_table("catchment_node")

#add nodes
md.add_manhole([1320.00, 1248260.00,99999])
md.add_manhole([6360.00, 1248068.00,99999])
md.add_manhole([1872.00, 1246448.00,99999])
md.add_manhole([4296.00, 1248128.00,99999])
md.add_manhole([2640.00, 1248188.00,99999])
md.add_manhole([5076.00, 1248116.00,99999])
md.add_manhole([3048.00, 1247240.00,99999])
md.add_manhole([7285., 1248501., 99999])
md.add_manhole([7393., 1246353., 99999])
md.add_manhole([151,251,999])
md.add_manhole([1789,12345,9999])
md.add_manhole([145488,8463459,9999])
md.add_manhole([145,84659, 99999])
md.add_manhole([1478,84659, 99999])
md.add_manhole([1478,84645, 99999])

md.add_manhole([17994,123,9999])

md.add_singularity_mrkb([17994,123], 0, 50)

id_manhole = md.add_manhole([7289., 1248502., 99999])
md.update_manhole(id_manhole, [7289., 1248502., 99999], 45)

#add link
id_pipe = md.add_pipe_link([[3048, 1247240], [4296, 1248128]], 99.52, 99, 'circular', 1)
id_pipe = md.add_pipe_link([[1872, 1246448], [3048, 1247240]], 100, 99.52, 'ovoid', 0)

id_pipe = md.add_pipe_link([[5076, 1248116], [6360, 1248068]], 98.62, 98,  'channel', 0)
id_pipe = md.add_pipe_link([[4296, 1248128], [5076, 1248116]], 99, 98.62, 'pipe', 2)

id_pipe = md.add_pipe_link([[2640, 1248188], [4296, 1248128]], 99.56, 99, 'circular', 5)

id_pipe2 = md.add_pipe_link([[1320, 1248260], [2640, 1248188]], 100, 99.56)
obj_project.execute("delete from {}.pipe_link where id={};".format(md.name, id_pipe2))

#MESH
id_c = md.add_constrain([[0,0],[.5,0],[1,0]])
md.update_constrain(id_c, elem_length=50)

id_d = md.add_domain_2d()
md.update_domain_2d(id_d)

id_e = md.add_elem_2d_node([[(0,0),(1,0),(1,1),(0,1),(0,0)]], 99, 12, id_d)
md.update_elem_2d_node(id_e, [[(0,0),(1,0),(1,1),(0,1),(0,0)]], 100, 12.6548, id_d)

# bief = md.get_attrib_from_table_byname("reach", "B1")
# name,dx, pk0 = 'B1', 50.0, 0.0
md.add_reachpath([['1188.00', '1248476.00'], ['2124.00', '1248476.00'], ['2688.00', '1248464.00'],
    ['3360.00', '1248488.00'], ['4308.00', '1248476.00']],50., 0., 'B1')
md.add_reachpath([['4668.00', '1248476.00'], ['5352.00', '1248488.00'], ['5928.00', '1248500.00'],
    ['6696.00', '1248536.00'], ['7284.00', '1248500.00']], 50., 1., 'B3')
md.add_reachpath([['1560.00', '1246520.00'], ['2544.00', '1247084.00'], ['3060.00', '1247396.00'],
    ['3780.00', '1247792.00'], ['4320.00', '1248116.00']], 50., 0., 'B2')

md.add_reachpath([['24.56','56.24'], ['43.00', '1248.00']], 50., 0., 'B52')
assert md.try_merge_reach([['43.00', '1248.00'], ['124.56','156.24']])==True
assert md.try_merge_reach([['243.00', '2248.00'], ['24.56','56.24']])==True
assert md.try_merge_reach([['243.00', '2248.00'], ['203.00', '2208.00']])==False
assert md.try_merge_reach([['224.56','256.24'], ['124.56','156.24']])==False

id_reach_path = md.add_reachpath([['5124.', '1247924.'], ['5417.', '1247722.'], ['5652.', '1247564.'],
    ['6216.', '1247204.'], ['6588.', '1246916.'], ['7392.', '1246352.']], 50., 0., 'B4')
md.update_reachpath(id_reach_path, [['5124.', '1247924.'], ['5417.', '1247722.'], ['5652.', '1247564.'],
    ['6216.', '1247204.'], ['6588.', '1246916.'], ['7392.', '1246352.']], 51., 12., 'B4')

# print 'river_node'
md.add_river_node(['2124.00', '1248476.00'], 100., 1., 'NOD_12')
md.add_river_node(['2688.00', '1248464.00'], 100., 1., 'NOD_7')
md.add_river_node(['3360.00', '1248488.00'], 100., 1., 'NOD_13')

md.add_river_node(['5352.00', '1248488.00'], 100.0, 1.0, 'NOD_14')
md.add_river_node(['5928.00', '1248500.00'], 100.0, 1.0, 'NOD_8')
md.add_river_node(['6696.00', '1248536.00'], 100.0, 1.0, 'NOD_15')

md.add_river_node(['2544.00', '1247084.00'], 100.0, 1.0, 'NOD_11')
md.add_river_node(['3060.00', '1247396.00'], 100.0, 1.0, 'NOD_9')

id_nod_10 = md.add_river_node(['3780.00', '1247792.00'], 100.0, 1.0, 'NOD_10')
assert obj_project.execute("select reach from {}.river_node where id={};".format(md.name, id_nod_10)).fetchone()[0] == 3

  #move this node on reach B4
obj_project.execute("update {}.river_node set geom={} where id={};".format(md.name, md.make_point(['6000', '1247341.87']), id_nod_10))
assert obj_project.execute("select reach from {}.river_node where id={};".format(md.name, id_nod_10)).fetchone()[0] == 5

# river_cross_section_profile
id_cross = md.add_river_cross_section_profile([1188.00,1248476.00], None, 100., 'circular', 'circular')
id_cross = md.add_river_cross_section_profile([4308.,1248476.], 99., None, 'circular', 'circular')

# QMV
md.add_manhole([12,12])
md.add_manhole([13,13])
id_qmv = md.add_link_qmv([[12,12],[13,13]],10, 15, 22, 0.8, 1.62, "one_way_downstream", "downward_opening", 12.5, 0.88)
md.update_link_qmv(id_qmv,[[12,12],[13,13]],11, 16, 22, 0.8, 1.68,  "one_way_upstream", "upward_opening", 13.5, 0.88)

# QMS
md.add_manhole([-12,-12])
md.add_manhole([-13,-13])
id_qms = md.add_link_qms([[-12,-12],[-13,-13]], 999, 99, 0.01, 998, 0.6)
md.update_link_qms(id_qms,[[-12,-12],[-13,-13]], 99, 9, 0.01, 98, 0.6)

#SECT from test3.hriv modele
id_open_geom = md.add_pipe_channel_open_section_geometry('{{1.0, 0.0}, {3.0, 0.5}, {3.001, 2.5}, \
        {999, 999}, {999, 999}, {999, 999}, {999, 999}, {999, 999}, {999, 999}, {999, 999}}', 'Dalot1')
id_vcs_geom = md.add_valley_section_geometry('{{98.0, 3.0}, {98.5, 5.0}, {101.0, 6.0}, {102.0, 6.5}, {103.0, 7.0}, {104.0, 10.0}}', '{{104.2, 65.0}, {105.0, 200.0}, {106.0, 400.0}, {110.0, 800.0}}', \
        '{{104.2, 50.0}, {105.0, 100.0}, {106.0, 200.0}, {110.0, 500.0}}', '0.50000', '0.65000', '0.85000', '104.25', '104.25\n', 'VL1')
id_closed_geom = md.add_pipe_channel_closed_section_geometry('{{1.0, 0.0}, {3.0, 0.6}, {3.001, 2.4}, \
        {999, 999}, {999, 999}, {999, 999}, {999, 999}, {999, 999}, {999, 999}, {999, 999}}', 'closed_geom_1')
xzArr=[[999.,999.] for r in range(2)]
id_vcs_topogeom = md.add_valley_section_topo_geometry(xzArr, 'VL_1')

#PTR from test3.hriv modele
md.add_river_cross_section_profile(['7284.00', '1248500.00'], '98', None, "circular", "circular")
md.add_river_cross_section_profile(['1560.00', '1246520.00'], None, '100', "circular", "circular")

# print 'Simple link'
is_s_link = md.add_simple_link('connector_link', [[4308., 1248476.], [4668., 1248476.]], 'CONF_1')
md.update_simple_link('connector_link', is_s_link, [[4308., 1248476.], [4668., 1248476.]], 'CONF_2')

# print 'parametric syngularity'
md.add_hydraulic_cut_singularity(['6000', '1247341.87'],
    [[0.0, 99.0], [5.0, 100.0], [10.0, 100.5]], True, 'CPR_1')
md.add_param_headloss_singularity(['6696.00', '1248536.00'],
    [[0.0, 0.0], [2.0, 0.1], [10.0, 0.5]], False, 'DH_1')
md.add_zq_bc_singularity(['7392.00', '1246352.00'],
    [[0.0, 99.0], [5.0, 100.0], [10.0, 101.0]], 'CLZQ_1')
md.add_tz_bc_singularity(['7393.00', '1246353.00'],
    [[0.0, 99.0], [5.0, 100.0], [10.0, 101.0]], True, False, 'CLZT_1')
md.add_tank_bc_singularity(['4308.00', '1248476.00'],
    [[97.0, 1050.2], [98.0, 1700.5], [99.5, 2000.0]], 97, None, None, 'CLBO_1')

#borda headloss
md.add_singularity_smk([151,251], 'q', json.dumps({"params":{"coef_kq_s2_m_minus_5": 50}}))

#zregul_weir_sing
id_zregul_sing = md.add_singularity_de([1789,12345] , 5, 4, 22, 0.7,'elevation',
                    'gameson', json.dumps({"r15": {"reoxygen_coef": 15.0, "temperature_correct_coef": 16.0}, "gameson": {"waterfall_coef": 2.0, "adjust_coef": 3.0, "pollution_coef": 1.0}}))
md.update_singularity_de(id_zregul_sing, [1789,12345] , 6, 8, 2333, 0.8,'width',
                    'r15', json.dumps({"r15": {"reoxygen_coef": 15.0, "temperature_correct_coef": 16.0}, "gameson": {"waterfall_coef": 2.0, "adjust_coef": 3.0, "pollution_coef": 1.0}}))

#MKP
id_mkp = md.add_marker_singularity([2544., 1247084.], 'MRKP_1', 'comment')
md.update_marker_singularity(id_mkp,[2544., 1247084.], 'MRKP_2', 'comment2')

#froude_bc
id_bc = md.add_simple_singularity('froude_bc_singularity',[7284., 1248500.], 'CLZF_1')
md.update_simple_singularity('froude_bc_singularity', id_bc, [7284., 1248500.], 'CLZF_3')

#CI
id_ci = md.add_ci_singularity([7285., 1248501.], 12)
md.update_ci_singularity(id_ci, [7285., 1248501.], 13)

#CI
id_hy = md.add_hy_singularity([7289., 1248502.], None, 22)
md.update_hy_singularity(id_hy, [7289., 1248502.], None, 23)

#gate
id_va = md.add_singularity_va([2124., 1248476.], 100., 101., 1.2, 0.6, 5.46,  full_section_discharge_for_headloss=True, name='VA_1')
md.update_singularity_va(id_va, [2124., 1248476.], 103., 102., 1.2, 0.7, 0.222, full_section_discharge_for_headloss=False, name='VA_2')

#DL
id_dl = md.add_singularity_dl([145488,8463459], 15, 3354, 0.777)
md.update_singularity_dl(id_dl, [145488,8463459], 189, 334, 0.77)

#RK
id_rk = md.add_singularity_rk([145,84659], 0.015, 17.25, 354)
md.update_singularity_rk(id_rk, [145,84659], 0.05, 17.5, 364)

#HC
id_hc = md.add_singularity_hc([1478,84659], 0.015)
md.update_singularity_hc(id_hc, [1478,84659], 0.05)

#RACC
id_racc = md.add_singularity_racc([1478,84645], 'zq_downstream_condition', [[0.0, 99.2], [0.5, 99.5], [4.0, 99.85]], [[0.0, 1], [0.5, 3], [4.0, 5]])
md.update_singularity_racc(id_racc, [1478,84645], 'hydrograph', [[0.0, 99.2], [0.5, 99.5], [4.0, 99.85]], [[0.0, 1], [0.5, 3], [4.0, 5]])

#RGB:
control_node = obj_project.execute("select id from {}._node where st_force2d(geom)='SRID=2154; POINT(4668 1248476)'::geometry limit 1".format(model_name)).fetchone()[0]
md.add_singularity_acta([4668.00, 1248476.00], '0', '0.500', '1.000', '0.60', '1.89', 'downward_opening', '0.50000', '1', '10', '1', \
        'elevation', control_node, [0.500, 0.650, 0.000], [[0.0, 99.2], [0.5, 99.5], [4.0, 99.85]], full_section_discharge_for_headloss=True, name='RGB_1')

#bradley
md.add_singularity_brd([24.56,56.24], [[0.0, 99.2], [0.5, 99.5], [4.0, 99.85]], 12, 15, 'rounded',  20.467)

#BV,catchment_node
area_ha, rl, slope, c_imp = 23.5, 800., 0.015, 0.50
netflow_type = 'constant_runoff'
runoff_type = 'Desbordes 1 Cr'
q_limit,q0= 5.,0.2

maxId=md.add_catchment_node(
    geom=[4256., 1248629.],
    name='BV1',
    area_ha=area_ha,
    rl=rl,
    slope=slope,
    c_imp=c_imp,
    netflow_type=netflow_type,
    constant_runoff=0.35,
    horner_ini_loss_coef=None,
    horner_recharge_coef=None,
    holtan_sat_inf_rate_mmh=None,
    holtan_dry_inf_rate_mmh=None,
    holtan_soil_storage_cap_mm=None,
    scs_j_mm=160,
    scs_soil_drainage_time_day=10,
    scs_rfu_mm=55,
    hydra_surface_soil_storage_rfu_mm=None,
    hydra_inf_rate_f0_mm_hr=None,
    hydra_int_soil_storage_j_mm=None,
    hydra_soil_drainage_time_qres_day=None,
    hydra_soil_infiltration_type=None,
    gr4_k1=None,
    gr4_k2=None,
    gr4_k3=None,
    gr4_k4=None,
    runoff_type=runoff_type,
    socose_tc_mn=None,
    socose_shape_param_beta=None,
    define_k_mn=60,
    q_limit=q_limit,
    q0=q0)
md.add_manhole_hydrology([4250., 1248637., 9999])
id_routing_hyd = md.add_routing_hydrology_link([[4256., 1248629],[4250., 1248637.]], 2.5, 0.015, None, 0.5)
md.update_routing_hydrology_link(id_routing_hyd, [[4256., 1248629],[4250., 1248637.]], 2, 0.01, 59, 0.7)

# TODO : Other hydrology elements to test
# TODO : tests on cross sections
# TODO : tests on links

#station
id_stn = md.add_station([[(10,10),(-10, 10),(-10, -10), (10, -10), (10,10)]])
md.add_station_node([5,5], 999, 1)
id_stn_node = md.add_station_node([-5,-5],  999, 1)
md.update_station_node(id_stn_node, [-5,-5], 1001, 1.5)

#storage
id_storage = md.add_storage_node([55,35], [[0.0, 99.2], [0.5, 99.5], [4.0, 99.85]], 0)
md.update_storage_node(id_storage, [55,35], [[0.0, 98], [0.5, 99], [4.0, 100]], 0.5)

# DATA FOR ADVANCED PIPES EDITION
md.add_manhole([-32,98])
md.add_manhole([-33,99])
md.add_manhole([-35,97])
md.add_manhole([-30,89])
node_1 = md.add_manhole([-34,95])

md.add_pipe_link([[-32,98], [-33,99]], 99, 98)
md.add_pipe_link([[-33,99], [-35,97]], 100, None)
md.add_pipe_link([[-35,97], [-30,89]], None, 97)
md.add_pipe_link([[-30,89], [-34,95]])
md.add_pipe_link([[-34,95], [-32,98]], 89, 67)

md.add_manhole_hydrology([36,46])
node_3 = md.add_manhole_hydrology([38,50])
md.add_manhole_hydrology([37,52])

md.add_pipe_link([[36,46], [38,50]], 97, 95)
md.add_pipe_link([[38,50], [37,52]], 67, None)
md.add_pipe_link([[37,52], [36,46]], None , 89)

# insert node
md.insert_node_on_pipe([-32.5,98.5])
node_2, type_2 = md.insert_node_on_pipe([-32,92])
md.insert_node_on_pipe([-32.5,93])

md.insert_node_on_pipe([37,48])
node_4, type_4 = md.insert_node_on_pipe([37.5,51])

# delete node
md.delete_node_update_pipe(node_1, 'manhole_node')
md.delete_node_update_pipe(node_2, 'manhole_node')

md.delete_node_update_pipe(node_3, 'manhole_hydrology_node')
md.delete_node_update_pipe(node_4, 'manhole_hydrology_node')

# DATA FOR ADVANCED REACH EDITION

r1 = md.add_reachpath([[94,84], [77,89], [51,67], [57,49]])
r2 = md.add_reachpath([[67,19], [84,59], [57,67]])

md.add_river_node([67,78])
md.add_river_cross_section_profile([67,78], None, 50., 'circular', 'circular')
md.add_singularity_va([57,67], 10., 11., 1.9, 0.6)
md.add_river_node([51,67])
md.add_link_qms([[51,67],[57,67]], 57, -5, 0.51, 98, 1)

# merge reaches
r3 = md.fuse_reach(r1, r2)
md.split_reach(r3, 0.45)

md.topo_check()


obj_project.commit()

wkt, = obj_project.execute("select st_astext(st_snaptogrid(geom, 0.1)) from {}.constrain".format(md.name)).fetchone()
assert wkt == 'LINESTRING Z (0 0 9999,0.5 0 9999,1 0 9999)'
# fix_print_with_import

# fix_print_with_import
print("ok")
