/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  Update of domain_2d                           */
/* ********************************************** */

alter table ${model}.domain_2d
    add column if not exists comment varchar(256);
;;

create table if not exists $model.domain_2d_marker(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('DOMAIN_MARKER_'),
    domain_2d integer references $model.domain_2d(id),
    geom geometry('POINTZ',$srid) not null,
    comment varchar(256)
)
;;

-- Add rule to update elem 2d after mkd modification
create or replace function ${model}.mkd_after_fct()
returns trigger
language plpgsql
as $$$$
    declare
        old_cov integer;
        new_cov integer;
    begin
        if tg_op = 'INSERT' then
            select id from $model.coverage as c where st_intersects(c.geom, new.geom) into new_cov;
            update $model.elem_2d_node as n set domain_2d = new.domain_2d from $model.coverage as c where st_intersects(n.geom, c.geom) and c.id = new_cov;
            return new;
        else
            if tg_op = 'UPDATE' then
                select id from $model.coverage as c where st_intersects(c.geom, old.geom) into old_cov;
                update $model.elem_2d_node as n set domain_2d = new.domain_2d from $model.coverage as c where st_intersects(n.geom, c.geom) and c.id = old_cov;
                select id from $model.coverage as c where st_intersects(c.geom, new.geom) into new_cov;
                update $model.elem_2d_node as n set domain_2d = new.domain_2d from $model.coverage as c where st_intersects(n.geom, c.geom) and c.id = new_cov;
                return new;
            else
                select id from $model.coverage as c where st_intersects(c.geom, old.geom) into old_cov;
                update $model.elem_2d_node as n set domain_2d = new.domain_2d from $model.coverage as c where st_intersects(n.geom, c.geom) and c.id = old_cov;
                return old;
            end if;
        end if;
    end;
$$$$
;;

drop trigger if exists ${model}_mkd_after_trig on ${model}.domain_2d_marker;
;;

create trigger ${model}_mkd_after_trig
    after insert or update of geom, domain_2d or delete on $model.domain_2d_marker
       for each row execute procedure ${model}.mkd_after_fct()
;;

drop trigger if exists ${model}_domain_2d_after_trig on ${model}.domain_2d;
;;

drop function if exists ${model}.domain_2d_after_fct();
;;

/* ********************************************** */
/*  Fix of view L2D                               */
/* ********************************************** */

create or replace view ${model}.l2d
as
with link as (
    select l.name, project.crossed_border(l.geom, n.contour) as geom
    from ${model}._link as l, ${model}.elem_2d_node as n
    where (n.id = l.up or n.id=l.down)
    and l.link_type!='mesh_2d'
    and l.link_type!='network_overflow'
    and st_intersects(l.geom, st_exteriorring(n.contour)))
select name, st_x(st_startpoint(geom)) as xa, st_y(st_startpoint(geom)) as ya, st_x(st_endpoint(geom)) as xb, st_y(st_endpoint(geom)) as yb from link
;;

/* ********************************************** */
/*  Fix of PTR node switch                        */
/* ********************************************** */

create or replace function ${model}.river_cross_section_profile_fct()
returns trigger
language plpgsql
as $$$$
    begin
        select (
            (new.type_cross_section_up is null or (
                new.z_invert_up is not null
                and new.up_rk is not null
                and (new.type_cross_section_up!='circular' or (new.up_circular_diameter is not null and new.up_circular_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_top_diameter is not null and new.up_ovoid_top_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_invert_diameter is not null and new.up_ovoid_invert_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_height is not null and new.up_ovoid_height >0 ))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_height > (new.up_ovoid_top_diameter+new.up_ovoid_invert_diameter)/2))
                and (new.type_cross_section_up!='pipe' or new.up_cp_geom is not null)
                and (new.type_cross_section_up!='channel' or new.up_op_geom is not null)
                and (new.type_cross_section_up!='valley'
                        or (new.up_rk = 0 and new.up_rk_maj = 0 and new.up_sinuosity = 0)
                        or (new.up_rk >0 and new.up_rk_maj is not null and new.up_rk_maj > 0 and new.up_sinuosity is not null and new.up_sinuosity > 0))
                and (new.type_cross_section_up!='valley' or new.up_vcs_geom is not null)
                )
            )
            and
            (new.type_cross_section_down is null or (
                new.z_invert_down is not null
                and new.down_rk is not null
                and (new.type_cross_section_down!='circular' or (new.down_circular_diameter is not null and new.down_circular_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_top_diameter is not null and new.down_ovoid_top_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_invert_diameter is not null and new.down_ovoid_invert_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_height is not null and new.down_ovoid_height >0 ))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_height > (new.down_ovoid_top_diameter+new.down_ovoid_invert_diameter)/2))
                and (new.type_cross_section_down!='pipe' or new.down_cp_geom is not null)
                and (new.type_cross_section_down!='channel' or new.down_op_geom is not null)
                and (new.type_cross_section_down!='valley'
                        or (new.down_rk = 0 and new.down_rk_maj = 0 and new.down_sinuosity = 0)
                        or (new.down_rk >0 and new.down_rk_maj is not null and new.down_rk_maj > 0 and new.down_sinuosity is not null and new.down_sinuosity > 0))
                and (new.type_cross_section_down!='valley' or new.down_vcs_geom is not null)
                )
            )
            and
            (new.type_cross_section_up is null or new.type_cross_section_down is null or (
                new.z_invert_up is not null and new.z_invert_down is not null
                )
            )
        ) into new.validity;

        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id from $model.river_node where ST_DWithin(new.geom, geom, .1) into new.id;
            select coalesce(new.name, 'CP'||new.id::varchar) into new.name;
            if new.id is null then
                raise exception 'no river node nearby river_cross_section_profile %', st_astext(new.geom);
            end if;
            insert into $model._river_cross_section_profile
                values (
                    new.id,
                    new.name,
                    new.z_invert_up,
                    new.z_invert_down,
                    new.type_cross_section_up,
                    new.type_cross_section_down,
                    new.up_rk,
                    new.up_rk_maj,
                    new.up_sinuosity,
                    new.up_circular_diameter,
                    new.up_ovoid_height,
                    new.up_ovoid_top_diameter,
                    new.up_ovoid_invert_diameter,
                    new.up_cp_geom,
                    new.up_op_geom,
                    new.up_vcs_geom,
                    new.up_vcs_topo_geom,
                    new.down_rk,
                    new.down_rk_maj,
                    new.down_sinuosity,
                    new.down_circular_diameter,
                    new.down_ovoid_height,
                    new.down_ovoid_top_diameter,
                    new.down_ovoid_invert_diameter,
                    new.down_cp_geom,
                    new.down_op_geom,
                    new.down_vcs_geom,
                    new.down_vcs_topo_geom,
                    new.validity
                );
            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id from $model.river_node where ST_DWithin(new.geom, geom, .1) into new.id;
            select coalesce(new.name, 'CP'||new.id::varchar) into new.name;
            if new.id is null then
                raise exception 'no river node nearby river_cross_section_profile %', st_astext(new.geom);
            end if;
            update $model._river_cross_section_profile set
                id=new.id,
                name=new.name,
                z_invert_up=new.z_invert_up,
                z_invert_down=new.z_invert_down,
                type_cross_section_up=new.type_cross_section_up,
                type_cross_section_down=new.type_cross_section_down,
                up_rk=new.up_rk,
                up_rk_maj=new.up_rk_maj,
                up_sinuosity=new.up_sinuosity,
                up_circular_diameter=new.up_circular_diameter,
                up_ovoid_height=new.up_ovoid_height,
                up_ovoid_top_diameter=new.up_ovoid_top_diameter,
                up_ovoid_invert_diameter=new.up_ovoid_invert_diameter,
                up_cp_geom=new.up_cp_geom,
                up_op_geom=new.up_op_geom,
                up_vcs_geom=new.up_vcs_geom,
                up_vcs_topo_geom=new.up_vcs_topo_geom,
                down_rk=new.down_rk,
                down_rk_maj=new.down_rk_maj,
                down_sinuosity=new.down_sinuosity,
                down_circular_diameter=new.down_circular_diameter,
                down_ovoid_height=new.down_ovoid_height,
                down_ovoid_top_diameter=new.down_ovoid_top_diameter,
                down_ovoid_invert_diameter=new.down_ovoid_invert_diameter,
                down_cp_geom=new.down_cp_geom,
                down_op_geom=new.down_op_geom,
                down_vcs_geom=new.down_vcs_geom,
                down_vcs_topo_geom=new.down_vcs_topo_geom,
                validity=new.validity
            where id=old.id;
            return new;
        end if;
    end;
$$$$
;;

