# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
hydra server-side interaction with terrain

GDAL imports are used locally because it is not needed to instal in some case (use without terrain)

"""

from math import sqrt, ceil, floor
import struct
import numpy
from numpy.linalg import norm
from shapely.ops import transform
from shapely import wkb

import shapely
if int(shapely.__version__.split('.')[0]) >=2:
    from shapely import get_srid, set_srid
else:
    from shapely import geos
    def set_srid(geom, srid):
        geos.lgeos.GEOSSetSRID(geom._geom, srid)
        return geom
    def get_srid(geom):
        return geos.lgeos.GEOSGetSRID(geom._geom)
from shapely.geometry import LineString, Polygon, Point
from PIL import Image, ImageDraw

def filling_curve(sources, polygon):
    """returns values clipped by polygon"""
    try:
        from osgeo import gdal
    except ImportError:
        import gdal

    for source in sources:
        src_ds = gdal.Open(source)
        if src_ds is None:
            raise Exception("'{}' not found".format(source)) # /!\ f string cannot be used in python < 3.6
        gt = src_ds.GetGeoTransform()
        p = transform(lambda x, y, z=None: ((x - gt[0])/gt[1], (y-gt[3])/gt[5], z), wkb.loads(polygon, True))
        orig = (int(round(p.bounds[0])-1), int(round(p.bounds[1])-1))
        rang = (int(round(p.bounds[2] - orig[0])+2), int(round(p.bounds[3]-orig[1])+2))
        img = Image.new("L", rang, 0)
        draw = ImageDraw.Draw(img)
        if p.geom_type == "Polygon":
            __draw_polygon(draw, p, orig)
        elif p.geom_type == "MultiPolygon":
            for geom in p.geoms:
                __draw_polygon(draw, geom, orig)
        mask = numpy.frombuffer(img.tobytes(), dtype=numpy.uint8).reshape(img.im.size[1], img.im.size[0])
        rb = src_ds.GetRasterBand(1).ReadRaster(int(orig[0]), int(orig[1]), int(rang[0]), int(rang[1]), buf_type=gdal.GDT_Float32)
        if rb is not None:
            height = numpy.frombuffer(rb,  dtype=numpy.float32).reshape(int(rang[1]), int(rang[0]))
            hmin, hmax = numpy.min(height[mask == 255]), numpy.max(height[mask == 255])
            if hmax < 9999:
                nsteps = 10
                dz = (hmax - hmin)/(nsteps-1)
                ds = abs(gt[1]*gt[5])
                sz = [[hmin+s*dz, ((height <= (hmin+s*dz)) & (mask == 255)).sum()*ds] for s in range(nsteps)]
                if sz[0][0]!=-9999:
                    return str(sz).replace('[','{').replace(']','}')
    return None

def __draw_polygon(draw, geom, orig):
    pix = [(round(x-orig[0]), round(y-orig[1])) for x, y, z in geom.exterior.coords]
    draw.polygon(pix, fill=255)
    for interior in geom.interiors:
        pix = [(round(x-orig[0]), round(y-orig[1])) for x, y, z in interior.coords]
        draw.polygon(pix, fill=0)

def altitudes(sources, segment, width, no_warning=False):
    """returns values clipped by rectangle defined by middle line and width
    sources are sorted by descending priority
    the image is rotated to be in segment/normal axes
    """
    try:
        from osgeo import gdal
    except ImportError:
        import gdal

    segment = numpy.array(segment)[:,:2]
    aspect_ratio = width/sqrt(numpy.sum(numpy.square(segment[1,:] - segment[0,:])))
    # move to pixel coordinates
    # x as the heigth direction and y as the width
    for source in sources:
        src_ds = gdal.Open(source)
        if src_ds is None:
            raise Exception("'{}' not found".format(source)) # /!\ f string cannot be used in python < 3.6
        gt = src_ds.GetGeoTransform()
        start, end = (segment - numpy.array([gt[0], gt[3]]))/numpy.array([gt[1], gt[5]]) # scale + translate
        start, end = numpy.array([start[1], start[0]]), numpy.array([end[1], end[0]]) # rotate 90deg clockwise
        direction = end - start
        length  = sqrt(numpy.sum(numpy.square(direction)))
        direction /= length
        normal = numpy.array([direction[1], -direction[0]])
        corners = numpy.array([start - normal*aspect_ratio*length/2,
                               start + normal*aspect_ratio*length/2,
                               end + normal*aspect_ratio*length/2,
                               end - normal*aspect_ratio*length/2])
        orig = numpy.array([floor(numpy.min(corners[:,0])), floor(numpy.min(corners[:,1]))])
        rang = numpy.array([ceil(numpy.max(corners[:,0]))+1, ceil(numpy.max(corners[:,1]))+1]) - orig
        corners -= orig # translate corners, because we floored origin
        rb = src_ds.GetRasterBand(1).ReadRaster(int(orig[1]), int(orig[0]), int(rang[1]), int(rang[0]), buf_type=gdal.GDT_Float32)
        if rb is not None:
            height = numpy.frombuffer(rb, dtype=numpy.float32).reshape(int(rang[0]), int(rang[1]))
            height = height.astype(float)  # convert to 64b
            dx, dy = int(ceil(length*aspect_ratio)), int(ceil(length))
            x, y = numpy.mgrid[:dx, :dy]
            idx = numpy.vstack((x.flatten(), y.flatten())).T
            rot = numpy.array([[normal[0], direction[0]], [normal[1], direction[1]]])
            nearest = (numpy.dot(idx.astype(numpy.float64), rot.T) + corners[0]).astype(numpy.int32)
            result = numpy.zeros((dx, dy))
            result[idx[:,0], idx[:,1]] = height[nearest[:,0], nearest[:,1]]
            if any([z not in (-99999, -9999, 9999, 99999) for z in numpy.nditer(result)]):
                return result
    return numpy.array([9999]).reshape(1,1)

def altitude(sources, x, y):
    try:
        from osgeo import gdal
    except ImportError:
        import gdal

    if x is None or y is None:
        return 9999
    for source in sources:
        src = gdal.Open(source)
        if src is None:
            raise Exception("'{}' not found".format(source)) # /!\ f string cannot be used in python < 3.6
        gt = src.GetGeoTransform()
        rb = src.GetRasterBand(1)
        # Convert from map to pixel coordinates.
        # Only works for geotransforms with no rotation.
        px = int((x - gt[0]) / gt[1]) # x pixel
        py = int((y - gt[3]) / gt[5]) # y pixel
        if px >= 0 and py >= 0 and px < src.RasterXSize and py < src.RasterYSize:
            value = rb.ReadRaster( px, py, 1, 1, buf_type=gdal.GDT_Float32)
            altitude = struct.unpack('f', value)[0]
            if altitude not in (-99999, -9999, 9999, 99999):
                return altitude
    return 9999


def set_altitude(sources, geom, srid):
    if geom is None:
        return None
    orig = wkb.loads(geom, True)
    g = transform(lambda x, y, z=None: (x, y, altitude(sources, x, y)), orig)
    g = set_srid(g, get_srid(orig))
    return g.wkb_hex


def crest_line(sources, line, srid):

    l = wkb.loads(line, True)
    out = []
    for p1, p2 in zip(l.coords[:-1], l.coords[1:]):
        width = Point(p1).distance(Point(p2))
        if width == 0:
            continue
        direction = numpy.array(p2) - numpy.array(p1)
        direction /=  sqrt(numpy.sum(numpy.square(direction)))
        nrml = numpy.array([direction[1], -direction[0]])
        height = altitudes(sources, (p1, p2), width, True)
        crest_idx = numpy.argmax(height,0)
        pt = numpy.array([0,0,0])
        pt[:2] = numpy.array(p1[:2]) + (crest_idx[0] - height.shape[1]/2)/height.shape[1] * width * nrml
        out.append(pt)

    pt = numpy.array([0,0,0])
    pt[:2] = numpy.array(p2[:2]) + (crest_idx[-1] - height.shape[1]/2)/height.shape[1] * width * nrml
    out.append(pt)

    out = LineString(out)
    out = set_srid(out, get_srid(l))
    return out


def set_link_altitude(plpy, link_id, model, srid):
    res = plpy.execute("""
        select link_type, up, up_type, down, down_type, geom from {model}._link
        where id={link_id}
        """.format(model=model, link_id=link_id))

    if not res:
        plpy.warning("{}._link id={} not found".format(model, link_id))
        return 0

    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select source from project.dem order by priority asc")]

    link = wkb.loads(res[0]['geom'], True)
    link_type = res[0]['link_type']

    lnk, = plpy.execute("""
            select border from {model}.{link_type}_link where id={link_id}
            """.format(
                model=model,
                link_id=link_id,
                link_type=link_type
            ))

    border = wkb.loads(lnk['border'], True)
    height = altitudes(sources, (border.coords[0], border.coords[-1]), link.length/10, True)
    crest = numpy.max(height,0)

    if "overflow" == link_type:
        z_crest1 = max([link.coords[0][2], link.coords[-1][2], numpy.min(crest)])
        width1 = 0
        z_crest2 = max([link.coords[0][2], link.coords[-1][2], numpy.mean(crest)])
        width2 = border.length
        plpy.execute("""
            update {model}.overflow_link
            set z_crest1={z_crest1}, width1={width1}, z_crest2={z_crest2}, width2={width2}
            where id={link_id}
            """.format(
                model=model,
                z_crest1=z_crest1,
                z_crest2=z_crest2,
                width1=width1,
                width2=width2,
                link_id=link_id))

    elif "strickler" == link_type:
        z_crest1 = max([link.coords[0][2], link.coords[-1][2], numpy.min(crest)])
        width1 = .1*border.length
        z_crest2 = max([link.coords[0][2]+0.001, link.coords[-1][2]+0.001, numpy.mean(crest)])
        width2 = border.length
        plpy.execute("""
            update {model}.strickler_link
            set z_crest1={z_crest1}, width1={width1}, z_crest2={z_crest2}, width2={width2}
            where id={link_id}
            """.format(
                model=model,
                z_crest1=z_crest1,
                z_crest2=z_crest2,
                width1=width1,
                width2=width2,
                link_id=link_id))

    elif "porous" == link_type:
        z_invert = max(link.coords[0][2], link.coords[-1][2])
        plpy.execute("""
            update {model}.porous_link
            set z_invert={z_invert}, width={width}
            where id={link_id}
            """.format(
                model=model,
                z_invert=z_invert,
                width=border.length,
                link_id=link_id))

    elif "mesh_2d" == link_type:
        z_invert = numpy.min(height)
        plpy.execute("""
            update {model}.mesh_2d_link
            set z_invert={z_invert}
            where id={link_id}
            """.format(
                model=model,
                z_invert=z_invert,
                link_id=link_id))
    else:
        plpy.warning("link type '{}' not handled by set_link_altitude".format(link_type))
        return 0

    return 1

def set_link_altitude_topo(plpy, link_id, model, srid):
    res = plpy.execute("""
        select link_type, up, up_type, down, down_type, geom from {model}._link
        where id={link_id}
        """.format(model=model, link_id=link_id))

    if not res:
        plpy.warning("{}._link id={} not found".format(model, link_id))
        return 0


    link = wkb.loads(res[0]['geom'], True)
    link_type = res[0]['link_type']

    cst = plpy.execute("""with precision as (select precision as prec from hydra.metadata )
                          select c.id
                          from {model}.constrain_3d as c, {model}.{link_type}_link as l, precision as p
                          where St_Distance(St_StartPoint(l.border),c.geom) < p.prec
                          and St_Distance(St_EndPoint(l.border),c.geom) < p.prec
                          and l.id = {link_id}""".format( model=model, link_id=link_id, link_type=link_type) )

    if not cst :
        plpy.warning("no constrain with 3D topo points found")
        return 0

    id_constrain_pt = cst[0]['id']

    border_geom, = plpy.execute("""
        with points as(
            select St_StartPoint(l.border) as start, St_EndPoint(l.border) as end
            from  {model}.{link_type}_link as l where l.id = {link_id}
        ), constrain as (
            select c.geom
            from {model}.constrain_3d as c
            where id = {id_constrain}
        )
        select st_linesubstring(
            (select constrain.geom from constrain),
            (select LEAST(ST_LineLocatePoint(constrain.geom, points.start), st_linelocatepoint(constrain.geom, points.end)) from points, constrain),
            (select GREATEST(ST_LineLocatePoint(constrain.geom, points.start), st_linelocatepoint(constrain.geom, points.end))  from points, constrain)
            ) as border
        """.format(
                model=model,
                link_id=link_id,
                link_type=link_type,
                id_constrain=id_constrain_pt
        ))

    border = wkb.loads(border_geom['border'], True)
    height = [[coord[2] for coord in border.coords]]
    crest = numpy.max(height,0)

    if link_type in ("overflow", "strickler"):
        crest_1 = []
        crest_2 = []

        plpy.notice(link.coords[0][2])
        plpy.notice(link.coords[-1][2])

        if link.coords[0][2] != 9999 :
            crest_1.append(link.coords[0][2])
            crest_2.append(link.coords[0][2] if link_type=="overflow" else link.coords[0][2]+0.001)
        if link.coords[-1][2] != 9999 :
            crest_1.append(link.coords[-1][2])
            crest_2.append(link.coords[-1][2] if link_type=="overflow" else link.coords[0][2]+0.001)
        if any(crest != 9999) :
            crest_1.append(numpy.min(crest))
            crest_2.append(numpy.mean(crest if link_type=="overflow" else crest+0.001))

        plpy.notice(crest_1)
        plpy.notice(crest_2)

        if crest_1 == [] :
            z_crest1 = 9999
        else :
            z_crest1 = max(crest_1)

        if crest_2 == [] :
            z_crest2 = 9999 if link_type=="overflow" else 9999+0.001
        else :
            z_crest2 = max(crest_2)

        width1 = 0 if link_type=="overflow" else .1*border.length
        width2 = border.length

        plpy.execute("""
            update {model}.{link_type}_link
            set z_crest1={z_crest1}, width1={width1}, z_crest2={z_crest2}, width2={width2}
            where id={link_id}
            """.format(
                model=model,
                link_type=link_type,
                z_crest1=z_crest1,
                z_crest2=z_crest2,
                width1=width1,
                width2=width2,
                link_id=link_id))

    elif "porous" == link_type:
        z_invert = max(link.coords[0][2], link.coords[-1][2])
        plpy.execute("""
            update {model}.porous_link
            set z_invert={z_invert}, width={width}
            where id={link_id}
            """.format(
                model=model,
                z_invert=z_invert,
                width=border.length,
                link_id=link_id))

    elif "mesh_2d" == link_type:
        z_invert = numpy.min(height)
        plpy.execute("""
            update {model}.mesh_2d_link
            set z_invert={z_invert}
            where id={link_id}
            """.format(
                model=model,
                z_invert=z_invert,
                link_id=link_id))
    else:
        plpy.warning("link type '{}' not handled by set_link_altitude".format(link_type))
        return 0

    return 1

def line_elevation(line_, dem):
    """
    line: sequence of (x,y) or (x,y,z) coordinates
    dem: name of a gdal-openable raster filewith elevation in first band
    line is discretized with the min pixel length
    values are bi-linearly interpolated
    """
    try:
        from osgeo import gdal
    except ImportError:
        import gdal

    src = gdal.Open(dem)
    if src is None:
        raise Exception("'{}' not found".format(dem)) # /!\ f string cannot be used in python < 3.6
    gt = src.GetGeoTransform()
    band = src.GetRasterBand(1)
    assert(gt[2] == 0 and gt[4] == 0)
    line = wkb.loads(line_, True)
    coords = numpy.array(line.coords)
    res = []
    for s, e in zip(coords[:-1,:2], coords[1:,:2]):
        se = e-s
        l = norm(se)
        n = ceil(l/min(abs(gt[1]), abs(gt[5]))) # note, we can increase resolution here
        dse = se/n
        for i in range(n+1):
            x = s + i*dse
            fx = (x[0] - gt[0])/gt[1], (x[1] - gt[3])/gt[5]
            idx =  floor(fx[0]) + (0 if fx[0]%1 > .5 else -1), floor(fx[1]) + (0 if fx[1]%1 > .5 else -1)
            if idx[0] >=0 and idx[1] >= 0 and idx[0]+1 < src.RasterXSize and idx[1]+1 < src.RasterYSize:
                a = band.ReadAsArray(idx[0], idx[1], 2, 2, buf_type=gdal.GDT_Float32)
                # a[0][0] a[0][1]
                # a[1][0] a[1][1]
                # pixels as displayed in image (y is down direction)
                u, v = fx[0] - idx[0] - .5, fx[1] - idx[1] - .5
                h = numpy.array([1-u, u]).dot(numpy.array(a)).dot(numpy.array([[1-v, v]]).T)
                p = (x[0], x[1], h[0])
                if len(res) == 0 or res[-1] != p:
                    res.append(p)

    g = LineString(res)
    g = set_srid(g, get_srid(line))
    return g.wkb_hex
