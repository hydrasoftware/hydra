from builtins import object
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from qgis.core import QgsWkbTypes
from qgis.PyQt.QtWidgets import QApplication
from hydra.gui.select_menu import SelectTool
from hydra.utility.map_point_tool import VisibleGeometry, MapPointTool
from hydra.utility.settings_properties import SettingsProperties

class ItemSelector(object):
    def __init__(self, project, iface):
        self.__project = project
        self.__model_name = project.get_current_model().name
        self.__iface = iface
        self.__result = [None, None, None]

    def select(self, table_list, add_condition = ""):
        subset_list = []
        for itable in table_list:
            subset_list.append((self.__model_name, itable))

        def on_left_click(point):
            geom = VisibleGeometry(self.__iface.mapCanvas(), QgsWkbTypes.PointGeometry)
            geom.add_point(point)
            snap = SettingsProperties.get_snap()
            size = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()*snap
            select = SelectTool(self.__project, point, size, subset=subset_list)
            if select.table:
                name = self.__project.execute("""select name from {m}.{t} where id={i} {c};
                    """.format(m=self.__model_name, t=select.table, i=select.id,c=add_condition)).fetchone()
                if name:
                    self.__result = [name[0], select.id, select.table]
                self.__iface.mapCanvas().unsetMapTool(tool)
                self.__iface.mapCanvas().setMapTool(None)

        def on_right_click(point):
            self.__iface.mapCanvas().unsetMapTool(tool)
            self.__iface.mapCanvas().setMapTool(None)

        tool = MapPointTool(self.__iface.mapCanvas(), self.__project.log)
        tool.leftClicked.connect(on_left_click)
        tool.rightClicked.connect(on_right_click)
        self.__iface.mapCanvas().setMapTool(tool)

        while self.__iface.mapCanvas().mapTool() == tool:
            QApplication.instance().processEvents()

    def get_selected_item_infos(self):
        return self.__result


