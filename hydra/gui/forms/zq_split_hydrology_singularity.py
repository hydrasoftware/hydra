# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from qgis.PyQt import uic
from hydra.utility.log import LogManager, ConsoleLogger
from hydra.utility.settings_properties import SettingsProperties
from hydra.gui.simple_singularity_dialog import SimpleSingularityDialog, tr
from hydra.gui.widgets.split_law_type_widget import SplitLawTypeWidget

class ZqSplitHydrologySingularityEditor(SimpleSingularityDialog):
    def __init__(self, project, geom, id=None, parent=None, iface=None, size=10):
        SimpleSingularityDialog.__init__(self, project, geom, "zq_split_hydrology_singularity", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "zq_split_hydrology_singularity.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.dir = current_dir
        self.__size = size
        self.__iface = iface
        self.__log_manager = LogManager(ConsoleLogger(), "Hydra")

        self.btn_downstream.clicked.connect(self.__select_downstream)
        self.btn_split1.clicked.connect(self.__select_split1)
        self.btn_split2.clicked.connect(self.__select_split2)
        self.check_split2.clicked.connect(self.set_check_split2)

        self.downstream_law_widget = SplitLawTypeWidget(self.project, self.downstream_widget)
        self.split1_law_widget = SplitLawTypeWidget(self.project, self.split1_widget)
        self.split2_law_widget = SplitLawTypeWidget(self.project, self.split2_widget)

        if iface==None:
            self.btn_downstream.setEnabled(False)
            self.btn_split1.setEnabled(False)
            self.btn_split2.setEnabled(False)

        self.node, name, downstream, downstream_type, split1, split1_type, split2, split2_type, downstream_law, downstream_param, split1_law, split1_param, split2_law, split2_param, comment = self.project.execute("""
            select node, name, downstream, downstream_type, split1, split1_type, split2, split2_type, downstream_law, downstream_param, split1_law, split1_param, split2_law, split2_param, comment
            from {}.zq_split_hydrology_singularity
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.name.setText(name)
        self.notes.setText(comment)
        # downstream
        if downstream:
            downstream_name, = self.project.execute("""select name from {}.{}_link where id={}""".format(self.model.name, downstream_type, downstream)).fetchone()
            self.downstream.setText(downstream_name)
        self.downstream_law_widget.set_values(downstream_law, downstream_param)
        # split1
        if split1:
            split1_name, = self.project.execute("""select name from {}.{}_link where id={}""".format(self.model.name, split1_type, split1)).fetchone()
            self.split1.setText(split1_name)
        self.split1_law_widget.set_values(split1_law, split1_param)
        # split2
        if split2:
            split2_name, = self.project.execute("""select name from {}.{}_link where id={}""".format(self.model.name, split2_type, split2)).fetchone()
            self.split2.setText(split2_name)
            self.check_split2.setChecked(True)
        else :
            self.check_split2.setChecked(False)
        self.split2_law_widget.set_values(split2_law, split2_param)

        self.set_check_split2()

        self.show()

    def __select_downstream(self):
        self.__select_link('downstream')

    def __select_split1(self):
        self.__select_link('split1')

    def __select_split2(self):
        self.__select_link('split2')

    def __select_link(self, field):
        from hydra.utility.item_selector import ItemSelector
        i_selector = ItemSelector(self.project, self.__iface)
        self.hide()

        i_selector.select(['pipe_link', 'connector_hydrology_link'])
        name, id, table = i_selector.get_selected_item_infos()

        if name:
            getattr(self, field).setText(name)
        self.stash()
        ZqSplitHydrologySingularityEditor(self.project, None, self.id, None, self.__iface, self.__size).exec_()
        self.ignore_rollback = True
        self.close()

    def set_check_split2(self):
        if self.check_split2.isChecked():
            self.split2.setEnabled(True)
            self.btn_split2.setEnabled(True)
            self.tab.setTabEnabled(2, True)
        else :
            self.split2.setEnabled(False)
            self.btn_split2.setEnabled(False)
            self.tab.setTabEnabled(2, False)

    def get_id(self):
        return self.id

    def stash(self):
        # downstream / splits
        downstream, downstream_law, downstream_param= None, None, '{}'
        split1, split1_law, split1_param= None, None, '{}'
        split2, split2_law, split2_param= None, None, '{}'

        if self.downstream.text():
            downstream, = self.project.execute("""select id from {}._link where name='{}'""".format(self.model.name, self.downstream.text())).fetchone()
            downstream_law, downstream_param = self.downstream_law_widget.get_values()
        if self.split1.text():
            split1, = self.project.execute("""select id from {}._link where name='{}'""".format(self.model.name, self.split1.text())).fetchone()
            split1_law, split1_param = self.split1_law_widget.get_values()
        if self.split2.text() and self.check_split2.isChecked():
            split2, = self.project.execute("""select id from {}._link where name='{}'""".format(self.model.name, self.split2.text())).fetchone()
            split2_law, split2_param = self.split2_law_widget.get_values()

        self.model.update_singularity_dzh(self.id, self.geom, downstream, downstream_law, downstream_param, split1, split1_law, split1_param, split2, split2_law, split2_param, self.name.text(), comment=self.notes.text())

        if not self.check_split2.isChecked():
            self.project.execute("""update {}.zq_split_hydrology_singularity set split2=null, split2_type=null where id={}""".format(self.model.name, self.id))

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.stash()
        SimpleSingularityDialog.save(self)
        self.close()




