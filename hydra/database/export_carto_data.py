﻿# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
export database for computation
"""

import os
import json
from hydra.utility import string
from .database import autoconnection

with open(os.path.join(os.path.dirname(__file__), "proj_srid.json")) as j:
    __json_srid__ = json.load(j)

class ExportCartoData(object):

    def __init__(self, project, log_manager=None):
        self.project = project
        self.log = log_manager or project.log


    #4.1
    def __exportcarto_controlfile(self, id_scenario, namescenario, lstmodel, streamf):

        tbegin_output_hr, tend_output_hr, dt_output_hr = self.project.fetchone(f"""
            select tbegin_output_hr, tend_output_hr, dt_output_hr
            from project.scenario where id={id_scenario}
            """)

        self.write_carto_controlfile(tbegin_output_hr, tend_output_hr, dt_output_hr,  namescenario, lstmodel, streamf)

    def write_carto_controlfile(self, tbegin_output_hr, tend_output_hr, dt_output_hr, namescenario, lstmodel, streamf):

        streamf.write("*scenario\n%s\n\n" % namescenario)

        streamf.write("*reseau\n")
        for model in lstmodel:
            streamf.write("%s\n" % model)
        streamf.write("\n")

        streamf.write("*timeh\n %15.3f %15.3f %10.3f\n\n" % (tbegin_output_hr.total_seconds()/3600, tend_output_hr.total_seconds()/3600, dt_output_hr.total_seconds()/3600))

        srid, srs_wkt = self.project.fetchone("""
                select m.srid, s.srtext
                from hydra.metadata m
                left join spatial_ref_sys s on s.srid=m.srid
                """)

        streamf.write("*sor_shp\n")
        if srs_wkt:
            streamf.write(srs_wkt)
            streamf.write("\n\n")
        else:
            self.log.notice("CRS not found in QGIS database, loading backup projection for srid: {}".format(srid))
            if str(srid) in __json_srid__.keys():
                streamf.write(__json_srid__[str(srid)])
            else:
                self.log.error("Unexpected coordinate system, srid: {}".format(srid))
            streamf.write("\n\n")
        streamf.write("\n\n")


    def __exportmodels(self, namescenario, lstmodel, directory):
        """
        export module for calculation: loop on models
        namescenario: name of a scenario
        lstmodel:   (tuple): array of models (formerly "réseaux")
        """

        for model in lstmodel:
            nomfichier=os.path.join(directory, model+"_geom.dat")
            with open(nomfichier,"w") as streamfgeom:
                self.__exportcarto_reach(model, streamfgeom)
                self.__exportcarto_2d(model, streamfgeom)
                self.__exportcarto_storage(model, "storage_node", streamfgeom)
                self.__exportcarto_street(model, streamfgeom)

            nomfichier=os.path.join(directory, model+"_ctrz.dat")
            with open(nomfichier,"w") as streamfgeom, \
                 open(os.path.join(directory, model+"_geom.mif"),"w") as streamfmif:
                self.__exportcarto_ctrz(model, streamfgeom, streamfmif)

    def export(self, id_scenario):
        """
        export module for calculation: main function
        id_scenario: id or name of a scenario
        """

        # Get list of models and groups
        lstreseau = [name for name, in self.project.fetchall("""
            select name from project.model order by name asc
            """)]

        try:
            if not isinstance(id_scenario, int):
                id_scenario, =  self.project.fetchone("""
                    select id from project.scenario where name='{}'
                    """.format(id_scenario))

            name_scenario, =  self.project.fetchone("""
                select name
                from project.scenario
                where id=%s
                """, (id_scenario,))
        except TypeError:
            exit(1)

        #  directories
        cur_dir = os.getcwd()
        directory_scenario = self.project.get_senario_path(id_scenario)
        directory_calculation = os.path.join(directory_scenario, "travail")

        # Create control file
        with open(os.path.join(directory_calculation, "triangulz.ctl"),"w") as streamf:
            self.__exportcarto_controlfile(id_scenario, name_scenario, lstreseau, streamf)

        self.log.notice("Carto data exported to {}".format(directory_scenario))
        os.chdir(cur_dir)


class ExportCartoDataSerie(ExportCartoData):

    def __init__(self, project):
        ExportCartoData.__init__(self, project)

    #4.1
    def __exportcarto_controlfile_serie(self, id_scenario, namescenario, lstmodel, streamf):

        tbegin_output_hr, tend_output_hr, dt_output_hr = self.project.fetchone(f"""
            select tbegin_output_hr, tend_output_hr, dt_output_hr
            from project.serie_scenario where id={id_scenario}
            """)

        self.write_carto_controlfile(tbegin_output_hr, tend_output_hr, dt_output_hr, namescenario, lstmodel, streamf)

    def export_scenario_serie(self, id_scenario):
        """
        export module for calculation: main function
        id_scenario: id of a scenario
        """

        # Get list of models and groups
        lstreseau = [name for name, in self.project.fetchall("""
            select name
            from project.model
            order by name asc
            """)]

        try:
            name_scenario, =  self.project.fetchone("""
                select name
                from project.serie_scenario
                where id=%s
                """, (id_scenario,))
        except TypeError:
            raise

        #  directories
        cur_dir = os.getcwd()
        directory_scenario = self.project.get_serie_scenario_path(id_scenario)
        directory_calculation = os.path.join(directory_scenario, "travail")

        # Create control file
        with open(os.path.join(directory_calculation, "triangulz.ctl"),"w") as streamf:
            self.__exportcarto_controlfile_serie(id_scenario, name_scenario, lstreseau, streamf)

        self.log.notice("Carto data exported to {}".format(directory_scenario))
        os.chdir(cur_dir)