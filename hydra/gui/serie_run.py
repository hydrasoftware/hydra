# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
from builtins import str

import os
import re
import unicodedata
from datetime import datetime
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QDialog, QApplication, QTableWidgetItem, QAbstractItemView, QWidget, QPushButton, QVBoxLayout, QMessageBox, QLabel
from qgis.PyQt.QtGui import QColor
from qgis.PyQt.QtCore import Qt
from hydra.gui.base_dialog import tr
from hydra.gui.widgets.scroll_log_widget import ScrollLogger
from hydra.database.export_calcul import ExportCalculSerie
from hydra.database.export_carto_data import ExportCartoDataSerie
from hydra.kernel import Kernel
from hydra.utility.string import is_date

_hydra_dir = os.path.join(os.path.expanduser("~"), ".hydra")

class ScenarioSerieRunError(Exception):
    pass

class SerieRun(QDialog):
    def __init__(self, project, serie_id, parent=None):
        QDialog.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "serie_run.ui"), self)

        self.project = project
        self.serie_id = serie_id

        self.scenarios = self.project.execute("""
                    select id, name
                    from project.serie_scenario as scn
                    where id_cs = {}""".format(self.serie_id)).fetchall()

        self.init_serie_data()

        self.btnRunSerie.clicked.connect(lambda x:self.run_serie())

        self.table.setSelectionMode(QAbstractItemView.SingleSelection)
        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.table.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.update_scenarios()

        self.log_scroll = ScrollLogger()
        self.log_lay = QVBoxLayout()
        self.log_lay.addWidget(self.log_scroll)
        self.log_placeholder.setLayout(self.log_lay)

        self.output_scroll = ScrollLogger(night_mode=True)
        self.output_lay = QVBoxLayout()
        self.output_lay.addWidget(self.output_scroll)
        self.output_placeholder.setLayout(self.output_lay)

        self.__runner = None

        self.show()

    def closeEvent(self, evnt):
        '''stops computation process at close event'''
        if self.__runner:
            self.__runner.kill_process()
        QApplication.restoreOverrideCursor()
        QDialog.closeEvent(self, evnt)

    def init_serie_data(self):
        self.serie_name, date_start, date_fin, duration = self.project.execute("""
            select name, date0, date0 + tfin, tfin from project.serie where id={id}""".format(id=self.serie_id)).fetchone()

        metadata={}
        metadata['Start date'] = date_start.strftime("%d/%m/%Y")
        metadata['End date'] = date_fin.strftime("%d/%m/%Y")
        metadata['Duration'] = '{} days'.format(duration.days)

        labels = ['<b>{}: </b>{}'.format(name, value) for name, value in metadata.items()]
        label = '<b>Time serie {}</b><br><br>'.format(self.serie_name) + '<br><br>'.join(labels)
        self.label_serie.setText(label)

    def __log(self, text):
        ''' add line for macro logging (big steps in time serie run)'''
        text = '\n'.join(re.split('\n+', text.strip()))
        if text:
            weight = None
            color = None
            if "error" in text :
                color = "red"
                weight = "bold"
                self.project.log.error(text)
                raise KernelError(text)
            elif "ok" in text:
                weight = "bold"
                self.project.log.notice(text)
            self.log_scroll.add_line(text, weight, color, centered="- - - " in text)

    def __log_separator(self):
        ''' add separator in log'''
        self.log_scroll.new_label()
        self.__log("- - - "*18)
        self.log_scroll.new_label()

    def __output_log(self, text):
        ''' add line for kernel output logging'''
        text = '\n'.join(re.split('\n+', text.strip()))
        if text:
            weight = None
            if "output" in text :
                self.output_scroll.new_label()
                self.output_scroll.add_line(text, weight='bold', color='#ffffff')
            else:
                self.output_scroll.add_line(text, color='#ffffff')
            if "PHASE CALCUL TRANSITOIRE" in text:
                self.output_scroll.new_label(10)

    def run_serie(self, scn = None):
        self.log_scroll.clear()
        self.output_scroll.clear()
        if scn is None :
            self.__log("Starting {}".format(self.serie_name))
        else :
            self.__log("Relaunch {} from scenario : {}".format(self.serie_name, scn[1]))
        # Analyzing serie data
        model_connect_settings, = self.project.execute("""
                select model_connect_settings
                from project.serie where id={}""".format(self.serie_id)).fetchone()
        lstmodel = [name for name, in self.project.execute("""select name from project.model""").fetchall()]
        if model_connect_settings=="mixed":
            lstmodel = []
            groups = self.project.execute("""
                select id, name, ord
                from project.mixed_serie as mixed, project.grp as grp
                where mixed.grp=grp.id
                and mixed.serie={}
                order by mixed.ord""".format(self.serie_id)).fetchall()
            for group in groups:
                lstmodel = [name for name, in self.project.execute("""
                    select name
                    from project.model_config as model
                    where model.id in (
                        select model from project.grp_model
                        where grp = %i)
                    """%(int(group[0]))).fetchall()]
        # Checking validity of concerned models
        invalid_models=[]
        for model in lstmodel:
            if self.project.execute(""" select * from {}.invalid""".format(model)).fetchall():
                invalid_models.append(model)
        if invalid_models:
            validity = QMessageBox(QMessageBox.Warning, tr('Warning'),
                            tr('There are still some invalidities in models {}.'.format(', '.join(invalid_models))),
                            QMessageBox.Ok).exec_()

        else :
            # Gettings scenarios to compute
            if scn is None :
                scenarios = self.project.execute("""
                        select id, name
                        from project.serie_scenario as scn
                        where id_cs = {}""".format(self.serie_id)).fetchall()
                self.__log("{} processing ok".format(self.serie_name))
            else :
                scenarios = self.project.execute("""
                        select id, name
                        from project.serie_scenario as scn
                        where id_cs = {}
                        and (index >= (select index from project.serie_scenario where id = {}) and id_b = (select id_b from project.serie_scenario where id = {}))
                        or id_b > (select id_b from project.serie_scenario where id = {})""".format(self.serie_id, scn[0], scn[0], scn[0])).fetchall()
                self.__log("{} processing ok".format(self.serie_name))

            for scn in scenarios:
                self.__log_separator()
                self.output_scroll.clear()

                self.__log("Exporting {}".format(scn[1]))
                # export topological data for calculation
                exporter = ExportCalculSerie(self.project)
                exporter.export_scenario_serie(int(scn[0]))
                # export cartograpic data for triangulation
                exporter = ExportCartoDataSerie(self.project)
                exporter.export_scenario_serie(int(scn[0]))
                self.__log("Export ok".format(scn[1]))

                #Computation with Kernel
                self.__runner = Kernel(self.project, scn[0], parent=self, mode_serie=True)
                self.__output_log("{} output:".format(scn[1]))
                try:
                    self.__runner.log.connect(self.__log)
                    self.__runner.output.connect(self.__output_log)
                    self.__runner.run()
                    QApplication.processEvents()
                    if not self.project.scn_has_run_from_name(scn[1]) :
                        raise ScenarioSerieRunError("Scenario {} didn't run as expected".format(scn[1]))
                except Exception as e:
                    self.update_scenarios()
                    QMessageBox.critical(self, tr('Error during computation'), tr("An error occured during {}:\n    {}").format(scn[1], str(e)), QMessageBox.Ok)
                    return

                self.update_scenarios()

            self.__log_separator()
            self.__write_output_file_sor1()
            self.__log("Time serie {} ok".format(self.serie_name))

    def __write_output_file_sor1 (self) :
        ''' fonction to write output data in a global file for the given serie'''
        types = ["deb","lim"]

        for typ in types :
            output=[]
            output_file = os.path.join(self.project.directory, self.serie_name+"_"+typ+"_info.csv")
            open(output_file, 'w').close() # Cleanup out file


            for i, scn in enumerate(self.scenarios):
                for model in self.project.get_models():
                    input_file = os.path.join(self.project.directory, scn[1], "hydraulique", scn[1]+"_"+model+"_"+typ+".csv")
                    if os.path.exists(input_file) :
                        self.__log("Writing {} data to {}".format(scn[1], os.path.basename(output_file)))
                        with open(output_file, 'a') as outfile, open(input_file, 'r') as infile:
                            if i==0:
                                outfile.writelines(infile.readlines())
                            else:
                                outfile.writelines(infile.readlines()[2:])
        # output.sort(key=lambda x:(x.split(';')[0],datetime.strptime(x.split(';')[1],"%d/%m/%Y %H:%M")))

    def update_scenarios(self):
        previous_scenario_has_run = True
        self.table.setRowCount(0)
        for scn in self.scenarios:
            current_scenario_has_run = self.add_scenario(scn, previous_scenario_has_run)
            if not current_scenario_has_run:
                previous_scenario_has_run = False

    def add_scenario(self, scn, previous_scn_has_run):
        rowcount = self.table.rowCount() + 1
        self.table.setRowCount(rowcount)
        row = rowcount - 1

        item = QTableWidgetItem(scn[1])
        self.table.setItem(row, 0, item)

        runner = Kernel(self.project, scn[0], mode_serie=True)
        err, message, date = runner.get_status()

        item = QTableWidgetItem(message)
        self.table.setItem(row, 1, item)
        if err==-1: #not calculated
            self.table.item(row, 1).setForeground(QColor(255,0,0))
        elif err!=0: #error
            self.table.item(row, 1).setBackground(QColor(255,0,0))

        item = QTableWidgetItem(date)
        self.table.setItem(row, 2, item)

        item = QTableWidgetItem()
        self.table.setItem(row, 3, item)

        if previous_scn_has_run and err != 0:
            btn_relaunch = QPushButton("Restart here")
            btn_relaunch.clicked.connect(lambda x:self.run_serie(scn))
            self.table.setCellWidget(row, 3, btn_relaunch)

        return err==0

class KernelError(Exception):
    pass

if __name__ == '__main__':
    from hydra.project import Project
    import sys
    app = QApplication(sys.argv)
    obj_project = Project.load_project("scngrp")
    run_dlg = SerieRun(obj_project, 1)
    run_dlg.exec_()