# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import traceback
from functools import partial
from math import sin, cos, pi
from numpy import array, dot, concatenate, abs as nabs
from matplotlib.figure import Figure
from matplotlib.patches import Rectangle
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from qgis.PyQt.QtCore import Qt, QPoint, QPointF
from qgis.PyQt.QtWidgets import  QMenu, QDialog, QHBoxLayout, QDoubleSpinBox, QDialogButtonBox, QApplication, QLabel
from .utility import snap

class CrossSectionCanvas(FigureCanvas):
    def __init__(self, profile=None, fig=None, parent=None):
        fig = fig if fig is not None else Figure()
        super().__init__(fig)
        #self.setParent(parent)
        self.setFocusPolicy(Qt.StrongFocus)
        self.axes = self.figure.add_subplot(1,8,(1,7))
        self.axes_r = self.figure.add_subplot(1,8,8)
        self.axes_r.tick_params(left=False)
        self.axes_r.set_yticks([])
        self.figure.subplots_adjust(left=0.05, bottom=0.05, right=0.98, top=0.98)

        self.selection = None
        self.selection_graph = None
        self.pressedPoint = None
        self.snap_distance_pixels = 10
        self.axes.set_xlim((0.,1))
        self.axes.set_ylim((0.,1))
        #self.unit_circle = array([[-cos(pi*t/16), -sin(pi*t/16)] for t in range(8+1)])
        self.unit_circle = array([[-1,0], [-1,-1], [0,-1]])
        self.unit_circle_graph, = self.axes.plot(self.unit_circle[:,0], self.unit_circle[:,1], color='black', linewidth=.5)

        self.profile = None
        if profile is not None:
            self.set_profile(profile, True)

    def set_profile(self, profile, adjust_scale=False):
        self.selection = None
        self.profile = profile

        if self.profile is not None:
            # center on riverbed or on zero
            xlim = self.axes.get_xlim()
            ylim = self.axes.get_ylim()
            points = self.profile.sz
            center = self.profile.center

            if xlim == (0.,1.):
                xlim = (min(points[:,0]), max(points[:,0]))
                ylim = (min(points[:,1]), max(points[:,1]))
                if self.profile.riverbed is not None:
                    xlim = (self.profile.riverbed[0][0], self.profile.riverbed[-1][0])
                    points = points[points[:, 0] >= self.profile.riverbed[0][0]]
                    points = points[points[:, 0] <= self.profile.riverbed[-1][0]]
                    ylim = (min(points[:,1]), max(points[:,1]))

                deltax = 2*max(abs(xlim[1] - center), abs(center- xlim[0]))
                deltay = max(ylim[1] - ylim[0], 1)
                xlim = (center - .6*deltax, center + .6*deltax)
                ylim = (ylim[0] - .1*deltay, ylim[1] + .1*deltay)

            deltax = max(xlim[1] - xlim[0], 1)
            deltay = max(ylim[1] - ylim[0], 1)
            ymin = min(points[:,1])
            if deltay > 1000:
                deltay = (max(points[:,1]) - ymin)*1.2
            self.axes.set_ylim([ymin-.1*deltay, ymin+.9*deltay])
            self.axes_r.set_ylim([ymin-.1*deltay, ymin+.9*deltay])
            self.axes.set_xlim([center-.5*deltax, center+.5*deltax])

        self.draw()

    def draw(self):
        self.selection_graph and self.selection_graph.remove()
        self.selection_graph = None

        # draw 5% x width circle to get a visual idea of the aspect ration
        xlim = self.axes.get_xlim()
        ylim = self.axes.get_ylim()
        r = min(.05*(xlim[1] - xlim[0]), .05*(ylim[1] - ylim[0]))
        self.unit_circle_graph.set_data(self.unit_circle[:,0]*r + xlim[1], self.unit_circle[:,1]*r + ylim[1])

        if self.profile is not None:
            self.profile.draw(self.axes, self.axes_r)

        if self.selection is not None:
            self.selection_graph, = self.axes.plot(self.selection[:,0], self.selection[:,1], '-o', color='red')

        super().draw()

    def get_profile(self):
        return self.profile

    def mouseMoveEvent(self, evt):
        if int(evt.buttons() & Qt.LeftButton):
            pt = self.screenToCoord(evt.pos())

            if self.selection is not None:
                if self.selection[0,0] == 0 and len(self.selection) == 2:
                    if (QApplication.keyboardModifiers() & Qt.ShiftModifier)== Qt.ShiftModifier and self.profile is not None:
                        pt = self.snapToPoint(pt, self.profile.sz)
                    delta = pt - self.pressedPoint
                    self.axes.set_xlim(self.axes.get_xlim() - delta[0])
                    npt = self.selection[0] + array([delta[0], 0])
                    self.selection = self.profile.move_point(self.selection, npt)
                else:
                    if (QApplication.keyboardModifiers() & Qt.ControlModifier)== Qt.ControlModifier:
                        pt[1] = self.selection[0][1]
                    if (QApplication.keyboardModifiers() & Qt.ShiftModifier)== Qt.ShiftModifier and self.profile is not None:
                        if len(self.selection) == 2:
                            pt = self.snapToPoint(pt, self.profile.sz)
                        else:
                            pt = self.snapToPoint(pt, self.profile.topo)

                    self.selection = self.profile.move_point(self.selection, pt)

                if self.selection is None: # selection lost, reset pan origin
                    self.pressedPoint = pt

                self.draw()

            elif self.pressedPoint is not None:
                # pan
                delta = pt - self.pressedPoint
                self.axes.set_xlim(self.axes.get_xlim() - delta[0])
                self.axes.set_ylim(self.axes.get_ylim() - delta[1])
                self.draw()

        return FigureCanvas.mouseMoveEvent(self, evt)

    def mouseReleaseEvent(self, evt):
        self.pressedPoint = None
        return FigureCanvas.mouseReleaseEvent(self, evt)

    def mousePressEvent(self, evt):
        pt = self.screenToCoord(evt.pos())
        if Qt.LeftButton == evt.button():
            # if (QApplication.keyboardModifiers() & Qt.ShiftModifier)== Qt.ShiftModifier:
            #     self.selectPoint(pt, True)
            # else:
            self.selectPoint(pt)

            self.pressedPoint = pt

        if Qt.RightButton == evt.button():
            m = QMenu()
            m.move(self.mapToGlobal(evt.pos()))
            if self.selectPoint(pt):
                m.addAction('remove').triggered.connect(self.removeSelectedPoint)
                m.addAction('edit').triggered.connect(self.editSelectedPoint)
            else:
                a = m.addAction('add')
                a.setData(QPointF(2,2))
                a.triggered.connect(partial(self.addPoint, pt))
            m.show()
            m.exec_()

        return FigureCanvas.mousePressEvent(self, evt)

    def keyPressEvent(self, evt):
        if evt.key() == Qt.Key_Delete:
            self.removeSelectedPoint()
        if evt.key() == Qt.Key_Return:
            self.editSelectedPoint()
        return FigureCanvas.keyPressEvent(self, evt)

    def wheelEvent(self, evt):
        # zoom
        pt = self.screenToCoord(evt.pos())
        dx = self.axes.get_xlim()[1] - self.axes.get_xlim()[0]
        dy = self.axes.get_ylim()[1] - self.axes.get_ylim()[0]
        ax = (pt[0] - self.axes.get_xlim()[0]) / dx
        ay = (pt[1] - self.axes.get_ylim()[0]) / dy
        if evt.angleDelta().y() < 0:
            if evt.pos().x() > 50:
                self.axes.set_xlim((self.axes.get_xlim()[0]  - .1*dx*ax, self.axes.get_xlim()[1] + .1*dx*(1-ax)))
            if evt.pos().y() < self.height() - 50:
                self.axes.set_ylim((self.axes.get_ylim()[0]  - .1*dy*ay, self.axes.get_ylim()[1] + .1*dy*(1-ay)))
        else:
            if evt.pos().x() > 50:
                self.axes.set_xlim((self.axes.get_xlim()[0]  + .1*dx*ax, self.axes.get_xlim()[1] - .1*dx*(1-ax)))
            if evt.pos().y() < self.height() - 50:
                self.axes.set_ylim((self.axes.get_ylim()[0]  + .1*dy*ay, self.axes.get_ylim()[1] - .1*dy*(1-ay)))

        self.draw()

        return FigureCanvas.wheelEvent(self, evt)

    def set_transect(self, name):
        pass

    def screenToCoord(self, p):
        return array(self.axes.transData.inverted().transform((p.x(), self.height()-p.y())))

    def coordToSreen(self, p):
        pt = self.axes.transData.transform(p)
        return QPoint(int(pt[0]), self.height()-int(pt[1]))

    def snapToPoint(self, pos, target):
        dx, dy = nabs((self.screenToCoord(QPoint(self.snap_distance_pixels, self.snap_distance_pixels)) - self.screenToCoord(QPoint())))
        return snap(pos, dx, dy, target)

    def selectPoint(self, pos, multiple=False):
        dx, dy = nabs((self.screenToCoord(QPoint(self.snap_distance_pixels, self.snap_distance_pixels)) - self.screenToCoord(QPoint())))
        if multiple:
            if self.profile is not None:
                s = self.profile.select_point(pos, dx, dy)
                if s is not None and len(s) == 1:
                    self.selection = concatenate((s, self.selection)) if self.selection is not None else s
                else:
                    self.selection = s
            else:
                self.selection = None
        else:
            self.selection = None
            if self.profile is not None:
                self.selection = self.profile.select_point(pos, dx, dy)

        self.draw()
        return self.selection is not None

    def addPoint(self, p):
        self.profile.add_point(p)
        self.draw()

    def mouseDoubleClickEvent(self, evt):
        self.addPoint(self.screenToCoord(evt.pos()))
        return FigureCanvas.mouseDoubleClickEvent(self, evt)

    def removeSelectedPoint(self):
        if self.selection is not None:
            self.profile.remove_point(self.selection)
            self.selection = None
            self.draw()

    def editSelectedPoint(self):
        if len(self.selection) != 1:
            return
        p = self.selection[0]
        w = QDialog()

        w.move(self.pos()+self.coordToSreen(p))
        l = QHBoxLayout()
        w.setLayout(l)
        x = QDoubleSpinBox()
        x.setRange(-9999, 9999)
        x.setButtonSymbols(QDoubleSpinBox.NoButtons)
        x.setValue(p[0])
        x.selectAll()
        y = QDoubleSpinBox()
        y.setRange(-9999, 9999)
        y.setButtonSymbols(QDoubleSpinBox.NoButtons)
        y.setValue(p[1])
        l.addWidget(x)
        l.addWidget(y)
        bb = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        bb.accepted.connect(w.accept)
        bb.rejected.connect(w.reject)
        l.addWidget(bb)
        if w.exec_():
            pt = array([x.value(), y.value()])
            self.selection = self.profile.move_point(self.selection, pt)
            self.draw()
