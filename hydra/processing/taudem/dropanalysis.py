# -*- coding: utf-8 -*-

"""
***************************************************************************
    dropanalysis.py
    ---------------------
    Date                 : June 2012
    Copyright            : (C) 2012-2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'June 2012'
__copyright__ = '(C) 2012-2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.core import (QgsVectorFileWriter,
                       QgsProcessing,
                       QgsProcessingException,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterEnum,
                       QgsProcessingParameterFileDestination
                      )

from .taudemAlgorithm import TauDemAlgorithm
from . import taudemUtils


class DropAnalysis(TauDemAlgorithm):

    PIT_FILLED = 'PIT_FILLED'
    D8_FLOWDIR = 'D8_FLOWDIR'
    D8_CONTRIB_AREA = 'D8_CONTRIB_AREA'
    ACCUM_STREAM_SOURCE = 'ACCUM_STREAM_SOURCE'
    OUTLETS = 'OUTLETS'
    MIN_THRESHOLD = 'MIN_THRESHOLD'
    MAX_THRESHOLD = 'MAX_THRESHOLD'
    DROP_TRESHOLDS = 'DROP_TRESHOLDS'
    STEP = 'STEP'
    DROP_ANALYSIS = 'DROP_ANALYSIS'

    def name(self):
        return 'dropanalysis'

    def displayName(self):
        return self.tr('Stream drop analysis')

    def group(self):
        return self.tr('Stream network analysis')

    def groupId(self):
        return 'streamanalysis'

    def tags(self):
        return self.tr('dem,hydrology,stream,drop,statistics').split(',')

    def shortHelpString(self):
        return self.tr('Applies a series of thresholds (determined from the '
                       'input parameters) to the input accumulated stream '
                       'source grid and outputs the results in the stream '
                       'drop statistics table.')

    def helpUrl(self):
        return 'http://hydrology.usu.edu/taudem/taudem5/help53/StreamDropAnalysis.html'

    def __init__(self):
        super().__init__()

    def initAlgorithm(self, config=None):
        self.steps = [self.tr('Logarithmic'),
                      self.tr('Arithmetic')
                     ]

        self.addParameter(QgsProcessingParameterRasterLayer(self.PIT_FILLED,
                                                            self.tr('Pit filled elevation')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.D8_FLOWDIR,
                                                            self.tr('D8 flow directions')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.D8_CONTRIB_AREA,
                                                            self.tr('D8 contributing area')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.ACCUM_STREAM_SOURCE,
                                                            self.tr('Accumulated stream source')))
        self.addParameter(QgsProcessingParameterFeatureSource(self.OUTLETS,
                                                              self.tr('Outlets'),
                                                              types=[QgsProcessing.TypeVectorPoint]))
        self.addParameter(QgsProcessingParameterNumber(self.MIN_THRESHOLD,
                                                       self.tr('Minimum threshold'),
                                                       QgsProcessingParameterNumber.Double,
                                                       defaultValue=5))
        self.addParameter(QgsProcessingParameterNumber(self.MAX_THRESHOLD,
                                                       self.tr('Maximum threshold'),
                                                       QgsProcessingParameterNumber.Double,
                                                       defaultValue=500))
        self.addParameter(QgsProcessingParameterNumber(self.DROP_TRESHOLDS,
                                                       self.tr('Number of drop thresholds'),
                                                       QgsProcessingParameterNumber.Double,
                                                       defaultValue=10))
        self.addParameter(QgsProcessingParameterEnum(self.STEP,
                                                     self.tr('Type of threshold step'),
                                                     options=self.steps,
                                                     defaultValue=0))

        self.addParameter(QgsProcessingParameterFileDestination(self.DROP_ANALYSIS,
                                                                self.tr('Drop analysis'),
                                                                self.tr('Text files (*.txt)')))

    def processAlgorithm(self, parameters, context, feedback):
        demLayer = self.parameterAsRasterLayer(parameters, self.PIT_FILLED, context)
        if demLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.PIT_FILLED))

        flowLayer = self.parameterAsRasterLayer(parameters, self.D8_FLOWDIR, context)
        if flowLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.D8_FLOWDIR))

        contribLayer = self.parameterAsRasterLayer(parameters, self.D8_CONTRIB_AREA, context)
        if contribLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.D8_CONTRIB_AREA))

        streamLayer = self.parameterAsRasterLayer(parameters, self.ACCUM_STREAM_SOURCE, context)
        if streamLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.ACCUM_STREAM_SOURCE))

        outlets = self.parameterAsSource(parameters, self.OUTLETS, context)
        if outlets is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.OUTLETS))

        arguments = []
        arguments.append(self.command())

        arguments.append('-fel')
        arguments.append(demLayer.source())
        arguments.append('-p')
        arguments.append(flowLayer.source())
        arguments.append('-ad8')
        arguments.append(contribLayer.source())
        arguments.append('-ssa')
        arguments.append(streamLayer.source())

        outlets = self.parameterAsCompatibleSourceLayerPath(parameters, self.OUTLETS, context,
                                                            QgsVectorFileWriter.supportedFormatExtensions(),
                                                            feedback=feedback)
        arguments.append('-o')
        arguments.append(outlets)

        arguments.append('-par')
        arguments.append('{}'.format(self.parameterAsDouble(parameters, self.MIN_THRESHOLD, context)))
        arguments.append('{}'.format(self.parameterAsDouble(parameters, self.MAX_THRESHOLD, context)))
        arguments.append('{}'.format(self.parameterAsDouble(parameters, self.DROP_TRESHOLDS, context)))
        arguments.append('{}'.format(self.parameterAsEnum(parameters, self.STEP, context)))

        arguments.append('-drp')
        arguments.append(self.parameterAsFileOutput(parameters, self.DROP_ANALYSIS, context))

        taudemUtils.execute(arguments, feedback)

        return self.algorithmResults(parameters)
