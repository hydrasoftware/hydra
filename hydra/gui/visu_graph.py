# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import json
import numpy
import datetime
import os
import shutil
import io
import glob
from dateutil.parser import parse
from matplotlib import dates as mdates, ticker
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT, FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QCoreApplication, Qt, QMargins
from qgis.PyQt.QtWidgets import QDialog, QTreeWidgetItem, QApplication, QMessageBox, QHBoxLayout, QWidget, QVBoxLayout, QLabel, QPushButton, QInputDialog, QHeaderView
from qgis.PyQt.QtGui import QImage
from hydra.utility.parser import parse as parse_datafile
from ..result import Result


def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

_desktop_dir = os.path.join(os.path.expanduser('~'), "Desktop")

with open(os.path.join(os.path.dirname(__file__), "visu.json")) as j:
    elements = json.load(j)

files_dir = {
    "w15":"hydraulique",
    "w14":"hydraulique",
    "w13":"hydrol"
}

titles = {
    "qam":'Flow(m3/s)',
    "zam":'Level(m)'
}

class Line:

    def __init__(self, file=None, project=None, color=None, table=None, date0=None, objct=None, column=None, model=None, scenario=None, time_format=None, shape='line', operation=''):

        if file is not None:
            df = parse_datafile(file, time_format, date0)
            self.name = ';'.join((os.path.basename(file), '', time_format, str(date0), column))
            self.date0 = date0
        else:
            res = Result(project, model, scenario)
            df = res.tables(table)[objct].dataframe
            self.name = ';'.join((scenario, model, objct, column))
            self.date0 = Result(project, model, scenario).date0


        self.dates = df['date()'].to_numpy()
        self.values = df[column].to_numpy()
        self.date0 = numpy.datetime64(self.date0)

        self.model = model
        self.scenario = scenario
        self.objct = objct
        self.color = color
        self.table = table
        self.time_format = time_format
        self.file =  file
        self.column = column
        self.shape = shape
        self.operation = operation

    def times(self, date0):
        'returns decimal hours relative to date0'
        return (self.dates - date0).astype('timedelta64[s]').astype(numpy.int32)/3600


class VisuGraph(QDialog):

    class NavigationToolbar(NavigationToolbar2QT):
        # only display the buttons we need
        toolitems = [t for t in NavigationToolbar2QT.toolitems if
                     t[0] in ('Home', 'Back', 'Forward', 'Pan', 'Zoom', 'Subplots')]

    def __init__(self, current_project, name_item=None, table_item=None, name_scn=None, parent=None, file=None, time_format=None, column=None, shape='line'):
        QDialog.__init__(self, parent)
        self.setWindowFlags(self.windowFlags() |
            Qt.WindowSystemMenuHint |
            Qt.WindowMinMaxButtonsHint)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "visu_graph.ui"), self)

        self.tabWidget.setCurrentIndex(0)

        self.__date0 = None
        self.__project = current_project
        self.__result_axe_1 = []
        self.__result_axe_2 = []
        self.__curves_axes = []
        self.__plot_list = []
        self.__dot_on_curve = None
        self.__axe1_colors = ["#0000ff", "#aa00ff", "#00aaff", "#00ffff", "#55007f", "#00557f"]
        self.__axe2_colors = ["#00ff00", "#005500", "#aaaa00", "#aaff00", "#aaaa7f", "#ffaa00"]

        self.main_frame = QWidget()

        self.__dpi = 75
        self.__fig = Figure(dpi=self.__dpi)
        self.__axe1 = self.__fig.add_subplot(111)
        self.__axe1.format_coord = lambda x,y: ""
        self.__axe2 = None
        self.canvas = FigureCanvas(self.__fig)
        self.canvas.setParent(self.main_frame)

        self.mpl_toolbar = VisuGraph.NavigationToolbar(self.canvas, self.main_frame)
        self.mpl_toolbar.setMaximumHeight(40)
        self.export_btn = QPushButton(tr("Copy data"))
        self.export_image_btn = QPushButton(tr("Copy image"))
        self.export_btn.clicked.connect(self.export_data_plot)
        self.export_image_btn.clicked.connect(self.export_image_plot)
        self.canvas.mpl_connect('motion_notify_event', self.__hover_curves)

        # graph toolbar
        hboxwidget = QWidget()
        hbox = QHBoxLayout(hboxwidget)
        hbox.setContentsMargins(QMargins(0,0,0,0))
        hbox.addWidget(self.mpl_toolbar)
        hbox.setAlignment(self.mpl_toolbar, Qt.AlignLeft)
        self.__labelXY = QLabel()
        hbox.addWidget(self.__labelXY)
        for w in [self.export_btn, self.export_image_btn]:
            hbox.addWidget(w)
            hbox.setAlignment(w, Qt.AlignRight)
        hbox.setContentsMargins(0,0,0,0)
        #hboxwidget.setFixedHeight(22);

        # graph widget
        vbox = QVBoxLayout()
        vbox.addWidget(hboxwidget)
        vbox.addWidget(self.canvas)
        vbox.setStretch(1,1)
        self.graph_placeholder.setLayout(vbox)

        self.graph_title.textChanged.connect(self.__on_graph_title_text_changed)
        self.left_axis_title.textChanged.connect(self.__on_left_title_text_changed)
        self.right_axis_title.textChanged.connect(self.__on_right_title_text_changed)

        # vmo failed to user QSortFilterProxyModel and resorted to brute force filtering in the objects attribute
        #self.filter_item = QSortFilterProxyModel(QStringListModel([], self))
        #self.cbo_objects.setModel(self.filter_item)

        self.filter.textChanged.connect(self.__on_filter_changed)

        # QTreeWidget
        self.curves.setColumnCount(1)
        labels = [tr("Axe"), tr("Curves")]
        self.curves.setHeaderLabels(labels)
        self.curves.setUniformRowHeights(True)
        self.__init_tree()

        self.btn_axe_1.clicked.connect(self.__add_axe1)
        self.btn_axe_2.clicked.connect(self.__add_axe2)
        self.btn_axe_1s.clicked.connect(self.__add_axe1s)
        self.btn_axe_2s.clicked.connect(self.__add_axe2s)
        self.btn_reset.clicked.connect(self.__reset)
        self.btn_remove.clicked.connect(self.__remove_selected_curve)
        self.save_graph.clicked.connect(self.__save_graph)
        self.load_graph.clicked.connect(self.__load_graph)
        self.next_graph.clicked.connect(self.__goto_next_graph)
        self.previous_graph.clicked.connect(self.__goto_previous_graph)
        self.chk_legend.stateChanged.connect(self.on_draw)
        self.date_rb.toggled.connect(self.on_draw)
        self.hour_rb.toggled.connect(self.on_draw)
        self.curves.header().setSectionResizeMode(QHeaderView.ResizeToContents)
        self.graph_title.setText("Hydra graph")
        self.graph_folder = os.path.join(self.__project.directory, "graphs")
        if not os.path.isdir(self.graph_folder):
            os.mkdir(self.graph_folder)

        self.datafile.fileChanged.connect(self.__datafile_changed)
        self.dateFormat.currentTextChanged.connect(self.__time_format_changed)

        self.cbo_scenario.addItems(self.scenarios)
        if name_scn:
            self.cbo_scenario.setCurrentText(name_scn.upper())

        # we just add one item to memorize imput values,
        # combo updates will be triggered by model change
        if self.__project.get_current_model():
            self.cbo_model.addItem(self.__project.get_current_model().name.upper())

        if table_item:
            self.cbo_types.addItem(table_item)
        if name_item:
            self.cbo_objects.addItem(name_item.upper())

        self.cbo_scenario.currentTextChanged.connect(self.__scn_changed)
        self.cbo_model.currentTextChanged.connect(self.__mdl_changed)
        self.cbo_types.currentTextChanged.connect(self.__type_changed)
        self.cbo_objects.currentTextChanged.connect(self.__object_changed)

        # triggers all refresh
        self.__scn_changed()

        if name_item and table_item and name_scn:
            self.__add_axe1()

        if file is not None:
            self.tabWidget.setCurrentIndex(1)
            self.datafile.setFilePath(file)
            self.dateFormat.setCurrentText(time_format)
            self.column.setCurrentText(column)
            if shape == 'step':
                self.__add_axe1s()
            else:
                self.__add_axe1()

        self.__update_status()

        self.show()

    @property
    def scenarios(self):
        return [os.path.basename(os.path.dirname(p)) for p in sorted(glob.glob(self.__project.directory+'/*/scenario.nom'))]

    @property
    def models(self):
        path = os.path.join(self.__project.directory, self.cbo_scenario.currentText(), 'scenario.nom')
        models = []
        if os.path.exists(path):
            with open(path) as s:
                for line in s:
                    line = line.strip().upper()
                    if line == '*GROUPAGE':
                        for ll in s:
                            ll = ll.strip()
                            if ll == '':
                                break
                            models += [m.upper() for m in ll.split()]
        return sorted(models)

    @property
    def objects(self):
        model = self.cbo_model.currentText()
        scenario = self.cbo_scenario.currentText()
        object_type = self.cbo_types.currentText()
        filt = self.filter.text().upper()
        return sorted(filter(lambda x: filt in x, Result(self.__project, model, scenario).tables(object_type).keys())) if model and scenario and object_type else []


    @property
    def object_types(self):
        model = self.cbo_model.currentText()
        #scenario = self.cbo_scenario.currentText()
        return sorted(set([ t for t, in self.__project.execute(f"""
            select distinct node_type||'_node' as t from {model}._node
            union all
            select distinct singularity_type||'_singularity' as t from {model}._singularity
            union all
            select distinct link_type||'_link' as t from {model}._link
            """).fetchall()]).intersection(elements.keys())) if model else []

    @property
    def results(self):
        model = self.cbo_model.currentText()
        scenario = self.cbo_scenario.currentText()
        object_type = self.cbo_types.currentText()
        objct = self.cbo_objects.currentText()
        return Result(self.__project, model, scenario).tables(object_type)[objct].columns[2:] if model and scenario and object_type and objct else []


    def __on_filter_changed(self, filt):
        VisuGraph.__update_combobox(self.cbo_objects, self.objects)

    def __time_format_changed(self, fmt=None):
        self.__datafile_changed(self.datafile.filePath())

    def __datafile_changed(self, path):
        self.column.clear()
        if os.path.isfile(path):
            try:
                df = parse_datafile(path, 'date' if self.dateFormat.currentText() == 'Date' else 'time', parse(self.timeOrigin.date().toString(Qt.ISODate)))
                self.column.addItems(df.columns[2:])
            except ValueError:
                pass

    @staticmethod
    def __update_combobox(cbo, items):
        item = cbo.currentText()
        cbo.blockSignals(True)
        cbo.clear()
        cbo.addItems(items)
        cbo.setCurrentText(item)
        cbo.blockSignals(False)
        cbo.currentTextChanged.emit(cbo.currentText())

    def __scn_changed(self):
        VisuGraph.__update_combobox(self.cbo_model, self.models)

    def __mdl_changed(self):
        VisuGraph.__update_combobox(self.cbo_types, self.object_types)

    def __type_changed(self):
        self.filter.setText('')
        VisuGraph.__update_combobox(self.cbo_objects, self.objects)

    def __object_changed(self):
        VisuGraph.__update_combobox(self.cbo_results, self.results)

    def __init_tree(self):
        ''' init the tree widget'''
        axe1 = QTreeWidgetItem()
        self.curves.addTopLevelItem(axe1)
        axe1.setText(0, tr('Axe 1'))
        axe2 = QTreeWidgetItem()
        self.curves.addTopLevelItem(axe2)
        axe2.setText(0, tr('Axe 2'))
        self.__curves_axes = []
        self.__curves_axes.append(axe1)
        self.__curves_axes.append(axe2)

    def __goto_next_graph(self):
        ''' connected to the next graph button , allow user to go at the next graph'''
        self.__goto_file(1)

    def __goto_previous_graph(self):
        ''' connected to the previous graph button , allow user to go at the previous graph'''
        self.__goto_file(-1)

    def __goto_file(self, offset):
        ''' get a file of the graph folder by its position'''
        graph_files = glob.glob(self.graph_folder+"/*.hgf")
        currentFile = os.path.join(self.graph_folder, str(self.graph_title.text()) +".hgf")
        if currentFile not in graph_files:
            return
        index_file = (graph_files.index(currentFile) + offset)%len(graph_files)
        self.__load_graph(graph_files[index_file])
        self.__update_status()

    def __update_status(self):
        graph_files = list(sorted(glob.glob(self.graph_folder+"/*.hgf")))
        currentFile = os.path.join(self.graph_folder, str(self.graph_title.text()) +".hgf")
        if currentFile not in graph_files:
            self.next_graph.setEnabled(False)
            self.previous_graph.setEnabled(False)
            return
        self.next_graph.setEnabled(True)
        self.previous_graph.setEnabled(True)

        n = graph_files.index(currentFile)
        t = len(graph_files)
        self.previous_graph.setText(f'{(n-1)%t + 1}/{t} <<')
        self.next_graph.setText(f'>> {(n+1)%t + 1}/{t}')

    def __save_graph(self):
        '''function which create a .hgf file and save the current graph in this faile'''
        fileName = os.path.join(self.graph_folder, str(self.graph_title.text()) +".hgf")

        if os.path.isfile(fileName) and QMessageBox.No == QMessageBox.question(self, tr('Visu tool'), str(tr("The file {f} already exists,\ndo you want overwrite?")).format(f=fileName), QMessageBox.Yes, QMessageBox.No):
            return

        data = dict()
        data["config"]={'title': str(self.graph_title.text()), 'left': str(self.left_axis_title.text()), 'right':str(self.right_axis_title.text())}

        for a, x in(('axe1', self.__result_axe_1), ('axe2', self.__result_axe_2)):
            data[a]=[{
                'name': n.name,
                'model': n.model,
                'scenario': n.scenario,
                'objct': n.objct,
                'color': n.color,
                'table': n.table,
                'time_format': n.time_format,
                'date0': str(n.date0),
                'file': n.file,
                'column': n.column,
                'shape': n.shape,
                'operation': n.operation
                } for n in x]

        with open(fileName, 'w') as fp:
            json.dump(data, fp, indent=4)

        QMessageBox.information(self, tr('Graphic saved'),
            str(tr('Graphic successfully saved to:\n {f}')).format(f=fileName), QMessageBox.Ok)
        self.__update_status()

    def __update_hgf_if_needed(self, filename):
        with open(filename) as fp:
            data = json.load(fp)

        if isinstance(data['config'], list): # converts old .hgf to new format
            new_data = {
                'config' : {
                    'title': data['config'][0],
                    'left': data['config'][1],
                    'right': data['config'][2]
                    }}
            for a in ('axe1', 'axe2'):
                new_data[a] = []
                for x in data[a]:
                    splt = [v.strip() for v in x[0].split(';')]

                    if len(splt[0]) and splt[0][-4:] in ('.hyd', '.dat', '.csv', '.xls'):
                        model = None
                        scenario = None
                        table = None
                        objct = None
                        column = splt[4].replace("'","").strip()
                        time_format = splt[2].lower()
                        date0 = splt[3]
                        file = os.path.join(self.graph_folder, splt[0])
                        if not date0:
                            date0, ok = QInputDialog.getText(self, 'Give origin date',
                                f'Enter the origin date of {file}:', text='2023-12-25 11:59:00')
                    else:
                        model = splt[1]
                        scenario = splt[0]
                        table = x[2]
                        objct = splt[2]
                        column = splt[3]
                        time_format = 'time'
                        date0 = str(self.__project.models[model].results[scenario].date0)
                        file = None

                    new_data[a].append({
                        'name': x[0],
                        'model': model,
                        'scenario': scenario,
                        'objct': objct,
                        'color': x[1],
                        'table': table,
                        'time_format': time_format,
                        'date0': date0,
                        'file': file,
                        'column': column,
                        'shape': 'line',
                        'operation': '',
                        })

            shutil.copy(filename, filename+'.old')

            with open(filename, 'w') as fp:
                json.dump(new_data, fp, indent=4)

            return new_data

        return data
##
    def __load_graph(self, filename=None):
        ''' function called to find and give a graph .hgf to load at load graph function'''
        filenames = list(sorted(glob.glob(self.graph_folder+"/*.hgf")))
        file=None
        if filename:
            file=filename
        elif len(filenames)==1:
            file=filenames[0]
        elif len(filenames)>1:
            f, ok = QInputDialog.getItem(None, "Graph selection", "Select the graph you want to display.", filenames, 0, False)
            if ok and f:
                file=f

        if file:
            data = self.__update_hgf_if_needed(file)

            self.__reset()
            self.graph_title.setText(data['config']['title'])
            self.left_axis_title.setText(data['config']['left'])
            self.right_axis_title.setText(data['config']['right'])

            if len(data['axe2']) and self.__axe2 is None:
                self.__axe2 = self.__axe1.twinx()
                self.__axe2.format_coord = lambda x,y: ""
                self.__on_right_title_text_changed()

            for pos, a, x in ((1, 'axe1', self.__result_axe_1), (2, 'axe2', self.__result_axe_2)):
                for c in data[a]:
                    del c['name']
                    c['project'] = self.__project
                    c['date0'] = parse(c['date0'])
                    x.append(Line(**c))
                    self.__add_line_to_curves_table(x[-1].name, pos)

        self.__update_status()
        self.on_draw()

    def __reset(self):
        self.__result_axe_1=[]
        self.__result_axe_2=[]
        self.curves.clear()
        self.__init_tree()
        self.on_draw()
        self.right_axis_title.setText("")
        self.left_axis_title.setText("")

    def __add_axe1(self):
        ''' add a line to axe1'''
        index = len(self.__result_axe_1)%len(self.__axe1_colors)
        self.__add_to_axe_results_from_gui(self.__result_axe_1, self.__axe1_colors[index])
        self.__add_line_to_curves_table(self.__result_axe_1[-1].name, 1)

    def __add_axe1s(self):
        ''' add a line to axe1'''
        index = len(self.__result_axe_1)%len(self.__axe1_colors)
        self.__add_to_axe_results_from_gui(self.__result_axe_1, self.__axe1_colors[index], 'step')
        self.__add_line_to_curves_table(self.__result_axe_1[-1].name, 1)

    def __add_axe2(self):
        ''' add a line to axe2'''
        if (self.__axe2 is None):
            self.__axe2 = self.__axe1.twinx()
        index = len(self.__result_axe_2)%len(self.__axe2_colors)
        self.__add_to_axe_results_from_gui(self.__result_axe_2, self.__axe2_colors[index])
        self.__add_line_to_curves_table(self.__result_axe_2[-1].name, 2)

    def __add_axe2s(self):
        ''' add a line to axe2'''
        if (self.__axe2 is None):
            self.__axe2 = self.__axe1.twinx()
        index = len(self.__result_axe_2)%len(self.__axe2_colors)
        self.__add_to_axe_results_from_gui(self.__result_axe_2, self.__axe2_colors[index], 'step')
        self.__add_line_to_curves_table(self.__result_axe_2[-1].name, 2)


    def __add_to_axe_results_from_gui(self, axe_result, color, shape='line'):
        ''' add to axe from the gui'''
        if self.tabWidget.currentIndex() == 0: # Simulation data

            table = self.cbo_types.currentText()
            scn_name = str(self.cbo_scenario.currentText())
            model = str(self.cbo_model.currentText())
            column = self.cbo_results.currentText()
            objct = self.cbo_objects.currentText()

            axe_result.append(Line(project=self.__project, table=table, scenario=scn_name, model=model, objct=objct, column=column, color=color, shape=shape))

        elif self.tabWidget.currentIndex() == 1: # Data from file

            path = self.datafile.filePath()
            date0 = parse(self.timeOrigin.date().toString(Qt.ISODate))
            time_format = 'date' if self.dateFormat.currentText() == 'Date' else 'time'
            column = self.column.currentText()

            axe_result.append(Line(file=path, date0=date0, time_format=time_format, column=column, color=color, shape=shape))

        self.on_draw()

    def __add_line_to_curves_table(self, line_name, num_axe):
        ''' add a line in the tree widget'''
        curveItem = QTreeWidgetItem(self.__curves_axes[num_axe-1])
        self.__curves_axes[num_axe-1].setExpanded(True)
        curveItem.setText(1, line_name)

    def __remove_selected_curve(self):
        getSelected = self.curves.selectedItems()
        if getSelected:
            base_node = getSelected[0]

            if base_node.parent() is None:
                return

            selected_curve = base_node.parent().indexOfChild(base_node)

            if base_node.parent().text(0) == self.__curves_axes[0].text(0):
                del self.__result_axe_1[selected_curve]
            else:
                del self.__result_axe_2[selected_curve]
            base_node.parent().removeChild(base_node)
            self.on_draw()

    def on_draw(self):
        ''' draw the canvas '''
        self.__plot_list = []
        self.__dot_on_curve = None
        self.__axe1.clear()

        if self.__axe2:
            self.__axe2.clear()

        #self.__date0 = None

        for res in self.__result_axe_1:
            res.__date0 = res.date0
            if res.shape == 'step':
                self.__plot_list.append(self.__axe1.step(
                    res.dates if self.date_rb.isChecked() else res.times(res.__date0),
                    res.values,
                    linestyle="-",
                    marker="",
                    color=res.color,
                    label=res.name,
                    where='post')[0])
            else:
                self.__plot_list.append(self.__axe1.plot(
                    res.dates if self.date_rb.isChecked() else res.times(res.__date0),
                    res.values,
                    linestyle="-",
                    marker=".",
                    color=res.color,
                    label=res.name)[0])

        for res in self.__result_axe_2:
            res.__date0 = res.date0
            if res.shape == 'step':
                self.__plot_list.append(self.__axe2.step(
                    res.dates if self.date_rb.isChecked() else res.times(res.__date0),
                    res.values,
                    linestyle="-",
                    marker=".",
                    color=res.color,
                    label=res.name,
                    where='post')[0])
            else:
                self.__plot_list.append(self.__axe2.plot(
                    res.dates if self.date_rb.isChecked() else res.times(res.__date0),
                    res.values,
                    linestyle="-",
                    marker=".",
                    color=res.color,
                    label=res.name)[0])

        if self.chk_legend.isChecked():
            if len(self.__result_axe_1)>0:
                self.__axe1.legend(loc=2)
            if len(self.__result_axe_2)>0:
                self.__axe2.legend(loc=1)

        # format data afterward because of duality date / numeric on axis ,
        # cannot support both type at the same time on twinx axis
        new_title = ""
        for res in self.__result_axe_1:
            new_title += ", " + (tr(titles[res.column]) if res.column in titles else res.column)
        new_title=new_title[2:]
        self.left_axis_title.setText(new_title)
        self.__format_axes(self.__axe1, 'b')
        self.__on_left_title_text_changed()

        new_title = ""
        for res in self.__result_axe_2:
            new_title += ", " + (tr(titles[res.column]) if res.column in titles else res.column)
        new_title=new_title[2:]
        self.right_axis_title.setText(new_title)
        if self.__axe2:
            self.__format_axes(self.__axe2, 'g')
            self.__on_right_title_text_changed()
            
        self.__axe1.set_xlabel(tr('Date') if self.date_rb.isChecked() else tr('Time(hr)'), fontsize=15)

        self.__fig.set_tight_layout(True)
        self.canvas.draw()

    def __on_graph_title_text_changed(self):
        self.__axe1.set_title(self.graph_title.text())
        self.canvas.draw()

    def __on_left_title_text_changed(self):
        self.__axe1.set_ylabel(self.left_axis_title.text(), fontsize=15, color='b')
        self.canvas.draw()

    def __on_right_title_text_changed(self):
        if self.__axe2:
            self.__axe2.set_ylabel(self.right_axis_title.text(), fontsize=15, color='g')
            self.__axe2.get_yaxis().set_label_position("right")
        self.canvas.draw()

    def __format_axes(self, axe, color):
        if self.date_rb.isChecked() :
            axe.get_xaxis().set_major_locator(mdates.AutoDateLocator())
            xmin, xmax = axe.get_xlim()
            if xmax - xmin < 7 :
                axe.get_xaxis().set_major_formatter(mdates.DateFormatter("%d-%m-%Y %H:%M"))
            else :
                axe.get_xaxis().set_major_formatter(mdates.DateFormatter("%d-%m-%Y"))
            for tick in axe.get_xaxis().get_ticklabels() :
                tick.set_rotation(65)
            axe.grid(True)
        else :
            axe.get_xaxis().set_major_formatter(ticker.ScalarFormatter(useMathText=True))
            axe.get_xaxis().get_major_formatter().set_useOffset(False)
            axe.grid(True)

        axe.tick_params(axis='both', which='major', labelsize=12)
        axe.tick_params(axis='both', which='minor', labelsize=12)

        # limits y scaling
        ymin, ymax = axe.get_ylim()
        if ymax - ymin < 1e-3:
            axe.set_ylim(ymin-1e-3, ymin+1e-3)
            axe.get_yaxis().get_major_formatter().set_useOffset(False)

        for tl in axe.get_yticklabels():
            tl.set_color(color)

    def export_data_plot(self):
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard)

        data_to_export = ""

        for res in self.__result_axe_1 + self.__result_axe_2:
            data_to_export += 'Time (H)' + '\t' +  res.name + '\t'
        data_to_export = data_to_export[:-1] + '\n'

        if (len(self.__result_axe_1)+len(self.__result_axe_2))>0:
            max_len = max([len(line.values) for line in self.__result_axe_1 + self.__result_axe_2])
            for i in range (max_len) :
                for line in self.__result_axe_1 + self.__result_axe_2 :
                    if i < len(line.values) and i < len(line.dates) :
                        data_to_export += str(to_datetime(line.dates[i]) if self.date_rb.isChecked() else line.times(line.__date0)[i]) + '\t' + str(line.values[i]) + '\t'
                    else :
                        data_to_export += '-\t-\t'
                data_to_export = data_to_export[:-1] + '\n'

        cb.setText(data_to_export, mode=cb.Clipboard)

    def export_image_plot(self):
        buf = io.BytesIO()
        self.__fig.savefig(buf)
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard )
        cb.setImage(QImage.fromData(buf.getvalue()), mode=cb.Clipboard)
        buf.close()

    def __hover_curves(self,event) :
        ''' fonction called when hovering a point in the canvas graph'''
        for line in self.__plot_list :
            cont, ind = line.contains(event)
            if cont :
                if self.__dot_on_curve is not None :
                    ll = self.__dot_on_curve.pop(0)
                    ll.remove()
                    del ll
                    self.__dot_on_curve = None
                x,y = line.get_data()

                if self.date_rb.isChecked() :
                    self.__labelXY.setText("{xlabel} : {x}  {ylabel} : {y}".format(
                        x=x[ind["ind"][0]].astype('datetime64[s]').item().strftime('%Y-%m-%d %H:%M:%S'),
                        y=y[ind["ind"][0]],
                        xlabel=self.__axe1.get_xlabel(),
                        ylabel=line.axes.get_ylabel()))
                else :
                    self.__labelXY.setText("{xlabel} : {x:.2f}  {ylabel} : {y:.4f}".format(
                        x=x[ind["ind"][0]],
                        y=y[ind["ind"][0]],
                        xlabel=self.__axe1.get_xlabel(),
                        ylabel=line.axes.get_ylabel()))
                line.axes.set_xlim(line.axes.get_xlim()) # disable autoscaling temporarily
                self.__dot_on_curve = line.axes.plot(x[ind["ind"][0]],y[ind["ind"][0]],marker=".",color='red')
                self.canvas.draw()
            else :
                self.canvas.draw()

def to_datetime(date):
    """
    Converts a numpy datetime64 object to a python datetime object
    """
    timestamp = ((date - numpy.datetime64('1970-01-01T00:00:00')) / numpy.timedelta64(1, 's'))
    dt = datetime.datetime.fromtimestamp(timestamp, datetime.UTC)
    dt = dt.replace(tzinfo=None)
    if dt.microsecond >= 500000:
        dt += datetime.timedelta(seconds=1)
    return dt.replace(microsecond=0)

if __name__ == '__main__':

    from qgis.PyQt.QtWidgets import QApplication
    from ..project import Project
    import sys

    app = QApplication(sys.argv)
    current_project = sys.argv[1]
    project = Project(current_project)
    #project.debug = True

    if len(sys.argv) == 6:
        current_model, name_item, table_item, name_scn = sys.argv[2:]
        project.set_current_model(current_model)
        dlg = VisuGraph(project, name_item, table_item, name_scn )
    elif len(sys.argv) == 5:
        file, time_format, column = sys.argv[2:]
        dlg = VisuGraph(project, file=file, time_format=time_format, column=column)
    else:
        dlg = VisuGraph(project)

    dlg.exec_()
