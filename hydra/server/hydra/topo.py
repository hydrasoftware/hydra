# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import numpy
import time
from io import StringIO
from shapely import wkb
from shapely.geometry import Point, LineString, Polygon
from math import sqrt

_max_iter = 1000000
_nb_points_interpolation = 1000
_nb_points_complete_bank = 10000

def __pg_array_to_list(arr):
    return [[float(v) for v in coord.split(',')] for coord in arr.replace('{{','').replace('}}','').split('},{')]

def __calc_new_start_end(coordstart, coordend):
    inc_x = (coordend[0] - coordstart[0])/(coordend[1]-coordstart[1])
    inc_y = (coordend[1]-coordstart[1])/(coordend[0] - coordstart[0])
    dist = sqrt((coordend[0] - coordstart[0])**2 + (coordend[1]-coordstart[1])**2)
    inc_x = (coordend[0] - coordstart[0])/dist
    inc_y = (coordend[1]-coordstart[1])/dist

    new_coords_start = (coordstart[0]-inc_x, coordstart[1]-inc_y, coordstart[2])
    new_coords_end = (coordend[0]+inc_x, coordend[1]+inc_y, coordend[2])
    return new_coords_start, new_coords_end

def matching_hydra_section(plpy,topo_minor_riverbed,section_brut):
    ''' topo_minor_riverbed : topography of the minor riverbed as list of list [[z,x],[z,x],...etc], section_brut : section as created by the program with [z,b] coordinates (6 points)
    the program will look to optimise the difference beetwen the section modelised for hydra and the section of the topography to have a difference less than 0.1%
    until this criteriea is not reached, we will increase or decrease the length of each couple (except the first one) by 1% each turn'''

    list_points_section = []
    list_points_section_neg = []
    list_points_section_pos = []
    list_points_topo = []

    # build the polygon for topography section
    for point in topo_minor_riverbed :
        list_points_topo.append(point)
    list_points_topo.append(list_points_topo[0])

    section_topo = Polygon(list_points_topo)

    # build the first polygon to compare section brut to topography section
    for s in section_brut :
        list_points_section_neg.append([-(s[0]/2),s[1]])
        list_points_section_pos.append([(s[0]/2),s[1]])

    list_points_section = list_points_section_neg + list_points_section_pos[::-1]
    list_points_section.append([-(section_brut[0][0]/2),section_brut[0][1]])

    hydra_section = Polygon(list_points_section)

    #init the hydra_section for computation
    section_temp = section_brut
    iter = 0

    # compare topography section to hydra section until there is less than 0.1% difference
    while (hydra_section.area / section_topo.area) > 1.001 or (hydra_section.area / section_topo.area) < 0.999 and iter < _max_iter :

        if (hydra_section.area / section_topo.area) > 1.001 :
            for i in range(1, len(section_temp)) :
                section_temp[i][0] = section_temp[i][0]*0.9999

        if (hydra_section.area / section_topo.area) < 0.999 :
            for i in range(1, len(section_temp)) :
                section_temp[i][0] = section_temp[i][0]*1.0001

        list_points_section = []
        list_points_section_neg = []
        list_points_section_pos = []

        for s in section_temp :
            list_points_section_neg.append([-(s[0]/2),s[1]])
            list_points_section_pos.append([(s[0]/2),s[1]])

        list_points_section = list_points_section_neg + list_points_section_pos[::-1]
        list_points_section.append([-(section_temp[0][0]/2),section_temp[0][1]])

        hydra_section = Polygon(list_points_section)
        iter += 1
        #plpy.notice(iter)

    if iter == _max_iter :
        plpy.error("the maximum number of iteration : {} has been reached, no convergence found".format(_max_iter))

    return section_temp

def create_section_bank(topo_bank,nb_point=6):
    '''topo_bank : topography of the river bank as [[z,b],[z,b],...etc] list nb_point: target number of point to reach as simplified geometry
    we will compute the area of the triangle made by a point and his 2 nearest neighbours and we eliminate the point with the lowest area each turn
    till we reach the targeted number of points'''

    section_simple = []
    keep_point = [True] * len(topo_bank)
    # we will make a list of the index we actually keep in the final section

    while (len([x for x in keep_point if x == True ])>nb_point) :

        index_list = [i for i, j in enumerate(keep_point) if j == True]
        area_min = max([z for [z,b] in topo_bank]) * max([b for [z,b] in topo_bank])

        # walk trough the points of the profil, we keep the index of the point with the smallest triangle made by 3 consecutive points.
        for k in range(len(index_list)-1):
            if k != 0 and k != len(index_list) :

                previous_point = topo_bank[index_list[k-1]]
                evaluate_point = topo_bank[index_list[k]]
                next_point = topo_bank[index_list[k+1]]

                polygon_test_list = []
                for i in range(index_list[k-1],index_list[k+1]) :
                    polygon_test_list.append(topo_bank[i])
                polygon_test_list.append(next_point)
                polygon_test_list.append(previous_point)

                polygon_test = Polygon(polygon_test_list)

                if polygon_test.area<area_min :
                    removed_index = index_list[k]
                    area_min = polygon_test.area

        # we remove the index of the eliminated point
        keep_point[removed_index] = False

    for i in range(len(keep_point)) :
        if keep_point[i] == True :
            section_simple.append(topo_bank[i])

    return(section_simple)

def complete_bank(topo_riverbed,bool_minor_riverbed):
    ''' complete the non monotous profil of a riverbed,
    add point where the z level intersect the topo and where there is no point'''

     # creation of an over-accurate topography to get intersection value for each z value.
    xdata = [x for [x,z] in topo_riverbed]
    zdata = [z for [x,z] in topo_riverbed]
    precision = (xdata[-1]-xdata[0])/_nb_points_complete_bank
    x_precise = numpy.arange(xdata[0],xdata[-1],precision)
    z_precise = numpy.interp(x_precise,xdata,zdata)

    list_point_topo =[]
    unique_zdata=list(set(zdata))

    # for all Z value we interpolate every intersection with the topo and add it to create a new topo (sorted_topo_byX)
    for z_init in unique_zdata :
        z_temp = [z-z_init for z in z_precise]
        list_idx = numpy.argwhere(numpy.diff(numpy.sign(z_temp)) != 0).reshape(-1)
        for i in range (len(list_idx)-1) :
            if list_idx[i]+1 == list_idx[i+1] :
                list_idx[i] = list_idx[i+1]
        list_idx = list(set(list_idx))
        for idx in list_idx :
            point = [round(x_precise[idx],2),round(z_init,2)]
            list_point_topo.append(point)

    # we sort the topo data and we dont forget to add the last point of the topo wich was excluded from the computation
    sorted_topo_byX = sorted(list_point_topo, key = lambda x: x[0])
    if bool_minor_riverbed :
        sorted_topo_byX.append([xdata[-1],zdata[-1]])
    else :
        sorted_topo_byX.append([max(xdata),max(zdata)])

    return(sorted_topo_byX)

def compute_tableau_BZ(topo_complete_and_sorted):
    ''' enter a fully completed and sorted topo with each point for each z considered in the dataset and
    then reduce it to a single occurence raw with an equivalent lenght'''

    xdata_all_points = [ x for [x,z] in topo_complete_and_sorted]
    zdata_all_points = [ round(z,2) for [x,z] in topo_complete_and_sorted]

    unique_Zdata = list(set(zdata_all_points))
    unique_Zdata.sort()
    BZ_tableau = []

    # then we take the values for each z to compute the lenght associated with each z

    # Explanation : we take index of point which their altitude is equal to Z (where z_temp[ind]==0)
    # then we take the indice of points which are below this z ( where z_temp[ind] < 0)
    # and we look if there is at least a value below the z altitude between two points in the intersection list
    # if it's the case, we create a couple with those two points, it's a sub-length.
    # then to get the global length for a given z value , we just have to sum the sub-lenght for this z.

    for z_unic in unique_Zdata :
        z_temp = [z-z_unic for z in zdata_all_points]
        list_idx_intersect = [ ind for ind, x in enumerate(z_temp) if z_temp[ind] == 0]
        couples_etendue_idx = []
        idx_where_z_inf_z_unic = [ind for ind, x in enumerate(z_temp) if z_temp[ind]<0]
        for i in range (len(list_idx_intersect)-1) :
            if len([idx for idx in idx_where_z_inf_z_unic if (idx> list_idx_intersect[i] and idx< list_idx_intersect[i+1])])>0 :
                couples_etendue_idx.append([list_idx_intersect[i],list_idx_intersect[i+1]])

        etendue = 0
        for couple in couples_etendue_idx :
            etendue += abs(xdata_all_points[couple[0]] - xdata_all_points[couple[1]])
        BZ_tableau.append([etendue,z_unic])

    return BZ_tableau


def create_BZ_minor_riverbed(topo_minor_riverbed) :
    ''' fonction that create the raw hydra section for the minor riverbed by the method of rising water level
    first step : we will create a full topo with a point for each intersection with each altitude in the topo_minor_riverbed
    then, as a second step : we will walk trough the full topo to evaluate the space for each altitude and create a Z,B couple for each value of Z in the initial topo'''

    # correct some raw data due to the interpolation for the riverbed's limit
    topo_minor_riverbed[0][0] = round(topo_minor_riverbed[0][0],2)
    topo_minor_riverbed[0][1] = round(topo_minor_riverbed[0][1],2)
    topo_minor_riverbed[-1][0] = round(topo_minor_riverbed[-1][0],2)
    topo_minor_riverbed[-1][1] = round(topo_minor_riverbed[-1][1],2)

    sorted_topo_byX = complete_bank(topo_minor_riverbed, True)

    BZ_tableau = compute_tableau_BZ(sorted_topo_byX)

    return BZ_tableau

def correct_profil(minor_river_bed,major_river_bed,nb_points) :

    for i in range(len(major_river_bed)) :
        if major_river_bed[i][1] < minor_river_bed[-1][1] :
            major_river_bed[i][1] = minor_river_bed[-1][1]

    while len(major_river_bed) < nb_points :
        major_river_bed.append(major_river_bed[-1])

    for i in range (len(major_river_bed)-1) :
        if major_river_bed[i][0] > major_river_bed[i+1][0] :
            major_river_bed[i][0] = major_river_bed[i+1][0]
        if major_river_bed[i][1] > major_river_bed[i+1][1] :
            major_river_bed[i][1] = major_river_bed[i+1][1]
    return(major_river_bed)


def calcul_profil(plpy,s_left_bank, s_right_bank, topo_profile, pk=None, pk_up=None, pk_down=None, zb_up=None, zb_down=None):
    '''returns [zb_left]+[zb_min]+[zb_right] array (list of 4+6+4 2-tuple)
    for the given topographic profile with pythonic way
    s_left_bank and s_right_bank are location of the bank along the profile
    given in in km
    pk, pk_up, pk_down are the location of current, up and down cross-sections
    zb_up, zb_down are the zb arrays for the up and down cross-sections
    '''
    start_time = time.time()
    #plpy.notice("start",time.time()-start_time)

    xdata = [x for [x,z] in topo_profile ]
    zdata = [z for [x,z] in topo_profile ]

    #interpolation of riverbed's limit
    z_left_bank = numpy.interp(s_left_bank,xdata,zdata)
    z_right_bank = numpy.interp(s_right_bank,xdata,zdata)

    topo_left_bank = [[x,z] for [x,z] in topo_profile if x<s_left_bank]
    topo_right_bank = [[x,z] for [x,z] in topo_profile if x>s_right_bank]

    #introduction of riverbed's limit in both major riverbed profil
    topo_left_bank.append([s_left_bank,z_left_bank])
    topo_right_bank.insert(0,[s_right_bank,z_right_bank])

    #conversion en profil B_Z avec le point de délimitation comme referent 0
    left_bank_zb = [[abs(x-topo_left_bank[-1][0]),z] for [x,z] in topo_left_bank]
    right_bank_zb = [[x-topo_right_bank[0][0],z] for [x,z] in topo_right_bank]

    # simplification topologique en 12 points
    #plpy.notice("start simplification 12 points",time.time()-start_time)
    left_bank_zb = create_section_bank(left_bank_zb,max(12,len(left_bank_zb)))
    right_bank_zb = create_section_bank(right_bank_zb,max(12,len(right_bank_zb)))
    #plpy.notice("end simplification 12 points",time.time()-start_time)

    # re_work major riverbed to solve non monotonous profil problem

    z_left_bank_zb = [z for [b,z] in left_bank_zb]
    unique_z_left_bank_zb = list(set(z_left_bank_zb))
    unique_z_left_bank_zb.sort(reverse = True)
    fictive_wall_left_bank = [[0,z] for z in unique_z_left_bank_zb]

    left_bank_complete = complete_bank(left_bank_zb[::-1],False)
    reworked_left_bank_zb = fictive_wall_left_bank + left_bank_complete
    left_bank_zb_monotonous = compute_tableau_BZ(reworked_left_bank_zb)

    z_right_bank_zb = [z for [b,z] in right_bank_zb]
    unique_z_right_bank_zb = list(set(z_right_bank_zb))
    unique_z_right_bank_zb.sort(reverse = True)
    fictive_wall_right_bank = [[0,z] for z in unique_z_right_bank_zb]

    right_bank_complete = complete_bank(right_bank_zb,False)
    #plpy.notice("right bank completed",time.time()-start_time)
    reworked_right_bank_zb = fictive_wall_right_bank + right_bank_complete
    right_bank_zb_monotonous = compute_tableau_BZ(reworked_right_bank_zb)
    #plpy.notice("data computed completed",time.time()-start_time)

    # creation des profils lits majeurs, pas de recalcul d'aire
    section_left_bank = create_section_bank(left_bank_zb_monotonous,4)
    section_right_bank = create_section_bank(right_bank_zb_monotonous,4)

    #plpy.notice("end recalcul major riverbed",time.time()-start_time)

    #computation for minor riverbed

    lowest_point = [[x,z] for [x,z] in topo_profile if z == min(zdata)]
    lowest_point = lowest_point[0]

    topo_minor_riverbed = [[round(x,3),round(z,3)] for [x,z] in topo_profile if (x<s_right_bank and x>s_left_bank)]

    xdata_gauche = [x for [x,z] in topo_profile if x <= lowest_point[0]]
    xdata_droit = [x for [x,z] in topo_profile if x >= lowest_point[0]]
    zdata_gauche = [z for [x,z] in topo_profile if x <= lowest_point[0]]
    zdata_droit = [z for [x,z] in topo_profile if x >= lowest_point[0]]

    # correction suivant la limite de lit mineure la plus haute.
    # Attention : le zmax_lit mineur ne doit être egal au Z du point le plus bas,
    # sinon l'index de la courbe interpolé est nul et le programme renvoi une erreur
    # par ailleurs si un point du lit mineur se retrouve avec un z supérieur aux limites des berges
    # du lit mineur, le programme ne peut aboutir à une section cohérente

    # fix depending on the highest riverbed limit.
    # Warning : the min of the riverbed cannot be the same as riverbed's limit,
    # because the interpolate curve's index is null and raise an error.
    # however , if there is a point in the minor riverbed
    # the programm cannot conclude and give a proper section for the given topography.

    #plpy.notice("start calcul minor riverbed",time.time()-start_time)

    if topo_left_bank[-1][1] > topo_right_bank[0][1] :
        #interpolation de la limite exacte
        z_max_lit_mineur = topo_right_bank[0][1]
        precision = (xdata_gauche[-1]- xdata_gauche[0])/_nb_points_interpolation
        x_precise = numpy.arange(xdata_gauche[0],xdata_gauche[-1],precision)
        z_precise = numpy.interp(x_precise,xdata_gauche,zdata_gauche)
        z_precise = [ z-z_max_lit_mineur for z in z_precise]
        if -2 in numpy.diff(numpy.sign(z_precise)):
            idx = numpy.argwhere(numpy.diff(numpy.sign(z_precise)) != 0)
            #idx is the index's list of the intersection of the topography and the line z = z_max_riverbed
            simple_idx = [item for [item] in idx][0]
            if len(idx) != 1 : # if the index is not unique we will use the best index for the computation
                simple_idx = [item for [item] in idx][-1]
                # for the left bank, the best index is the last intersection with the z level, which give the minimal riverbed possible.
        else :
            plpy.error("Z of the limit is the same as z of the lowest point in minor riverbed")
        x_interp_left = x_precise[simple_idx]
        topo_minor_riverbed = [[x,z] for [x,z] in topo_minor_riverbed if z<z_max_lit_mineur]
        topo_minor_riverbed.insert(0,[x_interp_left,z_max_lit_mineur])
        topo_minor_riverbed.append(topo_right_bank[0])

        # correction for the highest riverbed's limit
        corr_lit_majeur = abs(x_interp_left-topo_left_bank[-1][0])
        for l in range(len(section_left_bank)) :
            section_left_bank[l][0] = section_left_bank[l][0]+corr_lit_majeur

    elif topo_left_bank[-1][1] < topo_right_bank[0][1] :
        # interpolation de la limite exacte
        z_max_lit_mineur = topo_left_bank[-1][1]
        precision = (xdata_droit[-1] - xdata_droit[0])/_nb_points_interpolation
        x_precise = numpy.arange(xdata_droit[0],xdata_droit[-1],precision)
        z_precise = numpy.interp(x_precise,xdata_droit,zdata_droit)
        z_precise = [ z-z_max_lit_mineur for z in z_precise]
        if 2 in numpy.diff(numpy.sign(z_precise)):
            idx = numpy.argwhere(numpy.diff(numpy.sign(z_precise)) != 0)
            #idx is the index's list of the intersection of the topography and the line z = z_max_riverbed
            simple_idx = [item for [item] in idx][0]
            if len(idx) != 1 : # if the index is not unique we will use the best index for the computation
                simple_idx = [item for [item] in idx][0]
                # for the right bank, the best index is the first intersection with the z level, which give the minimal riverbed possible.
        else :
            plpy.error("Z of the limit is the same as z of the lowest point in minor riverbed")
        x_interp_right = x_precise[simple_idx]
        topo_minor_riverbed = [[x,z] for [x,z] in topo_minor_riverbed if z<z_max_lit_mineur]
        topo_minor_riverbed.insert(0,topo_left_bank[-1])
        topo_minor_riverbed.append([x_interp_right,z_max_lit_mineur])

        # correction for the highest riverbed's limit
        corr_lit_majeur = abs(x_interp_right-topo_right_bank[0][0])
        for l in range(len(section_right_bank)) :
            section_right_bank[l][0] = section_right_bank[l][0]+corr_lit_majeur

    tableau_BZ = create_BZ_minor_riverbed(topo_minor_riverbed)

    section_brut = create_section_bank(tableau_BZ,6)

    #plpy.notice("end calcul minor riverbed as brut go for matching section",time.time()-start_time)

    section_brut = matching_hydra_section(plpy,topo_minor_riverbed,section_brut)

    #plpy.notice("section matched , convergence found",time.time()-start_time)

    #check on major riverbed to correct if if needed
    section_left_bank = correct_profil(section_brut,section_left_bank,4)
    section_right_bank = correct_profil(section_brut,section_right_bank,4)

    # re-arranged data for export
    zb_min = [[z,round(b,3)] for [b,z] in section_brut]
    zb_majl = [[z,round(b,3)] for [b,z] in section_left_bank]
    zb_majr = [[z,round(b,3)] for [b,z] in section_right_bank]

    while len(zb_min) < 6 :
        zb_min.append(zb_min[-1])

    return zb_majl+zb_min+zb_majr


def profile_from_transect(plpy, transect, model, srid, interpolate=False, discretization=150, ignore_points=False, distance_point=100):
    '''returns a string representation of the array,
    the riverbed is taken fron the two transect points closest to the reach intersection
    a single point of intersection is assumed'''

    #plpy.notice("we enter topo module")
    plpy.log("we enter topo module log")

    #@todo find upstream and downstream profiles in this query
    inter = plpy.execute("""
        with reach as (
            select r.id, st_intersection(r.geom, 'SRID={srid};{transect}'::geometry) as inter, geom, pk0_km
            from {model}.reach as r
            where st_intersects(r.geom, 'SRID={srid};{transect}'::geometry)
        ),
        pk as (
            select st_linelocatepoint(geom, inter)*st_length(geom)/1000+pk0_km as pk from reach
        ),
        up_ as (
            select coalesce((select zbmin_array from {model}.valley_cross_section_geometry as g where g.id = p.down_vcs_geom),
                            (select zbmin_array from {model}.valley_cross_section_geometry as g where g.id = p.up_vcs_geom)) as zbmin,
                   n.pk_km
            from {model}.river_cross_section_profile as p
            join {model}.river_node as n on p.id=n.id
            where p.id = {model}.upstream_valley_section('SRID={srid};{transect}'::geometry)
        ),
        down_ as (
            select coalesce((select zbmin_array from {model}.valley_cross_section_geometry as g where g.id = p.up_vcs_geom),
                            (select zbmin_array from {model}.valley_cross_section_geometry as g where g.id = p.down_vcs_geom)) as zbmin,
                   n.pk_km
            from {model}.river_cross_section_profile as p
            join {model}.river_node as n on p.id=n.id
            where p.id = {model}.downstream_valley_section('SRID={srid};{transect}'::geometry)
        )
        select r.id as reach, pk.pk, r.inter, (select zbmin from up_)::varchar as su,  (select pk_km from up_) as pku,  (select zbmin from down_)::varchar as sd, (select pk_km from down_) as pkd
        from reach as r, pk
        """.format(
            model=model,
            transect=transect,
            discretiz=str(discretization),
            srid=srid
            ))
    if not inter:
        return None
    inter_reach = wkb.loads(inter[0]['inter'], True)
    transect_geom = wkb.loads(transect, True)

    if len(transect_geom.coords)==2:
        coords = transect_geom.coords
        new_coords_start, new_coords_end = __calc_new_start_end(coords[0],coords[1])
        geom=[new_coords_start]
        geom.extend(coords)
        geom.append(new_coords_end)
        transect_geom = LineString(geom)
        left_bank_pt = 2

    if len(transect_geom.coords)>=3:
        s_center = transect_geom.project(inter_reach)
        s = numpy.array([transect_geom.project(Point(p)) for p in transect_geom.coords])
        left_bank_pt = len(s[s < s_center])
        rigth_bank_pt = len(s[s > s_center])
        coords = transect_geom.coords
        if left_bank_pt==1:
            new_coords_start, new_coords_end = __calc_new_start_end(coords[0],coords[1])
            geom=[new_coords_start]
            geom.extend(coords)
            transect_geom = LineString(geom)
            left_bank_pt = 2
        if rigth_bank_pt==1:
            new_coords_start, new_coords_end = __calc_new_start_end(coords[left_bank_pt-1],coords[left_bank_pt])
            geom=[]
            geom.extend(coords)
            geom.append(new_coords_end)
            transect_geom = LineString(geom)

    s_center = transect_geom.project(inter_reach)
    s = numpy.array([transect_geom.project(Point(p)) for p in transect_geom.coords])
    s_left_bank = s[left_bank_pt-1]
    s_right_bank = s[left_bank_pt]
    # plpy.warning("geom: ", " ".join(["({} {} {})".format(p[0], p[1], p[2]) for p in transect_geom.coords]))
    # plpy.warning("s: ", s)
    # plpy.warning("s_left_bank: ", s_left_bank)
    # plpy.warning("s_right_bank: ", s_right_bank)

    topo_transect = plpy.execute("""select project.set_altitude(project.discretize_line('SRID={srid};{transect}'::geometry, st_length('SRID={srid};{transect}'::geometry)/{discretiz})) as geom""".format(
            transect=transect_geom,
            discretiz=str(discretization), srid=srid))

    topo_profile = [(transect_geom.project(Point(p)), p[2]) for p in wkb.loads(topo_transect[0]['geom'], True).coords]

    if ignore_points or interpolate:
        topo_final=topo_profile
    else:
        points_profile_geom = plpy.execute("""
                        select p.id,
                        p.z_ground as z,
                        st_linelocatepoint('SRID={srid};{transect}'::geometry, p.geom)*st_length('SRID={srid};{transect}'::geometry) as pk
                        from project.points_xyz as p
                        where st_intersects(ST_Buffer('SRID={srid};{transect}'::geometry, {distance}, 'endcap=flat join=round'),p.geom )=true
                        or st_startpoint('SRID={srid};{transect}'::geometry)=p.geom
                        or st_endpoint('SRID={srid};{transect}'::geometry)=p.geom
                        order by pk""".format(model=model, transect=transect_geom, distance=str(distance_point), srid=srid))


        if len(points_profile_geom)>1:
            points_profile = numpy.array([(p['pk'], p['z']) for p in points_profile_geom])
            start = points_profile[0][0]
            end = points_profile[-1][0]
            calculation_interval = (end-start)/_nb_points_interpolation
            point_profil_x = [x for [x,z] in points_profile]
            point_profil_z = [z for [x,z] in points_profile]
            point_profil_interp_x = numpy.arange(start,end,calculation_interval)
            point_profil_interp_z = numpy.interp(point_profil_interp_x,point_profil_x,point_profil_z)
            points_profile =  []
            for i in range(len(point_profil_interp_x)) :
                points_profile.append([point_profil_interp_x[i],point_profil_interp_z[i]])
            points_profile = numpy.array(points_profile)

            topo_final = [(p[0],p[1]) for p in topo_profile if p[0]<start]
            if len(topo_final)>1:
                topo_final=numpy.concatenate((topo_final, points_profile))
            else:
                topo_final=points_profile
            for p in topo_profile:
                if p[0]>end:
                    topo_final=numpy.concatenate((topo_final,[p]))
        else:
            topo_final=topo_profile
    #plpy.warning("intersection at ", s_center, s, s_left_bank, s_right_bank, inter[0]['su'], inter[0]['sd'])

    if interpolate and inter[0]['su'] is not None and inter[0]['sd'] is not None:
        pk, pk_up, pk_down =  inter[0]['pk'], inter[0]['pku'], inter[0]['pkd']
        zb_up, zb_down = __pg_array_to_list(inter[0]['su']), __pg_array_to_list(inter[0]['sd'])
        return str(calcul_profil(plpy,s_left_bank, s_right_bank, topo_final,
            pk, pk_up, pk_down, zb_up, zb_down)).replace('[', '{').replace('(', '{').replace(']', '}').replace(')', '}')
    else:
        return str(calcul_profil(plpy,s_left_bank, s_right_bank, topo_final)).replace('[', '{').replace('(', '{').replace(']', '}').replace(')', '}')
