# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from qgis.core import QgsPointXY, QgsRectangle
from qgis.PyQt import uic
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QTableWidgetItem, QAbstractItemView, QTextEdit, QInputDialog, QHeaderView, QMessageBox, QApplication
from .base_dialog import BaseDialog, tr
from hydra.gui.edit import EditTool
from hydra.gui.delete import DeleteTool
from hydra.gui.widgets.combo_with_values import ComboWithValues
from hydra.utility.tables_properties import TablesProperties
import hydra.utility.sql_json as sql_json

class ConfigurationTool(BaseDialog):
    def __init__(self, project, iface, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "configuration.ui"), self)

        self.project = project
        self.properties = TablesProperties.get_properties()
        self.iface = iface

        self.buttonBox.accepted.connect(self.sav)
        self.buttonBox.rejected.connect(self.close)
        self.copy_button.clicked.connect(self.__copy)
        self.edit_button.clicked.connect(self.edit)
        self.zoom_button.clicked.connect(self.zoom)
        self.delete_button.clicked.connect(self.delete)

        self.model.addItems(self.project.get_models())
        if self.project.get_current_model() is not None :
            self.model.setCurrentIndex(self.project.get_models().index(self.project.get_current_model().name))
        else :
            self.model.setCurrentIndex(0)

        self.model.activated.connect(self.model_changed)

        self.config_combo = ComboWithValues(self.config_combo_placeholder)
        self.config_combo.activated.connect(self.config_changed)
        self.model_changed()

    def model_changed(self):
        if self.model.currentIndex() != -1 :
            self.project.set_current_model(self.model.currentText())

            self.config_combo.clear()
            configs =  self.project.get_current_model().get_configurations()
            for config in configs:
                if config[1] != 'default':
                    self.config_combo.addItem(config[1], config[0])
            self.config_combo.addItem('All configurations', 0)
            self.config_combo.set_selected_value(self.project.get_current_model().get_current_configuration())
            self.refresh()
        else :
            self.project.log.warning(tr("No active model"))
            self.project.set_current_model(None)

    def config_changed(self):
        self.delete_button.setEnabled(self.config_combo.get_selected_value()!=0)
        self.refresh()

    def refresh(self):
        self.data_table=[]
        self.config_table.setRowCount(0)
        # "All Configurations"
        if self.config_combo.get_selected_value() == 0:
            configs = [name for id, name in self.project.get_current_model().get_configurations() if (id, name)!=(1, 'default')]
        else:
            configs = [str(self.config_combo.currentText())]

        self.config_table.setColumnCount(5 + len(configs))
        title_columns = [tr('Type'),tr('Id'),tr('Name'), tr('Parameter'), tr('Default value')] + configs
        self.config_table.setHorizontalHeaderLabels(title_columns)

        configured = self.project.execute("""select subtype::varchar||'_'||type::varchar, id, name, configuration as config from {}.configured order by type, subtype, name""".format(self.model.currentText())).fetchall()

        for item in configured:
            item_table = item[0]
            dict_brut = item[3]
            for cfg in list(dict_brut.keys()):
                if cfg!='default' and cfg not in configs:
                    dict_brut.pop(cfg)
            dict_cleaned_up = self.__clean_dict(dict_brut)
            default_value = dict_cleaned_up['default']

            for param in list(default_value.keys()):
                row_position = self.config_table.rowCount()
                self.config_table.insertRow(row_position)
                self.config_table.setRowHeight(row_position, self.config_table.rowHeight(row_position))
                self.config_table.setItem(row_position, 0, QTableWidgetItem(self.properties[item_table]['name']))
                self.config_table.setItem(row_position, 1, QTableWidgetItem(item[1].split(':')[1]))
                self.config_table.setItem(row_position, 2, QTableWidgetItem(item[2]))
                self.config_table.setItem(row_position, 3, QTableWidgetItem(str(param)))
                self.config_table.setItem(row_position, 4, QTableWidgetItem(str(default_value[param])))
                for config_name in configs:
                    if config_name in list(dict_cleaned_up.keys()):
                        config_value = dict_cleaned_up[config_name]
                        if param in list(config_value.keys()):
                            self.config_table.setItem(row_position, 5+configs.index(config_name), QTableWidgetItem(str(config_value[param])))

                self.data_table.append([item_table, item[1].split(':')[1]])

        self.config_table.resizeColumnsToContents()
        self.number_invalid.setText(str(len(configured)))

    def __clean_dict(self, dict):
        res = {}
        for cfg in list(dict.keys()):
            res[cfg]={}
        for param in list(dict['default'].keys()):
            for cfg in list(dict.keys()):
                # keep value if:
                #   it's a default value  OR  (value is None and different from default value
                if param in dict[cfg] and (cfg=='default' or (dict[cfg][param] is not None and dict[cfg][param]!=dict['default'][param])):
                    res[cfg][param] = dict[cfg][param]
                # else use ''
                else:
                    res[cfg][param] = ''

            # if values in all configs (except default) are empty (''), then pop them all
            if all([res[cfg][param]=='' for cfg in list(res.keys()) if cfg!='default']):
                for cfg in list(res.keys()):
                    res[cfg].pop(param)
        return res

    def edit(self):
        # sets current selection to first item selected (zooms on one item)
        selection = self.config_table.selectedItems()
        self.config_table.selectionModel().clearSelection()
        self.config_table.setCurrentItem(selection[0])

        if self.config_table.currentItem() is None:
            return
        EditTool.edit(self.project,
                        self.data_table[self.config_table.currentRow()][0],
                        self.data_table[self.config_table.currentRow()][1])
        self.refresh()
        return

    def delete(self):
        if self.config_combo.get_selected_value() is None or self.config_combo.get_selected_value() == 0:
            return

        selected_rows = [i.row() for i in self.config_table.selectedItems()]
        if selected_rows:
            to_delete = []
            for row in selected_rows:
                if [self.data_table[row][0], self.data_table[row][1]] not in to_delete:
                    to_delete.append([self.data_table[row][0], self.data_table[row][1]])
            for table, name in to_delete:
                self.project.get_current_model().drop_config_on_object(self.config_combo.get_selected_value(), table, name)

        self.refresh()
        return

    def zoom(self):
        # sets current selection to first item selected (zooms on one item)
        selection = self.config_table.selectedItems()
        self.config_table.selectionModel().clearSelection()
        self.config_table.setCurrentItem(selection[0])

        if self.config_table.currentItem() is not None:
            type, geom = self.project.execute("""select ST_GeometryType(ST_Envelope(geom)), ST_AsText(ST_Envelope(geom))  from {s}.{t} where id={i}
                                            """.format(s=self.project.get_current_model().name,
                                                       t=self.data_table[self.config_table.currentRow()][0],
                                                       i=self.data_table[self.config_table.currentRow()][1])).fetchone()
            self.__zoom_canvas(type, geom)

    def __zoom_canvas(self, type, geom):
        if type == 'ST_Point':
            # geom = 'POINT(X Y)'
            x, y = geom[6:-1].split(' ')
            point1 = QgsPointXY(float(x)-10, float(y)-10)
            point2 = QgsPointXY(float(x)+10, float(y)+10)
        elif type == 'ST_Polygon':
            # geom = 'POLYGON((X1 Y1,X2 Y2,X3 Y3,X4 Y4))'
            x1, y1 = geom[9:-2].split(',')[0].split(' ')
            x2, y2 = geom[9:-2].split(',')[2].split(' ')
            point1 = QgsPointXY(float(x1), float(y1))
            point2 = QgsPointXY(float(x2), float(y2))
        if point1:
            extent = QgsRectangle(point1, point2)
            self.iface.mapCanvas().setExtent(extent)
            self.iface.mapCanvas().refresh()
            self.close()
        else:
            return

    def __copy(self):
        copied_text = "\t".join([str(self.config_table.horizontalHeaderItem(i).text()) for i in range(self.config_table.columnCount())]) + "\n"
        for i in range(self.config_table.rowCount()):
            for j in range(self.config_table.columnCount()):
                try:
                    copied_text += self.config_table.item(i, j).text() + "\t"
                except AttributeError:
                    # quand une case n'a jamais été initialisée
                    copied_text += "\t"
            copied_text = copied_text[:-1] + "\n"  # le [:-1] élimine le '\t' en trop
        copied_text = copied_text[:-1]  # le [:-1] élimine le '\n' en trop
        # enregistrement dans le clipboard
        QApplication.clipboard().setText(copied_text)

    def sav(self):
        self.save()
        self.close()