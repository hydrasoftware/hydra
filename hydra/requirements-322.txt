-i https://gitlab.com/api/v4/projects/52774167/packages/pypi/simple
rasterio==1.3.9
numpy==1.24.1
scipy==1.10.1
shapely==1.8.0
pandas==1.1.3
wstar==0.5.2
rtree

