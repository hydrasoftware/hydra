/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*              Dry inflow                        */
/* ********************************************** */

create table project.dry_inflow_hourly_modulation(
    id serial primary key,
    name varchar(24) not null default project.unique_name('MODUL_'),
    comment varchar(256),
    hv_array real[][] check(array_length(hv_array, 2)=3)
)
;;

create table project.dry_inflow_sector(
    id serial primary key,
    name varchar(24) not null default project.unique_name('SECT_'),
    vol_curve integer references project.dry_inflow_hourly_modulation(id),
    comment varchar(256),
    geom geometry('POLYGON',$srid) not null check(ST_IsValid(geom))
)
;;

create table project.dry_inflow_scenario(
    id serial primary key,
    name varchar(24) not null default project.unique_name('DRYSCN_'),
    period hydra_dry_inflow_period not null default 'weekday',
    comment varchar(256)
)
;;

create table project.dry_inflow_scenario_sector_setting(
    dry_inflow_scenario integer not null references project.dry_inflow_scenario(id) on delete cascade,
    sector integer not null references project.dry_inflow_sector(id) on delete cascade,
        unique(dry_inflow_scenario, sector),
    volume_sewage_m3day real not null default 0,
    coef_volume_sewage_m3day real not null default 0,
    volume_clear_water_m3day real not null default 0,
    coef_volume_clear_water_m3day real not null default 0
)
;;

/* ********************************************** */
/*              Rainfalls                         */
/* ********************************************** */

create table project._rainfall(
    id serial primary key,
    rainfall_type hydra_rainfall_type not null,
        unique(id, rainfall_type),
    name varchar(24) not null,
    comment varchar(256),
    validity boolean
)
;;

create table project.coef_montana(
    id serial primary key,
    name varchar(24) not null default project.unique_name('MONTANA_'),
    comment varchar(256),
    return_period_yr real not null,
    coef_a real not null check(coef_a > 0),
    coef_b real not null check(coef_b < 0)
)
;;

create table project.rain_gage(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('RG_'),
    comment varchar(256),
    geom geometry('POINTZ',$srid) not null
)
;;

create table project.radar_grid(
    id integer not null default 1 unique check (id=1),
    origin_point geometry('POINT',$srid) not null default ST_SetSRID('POINT(0 0)'::geometry, $srid) check(ST_IsValid(origin_point)),
    dx real not null default 1000  check (dx>=0),
    dy real not null default 1000  check (dy>=0),
    nx integer not null default 0 check (nx>=0),
    ny integer not null default 0 check (ny>=0)
)
;;

insert into project.radar_grid default values;
;;

/* ********************************************** */
/*              Wind scenario                     */
/* ********************************************** */

create table project.wind_gage(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('WG_'),
    comment varchar(256),
    geom geometry('POINTZ',$srid) not null
)
;;

create table project.wind_scenario(
    id serial primary key,
    name varchar(24) not null default 'WIND_'||currval('project.wind_scenario_id_seq')::varchar,
    grid_connect_file varchar(256) not null default '',
    interpolation hydra_rainfall_interpolation_type not null default 'shortest_distance'
)
;;

create table project.wind_scenario_anemometer_data(
    wind_gage integer unique not null references project.wind_gage(id),
    wind_scenario integer unique not null references project.wind_scenario(id),
        unique(wind_gage, wind_scenario),
    hwd_array real[][] check(array_length(hwd_array, 2)=3)
)
;;


