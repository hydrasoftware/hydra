/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

create view $model.$tablename as
    select p.id, p.name, $tableview, p.geom, p.configuration::varchar as configuration, p.generated, p.validity, p.configuration as configuration_json, p.comment
    from $model._$tablename as c, $model._node as p
    where p.id = c.id
;;

/* note: id and node_type are not updatable */
create function ${model}.${tablename}_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated, comment)
                values ($tableid, coalesce(new.name, 'define_later'), $constraingeom, new.configuration::json, new.generated, new.comment)
                returning
                id, geom into new.id, new.geom;
            update $model._node set name = (select abbreviation||new.id::varchar
                from hydra.node_type where name = $tableid) where name = 'define_later' and id = new.id;
            insert into $model._$tablename(id, node_type, $tablecolumns)
                values (new.id, $tableid, $tableinsert)
                returning $tablecolumns into $newtablecolumns;
            perform $model.add_configuration_fct(new.configuration::json, new.id, '$tablename');

            -- Lines to update specific nodes that works with associated contours
            if $tableid = $storage_node_type then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=new.id;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            update $model._node set validity = (select $validity from  $model._$tablename where id = new.id) where id = new.id;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if (($newconfigfields) is distinct from ($oldconfigfields)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select $oldconfigfields) as o, (select $newconfigfields) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select $newconfigfields) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=$constraingeom, generated=new.generated, comment=new.comment where id=old.id returning geom into new.geom;
            ${commentupdate}update $model._$tablename set $tableupdate where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, '$tablename');

            -- Lines to update specific nodes that works with associated contours
            if $tableid = $catchment_node_type then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if $tableid = $river_node_type and not ST_equals(new.geom, old.geom) then
                update $model._river_node set reach=(select id from $model.reach as r where ST_DWithin(new.geom, r.geom, 0.1) order by ST_Distance(new.geom, r.geom) asc limit 1) where id=old.id;
            end if;
            if $tableid = $storage_node_type then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            update $model._node set validity = (select $validity from  $model._$tablename where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if $tableid = $storage_node_type and (select trigger_coverage from $model.metadata) then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            delete from project.interlink where (model_up='$model' and node_up=old.id) or (model_down='$model' and node_down=old.id);

            delete from $model._$tablename where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

create trigger ${model}_${tablename}_trig
    instead of insert or update or delete on $model.$tablename
       for each row execute procedure ${model}.${tablename}_fct()
;;