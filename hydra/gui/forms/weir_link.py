# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_link_dialog import SimpleLinkDialog, tr
import hydra.utility.string as string
from qgis.PyQt import uic, QtGui
import os

class WeirLinkEditor(SimpleLinkDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleLinkDialog.__init__(self, project, geom, "weir_link", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "weir_link.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        name, z_invert, width, cc, z_weir, v_max_cms, comment = self.project.execute("""
            select name, z_invert, width, cc, z_weir, v_max_cms, comment
            from {}.weir_link
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.name.setText(name)
        self.z_invert.setText(string.get_str(z_invert))
        self.width.setText(string.get_str(width))
        self.cc.setText(string.get_str(cc))
        self.z_weir.setText(string.get_str(z_weir))
        self.v_max_cms.setText(string.get_str(v_max_cms))
        self.notes.setText(comment)

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.model.update_link_qms(self.id, self.geom,
                                        string.get_sql_float(self.z_invert.text()),
                                        string.get_sql_float(self.width.text()), string.get_sql_float(self.cc.text()),
                                        string.get_sql_float(self.z_weir.text()), string.get_sql_float(self.v_max_cms.text()),
                                        self.name.text(), comment=self.notes.text())
        SimpleLinkDialog.save(self)
        self.close()









