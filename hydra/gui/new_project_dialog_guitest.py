# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


assert __name__ == "__main__"

import getopt
from qgis.PyQt.QtWidgets import QApplication
from qgis.PyQt.QtCore import QCoreApplication, QTranslator
from hydra.gui.new_project_dialog import NewProjectDialog

import sys

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "g",
            ["gui"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

try:
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    if "-g" in optlist or "--gui" in optlist:
        name=None
        new_project_dialog = NewProjectDialog()
        if new_project_dialog.exec_():
            name, srid, workspace = new_project_dialog.get_result()

        if name==None:
           sys.stderr.write("No project name returned")
        else:
           sys.stderr.write("Project name returned: "+str(name))
    else:
        new_project_dialog = NewProjectDialog()

except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

print("ok")