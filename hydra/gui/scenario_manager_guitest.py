# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.scenario_manager_guitest [-dhs] [-g project_name]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name
        run in gui mode

   -s  --solo
        run test in solo mode (create database)
"""

from __future__ import absolute_import # important to read the doc !
from __future__ import print_function
from builtins import str
from builtins import range
import os

from hydra.utility.string import normalized_name, normalized_model_name
import sys
import re
import getopt
import difflib
from hydra.project import Project
from qgis.PyQt.QtWidgets import QApplication
from qgis.PyQt.QtCore import QCoreApplication, QTranslator
from hydra.gui.scenario_manager import ScenarioManager
from hydra.database.database import TestProject, remove_project, project_exists
from hydra.utility.string import read_file, read_lines



def gui_mode(project_name):
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)
    obj_project = Project.load_project(project_name)
    test_dialog = ScenarioManager(obj_project)
    test_dialog.exec_()


def test():
    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)

    test_dialog = ScenarioManager(obj_project)
    test_dialog.new_scenario()
    test_dialog.table_scenario.table.setCurrentCell(test_dialog.table_scenario.table.rowCount() -1,0)
    #test of hydraulic files crd
    test_dialog.hydrology_settings.add_hyd_file("C:\\test.test")
    assert test_dialog.hydrology_settings.table_hyd_files.table.item(0,0).text()=="1"
    assert test_dialog.hydrology_settings.table_hyd_files.table.item(0,1).text()=="C:\\test.test"
    test_dialog.hydrology_settings.table_hyd_files.table.setCurrentCell(0,0)
    test_dialog.hydrology_settings.del_hyd_file()

    #test of regulation files crd
    test_dialog.regulation_and_configuration.add_reg_file("C:\\reg.test")
    assert test_dialog.regulation_and_configuration.table_reg_files.table.item(0,0).text()=="1"
    assert test_dialog.regulation_and_configuration.table_reg_files.table.item(0,1).text()=="C:\\reg.test"
    test_dialog.regulation_and_configuration.table_reg_files.table.setCurrentCell(0,0)
    test_dialog.regulation_and_configuration.del_reg_file()

    #test groups
    test_dialog.model_ordering.radio_mode_connexion_mixed.setChecked(True)
    test_dialog.model_ordering.add_group()
    test_dialog.model_ordering.listModels.setCurrentIndex(test_dialog.model_ordering.listModels.model().index(0,0))
    test_dialog.model_ordering.treeGroups.setCurrentIndex(test_dialog.model_ordering.treeGroups.model().index(0,0))
    test_dialog.model_ordering.add_model_to_group()
    test_dialog.model_ordering.treeGroups.setCurrentIndex(test_dialog.model_ordering.treeGroups.model().index(0,0))
    test_dialog.model_ordering.delete_group()
    test_dialog.model_ordering.add_group()
    test_dialog.model_ordering.listModels.setCurrentIndex(test_dialog.model_ordering.listModels.model().index(0,0))
    test_dialog.model_ordering.treeGroups.setCurrentIndex(test_dialog.model_ordering.treeGroups.model().index(0,0))
    test_dialog.model_ordering.add_model_to_group()
    test_dialog.model_ordering.treeGroups.setCurrentIndex(test_dialog.model_ordering.treeGroups.model().index(1,0))
    test_dialog.model_ordering.remove_model_from_group()

    #test transport types
    #pollution
    for i in range(0,len(test_dialog.transport.transport_types)):
        if test_dialog.transport.transport_types[i][0]=="pollution":
            test_dialog.transport.cmb_transport.setCurrentIndex(i)
            break
    test_dialog.transport.iflag_dbo5.setChecked(True)
    test_dialog.transport.iflag_mes.setChecked(True)
    test_dialog.transport.iflag_dco.setChecked(True)
    test_dialog.transport.iflag_ntk.setChecked(True)
    test_dialog.save()

    test_dialog = ScenarioManager(obj_project)
    test_dialog.table_scenario.table.setCurrentCell(test_dialog.table_scenario.table.rowCount() -1,0)
    assert test_dialog.transport.cmb_transport.currentText()=="Pollution: Pollution generation and network transport"
    assert test_dialog.transport.iflag_dbo5.isChecked()==True
    assert test_dialog.transport.iflag_mes.isChecked()==True
    assert test_dialog.transport.iflag_dco.isChecked()==True
    assert test_dialog.transport.iflag_ntk.isChecked()==True

    #test import scn via .csv file
    test_dialog.import_file(os.path.join(os.path.dirname(__file__), 'test_data', 'test_scenario.csv'))
    test_dialog.import_file(os.path.join(os.path.dirname(__file__), 'test_data', 'TP071014b.csv'))

    id = obj_project.execute("""select id from project.scenario where name = 'TP071014b' """).fetchone()[0]
    assert obj_project.execute("""select count(*) from project.param_external_hydrograph where param_scenario = {} """.format(id)).fetchone()[0] == 0
    assert obj_project.execute("""select count(*) from project.param_regulation where param_scenario = {}""".format(id)).fetchone()[0] == 0
    assert obj_project.execute("""select count(*) from project.param_measure where param_scenario = {} """.format(id)).fetchone()[0] == 0
    opsor_file =  obj_project.execute("""select option_file_path from project.scenario where id = {} """.format(id)).fetchone()[0]
    ref_file = os.path.join(os.path.dirname(__file__), 'test_data', 'parameter_TP071014b.dat')
    assert os.path.abspath(opsor_file) == os.path.abspath(os.path.join(os.path.expanduser('~'), ".hydra", "gui_test", "data", "parameter_TP071014b.dat"))

    if read_file(opsor_file).replace('\n','').replace('\nt','') != read_file(ref_file).replace('\n','').replace('\nt',''):
        ref = [line for line in read_lines(ref_file) if not re.match(r'^(=+ +)+=+$', line)]
        gen = [line for line in read_lines(opsor_file) if not re.match(r'^(=+ +)+=+$', line)]
        diff = difflib.unified_diff(ref, gen, fromfile=ref_file, tofile=opsor_file)
        nodiff = True
        for line in diff:
            nodiff &= False
        if not nodiff:
            sys.stderr.write("found diff {} {}\n".format(ref_file, opsor_file))
            sys.stderr.write('\n'.join([l for l in diff]))
            exit(1)

    #test scenario cloning
    scenario_count = obj_project.execute("""select count(*) from project.scenario""").fetchone()[0]

    id_source = obj_project.execute("""select id from project.scenario""").fetchall()
    for (id,) in id_source:
        test_dialog.select_scenario_by_id(id)
        test_dialog.clone_scenario()

    clones_count = obj_project.execute("""select count(*) from project.scenario""").fetchone()[0]

    assert clones_count == 2*scenario_count

    test_dialog.save()

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name, with_model=True)
    test()
    # fix_print_with_import
    print("ok")
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    project_name = normalized_name(project_name)
    gui_mode(project_name)
    exit(0)

test()
# fix_print_with_import

# fix_print_with_import
print("ok")
