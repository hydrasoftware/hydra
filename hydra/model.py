################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import string
import collections
from inspect import getargvalues, currentframe
from hydra.utility.result_decoder import HydraResult
import hydra.utility.sql_json as sql_json
from .result import Result

# @note inspect provide instrospection of function argument names and values
#       for magic to work, function arguments needto have the same name
#       as table fields

topo_error_dict = {1: "space found in name of project object",
                   2: "space found in name",
                   3: "links not connected",
                   4: "links not touching nodes",
                   5: "flood plain transects wrongly oriented",
                   6: "end points of transects not merged to other constrains",
                   7: "river nodes without reach affected",
                   8: "parametric or valley geometry where Z is not increasing",
                   9: "parametric or valley geometry where width < 0",
                   10: "open parametric or valley geometry where the width is not increasing",}

def _quote(value):
    return "'"+value+"'" \
            if isinstance(value, str) \
            and ( value == "" or value[0] != "'") and value != "null" else value

def _encapsulate(value):
    if value is None:
        return 'null'
    elif isinstance(value, str):
        return "'{}'".format(value)
    elif isinstance(value, list):
        return "'{}'::real[]".format(str(value).replace(']', '}'
                                              ).replace('[', '{'
                                              ).replace('(', '{'
                                              ).replace(')', '}'
                                              ).replace('None', 'null'))
    else:
        return str(value)

class Model(object):
    def __init__(self, proj, srid, model_name):
        self.__proj = proj
        self.__srid = srid
        self.name = model_name

    def __getattr__(self, att):
        if att=='srid':
             return self.__srid
        elif att=='results':
           return {s: Result(self.__proj, self.name, s) for s in self.__proj.scenarios if Result(self.__proj, self.name, s).exists}
        else:
            raise AttributeError(att)

    def set_branch_trigger(self, state=True):
        trigger_state = self.execute("""select trigger_branch from $model.metadata""").fetchone()
        self.execute("""update $model.metadata set trigger_branch = '{}'""".format(state))
        return trigger_state

    def topo_check(self):
        '''Checks everything is well connected
        returns a dict with keys as error code and values as list of corresponding items:
        1: Space in a project item's name
        2: Space in item's name
        3: Link not connected
        4: Link not touching nodes
        5: Flood plain transect wrongly
        6: Flood plain transect with end points not merged to other constrains
        7: River nodes all located on reaches
        8: Parametric & valley geometries Z increasing
        9: Parametric & valley geometries B > 0
        9: Open parametric & valley geometries B increasing
        '''
        error_dict = {}
        # spaces in names of project items
        spaces_in_project = self.execute("""
            select id, name, 'Scenario' from project.scenario where name ilike '% %' or name ilike '% ' or name ilike ' %'
                union
            select id, name, 'Rainfall' from project._rainfall where name ilike '% %' or name ilike '% ' or name ilike ' %'
                union
            select id, name, 'Dry inflow scenario' from project.dry_inflow_scenario where name ilike '% %' or name ilike '% ' or name ilike ' %'
                union
            select id, name, 'Dry inflow sector' from project.dry_inflow_sector where name ilike '% %' or name ilike '% ' or name ilike ' %'
                union
            select id, name, 'Dry inflow modulation curve' from project.dry_inflow_hourly_modulation where name ilike '% %' or name ilike '% ' or name ilike ' %'
            """).fetchall()
        if spaces_in_project:
            self.__proj.log.warning("Topo check - {}: {}".format(topo_error_dict[1], ', '.join(["{} ({})".format(n, t) for (i, n, t) in spaces_in_project])))
            error_dict[1] = spaces_in_project
        # spaces in names of model objects
        spaces_in_model = self.execute("""
            select id, name, 'Node '||node_type from $model._node
            where name ilike '% %' or name ilike '% ' or name ilike ' %'
            or name ilike E'%\n%' or name ilike E'%\n' or name ilike E'\n%'
                union
            select id, name, 'Link '||link_type from $model._link
            where name ilike '% %' or name ilike '% ' or name ilike ' %'
            or name ilike E'%\n%' or name ilike E'%\n' or name ilike E'\n%'
                union
            select id, name, 'Singularity '||singularity_type from $model._singularity
            where name ilike '% %' or name ilike '% ' or name ilike ' %'
            or name ilike E'%\n%' or name ilike E'%\n' or name ilike E'\n%'
                union
            select id, name, 'Reach' from $model.reach
            where name ilike '% %' or name ilike '% ' or name ilike ' %'
            or name ilike E'%\n%' or name ilike E'%\n' or name ilike E'\n%'
            """).fetchall()
        if spaces_in_model:
            self.__proj.log.warning("Topo check - {}: {}".format(topo_error_dict[2], ', '.join(["{} ({})".format(n, t) for (i, n, t) in spaces_in_model])))
            error_dict[2] = spaces_in_model
        # isolated nodes
        # links not touching nodes
        null_links = self.execute("""select id, name, 'Link '||link_type from $model._link where up is null or down is null""").fetchall()
        if null_links:
            self.__proj.log.warning("Topo check - {}: {}".format(topo_error_dict[3], ', '.join(["{} ({})".format(n, t) for (i, n, t) in null_links])))
            error_dict[3] = null_links
        not_touching_links = self.execute("""
            select id, name, 'Link '||link_type from $model._link
            where (up_type not in ('elem_2d', 'storage')   and not ST_DWithin(ST_StartPoint(geom), (select geom from $model._node where id=up), 0.01))
            or    (down_type not in ('elem_2d', 'storage') and not ST_DWithin(ST_EndPoint(geom), (select geom from $model._node where id=down), 0.01))
            or    (up_type='elem_2d'                       and not ST_Contains((select contour from $model.elem_2d_node where id=up), ST_StartPoint(geom)))
            or    (down_type='elem_2d'                     and not ST_Contains((select contour from $model.elem_2d_node where id=down), ST_EndPoint(geom)))
            or    (up_type='storage'                       and not ST_Contains((select geom from $model.coverage where ST_Contains(geom, (select geom from $model.storage_node where id=up))), ST_StartPoint(geom)))
            or    (down_type='storage'                     and not ST_Contains((select geom from $model.coverage where ST_Contains(geom, (select geom from $model.storage_node where id=down))), ST_EndPoint(geom)))
            order by name
            """).fetchall()
        if not_touching_links:
            self.__proj.log.warning("Topo check - {}: {}".format(topo_error_dict[4], ', '.join(["{} ({})".format(n, t) for (i, n, t) in not_touching_links])))
            error_dict[4] = not_touching_links
        # flood_plain transect orientation
        transects_orientation = self.execute("""
            with data as (
                select t.id as id, t.name as transect, r.geom as river_point, (ST_Dump(ST_Intersection(t.geom, r.geom))).geom as intersections, ST_EndPoint(t.geom) as endpoint
                from  $model.constrain as t
                inner join $model.reach as r on ST_Intersects(r.geom, t.geom)
                where t.constrain_type='flood_plain_transect'
                order by t.name
            ),
            vectors as (
                select id, transect,
                ST_MakeLine(intersections, endpoint) as t_vec,
                ST_MakeLine(ST_LineInterpolatePoint(river_point, ST_LineLocatePoint(river_point, intersections) * 0.999), intersections) as r_vec
                from  data
            )
            select id, transect, 'Flood plain transect' from vectors
            where MOD(degrees(ST_Azimuth(ST_StartPoint(t_vec), ST_EndPoint(t_vec)) - ST_Azimuth(ST_StartPoint(r_vec), ST_EndPoint(r_vec)))::integer + 360,360) > 180
            """).fetchall()
        if transects_orientation:
            self.__proj.log.warning("Topo check - {}: {}".format(topo_error_dict[5], ', '.join(["{} ({})".format(n, t) for (i, n, t) in transects_orientation])))
            error_dict[5] = transects_orientation
        # flood_plain transect end points on constrain lines
        transects_end_points = self.execute("""
            with reach as (
                select id, geom from $model.coverage where domain_type='reach'
            ), vertex as (
                select id, (ST_dumppoints(geom)).geom as geom from reach
            ), ends as (
                select id, name, st_startpoint(geom) as geom from $model.constrain as c
                where constrain_type='flood_plain_transect'
                and exists (select id from reach where ST_Contains(reach.geom, c.geom))
                union
                select id, name, st_endpoint(geom) as geom from $model.constrain as c
                where constrain_type='flood_plain_transect'
                and exists (select id from reach where ST_Contains(reach.geom, c.geom))
            )
            select distinct e.id, e.name, 'Flood plain transect' from ends as e
            where not exists (select id from vertex as v where ST_equals(v.geom, e.geom))
            """).fetchall()
        if transects_end_points:
            self.__proj.log.warning("Topo check - {}: {}".format(topo_error_dict[6], ', '.join(["{} ({})".format(n, t) for (i, n, t) in transects_end_points])))
            error_dict[6] = transects_end_points
        # river nodes not on reaches
        river_nodes = self.execute("""
            select distinct n.id, n.name, 'River node' from $model.river_node as n
            where reach is null
            """).fetchall()
        if river_nodes:
            self.__proj.log.warning("Topo check - {}: {}".format(topo_error_dict[7], ', '.join(["{} ({})".format(n, t) for (i, n, t) in river_nodes])))
            error_dict[7] = river_nodes
        # geom Z increasing
        geometries_z = []
        for table, geometry_column, name in [
            ['closed_parametric_geometry',    'zbmin_array',       'Closed parametric geometry'                 ],
            ['open_parametric_geometry',      'zbmin_array',       'Open parametric geometry'                   ],
            ['valley_cross_section_geometry', 'zbmin_array',       'Valley cross section geometry (river bed)'  ],
            ['valley_cross_section_geometry', 'zbmaj_lbank_array', 'Valley cross section geometry (left bank)'  ],
            ['valley_cross_section_geometry', 'zbmaj_rbank_array', 'Valley cross section geometry (right bank)' ]
            ]:
            geometries_z += self.execute(f"""
            with arr as (
                select id, name, ARRAY(select unnest({geometry_column}[:][1:1])) as z_array from $model.{table}
                ),
            sort as (
                select id, name, z_array, ARRAY(select unnest(z_array) order by 1) as sorted_z_array from arr
                )
            select id, name, '{name}' from sort where z_array != sorted_z_array
            """).fetchall()
        if geometries_z:
            self.__proj.log.warning("Topo check - {}: {}".format(topo_error_dict[8], ', '.join(["{} ({})".format(n, t) for (i, n, t) in geometries_z])))
            error_dict[8] = geometries_z
        # geom B > 0
        geometries_b0 = []
        for table, geometry_column, name in [
            ['closed_parametric_geometry',    'zbmin_array',       'Closed parametric geometry'                 ],
            ['open_parametric_geometry',      'zbmin_array',       'Open parametric geometry'                   ],
            ['valley_cross_section_geometry', 'zbmin_array',       'Valley cross section geometry (river bed)'  ],
            ['valley_cross_section_geometry', 'zbmaj_lbank_array', 'Valley cross section geometry (left bank)'  ],
            ['valley_cross_section_geometry', 'zbmaj_rbank_array', 'Valley cross section geometry (right bank)' ]
            ]:
            geometries_b0 += self.execute(f"""
            with values as (
                select id, name, unnest({geometry_column}[:][2:2]) as b from $model.{table}
                )
            select distinct id, name, '{name}' from values where b < 0
            """).fetchall()
        if geometries_b0:
            self.__proj.log.warning("Topo check - {}: {}".format(topo_error_dict[9], ', '.join(["{} ({})".format(n, t) for (i, n, t) in geometries_b0])))
            error_dict[9] = geometries_b0
        # geom B increasing
        geometries_b_increasing = []
        for table, geometry_column, name in [
            #['open_parametric_geometry',      'zbmin_array',       'Open parametric geometry'                   ], # finalement on vérifie pas ça
            ['valley_cross_section_geometry', 'zbmin_array',       'Valley cross section geometry (river bed)'  ],
            ['valley_cross_section_geometry', 'zbmaj_lbank_array', 'Valley cross section geometry (left bank)'  ],
            ['valley_cross_section_geometry', 'zbmaj_rbank_array', 'Valley cross section geometry (right bank)' ]
            ]:
            geometries_b_increasing += self.execute(f"""
            with arr as (
                select id, name, ARRAY(select unnest({geometry_column}[:][2:2])) as b_array from $model.{table}
                ),
            sort as (
                select id, name, b_array, ARRAY(select unnest(b_array) order by 1) as sorted_b_array from arr
                )
            select id, name, '{name}' from sort where b_array != sorted_b_array
            """).fetchall()
        if geometries_b_increasing:
            self.__proj.log.warning("Topo check - {}: {}".format(topo_error_dict[10], ', '.join(["{} ({})".format(n, t) for (i, n, t) in geometries_b_increasing])))
            error_dict[10] = geometries_b_increasing
        return error_dict

    def make_point(self, point):
        if isinstance(point, str):
            return point
        return "'srid={}; POINTZ({} {} {})'::geometry".format(
                str(self.__srid),
                str(point[0]),
                str(point[1]),
                str(point[2]) if len(point) == 3 else 9999
                ) if point else None

    def make_line(self, geom):
        if isinstance(geom, str):
            if geom[:10]=="LINESTRING" and geom[-10:]!="::geometry":
                return "'srid={}; {}'::geometry".format(str(self.__srid), geom)
            else:
                return geom
        return "'srid={}; LINESTRINGZ({})'::geometry".format(
                str(self.__srid),
                ",".join([
                    str(p[0])+" "+str(p[1])+" "+(str(p[2]) if len(p) == 3 else "9999")
                    for p in geom])) if geom else None

    def make_polygon(self, geom, has_z=True):
        if isinstance(geom, str):
            return geom
        return "'srid={}; POLYGON{}(({}))'::geometry".format(
                str(self.__srid),
                "Z" if has_z else "",
                ",".join([str(p[0])+" "+str(p[1])+
                    ((" "+(str(p[2]) if len(p) == 3 else "9999")) if has_z else "")
                    for p in geom[0]])) if geom else None

    def make_real_array(self, array):
        return "'{}'::real[]".format(
                str(array).replace(']', '}'
                         ).replace('[', '{'
                         ).replace('(', '{'
                         ).replace(')', '}'
                         ).replace('None', 'null')) if array else None

    def execute(self, sql):
        """execute statement, the variable $model and $srid in the sql are
        replaced with the appropriate value"""
        return self.__proj.execute(string.Template(sql).substitute(
            {"model": self.name, "srid": self.srid }))

    def fetchone(self, sql):
        """execute statement, the variable $model and $srid in the sql are
        replaced with the appropriate value"""
        return self.__proj.fetchone(string.Template(sql).substitute(
            {"model": self.name, "srid": self.srid }))


    def fetchall(self, sql):
        """execute statement, the variable $model and $srid in the sql are
        replaced with the appropriate value"""
        return self.__proj.fetchall(string.Template(sql).substitute(
            {"model": self.name, "srid": self.srid }))

    def get_attrib_from_table_byname(self, table, name):
        return self.execute("""select * from $model.{} where UPPER(name)=UPPER({})""".format(table, _quote(str(name)))).fetchall()

    def get_Distance_point_to_line_table(self, line_table, id_line, point):
        pstring = 'Point('+ point[0] + ' ' + point[1] + ')'
        lstring= self.execute("""select st_astext(geom) from $model.{} where  id={}""".format(line_table,id_line)).fetchall()[0][0]
        res = self.execute("""select ST_Distance(St_GeomFromText('{}', $srid), St_GeomFromText('{}', $srid))""".format(pstring, lstring))
        distance = res.fetchall()[0][0]
        return distance

    def __magic_insert(self, table, arg_map):
        argmap = {key: value for key, value in arg_map.items()
            if value is not None and key not in ['table','self']}
        if len(argmap):
            sql = ("""insert into $model.{}({}) values({}) returning id""".format(table,
                    ",".join(list(argmap.keys())),
                    ",".join(_quote(str(value)) for value in list(argmap.values()))))
            new_id, = self.execute(sql).fetchone()
        else:
            new_id, = self.execute("""insert into $model.{} default values returning id""".format(table)).fetchone()
        return new_id

    def __magic_update(self, table, id, arg_map):
        values_to_update=[]
        for key, value in arg_map.items():
            if value is not None and key not in ['table','self', 'id']:
                           values_to_update.append("{}={}".format(key, _quote(str(value))))
        if len(values_to_update)>0:
            self.execute("""update $model.{} set {} where id={}""".format(table, ", ".join(values_to_update), id))

    def get_current_configuration(self):
        return self.fetchone("""select configuration from $model.metadata;""")[0]

    def get_configurations(self):
        return self.fetchall("""select id, name from $model.configuration;""")

    def set_current_configuration(self, config_id):
        self.execute("""update $model.metadata set configuration={}""".format(config_id))
        self.__proj.commit()

    def get_number_configured(self):
        return self.fetchone("""select count(1) from $model.configured_current;""")[0]

    def drop_config_on_object(self, config_id, obj_table, obj_id):
        type=obj_table.split('_')[-1] if obj_table!='river_cross_section_profile' else obj_table
        obj_json = self.execute("""select configuration from $model.{table} where id={id};""".format(table='_'+type, id=obj_id)).fetchone()[0]
        config = self.fetchone("""select name from $model.configuration where id={};""".format(config_id))[0]

        if config in list(obj_json.keys()):
            obj_json.pop(config)
            if len(list(obj_json.keys()))==1:
                self.execute("""update $model.{table} set configuration=null where id={id};""".format(table='_'+type, id=obj_id))
            else:
                self.execute("""update $model.{table} set configuration='{cfg_json}' where id={id};""".format(table='_'+type, id=obj_id, cfg_json=sql_json.pack(obj_json, keep_text=True)))


            update_fields = []
            for k, v in obj_json['default'].items():
                if v is not None:
                    update_fields.append("""{} = {}""".format(k, _encapsulate(v)))

            self.execute("""update $model.{table} set {updt} where id={id};""".format(table='_'+obj_table, updt = ', '.join(update_fields), id=obj_id))

    def add_constrain(self, geom, elem_length=None, name=None,
            snap_distance=None, keep_left=None, keep_right=None,
            link_type=None, link_attributes=None, comment=None):
        geom = self.make_line(geom)
        return self.__magic_insert('constrain', getargvalues(currentframe()).locals)

    def update_constrain(self, id, geom=None, elem_length=None, name=None,
            snap_distance=None, keep_left=None, keep_right=None,
            link_type=None, link_attributes=None, comment=None):
        geom = self.make_line(geom)
        self.__magic_update('constrain', id, getargvalues(currentframe()).locals)

    def add_domain_2d(self, name=None ):
        #geom = self.make_polygon(geom)
        return self.__magic_insert('domain_2d', getargvalues(currentframe()).locals)

    def update_domain_2d(self, id, name=None ):
        #geom = self.make_polygon(geom)
        self.__magic_update('domain_2d', id, getargvalues(currentframe()).locals)

    def add_simple_singularity(self, table, geom, name=None, comment=None):
        """ used to insert default singularities into any tables """
        geom = self.make_point(geom)
        return self.__magic_insert(table, getargvalues(currentframe()).locals)

    def update_simple_singularity(self, table, id, geom, name=None, comment=None):
        """ used to update default singularities into any tables """
        geom = self.make_point(geom)
        self.__magic_update(table, id, getargvalues(currentframe()).locals)

    def add_marker_singularity(self, geom, name=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('marker_singularity', getargvalues(currentframe()).locals)

    def update_marker_singularity(self, id, geom, name=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('marker_singularity', id, getargvalues(currentframe()).locals)

    def add_coverage_marker(self, geom, name=None, comment=None, protected=None):
        geom = self.make_point(geom)
        return self.__magic_insert('coverage_marker', getargvalues(currentframe()).locals)

    def update_coverage_marker(self, id, geom, name=None, comment=None, protected=None):
        geom = self.make_point(geom)
        self.__magic_update('coverage_marker', id, getargvalues(currentframe()).locals)

    def add_2d_domain_marker(self, geom, name=None, domain_2d=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('domain_2d_marker', getargvalues(currentframe()).locals)

    def update_2d_domain_marker(self, id, geom, name=None, domain_2d=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('domain_2d_marker', id, getargvalues(currentframe()).locals)

    def add_ci_singularity(self, geom, q0=0, name=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('constant_inflow_bc_singularity', getargvalues(currentframe()).locals)

    def update_ci_singularity(self, id, geom, q0, name=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('constant_inflow_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_hy_singularity(self, geom=None, name=None, storage_area=0, tq_array=None, constant_dry_flow=None,
            distrib_coef=None, lag_time_hr=None, sector=None, hourly_modulation=None,
            pollution_quality_mode=None, pollution_quality_param=None, external_file_data=None,
            pollution_dryweather_runoff=None, quality_dryweather_runoff=None, comment=None):
        geom = self.make_point(geom)
        tq_array = self.make_real_array(tq_array)
        return self.__magic_insert('hydrograph_bc_singularity', getargvalues(currentframe()).locals)

    def update_hy_singularity(self, id, geom=None, name=None, storage_area=None, tq_array=None, constant_dry_flow=None,
                                distrib_coef=None, lag_time_hr=None, sector=None, hourly_modulation=None,
                                pollution_quality_mode=None, pollution_quality_param=None, external_file_data=None,
            pollution_dryweather_runoff=None, quality_dryweather_runoff=None, comment=None):
        geom = self.make_point(geom)
        tq_array = self.make_real_array(tq_array)
        self.__magic_update('hydrograph_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_manhole(self, geom, name=None, area=None, z_ground=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('manhole_node', getargvalues(currentframe()).locals)

    def update_manhole(self, id, geom=None, name=None, area=None, z_ground=None, cover_diameter=None, cover_critical_pressure=None, inlet_width=None, inlet_height=None, connection_law=None, comment=None):
        #delete_row_by_name_from_table('manhole_node', name)
        geom = self.make_point(geom)
        self.__magic_update('manhole_node', id, getargvalues(currentframe()).locals)

    def add_pipe_link(self, geom, z_invert_up=None, z_invert_down=None, cross_section_type=None, h_sable=None, exclude_from_branch=None,
                                    rk=None, circular_diameter=None, ovoid_height=None, ovoid_top_diameter=None, ovoid_invert_diameter=None,
                                    cp_geom=None, op_geom=None, name=None, comment=None, rk_maj=None):
        geom = self.make_line(geom)
        return self.__magic_insert('pipe_link', getargvalues(currentframe()).locals)

    def update_pipe_link(self, id, geom, z_invert_up=None, z_invert_down=None, cross_section_type = None, h_sable=None, exclude_from_branch=None,
                                    rk=None, circular_diameter=None, ovoid_height=None, ovoid_top_diameter=None, ovoid_invert_diameter=None,
                                    cp_geom=None, op_geom=None, name=None, comment=None, rk_maj=None):
        geom = self.make_line(geom)
        self.__magic_update('pipe_link', id, getargvalues(currentframe()).locals)

    def add_link_qmv(self, geom, z_invert=None, z_ceiling=None, width=None, cc=None, cc_submerged=None, mode_valve=None, action_gate_type=None, z_gate=None, v_max_cms=None, name=None, comment=None):
        geom = self.make_line(geom)
        return self.__magic_insert('gate_link', getargvalues(currentframe()).locals)

    def update_link_qmv(self, id, geom, z_invert=None, z_ceiling=None, width=None, cc=None, cc_submerged=None, mode_valve=None, action_gate_type=None, z_gate=None, v_max_cms=None, name=None, comment=None):
        geom = self.make_line(geom)
        self.__magic_update('gate_link', id, getargvalues(currentframe()).locals)

    def add_link_qms(self, geom, z_invert, width, cc, z_weir, v_max_cms, name=None, comment=None):
        geom = self.make_line(geom)
        return self.__magic_insert('weir_link', getargvalues(currentframe()).locals)

    def update_link_qms(self, id, geom, z_invert, width, cc, z_weir, v_max_cms, name=None, comment=None):
        geom = self.make_line(geom)
        self.__magic_update('weir_link', id, getargvalues(currentframe()).locals)

    def add_simple_link(self, table, geom, name=None, comment=None):
        """ used to insert default links into any tables """
        geom = self.make_line(geom)
        return self.__magic_insert(table, getargvalues(currentframe()).locals)

    def update_simple_link(self, table, id, geom, name=None, comment=None):
        """ used to update default links into any tables """
        geom = self.make_line(geom)
        self.__magic_update(table, id, getargvalues(currentframe()).locals)

    def add_street(self, geom, width=None, width_invert=None, rk=None, elem_length=None, name=None, comment=None):
        geom = self.make_line(geom)
        return self.__magic_insert('street', getargvalues(currentframe()).locals)

    def update_street(self, id, geom, width=None, width_invert=None, rk=None, elem_length=None, name=None, comment=None):
        geom = self.make_line(geom)
        self.__magic_update('street', id, getargvalues(currentframe()).locals)

    def add_street_link(self, geom, width=None, width_invert=None, rk=None, elem_length=None, name=None, comment=None):
        geom = self.make_line(geom)
        return self.__magic_insert('street', getargvalues(currentframe()).locals)

    def update_street_link(self, id, geom, width=None, width_invert=None, rk=None, elem_length=None, name=None, comment=None):
        geom = self.make_line(geom)
        self.__magic_update('street', id, getargvalues(currentframe()).locals)

    def add_reachpath(self, geom, dx=None, pk0_km=None, name=None, comment=None):
        geom = self.make_line(geom)
        return self.__magic_insert('reach', getargvalues(currentframe()).locals)

    def update_reachpath(self, id, geom, dx=None, pk0_km=None, name=None, comment=None):
        geom = self.make_line(geom)
        self.__magic_update('reach', id, getargvalues(currentframe()).locals)

    def try_merge_reach(self, geom):
        res=False
        # reaches endding at the start of the new line
        up_reaches = self.execute("""select id, name, st_astext(geom)
            from $model.reach
            where ST_Equals(st_endpoint(geom), st_startpoint({}));""".format(self.make_line(geom))).fetchall()
        down_reaches = self.execute("""select id, name, st_astext(geom)
            from $model.reach
            where ST_Equals(st_startpoint(geom), st_endpoint({}));""".format(self.make_line(geom))).fetchall()
        if len(up_reaches)>0 and len(down_reaches)==0:
            self.execute("""update $model.reach
                set geom=st_makeline(geom,{})
                where id = {};""".format(self.make_line(geom), up_reaches[0][0]))
            self.execute("""
                insert into $model.river_node(geom, z_ground)
                select st_endpoint({l}), {z}
                where not exists(select 1 from $model.river_node where ST_Intersects(ST_Buffer(geom, 0.1), {l}));
                """.format(l=self.make_line(geom), z=str(geom[-1][2] if len(geom[-1])>2 else 9999)))
            res = True
        # reaches starting at the end of the new line
        elif len(up_reaches)==0 and len(down_reaches)>0:
            self.execute("""update $model.reach
                set geom=st_makeline({}, geom)
                where id = {};""".format(self.make_line(geom), down_reaches[0][0]))
            self.execute("""
                insert into $model.river_node(geom, z_ground)
                select st_startpoint({l}), {z}
                where not exists(select 1 from $model.river_node where ST_Intersects(ST_Buffer(geom, 0.1), {l}));
                """.format(l=self.make_line(geom), z=str(geom[0][2] if len(geom[0])>2 else 9999)))
            res = True
        # reaches at both ends
        elif  len(up_reaches)>0 and len(down_reaches)>0:
            id=self.execute("""insert into $model.reach(geom) values (ST_MakeLine(ST_MakeLine({}, {}), {})) returning id;""".format(self.make_line(up_reaches[0][2]), self.make_line(geom), self.make_line(down_reaches[0][2]))).fetchone()[0]
            self.execute("""update $model.river_node set reach={i} where reach in ({u}, {d});""".format(i=id, u=up_reaches[0][0], d=down_reaches[0][0]))
            self.execute("""delete from $model.reach where id in ({}, {});""".format(up_reaches[0][0], down_reaches[0][0]))
            res = True
        return res

    def add_river_cross_section_profile(self, geom, z_invert_up=None, z_invert_down=None,
                                        type_cross_section_up=None,type_cross_section_down=None,
                                        up_rk=None, up_rk_maj=None, up_sinuosity=None, up_circular_diameter=None,
                                        up_ovoid_height=None, up_ovoid_top_diameter=None, up_ovoid_invert_diameter=None,
                                        up_cp_geom=None, up_op_geom=None, up_vcs_geom=None, up_vcs_topo_geom=None,
                                        down_rk=None, down_rk_maj=None, down_sinuosity=None, down_circular_diameter=None,
                                        down_ovoid_height=None, down_ovoid_top_diameter=None, down_ovoid_invert_diameter=None,
                                        down_cp_geom=None, down_op_geom=None, down_vcs_geom=None, down_vcs_topo_geom=None, name=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('river_cross_section_profile', getargvalues(currentframe()).locals)

    def update_river_cross_section_profile(self, id, geom, z_invert_up=None, z_invert_down=None,
                                        type_cross_section_up=None,type_cross_section_down=None,
                                        up_rk=None, up_rk_maj=None, up_sinuosity=None, up_circular_diameter=None,
                                        up_ovoid_height=None, up_ovoid_top_diameter=None, up_ovoid_invert_diameter=None,
                                        up_cp_geom=None, up_op_geom=None, up_vcs_geom=None, up_vcs_topo_geom=None,
                                        down_rk=None, down_rk_maj=None, down_sinuosity=None, down_circular_diameter=None,
                                        down_ovoid_height=None, down_ovoid_top_diameter=None, down_ovoid_invert_diameter=None,
                                        down_cp_geom=None, down_op_geom=None, down_vcs_geom=None, down_vcs_topo_geom=None, name=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('river_cross_section_profile', id, getargvalues(currentframe()).locals)

    def add_pipe_channel_closed_section_geometry(self, zbmin_array, name=None):
        zbmin_array = self.make_real_array(zbmin_array)
        return self.__magic_insert('closed_parametric_geometry', getargvalues(currentframe()).locals)

    def update_pipe_channel_closed_section_geometry(self, id, zbmin_array, name=None):
        zbmin_array = self.make_real_array(zbmin_array)
        self.__magic_update('closed_parametric_geometry', id, getargvalues(currentframe()).locals)

    def add_pipe_channel_open_section_geometry(self, zbmin_array, name=None, with_flood_plain=False, flood_plain_width=0, flood_plain_lateral_slope=0):
        zbmin_array = self.make_real_array(zbmin_array)
        return self.__magic_insert('open_parametric_geometry', getargvalues(currentframe()).locals)

    def update_pipe_channel_open_section_geometry(self, id, zbmin_array, name=None, with_flood_plain=False, flood_plain_width=0, flood_plain_lateral_slope=0):
        zbmin_array = self.make_real_array(zbmin_array)
        self.__magic_update('open_parametric_geometry', id, getargvalues(currentframe()).locals)

    def add_valley_section_geometry(self, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array,
            rlambda, rmu1,rmu2, zlevee_lb, zlevee_rb, name=None, transect=None, t_discret=150, t_ignore_pt=False, t_distance_pt=100):
        zbmin_array = self.make_real_array(zbmin_array)
        zbmaj_lbank_array = self.make_real_array(zbmaj_lbank_array)
        zbmaj_rbank_array = self.make_real_array(zbmaj_rbank_array)
        return self.__magic_insert('valley_cross_section_geometry', getargvalues(currentframe()).locals)

    def update_valley_section_geometry(self, id, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array,
            rlambda, rmu1,rmu2, zlevee_lb, zlevee_rb, name=None, transect=None, t_discret=150, t_ignore_pt=False, t_distance_pt=100):
        zbmin_array = self.make_real_array(zbmin_array)
        zbmaj_lbank_array = self.make_real_array(zbmaj_lbank_array)
        zbmaj_rbank_array = self.make_real_array(zbmaj_rbank_array)
        self.__magic_update('valley_cross_section_geometry', id, getargvalues(currentframe()).locals)

    def add_valley_section_topo_geometry(self, xz_array, name=None):
        xz_array = self.make_real_array(xz_array)
        return self.__magic_insert('valley_cross_section_topo_geometry', getargvalues(currentframe()).locals)

    def update_valley_section_topo_geometry(self, id, xz_array, name=None):
        xz_array = self.make_real_array(xz_array)
        self.__magic_update('valley_cross_section_topo_geometry', id, getargvalues(currentframe()).locals)

    def add_river_node(self, geom, z_ground=None, area=None, name=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('river_node', getargvalues(currentframe()).locals)

    def update_river_node(self, id, geom, z_ground=None, area=None, name=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('river_node', id, getargvalues(currentframe()).locals)

    def add_singularity_va(self, geom, z_invert=None, z_ceiling=None, width=None, cc=None, cc_submerged=None, mode_valve=None, action_gate_type=None, z_gate=None, v_max_cms=None, full_section_discharge_for_headloss=True, name=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('gate_singularity', getargvalues(currentframe()).locals)

    def update_singularity_va(self, id, geom, z_invert=None, z_ceiling=None, width=None, cc=None, cc_submerged=None, mode_valve=None, action_gate_type=None, z_gate=None, v_max_cms=None, full_section_discharge_for_headloss=True, name=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('gate_singularity', id, getargvalues(currentframe()).locals)

    def add_hydraulic_cut_singularity(self, geom, qz_array, full_section_discharge_for_headloss=True, name=None, comment=None):
        geom = self.make_point(geom)
        qz_array = self.make_real_array(qz_array)
        return self.__magic_insert('hydraulic_cut_singularity', getargvalues(currentframe()).locals)

    def update_hydraulic_cut_singularity(self, id, geom, qz_array, full_section_discharge_for_headloss=True, name=None, comment=None):
        geom = self.make_point(geom)
        qz_array = self.make_real_array(qz_array)
        self.__magic_update('hydraulic_cut_singularity', id, getargvalues(currentframe()).locals)

    def add_param_headloss_singularity(self, geom, q_dz_array, full_section_discharge_for_headloss=True, name=None, comment=None):
        geom = self.make_point(geom)
        q_dz_array = self.make_real_array(q_dz_array)
        return self.__magic_insert('param_headloss_singularity', getargvalues(currentframe()).locals)

    def update_param_headloss_singularity(self, id, geom, q_dz_array, full_section_discharge_for_headloss=True, name=None, comment=None):
        geom = self.make_point(geom)
        q_dz_array = self.make_real_array(q_dz_array)
        self.__magic_update('param_headloss_singularity', id, getargvalues(currentframe()).locals)

    def add_zq_bc_singularity(self, geom, zq_array, name=None, comment=None):
        geom = self.make_point(geom)
        zq_array = self.make_real_array(zq_array)
        return self.__magic_insert('zq_bc_singularity', getargvalues(currentframe()).locals)

    def update_zq_bc_singularity(self, id, geom, zq_array, name=None, comment=None):
        geom = self.make_point(geom)
        zq_array = self.make_real_array(zq_array)
        self.__magic_update('zq_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_tz_bc_singularity(self, geom, tz_array, cyclic=None, external_file_data=None, name=None, comment=None):
        geom = self.make_point(geom)
        tz_array = self.make_real_array(tz_array)
        return self.__magic_insert('tz_bc_singularity', getargvalues(currentframe()).locals)

    def update_tz_bc_singularity(self, id, geom, tz_array, cyclic=None, external_file_data=None, name=None, comment=None):
        geom = self.make_point(geom)
        tz_array = self.make_real_array(tz_array)
        self.__magic_update('tz_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_tank_bc_singularity(self, geom, zs_array, zini,
            treatment_mode=None, treatment_param=None,  name=None, comment=None):
        geom = self.make_point(geom)
        zs_array = self.make_real_array(zs_array)
        return self.__magic_insert('tank_bc_singularity', getargvalues(currentframe()).locals)

    def update_tank_bc_singularity(self, id, geom, zs_array, zini,
            treatment_mode=None, treatment_param=None,  name=None, comment=None):
        geom = self.make_point(geom)
        zs_array = self.make_real_array(zs_array)
        self.__magic_update('tank_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_smk(self, geom, law_type=None, param=None, full_section_discharge_for_headloss=True, name=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('borda_headloss_singularity', getargvalues(currentframe()).locals)

    def update_singularity_smk(self, id, geom, law_type=None, param=None, full_section_discharge_for_headloss=True, name=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('borda_headloss_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_de(self, geom, z_invert, z_regul, width, cc,
                            mode_regul=None, reoxy_law=None, reoxy_param=None, full_section_discharge_for_headloss=True, name=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('zregul_weir_singularity', getargvalues(currentframe()).locals)

    def update_singularity_de(self, id, geom,  z_invert, z_regul, width, cc,
                            mode_regul=None, reoxy_law=None, reoxy_param=None, full_section_discharge_for_headloss=True, name=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('zregul_weir_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_acta(self, geom, z_invert=None, z_ceiling=None, width=None, cc=None, cc_submerged=None,
                            action_gate_type=None, z_invert_stop=None, z_ceiling_stop=None,
                            v_max_cms=None, dt_regul_hr=None, mode_regul=None,
                            z_control_node=None, z_pid_array=None, z_tz_array=None,
                            q_z_crit=None, q_tq_array=None, nr_z_gate=None, full_section_discharge_for_headloss=True, name=None, hq_array=None, h_open=None, h_close=None, comment=None):
        geom = self.make_point(geom)
        z_pid_array = self.make_real_array(z_pid_array)
        z_tz_array = self.make_real_array(z_tz_array)
        q_tq_array = self.make_real_array(q_tq_array)
        hq_array = self.make_real_array(hq_array)
        return self.__magic_insert('regul_sluice_gate_singularity', getargvalues(currentframe()).locals)

    def update_singularity_acta(self, id, geom, z_invert=None, z_ceiling=None, width=None, cc=None, cc_submerged=None,
                            action_gate_type=None, z_invert_stop=None, z_ceiling_stop=None,
                            v_max_cms=None, dt_regul_hr=None, mode_regul=None,
                            z_control_node=None, z_pid_array=None, z_tz_array=None,
                            q_z_crit=None, q_tq_array=None, nr_z_gate=None, full_section_discharge_for_headloss=True, name=None, hq_array=None, h_open=None, h_close=None, comment=None):
        geom = self.make_point(geom)
        z_pid_array = self.make_real_array(z_pid_array)
        z_tz_array = self.make_real_array(z_tz_array)
        q_tq_array = self.make_real_array(q_tq_array)
        hq_array = self.make_real_array(hq_array)
        self.__magic_update('regul_sluice_gate_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_brd(self, geom, zw_array, d_abutment_l, d_abutment_r, abutment_type, z_ceiling, name=None, comment=None):
        geom = self.make_point(geom)
        zw_array = self.make_real_array(zw_array)
        return self.__magic_insert('bradley_headloss_singularity', getargvalues(currentframe()).locals)

    def update_singularity_brd(self, id, geom, zw_array, d_abutment_l, d_abutment_r, abutment_type, z_ceiling, name=None, comment=None):
        geom = self.make_point(geom)
        zw_array = self.make_real_array(zw_array)
        self.__magic_update('bradley_headloss_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_brdg(self, geom, zw_array, l_road, z_road, full_section_discharge_for_headloss=True, name=None, comment=None):
        geom = self.make_point(geom)
        zw_array = self.make_real_array(zw_array)
        return self.__magic_insert('bridge_headloss_singularity', getargvalues(currentframe()).locals)

    def update_singularity_brdg(self, id, geom, zw_array, l_road, z_road, full_section_discharge_for_headloss=True, name=None, comment=None):
        geom = self.make_point(geom)
        zw_array = self.make_real_array(zw_array)
        self.__magic_update('bridge_headloss_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_mrkb(self, geom, pk0_km, dx, name=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('pipe_branch_marker_singularity', getargvalues(currentframe()).locals)

    def update_singularity_mrkb(self,id, geom, pk0_km, dx, name=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('pipe_branch_marker_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_dl(self, geom, z_weir, width, cc, name=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('weir_bc_singularity', getargvalues(currentframe()).locals)

    def update_singularity_dl(self, id, geom, z_weir, width, cc, name=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('weir_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_rk(self, geom, slope, k, width, name=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('strickler_bc_singularity', getargvalues(currentframe()).locals)

    def update_singularity_rk(self,id, geom, slope, k, width, name=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('strickler_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_hc(self, geom, q0, name=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('constant_inflow_bc_singularity', getargvalues(currentframe()).locals)

    def update_singularity_hc(self,id, geom, q0, name=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('constant_inflow_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_racc(self, geom, cascade_mode, zq_array, tz_array, name=None, quality=None, comment=None):
        geom = self.make_point(geom)
        zq_array = self.make_real_array(zq_array)
        tz_array = self.make_real_array(tz_array)
        return self.__magic_insert('model_connect_bc_singularity', getargvalues(currentframe()).locals)

    def update_singularity_racc(self, id, geom, cascade_mode, zq_array, tz_array, name=None, quality=None, comment=None):
        geom = self.make_point(geom)
        zq_array = self.make_real_array(zq_array)
        tz_array = self.make_real_array(tz_array)
        self.__magic_update('model_connect_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_dqh(self, geom, qq_array, downstream, split1, split2=None, name=None, comment=None):
        geom = self.make_point(geom)
        qq_array = self.make_real_array(qq_array)
        # downstream, split1 and split2 are calculated by DB on creation
        return self.__magic_insert('qq_split_hydrology_singularity', getargvalues(currentframe()).locals)

    def update_singularity_dqh(self, id, geom, qq_array, downstream, split1, split2=None, name=None, comment=None):
        geom = self.make_point(geom)
        qq_array = self.make_real_array(qq_array)
        if downstream:
            downstream_type, = self.execute("""select link_type from $model._link where id={}""".format(downstream)).fetchone()
        if split1:
            split1_type, = self.execute("""select link_type from $model._link where id={}""".format(split1)).fetchone()
        if split2:
            split2_type, = self.execute("""select link_type from $model._link where id={}""".format(split2)).fetchone()
        self.__magic_update('qq_split_hydrology_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_dzh(self, geom, downstream, downstream_law, downstream_param,
                split1, split1_law, split1_param, split2, split2_law, split2_param, name=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('zq_split_hydrology_singularity', getargvalues(currentframe()).locals)

    def update_singularity_dzh(self, id, geom, downstream, downstream_law, downstream_param,
                split1, split1_law, split1_param, split2, split2_law, split2_param, name=None, comment=None):
        geom = self.make_point(geom)
        if downstream:
            downstream_type, = self.execute("""select link_type from $model._link where id={}""".format(downstream)).fetchone()
        if split1:
            split1_type, = self.execute("""select link_type from $model._link where id={}""".format(split1)).fetchone()
        if split2:
            split2_type, = self.execute("""select link_type from $model._link where id={}""".format(split2)).fetchone()
        self.__magic_update('zq_split_hydrology_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_rsh(self, geom, drainage, overflow, q_drainage, z_ini, zs_array,
            treatment_mode=None, treatment_param=None, name=None, comment=None):
        geom = self.make_point(geom)
        zs_array = self.make_real_array(zs_array)
        # drainage and overflow are calculated by DB on creation
        return self.__magic_insert('reservoir_rs_hydrology_singularity', getargvalues(currentframe()).locals)

    def update_singularity_rsh(self, id, geom, drainage, overflow, q_drainage, z_ini, zs_array,
            treatment_mode=None, treatment_param=None, name=None, comment=None):
        geom = self.make_point(geom)
        zs_array = self.make_real_array(zs_array)
        if drainage:
            drainage_type, = self.execute("""select link_type from $model._link where id={}""".format(drainage)).fetchone()
        if overflow:
            overflow_type, = self.execute("""select link_type from $model._link where id={}""".format(overflow)).fetchone()
        self.__magic_update('reservoir_rs_hydrology_singularity', id, getargvalues(currentframe()).locals)

    def add_singularity_rsph(self, geom, drainage, overflow, z_ini, zr_sr_qf_qs_array,
            treatment_mode=None, treatment_param=None, name=None, comment=None):
        geom = self.make_point(geom)
        zr_sr_qf_qs_array = self.make_real_array(zr_sr_qf_qs_array)
        # drainage and overflow are calculated by DB on creation
        return self.__magic_insert('reservoir_rsp_hydrology_singularity', getargvalues(currentframe()).locals)

    def update_singularity_rsph(self,id, geom, drainage, overflow, z_ini, zr_sr_qf_qs_array,
            treatment_mode=None, treatment_param=None, name=None, comment=None):
        geom = self.make_point(geom)
        zr_sr_qf_qs_array = self.make_real_array(zr_sr_qf_qs_array)
        if drainage:
            drainage_type, = self.execute("""select link_type from $model._link where id={}""".format(drainage)).fetchone()
        if overflow:
            overflow_type, = self.execute("""select link_type from $model._link where id={}""".format(overflow)).fetchone()
        self.__magic_update('reservoir_rsp_hydrology_singularity', id, getargvalues(currentframe()).locals)

    def add_catchment_node(self, geom, name=None, area_ha=None, rl=None, slope=None, c_imp=None,
            netflow_type=None, constant_runoff=None, horner_ini_loss_coef=None, horner_recharge_coef=None,
            holtan_sat_inf_rate_mmh=None, holtan_dry_inf_rate_mmh=None, holtan_soil_storage_cap_mm=None,
            scs_j_mm=None, scs_soil_drainage_time_day=None, scs_rfu_mm=None,
            hydra_surface_soil_storage_rfu_mm=None, hydra_inf_rate_f0_mm_hr=None, hydra_int_soil_storage_j_mm=None, hydra_soil_drainage_time_qres_day=None,
            hydra_split_coefficient=None, hydra_catchment_connect_coef=None, hydra_aquifer_infiltration_rate=None,
            hydra_soil_infiltration_type=None,
            gr4_k1=None, gr4_k2=None, gr4_k3=None, gr4_k4=None,
            runoff_type=None, socose_tc_mn=None, socose_shape_param_beta=None, define_k_mn=None, q_limit=None, q0=None, contour=None, network_type=None,
            rural_land_use=None, industrial_land_use=None, suburban_housing_land_use=None, dense_housing_land_use=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert("catchment_node", getargvalues(currentframe()).locals)

    def update_catchment_node(self,id, geom=None, name=None, area_ha=None, rl=None, slope=None, c_imp=None,
            netflow_type=None, constant_runoff=None, horner_ini_loss_coef=None, horner_recharge_coef=None,
            holtan_sat_inf_rate_mmh=None, holtan_dry_inf_rate_mmh=None, holtan_soil_storage_cap_mm=None,
            scs_j_mm=None, scs_soil_drainage_time_day=None, scs_rfu_mm=None,
            hydra_surface_soil_storage_rfu_mm=None, hydra_inf_rate_f0_mm_hr=None, hydra_int_soil_storage_j_mm=None, hydra_soil_drainage_time_qres_day=None,
            hydra_split_coefficient=None, hydra_catchment_connect_coef=None, hydra_aquifer_infiltration_rate=None,
            hydra_soil_infiltration_type=None,
            gr4_k1=None, gr4_k2=None, gr4_k3=None, gr4_k4=None,
            runoff_type=None, socose_tc_mn=None, socose_shape_param_beta=None, define_k_mn=None, q_limit=None, q0=None, contour=None, network_type=None,
            rural_land_use=None, industrial_land_use=None, suburban_housing_land_use=None, dense_housing_land_use=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update("catchment_node", id, getargvalues(currentframe()).locals)

    def add_catchment_contour(self, geom, name=None):
        geom = self.make_polygon(geom, False)
        return self.__magic_insert('catchment', getargvalues(currentframe()).locals)

    def update_catchment_contour(self, id, geom, name=None):
        geom = self.make_polygon(geom, False)
        self.__magic_update('catchment', id, getargvalues(currentframe()).locals)

    def add_station(self, geom, name=None, comment=None):
        geom = self.make_polygon(geom)
        return self.__magic_insert('station', getargvalues(currentframe()).locals)

    def update_station(self, id, geom, name=None, comment=None):
        geom = self.make_polygon(geom)
        self.__magic_update('station', id, getargvalues(currentframe()).locals)

    def add_station_node(self, geom, z_invert=None, area=None, name=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('station_node', getargvalues(currentframe()).locals)

    def update_station_node(self, id, geom, z_invert=None, area=None, name=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('station_node', id, getargvalues(currentframe()).locals)

    def add_storage_node(self, geom, zs_array=None, zini=None, contraction_coef=None, name=None, comment=None):
        geom = self.make_point(geom)
        zs_array = self.make_real_array(zs_array)
        return self.__magic_insert('storage_node', getargvalues(currentframe()).locals)

    def update_storage_node(self, id, geom, zs_array=None, zini=None, contraction_coef=None, name=None, comment=None):
        geom = self.make_point(geom)
        zs_array = self.make_real_array(zs_array)
        self.__magic_update('storage_node', id, getargvalues(currentframe()).locals)

    def add_elem_2d_node(self, contour, zb=None, rk=None, domain_2d=None, area=None, name=None, comment=None):
        contour = self.make_polygon(contour)
        return self.__magic_insert('elem_2d_node', getargvalues(currentframe()).locals)

    def update_elem_2d_node(self, id, contour, zb=None, rk=None, domain_2d=None, area=None, name=None, comment=None):
        contour = self.make_polygon(contour)
        self.__magic_update('elem_2d_node', id, getargvalues(currentframe()).locals)

    def add_river_cross_section_pl1d(self, id, profile, geom=None, name=None):
        geom = self.make_point(geom)
        profile = self.make_line(profile)
        return self.__magic_insert('river_cross_section_pl1d', getargvalues(currentframe()).locals)

    def update_river_cross_section_pl1d(self, id, profile, geom=None, name=None):
        geom = self.make_point(geom)
        profile = self.make_line(profile)
        self.__magic_update('river_cross_section_pl1d', id, getargvalues(currentframe()).locals)

    def add_routing_hydrology_link(self, geom, cross_section, slope=None, length=None, split_coef=None, name=None, comment=None):
        geom = self.make_line(geom)
        return self.__magic_insert('routing_hydrology_link', getargvalues(currentframe()).locals)

    def update_routing_hydrology_link(self, id, geom, cross_section, slope, length, split_coef, name=None, comment=None):
        geom = self.make_line(geom)
        self.__magic_update('routing_hydrology_link', id, getargvalues(currentframe()).locals)

    def add_mesh_2d_link(self, geom, z_invert=None, lateral_contraction_coef=None, name=None, border=None, comment=None):
        geom = self.make_line(geom)
        border = self.make_line(border)
        return self.__magic_insert('mesh_2d_link', getargvalues(currentframe()).locals)

    def update_mesh_2d_link(self, id, geom, z_invert=None, lateral_contraction_coef=None, name=None, border=None, comment=None):
        geom = self.make_line(geom)
        border = self.make_line(border)
        self.__magic_update('mesh_2d_link', id, getargvalues(currentframe()).locals)

    def add_strickler_link(self, geom, z_crest1, width1, rk, z_crest2, width2, length=None, name=None, comment=None):
        geom = self.make_line(geom)
        return self.__magic_insert('strickler_link', getargvalues(currentframe()).locals)

    def update_strickler_link(self, id, geom, z_crest1, width1, rk, z_crest2, width2, length=None, name=None, comment=None):
        geom = self.make_line(geom)
        self.__magic_update('strickler_link', id, getargvalues(currentframe()).locals)

    def add_weir_link(self, geom, z_invert, width, cc, z_weir, v_max_cms, name=None, comment=None):
        geom = self.make_line(geom)
        return self.__magic_insert('weir_link', getargvalues(currentframe()).locals)

    def update_weir_link(self, id, geom, z_invert, width, cc, z_weir, v_max_cms, name=None, comment=None):
        geom = self.make_line(geom)
        self.__magic_update('weir_link', id, getargvalues(currentframe()).locals)

    def add_regul_gate_link(self, geom, z_invert=None, z_ceiling=None, width=None, cc=None, cc_submerged=None, action_gate_type=None, z_invert_stop=None, z_ceiling_stop=None,
                            v_max_cms=None, dt_regul_hr=None, mode_regul=None,
                            z_control_node=None, z_pid_array=None, z_tz_array=None,
                            q_z_crit=None, q_tq_array=None, nr_z_gate=None, name=None, hq_array=None, h_open=None, h_close=None, comment=None):
        geom = self.make_line(geom)
        z_pid_array = self.make_real_array(z_pid_array)
        z_tz_array = self.make_real_array(z_tz_array)
        q_tq_array = self.make_real_array(q_tq_array)
        hq_array = self.make_real_array(hq_array)
        return self.__magic_insert('regul_gate_link', getargvalues(currentframe()).locals)

    def update_regul_gate_link(self, id, geom, z_invert=None, z_ceiling=None, width=None, cc=None, cc_submerged=None, action_gate_type=None, z_invert_stop=None, z_ceiling_stop=None,
                            v_max_cms=None, dt_regul_hr=None, mode_regul=None,
                            z_control_node=None, z_pid_array=None, z_tz_array=None,
                            q_z_crit=None, q_tq_array=None, nr_z_gate=None, name=None, hq_array=None, h_open=None, h_close=None, comment=None):
        geom = self.make_line(geom)
        z_pid_array = self.make_real_array(z_pid_array)
        z_tz_array = self.make_real_array(z_tz_array)
        q_tq_array = self.make_real_array(q_tq_array)
        hq_array = self.make_real_array(hq_array)
        self.__magic_update('regul_gate_link', id, getargvalues(currentframe()).locals)

    def get_attrib_from_table_byid(self, table, id):
        return self.execute('select * from {} where id={}'.format(table, _quote(str(id)))).fetchall()

    def add_manhole_hydrology(self, geom, name=None, area=None, z_ground=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('manhole_hydrology_node', getargvalues(currentframe()).locals)

    def update_manhole_hydrology(self, id, geom=None, name=None, area=None, z_ground=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('manhole_hydrology_node', id, getargvalues(currentframe()).locals)

    def add_strickler_bc_singularity(self, geom, slope,k, width, name=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('strickler_bc_singularity', getargvalues(currentframe()).locals)

    def update_strickler_bc_singularity(self, id, geom, slope,k, width, name=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('strickler_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_porous_link(self, geom, z_invert, width, transmitivity, name=None, comment=None):
        geom = self.make_line(geom)
        return self.__magic_insert('porous_link', getargvalues(currentframe()).locals)

    def update_porous_link(self, id, geom, z_invert, width, transmitivity, name=None, comment=None):
        geom = self.make_line(geom)
        self.__magic_update('porous_link', id, getargvalues(currentframe()).locals)

    def add_borda_link(self, geom, law_type=None, param=None, name=None, comment=None):
        geom = self.make_line(geom)
        return self.__magic_insert('borda_headloss_link', getargvalues(currentframe()).locals)

    def update_borda_link(self, id, geom, law_type=None, param=None, name=None, comment=None):
        geom = self.make_line(geom)
        self.__magic_update('borda_headloss_link', id, getargvalues(currentframe()).locals)

    def add_qdp_link(self, geom, q_pump, qz_array, name=None, comment=None):
        geom = self.make_line(geom)
        qz_array = self.make_real_array(qz_array)
        return self.__magic_insert('deriv_pump_link', getargvalues(currentframe()).locals)

    def update_qdp_link(self, id, geom, q_pump, qz_array, name=None, comment=None):
        geom = self.make_line(geom)
        qz_array = self.make_real_array(qz_array)
        self.__magic_update('deriv_pump_link', id, getargvalues(currentframe()).locals)

    def add_pump_link(self, geom, npump, zregul_array, hq_array, name=None, comment=None):
        geom = self.make_line(geom)
        zregul_array = self.make_real_array(zregul_array)
        hq_array = self.make_real_array(hq_array)
        return self.__magic_insert('pump_link', getargvalues(currentframe()).locals)

    def update_pump_link(self, id, geom, npump, zregul_array, hq_array, name=None, comment=None):
        geom = self.make_line(geom)
        zregul_array = self.make_real_array(zregul_array)
        hq_array = self.make_real_array(hq_array)
        self.__magic_update('pump_link', id, getargvalues(currentframe()).locals)

    def add_overflow_link(self, geom, z_crest1=None, z_crest2=None, width1=None, width2=None, cc=None, lateral_contraction_coef=None,
                            break_mode=None, z_break=None, t_break=None, z_invert=None, width_breach=None, grp=None, dt_fracw_array=None, name=None, comment=None):
        geom = self.make_line(geom)
        dt_fracw_array = self.make_real_array(dt_fracw_array)
        return self.__magic_insert('overflow_link', getargvalues(currentframe()).locals)

    def update_overflow_link(self, id, geom, z_crest1=None, z_crest2=None, width1=None, width2=None, cc=None, lateral_contraction_coef=None,
                            break_mode=None, z_break=None, t_break=None, z_invert=None, width_breach=None, grp=None, dt_fracw_array=None, name=None, comment=None):
        geom = self.make_line(geom)
        dt_fracw_array = self.make_real_array(dt_fracw_array)
        self.__magic_update('overflow_link', id, getargvalues(currentframe()).locals)

    def add_network_overflow_link(self, geom, z_overflow=None, area=None, name=None, comment=None):
        geom = self.make_line(geom)
        return self.__magic_insert('network_overflow_link', getargvalues(currentframe()).locals)

    def update_network_overflow_link(self, id, geom, z_overflow=None, area=None, name=None, comment=None):
        geom = self.make_line(geom)
        self.__magic_update('network_overflow_link', id, getargvalues(currentframe()).locals)

    def add_crossroad_node(self, geom, area=None, z_ground=None, h=None, name=None, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('crossroad_node', getargvalues(currentframe()).locals)

    def update_crossroad_node(self, id, geom, area=None, z_ground=None, h=None, name=None, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('crossroad_node', id, getargvalues(currentframe()).locals)

    def add_connector_link(self, geom, main_branch, name=None, comment=None):
        geom = self.make_line(geom)
        return self.__magic_insert('connector_link', getargvalues(currentframe()).locals)

    def update_connector_link(self, id, geom,  main_branch, name=None, comment=None):
        geom = self.make_line(geom)
        self.__magic_update('connector_link', id, getargvalues(currentframe()).locals)

    def add_air_flow_bc_singularity(self, geom, name, flow, is_inwards, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('air_flow_bc_singularity', getargvalues(currentframe()).locals)

    def update_air_flow_bc_singularity(self, id, geom, name, flow, is_inwards, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('air_flow_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_air_pressure_bc_singularity(self, geom, name, relative_pressure, comment=None):
        geom = self.make_point(geom)
        return self.__magic_insert('air_pressure_bc_singularity', getargvalues(currentframe()).locals)

    def update_air_pressure_bc_singularity(self, id, geom, name, relative_pressure, comment=None):
        geom = self.make_point(geom)
        self.__magic_update('air_pressure_bc_singularity', id, getargvalues(currentframe()).locals)

    def add_air_duct_link(self, geom, z_invert_up, z_invert_down, area, perimeter, friction_coefficient, custom_length, comment=None):
        geom = self.make_line(geom)
        return self.__magic_insert('air_duct_link', getargvalues(currentframe()).locals)

    def update_air_duct_link(self, id, geom, z_invert_up, z_invert_down, area, perimeter, friction_coefficient, custom_length, comment=None):
        geom = self.make_line(geom)
        self.__magic_update('air_duct_link', id, getargvalues(currentframe()).locals)

    def add_air_headloss_link(self, geom, area, up_to_down_headloss_coefficient, down_to_up_headloss_coefficient, comment=None):
        geom = self.make_line(geom)
        return self.__magic_insert('air_headloss_link', getargvalues(currentframe()).locals)

    def update_air_headloss_link(self, id, geom, area, up_to_down_headloss_coefficient, down_to_up_headloss_coefficient, comment=None):
        geom = self.make_line(geom)
        self.__magic_update('air_headloss_link', id, getargvalues(currentframe()).locals)

    def add_ventilator_link(self, geom, q_dp_array, is_up_to_down, comment=None):
        geom = self.make_line(geom)
        q_dp_array = self.make_real_array(q_dp_array)
        return self.__magic_insert('ventilator_link', getargvalues(currentframe()).locals)

    def update_ventilator_link(self, id, geom, q_dp_array, is_up_to_down, comment=None):
        geom = self.make_line(geom)
        q_dp_array = self.make_real_array(q_dp_array)
        self.__magic_update('ventilator_link', id, getargvalues(currentframe()).locals)

    def add_jet_fan_link(self, geom, unit_thrust_newton, number_of_units, efficiency, flow_velocity, pipe_area, is_up_to_down, comment=None):
        geom = self.make_line(geom)
        return self.__magic_insert('jet_fan_link', getargvalues(currentframe()).locals)

    def update_jet_fan_link(self, id, geom, unit_thrust_newton, number_of_units, efficiency, flow_velocity, pipe_area, is_up_to_down, comment=None):
        geom = self.make_line(geom)
        self.__magic_update('jet_fan_link', id, getargvalues(currentframe()).locals)

    def load_water_level(self,name_node,scn=None) :
        '''fonction which take the name of a node and an optional scenario;
        then return the water_level if possible
        or none if no results is available'''

        if scn is None :
            scn = self.__proj.get_current_scenario()
        if scn is not None :
            if scn[0] > 0 :
                if self.__proj.scn_has_run(scn[0]):
                    result_path = os.path.join(self.__proj.get_senario_path(scn[0]), "hydraulique", scn[1].upper() + "_" + self.name.upper() + ".w15")
                    resultfile = HydraResult(result_path)
                    result = resultfile[str(name_node)][:]
                    z_water = max([item[1][1] for item in result])
                    return(z_water)
        return(None)

    def get_section_reach(self, reach) :
        ''' fonction used to give a dico with all the cross_section along a reach '''
        res = collections.OrderedDict()
        if reach is not None:
            list_cross_section = self.execute("""
                select rcs.id, rcs.name, n.pk_km, n.name
                from $model.river_cross_section_profile as rcs
                inner join $model.river_node as n on n.id = rcs.id
                where n.reach = {reach}
                order by n.pk_km asc
                """.format(reach=reach)).fetchall()
            for i in range(len(list_cross_section)) :
                id_cross_section = str(list_cross_section[i][0])
                res[id_cross_section] = {}
                res[id_cross_section]["name"] = list_cross_section[i][1]
                res[id_cross_section]["pk"] = list_cross_section[i][2]
        return res

    def get_section_reach_valley(self, reach, scn=None) :
        ''' fonction used to give a dico with all the cross_section along a reach with valley geometry some feature
            you can give a scn in order to ahve the water level in each cross_section for this given scn
            exemple dico format : {"16_up"{CP_16,0.001,"up",id_geom,id_constrain,water_level(z)}}'''

        res = collections.OrderedDict()

        list_cross_section = self.execute("""
            select rcs.id, rcs.name, n.pk_km, c.id,
            CASE when rcs.up_vcs_geom = g.id then up_vcs_geom  END as up_vcs_geom,
            CASE when rcs.down_vcs_geom = g.id then down_vcs_geom END as down_vcs_geom,
            n.reach,
            n.name
            from $model.river_cross_section_profile as rcs
            inner join $model.river_node as n on n.id = rcs.id
            inner join $model.valley_cross_section_geometry as g on (rcs.up_vcs_geom = g.id or rcs.down_vcs_geom = g.id)
            inner join $model.constrain as c on g.transect = c.id
            where
                n.reach = {reach}
            order by n.pk_km asc
            """.format(reach=reach)).fetchall()

        for i in range(len(list_cross_section)) :
            if list_cross_section[i][4] is not None :
                id_cross_section = str(list_cross_section[i][0])+"_up"
                res[id_cross_section] = {}
                res[id_cross_section]["name"] = list_cross_section[i][1]
                res[id_cross_section]["pk"] = list_cross_section[i][2]
                res[id_cross_section]["updown"] = "up"
                res[id_cross_section]["valley_geom_id"] = list_cross_section[i][4]
                res[id_cross_section]["transect"] = list_cross_section[i][3]
                res[id_cross_section]["water_level"] = self.load_water_level(list_cross_section[i][7], scn)

            if list_cross_section[i][5] is not None :
                id_cross_section = str(list_cross_section[i][0])+"_down"
                res[id_cross_section] = {}
                res[id_cross_section]["name"] = list_cross_section[i][1]
                res[id_cross_section]["pk"] = list_cross_section[i][2]
                res[id_cross_section]["updown"] = "down"
                res[id_cross_section]["valley_geom_id"] = list_cross_section[i][5]
                res[id_cross_section]["transect"] = list_cross_section[i][3]
                res[id_cross_section]["water_level"] = self.load_water_level(list_cross_section[i][7], scn)

        return res

    def insert_node_on_pipe(self, geom, name=None, area=None, z_ground=None):
        branch_trigger_state = self.set_branch_trigger(False)

        geom= self.make_point(geom)

        id_pipe, node_up, node_down, z_invert_up, z_invert_mid, z_invert_down, l_up, l_down, geom_up, geom_down, \
            comment, cross_section_type, h_sable, exclude_from_branch, rk, circular_diameter, \
            ovoid_height, ovoid_top_diameter, ovoid_invert_diameter, cp_geom, op_geom = self.execute("""
                with brut1 as (
                    select id, ST_Split(ST_Snap(geom, {g}, 0.001), {g}) as geom
                    from $model.pipe_link where ST_DWithin({g}, geom, .1)
                ),
                brut2 as (
                    select id, ST_GeometryN(geom, 1) as geom_up, ST_GeometryN(geom, 2) as geom_down
                    from brut1
                ),
                brut3 as (
                    select id, geom_up, geom_down, ST_Length(geom_up) as l_up, ST_Length(geom_down) as l_down
                    from brut2
                )
                select p.id, up, down, z_invert_up, ROUND(((l_up*z_invert_down + l_down*z_invert_up) / (l_up + l_down))::numeric, 3), z_invert_down,
                    l_up, l_down, ST_AsText(geom_up), ST_AsText(geom_down), comment, cross_section_type, h_sable, exclude_from_branch, rk, circular_diameter,
                    ovoid_height, ovoid_top_diameter, ovoid_invert_diameter, cp_geom, op_geom
                from $model.pipe_link as p, brut3 where p.id=brut3.id
                """.format(g=geom)).fetchone()

        attributes = [comment, cross_section_type, h_sable, exclude_from_branch, rk, circular_diameter, ovoid_height, ovoid_top_diameter, ovoid_invert_diameter, cp_geom, op_geom]

        node_new = None
        type_new = None

        if id_pipe:
            #insert node
            type_new, = self.execute("""select node_type from $model._node where id={} """.format(node_up)).fetchone()

            if type_new == 'manhole':
                node_new = self.add_manhole(geom, name, area, z_ground)
                #self.execute(del_query)
            elif type_new == 'manhole_hydrology':
                node_new = self.add_manhole_hydrology(geom, name, area, z_ground)

            # insert pipes
            id_pipe_up, = self.execute("""insert into $model.pipe_link(geom, z_invert_up, z_invert_down, comment, cross_section_type, h_sable, exclude_from_branch, rk,
                                            circular_diameter, ovoid_height, ovoid_top_diameter, ovoid_invert_diameter, cp_geom, op_geom)
                                          values (ST_SetSRID(ST_GeomFromText('{g}'), $srid), {z_up}, {z_down}, {attr})
                                          returning id
                                      """.format(g=geom_up, z_up=_encapsulate(z_invert_up), z_down=_encapsulate(z_invert_mid), attr=", ".join([_encapsulate(v) for v in attributes]))).fetchone()
            id_pipe_down, = self.execute("""insert into $model.pipe_link(geom, z_invert_up, z_invert_down, comment, cross_section_type, h_sable, exclude_from_branch, rk,
                                            circular_diameter, ovoid_height, ovoid_top_diameter, ovoid_invert_diameter, cp_geom, op_geom)
                                          values (ST_SetSRID(ST_GeomFromText('{g}'), $srid), {z_up}, {z_down}, {attr})
                                          returning id
                                      """.format(g=geom_down, z_up=_encapsulate(z_invert_mid), z_down=_encapsulate(z_invert_down), attr=", ".join([_encapsulate(v) for v in attributes]))).fetchone()

            if type_new == 'manhole_hydrology':
                # updates on singularities zq_split, qq_split, reservoir_rs and reservoir rsp that might reference the deleted pipe
                tables = {'qq_split_hydrology_singularity':['downstream', 'split1', 'split2'],
                            'zq_split_hydrology_singularity':['downstream', 'split1', 'split2'],
                            'reservoir_rs_hydrology_singularity':['drainage', 'overflow'],
                            'reservoir_rsp_hydrology_singularity':['drainage', 'overflow']}
                for table, columns in tables.items():
                    for column in columns:
                        self.execute("""update $model.{table} set {column}='{new_pipe}' where {column}={old_pipe} and node={node}
                                      """.format(table=table, column=column, new_pipe=id_pipe_up, old_pipe= id_pipe, node=node_up))
                        self.execute("""update $model.{table} set {column}='{new_pipe}' where {column}={old_pipe} and node={node}
                                            """.format(table=table, column=column, new_pipe=id_pipe_down, old_pipe= id_pipe, node=node_down))
            #deleting pipe
            self.execute("""delete from $model.pipe_link where id={} """.format(id_pipe))

        if branch_trigger_state:
            self.set_branch_trigger(True)
        return node_new, type_new

    def delete_node_update_pipe(self, id_node, table):
        from hydra.gui.delete import DeleteTool

        branch_trigger_state = self.set_branch_trigger(False)

        pipe_up, node_up, geom_up, z_invert_up = self.execute("""select id, up, ST_AsText(geom), z_invert_up from $model.pipe_link where down={}""".format(id_node)).fetchone()
        pipe_down, node_down, geom_down, z_invert_down = self.execute("""select id, down, ST_AsText(geom), z_invert_down from $model.pipe_link where up={}""".format(id_node)).fetchone()

        # get properties
        pipe_columns = ["comment", "cross_section_type", "h_sable", "exclude_from_branch", "rk",
                            "circular_diameter", "ovoid_height", "ovoid_top_diameter", "ovoid_invert_diameter", "cp_geom", "op_geom"]
        matching_columns = {}
        for column in pipe_columns:
            column_up, column_down = self.execute("""select u.{p}, d.{p} from $model.pipe_link as u, $model.pipe_link as d where u.id={iu} and d.id={id}
                                                     """.format(p=column, iu=pipe_up, id=pipe_down)).fetchone()
            if column_up == column_down and column_up is not None:
                matching_columns[column] = column_up

        # insert new pipe
        id_pipe, = self.execute("""insert into $model.pipe_link(geom, z_invert_up, z_invert_down{columns})
                                   values (ST_SetSRID(ST_MakeLine(ST_GeomFromText('{g_up}'), ST_GeomFromText('{g_down}')), $srid), {z_up}, {z_down}{values}) returning id
                                      """.format(g_up=geom_up, g_down=geom_down, z_up=_encapsulate(z_invert_up), z_down=_encapsulate(z_invert_down),
                                                 columns=", " + ", ".join(list(matching_columns.keys())) if matching_columns else "",
                                                 values=", " + ", ".join([_encapsulate(v) for v in list(matching_columns.values())])) if matching_columns else "").fetchone()

        # updates on singularities zq_split, qq_split, reservoir_rs and reservoir rsp that might reference the deleted pipes
        tables = {'qq_split_hydrology_singularity':['downstream', 'split1', 'split2'],
                    'zq_split_hydrology_singularity':['downstream', 'split1', 'split2'],
                    'reservoir_rs_hydrology_singularity':['drainage', 'overflow'],
                    'reservoir_rsp_hydrology_singularity':['drainage', 'overflow']}
        for tab, columns in tables.items():
            for column in columns:
                self.execute("""update $model.{tab}
                                        set {column}='{new_pipe}'
                                        where {column}={old_pipe} and node={node}
                                        """.format(tab=tab, column=column, new_pipe=id_pipe, old_pipe= pipe_up, node=node_up))
                self.execute("""update $model.{tab}
                                        set {column}='{new_pipe}'
                                        where {column}={old_pipe} and node={node}
                                        """.format(tab=tab, column=column, new_pipe=id_pipe, old_pipe= pipe_down, node=node_down))
        #delete old node
        DeleteTool.delete_node(None, self.__proj, self.name, table, id_node)

        if branch_trigger_state:
            self.set_branch_trigger(True)
        return id_pipe

    def fuse_reach(self, reach1_id, reach2_id):
        # insert new reach
        id_reach = self.execute("""with r1 as (select geom from $model.reach where id={id1}),
                                        r2 as (select geom from $model.reach where id={id2})
                                   insert into $model.reach(geom) select ST_MakeLine(r1.geom, r2.geom) from r1, r2 limit 1 returning id
                                """.format(id1=reach1_id, id2=reach2_id)).fetchone()[0]
        # update nodes
        self.execute("""update $model.river_node set reach={} where reach={} or reach={}""".format(id_reach, reach1_id, reach2_id))
        # delete old reach
        self.execute("""delete from $model.reach where id={} or id={}""".format(reach1_id, reach2_id))
        return id_reach

    def split_reach(self, reach_id, pk):
        # get info on reach
        reach_length, reach_geom = self.execute("""select ST_Length(reach.geom), geom from $model.reach where id={}""".format(reach_id)).fetchone()
        # insert new river nodes at new reaches extremities
        geom_node_1, = self.execute("""select ST_LineInterpolatePoint(reach.geom, {}) from $model.reach where id={}""".format((pk*reach_length-5)/reach_length, reach_id)).fetchone()
        self.add_river_node(geom_node_1)
        geom_node_2, = self.execute("""select ST_LineInterpolatePoint(reach.geom, {}) from $model.reach where id={}""".format((pk*reach_length+5)/reach_length, reach_id)).fetchone()
        self.add_river_node(geom_node_2)
        # delete old reach
        self.execute("""update $model.river_node set reach=null where reach={}""".format(reach_id))
        self.execute("""delete from $model.reach where id={}""".format(reach_id))
        # insert new reaches
        reach_id_1, reach_geom_1 = self.execute("""insert into $model.reach(geom) select ST_LineSubstring('{g}', 0, {p}) returning id, geom""".format(g=reach_geom, p=(pk*reach_length-5)/reach_length)).fetchone()
        reach_id_2, reach_geom_2 = self.execute("""insert into $model.reach(geom) select ST_LineSubstring('{g}', {p}, 1) returning id, geom""".format(g=reach_geom, p=(pk*reach_length+5)/reach_length)).fetchone()
        # update nodes
        self.execute("""update $model.river_node set reach={} where ST_Intersects(ST_Buffer(river_node.geom, 0.1), '{}')""".format(reach_id_1, reach_geom_1))
        self.execute("""update $model.river_node set reach={} where ST_Intersects(ST_Buffer(river_node.geom, 0.1), '{}')""".format(reach_id_2, reach_geom_2))

    def reverse_link(self, table, link_id):
        up, down, up_type, down_type = self.execute("""select up, down, up_type, down_type from $model.{} where id={}""".format(table, link_id)).fetchone()
        geoms = self.execute("""with points as (select (st_dumppoints(geom)).geom as pt from $model.{} where id={})
                                select ST_X(pt), ST_Y(pt) from points""".format(table, link_id)).fetchall()
        geom = "'srid={}; LINESTRINGZ({})'::geometry".format(
                str(self.__srid),
                ",".join([
                    str(p[0])+" "+str(p[1])+" "+(str(p[2]) if len(p) == 3 else "9999")
                    for p in geoms[::-1]])) if geoms else None
        self.__proj.try_execute("""update {}.{} set geom={}, up={}, down={}, up_type='{}', down_type='{}' where id={}""".format(self.name, table, geom, down, up, down_type, up_type, link_id))
