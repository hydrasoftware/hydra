from builtins import str
from builtins import range
from builtins import object
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
import numpy
import tempfile
from datetime import timedelta, datetime
from shapely import wkt
from shapely.ops import transform
from shapely.geometry import Polygon
from osgeo import gdal
from qgis.PyQt.QtCore import QCoreApplication, QDateTime
from hydra.utility.timer import format_timestamp, display_timestamp
from hydra.utility.string import isint
from hydra.utility.process import run_cmd

_tempdir = tempfile.gettempdir()

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

def get_timestamp(file, template):
    file_name = os.path.splitext(os.path.basename(file))[0]
    r = re.compile(re.escape(template).replace(12*r'\?', r'(\d{12})'))
    m = r.match(file_name)
    if m:
        time_str = m.group(1)
        if len(time_str)==12 and isint(time_str):
            return QDateTime.fromString(time_str, 'yyyyMMddhhmm')
        else:
            raise FileNameError("""Could not read time from file {n}. Required format is ''YYYMMDDhhmm''""".format(n=file))
    else:
        raise FileNameError("""Could not find time in file {n}. File name format must be {t}.tif (or .asc)""".format(n=file, t=template.replace(12*'?', 'YYYYMMDDhhmm')))

class FileNameError(Exception):
    pass

class RadarRain(object):
    '''generates rain raster files data'''
    def __init__(self, data_directory, project):
        self.__data_directory = data_directory
        if not os.path.isdir(self.__data_directory):
            os.mkdir(self.__data_directory)
        self.__project = project
        self.__datasource = None

    def __getattr__(self, attr):
        if attr == 'data_directory':
            return self.__data_directory
        else:
            raise AttributeError

    def build_vrt(self, name, source_files, template=None, srid=None):
        '''add a list of files to rain data
        uses gdal to convert data to multiband geotif'''
        created_files = []
        timestamps = []

        template = name+12*'?' if template is None else template
        assert 12*'?' in template

        #timestep = get_timestamp(source_files[0], template).msecsTo(get_timestamp(source_files[1], template))/1000.
        vrt_name = os.path.abspath(os.path.join(self.__data_directory, name+'.vrt'))
        #file_name = os.path.abspath(os.path.join(self.__data_directory, template.replace(12*'?', '')))

        if os.path.isfile(vrt_name):
            self.__project.log.warning("File {} already exists".format(vrt_name))
            return
        (id_,) = self.__project.execute("""insert into project.radar_rainfall(name, file) values ('{}', '{}') returning  id""".format(name, vrt_name)).fetchone()

        progress = self.__project.log.progress(tr("Processing rain data"))

        input_file = os.path.join(_tempdir, name+".txt")
        open(input_file, 'w').close() # clear file if exists

        for i, source_file_ in enumerate(source_files):
            progress.set_ratio(float(i+1)/len(source_files))

            created_files.append(os.path.join(self.__data_directory, template.replace(12*'?', '')+str(id_)+"_"+format_timestamp(get_timestamp(source_file_, template))+'.tif'))
            timestamps.append([i, format_timestamp(get_timestamp(source_file_, template)), get_timestamp(source_files[0], template).msecsTo(get_timestamp(source_file_, template))/1000.])
            with open(input_file,'a') as f:
                f.write(created_files[-1] + '\n')

            cmd1 = ['gdalwarp', '-ot', 'Float32'] + ['-t_srs', 'EPSG:%d'%(int(self.__project.srid))] + [source_file_, created_files[-1]]
            try:
                run_cmd(cmd1)
            except Exception:
                for f in created_files[:-1]:
                    os.remove(f)
                self.__project.execute("""delete from project.radar_rainfall where id ={}""".format(id_))
                self.__project.log.error("Error building band TIF {}".format(created_files[-1]))
                return
        progress.set_ratio(1)
        self.__project.log.clear_progress()

        if created_files:
            cmd2 = ['gdalbuildvrt', '-separate', '-input_file_list', input_file, vrt_name]

            try:
                run_cmd(cmd2, self.__project.log)
            except Exception:
                self.__project.execute("""delete from project.radar_rainfall where id ={}""".format(id_))
                self.__project.log.error("Error generating multi-band VRT {}".format(vrt_name))
                return

            with open(os.path.abspath(os.path.join(self.__data_directory, name+'.time')), 'w') as f:
                for [n, date, time] in timestamps:
                    f.write("%i %s %d\n" % (n, date, time))
            return vrt_name

    def has_data(self):
        return self.__project.execute("select count(1) from project.radar_rainfall").fetchone()[0] > 0

class RainVrt(object):
    '''loads and holds a radar rain VRT file'''
    def __init__(self, file, project):
        self.__project = project
        self.__file = self.__project.unpack_path(file)

        self.__time_index = [] # [index, date, time]
        with open(self.__file.replace('.vrt', '.time'), 'r') as f:
            for line in f:
                assert len(line.split()) == 3
                self.__time_index.append([int(x) for x in line.split()])

        self.dataset = gdal.Open(self.__file, gdal.GA_ReadOnly)
        self.__raster_size = [self.dataset.RasterXSize, self.dataset.RasterYSize]
        self.__band_number = self.dataset.RasterCount

        self.geotransform = self.dataset.GetGeoTransform()
        if self.geotransform:
            self.__origin = [self.geotransform[0], self.geotransform[3]]
            self.__pixel_size = [self.geotransform[1], self.geotransform[5]]

        self.__correction_coef, = self.__project.fetchone("select unit_correction_coef from project.radar_rainfall where name=%s", (os.path.splitext(os.path.basename(file))[0],))

    def __getattr__(self, attr):
        if attr == 'raster_size':
            return self.__raster_size
        elif attr == 'nx':
            return self.__raster_size[0]
        elif attr == 'ny':
            return self.__raster_size[1]
        elif attr == 'band_number':
            return self.__band_number
        elif attr == 'origin':
            return self.__origin
        elif attr == 'x0':
            return self.__origin[0]
        elif attr == 'y0':
            return self.__origin[1]
        elif attr == 'pixel_size':
            return self.__pixel_size
        elif attr == 'dx':
            return self.__pixel_size[0]
        elif attr == 'dy':
            return self.__pixel_size[1]
        elif attr == 'dt':
            return self.__time_index[1][2]
        elif attr == 'time_index':
            return self.__time_index
        elif attr == 't0':
            return display_timestamp(str(self.__time_index[0][1]))
        elif attr == 'date0':
            return datetime.strptime(str(self.__time_index[0][1]),'%Y%m%d%H%M')
        elif attr == 'correction_coef':
            return self.__correction_coef
        elif attr == 'file':
            return self.__file
        else:
            raise AttributeError

    def hyetogram(self, catchment_id, model, unit_correction_coef=1):
        catchment_contour, catchment_name = self.__project.execute("""select st_astext(geom), name from {}.catchment where id={}""".format(model, catchment_id)).fetchone()
        catchment_polygon = wkt.loads(catchment_contour)
        p = transform(lambda x, y: ((x - self.__origin[0])/self.__pixel_size[0], (y - self.__origin[1])/self.__pixel_size[1]), catchment_polygon)
        orig = (int(round(p.bounds[0])-1), int(round(p.bounds[1])-1))
        rang = (int(round(p.bounds[2] - orig[0])+1), int(round(p.bounds[3]-orig[1])+1))
        result = []
        scale = abs(self.__pixel_size[0] * self.__pixel_size[1] / catchment_polygon.area) # scale = S(px) / S(catchment)
        if p.is_valid:
            for band in range(self.__band_number):
                rb = self.dataset.GetRasterBand(band+1).ReadRaster(int(orig[0]), int(orig[1]), int(rang[0]), int(rang[1]), buf_type=gdal.GDT_Float32)
                if rb is not None:
                    pix_value = numpy.frombuffer(rb,  dtype=numpy.float32).reshape(int(rang[1]), int(rang[0]))
                    intensity = 0
                    for i in range(rang[0]):
                        for j in range(rang[1]):
                            if pix_value[j][i] > 0:
                                (x,y) = [orig[0]+i, orig[1]+j]
                                intensity += scale * pix_value[j][i] * Polygon([(x,y),(x+1,y),(x+1,y+1),(x,y+1)]).intersection(p).area * unit_correction_coef
                    result.append([self.date0+timedelta(seconds=band*self.dt), intensity])
            return result
        else:
            raise PolygonError("""Geometry of catchment {} is not valid for this operation. Please correct it.""".format(catchment_name))

    def export_hyetogram(self, catchment, model, unit_correction_coef=1):
        # catchment=[id, name]
        h = self.hyetogram(catchment[0], model, unit_correction_coef)
        res = "'%s'\n" % catchment[1]
        for i in range(len(h)):
            if not (round(h[i][1],3)==0 and (i==0 or round(h[i-1][1],3)==0) and (i==len(h)-1 or round(h[i+1][1],3)==0)):
                res += "{dt}; {v}\n".format(dt=h[i][0].strftime("%Y-%m-%d %H:%M "), v=str(h[i][1]))
        res += "\n"
        return res

class PolygonError(Exception):
    pass
