# -*- coding: utf-8 -*-

"""
***************************************************************************
    taudemProvider.py
    ---------------------
    Date                 : May 2012
    Copyright            : (C) 2012-2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = "Alexander Bruy"
__date__ = "May 2012"
__copyright__ = "(C) 2012-2019, Alexander Bruy"

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = "$Format:%H$"

import os

from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtCore import QCoreApplication

from qgis.core import QgsProcessingProvider

from processing.core.ProcessingConfig import ProcessingConfig, Setting

from .pitremove import PitRemove
from .aread8 import AreaD8
from .d8flowdir import D8FlowDir
from .areadinf import AreaDinf
from .dinfflowdir import DinfFlowDir
from .gridnet import GridNet

from .peukerdouglas import PeukerDouglas
from .threshold import Threshold
from .d8flowpathextremeup import D8FlowPathExtremeUp
from .slopearea import SlopeArea
from .lengtharea import LengthArea
from .dropanalysis import DropAnalysis
from .streamnet import StreamNet
from .moveoutletstostreams import MoveOutletsToStreams
from .gagewatershed import GageWatershed
from .connectdown import ConnectDown
from .peukerdouglasstreamdef import PeukerDouglasStreamDef
from .streamdefdropanalysis import StreamDefDropAnalysis
from .slopeareastreamdef import SlopeAreaStreamDef

from .slopearearatio import SlopeAreaRatio
from .d8hdisttostrm import D8HDistToStrm
from .dinfupdependence import DinfUpDependence
from .dinfdecayaccum import DinfDecayAccum
from .dinfconclimaccum import DinfConcLimAccum
from .dinftranslimaccum import DinfTransLimAccum
from .dinfrevaccum import DinfRevAccum
from .dinfdistdown import DinfDistDown
from .dinfdistup import DinfDistUp
from .dinfavalanche import DinfAvalanche
from .slopeavedown import SlopeAveDown
from .twi import Twi

from . import taudemUtils

pluginPath = os.path.dirname(__file__)


class TauDemProvider(QgsProcessingProvider):
    def __init__(self):
        super().__init__()
        self.algs = []

    def id(self):
        return "taudem"

    def name(self):
        return "TauDEM"

    def icon(self):
        return QIcon(os.path.join(pluginPath, "icons", "taudem.svg"))

    def load(self):
        ProcessingConfig.settingIcons[self.name()] = self.icon()
        ProcessingConfig.addSetting(
            Setting(self.name(), taudemUtils.TAUDEM_ACTIVE, self.tr("Activate"), False)
        )
        ProcessingConfig.addSetting(
            Setting(
                self.name(),
                taudemUtils.TAUDEM_DIRECTORY,
                self.tr("TauDEM directory"),
                taudemUtils.taudemDirectory(),
                valuetype=Setting.FOLDER,
            )
        )
        ProcessingConfig.addSetting(
            Setting(
                self.name(),
                taudemUtils.TAUDEM_MPICH,
                self.tr("MPICH2/OpenMPI bin directory"),
                taudemUtils.mpichDirectory(),
                valuetype=Setting.FOLDER,
            )
        )
        ProcessingConfig.addSetting(
            Setting(
                self.name(),
                taudemUtils.TAUDEM_PROCESSES,
                self.tr("MPI processes to use"),
                2,
                valuetype=Setting.INT,
            )
        )
        ProcessingConfig.addSetting(
            Setting(
                self.name(),
                taudemUtils.TAUDEM_VERBOSE,
                self.tr("Log commands output"),
                False,
            )
        )
        ProcessingConfig.readSettings()
        self.refreshAlgorithms()
        return True

    def unload(self):
        ProcessingConfig.removeSetting(taudemUtils.TAUDEM_ACTIVE)
        ProcessingConfig.removeSetting(taudemUtils.TAUDEM_DIRECTORY)
        ProcessingConfig.removeSetting(taudemUtils.TAUDEM_MPICH)
        ProcessingConfig.removeSetting(taudemUtils.TAUDEM_PROCESSES)
        ProcessingConfig.removeSetting(taudemUtils.TAUDEM_VERBOSE)

    def isActive(self):
        return ProcessingConfig.getSetting(taudemUtils.TAUDEM_ACTIVE)

    def setActive(self, active):
        ProcessingConfig.setSettingValue(taudemUtils.TAUDEM_ACTIVE, active)

    def supportsNonFileBasedOutput(self):
        return False

    def getAlgs(self):
        algs = [
            PitRemove(),
            AreaD8(),
            D8FlowDir(),
            AreaDinf(),
            DinfFlowDir(),
            GridNet(),
            PeukerDouglas(),
            Threshold(),
            D8FlowPathExtremeUp(),
            SlopeArea(),
            LengthArea(),
            DropAnalysis(),
            StreamNet(),
            MoveOutletsToStreams(),
            GageWatershed(),
            ConnectDown(),
            PeukerDouglasStreamDef(),
            StreamDefDropAnalysis(),
            SlopeAreaStreamDef(),
            SlopeAreaRatio(),
            D8HDistToStrm(),
            DinfUpDependence(),
            DinfDecayAccum(),
            DinfConcLimAccum(),
            DinfTransLimAccum(),
            DinfRevAccum(),
            DinfDistDown(),
            DinfDistUp(),
            DinfAvalanche(),
            SlopeAveDown(),
            Twi(),
        ]

        return algs

    def loadAlgorithms(self):
        self.algs = self.getAlgs()
        for a in self.algs:
            self.addAlgorithm(a)

    def tr(self, string, context=""):
        if context == "":
            context = "TauDemProvider"
        return QCoreApplication.translate(context, string)
