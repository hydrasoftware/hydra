# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run update tests on a project from V0.0.27

Warning : you need to have hydra postgresql in version 0.0.27 installed !

USAGE

   update_test.py [-hk]

OPTIONS

   -h, --help
        print this help

   -k, --keep
        does not delete test database after update

"""

assert __name__ == "__main__"

import sys
import os
import json
import getopt
import psycopg2
from .update import update_project
from .dump_restore import create_db, run_psql
from ..database import database as dbhydra
from ..project import Project
from ..utility.empty_ui_manager import EmptyUIManager
from ..utility.log import LogManager, ConsoleLogger
from ..utility.settings_properties import SettingsProperties

def table_exists(schema_name, table_name):
    return test_project.execute("""select exists (select 1 from information_schema.tables where table_schema = '{}' and table_name = '{}')""".format(schema_name, table_name)).fetchone()[0]

def function_exists(function_name, table_name):
    return test_project.execute("""select exists (select 1 from information_schema.tables where table_schema = '{}' and table_name = '{}')""".format(schema_name, table_name)).fetchone()[0]

def enum_exists(enum_name, values=[]):
    res_exists = test_project.execute("select exists (select 1 from pg_type where typname = 'hydra_{}')".format(enum_name)).fetchone()[0]
    res_values = True
    if len(values)>0:
        enum_values = test_project.execute("SELECT enum_range(NULL::hydra_{})".format(enum_name)).fetchone()[0]
        for value in values:
            if value not in enum_values:
                res_values = False
    return res_exists and res_values

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hk",
            ["help", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist
keep = "-k" in optlist or "--keep" in optlist

log = LogManager(ConsoleLogger(), "Hydra")

__currendir = os.path.dirname(__file__)
dic = {"updt_river_test":[["global"], 27561], "updt_wastewater_test":[["alc", "mrs", "pdc"], 3944]}
with open(os.path.join(__currendir, "..", "server", "hydra", "hydra.instances.json")) as f:
    instances = json.load(f)

for project, properties in dic.items():
    if dbhydra.project_exists(project):
        dbhydra.remove_project(project)
    log.notice("Creating database {} in version 0.0.27".format(project))
    create_db(log, project)

    log.notice("Uploading data")
    current_dir = os.path.abspath(os.path.dirname(__file__))
    test_data = os.path.join(current_dir, "test_data", project+".sql")
    run_psql(log, test_data, project, strict=True)

    dbhydra.insert_project_metadata(project, properties[1])

    test_project = Project(project, log, EmptyUIManager())

    assert test_project.get_version()=='0.0.27'
    update_project(test_project)
    assert test_project.get_version()==dbhydra.data_version()
    log.notice("Final version: {}".format(test_project.get_version()))

    # Check hydra types and associated tables exists
    assert table_exists('hydra', 'metadata')
    for table in list(instances.keys()):
        assert enum_exists(table, list(instances[table].keys()))
        assert table_exists('hydra', table)
    log.notice("Checked: Hydra enums")

    # Check model custom views exists (often dropped and recreated at updates)
    for model in dic[test_project.name][0]:
        for view in ['channel_reach', 'open_reach', 'configured', 'configured_current', 'invalid', 'l2d',
                    'street_link', 'flood_plain_bluff', 'flood_plain_bluff_point', 'pipe_link', 'river_node']:
            assert test_project.execute("""select exists (select 1 from pg_views where schemaname = '{}' and viewname = '{}')""".format(model, view)).fetchone()[0]
        log.notice("Checked: Model {} views".format(model))

        for mat_view in ['flood_plain', 'flood_plain_bluff_point_transect', 'flood_plain_flow_line', 'coted_point', 'coted_point_with_interp', 'mesh_1d', 'mesh_1d_coted',
                    'results_river', 'results_manhole', 'results_pipe', 'results_pipe_hydrol', 'results_link', 'results_catchment', 'results_surface']:
            assert test_project.execute("""select exists (select 1 from pg_matviews where schemaname = '{}' and matviewname = '{}')""".format(model, mat_view)).fetchone()[0]
        log.notice("Checked: Model {} materialized views".format(model))

    if not keep:
        dbhydra.remove_project(project)
        log.notice("Deleting database {}...".format(project))

sys.stdout.write("ok\n")
