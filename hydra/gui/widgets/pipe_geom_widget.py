################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import webbrowser
from qgis.PyQt.QtWidgets import QWidget, QGridLayout
from qgis.PyQt import uic
from .graph_widget import GraphWidget
from ..widgets.array_widget import ArrayWidget
from hydra.gui.base_dialog import tr


class PipeGeomWidget(QWidget):
    def __init__(self, project, parent=None, pipe_geom_table=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "pipe_geom_widget.ui"), self)
        self.project = project
        self.model = self.project.get_current_model()

        self.cp_geom_xyarray = ArrayWidget(["H (m)", "Width (m)"], [20,2], None,
            draw_graph_func=self.__draw_cp_geom, help_func=self.help_hyd_file, hide_headers=False, parent=self.array_cp_geom_placeholder)
        self.graph_cp_geom = GraphWidget(self.graph_cp_geom_placeholder, True, True)
        self.pipe_geom_table = pipe_geom_table

        self.cp_geom_name.textChanged.connect(self.cp_geom_update_name)

        self.set_enabled(False)

        self.tmplayout = QGridLayout()
        self.tmplayout.addWidget(self,0,0)

    def get_layout(self):
        return self.tmplayout

    def set_items(self, id_pipe_geom):
        if id_pipe_geom is not None:
            name, zbmin_array = self.project.execute("""select name, zbmin_array
                                                from {}.closed_parametric_geometry where id={}""".format(self.model.name, id_pipe_geom)).fetchone()
            self.cp_geom_name.setText(name)
            self.cp_geom_xyarray.set_table_items(zbmin_array)
            self.set_enabled(True)

    def set_enabled(self, bool=True):
        self.cp_geom_name.setEnabled(bool)
        self.cp_geom_xyarray.setEnabled(bool)
        self.graph_cp_geom.canvas.setVisible(bool)

    def cp_geom_update_name(self):
        if self.pipe_geom_table:
            self.pipe_geom_table.table.item(self.pipe_geom_table.table.row(self.pipe_geom_table.table.selectedItems()[0]),1).setText(self.cp_geom_name.text())

    def save_geom(self, id_pipe_geom):
        if id_pipe_geom:
            name = self.cp_geom_name.text()
            zbmin_array = self.cp_geom_xyarray.get_table_items()
            self.model.update_pipe_channel_closed_section_geometry(id_pipe_geom, zbmin_array, name)

    def __draw_cp_geom(self,datas):
        title = self.cp_geom_name.text()
        if len(datas)>0:
            datas.insert(0,[datas[0][0], 0])
            datas.append([datas[-1][0], 0])
            self.graph_cp_geom.clear()
            self.graph_cp_geom.add_symetric_line(
                [r[1]/2. for r in datas],
                [r[0] for r in datas],
                "k",
                title,
                tr("Width (m)"), tr("H (m)"))
        else:
            self.graph_cp_geom.canvas.setVisible(False)

    def help_hyd_file(self):
        webbrowser.open('http://hydra-software.net/docs/notes_techniques/notice_import_section.pdf', new=0)
