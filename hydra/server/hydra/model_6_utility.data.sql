/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

-- FOR EVERYTHING THAT HAS TO BE DONE LAST AT MODEL CREATION

create index ${model}_elem_2d_node_contour_idx on $model._elem_2d_node using gist(contour)
;;
create index ${model}_constrain_geom_idx on $model._constrain using gist(geom)
;;
create index ${model}_constrain_discretized_geom_idx on $model._constrain using gist(discretized)
;;
create index ${model}_coverage_geom_idx on $model.coverage using gist(geom)
;;


