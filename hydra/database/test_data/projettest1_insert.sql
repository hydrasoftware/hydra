/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/*
******************************************************************************************************************************************************************************************************
    RÉSEAU "RIVIERE"
******************************************************************************************************************************************************************************************************
*/

-- branches rivière
insert into mhriv.reach (name, pk0_km, dx, geom) values ('101', 100, 50, 'SRID=2154; LINESTRINGZ(988800 6558300 9999, 988600 6559000 9999, 987300 6559700 9999)'::geometry);
insert into mhriv.reach (name, pk0_km, dx, geom) values ('102', 110, 50, 'SRID=2154; LINESTRINGZ(987200 6559700 9999, 986200 6560000 9999)'::geometry);
insert into mhriv.reach (name, pk0_km, dx, geom) values ('103', 120, 50, 'SRID=2154; LINESTRINGZ(988800 6558100 9999, 988600 6558800 9999, 987300 6559500 9999, 986200 6559800 9999)'::geometry);
insert into mhriv.reach (name, pk0_km, dx, geom) values ('104', 140, 40, 'SRID=2154; LINESTRINGZ(988800 6557600 9999, 986200 6557600 9999)'::geometry);
insert into mhriv.reach (name, pk0_km, dx, geom) values ('105', 140, 40, 'SRID=2154; LINESTRINGZ(988800 6557400 9999, 986200 6557400 9999)'::geometry);

update mhriv.river_node set z_ground=9999 where z_ground is null;

-- noeuds rivières
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv3', (select id from mhriv.reach where upper(name)='101') , 9999, 1, 'SRID=2154; POINTZ(988185 6559223.4 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv4', (select id from mhriv.reach where upper(name)='103') , 9999, 1, 'SRID=2154; POINTZ(988185 6559023.4 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv5', (select id from mhriv.reach where upper(name)='103') , 9999, 1, 'SRID=2154; POINTZ(987300 6559500 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv401', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(988700 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv402', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(988600 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv403', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(988500 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv404', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(988400 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv405', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(988300 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv406', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(988200 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv407', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(988100 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv408', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(988000 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv409', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(987900 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv410', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(987800 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv411', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(987700 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv412', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(987600 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv413', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(987500 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv414', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(987400 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv415', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(987300 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv416', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(987200 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv417', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(987100 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv418', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(987000 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv419', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(986900 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv420', (select id from mhriv.reach where upper(name)='104') , 9999, 1, 'SRID=2154; POINTZ(986800 6557600 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv501', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(988700 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv502', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(988600 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv503', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(988500 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv504', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(988400 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv505', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(988300 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv506', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(988200 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv507', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(988100 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv508', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(988000 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv509', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(987900 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv510', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(987800 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv511', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(987700 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv512', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(987600 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv513', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(987500 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv514', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(987400 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv515', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(987300 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv516', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(987200 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv517', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(987100 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv518', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(987000 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv519', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(986900 6557400 9999)'::geometry);
insert into mhriv.river_node (name, reach, z_ground, area, geom) values ('node_riv520', (select id from mhriv.reach where upper(name)='105') , 9999, 1, 'SRID=2154; POINTZ(986800 6557400 9999)'::geometry);

-- PT1D (river_cross_section_profile)
with n as (select geom from mhriv.river_node where name='NODR1')
    insert into mhriv.river_cross_section_profile (name,z_invert_up,z_invert_down,type_cross_section_up,type_cross_section_down,geom)
    select 'pt1',null,99,null,'valley',n.geom from n;
with n as (select geom from mhriv.river_node where name='NODR2')
    insert into mhriv.river_cross_section_profile (name,z_invert_up,z_invert_down,type_cross_section_up,type_cross_section_down,geom)
    select 'pt2',49.50,null,'valley',null,n.geom from n;
with n as (select geom from mhriv.river_node where name='NODR3')
    insert into mhriv.river_cross_section_profile (name,z_invert_up,z_invert_down,type_cross_section_up,type_cross_section_down,geom)
    select 'pt3',null,49.50,null,'valley',n.geom from n;
with n as (select geom from mhriv.river_node where name='NODR4')
    insert into mhriv.river_cross_section_profile (name,z_invert_up,z_invert_down,type_cross_section_up,type_cross_section_down,geom)
    select 'pt4',48.80,null,'valley',null,n.geom from n;
with n as (select geom from mhriv.river_node where name='NODR5')
    insert into mhriv.river_cross_section_profile (name,z_invert_up,z_invert_down,type_cross_section_up,type_cross_section_down,geom)
    select 'pt5',null,50.00,null,'valley',n.geom from n;
with n as (select geom from mhriv.river_node where name='NODR6')
    insert into mhriv.river_cross_section_profile (name,z_invert_up,z_invert_down,type_cross_section_up,type_cross_section_down,geom)
    select 'pt6',48.70,null,'valley',null,n.geom from n;
with n as (select geom from mhriv.river_node where name='NODR7')
    insert into mhriv.river_cross_section_profile (name,z_invert_up,z_invert_down,type_cross_section_up,type_cross_section_down,geom)
    select 'pt7',null,50.00,null,'valley',n.geom from n;
with n as (select geom from mhriv.river_node where name='NODR8')
    insert into mhriv.river_cross_section_profile (name,z_invert_up,z_invert_down,type_cross_section_up,type_cross_section_down,geom)
    select 'pt8',48.70,null,'valley',null,n.geom from n;
with n as (select geom from mhriv.river_node where name='NODR9')
    insert into mhriv.river_cross_section_profile (name,z_invert_up,z_invert_down,type_cross_section_up,type_cross_section_down,geom)
    select 'pt9',null,50.00,null,'valley',n.geom from n;
with n as (select geom from mhriv.river_node where name='NODR10')
    insert into mhriv.river_cross_section_profile (name,z_invert_up,z_invert_down,type_cross_section_up,type_cross_section_down,geom)
    select 'pt10',48.70,null,'valley',null,n.geom from n;


-- PL1D (river_cross_section_pl1d)
with n as (select id, geom from mhriv.river_node where upper(name)='NODR7')
    insert into mhriv.river_cross_section_pl1d(id, name, profile, geom)  select n.id, 'pl1d_1'  , 'SRID=2154; LINESTRINGZ(988800 6557500 9999, 988800 6557580 9999, 988800 6557620 9999, 988800 6557700 9999)'::geometry, n.geom from n;
with n as (select id, geom from mhriv.river_node where upper(name)='NODE_RIV401')
    insert into mhriv.river_cross_section_pl1d(id, name, profile, geom)  select n.id, 'pl1d_401', 'SRID=2154; LINESTRINGZ(988700 6557500 9999, 988700 6557580 9999, 988700 6557620 9999, 988700 6557700 9999)'::geometry, n.geom from n;

-- with n as (select id, geom from mhriv.river_node where upper(name)='NODE_RIV402')
--    insert into mhriv.river_cross_section_pl1d(id, name, profile, geom)  select n.id, 'pl1d_402', 'SRID=2154; LINESTRINGZ(                     988600 6557580 9999, 988600 6557620 9999                     )'::geometry, n.geom from n;


with n as (select id, geom from mhriv.river_node where upper(name)='NODR8')
    insert into mhriv.river_cross_section_pl1d(id, name, profile, geom)  select n.id, 'pl1d_r8' , 'SRID=2154; LINESTRINGZ(986200 6557500 9999, 986200 6557580 9999, 986200 6557620 9999, 986200 6557700 9999)'::geometry, n.geom from n;




-- géométries de sections - riviere
insert into mhriv.valley_cross_section_geometry(name, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, rlambda, rmu1, rmu2) VALUES ('geom-rv1', '{{50.0999985,10.5},{51,11},{51.2000008,11.3000002},{51.5,11.8000002},{52,12},{53,13}}', '{{53.0999985,13.5},{54,14},{55,15},{56,16}}', '{{53.0999985,13.5},{54,14},{55,15},{56,16}}', 0.899999976, -1, -1);
insert into mhriv.valley_cross_section_geometry(name, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, rlambda, rmu1, rmu2, zlevee_lb, zlevee_rb)  values ('PT.E', '{{  64.810,   0.00}, {  65.080,  29.00}, {  65.710,  31.00}, {  66.490,  36.00}, {  66.490,  36.00}, {  66.490,  36.00}}' ,'{{  66.810,   18.40}, {  67.220,  33.00}, {  67.570, 280.00}, {  68.300, 430.00}}' ,'{{  66.490,   18.00}, {  67.660,  21.50}, {  68.290,  73.00}, {  68.300, 330.00}}' ,  0.200, 0.00100 , 0.00010 ,   68.400,    0.000);
insert into mhriv.valley_cross_section_geometry(name, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, rlambda, rmu1, rmu2, zlevee_lb, zlevee_rb)  values ('PT.F', '{{  58.000,   5.60}, {  58.450,  13.00}, {  58.500,  20.00}, {  59.150,  37.00}, {  60.500,  44.00}, {  60.970,  46.00}}' ,'{{  60.970,   23.00}, {  61.710,  88.00}, {  63.640, 127.00}, {  63.640, 127.00}}' ,'{{  61.000,   23.40}, {  61.510, 242.00}, {  61.510, 242.00}, {  61.510, 242.00}}' ,  0.510, 0.00100 , 0.00010 ,    0.000,   62.400);
insert into mhriv.valley_cross_section_topo_geometry(name,xz_array)  values ('pt.e', '{{  64.810,   0.00}, {  65.080,  29.00}, {  65.710,  31.00}, {  66.490,  36.00}, {  66.490,  36.00}, {  66.490,  36.00}}');
insert into mhriv.valley_cross_section_topo_geometry(name,xz_array)  values ('pt.f', '{{  64.810,   0.00}, {  65.080,  29.00}, {  65.710,  31.00}, {  66.490,  36.00}, {  66.490,  36.00}, {  66.490,  36.00}}');

-- sections - riviere
update mhriv.river_cross_section_profile
    set down_rk=35, down_rk_maj=15, down_sinuosity=1.10000002, down_vcs_geom = (select id from mhriv.valley_cross_section_geometry where upper(name)='GEOM-RV1')
    where upper(name)='PT1';
update mhriv.river_cross_section_profile
    set up_rk=35, up_rk_maj=15, up_sinuosity=1, up_vcs_geom = (select id from mhriv.valley_cross_section_geometry where upper(name)='GEOM-RV1')
    where upper(name)='PT2';
update mhriv.river_cross_section_profile
    set down_rk=35, down_rk_maj=15, down_sinuosity=1.10000002, down_vcs_geom = (select id from mhriv.valley_cross_section_geometry where upper(name)='GEOM-RV1')
    where upper(name)='PT3';
update mhriv.river_cross_section_profile
    set up_rk=35, up_rk_maj=14, up_sinuosity=1, up_vcs_geom = (select id from mhriv.valley_cross_section_geometry where upper(name)='GEOM-RV1')
    where upper(name)='PT4';
update mhriv.river_cross_section_profile
    set down_rk=35, down_rk_maj=14, down_sinuosity=1, down_vcs_geom = (select id from  mhriv.valley_cross_section_geometry where upper(name)='PT.E'), down_vcs_topo_geom=(select id from  mhriv.valley_cross_section_topo_geometry where upper(name)='PT.E')
    where upper(name)='PT5';
update mhriv.river_cross_section_profile
    set up_rk=35, up_rk_maj=14, up_sinuosity=1, up_vcs_geom = (select id from  mhriv.valley_cross_section_geometry where upper(name)='PT.F'), up_vcs_topo_geom=(select id from  mhriv.valley_cross_section_topo_geometry where upper(name)='PT.F')
    where upper(name)='PT6';
update mhriv.river_cross_section_profile
    set down_rk=35, down_rk_maj=14, down_sinuosity=1, down_vcs_geom = (select id from  mhriv.valley_cross_section_geometry where upper(name)='PT.E'), down_vcs_topo_geom=(select id from  mhriv.valley_cross_section_topo_geometry where upper(name)='PT.E')
    where upper(name)='PT7';
update mhriv.river_cross_section_profile
    set up_rk=35, up_rk_maj=14, up_sinuosity=1, up_vcs_geom = (select id from  mhriv.valley_cross_section_geometry where upper(name)='PT.F'), up_vcs_topo_geom=(select id from  mhriv.valley_cross_section_topo_geometry where upper(name)='PT.F')
    where upper(name)='PT8';
update mhriv.river_cross_section_profile
    set down_rk=35, down_rk_maj=14, down_sinuosity=1, down_vcs_geom = (select id from  mhriv.valley_cross_section_geometry where upper(name)='PT.E'), down_vcs_topo_geom=(select id from  mhriv.valley_cross_section_topo_geometry where upper(name)='PT.E')
    where upper(name)='PT9';
update mhriv.river_cross_section_profile
    set up_rk=35, up_rk_maj=14, up_sinuosity=1, up_vcs_geom = (select id from  mhriv.valley_cross_section_geometry where upper(name)='PT.F'), up_vcs_topo_geom=(select id from  mhriv.valley_cross_section_topo_geometry where upper(name)='PT.F')
    where upper(name)='PT10';

-- singularités - riviere
with n as (select geom from mhriv.river_node where name='node_riv413') insert into mhriv.hydraulic_cut_singularity (geom, name, qz_array) select n.geom, 'cpr1', '{{0,49.7},{10,49.9},{30,50.2}}' from n;
with n as (select geom from mhriv.river_node where upper(name)='NODE_RIV4') insert into mhriv.param_headloss_singularity (geom, name, q_dz_array) select n.geom, 'dh1', '{{0,0.},{10,0.01},{30,0.05}}' from n;
with n as (select geom from mhriv.river_node where upper(name)='NODE_RIV3') insert into mhriv.bradley_headloss_singularity (geom, name, d_abutment_l, d_abutment_r, abutment_type, zw_array, z_ceiling) select n.geom, 'bradley1', 0.1, 150., 'angle_90', '{{49.75,5.},{50.10,5.2},{52.,5.5}}',55.90 from n;
with n as (select geom from mhriv.river_node where upper(name)='NODE_RIV5')
    insert into mhriv.zregul_weir_singularity (geom, name, z_invert, z_regul, width, cc, mode_regul, reoxy_law, reoxy_param )
	select n.geom, 'devm1', 62.0, 62.5, 32, 0.66, 'elevation' ,'r15', '{"r15":{"reoxygen_coef":0.8, "temperature_correct_coef":0.9 }}' from n;

with n as (select geom from mhriv.river_node where upper(name)='NODE_RIV401')
    insert into mhriv.weir_bc_singularity (geom, name, z_weir, width, cc )
	select n.geom, 'QDL1_1', 65.4, 2.5, 0.4  from n;

with n as (select geom from mhriv.river_node where upper(name)='NODE_RIV402')
    insert into mhriv.tank_bc_singularity (geom, name, zs_array, zini)
	select n.geom, 'tank1','{{59.,100.},{59.5,105.},{61,108.}}', 59  from n;

-- apports et CL aval
with n as (select geom from mhriv.river_node where upper(name)='NODE_RIV410')
    insert into mhriv.model_connect_bc_singularity (geom, name, cascade_mode)
    select n.geom, 'raccord1', 'hydrograph' from n;

with n as (select geom from mhriv.river_node where upper(name)='NODR1') insert into mhriv.hydrograph_bc_singularity (geom, name, storage_area, constant_dry_flow, distrib_coef, lag_time_hr, external_file_data) select n.geom, 'hydrogramme1', 100, 0, 1, 10, true    from n;
with n as (select geom from mhriv.river_node where upper(name)='NODR3') insert into mhriv.constant_inflow_bc_singularity (geom, name, q0) select n.geom,'qconst1',25.     from n;
with n as (select geom from mhriv.river_node where upper(name)='NODR4') insert into mhriv.strickler_bc_singularity (geom,name,slope,k,width) select n.geom,'clav1',0.001,34.,20.     from n;
with n as (select geom from mhriv.river_node where upper(name)='NODR6') insert into mhriv.zq_bc_singularity (geom,name,zq_array) select n.geom,'clav3','{{50.,0.},{50.5,5.},{51,8.}}'     from n;
with n as (select geom from mhriv.river_node where upper(name)='NODR8') insert into mhriv.tz_bc_singularity (geom,name,cyclic,tz_array) select n.geom,'clav4',true,'{{0.,50.4},{6.,51.8},{12,50.4}}' from n;

with n as (select geom from mhriv.river_node where upper(name)='NODR5')
    insert into mhriv.hydrograph_bc_singularity (geom, name, storage_area, constant_dry_flow, distrib_coef, lag_time_hr, external_file_data, tq_array, pollution_dryweather_runoff)
	select n.geom, 'hydrogramme2', 10, 0.1, 1, 10, false, '{{0,1.5},{2,8},{3,3.5},{6,1.5}}', '{{11,12,26,22},{10,15,10.7,10.5}}' from n;


-- station de gestion avec noeuds
insert into mhriv.station(name,geom) values('stg1',	'SRID=2154; POLYGONZ((987000 6558000 9999, 987000 6558300 9999, 987400 6558300 9999, 987400 6558000 9999, 987000 6558000 9999))'::geometry);

with s as (select id,name from mhriv.station where upper(name)='STG1')
	insert into mhriv.station_node(name, area, z_invert, station, geom)
	select 'nodstg1', 50., 40., s.id, 'SRID=2154; POINTZ(987100 6558100 9999)'::geometry from s;

with s as (select id,name from mhriv.station where upper(name)='STG1')
	insert into mhriv.station_node(name, area, z_invert, station, geom)
	select 'nodstg2', 50., 40., s.id, 'SRID=2154; POINTZ(987200 6558100 9999)'::geometry from s;



-- paves 2D (elem_2d_node)
insert into mhriv.domain_2d(name) values('dom2d1');

insert into mhriv.elem_2d_node(name, area, zb, rk, domain_2d, contour, geom) values('pav1', 250., 41.15, 12., (select d.id from mhriv.domain_2d as d where upper(d.name)='DOM2D1'), 'SRID=2154; POLYGONZ((987800 6557800 9999, 987800 6557900 9999, 987900 6557900 9999, 987900 6557800 9999, 987800 6557800 9999))'::geometry, 'SRID=2154; POINTZ(987850 6557850 9999)'::geometry );
insert into mhriv.elem_2d_node(name, area, zb, rk, domain_2d, contour, geom) values('pav2', 250., 41.50, 12., (select d.id from mhriv.domain_2d as d where upper(d.name)='DOM2D1'), 'SRID=2154; POLYGONZ((987900 6557800 9999, 987900 6557900 9999, 988000 6557900 9999, 988000 6557800 9999, 987900 6557800 9999))'::geometry, 'SRID=2154; POINTZ(987950 6557850 9999)'::geometry );
insert into mhriv.elem_2d_node(name, area, zb, rk, domain_2d, contour, geom) values('pav3', 255., 41.40, 12., (select d.id from mhriv.domain_2d as d where upper(d.name)='DOM2D1'), 'SRID=2154; POLYGONZ((988000 6557800 9999, 988000 6557900 9999, 988100 6557900 9999, 988100 6557800 9999, 988000 6557800 9999))'::geometry, 'SRID=2154; POINTZ(988050 6557850 9999)'::geometry );


-- casiers (storage_node)
insert into mhriv.storage_node(name, zs_array, zini, geom) values('cas1','{ {40, 10000}, {41, 35000}, {41.5, 80000 } }', 40.2, 'SRID=2154; POINTZ(987400 6558000 9999)'::geometry );
insert into mhriv.storage_node(name, zs_array, zini, geom) values('cas2','{ {41, 11000}, {42, 32000}, {42.5, 80000 } }', 41.2, 'SRID=2154; POINTZ(987600 6558000 9999)'::geometry );
insert into mhriv.storage_node(name, zini, zs_array, geom) values('ilot1', 41, '{ {41, 10000}, {42, 35000}, {41.5, 80000 } }', 'SRID=2154; POINTZ(988650 6557250 9999)'::geometry );

-- carrefours (crossroad_node)
insert into mhriv.crossroad_node(name, area, z_ground, h, geom) values('car1', 125, 55.0, 0.5,  'SRID=2154; POINTZ(988700 6557300 9999)'::geometry );
insert into mhriv.crossroad_node(name, area, z_ground, h, geom) values('car2', 125, 56.0, 0.5,  'SRID=2154; POINTZ(988600 6557300 9999)'::geometry );
insert into mhriv.crossroad_node(name, area, z_ground, h, geom) values('car3', 126, 55.4, 0.5,  'SRID=2154; POINTZ(988500 6557300 9999)'::geometry );

insert into mhriv.street(name, width, elem_length, rk, geom) values('rue1', 12.4, 100., 44, 'SRID=2154; LINESTRINGZ(988700 6557300 9999, 988600 6557300 9999, 988500 6557300 9999)'::geometry);

-- liaisons
insert into mhriv.connector_link(name,geom) values('lmtr1','SRID=2154; LINESTRINGZ(987300 6559700 9999, 987200 6559700 9999)'::geometry);
insert into mhriv.strickler_link(name,z_crest1, width1, length, rk,geom) values('lstk1',53.11,45,48,14,'SRID=2154; LINESTRINGZ(988800 6558300 9999, 988800 6558100 9999)'::geometry);
insert into mhriv.porous_link(name,z_invert,width,transmitivity,geom) values('lpor1',51.10,100,0.0033,'SRID=2154; LINESTRINGZ(988185 6559223.4 9999, 988185 6559023.4 9999)'::geometry);
insert into mhriv.weir_link(name,z_invert,width,cc,z_weir,geom) values('ldev1',52.50,22,0.6,53.50,'SRID=2154; LINESTRINGZ(987300 6559700 9999, 987300 6559500 9999)'::geometry);
insert into mhriv.gate_link(name,z_invert,z_ceiling,width,cc,mode_valve,action_gate_type,z_gate,geom) values('lorf1',52.50,53.50,22,0.6,'no_valve','downward_opening',53.50,'SRID=2154; LINESTRINGZ(987200 6559700 9999, 987300 6559500 9999)'::geometry);
insert into mhriv.pump_link(name,npump,zregul_array,hq_array,geom) values('lpump2',4,'{{53.,54},{53.7,54.7},{54,55.7},{56.5,58}}','{{{5,0},{0,3},{0.1,3.1}},{{4,0},{0,3.5},{9,3.9}},{{0,4},{3.5,0},{9,3.9}},{{4,0},{0,3},{3.9,9}}}', 'SRID=2154; LINESTRINGZ(988700 6557600 9999, 988700 6557400 9999)'::geometry);
insert into mhriv.deriv_pump_link(name,q_pump,qz_array,geom) values('lderivpump2',10.,'{{0,53.1}, {0.25,53.2}, {0.55,53.3}}', 'SRID=2154; LINESTRINGZ(988500 6557600 9999, 988500 6557400 9999)'::geometry);
insert into mhriv.deriv_pump_link(name,q_pump,qz_array,geom) values('lderivpump3',20 ,'{{0,54.1}, {0.25,54.2}, {0.60,54.3}}', 'SRID=2154; LINESTRINGZ(988400 6557600 9999, 988400 6557400 9999)'::geometry);

with n as (select id, name from mhriv.river_node where upper(name)='NODE_RIV409')
	insert into mhriv.regul_gate_link(name, mode_regul, z_invert, z_ceiling, width, cc, action_gate_type, z_invert_stop, z_ceiling_stop, v_max_cms,
    z_control_node, z_pid_array, z_tz_array, dt_regul_hr, geom)
	select 'rgz1', 'elevation', 52.60,53.65,23,0.6, 'downward_opening',52.70, 53.50, 2.,
	   n.id, '{ 5,0.5,0.05}', '{{0.,55.},{1.5,55.5},{11,57.}}', 1.5,  'SRID=2154; LINESTRINGZ(988600 6557600 9999, 988600 6557400 9999)'::geometry from n;



/*
******************************************************************************************************************************************************************************************************
    RÉSEAU "ASSAINISSEMENT"
******************************************************************************************************************************************************************************************************
*/

-- secteur d'apport de temps sec (schéma project)
insert into project.dry_inflow_sector(name, geom) values('sect_app_ts_1', 'SRID=2154; POLYGON((988700 6561800, 991000 6561800, 991000 6559500, 988700 6559500, 988700 6561800))'::geometry);

-- noeuds assainissement
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh1', 2, 60.00, 'SRID=2154; POINTZ(987690 6560670 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh2', 2, 57.50, 'SRID=2154; POINTZ(987860 6560480 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh3', 2, 55.55, 'SRID=2154; POINTZ(987990 6560220 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh4', 2, 54.00, 'SRID=2154; POINTZ(987960 6559980 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh5', 2, 53.00, 'SRID=2154; POINTZ(987830 6559690 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh6', 2, 60.00, 'SRID=2154; POINTZ(988100 6560900 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh7', 2, 59.00, 'SRID=2154; POINTZ(988000 6560700 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh101', 2, 59.00, 'SRID=2154; POINTZ(987700 6560800 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh102', 2, 59.00, 'SRID=2154; POINTZ(987700 6560900 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh103', 2, 59.00, 'SRID=2154; POINTZ(987700 6561000 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh104', 2, 59.00, 'SRID=2154; POINTZ(987700 6561100 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh105', 2, 59.00, 'SRID=2154; POINTZ(987700 6561200 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh106', 2, 59.00, 'SRID=2154; POINTZ(987700 6561300 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh107', 2, 59.00, 'SRID=2154; POINTZ(987700 6561400 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh108', 2, 59.00, 'SRID=2154; POINTZ(987700 6561500 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh109', 2, 59.00, 'SRID=2154; POINTZ(987700 6561600 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh110', 2, 59.00, 'SRID=2154; POINTZ(987700 6561700 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh111', 2, 59.00, 'SRID=2154; POINTZ(987700 6561800 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh112', 2, 59.00, 'SRID=2154; POINTZ(987700 6561900 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh113', 2, 59.00, 'SRID=2154; POINTZ(987700 6562000 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh114', 2, 59.00, 'SRID=2154; POINTZ(987700 6562100 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh115', 2, 59.00, 'SRID=2154; POINTZ(987700 6562200 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh116', 2, 59.00, 'SRID=2154; POINTZ(987700 6562300 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh117', 2, 59.00, 'SRID=2154; POINTZ(987700 6562400 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh118', 2, 59.00, 'SRID=2154; POINTZ(987700 6562500 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh119', 2, 59.00, 'SRID=2154; POINTZ(987700 6562600 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh120', 2, 59.00, 'SRID=2154; POINTZ(987700 6562700 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh201', 2, 59.00, 'SRID=2154; POINTZ(987600 6560800 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh202', 2, 59.00, 'SRID=2154; POINTZ(987600 6560900 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh203', 2, 59.00, 'SRID=2154; POINTZ(987600 6561000 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh204', 2, 59.00, 'SRID=2154; POINTZ(987600 6561100 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh205', 2, 59.00, 'SRID=2154; POINTZ(987600 6561200 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh206', 2, 59.00, 'SRID=2154; POINTZ(987600 6561300 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh207', 2, 59.00, 'SRID=2154; POINTZ(987600 6561400 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh208', 2, 59.00, 'SRID=2154; POINTZ(987600 6561500 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh209', 2, 59.00, 'SRID=2154; POINTZ(987600 6561600 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh210', 2, 59.00, 'SRID=2154; POINTZ(987600 6561700 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh211', 2, 59.00, 'SRID=2154; POINTZ(987600 6561800 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh212', 2, 59.00, 'SRID=2154; POINTZ(987600 6561900 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh213', 2, 59.00, 'SRID=2154; POINTZ(987600 6562000 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh214', 2, 59.00, 'SRID=2154; POINTZ(987600 6562100 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh215', 2, 59.00, 'SRID=2154; POINTZ(987600 6562200 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh216', 2, 59.00, 'SRID=2154; POINTZ(987600 6562300 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh217', 2, 59.00, 'SRID=2154; POINTZ(987600 6562400 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh218', 2, 59.00, 'SRID=2154; POINTZ(987600 6562500 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh219', 2, 59.00, 'SRID=2154; POINTZ(987600 6562600 9999)'::geometry);
insert into mhnet.manhole_node (name, area, z_ground, geom) values ('node_mh220', 2, 59.00, 'SRID=2154; POINTZ(987600 6562700 9999)'::geometry);

-- géométries de sections - assainissement
insert into mhnet.closed_parametric_geometry (name, zbmin_array) values ('GEOM-PF1', '{{0,0},{0.5,1},{0.699999988,1.29999995},{0.800000012,1.79999995},{1,1.20000005},{1.29999995,1.35000002},{1.5,0}}');
insert into mhnet.open_parametric_geometry (name, zbmin_array) values ('GEOM-PO1', '{{0,0},{0.5,5},{0.699999988,5.30000019},{0.800000012,5.80000019},{1,6.19999981},{1.29999995,6.3499999},{1.5,7}}');

-- liaisons
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'cana1', 59.00, 56.5, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987690 6560670 9999, 987860 6560480 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'cana2', 56.5, 54.55, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987860 6560480 9999, 987990 6560220 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'cana3', 54.55, 53.0, 'circular', 0, 85, 0.800, 'SRID=2154; LINESTRINGZ(987990 6560220 9999, 987960 6559980 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, geom) select 'cana4', 53.00, 52.7, 'pipe', 0, 65, 'SRID=2154; LINESTRINGZ(987960 6559980 9999, 987830 6559690 9999)'::geometry;
update mhnet.pipe_link set cp_geom = (select id from  mhnet.closed_parametric_geometry where upper(name)='GEOM-PF1') where name = 'cana4';
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana5', 59.70, 57.7, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(988100 6560900 9999, 988000 6560700 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana6', 57.70, 56.7, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(988000 6560700 9999, 987860 6560480 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana101', 59.10, 59.00, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6560800 9999, 987690 6560670 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana102', 59.20, 59.10, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6560900 9999, 987700 6560800 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana103', 59.30, 59.20, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6561000 9999, 987700 6560900 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana104', 59.40, 59.30, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6561100 9999, 987700 6561000 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana105', 59.50, 59.40, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6561200 9999, 987700 6561100 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana106', 59.60, 59.50, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6561300 9999, 987700 6561200 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana107', 59.70, 59.60, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6561400 9999, 987700 6561300 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana108', 59.80, 59.70, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6561500 9999, 987700 6561400 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana109', 59.90, 59.80, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6561600 9999, 987700 6561500 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana110', 60.00, 59.90, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6561700 9999, 987700 6561600 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana111', 60.10, 60.00, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6561800 9999, 987700 6561700 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana112', 60.20, 60.10, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6561900 9999, 987700 6561800 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana113', 60.30, 60.20, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6562000 9999, 987700 6561900 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana114', 60.40, 60.30, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6562100 9999, 987700 6562000 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana115', 60.50, 60.40, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6562200 9999, 987700 6562100 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana116', 60.60, 60.50, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6562300 9999, 987700 6562200 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana117', 60.70, 60.60, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6562400 9999, 987700 6562300 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana118', 60.80, 60.70, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6562500 9999, 987700 6562400 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana119', 60.90, 60.80, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6562600 9999, 987700 6562500 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana120', 61.00, 60.90, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987700 6562700 9999, 987700 6562600 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana201', 59.10, 59.00, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6560800 9999, 987690 6560670 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana202', 59.20, 59.10, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6560900 9999, 987600 6560800 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana203', 59.30, 59.20, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6561000 9999, 987600 6560900 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana204', 59.40, 59.30, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6561100 9999, 987600 6561000 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana205', 59.50, 59.40, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6561200 9999, 987600 6561100 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana206', 59.60, 59.50, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6561300 9999, 987600 6561200 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana207', 59.70, 59.60, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6561400 9999, 987600 6561300 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana208', 59.80, 59.70, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6561500 9999, 987600 6561400 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana209', 59.90, 59.80, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6561600 9999, 987600 6561500 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana210', 60.00, 59.90, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6561700 9999, 987600 6561600 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana211', 60.10, 60.00, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6561800 9999, 987600 6561700 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana212', 60.20, 60.10, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6561900 9999, 987600 6561800 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana213', 60.30, 60.20, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6562000 9999, 987600 6561900 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana214', 60.40, 60.30, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6562100 9999, 987600 6562000 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana215', 60.50, 60.40, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6562200 9999, 987600 6562100 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana216', 60.60, 60.50, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6562300 9999, 987600 6562200 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana217', 60.70, 60.60, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6562400 9999, 987600 6562300 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana218', 60.80, 60.70, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6562500 9999, 987600 6562400 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana219', 60.90, 60.80, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6562600 9999, 987600 6562500 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter,  geom) select 'cana220', 61.00, 60.90, 'circular', 0, 75, 0.500, 'SRID=2154; LINESTRINGZ(987600 6562700 9999, 987600 6562600 9999)'::geometry;

-- singularités - assainissement
with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH3') insert into mhnet.gate_singularity (geom, name, z_invert, z_ceiling, width, cc) select n.geom, 'Vanne1', 54.55, 55.55, 0.5, 0.6 from n;

with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH2'), nc as (select id from mhnet.manhole_node where upper(name)='NODE_MH5')
    insert into mhnet.regul_sluice_gate_singularity (geom, name, z_invert, width, z_ceiling, cc, v_max_cms, mode_regul, z_control_node, z_pid_array, z_tz_array, dt_regul_hr)
    select                                           n.geom, 'Acta1', 56.55, 0.8  , 57.45  ,0.6, 2.       ,  'elevation', nc.id, '{10.,11.1,12.2}','{{0,56.60},{1.5,56.65},{2.2,56.70}}',0.25 from n, nc;


-- singularités borda_headloss_singularity (14 types) - assainissement
with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH101')
    insert into mhnet.borda_headloss_singularity (geom, name, law_type, param)
	select n.geom, 'borda1', 'q', '{"q":{"coef_kq2":1.5 }}' from n;

with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH102')
    insert into mhnet.borda_headloss_singularity (geom, name, law_type, param)
	select n.geom, 'borda2', 'v', '{"v":{"reference_cs":"upstream", "coef_kv_12":0.7, "coef_kv_21":0.6 }}' from n;

with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH105')
    insert into mhnet.borda_headloss_singularity (geom, name, law_type, param)
	select n.geom, 'borda5', 'sharp_transition_geometry', '{"sharp_transition_geometry":{"middle_option":"yes", "middle_cs_area":1.51 }}' from n;

with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH106')
    insert into mhnet.borda_headloss_singularity (geom, name, law_type, param)
	select n.geom, 'borda6', 'contraction_with_transition', '{"contraction_with_transition":{"half_angle":15, "transition_option":"conic" }}' from n;

with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH107')
    insert into mhnet.borda_headloss_singularity (geom, name, law_type, param)
	select n.geom, 'borda7', 'enlargment_with_transition', '{"enlargment_with_transition":{"transition_option":"flat", "half_angle":45 }}' from n;

with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH108')
    insert into mhnet.borda_headloss_singularity (geom, name, law_type, param)
	select n.geom, 'borda8', 'circular_bend', '{"circular_bend":{"curve_radius":12, "curve_angle":10, "wall_friction_option":"rough" }}' from n;

with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH109')
    insert into mhnet.borda_headloss_singularity (geom, name, law_type, param)
	select n.geom, 'borda9', 'sharp_angle_bend', '{"sharp_angle_bend":{"connector_no":2, "angle":35.0, "ls":15.5, "wall_friction_option":"smooth" }}' from n;

with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH110')
    insert into mhnet.borda_headloss_singularity (geom, name, law_type, param)
	select n.geom, 'borda10', 'screen_normal_to_flow', '{"screen_normal_to_flow":{"grid_shape":"rectangular_edge", "blocage_coef":0.5, "v_angle":30.0, "deposition_option":"modern_automatic_grid" }}' from n;

with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH111')
    insert into mhnet.borda_headloss_singularity (geom, name, law_type, param)
	select n.geom, 'borda11', 'screen_oblique_to_flow', '{"screen_oblique_to_flow":{"grid_shape_oblique":"rectangular_edge", "v_angle":32.0, "blocage_coef":0.33 }}' from n;

with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH112')
    insert into mhnet.borda_headloss_singularity (geom, name, law_type, param)
	select n.geom, 'borda12', 'friction', '{"friction":{"ks":25, "length":111 }}' from n;

with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH113')
    insert into mhnet.borda_headloss_singularity (geom, name, law_type, param)
	select n.geom, 'borda13', 'parametric', '{"parametric":{"qh_array":[[0.0,0.0],[0.10,0.06],[0.20,0.30]]}}' from n;

with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH114')
    insert into mhnet.borda_headloss_singularity (geom, name, law_type, param)
	select n.geom, 'borda14', 'sharp_bend_rectangular', '{"sharp_bend_rectangular":{"bend_shape":"z_shaped_elbow", "param1":0.23, "param2":0.33 }}' from n;



-- apports et CL aval
-- with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH5') insert into mhnet.froude_bc_singularity (geom,name) select n.geom,'clzf1' from n;

with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH5')
    insert into mhnet.model_connect_bc_singularity (geom, name, cascade_mode, zq_array)
    select n.geom, 'raccord1', 'zq_downstream_condition', '{{53.,0.}, {54,0.5}, {55,0.8}}' from n;

with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH120') , s as (select id from project.dry_inflow_sector  where upper(name)='SECT_APP_TS_1')
    insert into mhnet.hydrograph_bc_singularity (geom, name, storage_area, constant_dry_flow, distrib_coef, lag_time_hr, sector, external_file_data ,tq_array, pollution_dryweather_runoff)
	select n.geom, 'hydrogramme3', 10, 0.1, 1, 10, s.id, false, '{{0,0.5},{1,8},{3,1.5},{6,0.5}}', '{{11,12,26,22},{10,15,10.7,10.5}}' from n, s;


-- markers des branches assainissement
with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH7') insert into mhnet.marker_singularity (geom,name) select n.geom,'b2' from n;
with n as (select geom from mhnet.manhole_node where upper(name)='NODE_MH4') insert into mhnet.marker_singularity (geom,name) select n.geom,'b3' from n;


-- liaisons LMK - assainissement
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk1', 'q', '{"q":{"coef_kq2":0.15 }}'  ,'SRID=2154; LINESTRINGZ(987700 6560800 9999, 987600 6560800 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk2', 'v', '{"v":{"up_geom":"computed", "down_geom":"computed", "up_cs_area":2.55, "down_cs_area":2.65, "up_cs_zinvert":48.23, "down_cs_zinvert":59.3, "reference_cs":"downstream", "coef_kv_12":1.5, "coef_kv_21":1.4 }}'  ,'SRID=2154; LINESTRINGZ(987700 6560900 9999, 987600 6560900 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk3', 'lateral_inlet', '{"lateral_inlet":{"down_geom":"computed", "down_cs_area":2.64, "down_cs_zinvert":59.98, "inlet_cs_area":1.12, "inlet_shape":"rounded" }}'  ,'SRID=2154; LINESTRINGZ(987700 6561000 9999, 987600 6561000 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk4', 'lateral_outlet', '{"lateral_outlet":{"up_geom":"computed", "up_cs_area":2.65,"up_cs_zinvert":59.92, "outlet_cs_area":1.11, "outlet_shape":"rounded" }}'  ,'SRID=2154; LINESTRINGZ(987700 6561100 9999, 987600 6561100 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk5', 'bifurcation', '{"bifurcation":{"up_geom":"computed", "down_geom":"computed", "up_cs_area":2.55, "down_cs_area":2.65, "up_cs_zinvert":60.15, "down_cs_zinvert":59.90, "angle":25.2 }}'  ,'SRID=2154; LINESTRINGZ(987700 6561200 9999, 987600 6561200 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk6', 'junction', '{"junction":{"up_geom":"computed", "down_geom":"computed", "up_cs_area":0.55, "down_cs_area":0.65, "up_cs_zinvert":60.1, "down_cs_zinvert":59.88, "angle":15.2 }}'  ,'SRID=2154; LINESTRINGZ(987700 6561300 9999, 987600 6561300 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk7', 'transition_edge_inlet', '{"transition_edge_inlet":{"down_geom":"computed", "down_cs_area":2.65,"down_cs_zinvert":59.82, "angle":90, "entrance_shape":"rounded" }}'  ,'SRID=2154; LINESTRINGZ(987700 6561400 9999, 987600 6561400 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk8', 'diffuser', '{"diffuser":{"up_geom":"computed", "up_cs_area":0.55, "up_cs_zinvert":61.5, "half_angle":65, "length":122, "diffus_geom":"square" }}'  ,'SRID=2154; LINESTRINGZ(987700 6561500 9999, 987600 6561500 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk9', 'sharp_transition_geometry', '{"sharp_transition_geometry":{"up_geom":"computed", "down_geom":"computed", "up_cs_area":2.55, "down_cs_area":2.65, "up_cs_zinvert":48.89, "down_cs_zinvert":59.7, "middle_option":"yes", "middle_cs_area":2.22 }}'  ,'SRID=2154; LINESTRINGZ(987700 6561600 9999, 987600 6561600 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk10', 'contraction_with_transition', '{"contraction_with_transition":{"up_geom":"computed", "down_geom":"computed", "up_cs_area":2.55, "down_cs_area":2.65, "up_cs_zinvert":48.3, "down_cs_zinvert":58.3, "transition_option":"conic", "half_angle":15 }}'  ,'SRID=2154; LINESTRINGZ(987700 6561700 9999, 987600 6561700 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk11', 'enlargment_with_transition', '{"enlargment_with_transition":{"up_geom":"computed", "down_geom":"computed", "up_cs_area":0.65, "down_cs_area":0.75, "up_cs_zinvert":48.2, "down_cs_zinvert":57.7, "transition_option":"conic", "half_angle":15.1 }}'  ,'SRID=2154; LINESTRINGZ(987700 6561800 9999, 987600 6561800 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk12', 'circular_bend', '{"circular_bend":{"up_geom":"computed", "up_cs_area":0.85, "up_cs_zinvert":48.2, "curve_radius":5.2, "curve_angle":48, "wall_friction_option":"smooth" }}'  ,'SRID=2154; LINESTRINGZ(987700 6561900 9999, 987600 6561900 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk13', 'sharp_angle_bend', '{"sharp_angle_bend":{"up_geom":"computed", "up_cs_area":0.55, "up_cs_zinvert":0.65, "connector_no":9, "ls":0.95, "angle":15.2, "wall_friction_option":"rough" }}'  ,'SRID=2154; LINESTRINGZ(987700 6562000 9999, 987600 6562000 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk14', 'screen_normal_to_flow', '{"screen_normal_to_flow":{"up_geom":"computed", "up_cs_area":0.52, "up_cs_zinvert":50.65, "obstruction_rate":0.65, "grid_shape":"circular_edge", "deposition_option":"used_automatic", "v_angle":15.2 }}'  ,'SRID=2154; LINESTRINGZ(987700 6562100 9999, 987600 6562100 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk15', 'screen_oblique_to_flow', '{"screen_oblique_to_flow":{"up_geom":"computed", "up_cs_area":0.55, "up_cs_zinvert":50.65, "grid_shape_oblique":"rectangular_edge", "v_angle":15.2, "blocage_coef":0.5 }}'  ,'SRID=2154; LINESTRINGZ(987700 6562200 9999, 987600 6562200 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk16', 'friction', '{"friction":{"up_geom":"computed", "up_cs_area":0.55, "up_cs_zinvert":50.65, "length":105, "ks":80 }}'  ,'SRID=2154; LINESTRINGZ(987700 6562300 9999, 987600 6562300 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk17', 'parametric', '{"parametric":{"qh_array":[[0.0,0.0],[0.10,0.06],[0.20,0.30]]}}'  ,'SRID=2154; LINESTRINGZ(987700 6562400 9999, 987600 6562400 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk18', 'sharp_bend_rectangular', '{"sharp_bend_rectangular":{"up_cs_area":1.3, "up_geom":"computed", "down_cs_area":1.3, "down_geom":"computed", "up_cs_zinvert":48.23, "down_cs_zinvert":59.3, "bend_shape":"z_shaped_elbow", "param1":0.23, "param2":0.33 }}'  ,'SRID=2154; LINESTRINGZ(987700 6562500 9999, 987600 6562500 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk19', 't_shape_bifurcation', '{"t_shape_bifurcation":{"up_geom":"computed", "down_geom":"computed", "up_cs_area":2.55, "down_cs_area":2.65, "up_cs_zinvert":48.23, "down_cs_zinvert":59.3 }}'  ,'SRID=2154; LINESTRINGZ(987700 6562600 9999, 987600 6562600 9999)'::geometry);
insert into mhnet.borda_headloss_link(name, law_type, param, geom) values('lmk20', 't_shape_junction', '{"t_shape_junction":{"up_geom":"computed", "down_geom":"computed", "up_cs_area":1.55, "down_cs_area":1.65, "up_cs_zinvert":48.23, "down_cs_zinvert":59.3 }}'  ,'SRID=2154; LINESTRINGZ(987700 6562700 9999, 987600 6562700 9999)'::geometry);


/*
******************************************************************************************************************************************************************************************************
    RACCORDEMENT ASSAINISSEMENT / RIVIERE
******************************************************************************************************************************************************************************************************
*/
/*
-- link pompe entre reseau assainissement et riviere
-- insert into mhnet.pump_link(name,npump,zregul_array,hq_array,geom) values('lpump1',2,'{{53.,54},{53.7,54.7}}','{{{5,0},{4,0.5},{2,1.7},{1,2},{0,3}},{{4,0},{3,0.5},{1,1.7},{0.5,2.2},{0,3}}}', 'SRID=2154; LINESTRINGZ(987830 6559690 9999, 988185 6559223.4 9999)'::geometry);
insert into mhnet.pump_link(name,npump,zregul_array,hq_array,geom) values('lpump1',2,'{{53.,54},{53.7,54.7}}','{{{5,0},{0,3}},{{4,0},{0,3.5}}}', 'SRID=2154; LINESTRINGZ(987830 6559690 9999, 988185 6559223.4 9999)'::geometry);
*/


/*
******************************************************************************************************************************************************************************************************
    RESEAU SECONDAIRE (HYDROLOGIE)
******************************************************************************************************************************************************************************************************
*/
-- hydrologie: noeuds
insert into mhnet.manhole_hydrology_node(name,area,z_ground,geom) values('nhydrol1',1,80,'SRID=2154; POINTZ(988700 6560700 9999)'::geometry);
insert into mhnet.manhole_hydrology_node(name,area,z_ground,geom) values('nhydrol2',1,80,'SRID=2154; POINTZ(988500 6560800 9999)'::geometry);
insert into mhnet.manhole_hydrology_node(name,area,z_ground,geom) values('nhydrol3',1,80,'SRID=2154; POINTZ(988300 6560900 9999)'::geometry);
insert into mhnet.manhole_hydrology_node(name,area,z_ground,geom) values('nhydrol4',1,80,'SRID=2154; POINTZ(988600 6560600 9999)'::geometry);
insert into mhnet.manhole_hydrology_node(name,area,z_ground,geom) values('nhydrol5',1,80,'SRID=2154; POINTZ(988400 6560400 9999)'::geometry);
insert into mhnet.manhole_hydrology_node(name,area,z_ground,geom) values('nhydrol11',1,80,'SRID=2154; POINTZ(988300 6561000 9999)'::geometry);
insert into mhnet.manhole_hydrology_node(name,area,z_ground,geom) values('nhydrol12',1,80,'SRID=2154; POINTZ(988300 6561100 9999)'::geometry);
insert into mhnet.manhole_hydrology_node(name,area,z_ground,geom) values('nhydrol13',1,80,'SRID=2154; POINTZ(988300 6561200 9999)'::geometry);
insert into mhnet.manhole_hydrology_node(name,area,z_ground,geom) values('nhydrol14',1,80,'SRID=2154; POINTZ(988300 6561300 9999)'::geometry);
insert into mhnet.manhole_hydrology_node(name,area,z_ground,geom) values('nhydrol15',1,80,'SRID=2154; POINTZ(988300 6561400 9999)'::geometry);
insert into mhnet.manhole_hydrology_node(name,area,z_ground,geom) values('nhydrol21',1,80,'SRID=2154; POINTZ(988400 6561000 9999)'::geometry);
insert into mhnet.manhole_hydrology_node(name,area,z_ground,geom) values('nhydrol22',1,80,'SRID=2154; POINTZ(988400 6561100 9999)'::geometry);
insert into mhnet.manhole_hydrology_node(name,area,z_ground,geom) values('nhydrol23',1,80,'SRID=2154; POINTZ(988400 6561200 9999)'::geometry);
insert into mhnet.manhole_hydrology_node(name,area,z_ground,geom) values('nhydrol24',1,80,'SRID=2154; POINTZ(988400 6561300 9999)'::geometry);
insert into mhnet.manhole_hydrology_node(name,area,z_ground,geom) values('nhydrol25',1,80,'SRID=2154; POINTZ(988400 6561400 9999)'::geometry);

-- hydrologie: BV
with c as (
    insert into mhnet.catchment(name, geom )
    values ('BV1', 'SRID=2154; POLYGON((988800 6560800, 989300 6561400, 990300 6560600, 988800 6560800))'::geometry)
    returning id)
insert into mhnet.catchment_node(name,area_ha,rl,slope,c_imp, netflow_type, constant_runoff, scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm,runoff_type,define_k_mn,q_limit,q0,contour,geom)
    values('BV1',23.5,800,0.015,0.50,
	'constant_runoff', 0.35, 160, 10, 55,
	'Desbordes 1 Cr', 5.07 * 23.5^(0.18) * (0.015 * 100)^(-0.36) * (1 + 0.50)^(-1.9) * 800^(0.15) * 30^(0.21) * 10^(-0.07),
	5.,0.2, (select c.id from c),'SRID=2154; POINTZ(989000 6560700 9999)'::geometry);

with c as (
    insert into mhnet.catchment(name, geom )
    values ('BV2', 'SRID=2154; POLYGON((988800 6559800, 989300 6560400, 990300 6559600, 988800 6559800))'::geometry)
    returning id)
insert into mhnet.catchment_node(name,area_ha,rl,slope,c_imp, netflow_type, scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm, runoff_type,socose_tc_mn,socose_shape_param_beta,q_limit,q0,contour,geom)
    values('BV2',33.5,900,0.015,0.50,
	'scs', 160, 10, 55,
	'Define Tc', 60, 2,
	5.,0.2, (select c.id from c), 'SRID=2154; POINTZ(989000 6559700 9999)'::geometry);

with c as (
    insert into mhnet.catchment(name, geom )
    values ('BV3', 'SRID=2154; POLYGON((988800 6558800, 989300 6559400, 990300 6558600, 988800 6558800))'::geometry)
    returning id)
insert into mhnet.catchment_node(name,area_ha,rl,slope,c_imp, netflow_type,horner_ini_loss_coef,horner_recharge_coef,runoff_type, define_k_mn, q_limit,q0,contour,geom)
    values('BV3',33.5,900,0.015,0.50,
	'horner',0.65, 0.135,
	'Define K', 60 * ( 0.4 * sqrt(33.5) + 0.0015 * 900 ) / ( 0.8 * sqrt(0.015 * 900) ), -- giandotti
	5.,0.2, (select id from c),'SRID=2154; POINTZ(989000 6558700 9999)'::geometry);

with c as (
    insert into mhnet.catchment(name, geom )
    values ('BV4', 'SRID=2154; POLYGON((989800 6558800, 990300 6559400, 991300 6558600, 989800 6558800))'::geometry)
    returning id)
insert into mhnet.catchment_node(name,area_ha,rl,slope,c_imp, netflow_type,holtan_sat_inf_rate_mmh,holtan_dry_inf_rate_mmh,holtan_soil_storage_cap_mm,runoff_type,define_k_mn, q_limit,q0,contour,geom)
    values('BV4',53.5,980,0.020,0.35,
	'holtan',0.54, 2, 2.5,
	'Define K', 0.14 * (53.5 * 980)^(1./3.) / sqrt(0.020), -- passini
	5.,0.2, (select id from c),'SRID=2154; POINTZ(990000 6558700 9999)'::geometry);

with c as (
    insert into mhnet.catchment(name, geom )
    values ('BV5', 'SRID=2154; POLYGON((989748 6558623,989758 6558623,989748 6558633,989748 6558623))'::geometry)
    returning id)
insert into mhnet.catchment_node(name,area_ha,rl,slope,c_imp, netflow_type,hydra_surface_soil_storage_rfu_mm,hydra_inf_rate_f0_mm_hr,hydra_int_soil_storage_j_mm,hydra_soil_drainage_time_qres_day,hydra_catchment_connect_coef,hydra_aquifer_infiltration_rate,hydra_split_coefficient, runoff_type,define_k_mn,q_limit,q0,contour,geom)
    values('BV5',23.5,800,0.015,0.50,
	'hydra',11, 22, 33, 0.5, 0.1, 0.2, 0.3,
	'Define K', 60,
	5.,0.2, (select id from c),'SRID=2154; POINTZ(989752 6558626 9999)'::geometry);

-- hydrologie: liaisons
insert into mhnet.routing_hydrology_link(name,cross_section,length,slope,geom) values('rout1',1.,10,0.01,'SRID=2154; LINESTRINGZ(989000 6560700 9999, 988700 6560700 9999)'::geometry);

with n as (select geom from mhriv.river_node where upper(name)='NODR1'),
h as (insert into mhnet.hydrograph_bc_singularity (geom, name, storage_area, constant_dry_flow, distrib_coef, lag_time_hr, external_file_data)    values ('SRID=2154; POINTZ(988100 6560900 9999)', 'hydrogramme1', 100, 0, 1, 10, true) returning id)
insert into mhnet.connector_hydrology_link(name,geom,hydrograph) select 'conh1','SRID=2154; LINESTRINGZ(988300 6560900 9999, 988100 6560900 9999)'::geometry, h.id from h;
/*
insert into mhnet.connector_hydrology_link(name,geom) values('conh2','SRID=2154; LINESTRINGZ(988500 6560800 9999, 988600 6560600 9999)'::geometry);
insert into mhnet.connector_hydrology_link(name,geom) values('conh14','SRID=2154; LINESTRINGZ(988400 6561300 9999, 988300 6561300 9999)'::geometry);
insert into mhnet.connector_hydrology_link(name,geom) values('conh13','SRID=2154; LINESTRINGZ(988400 6561200 9999, 988300 6561200 9999)'::geometry);
insert into mhnet.connector_hydrology_link(name,geom) values('conh12','SRID=2154; LINESTRINGZ(988400 6561100 9999, 988300 6561100 9999)'::geometry);
insert into mhnet.connector_hydrology_link(name,geom) values('conh15','SRID=2154; LINESTRINGZ(988400 6561400 9999, 988300 6561400 9999)'::geometry);
*/

insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'trh1', 75.70, 74.7, 'circular', 0, 75, 0.500,'SRID=2154; LINESTRINGZ(988700 6560700 9999, 988500 6560800 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'trh2', 75.70, 74.7, 'circular', 0, 75, 0.500,'SRID=2154; LINESTRINGZ(988500 6560800 9999, 988300 6560900 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'trh3', 75.70, 74.7, 'circular', 0, 75, 0.500,'SRID=2154; LINESTRINGZ(988600 6560600 9999, 988400 6560400 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'trh111', 76.00, 75.00, 'circular', 0, 75, 0.500,'SRID=2154; LINESTRINGZ(988300 6561000 9999, 988300 6560900 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'trh112', 77.00, 76.00, 'circular', 0, 75, 0.500,'SRID=2154; LINESTRINGZ(988300 6561100 9999, 988300 6561000 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'trh113', 78.00, 77.00, 'circular', 0, 75, 0.500,'SRID=2154; LINESTRINGZ(988300 6561200 9999, 988300 6561100 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'trh114', 79.00, 78.00, 'circular', 0, 75, 0.500,'SRID=2154; LINESTRINGZ(988300 6561300 9999, 988300 6561200 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'trh115', 80.00, 79.00, 'circular', 0, 75, 0.500,'SRID=2154; LINESTRINGZ(988300 6561400 9999, 988300 6561300 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'trh122', 78.00, 77.00, 'circular', 0, 75, 0.500,'SRID=2154; LINESTRINGZ(988400 6561100 9999, 988400 6561000 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'trh123', 79.00, 78.00, 'circular', 0, 75, 0.500,'SRID=2154; LINESTRINGZ(988400 6561200 9999, 988400 6561100 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'trh124', 80.00, 79.00, 'circular', 0, 75, 0.500,'SRID=2154; LINESTRINGZ(988400 6561300 9999, 988400 6561200 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'trh125', 81.00, 80.00, 'circular', 0, 75, 0.500,'SRID=2154; LINESTRINGZ(988400 6561400 9999, 988400 6561300 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'trh302', 74.70, 74.0, 'circular', 0, 75, 0.500,'SRID=2154; LINESTRINGZ(988500 6560800 9999, 988600 6560600 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'trh314', 80.00, 79.0, 'circular', 0, 75, 0.500,'SRID=2154; LINESTRINGZ(988400 6561300 9999, 988300 6561300 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'trh313', 79.00, 78.0, 'circular', 0, 75, 0.500,'SRID=2154; LINESTRINGZ(988400 6561200 9999, 988300 6561200 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'trh312', 78.00, 77.0, 'circular', 0, 75, 0.500,'SRID=2154; LINESTRINGZ(988400 6561100 9999, 988300 6561100 9999)'::geometry;
insert into mhnet.pipe_link (name, z_invert_up, z_invert_down, cross_section_type, h_sable, rk, circular_diameter, geom) select 'trh315', 81.00, 80.0, 'circular', 0, 75, 0.500,'SRID=2154; LINESTRINGZ(988400 6561400 9999, 988300 6561400 9999)'::geometry;

-- hydrologie: singularités
insert into mhnet.qq_split_hydrology_singularity(name,qq_array,downstream,split1,split2,geom)
    values ('dqh1','{{0,0},{3,1},{6,2}}',
        (select id from mhnet.pipe_link where upper(name)='TRH2'),(select id from mhnet.pipe_link where upper(name)='TRH302'),null,
        'SRID=2154; POINTZ(988500 6560800 9999)'::geometry);

insert into mhnet.hydrology_bc_singularity(name,geom) values ('clh1','SRID=2154; POINTZ(988400 6560400 9999)'::geometry);

insert into mhnet.zq_split_hydrology_singularity(name,downstream,split1,split2, downstream_law, downstream_param, split1_law, split1_param, split2_law, split2_param, geom)
    values ('dzq1',
        (select id from mhnet.pipe_link where upper(name)='TRH124'),(select id from mhnet.pipe_link where upper(name)='TRH314'),null,
		'gate', '{"gate":{"gate_cc":0.65, "gate_diameter":0.11, "gate_z_invert":80.01}}', 'weir', '{"weir":{"weir_cc":0.4, "weir_width":0.91, "weir_z_invert":80.51}}',  'weir', '{"weir":{"weir_cc":0.4, "weir_width":0.91, "weir_z_invert":80.51}}',
        'SRID=2154; POINTZ(988400 6561300 9999)'::geometry);

insert into mhnet.zq_split_hydrology_singularity(name,downstream,split1,split2, downstream_law, downstream_param, split1_law, split1_param, split2_law, split2_param, geom)
    values ('dzq2',
        (select id from mhnet.pipe_link where upper(name)='TRH123'),(select id from mhnet.pipe_link where upper(name)='TRH313'),null,
		'gate', '{"gate":{"gate_cc":0.62, "gate_diameter":0.15, "gate_z_invert":79.01}}', 'zq', '{"zq":{"zq_array":[[79.20,0.0],[79.25,0.11],[79.55,0.41]]}}',  'weir', '{"weir":{"weir_cc":0.36, "weir_width":0.95, "weir_z_invert":79.51}}',
        'SRID=2154; POINTZ(988400 6561200 9999)'::geometry);

insert into mhnet.reservoir_rs_hydrology_singularity(name, drainage, overflow, q_drainage, z_ini, zs_array, treatment_mode, treatment_param, geom)
    values ('rs1',
        (select id from mhnet.pipe_link where upper(name)='TRH122'),(select id from mhnet.pipe_link where upper(name)='TRH312'),
	    0.110, 78.05,
	    '{{78.00,100.},{78.25,110},{78.55,140} }', 'efficiency', '{"efficiency":{"ntk_efficiency_factor":40, "mes_efficiency_factor":10, "dco_efficiency_factor":30, "dbo5_efficiency_factor":20}}',
        'SRID=2154; POINTZ(988400 6561100 9999)'::geometry);

insert into mhnet.reservoir_rsp_hydrology_singularity(name, drainage, overflow, z_ini, zr_sr_qf_qs_array, treatment_mode, treatment_param, geom)
    values ('rsp1',
        (select id from mhnet.pipe_link where upper(name)='TRH125'),(select id from mhnet.pipe_link where upper(name)='TRH315'),
	    78.15,
	    '{{78.00,100.,0.010,0.100},{78.25,112,0.015,0.215},{78.55,140,0.030,0.320} }', 'residual_concentration', '{"residual_concentration":{"ntk_conc_mgl":40, "mes_conc_mgl":20, "dco_conc_mgl":30, "dbo5_conc_mgl":10}}',
        'SRID=2154; POINTZ(988400 6561400 9999)'::geometry);


/*
******************************************************************************************************************************************************************************************************
    DONNEES DE SCENARIO
******************************************************************************************************************************************************************************************************
*/

-- données de scénario. attention: il y a des références à des clés étrangères "id" en l'absence de champ "name"

insert into project.dry_inflow_hourly_modulation (hv_array) values ('{{0,1,0.25},{12,0.75,0.75},{24,1,0.5}}');
insert into project.dry_inflow_hourly_modulation (hv_array) values ('{{0,0,0},{12,1,0.5},{24,0.56,0.89}}');
insert into project.dry_inflow_sector (name, vol_curve, comment, geom) values ('secteur_tsec_1', 1, null, '01030000206A080000010000000500000087EDD1A36E1F2E419AABDD7EDF055941095052FC661D2E4130C89254F5045941024A0E931F262E41CBC3ACC8650459412B3ABA94C0272E4195CC78E08405594187EDD1A36E1F2E419AABDD7EDF055941');

insert into project.dry_inflow_scenario (name) values ('tempssec1');
insert into project.dry_inflow_scenario_sector_setting(dry_inflow_scenario, sector, volume_sewage_m3day, coef_volume_sewage_m3day, volume_clear_water_m3day, coef_volume_clear_water_m3day)
     values ((select id from project.dry_inflow_scenario where upper(name)='TEMPSSEC1'),
             (select id from project.dry_inflow_sector where upper(name)='SECTEUR_TSEC_1'),
             10., 0.5, 5., 0.400000006);
insert into project.coef_montana (return_period_yr, coef_a, coef_b) values (0, 1, -1);
insert into project.caquot_rainfall(montana) values ((select max(id) from project.coef_montana));

insert into project.rain_gage(name, comment, geom) values ('Pluviographe1', 'Pluviographe nord', 'SRID=2154; POINTZ(989000 6561000 9999)'::geometry);
insert into project.rain_gage(name, comment, geom) values ('Pluviographe2', 'Pluviographe sud' , 'SRID=2154; POINTZ(989000 6560000 9999)'::geometry);
insert into project.gage_rainfall(name, cbv_grid_connect_file, interpolation) values('Pluie_PR1', 'connfile1', 'shortest_distance');
insert into project.gage_rainfall(name,                        interpolation) values('Pluie_PR2',              'distance_ponderation');

with rf as (select id from project.gage_rainfall where upper(name)='PLUIE_PR1'),
     rg as (select id from project.rain_gage where upper(name)='PLUVIOGRAPHE1')
    insert into project.gage_rainfall_data(rainfall, rain_gage, is_active, t_mn_hcum_mm)    select rf.id, rg.id, true, '{{0,0}, {10.5,12.5}, {35,25} }' from rf,rg;
with rf as (select id from project.gage_rainfall where upper(name)='PLUIE_PR1'),
     rg as (select id from project.rain_gage where upper(name)='PLUVIOGRAPHE2')
    insert into project.gage_rainfall_data(rainfall, rain_gage, is_active, t_mn_hcum_mm)    select rf.id, rg.id, true, '{{0,0}, {10.5,13.5}, {33,23} }' from rf,rg;
with rf as (select id from project.gage_rainfall where upper(name)='PLUIE_PR2'),
     rg as (select id from project.rain_gage where upper(name)='PLUVIOGRAPHE2')
    insert into project.gage_rainfall_data(rainfall, rain_gage, is_active, t_mn_hcum_mm)    select rf.id, rg.id, false, '{{0,0}, {30.5,33.5}, {123,53} }' from rf,rg;
with rf as (select id from project.gage_rainfall where upper(name)='PLUIE_PR2'),
     rg as (select id from project.rain_gage where upper(name)='PLUVIOGRAPHE1')
    insert into project.gage_rainfall_data(rainfall, rain_gage, is_active, t_mn_hcum_mm)    select rf.id, rg.id, true, '{{0,0}, {31.5,31.5}, {121,51} }' from rf,rg;

insert into project.scenario (name, comment, dry_inflow, comput_mode, model_connect_settings, sediment_file,
    iflag_mes, iflag_dbo5, iflag_dco, iflag_ntk)
values ('scn1', 'scenario 1', 1, 'hydrology_and_hydraulics', 'global', 'fsediment.dat',
    true, false, true, true);

with stg as (select id from project.scenario where upper(name)='SCN1') insert into project.param_external_hydrograph (param_scenario,external_file) select stg.id, 'fhydrogrammes1.dat' from stg ;
with stg as (select id from project.scenario where upper(name)='SCN1') insert into project.param_external_hydrograph (param_scenario,external_file) select stg.id, 'fhydrogrammes2.dat' from stg ;
with stg as (select id from project.scenario where upper(name)='SCN1') insert into project.param_regulation (param_scenario,control_file) select stg.id, 'fregul1.dat' from stg ;
with stg as (select id from project.scenario where upper(name)='SCN1') insert into project.param_measure (param_scenario,measure_file) select stg.id,'fmesures.dat' from stg ;

insert into project.land_type(land_type, c_imp, netflow_type, c_runoff, curve, f0_mm_h, fc_mm_h, soil_cap_mm) values
                                ('water', 0, 'scs', null, 12.225, null, null, null),
                                ('buildings', 0.75, 'constant_runoff', 0.6, null, null, null, null),
                                ('forest', 0.2, 'holtan', null, null, 0.2, 0.3, 5);

