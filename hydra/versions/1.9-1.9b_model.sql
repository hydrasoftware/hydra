/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update $model.metadata set version = '1.9b';
;;

/* ********************************************** */
/*  option to flag coverage as protected or not   */
/* ********************************************** */

alter table ${model}.coverage_marker add column if not exists protected boolean default 'f';
;;

/* ********************************************** */
/*          Constrain 3D                          */
/* ********************************************** */
drop view ${model}.constrain cascade
;;

alter table ${model}._constrain
add points_xyz integer references project.points_type(id) on delete set null
;;

alter table ${model}._constrain
add points_xyz_proximity real not null default 1
;;

create or replace view ${model}.constrain as
select id, name, geom, discretized, elem_length, constrain_type, link_attributes, points_xyz, points_xyz_proximity
from ${model}._constrain
;;

create or replace view $model.close_point as
    with allpoints as (
        select id, (st_dumppoints(geom)).geom as geom from $model.constrain
    ),
    ends as (
        select id, st_startpoint(geom) as geom from $model.constrain
        union
        select id, st_endpoint(geom) as geom from $model.constrain
    ),
    cpoints as (
        select id, st_collect(geom) as geom from allpoints group by id
    ),
    clust as (
        select
          sqrt(ST_Area(ST_MinimumBoundingCircle(gc)) / pi()) as radius,
          ST_NumGeometries(gc) as nb,
          ST_Centroid(gc) as geom
        from (
          select unnest(ST_ClusterWithin(geom, 5)) gc
          from allpoints
        ) f
        union
        select
            1 as radius,
            0 as nb,
            st_force2d(e.geom) as geom
        from $model.constrain as c, cpoints as cp, ends as e
        where st_dwithin(c.geom, e.geom, 1)
        and not st_intersects(cp.geom, e.geom)
        and e.id != c.id
        and c.id = cp.id
    )
select row_number() over () AS id, radius, nb, geom::geometry('POINT', $srid) from clust where radius > 0
;;

create or replace view ${model}.constrain_points as(
with points_xyz as(
    select p.id,
    p.points_id,
    p.geom,
    p.name,
    p.z_ground,
    c.id as id_line
    from ${model}.constrain as c, project.points_xyz as p
    where ST_Distance(c.geom,p.geom) < c.points_xyz_proximity
    and c.points_xyz = p.points_id),
vertex as (
    select (ST_DumpPoints(C.geom)).geom as geom,
    c.id as id_line,
    (ST_DumpPoints(C.geom)).path[1] as id,
    St_LineLocatePoint(c.geom,(ST_DumpPoints(C.geom)).geom) as loc
    from ${model}.constrain as c),
small_dist as (
    select MIN(ST_distance(vertex.geom,points_xyz.geom)) as dist,
    vertex.id as id,
    vertex.id_line as id_line
    from points_xyz, vertex
    group by vertex.id, vertex.id_line),
points as (
    select points_xyz.id, St_LineInterpolatePoint(c.geom,St_LineLocatePoint(c.geom,points_xyz.geom)) as geom, points_xyz.z_ground, c.id as id_line, St_LineLocatePoint(c.geom,points_xyz.geom) as loc
    from ${model}.constrain as c, points_xyz
    where c.id = points_xyz.id_line
        UNION
    select vertex.id, vertex.geom as geom, points_xyz.z_ground, vertex.id_line as id_line, vertex.loc as loc
    from vertex, points_xyz, small_dist
    where vertex.id_line = points_xyz.id_line
    and small_dist.id = vertex.id
    and small_dist.id_line = vertex.id_line
    and small_dist.dist = ST_distance(vertex.geom,points_xyz.geom)),
ordered as (
    select *
    from points
    order by id_line,loc)
select ST_SetSRID(ST_MakeLine(St_MakePoint(St_X(pol.geom),St_Y(pol.geom),pol.z_ground))::geometry(Linestringz),$srid) as geom, id_line as id
from ordered as pol
group by id_line)
;;

drop trigger if exists ${model}_constrain_trig on ${model}.constrain
;;

drop function if exists $model.constrain_fct()

;;

create or replace function ${model}.constrain_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        prec_ real;
        snapped_ geometry;
        discretized_ geometry;
    begin
        if tg_op = 'INSERT' then
            select precision from hydra.metadata into prec_;
            select st_snaptogrid(new.geom, prec_) into snapped_;
            select st_snaptogrid(project.discretize_line(new.geom, coalesce(new.elem_length, 100)), prec_) into discretized_;
            insert into $model._constrain(name, geom, discretized, elem_length, constrain_type, link_attributes, points_xyz, points_xyz_proximity)
            values (coalesce(new.name, 'define_later'), snapped_, discretized_, coalesce(new.elem_length, 100), new.constrain_type, new.link_attributes, new.points_xyz, coalesce(new.points_xyz_proximity, 1))
            returning id, elem_length, points_xyz, points_xyz_proximity into new.id, new.elem_length, new.points_xyz, new.points_xyz_proximity ;
            update $model._constrain set name = 'CONSTR'||new.id::varchar where name = 'define_later' and id = new.id
            returning name into new.name;
            return new;
        elsif tg_op = 'UPDATE' then
            select precision from hydra.metadata into prec_;
            select st_snaptogrid(new.geom, prec_) into snapped_;
            select st_snaptogrid(project.discretize_line(new.geom, new.elem_length), prec_) into discretized_;
            update $model._constrain set name=new.name, geom=snapped_, discretized=discretized_,
                elem_length=new.elem_length, constrain_type=new.constrain_type, link_attributes=new.link_attributes,
                points_xyz=new.points_xyz, points_xyz_proximity=coalesce(new.points_xyz_proximity, 1)
                where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._constrain where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_constrain_trig
    instead of insert or update or delete on $model.constrain
       for each row execute procedure ${model}.constrain_fct()
;;

/* ********************************************** */
/*          Flood plain                           */
/* ********************************************** */

drop materialized view if exists ${model}.flood_plain cascade
;;

create materialized view ${model}.flood_plain as
    select row_number() over() as id, geom
    from (select (st_dump(st_union(geom))).geom as geom from ${model}.coverage where (domain_type='reach')) as t
    with no data
;;

create index ${model}_flood_plain_geom_idx on ${model}.flood_plain using gist(geom)
;;



drop view if exists ${model}.flood_plain_bluff cascade
;;

create view ${model}.flood_plain_bluff as
    with bluff as (
        select st_linemerge(st_difference(st_exteriorring(c.geom), st_collect(t.discretized))) as geom
        from ${model}.flood_plain c join ${model}.constrain t on st_intersects(c.geom, t.discretized)
        where t.constrain_type='flood_plain_transect'
        group by c.id, c.geom
    )
    select row_number() over() as id, geom from (select (st_dump(geom)).geom from bluff) as t
;;



drop view if exists ${model}.flood_plain_bluff_point cascade
;;

create view ${model}.flood_plain_bluff_point as
    select row_number() over() as id, (d).geom, (d).path, id as bluff
    from (select id, st_dumppoints(geom) as d, st_numpoints(geom) as n from ${model}.flood_plain_bluff) as t
    where (d).path[1] != 1 and (d).path[1] != n
;;



drop materialized view if exists ${model}.flood_plain_bluff_point_transect cascade
;;

create materialized view ${model}.flood_plain_bluff_point_transect as
    with fp as (
        select id, bluff, ${model}.interpolate_transect_at(geom) as geom, geom as pt from ${model}.flood_plain_bluff_point
    )
    select fp.id, fp.bluff, fp.geom,
    ${model}.valley_section_at(st_pointn(fp.geom, 2)) as section, case when st_intersects(fp.pt, st_startpoint(fp.geom)) then 'left' else 'right' end as side, r.id as reach, st_linelocatepoint(r.geom, st_pointn(fp.geom, 2)) as reach_rel_abs
    from fp join ${model}.reach r on st_dwithin(r.geom, st_pointn(fp.geom, 2), .1)
    where st_numpoints(fp.geom) = 3
    with no data
;;

/* ********************************************** */
/*          Flow lines                            */
/* ********************************************** */

drop materialized view if exists ${model}.flood_plain_flow_line cascade
;;

create materialized view ${model}.flood_plain_flow_line as
    with line as (
    select id, 1 as ord, st_makeline(array(
                select ${model}.section_interpolate_point(t.geom, t.section, .2,  t.side)
                from ${model}.flood_plain_bluff_point_transect as t where t.bluff=b.id
                order by st_linelocatepoint(b.geom,  case when t.side = 'left' then st_startpoint(t.geom) else st_endpoint(t.geom) end)
            )) as geom
    from ${model}.flood_plain_bluff as b
    union all
    select id, 2 as ord, st_makeline(array(
                select ${model}.section_interpolate_point(t.geom, t.section, .4,  t.side)
                from ${model}.flood_plain_bluff_point_transect as t where t.bluff=b.id
                order by st_linelocatepoint(b.geom,  case when t.side = 'left' then st_startpoint(t.geom) else st_endpoint(t.geom) end)
            )) as geom
    from ${model}.flood_plain_bluff as b
    union all
    select id, 3 as ord, st_makeline(array(
                select ${model}.section_interpolate_point(t.geom, t.section, .6,  t.side)
                from ${model}.flood_plain_bluff_point_transect as t where t.bluff=b.id
                order by st_linelocatepoint(b.geom,  case when t.side = 'left' then st_startpoint(t.geom) else st_endpoint(t.geom) end)
            )) as geom
    from ${model}.flood_plain_bluff as b
    union all
    select id, 4 as ord, st_makeline(array(
                select ${model}.section_interpolate_point(t.geom, t.section, .8,  t.side)
                from ${model}.flood_plain_bluff_point_transect as t where t.bluff=b.id
                order by st_linelocatepoint(b.geom,  case when t.side = 'left' then st_startpoint(t.geom) else st_endpoint(t.geom) end)
            )) as geom
    from ${model}.flood_plain_bluff as b
    ),
    segment as (
        select st_makeline(lag((pt).geom, 1, null) over (partition by id, ord order by id, (pt).path), (pt).geom) as geom
        from (select id, ord, ST_DumpPoints(geom) as pt from line) as dumps
    ),
    merged as (
        select st_linemerge(st_collect(s.geom))  as geom
        from segment s join ${model}.flood_plain p on st_covers(p.geom, s.geom)
        where not exists (select 1 from ${model}.reach as r where st_intersects(r.geom, s.geom))
    )
    select row_number() over() as id, geom from (select (st_dump(geom)).geom from merged) as t
    with no data
;;

create index ${model}_flood_plain_flow_line_geom_idx on ${model}.flood_plain_flow_line using gist(geom)
;;

/* ********************************************** */
/*          Coted points                          */
/* ********************************************** */

drop materialized view if exists ${model}.coted_point cascade
;;

create materialized view ${model}.coted_point as
    with transect_start as (
        select ${model}.section_interpolate_point(t.geom, t.section, 0, t.side) as geom
        from ${model}.flood_plain_bluff_point_transect as t
    ),
    contour_point as (
        select (st_dumppoints(st_exteriorring(geom))).geom from ${model}.flood_plain
    ),
    missing_tr as (
        select (st_dumppoints(st_difference(ct.geom, trc.geom))).geom from
        (select st_collect(geom) as geom from contour_point) as ct,
        (select st_collect(geom) as geom from transect_start) as trc
    ),
    pt as (
        select (st_dumppoints(geom)).geom as geom from ${model}.flood_plain_flow_line
        union all
        select geom from transect_start
        union all
        select ${model}.set_reach_point_altitude(geom) from missing_tr
    )
    select row_number() over() as id, geom from pt
    with no data
;;

create index ${model}_coted_point_geom_idx on ${model}.coted_point using gist(geom)
;;





drop materialized view if exists ${model}.coted_point_with_interp cascade
;;

create materialized view ${model}.coted_point_with_interp as
    with easy as (
        select distinct on (cp.id) cp.id, cp.geom, ft.reach, ft.reach_rel_abs
        from ${model}.coted_point as cp join ${model}.flood_plain_bluff_point_transect as ft on st_dwithin(cp.geom, ft.geom, .1)
    ),
    hard as (
        select id, geom from ${model}.coted_point
        except
        select id, geom from easy
    ),
    hard_section as (
        select id, geom, ${model}.interpolate_transect_at(geom) as transect
        from hard
    ),
    interp as (
        select id, geom, reach, reach_rel_abs from easy
        union all
        select distinct on (h.id) h.id, h.geom, r.id as reach, st_linelocatepoint(r.geom, st_closestpoint(r.geom, h.transect)) as reach_rel_abs
        from hard_section as h join ${model}.reach as r on st_intersects(r.geom, h.transect)
    ),
    reach_segment as (
        select n.reach, n.id as nd, st_linelocatepoint(r.geom, n.geom) as reach_rel_abs_nd,
        lag(n.id) over(w) as strt, lag(st_linelocatepoint(r.geom, n.geom)) over(w) as reach_rel_abs_strt,
        n.geom as nd_geom,
        lag(n.geom) over(w) as strt_geom
        from ${model}.river_node as n join ${model}.reach as r on r.id=n.reach
        window w as (partition by n.reach order by st_linelocatepoint(r.geom, n.geom))
    )
    select distinct on (i.id) i.id, i.geom, s.strt, s.nd, s.reach_rel_abs_strt, i.reach_rel_abs, s.reach_rel_abs_nd, st_makeline(array[s.strt_geom, i.geom, nd_geom]) as lnk, 1. - (i.reach_rel_abs - s.reach_rel_abs_strt)/(s.reach_rel_abs_nd - s.reach_rel_abs_strt) as weight_strt, (i.reach_rel_abs - s.reach_rel_abs_strt)/(s.reach_rel_abs_nd - s.reach_rel_abs_strt) as weight_nd
    from interp as i join reach_segment as s on (s.reach=i.reach and s.reach_rel_abs_strt <= i.reach_rel_abs and s.reach_rel_abs_nd >= i.reach_rel_abs)
    with no data
;;

create index ${model}_coted_point_with_interp_geom_idx on  ${model}.coted_point_with_interp using gist(geom)
;;

/* ********************************************** */
/*                     Mesh                       */
/* ********************************************** */

drop materialized view if exists ${model}.mesh_1d cascade
;;

create materialized view ${model}.mesh_1d as
    with blade as (
        select ST_Union(ST_Buffer(geom, 0.001, 'endcap=square')) as geom
        from ${model}.flood_plain_flow_line
    ),
    polyg as (
        select ST_Force2D(ST_Difference(p.geom, b.geom)) as geom
        from ${model}.flood_plain as p, blade as b
    ),
    triang as (
        select (ST_Dump(ST_Tesselate(geom))).geom from polyg
    )
    select row_number() over() as id, geom from triang
    with no data
;;

create index  ${model}_mesh_1d_geom_idx on  ${model}.mesh_1d using gist(geom)
;;



drop materialized view if exists ${model}.mesh_1d_coted cascade
;;

create materialized view ${model}.mesh_1d_coted as
    select row_number() over() as id,
    a.id as a, a.strt as a_strt, a.weight_strt as a_strt_w, a.nd as a_nd, a.weight_nd as a_nd_w,
    b.id as b, b.strt as b_strt, b.weight_strt as b_strt_w, b.nd as b_nd, b.weight_nd as b_nd_w,
    c.id as c, c.strt as c_strt, c.weight_strt as c_strt_w, c.nd as c_nd, c.weight_nd as c_nd_w,
    (st_z(a.geom) + st_z(b.geom) + st_z(c.geom))/3. z_mean,
    st_z(a.geom) as z_a, st_z(b.geom) as z_b, st_z(c.geom) as z_c,
    st_makepolygon(st_makeline(array[a.geom, b.geom, c.geom, a.geom])) as geom,
    st_makeline(array[st_startpoint(a.lnk), st_centroid(st_makepolygon(st_makeline(array[a.geom, b.geom, c.geom, a.geom]))), st_endpoint(a.lnk)]) as lnk
    from ${model}.mesh_1d as e
    join ${model}.coted_point_with_interp as a on st_dwithin(st_pointn(st_exteriorring(e.geom), 1), a.geom, .1)
    join ${model}.coted_point_with_interp as b on st_dwithin(st_pointn(st_exteriorring(e.geom), 2), b.geom, .1)
    join ${model}.coted_point_with_interp as c on st_dwithin(st_pointn(st_exteriorring(e.geom), 3), c.geom, .1)
    where a.id!=b.id and b.id!= c.id and c.id!=a.id
    with no data
;;



create or replace function ${model}.mesh_all_1d()
returns integer
language plpgsql
volatile
as
$$$$
declare
    _res integer;
begin
    refresh materialized view ${model}.flood_plain ;
    refresh materialized view ${model}.flood_plain_bluff_point_transect;
    refresh materialized view ${model}.flood_plain_flow_line;
    refresh materialized view ${model}.coted_point;
    refresh materialized view ${model}.coted_point_with_interp;
    refresh materialized view ${model}.mesh_1d;
    refresh materialized view ${model}.mesh_1d_coted;
    select count(*) from ${model}.mesh_1d_coted into _res;
    return _res;
end;
$$$$
;;

