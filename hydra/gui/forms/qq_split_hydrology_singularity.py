# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_singularity_dialog import SimpleSingularityDialog, tr
from hydra.utility.settings_properties import SettingsProperties
from ..widgets.array_widget import ArrayWidget
from hydra.utility.log import LogManager, ConsoleLogger
from qgis.PyQt import uic
import os

class QqSplitHydrologySingularityEditor(SimpleSingularityDialog):
    def __init__(self, project, geom, id=None, parent=None, iface=None, size=10):
        SimpleSingularityDialog.__init__(self, project, geom, "qq_split_hydrology_singularity", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "qq_split_hydrology_singularity.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.dir = current_dir
        self.__size = size
        self.__iface = iface
        self.__log_manager = LogManager(ConsoleLogger(), "Hydra")

        self.btn_downstream.clicked.connect(self.__select_downstream)
        self.btn_split1.clicked.connect(self.__select_split1)
        self.btn_split2.clicked.connect(self.__select_split2)
        self.check_split2.clicked.connect(self.set_check_split2)

        if iface==None:
            self.btn_downstream.setEnabled(False)
            self.btn_split1.setEnabled(False)
            self.btn_split2.setEnabled(False)

        self.node, name, qq_array, downstream, downstream_type, split1, split1_type, split2, split2_type, comment = self.project.execute("""
            select node, name, qq_array, downstream, downstream_type, split1, split1_type, split2, split2_type, comment
            from {}.qq_split_hydrology_singularity
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.name.setText(name)
        self.notes.setText(name)
        # downstream
        if downstream:
            downstream_name, = self.project.execute("""select name from {}.{}_link where id={}""".format(self.model.name, downstream_type, downstream)).fetchone()
            self.downstream.setText(downstream_name)
        # split1
        if split1:
            split1_name, = self.project.execute("""select name from {}.{}_link where id={}""".format(self.model.name, split1_type, split1)).fetchone()
            self.split1.setText(split1_name)
        # split2
        if split2:
            split2_name, = self.project.execute("""select name from {}.{}_link where id={}""".format(self.model.name, split2_type, split2)).fetchone()
            self.split2.setText(split2_name)
            self.check_split2.setChecked(True)
        else :
            self.check_split2.setChecked(False)

        if qq_array:
            for i in range(len(qq_array)):
                if len(qq_array[i])==2:
                    qq_array[i].append(0)
        self.array = ArrayWidget(["Q upstream (m3/s)", "Q split 1 (m3/s)", "Q split 2 (m3/s)"], [10,3], qq_array,
                draw_graph_func=self.__draw_graph, help_func=None, parent=self.array_placeholder)

        self.set_check_split2()

        self.show()

    def __draw_graph(self, datas):
        #no graph here (3 columns table)
        pass

    def __select_downstream(self):
        self.__select_link('downstream')

    def __select_split1(self):
        self.__select_link('split1')

    def __select_split2(self):
        self.__select_link('split2')

    def __select_link(self, field):
        from hydra.utility.item_selector import ItemSelector
        i_selector = ItemSelector(self.project, self.__iface)
        self.hide()

        i_selector.select(['pipe_link', 'connector_hydrology_link'])
        name, id, table = i_selector.get_selected_item_infos()

        if name:
            getattr(self, field).setText(name)
        self.stash()
        QqSplitHydrologySingularityEditor(self.project, None, self.id, None, self.__iface, self.__size).exec_()
        self.ignore_rollback = True
        self.close()

    def set_check_split2(self):
        if self.check_split2.isChecked():
            self.split2.setEnabled(True)
            self.btn_split2.setEnabled(True)
            self.array.table.setColumnHidden(2, False)
            self.array.table.resizeColumnsToContents()
            self.array.table.horizontalHeader().setStretchLastSection(True);
        else :
            self.split2.setEnabled(False)
            self.btn_split2.setEnabled(False)
            self.array.table.setColumnHidden(2, True)
            self.array.table.resizeColumnsToContents()
            self.array.table.horizontalHeader().setStretchLastSection(True);

    def get_id(self):
        return self.id

    def stash(self):
        # qq_array
        qq_array = self.array.get_table_items()
        if not (self.check_split2.isChecked() and self.split2.text()):
            for i in range(len(qq_array)):
                qq_array[i]=qq_array[i][0:-1]

        # downstream / splits
        downstream = None
        split1 = None
        split2 = None
        if self.downstream.text():
            downstream, = self.project.execute("""select id from {}._link where name='{}'""".format(self.model.name, self.downstream.text())).fetchone()
        if self.split1.text():
            split1, = self.project.execute("""select id from {}._link where name='{}'""".format(self.model.name, self.split1.text())).fetchone()
        if self.split2.text() and self.check_split2.isChecked():
            split2, = self.project.execute("""select id from {}._link where name='{}'""".format(self.model.name, self.split2.text())).fetchone()

        self.model.update_singularity_dqh(self.id, self.geom, qq_array,
                                                downstream, split1, split2, self.name.text())

        if not self.check_split2.isChecked():
            self.project.execute("""update {}.qq_split_hydrology_singularity set split2=null, split2_type=null where id={}""".format(self.model.name, self.id))

        self.project.execute("""update {}.qq_split_hydrology_singularity set comment=%s where id={}""".format(self.model.name, self.id), (self.notes.text(),))

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.stash()
        SimpleSingularityDialog.save(self)
        self.close()






