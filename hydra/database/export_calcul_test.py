# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
USAGE
   python -m hydra.database.export_calcul_test [-hk]

OPTIONS

   -h, --help
        print this help

   -k, --keep
        does not delete DB at the end
"""

import os, re
import sys
import shutil
import logging
import tempfile
import subprocess
import getopt
import difflib
import shlex
import psycopg2
from subprocess import Popen, PIPE
from hydra.project import Project
from hydra.database.database import TestProject, project_exists, remove_project
from hydra.database.export_calcul import ExportCalcul
from hydra.database.export_carto_data import ExportCartoData
from hydra.utility.settings_properties import SettingsProperties

DEBUG = True

def read(filename):
    with open(filename) as f:
        return f.read()

def readlines(filename):
    with open(filename) as f:
        return f.readlines()


hydra_dir = os.path.join(os.path.expanduser("~"), ".hydra")

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hk",
            ["help", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

keep = True if "-k" in optlist or "--keep" in optlist else False

if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

tesdb = "export_calcul_test"

if project_exists(tesdb):
    remove_project(tesdb)
if os.path.exists(os.path.join(hydra_dir, tesdb)):
    shutil.rmtree(os.path.join(hydra_dir, tesdb))

test_project = TestProject(tesdb, keep)
obj_project = Project.load_project(tesdb)
obj_project.add_new_model('mhriv')
obj_project.add_new_model('mhnet')

current_dir = os.path.abspath(os.path.dirname(__file__))
test_data = os.path.join(current_dir, "test_data")


test_output = os.path.join(tempfile.gettempdir(), "export_calcul_test.out")
sql_sript = os.path.join(test_data, 'projettest1_insert.sql')
cmd = ["psql", "-tXA",'-f', sql_sript, f"service={SettingsProperties.get_service()} dbname={tesdb}"]

if DEBUG:
    obj_project.log.notice(' '.join(cmd))

with open(test_output, "w") as o:
    out, err = Popen(cmd, stdout=o, stderr=PIPE).communicate()
    if err :
        obj_project.log.error(err.decode('utf8', errors='replace') + "\nerror while running SQL command from {}".format(sql_sript))
        exit(1)

ref_output = os.path.join(test_data, 'projettest1_insert.sql.out')

with open(test_output) as t, open(ref_output) as r:
    if t.read().replace('\n','').replace('\nt','') != r.read().replace('\n','').replace('\nt',''):
        obj_project.log.error(f"diff found between {ref_output} and {test_output}")
        exit(1)
os.remove(test_output)

test_output_dir = os.path.join(tempfile.gettempdir(), 'export_calcul_test')
if os.path.isdir(test_output_dir):
    shutil.rmtree(test_output_dir)
os.mkdir(test_output_dir)

id_scenario=1

obj_project.execute("update mhnet.manhole_node set z_ground=z_ground+10")
obj_project.commit()

# export data for computing (for hydra)
exporter = ExportCalcul(obj_project)
exporter = ExportCalcul(obj_project)
exporter.export(id_scenario)

# export cartographic data (for triangulz.exe)
exporter = ExportCartoData(obj_project)
exporter.export(id_scenario)

scenario_dir = obj_project.get_senario_path(id_scenario)
workdir="travail"    #    "simulation"
#--- compare files - 1st: "scenario.nom"
test_output_work = scenario_dir
test_ref_dir = os.path.join(test_data, 'projettest1_export')
test_output, ref_output =  os.path.join(test_output_work, "scenario.nom"), os.path.join(test_ref_dir, "scenario.nom")
if read(test_output).replace('\n','').replace('\nt','') != read(ref_output).replace('\n','').replace('\nt',''):
    ref = [line for line in readlines(ref_output) if not re.match(r'^(=+ +)+=+$', line)]
    gen = [line for line in readlines(test_output) if not re.match(r'^(=+ +)+=+$', line)]
    diff = difflib.unified_diff(ref, gen, fromfile=ref_output, tofile=test_output)
    nodiff = True
    diff_lines = ''
    for line in diff:
        diff_lines += line + '\n'
        nodiff &= False
    if not nodiff:
        obj_project.log.error(f"found diff {ref_output} {test_output}:\n{diff_lines}")
        exit(1)

#--- compare files - 2nd: other files (subdirectory workdir)
test_output_work = os.path.join(scenario_dir, workdir)

error = False
test_ref_dir = os.path.join(test_data, 'projettest1_export', "simulation")
for outfile in os.listdir(test_ref_dir):
    if outfile[-4:] in ['.li2', '.li1']: continue
    test_output, ref_output =  os.path.join(test_output_work, outfile), os.path.join(test_ref_dir, outfile)
    if read(test_output).replace('\n','').replace('\nt','') != read(ref_output).replace('\n','').replace('\nt',''):
        if outfile[-8:] not in ['ctrz.dat', 'geom.dat']:
            ref = [line for line in readlines(ref_output) if not re.match(r'^(=+ +)+=+$', line)]
            gen = [line for line in readlines(test_output) if not re.match(r'^(=+ +)+=+$', line)]
            if outfile[-11:] == "_optsor.dat" and os.name !="nt":
                for k in range(len(ref)): ref[k] = ref[k].replace("\\", "/")
            diff = difflib.unified_diff(ref, gen, fromfile=ref_output, tofile=test_output)
            nodiff = True
            diff_lines = ''
            for line in diff:
                diff_lines += line + '\n'
                nodiff &= False
            if not nodiff:
                obj_project.log.error(f"found diff {ref_output} {test_output}:\n{diff_lines}")
                error = True
        else:
            ref = set(sorted([line.replace('\n', '') for line in readlines(ref_output) if not re.match(r'^(=+ +)+=+$', line)]))
            gen = set(sorted([line.replace('\n', '') for line in readlines(test_output) if not re.match(r'^(=+ +)+=+$', line)]))
            if ref != gen:
                obj_project.log.error(f"found diff {ref_output} {test_output}:\n{ref-gen}")
                error = True

if error:
    exit(1)

if not keep:
    shutil.rmtree(test_output_dir)

sys.stdout.write(" ok\n")
