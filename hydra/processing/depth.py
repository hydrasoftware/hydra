from pathlib import Path
from multiprocessing import cpu_count
import shutil

from qgis.PyQt.QtCore import QCoreApplication
from qgis.utils import plugins as qgis_plugins
import processing
from qgis.core import (
    QgsProject,
    QgsRasterLayer,
    QgsProcessing,
    QgsProcessingException,
    QgsProcessingAlgorithm,
    QgsProcessingParameterRasterDestination,
    QgsProcessingParameterRasterLayer,
    QgsProcessingParameterNumber,
    QgsProcessingParameterEnum,
    QgsProcessingParameterString,
    QgsProcessingMultiStepFeedback
)
from .crgeng import run_crgeng
from .utility import ProjectWidget, ScenarioWidget
from ..project import Project


QML_DIR = Path(__file__).parent.parent / "ressources" / "qml"


CONTOURZ_CTL_CONTENT = """*WRKSPACE
{workspace}

*GRID_VRT
{terrain}

*MODE CONTOURZ
1
{project_name} {model} {scenario} {time_step}
1 0 1

*OPTION_RES
LAMB93
NVZ_ASC

*OPTION_ZF2

"""


class DepthRaster(QgsProcessingAlgorithm):
    """
    Algorithm creating a Depth raster from terrain and crgeng processing
    """

    INPUT = "INPUT"
    OUTPUT = "OUTPUT"
    PROJECT = "PROJECT"
    SCENARIO = "SCENARIO"
    TIME_STEP = 'TIME_STEP'

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return DepthRaster()

    def name(self):
        return "depth_raster"

    def displayName(self):
        return self.tr("crgeng Depth raster")

    def group(self):
        return self.tr("River and free surface flow mapping")

    def groupId(self):
        return "surfaceflowmapping"

    def shortHelpString(self):
        return self.tr("Depth Raster Output")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT,
                self.tr("Terrain Layer"),
                [QgsProcessing.TypeRaster],
            )
        )

        param = QgsProcessingParameterString(self.PROJECT, "Project")
        param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.SCENARIO, "Scenario")
        param.setMetadata({ 'widget_wrapper': {'class': ScenarioWidget}})
        self.addParameter(param)

        self.addParameter(
            QgsProcessingParameterNumber(
                self.TIME_STEP, "Time step",
                QgsProcessingParameterNumber.Double, defaultValue=-999.0
            )
        )

        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.OUTPUT, "Output depth raster"
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        self.project = Project(self.parameterAsString(parameters, self.PROJECT, context))
        self.srid = self.project.srid
        self.project_name = self.project.name
        self.workspace_dir = Path(self.project.directory).parent
        self.carto_dir = self.project.carto_dir
        scenario = self.parameterAsString(parameters, self.SCENARIO, context)

        multifb = QgsProcessingMultiStepFeedback(4, feedback)

        input = self.parameterAsRasterLayer(parameters, self.INPUT, context)
        self.output = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        time_step = self.parameterAsDouble(parameters, self.TIME_STEP, context)

        depth_folder = Path(self.carto_dir) / 'depth'
        depth_folder.mkdir(parents=True, exist_ok=True)

        models = self.project.get_models_from_scenario(scenario)

        if not models:
            raise QgsProcessingException(
                "depth: No models associated with current scenario, please check scenario's configuration"
            )

        # use to store temp files to be removed after processing
        self.tmpfiles = []
        # depth_merged = tif output files for each model
        depth_merged = []

        ctl_file_path = depth_folder / "nvz_asc.ctl"

        for model in models:
            feedback.pushInfo(f"Writing control file to {ctl_file_path}")
            # multifb.setCurrentStep(1)

            with ctl_file_path.open("wt", encoding="utf-8") as ctl_file:
                ctl_file.write(
                    CONTOURZ_CTL_CONTENT.format(
                        workspace=self.workspace_dir,
                        terrain=str(Path(input.source())),
                        project_name=self.project_name,
                        model=model.upper(),
                        scenario=scenario.upper(),
                        time_step=time_step,
                    )
                )

            run_crgeng([str(ctl_file_path)], multifb, depth_folder)

            asc_rasters = list(Path(self.carto_dir).glob(f"{scenario.upper()}_{model.upper()}_{time_step}_nvz*.asc"))
            self.tmpfiles.extend(asc_rasters)

            current_merged = Path(self.carto_dir) / f"{scenario.upper()}_{model.upper()}_{time_step}_nvz_merged.tif"
            depth_merged.append(current_merged)

            # https://docs.qgis.org/3.22/en/docs/user_manual/processing_algs/gdal/rastermiscellaneous.html?highlight=buildvirtualraster#id47
            feedback.pushInfo("merging source files")
            # multifb.setCurrentStep(2)

            processing.run(
                "gdal:merge",
                {
                    "INPUT": [str(p) for p in asc_rasters],
                    "OUTPUT": str(current_merged),
                    "NODATA_INPUT": -999,  # don't treat this bad nodata value
                    "NODATA_OUTPUT": -9999,
                    "OPTIONS": f"PREDICTOR=3|COMPRESS=DEFLATE|NUM_THREADS={cpu_count()}",
                    "EXTRA": f"-ps {input.rasterUnitsPerPixelX()} {input.rasterUnitsPerPixelY()}"
                },
                feedback=multifb
            )

            merged_layer = QgsRasterLayer(str(current_merged), current_merged.name)
            feedback.pushInfo(f"clipping DEM to: {merged_layer.extent()}")
            # multifb.setCurrentStep(3)
            mnt_clipped = str(depth_folder / 'mnt_clipped.tif')

            processing.run(
                "gdal:cliprasterbyextent",
                {
                    "INPUT": input,
                    "PROJWIN": merged_layer.extent(),
                    "OUTPUT": mnt_clipped,
                    "NODATA": -9999,
                    "EXTRA": f"-projwin_srs EPSG:{self.srid}"
                },
                feedback=multifb
            )

            feedback.pushInfo("Computing water depth using absolute value and DEM")
            # multifb.setCurrentStep(4)
            processing.run(
                "gdal:rastercalculator",
                {
                    "INPUT_A": str(current_merged),
                    "INPUT_B": mnt_clipped,
                    "BAND_A": 1,
                    "BAND_B": 1,
                    "NO_DATA": -9999,
                    # https://docs.qgis.org/3.22/en/docs/user_manual/processing_algs/gdal/rastermiscellaneous.html?highlight=buildvirtualraster#raster-calculator
                    "RTYPE": 5,  # float 32
                    "ASSIGN_CRS": f"EPSG:{self.srid}",
                    "PROJWIN": merged_layer.extent().intersect(input.extent()),
                    "OUTPUT": str(current_merged.with_suffix(".norm.tif")),
                    "FORMULA": "B*less(A,0)+A*greater(A,0)-B",
                    "OPTIONS": f"PREDICTOR=3|COMPRESS=DEFLATE|NUM_THREADS={cpu_count()}"
                },
                feedback=multifb
            )
        # assembling all depth raster to a VRT
        processing.run(
            "gdal:buildvirtualraster",
            {
                "INPUT": [str(v.with_suffix(".norm.tif")) for v in depth_merged],
                "ASSIGN_CRS": f"EPSG:{self.srid}",
                "OUTPUT": self.output,
                "SEPARATE": False,
            },
        )
        # prepare style for output layer
        shutil.copy(QML_DIR / "raster_hauteurs.qml", Path(self.output).with_suffix('.qml'))

        self.tmpfiles.extend(depth_merged)

        return {self.OUTPUT: self.output}

    def postProcessAlgorithm(self, context, feedback):
        """
        https://github.com/qgis/QGIS/blob/5f2326ac2f53cbb116b8fdce055e91cf000c8ed2/python/core/auto_generated/processing/qgsprocessingalgorithm.sip.in#L627
        """
        layer = QgsRasterLayer(self.output, Path(self.output).name)
        QgsProject.instance().addMapLayer(layer)

        # cleanup temp files
        for draster in self.tmpfiles:
            try:
                draster.unlink()
            except PermissionError as err:
                feedback.pushWarning(f'source raster not deleted, reason: {err}')

        # cleanup gdal metadata if exists
        for xml_metadata in Path(self.carto_dir).glob("*nvz*.asc.aux.xml"):
            xml_metadata.unlink()

        return {self.OUTPUT: self.output}
