# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from qgis.PyQt.QtWidgets import QWidget
from qgis.PyQt.QtCore import pyqtSignal
from qgis.PyQt import uic
import os
from shapely import wkb
from math import cos, sin, pi

from .geometry import Geometry

class OvoidSection(QWidget):

    geometry_changed = pyqtSignal(dict)
    transect_changed = pyqtSignal(list)
    section_changed = pyqtSignal(dict)
    new_geometry = pyqtSignal()

    def __init__(self, z_invert, rk, top_diameter, invert_diameter, height, transect, terrain, parent=None):
        QWidget.__init__(self, parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "ovoid_section.ui"), self)

        self.__geometry = None

        self.z_invert.setValue(z_invert if z_invert is not None else -999)
        self.rk.setValue(rk if rk is not None else  0)
        self.top_diameter.setValue(top_diameter if top_diameter is not None else 0)
        self.invert_diameter.setValue(invert_diameter if invert_diameter is not None else 0)
        self.height.setValue(height if height is not None else 0)


        self.z_invert.valueChanged.connect(self.__section_parameters_changed)
        self.rk.valueChanged.connect(self.__section_parameters_changed)
        self.top_diameter.valueChanged.connect(self.__section_parameters_changed)
        self.invert_diameter.valueChanged.connect(self.__section_parameters_changed)
        self.height.valueChanged.connect(self.__section_parameters_changed)

        self.__transect = wkb.loads(transect, True) if transect is not None else None
        self.__terrain = terrain

        self.__set_geometry()


    def __getattr__(self, name):
        if name == 'geom':
            return self.__geometry
        elif name == 'param':
            return {
            "z_invert": self.z_invert.value(),
            "rk": self.rk.value(),
            "ovoid_top_diameter": self.top_diameter.value(),
            "ovoid_invert_diameter": self.invert_diameter.value(),
            "ovoid_height": self.height.value()
            }
        else:
            raise AttributeError(name)

    def __set_geometry(self):
        top_diameter = self.top_diameter.value()
        invert_diameter = self.invert_diameter.value()
        height = self.height.value()
        dz = self.z_invert.value()
        points = [
            [.5*top_diameter*cos(pi*t/15),  .5*top_diameter*(sin(pi*t/15) - 1) + height]
            for t in range(15+1)] + [
            [-.5*invert_diameter*cos(pi*t/15),  .5*invert_diameter*(-sin(pi*t/15) + 1)]
            for t in range(15+1)]
        points.append(points[0])

        self.__geometry = Geometry(
            points,
            dz,
            self.__transect,
            self.__terrain)

    def __section_parameters_changed(self):
        self.__set_geometry()
        self.section_changed.emit(self.param)
