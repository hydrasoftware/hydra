from builtins import str
from builtins import range
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QTableWidgetItem, QMessageBox, QHBoxLayout, QSplitter
from qgis.PyQt.QtCore import Qt
from hydra.gui.base_dialog import BaseDialog, tr
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from hydra.gui.widgets.graph_widget import GraphWidget
from hydra.gui.widgets.valley_geom_widget import ValleyGeomWidget
from hydra.gui.widgets.valley_geom_widget import dico_code_error as dico_error
from hydra.gui.widgets.channel_geom_widget import ChannelGeomWidget
from hydra.gui.widgets.pipe_geom_widget import PipeGeomWidget
import hydra.utility.string as string

_table_geom = {'circular': None,
               'ovoid': None,
               'pipe': 'closed_parametric_geometry',
               'channel': 'open_parametric_geometry',
               'valley': 'valley_cross_section_geometry'}

class GeometrySelector(BaseDialog):
    def __init__(self, project, cross_section_type=None, geometry_id=None, parent=None):
        assert cross_section_type is None or cross_section_type in list(_table_geom.keys())

        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        self.project = project
        self.model = self.project.get_current_model()
        uic.loadUi(os.path.join(current_dir, "geometry_selector.ui"), self)

        self.validated = False

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.table_geom = None

        res_cs = self.project.execute("""select id, name, abbreviation from hydra.cross_section_type order by id desc""").fetchall()
        for i in range(0, len(res_cs)):
            self.cross_section_type.addItem(str(res_cs[i][1]))
            if cross_section_type==str(res_cs[i][1]):
                self.cross_section_type.setCurrentIndex(i)

        self.cross_section_type.activated.connect(self.__cross_section_type_changed)
        self.__cross_section_type_changed()

        if self.table_geom:
            self.__select_geometry_by_id(geometry_id)

    def __cross_section_type_changed(self):
        '''Called when the cross_section_type combobox value is changed'''
        if self.table_geom:
            self.table_geom.hide()

        if self.cross_section_type.currentIndex()>=0 and _table_geom[self.cross_section_type.currentText()] is not None:
            if not self.table_geom:
                self.table_geom = HydraTableWidget(self.project, ("id", "name"),(tr("Id"), tr("Name")),
                                self.model.name, _table_geom[self.cross_section_type.currentText()],
                                (self.new_geom, self.delete_geom), "", "id", self.widget_geometry_placeholder)
                self.table_geom.table.setSortingEnabled(True)
                self.table_geom.table.selectRow(0)
            else:
                self.table_geom.set_tablename(_table_geom[self.cross_section_type.currentText()])
            self.table_geom.show()

    def __select_geometry_by_id(self, geom_id):
        '''Selects a row in geometry table based on id value to select'''
        for i in range(0, self.table_geom.table.rowCount()):
            if self.table_geom.table.item(i,0).text()==str(geom_id):
                self.table_geom.table.setCurrentCell(i,0)

    def new_geom(self):
        '''Creates a new geometry'''
        if self.cross_section_type.currentText() in ['pipe', 'channel']:
            values_list = ['default', string.list_to_sql_array([[0,0]])]
            columns_list = ['zbmin_array']
        elif self.cross_section_type.currentText() == 'valley':
            values_list = ['default', string.list_to_sql_array([[0,0]]), string.list_to_sql_array([[0,0]]), string.list_to_sql_array([[0,0]])]
            columns_list = ['zbmin_array','zbmaj_lbank_array','zbmaj_rbank_array']
        else:
            return
        self.table_geom.add_row(values_list, columns_list)

    def delete_geom(self):
        '''Deletes selected geometry'''
        self.table_geom.del_selected_row()

    def get_selected_cross_section_type(self):
        '''Returns cross_section_type that is selected'''
        if self.validated:
            return self.cross_section_type.currentText()
        else:
            return None

    def get_selected_geometry(self):
        '''Returns geometry_id that is selected'''
        if self.validated and self.get_selected_cross_section_type() in ['pipe', 'channel', 'valley'] and self.table_geom:
            return string.get_sql_int(self.table_geom.table.selectedItems()[0].text())
        else:
            return None

    def save(self):
        '''Save changes'''
        self.validated = True
        self.project.log.notice(tr("Saved"))
        BaseDialog.save(self)
        self.close()