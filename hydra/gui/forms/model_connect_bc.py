# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from qgis.PyQt import uic
from hydra.gui.widgets.graph_widget import GraphWidget
from hydra.gui.widgets.array_widget import ArrayWidget
from hydra.gui.widgets.combo_with_values import ComboWithValues
from hydra.gui.simple_singularity_dialog import SimpleSingularityDialog, tr
import hydra.utility.string as string

quality_items = ['dbo5_mgl', 'nh4_mgl', 'no3_mgl', 'o2_mgl', 'ecoli_npp100ml', 'enti_npp100ml', 'ad3_npp100ml', 'ad4_npp100ml', 'mes_mgl']

class ModelConnectBcEditor(SimpleSingularityDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleSingularityDialog.__init__(self, project, geom, "model_connect_bc_singularity", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "model_connect_bc.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.cascade_values = self.project.execute("""
            select id, description, name
            from hydra.model_connect_mode""").fetchall()

        name, cascade_mode, self.zq_array, self.tz_array, quality, comment = self.project.execute("""
            select name, cascade_mode, zq_array, tz_array, quality, comment
            from {}.model_connect_bc_singularity
            where id={}""".format(self.model.name, self.id)).fetchone()
        self.name.setText(name)

        # initialize cascade mode
        self.__last_cascade = ''

        self.cascade_mode = ComboWithValues(self.cascade_mode_placeholder)
        self.cascade_mode.activated.connect(self.__cascadechanged)
        for id, description, name in self.cascade_values:
            self.cascade_mode.addItem(description, id)
            if cascade_mode == name:
                self.cascade_mode.set_selected_value(id)

        self.graph = GraphWidget(self.graph_placeholder)
        self.array = ArrayWidget(["Elevation (m)", "Discharge (m3/s)"], [100,2], None,
                draw_graph_func=self.__draw_graph, help_func=None, parent=self.array_placeholder)

        self.notes.setText(comment)
        self.__cascadechanged()

        #quality
        if quality and len(quality)==9:
            for i in range(len(quality)):
                getattr(self, '_'.join(['quality',  quality_items[i]])).setText(string.get_str(quality[i]))

        self.btn_quality.clicked.connect(self.__quality_clicked)
        self.quality_groupbox.setVisible(False)

    def __quality_clicked(self):
        self.quality_groupbox.setVisible(not self.quality_groupbox.isVisible())
        self.quality_groupbox.setMaximumHeight(16777215 if self.quality_groupbox.isVisible() else 0)

    def __draw_graph(self, datas):
        if self.get_cascade_mode() == 'zq_downstream_condition':
            axis = (tr("Z (m)"), tr("Q (m3/s)"))
        elif self.get_cascade_mode() == 'tz_downstream_condition':
            axis = (tr("t (hr)"), tr("Z (m)"))

        if len(datas)>0:
            self.graph.clear()
            self.graph.add_line(
                [r[0] for r in datas],
                [r[1] for r in datas],
                "r",
                self.name.text(),
                *axis)
        else:
            self.graph.canvas.setVisible(False)

    def __cascadechanged(self):
        if self.__last_cascade == 'zq_downstream_condition':
            self.zq_array = self.array.get_table_items()
        elif self.__last_cascade == 'tz_downstream_condition':
            self.tz_array = self.array.get_table_items()

        cascade = self.get_cascade_mode()

        if cascade == 'zq_downstream_condition' or cascade == 'tz_downstream_condition':
            self.graph_placeholder.setEnabled(True)
            self.array_placeholder.setEnabled(True)
            self.btn_quality.setEnabled(False)
            self.quality_groupbox.setVisible(False)
            self.quality_groupbox.setMaximumHeight(0)
            if cascade == 'zq_downstream_condition':
                self.__last_cascade = 'zq_downstream_condition'
                self.array.set_column_title(["Elevation (m)", "Discharge (m3/s)"])
                self.array.set_table_items(self.zq_array)
            elif cascade == 'tz_downstream_condition':
                self.__last_cascade = 'tz_downstream_condition'
                self.array.set_column_title(["Time (hours)", "Elevation (m)"])
                self.array.set_table_items(self.tz_array)
        elif cascade == 'hydrograph':
            self.graph_placeholder.setEnabled(False)
            self.array_placeholder.setEnabled(False)
            self.btn_quality.setEnabled(True)

    def get_id(self):
        return self.id

    def get_cascade_mode(self):
        res = None
        for id, description, name in self.cascade_values:
             if id == self.cascade_mode.get_selected_value():
                res = name
        return res

    def save(self):
        self.project.log.notice(tr("Saved"))

        # quality
        quality = []
        for i in range(len(quality_items)):
            quality.append(string.get_sql_forced_float(getattr(self, '_'.join(['quality', quality_items[i]])).text()))

        # arrays
        if self.__last_cascade == 'zq_downstream_condition':
            self.zq_array = self.array.get_table_items()
        elif self.__last_cascade == 'tz_downstream_condition':
            self.tz_array = self.array.get_table_items()

        self.model.update_singularity_racc(self.id, self.geom, self.get_cascade_mode(), self.zq_array, self.tz_array, self.name.text(), quality=string.list_to_sql_array(quality), comment=self.notes.text())
        SimpleSingularityDialog.save(self)
        self.close()
