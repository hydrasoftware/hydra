/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update ${model}.metadata set version = '3.2';
;;


create or replace function ${model}.crossroad_update()
returns integer
language plpgsql
as $$$$
    declare
        res_ integer;
    begin
        with nodes as (
            insert into $model.crossroad_node(geom)
                (select distinct s.geom
                    from (
                        select (st_dumppoints(geom)).geom
                        from $model.street
                            union
                        select (st_dumppoints(st_intersection(s1.geom, s2.geom))).geom as geom
                        from $model.street as s1, $model.street as s2
                        where ST_intersects(s1.geom,s2.geom)
                        and s1.id<s2.id
                    ) as s
                    left join (select st_force2d(geom) as geom from $model.crossroad_node) as c
                    on st_force2d(s.geom)=c.geom
                    where c.geom is null)
            returning id
        )
        select count(1) from nodes into res_;
        return res_;
    end;
$$$$
;;
