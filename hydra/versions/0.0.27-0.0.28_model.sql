/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  Add boundary type to constrain                */
/* ********************************************** */

create or replace function ${model}.create_boundary_links(constrain_id integer, station_node_id integer)
returns integer
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_boundary_links
    return create_boundary_links(plpy, constrain_id, station_node_id, '$model', $srid)
$$$$
;;

/* ********************************************** */
/*  Replace z_terrain with z_ground from manholes */
/* ********************************************** */

create or replace view ${model}.pipe_link as
 select p.id,
    p.name,
    p.up,
    p.down,
    c.comment,
    c.z_invert_up,
    c.z_invert_down,
    c.cross_section_type,
    c.h_sable,
    c.branch,
    c.rk,
    c.circular_diameter,
    c.ovoid_height,
    c.ovoid_top_diameter,
    c.ovoid_invert_diameter,
    c.cp_geom,
    c.op_geom,
    p.geom,
    p.configuration::character varying as configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration as configuration_json,
    (select case
    when (c.z_invert_up-c.z_invert_down)*st_length(p.geom)>0
    and st_length(p.geom)<>0
    then
        (select case when c.cross_section_type='circular' then
            (select case when c.circular_diameter>0 then
                (with n as (select
                    (pi()/4*pow(c.circular_diameter, 2.0)) as s,
                    (pi()*c.circular_diameter) as p,
                    c.rk as k)
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n))
            else -999
            end)
        when c.cross_section_type='ovoid' then
            (with n as (select
                (pi()/8*(c.ovoid_top_diameter*c.ovoid_top_diameter+c.ovoid_invert_diameter*c.ovoid_invert_diameter)+0.5*(c.ovoid_height-0.5*(c.ovoid_top_diameter+c.ovoid_invert_diameter))*(c.ovoid_top_diameter+c.ovoid_invert_diameter)) as s,
                (pi()/2*(c.ovoid_top_diameter+c.ovoid_invert_diameter)+2*(c.ovoid_height-0.5*(c.ovoid_top_diameter+c.ovoid_invert_diameter))) as p,
                c.rk as k)
            (select case when n.s>0 and n.p>0 then
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
            else -999
            end from n))
        when c.cross_section_type='pipe' then
            (with n as (select ${model}.geometric_calc_s_fct(cpg.zbmin_array) as s,
                    ${model}.cp_geometric_calc_p_fct(cpg.zbmin_array) as p,
                    c.rk as k
                from ${model}.closed_parametric_geometry cpg
                where c.cp_geom = cpg.id)
            (select case when n.s>0 and n.p>0 then
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
            else -999
            end from n))
        when c.cross_section_type='channel' then
            (with n as (select ${model}.geometric_calc_s_fct(opg.zbmin_array) as s,
                    ${model}.op_geometric_calc_p_fct(opg.zbmin_array) as p,
                    c.rk as k
                from ${model}.open_parametric_geometry opg
                where c.op_geom = opg.id)
            (select case when n.s>0 and n.p>0 then
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
            else -999
            end from n))
        else 0.0
        end)
    else -999
    end) as qcap,
    (select case when c.cross_section_type='circular' then
        (select (c.z_invert_up+c.circular_diameter))
    when c.cross_section_type='ovoid' then
        (select (c.z_invert_up+c.ovoid_height))
    when c.cross_section_type='pipe' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.closed_parametric_geometry cpg
            where c.cp_geom = cpg.id)
        (select (c.z_invert_up + n.height) from n))
    when c.cross_section_type='channel' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.open_parametric_geometry opg
            where c.op_geom = opg.id)
        (select (c.z_invert_up + n.height) from n))
    else null
    end) as z_vault_up,
    (select case when c.cross_section_type='circular' then
        (select (c.z_invert_down+c.circular_diameter))
    when c.cross_section_type='ovoid' then
        (select (c.z_invert_down+c.ovoid_height))
    when c.cross_section_type='pipe' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.closed_parametric_geometry cpg
            where c.cp_geom = cpg.id)
        (select (c.z_invert_down + n.height) from n))
    when c.cross_section_type='channel' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.open_parametric_geometry opg
            where c.op_geom = opg.id)
        (select (c.z_invert_down + n.height) from n))
    else null
    end) as z_vault_down,
    (select case when c.cross_section_type='channel' then
        (select (c.z_invert_up))::double precision
    else (select z_ground from ${model}.manhole_node where p.up = id)::double precision
    end) as z_tn_up,
    (select case when c.cross_section_type='channel' then
        (select (c.z_invert_down))::double precision
    else (select z_ground from ${model}.manhole_node where p.down = id)::double precision
    end) as z_tn_down

   from ${model}._pipe_link c,
    ${model}._link p
 where p.id = c.id
 ;;

/* ********************************************** */
/*  New view : model.channel_reach                */
/* ********************************************** */

create or replace view ${model}.channel_reach
as with
    ends as (
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'channel' and p.type_cross_section_down != 'channel' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up != 'channel' and p.type_cross_section_down = 'channel' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'channel' and st_dwithin(p.geom, r.geom, .1) and st_linelocatepoint(r.geom, p.geom) > .99
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_down = 'channel' and st_dwithin(p.geom, r.geom, .1) and st_linelocatepoint(r.geom, p.geom) < .01
    )
    ,
    bounds as (
        select FIRST_VALUE(reach) over (partition by reach order by alpha), typ, LAG(alpha) over (partition by reach order by alpha) as start, alpha as end, id, reach from ends
    )
    select row_number() over() as id, st_linesubstring(r.geom, b.start, b.end) as geom, r.id as reach from bounds as b join ${model}.reach as r on r.id=b.reach where b.typ = 'down'
;;

/* ********************************************** */
/*  Check for no singularity on node before       */
/*  deletion in model._link_after_delete_fct      */
/* ********************************************** */

create or replace function ${model}._link_after_delete_fct()
returns trigger
language plpgsql
as $$$$
    begin
        delete from $model._node
        where id=old.down
        and generated is not null
        and (select count(1) from $model._link where up=old.down and link_type != 'street') = 0
        and (select count(1) from $model._link where down=old.down and link_type != 'street') = 0
        and (select count(1) from $model._singularity as s where s.id=id) = 0;
        delete from $model._node
        where id=old.up
        and generated is not null
        and (select count(1) from $model._link where up=old.up and link_type != 'street') = 0
        and (select count(1) from $model._link where down=old.up and link_type != 'street') = 0
        and (select count(1) from $model._singularity as s where s.id=id) = 0;
        return old;
    end;
$$$$
;;