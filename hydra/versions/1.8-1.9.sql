/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update hydra.metadata set version = '1.9';
;;

create or replace function project.model_instead_fct()
returns trigger
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_model
    for statement in create_model(TD['new']['name'], $srid, '$version'):
        plpy.execute(statement)
$$$$
;;

do
$$$$
    begin
    if exists (
        select 1 from pg_catalog.pg_namespace where nspname = 'csv'
    )
    then
      execute 'insert into csv.hydra_compatible_versions(hydra_version) values (''1.9'')';
    end if;
end
$$$$;

/* ********************************************** */
/*              Chronological series              */
/* ********************************************** */

create or replace function project.get_tmax_serie (id_serie integer)
returns interval
language plpgsql
as
$$$$
    declare
        tmax interval;
    begin
        select serie.tfin
        from project.serie
        where id = id_serie
        into tmax;
        return tmax;
    end;
$$$$
;;

create or replace function project.get_tmax_bloc_serie (id_serie integer)
returns interval
language plpgsql
as
$$$$
    declare
        tmin interval;
    begin
        select max(serie_bloc.t_till_start)
        from project.serie_bloc, project.serie
        where serie.id = serie_bloc.id_cs and serie.id = id_serie
        into tmin;
        return tmin;
    end;
$$$$
;;

create table project.serie(
    id serial primary key,
    name varchar(24) not null default project.unique_name('CS'),
    comment varchar(256),
    comput_mode hydra_computation_mode not null default 'hydrology_and_hydraulics',
    dt_hydrol_mn interval default '00:05:00',
    dt_days int default 4 check(dt_days>0 and dt_days<5),
    date0 timestamp not null default '2000-01-01 00:00:00',
    tfin interval default '365 day' check(extract(seconds from tfin)=0 and tfin>project.get_tmax_bloc_serie(id)),
    tini_hydrau_hr interval default '12:00:00' check(extract(seconds from tini_hydrau_hr)=0),
    dtmin_hydrau_hr interval default '00:00:15',
    dtmax_hydrau_hr interval default '00:05:00',
    dzmin_hydrau real not null default 0.05,
    dzmax_hydrau real not null default 0.1,
    model_connect_settings hydra_model_connection_settings not null default 'cascade',
    dt_output_hr interval default '00:05:00' check(extract(seconds from dt_output_hr)=0),
    c_affin_param bool not null default 'f',
    t_affin_start_hr interval default '00:00:00' check(extract(seconds from t_affin_start_hr)=0),
    t_affin_min_surf_ddomains real not null default 0,
    strickler_param bool not null default 'f',
    option_file bool not null default 'f',
    option_file_path varchar(256),
    output_option bool not null default 'f',
    sor1_mode hydra_sor1_mode not null default 'two_files_extended',
    dt_sor1_hr interval default '01:00:00' check(extract(seconds from dt_sor1_hr)=0),
    transport_type hydra_transport_type not null default 'no_transport',
    iflag_mes bool not null default 'f',
    iflag_dbo5 bool not null default 'f',
    iflag_dco bool not null default 'f',
    iflag_ntk bool not null default 'f',
    quality_type hydra_quality_type not null default 'physico_chemical',
    diffusion bool not null default 'f',
    dispersion_coef real not null default 20,
    longit_diffusion_coef real not null default 41,
    lateral_diff_coef real not null default .45,
    water_temperature_c real not null default 20,
    min_o2_mgl real not null default 1.5,
    kdbo5_j_minus_1 real not null default 0.2,
    koxygen_j_minus_1 real not null default 0.45,
    knr_j_minus_1 real not null default 0.09,
    kn_j_minus_1 real not null default 0.035,
    bdf_mgl real not null default 1.5,
    o2sat_mgl real not null default 9.07,
    iflag_ecoli bool not null default 'f',
    iflag_enti bool not null default 'f',
    iflag_ad1 bool not null default 'f',
    iflag_ad2 bool not null default 'f',
    ecoli_decay_rate_jminus_one real not null default 0,
    enti_decay_rate_jminus_one real not null default 0,
    ad1_decay_rate_jminus_one real not null default 0,
    ad2_decay_rate_jminus_one real not null default 0,
    sediment_file varchar(256) not null default '',
    suspended_sediment_fall_velocity_mhr real not null default 0,
    scenario_ref integer default 0
)
;;

 create table project.serie_bloc(
    id serial primary key,
    id_cs integer not null references project.serie(id) on delete cascade,
    name varchar(24) not null default project.unique_name('CB'),
    t_till_start interval check(date_trunc('day', t_till_start) = t_till_start and t_till_start<project.get_tmax_serie(id_cs)),
        unique(id_cs, t_till_start),
    dry_inflow integer references project.dry_inflow_scenario(id),
    rainfall integer references project._rainfall(id),
    soil_moisture_coef real default 0 check(soil_moisture_coef >= 0 and soil_moisture_coef <=1),
    runoff_adjust_coef real default 1 check(runoff_adjust_coef >= 0),
    option_dim_hydrol_network bool not null default false,
    flag_gfx_control bool not null default 'f',
    flag_hydrology_rstart bool not null default 'f',
    scenario_hydrology_rstart integer references project.scenario(id),
    trstart_hr interval default '00:00:00' check(extract(seconds from trstart_hr)=0),
    sst_mgl real not null default 0,
    scenario_hydrol integer
)
;;

create or replace view project.serie_scenario as (
with
    tfin_bloc as (
        select min(b2.t_till_start) as tfin, b1.id
        from project.serie_bloc as b1, project.serie_bloc as b2
        where b2.t_till_start>b1.t_till_start
        and b2.id_cs = b1.id_cs
        group by b1.id
            union
        select s.tfin as tfin, b.id
        from project.serie as s, project.serie_bloc as b
        where b.id not in (
            select b1.id
            from project.serie_bloc as b1, project.serie_bloc as b2
            where b2.t_till_start>b1.t_till_start
            and b2.id_cs = b1.id_cs
            group by b1.id)
        and b.id_cs = s.id
    ),
    serie_bloc as (
        select s.id as id_cs,
            s.dt_days,
            s.tfin as tfin_cs,
            s.date0 as date0_cs,
            b.t_till_start as t_till_start_b,
            tfb.tfin as tfin_b,
            DATE_PART('day',tfb.tfin - b.t_till_start) as duration_days,
            DATE_PART('day',tfb.tfin - b.t_till_start)::int % s.dt_days as last_scn_duration,
            CEIL(DATE_PART('day',tfb.tfin - b.t_till_start)/s.dt_days)::int as nb_scn,
            b.id as id_b
        from project.serie as s, project.serie_bloc as b, tfin_bloc as tfb
        where s.id = b.id_cs and tfb.id = b.id
    ),
    serie_bloc_prev as (
        select
            sb1.*,
            sb2.id_b as id_prev,
            sb2.nb_scn as nb_prev,
            sb2.last_scn_duration as last_scn_prev
            from
            serie_bloc as sb1
            left join serie_bloc as sb2
            on sb1.t_till_start_b = sb2.tfin_b
            and sb1.id_cs = sb2.id_cs
	),
    gen as (
        select generate_series(1, nb_scn) as i, id_b from serie_bloc
    ),
    scn_gen as (
        select gen.i,
            (gen.i-1)*sb.dt_days as t_start,
            (gen.i)*sb.dt_days as t_fin,
            sb.dt_days as scn_duration,
            sb.id_b,
            sb.last_scn_duration,
            sb.nb_scn,
            sb.id_prev,
            sb.nb_prev,
            sb.last_scn_prev
        from serie_bloc_prev as sb, gen
        where gen.id_b = sb.id_b
        order by id_b,i
    ),
    scn_data as (
        select
            s.id as id_cs,
            b.id as id_b,
            scn_gen.i as index,
            scn_gen.t_start *'1 days'::interval + b.t_till_start as _tb_start,
            s.name || '_' || b.name || '_SCN' || to_char(scn_gen.i,'FM0000') as name,
            '' as comment,
            s.comput_mode as comput_mode,
            b.dry_inflow as dry_inflow,
            b.rainfall as rainfall,
            s.dt_hydrol_mn as dt_hydrol_mn,
            b.soil_moisture_coef as soil_moisture_coef,
            b.runoff_adjust_coef as runoff_adjust_coef,
            b.option_dim_hydrol_network as option_dim_hydrol_network,
            scn_gen.t_start *'1 days'::interval + b.t_till_start + s.date0 as date0,
            scn_gen.scn_duration * '24 hours'::interval as tfin_hr,
            s.tini_hydrau_hr as tini_hydrau_hr,
            s.dtmin_hydrau_hr as dtmin_hydrau_hr,
            s.dtmax_hydrau_hr as dtmax_hydrau_hr,
            s.dzmin_hydrau as dzmin_hydrau,
            s.dzmax_hydrau as dzmax_hydrau,
            s.dt_output_hr as dt_output_hr,
            't'::bool as flag_save,
            scn_gen.scn_duration * '24 hours'::interval as tsave_hr,
            case
                when scn_gen.i = 1 and scn_gen.id_prev is null then 'f'::bool
                else 't'::bool
            end as flag_rstart,
            b.flag_gfx_control as flag_gfx_control,
            case
                when scn_gen.i = 1 and scn_gen.id_prev is null then ARRAY[]::integer[]
                when scn_gen.i = 1 and scn_gen.id_prev is not null then ARRAY[s.id, scn_gen.id_prev, scn_gen.nb_prev]
                else ARRAY[s.id, b.id, scn_gen.i-1]
            end as scenario_rstart_array,
            'f'::bool as scenario_rstart,
            b.trstart_hr as trstart_hr,
            b.flag_hydrology_rstart as flag_hydrology_rstart,
            b.scenario_hydrology_rstart as scenario_hydrology_rstart,
            'f'::bool as graphic_control,
            s.model_connect_settings as model_connect_settings,
            '00:00:00'::interval as tbegin_output_hr,
            scn_gen.scn_duration * '24 hours'::interval as tend_output_hr,
            s.c_affin_param as c_affin_param,
            s.t_affin_start_hr as t_affin_start_hr,
            s.t_affin_min_surf_ddomains as t_affin_min_surf_ddomains,
            s.strickler_param as strickler_param,
            s.option_file as option_file,
            s.option_file_path as option_file_path,
            s.output_option as output_option,
            s.sor1_mode as sor1_mode,
            s.dt_sor1_hr as dt_sor1_hr,
            s.transport_type as transport_type,
            s.iflag_mes as iflag_mes,
            s.iflag_dbo5 as iflag_dbo5,
            s.iflag_dco as iflag_dco,
            s.iflag_ntk as iflag_ntk,
            s.quality_type as quality_type,
            s.diffusion as diffusion,
            s.dispersion_coef as dispersion_coef,
            s.longit_diffusion_coef as longit_diffusion_coef,
            s.lateral_diff_coef as lateral_diff_coef,
            s.water_temperature_c as water_temperature_c,
            s.min_o2_mgl as min_o2_mgl,
            s.kdbo5_j_minus_1 as kdbo5_j_minus_1,
            s.koxygen_j_minus_1 as koxygen_j_minus_1,
            s.knr_j_minus_1 as knr_j_minus_1,
            s.kn_j_minus_1 as kn_j_minus_1,
            s.bdf_mgl as bdf_mgl,
            s.o2sat_mgl as o2sat_mgl,
            s.iflag_ecoli as iflag_ecoli,
            s.iflag_enti as iflag_enti,
            s.iflag_ad1 as iflag_ad1,
            s.iflag_ad2 as iflag_ad2,
            s.ecoli_decay_rate_jminus_one as ecoli_decay_rate_jminus_one,
            s.enti_decay_rate_jminus_one as enti_decay_rate_jminus_one,
            s.ad1_decay_rate_jminus_one as ad1_decay_rate_jminus_one,
            s.ad2_decay_rate_jminus_one as ad2_decay_rate_jminus_one,
            b.sst_mgl as sst_mgl,
            s.sediment_file as sediment_file,
            s.suspended_sediment_fall_velocity_mhr as suspended_sediment_fall_velocity_mhr,
            s.scenario_ref as scenario_ref ,
            b.scenario_hydrol as scenario_hydrol
        from
            project.serie as s,
            project.serie_bloc as b,
            scn_gen
        where
            s.id = b.id_cs and
            scn_gen.id_b = b.id and
            scn_gen.t_fin != ((scn_gen.nb_scn) * scn_gen.scn_duration)
                union
        select
            s.id as id_cs,
            b.id as id_b,
            scn_gen.i+1 as index,
            scn_gen.t_fin *'1 days'::interval + b.t_till_start as _tb_start,
            s.name || '_' || b.name || '_SCN' || to_char(scn_gen.i+1,'FM0000') as name,
            '' as comment,
            s.comput_mode as comput_mode,
            b.dry_inflow as dry_inflow,
            b.rainfall as rainfall,
            s.dt_hydrol_mn as dt_hydrol_mn,
            b.soil_moisture_coef as soil_moisture_coef,
            b.runoff_adjust_coef as runoff_adjust_coef,
            b.option_dim_hydrol_network as option_dim_hydrol_network,
            scn_gen.t_fin *'1 days'::interval + b.t_till_start + s.date0 as date0,
            case
                when scn_gen.last_scn_duration = 0 then scn_gen.scn_duration * '24 hours'::interval
                else scn_gen.last_scn_duration * '24 hours'::interval
            end as tfin_hr,
            s.tini_hydrau_hr as tini_hydrau_hr,
            s.dtmin_hydrau_hr as dtmin_hydrau_hr,
            s.dtmax_hydrau_hr as dtmax_hydrau_hr,
            s.dzmin_hydrau as dzmin_hydrau,
            s.dzmax_hydrau as dzmax_hydrau,
            s.dt_output_hr as dt_output_hr,
            't'::bool as flag_save,
            case
                when scn_gen.last_scn_duration = 0 then scn_gen.scn_duration * '24 hours'::interval
                else scn_gen.last_scn_duration * '24 hours'::interval
            end as tsave_hr,
            case
                when scn_gen.i = 1 and scn_gen.id_prev is null then 'f'::bool
                else 't'::bool
            end as flag_rstart,
            b.flag_gfx_control as flag_gfx_control,
            case
                when scn_gen.i = 1 and scn_gen.id_prev is null then ARRAY[]::integer[]
                when scn_gen.i = 1 and scn_gen.id_prev is not null then ARRAY[s.id, scn_gen.id_prev, scn_gen.nb_prev]
                else ARRAY[s.id, b.id, scn_gen.i]
            end as scenario_rstart_array,
            'f'::bool as scenario_rstart,
            b.trstart_hr as trstart_hr,
            b.flag_hydrology_rstart as flag_hydrology_rstart,
            b.scenario_hydrology_rstart as scenario_hydrology_rstart,
            'f'::bool as graphic_control,
            s.model_connect_settings as model_connect_settings,
            '00:00:00'::interval as tbegin_output_hr,
            scn_gen.scn_duration * '24 hours'::interval as tend_output_hr,
            s.c_affin_param as c_affin_param,
            s.t_affin_start_hr as t_affin_start_hr,
            s.t_affin_min_surf_ddomains as t_affin_min_surf_ddomains,
            s.strickler_param as strickler_param,
            s.option_file as option_file,
            s.option_file_path as option_file_path,
            s.output_option as output_option,
            s.sor1_mode as sor1_mode,
            s.dt_sor1_hr as dt_sor1_hr,
            s.transport_type as transport_type,
            s.iflag_mes as iflag_mes,
            s.iflag_dbo5 as iflag_dbo5,
            s.iflag_dco as iflag_dco,
            s.iflag_ntk as iflag_ntk,
            s.quality_type as quality_type,
            s.diffusion as diffusion,
            s.dispersion_coef as dispersion_coef,
            s.longit_diffusion_coef as longit_diffusion_coef,
            s.lateral_diff_coef as lateral_diff_coef,
            s.water_temperature_c as water_temperature_c,
            s.min_o2_mgl as min_o2_mgl,
            s.kdbo5_j_minus_1 as kdbo5_j_minus_1,
            s.koxygen_j_minus_1 as koxygen_j_minus_1,
            s.knr_j_minus_1 as knr_j_minus_1,
            s.kn_j_minus_1 as kn_j_minus_1,
            s.bdf_mgl as bdf_mgl,
            s.o2sat_mgl as o2sat_mgl,
            s.iflag_ecoli as iflag_ecoli,
            s.iflag_enti as iflag_enti,
            s.iflag_ad1 as iflag_ad1,
            s.iflag_ad2 as iflag_ad2,
            s.ecoli_decay_rate_jminus_one as ecoli_decay_rate_jminus_one,
            s.enti_decay_rate_jminus_one as enti_decay_rate_jminus_one,
            s.ad1_decay_rate_jminus_one as ad1_decay_rate_jminus_one,
            s.ad2_decay_rate_jminus_one as ad2_decay_rate_jminus_one,
            b.sst_mgl as sst_mgl,
            s.sediment_file as sediment_file,
            s.suspended_sediment_fall_velocity_mhr as suspended_sediment_fall_velocity_mhr,
            s.scenario_ref as scenario_ref,
            b.scenario_hydrol as scenario_hydrol
        from
            project.serie as s,
            project.serie_bloc as b,
            scn_gen
        where
            s.id = b.id_cs and
            scn_gen.id_b = b.id and
            scn_gen.t_fin = ((scn_gen.nb_scn-1) * scn_gen.scn_duration)
    )
    select
        row_number() over(order by id_cs, id_b, index) as id,
        scn_data.*
    from scn_data
    )
;;

create table project.mixed_serie(
    ord integer not null,
    serie integer not null references project.serie(id) on delete cascade,
    grp integer not null references project.grp(id),
        unique(serie, grp),
        unique(serie, ord)
)
;;

create table project.config_serie(
    id serial primary key,
    serie integer not null references project.serie(id) on delete cascade,
    model varchar(24) not null,
    configuration integer not null,
    priority integer not null default currval('project.config_serie_id_seq')
)
;;

create table project.param_external_hydrograph_bloc(
    id serial primary key,
    param_bloc integer not null references project.serie_bloc(id) on delete cascade,
    external_file varchar(256) not null
)
;;

create table project.param_regulation_bloc(
    id serial primary key,
    param_bloc integer not null references project.serie_bloc(id) on delete cascade,
    control_file varchar(256) not null
)
;;

create table project.param_measure_bloc(
    id serial primary key,
    param_bloc integer not null references project.serie_bloc(id) on delete cascade,
    measure_file varchar(256) not null
)
;;

create table project.inflow_rerouting_bloc(
    id serial primary key,
    bloc integer not null references project.serie_bloc(id) on delete cascade,
    model_from varchar(24) not null,
    hydrograph_from integer not null,
    model_to varchar(24),
    hydrograph_to integer,
    option hydra_rerouting_option not null default 'fraction',
    parameter real not null default 0,
        unique(bloc, model_from, hydrograph_from, model_to, hydrograph_to)
)
;;

create table project.c_affin_param_serie(
    id serial primary key,
    serie integer not null references project.serie(id) on delete cascade,
    model varchar(24) not null,
    type_domain varchar(24) not null check(type_domain in ('reach', 'domain_2d', 'branch')),
    element varchar(24) not null,
        unique(serie, model, type_domain, element)
)
;;

create table project.strickler_param_serie(
    id serial primary key ,
    serie integer not null references project.serie(id) on delete cascade,
    model varchar(24) not null,
    type_domain varchar(24) not null check(type_domain in ('reach', 'domain_2d', 'branch')),
    element varchar(24) not null,
    rkmin real not null default 0,
    rkmaj real not null default 0,
    pk1 real not null default 0,
    pk2 real not null default 0,
        unique(serie, model, type_domain, element)
)
;;

create table project.output_option_serie(
    id serial primary key ,
    serie integer not null references project.serie(id) on delete cascade,
    model varchar(24) not null,
    type_element varchar(48) not null,
    element varchar(24) not null
)
;;
