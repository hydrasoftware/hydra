# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_singularity_dialog import SimpleSingularityDialog, tr
import hydra.utility.string as string
from ..widgets.graph_widget import GraphWidget
from ..widgets.array_widget import ArrayWidget
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QTableWidgetItem
import os

class AirFlowBcSingularityEditor(SimpleSingularityDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleSingularityDialog.__init__(self, project, geom, "air_flow_bc_singularity", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "air_flow_bc_singularity.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        name, flow, is_inwards, comment = self.project.execute("""
            select name, flow, is_inwards, comment
            from {}.air_flow_bc_singularity
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.name.setText(name)
        self.flow.setText(f"{flow:.2f}")
        self.is_inwards.setChecked(is_inwards)
        self.notes.setText(comment)

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.model.update_air_flow_bc_singularity(self.id, self.geom, self.name.text(), float(self.flow.text()), self.is_inwards.isChecked(), comment=self.notes.text())
        SimpleSingularityDialog.save(self)
        self.close()
