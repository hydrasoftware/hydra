# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from __future__ import absolute_import
from .base_dialog import BaseDialog, tr
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QDialog, QCheckBox
import os
import re

class ResultsSelector(QDialog):
    def __init__(self, selected_types, parent=None):
        QDialog.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "results_selector.ui"), self)

        self.buttonBox.accepted.connect(self.__save)
        self.__result = []

        for checkbox in self.findChildren(QCheckBox):
            if checkbox.objectName() in selected_types:
                checkbox.setChecked(True)

    def get_results(self):
        return self.__result

    def __save(self):
        for checkbox in self.findChildren(QCheckBox):
            if checkbox.isChecked():
                self.__result.append(checkbox.objectName())
        self.close()