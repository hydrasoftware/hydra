# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import subprocess
import platform
import requests

from hydra.utility.process import run_cmd
from hydra.utility.log import LogManager
from hydra.utility.settings_properties import SettingsProperties
from hydra.versions import __version_history__


def __deploy_to_postgres(plugin_dir, pg_dir, py3_dir, log_manager):
    """Deploys postgresql hydra dependencies to pg_dir"""
    if os.path.isdir(pg_dir):
        if os.path.isfile(os.path.abspath(os.path.join(py3_dir, "python.exe"))):
            install_args = [
                os.path.abspath(os.path.join(py3_dir, "python.exe")),
                os.path.abspath(os.path.join(plugin_dir, "server", "install.py")),
                os.path.abspath(pg_dir),
            ]
            install_env = os.environ.copy()
            if "PYTHONHOME" in install_env:
                del install_env["PYTHONHOME"]
            if "PYTHONPATH" in install_env:
                del install_env["PYTHONPATH"]

            run_cmd(install_args, log_manager, environment=install_env)
            log_manager.notice("Postgre extension hydra installed in {}.".format(pg_dir))

        # download all extension versions if needed
        log_manager.notice("Installing all legacy hydra extensions...")
        with requests.Session() as session:
            for version in __version_history__.keys():
                filename = f"hydra--{version}.sql"
                if (
                    version != "test.1.2.2"
                    and version != "current_version"
                    and not os.path.exists(os.path.join(pg_dir, filename))
                ):
                    try:
                        resp = session.get(
                            f"https://hydra-software.net/telechargement/hydra-extensions/{filename}",
                            timeout=4,
                            headers={
                                "User-Agent": "Mozilla"
                            },  # to avoid bans from amen host platform
                        )
                    except requests.exceptions.ConnectTimeout:
                        log_manager.warning(
                            "Too long to get files from hydra-software.net, please retry later"
                        )
                        break
                    except requests.exceptions.SSLError:
                        log_manager.warning(
                            "Connection issue to get files (hydra old version) from hydra-software.net, you may need them for opening old versions, try with another connection or check for firewall issue"
                        )
                        break
                    if resp.status_code != 200:
                        log_manager.warning(f"Remote file {filename} unavailable, skipping")
                        continue
                    with open(os.path.join(pg_dir, filename), mode="wb") as output:
                        output.write(resp.content)
                    log_manager.notice(f"Extension file {filename} installed")

    else:
        log_manager.error("Error with path to PostgreSQL repository in setting")


def __deploy_to_python3(plugin_dir, py3_dir, log_manager):
    """Deploys python3 hydra libraries"""
    if os.path.isdir(py3_dir):
        py_args = [
            os.path.join(py3_dir, "python.exe"),
            os.path.join(plugin_dir, "server", "setup.py"),
            "install",
        ]
        py_env3 = os.environ.copy()
        if "PYTHONHOME" in py_env3:
            del py_env3["PYTHONHOME"]
        if "PYTHONPATH" in py_env3:
            del py_env3["PYTHONPATH"]

        run_cmd(
            py_args, log_manager, environment=py_env3, directory=os.path.join(plugin_dir, "server")
        )
        log_manager.notice("Python 3 extension hydra installed")
    else:
        log_manager.error("Error with path to Python 3 repository in setting")


def deploy_dependencies(plugin_dir, log_manager=LogManager()):
    """Deploys all (postgresql and python3) hydra dependencies"""

    if SettingsProperties.local():
        # extension hydra pour PostgreSQL, ancienne archi via settings, nouvelle archi via chemin figé en dur
        for pg_path in set(
            [SettingsProperties.get_path("postgre"), "C:\\Program Files\\Hydra\\PostgreSQL\\13"]
        ):
            if os.path.isdir(pg_path):
                __deploy_to_postgres(
                    plugin_dir,
                    os.path.join(pg_path, "share", "extension"),
                    SettingsProperties.get_path("python"),
                    log_manager,
                )
            else:
                log_manager.notice(f"{pg_path} is not a valid directory")
        # dépendance hydra pour plpython3, ancienne archi via settings, nouvelle archi via chemin figé en dur
        for py3_path in set(
            [
                SettingsProperties.get_path("python"),
                "C:\\Program Files\\Hydra\\languagepack\\v1\\Python-3.7",
            ]
        ):
            if os.path.isdir(py3_path):
                __deploy_to_python3(plugin_dir, py3_path, log_manager)
            else:
                log_manager.notice(f"{py3_path} is not a valid directory")

        


def open_external(filename: str) -> None:
    """Open filename externally with default associated tool"""
    if platform.system() == "Windows":
        os.startfile(filename)
    else:
        subprocess.run(["open", filename])


if __name__ == "__main__":
    from hydra.utility.log import LogManager, ConsoleLogger

    log = LogManager(ConsoleLogger(), "Hydra")
    plugin_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))

    deploy_dependencies(plugin_dir, log)
