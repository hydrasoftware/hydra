# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.forms.gate_link_guitest [-dhs] [-g project_name model_name x y x y]

OPTIONS
   -s  --solo
        run test in solo mode (create database)

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name model_name x y
        run in gui mode

"""

from hydra.database.database import TestProject, remove_project, project_exists
from hydra.utility.string import normalized_name, normalized_model_name
import sys
import getopt

from hydra.project import Project

def gui_mode(project_name, model_name, geom, id):

    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.regul_gate_singularity import RegulGateSingularityEditor
    from hydra.utility.string import normalized_name, normalized_model_name

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)

    test_dialog = RegulGateSingularityEditor(obj_project, geom, id)
    test_dialog.exec_()


def test():
    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.regul_gate_singularity import RegulGateSingularityEditor
    from hydra.database.database import TestProject

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)

    obj_project.set_current_model(model_name)
    model = obj_project.get_current_model()

    model.add_manhole([70, 10])
    id_control_node = model.add_manhole([90, 30])
    name_control_node, = obj_project.execute("""select name from {}._node where id={}""".format(model_name, id_control_node)).fetchone()

    test_dialog = RegulGateSingularityEditor(obj_project, [70, 10])
    test_dialog.name.setText("Test_r_gate_sing")
    test_dialog.z_invert.setText(str(100))
    test_dialog.z_invert_stop.setText(str(101))
    test_dialog.z_ceiling.setText(str(200))
    test_dialog.z_ceiling_stop.setText(str(199))
    test_dialog.width.setText(str(55))
    test_dialog.cc.setText(str(0.9))
    test_dialog.cc_submerged.setText(str(2.1))
    test_dialog.v_max_cms.setText(str(10))
    test_dialog.upward_opening_gate.setChecked(True)
    test_dialog.downward_opening_gate.setChecked(True)
    test_dialog.dt_regul_hr.setText(str(0.003))
    test_dialog.z_control_node.setText(name_control_node)
    test_dialog.z_pid_0.setText(str(16))
    test_dialog.z_pid_1.setText(str(17))
    test_dialog.z_pid_2.setText(str(18))
    test_dialog.z_array.set_table_items([[1,2], [3,4]])
    test_dialog.q_z_crit.setText(str(25))
    test_dialog.q_array.set_table_items([[10,20], [30,40]])
    id = test_dialog.get_id()
    test_dialog.save()

    test_dialog = RegulGateSingularityEditor(obj_project, None, id)
    assert test_dialog.name.text()=="Test_r_gate_sing"
    assert float(test_dialog.z_invert.text())==100
    assert float(test_dialog.z_invert_stop.text())==101
    assert float(test_dialog.z_ceiling.text())==200
    assert float(test_dialog.z_ceiling_stop.text())==199
    assert float(test_dialog.width.text())==55
    assert float(test_dialog.cc.text())==0.9
    # corresponding checkbox not checked, must stay at None
    assert test_dialog.cc_submerged.text()==""
    assert not test_dialog.chk_cc_submerged.isChecked()
    assert float(test_dialog.v_max_cms.text())==10
    assert test_dialog.downward_opening_gate.isChecked()
    assert float(test_dialog.dt_regul_hr.text())==0.003
    assert test_dialog.z_control_node.text()==name_control_node
    assert float(test_dialog.z_pid_0.text())==16
    assert float(test_dialog.z_pid_1.text())==17
    assert float(test_dialog.z_pid_2.text())==18
    assert float(test_dialog.q_z_crit.text())==25

    def list_equals(list1, list2):
        for val in list1:
            if not val in list2:
                return False
        return True

    assert list_equals(test_dialog.z_array.get_table_items(),[[1,2], [3,4]])
    assert list_equals(test_dialog.q_array.get_table_items(),[[10,20], [30,40]])
    test_dialog.reject()

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])

except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name)
    test()
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    model_name = args[1]
    geom = [float(a) for a in args[2:]]
    id =geom[2] if len(geom)==3 else None
    project_name = normalized_name(project_name)
    model_name = normalized_model_name(model_name)
    gui_mode(project_name, model_name, geom, id)
    exit(0)

test()
# fix_print_with_import
print("ok")


