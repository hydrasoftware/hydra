# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
from qgis.PyQt.QtWidgets import QComboBox, QGridLayout
from qgis.PyQt.QtGui import QStandardItem, QColor, QPalette
from qgis.PyQt.QtCore import QMargins

class ComboWithValues(QComboBox):
    def __init__(self, parent = None):
        QComboBox.__init__(self, parent)
        if parent:
            tmplayout = QGridLayout()
            tmplayout.setContentsMargins(QMargins(0,0,0,0))
            tmplayout.addWidget(self)
            parent.setLayout(tmplayout)

        self.__values=[]
        self.__colors=[]

        self.currentIndexChanged.connect(self.__value_changed)

    def addItem(self, label, value, color='black'):
        self.__values.append(value)
        self.__colors.append(color)

        item = QStandardItem(label)
        item.setForeground(QColor(color))
        QComboBox.model(self).appendRow(item)

    def set_selected_value(self, value):
        if value in self.__values:
            QComboBox.setCurrentIndex(self, self.__values.index(value))
        else:
            QComboBox.setCurrentIndex(self, -1)

    def get_selected_value(self):
        if self.currentIndex() == -1:
            return None
        else:
            return self.__values[self.currentIndex()]

    def clear(self):
        QComboBox.clear(self)
        self.__values=[]
        self.__colors=[]

    def __value_changed(self):
        if self.currentIndex() == -1:
            color = 'black'
        else:
            color =  self.__colors[self.currentIndex()]

        palette = QPalette()
        palette.setColor(8, QColor(color))
        self.setPalette(palette)
