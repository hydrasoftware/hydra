# coding=UTF-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run all tests

USAGE

    test.py [-hk] [project_name projet1.csv [projet2.csv ...]]

    if project_name and project1.csv are specified, import the file in the specified project (create it if needed)

OPTIONS

   -h, --help
        print this help

   -k, --keep
       don't remove the database after the test
"""

import os, sys
import getopt
from hydra.project import Project
from hydra.database.import_project import import_project_csv
from hydra.database.database import TestProject, project_exists, remove_project

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hk",
            ["help", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

keep =  "-k" in optlist or "--keep" in optlist

if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

data_dir = os.path.join(os.path.dirname(__file__), 'test_data', 'tests_import_projet')

project_name, files = 'import_project_test', [os.path.join(data_dir, f)
        for f in os.listdir(data_dir) if f.endswith(".csv")]

if project_exists(project_name):
    remove_project(project_name)

if len(args) >= 2:
    project_name, files = args[0], args[1:]
    if not project_exists(project_name):
        Project.create_new_project(project_name, 2154)
else:
    test_project = TestProject(project_name, keep)

project = Project.load_project(project_name)

for file in files:
    import_project_csv(file, project)
    project.commit()
print('ok')
