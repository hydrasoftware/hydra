<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="100000000" styleCategories="AllStyleCategories" simplifyDrawingTol="1" symbologyReferenceScale="-1" version="3.22.7-Białowieża" maxScale="0" simplifyMaxScale="1" labelsEnabled="0" simplifyAlgorithm="0" simplifyLocal="1" simplifyDrawingHints="0" hasScaleBasedVisibilityFlag="0" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal durationField="" durationUnit="min" accumulate="0" startField="" limitMode="0" mode="0" startExpression="" fixedDuration="0" endField="" enabled="0" endExpression="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 forceraster="0" symbollevels="0" referencescale="-1" enableorderby="0" type="singleSymbol">
    <symbols>
      <symbol force_rhr="0" clip_to_extent="1" alpha="1" name="0" type="marker">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" class="SimpleMarker" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="0,0,0,0" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="circle" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="0,0,0,0" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="2" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="0,0,0,0" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0,0,0,0" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option name="dualview/previewExpressions" type="List">
        <Option value="COALESCE( &quot;name&quot;, '&lt;NULL>' )" type="QString"/>
      </Option>
      <Option value="0" name="embeddedWidgets/count" type="QString"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Pie" attributeLegend="1">
    <DiagramCategory backgroundAlpha="255" maxScaleDenominator="1e+08" penAlpha="255" minScaleDenominator="1000" labelPlacementMethod="XHeight" spacing="0" scaleDependency="Area" opacity="1" rotationOffset="270" lineSizeType="MM" spacingUnitScale="3x:0,0,0,0,0,0" penColor="#000000" barWidth="5" penWidth="0" diagramOrientation="Up" scaleBasedVisibility="0" showAxis="0" sizeType="Pixel" width="15" minimumSize="0" lineSizeScale="3x:0,0,0,0,0,0" spacingUnit="MM" backgroundColor="#ffffff" height="15" direction="1" enabled="1" sizeScale="3x:0,0,0,0,0,0">
      <fontProperties description="Arial,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute field="&quot;catchment_Hbrut(mm)&quot; -  &quot;catchment_Hnet(mm)&quot; " label="H capt (brut - net)" color="#01f3ff"/>
      <attribute field="&quot;catchment_Hnet(mm)&quot;" label="H net" color="#0303fe"/>
      <axisSymbol>
        <symbol force_rhr="0" clip_to_extent="1" alpha="1" name="" type="line">
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
          <layer pass="0" class="SimpleLine" enabled="1" locked="0">
            <Option type="Map">
              <Option value="0" name="align_dash_pattern" type="QString"/>
              <Option value="square" name="capstyle" type="QString"/>
              <Option value="5;2" name="customdash" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale" type="QString"/>
              <Option value="MM" name="customdash_unit" type="QString"/>
              <Option value="0" name="dash_pattern_offset" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale" type="QString"/>
              <Option value="MM" name="dash_pattern_offset_unit" type="QString"/>
              <Option value="0" name="draw_inside_polygon" type="QString"/>
              <Option value="bevel" name="joinstyle" type="QString"/>
              <Option value="35,35,35,255" name="line_color" type="QString"/>
              <Option value="solid" name="line_style" type="QString"/>
              <Option value="0.26" name="line_width" type="QString"/>
              <Option value="MM" name="line_width_unit" type="QString"/>
              <Option value="0" name="offset" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
              <Option value="MM" name="offset_unit" type="QString"/>
              <Option value="0" name="ring_filter" type="QString"/>
              <Option value="0" name="trim_distance_end" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale" type="QString"/>
              <Option value="MM" name="trim_distance_end_unit" type="QString"/>
              <Option value="0" name="trim_distance_start" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale" type="QString"/>
              <Option value="MM" name="trim_distance_start_unit" type="QString"/>
              <Option value="0" name="tweak_dash_pattern_on_corners" type="QString"/>
              <Option value="0" name="use_custom_dash" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="width_map_unit_scale" type="QString"/>
            </Option>
            <prop v="0" k="align_dash_pattern"/>
            <prop v="square" k="capstyle"/>
            <prop v="5;2" k="customdash"/>
            <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
            <prop v="MM" k="customdash_unit"/>
            <prop v="0" k="dash_pattern_offset"/>
            <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
            <prop v="MM" k="dash_pattern_offset_unit"/>
            <prop v="0" k="draw_inside_polygon"/>
            <prop v="bevel" k="joinstyle"/>
            <prop v="35,35,35,255" k="line_color"/>
            <prop v="solid" k="line_style"/>
            <prop v="0.26" k="line_width"/>
            <prop v="MM" k="line_width_unit"/>
            <prop v="0" k="offset"/>
            <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
            <prop v="MM" k="offset_unit"/>
            <prop v="0" k="ring_filter"/>
            <prop v="0" k="trim_distance_end"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_end_map_unit_scale"/>
            <prop v="MM" k="trim_distance_end_unit"/>
            <prop v="0" k="trim_distance_start"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_start_map_unit_scale"/>
            <prop v="MM" k="trim_distance_start_unit"/>
            <prop v="0" k="tweak_dash_pattern_on_corners"/>
            <prop v="0" k="use_custom_dash"/>
            <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings priority="0" obstacle="0" dist="0" placement="1" linePlacementFlags="2" showAll="1" zIndex="0">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="None" name="id">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="last_refresh">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="catchment_model">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="catchment_Hbrut(mm)">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="catchment_Hnet(mm)">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="catchment_cr">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="catchment_q(m3/s)">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="catchment_Lag_time(mn)">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="catchment_Vol(m3)">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" index="0" name=""/>
    <alias field="name" index="1" name=""/>
    <alias field="last_refresh" index="2" name=""/>
    <alias field="catchment_model" index="3" name=""/>
    <alias field="catchment_Hbrut(mm)" index="4" name=""/>
    <alias field="catchment_Hnet(mm)" index="5" name=""/>
    <alias field="catchment_cr" index="6" name=""/>
    <alias field="catchment_q(m3/s)" index="7" name=""/>
    <alias field="catchment_Lag_time(mn)" index="8" name=""/>
    <alias field="catchment_Vol(m3)" index="9" name=""/>
  </aliases>
  <defaults>
    <default field="id" expression="" applyOnUpdate="0"/>
    <default field="name" expression="" applyOnUpdate="0"/>
    <default field="last_refresh" expression="" applyOnUpdate="0"/>
    <default field="catchment_model" expression="" applyOnUpdate="0"/>
    <default field="catchment_Hbrut(mm)" expression="" applyOnUpdate="0"/>
    <default field="catchment_Hnet(mm)" expression="" applyOnUpdate="0"/>
    <default field="catchment_cr" expression="" applyOnUpdate="0"/>
    <default field="catchment_q(m3/s)" expression="" applyOnUpdate="0"/>
    <default field="catchment_Lag_time(mn)" expression="" applyOnUpdate="0"/>
    <default field="catchment_Vol(m3)" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint field="id" constraints="3" exp_strength="0" unique_strength="1" notnull_strength="1"/>
    <constraint field="name" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="last_refresh" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="catchment_model" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="catchment_Hbrut(mm)" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="catchment_Hnet(mm)" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="catchment_cr" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="catchment_q(m3/s)" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="catchment_Lag_time(mn)" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="catchment_Vol(m3)" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" exp="" desc=""/>
    <constraint field="name" exp="" desc=""/>
    <constraint field="last_refresh" exp="" desc=""/>
    <constraint field="catchment_model" exp="" desc=""/>
    <constraint field="catchment_Hbrut(mm)" exp="" desc=""/>
    <constraint field="catchment_Hnet(mm)" exp="" desc=""/>
    <constraint field="catchment_cr" exp="" desc=""/>
    <constraint field="catchment_q(m3/s)" exp="" desc=""/>
    <constraint field="catchment_Lag_time(mn)" exp="" desc=""/>
    <constraint field="catchment_Vol(m3)" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" sortExpression="&quot;catchment_Lag_time(mn)&quot;" actionWidgetStyle="dropDown">
    <columns>
      <column hidden="0" name="id" type="field" width="-1"/>
      <column hidden="0" name="name" type="field" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
      <column hidden="0" name="catchment_model" type="field" width="-1"/>
      <column hidden="0" name="catchment_Hbrut(mm)" type="field" width="154"/>
      <column hidden="0" name="catchment_Hnet(mm)" type="field" width="282"/>
      <column hidden="0" name="catchment_cr" type="field" width="-1"/>
      <column hidden="0" name="catchment_q(m3/s)" type="field" width="-1"/>
      <column hidden="0" name="catchment_Lag_time(mn)" type="field" width="-1"/>
      <column hidden="0" name="last_refresh" type="field" width="-1"/>
      <column hidden="0" name="catchment_Vol(m3)" type="field" width="516"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">.</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>.</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="0" name="catchment_Hbrut(mm)"/>
    <field editable="0" name="catchment_Hnet(mm)"/>
    <field editable="0" name="catchment_Lag_time(mn)"/>
    <field editable="0" name="catchment_Vol(m3)"/>
    <field editable="0" name="catchment_cr"/>
    <field editable="0" name="catchment_model"/>
    <field editable="0" name="catchment_q(m3/s)"/>
    <field editable="1" name="id"/>
    <field editable="1" name="last_refresh"/>
    <field editable="1" name="name"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="catchment_Hbrut(mm)"/>
    <field labelOnTop="0" name="catchment_Hnet(mm)"/>
    <field labelOnTop="0" name="catchment_Lag_time(mn)"/>
    <field labelOnTop="0" name="catchment_Vol(m3)"/>
    <field labelOnTop="0" name="catchment_cr"/>
    <field labelOnTop="0" name="catchment_model"/>
    <field labelOnTop="0" name="catchment_q(m3/s)"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="last_refresh"/>
    <field labelOnTop="0" name="name"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="catchment_Hbrut(mm)" reuseLastValue="0"/>
    <field name="catchment_Hnet(mm)" reuseLastValue="0"/>
    <field name="catchment_Lag_time(mn)" reuseLastValue="0"/>
    <field name="catchment_Vol(m3)" reuseLastValue="0"/>
    <field name="catchment_cr" reuseLastValue="0"/>
    <field name="catchment_model" reuseLastValue="0"/>
    <field name="catchment_q(m3/s)" reuseLastValue="0"/>
    <field name="id" reuseLastValue="0"/>
    <field name="last_refresh" reuseLastValue="0"/>
    <field name="name" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>COALESCE( "name", '&lt;NULL>' )</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
