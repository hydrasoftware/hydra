# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..base_dialog import BaseDialog, tr
import hydra.utility.string as string
from ..widgets.graph_widget import GraphWidget
from ..widgets.array_widget import ArrayWidget
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QTableWidgetItem
import os

class StorageEditor(BaseDialog):
    def __init__(self, project, geom, id=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "storage.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.__model = self.project.get_current_model()
        assert not self.__model == None
        self.__geom = geom
        if id==None:
            self.__id = self.__model.add_storage_node(self.__geom)
        else:
            self.__id = id
            if self.__geom is None:
                geom,= self.project.execute("""
                select geom
                from {}.storage_node
                where id={}""".format(self.__model.name, self.__id)).fetchone()

        name, zini, zs_array, contour, contraction_coef, comment = self.project.execute("""
            select name, zini, zs_array, contour, contraction_coef, comment
            from {}.storage_node
            where id={}""".format(self.__model.name, self.__id)).fetchone()

        self.name.setText(name)
        self.zini.setText(string.get_str(zini))
        self.contour.setText(str(contour))
        self.__contour_id=contour
        self.contraction_coef.setText(string.get_str(contraction_coef))
        self.notes.setText(comment)

        self.btn_zs_array.clicked.connect(self.gen_filling_curve)
        terrain_exists = self.project.execute("""select * from project.dem""").fetchall()
        if self.__contour_id is None or not terrain_exists:
            self.btn_zs_array.setEnabled(False)

        self.graph = GraphWidget(self.graph_placeholder, extend_zone=True)
        self.array = ArrayWidget([tr("Elevation (m)"), tr("Area (m2)")], [10,2], zs_array,
                draw_graph_func=self.__draw_graph, help_func=None, parent=self.array_placeholder)

    def __draw_graph(self, datas):
        if len(datas)>0:
            self.graph.clear()
            self.graph.add_symetric_line(
                [r[1]/2 for r in datas],
                [r[0] for r in datas],
                "g",
                self.name.text(),
                tr("Area (m2)"), tr("Z (m)"),
                bottom=True)
        else:
            self.graph.canvas.setVisible(False)

    def gen_filling_curve(self):
        if self.__contour_id:
            (arr,) = self.project.execute("""
                select project.filling_curve(geom)
                from {}.coverage
                where id={}""".format(self.__model.name, int(self.__contour_id))).fetchone()
            self.zini.setText(str(arr[0][0]))
            self.array.set_table_items(arr)

    def get_id(self):
        return self.__id

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.__model.update_storage_node(self.__id, self.__geom,
                                                self.array.get_table_items(), string.get_sql_float(self.zini.text()),
                                                string.get_sql_float(self.contraction_coef.text()), self.name.text(), comment=self.notes.text())
        BaseDialog.save(self)
        self.close()
