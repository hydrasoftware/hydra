from __future__ import absolute_import
from builtins import str
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from qgis.PyQt.QtWidgets import QDialog
from qgis.PyQt.QtCore import QCoreApplication
from .base_dialog import BaseDialog

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

class SimpleLinkDialog(BaseDialog):
    """Inherits base dialog, used for links to get control node and reach info if needed"""
    def __init__(self, project, geom, table, id=None, parent=None):
        BaseDialog.__init__(self, parent)
        self.project = project
        self.model = self.project.get_current_model()
        assert not self.model == None
        self.geom = geom
        if id is None:
            self.id = self.model.add_simple_link(table ,self.geom)
        else:
            self.id = id
            if self.geom is None:
                self.geom,= self.project.execute("""
                select geom
                from {}.{}
                where id={}""".format(self.model.name, table, self.id)).fetchone()

    def get_info(self):
        name_up = self.__get_node(self.model, self.id, 'up')
        name_down = self.__get_node(self.model, self.id, 'down')

        self.__info = tr(" - ") + str(name_up) + tr(" => ") + str(name_down)
        return self.__info

    def __get_node(self, model, id, up_down):
        name = self.project.execute("""
                                with id_link_res as(select {updown} from {model_name}._link where id={id_link} limit 1)
                                select name
                                from {model_name}._node, id_link_res
                                where {model_name}._node.id=id_link_res.{updown}
                                """.format(updown=up_down, model_name=model.name,id_link=id)).fetchone()
        if name is not None:
            name = name[0]
        return name

