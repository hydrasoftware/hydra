# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import webbrowser
import re
from collections import defaultdict
from qgis.PyQt import uic, QtCore
from PyQt5.QtCore import Qt
from qgis.PyQt.QtWidgets import QTableWidgetItem, QTreeWidgetItem, QFileDialog, QMessageBox, QProgressDialog, QAbstractItemView, QTreeWidget
from qgis.PyQt.QtGui import QFont, QColor
import hydra.utility.string as string
from hydra.gui.base_dialog import BaseDialog, tr
from hydra.gui.radar_rain import RadarRainDialog
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from hydra.gui.widgets.rain_table_widget import RainTableWidget
from hydra.gui.widgets.raingaged_table_widget import RainGagedTableWidget
from hydra.gui.widgets.graph_widget import GraphWidget
from hydra.gui.widgets.array_widget import ArrayWidget
from hydra.database.radar_rain import RainVrt
from hydra.utility.system import open_external

bold = QFont()
bold.setWeight(QFont.Bold)

_hydra_dir = os.path.join(os.path.expanduser('~'), ".hydra")

class RainManager(BaseDialog):

    curve_color = "#0066ff"

    def list_to_sql_array(self, py_list):
        return str(list(map(str, py_list))).replace('[', '{').replace(']', '}').replace('\'', '') if py_list else None

    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "rain_manager.ui"), self)

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.table_montana = HydraTableWidget(project,
            ("id", "name", "return_period_yr", "coef_a", "coef_b", "comment"),
            (tr("Id"), tr("Name"), tr("Return period (years)"), tr("a"), tr("b"), tr("Comment")),
            "project", "coef_montana",
            (self.new_montana, self.delete_montana), "", "id",
            self.table_montana_placeholder)
        self.table_montana.set_editable(True)
        self.table_montana.data_edited.connect(self.save_montana)

        self.table_hyetograph = HydraTableWidget(project,
            ("id", "name"),
            (tr("Id"), tr("Name")),
            "project", "intensity_curve_rainfall",
            (self.new_hyetograph, self.delete_hyetograph), "", "id",
            self.table_hyetograph_placeholder)
        self.table_hyetograph.set_editable(True)
        self.table_hyetograph.data_edited.connect(self.save_hyetograph)
        self.table_hyetograph.table.itemSelectionChanged.connect(self.set_active_hyetograph)
        self.active_hyetograph_id=None

        self.hyetograph_values = ArrayWidget(["Time (mn)", "Intensity (mm/h)"], [100, 2], None,
                draw_graph_func=self.__draw_hyetograph, parent=self.hyetograph_values_placeholder)
        self.hyetograph_values.setEnabled(False)

        self.graph_hyetograph = GraphWidget(self.graph_hyetograph_placeholder)

        self.table_caquot = RainTableWidget(project,
            ("id", "name", "montana"),
            (tr("Id"), tr("Name"), tr("Montana")),
            "project", "caquot_rainfall",
            (self.new_caquot, self.delete_caquot), "", "id",
            self.table_caquot_placeholder)
        self.table_caquot.set_editable(True)
        self.table_caquot.data_edited.connect(self.save_caquot)

        self.table_simple_tri = RainTableWidget(project,
            ("id", "name", "montana", "total_duration_mn", "peak_time_mn"),
            (tr("Id"), tr("Name"), tr("Montana"), tr("Total duration (min)"), tr("Peak time (min)")),
            "project", "single_triangular_rainfall",
            (self.new_simple_tri, self.delete_simple_tri), "", "id",
            self.table_simple_tri_placeholder)
        self.table_simple_tri.set_editable(True)
        self.table_simple_tri.data_edited.connect(self.save_simple_tri)
        self.graph_simple_tri = GraphWidget(self.graph_simple_placeholder)
        self.table_simple_tri.table.currentCellChanged.connect(self.__draw_simple_tri)
        self.table_simple_tri.table.itemSelectionChanged.connect(self.__draw_simple_tri)

        self.table_double_tri = RainTableWidget(project,
            ("id", "name", "peak_time_mn", "total_duration_mn", "montana_total", "peak_duration_mn", "montana_peak"),
            (tr("Id"), tr("Name"), tr("Peak time (min)"), tr("Total event\n Duration (min)"), tr("Total event\n Montana coefficient"),
            tr("Peak\n Duration (min)"), tr("Peak\n Montana coefficient"),),
            "project", "double_triangular_rainfall",
            (self.new_double_tri, self.delete_double_tri), "", "id",
            self.table_double_tri_placeholder)
        self.table_double_tri.set_editable(True)
        self.table_double_tri.data_edited.connect(self.save_double_tri)
        self.graph_double_tri = GraphWidget(self.graph_double_placeholder)
        self.table_double_tri.table.currentCellChanged.connect(self.__draw_double_tri)
        self.table_double_tri.table.itemSelectionChanged.connect(self.__draw_double_tri)

        self.table_gage_rainfall = RainGagedTableWidget(project,
            ("id", "name", "interpolation"),
            (tr("Id"), tr("Name"), tr("Interpolation mode")),
            "project", "gage_rainfall",
            (self.new_gage_rainfall, self.delete_gage_rainfall), "", "id",
            self.gage_rainfall_placeholder)
        self.table_gage_rainfall.set_editable(True)
        self.table_gage_rainfall.data_edited.connect(self.save_gage_rainfall)
        #self.table_gage_rainfall.interpolation_changed.connect(self.refresh_rain_gages_table)
        self.table_gage_rainfall.table.itemSelectionChanged.connect(self.set_active_gage_rainfall)
        self.active_gage_rainfall_id=None
        self.gage_list.itemSelectionChanged.connect(self.set_active_rain_gage)
        self.gage_list.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.gage_list.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.gage_list.resizeColumnsToContents()
        self.gage_list.horizontalHeader().setStretchLastSection(True)
        self.gage_list.setEnabled(False)
        self.active_rain_gage_id=None
        self.gage_data = ArrayWidget(["Time (mn)", "Cumulated height (mm)"], [1000,2], None,
                draw_graph_func=self.__draw_gage, help_func=None, parent=self.gage_data_placeholder)
        self.gage_data.setEnabled(False)
        self.graph_gage = GraphWidget(self.graph_gage_placeholder)

        self.btn_thiessen.clicked.connect(self.__export_thiessen_coefficients)

        self.table_gage_file_rainfall = RainGagedTableWidget(project,
            ("id", "name", "interpolation", "file"),
            (tr("Id"), tr("Name"), tr("Interpolation mode"), tr("Rainfall file")),
            "project", "gage_file_rainfall",
            (self.new_gage_file_rainfall, self.delete_gage_file_rainfall, self.edit_gage_file_rainfall, self.help_gage_file_rainfall), "", "id",
            self.gage_file_rainfall_placeholder, showEdit=True, showHelp=True, file_columns=[3])
        self.table_gage_file_rainfall.set_editable(True)
        self.table_gage_file_rainfall.data_edited.connect(self.save_gage_file_rainfall)
        self.table_gage_file_rainfall.table.itemSelectionChanged.connect(self.set_active_gage_file_rainfall)
        self.active_gage_file_rainfall_id=None

        self.btn_add_radar_rain.clicked.connect(self.new_radar_rain)
        self.btn_delete_radar_rain.clicked.connect(self.delete_radar_rain)
        self.btn_add_tiffs.clicked.connect(self.__add_tiffs)
        self.btn_generate_rain_file.clicked.connect(self.__export_selected_rain)
        self.radar_rain_tree.itemClicked.connect(self.set_active_radar_rain)
        self.radar_rain_tree.itemDoubleClicked.connect(self.unit_cc_double_clicked)
        self.radar_rain_tree.setEditTriggers(QTreeWidget.NoEditTriggers)
        self.radar_rain_tree.itemChanged.connect(self.rain_tree_item_changed)

        self.refresh_radar_rain()
        self.set_active_radar_rain()

        self.table_mages_rainfall = HydraTableWidget(project,
            ("id", "name", "rain_file"),
            (tr("Id"), tr("Name"), tr("Pluvio file")),
            "project", "mages_rainfall",
            (self.new_mages_rainfall, self.delete_mages_rainfall), "", "id",
            self.mages_rainfall_placeholder, file_columns=[2])
        self.table_mages_rainfall.set_editable(True)
        self.table_mages_rainfall.data_edited.connect(self.save_mages_rainfall)
        self.table_mages_rainfall.table.itemSelectionChanged.connect(self.set_active_mages_rainfall)
        self.active_mages_rainfall_id=None

    def select_montana(self, rain_name):
        self.__select_in_table(self.table_montana.table, rain_name)
        self.tabWidget.setCurrentIndex(0)

    def select_rain(self, rain_name):
        rain_type, = self.project.execute("""
                select rainfall_type from project._rainfall where name='{}'
            """.format(rain_name)).fetchone()

        if rain_type=="caquot":
            self.__select_in_table(self.table_caquot.table, rain_name)
            self.tabWidget.setCurrentIndex(0)
            self.tabWidget_2.setCurrentIndex(0)
        elif rain_type=="single_triangular":
            self.__select_in_table(self.table_simple_tri.table, rain_name)
            self.tabWidget.setCurrentIndex(0)
            self.tabWidget_2.setCurrentIndex(1)
        elif rain_type=="double_triangular":
            self.__select_in_table(self.table_double_tri.table, rain_name)
            self.tabWidget.setCurrentIndex(0)
            self.tabWidget_2.setCurrentIndex(2)
        elif rain_type=="intensity_curve":
            self.__select_in_table(self.table_hyetograph.table, rain_name)
            self.tabWidget.setCurrentIndex(1)
        elif rain_type=="gage":
            self.__select_in_table(self.table_gage_rainfall.table, rain_name)
            self.tabWidget.setCurrentIndex(2)
        elif rain_type=="gage_file":
            self.__select_in_table(self.table_gage_file_rainfall.table, rain_name)
            self.tabWidget.setCurrentIndex(2)
        elif rain_type=="radar":
            self.__select_in_tree(self.radar_rain_tree, rain_name)
            self.tabWidget.setCurrentIndex(3)
        elif rain_type=="mages":
            self.__select_in_table(self.table_mages_rainfall.table, rain_name)
            self.tabWidget.setCurrentIndex(4)

    def __select_in_table(self, table, text):
        for i in range(0, table.rowCount()):
            if table.item(i,1).text()==text:
                table.setCurrentCell(i,0)

    def __select_in_tree(self, tree, text):
        for i in range(0, tree.topLevelItemCount ()):
            if tree.topLevelItem(i).text(1)==text:
                tree.setCurrentItem(tree.topLevelItem(i))

    def delete_caquot(self):
        self.table_caquot.del_selected_row()

    def new_caquot(self):
        self.table_caquot.add_row()

    def save_caquot(self):
        self.table_caquot.save_selected_row()

    def delete_simple_tri(self):
        self.table_simple_tri.del_selected_row()

    def new_simple_tri(self):
        self.table_simple_tri.add_row()

    def save_simple_tri(self):
        self.table_simple_tri.save_selected_row()

    def __draw_simple_tri(self):
        items = self.table_simple_tri.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_simple_tri.table.row(items[0])
            try:
                name = self.table_simple_tri.table.item(selected_row,1).text()
                duration = float(self.table_simple_tri.table.item(selected_row,3).text())
                peak_time = float(self.table_simple_tri.table.item(selected_row,4).text())
                id_montana = self.table_simple_tri.table.item(selected_row,2).text()
                a, b = self.project.execute("""select coef_a, coef_b from project.coef_montana
                    where id={}""".format(id_montana)).fetchone()
                i_moy = float(a) * (duration**float(b)) * 60
                i_peak = 2.0 * i_moy
                self.graph_simple_tri.clear()
                self.graph_simple_tri.add_filled_line([0, peak_time, duration], [0, i_peak, 0],
                    RainManager.curve_color, name, tr("t (min)"), tr("i (mm/h)"))
            except:
                self.graph_simple_tri.canvas.setVisible(False)

    def delete_double_tri(self):
        self.table_double_tri.del_selected_row()

    def new_double_tri(self):
        self.table_double_tri.add_row()

    def save_double_tri(self):
        self.table_double_tri.save_selected_row()

    def __draw_double_tri(self):
        items = self.table_double_tri.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_double_tri.table.row(items[0])
            try:
                name = self.table_double_tri.table.item(selected_row,1).text()
                peak_time = float(self.table_double_tri.table.item(selected_row,2).text())

                duration1 = float(self.table_double_tri.table.item(selected_row,3).text())
                id_montana1 = self.table_double_tri.table.item(selected_row,4).text()
                if not id_montana1:
                    return
                a1, b1 = self.project.execute("""select coef_a, coef_b from project.coef_montana
                    where id={}""".format(id_montana1)).fetchone()
                i_moy1 = float(a1) * (duration1**float(b1)) * 60
                i_peak1 = 2.0 * i_moy1

                duration2 = float(self.table_double_tri.table.item(selected_row,5).text())
                id_montana2 = self.table_double_tri.table.item(selected_row,6).text()
                if not id_montana2:
                    return
                a2, b2 = self.project.execute("""select coef_a, coef_b from project.coef_montana
                    where id={}""".format(id_montana2)).fetchone()
                i_moy2 = float(a2) * (duration2**float(b2)) * 60
                i_peak2 = 2.0 * i_moy2

                self.graph_double_tri.clear()
                self.graph_double_tri.add_filled_line(
                    [0, peak_time, duration1],
                    [0, i_peak1, 0],
                    RainManager.curve_color, name, tr("t (min)"), tr("i (mm/h)"))
                self.graph_double_tri.add_filled_line(
                    [peak_time-duration2/2, peak_time, peak_time+duration2/2],
                    [0, i_peak2, 0],
                    RainManager.curve_color, name, tr("t (min)"), tr("i (mm/h)"))
            except:
                self.graph_double_tri.canvas.setVisible(False)

    def __draw_hyetograph(self, datas):
        items = self.table_hyetograph.table.selectedItems()
        if len(items)>0:
            if len(datas)>0:
                    self.graph_hyetograph.clear()
                    self.graph_hyetograph.add_filled_line(
                        [r[0] for r in datas],
                        [r[1] for r in datas],
                        RainManager.curve_color,
                        items[1].text(),
                        tr("t (min)"), tr("i (mm/h)"))
            else:
                self.graph_hyetograph.canvas.setVisible(False)

    def set_active_hyetograph(self):
        items = self.table_hyetograph.table.selectedItems()
        if len(items)>0:
            if self.active_hyetograph_id is not None:
                self.save_hyetograph()

            self.active_hyetograph_id = items[0].text()
            t_mn_intensity_mmh, = self.project.execute("""
                select t_mn_intensity_mmh from project.intensity_curve_rainfall where id={};
            """.format(self.active_hyetograph_id)).fetchone()
            self.hyetograph_values.setEnabled(True)
            self.hyetograph_values.set_table_items(t_mn_intensity_mmh)

    def delete_hyetograph(self):
        if self.active_hyetograph_id is not None:
            self.table_hyetograph.del_selected_row()

    def new_hyetograph(self):
        if self.active_hyetograph_id is not None:
            self.save_hyetograph()
        self.table_hyetograph.add_row()
        self.set_active_hyetograph()

    def save_hyetograph(self):
        if self.active_hyetograph_id is not None:
            self.table_hyetograph.save_selected_row()
            items = self.list_to_sql_array(self.hyetograph_values.get_table_items())
            if items is not None:
                self.project.execute("""update project.intensity_curve_rainfall
                    set t_mn_intensity_mmh='{}' where id={}""".format(
                    items, self.active_hyetograph_id))

    def delete_montana(self):
        self.table_montana.del_selected_row()

    def new_montana(self):
        self.table_montana.add_row(('default', 0, 1, -1))
        self.table_caquot.update_data()
        self.table_simple_tri.update_data()
        self.table_double_tri.update_data()

    def save_montana(self):
        self.table_montana.save_selected_row()
        self.table_caquot.update_data()
        self.table_simple_tri.update_data()
        self.table_double_tri.update_data()

    def save(self):
        if self.active_hyetograph_id is not None:
            self.save_hyetograph()
        if self.active_gage_rainfall_id is not None:
            self.save_gage_rainfall()
        BaseDialog.save(self)
        self.close()

    def __draw_gage(self, datas):
        if self.active_gage_rainfall_id is not None:
            items = self.gage_list.selectedItems()
            if len(items)>0:
                selected_row = self.gage_list.row(items[0])
                if len(datas)>0:
                    self.graph_gage.clear()
                    self.graph_gage.add_filled_line(
                        [r[0] for r in datas],
                        [r[1] for r in datas],
                        RainManager.curve_color,
                        self.gage_list.item(selected_row, 1).text(),
                        tr("Time (mn)"), tr("Cumulated height (mm)"))
                else:
                    self.graph_gage.canvas.setVisible(False)
            else:
                self.graph_gage.canvas.setVisible(False)
        else:
            self.graph_gage.canvas.setVisible(False)

    def set_active_rain_gage(self):
        if self.active_gage_rainfall_id is not None:
            items = self.gage_list.selectedItems()
            if len(items)>0:
                selected_row = self.gage_list.row(items[0])
                if self.active_rain_gage_id is not None:
                    self.save_rain_gage()

                self.active_rain_gage_id = self.gage_list.item(selected_row,0).text()
                result = self.project.execute("""
                    select t_mn_hcum_mm from project.gage_rainfall_data
                    where rainfall={} and rain_gage={};
                """.format(self.active_gage_rainfall_id,self.active_rain_gage_id)).fetchone()
                t_mn_hcum_mm = result[0] if result else []
                self.gage_data.set_table_items(t_mn_hcum_mm)
                self.gage_data.setEnabled(True)

    def save_rain_gage(self):
        if not (self.active_rain_gage_id is None or self.active_gage_rainfall_id is None):
            items = self.list_to_sql_array(self.gage_data.get_table_items())
            if items is not None:
                self.project.execute("""update project.gage_rainfall_data
                    set t_mn_hcum_mm='{}' where rainfall={} and rain_gage={};""".format(
                    items, self.active_gage_rainfall_id,self.active_rain_gage_id))

    def set_active_gage_rainfall(self):
        items = self.table_gage_rainfall.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_gage_rainfall.table.row(items[0])
            if self.active_gage_rainfall_id is not None:
                self.save_gage_rainfall()
            self.active_gage_rainfall_id = self.table_gage_rainfall.table.item(selected_row,0).text()
            self.refresh_rain_gages_table()

    def refresh_rain_gages_table(self):
        self.gage_list.setEnabled(True)
        self.gage_list.setRowCount(0)
        self.gage_data.set_table_items([[0,0]])

        # thiessen_selected = (self.table_gage_rainfall.table.item(self.table_gage_rainfall.table.currentRow(),2).text()=='thiessen')
        rain_gages = self.project.execute("""select rg.id, rg.name, grd.is_active
            from project.rain_gage as rg
            left join project.gage_rainfall_data as grd on rg.id = grd.rain_gage
            and grd.rainfall={}
            order by rg.name;""".format(self.active_gage_rainfall_id)
            ).fetchall()

        n = len(rain_gages)
        self.gage_list.setRowCount(n)
        for row in range(0, n):
            self.gage_list.setItem(row,0,QTableWidgetItem(string.get_str(rain_gages[row][0])))
            self.gage_list.setItem(row,1,QTableWidgetItem(string.get_str(rain_gages[row][1])))
            self.gage_list.setItem(row,2,QTableWidgetItem(""))
            if rain_gages[row][2]:# or thiessen_selected:
                self.gage_list.item(row,2).setCheckState(QtCore.Qt.Checked)
            else:
                self.gage_list.item(row,2).setCheckState(QtCore.Qt.Unchecked)
            # if thiessen_selected:
                # self.gage_list.item(row,2).setFlags(QtCore.Qt.ItemIsSelectable)
            # else:
                # self.gage_list.item(row,2).setFlags(QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)

        self.active_rain_gage_id = None

    def get_rain_gages_table_items(self):
        n = self.gage_list.rowCount()
        result = list()
        index_list=0
        for i in range(0, n):
            result.append(list())
            result[index_list].append(self.gage_list.item(i,0).text())
            result[index_list].append(self.gage_list.item(i,1).text())
            result[index_list].append(self.gage_list.item(i,2).checkState()==QtCore.Qt.Checked)
            index_list = index_list + 1
        return result

    def new_gage_rainfall(self):
        if self.active_gage_rainfall_id is not None:
            self.save_gage_rainfall()
        self.table_gage_rainfall.add_row()
        self.set_active_gage_rainfall()

    def delete_gage_rainfall(self):
        if self.active_gage_rainfall_id is not None:
            self.table_gage_rainfall.del_selected_row()
            self.active_gage_rainfall_id = None

    def save_gage_rainfall(self):
        if self.active_gage_rainfall_id is not None:
            if self.active_rain_gage_id is not None:
                self.save_rain_gage()
            self.table_gage_rainfall.save_selected_row()
            items = self.get_rain_gages_table_items()
            if len(items)>0:
                values = list()
                for item in items:
                    values.append("({},{},{})".format(self.active_gage_rainfall_id, item[0], item[2]))
                sql= """insert into project.gage_rainfall_data(rainfall,rain_gage,is_active)
                    values {} on conflict (rainfall,rain_gage)
                    do update set is_active=excluded.is_active;
                    """.format(','.join(values))
                self.project.execute(sql)

    def write_thiessen_file(self, model):
        thiessen_coefficients = self.project.execute("""
                select c.name, rg.name, (ST_Area(ST_Intersection(c.geom, rg.geom)) / ST_Area(c.geom)) as alpha
                from {m}.catchment as c, {m}.thiessen_rain_gage as rg
                where ST_intersects(c.geom, rg.geom)
                order by c.name, rg.name
                """.format(m=model)).fetchall()

        res = self.project.execute("""
                select name from project.rain_gage order by name
                """).fetchall()
        rain_gages = [rg for (rg,) in res]

        # creates a dict: {catchment_name: {rain_gage: coefficient}}
        catchments_dict = defaultdict(dict)
        for catchment, rain_gage, coef in thiessen_coefficients:
            catchments_dict[catchment][rain_gage] = str(round(coef, 3))

        file = os.path.join(self.project.data_dir, model+"_thiessen.csv")

        # init a progress bar. 1 step per catchment written
        progress = QProgressDialog(self)
        progress.setWindowTitle(tr("Exporting Thiessen coefficients"))
        progress.setLabelText(tr('Init...'))
        progress.setMinimum(0)
        progress.setMaximum(len(catchments_dict))
        progress.setCancelButtonText(None)
        progress.show()
        progress.setWindowModality(1) # set QProgress dialog as modal

        i=0
        with open(file, 'w') as f:
            # header
            f.write(";".join(["CATCHMENT"]+rain_gages))
            f.write("\n")
            # one line per catchment
            for catchment, coef_dict in catchments_dict.items():
                # progres bar +1
                i+=1
                progress.setLabelText(tr('Catchment {}').format(catchment))
                progress.setValue(i)
                progress.show()

                # write line associated with catchment
                line = [catchment]
                for rain_gage in rain_gages:
                    if rain_gage in list(coef_dict.keys()):
                        line.append(str(coef_dict[rain_gage]))
                    else:
                        line.append(str(0))

                f.write(";".join(line))
                f.write("\n")

        progress.close()
        return file

    def __export_thiessen_coefficients(self):
        res=[]
        if self.project.get_models():
            for model in self.project.get_models():
                f=self.write_thiessen_file(model)
                res.append(f)
            if res:
                QMessageBox.information(self, tr('Thiessen coefficients file generated'),
                                        tr("""Successfully created files {} in folder {}.""").format(', '.join([os.path.basename(file) for file in res]), os.path.dirname(res[0])), QMessageBox.Ok)

    def set_active_gage_file_rainfall(self):
        items = self.table_gage_file_rainfall.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_gage_file_rainfall.table.row(items[0])
            if self.active_gage_file_rainfall_id is not None:
                self.save_gage_file_rainfall()
            self.active_gage_file_rainfall_id = self.table_gage_file_rainfall.table.item(selected_row,0).text()

    def delete_gage_file_rainfall(self):
        if self.active_gage_file_rainfall_id is not None:
            self.table_gage_file_rainfall.del_selected_row()
            self.active_gage_file_rainfall_id = None

    def new_gage_file_rainfall(self):
        if self.active_gage_file_rainfall_id is not None:
            self.save_gage_file_rainfall()
        file_url, __ = QFileDialog.getOpenFileName(self, tr("Select a rain file"), self.project.directory)
        if file_url:
            self.project.execute("""insert into project.gage_file_rainfall(file) values ('{f}');""".format(f=self.project.pack_path(file_url)))
        self.table_gage_file_rainfall.update_data()
        self.set_active_gage_file_rainfall()

    def save_gage_file_rainfall(self):
        if self.active_gage_file_rainfall_id is not None:
            self.table_gage_file_rainfall.save_selected_row()

    def edit_gage_file_rainfall(self):
        file = self.table_gage_file_rainfall.table.item(self.table_gage_file_rainfall.table.currentRow(),3).text()
        open_external(file)

    def help_gage_file_rainfall(self):
        webbrowser.open('http://hydra-software.net/docs/notes_techniques/NT53.pdf', new=0)

    def refresh_radar_rain(self):
        self.radar_rain_tree.itemChanged.disconnect(self.rain_tree_item_changed)
        self.radar_rain_tree.clear()

        header = QTreeWidgetItem(["Id","Name", "File path"])
        self.radar_rain_tree.setHeaderItem(header)

        rains = self.project.execute("""select id, name, file, unit_correction_coef from project.radar_rainfall order by id asc;""").fetchall()
        attributes = {"x0": "X origin (m):",
                        "y0": "Y origin (m):",
                        "dx": "Horizontal pixel size (m):",
                        "dy": "Vertical pixel size (m):",
                        "nx": "Columns:",
                        "ny": "Rows:",
                        "t0": "Start time:",
                        "dt": "Timestep (s):",
                        "band_number": "Steps number:"}

        for radar_rain in rains:
            rain_node = QTreeWidgetItem(self.radar_rain_tree)
            rain_node.setExpanded(True)
            file_path = self.project.unpack_path(radar_rain[2])
            for i in range(2): # adds id, name and file in first line
                rain_node.setFont(i, bold)
                rain_node.setText(i, str(radar_rain[i]))
            rain_node.setFont(2, bold)
            rain_node.setText(2, file_path)
            if not (os.path.isfile(file_path) and os.path.isfile(file_path.replace('.vrt', '.time'))):
                rain_node.setForeground(2, QColor(255,0,0))
            else:
                rain_raster = RainVrt(file_path, self.project)
                for key, legend in attributes.items():
                    param_node = QTreeWidgetItem(rain_node)
                    param_node.setText(1,legend)
                    param_node.setText(2,str(getattr(rain_raster, key)))
                    param_node.setTextAlignment(2,2)
                unit_cc_node = QTreeWidgetItem(rain_node)
                unit_cc_node.setText(1,"Unit correction coefficient")
                unit_cc_node.setText(2,str(radar_rain[3]))
                unit_cc_node.setTextAlignment(2,2)
                unit_cc_node.setFlags(unit_cc_node.flags() | QtCore.Qt.ItemIsEditable)

        for j in range(header.columnCount()):
            header.setTextAlignment(j, Qt.AlignCenter)
            self.radar_rain_tree.resizeColumnToContents(j)
        self.radar_rain_tree.setColumnWidth(1, self.radar_rain_tree.columnWidth(1) + 50)

        self.radar_rain_tree.itemChanged.connect(self.rain_tree_item_changed)

    def unit_cc_double_clicked(self, item, column):
        if column==2:
            self.radar_rain_tree.editItem(item, column)
    
    def rain_tree_item_changed(self, item, column):
        if item.parent():
            assert item.data(1, QtCore.Qt.EditRole) == "Unit correction coefficient"
            self.project.execute("update project.radar_rainfall set unit_correction_coef=%s where id=%s",
                (item.data(2, QtCore.Qt.EditRole), item.parent().data(0, QtCore.Qt.EditRole)))

    def set_active_radar_rain(self):
        selected_item = self.radar_rain_tree.currentItem()
        if selected_item is not None:
            if selected_item.parent() is not None:
                selected_item = self.radar_rain_tree.currentItem().parent()
            self.active_radar_rain_id = int(selected_item.text(0))
            self.active_radar_rain_name = selected_item.text(1)
            self.active_radar_rain_file = selected_item.text(2)
        else:
            self.active_radar_rain_id = None
            self.active_radar_rain_name = None
            self.active_radar_rain_file = None

    def new_radar_rain(self):
        file_url, __ = QFileDialog.getOpenFileName(self, tr("Select a rain file"), self.project.directory, tr("Hydra rain (*.vrt)"))
        if file_url:
            name, ext = os.path.splitext(os.path.basename(file_url))
            if not os.path.isfile(file_url.replace('.vrt', '.time')):
                QMessageBox.critical(self, tr('Radar rain error'), tr('Error importing file {}. Required file {} not found.').format(os.path.basename(file_url), os.path.basename(file_url.replace('.vrt', '.time'))), QMessageBox.Ok)
            else:
                self.project.execute("""insert into project.radar_rainfall(name, file) values ('{n}', '{f}');""".format(n=name, f=self.project.pack_path(file_url)))
        self.refresh_radar_rain()

    def __add_tiffs(self):
        files, __ = QFileDialog.getOpenFileNames(self, tr("Select one or more files to open"), self.project.directory, tr("Images (*.asc *.tiff *.tif)"))
        brut_name = os.path.splitext(os.path.basename(files[0]))[0]
        no_timestamp_name =  re.sub(r'\d{12}', '', brut_name)
        template_name = re.sub(r'\d{12}', 12*'?', brut_name)
        if files:
            new_radar_rain_dialog = RadarRainDialog(self, no_timestamp_name, self.project.srid, template_name)
            new_radar_rain_dialog.exec_()
            name, srid, template = new_radar_rain_dialog.get_result()
            if name and srid and files and (12*'?' in template):
                vrt = self.project.build_rain_multivrt(name, files, template, srid)
                del vrt
                self.refresh_radar_rain()

    def delete_radar_rain(self):
        if self.active_radar_rain_id is not None:
            self.project.execute("""delete from project.radar_rainfall where id={};""".format(self.active_radar_rain_id))
            self.refresh_radar_rain()

    def write_rain_file(self, model):
        rain_raster = RainVrt(self.active_radar_rain_file, self.project)
        unit_cc = self.project.execute("""select unit_correction_coef from project.radar_rainfall where id={}""".format(self.active_radar_rain_id)).fetchone()[0]
        file = os.path.join(self.project.radar_rain.data_directory, self.active_radar_rain_name+"_"+model+".rad")
        catchments = self.project.execute("""select id, name from {}.catchment""".format(model)).fetchall()

        progress = QProgressDialog(self)
        progress.setWindowTitle(tr("Exporting rain"))
        progress.setLabelText(tr('Init...'))
        progress.setMinimum(0)
        progress.setMaximum(len(catchments))
        progress.setCancelButtonText(None)
        progress.show()
        progress.setWindowModality(1) # set QProgress dialog as modal

        i=0
        with open(file, 'w') as f:
            for catchment in catchments:
                i+=1
                progress.setLabelText(tr('Catchment {}').format(catchment[1]))
                progress.setValue(i)
                progress.show()
                f.write(rain_raster.export_hyetogram(catchment, model, unit_cc))
        progress.close()
        return file

    def __export_selected_rain(self):
        if self.active_radar_rain_id is not None:
            rain = self.project.execute("""select name, file from project.radar_rainfall where id={};""".format(self.active_radar_rain_id)).fetchone()
            rain_raster = RainVrt(self.project.unpack_path(rain[1]), self.project)
            del rain_raster
            res=[]
            if self.project.get_models():
                for model in self.project.get_models():
                    f=self.write_rain_file(model)
                    res.append(f)
                if res:
                    QMessageBox.information(self, tr('Rain file generated'),
                                            tr("""Successfully created files {} in folder {}.""").format(', '.join([os.path.basename(file) for file in res]), os.path.dirname(res[0])), QMessageBox.Ok)

    def set_active_mages_rainfall(self):
        items = self.table_mages_rainfall.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_mages_rainfall.table.row(items[0])
            if self.active_mages_rainfall_id is not None:
                self.save_mages_rainfall()
            self.active_mages_rainfall_id = self.table_mages_rainfall.table.item(selected_row,0).text()

    def delete_mages_rainfall(self):
        if self.active_mages_rainfall_id is not None:
            self.table_mages_rainfall.del_selected_row()
            self.active_mages_rainfall_id = None

    def new_mages_rainfall(self):
        if self.active_mages_rainfall_id is not None:
            self.save_mages_rainfall()
        file_url, __ = QFileDialog.getOpenFileName(self, tr("Select a rain file"), self.project.directory)
        if file_url:
            self.project.execute("""insert into project.mages_rainfall(rain_file) values ('{f}');""".format(f=self.project.pack_path(file_url)))
        self.table_mages_rainfall.update_data()
        self.set_active_mages_rainfall()

    def save_mages_rainfall(self):
        if self.active_mages_rainfall_id is not None:
            self.table_mages_rainfall.save_selected_row()
