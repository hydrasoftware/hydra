/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update hydra.metadata set version = '1.6';
;;

create or replace function project.model_instead_fct()
returns trigger
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_model
    for statement in create_model(TD['new']['name'], $srid, '$version'):
        plpy.execute(statement)
$$$$
;;

/* ********************************************** */
/*  Scenario table time formats                   */
/* ********************************************** */

-- dt_hydrol_mn
alter table project.scenario alter column dt_hydrol_mn drop default;
alter table project.scenario alter column dt_hydrol_mn type interval using date_trunc('second', to_timestamp((dt_hydrol_mn-60)*60)) - timestamp '1970-01-01 00:00:00';
alter table project.scenario alter column dt_hydrol_mn set default '00:05:00';
;;

-- tfin_hr
alter table project.scenario alter column tfin_hr drop default;
alter table project.scenario alter column tfin_hr type interval using date_trunc('minute', to_timestamp((tfin_hr-1)*3600)) - timestamp '1970-01-01 00:00:00';
alter table project.scenario alter column tfin_hr set default '12:00:00';
alter table project.scenario alter column tfin_hr set not null;
alter table project.scenario add constraint scenario_tfin_hr_check check (date_part('seconds', tfin_hr) = 0);
;;

--tini_hydrau_hr
alter table project.scenario alter column tini_hydrau_hr drop default;
alter table project.scenario alter column tini_hydrau_hr type interval using date_trunc('minute', to_timestamp((tini_hydrau_hr-1)*3600)) - timestamp '1970-01-01 00:00:00';
alter table project.scenario alter column tini_hydrau_hr set default '12:00:00';
alter table project.scenario alter column tini_hydrau_hr set not null;
alter table project.scenario add constraint scenario_tini_hydrau_hr_check check (date_part('seconds', tini_hydrau_hr) = 0);
;;

-- dtmin_hydrau_hr
alter table project.scenario alter column dtmin_hydrau_hr drop default;
alter table project.scenario alter column dtmin_hydrau_hr type interval using date_trunc('second', to_timestamp((dtmin_hydrau_hr-1)*3600)) - timestamp '1970-01-01 00:00:00';
alter table project.scenario alter column dtmin_hydrau_hr set default '00:00:15';
alter table project.scenario alter column dtmin_hydrau_hr set not null;
;;

-- dtmax_hydrau_hr
alter table project.scenario alter column dtmax_hydrau_hr drop default;
alter table project.scenario alter column dtmax_hydrau_hr type interval using date_trunc('second', to_timestamp((dtmax_hydrau_hr-1)*3600)) - timestamp '1970-01-01 00:00:00';
alter table project.scenario alter column dtmax_hydrau_hr set default '00:05:00';
alter table project.scenario alter column dtmax_hydrau_hr set not null;
;;

-- tsave_hr
alter table project.scenario alter column tsave_hr drop default;
alter table project.scenario alter column tsave_hr type interval using date_trunc('minute', to_timestamp((tsave_hr-1)*3600)) - timestamp '1970-01-01 00:00:00';
alter table project.scenario alter column tsave_hr set default '00:00:00';
alter table project.scenario add constraint scenario_tsave_hr_check check (date_part('seconds', tsave_hr) = 0);
;;

-- trstart_hr
alter table project.scenario alter column trstart_hr drop default;
alter table project.scenario alter column trstart_hr type interval using date_trunc('minute', to_timestamp((trstart_hr-1)*3600)) - timestamp '1970-01-01 00:00:00';
alter table project.scenario alter column trstart_hr set default '00:00:00';
alter table project.scenario add constraint scenario_trstart_hr_check check (date_part('seconds', trstart_hr) = 0);
;;

-- tbegin_output_hr
alter table project.scenario alter column tbegin_output_hr drop default;
alter table project.scenario alter column tbegin_output_hr type interval using date_trunc('minute', to_timestamp((tbegin_output_hr-1)*3600)) - timestamp '1970-01-01 00:00:00';
alter table project.scenario alter column tbegin_output_hr set default '00:00:00';
alter table project.scenario alter column tbegin_output_hr set not null;
alter table project.scenario add constraint scenario_tbegin_output_hr_check check (date_part('seconds', tbegin_output_hr) = 0);
;;

-- tend_output_hr
alter table project.scenario alter column tend_output_hr drop default;
alter table project.scenario alter column tend_output_hr type interval using date_trunc('minute', to_timestamp((tend_output_hr-1)*3600)) - timestamp '1970-01-01 00:00:00';
alter table project.scenario alter column tend_output_hr set default '12:00:00';
alter table project.scenario alter column tend_output_hr set not null;
alter table project.scenario add constraint scenario_tend_output_hr_check check (date_part('seconds', tend_output_hr) = 0);
;;

-- dt_output_hr
alter table project.scenario alter column dt_output_hr drop default;
alter table project.scenario alter column dt_output_hr type interval using date_trunc('minute', to_timestamp((dt_output_hr-1)*3600)) - timestamp '1970-01-01 00:00:00';
alter table project.scenario alter column dt_output_hr set default '00:05:00';
alter table project.scenario alter column dt_output_hr set not null;
alter table project.scenario add constraint scenario_dt_output_hr_check check (date_part('seconds', dt_output_hr) = 0);
;;

-- t_affin_start_hr
alter table project.scenario alter column t_affin_start_hr drop default;
alter table project.scenario alter column t_affin_start_hr type interval using date_trunc('minute', to_timestamp((t_affin_start_hr-1)*3600)) - timestamp '1970-01-01 00:00:00';
alter table project.scenario alter column t_affin_start_hr set default '00:00:00';
alter table project.scenario alter column t_affin_start_hr set not null;
alter table project.scenario add constraint scenario_t_affin_start_hr_check check (date_part('seconds', t_affin_start_hr) = 0);
;;

/* ********************************************** */
/*  Street removal                                */
/* ********************************************** */

delete from hydra.link_type where name='street';
;;

delete from hydra.coverage_type where name in ('crossroad', 'reach_end', 'left_flood_plain', 'right_flood_plain');
;;

/* ********************************************** */
/*  MAGES rainfall                                */
/* ********************************************** */

--isolated--
alter type hydra_rainfall_type add value if not exists 'mages';
;;

insert into hydra.rainfall_type(id, name) values (7, 'mages');
;;

create table project._mages_rainfall(
    id integer unique not null,
    rainfall_type hydra_rainfall_type not null,
        foreign key (id, rainfall_type) references project._rainfall(id, rainfall_type),
        check(rainfall_type='mages'),
    rain_file character varying(256)
    );
;;

create or replace view project.mages_rainfall as
    select p.id, c.rain_file, p.name
    from project._mages_rainfall as c, project._rainfall as p
    where p.id = c.id
;;

create or replace function project.mages_rainfall_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into project._rainfall(rainfall_type, name)
                values('mages', coalesce(new.name, 'define_later')) returning id into id_;
            update project._rainfall set name = 'RAIN_'||id_::varchar
                where name = 'define_later' and id = id_;
            insert into project._mages_rainfall
                values (id_, 'mages', new.rain_file);
            update project._rainfall set validity = (select (rain_file is not null) from  project._mages_rainfall where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update project._rainfall set name=new.name where id=old.id;
            update project._mages_rainfall set rain_file=new.rain_file where id=old.id;
            update project._rainfall set validity = (select (rain_file is not null) from  project._mages_rainfall where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from project._mages_rainfall where id=old.id;
            delete from project._rainfall where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger project_mages_rainfall_trig
    instead of insert or update or delete on project.mages_rainfall
       for each row execute procedure project.mages_rainfall_fct()
;;

drop table if exists project.radar_grid;
;;

create table project.radar_grid(
    id integer not null default 1 unique check (id=1),
    origin_point geometry('POINT',$srid) not null default ST_SetSRID('POINT(0 0)'::geometry, $srid) check(ST_IsValid(origin_point)),
    dx real not null default 1000  check (dx>=0),
    dy real not null default 1000  check (dy>=0),
    nx integer not null default 0 check (nx>=0),
    ny integer not null default 0 check (ny>=0)
)
;;

insert into project.radar_grid default values;
;;

create or replace view project.radar_grid_display as(
    with grid as (
            select ST_X(origin_point) as x0, ST_Y(origin_point) as y0, nx, ny, dx, dy from project.radar_grid
            )
    select
        row_number() OVER () AS id,
        i + 1 as row,
        j + 1 as col,
        ST_Translate(cell, x0 + i * dx, y0 - j * dy) as geom
	from grid,
		generate_series(0, nx - 1) AS i,
	    generate_series(0, ny - 1) AS j,
	    (select ST_SetSRID(('POLYGON((0 0, 0 -'||dy||', '||dx||' -'||dy||', '||dx||' 0,0 0))')::geometry, $srid) as cell from grid) as poly
)
;;

/* ********************************************** */
/*  Dry inflow changes                            */
/* ********************************************** */

do
$$$$
    begin
        if not exists (select 1 from pg_type where typname = 'hydra_dry_inflow_period') then
            create type public.hydra_dry_inflow_period as enum ('weekday', 'weekend');
        end if;
    end
$$$$;
;;

create table if not exists hydra.dry_inflow_period(
    name hydra_dry_inflow_period primary key,
    id integer unique,
    description varchar
)
;;

insert into hydra.dry_inflow_period(name, id) values ('weekday', 1), ('weekend', 2) on conflict do nothing;
;;

alter table project.dry_inflow_sector add column if not exists vol_curve integer references project.dry_inflow_hourly_modulation(id);
;;

alter table project.dry_inflow_scenario_sector_setting drop column if exists vol_curve;
;;

alter table project.dry_inflow_scenario add column if not exists period hydra_dry_inflow_period not null default 'weekday';
;;

alter table project.dry_inflow_hourly_modulation drop constraint dry_inflow_hourly_modulation_hv_array_check ;

with elem_array as (
	select name, array_append(ARRAY(select unnest(hv_array[n:n][1:2])), unnest(hv_array[n:n][2:2])) as arr
	from project.dry_inflow_hourly_modulation, generate_series(1, array_length(hv_array,1)) as n
    ),
rebuilt_array as (
	select d.name, array_agg(arr) as arr
	from project.dry_inflow_hourly_modulation as d
	join elem_array on d.name = elem_array.name
	group by d.name
	)
update project.dry_inflow_hourly_modulation as d
set hv_array = (select arr from rebuilt_array where d.name=rebuilt_array.name);
;;

alter table project.dry_inflow_hourly_modulation add constraint dry_inflow_hourly_modulation_hv_array_check  check (array_length(hv_array, 2)=3);
;;

/* ********************************************** */
/*  Utilities                                     */
/* ********************************************** */

create or replace function notice(msg text, param anyelement)
returns anyelement
language plpgsql
immutable
as
$$$$
    begin
        raise notice '% %', msg, param;
        return param;
    end;
$$$$
;;

/* ********************************************** */
/*  Linear geometry tools                         */
/* ********************************************** */

do
$$$$
    begin
        if not exists (select 1 from pg_type where typname = 'vector2') then
            create type public.vector2 as (x double precision, y double precision);
        end if;
    end;
$$$$
;;

-- difference of two points yielding a vector
create or replace function difference(a geometry, b geometry)
returns vector2
language plpgsql immutable
as
$$$$
    begin
        return row(st_x(a)-st_x(b), st_y(a)-st_y(b))::vector2;
    end;
$$$$
;;

-- dot product of two points considered to be vectors
create or replace function dot(u vector2, v vector2)
returns float
language plpgsql immutable
as
$$$$
    begin
        return u.x*v.x + u.y*v.y;
    end;
$$$$
;;

-- normalized vector
create or replace function normalized(u vector2)
returns vector2
language plpgsql immutable
as
$$$$
    declare
        norm double precision;
    begin
        norm := sqrt(dot(u, u));
        if norm < 1e-9 then
            return u;
        end if;
        return row(u.x/norm, u.y/norm)::vector2;
    end;
$$$$
;;

-- angular interpolation of direction of two lines at a point
-- either points to intersection or // to both
create or replace function interpolate_direction(a geometry, b geometry, point geometry)
returns vector2
language plpgsql immutable
as
$$$$
    declare
        det double precision;
        u vector2;
        v vector2;
        d vector2;
        alpha double precision;
        res vector2;
    begin
    -- let a and b be the start points of the segments and u and v their directions,
    -- intesection point satisfies:

    -- a + alpha*u = b + beta*v

    -- ax + alpha*ux = bx + beta*vx
    -- ay + alpha*uy = by + beta*vy

    -- or

    -- alpha*ux - beta*vx = bx - ax
    -- alpha*uy - beta*uy = by - ay

    -- we define: d = b - a

    -- [ ux   -vx ][ alpha ] = [ d.x ]
    -- [ uy   -vy ][ beta  ] = [ d.y ]

    -- det = -ux*vy + uy*vx
    -- comT = [ -vy  vx ]
    --        [ -uy  ux ]


    -- [ alpha ] = 1/det comT * [ bx-ax ] = 1/det  [ -vy  vx ] * [ d.x ]
    -- [ beta  ]                [ by-ay ]          [ -uy  ux ]   [ d.y ]

        u := difference( st_endpoint(a), st_startpoint(a) );
        v := difference( st_endpoint(b), st_startpoint(b) );
        det = u.y*v.x - u.x*v.y;
        if abs(det) < 1.e-12 then -- lines are parallel
            --raise notice 'parallel %', det;
            return u;
        end if;
        d := difference( st_startpoint(b), st_startpoint(a) );
        alpha := (1./det)*(-v.y*d.x + v.x*d.y);
        --raise notice 'intersection % alpha % % % %', alpha, u, st_translate(st_startpoint(a), alpha*u.x, alpha*u.y), a, b;
        res := normalized(difference(st_translate(st_startpoint(a), alpha*u.x, alpha*u.y), point));
        if dot(res, u) > 0 then
            return res;
        else
            return row(-res.x, -res.y)::vector2;
        end if;
    end;
$$$$
;;

create or replace function check_is_point(geom geometry)
returns geometry
language plpgsql immutable
as
$$$$
    begin
        if st_geometrytype(geom) = 'ST_Point' or geom is null then
            return geom;
        else
            raise '% is not a point but a %', st_astext(geom), st_geometrytype(geom);
            return geom;
        end if;
    end;
$$$$
;;

-- linear interpolation of array points (2d), t is in [0,1]
create or replace function interpolate_array(u real[], v real[], t double precision)
returns real[]
language plpgsql
as
$$$$
    begin
        return (
            select array_agg(array[u[i][1] * (1. - t) + v[i][1] * t, u[i][2] * (1. - t) + v[i][2] * t])
            from generate_subscripts(u, 1) as i
        );
    end;
$$$$
;;

create or replace function zb_cat(zb_maj_l real[], zb_min real[], zb_maj_r real[])
returns real[]
language plpgsql
immutable
as
$$$$
    begin
        --raise notice 'array % % % %', zb_maj_l, array_length(zb_maj_l ,1 ), zb_maj_l[1][:], array(select array[ zb_maj_l[array_length(zb_maj_l ,1 )][1], zb_maj_l[array_length(zb_maj_l ,1 )][2]] from generate_series(array_length(zb_maj_l, 1)+1, 4));
        return array_cat(
            array_cat(
                zb_maj_l,
                array(select array[zb_maj_l[array_length(zb_maj_l ,1 )][1], zb_maj_l[array_length(zb_maj_l ,1 )][2]] from generate_series(array_length(zb_maj_l, 1)+1, 4))
            ),
            array_cat(
                array_cat(
                    zb_min,
                    array(select array[zb_min[array_length(zb_min ,1 )][1], zb_min[array_length(zb_min ,1 )][2]] from generate_series(array_length(zb_min , 1)+1, 6))
                ),
                array_cat(
                    zb_maj_r,
                    array(select array[zb_maj_r[array_length(zb_maj_r ,1 )][1], zb_maj_r[array_length(zb_maj_r ,1 )][2]]  from generate_series(array_length(zb_maj_r , 1)+1, 4))
                )
            )
        );
    end;
$$$$
;;

