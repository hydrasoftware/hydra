# coding=utf-8
"""
################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################
"""
import os
import re
import argparse
import shutil
from pathlib import Path

__currendir = os.path.dirname(__file__)

if __name__ == '__main__':
    from hydra import build

    version_dir = os.path.join(os.path.dirname(__file__), '..', 'versions')
    previous_versions = set([f.split('-')[0] for f in os.listdir(version_dir) if f.endswith('.sql') and not f.endswith('_model.sql')])
    __version__ = '.'.join(map(str, max([tuple(map(int, f.split('-')[1].replace('.sql', '').split('.')))
        for f in os.listdir(version_dir) if re.match('.+-(\d+)\.(\d+).(\d+)\.sql' , f)])))

    parser = argparse.ArgumentParser()
    parser.add_argument('postgresql_ext_dir', help='Postgresql extension directory')
    parser.add_argument('--all-versions', action='store_true', help="Install current version + all legacy extensions")

    args = parser.parse_args()

    assert os.path.isdir(args.postgresql_ext_dir)
    print(f'Installing hydra extension {__version__}...')
    build(args.postgresql_ext_dir, __version__)
    print('OK')

    if os.path.exists(os.path.join(__currendir, '..', 'build.txt')):
        # we have a packages plugin, legacy extension are installed from the extension directory
        for f in os.listdir(os.path.join(__currendir, 'extension')):
            source = os.path.join(__currendir, 'extension', f)
            destination = os.path.join(args.postgresql_ext_dir, f)
            print(f'Copy {source} to {destination}')
            shutil.copy(source, destination)


    if args.all_versions:
        import requests # request is not in the hydra installation directory
        print('Installing all legacy hydra extensions...')
        with requests.Session() as session:
            for version in sorted(previous_versions, reverse=True):
                filename = f"hydra--{version}.sql"
                if version=='test.1.2.2':
                    continue
                if not os.path.exists(Path(args.postgresql_ext_dir) / filename):
                    try:
                        resp = session.get(
                            f'https://hydra-software.net/telechargement/hydra-extensions/{filename}',
                            timeout=4,
                            headers={"User-Agent": "Mozilla"}  # to avoid bans from amen host platform
                        )
                    except requests.exceptions.ConnectTimeout:
                        print('Too long to get files from hydra-software.net, please retry later')
                        break
                    if resp.status_code != 200:
                        print(f'Remote file {filename} unavailable, skipping')
                        continue
                    with open(Path(args.postgresql_ext_dir) / filename, mode='wb') as output:
                        output.write(resp.content)
                print(f'Extension file {filename} installed')
