# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
from builtins import str
from builtins import range
import os
import webbrowser
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QFileDialog, QWidget, QGridLayout
from hydra.gui.base_dialog import tr

class AeraulicsWidget(QWidget):

    def __init__(self, current_project, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "aeraulics.ui"), self)
        tmplayout = QGridLayout()
        tmplayout.addWidget(self,0,0)
        parent.setLayout(tmplayout)
        self.active_scn_id = None
        self.project = current_project
        self.show()

    def set(self, scn_id):
        self.active_scn_id = scn_id

        if self.active_scn_id is not None:
            has_aeraulics, speed_of_sound_in_air, absolute_ambiant_air_pressure, \
                    average_altitude_msl, air_water_skin_friction_coefficient, air_pipe_friction_coefficient, \
                    air_temperature_in_network_celcius, ambiant_air_temperature_celcius, leak_pressure \
                = self.project.execute(f"""
                select
                    has_aeraulics, speed_of_sound_in_air, absolute_ambiant_air_pressure,
                    average_altitude_msl, air_water_skin_friction_coefficient, air_pipe_friction_coefficient,
                    air_temperature_in_network_celcius, ambiant_air_temperature_celcius, leak_pressure
                from project.scenario
                where id={self.active_scn_id}
                """).fetchone()

            self.has_aeraulics.setChecked(has_aeraulics)
            self.speed_of_sound_in_air.setText(f"{speed_of_sound_in_air}")
            self.absolute_ambiant_air_pressure.setText(f"{absolute_ambiant_air_pressure}")
            self.average_altitude_msl.setText(f"{average_altitude_msl}")
            self.air_water_skin_friction_coefficient.setText(f"{air_water_skin_friction_coefficient}")
            self.air_pipe_friction_coefficient.setText(f"{air_pipe_friction_coefficient}")
            self.air_temperature_in_network_celcius.setText(f"{air_temperature_in_network_celcius}")
            self.ambiant_air_temperature_celcius.setText(f"{ambiant_air_temperature_celcius}")
            self.leak_pressure.setText(f"{leak_pressure}")

    def save(self):
        if self.active_scn_id:
            self.project.execute(f"""
                update project.scenario set
                    has_aeraulics=%s,
                    speed_of_sound_in_air=%s,
                    absolute_ambiant_air_pressure=%s,
                    average_altitude_msl=%s,
                    air_water_skin_friction_coefficient=%s,
                    air_pipe_friction_coefficient=%s,
                    air_temperature_in_network_celcius=%s,
                    ambiant_air_temperature_celcius=%s,
                    leak_pressure=%s
                where id=%s
                """, (
                self.has_aeraulics.isChecked(),
                self.speed_of_sound_in_air.text(),
                self.absolute_ambiant_air_pressure.text(),
                self.average_altitude_msl.text(),
                self.air_water_skin_friction_coefficient.text(),
                self.air_pipe_friction_coefficient.text(),
                self.air_temperature_in_network_celcius.text(),
                self.ambiant_air_temperature_celcius.text(),
                self.leak_pressure.text(),
                self.active_scn_id))
