# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import webbrowser
from functools import partial
from qgis.PyQt import uic
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QFileDialog, QWidget, QGridLayout, QTableWidgetItem, QComboBox, QAbstractItemView
from hydra.gui.base_dialog import tr
from hydra.utility.tables_properties import TablesProperties
from hydra.gui.widgets.combo_with_values import ComboWithValues
from hydra.gui.widgets.time_input_widget import TimeInputWidget
from hydra.utility.system import open_external
from ...kernel import find_exe
import subprocess

class SerieComputOptionsWidget(QWidget):
    def __init__(self, current_project, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "comput_options.ui"), self)
        tmplayout = QGridLayout()
        tmplayout.addWidget(self,0,0)
        parent.setLayout(tmplayout)
        self.__current_bloc_id = None
        self.__current_serie_id = None
        self.__properties = TablesProperties.get_properties()
        self.__output_family_order = ["bc", "link", "singularity"]
        self.__output_family = {
            "link": {
                "title": tr("Links"),
                "tables": [ "borda_headloss_link",
                            "connector_link",
                            "connector_hydrology_link",
                            "routing_hydrology_link",
                            "gate_link",
                            "regul_gate_link",
                            "deriv_pump_link",
                            "mesh_2d_link",
                            "network_overflow_link",
                            "overflow_link",
                            "porous_link",
                            "pump_link",
                            "strickler_link",
                            "street_link",
                            "weir_link"]
            },
            "singularity": {
                "title": tr("Singularities"),
                "tables": [ "borda_headloss_singularity",
                            "bradley_headloss_singularity",
                            "marker_singularity",
                            "bridge_headloss_singularity",
                            "hydraulic_cut_singularity",
                            "param_headloss_singularity",
                            "pipe_branch_marker_singularity",
                            "gate_singularity",
                            "regul_sluice_gate_singularity",
                            "zregul_weir_singularity" ]
            },
            "bc": {
                "title": tr("Boundary conditions"),
                "tables": [ "constant_inflow_bc_singularity",
                            "froude_bc_singularity",
                            "hydrograph_bc_singularity",
                            "hydrology_bc_singularity",
                            "model_connect_bc_singularity",
                            "zq_bc_singularity" ,
                            "strickler_bc_singularity",
                            "tank_bc_singularity",
                            "tz_bc_singularity",
                            "weir_bc_singularity"]
            }
        }

        try:
            kernel_versions = ""
            for p in ('whydram24', 'hydrol'):
                cmd = [find_exe(p), '--kernel-versions']
                out = subprocess.check_output(cmd, creationflags=subprocess.CREATE_NO_WINDOW) if os.name == 'nt' else subprocess.check_output(cmd)
                kernel_versions += f"<b>{p}</b>\n<table>"
                for r in out.decode('utf8').split('\n'):
                    if len(r.split(';')) >= 2:
                        v, d = r.split(';', 1)
                        kernel_versions += f"<tr><td>{v}</td><td>{d}</td></tr>\n"
                kernel_versions += "</table>\n"

            self.kernelVersion.setToolTip(kernel_versions)
        except RuntimeError:
            pass

        self.project = current_project
        self.btnAdd.clicked.connect(self.__select_file)
        self.btnEdit.clicked.connect(self.__edit_file)
        self.btnHelp.clicked.connect(self.__help_file)
        self.btnAffinAdd.clicked.connect(self.__affin_add)
        self.btnAffinRemove.clicked.connect(self.__affin_remove)
        self.btnRunoffAdd.clicked.connect(self.__runoff_add)
        self.btnRunoffRemove.clicked.connect(self.__runoff_remove)
        self.btnStricklerAdd.clicked.connect(self.__strickler_add)
        self.btnStricklerRemove.clicked.connect(self.__strickler_remove)
        self.btnOutputAdd.clicked.connect(self.__output_add)
        self.btnOutputRemove.clicked.connect(self.__output_remove)

        self.t_affin_start_hr = TimeInputWidget(self.t_affin_start_hr_placeholder)
        self.t_affin_start_hr.set_format(hr=99999, min=59, sec=-1)
        self.dt_sor1_hr = TimeInputWidget(self.dt_sor1_hr_placeholder)
        self.dt_sor1_hr.set_format(hr=99999, min=59, sec=-1)

        self.option_file.stateChanged.connect(self.__option_file_changed)
        self.__init_combos()
        self.cboAffinModel.activated.connect(self.__refresh_affin_elem_list)
        self.cboAffinElemType.activated.connect(self.__refresh_affin_elem_list)
        self.cboRunoffModel.activated.connect(self.__refresh_runoff_domain_list)
        self.cboRunoffDomain = ComboWithValues(self.cboRunoffDomain_placeholder)
        self.cboRunoffDomain.activated.connect(self.__refresh_runoff_domain_list)
        self.cboStricklerModel.activated.connect(self.__refresh_strickler_elem_list)
        self.cboStricklerElemType.activated.connect(self.__refresh_strickler_elem_list)
        self.cboOutputModel.activated.connect(self.__refresh_output_elem_list)
        self.cboOutputElemFamily.activated.connect(self.__refresh_output_elem_type)
        self.cboOutputElemType.activated.connect(self.__refresh_output_elem_list)
        self.show()

    def __select_file(self):
        file_url, __ = QFileDialog.getSaveFileName(self, tr('Select options file'), self.project.directory, tr('All file (*.*)'), options=QFileDialog.DontConfirmOverwrite)
        if file_url:
            # creates file empty if not exists
            open(file_url, 'a').close()
            self.option_file_path.setText(file_url)

    def __edit_file(self):
        file = self.option_file_path.text()
        open_external(file)

    def __help_file(self):
        webbrowser.open('http://hydra-software.net/docs/notes_techniques/NT25.pdf', new=0)

    def __option_file_changed(self):
        self.option_file_path.setEnabled(self.option_file.isChecked())
        self.btnAdd.setEnabled(self.option_file.isChecked())
        self.btnEdit.setEnabled(self.option_file.isChecked())

    def __init_combos(self):
        self.cboAffinElemType.clear()
        self.cboAffinElemType.addItem(self.type_domain_to_str('branch'))
        self.cboAffinElemType.addItem(self.type_domain_to_str('domain_2d'))
        self.cboAffinElemType.addItem(self.type_domain_to_str('reach'))
        self.cboAffinElemType.setCurrentIndex(0)

        self.cboStricklerElemType.clear()
        self.cboStricklerElemType.addItem(self.type_domain_to_str('branch'))
        self.cboStricklerElemType.addItem(self.type_domain_to_str('domain_2d'))
        self.cboStricklerElemType.addItem(self.type_domain_to_str('reach'))
        self.cboStricklerElemType.setCurrentIndex(0)

        self.cboAffinModel.clear()
        self.cboRunoffModel.clear()
        self.cboOutputModel.clear()
        self.cboStricklerModel.clear()
        models = self.project.get_models()
        for model in models:
            self.cboAffinModel.addItem(model)
            self.cboRunoffModel.addItem(model)
            self.cboOutputModel.addItem(model)
            self.cboStricklerModel.addItem(model)
        self.cboAffinModel.setCurrentIndex(0)
        self.cboRunoffModel.setCurrentIndex(0)
        self.cboOutputModel.setCurrentIndex(0)
        self.cboStricklerModel.setCurrentIndex(0)

        self.cboOutputElemFamily.clear()
        for family in self.__output_family_order:
            self.cboOutputElemFamily.addItem(self.__output_family[family]['title'])

        self.cboSor1Mode.clear()
        sor1_modes = self.project.execute("""select name, description from hydra.sor1_mode order by id""").fetchall()
        for n, d in sor1_modes:
            self.cboSor1Mode.addItem(d)

        self.kernelVersion.setText('')

    def type_domain_to_str(self,type):
        if type=='branch':
            return tr('Branch')
        if type=='reach':
            return tr('Reach')
        if type=='domain_2d':
            return tr('Domain 2D')

    def str_to_type_domain(self,text):
        if text==tr('Branch'):
            return 'branch'
        if text==tr('Reach'):
            return 'reach'
        if text==tr('Domain 2D'):
            return 'domain_2d'

    def set_bloc_serie(self, id_bloc, id_serie):
        self.__current_bloc_id = id_bloc
        self.__current_serie_id = id_serie
        if self.__current_serie_id :
            option_file, brut_option_file_path, c_affin_param, domain_2d_param, t_affin_start_hr, t_affin_min_surf_ddomains, \
                output_option, strickler_param, sor1_mode, dt_sor1_hr, kernel_version = self.project.execute("""
                select option_file, option_file_path, c_affin_param, domain_2d_param, t_affin_start_hr, t_affin_min_surf_ddomains,
                output_option, strickler_param, sor1_mode, dt_sor1_hr, kernel_version
                from project.serie
                where id={} limit 1;""".format(self.__current_serie_id)).fetchone()

            option_file_path = self.project.unpack_path(brut_option_file_path)

            # affin option
            self.c_affin_param.setChecked(c_affin_param)
            self.t_affin_start_hr.set_time(t_affin_start_hr)
            self.t_affin_min_surf_ddomains.setText(str(t_affin_min_surf_ddomains))
            self.refresh_affin_table()

            # domain_runoff option
            self.domain_2d_param.setChecked(domain_2d_param)
            self.refresh_runoff_table()

            # strickler option
            self.strickler_param.setChecked(strickler_param)
            self.refresh_strickler_table()

            # output option
            sor1_mode_name = self.project.execute("""select description from hydra.sor1_mode where name='{}'""".format(sor1_mode)).fetchone()[0]
            self.cboSor1Mode.setCurrentIndex(self.cboSor1Mode.findText(sor1_mode_name))
            self.dt_sor1_hr.set_time(dt_sor1_hr)
            self.__refresh_output_elem_type()
            self.refresh_output_table()
            self.ouput_option.setChecked(output_option)

            # external file
            self.option_file_path.setText(option_file_path)
            self.option_file.setChecked(option_file)
            self.__option_file_changed()

            # kernel specific version
            if kernel_version:
                self.kernelVersion.setText(kernel_version)

    def save(self):
        if self.__current_serie_id:
            brut_option_file_path = self.project.pack_path(self.option_file_path.text())
            sor1_mode = self.project.execute("""select name from hydra.sor1_mode where description='{}'""".format(self.cboSor1Mode.currentText())).fetchone()[0]
            kernel_version = self.kernelVersion.text()
            if kernel_version:
                kernel_version_txt = "'" + kernel_version + "'"
            else:
                kernel_version_txt = 'null'

            self.project.execute("""
                update project.serie
                set option_file='{}', option_file_path='{}', c_affin_param='{}', domain_2d_param='{}',
                t_affin_start_hr='{}', t_affin_min_surf_ddomains='{}', output_option='{}',
                sor1_mode='{}', dt_sor1_hr='{}', strickler_param='{}', kernel_version={}
                where id={}""".format(
                    str(self.option_file.isChecked()), brut_option_file_path, str(self.c_affin_param.isChecked()), str(self.domain_2d_param.isChecked()),
                    self.t_affin_start_hr.get_time(), str(self.t_affin_min_surf_ddomains.text()), str(self.ouput_option.isChecked()),
                    sor1_mode, self.dt_sor1_hr.get_time(), str(self.strickler_param.isChecked()), kernel_version_txt,
                    self.__current_serie_id))
            self.save_strickler_coef()

# AFFIN Options
    def refresh_affin_table(self):
        if self.__current_serie_id is not None and self.cboAffinModel.currentIndex()!=-1:
            affin_list = self.project.execute("""
                select model, type_domain, element, affin_option
                from project.c_affin_param_serie
                where serie={} order by model, type_domain, element;""".format(self.__current_serie_id)).fetchall()

            self.affin_table.setRowCount(0)
            for item in affin_list:
                rowcount = self.affin_table.rowCount() + 1
                self.affin_table.setRowCount(rowcount)
                row = rowcount - 1
                self.affin_table.setItem(row, 0, QTableWidgetItem(str(item[0])))
                self.affin_table.setItem(row, 1, QTableWidgetItem(self.type_domain_to_str(item[1])))
                self.affin_table.setItem(row, 2, QTableWidgetItem(str(item[2])))
                # Combobox with affin options in column 3
                self.affin_table.setItem(row, 3, QTableWidgetItem(str(item[3])))
                cbox = self.__get_affin_option_combo(str(item[3]))
                cbox.currentIndexChanged.connect(partial(self.__affin_option_combo_index_changed, row))
                self.affin_table.setCellWidget(row, 3, cbox)


            self.affin_table.resizeColumnsToContents()
            self.affin_table.horizontalHeader().setStretchLastSection(True)
            self.__refresh_affin_elem_list()

    def __get_affin_option_combo(self, affin_option):
        list_affin_option = self.project.execute("select id, name, description from hydra.affin_option").fetchall()

        combobox = QComboBox()
        combobox.addItem("")
        for i in range(0, len(list_affin_option)):
            id, name, description = list_affin_option[i]
            combobox.addItem(description)
            if affin_option == name:
                combobox.setCurrentIndex(i+1)
        return combobox

    def __affin_option_combo_index_changed(self, irow):
        self.affin_table.selectRow(irow)
        model=self.affin_table.item(irow, 0).text()
        type_domain=self.str_to_type_domain(self.affin_table.item(irow, 1).text())
        element=self.affin_table.item(irow, 2).text()

        list_affin_option = self.project.execute("select name, description from hydra.affin_option").fetchall()
        for name, description in list_affin_option:
            if description == self.affin_table.cellWidget(irow, 3).currentText():
                affin_option = name
                self.project.execute("""update project.c_affin_param_serie set affin_option = '{}' where serie={} and model='{}' and type_domain='{}' and element='{}';
                                     """.format(affin_option, self.__current_serie_id, model, type_domain, element))


    def __refresh_affin_elem_list(self):
        if self.__current_serie_id is not None and self.cboAffinModel.currentIndex()!=-1:
            model = str(self.cboAffinModel.currentText())
            type_domain=self.str_to_type_domain(str(self.cboAffinElemType.currentText()))
            elements = self.project.execute("""select name from {model}.{table}
            where name not in (
                select element from project.c_affin_param_serie where serie={serie} and model='{model}' and type_domain='{type}'
            ) order by name;""".format(model=model, table=type_domain, serie=self.__current_serie_id, type=type_domain)).fetchall()
            all_already_selected = self.project.execute("""select exists (
                select 1 from project.c_affin_param_serie where serie={serie} and model='{model}' and type_domain='{type}' and element='All'
                );""".format(model=model, serie=self.__current_serie_id, type=type_domain)).fetchone()[0]

            self.cboAffinElem.clear()
            if not all_already_selected:
                self.cboAffinElem.addItem('All')
                for item in elements:
                    self.cboAffinElem.addItem(item[0])
            self.cboAffinElem.setCurrentIndex(0)

    def __affin_add(self):
        if self.cboAffinModel.currentIndex()==-1:
            return
        if self.cboAffinElemType.currentIndex()==-1:
            return
        if self.cboAffinElem.currentIndex()==-1:
            return

        model=str(self.cboAffinModel.currentText())
        type_domain=self.str_to_type_domain(str(self.cboAffinElemType.currentText()))
        element=str(self.cboAffinElem.currentText())

        if element == 'All':
            self.project.execute("""delete from project.c_affin_param_serie where serie={} and model='{}' and type_domain='{}';
                                 """.format(self.__current_serie_id, model, type_domain))

        self.project.execute("""insert into project.c_affin_param_serie(serie, model, type_domain, element)
            values({}, '{}', '{}', '{}');""".format(self.__current_serie_id, model, type_domain, element))
        self.refresh_affin_table()

    def __affin_remove(self):
        items = self.affin_table.selectedItems()
        if len(items)>0:
            selected_row = self.affin_table.row(items[0])
            model=self.affin_table.item(selected_row,0).text()
            type_domain=self.str_to_type_domain(self.affin_table.item(selected_row,1).text())
            element=self.affin_table.item(selected_row,2).text()

            self.project.execute("""delete from project.c_affin_param_serie where serie={} and model='{}' and type_domain='{}' and element='{}';
                                 """.format(self.__current_serie_id, model, type_domain, element))
            self.refresh_affin_table()

# DOMAIN 2D RUNOFF Options
    def refresh_runoff_table(self):
        if self.__current_serie_id is not None and self.cboRunoffModel.currentIndex()!=-1:
            model = str(self.cboRunoffModel.currentText())
            runoff_list = self.project.execute("""
                select p.model, d.name
                from project.domain_2d_param_serie as p
                join {}.domain_2d as d on d.id=p.domain_2d
                where serie={} order by model, domain_2d;""".format(model, self.__current_serie_id)).fetchall()

            self.runoff_table.setRowCount(0)
            for item in runoff_list:
                rowcount = self.runoff_table.rowCount() + 1
                self.runoff_table.setRowCount(rowcount)
                row = rowcount - 1
                self.runoff_table.setItem(row, 0, QTableWidgetItem(str(item[0])))
                self.runoff_table.setItem(row, 1, QTableWidgetItem(str(item[1])))

            self.runoff_table.resizeColumnsToContents()
            self.runoff_table.horizontalHeader().setStretchLastSection(True)
            self.__refresh_runoff_domain_list()

    def __refresh_runoff_domain_list(self):
        if self.__current_serie_id is not None and self.cboRunoffModel.currentIndex()!=-1:
            model = str(self.cboRunoffModel.currentText())
            domains = self.project.execute("""select id, name from {model}.domain_2d
            where id not in (
                select domain_2d from project.domain_2d_param_serie where serie={serie} and model='{model}'
            ) order by name;""".format(model=model, serie=self.__current_serie_id)).fetchall()
            self.cboRunoffDomain.clear()
            for item in domains:
                self.cboRunoffDomain.addItem(item[1], item[0])
            self.cboRunoffDomain.setCurrentIndex(0)

    def __runoff_add(self):
        if self.cboRunoffModel.currentIndex()==-1:
            return
        if self.cboRunoffDomain.currentIndex()==-1:
            return

        model=str(self.cboRunoffModel.currentText())
        domain_id=str(self.cboRunoffDomain.get_selected_value())

        self.project.execute("""insert into project.domain_2d_param_serie(serie, model, domain_2d)
            values({},'{}',{});""".format(self.__current_serie_id, model, domain_id))
        self.refresh_runoff_table()

    def __runoff_remove(self):
        items = self.runoff_table.selectedItems()
        if len(items)>0:
            selected_row = self.runoff_table.row(items[0])
            model=self.runoff_table.item(selected_row, 0).text()
            domain_name=self.runoff_table.item(selected_row, 1).text()

            self.project.execute("""delete from project.domain_2d_param_serie
                where serie={serie} and model='{m}' and domain_2d=(select id from {m}.domain_2d where name='{name}');""".format(serie=self.__current_serie_id, m=model, name=domain_name))
            self.refresh_runoff_table()

# Strickler Options
    def refresh_strickler_table(self):
        if self.__current_serie_id is not None:
            item_list = self.project.execute("""
                select model, type_domain, element, rkmin, rkmaj, pk1, pk2
                from project.strickler_param_serie
                where serie={} order by model, type_domain, element;""".format(self.__current_serie_id)).fetchall()

            self.strickler_table.setSelectionBehavior(QAbstractItemView.SelectRows)
            self.strickler_table.setEditTriggers(QAbstractItemView.AllEditTriggers)

            self.strickler_table.setRowCount(0)
            for item in item_list:
                rowcount = self.strickler_table.rowCount() + 1
                self.strickler_table.setRowCount(rowcount)
                row = rowcount - 1

                self.strickler_table.setItem(row, 0, QTableWidgetItem(str(item[0])))
                self.strickler_table.setItem(row, 1, QTableWidgetItem(self.type_domain_to_str(item[1])))
                self.strickler_table.setItem(row, 2, QTableWidgetItem(str(item[2])))
                self.strickler_table.item(row,0).setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
                self.strickler_table.item(row,1).setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
                self.strickler_table.item(row,2).setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)

                for i in range(0,4):
                    self.strickler_table.setItem(row, 3+i, QTableWidgetItem(str(item[3+i])))

            self.strickler_table.resizeColumnsToContents()
            self.strickler_table.horizontalHeader().setStretchLastSection(True)
            self.__refresh_strickler_elem_list()

    def save_strickler_coef(self):
        for irow in range(0,self.strickler_table.rowCount()):
            model=self.strickler_table.item(irow,0).text()
            type_domain=self.str_to_type_domain(self.strickler_table.item(irow,1).text())
            element=self.strickler_table.item(irow,2).text()
            rkmin=self.strickler_table.item(irow,3).text()
            rkmaj=self.strickler_table.item(irow,4).text()
            pk1=self.strickler_table.item(irow,5).text()
            pk2=self.strickler_table.item(irow,6).text()
            self.project.execute("""update project.strickler_param_serie set rkmin={}, rkmaj={}, pk1={}, pk2={}
            where serie={} and model='{}' and type_domain='{}' and element='{}';""".format(rkmin, rkmaj, pk1, pk2,
            self.__current_serie_id, model, type_domain, element))

    def __refresh_strickler_elem_list(self):
        if self.__current_serie_id is not None and self.cboStricklerModel.currentIndex()!=-1:
            model = str(self.cboStricklerModel.currentText())
            type_domain=self.str_to_type_domain(str(self.cboStricklerElemType.currentText()))
            elements = self.project.execute("""select name from {model}.{table}
            where name not in (
                select element from project.strickler_param_serie where serie={serie} and model='{model}' and type_domain='{type}'
            ) order by name;""".format(model=model, table=type_domain, serie=self.__current_serie_id, type=type_domain)).fetchall()
            self.cboStricklerElem.clear()
            for item in elements:
                self.cboStricklerElem.addItem(item[0])
            self.cboStricklerElem.setCurrentIndex(0)

    def __strickler_add(self):
        if self.cboStricklerModel.currentIndex()==-1:
            return
        if self.cboStricklerElemType.currentIndex()==-1:
            return
        if self.cboStricklerElem.currentIndex()==-1:
            return

        model=str(self.cboStricklerModel.currentText())
        type_domain=self.str_to_type_domain(str(self.cboStricklerElemType.currentText()))
        element=str(self.cboStricklerElem.currentText())

        self.project.execute("""insert into project.strickler_param_serie(serie, model, type_domain, element)
            values({},'{}','{}','{}');""".format(self.__current_serie_id, model, type_domain, element))
        self.refresh_strickler_table()

    def __strickler_remove(self):
        items = self.strickler_table.selectedItems()
        if len(items)>0:
            selected_row = self.strickler_table.row(items[0])
            model=self.strickler_table.item(selected_row,0).text()
            type_domain=self.str_to_type_domain(self.strickler_table.item(selected_row,1).text())
            element=self.strickler_table.item(selected_row,2).text()

            self.project.execute("""delete from project.strickler_param_serie
                where serie={} and model='{}' and type_domain='{}' and element='{}';""".format(self.__current_serie_id,
                    model, type_domain, element))
            self.refresh_strickler_table()

# OUTPUT Options
    def __refresh_output_elem_type(self):
        elem_family = self.__find_family(str(self.cboOutputElemFamily.currentText()))
        self.cboOutputElemType.clear()
        for table in self.__output_family[elem_family]['tables']:
            self.cboOutputElemType.addItem(self.__properties[table]['name'])
        self.__refresh_output_elem_list()

    def __refresh_output_elem_list(self):
        if self.__current_serie_id is not None and self.cboOutputModel.currentIndex()!=-1:
            model = str(self.cboOutputModel.currentText())
            type_elem = self.__find_table_name(str(self.cboOutputElemType.currentText()))
            elements = self.project.execute("""select name from {model}.{table}
            where name not in (
                select element from project.output_option_serie where serie={serie} and model='{model}' and type_element='{table}'
            ) order by name;""".format(model=model, table=type_elem, serie=self.__current_serie_id)).fetchall()
            self.cboOutputElem.clear()
            for item in elements:
                self.cboOutputElem.addItem(item[0])
            self.cboOutputElem.setCurrentIndex(0)

    def __find_family(self, text):
        for family in self.__output_family:
            if self.__output_family[family]['title']==text:
                return family
        raise "family not found"

    def __find_table_name(self, text):
        for table in self.__properties:
            if self.__properties[table]['name']==text:
                return table
        raise "table not found"

    def refresh_output_table(self):
        if self.__current_serie_id is not None and self.cboOutputModel.currentIndex()!=-1:
            output_list = self.project.execute("""
                select model, type_element, element
                from project.output_option_serie
                where serie={} order by model, type_element, element;""".format(self.__current_serie_id)).fetchall()

            self.output_table.setRowCount(0)
            for item in output_list:
                rowcount = self.output_table.rowCount() + 1
                self.output_table.setRowCount(rowcount)
                row = rowcount - 1
                self.output_table.setItem(row, 0, QTableWidgetItem(str(item[0])))
                self.output_table.setItem(row, 1, QTableWidgetItem(self.__properties[str(item[1])]['name']))
                self.output_table.setItem(row, 2, QTableWidgetItem(str(item[2])))

            self.output_table.resizeColumnsToContents()
            self.output_table.horizontalHeader().setStretchLastSection(True)
            self.__refresh_output_elem_list()

    def __output_add(self):
        if self.cboOutputModel.currentIndex()==-1:
            return
        if self.cboOutputElemType.currentIndex()==-1:
            return
        if self.cboOutputElem.currentIndex()==-1:
            return

        model = str(self.cboOutputModel.currentText())
        type_elem = self.__find_table_name(str(self.cboOutputElemType.currentText()))
        element=str(self.cboOutputElem.currentText())

        self.project.execute("""insert into project.output_option_serie(serie, model, type_element, element)
            values({},'{}','{}','{}');""".format(self.__current_serie_id, model, type_elem, element))
        self.refresh_output_table()

    def __output_remove(self):
        items = self.output_table.selectedItems()
        if len(items)>0:
            selected_row = self.output_table.row(items[0])
            model=self.output_table.item(selected_row,0).text()
            type_elem=self.__find_table_name(self.output_table.item(selected_row,1).text())
            element=self.output_table.item(selected_row,2).text()

            self.project.execute("""delete from project.output_option_serie
                where serie={} and model='{}' and type_element='{}' and element='{}';""".format(self.__current_serie_id,
                    model, type_elem, element))
            self.refresh_output_table()
