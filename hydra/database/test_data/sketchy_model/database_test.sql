/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/*

Script pour valider le modele de données

Le but est de créer une base qui une fois ouverte dans qgis ressemblera à la
figure 1.1 du rapport

*/

select 'insert reach';
insert into model.reach(geom, name)
select ST_Force3D(geom), nom  from brut.reach
;

select 'insert manhole';
insert into model.manhole_node(area, geom)
select 1, ST_Force3D(geom) from brut.manhole
;

select 'test first id of manhole';
select min(id)=7 from model.manhole_node;

select 'test update';
update model.manhole_node set area=2, z_ground=1 where id=7;
select area=2 from model.manhole_node where id=7;
-- note: id is not updated
update model.manhole_node set id=4 where id=7;
select count(1)=0 from model.manhole_node where id=4;

select 'test delete';
select count(1)=1 from model.manhole_node where id=7;
select count(1)=1 from model._manhole_node where id=7;
delete from model.manhole_node where id=7;
select count(1)=0 from model._manhole_node where id=7;
select count(1)=0 from model._node where id=7;

select 'put the deleted collector back';
delete from model.manhole_node;
insert into model.manhole_node(area, geom)
select 1, ST_Force3D(geom) from brut.manhole
;

select 'noeud de river';
insert into model.river_node(geom)
select ST_Force3D(ST_ClosestPoint(model.reach.geom, brut.river_node.geom))
from model.reach, brut.river_node
where ST_DWithin(model.reach.geom, brut.river_node.geom, 300)
;

select 'insert station de gestion and chambre';
insert into model.station(geom)
select ST_Force3D(brut.station_gestion.geom)
from brut.station_gestion
;
insert into model.station_node(geom, area)
select ST_Force3D(ST_Centroid(brut.station_gestion.geom)), 1
from brut.station_gestion
;

with line as (select ST_Force3D(st_boundary(geom)) as geom from brut.storage),
     constrain as (
        insert into model.constrain(geom)
        select line.geom from line
        returning id
        )
insert into model.storage_node(geom, zs_array)
select ST_Force3D(st_centroid(geom)), '{{0,0},{10,10}}'::real[][] from brut.storage
;

select 'insert mailles 2D';
with domaine as (
    insert into model.domain_2d(name) values ('mon domaine 2D') returning id
)
insert into model.elem_2d_node(domain_2d, contour, zb, rk)
select domaine.id, ST_Force3D(geom), 0, 1 from domaine, brut.mailles
;

select 'insert crossroad';
insert into model.crossroad_node(geom, area)
select ST_Force3D(geom), 1 from brut.crossroad
;

select 'insert urban_storage';
with line as (select ST_Force3D(st_boundary(geom)) as geom from brut.urban_storage),
     constrain as (
        insert into model.constrain(geom)
        select line.geom from line
        returning id
        )
insert into model.storage_node(geom, zs_array)
select ST_Force3D(st_centroid(geom)), '{{0,0},{1,1}}'::real[][] from brut.urban_storage
;

select 'insert crossroad-urban_storage';
insert into model.overflow_link(geom, width1, z_crest1)
select ST_Force3D(geom), 1, 1 from brut.crossroad_urban_storage
;


select 'insert bassins versants';
insert into model.catchment_node(geom, area_ha, rl, slope, c_imp, netflow_type, constant_runoff, runoff_type)
select ST_Force3D(geom), 1., 1., 1., 1., 'constant_runoff', 10, 'Desbordes 1 Cr' from brut.catchment
;

update brut.collecteur set geom=ST_Force2D(ST_Snap(geom, (select ST_Collect(r.geom) from model.manhole_node as r where ST_DWithin(r.geom, geom, 300)), 300));

select 'insert link_pipe';
insert into model.pipe_link(geom, z_invert_up, z_invert_down)
select ST_Reverse(ST_Force3D(geom)), 1, 0
from brut.collecteur as c;

select 'insert routage';
with ids as (
    select v.id as up, (
        select t.id from model._node as t
        where t.node_type in ('river', 'station')
        order by ST_Distance(t.geom, v.geom) limit 1) as down
    from model.catchment_node as v
    )
insert into model.routing_hydrology_link(geom)
select ST_MakeLine(v.geom, t.geom) from model._node as t, model.catchment_node as v, ids where v.id=ids.up and t.id=ids.down
;

select 'insert river_pave';
insert into model.overflow_link(geom, width1, z_crest2)
select ST_Force3D(geom), 1, 0 from brut.river_pave
;

select 'check_catchment_has_one_routage_fct';
select model.check_catchment_has_one_route_fct();

select 'check_hydrogramme_apport_on_down_routage_fct';
select model.check_hydrogramme_apport_on_down_route_fct();

select 'insert inter pave';
insert into model.mesh_2d_link(geom, z_invert)
select ST_Force3D(geom), 1. from brut.inter_pave
;

select 'insert weir sigularity';
insert into model.weir_bc_singularity(geom, z_weir)
select ST_Force3D(geom), 1. from brut.manhole limit 4;
update model.weir_bc_singularity set name='titi' where name='DL1';
select count(1) from  model.weir_bc_singularity where name='titi';

select 'insert hydrograph_bc';
insert into model.hydrograph_bc_singularity(geom, storage_area)
select geom, 1. from model.manhole_node where id=8;

select 'insert hydrograph_bc';
insert into model.hydrograph_bc_singularity(geom, storage_area)
select geom, 1. from model.river_node where id=35;

-- select 'test clavh is down';
-- select model.check_clavh_is_down_fct();


select 'insert into pipe_branch_marker_singularity';
with pt as (select geom from model.manhole_node where id=24)
insert into model.pipe_branch_marker_singularity(geom, pk0_km, dx, name)
select pt.geom, 10, 10, 'toto' from pt;

select id, branch from model.pipe_link order by id;

select 'insert river_cross_section_profile';
insert into model.river_cross_section_profile(geom)
select geom from model.river_node limit 1;

insert into project.intensity_curve_rainfall (t_mn_intensity_mmh, name) values ('{{0,1},{1,3}}', 'rain_1');

select 'insert geoms';
insert into model.closed_parametric_geometry(name, zbmin_array) values('cp', '{{-10,10},{0,0},{10,10}}'::real[][]);
insert into model.open_parametric_geometry(name, zbmin_array) values('op', '{{-10,10},{0,0},{10,10}}'::real[][]);
insert into model.valley_cross_section_geometry(name, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array) values('valley', '{{-10,10},{0,0},{10,10},{-10,10},{0,0},{10,10}}'::real[][], '{{-10,10},{0,0},{10,10},{-10,10}}'::real[][], '{{-10,10},{0,0},{10,10},{-10,10}}'::real[][]);
insert into model.valley_cross_section_topo_geometry(name, xz_array) values('valey_topo', '{{-10,10},{0,0},{10,10},{-10,10},{0,0},{10,10}}'::real[][]);

select 'test configuration';
select * from model.manhole_node where id=(select min(id) from model.manhole_node);
update model.manhole_node set area=4, configuration='{"default":{"area":1, "z_ground": 999}, "test_config":{"area":2, "z_ground": 888}}' where id=(select min(id) from model.manhole_node);
select * from model.manhole_node where id=(select min(id) from model.manhole_node);
select * from model.configuration;
update model.metadata set configuration = 2;
select * from model.configuration;

select * from model.manhole_node where id=(select min(id) from model.manhole_node);
update model.metadata set configuration = 1;
select * from model.manhole_node where id=(select min(id) from model.manhole_node);
