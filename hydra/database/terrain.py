from builtins import str
from builtins import object
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import numpy
import tempfile
from zipfile import ZipFile
from io import BytesIO
from osgeo import gdal
from math import ceil, floor
from qgis.PyQt.QtCore import QCoreApplication
from hydra.utility.process import run_cmd
from hydra.database import database as dbhydra
import requests
#from shapely.geometry import MultiPolygon, Polygon # for DEBUG

_tempdir = tempfile.gettempdir()


def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

class Terrain(object):
    '''manage terrain data'''
    def __init__(self, data_directory, project):
        self.__data_directory = data_directory
        if not os.path.isdir(self.__data_directory):
            os.mkdir(self.__data_directory)
        self.__project = project
        self.__datasource = None

    def __getattr__(self, attr):
        if attr == 'data_directory':
            return self.__data_directory
        else:
            raise AttributeError

    def build_vrt(self, source_files, srid=None, name="terrain"):
        '''add a list of files to terrain data
        uses gdal to convert data to geotif'''
        created_files = []

        vrt_name = os.path.abspath(os.path.join(self.__data_directory, name+'.vrt'))
        if os.path.isfile(vrt_name):
            self.__project.log.warning("File {} already exists".format(vrt_name))
            return
        (id_,) = self.__project.execute(f"""insert into project.dem(source) values ('{vrt_name}') returning id;""").fetchone()

        progress = self.__project.log.progress(tr("Processing terrain data"))

        input_file = os.path.join(_tempdir, name+".txt")
        open(input_file, 'w').close() # clear file if exists

        for i, source_file_ in enumerate(source_files):
            progress.set_ratio(float(i+1)/len(source_files))

            created_files.append(os.path.join(self.__data_directory, name+"_"+str(id_)+"_"+str(i)+'.tif'))
            with open(input_file,'a') as f:
                f.write(created_files[-1] + '\n')

            cmd1 = ['gdalwarp', '-ot', 'Float32'] + (['-s_srs', 'EPSG:%d'%(int(srid))] if srid else []) + ['-t_srs', 'EPSG:%d'%(int(self.__project.srid)), source_file_, created_files[-1]]
            try:
                run_cmd(cmd1)
            except Exception as error:
                for f in created_files[:-1]:
                    os.remove(f)
                self.__project.execute("""delete from project.dem where id ={}""".format(id_))
                self.__project.log.error("Error building TIF {}".format(created_files[-1]))
                raise error
                return
        progress.set_ratio(1)
        self.__project.log.clear_progress()

        if created_files:
            cmd2 = ['gdalbuildvrt', '-input_file_list', input_file, vrt_name]
            try:
                run_cmd(cmd2, self.__project.log)
            except Exception:
                self.__project.execute("""delete from project.dem where id ={}""".format(id_))
                self.__project.log.error("Error generating VRT {}".format(vrt_name))
                return

            self.__project.log.notice("Generated file {}".format(vrt_name))

            # upload terrain on public server
            server = dbhydra.public_server()
            if server:
                archive = BytesIO()
                archive.name = os.path.basename(vrt_name).replace('.vrt', '.zip')
                with ZipFile(archive, 'w') as zip_archive:
                    for fil in [vrt_name]+created_files:
                        zip_archive.open(os.path.basename(fil), 'w').write(open(fil, 'rb').read())
                # open('/tmp/test.zip', 'wb').write(archive.getbuffer())
                res = requests.post(
                    server['url']+'/add_dem?name={}'.format(self.__project.name),
                    files={'zip_': archive.getvalue()},
                    headers={'authorization': '{token_type} {access_token}'.format(**server['token'])})
                if res.status_code != 200:
                    self.__project.execute("""delete from project.dem where id ={}""".format(id_))
                    raise RuntimeError('code: {} msg:{}'.format(res.status_code, res.text))

            return vrt_name

    def altitude(self, point, no_warning=False):
        if not self.has_data():
            if not no_warning:
                self.__project.log.error("No terrain data in this project")
            return 9999

        mx, my = (point[0], point[1]) if hasattr(point, '__getitem__') else (point.x(), point.y())
        return self.__project.execute("""
            select project.altitude({}, {})
            """.format(mx, my)).fetchone()[0]

    def has_data(self):
        return self.__project.execute("select count(1) from project.dem").fetchone()[0] > 0

    def error_files(self):
        return self.__project.fetchall("""
            select id, source from project.dem
            where not project.check_source(source)
            """)

    def dems(self):
        '''ordered by asc priority, i.e. top is last'''
        return [self.__project.unpack_path(f) for f, in self.__project.fetchall("""
            select source from project.dem order by priority asc""")]



    def line_elevation(self, line_):
        """
        line: sequence of (x,y) or (x,y,z) coordinates
        dem: name of a gdal-openable raster filewith elevation in first band
        line is discretized with the min pixel length
        values are bi-linearly interpolated
        """
        dems = self.dems()
        assert (len(dems) <= 1), 'multiple DEM not supported'
        if len(dems) == 0:
            return line_

        dem = dems[0]
        src = gdal.Open(dem)
        if src is None:
            raise Exception(f"'{dem}' not found")

        gt = src.GetGeoTransform()
        band = src.GetRasterBand(1)
        assert(gt[2] == 0 and gt[4] == 0)
        coords = numpy.array(line_)
        res = []
        # mp = []
        for s, e in zip(coords[:-1,:2], coords[1:,:2]):
            se = e-s
            L = numpy.linalg.norm(se)
            if L == 0:
                continue # repeated point
            n = 2*ceil(L/min(abs(gt[1]), abs(gt[5]))) # note, we can increase resolution here
            dse = se/n
            for i in range(n+1):
                x = s + i*dse
                fx = (x[0] - gt[0])/gt[1], (x[1] - gt[3])/gt[5]
                idx =  floor(fx[0]) + (0 if fx[0]%1 > .5 else -1), floor(fx[1]) + (0 if fx[1]%1 > .5 else -1)
                if idx[0] >=0 and idx[1] >= 0 and idx[0]+1 < src.RasterXSize and idx[1]+1 < src.RasterYSize:
                    a = band.ReadAsArray(idx[0], idx[1], 2, 2, buf_type=gdal.GDT_Float32)
                    # a[0][0] a[0][1]
                    # a[1][0] a[1][1]
                    # pixels as displayed in image (y is down direction)
                    # mp.append(Polygon([[idx[0]*gt[1]+gt[0], idx[1]*gt[5] + gt[3]],
                    #            [(idx[0]+2)*gt[1]+gt[0], idx[1]*gt[5] + gt[3]],
                    #            [(idx[0]+2)*gt[1]+gt[0], (idx[1]+2)*gt[5] + gt[3]],
                    #            [idx[0]*gt[1]+gt[0], (idx[1]+2)*gt[5] + gt[3]],
                    #            [idx[0]*gt[1]+gt[0], idx[1]*gt[5] + gt[3]]
                    #            ]))
                    u, v = fx[0] - idx[0] - .5, fx[1] - idx[1] - .5
                    #print(numpy.array([1-u, u]).dot(numpy.array(a))-1020)
                    h = numpy.array([1-v, v]).dot(numpy.array(a)).dot(numpy.array([[1-u, u]]).T)
                    #print(u,v,h-1020)

                    p = (x[0], x[1], h[0])
                    if len(res) == 0 or res[-1] != p:
                        res.append(p)
        #print(MultiPolygon([mp[0]]).wkt)
        return numpy.array(res)
