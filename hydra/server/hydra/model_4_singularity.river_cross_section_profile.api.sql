/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*       Abstract entities: CROSS-SECTION         */
/* ********************************************** */

create view $model.river_cross_section_profile as
    select g.id,
        c.name,
        case
            when c.type_cross_section_up is not null then c.z_invert_up
            else null
        end as z_invert_up,
        case
            when c.type_cross_section_down is not null then c.z_invert_down
            else null
        end as z_invert_down,
        c.type_cross_section_up,
        c.type_cross_section_down,
        c.up_rk,
        c.up_rk_maj,
        c.up_sinuosity,
        c.up_circular_diameter,
        c.up_ovoid_height,
        c.up_ovoid_top_diameter,
        c.up_ovoid_invert_diameter,
        c.up_cp_geom,
        c.up_op_geom,
        c.up_vcs_geom,
        c.up_vcs_topo_geom,
        c.down_rk,
        c.down_rk_maj,
        c.down_sinuosity,
        c.down_circular_diameter,
        c.down_ovoid_height,
        c.down_ovoid_top_diameter,
        c.down_ovoid_invert_diameter,
        c.down_cp_geom,
        c.down_op_geom,
        c.down_vcs_geom,
        c.down_vcs_topo_geom,
        c.configuration,
        (select case when (c.type_cross_section_up='channel' or c.type_cross_section_up='valley') then
            (select (c.z_invert_up))
        else (select st_z(st_startpoint(g.geom)))
        end) as z_tn_up,
        (select case when (c.type_cross_section_down='channel' or c.type_cross_section_down='valley') then
            (select (c.z_invert_down))
        else (select st_z(st_endpoint(g.geom)))
        end) as z_tn_down,
        (select case when c.type_cross_section_up='circular' then
            (select (c.z_invert_up+c.up_circular_diameter))
        when c.type_cross_section_up='ovoid' then
            (select (c.z_invert_up+c.up_ovoid_height))
        when c.type_cross_section_up='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.up_cp_geom = cpg.id)
            (select (c.z_invert_up + n.height) from n))
        when c.type_cross_section_up='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.up_op_geom = opg.id)
            (select (c.z_invert_up + n.height) from n))
        when c.type_cross_section_up='valley' then
            (with n as (select (zbmaj_lbank_array[1][1] - zbmin_array[1][1]) as height
                from ${model}.valley_cross_section_geometry opg
                where c.up_vcs_geom = opg.id)
            (select (c.z_invert_up + n.height) from n))
        else null
        end) as z_lbank_up,
        (select case when c.type_cross_section_down='circular' then
            (select (c.z_invert_down+c.down_circular_diameter))
        when c.type_cross_section_down='ovoid' then
            (select (c.z_invert_down+c.down_ovoid_height))
        when c.type_cross_section_down='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.down_cp_geom = cpg.id)
            (select (c.z_invert_down + n.height) from n))
        when c.type_cross_section_down='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.down_op_geom = opg.id)
            (select (c.z_invert_down + n.height) from n))
        when c.type_cross_section_down='valley' then
            (with n as (select (zbmaj_lbank_array[1][1] - zbmin_array[1][1]) as height
                from ${model}.valley_cross_section_geometry opg
                where c.down_vcs_geom = opg.id)
            (select (c.z_invert_down + n.height) from n))
        else null
        end) as z_lbank_down,
        (select case when c.type_cross_section_up='circular' then
            (select (c.z_invert_up+c.up_circular_diameter))
        when c.type_cross_section_up='ovoid' then
            (select (c.z_invert_up+c.up_ovoid_height))
        when c.type_cross_section_up='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.up_cp_geom = cpg.id)
            (select (c.z_invert_up + n.height) from n))
        when c.type_cross_section_up='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.up_op_geom = opg.id)
            (select (c.z_invert_up + n.height) from n))
        when c.type_cross_section_up='valley' then
            (with n as (select (zbmaj_rbank_array[1][1] - zbmin_array[1][1]) as height
                from ${model}.valley_cross_section_geometry opg
                where c.up_vcs_geom = opg.id)
            (select (c.z_invert_up + n.height) from n))
        else null
        end) as z_rbank_up,
        (select case when c.type_cross_section_down='circular' then
            (select (c.z_invert_down+c.down_circular_diameter))
        when c.type_cross_section_down='ovoid' then
            (select (c.z_invert_down+c.down_ovoid_height))
        when c.type_cross_section_down='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.down_cp_geom = cpg.id)
            (select (c.z_invert_down + n.height) from n))
        when c.type_cross_section_down='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.down_op_geom = opg.id)
            (select (c.z_invert_down + n.height) from n))
        when c.type_cross_section_down='valley' then
            (with n as (select (zbmaj_rbank_array[1][1] - zbmin_array[1][1]) as height
                from ${model}.valley_cross_section_geometry opg
                where c.down_vcs_geom = opg.id)
            (select (c.z_invert_down + n.height) from n))
        else null
        end) as z_rbank_down,
        (select case when c.type_cross_section_down='valley' then
            null
        when c.type_cross_section_down='channel' then
            null
        else
            (with n as (select zbmin_array[array_length(zbmin_array, 1)][1] as height
                from ${model}.closed_parametric_geometry opg
                where c.down_op_geom = opg.id)
            select (c.z_invert_up + n.height) from n)
        end) as z_ceiling_up,
        (select case when c.type_cross_section_down='valley' then
            null
        when c.type_cross_section_down='channel' then
            null
        else
            (with n as (select zbmin_array[array_length(zbmin_array, 1)][1] as height
                from ${model}.closed_parametric_geometry opg
                where c.down_op_geom = opg.id)
            select (c.z_invert_down + n.height) from n)
        end) as z_ceiling_down,
        c.validity,
        c.comment,
        g.geom
    from $model._river_cross_section_profile as c, $model._node as g
    where g.id = c.id
;;

/* ********************************************** */
/*                  Triggers                      */
/* ********************************************** */

-- Insert & Update

create function ${model}.river_cross_section_profile_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        select (
            (new.type_cross_section_up is null or (
                new.z_invert_up is not null
                and new.up_rk is not null
                and (new.type_cross_section_up!='circular' or (new.up_circular_diameter is not null and new.up_circular_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_top_diameter is not null and new.up_ovoid_top_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_invert_diameter is not null and new.up_ovoid_invert_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_height is not null and new.up_ovoid_height >0 ))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_height > (new.up_ovoid_top_diameter+new.up_ovoid_invert_diameter)/2))
                and (new.type_cross_section_up!='pipe' or new.up_cp_geom is not null)
                and (new.type_cross_section_up!='channel' or new.up_op_geom is not null)
                and (new.type_cross_section_up!='valley'
                        or (new.up_rk = 0 and new.up_rk_maj = 0 and new.up_sinuosity = 0)
                        or (new.up_rk >0 and new.up_rk_maj is not null and new.up_rk_maj > 0 and new.up_sinuosity is not null and new.up_sinuosity > 0))
                and (new.type_cross_section_up!='valley' or new.up_vcs_geom is not null)
                )
            )
            and
            (new.type_cross_section_down is null or (
                new.z_invert_down is not null
                and new.down_rk is not null
                and (new.type_cross_section_down!='circular' or (new.down_circular_diameter is not null and new.down_circular_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_top_diameter is not null and new.down_ovoid_top_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_invert_diameter is not null and new.down_ovoid_invert_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_height is not null and new.down_ovoid_height >0 ))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_height > (new.down_ovoid_top_diameter+new.down_ovoid_invert_diameter)/2))
                and (new.type_cross_section_down!='pipe' or new.down_cp_geom is not null)
                and (new.type_cross_section_down!='channel' or new.down_op_geom is not null)
                and (new.type_cross_section_down!='valley'
                        or (new.down_rk = 0 and new.down_rk_maj = 0 and new.down_sinuosity = 0)
                        or (new.down_rk >0 and new.down_rk_maj is not null and new.down_rk_maj > 0 and new.down_sinuosity is not null and new.down_sinuosity > 0))
                and (new.type_cross_section_down!='valley' or new.down_vcs_geom is not null)
                )
            )
            and
            (new.type_cross_section_up is null or new.type_cross_section_down is null or (
                new.z_invert_up is not null and new.z_invert_down is not null
                )
            )
        ) into new.validity;

        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id from $model.river_node where ST_DWithin(new.geom, geom, .1) into new.id;
            select coalesce(new.name, 'CP'||new.id::varchar) into new.name;
            if new.id is null then
                raise exception 'no river node nearby river_cross_section_profile %', st_astext(new.geom);
            end if;

            insert into $model._river_cross_section_profile (
                    id,
                    name,
                    z_invert_up,
                    z_invert_down,
                    type_cross_section_up,
                    type_cross_section_down,
                    up_rk,
                    up_rk_maj,
                    up_sinuosity,
                    up_circular_diameter,
                    up_ovoid_height,
                    up_ovoid_top_diameter,
                    up_ovoid_invert_diameter,
                    up_cp_geom,
                    up_op_geom,
                    up_vcs_geom,
                    up_vcs_topo_geom,
                    down_rk,
                    down_rk_maj,
                    down_sinuosity,
                    down_circular_diameter,
                    down_ovoid_height,
                    down_ovoid_top_diameter,
                    down_ovoid_invert_diameter,
                    down_cp_geom,
                    down_op_geom,
                    down_vcs_geom,
                    down_vcs_topo_geom,
                    configuration,
                    validity,
                    comment
                )
                values (
                    new.id,
                    new.name,
                    new.z_invert_up,
                    new.z_invert_down,
                    new.type_cross_section_up,
                    new.type_cross_section_down,
                    new.up_rk,
                    new.up_rk_maj,
                    new.up_sinuosity,
                    new.up_circular_diameter,
                    new.up_ovoid_height,
                    new.up_ovoid_top_diameter,
                    new.up_ovoid_invert_diameter,
                    new.up_cp_geom,
                    new.up_op_geom,
                    new.up_vcs_geom,
                    new.up_vcs_topo_geom,
                    new.down_rk,
                    new.down_rk_maj,
                    new.down_sinuosity,
                    new.down_circular_diameter,
                    new.down_ovoid_height,
                    new.down_ovoid_top_diameter,
                    new.down_ovoid_invert_diameter,
                    new.down_cp_geom,
                    new.down_op_geom,
                    new.down_vcs_geom,
                    new.down_vcs_topo_geom,
                    new.configuration::json,
                    new.validity,
                    new.comment
                );
            if new.z_invert_up is not null or new.z_invert_down is not null then
                update $model.river_node
                    set geom=ST_SetSRID(ST_MakePoint(ST_X(geom), ST_Y(geom), coalesce($model.river_node_z_invert(geom), project.altitude(geom))), $srid)
                    where reach=(select reach from $model.river_node where id=new.id);
            end if;

            perform $model.add_configuration_fct(new.configuration::json, new.id, 'river_cross_section_profile');

            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id from $model.river_node where ST_DWithin(new.geom, geom, .1) into new.id;
            select coalesce(new.name, 'CP'||new.id::varchar) into new.name;
            if new.id is null then
                raise exception 'no river node nearby river_cross_section_profile %', st_astext(new.geom);
            end if;

            -- Handle configurations
            if ((new.z_invert_up, new.z_invert_down,
                    new.type_cross_section_up, new.type_cross_section_down,
                    new.up_rk, new.up_rk_maj, new.up_sinuosity, new.up_circular_diameter,
                    new.up_ovoid_height, new.up_ovoid_top_diameter, new.up_ovoid_invert_diameter,
                    new.up_cp_geom, new.up_op_geom, new.up_vcs_geom, new.up_vcs_topo_geom,
                    new.down_rk, new.down_rk_maj, new.down_sinuosity, new.down_circular_diameter,
                    new.down_ovoid_height, new.down_ovoid_top_diameter, new.down_ovoid_invert_diameter,
                    new.down_cp_geom, new.down_op_geom, new.down_vcs_geom, new.down_vcs_topo_geom)
            is distinct from (old.z_invert_up, old.z_invert_down,
                    old.type_cross_section_up, old.type_cross_section_down,
                    old.up_rk, old.up_rk_maj, old.up_sinuosity, old.up_circular_diameter,
                    old.up_ovoid_height, old.up_ovoid_top_diameter, old.up_ovoid_invert_diameter,
                    old.up_cp_geom, old.up_op_geom, old.up_vcs_geom, old.up_vcs_topo_geom,
                    old.down_rk, old.down_rk_maj, old.down_sinuosity, old.down_circular_diameter,
                    old.down_ovoid_height, old.down_ovoid_top_diameter, old.down_ovoid_invert_diameter,
                    old.down_cp_geom, old.down_op_geom, old.down_vcs_geom, old.down_vcs_topo_geom))
            then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}'
                            from (select old.z_invert_up, old.z_invert_down,
                                old.type_cross_section_up, old.type_cross_section_down,
                                old.up_rk, old.up_rk_maj, old.up_sinuosity, old.up_circular_diameter,
                                old.up_ovoid_height, old.up_ovoid_top_diameter, old.up_ovoid_invert_diameter,
                                old.up_cp_geom, old.up_op_geom, old.up_vcs_geom, old.up_vcs_topo_geom,
                                old.down_rk, old.down_rk_maj, old.down_sinuosity, old.down_circular_diameter,
                                old.down_ovoid_height, old.down_ovoid_top_diameter, old.down_ovoid_invert_diameter,
                                old.down_cp_geom, old.down_op_geom, old.down_vcs_geom, old.down_vcs_topo_geom) as o,
                                (select new.z_invert_up, new.z_invert_down,
                                new.type_cross_section_up, new.type_cross_section_down,
                                new.up_rk, new.up_rk_maj, new.up_sinuosity, new.up_circular_diameter,
                                new.up_ovoid_height, new.up_ovoid_top_diameter, new.up_ovoid_invert_diameter,
                                new.up_cp_geom, new.up_op_geom, new.up_vcs_geom, new.up_vcs_topo_geom,
                                new.down_rk, new.down_rk_maj, new.down_sinuosity, new.down_circular_diameter,
                                new.down_ovoid_height, new.down_ovoid_top_diameter, new.down_ovoid_invert_diameter,
                                new.down_cp_geom, new.down_op_geom, new.down_vcs_geom, new.down_vcs_topo_geom) as n
                            into new_config;
                        update $model._river_cross_section_profile set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}'
                            from (select new.z_invert_up, new.z_invert_down,
                                new.type_cross_section_up, new.type_cross_section_down,
                                new.up_rk, new.up_rk_maj, new.up_sinuosity, new.up_circular_diameter,
                                new.up_ovoid_height, new.up_ovoid_top_diameter, new.up_ovoid_invert_diameter,
                                new.up_cp_geom, new.up_op_geom, new.up_vcs_geom, new.up_vcs_topo_geom,
                                new.down_rk, new.down_rk_maj, new.down_sinuosity, new.down_circular_diameter,
                                new.down_ovoid_height, new.down_ovoid_top_diameter, new.down_ovoid_invert_diameter,
                                new.down_cp_geom, new.down_op_geom, new.down_vcs_geom, new.down_vcs_topo_geom) n
                            into new_config;
                        update $model._river_cross_section_profile set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._river_cross_section_profile set
                id=new.id,
                name=new.name,
                z_invert_up=new.z_invert_up,
                z_invert_down=new.z_invert_down,
                type_cross_section_up=new.type_cross_section_up,
                type_cross_section_down=new.type_cross_section_down,
                up_rk=new.up_rk,
                up_rk_maj=new.up_rk_maj,
                up_sinuosity=new.up_sinuosity,
                up_circular_diameter=new.up_circular_diameter,
                up_ovoid_height=new.up_ovoid_height,
                up_ovoid_top_diameter=new.up_ovoid_top_diameter,
                up_ovoid_invert_diameter=new.up_ovoid_invert_diameter,
                up_cp_geom=new.up_cp_geom,
                up_op_geom=new.up_op_geom,
                up_vcs_geom=new.up_vcs_geom,
                up_vcs_topo_geom=new.up_vcs_topo_geom,
                down_rk=new.down_rk,
                down_rk_maj=new.down_rk_maj,
                down_sinuosity=new.down_sinuosity,
                down_circular_diameter=new.down_circular_diameter,
                down_ovoid_height=new.down_ovoid_height,
                down_ovoid_top_diameter=new.down_ovoid_top_diameter,
                down_ovoid_invert_diameter=new.down_ovoid_invert_diameter,
                down_cp_geom=new.down_cp_geom,
                down_op_geom=new.down_op_geom,
                down_vcs_geom=new.down_vcs_geom,
                down_vcs_topo_geom=new.down_vcs_topo_geom,
                validity=new.validity,
                comment=new.comment
            where id=old.id;

            if new.z_invert_up != old.z_invert_down or new.z_invert_down != old.z_invert_down then
                update $model.river_node
                    set geom=ST_SetSRID(ST_MakePoint(ST_X(geom), ST_Y(geom), coalesce($model.river_node_z_invert(geom), project.altitude(geom))), $srid)
                    where reach=(select reach from $model.river_node where id=new.id);
            end if;

            perform $model.add_configuration_fct(new.configuration::json, new.id, 'river_cross_section_profile');

            return new;
        end if;
    end;
$$$$
;;

create trigger ${model}_river_cross_section_profile_trig
    instead of insert or update on $model.river_cross_section_profile
       for each row execute procedure ${model}.river_cross_section_profile_fct()
;;

-- Delete

create function ${model}.river_cross_section_profile_del_fct()
returns trigger
language plpgsql
as $$$$
    begin
        delete from $model._river_cross_section_profile where id=old.id;
        return old;
    end;
$$$$
;;

create trigger ${model}_river_cross_section_profile_del_trig
    instead of delete on $model.river_cross_section_profile
       for each row execute procedure ${model}.river_cross_section_profile_del_fct()
;;


/* ********************************************** */
/*                     PL1D                       */
/* ********************************************** */

create view $model.river_cross_section_pl1d as
    select g.id, c.name, c.profile, c.generated, g.geom
    from $model._river_cross_section_pl1d as c, $model._node as g
    where g.id = c.id
;;

create function ${model}.river_cross_section_pl1d_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id from $model.river_node where ST_DWithin(new.profile, geom, .1) into id_;
            if id_ is null then
                raise exception 'no river node nearby river_cross_section_pl1d %', st_astext(new.geom);
            end if;
            insert into $model._river_cross_section_pl1d
                values (id_, coalesce(new.name, 'PL1D'||id_::varchar), new.profile, new.generated);
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update $model._river_cross_section_pl1d set name=new.name, profile=new.profile, generated=new.generated where id=old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._river_cross_section_pl1d where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_river_cross_section_pl1d_trig
    instead of insert or update or delete on $model.river_cross_section_pl1d
       for each row execute procedure ${model}.river_cross_section_pl1d_fct()
;;
