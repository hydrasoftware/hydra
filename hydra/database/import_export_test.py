# coding=UTF-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run all tests

USAGE

    python -m hydra.database.import_export_test.py [-kh] [model]

    if model is specified, only export this model and keep it


OPTIONS

   -h, --help
        print this help

    -k, --keep
        project and models, do export only

"""
from __future__ import absolute_import # important to read the doc !
import sip

import os, sys
import getopt
from qgis.PyQt.QtWidgets import QApplication
from qgis.PyQt.QtCore import QCoreApplication, QTranslator
from hydra.project import Project
from hydra.database.import_model import import_model_csv
from hydra.database.export_calcul import ExportCalcul
from hydra.gui.scenario_manager import ScenarioManager
from hydra.gui.rain_manager import RainManager
from hydra.database.database import TestProject, project_exists, remove_project

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hk",
            ["help", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

only = args if len(args) else None

keep = True if "-k" in optlist or "--keep" in optlist or only else False

if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

data_dir = os.path.join(os.path.dirname(__file__), 'test_data', 'light_projects')

project_name = 'import_export_test'

if project_exists(project_name):
    remove_project(project_name)

models = [os.path.join(data_dir, f)
        for f in os.listdir(data_dir) if f.endswith(".csv")]

test_project = TestProject(project_name, keep)

project = Project.load_project(project_name)
project.delete_model('model')

id_rainfall, = project.execute("""
    with montana as (
       insert into project.coef_montana(return_period_yr, coef_a, coef_b)
       values (0, 1, -1) returning id)
    insert into project.caquot_rainfall(montana)
    select id from montana returning id
    """).fetchone()

id_scenario = project.add_new_scenario()
project.execute("""update project.scenario set
        rainfall={},
        dtmin_hydrau_hr='01:00:00',
        tfin_hr='12:00:00'
        where id={}
        """.format(id_rainfall, id_scenario))
project.commit()

for model in models:
    model_name = os.path.split(model)[1][:-4]
    if only and model_name not in only:
        continue

    if model_name in project.get_models():
        project.delete_model(model_name)
    project.add_new_model(model_name)
    project.set_current_model(model_name)
    import_model_csv(model, project, project.get_current_model(), model_name)

    model = project.get_current_model()

    exporter = ExportCalcul(project)
    exporter.export(id_scenario)
    scenario_dir = project.get_senario_path(id_scenario)

    if not keep:
        project.delete_model(model_name)
    project.commit()

print('ok')
