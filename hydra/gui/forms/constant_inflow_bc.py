# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_singularity_dialog import SimpleSingularityDialog, tr
import hydra.utility.string as string
from qgis.PyQt import uic
import os

class ConstantInflowBcEditor(SimpleSingularityDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleSingularityDialog.__init__(self, project, geom, "constant_inflow_bc_singularity", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "constant_inflow_bc.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        name, q0, comment = self.project.execute("""
            select name, q0, comment
            from {}.constant_inflow_bc_singularity
            where id={}""".format(self.model.name, self.id)).fetchone()
        self.name.setText(name)
        self.q0.setText(string.get_str(q0))
        self.notes.setText(comment)

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.model.update_ci_singularity(self.id, self.geom, string.get_sql_float(self.q0.text()), self.name.text(), comment=self.notes.text())
        SimpleSingularityDialog.save(self)
        self.close()
