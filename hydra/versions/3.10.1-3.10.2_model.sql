alter table ${model}._catchment_node
rename column hydra_inf_rate_f0_mm_day to hydra_inf_rate_f0_mm_hr
;;

update ${model}._catchment_node
set hydra_inf_rate_f0_mm_hr=hydra_inf_rate_f0_mm_hr/24.
;;

update ${model}._node
set configuration=replace(configuration::varchar, 'hydra_inf_rate_f0_mm_day', 'hydra_inf_rate_f0_mm_hr')::json
where node_type='catchment'
;;
