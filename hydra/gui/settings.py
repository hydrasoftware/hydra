# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
Settings is a dictionnary in hydra.config

First key : external ressource
Second key : specific value

{'postgre': {'path' : postgrepath}}

"""
from builtins import map
from builtins import str
from builtins import range

import json
import os
import re
import datetime
from configparser import ConfigParser
from functools import partial
from qgis.PyQt import uic
from qgis.PyQt.QtCore import Qt, QCoreApplication
from qgis.PyQt.QtWidgets import QDialog, QFileDialog, QCheckBox, QPushButton, QColorDialog, \
    QWidget, QVBoxLayout, QMessageBox, QTableWidgetItem, QLabel, QLineEdit
from qgis.PyQt.QtGui import QColor, QPalette
from hydra.utility.settings_properties import SettingsProperties

_desktop_dir = os.path.join(os.path.expanduser('~'), "Desktop")

_layers = {'results_river': 'River nodes',
           'results_manhole': 'Manholes',
           'results_surface': '2D elements',
           'results_pipe': 'Pipes',
           'results_pipe_hydrol': 'Hydrology pipes',
           'results_link': 'Links',
           'results_catchment': 'Catchments'}

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)


class HydraSettings(QDialog):
    def __init__(self, parent=None, project=None):
        QDialog.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "settings.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.service.currentTextChanged.connect(self.__refresh_service_label)
        self.postgre_btn.clicked.connect(lambda: self.__browse('path_postgre'))
        self.python_btn.clicked.connect(lambda: self.__browse('path_python'))

        self.service_file = os.environ['PGSERVICEFILE'] if 'PGSERVICEFILE' in os.environ else os.path.join(os.path.expanduser('~'), '.pg_service.conf')
        self.services = ConfigParser(default_section='hydra')
        self.services.read(self.service_file)
        if 'user' not in self.services['hydra']:
           self.services['hydra']={'user':'hydra', 'password':'hydra', 'host':'127.0.0.1', 'port':'5454'}
        if 'hydra_remote' not in self.services:
           self.services['hydra_remote']={'user':'', 'password':'', 'host':'database.hydra-software.net', 'port':'5444'}

        for service in self.services:
            self.service.addItem(service)

        self.service.setCurrentText(SettingsProperties.get_service())
        self.path_postgre.setText(str(SettingsProperties.get_path('postgre')))
        self.path_python.setText(str(SettingsProperties.get_path('python')))

        self.snap.setText(str(SettingsProperties.get_snap()))

        self.project = project
        self.settings_tab.setTabEnabled(3, self.project is not None and self.project.get_current_model() is not None)
        if self.project is not None and self.project.get_current_model() is not None:
            self.label_current_model.setText(self.project.get_current_model().name)
        self.refresh_synthetic_results()

    def __refresh_service_label(self):
        #self.service_content.setText('<br>'.join([f"{key} = {self.services[self.service.currentText()][key]}" for key in self.services[self.service.currentText()]]))
        layout = self.serviceAttributesLayout
        for i in reversed(range(layout.count())):
            layout.itemAt(i).widget().setParent(None)

        for i, (k, v) in enumerate(
                self.services[self.service.currentText()].items()
                if self.service.currentText() in self.services
                else [('user', ''), ('password', ''), ('host', ''), ('port', '5432')]):
            layout.addWidget(QLabel(k), i, 0)
            layout.addWidget(QLineEdit(v), i, 1)

    def __browse(self, external):
        title = tr('Select {} path:').format(external)
        dir_name = QFileDialog.getExistingDirectory(None, title, _desktop_dir)
        if dir_name:
            getattr(self, external).setText(dir_name)

    def __select_color(self, index, code):
        in_color = self.__get_color('color_button_{}_{}'.format(code, index))
        out_color = QColorDialog.getColor(QColor(*in_color))
        if out_color.isValid():
            getattr(self, 'color_button_{}_{}'.format(code, index)).setStyleSheet("background-color:rgb{}".format(out_color.getRgb()))

    def __set_classes(self, array):
        if len(array)==5:
            type="velocity"
        elif len(array)==6:
            type="water_level"
        else:
            raise AttributeError("Wrong number of classes !")

        for i in range(len(array)):
            getattr(self, '{}_{}'.format(type, i)).setText(str(array[i]))

    def __set_color(self, color_array):
        if len(color_array)==5:
            type="vlc"
        elif len(color_array)==6:
            type="lvl"
        else:
            raise AttributeError("Wrong number of classes !")
        for i in range(len(color_array)):
            color = QColor(*color_array[i])
            getattr(self, 'color_button_{}_{}'.format(type, i)).setStyleSheet("background-color:rgb{}".format(color.getRgb()))

    def __get_color(self, button_name):
        style = getattr(self, button_name).styleSheet()
        color_array = style.split('(')[1].strip(')').split(', ')
        return list(map(int, color_array))

    def __get_colors(self, type):
        if type=="velocity":
            l=5
            code="vlc"
        elif type=="water_level":
            l=6
            code="lvl"
        else:
            raise AttributeError("Wrong number of classes !")

        colors=[]
        for i in range(0,l):
            colors.append(self.__get_color('color_button_{}_{}'.format(code, i)))
        return colors

    def refresh_synthetic_results(self):
        if self.project is not None:
            self.table.clear()
            self.table.setRowCount(0)
            self.table.setSortingEnabled(False)
            self.table.setHorizontalHeaderLabels(['Item count', 'Last refresh', 'Refresh', 'Clear'])

            self.vertical_header = []

            # Results Materialized views
            self.refresh_mat_views_lines()

            # Mesh 1D
            self.refresh_1d_mesh_lines()

            self.table.resizeRowsToContents()
            self.table.resizeColumnsToContents()
            self.table.setVerticalHeaderLabels(self.vertical_header)
            self.table.horizontalHeader().setStretchLastSection(True)

    def refresh_mat_views_lines(self):
        if self.project is not None and self.project.get_current_model() is not None:
            model_name = self.project.get_current_model().name

            for mat_view, label in _layers.items():
                row_position = self.table.rowCount()
                self.table.insertRow(row_position)
                self.vertical_header.append(label)

                has_data = self.project.execute("""select ispopulated from pg_matviews where schemaname='{}' and matviewname='{}'""".format(model_name, mat_view)).fetchone()[0]
                if not has_data:
                    self.table.setItem(row_position, 0, QTableWidgetItem(tr('Empty')))
                    self.table.item(row_position, 0).setForeground(QColor(255,0,0))
                    self.table.setItem(row_position, 1, QTableWidgetItem(tr('None')))
                    self.table.item(row_position, 1).setForeground(QColor(255,0,0))
                else:
                    count = self.project.execute("""select count(1) from {}.{}""".format(model_name, mat_view)).fetchone()[0]
                    self.table.setItem(row_position, 0, QTableWidgetItem(str(count)))
                    if count >0:
                        last_refresh = self.project.execute("""select last_refresh from {}.{} limit 1""".format(model_name, mat_view)).fetchone()[0]
                        self.table.setItem(row_position, 1, QTableWidgetItem(last_refresh.strftime('%H:%M - %d/%m/%Y')))
                    else:
                        self.table.setItem(row_position, 1, QTableWidgetItem(tr('None')))
                        self.table.item(row_position, 1).setForeground(QColor(255,0,0))

                btn_refresh = QPushButton('Refresh')
                btn_refresh.clicked.connect(partial(self.__populate_view, mat_view, label))
                self.table.setCellWidget(row_position, 2, btn_refresh)

                btn_clear = QPushButton('Clear')
                btn_clear.clicked.connect(partial(self.__clear_view, mat_view, label))
                self.table.setCellWidget(row_position, 3, btn_clear)

    def __populate_view(self, mat_view, name=None):
        if name is None:
            name = mat_view.capitalize().replace('_', ' ')

        if self.project is not None and self.project.get_current_model() is not None:
            model_name = self.project.get_current_model().name
            try :
                self.project.execute("""refresh materialized view {}.{} ;""".format(model_name, mat_view))
                nb = self.project.execute("""select count(*) from {}.{} ;""".format(model_name, mat_view)).fetchone()[0]
                QMessageBox.information(self, tr('Generation successfull'), tr('Generation of {} for model {} successful. {} items created.'.format(name, model_name, nb)), QMessageBox.Ok)
            except Exception as e :
                self.project.log.error(e)
                QMessageBox.critical(self, tr('Generation failed'), tr('see log file for more details.').format(model_name), QMessageBox.Ok)
        self.refresh_synthetic_results()

    def __clear_view(self, mat_view, name=None):
        if name is None:
            name = mat_view.capitalize().replace('_', ' ')

        if self.project is not None and self.project.get_current_model() is not None:
            model_name = self.project.get_current_model().name
            try :
                self.project.execute("""refresh materialized view {}.{} with no data;""".format(model_name, mat_view))
                QMessageBox.information(self, tr('Clear successfull'), tr('{} of model {} successfully cleared'.format(name, model_name)), QMessageBox.Ok)
            except Exception as e :
                QMessageBox.critical(self, tr('Clear failed'), tr('See log file for more details.').format(model_name), QMessageBox.Ok)
        self.refresh_synthetic_results()

    def refresh_1d_mesh_lines(self):
        if self.project is not None and self.project.get_current_model() is not None:
            model_name = self.project.get_current_model().name

            row_position = self.table.rowCount()
            self.table.insertRow(row_position)
            self.vertical_header.append('1D domain triangles')

            has_data = self.project.execute("""select ispopulated from pg_matviews where schemaname='{}' and matviewname='mesh_1d_coted'""".format(model_name)).fetchone()[0]
            if not has_data:
                self.table.setItem(row_position, 0, QTableWidgetItem(tr('Empty')))
                self.table.item(row_position, 0).setForeground(QColor(255,0,0))
                self.table.setItem(row_position, 1, QTableWidgetItem(tr('None')))
                self.table.item(row_position, 1).setForeground(QColor(255,0,0))
            else:
                count = self.project.execute("""select count(1) from {}.mesh_1d_coted""".format(model_name)).fetchone()[0]
                self.table.setItem(row_position, 0, QTableWidgetItem(str(count)))
                if count >0:
                    last_refresh = self.project.execute("""select last_refresh from {}.mesh_1d_coted limit 1""".format(model_name)).fetchone()[0]
                    self.table.setItem(row_position, 1, QTableWidgetItem(last_refresh.strftime('%H:%M - %d/%m/%Y')))
                else:
                    self.table.setItem(row_position, 1, QTableWidgetItem(tr('None')))
                    self.table.item(row_position, 1).setForeground(QColor(255,0,0))

            btn_regen = QPushButton('Refresh')
            btn_regen.setToolTip(tr('Warning: triangulation can take some time'))
            btn_regen.clicked.connect(self.__regen_1d)
            btn_clear = QPushButton('Clear')
            btn_clear.clicked.connect(self.__clear_1d)
            self.table.setCellWidget(row_position, 2, btn_regen)
            self.table.setCellWidget(row_position, 3, btn_clear)

    def __regen_1d(self):
        if self.project is not None and self.project.get_current_model() is not None:
            model_name = self.project.get_current_model().name
            try :
                self.project.execute("""select {}.mesh_all_1d()""".format(model_name))
                nb = self.project.execute("""select count(*) from {}.mesh_1d_coted ;""".format(model_name)).fetchone()[0]
                QMessageBox.information(self, tr('Generation successfull'), tr('Model {} successfully meshed, {} triangles created'.format(model_name, nb)), QMessageBox.Ok)
            except Exception as e :
                QMessageBox.critical(self, tr('Generation failed'), tr('See log file for more details.').format(model_name), QMessageBox.Ok)
        self.refresh_synthetic_results()

    def __clear_1d(self):
        if self.project is not None and self.project.get_current_model() is not None:
            model_name = self.project.get_current_model().name
            try :
                self.project.execute("""refresh materialized view {}.mesh_1d_coted with no data;""".format(model_name))
                QMessageBox.information(self, tr('Clear successfull'), tr('Triangles of model {} successfully cleared'.format(model_name)), QMessageBox.Ok)
            except Exception as e :
                QMessageBox.critical(self, tr('Clear failed'), tr('See log file for more details.').format(model_name), QMessageBox.Ok)
        self.refresh_synthetic_results()

    def save(self):
        SettingsProperties.write_settings_file(float(self.snap.text()), self.service.currentText(),
                                                self.path_postgre.text().strip(), self.path_python.text().strip())

        layout = self.serviceAttributesLayout
        self.services[self.service.currentText()] = {
            layout.itemAt(i).widget().text(): layout.itemAt(i+1).widget().text()
            for i in range(0, layout.count(), 2)}

        with open(self.service_file, 'w', newline='\n') as cf:
            self.services.write(cf, False)

        if self.project is not None:
            self.project.commit()

        self.close()