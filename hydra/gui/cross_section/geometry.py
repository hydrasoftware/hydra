# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import re
from numpy import array
from matplotlib.colors import ListedColormap, BoundaryNorm
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QWidget, QGridLayout, QMessageBox, QLabel
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtCore import pyqtSignal
from .utility import hydraulic_profile, project_topo

class Geometry(QWidget):

    geometry_changed = pyqtSignal(dict)
    transect_changed = pyqtSignal(list)

    def __init__(self, sz, dz, transect, terrain, parent=None):

        super().__init__(parent)
        self.__sz = array(sz)
        self.__dz = dz

        self.__transect_geom = transect
        self.__topo, self.__ref, self.__dem = project_topo(terrain, transect, None, None, True)

        self.__riverbed_graph = None
        self.__ref_graph = None
        self.__r_graph = None
        self.__topo_graph = None
        self.__topo_graph_fill = None
        self.__r_topo_graph = None
        self.__dem_graph = None
        self.__sz_graph = None
        self.__simplified_graph = None
        self.__simplified_graph_fill = None
        self.__simplified_no_offset_graph = None
        self.__l_levee_graph = None
        self.__r_levee_graph = None
        self.__invalidity_graph = None

    def set_sz(self, sz):
        self.__sz = array(sz)

    def set_dz(self, dz):
        self.__dz = dz

    def __del__(self):
        self.__clear_axes()

    def __getattr__(self, name):
        if name == 'simplified':
            return self.sz
        elif name == 'sz':
            return self.__sz + array([0, self.__dz]) if self.__sz is not None else None
        elif name == 'dz':
            return self.__dz
        elif name == 'z_invert':
            return self.__dz
        elif name == 'topo':
            return self.__topo
        elif name == 'dem':
            return self.__dem
        elif name == 'ref':
            return self.__ref
        elif name == 'center':
            return 0
        elif name == 'left_levee':
            return None
        elif name == 'right_levee':
            return None
        elif name == 'riverbed':
            return None
        elif name == 'transect':
            return list(self.__transect_geom.coords) if self.__transect_geom is not None else None
        elif name == 'invalidity':
            return None
        elif name == 'param':
            return None
        elif name == 'water':
            return self.simplified
        else:
            raise AttributeError(name)

    def add_point(self, p):
        pass

    def remove_point(self, idx):
        pass

    def select_point(self, pos, dx, dy):
        pass

    def move_point(self, orig, dest):
        pass

    def __clear_axes(self):
        if self.__riverbed_graph is not None:
            for g in self.__riverbed_graph:
                g.remove()
        self.__riverbed_graph = None
        self.__ref_graph and self.__ref_graph.remove()
        self.__ref_graph = None
        self.__r_graph and self.__r_graph.remove()
        self.__r_graph = None
        self.__topo_graph and self.__topo_graph.remove()
        self.__topo_graph = None
        self.__r_topo_graph and self.__r_topo_graph.remove()
        self.__r_topo_graph = None
        self.__topo_graph_fill and self.__topo_graph_fill.remove()
        self.__topo_graph_fill = None
        self.__dem_graph and self.__dem_graph.remove()
        self.__dem_graph = None
        self.__sz_graph and self.__sz_graph.remove()
        self.__sz_graph = None
        self.__simplified_graph and self.__simplified_graph.remove()
        self.__simplified_graph = None
        self.__simplified_graph_fill and self.__simplified_graph_fill.remove()
        self.__simplified_graph_fill = None
        self.__simplified_no_offset_graph and self.__simplified_no_offset_graph.remove()
        self.__simplified_no_offset_graph = None
        self.__l_levee_graph and self.__l_levee_graph.remove()
        self.__l_levee_graph = None
        self.__r_levee_graph and self.__r_levee_graph.remove()
        self.__r_levee_graph = None
        self.__invalidity_graph and self.__invalidity_graph.remove()
        self.__invalidity_graph = None

    def draw(self, axes, axes_r):
        self.__clear_axes()
        axes_r.set_ylim(axes.get_ylim())

        invalidity = self.invalidity
        if invalidity is not None:
            xlim = axes.get_xlim()
            ylim = axes.get_ylim()
            self.__invalidity_graph = axes.annotate(invalidity, xy=(xlim[0]+.05*(ylim[1]-ylim[0]), ylim[0]+.95*(ylim[1]-ylim[0])), size=15)
            axes.set_facecolor("pink")
        else:
            axes.set_facecolor("white")


        if self.topo is not None:
            self.__topo_graph_fill = axes.fill_between(self.topo[:,0], self.topo[:,1], -999, color='goldenrod', linewidth=.5, alpha=.1)
            self.__topo_graph, = axes.plot(self.topo[:,0], self.topo[:,1], color='black', linewidth=.5)


        if self.dem is not None:
            self.__dem_graph, = axes.plot(self.dem[:,0], self.dem[:,1], color='grey', linewidth=.5)


        if self.topo is not None:
            hp = hydraulic_profile(self.topo, axes.get_ylim(), None if self.riverbed is None else (self.riverbed[0,0], self.riverbed[-1,0]))
            if hp is not None:
                mx = max(hp[:,0]) if len(hp) else 1
                axes_r.set_xlim([0, mx])
                self.__r_topo_graph, = axes_r.plot(hp[:,0], hp[:,1], color='black', linewidth=.5)

        hp = hydraulic_profile(self.simplified, axes.get_ylim(), None if self.riverbed is None else (self.riverbed[0,0], self.riverbed[-1,0]))
        if hp is not None:
            mx = max(hp[:,0]) if len(hp) else 1
            axes_r.set_xlim([0, mx])
            self.__r_graph, = axes_r.plot(hp[:,0], hp[:,1], color='royalblue', linewidth=1)

        self.__sz_graph, = axes.plot(self.sz[:,0], self.sz[:,1], '-+', ms=1, color='red', lw=.5)


        rb = self.riverbed
        if rb is not None:
            if len(rb) == 8:
                self.__riverbed_graph = [
                    axes.plot(self.riverbed[ :2,0], self.riverbed[ :2,1], color='red', linewidth=.5, alpha=1)[0],
                    axes.plot(self.riverbed[2:4,0], self.riverbed[2:4,1], color='red', linewidth=.5, alpha=.3)[0],
                    axes.plot(self.riverbed[4:6,0], self.riverbed[4:6,1], color='green', linewidth=.5, alpha=.3)[0],
                    axes.plot(self.riverbed[6: ,0], self.riverbed[6:8,1], color='green', linewidth=.5, alpha=1)[0]
                ]
            else:
                self.__riverbed_graph = [
                    axes.plot(self.riverbed[ :2,0], self.riverbed[ :2,1], color='red', linewidth=.5, alpha=1)[0],
                    axes.plot(self.riverbed[2:4,0], self.riverbed[2:4,1], color='red', linewidth=.5, alpha=.3)[0],
                    axes.plot(self.riverbed[4:6,0], self.riverbed[4:6,1], color='blue', linewidth=.5, alpha=1)[0],
                    axes.plot(self.riverbed[6:8,0], self.riverbed[6:8,1], color='green', linewidth=.5, alpha=.3)[0],
                    axes.plot(self.riverbed[8: ,0], self.riverbed[8: ,1], color='green', linewidth=.5, alpha=1)[0]
                ]
            self.__simplified_no_offset_graph, = axes.plot(self.simplified[:,0], self.simplified[:,1] - self.dz, '-', ms=2, color='royalblue', lw=.5)

        if self.ref is not None:
            self.__ref_graph, = axes.plot(self.ref[:,0], self.ref[:,1], '+', color='black', linewidth=.5, ms=5)



        self.__simplified_graph, = axes.plot(self.simplified[:,0], self.simplified[:,1], '-o', ms=3, color='royalblue', lw=2)
        self.__simplified_graph_fill, = axes.fill(self.water[:,0], self.water[:,1], color='royalblue', alpha=.1)

        if self.left_levee is not None:
            self.__l_levee_graph, = axes.plot(self.left_levee[:,0], self.left_levee[:,1], color='royalblue', lw=4)
            self.__r_levee_graph, = axes.plot(self.right_levee[:,0], self.right_levee[:,1], color='royalblue', lw=4)
