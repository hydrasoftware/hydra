from __future__ import absolute_import
from builtins import str
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from .base_dialog import BaseDialog, tr
from hydra.gui.edit import EditTool
from hydra.gui.delete import DeleteTool
from hydra.utility.tables_properties import TablesProperties
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QTableWidgetItem, QAbstractItemView, QTextEdit, QApplication
from qgis.PyQt.QtGui import QFontMetrics
from qgis.core import QgsPointXY, QgsRectangle
import os
import re

class ValidityTool(BaseDialog):
    def __init__(self, project, iface, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "validity.ui"), self)

        self.project = project
        self.properties = TablesProperties.get_properties()
        self.iface = iface
        self.clipboard = QApplication.clipboard()

        self.buttonBox.accepted.connect(self.sav)
        self.edit_button.clicked.connect(self.edit)
        self.zoom_button.clicked.connect(self.zoom)
        self.delete_button.clicked.connect(self.delete)
        self.copy_button.clicked.connect(self.copy)

        self.model.addItems(self.project.get_models())
        if self.project.get_current_model() is not None :
            self.model.setCurrentIndex(self.project.get_models().index(self.project.get_current_model().name))
        else :
            self.model.setCurrentIndex(-1)
        self.model.activated.connect(self.model_changed)
        self.refresh()

    def refresh(self):
        # table entry : [table, id]
        self.data_table=[]
        self.validity_table.setRowCount(0)
        self.validity_table.setHorizontalHeaderLabels(['Type','Id','Name','Invalidity reason (rules broken)'])

        invalids = self.project.execute("""select subtype::varchar||'_'||type::varchar, id, name, reason from {}.invalid""".format(self.model.currentText())).fetchall()

        for item in invalids:
            if item[0]=='_profile':
                item_table = 'river_cross_section_profile'
                item_reason = '   check cross section attributes carefully   '
            else:
                item_table = item[0]
                item_reason = item[3].split('*/')[0].split('/*')[-1]

            self.data_table.append([item_table, item[1].split(':')[1]])
            row_position = self.validity_table.rowCount()
            self.validity_table.insertRow(row_position)
            self.validity_table.setItem(row_position, 0, QTableWidgetItem(self.properties[item_table]['name']))
            self.validity_table.setItem(row_position, 1, QTableWidgetItem(item[1].split(':')[1]))
            self.validity_table.setItem(row_position, 2, QTableWidgetItem(item[2]))
            box = QTextEdit()
            lines = item_reason[3:-3].split('      ')
            box.setPlainText('- '+('\n- ').join(lines))
            box.setReadOnly(True)
            self.validity_table.setCellWidget(row_position, 3, box)
            m = QFontMetrics(box.font())
            self.validity_table.setRowHeight(row_position, int(m.lineSpacing()*len(lines)*1.5))

        #self.validity_table.resizeColumnsToContents()
        self.number_invalid.setText(str(len(invalids)))
        self.details.setText(self.detail_string(self.data_table))

    def model_changed(self):
        if self.model.currentIndex() != -1 :
            self.project.set_current_model(self.model.currentText())
        else :
            self.project.log.warning(tr("No active model"))
            self.project.set_current_model(None)
        self.refresh()

    def copy(self):
        self.clipboard.clear()
        rows = []
        columns = []
        data_to_export = ''
        for row in range(self.validity_table.rowCount()):
            for column in range(self.validity_table.columnCount()):
                _item = self.validity_table.item(row, column)
                if _item:
                    txt = self.validity_table.item(row, column).text() +'\t'
                data_to_export += txt
            data_to_export += '\n'
        self.clipboard.setText(data_to_export, mode=self.clipboard.Clipboard)

    def detail_string(self, table):
        n, s, l, o = 0, 0 ,0, 0
        for object in table:
            type= object[0].split('_')[-1]
            if type == 'node':
                n += 1
            elif type == 'singularity':
                s += 1
            elif type == 'link':
                l += 1
        o = len(table) - n - s - l
        if o == 0 :
            string = "({} nodes, {} singularities and {} links)".format(str(n), str(s), str(l))
        elif o > 0:
            string = "({} nodes, {} singularities, {} links and {} others)".format(str(n), str(s), str(l), str(o))
        return string

    def edit(self):
        if self.validity_table.currentItem() is None:
            return
        EditTool.edit(self.project,
                        self.data_table[self.validity_table.currentRow()][0],
                        self.data_table[self.validity_table.currentRow()][1])
        self.refresh()
        return

    def delete(self):
        if self.validity_table.currentItem() is None:
            return
        DeleteTool.delete(self, self.project, self.model.currentText(),
                            self.data_table[self.validity_table.currentRow()][0],
                            'id', self.data_table[self.validity_table.currentRow()][1])
        self.refresh()
        return

    def zoom(self):
        if self.validity_table.currentItem() is not None:
            type, geom = self.project.execute("""select ST_GeometryType(ST_Envelope(geom)), ST_AsText(ST_Envelope(geom))  from {s}.{t} where id={i}
                                            """.format(s=self.project.get_current_model().name,
                                                       t=self.data_table[self.validity_table.currentRow()][0],
                                                       i=self.data_table[self.validity_table.currentRow()][1])).fetchone()
            self.__zoom_canvas(type, geom)

    def __zoom_canvas(self, type, geom):
        if type == 'ST_Point':
            # geom = 'POINT(X Y)'
            x, y = geom[6:-1].split(' ')
            point1 = QgsPointXY(float(x)-10, float(y)-10)
            point2 = QgsPointXY(float(x)+10, float(y)+10)
        elif type == 'ST_Polygon':
            # geom = 'POLYGON((X1 Y1,X2 Y2,X3 Y3,X4 Y4))'
            x1, y1 = geom[9:-2].split(',')[0].split(' ')
            x2, y2 = geom[9:-2].split(',')[2].split(' ')
            point1 = QgsPointXY(float(x1), float(y1))
            point2 = QgsPointXY(float(x2), float(y2))
        if point1:
            extent = QgsRectangle(point1, point2)
            self.iface.mapCanvas().setExtent(extent)
            self.iface.mapCanvas().refresh()
            self.close()
        else:
            return

    def sav(self):
        self.save()
        self.close()