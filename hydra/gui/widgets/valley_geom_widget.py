################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import sys, traceback
import time
import numpy
from math import sqrt
from shapely import wkt
from shapely.geometry import LineString, Point
from qgis.PyQt import uic
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QApplication, QWidget, QMessageBox, QTableWidgetItem, QGridLayout, QAbstractItemView
from hydra.gui.base_dialog import tr
from hydra.gui.widgets.array_widget import ArrayWidget
from hydra.gui.widgets.graph_widget import GraphWidget
from hydra.gui.widgets.combo_with_values import ComboWithValues
import hydra.utility.string as string

dico_code_error = {11 : tr("Riverbed Z not monotonously increasing"),
                   12 : tr("Riverbed width not monotonously increasing"),
                   21 : tr("Left flood plain Z not monotonously increasing"),
                   22 : tr("Left flood plain width not monotonously increasing"),
                   31 : tr("Right flood plain Z not monotonously increasing"),
                   32 : tr("Right flood plain width not monotonously increasing"),
                   41 : tr("Left flood plain start beneath riverbed end"),
                   42 : tr("Right flood plain start beneath riverbed end")}

class ValleyGeomWidget(QWidget):
    def __init__(self, project, parent=None, valley_geom_table=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "valley_geom_widget.ui"), self)
        self.project = project
        self.model = self.project.get_current_model()

        self.__loading = True
        self.transect = None
        self.__topo_profile = None
        self.__points_profile = None
        self.s_left_bank = None
        self.s_right_bank = None
        self.offset_deca = 0
        self.transposed_left_bank=None
        self.transposed_right_bank=None
        self._ref_transposed_left_bank = None
        self._ref_transposed_right_bank = None
        self.inter_reach=None
        self.inter_reach_position=None
        self.value_graph = None

        # handle trigger coverage
        self.init_trigger_status = self.project.execute("""select trigger_coverage from {model}.metadata""".format(model= self.project.get_current_model().name)).fetchone()[0]
        self.project.execute("""update {model}.metadata set trigger_coverage = False""".format(model= self.project.get_current_model().name))

        self.vcs_geom_name.textChanged.connect(self.vcs_geom_update_name)

        self.graph_vcs_geom = GraphWidget(self.graph_vcs_geom_placeholder, False, True)
        self.valley_geom_table = valley_geom_table

        self.vcs_geom_curvarray = ArrayWidget(["X (m)", "Z (m)"], [20,2], None, hide_start=True)
        self.vcs_geom_curvarray.add.hide()
        self.vcs_geom_curvarray.delete.hide()
        self.vcs_geom_curvarray.table.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tab_array_layout.addWidget(self.vcs_geom_curvarray)

        self.vcs_geom_xyarray_maj_lbank = ArrayWidget(["Z (m)", "Width (m)"], [4,2], None, draw_graph_func=self.__refresh, hide_start=True)#, hide_headers=True)
        self.vcs_geom_xyarray_min = ArrayWidget(["Z (m)", "Width (m)"], [6,2], None, draw_graph_func=self.__refresh, hide_start=True)#, hide_headers=True)
        self.vcs_geom_xyarray_maj_rbank = ArrayWidget(["Z (m)", "Width (m)"], [4,2], None, draw_graph_func=self.__refresh, hide_start=True)#, hide_headers=True)
        self.left_bank_layout.addWidget(self.vcs_geom_xyarray_maj_lbank)
        self.river_bed_layout.addWidget(self.vcs_geom_xyarray_min)
        self.right_bank_layout.addWidget(self.vcs_geom_xyarray_maj_rbank)

        self.vcs_geom_zlevee_lb.textChanged.connect(self.__refresh)
        self.vcs_geom_zlevee_rb.textChanged.connect(self.__refresh)
        self.set_enabled(False)
        self.btn_generate_geom.clicked.connect(self.__interpolate_from_linestring)
        self.graph_vcs_geom.set_zoom_enabled(True)
        self.graph_vcs_geom.canvas.mpl_connect('button_press_event', self.__on_press)
        self.graph_vcs_geom.canvas.mpl_connect('motion_notify_event', self.__on_move)

        self.chk_hide_dem.setChecked(not self.project.terrain.has_data())
        self.btn_rescale_graph.clicked.connect(self.rescale)

        self.chk_points_ignore.stateChanged.connect(self.__refresh)
        self.chk_hide_dem.stateChanged.connect(self.__refresh)
        self.chk_axe.stateChanged.connect(self.__refresh)


        self.constrain_combo = ComboWithValues(self.constrain_combo_placeholder)
        self.__init_combo_constrain()
        self.constrain_combo.activated.connect(self.__constrain_changed)
        self.__move=None

        self.__loading = False

        self.tmplayout = QGridLayout()
        self.tmplayout.addWidget(self,0,0)

    def __refresh(self, datas = None):
        if not self.__loading :
            x_min, x_max, y_min, y_max = self.graph_vcs_geom.get_zoomed_zone()
            if self.chk_axe.isChecked():
                self.offset_deca = self.vcs_geom_xyarray_maj_lbank.get_table_items()[-1][1]
                if self._ref_transposed_left_bank == self.transposed_left_bank and self._ref_transposed_right_bank == self.transposed_right_bank :
                    if self.transposed_left_bank is not None and self.transposed_right_bank is not None:
                        self.transposed_left_bank = self.transposed_left_bank + self.offset_deca
                        self.transposed_right_bank = self.transposed_right_bank + self.offset_deca
                self.tab_array_widget.hide()
                self.vcs_geom_curvarray.show()
            else:
                self.offset_deca = 0
                self.transposed_left_bank = self._ref_transposed_left_bank
                self.transposed_right_bank = self._ref_transposed_right_bank
                self.tab_array_widget.show()
                self.vcs_geom_curvarray.hide()
            self.__draw_vcs_geom(datas)
            if not y_max > 9000 :
                self.graph_vcs_geom.set_zoomed_zone(x_min, x_max, y_min, y_max)
            self.distance.setEnabled(not self.chk_points_ignore.isChecked())

    def get_layout(self):
        return self.tmplayout

    def rescale(self) :
        self.__refresh()

        zbmin_array = self.vcs_geom_xyarray_min.get_table_items()
        zbmaj_lbank_array = self.vcs_geom_xyarray_maj_lbank.get_table_items()
        zbmaj_rbank_array = self.vcs_geom_xyarray_maj_rbank.get_table_items()

        z_value = [point[0] for point in zbmaj_lbank_array] + [point[0] for point in zbmaj_rbank_array] + [point[0] for point in zbmin_array]
        x_min_value = zbmaj_lbank_array[-1][1] + zbmin_array[-1][1]/2
        x_max_value = zbmaj_rbank_array[-1][1] + zbmin_array[-1][1]/2
        x_min,x_max,y_min,y_max = self.graph_vcs_geom.get_zoomed_zone()

        if zbmin_array != [[0.0,0.0]] or zbmaj_lbank_array != [[0.0,0.0]] or zbmaj_lbank_array != [[0.0,0.0]] :

            if not self.chk_axe.isChecked() :
                self.graph_vcs_geom.set_zoomed_zone(-x_min_value,x_max_value,min(z_value),max(z_value))
            else:
                self.graph_vcs_geom.set_zoomed_zone(0, zbmaj_rbank_array[-1][1] + zbmin_array[-1][1] + zbmaj_lbank_array[-1][1],min(z_value),max(z_value))

        elif self.__points_profile is not None :

            z_value_pt = [elem[1] + self.offset_deca for elem in self.__points_profile]
            x_value_pt = [elem[0] + self.offset_deca for elem in self.__points_profile]

            self.graph_vcs_geom.set_zoomed_zone(min(x_value_pt),max(x_value_pt),min(z_value_pt),max(z_value_pt))

        elif self.__topo_profile is not None :

            z_value_pt = [elem[1] + self.offset_deca for elem in self.__topo_profile]
            x_value_pt = [elem[0] + self.offset_deca for elem in self.__topo_profile]

            self.graph_vcs_geom.set_zoomed_zone(min(x_value_pt),max(x_value_pt),min(z_value_pt),max(z_value_pt))

        self.graph_vcs_geom.render()

    def __init_combo_constrain(self):
        if self.model is not None:
            constrains = self.project.execute("""select name, id from {model}.constrain order by name asc;""".format(
                model=self.model.name
                )).fetchall()
            self.constrain_combo.clear()
            # self.constrain_combo.addItem(tr("None"), None)
            self.constrain_combo.addItem("", None)
            for constrain in constrains:
                self.constrain_combo.addItem(constrain[0], constrain[1])
            self.constrain_combo.set_selected_value(None)
            self.constrain_combo.setEditable(True)

    def __constrain_changed(self):
        self.transect = self.constrain_combo.get_selected_value()
        if self.model is not None and self.transect is not None:
            self.__loading = True
            self.__load_profile(self.transect, self.model.name)
            self.__loading = False
        else:
            self.__topo_profile = None
            self.__points_profile = None
        self.__refresh()

    def __interpolate_from_linestring(self):
        self.transect = self.constrain_combo.get_selected_value()
        if self.model is not None and self.transect is not None:
            self.__loading = True
            self.__load_pl1d(self.transect, self.model.name,False)
            self.__load_profile(self.transect, self.model.name)
            self.__loading = False
        else:
            self.__topo_profile = None
            self.__points_profile = None
        self.__refresh()

    def __reload_from_linestring(self):
        self.transect = self.constrain_combo.get_selected_value()
        if self.model is not None and self.transect is not None:
            self.__loading = True
            self.__load_pl1d(self.transect, self.model.name, False)
            self.__load_profile(self.transect, self.model.name)
            self.__loading = False
        else:
            self.__topo_profile = None
            self.__points_profile = None
        self.__refresh()

    def __load_profile(self, transect_id, model):
        mid_center=None
        if self.vcs_geom_xyarray_maj_lbank.table.rowCount()>=4 and self.vcs_geom_xyarray_min.table.rowCount()>=6:
            item1 = self.vcs_geom_xyarray_maj_lbank.table.item(3,1)
            item2 = self.vcs_geom_xyarray_min.table.item(5,1)
            if item1 is not None and item2 is not None:
                mid_center = string.get_sql_float(item1.text())
        t_ignore_pt=self.chk_points_ignore.isChecked()
        t_distance_pt=int(self.distance.text())
        t_discret=int(self.discretisation.text())
        transect, = self.project.execute("""select geom from {model}.constrain where id={id}""".format(
                    model=model,
                    id=transect_id
                    )).fetchone()
        inter = self.project.execute("""
                with reach as (
                    select r.id, st_intersection(r.geom, '{transect}'::geometry) as inter, geom
                    from {model}.reach as r
                    where st_intersects(r.geom, '{transect}'::geometry)
                ),
                pk as (
                    select st_linelocatepoint(geom, inter) as pk from reach
                ),
                topo_transect as (
                    select project.set_altitude(
                        project.discretize_line('{transect}'::geometry, st_length('{transect}'::geometry)/{discret})
                        ) as geom
                )
                select r.id as reach, r.inter, t.geom as topo_transect from reach as r, topo_transect as t
                """.format(
                    model=model,
                    transect=transect,
                    discret=str(t_discret)
                    )).fetchall()

        if inter:
            self.inter_reach = wkt.loads((self.project.execute("""select wkb_loads('{}')""".format(inter[0][1])).fetchone())[0])
            transect_geom = wkt.loads((self.project.execute("""select wkb_loads('{}')""".format(transect)).fetchone())[0])

            if len(transect_geom.coords)==2:
                coords = transect_geom.coords
                new_coords_start, new_coords_end = self.__calc_new_start_end(coords[0],coords[1])
                geom=[new_coords_start]
                geom.extend(coords)
                geom.append(new_coords_end)
                transect_geom = LineString(geom)
                left_bank_pt = 2

            if len(transect_geom.coords)>=3:
                s_center = transect_geom.project(self.inter_reach)
                s = numpy.array([transect_geom.project(Point(p)) for p in transect_geom.coords])
                left_bank_pt = len(s[s < s_center])
                rigth_bank_pt = len(s[s > s_center])
                coords = transect_geom.coords
                if left_bank_pt==1:
                    new_coords_start, new_coords_end = self.__calc_new_start_end(coords[0],coords[1])
                    geom=[new_coords_start]
                    geom.extend(coords)
                    transect_geom = LineString(geom)
                    left_bank_pt = 2
                if rigth_bank_pt==1:
                    new_coords_start, new_coords_end = self.__calc_new_start_end(coords[left_bank_pt-1],coords[left_bank_pt])
                    geom=[]
                    geom.extend(coords)
                    geom.append(new_coords_end)
                    transect_geom = LineString(geom)

            s_center = transect_geom.project(self.inter_reach)


            s = numpy.array([transect_geom.project(Point(p)) for p in transect_geom.coords])
            self.s_left_bank = s[left_bank_pt-1]
            self.s_right_bank = s[left_bank_pt]

            if mid_center is None or mid_center==0:
                mid_center = (self.s_left_bank + self.s_right_bank)/2
            self.transposed_left_bank = self.s_left_bank - mid_center
            self.transposed_right_bank = self.s_right_bank - mid_center
            self._ref_transposed_left_bank = self.transposed_left_bank
            self._ref_transposed_right_bank = self.transposed_right_bank
            self.__topo_profile = numpy.array([(transect_geom.project(Point(p))-mid_center, p[2]) for p in wkt.loads((self.project.execute("""select wkb_loads('{}')""".format(inter[0][2])).fetchone())[0]).coords])
            if t_ignore_pt:
                self.__points_profile = None
            else:
                points_profile_geom = self.project.execute("""
                        select p.id,
                        p.z_ground as z,
                        st_linelocatepoint(cs.geom, p.geom)*st_length(cs.geom) as pk
                        from {model}.constrain as cs, project.points_xyz as p
                        where cs.id={id}
                        and (st_intersects(ST_Buffer(cs.geom, {distance}, 'endcap=flat join=round'),p.geom )=true
                        or st_startpoint(cs.geom)=p.geom
                        or st_endpoint(cs.geom)=p.geom)
                        order by pk""".format(
                            model=model,
                            id=transect_id,
                            distance=str(t_distance_pt)
                            )).fetchall()
                if len(points_profile_geom)>1:
                    self.inter_reach_position = s_center-mid_center
                    self.__points_profile = numpy.array([(p[2]-mid_center, p[1]) for p in points_profile_geom])
                else:
                    self.inter_reach_position = s_center-mid_center
                    self.__points_profile = None
        else:
            self.__topo_profile = None
            self.__points_profile = None
            self.inter_reach_position = None

    def __load_pl1d(self, transect_id, model, interpolate):
        transect, = self.project.execute("""select geom from {model}.constrain where id={id}""".format(
                    model=model,
                    id=transect_id
                    )).fetchone()
        try:
            t_ignore_pt = self.chk_points_ignore.isChecked()
            t_distance_pt=int(self.distance.text())
            t_discret=int(self.discretisation.text())
            pl1d, = self.project.execute("""select {model}.profile_from_transect('{geometry}', {interpole}, {discret}, {ignore_pt}, {distance})""".format(model=model, geometry=transect, interpole=interpolate, discret=str(t_discret), ignore_pt=t_ignore_pt, distance=str(t_distance_pt))).fetchone()
            if pl1d is None:
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Information)
                msg.setWindowTitle(tr("Geometry generation failed"))
                msg.setText(tr("No profile found, check that your constrain line crosses a reach."))
                msg.setStandardButtons(QMessageBox.Ok)
                msg.exec_()
                return

            offset = pl1d[9][1]/2

            self.vcs_geom_xyarray_maj_lbank.table.setRowCount(0)
            self.vcs_geom_xyarray_min.table.setRowCount(0)
            self.vcs_geom_xyarray_maj_rbank.table.setRowCount(0)

            self.vcs_geom_xyarray_maj_lbank.table.setRowCount(4)
            self.vcs_geom_xyarray_min.table.setRowCount(6)
            self.vcs_geom_xyarray_maj_rbank.table.setRowCount(4)

            for row in range(0, 4):
                self.vcs_geom_xyarray_maj_lbank.table.setItem(row,0,QTableWidgetItem(string.get_str(pl1d[row][0])))
                self.vcs_geom_xyarray_maj_lbank.table.setItem(row,1,QTableWidgetItem(string.get_str(pl1d[row][1] + offset)))
            for row in range(4, 10):
                self.vcs_geom_xyarray_min.table.setItem(row-4,0,QTableWidgetItem(string.get_str(pl1d[row][0])))
                self.vcs_geom_xyarray_min.table.setItem(row-4,1,QTableWidgetItem(string.get_str(pl1d[row][1])))
            for row in range(10, 14):
                self.vcs_geom_xyarray_maj_rbank.table.setItem(row-10,0,QTableWidgetItem(string.get_str(pl1d[row][0])))
                self.vcs_geom_xyarray_maj_rbank.table.setItem(row-10,1,QTableWidgetItem(string.get_str(pl1d[row][1] + offset)))

            if float(self.vcs_geom_xyarray_maj_lbank.table.item(0,1).text()) != 0 :
                self.vcs_geom_xyarray_maj_lbank.setColortoRow(0,'yellow')
            if float(self.vcs_geom_xyarray_maj_rbank.table.item(0,1).text()) != 0 :
                self.vcs_geom_xyarray_maj_rbank.setColortoRow(0,'yellow')

        except:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText(tr("Geometry generation failed, click details below to show more information."))
            msg.setWindowTitle(tr("Geometry generation failed"))
            msg.setDetailedText(''.join(traceback.format_exception(*sys.exc_info())[-2:]).strip().replace('\n',': '))
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()

    def __calc_new_start_end(self, coordstart, coordend):
        inc_x = (coordend[0] - coordstart[0])/(coordend[1]-coordstart[1])
        inc_y = (coordend[1]-coordstart[1])/(coordend[0] - coordstart[0])
        dist = sqrt((coordend[0] - coordstart[0])**2 + (coordend[1]-coordstart[1])**2)
        inc_x = (coordend[0] - coordstart[0])/dist
        inc_y = (coordend[1]-coordstart[1])/dist

        new_coords_start = (coordstart[0]-inc_x, coordstart[1]-inc_y, coordstart[2])
        new_coords_end = (coordend[0]+inc_x, coordend[1]+inc_y, coordend[2])
        return new_coords_start, new_coords_end

    def set_items(self, id_valley_geom):
        if id_valley_geom is not None:
            self.__loading=True
            name, rlambda, rmu1, rmu2, zbmin_array, zlevee_lb, zlevee_rb, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, self.transect, t_ignore_pt, t_discret, t_distance_pt = self.project.execute("""
                                                                                            select name, rlambda, rmu1, rmu2, zbmin_array, zlevee_lb, zlevee_rb,
                                                                                            zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, transect, t_ignore_pt, t_discret, t_distance_pt
                                                                                            from {}.valley_cross_section_geometry where id={}""".format(self.model.name, id_valley_geom)).fetchone()

            self.vcs_geom_name.setText(name)
            self.vcs_geom_rlambda.setText(string.get_str(rlambda))
            self.vcs_geom_rmu1.setText(string.get_str(rmu1))
            self.vcs_geom_rmu2.setText(string.get_str(rmu2))
            self.vcs_geom_zlevee_lb.setText(string.get_str(zlevee_lb))
            self.vcs_geom_zlevee_rb.setText(string.get_str(zlevee_rb))

            offset = round(zbmin_array[-1][1]/2, 3)

            zbmaj_lbank_array = [[elem[0], round(elem[1] + offset, 3)] for elem in zbmaj_lbank_array]
            zbmaj_rbank_array = [[elem[0], round(elem[1] + offset, 3)] for elem in zbmaj_rbank_array]

            self.vcs_geom_xyarray_min.set_table_items(zbmin_array)
            self.vcs_geom_xyarray_maj_lbank.set_table_items(zbmaj_lbank_array)
            self.vcs_geom_xyarray_maj_rbank.set_table_items(zbmaj_rbank_array)

            self.chk_points_ignore.setChecked(t_ignore_pt)
            self.discretisation.setText(string.get_str(t_discret))
            self.distance.setText(string.get_str(t_distance_pt))
            if self.model is not None and self.transect is not None:
                self.constrain_combo.set_selected_value(self.transect)
                self.__load_profile(self.transect, self.model.name)
            else:
                self.constrain_combo.set_selected_value(None)
                self.__topo_profile = None
                self.__points_profile = None

            self.__loading=False
            self.rescale()
            self.set_enabled(True)

    def set_enabled(self, bool=True):
        self.vcs_geom_name.setEnabled(bool)
        self.vcs_geom_xyarray_min.setEnabled(bool)
        self.vcs_geom_xyarray_maj_lbank.setEnabled(bool)
        self.vcs_geom_xyarray_maj_rbank.setEnabled(bool)
        self.vcs_geom_rlambda.setEnabled(bool)
        self.vcs_geom_rmu2.setEnabled(bool)
        self.vcs_geom_rmu1.setEnabled(bool)
        self.vcs_geom_zlevee_lb.setEnabled(bool)
        self.vcs_geom_zlevee_rb.setEnabled(bool)
        self.graph_vcs_geom.canvas.setVisible(bool)
        self.btn_help.setEnabled(False)

    def vcs_geom_update_name(self):
        if self.__loading:
            return
        if self.valley_geom_table:
            self.valley_geom_table.table.item(self.valley_geom_table.table.row(self.valley_geom_table.table.selectedItems()[0]),1).setText(self.vcs_geom_name.text())

    def check_geom_validity(self):
        # trick to close and save the editor
        self.vcs_geom_xyarray_min.table.setDisabled(True)
        self.vcs_geom_xyarray_min.table.setDisabled(False)
        self.vcs_geom_xyarray_maj_lbank.setDisabled(True)
        self.vcs_geom_xyarray_maj_lbank.setDisabled(False)
        self.vcs_geom_xyarray_maj_rbank.setDisabled(True)
        self.vcs_geom_xyarray_maj_rbank.setDisabled(False)

        zbmin_array = self.vcs_geom_xyarray_min.get_table_items()
        zbmaj_lbank_array = self.vcs_geom_xyarray_maj_lbank.get_table_items()
        zbmaj_rbank_array = self.vcs_geom_xyarray_maj_rbank.get_table_items()

        if [len(zbmin_array),len(zbmaj_lbank_array),len(zbmaj_rbank_array)] == [0,0,0] :
            return 0
        elif any(zbmin_array[i][0]>zbmin_array[i+1][0] for i in range(0,len(zbmin_array)-1)) :
            return 11
        elif any(zbmin_array[i][1]>zbmin_array[i+1][1] for i in range(0,len(zbmin_array)-1)) :
            return 12
        elif any(zbmaj_lbank_array[i][0]>zbmaj_lbank_array[i+1][0] for i in range(0,len(zbmaj_lbank_array)-1)) :
            return 21
        elif any(zbmaj_lbank_array[i][1]>zbmaj_lbank_array[i+1][1] for i in range(0,len(zbmaj_lbank_array)-1)) :
            return 22
        elif any(zbmaj_rbank_array[i][0]>zbmaj_rbank_array[i+1][0] for i in range(0,len(zbmaj_rbank_array)-1)) :
            return 31
        elif any(zbmaj_rbank_array[i][1]>zbmaj_rbank_array[i+1][1] for i in range(0,len(zbmaj_rbank_array)-1)) :
            return 32
        elif len(zbmaj_lbank_array) != 0 and zbmaj_lbank_array[0][0] < zbmin_array[-1][0] :
            return 41
        elif len(zbmaj_rbank_array) != 0 and zbmaj_rbank_array[0][0] < zbmin_array[-1][0] :
            return 42
        return 0

    def save_geom(self, id_valley_geom):
        if id_valley_geom:
            name = self.vcs_geom_name.text()
            rlambda = string.get_sql_float(self.vcs_geom_rlambda.text())
            rmu1 = string.get_sql_float(self.vcs_geom_rmu1.text())
            rmu2 = string.get_sql_float(self.vcs_geom_rmu2.text())
            zlevee_lb = string.get_sql_float(self.vcs_geom_zlevee_lb.text())
            zlevee_rb = string.get_sql_float(self.vcs_geom_zlevee_rb.text())
            transect = self.constrain_combo.get_selected_value() if self.constrain_combo.get_selected_value() is not None else 'null'

            zbmin_array = self.vcs_geom_xyarray_min.get_table_items()
            offset = round(zbmin_array[-1][1]/2, 3)
            zbmin_array = string.list_to_sql_array(zbmin_array)

            zbmaj_lbank_array = string.list_to_sql_array([[elem[0], round(elem[1] - offset, 3)] for elem in self.vcs_geom_xyarray_maj_lbank.get_table_items()])
            zbmaj_rbank_array = string.list_to_sql_array([[elem[0], round(elem[1] - offset, 3)] for elem in self.vcs_geom_xyarray_maj_rbank.get_table_items()])

            t_ignore_pt=self.chk_points_ignore.isChecked()
            t_distance_pt = string.get_sql_float(self.distance.text())
            t_discret = string.get_sql_float(self.discretisation.text())

            self.model.update_valley_section_geometry(id_valley_geom, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, rlambda, rmu1, rmu2, zlevee_lb, zlevee_rb, name, transect, t_discret, t_ignore_pt, t_distance_pt)

    def __draw_construction_lines(self,d0_lb,d0_rb,left_bank,right_bank,zbmin_array):

        self.graph_vcs_geom.add_vline(self.offset_deca, color="#fc9b1d",  linestyle =":")

        if not self.chk_points_ignore.isChecked() and self.__points_profile is not None :
            reach_y_interp = numpy.interp(self.inter_reach_position,[ r[0] for r in self.__points_profile],[ r[1] for r in self.__points_profile])
            self.graph_vcs_geom.add_line(self.inter_reach_position + self.offset_deca,reach_y_interp,'blue',marker='*')
        elif self.__topo_profile is not None and self.inter_reach_position is not None :
            reach_y_interp = numpy.interp(self.inter_reach_position,[ r[0] for r in self.__topo_profile],[ r[1] for r in self.__topo_profile])
            self.graph_vcs_geom.add_line(self.inter_reach_position + self.offset_deca,reach_y_interp,'blue',marker='*')

        riverbed_limit_x = [left_bank[0][1] + self.offset_deca,right_bank[0][1] + self.offset_deca]
        riverbed_limit_y = [left_bank[0][0],right_bank[0][0]]
        riverbed_points_x =  [-r[1]/2 + self.offset_deca for r in zbmin_array[::-1]] + [r[1]/2 + self.offset_deca for r in zbmin_array]
        riverbed_points_y =  [r[0] for r in zbmin_array[::-1]] + [r[0] for r in zbmin_array]

        self.graph_vcs_geom.add_fill_between_lines(riverbed_limit_x,riverbed_points_x,
                riverbed_limit_y,riverbed_points_y,
                "#b3e6ff")

        for point in left_bank :
            self.graph_vcs_geom.add_line([self.offset_deca,point[1]],
                [point[0],point[0]],
                color="#fc9b1d" ,linestyle =":")

        for point in right_bank :
            self.graph_vcs_geom.add_line([self.offset_deca,point[1]],
                [point[0],point[0]],
                color="#fc9b1d",linestyle =":")

    def __draw_vcs_geom(self, datas):
        # datas is not used, we get all datas from the 3 array widget
        if self.__loading:
            return

        title = self.vcs_geom_name.text()
        zlevee_lb = string.get_sql_float(self.vcs_geom_zlevee_lb.text())
        zlevee_rb = string.get_sql_float(self.vcs_geom_zlevee_rb.text())
        zbmin_array = self.vcs_geom_xyarray_min.get_table_items()
        zbmaj_lbank_array = self.vcs_geom_xyarray_maj_lbank.get_table_items()
        zbmaj_rbank_array = self.vcs_geom_xyarray_maj_rbank.get_table_items()

        offset = zbmin_array[-1][1]/2

        if len(zbmin_array)>0 :
            self.graph_vcs_geom.clear()
            if self.__topo_profile is not None and not self.chk_hide_dem.isChecked():
                self.graph_vcs_geom.add_line(
                    [r[0] for r in self.__topo_profile - self.inter_reach_position + self.offset_deca],
                    [r[1] for r in self.__topo_profile],
                    '#c3c3c3',
                    title,
                    tr("Width (m)"), tr("Z (m)"))

            if self.__points_profile is not None and not self.chk_points_ignore.isChecked():
                self.graph_vcs_geom.add_line(
                    [r[0] for r in self.__points_profile - self.inter_reach_position + self.offset_deca],
                    [r[1] for r in self.__points_profile],
                    '#e60000',
                    title,
                    tr("Width (m)"), tr("Z (m)"))
            if not (len(zbmin_array)==1 and zbmin_array[0][0]==0 \
                and len(zbmaj_lbank_array)==1 and zbmaj_lbank_array[0][0]==0 \
                and len(zbmaj_rbank_array)==1 and zbmaj_rbank_array[0][0]==0):

                zbmin_array.insert(0, [zbmin_array[0][0], 0])
                ymin,ymax = self.graph_vcs_geom.get_zoomed_zone()[2:]

                self.graph_vcs_geom.add_symetric_line(
                    [r[1]/2 for r in zbmin_array],
                    [r[0] for r in zbmin_array],
                    "#0066ff",
                    title,
                    tr("Width (m)"), tr("Z (m)"), marker=".", center=self.offset_deca)


                left_bank = [[zbmin_array[-1][0], -offset + self.offset_deca]]

                for p in zbmaj_lbank_array:
                    left_bank.append([p[0], -p[1] + self.offset_deca])

                self.graph_vcs_geom.add_line(
                    [r[1] for r in left_bank],
                    [r[0] for r in left_bank],
                    "#0066ff",
                    title,
                    tr("Width (m)"), tr("Z (m)"), marker=".")

                if zlevee_lb> zbmin_array[-1][0]:
                    self.graph_vcs_geom.add_line(
                        [-zbmaj_lbank_array[0][1] + self.offset_deca, -zbmaj_lbank_array[0][1] + self.offset_deca],
                        [zbmaj_lbank_array[0][0], zlevee_lb],
                        "r",
                        title,
                        tr("Width (m)"), tr("Z (m)"))

                right_bank = [[zbmin_array[-1][0], offset + self.offset_deca]]

                for p in zbmaj_rbank_array:
                    right_bank.append([p[0], p[1] + self.offset_deca])

                self.graph_vcs_geom.add_line(
                    [r[1] for r in right_bank],
                    [r[0] for r in right_bank],
                    "#0066ff",
                    title,
                    tr("Width (m)"), tr("Z (m)"), marker=".")

                if zlevee_rb> zbmin_array[-1][0]:
                    self.graph_vcs_geom.add_line(
                        [zbmaj_rbank_array[0][1] + self.offset_deca, zbmaj_rbank_array[0][1] + self.offset_deca],
                        [zbmaj_rbank_array[0][0], zlevee_rb],
                        "r",
                        title,
                        tr("Width (m)"), tr("Z (m)"))


                x_bank = [r[1] for r in left_bank[::-1]] + [self.offset_deca - r[1]/2 for r in zbmin_array[::-1][1:-1]] + [self.offset_deca + r[1]/2 for r in zbmin_array[1:-1]] + [r[1] for r in right_bank]
                z_bank = [r[0] for r in left_bank[::-1]] + [r[0] for r in zbmin_array[::-1][1:-1]] + [r[0] for r in zbmin_array[1:-1]] + [r[0] for r in right_bank]
                assert len(x_bank) == len(z_bank)
                self.value_graph = []
                for i in range(len(z_bank)):
                    self.value_graph.append([round(x_bank[i],2),z_bank[i]])

                self.vcs_geom_curvarray.table.setRowCount(0)
                self.vcs_geom_curvarray.table.setRowCount(len(self.value_graph))
                for row in range(len(self.value_graph)):
                    self.vcs_geom_curvarray.table.setItem(row,0,QTableWidgetItem(string.get_str(self.value_graph[row][0])))
                    self.vcs_geom_curvarray.table.setItem(row,1,QTableWidgetItem(string.get_str(self.value_graph[row][1])))

                self.__draw_construction_lines(-offset, offset, left_bank, right_bank ,zbmin_array)


            if self.__topo_profile is not None:
                ymin,ymax = self.graph_vcs_geom.get_zoomed_zone()[2:]
                if self.s_left_bank is not None:
                    self.graph_vcs_geom.add_vline(self.transposed_left_bank,color="g" if self.__move!="left" else "y")
                if self.s_right_bank is not None:
                    self.graph_vcs_geom.add_vline(self.transposed_right_bank, color="g" if self.__move!="right" else "y")
        else:
            self.graph_vcs_geom.canvas.setVisible(False)

    def __on_press(self, event):
        "initialise data for moving banks"
        if event.button == 1:
            x_min, x_max, y_min, y_max = self.graph_vcs_geom.get_zoomed_zone()
            if self.__move == None:
                if self.transposed_left_bank is not None and abs((self.transposed_left_bank-event.xdata)/self.transposed_left_bank)<0.1:
                    self.__move="left"
                    self.__start_x=self.transposed_left_bank
                    self.__refresh()
                elif self.transposed_right_bank is not None and abs((self.transposed_right_bank-event.xdata)/self.transposed_right_bank)<0.1:
                    self.__move="right"
                    self.__start_x=self.transposed_right_bank
                    self.__refresh()
            else:
                if abs((self.transposed_left_bank-event.xdata)/self.transposed_left_bank)<0.1 or abs((self.transposed_right_bank-event.xdata)/self.transposed_right_bank)<0.1:
                    self.__move = None
                    self.__refresh()
                else:
                    self.__calc_transect(event)
                    self.__move = None
                    self.__constrain_changed()
            self.graph_vcs_geom.set_zoomed_zone(x_min, x_max, y_min, y_max)

    def __on_move(self, event):
        if self.transposed_left_bank is None or self.transposed_right_bank is None or event.xdata is None:
            QApplication.restoreOverrideCursor()
        elif abs((self.transposed_left_bank-event.xdata)/self.transposed_left_bank)<0.1 or abs((self.transposed_right_bank-event.xdata)/self.transposed_right_bank)<0.1:
            QApplication.setOverrideCursor(Qt.PointingHandCursor)
        else:
            QApplication.restoreOverrideCursor()

    def __calc_transect(self, event):
        if event.button == 1 and self.__move is not None and self.transect is not None:
            def get_point(geom, pk):
                for i in range(0, len(geom.coords)):
                    if geom.project(Point(geom.coords[i]))==pk:
                        return i, geom.coords[i]
                return None, None

            def get_extrapoled_point(p1, p2, pk):
                'Creates a line extrapoled in p1->p2 direction'
                l = LineString([p1,p2]).length
                ratio = (l+pk)/l
                a = p1
                b = (p1[0]+ratio*(p2[0]-p1[0]), p1[1]+ratio*(p2[1]-p1[1]))
                return b

            delta = event.xdata - self.__start_x

            transect, = self.project.execute("""select geom from {model}.constrain where id={id}""".format(
                model=self.model.name,
                id=self.transect
                )).fetchone()

            transect_geom = wkt.loads((self.project.execute("""select wkb_loads('{}')""".format(transect)).fetchone())[0])
            s_center = transect_geom.project(self.inter_reach)
            s = numpy.array([transect_geom.project(Point(p)) for p in transect_geom.coords])
            s_left_bank, s_right_bank = numpy.sort(s[numpy.argsort(numpy.abs(s - s_center))[:2]])

            index_l, point_l=get_point(transect_geom, s_left_bank)
            index_r, point_r=get_point(transect_geom, s_right_bank)

            if point_l==None or point_r==None:
                return

            index=None
            z_coord=None
            y_coord=None
            x_coord=None

            if self.__move=="left":
                index=index_l
                pt = get_extrapoled_point(point_r, point_l, -delta)
                x_coord=pt[0]
                y_coord=pt[1]
                z_coord=point_l[2]
            else:
                index=index_r
                pt = get_extrapoled_point(point_l, point_r, delta)
                x_coord=pt[0]
                y_coord=pt[1]
                z_coord=point_l[2]

            if index is not None:
                self.project.execute(""" with geometry as (
                                select geom from {model}.constrain where id={id}
                            )
                            update {model}.constrain
                            set geom=st_setpoint(geom, {index}, st_geomfromewkt('POINT({x} {y} {z})'))
                            where id={id};""".format(model=self.model.name, id=self.transect, index=index, x=x_coord, y=y_coord, z=z_coord))