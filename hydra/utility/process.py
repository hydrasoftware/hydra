# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import time
import os
import sys
import signal
import subprocess
from subprocess import Popen, PIPE, STDOUT
if os.name == 'nt':
    from subprocess import STARTUPINFO, STARTF_USESHOWWINDOW
from hydra.utility.log import LogManager, SilentLogger
from qgis.PyQt.QtCore import QObject, pyqtSignal


# use system default encoding for log files
ENCODING = sys.getdefaultencoding()


class HydraExecError(Exception):
    pass


class ExecRunner(QObject):
    log = pyqtSignal(str)
    output = pyqtSignal(str)

    @staticmethod
    def get_error(fname):
        if not os.path.isfile(fname):
            return -1
        else:
            err = 0
            with open(fname) as f:
                content = f.readlines()
                err = int(content[0])
            return err

    def __init__(self, exe, log=None):
        '''creates an object to start execution of a given file for a given exec'''
        QObject.__init__(self)
        self.executable = exe
        self.logger = log

    def run_cmd_outfile(self, arguments, file, directory=None, ignore_error=False, hide=True):
        '''Runs a command and read exit_code and error message in associated files'''

        if self.logger is None:
            self.logger = LogManager(SilentLogger(), "Hydra")

        if os.path.isfile(file + '.log'):
            os.remove(file + '.log')
        if os.path.isfile(file + '.err'):
            os.remove(file + '.err')
        if os.name == 'nt':
            si = STARTUPINFO()
            if hide:
                si.dwFlags |= STARTF_USESHOWWINDOW
            else: # default
                si.wShowWindow = subprocess.SW_HIDE
        else : # linux
            si = None

        try:
            self.__process = Popen(self.executable + arguments, stdin=PIPE, stdout=PIPE, stderr=STDOUT, encoding=ENCODING, cwd=directory, startupinfo=si)

            # Emitting STDOUT of execution through "output" signal
            self.output.emit("{} output:".format(os.path.basename(arguments[0])))
            for stdout_line in iter(self.__process.stdout.readline, ""):
                self.output.emit(stdout_line)
            self.__process.wait()
        except:
            # make sure the subprocess does not hang when an exceprion is raised
            os.kill(self.__process.pid, signal.CTRL_C_EVENT)
            raise

        if file:
            if os.path.isfile(file + '.log'):
                with open(file + '.log', encoding=ENCODING) as f:
                    msg = f.readlines()
                if not ignore_error:
                    with open(file + '.err', encoding=ENCODING) as f:
                        exit_code = int(f.readlines()[0])
                    if exit_code != 0:
                        self.logger.error("Command: {}\nError message:\n{}".format(' '.join(self.executable + arguments), '    '.join(msg)))
                        raise HydraExecError('Error running command {}'.format(' '.join(self.executable + arguments)))
        self.logger.notice("Command successful: {}".format(' '.join(self.executable + arguments)))


def run_cmd(arguments, log=None, environment=None, directory=None, hide=True):
    '''Runs a command and logs outputs'''
    if log is None:
        log=LogManager(SilentLogger(), "Hydra")

    if os.name == 'nt':
        si = STARTUPINFO()
        if hide:
            si.dwFlags |= STARTF_USESHOWWINDOW
        else: # default
            si.wShowWindow = subprocess.SW_HIDE
    else:
        si = None

    process = Popen(arguments, stdin=PIPE, stdout=PIPE, stderr=PIPE, cwd=directory, env=environment, startupinfo=si)
    out, err = process.communicate()
    exit_code = process.returncode

    if exit_code != 0:
        log.error(f"Command: {' '.join(arguments)}\nError message:\n{err.decode()}")
        raise HydraExecError(f"Error running command {' '.join(arguments)}: {err.decode()}")
    log.notice(f"Command successful: {' '.join(arguments)}")

def wait_timeout(proc, seconds):
    '''Wait for a process to finish, or raise exception after timeout'''
    start = time.time()
    end = start + seconds
    interval = min(seconds / 1000.0, .25)

    while True:
        result = proc.poll()
        if result is not None:
            return result
        if time.time() >= end:
            proc.kill()
            raise RuntimeError("Process timed out")
        time.sleep(interval)
