# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from qgis.PyQt.QtWidgets import QDockWidget, QTreeWidget, QTreeWidgetItem, QScrollArea, QMenu
from qgis.PyQt.QtGui import QFont
from qgis.PyQt.QtCore import Qt
from .base_dialog import tr

import os.path

_plugin_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
_layer_icons_dir = os.path.join(_plugin_dir, "ressources", "layer_icons")
_icons_dir = os.path.join(_plugin_dir, "ressources", "images")
bold = QFont()
bold.setWeight(QFont.Bold)

rains = [tr('Montanas'), tr('Caquot'), tr('Single triangular'),
        tr('Double triangular'), tr('Hyetographs'), tr('Gaged rainfalls'),
        tr('Gaged rainfalls (file)'), tr('Radar rainfalls'), tr('MAGES rainfalls')]

class ModelMenu(QMenu):
    def __init__(self, project, model_name):
        QMenu.__init__(self)
        self.project = project
        self.model_name = model_name
        self.addAction(self.tr("Set current")).triggered.connect(self.set_current)
        self.addAction(self.tr("Delete")).triggered.connect(self.delete)

    def set_current(self):
        self.project.set_current_model(self.model_name)

    def delete(self):
        self.project.delete_model(self.model_name)

class ScenarioMenu(QMenu):
    def __init__(self, project, scenario_name):
        QMenu.__init__(self)
        self.project = project
        self.scenario_name = scenario_name
        self.addAction(self.tr("Set current")).triggered.connect(self.set_current)
        self.addAction(self.tr("Edit")).triggered.connect(self.edit)
        self.addAction(self.tr("Delete")).triggered.connect(self.delete)
        self.addAction(self.tr("Duplicate")).triggered.connect(self.duplicate)

    def edit(self):
        from hydra.gui.scenario_manager import ScenarioManager
        scn_dialog = ScenarioManager(self.project)
        scn_dialog.select_scenario_by_name(self.scenario_name)
        scn_dialog.exec_()

    def set_current(self):
        id, = self.project.execute("""select id from project.scenario where name='{}';""".format(self.scenario_name)).fetchone()
        self.project.set_current_scenario(id)

    def delete(self):
        from hydra.gui.scenario_manager import ScenarioManager
        scn_dialog = ScenarioManager(self.project)
        scn_dialog.select_scenario_by_name(self.scenario_name)
        scn_dialog.delete_scenario()
        scn_dialog.save()

    def duplicate(self):
        from hydra.gui.scenario_manager import ScenarioManager
        scn_dialog = ScenarioManager(self.project)
        scn_dialog.select_scenario_by_name(self.scenario_name)
        scn_dialog.clone_scenario()
        scn_dialog.save()

class SerieMenu(QMenu):
    def __init__(self, project, serie_name):
        QMenu.__init__(self)
        self.project = project
        self.serie_name = serie_name
        self.addAction(self.tr("Edit")).triggered.connect(self.edit)
        self.addAction(self.tr("Delete")).triggered.connect(self.delete)

    def edit(self):
        from hydra.gui.serie_manager import SerieManager
        serie_dialog = SerieManager(self.project)
        serie_dialog.select_serie_by_name(self.serie_name)
        serie_dialog.exec_()

    def delete(self):
        from hydra.gui.serie_manager import SerieManager
        serie_dialog = SerieManager(self.project)
        serie_dialog.select_serie_by_name(self.serie_name)
        serie_dialog.delete_serie()
        serie_dialog.save()

class RainMenu(QMenu):
    def __init__(self, project, rain_name, type_rain):
        QMenu.__init__(self)
        self.project = project
        self.rain_name = rain_name
        self.type_rain = type_rain
        self.addAction(self.tr("Edit")).triggered.connect(self.edit)
        self.addAction(self.tr("Delete")).triggered.connect(self.delete)

    def edit(self):
        from hydra.gui.rain_manager import RainManager
        rain = RainManager(self.project)
        if self.type_rain==tr('Montanas'):
            rain.select_montana(self.rain_name)
        else:
            rain.select_rain(self.rain_name)
        rain.exec_()

    def delete(self):
        from hydra.gui.rain_manager import RainManager
        rain = RainManager(self.project)
        if self.type_rain==tr('Montanas'):
            rain.select_montana(self.rain_name)
            rain.delete_montana()
        else:
            rain.select_rain(self.rain_name)
            if self.type_rain==tr('Caquot'):
                rain.delete_caquot()
            elif self.type_rain==tr('Single triangular'):
                rain.delete_simple_tri()
            elif self.type_rain==tr('Double triangular'):
                rain.delete_double_tri()
            elif self.type_rain==tr('Hyetographs'):
                rain.delete_hyetograph()
            elif self.type_rain==tr('Gaged rainfalls'):
                rain.delete_gage_rainfall()
            elif self.type_rain==tr('Gaged rainfalls (file)'):
                rain.delete_gage_file_rainfall()
            elif self.type_rain==tr('Radar rainfalls'):
                rain.delete_radar_rainfall()
            elif self.type_rain==tr('MAGES rainfalls'):
                rain.delete_mages_rainfall()
        rain.save()

class DryScenarioMenu(QMenu):
    def __init__(self, project, scenario_name):
        QMenu.__init__(self)
        self.project = project
        self.scenario_name = scenario_name
        self.addAction(self.tr("Edit")).triggered.connect(self.edit)
        self.addAction(self.tr("Delete")).triggered.connect(self.delete)

    def edit(self):
        from hydra.gui.dry_inflow_manager import DryInflowManager
        scn_dialog = DryInflowManager(self.project)
        scn_dialog.select_by_name(self.scenario_name)
        scn_dialog.exec_()

    def delete(self):
        from hydra.gui.dry_inflow_manager import DryInflowManager
        scn_dialog = DryInflowManager(self.project)
        scn_dialog.select_by_name(self.scenario_name)
        scn_dialog.delete_dry_scenario()
        scn_dialog.save()

class ProjectTree(QDockWidget):
    def __init__(self, title, current_project, refresh_ui_fct):
        QDockWidget.__init__(self, title)
        self.__tree_widget = QTreeWidget()
        self.__tree_widget.header().close()
        self.refresh_ui = refresh_ui_fct
        self.refresh_content(current_project)

        scroll_area = QScrollArea()
        scroll_area.setWidget(self.__tree_widget)
        scroll_area.setWidgetResizable(True)
        self.setWidget(scroll_area)
        self.__tree_widget.itemDoubleClicked.connect(self.edit_item)
        self.__tree_widget.setContextMenuPolicy(Qt.CustomContextMenu)
        self.__tree_widget.customContextMenuRequested.connect(self.open_menu)

    def edit_item(self):
        indexes = self.__tree_widget.selectedItems()
        if len(indexes) > 0:
            item = indexes[0]
            menu = None
            if item.parent():
                type = item.parent().text(0)
                if type==tr('Models'):
                    menu = ModelMenu(self.project, item.text(0))
                    menu.set_current()
                elif type==tr('Scenarios'):
                    menu = ScenarioMenu(self.project, item.text(0))
                    menu.edit()
                elif type==tr('Time series'):
                    menu = SerieMenu(self.project, item.text(0))
                    menu.edit()
                elif type in rains:
                    menu = RainMenu(self.project, item.text(0), type)
                    menu.edit()
                elif type==tr('Dry inflow scenarios'):
                    menu = DryScenarioMenu(self.project, item.text(0))
                    menu.edit()
                self.refresh_ui()

    def open_menu(self, position):
        indexes = self.__tree_widget.selectedItems()
        if len(indexes) > 0:
            item = indexes[0]

            menu = None

            if item.parent().text(0)==tr('Models'):
                menu = ModelMenu(self.project, item.text(0))
            elif item.parent().text(0)==tr('Scenarios'):
                menu = ScenarioMenu(self.project, item.text(0))
            elif item.parent().text(0)==tr('Time series'):
                menu = SerieMenu(self.project, item.text(0))
            elif item.parent().text(0) in rains:
                menu = RainMenu(self.project, item.text(0), item.parent().text(0))
            elif item.parent().text(0)==tr('Dry inflow scenarios'):
                menu = DryScenarioMenu(self.project, item.text(0))

            if not menu is None:
                menu.exec_(self.__tree_widget.viewport().mapToGlobal(position))
                self.refresh_ui()

    def refresh_content(self, current_project):
        self.__tree_widget.clear()
        self.project =current_project
        # models
        self.__add_list(self.__tree_widget, tr('Models'), 'model')
        # scenarios
        self.__add_list(self.__tree_widget, tr('Scenarios'), 'scenario')
        # series
        self.__add_list(self.__tree_widget, tr('Time series'), 'serie')
        # hydrology
        hydrology = QTreeWidgetItem(self.__tree_widget)
        hydrology.setExpanded(True)
        hydrology.setText(0,tr('Hydrology'))
         # montanas
        self.__add_list(hydrology, tr('Montanas'), 'coef_montana')
         # rainfalls
        rainfalls = QTreeWidgetItem(hydrology)
        rainfalls.setExpanded(True)
        rainfalls.setText(0,tr('Rainfalls'))
          # caquot
        self.__add_list(rainfalls, tr('Caquot'), 'caquot_rainfall')
          # single triangular
        self.__add_list(rainfalls, tr('Single triangular'), 'single_triangular_rainfall')
          # double triangular
        self.__add_list(rainfalls, tr('Double triangular'), 'double_triangular_rainfall')
          # hyetograph
        self.__add_list(rainfalls, tr('Hyetographs'), 'intensity_curve_rainfall')
          # gaged rainfall
        self.__add_list(rainfalls, tr('Gaged rainfalls'), 'gage_rainfall')
          # gaged file rainfall
        self.__add_list(rainfalls, tr('Gaged rainfalls (file)'), 'gage_file_rainfall')
          # radar rainfall
        self.__add_list(rainfalls, tr('Radar rainfalls'), 'radar_rainfall')
          # mages rainfall
        self.__add_list(rainfalls, tr('MAGES rainfalls'), 'mages_rainfall')
        # dry inflows
        self.__add_list(hydrology, tr('Dry inflow scenarios'), 'dry_inflow_scenario')

    def __add_list(self, parent, title, table):
        list = [name for name, in self.project.execute("""select name from project.{};""".format(table)).fetchall()]
        if list:
            parent_node = QTreeWidgetItem(parent)
            parent_node.setExpanded(True)
            parent_node.setText(0,title)
            for item in sorted(list, key=str.lower):
                children_node = QTreeWidgetItem(parent_node)
                children_node.setFont(0,bold)
                children_node.setText(0,item)
                if table == 'serie':
                    children_node.setExpanded(True)
                    self.__add_bloc_list(children_node, item)

    def __add_bloc_list(self, serie_node, serie_name):
        list = [name for name, in self.project.execute("""select name from project.serie_bloc where id_cs=(select id from project.serie where name='{}');""".format(serie_name)).fetchall()]
        if list:
            for item in sorted(list, key=str.lower):
                bloc_node = QTreeWidgetItem(serie_node)
                bloc_node.setFont(0,bold)
                bloc_node.setText(0,item)

