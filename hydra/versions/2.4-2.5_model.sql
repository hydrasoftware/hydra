/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update $model.metadata set version = '2.5';
;;

/* ********************************************** */
/*  Land use function name                        */
/* ********************************************** */

create or replace function $model.auto_param_catchment(catchment_id integer)
returns hydra_catchment_param
language plpgsql
as
$$$$
    declare
	area real;
	rl real;
	slope real;
	c_imp real;
    begin
        select ST_Area(c.geom)
        from $model.catchment_node as n, $model.catchment as c
        where n.id=catchment_id and ST_Contains(c.geom, n.geom)
        into area;

        select ST_Length(l.geom), Greatest(( project.altitude(ST_StartPoint(l.geom)) - project.altitude(ST_EndPoint(l.geom)) ) / ST_Length(l.geom), 0.005)
        from $model.catchment_node as n, $model.routing_hydrology_link as l
        where n.id=catchment_id and l.up=n.id
        into rl, slope;

        select Sum((ST_Area(ST_Intersection(c.geom, lo.geom)) / ST_Area(c.geom)) * t.c_imp)
	    from $model.catchment_node as n, $model.catchment as c, project.land_occupation as lo
	    join project.land_type as t on t.land_type=lo.land_type
	    where n.id=catchment_id and ST_Contains(c.geom, n.geom) and ST_Intersects(c.geom, lo.geom)
	    into c_imp;

        return (area, rl, slope, c_imp);
    end;
$$$$
;;

/* ********************************************** */
/*  Auto HY before hydrology links                */
/* ********************************************** */

create or replace function ${model}.connector_hydrology_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;

            -- Creation of HY singularity if type = routing_hydrology_link or type = connector_hydrology_link
            if 'connector_hydrology' = 'routing_hydrology' or 'connector_hydrology' = 'connector_hydrology' then
                if down_type_ != 'manhole_hydrology' and not (select exists (select 1 from $model.hydrograph_bc_singularity where node=down_)) then
                    insert into $model.hydrograph_bc_singularity(geom) select geom from $model._node where id=down_;
                end if;
            end if;

            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('connector_hydrology', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'connector_hydrology') where name = 'define_later' and id = id_;
            insert into $model._connector_hydrology_link(id, link_type, hydrograph)
                values (id_, 'connector_hydrology', coalesce(new.hydrograph, (select hbc.id from $model.hydrograph_bc_singularity as hbc where ST_DWithin(ST_EndPoint(new.geom), hbc.geom, .1))));

            perform $model.add_configuration_fct(new.configuration::json, id_, 'connector_hydrology_link');
            update $model._link set validity = (select (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station', 'storage', 'crossroad', 'elem_2d') and (hydrograph is not null))) from  $model._connector_hydrology_link where id = id_) where id = id_;
            new.id = id_;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if (('no_column') is distinct from ('no_column')) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select 'no_column') as o, (select 'no_column') as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select 'no_column') n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._connector_hydrology_link set hydrograph=new.hydrograph where id=old.id;

            perform $model.add_configuration_fct(new.configuration::json, old.id, 'connector_hydrology_link');

            update $model._link set validity = (select (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station', 'storage', 'crossroad', 'elem_2d') and (hydrograph is not null))) from  $model._connector_hydrology_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._connector_hydrology_link where id=old.id;
            delete from $model._link where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create or replace function ${model}.routing_hydrology_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;

            -- Creation of HY singularity if type = routing_hydrology_link or type = connector_hydrology_link
            if 'routing_hydrology' = 'routing_hydrology' or 'routing_hydrology' = 'connector_hydrology' then
                if down_type_ != 'manhole_hydrology' and not (select exists (select 1 from $model.hydrograph_bc_singularity where node=down_)) then
                    insert into $model.hydrograph_bc_singularity(geom) select geom from $model._node where id=down_;
                end if;
            end if;

            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('routing_hydrology', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'routing_hydrology') where name = 'define_later' and id = id_;
            insert into $model._routing_hydrology_link(id, link_type, cross_section, length, slope, hydrograph)
                values (id_, 'routing_hydrology', coalesce(new.cross_section, 1), coalesce(new.length, 0.1), coalesce(new.slope, .01), coalesce(new.hydrograph, (select hbc.id from $model.hydrograph_bc_singularity as hbc where ST_DWithin(ST_EndPoint(new.geom), hbc.geom, .1))));

            perform $model.add_configuration_fct(new.configuration::json, id_, 'routing_hydrology_link');
            update $model._link set validity = (select (cross_section is not null) and (cross_section>=0) and (length is not null) and (length>=0) and (slope is not null) and (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station', 'crossroad', 'storage', 'elem_2d') and (hydrograph is not null))) from  $model._routing_hydrology_link where id = id_) where id = id_;
            new.id = id_;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.cross_section, new.length, new.slope) is distinct from (old.cross_section, old.length, old.slope)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.cross_section, old.length, old.slope) as o, (select new.cross_section, new.length, new.slope) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.cross_section, new.length, new.slope) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._routing_hydrology_link set cross_section=new.cross_section, length=new.length, slope=new.slope, hydrograph=new.hydrograph where id=old.id;

            perform $model.add_configuration_fct(new.configuration::json, old.id, 'routing_hydrology_link');

            update $model._link set validity = (select (cross_section is not null) and (cross_section>=0) and (length is not null) and (length>=0) and (slope is not null) and (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station', 'crossroad', 'storage', 'elem_2d') and (hydrograph is not null))) from  $model._routing_hydrology_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._routing_hydrology_link where id=old.id;
            delete from $model._link where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

/* ********************************************** */
/*  RACC update                                   */
/* ********************************************** */

alter table ${model}._model_connect_bc_singularity add column if not exists tz_array real[20][2];
;;

drop view if exists $model.model_connect_bc_singularity cascade;
;;

 create or replace view $model.model_connect_bc_singularity as
    select p.id,
        p.name,
        n.id as node,
        c.cascade_mode,
        c.zq_array,
        c.tz_array,
        c.quality,
        n.geom,
        p.configuration::character varying as configuration,
        p.validity,
        p.configuration as configuration_json
    from $model._model_connect_bc_singularity c,
        $model._singularity p,
        $model._node n
    where p.id = c.id and n.id = p.node;
;;

create or replace function ${model}.model_connect_bc_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'model_connect_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'model_connect_bc') where name = 'define_later' and id = id_;

            insert into $model._model_connect_bc_singularity(id, singularity_type, cascade_mode, zq_array, tz_array, quality)
                values (id_, 'model_connect_bc', coalesce(new.cascade_mode, 'hydrograph'), new.zq_array, new.tz_array, coalesce(new.quality, '{0, 0, 0, 0, 0, 0, 0, 0, 0}'::real[]));

            perform $model.add_configuration_fct(new.configuration::json, id_, 'model_connect_bc_singularity');
            update $model._singularity set validity = (select (cascade_mode is not null) and (cascade_mode='hydrograph' or zq_array is not null or tz_array is not null) and (array_length(zq_array, 1)<=20) and (array_length(zq_array, 1)>=1) and (array_length(zq_array, 2)=2) and (array_length(tz_array, 1)<=20) and (array_length(tz_array, 1)>=1) and (array_length(tz_array, 2)=2) and (array_length(quality, 1)=9) from  $model._model_connect_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.cascade_mode, new.zq_array, new.tz_array, new.quality) is distinct from (old.cascade_mode, old.zq_array, old.tz_array, old.quality)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.cascade_mode, old.zq_array, old.tz_array, old.quality) as o, (select new.cascade_mode, new.zq_array, new.tz_array, new.quality) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.cascade_mode, new.zq_array, new.tz_array, new.quality) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._model_connect_bc_singularity set cascade_mode=new.cascade_mode, zq_array=new.zq_array, tz_array=new.tz_array, quality=new.quality where id=old.id;

            perform $model.add_configuration_fct(new.configuration::json, old.id, 'model_connect_bc_singularity');
            update $model._singularity set validity = (select (cascade_mode is not null) and (cascade_mode='hydrograph' or zq_array is not null or tz_array is not null) and (array_length(zq_array, 1)<=20) and (array_length(zq_array, 1)>=1) and (array_length(zq_array, 2)=2) and (array_length(tz_array, 1)<=20) and (array_length(tz_array, 1)>=1) and (array_length(tz_array, 2)=2) and (array_length(quality, 1)=9) from  $model._model_connect_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._model_connect_bc_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_model_connect_bc_singularity_trig
    instead of insert or update or delete on $model.model_connect_bc_singularity
    for each row execute procedure $model.model_connect_bc_singularity_fct();
;;

/* ********************************************** */
/*  Active scenario in DB                         */
/* ********************************************** */

create or replace view $model.regulated_current_scn as
    select distinct on (name)
        row_number() over () AS id,
        CONCAT(type, ':', r.id) as orig_id,
        r.type,
        r.subtype,
        r.name,
        r.geom
    from project.regulated() as r
    where model = '$model' and scenario = (select name from project.scenario where id = (select current_scenario from project.metadata));
;;

/* ********************************************** */
/*  Coherent update triggers for ZGeom of nodes   */
/* ********************************************** */

create or replace function ${model}.crossroad_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('crossroad', coalesce(new.name, 'define_later'), (ST_SetSRID(ST_MakePoint(ST_X(new.geom), ST_Y(new.geom), coalesce(new.z_ground, project.altitude(new.geom))), $srid)), new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'crossroad') where name = 'define_later' and id = id_;
            insert into $model._crossroad_node(id, node_type, area, z_ground, h)
                values (id_, 'crossroad', coalesce(new.area, 1), coalesce(new.z_ground, (select project.altitude(new.geom))), coalesce(new.h, 0));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'crossroad_node');

            update $model._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) and (h is not null) and (h>=0) from  $model._crossroad_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area, new.z_ground, new.h) is distinct from (old.area, old.z_ground, old.h)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.z_ground, old.h) as o, (select new.area, new.z_ground, new.h) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.z_ground, new.h) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=(ST_SetSRID(ST_MakePoint(ST_X(new.geom), ST_Y(new.geom), coalesce(new.z_ground, project.altitude(new.geom))), $srid)), generated=new.generated where id=old.id;
            update $model._crossroad_node set area=new.area, z_ground=new.z_ground, h=new.h where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'crossroad_node');

            update $model._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) and (h is not null) and (h>=0) from  $model._crossroad_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            delete from project.interlink where (model_up='$model' and node_up=old.id) or (model_down='$model' and node_down=old.id);

            delete from $model._crossroad_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

create or replace function ${model}.elem_2d_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('elem_2d', coalesce(new.name, 'define_later'), (ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), coalesce(new.zb, project.altitude(new.geom))), $srid), (select precision from hydra.metadata))), new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'elem_2d') where name = 'define_later' and id = id_;
            insert into $model._elem_2d_node(id, node_type, area, zb, rk, domain_2d, contour)
                values (id_, 'elem_2d', coalesce(new.area, ST_Area(new.contour)), coalesce(new.zb, (select project.altitude(ST_Centroid(new.contour)))), coalesce(new.rk, 12), new.domain_2d, new.contour);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'elem_2d_node');

            update $model._node set validity = (select (area is not null) and (area>0) and (zb is not null) and (rk is not null) and (rk>0) from  $model._elem_2d_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area, new.zb, new.rk) is distinct from (old.area, old.zb, old.rk)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.zb, old.rk) as o, (select new.area, new.zb, new.rk) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.zb, new.rk) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=(ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), coalesce(new.zb, project.altitude(new.geom))), $srid), (select precision from hydra.metadata))), generated=new.generated where id=old.id;
            update $model._elem_2d_node set area=new.area, zb=new.zb, rk=new.rk, domain_2d=new.domain_2d, contour=new.contour where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'elem_2d_node');

            update $model._node set validity = (select (area is not null) and (area>0) and (zb is not null) and (rk is not null) and (rk>0) from  $model._elem_2d_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            delete from project.interlink where (model_up='$model' and node_up=old.id) or (model_down='$model' and node_down=old.id);

            delete from $model._elem_2d_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

create or replace function ${model}.storage_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('storage', coalesce(new.name, 'define_later'), (ST_SetSRID(ST_MakePoint(ST_X(new.geom), ST_Y(new.geom), coalesce(new.zs_array[1][1], project.altitude(new.geom))), $srid)), new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'storage') where name = 'define_later' and id = id_;
            insert into $model._storage_node(id, node_type, zs_array, zini, contour, contraction_coef)
                values (id_, 'storage', new.zs_array, new.zini, coalesce(new.contour, (select id from $model.coverage where st_intersects(geom, new.geom))), coalesce(new.contraction_coef, 1));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'storage_node');

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            update $model._node set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1) <= 10 ) and (array_length(zs_array, 1) >= 1) and (array_length(zs_array, 2) = 2) and (contraction_coef is not null) and (contraction_coef<=1) and (contraction_coef>=0) from  $model._storage_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.zs_array, new.zini, new.contraction_coef) is distinct from (old.zs_array, old.zini, old.contraction_coef)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.zs_array, old.zini, old.contraction_coef) as o, (select new.zs_array, new.zini, new.contraction_coef) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.zs_array, new.zini, new.contraction_coef) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=(ST_SetSRID(ST_MakePoint(ST_X(new.geom), ST_Y(new.geom), coalesce(new.zs_array[1][1], project.altitude(new.geom))), $srid)), generated=new.generated where id=old.id;
            update $model._storage_node set zs_array=new.zs_array, zini=new.zini, contour=new.contour, contraction_coef=new.contraction_coef where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'storage_node');

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            update $model._node set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1) <= 10 ) and (array_length(zs_array, 1) >= 1) and (array_length(zs_array, 2) = 2) and (contraction_coef is not null) and (contraction_coef<=1) and (contraction_coef>=0) from  $model._storage_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' and (select trigger_coverage from $model.metadata) then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            delete from project.interlink where (model_up='$model' and node_up=old.id) or (model_down='$model' and node_down=old.id);

            delete from $model._storage_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

create or replace function ${model}.river_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('river', coalesce(new.name, 'define_later'), (ST_SetSRID(ST_MakePoint(ST_X(new.geom), ST_Y(new.geom), coalesce($model.river_node_z_invert(new.geom), project.altitude(new.geom))), $srid)), new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'river') where name = 'define_later' and id = id_;
            insert into $model._river_node(id, node_type, reach, z_ground, area)
                values (id_, 'river', coalesce(new.reach, (select id from $model.reach where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) asc limit 1)), coalesce(new.z_ground, (project.altitude(new.geom))), coalesce(new.area, 1));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'river_node');

            update $model._node set validity = (select (z_ground is not null) and (area is not null) and (area>0) and (reach is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) from  $model._river_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.z_ground, new.area) is distinct from (old.z_ground, old.area)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_ground, old.area) as o, (select new.z_ground, new.area) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_ground, new.area) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=(ST_SetSRID(ST_MakePoint(ST_X(new.geom), ST_Y(new.geom), coalesce($model.river_node_z_invert(new.geom), project.altitude(new.geom))), $srid)), generated=new.generated where id=old.id;
            update $model._river_node set reach=new.reach, z_ground=new.z_ground, area=new.area where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'river_node');

            -- Lines to update specific nodes that works with associated contours
            if 'river' = 'river' and not ST_equals(new.geom, old.geom) then
                update $model._river_node set reach=(select id from $model.reach as r where ST_DWithin(new.geom, r.geom, 0.1) order by ST_Distance(new.geom, r.geom) asc limit 1) where id=old.id;
            end if;

            update $model._node set validity = (select (z_ground is not null) and (area is not null) and (area>0) and (reach is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) from  $model._river_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            delete from project.interlink where (model_up='$model' and node_up=old.id) or (model_down='$model' and node_down=old.id);

            delete from $model._river_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

create or replace function ${model}.manhole_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('manhole', coalesce(new.name, 'define_later'), (ST_SetSRID(ST_MakePoint(ST_X(new.geom), ST_Y(new.geom), coalesce(new.z_ground, project.altitude(new.geom))), $srid)), new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'manhole') where name = 'define_later' and id = id_;
            insert into $model._manhole_node(id, node_type, area, z_ground, cover_diameter, cover_critical_pressure, inlet_width, inlet_height, connection_law)
                values (id_, 'manhole', coalesce(new.area, 1), coalesce(new.z_ground, (select project.altitude(new.geom))), coalesce(new.cover_diameter, 0.6), coalesce(new.cover_critical_pressure, 2.0), coalesce(new.inlet_width, 1.0), coalesce(new.inlet_height, 0.25), coalesce(new.connection_law, 'manhole_cover'));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'manhole_node');

            update $model._node set validity = (select (connection_law is not null) and (connection_law != 'manhole_cover' or cover_diameter is not null) and (connection_law != 'manhole_cover' or cover_diameter > 0) and (connection_law != 'manhole_cover' or cover_critical_pressure is not null) and (connection_law != 'manhole_cover' or cover_critical_pressure >0) and (connection_law != 'drainage_inlet' or inlet_width is not null) and (connection_law != 'drainage_inlet' or inlet_width >0) and (connection_law != 'drainage_inlet' or inlet_height is not null) and (connection_law != 'drainage_inlet' or inlet_height >0) and (area is not null) and (area>0) and (z_ground is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) and ((select exists (select 1 from $model.pipe_link where up=new.id or down=new.id))) from  $model._manhole_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area, new.z_ground, new.cover_diameter, new.cover_critical_pressure, new.inlet_width, new.inlet_height, new.connection_law) is distinct from (old.area, old.z_ground, old.cover_diameter, old.cover_critical_pressure, old.inlet_width, old.inlet_height, old.connection_law)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.z_ground, old.cover_diameter, old.cover_critical_pressure, old.inlet_width, old.inlet_height, old.connection_law) as o, (select new.area, new.z_ground, new.cover_diameter, new.cover_critical_pressure, new.inlet_width, new.inlet_height, new.connection_law) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.z_ground, new.cover_diameter, new.cover_critical_pressure, new.inlet_width, new.inlet_height, new.connection_law) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=(ST_SetSRID(ST_MakePoint(ST_X(new.geom), ST_Y(new.geom), coalesce(new.z_ground, project.altitude(new.geom))), $srid)), generated=new.generated where id=old.id;
            update $model._manhole_node set area=new.area, z_ground=new.z_ground, cover_diameter=new.cover_diameter, cover_critical_pressure=new.cover_critical_pressure, inlet_width=new.inlet_width, inlet_height=new.inlet_height, connection_law=new.connection_law where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'manhole_node');
            update $model._node set validity = (select (connection_law is not null) and (connection_law != 'manhole_cover' or cover_diameter is not null) and (connection_law != 'manhole_cover' or cover_diameter > 0) and (connection_law != 'manhole_cover' or cover_critical_pressure is not null) and (connection_law != 'manhole_cover' or cover_critical_pressure >0) and (connection_law != 'drainage_inlet' or inlet_width is not null) and (connection_law != 'drainage_inlet' or inlet_width >0) and (connection_law != 'drainage_inlet' or inlet_height is not null) and (connection_law != 'drainage_inlet' or inlet_height >0) and (area is not null) and (area>0) and (z_ground is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) and ((select exists (select 1 from $model.pipe_link where up=new.id or down=new.id))) from  $model._manhole_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            delete from project.interlink where (model_up='$model' and node_up=old.id) or (model_down='$model' and node_down=old.id);

            delete from $model._manhole_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

create or replace function ${model}.river_cross_section_profile_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        select (
            (new.type_cross_section_up is null or (
                new.z_invert_up is not null
                and new.up_rk is not null
                and (new.type_cross_section_up!='circular' or (new.up_circular_diameter is not null and new.up_circular_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_top_diameter is not null and new.up_ovoid_top_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_invert_diameter is not null and new.up_ovoid_invert_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_height is not null and new.up_ovoid_height >0 ))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_height > (new.up_ovoid_top_diameter+new.up_ovoid_invert_diameter)/2))
                and (new.type_cross_section_up!='pipe' or new.up_cp_geom is not null)
                and (new.type_cross_section_up!='channel' or new.up_op_geom is not null)
                and (new.type_cross_section_up!='valley'
                        or (new.up_rk = 0 and new.up_rk_maj = 0 and new.up_sinuosity = 0)
                        or (new.up_rk >0 and new.up_rk_maj is not null and new.up_rk_maj > 0 and new.up_sinuosity is not null and new.up_sinuosity > 0))
                and (new.type_cross_section_up!='valley' or new.up_vcs_geom is not null)
                )
            )
            and
            (new.type_cross_section_down is null or (
                new.z_invert_down is not null
                and new.down_rk is not null
                and (new.type_cross_section_down!='circular' or (new.down_circular_diameter is not null and new.down_circular_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_top_diameter is not null and new.down_ovoid_top_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_invert_diameter is not null and new.down_ovoid_invert_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_height is not null and new.down_ovoid_height >0 ))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_height > (new.down_ovoid_top_diameter+new.down_ovoid_invert_diameter)/2))
                and (new.type_cross_section_down!='pipe' or new.down_cp_geom is not null)
                and (new.type_cross_section_down!='channel' or new.down_op_geom is not null)
                and (new.type_cross_section_down!='valley'
                        or (new.down_rk = 0 and new.down_rk_maj = 0 and new.down_sinuosity = 0)
                        or (new.down_rk >0 and new.down_rk_maj is not null and new.down_rk_maj > 0 and new.down_sinuosity is not null and new.down_sinuosity > 0))
                and (new.type_cross_section_down!='valley' or new.down_vcs_geom is not null)
                )
            )
            and
            (new.type_cross_section_up is null or new.type_cross_section_down is null or (
                new.z_invert_up is not null and new.z_invert_down is not null
                )
            )
        ) into new.validity;

        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id from $model.river_node where ST_DWithin(new.geom, geom, .1) into new.id;
            select coalesce(new.name, 'CP'||new.id::varchar) into new.name;
            if new.id is null then
                raise exception 'no river node nearby river_cross_section_profile %', st_astext(new.geom);
            end if;

            insert into $model._river_cross_section_profile (
                    id,
                    name,
                    z_invert_up,
                    z_invert_down,
                    type_cross_section_up,
                    type_cross_section_down,
                    up_rk,
                    up_rk_maj,
                    up_sinuosity,
                    up_circular_diameter,
                    up_ovoid_height,
                    up_ovoid_top_diameter,
                    up_ovoid_invert_diameter,
                    up_cp_geom,
                    up_op_geom,
                    up_vcs_geom,
                    up_vcs_topo_geom,
                    down_rk,
                    down_rk_maj,
                    down_sinuosity,
                    down_circular_diameter,
                    down_ovoid_height,
                    down_ovoid_top_diameter,
                    down_ovoid_invert_diameter,
                    down_cp_geom,
                    down_op_geom,
                    down_vcs_geom,
                    down_vcs_topo_geom,
                    configuration,
                    validity
                )
                values (
                    new.id,
                    new.name,
                    new.z_invert_up,
                    new.z_invert_down,
                    new.type_cross_section_up,
                    new.type_cross_section_down,
                    new.up_rk,
                    new.up_rk_maj,
                    new.up_sinuosity,
                    new.up_circular_diameter,
                    new.up_ovoid_height,
                    new.up_ovoid_top_diameter,
                    new.up_ovoid_invert_diameter,
                    new.up_cp_geom,
                    new.up_op_geom,
                    new.up_vcs_geom,
                    new.up_vcs_topo_geom,
                    new.down_rk,
                    new.down_rk_maj,
                    new.down_sinuosity,
                    new.down_circular_diameter,
                    new.down_ovoid_height,
                    new.down_ovoid_top_diameter,
                    new.down_ovoid_invert_diameter,
                    new.down_cp_geom,
                    new.down_op_geom,
                    new.down_vcs_geom,
                    new.down_vcs_topo_geom,
                    new.configuration::json,
                    new.validity
                );

            update $model.river_node
                set geom=ST_SetSRID(ST_MakePoint(ST_X(geom), ST_Y(geom), coalesce($model.river_node_z_invert(geom), project.altitude(geom))), $srid)
                where reach=(select reach from $model.river_node where id=new.id);

            perform $model.add_configuration_fct(new.configuration::json, new.id, 'river_cross_section_profile');

            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id from $model.river_node where ST_DWithin(new.geom, geom, .1) into new.id;
            select coalesce(new.name, 'CP'||new.id::varchar) into new.name;
            if new.id is null then
                raise exception 'no river node nearby river_cross_section_profile %', st_astext(new.geom);
            end if;

            -- Handle configurations
            if ((new.z_invert_up, new.z_invert_down,
                    new.type_cross_section_up, new.type_cross_section_down,
                    new.up_rk, new.up_rk_maj, new.up_sinuosity, new.up_circular_diameter,
                    new.up_ovoid_height, new.up_ovoid_top_diameter, new.up_ovoid_invert_diameter,
                    new.up_cp_geom, new.up_op_geom, new.up_vcs_geom, new.up_vcs_topo_geom,
                    new.down_rk, new.down_rk_maj, new.down_sinuosity, new.down_circular_diameter,
                    new.down_ovoid_height, new.down_ovoid_top_diameter, new.down_ovoid_invert_diameter,
                    new.down_cp_geom, new.down_op_geom, new.down_vcs_geom, new.down_vcs_topo_geom)
            is distinct from (old.z_invert_up, old.z_invert_down,
                    old.type_cross_section_up, old.type_cross_section_down,
                    old.up_rk, old.up_rk_maj, old.up_sinuosity, old.up_circular_diameter,
                    old.up_ovoid_height, old.up_ovoid_top_diameter, old.up_ovoid_invert_diameter,
                    old.up_cp_geom, old.up_op_geom, old.up_vcs_geom, old.up_vcs_topo_geom,
                    old.down_rk, old.down_rk_maj, old.down_sinuosity, old.down_circular_diameter,
                    old.down_ovoid_height, old.down_ovoid_top_diameter, old.down_ovoid_invert_diameter,
                    old.down_cp_geom, old.down_op_geom, old.down_vcs_geom, old.down_vcs_topo_geom))
            then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}'
                            from (select old.z_invert_up, old.z_invert_down,
                                old.type_cross_section_up, old.type_cross_section_down,
                                old.up_rk, old.up_rk_maj, old.up_sinuosity, old.up_circular_diameter,
                                old.up_ovoid_height, old.up_ovoid_top_diameter, old.up_ovoid_invert_diameter,
                                old.up_cp_geom, old.up_op_geom, old.up_vcs_geom, old.up_vcs_topo_geom,
                                old.down_rk, old.down_rk_maj, old.down_sinuosity, old.down_circular_diameter,
                                old.down_ovoid_height, old.down_ovoid_top_diameter, old.down_ovoid_invert_diameter,
                                old.down_cp_geom, old.down_op_geom, old.down_vcs_geom, old.down_vcs_topo_geom) as o,
                                (select new.z_invert_up, new.z_invert_down,
                                new.type_cross_section_up, new.type_cross_section_down,
                                new.up_rk, new.up_rk_maj, new.up_sinuosity, new.up_circular_diameter,
                                new.up_ovoid_height, new.up_ovoid_top_diameter, new.up_ovoid_invert_diameter,
                                new.up_cp_geom, new.up_op_geom, new.up_vcs_geom, new.up_vcs_topo_geom,
                                new.down_rk, new.down_rk_maj, new.down_sinuosity, new.down_circular_diameter,
                                new.down_ovoid_height, new.down_ovoid_top_diameter, new.down_ovoid_invert_diameter,
                                new.down_cp_geom, new.down_op_geom, new.down_vcs_geom, new.down_vcs_topo_geom) as n
                            into new_config;
                        update $model._river_cross_section_profile set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}'
                            from (select new.z_invert_up, new.z_invert_down,
                                new.type_cross_section_up, new.type_cross_section_down,
                                new.up_rk, new.up_rk_maj, new.up_sinuosity, new.up_circular_diameter,
                                new.up_ovoid_height, new.up_ovoid_top_diameter, new.up_ovoid_invert_diameter,
                                new.up_cp_geom, new.up_op_geom, new.up_vcs_geom, new.up_vcs_topo_geom,
                                new.down_rk, new.down_rk_maj, new.down_sinuosity, new.down_circular_diameter,
                                new.down_ovoid_height, new.down_ovoid_top_diameter, new.down_ovoid_invert_diameter,
                                new.down_cp_geom, new.down_op_geom, new.down_vcs_geom, new.down_vcs_topo_geom) n
                            into new_config;
                        update $model._river_cross_section_profile set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._river_cross_section_profile set
                id=new.id,
                name=new.name,
                z_invert_up=new.z_invert_up,
                z_invert_down=new.z_invert_down,
                type_cross_section_up=new.type_cross_section_up,
                type_cross_section_down=new.type_cross_section_down,
                up_rk=new.up_rk,
                up_rk_maj=new.up_rk_maj,
                up_sinuosity=new.up_sinuosity,
                up_circular_diameter=new.up_circular_diameter,
                up_ovoid_height=new.up_ovoid_height,
                up_ovoid_top_diameter=new.up_ovoid_top_diameter,
                up_ovoid_invert_diameter=new.up_ovoid_invert_diameter,
                up_cp_geom=new.up_cp_geom,
                up_op_geom=new.up_op_geom,
                up_vcs_geom=new.up_vcs_geom,
                up_vcs_topo_geom=new.up_vcs_topo_geom,
                down_rk=new.down_rk,
                down_rk_maj=new.down_rk_maj,
                down_sinuosity=new.down_sinuosity,
                down_circular_diameter=new.down_circular_diameter,
                down_ovoid_height=new.down_ovoid_height,
                down_ovoid_top_diameter=new.down_ovoid_top_diameter,
                down_ovoid_invert_diameter=new.down_ovoid_invert_diameter,
                down_cp_geom=new.down_cp_geom,
                down_op_geom=new.down_op_geom,
                down_vcs_geom=new.down_vcs_geom,
                down_vcs_topo_geom=new.down_vcs_topo_geom,
                validity=new.validity
            where id=old.id;

            update $model.river_node
                set geom=ST_SetSRID(ST_MakePoint(ST_X(geom), ST_Y(geom), coalesce($model.river_node_z_invert(geom), project.altitude(geom))), $srid)
                where reach=(select reach from $model.river_node where id=new.id);

            perform $model.add_configuration_fct(new.configuration::json, new.id, 'river_cross_section_profile');

            return new;
        end if;
    end;
$$$$
;;

/* ********************************************** */
/*  Border attribute for connectors               */
/* ********************************************** */

drop view ${model}.connector_link cascade
;;

alter table ${model}._connector_link
add border geometry('LINESTRINGZ',$srid)
;;

create view $model.connector_link as
    select p.id,
        p.name,
        p.up,
        p.down,
        c.main_branch,
        c.border,
        p.geom,
        p.configuration::character varying as configuration,
        p.generated,
        p.validity,
        p.up_type,
        p.down_type,
        p.configuration as configuration_json
    from $model._connector_link as c,
        $model._link as p
    where p.id = c.id
;;

create or replace function ${model}.connector_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;

            -- Creation of HY singularity if type = routing_hydrology_link or type = connector_hydrology_link
            if 'connector' = 'routing_hydrology' or 'connector' = 'connector_hydrology' then
                if down_type_ != 'manhole_hydrology' and not (select exists (select 1 from $model.hydrograph_bc_singularity where node=down_)) then
                    insert into $model.hydrograph_bc_singularity(geom) select geom from $model._node where id=down_;
                end if;
            end if;

            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('connector', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'connector') where name = 'define_later' and id = id_;
            insert into $model._connector_link(id, link_type, main_branch, border)
                values (id_, 'connector', coalesce(new.main_branch, 'f'), new.border);
            if 'connector' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'connector_link');
            update $model._link set validity = (select 't'::boolean from  $model._connector_link where id = id_) where id = id_;
            new.id = id_;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.main_branch) is distinct from (old.main_branch)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.main_branch) as o, (select new.main_branch) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.main_branch) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._connector_link set main_branch=new.main_branch, border=new.border where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'connector_link');

            update $model._link set validity = (select 't'::boolean from  $model._connector_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._connector_link where id=old.id;
            delete from $model._link where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_connector_link_trig
    instead of insert or update or delete on $model.connector_link
       for each row execute procedure ${model}.connector_link_fct()
;;

/* ********************************************** */
/*            River node tools                    */
/* ********************************************** */

create or replace function ${model}.river_node_pk(node_geom geometry)
returns double precision
language plpgsql
as $$$$
    declare
        _res real;
    begin
        select st_linelocatepoint(r.geom, node_geom) * st_length(r.geom) / 1000::double precision + r.pk0_km
            from ${model}.reach as r
            where ST_DWithin(r.geom, node_geom, .1) order by ST_Distance(r.geom, node_geom) asc limit 1
            into _res;
        return _res;
    end;
$$$$
;;

create or replace function ${model}.river_node_z_invert(node_geom geometry)
returns real
language plpgsql
as $$$$
    declare
        _res real;
    begin
        with node as (
                select ${model}.river_node_pk(node_geom) as pk_km, id as reach from ${model}.reach as r
                where ST_DWithin(r.geom, node_geom, .1) order by ST_Distance(r.geom, node_geom) asc limit 1
            ),
            pts_up as (
                select p.id, ${model}.river_node_pk(p.geom) as pk_km, COALESCE(z_invert_down, z_invert_up) as z_invert
                from ${model}._river_node as n, node, ${model}.river_cross_section_profile as p
                where n.reach=node.reach and p.id=n.id and ${model}.river_node_pk(p.geom) <= node.pk_km
                ),
            pts_down as (
                select p.id, ${model}.river_node_pk(p.geom) as pk_km, COALESCE(z_invert_up, z_invert_down) as z_invert
                from ${model}._river_node as n, node, ${model}.river_cross_section_profile as p
                where n.reach=node.reach and p.id=n.id and node.pk_km <= ${model}.river_node_pk(p.geom)
                ),
            up as (
                select z_invert, pk_km, id from pts_up
                where z_invert is not null
                order by pk_km desc limit 1
                ),
            down as (
                select z_invert, pk_km, id from pts_down
                where z_invert is not null
                order by pk_km asc limit 1
                ),
            cote_minmax as (
                select up.z_invert as cote, COALESCE(NULLIF(down.pk_km - node.pk_km, 0), 1) as weight from up, down, node
                union all
                select down.z_invert as cote, COALESCE(NULLIF(node.pk_km - up.pk_km, 0), 1) as weight from up, down, node
            ),
            z_invert as (
                select SUM(cote * weight)/SUM(weight) as z
                from cote_minmax
                having bool_and(cote is not null)
            )
            select case
                when not exists (select 1 from z_invert) then null
                else (select z from z_invert) end
            into _res;
        return _res;
    end;
$$$$
;;


/* ********************************************** */
/*            results layers                      */
/* ********************************************** */

create materialized view ${model}.results_link as
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.gate_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.regul_gate_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.weir_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.borda_headloss_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.pump_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.deriv_pump_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.fuse_spillway_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.connector_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.strickler_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.porous_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.overflow_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.network_overflow_link
with no data;
;;

/* ********************************************** */
/*  Invalid view                                  */
/* ********************************************** */

create or replace view ${model}.invalid as
    WITH node_invalidity_reason AS (
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END ||
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END) ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.reach IS NOT NULL THEN ''::text
                    ELSE '   reach is not null   '::text
                END) ||
                CASE
                    WHEN ( SELECT NOT (EXISTS ( SELECT 1
                               FROM $model.station
                              WHERE st_intersects(station.geom, new_1.geom)))) THEN ''::text
                    ELSE '   (select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))   '::text
                END AS reason
           FROM $model.river_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   (area > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END AS reason
           FROM $model.manhole_hydrology_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END ||
                CASE
                    WHEN new_1.zini IS NOT NULL THEN ''::text
                    ELSE '   zini is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1) <= 10    '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1) >= 1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2) = 2   '::text
                END) ||
                CASE
                    WHEN new_1.contraction_coef IS NOT NULL THEN ''::text
                    ELSE '   contraction_coef is not null   '::text
                END) ||
                CASE
                    WHEN new_1.contraction_coef <= 1::double precision THEN ''::text
                    ELSE '   contraction_coef<=1   '::text
                END) ||
                CASE
                    WHEN new_1.contraction_coef >= 0::double precision THEN ''::text
                    ELSE '   contraction_coef>=0   '::text
                END AS reason
           FROM $model.storage_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((((((((((((((((((((((((
                CASE
                    WHEN new_1.area_ha IS NOT NULL THEN ''::text
                    ELSE '   area_ha is not null   '::text
                END ||
                CASE
                    WHEN new_1.area_ha > 0::double precision THEN ''::text
                    ELSE '   area_ha>0   '::text
                END) ||
                CASE
                    WHEN new_1.rl IS NOT NULL THEN ''::text
                    ELSE '   rl is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rl > 0::double precision THEN ''::text
                    ELSE '   rl>0   '::text
                END) ||
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp IS NOT NULL THEN ''::text
                    ELSE '   c_imp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp >= 0::double precision THEN ''::text
                    ELSE '   c_imp>=0   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp <= 1::double precision THEN ''::text
                    ELSE '   c_imp<=1   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type IS NOT NULL THEN ''::text
                    ELSE '   netflow_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'constant_runoff'::hydra_netflow_type OR new_1.constant_runoff IS NOT NULL AND new_1.constant_runoff >= 0::double precision AND new_1.constant_runoff <= 1::double precision THEN ''::text
                    ELSE '   netflow_type!=''constant_runoff'' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'horner'::hydra_netflow_type OR new_1.horner_ini_loss_coef IS NOT NULL AND new_1.horner_ini_loss_coef >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''horner'' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'horner'::hydra_netflow_type OR new_1.horner_recharge_coef IS NOT NULL AND new_1.horner_recharge_coef >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''horner'' or (horner_recharge_coef is not null and horner_recharge_coef>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'holtan'::hydra_netflow_type OR new_1.holtan_sat_inf_rate_mmh IS NOT NULL AND new_1.holtan_sat_inf_rate_mmh >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''holtan'' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'holtan'::hydra_netflow_type OR new_1.holtan_dry_inf_rate_mmh IS NOT NULL AND new_1.holtan_dry_inf_rate_mmh >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''holtan'' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'holtan'::hydra_netflow_type OR new_1.holtan_soil_storage_cap_mm IS NOT NULL AND new_1.holtan_soil_storage_cap_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''holtan'' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'scs'::hydra_netflow_type OR new_1.scs_j_mm IS NOT NULL AND new_1.scs_j_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''scs'' or (scs_j_mm is not null and scs_j_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'scs'::hydra_netflow_type OR new_1.scs_soil_drainage_time_day IS NOT NULL AND new_1.scs_soil_drainage_time_day >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''scs'' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'scs'::hydra_netflow_type OR new_1.scs_rfu_mm IS NOT NULL AND new_1.scs_rfu_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''scs'' or (scs_rfu_mm is not null and scs_rfu_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'hydra'::hydra_netflow_type OR new_1.hydra_surface_soil_storage_rfu_mm IS NOT NULL AND new_1.hydra_surface_soil_storage_rfu_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''hydra'' or (hydra_surface_soil_storage_rfu_mm is not null and hydra_surface_soil_storage_rfu_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'hydra'::hydra_netflow_type OR new_1.hydra_inf_rate_f0_mm_day IS NOT NULL AND new_1.hydra_inf_rate_f0_mm_day >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''hydra'' or (hydra_inf_rate_f0_mm_day is not null and hydra_inf_rate_f0_mm_day>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'hydra'::hydra_netflow_type OR new_1.hydra_int_soil_storage_j_mm IS NOT NULL AND new_1.hydra_int_soil_storage_j_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''hydra'' or (hydra_int_soil_storage_j_mm is not null and hydra_int_soil_storage_j_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'hydra'::hydra_netflow_type OR new_1.hydra_soil_drainage_time_qres_day IS NOT NULL AND new_1.hydra_soil_drainage_time_qres_day >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''hydra'' or (hydra_soil_drainage_time_qres_day is not null and hydra_soil_drainage_time_qres_day>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'gr4'::hydra_netflow_type OR new_1.gr4_k1 IS NOT NULL AND new_1.gr4_k1 >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''gr4'' or (gr4_k1 is not null and gr4_k1>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'gr4'::hydra_netflow_type OR new_1.gr4_k2 IS NOT NULL THEN ''::text
                    ELSE '   netflow_type!=''gr4'' or (gr4_k2 is not null)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'gr4'::hydra_netflow_type OR new_1.gr4_k3 IS NOT NULL AND new_1.gr4_k3 >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''gr4'' or (gr4_k3 is not null and gr4_k3>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'gr4'::hydra_netflow_type OR new_1.gr4_k4 IS NOT NULL AND new_1.gr4_k4 >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''gr4'' or (gr4_k4 is not null and gr4_k4>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type IS NOT NULL OR new_1.netflow_type = 'gr4'::hydra_netflow_type THEN ''::text
                    ELSE '   runoff_type is not null or netflow_type=''gr4''   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type <> 'socose'::hydra_runoff_type OR new_1.socose_tc_mn IS NOT NULL AND new_1.socose_tc_mn > 0::double precision OR new_1.netflow_type = 'gr4'::hydra_netflow_type THEN ''::text
                    ELSE '   runoff_type!=''socose'' or (socose_tc_mn is not null and socose_tc_mn>0) or netflow_type=''gr4''   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type <> 'socose'::hydra_runoff_type OR new_1.socose_shape_param_beta IS NOT NULL AND new_1.socose_shape_param_beta >= 1::double precision AND new_1.socose_shape_param_beta <= 6::double precision OR new_1.netflow_type = 'gr4'::hydra_netflow_type THEN ''::text
                    ELSE '   runoff_type!=''socose'' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6) or netflow_type=''gr4''   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type <> 'define_k'::hydra_runoff_type OR new_1.define_k_mn IS NOT NULL AND new_1.define_k_mn > 0::double precision OR new_1.netflow_type = 'gr4'::hydra_netflow_type THEN ''::text
                    ELSE '   runoff_type!=''define_k'' or (define_k_mn is not null and define_k_mn>0) or netflow_type=''gr4''   '::text
                END) ||
                CASE
                    WHEN new_1.q_limit IS NOT NULL THEN ''::text
                    ELSE '   q_limit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q0 IS NOT NULL THEN ''::text
                    ELSE '   q0 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.network_type IS NOT NULL THEN ''::text
                    ELSE '   network_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rural_land_use IS NULL OR new_1.rural_land_use >= 0::double precision THEN ''::text
                    ELSE '   rural_land_use is null or rural_land_use>=0   '::text
                END) ||
                CASE
                    WHEN new_1.industrial_land_use IS NULL OR new_1.industrial_land_use >= 0::double precision THEN ''::text
                    ELSE '   industrial_land_use is null or industrial_land_use>=0   '::text
                END) ||
                CASE
                    WHEN new_1.suburban_housing_land_use IS NULL OR new_1.suburban_housing_land_use >= 0::double precision THEN ''::text
                    ELSE '   suburban_housing_land_use is null or suburban_housing_land_use>=0   '::text
                END) ||
                CASE
                    WHEN new_1.dense_housing_land_use IS NULL OR new_1.dense_housing_land_use >= 0::double precision THEN ''::text
                    ELSE '   dense_housing_land_use is null or dense_housing_land_use>=0   '::text
                END AS reason
           FROM $model.catchment_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END) ||
                CASE
                    WHEN new_1.station IS NOT NULL THEN ''::text
                    ELSE '   station is not null    '::text
                END) ||
                CASE
                    WHEN ( SELECT st_intersects(new_1.geom, station.geom) AS st_intersects
                       FROM $model.station
                      WHERE station.id = new_1.station) THEN ''::text
                    ELSE '   (select st_intersects(new.geom, geom) from $model.station where id=new.station)   '::text
                END AS reason
           FROM $model.station_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((
                CASE
                    WHEN new_1.connection_law IS NOT NULL THEN ''::text
                    ELSE '   connection_law is not null   '::text
                END ||
                CASE
                    WHEN new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type OR new_1.cover_diameter IS NOT NULL THEN ''::text
                    ELSE '   connection_law != ''manhole_cover'' or cover_diameter is not null   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type OR new_1.cover_diameter > 0::double precision THEN ''::text
                    ELSE '   connection_law != ''manhole_cover'' or cover_diameter > 0   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type OR new_1.cover_critical_pressure IS NOT NULL THEN ''::text
                    ELSE '   connection_law != ''manhole_cover'' or cover_critical_pressure is not null   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type OR new_1.cover_critical_pressure > 0::double precision THEN ''::text
                    ELSE '   connection_law != ''manhole_cover'' or cover_critical_pressure >0   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type OR new_1.inlet_width IS NOT NULL THEN ''::text
                    ELSE '   connection_law != ''drainage_inlet'' or inlet_width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type OR new_1.inlet_width > 0::double precision THEN ''::text
                    ELSE '   connection_law != ''drainage_inlet'' or inlet_width >0   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type OR new_1.inlet_height IS NOT NULL THEN ''::text
                    ELSE '   connection_law != ''drainage_inlet'' or inlet_height is not null   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type OR new_1.inlet_height > 0::double precision THEN ''::text
                    ELSE '   connection_law != ''drainage_inlet'' or inlet_height >0   '::text
                END) ||
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END) ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END) ||
                CASE
                    WHEN ( SELECT NOT (EXISTS ( SELECT 1
                               FROM $model.station
                              WHERE st_intersects(station.geom, new_1.geom)))) THEN ''::text
                    ELSE '   (select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))   '::text
                END) ||
                CASE
                    WHEN ( SELECT (EXISTS ( SELECT 1
                               FROM $model.pipe_link
                              WHERE pipe_link.up = new_1.id OR pipe_link.down = new_1.id)) AS "exists") THEN ''::text
                    ELSE '   (select exists (select 1 from $model.pipe_link where up=new.id or down=new.id))   '::text
                END AS reason
           FROM $model.manhole_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.zb IS NOT NULL THEN ''::text
                    ELSE '   zb is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk > 0::double precision THEN ''::text
                    ELSE '   rk>0   '::text
                END AS reason
           FROM $model.elem_2d_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   (area > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h IS NOT NULL THEN ''::text
                    ELSE '   h is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h >= 0::double precision THEN ''::text
                    ELSE '   h>=0   '::text
                END AS reason
           FROM $model.crossroad_node new_1
          WHERE NOT new_1.validity
        ), link_invalidity_reason AS (
         SELECT new_1.id,
            ((((((((((((((((
                CASE
                    WHEN new_1.z_invert_up IS NOT NULL THEN ''::text
                    ELSE '   z_invert_up is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_invert_down IS NOT NULL THEN ''::text
                    ELSE '   z_invert_down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h_sable IS NOT NULL THEN ''::text
                    ELSE '   h_sable is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'valley'::hydra_cross_section_type THEN ''::text
                    ELSE '   cross_section_type not in (''valley'')   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk > 0::double precision THEN ''::text
                    ELSE '   rk >0   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'circular'::hydra_cross_section_type OR new_1.circular_diameter IS NOT NULL AND new_1.circular_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''circular'' or (circular_diameter is not null and circular_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_top_diameter IS NOT NULL AND new_1.ovoid_top_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_invert_diameter IS NOT NULL AND new_1.ovoid_invert_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_height IS NOT NULL AND new_1.ovoid_height > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_height is not null and ovoid_height >0 )   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_height > ((new_1.ovoid_top_diameter + new_1.ovoid_invert_diameter) / 2::double precision) THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'pipe'::hydra_cross_section_type OR new_1.cp_geom IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type!=''pipe'' or cp_geom is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'channel'::hydra_cross_section_type OR new_1.op_geom IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type!=''channel'' or op_geom is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.pipe_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= '0'::numeric::double precision THEN ''::text
                    ELSE '   width>=0.   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= '0'::numeric::double precision THEN ''::text
                    ELSE '   cc>=0.   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode IS NOT NULL THEN ''::text
                    ELSE '   break_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode OR new_1.z_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''zw_critical'' or z_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode OR new_1.t_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''time_critical'' or t_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or grp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp > 0 THEN ''::text
                    ELSE '   break_mode=''none'' or grp>0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp < 100 THEN ''::text
                    ELSE '   break_mode=''none'' or grp<100   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.dt_fracw_array IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or dt_fracw_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) <= 10 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) >= 1 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 2) = 2 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.fuse_spillway_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.cross_section IS NOT NULL THEN ''::text
                    ELSE '   cross_section is not null   '::text
                END ||
                CASE
                    WHEN new_1.cross_section >= 0::double precision THEN ''::text
                    ELSE '   cross_section>=0   '::text
                END) ||
                CASE
                    WHEN new_1.length IS NOT NULL THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN new_1.length >= 0::double precision THEN ''::text
                    ELSE '   length>=0   '::text
                END) ||
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type = 'manhole_hydrology'::hydra_node_type OR (new_1.down_type = ANY (ARRAY['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'crossroad'::hydra_node_type, 'storage'::hydra_node_type, 'elem_2d'::hydra_node_type])) AND new_1.hydrograph IS NOT NULL THEN ''::text
                    ELSE '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'', ''crossroad'', ''storage'', ''elem_2d'') and (hydrograph is not null))   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.routing_hydrology_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((
                CASE
                    WHEN new_1.q_pump IS NOT NULL THEN ''::text
                    ELSE '   q_pump is not null   '::text
                END ||
                CASE
                    WHEN new_1.q_pump >= 0::double precision THEN ''::text
                    ELSE '   q_pump>= 0   '::text
                END) ||
                CASE
                    WHEN new_1.qz_array IS NOT NULL THEN ''::text
                    ELSE '   qz_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(qz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN ( SELECT bool_and(data_set.bool) AS bool_and
                       FROM ( SELECT 0::double precision <= unnest(new_1.qz_array[:][1:1]) AS bool) data_set) THEN ''::text
                    ELSE '   (/*   qz_array: 0<=q   */ select bool_and(bool) from (select 0<=unnest(new.qz_array[:][1]) as bool) as data_set)   '::text
                END) ||
                CASE
                    WHEN ( SELECT bool_and(data_set.bool) AS bool_and
                       FROM ( SELECT unnest(new_1.qz_array[:][1:1]) <= 1::double precision AS bool) data_set) THEN ''::text
                    ELSE '   (/*   qz_array: q<=1   */ select bool_and(bool) from (select unnest(new.qz_array[:][1])<=1 as bool) as data_set)   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.deriv_pump_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.transmitivity IS NOT NULL THEN ''::text
                    ELSE '   transmitivity is not null   '::text
                END) ||
                CASE
                    WHEN new_1.transmitivity >= 0::double precision THEN ''::text
                    ELSE '   transmitivity>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.porous_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null    '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.weir_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.law_type IS NOT NULL THEN ''::text
                    ELSE '   law_type is not null   '::text
                END ||
                CASE
                    WHEN new_1.param IS NOT NULL THEN ''::text
                    ELSE '   param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.borda_headloss_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN new_1.npump IS NOT NULL THEN ''::text
                    ELSE '   npump is not null   '::text
                END ||
                CASE
                    WHEN new_1.npump <= 10 THEN ''::text
                    ELSE '   npump<=10   '::text
                END) ||
                CASE
                    WHEN new_1.npump >= 1 THEN ''::text
                    ELSE '   npump>=1   '::text
                END) ||
                CASE
                    WHEN new_1.zregul_array IS NOT NULL THEN ''::text
                    ELSE '   zregul_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.hq_array IS NOT NULL THEN ''::text
                    ELSE '   hq_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zregul_array, 1) = new_1.npump THEN ''::text
                    ELSE '   array_length(zregul_array, 1)=npump   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zregul_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zregul_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 1) = new_1.npump THEN ''::text
                    ELSE '   array_length(hq_array, 1)=npump   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 2) <= 10 THEN ''::text
                    ELSE '   array_length(hq_array, 2)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 2) >= 1 THEN ''::text
                    ELSE '   array_length(hq_array, 2)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 3) = 2 THEN ''::text
                    ELSE '   array_length(hq_array, 3)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.pump_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((
                CASE
                    WHEN new_1.z_overflow IS NOT NULL THEN ''::text
                    ELSE '   z_overflow is not null   '::text
                END ||
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END) ||
                CASE
                    WHEN new_1.area >= 0::double precision THEN ''::text
                    ELSE '   area>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.network_overflow_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.lateral_contraction_coef IS NOT NULL THEN ''::text
                    ELSE '   lateral_contraction_coef is not null   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef <= 1::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef<=1   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef >= 0::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef>=0   '::text
                END) ||
                CASE
                    WHEN st_npoints(new_1.border) = 2 THEN ''::text
                    ELSE '   ST_NPoints(border)=2   '::text
                END) ||
                CASE
                    WHEN st_isvalid(new_1.border) THEN ''::text
                    ELSE '   ST_IsValid(border)   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.mesh_2d_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling > new_1.z_invert THEN ''::text
                    ELSE '   z_ceiling>z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc <=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc >=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_valve IS NOT NULL THEN ''::text
                    ELSE '   mode_valve is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate IS NOT NULL THEN ''::text
                    ELSE '   z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate >= new_1.z_invert THEN ''::text
                    ELSE '   z_gate>=z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate <= new_1.z_ceiling THEN ''::text
                    ELSE '   z_gate<=z_ceiling   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc_submerged > 0::double precision THEN ''::text
                    ELSE '   cc_submerged >0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.gate_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((((((((((((((((
                CASE
                    WHEN new_1.z_crest1 IS NOT NULL THEN ''::text
                    ELSE '   z_crest1 is not null   '::text
                END ||
                CASE
                    WHEN new_1.width1 IS NOT NULL THEN ''::text
                    ELSE '   width1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width1 >= 0::double precision THEN ''::text
                    ELSE '   width1>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 IS NOT NULL THEN ''::text
                    ELSE '   z_crest2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 >= new_1.z_crest1 THEN ''::text
                    ELSE '   z_crest2>=z_crest1   '::text
                END) ||
                CASE
                    WHEN new_1.width2 IS NOT NULL THEN ''::text
                    ELSE '   width2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width2 >= 0::double precision THEN ''::text
                    ELSE '   width2>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef IS NOT NULL THEN ''::text
                    ELSE '   lateral_contraction_coef is not null   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef <= 1::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef<=1   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef >= 0::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef>=0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode IS NOT NULL THEN ''::text
                    ELSE '   break_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode OR new_1.z_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''zw_critical'' or z_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode OR new_1.t_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''time_critical'' or t_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.width_breach IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or width_breach is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or z_invert is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or grp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp > 0 THEN ''::text
                    ELSE '   break_mode=''none'' or grp>0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp < 100 THEN ''::text
                    ELSE '   break_mode=''none'' or grp<100   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.dt_fracw_array IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or dt_fracw_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) <= 10 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) >= 1 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 2) = 2 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.overflow_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert_stop IS NOT NULL THEN ''::text
                    ELSE '   z_invert_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling_stop IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.dt_regul_hr IS NOT NULL THEN ''::text
                    ELSE '   dt_regul_hr is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_control_node IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_pid_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_tz_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR new_1.q_z_crit IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul OR new_1.nr_z_gate IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc_submerged > 0::double precision THEN ''::text
                    ELSE '   cc_submerged >0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.regul_gate_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.down_type = 'manhole_hydrology'::hydra_node_type OR (new_1.down_type = ANY (ARRAY['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'storage'::hydra_node_type, 'crossroad'::hydra_node_type, 'elem_2d'::hydra_node_type])) AND new_1.hydrograph IS NOT NULL THEN ''::text
                    ELSE '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'', ''storage'', ''crossroad'', ''elem_2d'') and (hydrograph is not null))   '::text
                END ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.connector_hydrology_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN new_1.z_crest1 IS NOT NULL THEN ''::text
                    ELSE '   z_crest1 is not null   '::text
                END ||
                CASE
                    WHEN new_1.width1 IS NOT NULL THEN ''::text
                    ELSE '   width1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width1 > 0::double precision THEN ''::text
                    ELSE '   width1>0   '::text
                END) ||
                CASE
                    WHEN new_1.length IS NOT NULL THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN new_1.length > 0::double precision THEN ''::text
                    ELSE '   length>0   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk >= 0::double precision THEN ''::text
                    ELSE '   rk>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 IS NOT NULL THEN ''::text
                    ELSE '   z_crest2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 > new_1.z_crest1 THEN ''::text
                    ELSE '   z_crest2>z_crest1   '::text
                END) ||
                CASE
                    WHEN new_1.width2 IS NOT NULL THEN ''::text
                    ELSE '   width2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width2 > new_1.width1 THEN ''::text
                    ELSE '   width2>width1   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.strickler_link new_1
          WHERE NOT new_1.validity
        ), singularity_invalidity_reason AS (
         SELECT new_1.id,
            ((
                CASE
                    WHEN new_1.zq_array IS NOT NULL THEN ''::text
                    ELSE '   zq_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) <= 20 THEN ''::text
                    ELSE '   array_length(zq_array, 1)<=20   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zq_array, 2)=2   '::text
                END AS reason
           FROM $model.zq_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((
                CASE
                    WHEN new_1.downstream IS NOT NULL THEN ''::text
                    ELSE '   downstream is not null   '::text
                END ||
                CASE
                    WHEN new_1.split1 IS NOT NULL THEN ''::text
                    ELSE '   split1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.downstream_param IS NOT NULL THEN ''::text
                    ELSE '   downstream_param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1_law IS NOT NULL THEN ''::text
                    ELSE '   split1_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1_param IS NOT NULL THEN ''::text
                    ELSE '   split1_param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NULL OR new_1.split2_law IS NOT NULL THEN ''::text
                    ELSE '   split2 is null or split2_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NULL OR new_1.split2_param IS NOT NULL THEN ''::text
                    ELSE '   split2 is null or split2_param is not null   '::text
                END AS reason
           FROM $model.zq_split_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.pk0_km IS NOT NULL THEN ''::text
                    ELSE '   pk0_km is not null   '::text
                END ||
                CASE
                    WHEN new_1.dx IS NOT NULL THEN ''::text
                    ELSE '   dx is not null   '::text
                END) ||
                CASE
                    WHEN new_1.dx >= 0.1::double precision THEN ''::text
                    ELSE '   dx>=0.1   '::text
                END AS reason
           FROM $model.pipe_branch_marker_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END ||
                CASE
                    WHEN new_1.k IS NOT NULL THEN ''::text
                    ELSE '   k is not null   '::text
                END) ||
                CASE
                    WHEN new_1.k > 0::double precision THEN ''::text
                    ELSE '   k>0   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END AS reason
           FROM $model.strickler_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.cascade_mode IS NOT NULL THEN ''::text
                    ELSE '   cascade_mode is not null   '::text
                END ||
                CASE
                    WHEN new_1.cascade_mode = 'hydrograph'::hydra_model_connect_mode OR new_1.zq_array IS NOT NULL OR new_1.tz_array IS NOT NULL THEN ''::text
                    ELSE '   cascade_mode=''hydrograph'' or zq_array is not null or tz_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) <= 20 THEN ''::text
                    ELSE '   array_length(zq_array, 1)<=20   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zq_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.tz_array, 1) <= 20 THEN ''::text
                    ELSE '   array_length(tz_array, 1)<=20   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.tz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.tz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(tz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.quality, 1) = 9 THEN ''::text
                    ELSE '   array_length(quality, 1)=9   '::text
                END AS reason
           FROM $model.model_connect_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.d_abutment_l IS NOT NULL THEN ''::text
                    ELSE '   d_abutment_l is not null   '::text
                END ||
                CASE
                    WHEN new_1.d_abutment_r IS NOT NULL THEN ''::text
                    ELSE '   d_abutment_r is not null   '::text
                END) ||
                CASE
                    WHEN new_1.abutment_type IS NOT NULL THEN ''::text
                    ELSE '   abutment_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zw_array IS NOT NULL THEN ''::text
                    ELSE '   zw_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.bradley_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.qz_array IS NOT NULL THEN ''::text
                    ELSE '   qz_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(qz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.hydraulic_cut_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.drainage IS NOT NULL THEN ''::text
                    ELSE '   drainage is not null   '::text
                END ||
                CASE
                    WHEN new_1.overflow IS NOT NULL THEN ''::text
                    ELSE '   overflow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ini IS NOT NULL THEN ''::text
                    ELSE '   z_ini is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zr_sr_qf_qs_array IS NOT NULL THEN ''::text
                    ELSE '   zr_sr_qf_qs_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.treatment_mode IS NOT NULL THEN ''::text
                    ELSE '   treatment_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.treatment_param IS NOT NULL THEN ''::text
                    ELSE '   treatment_param is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 2) = 4 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 2)=4   '::text
                END AS reason
           FROM $model.reservoir_rsp_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN new_1.storage_area IS NOT NULL THEN ''::text
                    ELSE '   storage_area is not null   '::text
                END ||
                CASE
                    WHEN new_1.storage_area >= 0::double precision THEN ''::text
                    ELSE '   storage_area>=0   '::text
                END) ||
                CASE
                    WHEN new_1.constant_dry_flow IS NOT NULL THEN ''::text
                    ELSE '   constant_dry_flow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.constant_dry_flow >= 0::double precision THEN ''::text
                    ELSE '   constant_dry_flow>=0   '::text
                END) ||
                CASE
                    WHEN new_1.distrib_coef IS NULL OR new_1.distrib_coef >= 0::double precision THEN ''::text
                    ELSE '   distrib_coef is null or distrib_coef>=0   '::text
                END) ||
                CASE
                    WHEN new_1.lag_time_hr IS NULL OR new_1.lag_time_hr >= 0::double precision THEN ''::text
                    ELSE '   lag_time_hr is null or (lag_time_hr>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.sector IS NULL OR new_1.distrib_coef IS NOT NULL AND new_1.lag_time_hr IS NOT NULL THEN ''::text
                    ELSE '   sector is null or (distrib_coef is not null and lag_time_hr is not null)   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.pollution_dryweather_runoff, 1) = 2 THEN ''::text
                    ELSE '   array_length(pollution_dryweather_runoff, 1)=2   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.pollution_dryweather_runoff, 2) = 4 THEN ''::text
                    ELSE '   array_length(pollution_dryweather_runoff, 2)=4   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.quality_dryweather_runoff, 1) = 2 THEN ''::text
                    ELSE '   array_length(quality_dryweather_runoff, 1)=2   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.quality_dryweather_runoff, 2) = 9 THEN ''::text
                    ELSE '   array_length(quality_dryweather_runoff, 2)=9   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR new_1.tq_array IS NOT NULL THEN ''::text
                    ELSE '   external_file_data or tq_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 1) <= 10 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 1) >= 1 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 2) = 2 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 2)=2   '::text
                END AS reason
           FROM $model.hydrograph_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null    '::text
                END ||
                CASE
                    WHEN new_1.z_regul IS NOT NULL THEN ''::text
                    ELSE '   z_regul is not null    '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul IS NOT NULL THEN ''::text
                    ELSE '   mode_regul is not null   '::text
                END) ||
                CASE
                    WHEN new_1.reoxy_law IS NOT NULL THEN ''::text
                    ELSE '   reoxy_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.reoxy_param IS NOT NULL THEN ''::text
                    ELSE '   reoxy_param is not null   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.zregul_weir_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((
                CASE
                    WHEN new_1.l_road IS NOT NULL THEN ''::text
                    ELSE '   l_road is not null   '::text
                END ||
                CASE
                    WHEN new_1.l_road >= 0::double precision THEN ''::text
                    ELSE '   l_road>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_road IS NOT NULL THEN ''::text
                    ELSE '   z_road is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zw_array IS NOT NULL THEN ''::text
                    ELSE '   zw_array is not null    '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.bridge_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.qq_array IS NOT NULL THEN ''::text
                    ELSE '   qq_array is not null   '::text
                END ||
                CASE
                    WHEN new_1.downstream IS NOT NULL THEN ''::text
                    ELSE '   downstream is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1 IS NOT NULL THEN ''::text
                    ELSE '   split1 is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qq_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NOT NULL AND array_length(new_1.qq_array, 2) = 3 OR new_1.split2 IS NULL AND array_length(new_1.qq_array, 2) = 2 THEN ''::text
                    ELSE '   (split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2)   '::text
                END AS reason
           FROM $model.qq_split_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling > new_1.z_invert THEN ''::text
                    ELSE '   z_ceiling>z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc <=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc >=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_valve IS NOT NULL THEN ''::text
                    ELSE '   mode_valve is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate IS NOT NULL THEN ''::text
                    ELSE '   z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate >= new_1.z_invert THEN ''::text
                    ELSE '   z_gate>=z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate <= new_1.z_ceiling THEN ''::text
                    ELSE '   z_gate<=z_ceiling   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END) ||
                CASE
                    WHEN new_1.cc_submerged > 0::double precision THEN ''::text
                    ELSE '   cc_submerged >0   '::text
                END AS reason
           FROM $model.gate_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.drainage IS NOT NULL THEN ''::text
                    ELSE '   drainage is not null   '::text
                END ||
                CASE
                    WHEN new_1.overflow IS NOT NULL THEN ''::text
                    ELSE '   overflow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q_drainage IS NOT NULL THEN ''::text
                    ELSE '   q_drainage is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q_drainage >= 0::double precision THEN ''::text
                    ELSE '   q_drainage>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_ini IS NOT NULL THEN ''::text
                    ELSE '   z_ini is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.treatment_mode IS NOT NULL THEN ''::text
                    ELSE '   treatment_mode is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END AS reason
           FROM $model.reservoir_rs_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((
                CASE
                    WHEN new_1.q_dz_array IS NOT NULL THEN ''::text
                    ELSE '   q_dz_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(q_dz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(q_dz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(q_dz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.q_dz_array[1][1] = 0::double precision THEN ''::text
                    ELSE '   q_dz_array[1][1]=0   '::text
                END) ||
                CASE
                    WHEN new_1.q_dz_array[1][2] = 0::double precision THEN ''::text
                    ELSE '   q_dz_array[1][2]=0   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.param_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((
                CASE
                    WHEN new_1.external_file_data OR new_1.tz_array IS NOT NULL THEN ''::text
                    ELSE '   external_file_data or tz_array is not null   '::text
                END ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 1) <= 10 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 1) >= 1 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 2) = 2 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 2)=2   '::text
                END AS reason
           FROM $model.tz_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width >=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc <=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc >=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert_stop IS NOT NULL THEN ''::text
                    ELSE '   z_invert_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling_stop IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.dt_regul_hr IS NOT NULL THEN ''::text
                    ELSE '   dt_regul_hr is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_control_node IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_pid_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_tz_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR new_1.q_z_crit IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul OR new_1.nr_z_gate IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc_submerged > 0::double precision THEN ''::text
                    ELSE '   cc_submerged >0   '::text
                END AS reason
           FROM $model.regul_sluice_gate_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END ||
                CASE
                    WHEN new_1.zini IS NOT NULL THEN ''::text
                    ELSE '   zini is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.treatment_mode IS NOT NULL THEN ''::text
                    ELSE '   treatment_mode is not null   '::text
                END AS reason
           FROM $model.tank_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.law_type IS NOT NULL THEN ''::text
                    ELSE '   law_type is not null   '::text
                END ||
                CASE
                    WHEN new_1.param IS NOT NULL THEN ''::text
                    ELSE '   param is not null   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.borda_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
                CASE
                    WHEN new_1.q0 IS NOT NULL THEN ''::text
                    ELSE '   q0 is not null   '::text
                END AS reason
           FROM $model.constant_inflow_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.z_weir IS NOT NULL THEN ''::text
                    ELSE '   z_weir is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= '0'::numeric::double precision THEN ''::text
                    ELSE '   cc>=0.   '::text
                END AS reason
           FROM $model.weir_bc_singularity new_1
          WHERE NOT new_1.validity
        )select CONCAT('node:', n.id) as
        id,
        validity,
        name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom,
        reason from $model._node as n join node_invalidity_reason as r on r.id=n.id
        where validity=FALSE
    union
    select CONCAT('link:', l.id) as
        id,
        validity,
        name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom,
        reason from $model._link as l join link_invalidity_reason as r on  r.id=l.id
        where validity=FALSE
    union
    select CONCAT('singularity:', s.id) as
        id,
        s.validity,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        reason from $model._singularity as s join $model._node as n on s.node = n.id join singularity_invalidity_reason as r on r.id=s.id
        where s.validity=FALSE
    union
    select CONCAT('profile:', p.id) as
        id,
        p.validity,
        p.name,
        'profile'::varchar as type,
        ''::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        ''::varchar as reason from $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where p.validity=FALSE
;;