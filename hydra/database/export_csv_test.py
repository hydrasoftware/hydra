# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


'''
uploads a test project (MAGES), updates it, then tests the SQL functions to export to DB

USAGE

   export_csv_test.py [-h] [-k -p <projet_name> -f <foldername>]

OPTIONS

   -h, --help
        print this help

   -k, --keep
        does not delete test database after test

   -p, --project
        export specific project

   -f, --folder
        export to specific folder
'''

assert __name__ == "__main__"

import sys
import os
import re
import json
import string
import getopt
import tempfile
import psycopg2
from hydra.versions.update import update_project
from hydra.versions.dump_restore import create_db, run_psql
from hydra.database import database as dbhydra
from hydra.database.export_csv import ExportCsv
from hydra.project import Project
from hydra.utility.empty_ui_manager import EmptyUIManager
from hydra.utility.log import LogManager, ConsoleLogger
from hydra.utility.settings_properties import SettingsProperties

__currendir = os.path.dirname(__file__)
with open(os.path.join(__currendir, 'csv_properties.json')) as f:
    __csv_columns = json.load(f)
__tempdir = tempfile.gettempdir()

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hkp:f:",
            ["help", "keep", "project=", "folder="])
except Exception as e:
    sys.stderr.write("export_csv_test.py [-h] [-k -p <projet_name> -f <foldername>]\n")
    exit(1)

keep=False
folder_name = __tempdir
project_name = None

for opt, arg in optlist:
    if opt in ("-h", "--help"):
        help(sys.modules[__name__])
        exit(0)
    elif opt in ("-k", "--keep"):
        keep=True
    elif opt in ("-p", "--project"):
        project_name = arg
        if not dbhydra.project_exists(project_name):
            Project.create_new_project(project_name, 2154)
    elif opt in ("-f", "--folder"):
        folder_name = arg

log = LogManager(ConsoleLogger(), "Hydra")

if project_name is None:
    if dbhydra.project_exists('export_csv_test'):
        dbhydra.remove_project('export_csv_test')
    create_db(log, 'export_csv_test')

    current_dir = os.path.abspath(os.path.dirname(__file__))
    test_data = os.path.join(current_dir, "test_data", 'export_csv_test'+".sql")
    run_psql(log, test_data, 'export_csv_test', strict=True)

    dbhydra.insert_project_metadata('export_csv_test', 2154)
    project_name = 'export_csv_test'

test_project = Project(project_name, log, EmptyUIManager())
update_project(test_project)

exporter = ExportCsv(test_project)
exporter.write_csv_to_folder(folder_name)

if not keep and dbhydra.project_exists('export_csv_test'):
    dbhydra.remove_project('export_csv_test')
    log.notice("Deleting database {}...".format('export_csv_test'))

sys.stdout.write("ok\n")

