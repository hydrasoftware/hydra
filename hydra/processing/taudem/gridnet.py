# -*- coding: utf-8 -*-

"""
***************************************************************************
    gridnet.py
    ---------------------
    Date                 : June 2012
    Copyright            : (C) 2012-2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'June 2012'
__copyright__ = '(C) 2012-2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.core import (QgsVectorFileWriter,
                       QgsProcessing,
                       QgsProcessingException,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterRasterDestination
                      )

from .taudemAlgorithm import TauDemAlgorithm
from . import taudemUtils


class GridNet(TauDemAlgorithm):

    D8_FLOWDIR = 'D8_FLOWDIR'
    MASK_GRID = 'MASK_GRID'
    THRESHOLD = 'THRESHOLD'
    OUTLETS = 'OUTLETS'
    LONGEST_PATH = 'LONGEST_PATH'
    TOTAL_PATH = 'TOTAL_PATH'
    STRAHLER_ORDER = 'STRAHLER_ORDER'

    def name(self):
        return 'gridnet'

    def displayName(self):
        return self.tr('Grid Network')

    def group(self):
        return self.tr('Basic grid analysis')

    def groupId(self):
        return 'basicanalysis'

    def tags(self):
        return self.tr('dem,hydrology,strahler,order,path').split(',')

    def shortHelpString(self):
        return self.tr('Creates 3 grids that contain for each grid cell: '
                       '1) the longest upslope path length, 2) the total '
                       'upslope path length, and 3) the Strahler order number.')

    def helpUrl(self):
        return 'http://hydrology.usu.edu/taudem/taudem5/help53/GridNetwork.html'

    def __init__(self):
        super().__init__()

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterRasterLayer(self.D8_FLOWDIR,
                                                            self.tr('D9 flow directions')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.MASK_GRID,
                                                            self.tr('Mask grid'),
                                                            optional=True))
        self.addParameter(QgsProcessingParameterNumber(self.THRESHOLD,
                                                       self.tr('Mask threshold'),
                                                       QgsProcessingParameterNumber.Double,
                                                       defaultValue=100.0,
                                                       optional=True))
        self.addParameter(QgsProcessingParameterFeatureSource(self.OUTLETS,
                                                              self.tr('Outlets'),
                                                              types=[QgsProcessing.TypeVectorPoint],
                                                              optional=True))

        self.addParameter(QgsProcessingParameterRasterDestination(self.LONGEST_PATH,
                                                                  self.tr('Longest upslope length')))
        self.addParameter(QgsProcessingParameterRasterDestination(self.TOTAL_PATH,
                                                                  self.tr('Total upslope length')))
        self.addParameter(QgsProcessingParameterRasterDestination(self.STRAHLER_ORDER,
                                                                  self.tr('Strahler network order')))

    def processAlgorithm(self, parameters, context, feedback):
        d8Layer = self.parameterAsRasterLayer(parameters, self.D8_FLOWDIR, context)
        if d8Layer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.D8_FLOWDIR))

        arguments = []
        arguments.append(self.command())

        arguments.append('-p')
        arguments.append(d8Layer.source())

        if self.MASK_GRID in parameters and parameters[self.MASK_GRID] is not None:
            mask = self.parameterAsRasterLayer(parameters, self.MASK_GRID, context)
            arguments.append('-mask')
            arguments.append(mask.source())
            arguments.append('-thresh')
            arguments.append('{}'.format(self.parameterAsDouble(parameters, self.THRESHOLD, context)))

        if self.OUTLETS in parameters and parameters[self.OUTLETS] is not None:
            outlets = self.parameterAsCompatibleSourceLayerPath(parameters, self.OUTLETS, context,
                                                                QgsVectorFileWriter.supportedFormatExtensions(),
                                                                feedback=feedback)
            arguments.append('-o')
            arguments.append(outlets)

        arguments.append('-plen')
        arguments.append(self.parameterAsOutputLayer(parameters, self.LONGEST_PATH, context))

        arguments.append('-tlen')
        arguments.append(self.parameterAsOutputLayer(parameters, self.TOTAL_PATH, context))

        arguments.append('-gord')
        arguments.append(self.parameterAsOutputLayer(parameters, self.STRAHLER_ORDER, context))

        taudemUtils.execute(arguments, feedback)

        return self.algorithmResults(parameters)
