# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_singularity_dialog import SimpleSingularityDialog, tr
import hydra.utility.string as string
from ..widgets.graph_widget import GraphWidget
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QTableWidgetItem
import os

class TzBcEditor(SimpleSingularityDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleSingularityDialog.__init__(self, project, geom, "tz_bc_singularity", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "tz_bc.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        name, cyclic, tz_array, external_file_data, comment = self.project.execute("""
            select name, cyclic, tz_array, external_file_data, comment
            from {}.tz_bc_singularity
            where id={}""".format(self.model.name, self.id)).fetchone()
        self.name.setText(name)
        self.notes.setText(comment)

        if cyclic :
            self.checkBox_cyclic.setChecked(True)
        else :
            self.checkBox_cyclic.setChecked(False)

        self.graph = GraphWidget(self.graph_placeholder)

        if not tz_array is None:
            self.set_table_items(tz_array)
            self.__draw_graph()

        self.table.cellChanged.connect(self.__cell_changed)
        self.table.currentCellChanged.connect(self.__draw_graph)
        combo_value = 'Yes' if external_file_data else 'No'
        self.combo_external_file_data.setCurrentIndex(self.combo_external_file_data.findText(combo_value))
        self.combo_external_file_data.activated.connect(self.__external_data_changed)

        self.__external_data_changed()

    def __external_data_changed(self):
        self.table.setEnabled(True)
        if self.combo_external_file_data.currentText() == 'Yes':
            self.table.setEnabled(False)

    def __draw_graph(self):
        datas = self.get_table_items()
        if len(datas)>0:
            self.graph.clear()
            self.graph.add_line(
                [r[0] for r in datas],
                [r[1] for r in datas],
                "r",
                self.name.text(),
                tr("t (h)"), tr("Z (m)"))
        else:
            self.graph.canvas.setVisible(False)

    def __cell_changed(self,row,column):
        if row == (self.table.rowCount()-1):
            self.table.setRowCount(self.table.rowCount() + 1)

    def set_table_items(self, tq_array):
        n = len(tq_array)
        self.table.setRowCount(n+1)
        for row in range(0, n):
            self.table.setItem(row,0,QTableWidgetItem(string.get_str(tq_array[row][0])))
            self.table.setItem(row,1,QTableWidgetItem(string.get_str(tq_array[row][1])))

    def get_table_items(self):
        n = self.table.rowCount()
        result = list()
        index_list=0
        for i in range(0, n):
            if not self.table.item(i,0) == None and not self.table.item(i,1) == None:
                result.append(list())
                m = self.table.columnCount()
                result[index_list].append(string.get_sql_float(self.table.item(i,0).text()))
                result[index_list].append(string.get_sql_float(self.table.item(i,1).text()))
                index_list = index_list + 1
        return result

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))

        bool = True if self.combo_external_file_data.currentText() == 'Yes' else False

        self.model.update_tz_bc_singularity(self.id, self.geom, self.get_table_items(),  self.checkBox_cyclic.isChecked(), bool, self.name.text(), comment=self.notes.text())
        SimpleSingularityDialog.save(self)
        self.close()

