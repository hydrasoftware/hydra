/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update hydra.metadata set version = '3.3';
;;

create or replace function project.model_instead_fct()
returns trigger
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_model
    for statement in create_model(TD['new']['name'], $srid, '$version'):
        plpy.execute(statement)
$$$$
;;

do
$$$$
    begin
    if exists (
        select 1 from pg_catalog.pg_namespace where nspname = 'csv'
    )
    then
      execute 'insert into csv.hydra_compatible_versions(hydra_version) values (''3.3'')';
    end if;
end
$$$$;
;;

create or replace function project.check_source(source varchar)
returns bool
language plpython3u immutable
as
$$$$
    try:
        from osgeo import gdal
    except ImportError:
        import gdal

    src_ds = gdal.Open(source)
    if src_ds is None:
        return False
    else:
        return True
$$$$
;;

