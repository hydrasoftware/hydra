from builtins import object
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from datetime import datetime, date, timedelta

class Line(object):
    ''' class which handle a line in the graph'''
    def __init__(self, levels, values, name ,type = None, table = None, color='k', date0=None):

        self.__levels = levels
        self.__values = values
        self.__date0 = date0
        self.name = name
        self.type = type
        self.table = table
        self.color = color

    def get_values(self):
        return self.__values

    def get_levels(self, as_date=False):
        return self.__levels

    def get_dates(self):
        return [self.__date0 + timedelta(seconds=3600*v) for v in self.__levels]
