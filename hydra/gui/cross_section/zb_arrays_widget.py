
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from qgis.PyQt.QtWidgets import QWidget, QHBoxLayout
from qgis.PyQt.QtCore import pyqtSignal
from ..widgets.array_widget import ArrayWidget

class ZbArraysWidget(QWidget):
    arrays_changed = pyqtSignal(list)

    def __init__(self, parent=None):
        super().__init__(parent)
        QHBoxLayout(self)
        l = QWidget()
        m = QWidget()
        r = QWidget()
        self.layout().addWidget(l)
        self.layout().addWidget(m)
        self.layout().addWidget(r)
        self.zbmaj_lbank_array = ArrayWidget(["z[m]", "b [m]"], [8,2], [], parent=l)
        self.zbmin_array = ArrayWidget(["z[m]", "b [m]"], [6,2], [], parent=m)
        self.zbmaj_rbank_array = ArrayWidget(["z[m]", "b [m]"], [8,2], [], parent=r)

        self.zbmaj_lbank_array.data_edited.connect(self.__arrays_changed)
        self.zbmin_array.data_edited.connect(self.__arrays_changed)
        self.zbmaj_rbank_array.data_edited.connect(self.__arrays_changed)

    def set_arrays(self, l, m, r):
        self.zbmaj_lbank_array.set_table_items(l)
        self.zbmin_array.set_table_items(m)
        self.zbmaj_rbank_array.set_table_items(r)

    def __arrays_changed(self, data):
        self.arrays_changed.emit([
            self.zbmaj_lbank_array.get_table_items(),
            self.zbmin_array.get_table_items(),
            self.zbmaj_rbank_array.get_table_items()
            ])

