# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..base_dialog import BaseDialog, tr
import hydra.utility.string as string
from qgis.PyQt import uic
import os

class RiverNodeEditor(BaseDialog):
    def __init__(self, project, geom, id=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "river_node.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.__model = self.project.get_current_model()
        assert not self.__model == None
        self.__geom = geom
        if id==None:
            self.__id = self.__model.add_river_node(self.__geom)
        else:
            self.__id = id
            if self.__geom is None:
                geom, = self.project.execute("""
                select geom
                from {}.river_node
                where id={}""".format(self.__model.name, self.__id)).fetchone()

        name, area, z_ground, pk_km, z_invert, comment = self.project.execute("""
            select name, area, z_ground, pk_km, {m}.river_node_z_invert(geom), comment
            from {m}.river_node
            where id={i}""".format(m=self.__model.name, i=self.__id)).fetchone()
        self.name.setText(name)
        self.area.setText(string.get_str(area))
        self.z_ground.setText(string.get_str(z_ground))
        self.notes.setText(comment)

        if self.get_reach() is not None:
            # init pk info
            self.pk_km.setText(str(round(pk_km,3)))

            # init Z invert info
            if z_invert:
                self.z_invert.setText(str(round(z_invert,3)))

            title = self.windowTitle()
            self.setWindowTitle(title + " - " + str(self.get_reach()) + ": " + str(round(pk_km, 3)) + " km")
        else:
            title = self.windowTitle()
            self.setWindowTitle(title + " - No reach found")

    def get_id(self):
        return self.__id

    def get_reach(self):
        reach = self.project.execute("""
            with idreach as
            (select reach from {model}.river_node where id={id})
            select name
            from {model}.reach, idreach
            where id=idreach.reach""".format(model=self.__model.name, id=self.__id)).fetchone()
        return reach[0] if reach is not None else None

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.__model.update_river_node(
                self.__id,
                self.__geom,
                string.get_sql_float(self.z_ground.text()),
                string.get_sql_float(self.area.text()),
                self.name.text(),
                comment=self.notes.text())

        BaseDialog.save(self)
        self.close()
