from builtins import str
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from qgis.PyQt.QtWidgets import QDialog
from qgis.PyQt.QtCore import QCoreApplication
from hydra.gui.base_dialog import BaseDialog
from hydra.utility.string import isfloat

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

class SimpleSingularityDialog(BaseDialog):
    """Inherits base dialog, used for singularities to get control node and reach info if needed"""
    def __init__(self, project, geom, table, id=None, parent=None):
        BaseDialog.__init__(self, parent)
        self.project = project
        self.model = self.project.get_current_model()
        self.tbl = table
        assert not self.model == None
        self.geom = geom
        if id is None:
            self.id = self.model.add_simple_singularity(table ,self.geom)
        else:
            self.id = id

        self.geom,= self.project.execute("""
            select geom
            from {}.{}
            where id={}""".format(self.model.name, table, self.id)).fetchone()

    def get_info(self):
        node_name, node_type = self.__get_node_info()

        self.__info = None
        if node_type == "river":
            node_reach, node_pk = self.__get_reach_info(node_name)
            if isfloat(node_pk):
                self.__info = tr(" - ") + str(node_name) + tr(" (") + str(node_reach) + tr(": ") + str(round(node_pk, 3) ) + tr(" km)")
            else:
                self.__info = tr(" - ") + str(node_name) + tr(" (") + str(node_reach) + tr(")")
        else:
            self.__info = tr(" - ") + str(node_name)
        return self.__info

    def __get_node_info(self):
        if self.tbl == 'river_cross_section_profile':
            name, type = self.project.execute("""
                                select name, node_type
                                from {model_name}._node
                                where {model_name}._node.id={id_node}
                                """.format(model_name=self.model.name,id_node=self.id)).fetchone()
        else:
            name, type = self.project.execute("""
                                with id_node_res as(
                                select node
                                from {model_name}._singularity
                                where id={id_node} limit 1)
                                select name, node_type
                                from {model_name}._node, id_node_res
                                where {model_name}._node.id=id_node_res.node
                                """.format(model_name=self.model.name,id_node=self.id)).fetchone()
        return name, type

    def __get_reach_info(self, name):
        reach_info = self.project.execute("""
                                select {model}.reach.name, {model}.river_node.pk_km
                                from {model}.river_node, {model}.reach
                                where {model}.river_node.name='{node_name}'
                                and {model}.river_node.reach = {model}.reach.id""".format(model=self.model.name, node_name=name)).fetchone()
        reach, pk = reach_info if reach_info is not None else ["No reach found", "-"]
        return reach, pk


