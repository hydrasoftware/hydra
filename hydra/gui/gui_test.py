# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run gui tests

USAGE
    python -m hydra.gui.gui_test [-h, -v, -e test]

OPTIONS
    -h
        print this help

    -k, --keep
        keep database after test

    -d, --debug
        well, you know...

    -e test1,tes2
        exclude test1 and test2
"""
from __future__ import print_function
from builtins import str


import subprocess
import getopt
import sys
import os
import re
from subprocess import Popen, PIPE
from tempfile import gettempdir
from time import time
from multiprocessing.pool import ThreadPool
from hydra.database.database import TestProject, project_exists

def list_tests(excludes, regex):
    "return module names for tests"
    tests = []
    hydra_gui_dir = os.path.abspath(os.path.dirname(__file__))
    top_dir = os.path.dirname(hydra_gui_dir)
    for root, dirs, files in os.walk(hydra_gui_dir):
        for file_ in files:
            if re.match(r".*_guitest.py$", file_):
                # remove the trailing '.py' (3 characters)
                # replace \ or / by dots
                test = '.'.join(
                            os.path.abspath(
                                os.path.join(root, file_)
                            ).replace(hydra_gui_dir, "hydra.gui").split(os.sep))[:-3]
                if test not in excludes and (regex is None or re.match(regex, test)):
                    tests.append(test)
    return [test for test in tests if not test in excluded_tests]

def run(test):
    start = time()
    process = subprocess.Popen([sys.executable, "-m", test], stderr=PIPE, stdout=PIPE)
    out, err = process.communicate()
    code = process.returncode
    if code != 0:
        print(out.decode('utf8'))
        return code, err.decode('utf8'), out
    return  code, " ran in %.2f sec"%(time() - start), out


try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hve:x:k",
            ["help"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

excluded_tests = optlist['-e'].split(',') if '-e' in optlist else []
keep = "-k" in optlist or "--keep" in optlist
debug = "-d" in optlist or "--debug" in optlist

start = time()


if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")


excludes = optlist['-e'].split(',') if '-e' in optlist else []
regex = optlist['-x'] if '-x' in optlist else None
tests = list_tests(excludes, regex)
project_name = "gui_test"
test_project = TestProject(project_name, with_model=True, keep=keep, debug=debug)

for i, test in enumerate(tests):
    # fix_print_with_import
    print("% 3d/%d %s"%(i+1, len(tests), test), end=' ')
    rt, msg, out = run(test)
    if "-v" in optlist or "--verbose" in optlist:
        print(out.decode('utf8'))

    if rt != 0:
        raise Exception(msg)

    print(msg)

print("gui tests run in %d sec"%(int(time() - start)))
exit(0)
