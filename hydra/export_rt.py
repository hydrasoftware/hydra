from .kernel import find_exe
from .project import Project
from .database.export_calcul import ExportCalcul
from .database.export_csv import ExportCsv
from pathlib import Path
import shutil
from .versions.dump_restore import dump_project
import fiona
import json
import subprocess

CMD_TEMPLATE=r"""*time
{hydra_start_str}
     '{hydra_end_str}'
       '{hydra_start_str}'     '{hydra_end_str}'      {dt_hydrol_hr}

*hydrol
    __KEEP__
    {dt_hydrol}
    __KEEP__
    __KEEP__

*hydrau
      {hydra_warmup_duration}
    __KEEP__
    __KEEP__

*rstart
{hydra_warmup}
{start_scenario}
'{hydra_start_str}'

*save
{save}
'{hydra_save_date_str}'

"""

def export(project_name, flavor=None):

    if flavor is None:
        if project_name.find('gao') != -1:
            flavor = 'gao'
        elif project_name.find('hydreaulys') != -1:
            flavor = 'hydreaulys'
        else:
            raise RuntimeError("cannot determine flavor from project name")


    project = Project(project_name)
    project_dir = Path(project.directory)
    export_dir = project_dir / 'REF'
    models = project.get_models_from_scenario('REF')
    
    if export_dir.exists():
        shutil.rmtree(export_dir)
    
    # export modèle et création répertoire
    exporter = ExportCalcul(project)
    exporter.export('REF')

    # lancement hydragen et nettoyage .txt
    hydragen = find_exe('hydragen')
    subprocess.run(hydragen, cwd= export_dir / 'travail')
    for txt in (export_dir / 'hydraulique').glob('*.txt'):
        #print('rm', txt)
        txt.unlink()

    # export sql
    dump_project(project.log, str(export_dir / (project.name + '.sql')), project.name)

    # copie du répertoire config
    shutil.copytree(project_dir / 'config', export_dir / 'config')

    if flavor == 'hydreaulys':
        # creation hydrogrammes null
        with open(export_dir / 'hydrol' / 'hydrogrammes_nuls.dat', 'w') as hydrg_null:
            for model in models:
                for hy, in project.fetchall(f"""
                        select name
                        from {model}.hydrograph_bc_singularity
                        where external_file_data"""):
                    hydrg_null.write(f"'{hy.lower()}'\n0. 0.\n1. 0.\n\n")

        # creation des coeeficients de thiessen
        gages = [name for name, in project.fetchall("""
                select name from project.rain_gage order by name
                """)]
        for model in models:
            thiessen_coefficients = project.fetchall(f"""
                    select c.name ||';'|| string_agg(
                        coalesce((ST_Area(ST_Intersection(c.geom, rg.geom)) / ST_Area(c.geom))::numeric(5,3), 0.000)::varchar, ';' order by rg.name)
                    from {model}.catchment as c
                    cross join project.rain_gage g
                    left join {model}.thiessen_rain_gage as rg on rg.id=g.id and ST_intersects(c.geom, rg.geom)
                    group by c.name
                    order by c.name
                    """)

            with open(export_dir / 'hydrol' / (model+'_thiessen.csv'), 'w') as thiess:
                thiess.write('CATCHMENT;'+';'.join(gages)+'\n'+
                    '\n'.join([c for c, in thiessen_coefficients])+'\n')

        # creation de PRADP
        with open(export_dir / 'travail' / 'PRADP.dat.in', 'w') as pradp:
            pradp.write("PRADP\n{mesures_pluvio}\n{previ_pluvio}\n")

        # creation des templates
        with open(export_dir / 'scenario.nom') as scn, open(export_dir / 'scenario.nom.in', 'w') as scn_in:
            lines = scn.readlines()
            assert lines[0].strip() == '*scenario'
            assert lines[1].strip() == 'REF'
            lines[1] = '{name}\n'
            lines = lines[:3] + ['*scnref\n', 'REF\n', '\n'] + lines[3:]
            scn_in.write(''.join(lines))
        
        # dans le .cmd on récupère certaines lignes
        with open(export_dir / 'travail' / 'REF.cmd') as cmd, open(export_dir / 'travail' / 'REF.cmd.in', 'w') as cmd_in:
            lines = cmd.readlines()
            assert lines[5].strip() == '*hydrol'
            assert lines[11].strip() == '*hydrau'
            lines_out = CMD_TEMPLATE.split('\n')
            for i in range(len(lines_out)):
                if lines_out[i].strip() == '__KEEP__':
                    lines_out[i] = lines[i].rstrip()
            cmd_in.write('\n'.join(lines_out))

        # dans le fichier optsor on remplace les chemin vers config en chemins vers REF/config et les chemin vers project_dir/REF en chemin relatif
        with open(export_dir / 'travail' / 'REF_optsor.dat') as optsor, open(export_dir / 'travail' / 'REF_optsor.dat.in', 'w') as optsor_in:
            for line in optsor:
                # ATTTENTION à l'ordre des substitutions
                line = line.replace('\\', '/')
                line = line.replace('../../config', '../../REF/config')
                line = line.replace('../travail', '../../REF/travail')
                line = line.replace(str(project_dir / 'config' ), str('../../REF/config'))
                line = line.replace(str(project_dir), str('../..'))
                optsor_in.write(line)


    elif flavor == 'gao':

        # export csv
        exporter = ExportCsv(project)
        export_csv_dir = export_dir / 'export_csv'
        export_csv_dir.mkdir()
        exporter.write_csv_to_folder( export_csv_dir)

        # export exe
        export_exe_dir = export_dir / 'exe'
        export_exe_dir.mkdir()
        for exe in ('hydrol.exe', 'whydram24_dos.exe'):
            shutil.copy(Path(__file__).parent / 'kernel' / exe, export_exe_dir / exe)

        # contours_bv_shp
        export_contours_bv_shp_dir = export_dir / 'export_contours_bv_shp'
        export_contours_bv_shp_dir.mkdir()
        schema = {'geometry': 'Polygon', 'properties': {
            'id': 'int',
            'name': 'str:24'
            }}
        for model in project.models:
            with fiona.open(export_contours_bv_shp_dir / (model + '.shp'), mode='w', driver='ESRI Shapefile', schema=schema, crs=f'EPSG:{project.srid}') as shp:
                for id, name, geom in project.fetchall(f"select id, name, ST_AsGeoJSON(geom) from {model}.catchment"):
                    shp.write({"geometry": json.loads(geom), 'properties': {
                        'id': id,
                        'name': name}})
    else:
        raise RuntimeError(f'invalid flavor {flavor} (not in nydreaulys, gao)')


if __name__=='__main__':
    import sys
    project_name = sys.argv[1]
    flavor = sys.argv[2] if len(sys.argv) > 2 else None
    export(project_name, flavor)



