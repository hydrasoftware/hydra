<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyAlgorithm="0" labelsEnabled="0" styleCategories="AllStyleCategories" maxScale="0" simplifyDrawingHints="0" simplifyLocal="1" version="3.16.5-Hannover" minScale="20000" simplifyMaxScale="1" hasScaleBasedVisibilityFlag="0" simplifyDrawingTol="1" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal endField="" startField="" enabled="0" durationUnit="min" accumulate="0" durationField="" fixedDuration="0" startExpression="" endExpression="" mode="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 type="singleSymbol" enableorderby="0" symbollevels="0" forceraster="0">
    <symbols>
      <symbol name="0" type="marker" force_rhr="0" alpha="1" clip_to_extent="1">
        <layer enabled="1" class="SimpleMarker" locked="0" pass="0">
          <prop k="angle" v="180"/>
          <prop k="color" v="64,130,255,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="triangle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="0,0,0,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,1,0.1,1,2"/>
          <prop k="outline_width_unit" v="MapUnit"/>
          <prop k="scale_method" v="area"/>
          <prop k="size" v="2"/>
          <prop k="size_map_unit_scale" v="3x:0,0,1,1,1,2"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Pie" attributeLegend="1">
    <DiagramCategory rotationOffset="270" spacing="0" maxScaleDenominator="1e+08" enabled="0" scaleDependency="Area" showAxis="0" scaleBasedVisibility="0" backgroundAlpha="255" penColor="#000000" opacity="1" height="15" minScaleDenominator="1000" labelPlacementMethod="XHeight" lineSizeScale="3x:0,0,0,0,0,0" sizeType="MM" spacingUnit="MM" penAlpha="255" diagramOrientation="Up" minimumSize="0" direction="1" lineSizeType="MM" width="15" spacingUnitScale="3x:0,0,0,0,0,0" barWidth="5" backgroundColor="#ffffff" penWidth="0" sizeScale="3x:0,0,0,0,0,0">
      <fontProperties description="Arial,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute field="" label="" color="#000000"/>
      <axisSymbol>
        <symbol name="" type="line" force_rhr="0" alpha="1" clip_to_extent="1">
          <layer enabled="1" class="SimpleLine" locked="0" pass="0">
            <prop k="align_dash_pattern" v="0"/>
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="dash_pattern_offset" v="0"/>
            <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="dash_pattern_offset_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="tweak_dash_pattern_on_corners" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings priority="0" showAll="1" dist="0" placement="0" obstacle="0" linePlacementFlags="2" zIndex="0">
    <properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties" type="Map">
          <Option name="show" type="Map">
            <Option name="active" type="bool" value="true"/>
            <Option name="field" type="QString" value="id"/>
            <Option name="type" type="int" value="2"/>
          </Option>
        </Option>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="id" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="QString" value="0"/>
            <Option name="UseHtml" type="QString" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="name" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="QString" value="0"/>
            <Option name="UseHtml" type="QString" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="node" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="QString" value="0"/>
            <Option name="UseHtml" type="QString" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="storage_area" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="QString" value="0"/>
            <Option name="UseHtml" type="QString" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="tq_array" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="QString" value="0"/>
            <Option name="UseHtml" type="QString" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="constant_dry_flow" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="QString" value="0"/>
            <Option name="UseHtml" type="QString" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="distrib_coef" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="QString" value="0"/>
            <Option name="UseHtml" type="QString" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="lag_time_hr" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="QString" value="0"/>
            <Option name="UseHtml" type="QString" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="sector" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="QString" value="0"/>
            <Option name="UseHtml" type="QString" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="hourly_modulation" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="QString" value="0"/>
            <Option name="UseHtml" type="QString" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pollution_dryweather_runoff" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="QString" value="0"/>
            <Option name="UseHtml" type="QString" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="quality_dryweather_runoff" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="QString" value="0"/>
            <Option name="UseHtml" type="QString" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="external_file_data" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="QString" value="0"/>
            <Option name="UseHtml" type="QString" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="configuration" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="QString" value="0"/>
            <Option name="UseHtml" type="QString" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="validity" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="QString" value="0"/>
            <Option name="UseHtml" type="QString" value="0"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="configuration_json" configurationFlags="None">
      <editWidget type="KeyValue">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" index="0" field="id"/>
    <alias name="" index="1" field="name"/>
    <alias name="" index="2" field="node"/>
    <alias name="" index="3" field="storage_area"/>
    <alias name="" index="4" field="tq_array"/>
    <alias name="" index="5" field="constant_dry_flow"/>
    <alias name="" index="6" field="distrib_coef"/>
    <alias name="" index="7" field="lag_time_hr"/>
    <alias name="" index="8" field="sector"/>
    <alias name="" index="9" field="hourly_modulation"/>
    <alias name="" index="10" field="pollution_dryweather_runoff"/>
    <alias name="" index="11" field="quality_dryweather_runoff"/>
    <alias name="" index="12" field="external_file_data"/>
    <alias name="" index="13" field="configuration"/>
    <alias name="" index="14" field="validity"/>
    <alias name="" index="15" field="configuration_json"/>
  </aliases>
  <defaults>
    <default expression="" field="id" applyOnUpdate="0"/>
    <default expression="" field="name" applyOnUpdate="0"/>
    <default expression="" field="node" applyOnUpdate="0"/>
    <default expression="" field="storage_area" applyOnUpdate="0"/>
    <default expression="" field="tq_array" applyOnUpdate="0"/>
    <default expression="" field="constant_dry_flow" applyOnUpdate="0"/>
    <default expression="" field="distrib_coef" applyOnUpdate="0"/>
    <default expression="" field="lag_time_hr" applyOnUpdate="0"/>
    <default expression="" field="sector" applyOnUpdate="0"/>
    <default expression="" field="hourly_modulation" applyOnUpdate="0"/>
    <default expression="" field="pollution_dryweather_runoff" applyOnUpdate="0"/>
    <default expression="" field="quality_dryweather_runoff" applyOnUpdate="0"/>
    <default expression="" field="external_file_data" applyOnUpdate="0"/>
    <default expression="" field="configuration" applyOnUpdate="0"/>
    <default expression="" field="validity" applyOnUpdate="0"/>
    <default expression="" field="configuration_json" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" notnull_strength="1" field="id" constraints="3" unique_strength="1"/>
    <constraint exp_strength="0" notnull_strength="0" field="name" constraints="0" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" field="node" constraints="0" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" field="storage_area" constraints="0" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" field="tq_array" constraints="0" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" field="constant_dry_flow" constraints="0" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" field="distrib_coef" constraints="0" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" field="lag_time_hr" constraints="0" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" field="sector" constraints="0" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" field="hourly_modulation" constraints="0" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" field="pollution_dryweather_runoff" constraints="0" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" field="quality_dryweather_runoff" constraints="0" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" field="external_file_data" constraints="0" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" field="configuration" constraints="0" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" field="validity" constraints="0" unique_strength="0"/>
    <constraint exp_strength="0" notnull_strength="0" field="configuration_json" constraints="0" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" desc="" exp=""/>
    <constraint field="name" desc="" exp=""/>
    <constraint field="node" desc="" exp=""/>
    <constraint field="storage_area" desc="" exp=""/>
    <constraint field="tq_array" desc="" exp=""/>
    <constraint field="constant_dry_flow" desc="" exp=""/>
    <constraint field="distrib_coef" desc="" exp=""/>
    <constraint field="lag_time_hr" desc="" exp=""/>
    <constraint field="sector" desc="" exp=""/>
    <constraint field="hourly_modulation" desc="" exp=""/>
    <constraint field="pollution_dryweather_runoff" desc="" exp=""/>
    <constraint field="quality_dryweather_runoff" desc="" exp=""/>
    <constraint field="external_file_data" desc="" exp=""/>
    <constraint field="configuration" desc="" exp=""/>
    <constraint field="validity" desc="" exp=""/>
    <constraint field="configuration_json" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortExpression="" sortOrder="0">
    <columns>
      <column name="id" type="field" width="-1" hidden="0"/>
      <column name="name" type="field" width="-1" hidden="0"/>
      <column name="node" type="field" width="-1" hidden="0"/>
      <column name="storage_area" type="field" width="-1" hidden="0"/>
      <column name="tq_array" type="field" width="-1" hidden="0"/>
      <column name="constant_dry_flow" type="field" width="-1" hidden="0"/>
      <column name="distrib_coef" type="field" width="-1" hidden="0"/>
      <column name="lag_time_hr" type="field" width="-1" hidden="0"/>
      <column name="sector" type="field" width="-1" hidden="0"/>
      <column name="hourly_modulation" type="field" width="-1" hidden="0"/>
      <column name="external_file_data" type="field" width="-1" hidden="0"/>
      <column name="configuration" type="field" width="-1" hidden="0"/>
      <column name="validity" type="field" width="-1" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
      <column name="pollution_dryweather_runoff" type="field" width="-1" hidden="0"/>
      <column name="quality_dryweather_runoff" type="field" width="-1" hidden="0"/>
      <column name="configuration_json" type="field" width="-1" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>C:/OSGEO4~1/bin</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ
"Fonction d'initialisation Python"
Voici un exemple à suivre:
"""
from PyQt4.QtGui import QWidget

def my_form_open(dialog, layer, feature):
⇥geom = feature.geometry()
⇥control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="configuration" editable="1"/>
    <field name="configuration_json" editable="1"/>
    <field name="constant_dry_flow" editable="1"/>
    <field name="distrib_coef" editable="1"/>
    <field name="external_file_data" editable="1"/>
    <field name="hourly_modulation" editable="1"/>
    <field name="id" editable="1"/>
    <field name="lag_time_hr" editable="1"/>
    <field name="name" editable="1"/>
    <field name="node" editable="1"/>
    <field name="pollution_dryweather_runoff" editable="1"/>
    <field name="quality_dryweather_runoff" editable="1"/>
    <field name="sector" editable="1"/>
    <field name="storage_area" editable="1"/>
    <field name="tq_array" editable="1"/>
    <field name="validity" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="configuration" labelOnTop="0"/>
    <field name="configuration_json" labelOnTop="0"/>
    <field name="constant_dry_flow" labelOnTop="0"/>
    <field name="distrib_coef" labelOnTop="0"/>
    <field name="external_file_data" labelOnTop="0"/>
    <field name="hourly_modulation" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="lag_time_hr" labelOnTop="0"/>
    <field name="name" labelOnTop="0"/>
    <field name="node" labelOnTop="0"/>
    <field name="pollution_dryweather_runoff" labelOnTop="0"/>
    <field name="quality_dryweather_runoff" labelOnTop="0"/>
    <field name="sector" labelOnTop="0"/>
    <field name="storage_area" labelOnTop="0"/>
    <field name="tq_array" labelOnTop="0"/>
    <field name="validity" labelOnTop="0"/>
  </labelOnTop>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"name"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
