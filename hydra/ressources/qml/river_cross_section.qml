<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingTol="1" simplifyLocal="1" version="3.10.11-A Coruña" maxScale="0" labelsEnabled="0" readOnly="0" minScale="1e+08" simplifyMaxScale="1" hasScaleBasedVisibilityFlag="0" simplifyDrawingHints="0" styleCategories="AllStyleCategories" simplifyAlgorithm="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 type="singleSymbol" symbollevels="0" enableorderby="0" forceraster="0">
    <symbols>
      <symbol name="0" type="marker" alpha="1" force_rhr="0" clip_to_extent="1">
        <layer locked="0" pass="0" enabled="1" class="EllipseMarker">
          <prop v="0" k="angle"/>
          <prop v="255,0,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="255,0,0,255" k="outline_color"/>
          <prop v="no" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="10" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="10" k="symbol_height"/>
          <prop v="3x:0,0,1,2.5,1,10" k="symbol_height_map_unit_scale"/>
          <prop v="MapUnit" k="symbol_height_unit"/>
          <prop v="rectangle" k="symbol_name"/>
          <prop v="0.8" k="symbol_width"/>
          <prop v="3x:0,0,0,0,0,0" k="symbol_width_map_unit_scale"/>
          <prop v="MM" k="symbol_width_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="SimpleMarker">
          <prop v="90" k="angle"/>
          <prop v="255,0,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="line" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,1,0.2,1,2" k="size_map_unit_scale"/>
          <prop v="MapUnit" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Pie">
    <DiagramCategory maxScaleDenominator="1e+08" height="15" width="15" diagramOrientation="Up" rotationOffset="270" scaleBasedVisibility="0" lineSizeScale="3x:0,0,0,0,0,0" barWidth="5" sizeScale="3x:0,0,0,0,0,0" opacity="1" enabled="0" labelPlacementMethod="XHeight" backgroundAlpha="255" lineSizeType="MM" sizeType="MM" backgroundColor="#ffffff" penWidth="0" minScaleDenominator="0" penAlpha="255" minimumSize="0" scaleDependency="Area" penColor="#000000">
      <fontProperties description="Arial,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute label="" color="#000000" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings dist="0" zIndex="0" placement="0" priority="0" linePlacementFlags="2" obstacle="0" showAll="1">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties" type="Map">
          <Option name="show" type="Map">
            <Option value="true" name="active" type="bool"/>
            <Option value="id" name="field" type="QString"/>
            <Option value="2" name="type" type="int"/>
          </Option>
        </Option>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="name">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="z_invert_up">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="z_invert_down">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="type_cross_section_up">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="type_cross_section_down">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="up_rk">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="up_rk_maj">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="up_sinuosity">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="up_circular_diameter">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="up_ovoid_height">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="up_ovoid_top_diameter">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="up_ovoid_invert_diameter">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="up_cp_geom">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="up_op_geom">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="up_vcs_geom">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="up_vcs_topo_geom">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="down_rk">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="down_rk_maj">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="down_sinuosity">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="down_circular_diameter">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="down_ovoid_height">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="down_ovoid_top_diameter">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="down_ovoid_invert_diameter">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="down_cp_geom">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="down_op_geom">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="down_vcs_geom">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="down_vcs_topo_geom">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="configuration">
      <editWidget type="KeyValue">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="z_tn_up">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="z_tn_down">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="z_lbank_up">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="z_lbank_down">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="z_rbank_up">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="z_rbank_down">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="z_ceiling_up">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="z_ceiling_down">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="validity">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" index="0" field="id"/>
    <alias name="" index="1" field="name"/>
    <alias name="" index="2" field="z_invert_up"/>
    <alias name="" index="3" field="z_invert_down"/>
    <alias name="" index="4" field="type_cross_section_up"/>
    <alias name="" index="5" field="type_cross_section_down"/>
    <alias name="" index="6" field="up_rk"/>
    <alias name="" index="7" field="up_rk_maj"/>
    <alias name="" index="8" field="up_sinuosity"/>
    <alias name="" index="9" field="up_circular_diameter"/>
    <alias name="" index="10" field="up_ovoid_height"/>
    <alias name="" index="11" field="up_ovoid_top_diameter"/>
    <alias name="" index="12" field="up_ovoid_invert_diameter"/>
    <alias name="" index="13" field="up_cp_geom"/>
    <alias name="" index="14" field="up_op_geom"/>
    <alias name="" index="15" field="up_vcs_geom"/>
    <alias name="" index="16" field="up_vcs_topo_geom"/>
    <alias name="" index="17" field="down_rk"/>
    <alias name="" index="18" field="down_rk_maj"/>
    <alias name="" index="19" field="down_sinuosity"/>
    <alias name="" index="20" field="down_circular_diameter"/>
    <alias name="" index="21" field="down_ovoid_height"/>
    <alias name="" index="22" field="down_ovoid_top_diameter"/>
    <alias name="" index="23" field="down_ovoid_invert_diameter"/>
    <alias name="" index="24" field="down_cp_geom"/>
    <alias name="" index="25" field="down_op_geom"/>
    <alias name="" index="26" field="down_vcs_geom"/>
    <alias name="" index="27" field="down_vcs_topo_geom"/>
    <alias name="" index="28" field="configuration"/>
    <alias name="" index="29" field="z_tn_up"/>
    <alias name="" index="30" field="z_tn_down"/>
    <alias name="" index="31" field="z_lbank_up"/>
    <alias name="" index="32" field="z_lbank_down"/>
    <alias name="" index="33" field="z_rbank_up"/>
    <alias name="" index="34" field="z_rbank_down"/>
    <alias name="" index="35" field="z_ceiling_up"/>
    <alias name="" index="36" field="z_ceiling_down"/>
    <alias name="" index="37" field="validity"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="id" applyOnUpdate="0"/>
    <default expression="" field="name" applyOnUpdate="0"/>
    <default expression="" field="z_invert_up" applyOnUpdate="0"/>
    <default expression="" field="z_invert_down" applyOnUpdate="0"/>
    <default expression="" field="type_cross_section_up" applyOnUpdate="0"/>
    <default expression="" field="type_cross_section_down" applyOnUpdate="0"/>
    <default expression="" field="up_rk" applyOnUpdate="0"/>
    <default expression="" field="up_rk_maj" applyOnUpdate="0"/>
    <default expression="" field="up_sinuosity" applyOnUpdate="0"/>
    <default expression="" field="up_circular_diameter" applyOnUpdate="0"/>
    <default expression="" field="up_ovoid_height" applyOnUpdate="0"/>
    <default expression="" field="up_ovoid_top_diameter" applyOnUpdate="0"/>
    <default expression="" field="up_ovoid_invert_diameter" applyOnUpdate="0"/>
    <default expression="" field="up_cp_geom" applyOnUpdate="0"/>
    <default expression="" field="up_op_geom" applyOnUpdate="0"/>
    <default expression="" field="up_vcs_geom" applyOnUpdate="0"/>
    <default expression="" field="up_vcs_topo_geom" applyOnUpdate="0"/>
    <default expression="" field="down_rk" applyOnUpdate="0"/>
    <default expression="" field="down_rk_maj" applyOnUpdate="0"/>
    <default expression="" field="down_sinuosity" applyOnUpdate="0"/>
    <default expression="" field="down_circular_diameter" applyOnUpdate="0"/>
    <default expression="" field="down_ovoid_height" applyOnUpdate="0"/>
    <default expression="" field="down_ovoid_top_diameter" applyOnUpdate="0"/>
    <default expression="" field="down_ovoid_invert_diameter" applyOnUpdate="0"/>
    <default expression="" field="down_cp_geom" applyOnUpdate="0"/>
    <default expression="" field="down_op_geom" applyOnUpdate="0"/>
    <default expression="" field="down_vcs_geom" applyOnUpdate="0"/>
    <default expression="" field="down_vcs_topo_geom" applyOnUpdate="0"/>
    <default expression="" field="configuration" applyOnUpdate="0"/>
    <default expression="" field="z_tn_up" applyOnUpdate="0"/>
    <default expression="" field="z_tn_down" applyOnUpdate="0"/>
    <default expression="" field="z_lbank_up" applyOnUpdate="0"/>
    <default expression="" field="z_lbank_down" applyOnUpdate="0"/>
    <default expression="" field="z_rbank_up" applyOnUpdate="0"/>
    <default expression="" field="z_rbank_down" applyOnUpdate="0"/>
    <default expression="" field="z_ceiling_up" applyOnUpdate="0"/>
    <default expression="" field="z_ceiling_down" applyOnUpdate="0"/>
    <default expression="" field="validity" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" constraints="3" exp_strength="0" field="id" notnull_strength="1"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="name" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="z_invert_up" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="z_invert_down" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="type_cross_section_up" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="type_cross_section_down" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="up_rk" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="up_rk_maj" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="up_sinuosity" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="up_circular_diameter" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="up_ovoid_height" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="up_ovoid_top_diameter" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="up_ovoid_invert_diameter" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="up_cp_geom" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="up_op_geom" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="up_vcs_geom" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="up_vcs_topo_geom" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="down_rk" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="down_rk_maj" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="down_sinuosity" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="down_circular_diameter" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="down_ovoid_height" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="down_ovoid_top_diameter" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="down_ovoid_invert_diameter" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="down_cp_geom" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="down_op_geom" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="down_vcs_geom" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="down_vcs_topo_geom" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="configuration" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="z_tn_up" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="z_tn_down" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="z_lbank_up" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="z_lbank_down" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="z_rbank_up" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="z_rbank_down" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="z_ceiling_up" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="z_ceiling_down" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="validity" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" desc="" field="id"/>
    <constraint exp="" desc="" field="name"/>
    <constraint exp="" desc="" field="z_invert_up"/>
    <constraint exp="" desc="" field="z_invert_down"/>
    <constraint exp="" desc="" field="type_cross_section_up"/>
    <constraint exp="" desc="" field="type_cross_section_down"/>
    <constraint exp="" desc="" field="up_rk"/>
    <constraint exp="" desc="" field="up_rk_maj"/>
    <constraint exp="" desc="" field="up_sinuosity"/>
    <constraint exp="" desc="" field="up_circular_diameter"/>
    <constraint exp="" desc="" field="up_ovoid_height"/>
    <constraint exp="" desc="" field="up_ovoid_top_diameter"/>
    <constraint exp="" desc="" field="up_ovoid_invert_diameter"/>
    <constraint exp="" desc="" field="up_cp_geom"/>
    <constraint exp="" desc="" field="up_op_geom"/>
    <constraint exp="" desc="" field="up_vcs_geom"/>
    <constraint exp="" desc="" field="up_vcs_topo_geom"/>
    <constraint exp="" desc="" field="down_rk"/>
    <constraint exp="" desc="" field="down_rk_maj"/>
    <constraint exp="" desc="" field="down_sinuosity"/>
    <constraint exp="" desc="" field="down_circular_diameter"/>
    <constraint exp="" desc="" field="down_ovoid_height"/>
    <constraint exp="" desc="" field="down_ovoid_top_diameter"/>
    <constraint exp="" desc="" field="down_ovoid_invert_diameter"/>
    <constraint exp="" desc="" field="down_cp_geom"/>
    <constraint exp="" desc="" field="down_op_geom"/>
    <constraint exp="" desc="" field="down_vcs_geom"/>
    <constraint exp="" desc="" field="down_vcs_topo_geom"/>
    <constraint exp="" desc="" field="configuration"/>
    <constraint exp="" desc="" field="z_tn_up"/>
    <constraint exp="" desc="" field="z_tn_down"/>
    <constraint exp="" desc="" field="z_lbank_up"/>
    <constraint exp="" desc="" field="z_lbank_down"/>
    <constraint exp="" desc="" field="z_rbank_up"/>
    <constraint exp="" desc="" field="z_rbank_down"/>
    <constraint exp="" desc="" field="z_ceiling_up"/>
    <constraint exp="" desc="" field="z_ceiling_down"/>
    <constraint exp="" desc="" field="validity"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column name="id" type="field" width="-1" hidden="0"/>
      <column name="name" type="field" width="-1" hidden="0"/>
      <column name="z_invert_up" type="field" width="-1" hidden="0"/>
      <column name="z_invert_down" type="field" width="-1" hidden="0"/>
      <column name="type_cross_section_up" type="field" width="-1" hidden="0"/>
      <column name="type_cross_section_down" type="field" width="-1" hidden="0"/>
      <column name="up_rk" type="field" width="-1" hidden="0"/>
      <column name="up_rk_maj" type="field" width="-1" hidden="0"/>
      <column name="up_sinuosity" type="field" width="-1" hidden="0"/>
      <column name="up_circular_diameter" type="field" width="-1" hidden="0"/>
      <column name="up_ovoid_height" type="field" width="-1" hidden="0"/>
      <column name="up_ovoid_top_diameter" type="field" width="-1" hidden="0"/>
      <column name="up_ovoid_invert_diameter" type="field" width="-1" hidden="0"/>
      <column name="up_cp_geom" type="field" width="-1" hidden="0"/>
      <column name="up_op_geom" type="field" width="-1" hidden="0"/>
      <column name="up_vcs_geom" type="field" width="-1" hidden="0"/>
      <column name="up_vcs_topo_geom" type="field" width="-1" hidden="0"/>
      <column name="down_rk" type="field" width="-1" hidden="0"/>
      <column name="down_rk_maj" type="field" width="-1" hidden="0"/>
      <column name="down_sinuosity" type="field" width="-1" hidden="0"/>
      <column name="down_circular_diameter" type="field" width="-1" hidden="0"/>
      <column name="down_ovoid_height" type="field" width="-1" hidden="0"/>
      <column name="down_ovoid_top_diameter" type="field" width="-1" hidden="0"/>
      <column name="down_ovoid_invert_diameter" type="field" width="-1" hidden="0"/>
      <column name="down_cp_geom" type="field" width="-1" hidden="0"/>
      <column name="down_op_geom" type="field" width="-1" hidden="0"/>
      <column name="down_vcs_geom" type="field" width="-1" hidden="0"/>
      <column name="down_vcs_topo_geom" type="field" width="-1" hidden="0"/>
      <column name="z_tn_up" type="field" width="-1" hidden="0"/>
      <column name="z_tn_down" type="field" width="-1" hidden="0"/>
      <column name="z_lbank_up" type="field" width="-1" hidden="0"/>
      <column name="z_lbank_down" type="field" width="-1" hidden="0"/>
      <column name="z_rbank_up" type="field" width="-1" hidden="0"/>
      <column name="z_rbank_down" type="field" width="-1" hidden="0"/>
      <column name="z_ceiling_up" type="field" width="-1" hidden="0"/>
      <column name="z_ceiling_down" type="field" width="-1" hidden="0"/>
      <column name="validity" type="field" width="-1" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
      <column name="configuration" type="field" width="-1" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>C:/Travail/46152_AUDIT_OF_RIVERS/Mission/Travail/OSGEO4~1/bin</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ
"Fonction d'initialisation Python"
Voici un exemple à suivre:
"""
from PyQt4.QtGui import QWidget

def my_form_open(dialog, layer, feature):
⇥geom = feature.geometry()
⇥control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="configuration" editable="1"/>
    <field name="down_circular_diameter" editable="1"/>
    <field name="down_cp_geom" editable="1"/>
    <field name="down_op_geom" editable="1"/>
    <field name="down_ovoid_height" editable="1"/>
    <field name="down_ovoid_invert_diameter" editable="1"/>
    <field name="down_ovoid_top_diameter" editable="1"/>
    <field name="down_rk" editable="1"/>
    <field name="down_rk_maj" editable="1"/>
    <field name="down_sinuosity" editable="1"/>
    <field name="down_vcs_geom" editable="1"/>
    <field name="down_vcs_topo_geom" editable="1"/>
    <field name="id" editable="1"/>
    <field name="name" editable="1"/>
    <field name="type_cross_section_down" editable="1"/>
    <field name="type_cross_section_up" editable="1"/>
    <field name="up_circular_diameter" editable="1"/>
    <field name="up_cp_geom" editable="1"/>
    <field name="up_op_geom" editable="1"/>
    <field name="up_ovoid_height" editable="1"/>
    <field name="up_ovoid_invert_diameter" editable="1"/>
    <field name="up_ovoid_top_diameter" editable="1"/>
    <field name="up_rk" editable="1"/>
    <field name="up_rk_maj" editable="1"/>
    <field name="up_sinuosity" editable="1"/>
    <field name="up_vcs_geom" editable="1"/>
    <field name="up_vcs_topo_geom" editable="1"/>
    <field name="validity" editable="1"/>
    <field name="z_ceiling_down" editable="1"/>
    <field name="z_ceiling_up" editable="1"/>
    <field name="z_invert_down" editable="1"/>
    <field name="z_invert_up" editable="1"/>
    <field name="z_lbank_down" editable="1"/>
    <field name="z_lbank_up" editable="1"/>
    <field name="z_rbank_down" editable="1"/>
    <field name="z_rbank_up" editable="1"/>
    <field name="z_tn_down" editable="1"/>
    <field name="z_tn_up" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="configuration" labelOnTop="0"/>
    <field name="down_circular_diameter" labelOnTop="0"/>
    <field name="down_cp_geom" labelOnTop="0"/>
    <field name="down_op_geom" labelOnTop="0"/>
    <field name="down_ovoid_height" labelOnTop="0"/>
    <field name="down_ovoid_invert_diameter" labelOnTop="0"/>
    <field name="down_ovoid_top_diameter" labelOnTop="0"/>
    <field name="down_rk" labelOnTop="0"/>
    <field name="down_rk_maj" labelOnTop="0"/>
    <field name="down_sinuosity" labelOnTop="0"/>
    <field name="down_vcs_geom" labelOnTop="0"/>
    <field name="down_vcs_topo_geom" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="name" labelOnTop="0"/>
    <field name="type_cross_section_down" labelOnTop="0"/>
    <field name="type_cross_section_up" labelOnTop="0"/>
    <field name="up_circular_diameter" labelOnTop="0"/>
    <field name="up_cp_geom" labelOnTop="0"/>
    <field name="up_op_geom" labelOnTop="0"/>
    <field name="up_ovoid_height" labelOnTop="0"/>
    <field name="up_ovoid_invert_diameter" labelOnTop="0"/>
    <field name="up_ovoid_top_diameter" labelOnTop="0"/>
    <field name="up_rk" labelOnTop="0"/>
    <field name="up_rk_maj" labelOnTop="0"/>
    <field name="up_sinuosity" labelOnTop="0"/>
    <field name="up_vcs_geom" labelOnTop="0"/>
    <field name="up_vcs_topo_geom" labelOnTop="0"/>
    <field name="validity" labelOnTop="0"/>
    <field name="z_ceiling_down" labelOnTop="0"/>
    <field name="z_ceiling_up" labelOnTop="0"/>
    <field name="z_invert_down" labelOnTop="0"/>
    <field name="z_invert_up" labelOnTop="0"/>
    <field name="z_lbank_down" labelOnTop="0"/>
    <field name="z_lbank_up" labelOnTop="0"/>
    <field name="z_rbank_down" labelOnTop="0"/>
    <field name="z_rbank_up" labelOnTop="0"/>
    <field name="z_tn_down" labelOnTop="0"/>
    <field name="z_tn_up" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>"name"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
