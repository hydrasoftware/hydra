# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import json
import numpy
import os
from functools import partial
from qgis.PyQt.QtCore import QCoreApplication, Qt
from qgis.PyQt.QtWidgets import QDialog, QTreeWidgetItem, QApplication, QComboBox, QMenu, QMessageBox, QHBoxLayout, QWidget, QMainWindow, QVBoxLayout, QFileDialog, QLabel, QTableWidget, QPushButton, QStatusBar, QInputDialog, QTableWidgetItem, QAbstractItemView
from qgis.PyQt.QtGui import QCursor, QImage
from hydra.gui.widgets.combo_with_values import ComboWithValues
from hydra.utility.result_decoder import HydraResult
from hydra.utility.tables_properties import TablesProperties

with open(os.path.join(os.path.dirname(__file__), "visu.json")) as j:
    elements = json.load(j)

files_dir = {
    "w15":"hydraulique",
    "w14":"hydraulique",
    "w13":"hydrol"
}

titles = {
    "qam":'Flow(m3/s)',
    "zam":'Level(m)'
}

# setting for GUI table
rowsize = 27
columnsize = 85
paddingtop = 30
paddingleft = 45

class TableauVisu(QTableWidget) :

        def __init__(self,scn_name,name_item):
            QTableWidget.__init__(self)
            self.scn_name = scn_name
            self.name_item = name_item

        def __copy(self):

            copied_text = ""
            copied_text = self.scn_name + ":" + self.name_item + "\t"
            for k in range (self.columnCount()) :
                temp = self.horizontalHeaderItem(k)
                copied_text = copied_text + temp.text() + "\t"
            copied_text = copied_text[:-1] + "\n"

            for i in range (0,self.rowCount()) :
                copied_text += self.verticalHeaderItem(i).text() + "\t"
                for j in range (0,self.columnCount()) :
                    try:
                        copied_text = copied_text + str(self.item(i,j).text()) + "\t"
                    except AttributeError:
                        # quand une case n'a jamais été initialisée
                        copied_text += "\t"
                copied_text = copied_text[:-1] + "\n"  # le [:-1] élimine le '\t' en trop
            copied_text = copied_text[:-1]  # le [:-1] élimine le '\n' en trop
            # enregistrement dans le clipboard
            QApplication.clipboard().setText(copied_text)

        def keyPressEvent(self, event):
            if self.hasFocus():
                if event.key() == Qt.Key_C and  (event.modifiers() & Qt.ControlModifier):
                    self.__copy()
                    event.accept()
                else:
                    event.ignore()
            else:
                event.ignore()


class VisuTable(QMainWindow) :
    def __init__(self, current_project, name_item, table_item, parent=None):
        QMainWindow.__init__(self, parent)
        self.__project = current_project
        ext = elements[table_item]["file"]
        scn_name = self.__project.get_current_scenario()[1]
        scn_path = self.__project.get_senario_path_from_name(scn_name)
        subdir = files_dir[elements[table_item]["file"]]
        result_path = os.path.join(scn_path,subdir, scn_name.upper()+ "_" + str(self.__project.get_current_model().name).upper() + "." + ext)
        result_file = HydraResult(result_path)
        self.setWindowTitle( str(name_item) + " , " + str(scn_name))
        self.__result_table = result_file[str(name_item)][:]
        tableauresult = []

        for i in range (len(elements[table_item]["results"])) :
            values = [item[1][i] for item in self.__result_table]
            if str(elements[table_item]["results"][i]) is not None :
                row = [str(elements[table_item]["results"][i]),self.split_value(max(values)),self.split_value(min(values))]
                tableauresult.append(row)

        self.create_labels(tableauresult,scn_name,name_item)
        self.setFixedSize(columnsize*self.TableResult.columnCount()+paddingleft,rowsize*self.TableResult.rowCount()+paddingtop)
        self.show()

    @staticmethod
    def split_value(value) :
        return f"{value:16.4f}"

    def create_labels (self,tableauresult,scn_name,name_item) :
        """ fonction pour afficher et remplir une instance de TableauVisu (QtableWidget modifie) afin de visualiser les valeurs """
        self.TableResult = TableauVisu(scn_name,name_item)
        self.setCentralWidget(self.TableResult)
        self.TableResult.setColumnCount(len(tableauresult))
        self.TableResult.setRowCount(len(tableauresult[0])-1)
        self.TableResult.setEditTriggers(QAbstractItemView.NoEditTriggers)

        for i in range (len(tableauresult)) :
            self.TableResult.setRowHeight(i,rowsize)
            for j in range(1, len(tableauresult[i])) :
                TempItem = QTableWidgetItem()
                TempItem.setText(tableauresult[i][j])
                TempItem.setTextAlignment(Qt.AlignRight)
                self.TableResult.setItem(j-1,i,TempItem)

        for j in range(0, len(tableauresult[i])+1) :
                self.TableResult.setColumnWidth(j,columnsize)

        HorizontalHeader = [i[0] for i in tableauresult]
        self.TableResult.setHorizontalHeaderLabels(HorizontalHeader)
        self.TableResult.setVerticalHeaderLabels(["Max", "Min"])
