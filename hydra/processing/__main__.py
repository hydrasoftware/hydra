from qgis.core import QgsApplication, QgsProcessingFeedback
import sys
import os

class Feedback(QgsProcessingFeedback):
    def __del__(self):
        sys.stdout.write(f"\r{100:6.2f}%\n")

    def __init__(self, logfeedback=True):
        super().__init__(logfeedback)

    def setProgress(self, percent):
        sys.stdout.write(f"\r{percent:6.2f}% ")

    def pushInfo(self, msg):
        sys.stdout.write(msg+'\n')

    def pushConsoleInfo(self, msg):
        sys.stdout.write(msg+'\n')

    def pushWarning(self, msg):
        sys.stderr.write(msg+'\n')

    def reportError(self, msg):
        sys.stderr.write(msg+'\n')

QgsApplication.setPrefixPath(os.environ['QGIS_PREFIX_PATH'], True)
qgs = QgsApplication([], False)
qgs.initQgis()

import processing
from .provider import HydraProvider
provider = HydraProvider()
QgsApplication.processingRegistry().addProvider(provider)

if len(sys.argv)>1 and sys.argv[1]=='-h':
    print('Usage: python -m hydra.processing hydra:algorithm_name param1=value1 param2=value2')
    print()
    print('\n'.join([f"{a.name()} ({', '.join([p.name() for p in a.parameterDefinitions()])}): \n    {a.shortHelpString()}\n" for a in provider.algorithms()]))
    exit(0)

param = dict([tuple(a.split('=')) for a in sys.argv[2:]])
processing.run(sys.argv[1], param, feedback=Feedback())


qgs.exitQgis()
