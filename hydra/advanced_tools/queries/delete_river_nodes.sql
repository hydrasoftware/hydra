/* ********************************************************************************** */
/* Delete nodes wich are not:                                                         */
/*     + Holding a singularity or a river cross section profile                       */
/*     + Connected to a link                                                          */
/* ********************************************************************************** */

delete from $model.river_node
where id not in (select node from $model._singularity)
  and id not in (select id from $model.river_cross_section_profile)
  and id not in (select up from $model._link where up is not null)
  and id not in (select down from $model._link  where down is not null)
;;
