# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_link_dialog import SimpleLinkDialog, tr
from qgis.PyQt import uic
import os

class AirDuctLinkEditor(SimpleLinkDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleLinkDialog.__init__(self, project, geom, "air_duct_link", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "air_duct_link.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        z_invert_up, z_invert_down, area, perimeter, \
            friction_coefficient, custom_length, comment = self.project.execute("""
            select z_invert_up, z_invert_down, area, perimeter,
            friction_coefficient, custom_length, comment
            from {}.air_duct_link
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.z_invert_up.setText(f"{z_invert_up:.3f}" if z_invert_up is not None else '')
        self.z_invert_down.setText(f"{z_invert_down:.3f}" if z_invert_up is not None else '')
        self.area.setText(f"{area:.2f}" if area is not None else '')
        self.perimeter.setText(f"{perimeter:.2f}" if perimeter is not None else '')
        self.friction_coefficient.setText(f"{friction_coefficient:.3f}" if friction_coefficient is not None else '')
        self.custom_length.setText(f"{custom_length:.2f}" if custom_length is not None else '')
        self.notes.setText(comment)

    def get_id(self):
        return self.id

    def save(self):
        float_or_none = lambda s: float(s) if s else None
        self.project.log.notice(tr("Saved"))
        self.model.update_air_duct_link(self.id, self.geom,
            float_or_none(self.z_invert_up.text()),
            float_or_none(self.z_invert_down.text()),
            float_or_none(self.area.text()),
            float_or_none(self.perimeter.text()),
            float_or_none(self.friction_coefficient.text()),
            float_or_none(self.custom_length.text()),
            comment=self.notes.text())
        SimpleLinkDialog.save(self)
        self.close()
