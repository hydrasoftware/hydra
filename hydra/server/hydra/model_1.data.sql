/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*              Schema model                      */
/* ********************************************** */

create schema $model
;;

create table $model.configuration(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('CFG_'),
    description varchar
)
;;

insert into $model.configuration(name) values('default')
;;

create table $model.metadata(
    id integer not null default 1 unique check (id=1),
    version varchar(24) not null default '$version',
    creation_date timestamp not null default current_date,
    trigger_coverage boolean not null default 't',
    trigger_branch boolean not null default 't',
    configuration integer not null references $model.configuration(id),
    is_switching boolean default 'f'
)
;;

create table $model.config_switch(
    id integer not null default 1 unique check (id=1),
    is_switching boolean default 'f'
)
;;

insert into $model.config_switch default values;
;;

insert into $model.metadata(version, configuration)
select pm.version, 1 from hydra.metadata as pm;
;;

create table $model.generation_step(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('GEN_STEP_'),
    description varchar
)
;;

/* ********************************************** */
/*         Abstract entities: NODES               */
/* ********************************************** */

create table $model._node(
    id serial primary key,
    node_type hydra_node_type not null,
       unique (id, node_type),
    name varchar(24) unique not null,
    geom geometry('POINTZ',$srid) unique not null check(ST_IsValid(geom)),
    generated integer references $model.generation_step(id) on delete cascade,
    configuration json,
    validity boolean,
    comment varchar
)
;;

create index ${model}_node_geom_idx on $model._node using gist(geom)
;;
create index ${model}_node_name_idx on $model._node(name)
;;
create index ${model}_node_generated_idx on $model._node(generated)
;;
create index ${model}_node_configuration_idx on $model._node((configuration is not null))
;;
create index ${model}_node_validity_idx on $model._node(validity)
;;



/* ********************************************** */
/*         Abstract entities: LINKS               */
/* ********************************************** */

create table $model._link(
    id serial primary key,
    link_type hydra_link_type not null,
       unique (id, link_type),
    name varchar(24) unique not null,
    generated integer references $model.generation_step(id) on delete cascade,
    up integer,
    up_type hydra_node_type,
        foreign key (up, up_type) references $model._node(id, node_type) on delete set null,
    down integer,
    down_type hydra_node_type,
        foreign key (down, down_type) references $model._node(id, node_type) on delete set null,
        check(down_type != $catchment_node_type),
        check(up is not null or up_type = $elem_2d_node_type),
        check(down is not null or down_type = $elem_2d_node_type),
    geom geometry('LINESTRINGZ',$srid) not null check(ST_IsValid(geom)),
    configuration json,
    validity boolean,
    comment varchar
)
;;

create index ${model}_link_geom_idx on $model._link using gist(geom)
;;
create index ${model}_link_name_idx on $model._link(name)
;;
create index ${model}_link_generated_idx on $model._link(generated)
;;
create index ${model}_link_up_idx on $model._link(up)
;;
create index ${model}_link_down_idx on $model._link(down)
;;
create index ${model}_link_configuration_idx on $model._link((configuration is not null))
;;
create index ${model}_link_validity_idx on $model._link(validity)
;;

/* ********************************************** */
/*       Abstract entities: SINGULARITIES         */
/* ********************************************** */

create table $model._singularity(
    id serial primary key,
    singularity_type hydra_singularity_type not null,
        unique(id, singularity_type),
    name varchar(24) unique not null,
    node integer unique not null,
    node_type hydra_node_type not null,
        foreign key(node, node_type) references $model._node(id, node_type),
    configuration json,
    validity boolean,
    comment varchar
)
;;

create index ${model}_singularity_name_idx on $model._singularity(name)
;;
create index ${model}_singularity_configuration_idx on $model._singularity((configuration is not null))
;;
create index ${model}_singularity_validity_idx on $model._singularity(validity)
;;

