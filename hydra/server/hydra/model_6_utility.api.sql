/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */


/* ********************************************** */
/*              Street links_view                 */
/* ********************************************** */

create view $model.street_link as (
    with p as (select precision as prec from hydra.metadata),
         links as (
                select s.rk,
                    s.width,
                    s.width_invert,
                    ST_MakeLine(
                        lag(n.geom) over (partition by s.id order by ST_LineLocatePoint(s.geom, n.geom)),
                        n.geom
                        ) as geom
                from $model.street as s, $model.crossroad_node as n, p
                where ST_DWithin(s.geom, n.geom, p.prec)
                union all -- case of street loop
                select s.rk,
                    s.width,
                    s.width_invert,
                    ST_MakeLine(
                        l.geom,
                        f.geom
                        ) as geom
                from $model.street as s
                cross join p
                join $model.crossroad_node as f on ST_DWithin(st_startpoint(s.geom), f.geom, p.prec)
                cross join lateral (
                    select geom
                    from $model.crossroad_node as n
                    where ST_DWithin(s.geom, n.geom, p.prec) order by ST_LineLocatePoint(s.geom, n.geom) desc limit 1
                    ) l
                where st_isring(s.geom)
                )
    select
        (row_number() OVER ())::integer + mid.i AS id,
        'LRUE'::text || ((row_number() OVER ())::integer + mid.i)::character varying::text AS name,
        u.id as up,
        d.id as down,
        width,
        width_invert,
        ST_Length(l.geom)::real as length,
        rk,
        l.geom
    from links as l
    join $model.crossroad_node as u on ST_StartPoint(l.geom) = u.geom -- DO NOT USE ST_Equal here, it slows down the query to a crawl
    join $model.crossroad_node as d on ST_EndPoint(l.geom) = d.geom   --
    cross join ( SELECT max(_link.id) as i FROM $model._link) as mid
    where l.geom is not null and ST_IsValid(l.geom)
    order by id asc
    );
;;

/* ********************************************** */
/*          Configuration handling                */
/* ********************************************** */

create view $model.configured as
    select CONCAT('node:', n.id) as id,
        configuration,
        name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom
        from $model._node as n
        where configuration is not null
    union all
    select CONCAT('link:', l.id) as id,
        configuration,
        name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom
        from $model._link as l
        where configuration is not null
    union all
    select CONCAT('singularity:', s.id) as id,
        s.configuration,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom
        from $model._singularity as s join $model._node as n on s.node = n.id
        where s.configuration is not null
    union all
    select CONCAT('river_cross_section_profile:', p.id) as id,
        p.configuration,
        p.name,
        'profile'::varchar as type,
        'river_cross_section'::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom
        from $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where p.configuration is not null;
;;

create view $model.configured_current as
    with config as (select c.name from $model.configuration as c, $model.metadata as m where m.configuration=c.id)
    select CONCAT('node:', n.id) as id,
        configuration,
        n.name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom
        from $model._node as n, config as c
        where exists (select 1 from json_object_keys(n.configuration) where json_object_keys = c.name limit 1) and c.name != 'default'
    union all
    select CONCAT('link:', l.id) as id,
        configuration,
        l.name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom
        from $model._link as l, config as c
        where exists (select 1 from json_object_keys(l.configuration) where json_object_keys = c.name limit 1) and c.name != 'default'
    union all
    select CONCAT('singularity:', s.id) as id,
        s.configuration,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom
        from config as c, $model._singularity as s join $model._node as n on s.node = n.id
        where exists (select 1 from json_object_keys(s.configuration) where json_object_keys = c.name limit 1) and c.name != 'default'
    union all
    select CONCAT('river_cross_section_profile:', p.id) as id,
        p.configuration,
        p.name,
        'profile'::varchar as type,
        'river_cross_section'::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom
        from config as c, $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where exists (select 1 from json_object_keys(p.configuration) where json_object_keys = c.name limit 1) and c.name != 'default';
;;

/* ********************************************** */
/*              Regulated items                   */
/* ********************************************** */

create view $model.regulated as
    select distinct on (name)
        row_number() over () AS id,
        CONCAT(type, ':', r.id) as orig_id,
        r.type,
        r.subtype,
        r.name,
        r.geom
    from project.regulated() as r
    where model = '$model';
;;

create view $model.regulated_detailed as
    select
        row_number() over() as id,
        r.scenario,
        r.file,
        r.iline,
        CONCAT(type, ':', r.id) as orig_id,
        r.type,
        r.subtype,
        r.name,
        r.geom
    from project.regulated() as r
    where model = '$model';
;;

create view $model.regulated_current_scn as
    select distinct on (name)
        row_number() over () AS id,
        CONCAT(type, ':', r.id) as orig_id,
        r.type,
        r.subtype,
        r.name,
        r.geom
    from project.regulated() as r
    where model = '$model' and scenario = (select name from project.scenario where id = (select current_scenario from project.metadata));
;;

/* ********************************************** */
/*          Results and carto generation          */
/* ********************************************** */

create function ${model}.geom_dat(type_ varchar, id integer)
returns varchar
language plpython3u
as
$$$$
    import plpy
    from $hydra import geom_dat
    return geom_dat(plpy, '$model', type_, id)
$$$$
;;

create function ${model}.ctrz_dat()
returns varchar
language plpython3u
as
$$$$
    import plpy
    from $hydra import ctrz_dat
    return ctrz_dat(plpy, '$model')
$$$$
;;

create view ${model}.l2d
as
with link as (
    select l.name, project.crossed_border(l.geom, n.contour) as geom
    from ${model}._link as l, ${model}.elem_2d_node as n
    where (n.id = l.up or n.id=l.down)
    and l.link_type!='mesh_2d'
    and l.link_type!='network_overflow'
    and st_intersects(l.geom, st_exteriorring(n.contour)))
select name, st_x(st_startpoint(geom)) as xb, st_y(st_startpoint(geom)) as yb, st_x(st_endpoint(geom)) as xa, st_y(st_endpoint(geom)) as ya from link
;;

/* ********************************************** */
/*                  Reach types                   */
/* ********************************************** */

create view ${model}.open_reach
as with
    ends as (
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'valley' and p.type_cross_section_down != 'valley' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up != 'valley' and p.type_cross_section_down = 'valley' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'valley' and st_dwithin(p.geom, r.geom, .1) and ST_DWithin(p.geom, ST_EndPoint(r.geom), 1)
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_down = 'valley' and st_dwithin(p.geom, r.geom, .1) and ST_DWithin(p.geom, ST_StartPoint(r.geom), 1)
    )
    ,
    bounds as (
        select FIRST_VALUE(reach) over (partition by reach order by alpha), typ, LAG(alpha) over (partition by reach order by alpha) as start, alpha as end, id, reach from ends
    )
    select row_number() over() as id, st_linesubstring(r.geom, b.start, b.end) as geom, r.id as reach from bounds as b join ${model}.reach as r on r.id=b.reach where b.typ = 'down'
;;

create view ${model}.channel_reach
as with
    ends as (
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'channel' and p.type_cross_section_down != 'channel' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up != 'channel' and p.type_cross_section_down = 'channel' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'channel' and st_dwithin(p.geom, r.geom, .1) and ST_DWithin(p.geom, ST_EndPoint(r.geom), 1)
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_down = 'channel' and st_dwithin(p.geom, r.geom, .1) and ST_DWithin(p.geom, ST_StartPoint(r.geom), 1)
    )
    ,
    bounds as (
        select FIRST_VALUE(reach) over (partition by reach order by alpha), typ, LAG(alpha) over (partition by reach order by alpha) as start, alpha as end, id, reach from ends
    )
    select row_number() over() as id, st_linesubstring(r.geom, b.start, b.end) as geom, r.id as reach from bounds as b join ${model}.reach as r on r.id=b.reach where b.typ = 'down'
;;

create function $model.downstream_valley_section(transect geometry)
returns integer
language plpgsql immutable
as
$$$$
    begin
        return (
        with reach as (
            select r.id, st_intersection(r.geom, transect) as inter, geom, pk0_km
            from ${model}.reach as r
            where st_intersects(r.geom, transect)
        ),
        pk as (
            select st_linelocatepoint(geom, inter)*st_length(geom)/1000+pk0_km as pk_km from reach
        ),
        down_reach as (
            select ndown.reach as id
            from ${model}.river_node as ndown
            join ${model}.connector_link as l on l.down=ndown.id
            join ${model}.river_node as nup on nup.id=l.up
            where l.main_branch
            and nup.reach = (select id from reach)
        ),
        extended_reach_node as (
            select n.id, n.pk_km
            from ${model}.river_node as n, pk
            where n.reach in (select id from reach union select id from down_reach)
            and n.pk_km >= pk.pk_km
        ),
        section_down as (
            select c.id
            from extended_reach_node as n
            join ${model}.river_cross_section_profile as c on c.id = n.id,
            pk
            where ((c.type_cross_section_down='valley' and c.down_vcs_geom is not null)
                or (c.type_cross_section_up='valley' and c.up_vcs_geom is not null))
            order by n.pk_km asc
            limit 1
        )
        select id from section_down);
    end;
$$$$
;;

/* ********************************************** */
/*                  Pipe capacity                 */
/* ********************************************** */

create function $model.geometric_calc_s_fct(real[])
returns double precision
language plpgsql immutable
as
$$$$
    declare
        z_array alias for $$1;
        x real[];
        s double precision := 0.0;
        last_h double precision := 0.0;
        last_w double precision := 0.0;
    begin
        foreach x slice 1 in array z_array
        loop
            s:=s+0.5*(x[2]+last_w)*(x[1]-last_h);
            last_h:=x[1];
            last_w:=x[2];
        end loop;
        return s;
    end;
$$$$
;;

create function ${model}.op_geometric_calc_p_fct(real[])
returns double precision
language sql immutable
as
$$$$
    select $$1[1][2] + sum(2*sqrt(($$1[i+1][1]-$$1[i][1])^2 + (0.5*($$1[i+1][2]-$$1[i][2]))^2 )) 
    from generate_series(1, array_length($$1,1) - 1) i;

$$$$
;;

create function ${model}.cp_geometric_calc_p_fct(real[])
returns double precision
language sql immutable
as
$$$$
    select ${model}.op_geometric_calc_p_fct($$1) + $$1[array_length($$1, 1)][2];
$$$$
;;


/* ********************************************** */
/*          Overloads pipe_link view              */
/* ********************************************** */

create or replace view ${model}.pipe_link as
 select p.id,
    p.name,
    p.up,
    p.down,
    c.z_invert_up,
    c.z_invert_down,
    c.cross_section_type,
    c.h_sable,
    c.branch,
    c.exclude_from_branch,
    c.rk,
    c.custom_length,
    c.circular_diameter,
    c.ovoid_height,
    c.ovoid_top_diameter,
    c.ovoid_invert_diameter,
    c.cp_geom,
    c.op_geom,
    c.rk_maj,
    p.geom,
    p.configuration::character varying as configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration as configuration_json,
    p.comment,
    (select case
    when (c.z_invert_up-c.z_invert_down)*st_length(p.geom)>0
    and st_length(p.geom)<>0
    then
        (select case when c.cross_section_type='circular' then
            (select case when c.circular_diameter>0 then
                (with n as (select
                    (pi()/4*pow(c.circular_diameter, 2.0)) as s,
                    (pi()*c.circular_diameter) as p,
                    c.rk as k)
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n))
            else -999
            end)
        when c.cross_section_type='ovoid' then
            (with n as (select
                (pi()/8*(c.ovoid_top_diameter*c.ovoid_top_diameter+c.ovoid_invert_diameter*c.ovoid_invert_diameter)+0.5*(c.ovoid_height-0.5*(c.ovoid_top_diameter+c.ovoid_invert_diameter))*(c.ovoid_top_diameter+c.ovoid_invert_diameter)) as s,
                (pi()/2*(c.ovoid_top_diameter+c.ovoid_invert_diameter)+2*sqrt( (c.ovoid_height-0.5*(c.ovoid_top_diameter+c.ovoid_invert_diameter))^2 + (0.5*(c.ovoid_top_diameter-c.ovoid_invert_diameter))^2)) as p,
                c.rk as k)
            (select case when n.s>0 and n.p>0 then
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
            else -999
            end from n))
        when c.cross_section_type='pipe' then
            (with n as (select ${model}.geometric_calc_s_fct(cpg.zbmin_array) as s,
                    ${model}.cp_geometric_calc_p_fct(cpg.zbmin_array) as p,
                    c.rk as k
                from ${model}.closed_parametric_geometry cpg
                where c.cp_geom = cpg.id)
            (select case when n.s>0 and n.p>0 then
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
            else -999
            end from n))
        when c.cross_section_type='channel' then
            (with n as (select ${model}.geometric_calc_s_fct(opg.zbmin_array) as s,
                    ${model}.op_geometric_calc_p_fct(opg.zbmin_array) as p,
                    c.rk as k
                from ${model}.open_parametric_geometry opg
                where c.op_geom = opg.id)
            (select case when n.s>0 and n.p>0 then
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
            else -999
            end from n))
        else 0.0
        end)
    else -999
    end) as qcap,
    (select case when c.cross_section_type='circular' then
        (select (c.z_invert_up+c.circular_diameter))
    when c.cross_section_type='ovoid' then
        (select (c.z_invert_up+c.ovoid_height))
    when c.cross_section_type='pipe' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.closed_parametric_geometry cpg
            where c.cp_geom = cpg.id)
        (select (c.z_invert_up + n.height) from n))
    when c.cross_section_type='channel' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.open_parametric_geometry opg
            where c.op_geom = opg.id)
        (select (c.z_invert_up + n.height) from n))
    else null
    end) as z_vault_up,
    (select case when c.cross_section_type='circular' then
        (select (c.z_invert_down+c.circular_diameter))
    when c.cross_section_type='ovoid' then
        (select (c.z_invert_down+c.ovoid_height))
    when c.cross_section_type='pipe' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.closed_parametric_geometry cpg
            where c.cp_geom = cpg.id)
        (select (c.z_invert_down + n.height) from n))
    when c.cross_section_type='channel' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.open_parametric_geometry opg
            where c.op_geom = opg.id)
        (select (c.z_invert_down + n.height) from n))
    else null
    end) as z_vault_down,
    (select case when c.cross_section_type='channel' then
        (select (c.z_invert_up))::double precision
    else (select z_ground from ${model}.manhole_node where p.up = id)::double precision
    end) as z_tn_up,
    (select case when c.cross_section_type='channel' then
        (select (c.z_invert_down))::double precision
    else (select z_ground from ${model}.manhole_node where p.down = id)::double precision
    end) as z_tn_down

    from ${model}._pipe_link c,
        ${model}._link p
where p.id = c.id
;;


/* ********************************************** */
/*            River node tools                    */
/* ********************************************** */

create function ${model}.river_node_pk(node_geom geometry)
returns double precision
language plpgsql
as $$$$
    declare
        _res real;
    begin
        select st_linelocatepoint(r.geom, node_geom) * st_length(r.geom) / 1000::double precision + r.pk0_km
            from ${model}.reach as r
            where ST_DWithin(r.geom, node_geom, .1) order by ST_Distance(r.geom, node_geom) asc limit 1
            into _res;
        return _res;
    end;
$$$$
;;

create function ${model}.river_node_z_invert(node_geom geometry)
returns real
language plpgsql
as $$$$
    declare
        _res real;
    begin
        with node as (
                select ${model}.river_node_pk(node_geom) as pk_km, id as reach from ${model}.reach as r
                where ST_DWithin(r.geom, node_geom, .1) order by ST_Distance(r.geom, node_geom) asc limit 1
            ),
            pts_up as (
                select p.id, ${model}.river_node_pk(p.geom) as pk_km, COALESCE(z_invert_down, z_invert_up) as z_invert
                from ${model}._river_node as n, node, ${model}.river_cross_section_profile as p
                where n.reach=node.reach and p.id=n.id and ${model}.river_node_pk(p.geom) <= node.pk_km
                ),
            pts_down as (
                select p.id, ${model}.river_node_pk(p.geom) as pk_km, COALESCE(z_invert_up, z_invert_down) as z_invert
                from ${model}._river_node as n, node, ${model}.river_cross_section_profile as p
                where n.reach=node.reach and p.id=n.id and node.pk_km <= ${model}.river_node_pk(p.geom)
                ),
            up as (
                select z_invert, pk_km, id from pts_up
                where z_invert is not null
                order by pk_km desc limit 1
                ),
            down as (
                select z_invert, pk_km, id from pts_down
                where z_invert is not null
                order by pk_km asc limit 1
                ),
            cote_minmax as (
                select up.z_invert as cote, COALESCE(NULLIF(down.pk_km - node.pk_km, 0), 1) as weight from up, down, node
                union all
                select down.z_invert as cote, COALESCE(NULLIF(node.pk_km - up.pk_km, 0), 1) as weight from up, down, node
            ),
            z_invert as (
                select SUM(cote * weight)/SUM(weight) as z
                from cote_minmax
                having bool_and(cote is not null)
            )
            select case
                when not exists (select 1 from z_invert) then null
                else (select z from z_invert) end
            into _res;
        return _res;
    end;
$$$$
;;

/* ********************************************** */
/*    cross sections creation from line_xyz       */
/* ********************************************** */


create view $model.flood_plain_transect_candidate as
select l.id, l.name, l.id as line_xyz, project.orient_transect(r.geom, st_makeline(st_startpoint(l.geom), st_endpoint(l.geom))) as geom, 'ignored_for_coverages'::hydra_constrain_type as constrain_type,
    st_intersection(r.geom, st_makeline(st_startpoint(l.geom), st_endpoint(l.geom))) as reach_intersection, project.orient_transect(r.geom, l.geom) as topo
from project.line_xyz l
join $model.reach r on st_intersects(r.geom, l.geom)
where not exists (select 1 from $model._river_cross_section_profile p where p.name = l.name)
;;

-- create constrain, river cross section and associated geometry from a list of flood_plain_transect_candidate ids
create function $model.create_river_cross_section_from_line_xyz(candidate_id integer)
returns integer -- created cross section ids
language plpgsql volatile as
$$$$
declare
    res integer;
    z_invert real;
    top timestamp;
begin
    top := clock_timestamp();

    insert into $model.constrain(name, line_xyz, geom, constrain_type)
    select name, line_xyz, geom, constrain_type
    from $model.flood_plain_transect_candidate t
    where t.id = candidate_id and not exists (select 1 from $model.constrain c where c.name=t.name)
    ;

    select min(st_z(p.geom))
    from $model.flood_plain_transect_candidate t
    join lateral st_dumppoints(t.topo) p on true
    where t.id = candidate_id
    into z_invert
    ;

    insert into $model.valley_cross_section_geometry(name, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, transect, sz_array)
    select t.name, ('{{'||z_invert||', 0}}')::real[], ('{{'||z_invert||', 0}}')::real[], ('{{'||z_invert||', 0}}')::real[],
        c.id, array_agg(ARRAY[st_linelocatepoint(t.geom, p.geom)*st_length(t.geom), st_z(p.geom)] order by p.path)
    from $model.flood_plain_transect_candidate t
    join $model.constrain c on c.name = t.name
    join lateral st_dumppoints(c.topo) p on true
    where t.id = candidate_id and not exists (select 1 from $model.valley_cross_section_geometry g where g.name=t.name)
    group by t.name, c.id
    ;

    -- create river nodes where needed
    insert into $model.river_node(name, geom)
    select distinct t.name, t.reach_intersection
    from $model.flood_plain_transect_candidate t
    where t.id = candidate_id and not exists (select 1 from $model.river_node n where st_dwithin(t.reach_intersection, n.geom, 1.))
    ;

    insert into $model._river_cross_section_profile(name, id, type_cross_section_up, type_cross_section_down, down_vcs_geom, z_invert_down)
    select t.name, (select n.id from $model.river_node n order by n.geom <-> t.reach_intersection limit 1) id, null, 'valley', g.id down_vcs_geom, z_invert
    from $model.flood_plain_transect_candidate t
    join $model.valley_cross_section_geometry g on g.name = t.name
    where not exists (select 1 from $model.river_cross_section_profile p where st_dwithin(t.reach_intersection, p.geom, 1.))
    and t.id = candidate_id
    returning id into res
    ;

    return res;
end;
$$$$
;;

/* ********************************************** */
/*              Validity handling                 */
/* ********************************************** */

$node_invalidity_reason
;;

$link_invalidity_reason
;;

$singularity_invalidity_reason
;;

create view $model.invalid as
    select CONCAT('node:', n.id) as id,
        validity,
        name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom,
        $model.node_invalidity_reason(n.id)::text as reason
        from $model._node as n
        where validity=FALSE
    union all
    select CONCAT('link:', l.id) as id,
        validity,
        name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom,
        $model.link_invalidity_reason(l.id)::text as reason
        from $model._link as l
        where validity=FALSE
    union all
    select CONCAT('singularity:', s.id) as id,
        s.validity,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        $model.singularity_invalidity_reason(s.id)::text as reason
        from $model._singularity as s join $model._node as n on s.node = n.id
        where s.validity=FALSE
    union all
    select CONCAT('profile:', p.id) as id,
        p.validity,
        p.name,
        'profile'::varchar as type,
        ''::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        ''::varchar as reason from $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where p.validity=FALSE
    union all
    select CONCAT('duplicate:', row_number() over()) as id,
        false as validity,
        a.name,
        'duplicate' as type,
        ''::varchar as subtype,
        a.geom::geometry('POINTZ', $srid) as geom,
        'node '||a.name||' is duplicated with node '||b.name as reason
        from hydra.metadata as m
        join $model._node as a on true
        join $model._node as b on st_dwithin(a.geom, b.geom, m.precision)
        where a.id < b.id
    union all
    select 
        CONCAT('manhole:', id) as id,
        False as validity,
        name,
        'node'::varchar as type,
        'manhole'::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom,
        'at least one connected pipe vault is above z_ground' as reason
    from (
        select distinct id, name, geom 
        from (
            select m.id, m.name, m.geom
            from $model.manhole_node m
            join $model.pipe_link u on u.up=m.id
            where m.z_ground < u.z_vault_up
            union all
            select m.id, m.name, m.geom
            from $model.manhole_node m
            join $model.pipe_link d on d.down=m.id
            where m.z_ground < d.z_vault_down
            ) a 
        ) b 
;;


/* ********************************************** */
/*            View for results display            */
/* ********************************************** */

-- River nodes
create materialized view ${model}.results_river as
    select
        n.id,
        UPPER(n.name)::varchar(24) as name,
        n.geom,
        case when v.id is not null then 'valley'
             when o.id is not null then 'channel'
             else 'closed'
        end as section_type,
        now() as last_refresh
    from ${model}.river_node as n
    left join ${model}.open_reach as v on ST_DWithin(v.geom, n.geom, 0.001)
    left join ${model}.channel_reach as o on ST_DWithin(o.geom, n.geom, 0.001)
with no data;
;;

-- Manholes nodes
create materialized view ${model}.results_manhole as
    select
        n.id,
        UPPER(n.name)::varchar(24) as name,
        n.geom,
        now() as last_refresh
    from ${model}.manhole_node as n
with no data;
;;

-- Pipes
create materialized view ${model}.results_pipe as
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.pipe_link
    where up_type='manhole' and down_type='manhole'
with no data;
;;

-- Hydrol pipes
create materialized view ${model}.results_pipe_hydrol as
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.pipe_link
    where up_type='manhole_hydrology' and down_type='manhole_hydrology'
with no data;
;;

-- Catchments
create materialized view ${model}.results_catchment as
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.catchment_node
with no data;
;;

-- Links
create materialized view ${model}.results_link as
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.gate_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.regul_gate_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.weir_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.borda_headloss_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.pump_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.deriv_pump_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.fuse_spillway_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.connector_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.strickler_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.porous_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.overflow_link
        union
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.network_overflow_link
with no data;
;;

-- Surface elements
create materialized view $model.results_surface as
 with dump as (
         select st_collect(n.geom) as points,
            s.geom as envelope,
            s.id
           from $model.coverage s,
            $model.crossroad_node n
          where s.domain_type = 'street'::hydra_coverage_type and st_intersects(s.geom, n.geom)
          group by s.id, s.geom
        ), voronoi_1 as (
         select st_intersection(st_setsrid((st_dump(st_voronoipolygons(dump.points, 0::double precision, dump.envelope))).geom, $srid), dump.envelope) as geom
           from dump
        ), voronoi as (
			select (st_dump(geom)).geom as geom from voronoi_1
		)
 select e.id,
    upper(e.name::text)::character varying(24) as name,
    e.contour as geom,
    now() as last_refresh
   from $model.elem_2d_node e
union
 select n.id,
    upper(n.name::text)::character varying(24) as name,
    c.geom,
    now() as last_refresh
   from $model.storage_node n
     join $model.coverage c on n.contour = c.id
union
 select n.id,
    upper(n.name::text)::character varying(24) as name,
    voronoi.geom,
    now() as last_refresh
   from voronoi,
    $model.crossroad_node n
  where st_intersects(voronoi.geom, n.geom)
  order by 1
with no data
;;


create materialized view $model.riverbank as
--explain analyse
with blade as (
    select st_linemerge(st_collect(geom)) as geom
    from (
        select coalesce(st_snap(st_force2d(r.geom), st_collect(st_force2d(l.geom)), .1), st_force2d(r.geom)) as geom
        from $model.open_reach r
        left join $model.connector_link l on st_dwithin(r.geom, l.geom, .1)
        group by r.geom
        union
        select st_force2d(l.geom) as geom
        from $model.open_reach r
        left join $model.connector_link l on st_dwithin(r.geom, l.geom, .1) and st_dwithin(st_collect(ARRAY[st_startpoint(r.geom), st_endpoint(r.geom)]), l.geom, .1)
    ) t
),
cov as (
    select c.id as coverage, st_force2d(c.geom) as geom, st_collect(d.geom) as blade, t.transects, 
    st_difference(st_force2d(st_exteriorring(c.geom)), t.discretized) as mbanks, t.elem_length
    from $model.coverage c
    cross join blade b
    cross join lateral (
        select st_collect(st_force2d(t.geom)) as transects, st_collect(st_force2d(t.discretized)) as discretized, min(t.elem_length) as elem_length
        from $model.constrain t
        where t.geom && c.geom and st_length(st_intersection(t.discretized, c.geom))>0
        and t.constrain_type = 'flood_plain_transect'
        ) t
    join lateral st_dump(b.geom) d on st_intersects(d.geom, c.geom)
    where c.domain_type='reach'
    --and c.id=113
    group by c.id, c.geom, t.transects, t.discretized, t.elem_length
),
covr as (
    select
        c.coverage,
        d.geom,
        c.blade,
        st_linemerge(st_intersection(st_snap(c.blade, d.geom , .1), st_exteriorring(d.geom))) as reach,
        c.transects,
        c.mbanks, 
        c.elem_length
    from cov c
    cross join lateral st_dump(st_split(c.geom, c.blade)) d
),
bank as (
    select c.geom as side, c.reach, s.geom as start_, e.geom as end_, -- c.transects,
       project.line_offset(c.reach, s.geom, e.geom, c.geom) as geom, c.coverage,
       e.transect as end_transect, s.transect as start_transect,
       coalesce(left_.geom, st_makeline(st_startpoint(s.transect), st_startpoint(e.transect))) as left_, 
       coalesce(right_.geom,st_makeline(st_endpoint(s.transect), st_endpoint(e.transect))) as right_, 
       c.elem_length
    from covr c
    cross join lateral (
        select p.geom, p.path[1] in (1, st_numpoints(t.geom)) as is_end, t.geom as transect
        from st_dump(c.transects) t
        cross join lateral st_dumppoints(t.geom) p
        where st_dwithin(p.geom, c.geom, .1) and  st_dwithin(t.geom, st_startpoint(c.reach), .1)
        order by st_distance(p.geom, st_startpoint(c.reach))
        limit 1) s
    cross join lateral (
        select p.geom, p.path[1] in (1, st_numpoints(t.geom)) as is_end, t.geom as transect
        from st_dump(c.transects) t
        cross join lateral st_dumppoints(t.geom) p
        where st_dwithin(p.geom, c.geom, .1) and  st_dwithin(t.geom, st_endpoint(c.reach), .1)
        order by st_distance(p.geom, st_endpoint(c.reach))
        limit 1) e
    left join lateral (
        select d.geom
        from st_dump(c.mbanks) d
        where st_dwithin(d.geom, st_startpoint(e.transect), .1)) left_ on true
    left join lateral (
        select d.geom
        from st_dump(c.mbanks) d
        where st_dwithin(d.geom, st_endpoint(e.transect), .1)) right_ on true
    where not (e.is_end and s.is_end) and not s.geom=e.geom
)
select row_number() over() as id, b.coverage, b.geom, d.geom as major, b.reach, t.side,-- b.transects
    case when t.side='left' then
        st_makeline(st_startpoint(start_transect), st_startpoint(b.reach))
    else
        st_makeline(st_startpoint(b.reach), st_endpoint(start_transect))
    end as start_half_transect,
    case when t.side='left' then
        st_makeline(st_startpoint(end_transect), st_endpoint(b.reach))
    else
        st_makeline(st_endpoint(b.reach), st_endpoint(end_transect))
    end as end_half_transect,
    case when t.side='left' then
        b.left_ 
    else
        b.right_
    end as bank, 
    b.elem_length
from bank b
cross join lateral st_dump(st_polygonize(ARRAY[st_union(st_exteriorring(b.side), b.geom)])) d
cross join lateral (
    select  case when st_ispolygonccw(st_makepolygon(st_makeline(ARRAY[start_, st_pointn(b.reach, 1), st_pointn(b.reach, 2), start_]))) then 'left' else 'right' end as side
    ) t
where not st_intersects(d.geom, b.reach)
with no data
;;

create index riverbank_coverage_idx on $model.riverbank(coverage)
;;

create index riverbank_geom_idx on $model.riverbank using gist(geom)
;;

create index riverbank_major_idx on $model.riverbank using gist(major)
;;


create materialized view $model.flowline as
with transects as (
    select row_number() over(order by alpha) as id, geom, elem_length, bank, domain, max(st_length(geom)) over(partition by bank) as maxlength
    from (
        select 
            case when st_distance(st_startpoint(i.geom), b.bank) > st_distance(st_endpoint(i.geom), b.bank) 
            then st_reverse(i.geom) 
            else i.geom end as geom, 
            b.elem_length, 
            b.id as bank, 
            st_linelocatepoint(b.bank, st_closestpoint(b.bank, i.geom)) as alpha, 
            b.major as domain
        from $model.riverbank b
        left join lateral st_dumppoints(b.bank) p on p.path[1] not in (1, st_numpoints(b.bank))
        cross join lateral interpolate_direction(b.start_half_transect, b.end_half_transect, p.geom) d
        cross join lateral st_dump( 
            st_intersection(
                st_makeline(
                    st_translate(p.geom,-10000*d.x,-10000*d.y),
                    st_translate(p.geom, 10000*d.x, 10000*d.y)
                    ),
                b.major)) as i
        where st_intersects(i.geom, b.bank)
        and (st_distance(st_startpoint(i.geom), b.bank) > 0.1 or st_distance(st_endpoint(i.geom), b.bank) > 0.1)
        union all
        select case when st_distance(st_startpoint(i.geom), b.bank) > st_distance(st_endpoint(i.geom), b.bank) then st_reverse(i.geom) else i.geom end as geom, b.elem_length, b.id as bank, l.alpha, b.major as domain
        from $model.riverbank b
        cross join lateral st_dump(st_difference(st_exteriorring(b.major), st_collect(st_intersection(b.geom, b.major), st_intersection(b.bank, b.major)))) as i
        cross join lateral (select st_linelocatepoint(b.bank, st_closestpoint(b.bank, i.geom)) as alpha ) l

    ) g
    where st_geometrytype(geom) = 'ST_LineString'
)
,
fl as (
    select st_intersection(b.geom, st_makeline(st_lineinterpolatepoint(t.geom, least(i*t.elem_length/t.maxlength, .99)) order by t.id)) as geom, t.bank
    from transects t
    cross join lateral generate_series(1, (t.maxlength/t.elem_length)::integer) i
    cross join lateral (select st_buffer(t.domain, -t.elem_length/2) as geom) as b
    group by i, t.bank, b.geom
    having count(1) > 1
)
select row_number() over() as id, d.geom, fl.bank
from fl
cross join lateral st_dump(geom) as d
with no data
;;

create index flowline_bank_idx on $model.flowline(bank)
;;


