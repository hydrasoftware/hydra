/* ********************************************************************************** */
/* Will delete crossroad_node wich are not :                                          */
/*     + at intersection beetween two street                                          */
/*     + at the end of a street                                                       */
/*     + with a link or a singularity                                                 */
/*                                                                                    */
/* ********************************************************************************** */


delete from $model.crossroad_node
where geom not in (
    -- Sommets de rues
    select (st_dumppoints(geom)).geom as geom
    from $model.street
        union
    -- Intersections de rues
    select (st_dumppoints(st_intersection(s1.geom, s2.geom))).geom as geom
    from $model.street as s1, $model.street as s2
    where ST_intersects(s1.geom,s2.geom)
    and s1.id<s2.id
)
and id not in (select down from $model._link) -- noeuds aval à une liaison
and id not in (select up from $model._link) -- noeuds amont à une liaison
and id not in (select id from $model._singularity) -- noeuds porteurs d'une singularité