# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.forms.borda_headloss_link_guitest [-dhs] [-g project_name model_name x y x y]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name model_name x y
        run in gui mode

   -s, --solo
        runs solo

"""

from hydra.database.database import TestProject, remove_project, project_exists
from hydra.utility.string import normalized_name, normalized_model_name
from hydra.utility.sql_json import instances as INSTANCES
import sys
import getopt

from hydra.project import Project

def gui_mode(project_name, model_name, geom, id):

    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.borda_headloss_link import BordaHeadlossLinkEditor
    from hydra.utility.string import normalized_name, normalized_model_name

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)

    test_dialog = BordaHeadlossLinkEditor(obj_project, geom, id)
    test_dialog.exec_()


def test():
    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.borda_headloss_link import BordaHeadlossLinkEditor
    from hydra.database.database import TestProject

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)

    obj_project.set_current_model(model_name)
    model = obj_project.get_current_model()


    model.add_manhole([12, 15])
    model.add_manhole([13, 22])

    test_dialog = BordaHeadlossLinkEditor(obj_project, [[12,15],[13,22]])
    test_dialog.name.setText("test_borda_link")
    #test form behaviour for all borda types
    for law in list(INSTANCES['borda_headloss_link_type'].keys()):
        description = INSTANCES['borda_headloss_link_type'][law]['description']
        assert description in [test_dialog.law_type.itemText(i).split(': ')[1] for i in range(test_dialog.law_type.count())]
        test_dialog.law_type.setCurrentIndex(test_dialog.law_type.findText(description))
        test_dialog.law_changed()
        for param_name in INSTANCES['borda_headloss_link_type'][law]['params']:
            assert hasattr(test_dialog, param_name)
    test_dialog.law_type.setCurrentIndex(test_dialog.law_type.findText('13: Sharp angle bend'))
    test_dialog.up_geom.setCurrentIndex(test_dialog.up_geom.findText('1: Circular'))
    test_dialog.up_cs_area.setText('6.25')
    test_dialog.up_cs_zinvert.setText('56.5')
    test_dialog.angle.setText('47')
    test_dialog.connector_no.setValue(6)
    test_dialog.ls.setText('0.59')
    test_dialog.wall_friction_option.setCurrentIndex(test_dialog.wall_friction_option.findText('1: Smooth'))
    id = test_dialog.get_id()
    test_dialog.save()

    test_dialog = BordaHeadlossLinkEditor(obj_project, None, id)
    assert test_dialog.name.text()=="test_borda_link"
    assert test_dialog.law_type.currentText() == '13: Sharp angle bend'
    assert test_dialog.up_geom.currentText() == '1: Circular'
    assert float(test_dialog.up_cs_area.text()) == 6.25
    assert float(test_dialog.up_cs_zinvert.text()) == 56.5
    assert float(test_dialog.angle.text()) == 47
    assert test_dialog.connector_no.value() == 6
    assert float(test_dialog.ls.text()) == 0.59
    assert test_dialog.wall_friction_option.currentText() == '1: Smooth'
    test_dialog.close()

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])

except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name, with_model=True)
    test()
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    model_name = args[1]
    geom = [float(a) for a in args[2:]]
    id =geom[2] if len(geom)==3 else None
    project_name = normalized_name(project_name)
    model_name = normalized_model_name(model_name)
    gui_mode(project_name, model_name, geom, id)
    exit(0)

test()
# fix_print_with_import
print("ok")
