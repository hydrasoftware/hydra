# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from qgis.PyQt import uic
from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtWidgets import QMenu, QMessageBox, QWidget
from qgis.PyQt.QtGui import QCursor
from hydra.gui.select_menu import SelectTool
from hydra.utility.tables_properties import TablesProperties
from hydra.utility.settings_properties import SettingsProperties
import json
import importlib
import os
import re

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)


class EditTool(SelectTool):
    def __init__(self, project, point=None, size=10, parent=None, iface=None, excluded_layers=[]):
        SelectTool.__init__(self, project, point, size, parent, excluded_layers=excluded_layers)
        EditTool.edit(project, self.table, self.id, size, iface)

    @staticmethod
    def edit(currentproject, table, id, size=10, iface=None):
        properties = TablesProperties.get_properties()

        if table is None:
            return

        if (table == "regul_gate_link"):
            from hydra.gui.forms.regul_gate_link import RegulGateLinkEditor
            map_per_pix = iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel() if iface else 0
            snap = SettingsProperties.get_snap()
            RegulGateLinkEditor(currentproject, None, id, None, iface, map_per_pix*snap).exec_()
            return

        if (table == "regul_sluice_gate_singularity"):
            from hydra.gui.forms.regul_gate_singularity import RegulGateSingularityEditor
            map_per_pix = iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel() if iface else 0
            snap = SettingsProperties.get_snap()
            RegulGateSingularityEditor(currentproject, None, id, None, iface, map_per_pix*snap).exec_()
            return

        if (table == "qq_split_hydrology_singularity"):
            from hydra.gui.forms.qq_split_hydrology_singularity import QqSplitHydrologySingularityEditor
            map_per_pix = iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel() if iface else 0
            snap = SettingsProperties.get_snap()
            QqSplitHydrologySingularityEditor(currentproject, None, id, None, iface, map_per_pix*snap).exec_()
            return

        if (table == "zq_split_hydrology_singularity"):
            from hydra.gui.forms.zq_split_hydrology_singularity import ZqSplitHydrologySingularityEditor
            map_per_pix = iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel() if iface else 0
            snap = SettingsProperties.get_snap()
            ZqSplitHydrologySingularityEditor(currentproject, None, id, None, iface, map_per_pix*snap).exec_()
            return

        if (table == "reservoir_rs_hydrology_singularity"):
            from hydra.gui.forms.reservoir_rs_hydrology_singularity import ReservoirRsHydrologySingularityEditor
            map_per_pix = iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel() if iface else 0
            snap = SettingsProperties.get_snap()
            ReservoirRsHydrologySingularityEditor(currentproject, None, id, None, iface, map_per_pix*snap).exec_()
            return

        if (table == "reservoir_rsp_hydrology_singularity"):
            from hydra.gui.forms.reservoir_rsp_hydrology_singularity import ReservoirRspHydrologySingularityEditor
            map_per_pix = iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel() if iface else 0
            snap = SettingsProperties.get_snap()
            ReservoirRspHydrologySingularityEditor(currentproject, None, id, None, iface, map_per_pix*snap).exec_()
            return

        if (table == "constrain"):
            from hydra.gui.forms.model_constrain import ModelConstrainWidget
            ModelConstrainWidget(currentproject, None, id).exec_()
            return

        if (table == "regulated"):
            from hydra.gui.visu_regulation import VisuRegulation
            VisuRegulation(currentproject, id, None).exec_()
            return

        if (table == "branch"):
            from hydra.gui.forms.mkb import MarkerBranchEditor
            mrkb_name, = currentproject.execute("""
                    select name
                    from {}.branch
                    where id={}""".format(currentproject.get_current_model().name, id)).fetchone()
            if mrkb_name:
                    mrkb_id, = currentproject.execute("""
                                select id
                                from {}.pipe_branch_marker_singularity
                                where name='{}'""".format(currentproject.get_current_model().name, mrkb_name)).fetchone()
                    MarkerBranchEditor(currentproject, None, mrkb_id).exec_()
            else:
                currentproject.log.error("No MRKB on this branch")
            return

        if (table == "river_cross_section_profile"):
            from hydra.gui.forms.river_cross_section_profile import RiverCrossSectionProfileEditor
            RiverCrossSectionProfileEditor(currentproject, None, id=id, iface=iface).exec_()
            return

        if properties[table]['file']:
            module = importlib.import_module("hydra.gui.forms." + properties[table]['file'])
            form_class = getattr(module, properties[table]['class'])
            form_class(currentproject, None, id).exec_()
            return

        currentproject.log.error("Not implemented edition: {}".format(table))
