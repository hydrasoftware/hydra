# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import random
import importlib
from qgis.PyQt import uic
from qgis.PyQt.QtGui import QColor
from qgis.PyQt.QtCore import pyqtSignal, Qt
from qgis.PyQt.QtWidgets import QApplication, QMessageBox, QToolButton, QDockWidget, QInputDialog
from qgis.core import QgsDataSourceUri, QgsWkbTypes, QgsRendererCategory, QgsCategorizedSymbolRenderer
import hydra.utility.map_point_tool as map_point_tool
from hydra.gui.base_dialog import tr
from hydra.utility.map_point_tool import create_polygon, create_line, create_segment, create_point, Snapper, VisibleGeometry, MapPointTool
from hydra.utility.tables_properties import TablesProperties
from hydra.utility.settings_properties import SettingsProperties
from hydra.database.mesh_interface import MeshInterface
from hydra.database.hydrology_converter import HydrologyConverter

__currendir = os.path.dirname(__file__)

class ModelDock(QDockWidget):
    layers_changed_signal = pyqtSignal(list)

    def __init__(self, project, iface):
        QDockWidget.__init__(self, tr("Model dock"))
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "model_dock.ui"), self)

        self.__project = project
        self.__iface = iface
        self.__mesh_interface = MeshInterface(self.__iface.mapCanvas(), self.__project, self)

        self.__mesh_interface.layers_changed_signal.connect(self.layers_changed_signal.emit)

        self.__connect()
        self.refresh()

    def __connect(self):
        # Project
        self.dry_inflow_sector.clicked.connect(self.__dry_inflow_sector)
        self.rain_gage.clicked.connect(self.__build_object)
        self.wind_gage.clicked.connect(self.__build_object)
        self.affect_group.clicked.connect(self.__add_to_terrain_group)

        # Boundary conditions
        self.hydrograph_bc_singularity.clicked.connect(self.__build_object)
        self.constant_inflow_bc_singularity.clicked.connect(self.__build_object)
        self.model_connect_bc_singularity.clicked.connect(self.__build_object)
        self.froude_bc_singularity.clicked.connect(self.__build_object)
        self.strickler_bc_singularity.clicked.connect(self.__build_object)
        self.zq_bc_singularity.clicked.connect(self.__build_object)
        self.tank_bc_singularity.clicked.connect(self.__build_object)
        self.weir_bc_singularity.clicked.connect(self.__build_object)
        self.tz_bc_singularity.clicked.connect(self.__build_object)
        self.air_flow_bc_singularity.clicked.connect(self.__build_object)
        self.air_pressure_bc_singularity.clicked.connect(self.__build_object)
        assert(self.air_flow_bc_singularity.objectName())

        # Hydrology
        self.catchment.clicked.connect(self.__build_object)
        self.manhole_hydrology_node.clicked.connect(self.__build_object)
        self.routing_hydrology_link.clicked.connect(self.__build_object)
        self.qq_split_hydrology_singularity.clicked.connect(self.__qq_split_hydrology_singularity)
        self.reservoir_rs_hydrology_singularity.clicked.connect(self.__reservoir_rs_hydrology_singularity)
        self.catchment_node.clicked.connect(self.__build_object)
        self.hydrology_bc_singularity.clicked.connect(self.__build_object)
        self.connector_hydrology_link.clicked.connect(self.__build_object)
        self.zq_split_hydrology_singularity.clicked.connect(self.__zq_split_hydrology_singularity)
        self.reservoir_rsp_hydrology_singularity.clicked.connect(self.__reservoir_rsp_hydrology_singularity)
        self.hydrology_convert_btn.clicked.connect(self.__change_select_to_hydraulic)

        # Structures and headlosses
        self.gate_singularity.clicked.connect(self.__build_object)
        self.zregul_weir_singularity.clicked.connect(self.__build_object)
        self.borda_headloss_singularity.clicked.connect(self.__build_object)
        self.param_headloss_singularity.clicked.connect(self.__build_object)
        self.bradley_headloss_singularity.clicked.connect(self.__build_object)
        self.bridge_headloss_singularity.clicked.connect(self.__build_object)
        self.regul_sluice_gate_singularity.clicked.connect(self.__regul_gate_singularity)
        self.hydraulic_cut_singularity.clicked.connect(self.__build_object)
        self.marker_singularity.clicked.connect(self.__build_object)

        # Links
        self.gate_link.clicked.connect(self.__build_object)
        self.regul_gate_link.clicked.connect(self.__regul_gate_link)
        self.weir_link.clicked.connect(self.__build_object)
        self.pump_link.clicked.connect(self.__build_object)
        self.deriv_pump_link.clicked.connect(self.__build_object)
        self.borda_headloss_link.clicked.connect(self.__build_object)
        self.connector_link.clicked.connect(self.__build_object)
        self.strickler_link.clicked.connect(self.__build_object)
        self.porous_link.clicked.connect(self.__build_object)
        self.overflow_link.clicked.connect(self.__build_object)
        self.network_overflow_link.clicked.connect(self.__build_object)
        self.invert_link.clicked.connect(self.__reverse_link)
        self.air_duct_link.clicked.connect(self.__build_object)
        self.air_headloss_link.clicked.connect(self.__build_object)
        self.ventilator_link.clicked.connect(self.__build_object)
        self.jet_fan_link.clicked.connect(self.__build_object)

        # Network
        self.manhole_node.clicked.connect(self.__build_object)
        self.pipe_link.clicked.connect(self.__build_object)
        self.pipe_branch_marker_singularity.clicked.connect(self.__build_object)
        self.create_network_overflow.clicked.connect(self.__mesh_interface.create_network_overflow_links)
        self.insert_node.clicked.connect(self.__insert_node)
        self.delete_node.clicked.connect(self.__delete_node)

        # Station
        self.station_node.clicked.connect(self.__build_object)
        self.station.clicked.connect(self.__build_object)

        # River and free surface flow
        self.river_cross_section_profile.clicked.connect(self.__build_object)
        self.river_node.clicked.connect(self.__river_node)
        self.reach.clicked.connect(self.__river_reach)
        self.merge_reachs.clicked.connect(self.__fuse_reachs)
        self.split_reach.clicked.connect(self.__split_reach)
        self.split_constrain.clicked.connect(self.__split_constrain)
        self.manage_2d_domains.clicked.connect(self.__manage_domains)
        self.domain_2d_marker.clicked.connect(self.__build_object)
        self.coverage_marker.clicked.connect(self.__build_object)
        self.storage_node.clicked.connect(self.__build_object)
        self.crossroad_node.clicked.connect(self.__build_object)
        self.street.clicked.connect(self.__street)
        self.constrain.clicked.connect(self.__mesh_interface.create_constrain)
        self.mesh.clicked.connect(self.__mesh_interface.create_mesh)
        self.delete_mesh.clicked.connect(self.__mesh_interface.delete_mesh)
        self.create_links.clicked.connect(self.__mesh_interface.create_links)
        self.regen_mesh.clicked.connect(self.__mesh_interface.mesh_regen)
        self.mesh_unmeshed.clicked.connect(self.__mesh_interface.mesh_unmeshed)
        self.create_interlink.clicked.connect(self.__create_interlink)

        # Triggers handling
        self.coverage_update.clicked.connect(self.__trigger)
        self.branch_update_fct.clicked.connect(self.__trigger)
        self.trigger_coverage.stateChanged.connect(self.__checkbox_checked)
        self.trigger_branch.stateChanged.connect(self.__checkbox_checked)

    def refresh(self):
        '''refresh interactivity of items based on active model existence and triggers state'''
        self.hydrology_box.setEnabled(self.__project.get_current_model() is not None)
        self.boundary_box.setEnabled(self.__project.get_current_model() is not None)
        self.headloss_box.setEnabled(self.__project.get_current_model() is not None)
        self.link_box.setEnabled(self.__project.get_current_model() is not None)
        self.network_box.setEnabled(self.__project.get_current_model() is not None)
        self.station_box.setEnabled(self.__project.get_current_model() is not None)
        self.river_box.setEnabled(self.__project.get_current_model() is not None)
        self.trigger_box.setEnabled(self.__project.get_current_model() is not None)
        if self.__project.get_current_model() is not None:
            self.trigger_coverage.setChecked(self.__project.fetchone("""select trigger_coverage from {}.metadata""".format(self.__project.get_current_model().name))[0])
            self.trigger_branch.setChecked(self.__project.fetchone("""select trigger_branch from {}.metadata""".format(self.__project.get_current_model().name))[0])

    def __trigger(self):
        '''at trigger_button click, launches corresponding DB function'''
        sending_button = self.sender()
        fct_name = sending_button.objectName()

        self.__project.fetchone("""select {}.{}();""".format(self.__project.get_current_model().name, fct_name))

        self.layers_changed_signal.emit([fct_name.split('_')[0]])

    def __checkbox_checked(self):
        '''at trigger_checkbox activation, changes corresponding field in DB'''
        sending_checkbox = self.sender()
        column_name = sending_checkbox.objectName()

        self.__project.execute("""update {}.metadata set {}={}""".format(self.__project.get_current_model().name, column_name, sending_checkbox.isChecked()))
        self.__project.commit()

    def __build_object(self):
        '''allows user to create geometry with snapper and have the associated form pop afterwards'''
        sending_button = self.sender()
        table = sending_button.objectName()

        module = importlib.import_module("hydra.gui.forms." + TablesProperties.get_properties()[table]['file'])
        form_class = getattr(module, TablesProperties.get_properties()[table]['class'])
        create_func = getattr(map_point_tool, 'create_' + TablesProperties.get_properties()[table]['type'])

        snapper = self.__get_snapper(TablesProperties.get_properties()[table]['snap']) if 'snap' in TablesProperties.get_properties()[table] else None
        if TablesProperties.get_properties()[table]['type'] in ('segment', 'segmentz'):
            snapper_end = self.__get_snapper(TablesProperties.get_properties()[table]['snap_end']) if 'snap_end' in TablesProperties.get_properties()[table] else None
            geom = create_func(self.__iface.mapCanvas(), self.__project.log, self.__project.terrain, snapper, snapper_end)
        else:
            geom = create_func(self.__iface.mapCanvas(), self.__project.log, self.__project.terrain, snapper)

        if geom:
            if 'snap' in TablesProperties.get_properties()[table]:
                if 'lines' in list(TablesProperties.get_properties()[table]['snap'].keys()) and 'points' in TablesProperties.get_properties()[table]['snap']:
                    if "reach" in TablesProperties.get_properties()[table]['snap']["lines"] and "river_node" in TablesProperties.get_properties()[table]['snap']["points"]:
                        self.__auto_node_on_reach(geom)

            cur_cfg = self.__project.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__project.get_current_model().set_current_configuration(1)
            form_class(self.__project, geom).exec_()
            if cur_cfg != 1:
                self.__project.get_current_model().set_current_configuration(cur_cfg)

            self.layers_changed_signal.emit([table, 'coverage'])
            if table=='river_cross_section_profile':
                self.layers_changed_signal.emit(['constrain'])
            elif table in ['routing_hydrology_link', 'connector_hydrology_link']:
                self.layers_changed_signal.emit(['hydrograph_bc_singularity'])

    def __get_snapper(self, snap_rules):
        '''returns a snapper based on set of rules:
                "points": ["manhole_node", "river_node"],
                "lines": ["reach"]
        '''
        map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
        snap_distance = SettingsProperties.get_snap()
        snapper = Snapper(snap_distance*map_per_pix)

        sql_subrequest = []

        if 'points' in snap_rules:
            for node in snap_rules['points']:
                sql_subrequest.append("""
                    select (st_dumppoints(geom)).geom as geom
                    from {model}.{table}""".format(
                    model=self.__project.get_current_model().name,
                    table=node))
        if 'contours' in snap_rules:
            for contour in snap_rules['contours']:
                sql_subrequest.append("""
                    select (st_dumppoints({column})).geom as geom
                    from {model}.{table}""".format(
                    column='geom' if contour=='station' else 'contour',
                    model=self.__project.get_current_model().name,
                    table=contour))
        if len(sql_subrequest)>0:
            sql_request = """
                with points as ({})
                select st_x(geom), st_y(geom) from points
                """.format(" union ".join(sql_subrequest))
            for x, y in self.__project.fetchall(sql_request):
                snapper.add_point((x,y))

        if 'lines' in snap_rules:
            for line in snap_rules['lines']:
                for id, x1, y1, x2, y2 in self.__project.fetchall("""
                        with
                        dumps as (select id, st_dumppoints(geom) as pt from {model}.{table}),
                        pts as (select id, (pt).geom, (pt).path[1] as vert from dumps)
                        select a.id, st_x(a.geom), st_y(a.geom), st_x(b.geom),st_y(b.geom)
                        from pts a, pts b
                        where a.id = b.id and a.vert = b.vert-1 and b.vert > 1;
                        """.format(
                        model=self.__project.get_current_model().name,
                        table=line)):
                    snapper.add_segment(((x1,y1), (x2,y2)), id)
        return snapper

    def __refresh_layers(self, names=None):
        for layer in self.__iface.mapCanvas().layers():
            if (names is None) or (layer.name() in names) or (layer.name() == TablesProperties.get_properties()['invalid']['name']) or (layer.name() == TablesProperties.get_properties()['configured_current']['name']):
                layer.triggerRepaint()

    def __auto_node_on_reach(self, point):
        '''if there is a reach but no node under the point, creates a river node there'''
        node_exists, = self.__project.fetchone("""
                                                    select exists(
                                                        select 1 from {m}._node where ST_DWithin(geom, {g}, 0.1)
                                                        )""".format(
                                                    m=self.__project.get_current_model().name,
                                                    g=self.__project.get_current_model().make_point(point)
                                                    ))
        reach_exists, = self.__project.fetchone("""
                                                    select exists(
                                                        select 1 from {m}.reach where ST_DWithin(geom, {g}, 0.1)
                                                        )""".format(
                                                    m=self.__project.get_current_model().name,
                                                    g=self.__project.get_current_model().make_point(point)
                                                    ))
        if not node_exists and reach_exists:
            self.__project.get_current_model().add_river_node(point)

    def __river_node(self):
        from hydra.gui.forms.river_node import RiverNodeEditor
        map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
        snap = SettingsProperties.get_snap()
        snapper = Snapper(snap*map_per_pix)
        for id, x1, y1, x2, y2 in self.__project.fetchall("""
                with
                dumps as (
                  select id, st_dumppoints(geom) as pt from {model}.reach
                ),
                pts as (
                  select id, (pt).geom, (pt).path[1] as vert from dumps
                )
                select a.id, st_x(a.geom), st_y(a.geom), st_x(b.geom),st_y(b.geom)
                from pts a, pts b
                where a.id = b.id and a.vert = b.vert-1 and b.vert > 1;
                """.format(model=self.__project.get_current_model().name)):
            snapper.add_segment(((x1,y1), (x2,y2)), id)

        point = create_point(self.__iface.mapCanvas(), self.__project.log, self.__project.terrain, snapper)
        if point:
            cur_cfg = self.__project.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__project.get_current_model().set_current_configuration(1)
            RiverNodeEditor(self.__project, point).exec_()
            if cur_cfg != 1:
                self.__project.get_current_model().set_current_configuration(cur_cfg)
            self.layers_changed_signal.emit(['river_node'])

    def __add_to_terrain_group(self):
        ''' get selected points in points_xyz layer and change group to the group given by the user'''
        points = []
        active_layer = self.__iface.mapCanvas().currentLayer()
        if active_layer and QgsDataSourceUri(active_layer.source()).schema() == "project" and QgsDataSourceUri(active_layer.source()).table() == "points_xyz" and active_layer.selectedFeatureCount() > 0:
            for f in active_layer.selectedFeatures():
                points.append(f['id'])
        else:
            QMessageBox.information(self, tr("""No points found""") , tr("""Please select layer Terrain points and some points to affect them to a group"""))

        groups = self.__project.fetchall("""select name, id from project.points_type""")
        # add capacity to desaffect group
        groups.sort(key=lambda x: x[0])
        groups += [(' No group', 'NULL')]

        if points and groups:
            group, ok = QInputDialog.getItem(self.__iface.mainWindow(), "Group selection", "Select which group to affect", [group[0] for group in groups], 0, False)
            if group and ok:
                group_id = [gp[1] for gp in groups if gp[0] == group][0]
                self.__project.execute("""update project.points_xyz
                                        set points_id = {group_id}
                                        where id IN ({list_id})""".format(group_id=group_id, list_id=",".join([str(point) for point in points])))
         
                renderer = active_layer.renderer()
                if isinstance(renderer, QgsCategorizedSymbolRenderer):
                    categories = renderer.categories()
                    symbol = categories[0].symbol().clone()
                    for category in categories:
                        if category.value() == str(group_id):
                            symbol = None
                            break
                    if symbol is not None:
                        group_name = self.__project.execute(f"""SELECT name FROM project.points_type WHERE id = {group_id}""").fetchone()[0]
                        symbol.setColor(QColor(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)))
                        new_category = QgsRendererCategory(group_id, symbol, group_name)
                        renderer.addCategory(new_category)
                        active_layer.triggerRepaint()
                        self.__iface.layerTreeView().refreshLayerSymbology(active_layer.id())
        
        self.__project.commit()

    def __river_reach(self):
        from hydra.gui.forms.river_reach import RiverReachEditor
        snapper = self.__get_snapper(TablesProperties.get_properties()['reach']['snap'])
        line = create_line(self.__iface.mapCanvas(),
            self.__project.log, self.__project.terrain, snapper)
        if line:
            if self.__project.get_current_model().try_merge_reach(line):
                self.__project.commit()
            else:
                RiverReachEditor(self.__project, line).exec_()
            self.layers_changed_signal.emit(['reach', 'river_node', 'coverage', 'constrain'])

    def __change_select_to_hydraulic(self):
        model_name = self.__project.get_current_model().name

        points = []
        active_layer = self.__iface.mapCanvas().currentLayer()
        if active_layer and QgsDataSourceUri(active_layer.source()).schema() == model_name and QgsDataSourceUri(active_layer.source()).table() == "manhole_hydrology_node" and active_layer.selectedFeatureCount() > 0:
            for f in active_layer.selectedFeatures():
                points.append(f['id'])

        if points:
            node_network_id, = self.__project.fetchone('''select {model}.find_minimal_hydrology_network('{points}')'''.format(
                model=model_name, points="{"+",".join([str(pt) for pt in points])+"}"))

            missing = list(set(node_network_id)-set(points))
            if missing:
                active_layer.selectByIds(node_network_id)
                diff_in_select = QMessageBox.question(self, tr('Selection is not complete'), tr("The Hydrology network is not complete, you need to select more nodes : {}, do you wish to continue with this extended selection ?".format([str(id) for id in missing])), QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
                if diff_in_select != QMessageBox.Yes:
                    #print("return")
                    return
            hydrology_converter = HydrologyConverter(self.__project, self.__project.get_current_model().name)
            singularities = hydrology_converter.check_for_singularity(node_network_id)
            if singularities:
                m_box = QMessageBox(self)
                m_box.setWindowTitle(tr("Singularities detected"))
                m_box.setTextFormat(Qt.RichText)
                m_box.setText(tr("Please remove singularities from nodes with ids : [{}]".format(",".join([str(id) for id in singularities]))))
                m_box.exec_()
                return
            hydrology_converter.convert_to_hydraulic_network(node_network_id)
            hydrology_converter.update_data_hydro()
            hydrology_converter.add_hydrograph_to_hydraulics_routing()
            self.__project.commit()
            m_box = QMessageBox(self)
            m_box.setWindowTitle(tr("Hydrology Converted"))
            m_box.setTextFormat(Qt.RichText)
            m_box.setText(tr(hydrology_converter.message()))
            m_box.exec_()

    def __regul_gate_link(self):
        from hydra.gui.forms.regul_gate_link import RegulGateLinkEditor
        snapper = self.__get_snapper(TablesProperties.get_properties()['regul_gate_link']['snap'])
        line = create_segment(self.__iface.mapCanvas(),
            self.__project.log, self.__project.terrain, snapper)
        if line:
            map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
            snap = SettingsProperties.get_snap()
            cur_cfg = self.__project.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__project.get_current_model().set_current_configuration(1)
            RegulGateLinkEditor(self.__project, line, None, self, self.__iface, map_per_pix*snap).exec_()
            if cur_cfg != 1:
                self.__project.get_current_model().set_current_configuration(cur_cfg)
            self.layers_changed_signal.emit(['regul_gate_link'])

    def __regul_gate_singularity(self):
        from hydra.gui.forms.regul_gate_singularity import RegulGateSingularityEditor
        snapper = self.__get_snapper(TablesProperties.get_properties()['regul_sluice_gate_singularity']['snap'])
        point = create_point(self.__iface.mapCanvas(),
            self.__project.log, self.__project.terrain, snapper)
        if point:
            map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
            snap = SettingsProperties.get_snap()
            cur_cfg = self.__project.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__project.get_current_model().set_current_configuration(1)
            RegulGateSingularityEditor(self.__project, point, None, self, self.__iface, map_per_pix*snap).exec_()
            if cur_cfg != 1:
                self.__project.get_current_model().set_current_configuration(cur_cfg)
            self.layers_changed_signal.emit(['regul_sluice_gate_singularity'])

    def __qq_split_hydrology_singularity(self):
        from hydra.gui.forms.qq_split_hydrology_singularity import QqSplitHydrologySingularityEditor
        snapper = self.__get_snapper(TablesProperties.get_properties()['qq_split_hydrology_singularity']['snap'])
        point = create_point(self.__iface.mapCanvas(),
            self.__project.log, self.__project.terrain, snapper)
        if point:
            map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
            snap = SettingsProperties.get_snap()
            cur_cfg = self.__project.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__project.get_current_model().set_current_configuration(1)
            QqSplitHydrologySingularityEditor(self.__project, point, None, self, self.__iface, map_per_pix*snap).exec_()
            if cur_cfg != 1:
                self.__project.get_current_model().set_current_configuration(cur_cfg)
            self.layers_changed_signal.emit(['qq_split_hydrology_singularity'])

    def __zq_split_hydrology_singularity(self):
        from hydra.gui.forms.zq_split_hydrology_singularity import ZqSplitHydrologySingularityEditor
        snapper = self.__get_snapper(TablesProperties.get_properties()['zq_split_hydrology_singularity']['snap'])
        point = create_point(self.__iface.mapCanvas(),
            self.__project.log, self.__project.terrain, snapper)
        if point:
            map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
            snap = SettingsProperties.get_snap()
            cur_cfg = self.__project.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__project.get_current_model().set_current_configuration(1)
            ZqSplitHydrologySingularityEditor(self.__project, point, None, self, self.__iface, map_per_pix*snap).exec_()
            if cur_cfg != 1:
                self.__project.get_current_model().set_current_configuration(cur_cfg)
            self.layers_changed_signal.emit(['zq_split_hydrology_singularity'])

    def __reservoir_rs_hydrology_singularity(self):
        from hydra.gui.forms.reservoir_rs_hydrology_singularity import ReservoirRsHydrologySingularityEditor
        snapper = self.__get_snapper(TablesProperties.get_properties()['reservoir_rs_hydrology_singularity']['snap'])
        point = create_point(self.__iface.mapCanvas(),
            self.__project.log, self.__project.terrain, snapper)
        if point:
            map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
            snap = SettingsProperties.get_snap()
            cur_cfg = self.__project.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__project.get_current_model().set_current_configuration(1)
            ReservoirRsHydrologySingularityEditor(self.__project, point, None, self, self.__iface, map_per_pix*snap).exec_()
            if cur_cfg != 1:
                self.__project.get_current_model().set_current_configuration(cur_cfg)
            self.layers_changed_signal.emit(['reservoir_rs_hydrology_singularity'])

    def __reservoir_rsp_hydrology_singularity(self):
        from hydra.gui.forms.reservoir_rsp_hydrology_singularity import ReservoirRspHydrologySingularityEditor
        snapper = self.__get_snapper(TablesProperties.get_properties()['reservoir_rsp_hydrology_singularity']['snap'])
        point = create_point(self.__iface.mapCanvas(),
            self.__project.log, self.__project.terrain, snapper)
        if point:
            map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
            snap = SettingsProperties.get_snap()
            cur_cfg = self.__project.get_current_model().get_current_configuration()
            if cur_cfg != 1:
                self.__project.get_current_model().set_current_configuration(1)
            ReservoirRspHydrologySingularityEditor(self.__project, point, None, self, self.__iface, map_per_pix*snap).exec_()
            if cur_cfg != 1:
                self.__project.get_current_model().set_current_configuration(cur_cfg)
            self.layers_changed_signal.emit(['reservoir_rsp_hydrology_singularity'])

    def __street(self):
        from hydra.gui.forms.street import StreetEditor
        snapper = self.__get_snapper(TablesProperties.get_properties()['street']['snap'])
        line = create_line(self.__iface.mapCanvas(),
            self.__project.log, self.__project.terrain, snapper)
        if line:
            StreetEditor(self.__project, line).exec_()
            self.layers_changed_signal.emit(['street', 'crossroad_node', 'street_link', 'coverage', 'constrain'])

    def __dry_inflow_sector(self):
        from hydra.gui.forms.dry_inflow_sector import DryInflowSectorEditor
        map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
        snap = SettingsProperties.get_snap()
        snapper = Snapper(snap*map_per_pix)
        snapper.add_points(self.__project.fetchall("""
                with points as (
                        select (st_dumppoints(geom)).geom as geom
                        from project.dry_inflow_sector
                        )
                select st_x(geom), st_y(geom) from points
                """))

        polygon = create_polygon(self.__iface.mapCanvas(),
            self.__project.log, self.__project.terrain,snapper)
        if polygon:
            DryInflowSectorEditor(self.__project, polygon).exec_()
            self.layers_changed_signal.emit(['dry_inflow_sector'])

    def __manage_domains(self):
        from hydra.gui.domain_manager import DomainManager
        DomainManager(self.__project).exec_()

    def __insert_node(self):
        from hydra.gui.forms.manhole_node import ManholeNodeEditor
        from hydra.gui.forms.manhole_hydrology_node import ManholeHydrologyNodeEditor
        map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
        snap = SettingsProperties.get_snap()
        snapper = Snapper(snap*map_per_pix)
        for id, x1, y1, x2, y2 in self.__project.fetchall("""
                with
                dumps as (
                  select id, st_dumppoints(geom) as pt from {model}.pipe_link
                ),
                pts as (
                  select id, (pt).geom, (pt).path[1] as vert from dumps
                )
                select a.id, st_x(a.geom), st_y(a.geom), st_x(b.geom),st_y(b.geom)
                from pts a, pts b
                where a.id = b.id and a.vert = b.vert-1 and b.vert > 1;
                """.format(model=self.__project.get_current_model().name)):
            snapper.add_segment(((x1,y1), (x2,y2)), id)
        point = create_point(self.__iface.mapCanvas(), self.__project.log, self.__project.terrain, snapper)
        if point:
            id_node, type_node = self.__project.get_current_model().insert_node_on_pipe(point)
            if id_node and type_node=='manhole':
                ManholeNodeEditor(self.__project, None, id_node).exec_()
                self.layers_changed_signal.emit(['manhole_node', 'pipe_link'])
            elif id_node and type_node=='manhole_hydrology':
                ManholeHydrologyNodeEditor(self.__project, None, id_node).exec_()
                self.layers_changed_signal.emit(['manhole_hydrology_node', 'pipe_link'])
            self.__project.commit()

    def __create_interlink(self):
        '''Generate interlinks in a coverage witgh underlying river nodes'''
        active_layer = self.__iface.mapCanvas().currentLayer()

        coverages = []
        if not active_layer or QgsDataSourceUri(active_layer.source()).table() != 'coverage' or active_layer.selectedFeatureCount() == 0:
            point = create_point(self.__iface.mapCanvas(), self.__project.log)

            res = self.__project.fetchone("""
                select id, domain_type from {model}.coverage
                where st_intersects(geom, 'SRID={srid}; POINT({x} {y})'::geometry)
                """.format(model=self.__project.get_current_model().name, srid=self.__project.srid, x=point[0], y=point[1]))
            if res:
                coverages = [(str(res[0]), str(res[1]))]
        else:
            coverages = [(str(f['id']), str(f['domain_type'])) for f in active_layer.selectedFeatures()]

        # Find models with river nodes in selected coverages
        models = self.__project.get_models()
        valid_models=[]
        for model in models:
            if model != self.__project.get_current_model().name:
                for id, type in coverages:
                    check_for_manholes = self.__project.fetchone("""
                        select exists (select 1 from {model_down}.manhole_node as m, {model_up}.coverage as c
                        where st_intersects(m.geom, c.geom) and c.id={id} limit 1)
                        """.format(model_down=model, model_up=self.__project.get_current_model().name, id=id))
                    if check_for_manholes and check_for_manholes[0]:
                        valid_models.append(model)

        model=None
        if len(valid_models)==1:
            model=valid_models[0]
        elif len(valid_models)>1:
            m, ok = QInputDialog.getItem(self.__iface.mainWindow(), "Model selection", "Found multiple models with manholes in selected coverages", valid_models, 0, False)
            if ok and m:
                model=m
        if model:
            for id, type in coverages:
                if type == 'street':
                    self.__project.execute("""
                        with manhole_nodes as (
                            select m.geom
                            from {model_down}.manhole_node as m, {model_up}.coverage as c
                            where ST_Contains(c.geom, m.geom)
                            and c.id={id}
                        ),
                        crossroads as (
                            select c.geom, c.z_ground
                            from {model_up}.crossroad_node as c, {model_up}.coverage as cov
                            where ST_Contains(cov.geom, c.geom)
                            and cov.id={id}
                        ),
                        nodes as (
                            select m.geom as down_geom,
                                  (select c.geom from crossroads as c order by m.geom <-> c.geom limit 1) as up_geom
                            from manhole_nodes as m
                        )
                        insert into project.interlink(model_up, model_down, geom)
                        select '{model_up}', '{model_down}', ST_MakeLine(ARRAY[n.up_geom, n.down_geom])
                        from nodes as n
                        """.format(model_down=model, model_up=self.__project.get_current_model().name, id=id))
                elif type == 'storage':
                    self.__project.execute("""
                        with nodes as (
                            select m.geom as down_geom, s.geom as up_geom
                            from {model_down}.manhole_node as m, {model_up}.storage_node as s, {model_up}.coverage as c
                            where ST_Contains(c.geom, m.geom)
                            and ST_Contains(c.geom, s.geom)
                            and c.id={id}
                        )
                        insert into project.interlink(model_up, model_down, geom)
                        select '{model_up}', '{model_down}', ST_MakeLine(ARRAY[n.up_geom, n.down_geom])
                        from nodes as n
                        """.format(model_down=model, model_up=self.__project.get_current_model().name, id=id))
                elif type == '2d':
                    self.__project.execute("""
                        with manhole_nodes as (
                            select m.geom
                            from {model_down}.manhole_node as m, {model_up}.coverage as c
                            where ST_Contains(c.geom, m.geom)
                            and c.id={id}
                        ),
                        nodes as (
                            select m.geom as down_geom, e.geom as up_geom
                            from manhole_nodes as m
                            join {model_up}.elem_2d_node as e on ST_Contains(e.contour, m.geom)
                        )
                        insert into project.interlink(model_up, model_down, geom)
                        select '{model_up}', '{model_down}', ST_MakeLine(ARRAY[n.up_geom, n.down_geom])
                        from nodes as n
                        """.format(model_down=model, model_up=self.__project.get_current_model().name, id=id))
            self.__project.commit()
            self.layers_changed_signal.emit([])

    def __fuse_reachs(self):
        from hydra.gui.forms.river_reach import RiverReachEditor
        pu = create_point(self.__iface.mapCanvas(), self.__project.log, self.__project.terrain)
        pd = create_point(self.__iface.mapCanvas(), self.__project.log, self.__project.terrain)

        # see if selection is 2 reaches:
        precision = SettingsProperties.get_snap()
        reach1_id = self.__project.fetchone("""
                select id, name
                from {model}.reach
                where ST_DWithin(geom, {point}, {prec})
                order by ST_Distance(geom, {point}) limit 1
                """.format(
                    model=self.__project.get_current_model().name,
                    point=self.__project.get_current_model().make_point(pu),
                    prec=precision
                    ))
        reach2_id = self.__project.fetchone("""
                select id, name
                from {model}.reach
                where ST_DWithin(geom, {point}, {prec})
                order by ST_Distance(geom, {point}) limit 1
                """.format(
                    model=self.__project.get_current_model().name,
                    point=self.__project.get_current_model().make_point(pd),
                    prec=precision
                    ))
        if reach1_id is not None and reach2_id is not None:
            reply = QMessageBox.question(self, tr('Confirm'), tr("This will merge {} end point and {} startpoint. Confirm ?".format(reach1_id[1], reach2_id[1])), QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if reply == QMessageBox.Yes:
                new_reach_id = self.__project.get_current_model().fuse_reach(reach1_id[0], reach2_id[0])
                RiverReachEditor(self.__project, None, new_reach_id).exec_()
                self.__project.commit()
                self.layers_changed_signal.emit([])

    def __split_reach(self):
        map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
        snap = SettingsProperties.get_snap()
        snapper = Snapper(snap*map_per_pix)
        for id, x1, y1, x2, y2 in self.__project.fetchall("""
                with
                dumps as (
                  select id, ST_Dumppoints(geom) as pt from {model}.reach
                ),
                pts as (
                  select id, (pt).geom, (pt).path[1] as vert from dumps
                )
                select a.id, ST_X(a.geom), ST_Y(a.geom), ST_X(b.geom),ST_Y(b.geom)
                from pts a, pts b
                where a.id = b.id and a.vert = b.vert-1 and b.vert > 1;
                """.format(model=self.__project.get_current_model().name)):
            snapper.add_segment(((x1,y1), (x2,y2)), id)
        point = create_point(self.__iface.mapCanvas(), self.__project.log, self.__project.terrain, snapper)
        if point:
            id, pk, length, name = self.__project.fetchone("""select id, ST_LineLocatePoint(reach.geom, {p}), ST_Length(geom), name from {m}.reach order by ST_Distance({p}, reach.geom) limit 1""".format(
                p=self.__project.get_current_model().make_point(point), m=self.__project.get_current_model().name))
            reply = QMessageBox.question(self, tr('Confirm'), tr("This will split {} at {} km. Confirm ?".format(name, round(pk*length/1000, 2))), QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if reply == QMessageBox.Yes:
                self.__project.get_current_model().split_reach(id, pk)
                self.__project.commit()
                self.layers_changed_signal.emit([])

    def __split_constrain(self):
        map_per_pix = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()
        snap = SettingsProperties.get_snap()
        snapper = Snapper(snap*map_per_pix)
        for id, x1, y1, x2, y2 in self.__project.fetchall("""
                with
                dumps as (
                  select id, ST_Dumppoints(geom) as pt from {model}.constrain
                ),
                pts as (
                  select id, (pt).geom, (pt).path[1] as vert from dumps
                )
                select a.id, ST_X(a.geom), ST_Y(a.geom), ST_X(b.geom),ST_Y(b.geom)
                from pts a, pts b
                where a.id = b.id and a.vert = b.vert-1 and b.vert > 1;
                """.format(model=self.__project.get_current_model().name)):
            snapper.add_segment(((x1,y1), (x2,y2)), id)
        point = create_point(self.__iface.mapCanvas(), self.__project.log, self.__project.terrain, snapper)
        if point:
            id, = self.__project.execute("""select id from {m}.constrain where ST_Intersects(ST_Buffer({p},0.1), constrain.geom)""".format(p=self.__project.get_current_model().make_point(point), m=self.__project.get_current_model().name)).fetchone()
            self.__project.execute("""select {m}.split_constrain({i}, {p});""".format(m=self.__project.get_current_model().name, i=id, p=self.__project.get_current_model().make_point(point)))
            self.__project.commit()
            self.layers_changed_signal.emit([])

    def __delete_node(self):
        from hydra.gui.select_menu import SelectTool
        from hydra.gui.forms.pipe_link import PipeLinkEditor

        def on_left_click(point):
            geom = VisibleGeometry(self.__iface.mapCanvas(), QgsWkbTypes.PointGeometry)
            geom.add_point(point)
            snap = SettingsProperties.get_snap()
            size = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()*snap
            tool = SelectTool(self.__project, point, size, subset=[(self.__project.get_current_model().name, 'manhole_node'), (self.__project.get_current_model().name, 'manhole_hydrology_node')])
            if tool.table is None:
                return
            pipe_up = [i for i, in self.__project.fetchall("""select id from {}.pipe_link where up={}""".format(self.__project.get_current_model().name, tool.id))]
            pipe_down = [i for i, in self.__project.fetchall("""select id from {}.pipe_link where down={}""".format(self.__project.get_current_model().name, tool.id))]
            if len(pipe_up)==1 and len(pipe_down)==1:
                reply = QMessageBox.question(self, tr('Confirm'), tr("Are you sure you want to delete this item?"), QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
                if reply == QMessageBox.Yes:
                    id_pipe = self.__project.get_current_model().delete_node_update_pipe(tool.id, tool.table)
                    PipeLinkEditor(self.__project, None, id_pipe).exec_()
                    self.__project.commit()
                    self.layers_changed_signal.emit([])

        def on_right_click(point):
            self.__iface.mapCanvas().setMapTool(self.prev_tool)

        self.__current_tool = MapPointTool(self.__iface.mapCanvas(), self.__project.log)
        self.__current_tool.leftClicked.connect(on_left_click)
        self.__current_tool.rightClicked.connect(on_right_click)
        self.__iface.mapCanvas().setMapTool(self.__current_tool)

        while self.__iface.mapCanvas().mapTool() == self.__current_tool:
            QApplication.instance().processEvents()

        if self.__current_tool:
            self.__iface.mapCanvas().unsetMapTool(self.__current_tool)
            self.__current_tool=None

    def __reverse_link(self):
        from hydra.gui.select_menu import SelectTool

        def on_left_click(point):
            geom = VisibleGeometry(self.__iface.mapCanvas(), QgsWkbTypes.PointGeometry)
            geom.add_point(point)
            snap = SettingsProperties.get_snap()
            size = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()*snap
            subset = [(self.__project.get_current_model().name, i+'_link') for i, in self.__project.fetchall("""select name from hydra.link_type""")]
            tool = SelectTool(self.__project, point, size, subset=subset)
            if tool.table is None:
                return
            self.__project.get_current_model().reverse_link(tool.table, tool.id)
            self.__project.commit()
            self.layers_changed_signal.emit([])

        def on_right_click(point):
            self.__iface.mapCanvas().setMapTool(self.prev_tool)

        self.__current_tool = MapPointTool(self.__iface.mapCanvas(), self.__project.log)
        self.__current_tool.leftClicked.connect(on_left_click)
        self.__current_tool.rightClicked.connect(on_right_click)
        self.__iface.mapCanvas().setMapTool(self.__current_tool)

        while self.__iface.mapCanvas().mapTool() == self.__current_tool:
            QApplication.instance().processEvents()

        if self.__current_tool:
            self.__iface.mapCanvas().unsetMapTool(self.__current_tool)
            self.__current_tool=None

    def __del__(self):
        for button in self.findChildren(QToolButton):
            try:
                button.clicked.disconnect()
            except Exception:
                pass
        self.__iface.removeDockWidget(self)
