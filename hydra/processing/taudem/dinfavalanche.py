# -*- coding: utf-8 -*-

"""
***************************************************************************
    dinfavalanche.py
    ---------------------
    Date                 : January 2018
    Copyright            : (C) 2018-2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'January 2018'
__copyright__ = '(C) 2018-2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.core import (QgsProcessingException,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterBoolean,
                       QgsProcessingParameterRasterDestination
                      )
from .taudemAlgorithm import TauDemAlgorithm
from . import taudemUtils


class DinfAvalanche(TauDemAlgorithm):

    PIT_FILLED = 'PIT_FILLED'
    DINF_FLOWDIR = 'DINF_FLOWDIR'
    AVALANCHE_SOURCE = 'AVALANCHE_SOURCE'
    THRESHOLD = 'THRESHOLD'
    ANGLE = 'ANGLE'
    DIRECT = 'DIRECT'
    AVALANCHE_RUNOUT = 'AVALANCHE_RUNOUT'
    DISTANCE_DOWN = 'DISTANCE_DOWN'

    def name(self):
        return 'dinfavalanche'

    def displayName(self):
        return self.tr('D-infinity avalanche runout')

    def group(self):
        return self.tr('Specialized grid analysis')

    def groupId(self):
        return 'specializedanalysis'

    def tags(self):
        return self.tr('dem,hydrology,d-infinity,avalanche').split(',')

    def shortHelpString(self):
        return self.tr('Identifies an avalanche\'s affected area and the flow '
                       'path length to each cell in that affected area.')

    def helpUrl(self):
        return 'http://hydrology.usu.edu/taudem/taudem5/help53/DInfinityAvalancheRunout.html'

    def __init__(self):
        super().__init__()

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterRasterLayer(self.PIT_FILLED,
                                                            self.tr('Pit filled elevation')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.DINF_FLOWDIR,
                                                            self.tr('D-infinity flow directions')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.AVALANCHE_SOURCE,
                                                            self.tr('Avalanche source site')))
        self.addParameter(QgsProcessingParameterNumber(self.THRESHOLD,
                                                       self.tr('Proportion threshold'),
                                                       QgsProcessingParameterNumber.Double,
                                                       defaultValue=0.2,
                                                       optional=True))
        self.addParameter(QgsProcessingParameterNumber(self.ANGLE,
                                                       self.tr('Alpha angle threshold'),
                                                       QgsProcessingParameterNumber.Double,
                                                       defaultValue=18,
                                                       optional=True))
        self.addParameter(QgsProcessingParameterBoolean(self.DIRECT,
                                                        self.tr('Measure distance as a straight line'),
                                                        defaultValue=False))

        self.addParameter(QgsProcessingParameterRasterDestination(self.AVALANCHE_RUNOUT,
                                                                  self.tr('Runout zone')))
        self.addParameter(QgsProcessingParameterRasterDestination(self.DISTANCE_DOWN,
                                                                  self.tr('Path distance')))

    def processAlgorithm(self, parameters, context, feedback):
        demLayer = self.parameterAsRasterLayer(parameters, self.PIT_FILLED, context)
        if demLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.PIT_FILLED))

        flowLayer = self.parameterAsRasterLayer(parameters, self.DINF_FLOWDIR, context)
        if flowLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.DINF_FLOWDIR))

        avalancheLayer = self.parameterAsRasterLayer(parameters, self.AVALANCHE_SOURCE, context)
        if avalancheLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.AVALANCHE_SOURCE))

        arguments = []
        arguments.append(self.command())

        arguments.append('-fel')
        arguments.append(demLayer.source())
        arguments.append('-ang')
        arguments.append(flowLayer.source())
        arguments.append('-ass')
        arguments.append(avalancheLayer.source())

        if self.THRESHOLD in parameters and parameters[self.THRESHOLD] is not None:
            arguments.append('-thresh')
            arguments.append('{}'.format(self.parameterAsDouble(parameters, self.THRESHOLD, context)))

        if self.ANGLE in parameters and parameters[self.ANGLE] is not None:
            arguments.append('-alpha')
            arguments.append('{}'.format(self.parameterAsDouble(parameters, self.ANGLE, context)))

        if self.parameterAsBool(parameters, self.DIRECT, context):
            arguments.append('-direct')

        arguments.append('-rz')
        arguments.append(self.parameterAsOutputLayer(parameters, self.AVALANCHE_RUNOUT, context))

        arguments.append('-dfs')
        arguments.append(self.parameterAsOutputLayer(parameters, self.DISTANCE_DOWN, context))

        taudemUtils.execute(arguments, feedback)

        return self.algorithmResults(parameters)
