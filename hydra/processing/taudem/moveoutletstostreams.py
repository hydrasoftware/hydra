# -*- coding: utf-8 -*-

"""
***************************************************************************
    moveoutletstostreams.py
    ---------------------
    Date                 : January 2018
    Copyright            : (C) 2018-2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'January 2018'
__copyright__ = '(C) 2018-2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.core import (QgsVectorFileWriter,
                       QgsProcessing,
                       QgsProcessingException,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterVectorDestination,
                      )

from .taudemAlgorithm import TauDemAlgorithm
from . import taudemUtils


class MoveOutletsToStreams(TauDemAlgorithm):

    D8_FLOWDIR = 'D8_FLOWDIR'
    STREAM_RASTER = 'STREAM_RASTER'
    OUTLETS = 'OUTLETS'
    CELLS_TRAVERSE = 'CELLS_TRAVERSE'
    MOVED_OUTLETS = 'MOVED_OUTLETS'

    def name(self):
        return 'moveoutletstostreams'

    def displayName(self):
        return self.tr('Move outlets to Streams')

    def group(self):
        return self.tr('Stream network analysis')

    def groupId(self):
        return 'streamanalysis'

    def tags(self):
        return self.tr('dem,hydrology,stream,outlet').split(',')

    def shortHelpString(self):
        return self.tr('Moves outlet points that are not aligned with '
                       'a stream cell from a stream raster grid, downslope '
                       'along the D8 flow direction until a stream raster '
                       'cell is encountered.')

    def helpUrl(self):
        return 'http://hydrology.usu.edu/taudem/taudem5/help53/MoveOutletsToStreams.html'

    def __init__(self):
        super().__init__()

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterRasterLayer(self.D8_FLOWDIR,
                                                            self.tr('D8 flow directions')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.STREAM_RASTER,
                                                            self.tr('Stream raster')))
        self.addParameter(QgsProcessingParameterFeatureSource(self.OUTLETS,
                                                              self.tr('Outlets'),
                                                              types=[QgsProcessing.TypeVectorPoint]))
        self.addParameter(QgsProcessingParameterNumber(self.CELLS_TRAVERSE,
                                                       self.tr('Maximum number of grid cells to traverse'),
                                                       QgsProcessingParameterNumber.Integer,
                                                       minValue=0,
                                                       defaultValue=50))

        self.addParameter(QgsProcessingParameterVectorDestination(self.MOVED_OUTLETS,
                                                                  self.tr('Moved outlets'),
                                                                  QgsProcessing.TypeVectorPoint))

    def processAlgorithm(self, parameters, context, feedback):
        d8Layer = self.parameterAsRasterLayer(parameters, self.D8_FLOWDIR, context)
        if d8Layer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.D8_FLOWDIR))

        streamLayer = self.parameterAsRasterLayer(parameters, self.STREAM_RASTER, context)
        if streamLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.STREAM_RASTER))

        outlets = self.parameterAsSource(parameters, self.OUTLETS, context)
        if outlets is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.OUTLETS))

        arguments = []
        arguments.append(self.command())

        arguments.append('-p')
        arguments.append(d8Layer.source())
        arguments.append('-src')
        arguments.append(streamLayer.source())

        outlets = self.parameterAsCompatibleSourceLayerPath(parameters, self.OUTLETS, context,
                                                            QgsVectorFileWriter.supportedFormatExtensions(),
                                                            feedback=feedback)
        arguments.append('-o')
        arguments.append(outlets)

        arguments.append('-md')
        arguments.append('{}'.format(self.parameterAsInt(parameters, self.CELLS_TRAVERSE, context)))

        arguments.append('-om')
        arguments.append(self.parameterAsOutputLayer(parameters, self.MOVED_OUTLETS, context))

        taudemUtils.execute(arguments, feedback)

        return self.algorithmResults(parameters)
