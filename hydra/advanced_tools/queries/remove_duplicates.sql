/* ****************************************** */
/* Remove duplicates based on geometry        */
/* Warning: you must select the geom column   */
/* where you want to perform the query        */
/* ****************************************** */

alter table work.$work_table add column if not exists id serial;
;;

delete from work.$work_table where id in (
    select min(id)
    from work.$work_table
    where $work_table_geom_column in (
        select $work_table_geom_column
        from work.$work_table
        group by $work_table_geom_column having count(*) > 1
        )
    group by $work_table_geom_column
    )
;;