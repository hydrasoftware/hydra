# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from setuptools import setup
import re
import os
import sys

__currendir = os.path.dirname(__file__)

__version__ = '.'.join(map(str, max([tuple(map(int, f.split('-')[1].replace('.sql', '').split('.')))
        for f in os.listdir(os.path.join(os.path.dirname(__file__), '..', 'versions'))
        if re.match('.+-(\d+)\.(\d+).(\d+)\.sql$' , f)])))

if sys.version_info < (3,0):
    sys.exit('Sorry, Python < 3 is not supported')

hydra_module = 'hydra_'+'_'.join(__version__.split('.'))

setup(name='hydra_'+'_'.join(__version__.split('.')),
      version=__version__,
      description='hydrology model, postgres server module',
      author='hydratec',
      author_email='contact@hydra-software.net',
      url='http://hydra-software.net/',
      packages=[hydra_module],
      package_dir={hydra_module: 'hydra'},
      license='MIT',
      package_data={hydra_module: ['*.sql', '*.json']},
      include_package_data=True,
      zip_safe=False
     )
