/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update hydra.metadata set version = '2.5';
;;

create or replace function project.model_instead_fct()
returns trigger
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_model
    for statement in create_model(TD['new']['name'], $srid, '$version'):
        plpy.execute(statement)
$$$$
;;

do
$$$$
    begin
    if exists (
        select 1 from pg_catalog.pg_namespace where nspname = 'csv'
    )
    then
      execute 'insert into csv.hydra_compatible_versions(hydra_version) values (''2.5'')';
    end if;
end
$$$$;
;;

/* ********************************************** */
/*  RACC update                                   */
/* ********************************************** */

--isolated--
alter type hydra_model_connect_mode add value if not exists 'tz_downstream_condition';
;;

insert into hydra.model_connect_mode (id, name, description) values (3, 'tz_downstream_condition', 'Downstream condition: Z(t)');
;;

/* ********************************************** */
/*  Active scenario in DB                         */
/* ********************************************** */

create table project.metadata(
    current_scenario integer references project.scenario(id) on delete set null,
    id integer unique not null default 1 check (id=1) -- Only one row
)
;;

insert into project.metadata values (null, 1)
;;