/* ********************************************************************************** */
/* Update all links crossing a 3D_constrain with a group_points:                      */
/*     + will update all links crossing a 3D                                          */
/*     + may need to refine this query to have selected behaviour                     */
/*     + (select constrain by id or target links with generated number only)          */
/* ********************************************************************************** */

with links as (
    select _link.id
    from $model._link, $model.constrain
    where St_intersects(_link.geom, constrain.geom)
    and constrain.points_xyz is not null
)
select $model.set_link_altitude_topo(links.id)
from links
;;