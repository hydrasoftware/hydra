from .project import Project
from pathlib import Path
import shutil
from zipfile import ZipFile
import hashlib
import os
import tempfile
from collections import defaultdict
from datetime import datetime, timedelta
import re

def timedelta_from(value, date0=None):
    if value.lower() == 'nul':
        return None
    dt = (datetime.fromisoformat(value) - date0) \
            if value.find('T') != -1 else timedelta(hours=float(value))
    return timedelta(days=dt.days, seconds=round(dt.seconds/60)*60)

def file_blocks(cmd_file):
    blocks = defaultdict(list)
    with open(cmd_file) as cmd:
        current_block = None
        for line in cmd:
            if line.startswith('*'):
                current_block = line.strip()[1:].lower()
            else:
                line = re.sub(r"(\d\d\d\d) +(\d\d) +(\d\d) +(\d\d) +(\d\d)",
                            r"\1-\2-\3T\4:\5", line.strip()).replace("'", "")
                blocks[current_block].append(line)

    return blocks

class Scenario(dict):
    def __getattr__(self, key):
        return self[key]

    def __setattr__(self, key, value):
        self[key] = value

    def parse_time(self, block):
        self.date0 = datetime.fromisoformat(block[0])
        self.tfin_hr = timedelta_from(block[1], self.date0)
        splt = block[2].split()
        self.tbegin_output_hr = timedelta_from(splt[0], self.date0)
        self.tend_output_hr = timedelta_from(splt[1], self.date0)
        self.dt_output_hr = timedelta_from(splt[2])

    def parse_hydrol(self, block):
        #TODO charger les dry_inflow et rainfall
        #splt = block[0].split()
        #self.dry_inflow, self.rainfall = int(splt[0]), int(splt[1])
        self.dt_hydrol_mn = timedelta(seconds=float(block[1])*60)
        self.soil_cr_alp, self.soil_horner_sat, self.soil_holtan_sat, \
                self.soil_scs_sat, self.soil_hydra_psat, \
                self.soil_hydra_rsat, self.soil_gr4_psat, \
                self.soil_gr4_rsat = [float(v) for v in block[2].split()]
        self.option_dim_hydrol_network = int(block[3]) == 1

    def parse_hydrau(self, block):
        self.tini_hydrau_hr = timedelta(seconds=float(block[0])*3600)
        splt = block[1].split()
        self.dtmin_hydrau_hr = timedelta(seconds=float(splt[0])*3600)
        self.dtmax_hydrau_hr = timedelta(seconds=float(splt[1])*3600)
        splt = block[2].split()
        self.dzmin_hydrau = float(splt[0])
        self.dzmax_hydrau = float(splt[1])

    def parse_save(self, block):
        self.flag_save = int(block[0]) == 1
        self.tsave_hr = timedelta_from(block[1], self.date0)

    def parse_rstart(self, block, project):
        self.flag_rstart = int(block[0]) == 1
        self.scenario_rstart, = project.fetchone(
                "select id from project.scenario where name ilike %s", (block[1],)) or (None,)
        self.trstart_hr = timedelta_from(block[2], self.date0) or timedelta()

    def parse_hydrol_rst(self, block, project):
        self.flag_hydrology_rstart = int(block[0]) == 1
        self.scenario_hydrology_rstart, = project.fetchone(
                "select id from project.scenario where name ilike %s", (block[1],)) or (None,)

    def __init__(self, name, project, cmd_file, optsor_file):
        """parse cmd and optsor to retrive scenario parameters"""
        
        self.name = name
        #TODO récupérer le scénario de référence dans scenario.nom
        self.scenario_ref, = project.fetchone("""
            select id from project.scenario where name ilike 'REF'
            """)

        # traitement du .cmd

        blocks = file_blocks(cmd_file)

        if 'hydrol' in blocks and 'hydrau' in blocks:
            self.comput_mode = 'hydrology_and_hydraulics'
        elif 'hydrol' in blocks:
            self.comput_mode = 'hydrology'
        else:
            assert 'hydrau' in blocks
            self.comput_mode = 'hydraulics'

        self.flag_hydrology_auto_dimensioning = 'dim_hydrol' in blocks
        self.parse_time(blocks['time'])
        if 'hydrol' in blocks:
            self.parse_hydrol(blocks['hydrol'])
        if 'hydrau' in blocks:
            self.parse_hydrau(blocks['hydrau'])
        if 'save' in blocks:
            self.parse_save(blocks['save'])
        if 'rstart' in blocks:
            self.parse_rstart(blocks['rstart'], project)
        if 'hydrol_rstart' in blocks:
            self.parse_hydrol_rst(blocks['hydrol_rstart'], project)

        # TODO implementer les options qualité et sédiment
        assert 'option_sediment' not in blocks
        assert 'option_pollution' not in blocks
        assert 'option_quality1' not in blocks
        assert 'option_quality2' not in blocks
        assert 'option_quality3' not in blocks

        # traitement du _optsor.dat
        # blocks = file_blocks(optsor_file)
        # assert 'c_affin' not in blocks
        # assert 'strickl' not in blocks
        # assert 'runoff_2d' not in blocks
        # assert 'sor1' not in blocks #TODO à traiter
        # assert 'vent' not in blocks
        # assert 'version_kernel' not in blocks
        # assert 'deroutage_apport' not in blocks
        # assert 'f_hy' not in blocks
        # assert 'f_regul' not in blocks
        # assert 'f_mesures' not in blocks
        # assert 'f_mesures' not in blocks
        # assert 'aeraulics' not in blocks
        # assert 'f_pluvio_new' not in blocks
        # assert 'f_pluvio' not in blocks

class ReferenceScenarioMismatch(Exception):
    def __init__(self, message):
        super().__init__(message)


def import_(
        project_name: str,
        zip_file: Path,
        flavor: str=None,
        overwrite_ref=False
        ):
    """import scenario from zip file comming from various realtime installations of hydra"""

    if flavor is None:
        if project_name.find('gao') != -1:
            flavor = 'gao'
        elif project_name.find('hydreaulys') != -1:
            flavor = 'hydreaulys'
        else:
            raise RuntimeError("cannot determine flavor from project name")

    project = Project(project_name)
    project_dir = Path(project.directory)
    zip_file = Path(zip_file)
    
    if overwrite_ref and (project_dir / 'REF').exists():
        shutil.rmtree(project_dir / 'REF')

    if flavor == 'gao':

        tmp_dir = Path(tempfile.gettempdir()) / zip_file.stem
        if tmp_dir.exists():
            shutil.rmtree(tmp_dir)
        with ZipFile(zip_file) as zf:

            if all([f.startswith(zip_file.stem) for f in zf.namelist()]): # démarrage à frois, il y a un sous-répertoire
                zf.extractall(tempfile.gettempdir())
            else:
                for f in zf.namelist():
                    o_path = tmp_dir / Path(f.replace('\\', '/'))
                    if not o_path.parent.exists():
                        os.makedirs(o_path.parent)
                    if not f.endswith('\\'):
                        with zf.open(f) as i, open(o_path, 'wb') as o:
                            o.write(i.read())

        differences = []
        # on verifie que le scenario REF est identique
        # et on détermine le nom du scenario
        for path in tmp_dir.rglob('*'):
            ref = Path(project_dir, *path.parts[len(tmp_dir.parts):])
            if not overwrite_ref and ref.parts[len(project_dir.parts)] == 'REF' and ref.is_file():
                with open(path, 'rb') as f, open(ref, 'rb') as r:
                    md5_ref = hashlib.md5(r.read()).hexdigest()
                    md5 = hashlib.md5(f.read()).hexdigest()
                    if md5 != md5_ref:
                        differences.append(str(Path(*ref.parts[len(project_dir.parts):])))
            if path.name == 'scenario.nom':
                blocks = file_blocks(path)
                scenario = blocks['scenario'][0]

        assert scenario

        if not overwrite_ref and len(differences):
            # TODO utiliser une classe spécifique
            raise ReferenceScenarioMismatch(f"differences found in REF scenario in {path}: {', '.join(differences)}")

        scn_dir = project_dir / scenario.upper()
        if scn_dir.exists():
            shutil.rmtree(scn_dir)
        
        shutil.move(tmp_dir / 'RUN', scn_dir)
        for path in scn_dir.rglob('*'):
            if path.name.find(scenario) != -1:
                path.rename(path.parent / path.name.replace(scenario, scenario.upper()))

        if overwrite_ref:
            shutil.move(tmp_dir / 'REF', project_dir / 'REF')

        # force la présence du whydram24.err (supprimé par AQVD) e 'hydraulique', 'whydram24.err'
        with open(scn_dir / 'hydraulique' / 'whydram24.err', 'w') as werr:
            werr.write(' 0\n')


        blocks = file_blocks(scn_dir / 'travail' / f'{scenario.upper()}.cmd')
        if 'rstart' in blocks and blocks['rstart'][0] == '1':
            scenario_rst = blocks['rstart'][1]
            if not (project_dir / scenario_rst.upper()).exists():
                shutil.move(tmp_dir / scenario_rst.upper(), project_dir / scenario_rst.upper())
            if not project.fetchone("select 1 from project.scenario where name ilike %s", (scenario_rst,)):
                project.execute("insert into project.scenario(name) values(%s)", (scenario_rst,))

        # create scenario
        scn = Scenario(scenario, project,
                scn_dir / 'travail' / f'{scenario.upper()}.cmd',
                scn_dir / 'travail' / f'{scenario.upper()}_optsor.dat')

        project.execute("delete from project.scenario where name ilike %s", (scenario,))
        scenario_id, = project.execute(f"""
            insert into project.scenario({','.join(scn.keys())})
            values ({','.join(['%s']*len(scn.keys()))})
            returning id
            """, list(scn.values())).fetchone()
        project.commit()

        #for k, v in scn.items():
        #    print(k,':',v)

        shutil.rmtree(tmp_dir)

    else:
        raise RuntimeError(f'invalid flavor {flavor} (not in nydreaulys, gao)')

    return scenario, scenario_id


if __name__=='__main__':
    import sys
    project_name = sys.argv[1]
    zipfile = sys.argv[2]
    flavor = sys.argv[3] if len(sys.argv) > 3 else None
    import_(project_name, zipfile, flavor, overwrite_ref=False)



