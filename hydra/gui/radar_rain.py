# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QDialog, QMessageBox
from qgis.gui import QgsProjectionSelectionDialog
from hydra.gui.base_dialog import BaseDialog, tr

class RadarRainDialog(QDialog):
    def __init__(self, parent = None, name='', srid=2154, template=12*'?'):
        QDialog.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "radar_rain.ui"), self)

        self.buttonBox.accepted.connect(self.save)
        self.btn_epsg.clicked.connect(self.select_epsg)

        self.name.setText(name)
        self.srid.setText(str(srid))
        self.template_.setText(template)

        self.__results = [name, srid, template]

    def select_epsg(self):
        projSelector = 	QgsProjectionSelectionDialog()
        projSelector.exec_()
        authId = projSelector.crs().authid()
        if authId[:5]=='EPSG:':
            self.srid.setText(authId[5:])
        else:
            QMessageBox(QMessageBox.Warning, tr('Not a valid SRID'), tr('Please select a SRID with code starting with "ESPG:..."'), QMessageBox.Ok).exec_()

    def save(self):
        if 12*'?' in self.template_.text() and self.name.text() !='':
            self.__results = [self.name.text(), int(self.srid.text()), self.template_.text()]
            self.close()
        else:
            QMessageBox.critical(self, tr('Error'), tr("""Please enter a name and a template containing exactly 12 consecutive ''?''"""), QMessageBox.Ok)

    def get_result(self):
        return self.__results
