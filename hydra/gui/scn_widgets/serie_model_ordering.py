# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
from builtins import str
from builtins import range
from qgis.PyQt.QtWidgets import QWidget, QGridLayout
from qgis.PyQt.QtGui import QStandardItem, QStandardItemModel

from qgis.PyQt import uic
from hydra.gui.base_dialog import tr
import os

class SerieModelOrderingWidget(QWidget):

    def __init__(self, current_project, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "model_ordering.ui"), self)
        tmplayout = QGridLayout()
        tmplayout.addWidget(self,0,0)
        parent.setLayout(tmplayout)
        self.active_serie_id = None
        self.project = current_project

        self.btnAddGroup.clicked.connect(self.add_group)
        self.btnDelGroup.clicked.connect(self.delete_group)
        self.btnAddModelGroup.clicked.connect(self.add_model_to_group)
        self.btnRemoveModelGroup.clicked.connect(self.remove_model_from_group)
        self.btn_up.clicked.connect(self.cascade_up)
        self.btn_down.clicked.connect(self.cascade_down)

        self.__list_model = QStandardItemModel(self.listModels)
        self.__tree_groups = QStandardItemModel(self.treeGroups)
        self.listModels.setModel(self.__list_model)
        self.treeGroups.setModel(self.__tree_groups)

        self.radio_mode_connexion_global.toggled.connect(self.__refresh_stacked)
        self.radio_mode_connexion_mixed.toggled.connect(self.__refresh_lists_groups)
        self.radio_mode_connexion_cascade.toggled.connect(self.__refresh_list_cascade)

        self.show()

    def cascade_up(self):
        current_index = self.list_cascade.currentRow()
        if current_index is None:
            return
        item = self.list_cascade.takeItem(current_index)
        self.list_cascade.insertItem(current_index - 1, item)
        self.list_cascade.setCurrentRow(current_index - 1)

    def cascade_down(self):
        current_index = self.list_cascade.currentRow()
        if current_index is None:
            return
        item = self.list_cascade.takeItem(current_index)
        self.list_cascade.insertItem(current_index + 1, item)
        self.list_cascade.setCurrentRow(current_index + 1)

    def __get_selected_model_cascade(self):
        items = self.list_cascade.selectedIndexes()
        if len(items)>0:
            name = self.__list_cascade.item(items[0].row(),0).text()
            return name
        return None

    def set_serie(self, id_serie):
        if id_serie :
            self.active_serie_id = int(id_serie)
            model_connect_settings, = self.project.execute("""
                select model_connect_settings from project.serie
                where id={} limit 1""".format(self.active_serie_id)).fetchone()
            if model_connect_settings=="global":
                self.radio_mode_connexion_global.setChecked(True)
                self.__refresh_stacked()
            elif model_connect_settings=="cascade":
                self.radio_mode_connexion_cascade.setChecked(True)
                self.__refresh_list_cascade()
            elif model_connect_settings=="mixed":
                self.radio_mode_connexion_mixed.setChecked(True)
                self.__refresh_lists_groups()
                self.treeGroups.setCurrentIndex(self.__tree_groups.index(0, 0))
        else :
            self.active_serie_id = id_serie

    def __refresh_stacked(self):
        if self.radio_mode_connexion_global.isChecked():
            self.stacked.setCurrentIndex(0)
        elif self.radio_mode_connexion_cascade.isChecked():
            self.stacked.setCurrentIndex(1)
        elif self.radio_mode_connexion_mixed.isChecked():
            self.stacked.setCurrentIndex(2)

    def __refresh_list_cascade(self):
        self.__refresh_stacked()
        models = self.project.execute("""
                    select name, cascade_order
                    from project.model_config
                    order by cascade_order asc
                    """).fetchall()
        self.list_cascade.clear()
        for model in models:
            self.list_cascade.addItem(model[0])

    def save(self):
        if self.active_serie_id:
            if self.radio_mode_connexion_global.isChecked():
                model_connect_settings = "global"
            elif self.radio_mode_connexion_cascade.isChecked():
                model_connect_settings = "cascade"
                self.__save_cascade()
            elif self.radio_mode_connexion_mixed.isChecked():
                model_connect_settings = "mixed"

            self.project.execute("""
            update project.serie set
                model_connect_settings='{}' where id={}""".format( model_connect_settings,
                str(self.active_serie_id)))

    def __save_cascade(self):
        for i in range(0, self.list_cascade.count()):
            self.project.execute("""
                update project.model_config set cascade_order={} where name='{}';
            """.format(i, self.list_cascade.item(i).text()))

    def add_group(self):
        if not self.active_serie_id is None:
            self.__refresh_stacked()
            id, name = self.project.execute("insert into project.grp default values returning id, name").fetchone()
            self.project.execute("insert into project.mixed_serie(ord, serie, grp) values (%i, %i, %i)"%(self.__tree_groups.rowCount()+1, self.active_serie_id, id))
            self.__refresh_lists_groups()
            self.select_group_by_name(name)

    def select_group_by_name(self, group_name):
        select_group = self.__tree_groups.findItems(group_name)
        if select_group:
            group_item = select_group[0]
            self.treeGroups.setCurrentIndex(group_item.index())

    def remove_model_from_group(self):
        if not self.active_serie_id is None:
            id_group, id_model = self.get_selected_group_model()
            if id_model is None:
                return
            self.project.execute("delete from project.grp_model where grp=%i and model=%i"%(id_group, id_model))
            self.__refresh_lists_groups()

    def add_model_to_group(self):
        if not self.active_serie_id is None:
            id_group = self.get_selected_group()
            if id_group is None:
                return
            id_model = self.get_selected_model()
            if id_model is None:
                return
            self.project.execute("insert into project.grp_model(grp, model) values (%i, %i)"%(id_group, id_model))
            self.__refresh_lists_groups()

    def get_selected_model(self):
        items = self.listModels.selectedIndexes()
        if len(items)>0:
            name = self.__list_model.item(items[0].row(),0).text()
            id, = self.project.execute("select id from project.model_config where name='%s'"%(name)).fetchone()
            return int(id)
        return None

    def get_selected_group_model(self):
        items = self.treeGroups.selectedIndexes()
        if len(items)>0:
            model_index = items[0]
            group_index = model_index.parent()
            if group_index is None:
                return None, None # A group is selected, not a model
            model_name = str(self.__tree_groups.data(model_index))
            id_model, = self.project.execute("select id from project.model_config where name='%s'"%(model_name)).fetchone()
            group_name = str(self.__tree_groups.data(group_index))
            id_group, = self.project.execute("select id from project.grp where name='%s'"%(group_name)).fetchone()

            return int(id_group), int(id_model)
        return None, None

    def get_selected_group(self):
        items = self.treeGroups.selectedIndexes()
        if len(items)>0:
            selected_item = items[0]
            if self.__tree_groups.data(selected_item.parent()) is not None:
                return None # A model is selected, not a group
            name = self.__tree_groups.item(items[0].row(),0).text()
            id, = self.project.execute("select id from project.grp where name='%s'"%(name)).fetchone()
            return int(id)
        return None

    def delete_group(self):
        if not self.active_serie_id is None:
            id = self.get_selected_group()
            if not id is None:
                self.project.execute("delete from project.mixed_serie where grp=%i"%(id))
                self.project.execute("delete from project.grp where id=%i"%(id))
                self.__refresh_lists_groups()

    def __refresh_lists_groups(self):
        mixed = self.radio_mode_connexion_mixed.isChecked()
        self.gbx_groups.setVisible(mixed)

        if mixed and not self.active_serie_id is None:
            self.__list_model.setRowCount(0)
            self.__tree_groups.setRowCount(0)
            models = self.project.execute("""
                select name
                from project.model_config as model
                where model.id not in (
                    select model
                    from project.mixed_serie as mixed, project.grp as grp, project.grp_model as grp_model
                    where mixed.grp=grp.id
                    and grp.id = grp_model.grp
                    and mixed.serie=%i
                    )"""%(self.active_serie_id)).fetchall()

            for model in models:
                item = QStandardItem(model[0])
                item.setEditable(False)
                self.__list_model.appendRow(item)

            groups = self.project.execute("""
                select id, name, ord
                from project.mixed_serie as mixed, project.grp as grp
                where mixed.grp=grp.id
                and mixed.serie=%i
                order by mixed.ord"""%(self.active_serie_id)).fetchall()

            for group in groups:
                node_group = QStandardItem(group[1])
                node_group.setEditable(False)
                self.__tree_groups.appendRow(node_group)
                model_groups = self.project.execute("""
                    select name
                    from project.model_config as model
                    where model.id in (
                        select model from project.grp_model
                        where grp = %i)
                    """%(int(group[0]))).fetchall()
                for model in model_groups:
                    item = QStandardItem(model[0])
                    item.setEditable(False)
                    node_group.appendRow(item)

            self.treeGroups.expandAll()