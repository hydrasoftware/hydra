from builtins import object
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import time
from qgis.PyQt.QtCore import QCoreApplication, QDateTime

def format_timestamp(timestamp):
    return timestamp.toString('yyyyMMddhhmm')

def display_timestamp(str):
    timestamp = QDateTime.fromString(str, 'yyyyMMddhhmm')
    return timestamp.toString('hh:mm - dd/MM/yyyy')

class Timer(object):
    def __init__(self):
        self.start = time.time()

    def reset(self, text=""):
        s = self.start
        self.start = time.time()
        return "%30s % 8.4f sec"%(text, (self.start - s))
