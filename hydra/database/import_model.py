# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
import old hydra models

USAGE

    python -m hydra.database.import_model <project> <srid> <file>.csv
"""

from __future__ import absolute_import # important to read the doc !
import os
import re
import json
import math
import codecs
import numpy
import traceback
from math import sqrt
from collections import defaultdict
from qgis.PyQt.QtWidgets import QApplication
from hydra.project import Project
from hydra.utility.timer import Timer
from hydra.utility.log import LogManager
from hydra.utility.sql_json import instances as INSTANCES
import psycopg2

def _readline_to_array(csv_streamFile):
    return [v[1:-1] if len(v) and v[0]=="'" and v[-1]=="'" else v
            for v in csv_streamFile.readline().strip().split(';')]

def _point(string_):
    spl = string_.split('-')
    return (float(spl[0]), float(spl[1]))

def _point_from_node(md, node_name):
    sql = "select ST_X(geom), ST_Y(geom) from $model._node where UPPER(name)=UPPER('{}')"
    return md.execute(sql.format(str(node_name))).fetchone()

def _link_geometry(md, start_name, end_name):
    sql = "select ST_X(geom), ST_Y(geom) from $model._node where UPPER(name)=UPPER('{}')"
    return [ md.execute(sql.format(start_name)).fetchone(),
           md.execute(sql.format(end_name)).fetchone() ]

def _sm_link_geometry(md, lineArr, sm_index=None):
    if sm_index is None:
        if re.match(r'[0-9\.]+-[0-9\.]+', lineArr[-1]):
            sm_index = len(lineArr) - 3
        else:
            sm_index = len(lineArr) - 1
    link = _link_geometry(md, lineArr[3], lineArr[4])
    if str(lineArr[sm_index])=='1':
        link = [link[0],_point(lineArr[sm_index+1]),_point(lineArr[sm_index+2]),link[1]]
    return link

def unique_node_name(md, n):
    name = n
    i=0
    taken = md.execute("""select exists(select 1 from $model._node where name='{}')""".format(name)).fetchone()[0]
    while taken:
        i+=1
        name += '_'+str(i)
        taken = md.execute("""select exists(select 1 from $model._node where name='{}')""".format(name)).fetchone()[0]
    return name

def unique_singularity_name(md, n):
    name = n
    i=0
    taken = md.execute("""select exists(select 1 from $model._singularity where name='{}')""".format(name)).fetchone()[0]
    while taken:
        i+=1
        name += '_'+str(i)
        taken = md.execute("""select exists(select 1 from $model._singularity where name='{}')""".format(name)).fetchone()[0]
    return name

def unique_link_name(md, n):
    name = n
    i=0
    taken = md.execute("""select exists(select 1 from $model._link where name='{}')""".format(name)).fetchone()[0]
    while taken:
        i+=1
        name += '_'+str(i)
        taken = md.execute("""select exists(select 1 from $model._link where name='{}')""".format(name)).fetchone()[0]
    return name

def _parse_array(lineArr, num_start_col, nbcol, csv_streamFile, beclosed=False, optional=False):
    nbpt = int(str(lineArr[num_start_col]).split(' ')[0])
    if nbpt > 0:
        zzArr = [[float(lineArr[num_start_col+icol+1]) for icol in range(nbcol)]]
        for i in range(nbpt-1):
            lineArr = _readline_to_array(csv_streamFile)
            if lineArr[num_start_col+1]:
                zzArr.append([float(lineArr[num_start_col+icol+1]) for icol in range(nbcol)])
        if beclosed==True and zzArr[0] != zzArr[-1]:
            zzArr.append(zzArr[0])
        return zzArr
    elif optional:
        return []
    else:
        raise Exception("no element for array "+str(nbpt))

def parse_noda(md, lineArr):
    name, zz, area, node_hydrology, stn_name = unique_node_name(md, lineArr[2]), float(lineArr[4]), float(lineArr[5]), int(lineArr[6]), lineArr[7]
    geom = _point(lineArr[3])
    if stn_name:
        md.add_station_node(geom, zz, area, name)
        node = md.get_attrib_from_table_byname("station_node", name)
    elif node_hydrology==0:
        if not md.execute("""
                select 1 from $model.manhole_node
                where ST_DWithin(geom, 'SRID=$srid;POINT({} {})'::geometry, 0.001)
                """.format(*geom)).fetchone() :
            md.add_manhole(geom, name, area, zz)
    else:
        if not md.execute("""
                select 1 from $model.manhole_hydrology_node
                where ST_DWithin(geom, 'SRID=$srid;POINT({} {})'::geometry, 0.001)
                """.format(*geom)).fetchone() :
            md.add_manhole_hydrology(geom, name, area, zz)

def parse_car(md, lineArr):
    name = unique_node_name(md, lineArr[2])
    geom = _point(lineArr[4])
    z_ground = float(lineArr[5]) if lineArr[5] else None
    ht = float(lineArr[6])
    area = float(lineArr[7])
    idomain = int(lineArr[8]) if lineArr[8] else None
    if not md.execute("""
            select 1 from $model.crossroad_node
            where ST_DWithin(geom, 'SRID=$srid;POINT({} {})'::geometry, 0.001)
            """.format(*geom)).fetchone() :
        md.add_crossroad_node(geom, area, z_ground, ht, name)

def parse_nodr(md, lineArr):
    name = unique_node_name(md, lineArr[2])
    zz = float(lineArr[4])
    area = float(lineArr[5])
    ibief = lineArr[6] or None
    geom = _point(lineArr[3])
    mrkbname = lineArr[7] or None
    pk = (lineArr[8] or '0')
    node_hydrology = int(lineArr[9]) if len(lineArr) > 9 else None
    stn_name = lineArr[10] if len(lineArr) > 7 else None

    if stn_name:
        md.add_station_node(geom, zz, area, name)
    elif node_hydrology:
        md.add_manhole_hydrology(geom, name, area, zz)
    else:
        # check if the node already exists, its the case for end nodes
        # that are automagically added by triggers
        nid = md.execute("""
            select id from $model.river_node
            where ST_DWithin(geom, 'SRID=$srid;POINT({} {})'::geometry, 0.1)
            """.format(*geom)).fetchone()
        if nid:
            md.update_river_node(nid[0], None, zz, area, name)
        else:
            md.add_river_node(geom, zz, area, name)

def parse_reach(md, lineArr):
    name = lineArr[2]
    pk0 = float(lineArr[3])
    dx = float(lineArr[4])
    nbpt = int(lineArr[5])
    geom = [_point(lineArr[6+i]) for i in range(nbpt)]
    md.add_reachpath(geom, dx, pk0, name)

def parse_ci(md, lineArr):
    name = unique_link_name(md, lineArr[2])
    zradam = float(lineArr[7])
    zradav = float(lineArr[8])
    rk = float(lineArr[10])
    ht = float(lineArr[11])
    geom = _link_geometry(md, lineArr[3], lineArr[4])
    id_pipe = md.add_pipe_link(geom, zradam, zradav, 'circular', exclude_from_branch=None, rk=rk, circular_diameter=ht, name=name)

def parse_cih(md, lineArr):
    parse_ci(md, lineArr)

def parse_ov(md, lineArr):
    name = unique_link_name(md, lineArr[2])
    zradam = float(lineArr[7])
    zradav = float(lineArr[8])
    rk = float(lineArr[10])
    ht = float(lineArr[11])
    geom = _link_geometry(md, lineArr[3], lineArr[4])
    pt_diam = float(lineArr[12])
    gd_diam = float(lineArr[13])
    id_pipe = md.add_pipe_link(geom, zradam, zradav, 'ovoid', rk=rk, ovoid_height=ht, ovoid_top_diameter=gd_diam, ovoid_invert_diameter=pt_diam, name=name)

def parse_ovh(md, lineArr):
    parse_ov(md, lineArr)

def parse_pf(md, lineArr):
    name = unique_link_name(md, lineArr[2])
    zradam = float(lineArr[7])
    zradav = float(lineArr[8])
    rk = float(lineArr[10])
    ht = float(lineArr[11])
    geom = _link_geometry(md, lineArr[3], lineArr[4])
    idsect = lineArr[12]

    #--> pipe and channel_cross_section can share a same closed or open_parametric_geometry
    id_sect = md.execute("""select id from $model.closed_parametric_geometry where UPPER(name)=UPPER('{}')""".format(idsect)).fetchone()
    if id_sect:
        id_sectgeom = id_sect[0]
        id_pipe = md.add_pipe_link(geom, zradam, zradav, 'pipe', rk=rk, cp_geom=id_sectgeom, name=name)
    else:
        id_sectgeom, = md.execute("""select id from $model.open_parametric_geometry where UPPER(name)=UPPER('{}')""".format(idsect)).fetchone()
        id_pipe = md.add_pipe_link(geom, zradam, zradav, 'pipe', rk=rk, op_geom=id_sectgeom, name=name)

def parse_pfh(md, lineArr):
    parse_pf(md, lineArr)

def parse_po(md, lineArr):
    name = unique_link_name(md, lineArr[2])
    zradam = float(lineArr[7])
    zradav = float(lineArr[8])
    rk = float(lineArr[10])
    ht = float(lineArr[11])
    geom = _link_geometry(md, lineArr[3], lineArr[4])
    idsect = lineArr[12]

    #--> pipe and channel_cross_section can share same close or open_parametric_geometry
    id_sect = md.execute("""select id from $model.open_parametric_geometry where UPPER(name)=UPPER('{}')""".format(idsect)).fetchone()
    if id_sect:
        id_sectgeom = id_sect[0]
        id_pipe = md.add_pipe_link(geom, zradam, zradav, 'channel', rk=rk, op_geom=id_sectgeom, name=name)
    else:
        id_sectgeom, = md.execute("""select id from $model.closed_parametric_geometry where UPPER(name)=UPPER('{}')""".format(idsect)).fetchone()
        id_pipe = md.add_pipe_link(geom, zradam, zradav, 'channel', rk=rk, cp_geom=id_sectgeom, name=name)

def parse_poh(md, lineArr):
    parse_po(md, lineArr)

def multi_parse_trc(md, lineArr, csv_streamFile):
    name = lineArr[2]
    lineArr = _readline_to_array(csv_streamFile)
    coords = _parse_array(lineArr, 1, 2, csv_streamFile)
    line = md.make_line(coords)
    joined = md.try_merge_reach(line)
    if not joined:
        md.add_reachpath(line, name=name)

def multi_parse_stn(md, lineArr, csv_streamFile):
    name = lineArr[2].replace('STN_', '') if len(lineArr[2])>16 else lineArr[2]
    lineArr = _readline_to_array(csv_streamFile)
    contour = _parse_array(lineArr, 1, 2, csv_streamFile, True) #100 rows max
    contour=[contour]
    md.add_station(contour, name)

def multi_parse_cas(md, lineArr, csv_streamFile):
    name = unique_node_name(md, lineArr[2])
    zini = float(lineArr[3])
    zsArr = []
    for i in range(10):
        if [float(lineArr[5+(2*i)]), float(lineArr[6+(2*i)])] != [0,0]:
            zsArr.append([float(lineArr[5+(2*i)]), float(lineArr[6+(2*i)])])
    lineArr = _readline_to_array(csv_streamFile)
    coords = _parse_array(lineArr, 1, 2, csv_streamFile, True)

    poly = md.make_polygon([coords])
    centroid = md.execute("""select st_astext(ST_PointOnSurface(ST_MakeValid({})))""".format(poly)).fetchone()[0]
    x = float(centroid.split('(')[1].split(' ')[0])
    y = float(centroid.split('(')[1].split(' ')[1][0:-1])
    if not md.execute("""
            select 1 from $model.storage_node
            where ST_DWithin(geom, 'SRID=$srid;POINT({} {})'::geometry, 0.001)
            """.format(x, y)).fetchone() :
        md.add_storage_node([x,y], zsArr, zsArr[0][0], None, name)
        md.execute("""insert into $model.constrain(geom) values (st_force3d(st_exteriorring(st_buffer(ST_ConvexHull(ST_MakeValid({})), -1))))""".format(poly))

def multi_parse_ilot(md, lineArr, csv_streamFile):
    name = unique_node_name(md, lineArr[2])
    coef = float(lineArr[4])
    zsArr = []
    for i in range(10):
        if [float(lineArr[6+(2*i)]), float(lineArr[7+(2*i)])] != [0,0]:
            zsArr.append([float(lineArr[6+(2*i)]), float(lineArr[7+(2*i)])])
    lineArr = _readline_to_array(csv_streamFile)
    coords = _parse_array(lineArr, 1, 2, csv_streamFile, True)

    poly = md.make_polygon([coords])
    centroid = md.execute("""select st_astext(ST_PointOnSurface(ST_MakeValid({})))""".format(poly)).fetchone()[0]
    x = float(centroid.split('(')[1].split(' ')[0])
    y = float(centroid.split('(')[1].split(' ')[1][0:-1])
    if not md.execute("""
            select 1 from $model.storage_node
            where ST_DWithin(geom, 'SRID=$srid;POINT({} {})'::geometry, 0.001)
            """.format(x, y)).fetchone() :
        md.add_storage_node([x,y], zsArr, zsArr[0][0], None, name)
        md.execute("""insert into $model.constrain(geom) values (st_force3d(st_exteriorring(st_buffer(ST_ConvexHull(ST_MakeValid({})), -1))))""".format(poly))

def multi_parse_pav(md, lineArr, csv_streamFile):
    table='elem_2d_node'
    name=unique_node_name(md, lineArr[2])
    area = float(lineArr[3])
    zb = float(lineArr[4])
    rk = float(lineArr[5])
    if isinstance(lineArr[8], int):
        domain_2d = "DOMAIN_2D_{}".format(int(lineArr[8]))
    elif isinstance(lineArr[8], str):
        domain_2d = "DOMAIN_{}".format(lineArr[8])
    lineArr = _readline_to_array(csv_streamFile)
    coords = _parse_array(lineArr, 1, 2, csv_streamFile, True) #100 rows max
    id_domain_2d = md.execute("""
        select id from $model.domain_2d where UPPER(name)=UPPER('{}')
        """.format(domain_2d)).fetchone()
    if id_domain_2d is None:
        id_domain_2d = md.execute("""
            insert into $model.domain_2d(name) values('{}') returning id
            """.format(domain_2d)).fetchone()
    md.add_elem_2d_node([coords], zb, rk, id_domain_2d[0], area, name)

def multi_parse_hy(md, lineArr, csv_streamFile):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    # if not xyArr:
        # xyArr = _point(lineArr[4])
    storage_area = float(lineArr[5]) if lineArr[5] else 0
    constant_dry_flow = float(lineArr[8]) if lineArr[8] else 0
    distrib_coef = float(lineArr[10]) if lineArr[10] else 0
    nbpoint_tq = int(lineArr[13])
    lag_time_hr = float(lineArr[11]) if lineArr[11] else 0
    tqArr = _parse_array(lineArr, 13, 2, csv_streamFile, optional=True)
    ext_data = True if len(tqArr)==0 else False
    md.add_hy_singularity(xyArr, name, storage_area, tqArr, constant_dry_flow,
            distrib_coef,lag_time_hr, external_file_data=ext_data)

def multi_parse_hyh(md, lineArr, csv_streamFile):
    multi_parse_hy(md, lineArr, csv_streamFile)

def parse_zk(md, lineArr):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    slope = float(lineArr[5])
    k = float(lineArr[6])
    width = float(lineArr[7])
    md.add_strickler_bc_singularity(xyArr, slope,k, width, name)

def parse_rk(md, lineArr):
    parse_zk(md, lineArr)

def parse_clrk(md, lineArr):
    parse_zk(md, lineArr)

def parse_zf(md, lineArr):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    md.add_simple_singularity('froude_bc_singularity', xyArr, name)

def parse_mrkp(md, lineArr):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    md.add_simple_singularity('marker_singularity', xyArr, name)

def parse_clav(md, lineArr):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    md.add_simple_singularity('hydrology_bc_singularity', xyArr, name)

def parse_va(md, lineArr):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    if xyArr is None:
        xyArr = lineArr[4].split('-')
    zr =  float(lineArr[5])
    zv = float(lineArr[6])
    sec = float(lineArr[7])
    cc = float(lineArr[8])
    w=sec/(zv-zr)
    md.add_singularity_va( xyArr, z_invert=zr, z_ceiling=zv, width=w, cc=cc, name=name)

def multi_parse_cpr(md, lineArr, csv_streamFile):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    zzArr = _parse_array(lineArr, 5, 2, csv_streamFile)
    md.add_hydraulic_cut_singularity(xyArr, zzArr, True, name)

def multi_parse_dh(md, lineArr, csv_streamFile):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    if xyArr is None:
        xyArr = lineArr[4].split('-')
    zzArr = _parse_array(lineArr, 5, 2, csv_streamFile)
    md.add_param_headloss_singularity(xyArr, zzArr, True, name)

def multi_parse_zq(md, lineArr, csv_streamFile):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    zzArr = _parse_array(lineArr, 5, 2, csv_streamFile)
    md.add_zq_bc_singularity(xyArr, zzArr, name)

def multi_parse_qdz1(md, lineArr, csv_streamFile):
    multi_parse_zq(md, lineArr, csv_streamFile)

def multi_parse_zt(md, lineArr, csv_streamFile):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    cyclic = True if int(lineArr[5])==1 else False
    zzArr = None
    if int(lineArr[6])!=0:
        zzArr = _parse_array(lineArr, 6, 2, csv_streamFile)
    md.add_tz_bc_singularity(xyArr, zzArr, cyclic, name=name)

def multi_parse_brd(md, lineArr, csv_streamFile):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    d_abutment_l = float(lineArr[5])
    d_abutment_r = float(lineArr[6])
    abutment_type = lineArr[7] if lineArr[7] in ['rounded', 'angle_45', 'angle_90'] else None
    z_ceiling = float(lineArr[8])
    zzArr = _parse_array(lineArr, 9, 2, csv_streamFile) # 10 rows max
    md.add_singularity_brd(xyArr, zzArr, d_abutment_l, d_abutment_r, abutment_type, z_ceiling, name)

def multi_parse_brdg(md, lineArr, csv_streamFile):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    z_road = float(lineArr[5])
    l_road = float(lineArr[6])
    zw_array = _parse_array(lineArr, 7, 2, csv_streamFile) # 10 rows max
    md.add_singularity_brdg(xyArr, zw_array, l_road, z_road, True, name)

def multi_parse_clbo(md, lineArr, csv_streamFile):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    zzArr = _parse_array(lineArr, 5, 2, csv_streamFile)
    md.add_tank_bc_singularity(xyArr, zzArr, zzArr[0][0], name=name)

def multi_parse_bo(md, lineArr, csv_streamFile):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    zzArr = _parse_array(lineArr, 7, 2, csv_streamFile)
    md.add_tank_bc_singularity(xyArr, zzArr, zzArr[0][0], name=name)

def multi_parse_rsh(md, lineArr, csv_streamFile):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    drainage, = md.execute("""
        select id from $model._link where UPPER(name)=UPPER('{}')
        """.format(lineArr[5])).fetchone()
    overflow, =  md.execute("""
        select id from $model._link where UPPER(name)=UPPER('{}')
        """.format(lineArr[6])).fetchone()
    q_drainage = float(lineArr[7])
    z_ini = float(lineArr[8])
    zzArr = _parse_array(lineArr, 9, 2, csv_streamFile) # 10 rows, 2 col

    treatment_mode = "residual_concentration"
    treatment_param = {"residual_concentration":{
            "mes_conc_mgl": 0,
            "ntk_conc_mgl": 0,
            "dbo5_conc_mgl": 0,
            "dco_conc_mgl": 0}}

    md.add_singularity_rsh(xyArr, drainage, overflow, q_drainage, z_ini, zzArr,
            treatment_mode, json.dumps(treatment_param),name)

def multi_parse_rsph(md, lineArr, csv_streamFile):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    drainage, = md.execute("""
        select id from $model._link where UPPER(name)=UPPER('{}')
        """.format(lineArr[5])).fetchone()
    overflow, =  md.execute("""
        select id from $model._link where UPPER(name)=UPPER('{}')
        """.format(lineArr[6])).fetchone()
    z_ini = float(lineArr[7])
    zzArr = _parse_array(lineArr, 8, 4, csv_streamFile) # 10 rows, 4 col

    treatment_mode = "residual_concentration"
    treatment_param = {"residual_concentration":{
            "mes_conc_mgl": 0,
            "ntk_conc_mgl": 0,
            "dbo5_conc_mgl": 0,
            "dco_conc_mgl": 0}}

    md.add_singularity_rsph(xyArr, drainage, overflow, z_ini, zzArr,
            treatment_mode, json.dumps(treatment_param), name)

def multi_parse_cbv(md, lineArr, csv_streamFile):
    name = lineArr[2]
    lineArr = _readline_to_array(csv_streamFile)
    contour = [_parse_array(lineArr, 1, 2, csv_streamFile, True)]
    # adds contour
    md.add_catchment_contour(contour, name)

    # uddate catchment_node in contour
    md.execute("""
        update $model.catchment_node as n
        set contour=(
            select c.id from $model.catchment as c
            where st_intersects(c.geom, n.geom)
            limit 1)""")

def parse_bv(md, lineArr):
    name = unique_node_name(md, lineArr[2])
    xyArr = _point(lineArr[4])
    area_ha = float(lineArr[5])
    rl = float(lineArr[6])
    slope = float(lineArr[7])
    c_imp = float(lineArr[8])
    netflow_type = {
            1: "constant_runoff",
            2: "horner",
            3: "holtan",
            4: "scs",
            5: "permeable_soil"}[int(lineArr[9].split(" ")[0])]
    runoff_type = {
            1: ("Desbordes 1 Cr", "k_desbordes"),
            2: ("Define K", "passini"),
            3: ("Define K", "giandotti"),
            4: ("Define Tc", "socose")
            }[int(lineArr[15].split(" ")[0])]

    q_limit = lineArr[18] or 9999
    q0 = 0

    constant_runoff=None
    horner_ini_loss_coef=None
    horner_recharge_coef=None
    holtan_sat_inf_rate_mmh=None
    holtan_dry_inf_rate_mmh=None
    holtan_soil_storage_cap_mm=None
    scs_j_mm=None
    scs_soil_drainage_time_day=None
    scs_rfu_mm=None
    hydra_surface_soil_storage_rfu_mm=None
    hydra_inf_rate_f0_mm_hr=None
    hydra_int_soil_storage_j_mm=None
    hydra_soil_drainage_time_qres_day=None

    if netflow_type == "constant_runoff":
        constant_runoff=float(lineArr[10])
    elif netflow_type == "horner":
        horner_recharge_coef=float(lineArr[10])
        horner_ini_loss_coef=float(lineArr[11])
    elif netflow_type == "holtan":
        holtan_soil_storage_cap_mm= float(lineArr[10])
        holtan_sat_inf_rate_mmh= float(lineArr[11])
        holtan_dry_inf_rate_mmh= float(lineArr[12])
    elif netflow_type == "scs":
        scs_j_mm=float(lineArr[10])
        scs_soil_drainage_time_day=float(lineArr[11])
        scs_rfu_mm=float(lineArr[12])
    elif netflow_type == "hydra":
        hydra_surface_soil_storage_rfu_mm=float(lineArr[10])
        hydra_inf_rate_f0_mm_hr=float(lineArr[12])
        hydra_int_soil_storage_j_mm=float(lineArr[13])
        hydra_soil_drainage_time_qres_day=float(lineArr[14])
    else:
        raise Exception("netflow_type unknown")

    socose_tc_mn=None
    socose_shape_param_beta=None
    define_k_mn=None

    if runoff_type[0] == "Define Tc": #"socose":
        socose_tc_mn=float(lineArr[16])
        socose_shape_param_beta=float(lineArr[17])
    elif runoff_type[0] in ['Desbordes 1 Cr', 'Define K']: #['k_desbordes', 'passini', 'giandotti']:
        try:
            define_k_mn=float(lineArr[16])
        except:
            define_k_mn=None
            if runoff_type[1] == 'passini':
                define_k_mn = 0.14 * (area_ha * rl)**(1./3.) / sqrt(slope)
            elif runoff_type[1] == 'giandotti':
                define_k_mn =  60 * ( 0.4 * sqrt(area_ha) + 0.0015 * rl ) / ( 0.8 * sqrt(slope * rl) )
            elif runoff_type[1] == 'k_desbordes':
                define_k_mn = 5.07 * area_ha**(0.18) * (slope * 100)**(-0.36) * (1 + c_imp)**(-1.9) * rl**(0.15) * 30**(0.21) * 10**(-0.07)
    else:
        raise Exception("runoff_type unknown")

    md.add_catchment_node(xyArr, name, area_ha, rl, slope, c_imp,
                    netflow_type, constant_runoff, horner_ini_loss_coef, horner_recharge_coef,
                    holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm,
                    scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm,
                    hydra_surface_soil_storage_rfu_mm, hydra_inf_rate_f0_mm_hr, hydra_int_soil_storage_j_mm, hydra_soil_drainage_time_qres_day,
                    None, hydra_inf_rate_f0_mm_hr, None, 'rate',
                    None, None, None, None, runoff_type[0], socose_tc_mn, socose_shape_param_beta, define_k_mn,
                    q_limit, q0)

def parse_de(md, lineArr):
    name = unique_singularity_name(md, lineArr[2])
    geom = _point(lineArr[4])
    zs = float(lineArr[5])
    zr = float(lineArr[6])
    wt = float(lineArr[7])
    cc = float(lineArr[8])
    mode_regul = 'elevation' if lineArr[9] in ['0', ''] else 'width'
    reoxy_law = 'gameson' if lineArr[10] in ['0', ''] else 'r15'
    if reoxy_law == 'gameson':
        reoxy_param = {"gameson":{
            "waterfall_coef": float(lineArr[11]) if len(lineArr) > 13 and lineArr[11] else 0.,
            "adjust_coef": float(lineArr[12]) if len(lineArr) > 13 and lineArr[12] else 0.,
            "pollution_coef": float(lineArr[13]) if len(lineArr) > 13 and lineArr[13] else 0.}}
    elif reoxy_law == 'r15':
        reoxy_param = {"r15":{
            "reoxygen_coef": float(lineArr[11]),
            "temperature_correct_coef": float(lineArr[12])}}
    md.add_singularity_de(geom, zs, zr, wt, cc, mode_regul, reoxy_law, json.dumps(reoxy_param), True, name)

class Properties(object):
    def __init__(self, index_of, lineArr):
        self.__index_of = index_of
        self.__lineArr = lineArr

    def __prop(self, kwd):
        idx = self.__index_of[kwd]
        return self.__lineArr[idx] if idx < len(self.__lineArr) else None

    def f(self, kwd):
        s = self.__prop(kwd)
        return float(s) if s else 0

    def i(self, kwd):
        s = self.__prop(kwd)
        return int(float(s)) if s else 0

    def geom(self, kwd):
        s = self.i(kwd)
        return {1:'user_defined', 2:'computed'}[s] if s else 'computed'

    def geomqmk(self, kwd):
        s = self.i(kwd)
        return {1:'circular',2:'canal', 3:'computed'}[s] if s else 'computed'

    def narrow_opt(self, kwd):
        s = self.i(kwd)
        return {0:'no', 1:'yes'}[s] if s in [0,1] else 'no'

    def diffus(self, kwd):
        s = self.i(kwd)
        return {1:'circular', 2:'square', 3:'plane'}[s] if s else 'circular'

    def transition(self, kwd):
        s = self.i(kwd)
        return {1:'conic', 2:'rectangular', 3:'rectangular', 4:'flat'}[s] if s else 'conic'

    def shape(self, kwd):
        s = self.i(kwd)
        return {1:'sharp', 2:'rounded'}[s] if s else 'sharp'

    def friction(self, kwd):
        s = self.i(kwd)
        return {1:'smooth', 2:'rough'}[s] if s else 'smooth'

    def deposition(self, kwd):
        s = self.i(kwd)
        return {1:'modern_automatic_grid', 2:'used_automatic', 3:'manual'}[s] if s else 'modern_automatic_grid'

    def grid_shape(self, kwd):
        s = self.i(kwd)
        return {1:'rectangular_edge', 2:'round_rectangular_edge', 3:'circular_edge'}[s] if s else 'rectangular_edge'

    def bend_shape(self, kwd):
        s = self.i(kwd)
        return {1:'elbow_90_deg', 2:'z_shaped_elbow', 3:'u_shaped_elow', 4:'two_elbows_in_distinct_planes'}[s] if s else 'elbow_90_deg'

    def ref_cs(self, kwd):
        s = self.i(kwd)
        return {1:'upstream', 2:'downstream'}[s] if s else 'upstream'

    def geomkv1(self, kwd):
        s = self.i(kwd)
        return {1:'defined',2:'upstream', 3:'downstream'}[s] if s else 'defined'

def parse_kv(md, lineArr):
    index_of = {
        u"Modele":                   0,
        u"Element":                  1,
        u"Identifiant":              2,
        u"Idnoeud":                  3,
        u"XYnoeud":                  4,
        u"Type":                     5,
        u"KV1_TypeSection":          6,
        u"KV1_Section(m2)":          7,
        u"KV1_K":                    8,
        u"KV2_TypeSectionAm":        9,
        u"KV2_SectionAm(m2)":       10,
        u"KV2_TypeSectionAv":       11,
        u"KV2_SectionAv(m2)":       12,
        u"KV2_SectionCour":         13,
        u"KV2_K":                   14,
        u"KV7_TypeSection":         15,
        u"KV7_Section(m2)":         16,
        u"KV7_Angle(°)":            17,
        u"KV7_Entonnement":         18,
        u"KV8_TypeSectionAm":       19,
        u"KV8_SectionAm(m2)":       20,
        u"KV8_SectionAv(m2)":       21,
        u"KV8_Angle(°)":            22,
        u"KV8_geometrie":           23,
        u"KV9_TypeSectionAm":       24,
        u"KV9_SectionAm(m2)":       25,
        u"KV9_TypeSectionAv":       26,
        u"KV9_SectionAv(m2)":       27,
        u"KV9_iSectCour":           28,
        u"KV9_Sect":                29,
        u"KV10_TypeSectionAm":      30,
        u"KV10_SectionAm(m2)":      31,
        u"KV10_TypeSectionAv":      32,
        u"KV10_SectionAv(m2)":      33,
        u"KV10_TypeSectionCour":    34,
        u"KV10_Angle(°)":           35,
        u"KV11_TypeSectionAm":      36,
        u"KV11_SectionAm(m2)":      37,
        u"KV11_TypeSectionAv":      38,
        u"KV11_SectionAv(m2)":      39,
        u"KV11_TypeSectionCour":    40,
        u"KV11_Angle(°)":           41,
        u"KV12_TypeSection":        42,
        u"KV12_Section(m2)":        43,
        u"KV12_Rayon(m)":           44,
        u"KV12_Angle(°)":           45,
        u"KV12_Aspect":             46,
        u"KV13_TypeSection":        47,
        u"KV13_Section(m2)":        48,
        u"KV13_Angle(°)":           49,
        u"KV13_NbRaccord":          50,
        u"KV13_rapport L/Dn":       51,
        u"KV13_Aspect":             52,
        u"KV14_TypeSection":        53,
        u"KV14_Section(m2)":        54,
        u"KV14_TauxObstruc":        55,
        u"KV14_Angle(°)":           56,
        u"KV14_Depot":              57,
        u"KV14_Barreau":            58,
        u"KV15_TypeSection":        59,
        u"KV15_Section(m2)":        60,
        u"KV15_TauxObstruc":        61,
        u"KV15_Angle(°)":           62,
        u"KV15_Barreau":            63,
        u"KV16_Section(m2)":        64,
        u"KV16_Longueur L(m)":      65,
        u"KV16_Strickler K":        66,
        u"KV17_Q1":                 67,
        u"KV17_dZ1":                68,
        u"KV17_Q2":                 69,
        u"KV17_dZ2":                70,
        u"KV17_Q3":                 71,
        u"KV17_dZ3":                72,
        u"KV17_Q4":                 73,
        u"KV17_dZ4":                74,
        u"KV17_Q5":                 75,
        u"KV17_dZ5":                76,
        u"KV18_TypeSectionAm":      77,
        u"KV18_SectionAm(m2)":      78,
        u"KV18_TypeSectionAv":      79,
        u"KV18_SectionAv(m2)":      80,
        u"KV18_Coude":              81,
        u"KV18_a0/b0":              82,
        u"KV18_b1/b0":              83,
    }
    name = lineArr[2]
    geom = _point(lineArr[4])
    law=None
    laws=None

    if isinstance(lineArr[5].replace('.', ' ').split(' ')[0], int):
        hdl_id = int(lineArr[5].replace('.', ' ').split(' ')[0])
        if hdl_id>6:
            hdl_id = hdl_id - 4
        law, = md.execute("""
            select name from hydra.borda_headloss_singularity_type
            where id={}""".format(hdl_id)).fetchone()

        prop = Properties(index_of, lineArr)

        laws = {
            "q":{
                "coef_kq2": prop.f(u"KV1_K")},
            "v":{
                "reference_cs": prop.ref_cs(u"KV2_SectionCour"),
                "coef_kv_12": prop.f(u"KV2_K"),
                "coef_kv_21": prop.f(u"KV2_K")},
            "diffuser":{
                "angle": prop.f(u"KV8_Angle(°)"),
                "diffus_geom": prop.f("KV8_geometrie")},
            "sharp_transition_geometry":{
                "middle_option": prop.narrow_opt(u"KV9_iSectCour"),
                "middle_cs_area": prop.f(u"KV9_Sect")},
            "contraction_with_transition":{
                "transition_option": prop.transition(u"KV10_TypeSectionCour"),
                "half_angle": prop.f(u"KV10_Angle(°)")},
            "enlargment_with_transition":{
                "transition_option": prop.transition(u"KV11_TypeSectionCour"),
                "half_angle": prop.f(u"KV11_Angle(°)")},
            "circular_bend":{
                "curve_radius": prop.f(u"KV12_Rayon(m)"),
                "curve_angle": prop.f(u"KV12_Angle(°)"),
                "wall_friction_option": prop.friction(u"KV12_Aspect")},
            "sharp_angle_bend":{
                "angle": prop.f(u"KV13_Angle(°)"),
                "connector_no": prop.i(u"KV13_NbRaccord"),
                "ls": prop.f(u"KV13_rapport L/Dn"),
                "wall_friction_option": prop.friction(u"KV13_Aspect")},
            "screen_normal_to_flow":{
                "blocage_coef": prop.f(u"KV14_TauxObstruc"),
                "v_angle": prop.f(u"KV14_Angle(°)"),
                "deposition_option": prop.deposition(u"KV14_Depot"),
                "grid_shape": prop.grid_shape(u"KV14_Barreau")},
            "screen_oblique_to_flow":{
                "blocage_coef": prop.f(u"KV15_TauxObstruc"),
                "v_angle": prop.f(u"KV15_Angle(°)"),
                "grid_shape": prop.grid_shape(u"KV15_Barreau")},
            "friction":{
                "length": prop.f(u"KV16_Longueur L(m)"),
                "ks": prop.f(u"KV16_Strickler K")},
            "parametric":{
                "qh_array": [
                    [prop.f(u"KV17_Q1"), prop.f(u"KV17_dZ1")],
                    [prop.f(u"KV17_Q2"), prop.f(u"KV17_dZ2")],
                    [prop.f(u"KV17_Q3"), prop.f(u"KV17_dZ3")],
                    [prop.f(u"KV17_Q4"), prop.f(u"KV17_dZ4")],
                    [prop.f(u"KV17_Q5"), prop.f(u"KV17_dZ5")]]},
            "sharp_bend_rectangular":{
                "bend_shape": prop.bend_shape(u"KV18_Coude"),
                "param1": prop.f(u"KV18_a0/b0"),
                "param2": prop.f(u"KV18_b1/b0"),
            }
        }


        # check validity of laws
        for key, value in INSTANCES["borda_headloss_singularity_type"].items():
            if key not in laws:
                raise Exception("borda_headloss_singularity_type, {} not in {}".format(key, laws))
            for k in value["params"].keys():
                if k not in laws[key]:
                    raise Exception("{}: {} not in {}".format(key, k, laws[key]))

    md.add_singularity_smk(geom, law, json.dumps(laws), True, name)

def parse_smk(md, lineArr):
    parse_kv(md, lineArr)

def parse_qmk(md, lineArr):
    index_of = {
        u"Modele":                0,
        u"Element":               1,
        u"Identifiant":           2,
        u"Noeud amont":           3,
        u"Noeud aval":            4,
        u"XYam":                  5,
        u"XYav":                  6,
        u"Type":                  7,
        u"MK1_K":                 8,
        u"MK2_TypeSecAm":         9,
        u"MK2_SecAm/wAm":        10,
        u"MK2_ZrAm":             11,
        u"MK2_TypeSecAv":        12,
        u"MK2_SecAv/lAv":        13,
        u"MK2_ZrAv":             14,
        u"MK2_iSecCour":         15,
        u"MK2_K":                16,
        u"MK3_TypeSec":          17,
        u"MK3_Sec/w":            18,
        u"MK3_ZR":               19,
        u"MK3_Section(m2)":      20,
        u"MK3_iForme":           21,
        u"MK4_TypeSec":          22,
        u"MK4_Sec/w":            23,
        u"MK4_ZR":               24,
        u"MK4_Section(m2)":      25,
        u"MK4_iForme":           26,
        u"MK5_TypeSecAm":        27,
        u"MK5_SecAm/wAm":        28,
        u"MK5_ZrAm":             29,
        u"MK5_TypeSecLat":       30,
        u"MK5_SecLat/wLat":      31,
        u"MK5_ZrLat":            32,
        u"MK5_Angle(°)":         33,
        u"MK6_TypeSecAm":        34,
        u"MK6_SecAm/wAm":        35,
        u"MK6_ZrAm":             36,
        u"MK6_TypeSecAv":        37,
        u"MK6_SecAv/wAv":        38,
        u"MK6_ZrAv":             39,
        u"MK6_Angle(°)":         40,
        u"MK7_TypeSecAv":        41,
        u"MK7_SecAv/wAv":        42,
        u"MK7_ZrAv":             43,
        u"MK7_Angle(°)":         44,
        u"MK7_Entonnement":      45,
        u"MK8_TypeSecAm":        46,
        u"MK8_SecAm/wAm":        47,
        u"MK8_ZrAm":             48,
        u"MK8_L_diffuseur(m)":   49,
        u"MK8_Angle(°)":         50,
        u"MK8_geometrie":        51,
        u"MK9_TypeSecAm":        52,
        u"MK9_SecAm/wAm":        53,
        u"MK9_ZrAm":             54,
        u"MK9_TypeSecAv":        55,
        u"MK9_SecAv/lAv":        56,
        u"MK9_ZrAv":             57,
        u"MK9_iSectCour":        58,
        u"MK9_Sect":             59,
        u"MK10_TypeSecAm":       60,
        u"MK10_SecAm/wAm":       61,
        u"MK10_ZrAm":            62,
        u"MK10_TypeSecAv":       63,
        u"MK10_SecAv/lAv":       64,
        u"MK10_ZrAv":            65,
        u"MK10_geometrie":       66,
        u"MK10_Angle(°)":        67,
        u"MK11_TypeSecAm":       68,
        u"MK11_SecAm/wAm":       69,
        u"MK11_ZrAm":            70,
        u"MK11_TypeSecAv":       71,
        u"MK11_SecAv/lAv":       72,
        u"MK11_ZrAv":            73,
        u"MK11_geometrie":       74,
        u"MK11_Angle(°)":        75,
        u"MK12_TypeSecAm":       76,
        u"MK12_SecAm/wAm":       77,
        u"MK12_ZrAm":            78,
        u"MK12_TypeSecAv":       79,
        u"MK12_SecAv/wAv":       80,
        u"MK12_ZrAv":            81,
        u"MK12_Rayon (m)":       82,
        u"MK12_Angle(°)":        83,
        u"MK12_Aspect":          84,
        u"MK13_TypeSecAm":       85,
        u"MK13_SecAm/wAm":       86,
        u"MK13_ZrAm":            87,
        u"MK13_TypeSecAv":       88,
        u"MK13_SecAv/wAv":       89,
        u"MK13_ZrAv":            90,
        u"MK13_Angle(°)":        91,
        u"MK13_Nb Tronçon":      92,
        u"MK13_Rapport L/d":     93,
        u"MK13_Aspect":          94,
        u"MK14_TypeSecAm":       95,
        u"MK14_SecAm/wAm":       96,
        u"MK14_ZrAm":            97,
        u"MK14_TypeSecAv":       98,
        u"MK14_SecAv/wAv":       99,
        u"MK14_ZrAv":           100,
        u"MK14_Obstruction":    101,
        u"MK14_Angle (°)":      102,
        u"MK14_Depot":          103,
        u"MK14_Barreaux":       104,
        u"MK15_TypeSecAm":      105,
        u"MK15_SecAm/wAm":      106,
        u"MK15_ZrAm":           107,
        u"MK15_TypeSecAv":      108,
        u"MK15_SecAv/wAv":      109,
        u"MK15_ZrAv":           110,
        u"MK15_Obstruction":    111,
        u"MK15_Angle (°)":      112,
        u"MK15_Barreaux":       113,
        u"MK16_TypeSection":    114,
        u"MK16_Sec/w":          115,
        u"MK16_Zr":             116,
        u"MK16_Longueur L(m)":  117,
        u"MK16_Strickler K":    118,
        u"MK17_Q1":             119,
        u"MK17_dZ1":            120,
        u"MK17_Q2":             121,
        u"MK17_dZ2":            122,
        u"MK17_Q3":             123,
        u"MK17_dZ3":            124,
        u"MK17_Q4":             125,
        u"MK17_dZ4":            126,
        u"MK17_Q5":             127,
        u"MK17_dZ5":            128,
        u"MK18_TypeSecAm":      129,
        u"MK18_SecAm/wAm":      130,
        u"MK18_ZrAm":           131,
        u"MK18_TypeSecAv":      132,
        u"MK18_SecAv/wAv":      133,
        u"MK18_ZrAv":           134,
        u"MK18_Coude":          135,
        u"MK18_a0/b0":          136,
        u"MK18_b1/b0":          137,
        u"MK18_L0/b0":          138,
        u"MK18_bk/b0":          139,
        u"MK19_TypeSecAm":      140,
        u"MK19_SecAm/wAm":      141,
        u"MK19_ZrAm":           142,
        u"MK19_TypeSecAv":      143,
        u"MK19_SecAv/wAv":      144,
        u"MK19_ZrAv":           145,
        u"MK19_Angle(°)":       146,
        u"MK20_TypeSecAm":      147,
        u"MK20_SecAm/wAm":      148,
        u"MK20_ZrAm":           149,
        u"MK20_TypeSecAv":      150,
        u"MK20_SecAv/wAv":      151,
        u"MK20_ZrAv":           152,
        u"MK20_Angle(°)":       153,
        u"SM":                  154}

    name=lineArr[2]
    geom = _sm_link_geometry(md, lineArr)

    lineArr = lineArr + ['']*(155 - len(lineArr))
    hdl_id = int(lineArr[7].replace('.', ' ').split(' ')[0])
    law, = md.execute("""
        select name from hydra.borda_headloss_link_type
        where id={}""".format(hdl_id)).fetchone()

    prop = Properties(index_of, lineArr)

    laws = {
        "q":{
            "coef_kq2": prop.f(u"MK1_K")},
        "v":{
            "up_geom": prop.geomqmk(u"MK2_TypeSecAm"),
            "up_cs_area": prop.f(u"MK2_SecAm/wAm"),
            "up_cs_zinvert": prop.f(u"MK2_ZrAm"),
            "down_geom": prop.geomqmk(u"MK2_TypeSecAv"),
            "down_cs_area": prop.f(u"MK2_SecAv/lAv"),
            "down_cs_zinvert": prop.f(u"MK2_ZrAv"),
            "reference_cs": prop.ref_cs(u"MK2_iSecCour"),
            "coef_kv_12": prop.f(u"MK2_K"),
            "coef_kv_21": prop.f(u"MK2_K")},
        "lateral_inlet":{
            "down_geom": prop.geomqmk(u"MK3_TypeSec"),
            "down_cs_area": prop.f(u"MK3_Section(m2)"),
            "down_cs_zinvert": prop.f(u"MK3_ZR"),
            "inlet_cs_area": prop.f(u"MK3_Sec/w"),
            "inlet_shape": prop.shape(u"MK3_iForme")},
        "lateral_outlet":{
            "up_geom": prop.geomqmk(u"MK4_TypeSec"),
            "up_cs_area": prop.f(u"MK4_Sec/w"),
            "up_cs_zinvert": prop.f(u"MK4_ZR"),
            "outlet_cs_area": prop.f(u"MK4_Section(m2)"),
            "outlet_shape": prop.shape(u"MK4_iForme")},
        "bifurcation":{
            "up_geom": prop.geomqmk(u"MK5_TypeSecAm"),
            "up_cs_area": prop.f(u"MK5_SecAm/wAm"),
            "up_cs_zinvert": prop.f(u"MK5_ZrAm"),
            "down_geom": prop.geomqmk(u"MK5_TypeSecLat"),
            "down_cs_area": prop.f(u"MK5_SecLat/wLat"),
            "down_cs_zinvert": prop.f(u"MK5_ZrLat"),
            "angle": prop.f(u"MK5_Angle(°)")},
        "junction":{
            "up_geom": prop.geomqmk(u"MK6_TypeSecAm"),
            "up_cs_area": prop.f(u"MK6_SecAm/wAm"),
            "up_cs_zinvert": prop.f(u"MK6_ZrAm"),
            "down_geom": prop.geomqmk(u"MK6_TypeSecAv"),
            "down_cs_area": prop.f(u"MK6_SecAv/wAv"),
            "down_cs_zinvert": prop.f(u"MK6_ZrAv"),
            "angle": prop.f(u"MK6_Angle(°)")},
        "transition_edge_inlet":{
            "down_geom": prop.geomqmk(u"MK7_TypeSecAv"),
            "down_cs_area": prop.f(u"MK7_SecAv/wAv"),
            "down_cs_zinvert": prop.f(u"MK7_ZrAv"),
            "angle": prop.f(u"MK7_Angle(°)"),
            "entrance_shape": prop.shape(u"MK7_Entonnement")},
        "diffuser":{
            "up_geom": prop.geomqmk(u"MK8_TypeSecAm"),
            "up_cs_area": prop.f(u"MK8_SecAm/wAm"),
            "up_cs_zinvert": prop.f(u"MK8_ZrAm"),
            "length": prop.f(u"MK8_L_diffuseur(m)"),
            "half_angle": prop.f(u"MK8_Angle(°)"),
            "diffus_geom": prop.diffus(u"MK8_geometrie")},
        "sharp_transition_geometry":{
            "up_geom": prop.geomqmk(u"MK9_TypeSecAm"),
            "up_cs_area": prop.f(u"MK9_SecAm/wAm"),
            "up_cs_zinvert": prop.f(u"MK9_ZrAv"),
            "down_geom": prop.geomqmk(u"MK9_TypeSecAv"),
            "down_cs_area": prop.f(u"MK9_SecAv/lAv"),
            "down_cs_zinvert": prop.f(u"MK9_ZrAv"),
            "middle_option": prop.narrow_opt(u"MK9_iSectCour"),
            "middle_cs_area": prop.f(u"MK9_Sect")},
        "contraction_with_transition":{
            "up_geom": prop.geomqmk(u"MK10_TypeSecAm"),
            "up_cs_area": prop.f(u"MK10_SecAm/wAm"),
            "up_cs_zinvert": prop.f(u"MK10_ZrAm"),
            "down_geom": prop.geomqmk(u"MK10_TypeSecAv"),
            "down_cs_area": prop.f(u"MK10_SecAv/lAv"),
            "down_cs_zinvert": prop.f(u"MK10_ZrAv"),
            "transition_option": prop.transition(u"MK10_geometrie"),
            "half_angle": prop.f(u"MK10_Angle(°)")},
        "enlargment_with_transition":{
            "up_geom": prop.geomqmk(u"MK11_TypeSecAm"),
            "up_cs_area": prop.f(u"MK11_SecAm/wAm"),
            "up_cs_zinvert": prop.f(u"MK11_ZrAm"),
            "down_geom": prop.geomqmk(u"MK11_TypeSecAv"),
            "down_cs_area": prop.f(u"MK11_SecAv/lAv"),
            "down_cs_zinvert": prop.f(u"MK11_ZrAv"),
            "transition_option": prop.transition(u"MK11_geometrie"),
            "half_angle": prop.f(u"MK11_Angle(°)")},
        "circular_bend":{
            "up_geom": prop.geomqmk(u"MK12_TypeSecAm"),
            "up_cs_area": prop.f(u"MK12_SecAm/wAm"),
            "up_cs_zinvert": prop.f(u"MK12_ZrAm"),
            "curve_radius": prop.f(u"MK12_Rayon (m)"),
            "curve_angle": prop.f(u"MK12_Angle(°)"),
            "wall_friction_option": prop.friction(u"MK12_Aspect")},
        "sharp_angle_bend":{
            "up_geom": prop.geomqmk(u"MK13_TypeSecAm"),
            "up_cs_area": prop.f(u"MK13_SecAm/wAm"),
            "up_cs_zinvert": prop.f(u"MK13_ZrAm"),
            "angle": prop.f(u"MK13_Angle(°)"),
            "connector_no": prop.i(u"MK13_Nb Tronçon"),
            "ls": prop.f(u"MK13_Rapport L/d"),
            "wall_friction_option": prop.friction(u"MK13_Aspect")},
        "screen_normal_to_flow":{
            "up_geom": prop.geomqmk(u"MK14_TypeSecAm"),
            "up_cs_area": prop.f(u"MK14_SecAm/wAm"),
            "up_cs_zinvert": prop.f(u"MK14_ZrAm"),
            "obstruction_rate": prop.f(u"MK14_Obstruction"),
            "v_angle": prop.f(u"MK14_Angle (°)"),
            "deposition_option": prop.deposition(u"MK14_Depot"),
            "grid_shape": prop.grid_shape(u"MK14_Barreaux")},
        "screen_oblique_to_flow":{
            "up_geom": prop.geomqmk(u"MK15_TypeSecAm"),
            "up_cs_area": prop.f(u"MK15_SecAm/wAm"),
            "up_cs_zinvert": prop.f(u"MK14_ZrAm"),
            "blocage_coef": prop.f(u"MK15_Obstruction"),
            "v_angle": prop.f(u"MK15_Angle (°)"),
            "grid_shape_oblique": prop.grid_shape(u"MK15_Barreaux")},
        "friction":{
            "up_geom": prop.geomqmk(u"MK16_TypeSection"),
            "up_cs_area": prop.f(u"MK16_Sec/w"),
            "up_cs_zinvert": prop.f(u"MK16_Zr"),
            "length": prop.f(u"MK16_Longueur L(m)"),
            "ks": prop.f(u"MK16_Strickler K")},
        "parametric":{
            "qh_array": [
                [prop.f(u"MK17_Q1"), prop.f(u"MK17_dZ1")],
                [prop.f(u"MK17_Q2"), prop.f(u"MK17_dZ2")],
                [prop.f(u"MK17_Q3"), prop.f(u"MK17_dZ3")],
                [prop.f(u"MK17_Q4"), prop.f(u"MK17_dZ4")],
                [prop.f(u"MK17_Q5"), prop.f(u"MK17_dZ5")]]},
        "sharp_bend_rectangular":{
            "up_geom": prop.geomqmk(u"MK18_TypeSecAm"),
            "up_cs_area": prop.f(u"MK18_SecAm/wAm"),
            "up_cs_zinvert": prop.f(u"MK18_ZrAm"),
            "down_geom": prop.geomqmk(u"MK18_TypeSecAv"),
            "down_cs_area": prop.f(u"MK18_SecAv/wAv"),
            "down_cs_zinvert": prop.f(u"MK18_ZrAv"),
            "bend_shape": prop.bend_shape(u"MK18_Coude"),
            "param1": prop.f(u"MK18_a0/b0"),
            "param2": prop.f(u"MK18_b1/b0"),
            "param3": prop.f(u"MK18_b1/b0"),
            "param4": prop.f(u"MK18_b1/b0")},
        "t_shape_bifurcation":{
            "up_geom": prop.geomqmk(u"MK19_TypeSecAm"),
            "up_cs_area": prop.f(u"MK19_SecAm/wAm"),
            "up_cs_zinvert": prop.f(u"MK19_ZrAm"),
            "down_geom": prop.geomqmk(u"MK19_TypeSecAv"),
            "down_cs_area": prop.f(u"MK19_SecAv/wAv"),
            "down_cs_zinvert": prop.f(u"MK19_ZrAv")},
        "t_shape_junction":{
            "up_geom": prop.geomqmk(u"MK20_TypeSecAm"),
            "up_cs_area": prop.f(u"MK20_SecAm/wAm"),
            "up_cs_zinvert": prop.f(u"MK20_ZrAm"),
            "down_geom": prop.geomqmk(u"MK20_TypeSecAv"),
            "down_cs_area": prop.f(u"MK20_SecAv/wAv"),
            "down_cs_zinvert": prop.f(u"MK20_ZrAv")}}

    # check validity of laws
    for key, value in INSTANCES["borda_headloss_link_type"].items():
        if key not in laws:
            raise Exception("borda_headloss_link_type, {} not in {}".format(key, laws))
        for k in value["params"].keys():
            if k not in laws[key]:
                raise Exception("{}: {} not in {}".format(key, k, laws[key]))


    md.add_borda_link(geom, law, json.dumps(laws), name)

def multi_parse_acta(md, lineArr, csv_streamFile):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    zr = float(lineArr[5])
    wt = float(lineArr[6])
    hmax =float(lineArr[7])
    cc = float(lineArr[8])
    vmax = float(lineArr[9])
    mode_regul = {0: 'discharge', 1: 'elevation'}[int(lineArr[10])]
    control_node, = md.execute("""
        select id from $model._node where UPPER(name)=UPPER('{}')
        """.format(lineArr[11])).fetchone()
    dtRegul = float(lineArr[16])
    pidArr = [float(lineArr[13]), float(lineArr[14]), float(lineArr[15])]
    tzArr =_parse_array(lineArr, 17, 2, csv_streamFile) # 10 rows max
    md.add_singularity_acta(xyArr, z_invert=zr, z_ceiling=zr+hmax, width=wt, cc=cc,
                            v_max_cms=vmax, dt_regul_hr=dtRegul, mode_regul=mode_regul,
                            z_control_node=control_node, z_pid_array=pidArr, z_tz_array=tzArr, name=name)

def multi_parse_rgb(md, lineArr, csv_streamFile):
    multi_parse_acta(md, lineArr, csv_streamFile)

def parse_mrkb(md, lineArr):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    pk0 = float(lineArr[5])
    dx = float(lineArr[6])
    md.add_singularity_mrkb(xyArr, pk0, dx, name)

def parse_dl(md, lineArr):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    zs = float(lineArr[5])
    wt = float(lineArr[6])
    cc = float(lineArr[7])
    md.add_singularity_dl(xyArr, zs, wt, cc,name)

def parse_qdl1(md, lineArr):
    parse_dl(md, lineArr)

def parse_rk(md, lineArr):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    rslope = float(lineArr[5])
    rk = float(lineArr[6])
    rb = float(lineArr[7])
    md.add_singularity_rk(xyArr, rslope, rk, rb, name)

def parse_hc(md, lineArr):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    q0 = float(lineArr[5]) if lineArr[5] else 0.
    md.add_singularity_hc(xyArr, q0, name)

def multi_parse_racc(md, lineArr, csv_streamFile):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    imode = {
            1: "hydrograph",
            2: "zq_downstream_condition"}[int(lineArr[5]) if lineArr[5] in ['1', '2'] else 1 ]
    zzArr = _parse_array(lineArr, 6, 2, csv_streamFile, optional=True) # 100 rows max
    md.add_singularity_racc(xyArr, imode, zzArr, None, name)

def multi_parse_dqh(md, lineArr, csv_streamFile):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    id_dowstream, = md.execute("""
        select id from $model._link where UPPER(name)=UPPER('{}')
        """.format(lineArr[5])).fetchone()
    nbsplit = int(lineArr[6])
    id_split1, = md.execute("""
        select id from $model._link where UPPER(name)=UPPER('{}')
        """.format(lineArr[7])).fetchone()
    id_split2, = md.execute("""
        select id from $model._link where UPPER(name)=UPPER('{}')
        """.format(lineArr[8])).fetchone() if lineArr[8] else (None,)
    qqArr = _parse_array(lineArr, 9, 2 if id_split2 is None else 3, csv_streamFile) # 3 col
    md.add_singularity_dqh(xyArr, qqArr, id_dowstream, id_split1, id_split2, name)

def multi_parse_dzh(md, lineArr, csv_streamFile):
    name = unique_singularity_name(md, lineArr[2])
    xyArr = _point_from_node(md, lineArr[3])
    pipes = {
            "pipe_down": lineArr[5],
            "split1": lineArr[11],
            "split2": lineArr[16] if lineArr[16] else None}
    laws = {
            "pipe_down": lineArr[6],
            "split1": lineArr[12],
            "split2": lineArr[17] if "split2" in pipes else None}
    params = {
            "pipe_down": lineArr[7:10],
            "split1": lineArr[13:16],
            "split2": lineArr[18:21]}
    nb_pt = {
            "pipe_down": int(lineArr[7]) if laws["pipe_down"]=='zq' else 0,
            "split1": int(lineArr[13]) if laws["split1"]=='zq' else 0,
            "split2": int(lineArr[18]) if laws["split2"]=='zq' else 0}
    max_nbpt=max(nb_pt.values())

    if max_nbpt>0:
        idx = {"pipe_down": (8,9), "split1": (14,15), "split2": (18,20)}
        zqArr = {}
        for pipe in pipes.keys():
            if nb_pt[pipe] > 0:
                zqArr[pipe] = [[float(lineArr[j]) for j in idx[pipe]]]
        for i in range(1, max_nbpt):
            lineArr = _readline_to_array(csv_streamFile)
            for pipe in pipes.keys():
                if nb_pt[pipe] > i:
                    zqArr[pipe].append([float(lineArr[j]) for j in idx[pipe]])

    law_params = {}
    ids = {}
    for pipe in pipes.keys():
        law = laws[pipe]
        if law == 'zq':
            law_params[pipe]= json.dumps({"zq":{
                "zq_array": zqArr[pipe]}})
        elif law == 'gate':
            law_params[pipe]= json.dumps({"gate": {
                "gate_z_invert": float(params[pipe][0]),
                "gate_diameter": float(params[pipe][1]),
                "gate_cc": float(params[pipe][2])}})
        elif law == 'weir':
            law_params[pipe] = json.dumps({"weir":{
                "weir_z_invert": float(params[pipe][0]),
                "weir_width": float(params[pipe][1]),
                "weir_cc": float(params[pipe][2])}})
        elif law == 'stickler':
            law_params[pipe] = json.dumps({"stickler":{}})
        else:
            law_params[pipe] = None
        ids[pipe], = md.execute("""
            select id from $model._link where UPPER(name)=UPPER('{}')
            """.format(pipes[pipe])).fetchone() if pipes[pipe] else (None,)

    md.add_singularity_dzh(xyArr,
            ids["pipe_down"], laws["pipe_down"], law_params["pipe_down"],
            ids["split1"], laws["split1"], law_params["split1"],
            ids["split2"], laws["split2"] or None, law_params["split2"],
            name)

def multi_parse_qmp(md, lineArr, csv_streamFile):
    name = lineArr[2]
    geom = _sm_link_geometry(md, lineArr, 14)

    npump =  int(lineArr[7])
    if npump > 10:
        raise Exception('10 pumps maximum')
    zregulArr = [[float(lineArr[9]), float(lineArr[10])]]
    hqArr = [_parse_array(lineArr, 11, 2, csv_streamFile)]
    for p in range(npump-1):
        lineArr = _readline_to_array(csv_streamFile)
        zregulArr.append([float(lineArr[9]), float(lineArr[10])])
        hqArr.append(_parse_array(lineArr, 11, 2, csv_streamFile))
    hqArr = [ [ [ hqArr[i][j][k] if j < len(hqArr[i]) else None
                for k in range(2)]
                for j in range(10)]
                for i in range(npump)]
    md.add_pump_link(geom, npump, zregulArr, hqArr, name)

def multi_parse_rgz(md, lineArr, csv_streamFile):
    name = lineArr[2]
    geom = _sm_link_geometry(md, lineArr, 25)
    zr = float(lineArr[7])
    zv = float(lineArr[8])
    wt = float(lineArr[9])
    cc = float(lineArr[10])
    action_gate_type = {1: "downward_opening", 2: "upward_opening"}[int(lineArr[12])]
    zrStop = float(lineArr[13])
    zvStop = float(lineArr[14])
    vmax = float(lineArr[15])
    res = md.execute("""
        select id from $model._node where UPPER(name)=UPPER('{}')
        """.format(lineArr[16])).fetchone()
    if res:
        control_node = res[0]
    dtRegul = float(lineArr[21])
    pidArr = [float(lineArr[18]), float(lineArr[19]), float(lineArr[20])]
    tzArr = _parse_array(lineArr, 22, 2, csv_streamFile)
    md.add_regul_gate_link(geom, zr, zv, wt, cc, None,
            action_gate_type, zrStop, zvStop, vmax, dtRegul, 'elevation',
            control_node, pidArr, tzArr, None, None, None, name)

def multi_parse_rgq(md, lineArr, csv_streamFile):
    name = lineArr[2]
    geom = _sm_link_geometry(md, lineArr, 25)
    zr = float(lineArr[7])
    zv = float(lineArr[8])
    wt = float(lineArr[9])
    cc = float(lineArr[10])
    action_gate_type = {1: "downward_opening", 2: "upward_opening"}[int(lineArr[12])]
    zrStop = float(lineArr[13])
    zvStop = float(lineArr[14])
    vmax = float(lineArr[15])
    res = md.execute("""
        select id from $model._node where UPPER(name)=UPPER('{}')
        """.format(lineArr[16])).fetchone()
    if res:
        control_node = res[0]
    zc = float(lineArr[17])
    dtRegul = float(lineArr[21])
    tqArr = _parse_array(lineArr, 22, 2, csv_streamFile)
    zc = float(zc) if zc else None
    maxId=md.add_regul_gate_link(geom, zr, zv, wt, cc, None,
            action_gate_type, zrStop, zvStop, vmax, dtRegul, 'discharge', None, None, None, zc, tqArr, None, name)

def multi_parse_ldvf(md, lineArr, csv_streamFile):
    name = lineArr[2]
    geom = _sm_link_geometry(md, lineArr, 15)
    z_invert = float(lineArr[7])
    width = float(lineArr[8])
    cc = float(lineArr[9])
    x_break = float(lineArr[10])
    group_dyke = lineArr[11] if isinstance(lineArr[11],int) else 1
    dt_fracwArr = _parse_array(lineArr, 12, 2, csv_streamFile)
    maxId=md.add_overflow_link(geom, z_crest1=(z_invert+10), z_crest2=(z_invert+10), width1=width, width2=0, cc=cc, lateral_contraction_coef=cc,
                            break_mode='zw_critical', z_break=x_break, z_invert=z_invert, width_breach=width, grp=group_dyke, dt_fracw_array=dt_fracwArr, name=name)

def multi_parse_ldvt(md, lineArr, csv_streamFile):
    name = lineArr[2]
    geom = _sm_link_geometry(md, lineArr, 15)
    dt_fracwArr = _parse_array(lineArr, 12, 2, csv_streamFile)
    z_invert, width, cc = lineArr[7],lineArr[8],lineArr[9]
    x_break = float(lineArr[10])
    group_dyke = lineArr[11] if isinstance(lineArr[11],int) else 1
    maxId=md.add_overflow_link(geom, z_crest1=(z_invert+10), z_crest2=(z_invert+10), width1=width, width2=0, cc=cc, lateral_contraction_coef=cc,
                            break_mode='zw_critical', t_break=x_break, z_invert=z_invert, width_breach=width, grp=group_dyke, dt_fracw_array=dt_fracwArr, name=name)

def multi_parse_qdz(md, lineArr, csv_streamFile):
    name = lineArr[2]
    geom = _sm_link_geometry(md, lineArr, 10)
    qzArr = _parse_array(lineArr, 7, 2, csv_streamFile)
    md.add_qdp_link(geom, 10, qzArr, name)

def multi_parse_qdp(md, lineArr, csv_streamFile):
    name = lineArr[2]
    geom = _sm_link_geometry(md, lineArr, 11)
    qpump = lineArr[7]
    qzArr = _parse_array(lineArr, 8, 2, csv_streamFile)
    md.add_qdp_link(geom, qpump, qzArr, name)

def parse_conf(md, lineArr):
    name = lineArr[2]
    geom = _link_geometry(md, lineArr[3], lineArr[4])
    main_branch = bool(int(lineArr[7]))
    md.add_connector_link(geom, main_branch, name)

def parse_qmtr(md, lineArr):
    parse_conf(md, lineArr)

def parse_trh(md, lineArr):
    name = lineArr[2]
    geom = _link_geometry(md, lineArr[3], lineArr[4])
    md.add_simple_link('connector_hydrology_link', geom, name)

def parse_lpav(md, lineArr):
    name = lineArr[2]
    geom = _link_geometry(md, lineArr[3], lineArr[4])
    if lineArr[9]:
        z_invert = float(lineArr[9])
    elif lineArr[8]:
        z_invert = float(lineArr[8])
    else:
        z_invert = None
    width = float(lineArr[10])
    x1, y1 = geom[0][0], geom[0][1]
    x2, y2 = geom[1][0], geom[1][1]
    ll = math.sqrt((x2-x1)**2+(y2-y1)**2)
    xm, ym = (x1+x2)/2, (y1+y2)/2
    xn, yn = (x2-x1)/ll, (y2-y1)/ll
    border = [[xm - yn*width/2, ym + xn*width/2],[xm + yn*width/2, ym - xn*width/2]]
    md.add_mesh_2d_link(geom, z_invert, name=name, border=border)

def parse_rue(md, lineArr):
    return
    name = lineArr[2]
    geom = _link_geometry(md, lineArr[3], lineArr[4])
    width = 0#◙float(lineArr[7])
    length = float(lineArr[8])
    rk = float(lineArr[9])
    if not any(xy is None for xy in geom):
        md.add_street_link(geom, width, None, rk, None, name)

def parse_lci(md, lineArr):
    name = lineArr[2]
    geom = _link_geometry(md, lineArr[3], lineArr[4])
    z_invert = float(lineArr[7])
    width = float(lineArr[8])
    length = float(lineArr[10])
    rk = float(lineArr[11])
    z_crest = float(lineArr[12] or 9999)
    md.add_overflow_link(geom, z_invert=z_invert, width1=width, z_crest1=z_crest, name=name)

def parse_qms(md, lineArr):
    name = lineArr[2]
    geom = _sm_link_geometry(md, lineArr, 12)
    z_invert = lineArr[7]
    width = lineArr[8]
    cc = lineArr[9]
    z_weir = lineArr[10]
    v_max_cms = lineArr[11]
    md.add_link_qms(geom, z_invert, width, cc, z_weir, v_max_cms, name)

def parse_qmv(md, lineArr):
    name = lineArr[2]
    geom = _sm_link_geometry(md, lineArr, 15)
    if None in geom:
        geom = [lineArr[5].split('-'), lineArr[6].split('-')]
    z_invert = float(lineArr[7])
    z_ceiling = float(lineArr[8])
    width = float(lineArr[9])
    cc = float(lineArr[10])
    mode_valve = {-1: 'no_valve',
                   0: 'no_valve',
                   1: 'no_valve',
                   2: 'one_way_downstream',
                   3: 'one_way_upstream'}[int(lineArr[11] or 1)]
    action_gate_type = {0: 'upward_opening',
                        1: 'downward_opening',
                        2: 'upward_opening'}[int(lineArr[12] or 2)]
    z_gate = float(lineArr[13])
    v_max_cms = float(lineArr[14])
    md.add_link_qmv(geom, z_invert, z_ceiling, width, cc, None, mode_valve, action_gate_type, z_gate, v_max_cms, name)

def parse_lrc(md, lineArr):
    name = lineArr[2]
    geom = _sm_link_geometry(md, lineArr, 14)
    if None in geom:
        geom = [lineArr[5].split('-'), lineArr[6].split('-')]
    z_crest1 = float(lineArr[7]) if lineArr[7] else None
    width1 = float(lineArr[8]) if lineArr[8] else None
    z_crest2 = float(lineArr[9]) if lineArr[9] else None
    width2 = float(lineArr[10]) if lineArr[10] else None
    cc = float(lineArr[13]) if lineArr[13] else None
    md.add_overflow_link(geom, z_crest1, z_crest2, width1, width2, cc, name=name)

def parse_lstk(md, lineArr):
    name = lineArr[2]
    geom = _sm_link_geometry(md, lineArr, 13)
    z_crest1 = float(lineArr[7]) if lineArr[7] else None
    width1 = float(lineArr[8]) if lineArr[8] else None
    length = float(lineArr[9]) if lineArr[9] else None
    rk = float(lineArr[10]) if lineArr[10] else None
    z_crest2 = max(float(lineArr[11]), z_crest1+1e-3) if lineArr[11] else None
    width2 = max(float(lineArr[12]), width1+1e-3) if lineArr[12] else None
    md.add_strickler_link(geom, z_crest1, width1, rk, z_crest2, width2, length, name=name)

def parse_lpor(md, lineArr):
    name = lineArr[2]
    geom = _sm_link_geometry(md, lineArr, 10)
    transmitivity = float(lineArr[7])
    width = float(lineArr[8])
    z_invert = float(lineArr[9])
    md.add_porous_link(geom, z_invert, width, transmitivity, name)

def parse_qdl(md, lineArr):
    name = lineArr[2]
    geom = _sm_link_geometry(md, lineArr, 10)
    z_weir = float(lineArr[7])
    width = float(lineArr[8])
    cc = float(lineArr[9])
    md.add_link_qms(geom, z_weir, width, cc, z_weir, None, name)

def parse_rout(md, lineArr):
    name=lineArr[2]
    geom = _link_geometry(md, lineArr[3], lineArr[4])
    cs = float(lineArr[7])
    length = float(lineArr[8])
    slope = float(lineArr[9])
    md.add_routing_hydrology_link(geom, cs, slope, length, 1,name)

def multi_parse_pl1d(md, lineArr, csv_streamFile ):
    name = lineArr[2]
    nodename = lineArr[3]
    geom = md.execute("""
        select ST_X(geom), ST_Y(geom) from $model.river_node
        where UPPER(name)=UPPER('{}')
        """.format(nodename)).fetchone()
    lineArr = _readline_to_array(csv_streamFile)
    pl1d_profile = _parse_array(lineArr, 1, 2, csv_streamFile) # 4 points max
    geom = "'srid=$srid; LINESTRINGZ({})'::geometry".format(
                ",".join([
                    str(p[0])+" "+str(p[1])+" "+"9999"
                    for p in pl1d_profile]))

    already_created = md.execute("""select exists(select 1 from $model.constrain where ST_Overlaps(geom, ST_MakeValid({})))""".format(geom))
    if not already_created:
        md.execute("""insert into $model.constrain(name, geom, constrain_type) values ('{}', ST_MakeValid({}), 'flood_plain_transect')""".format(name, geom))

def parse_ptr(md, lineArr):
    lineArr= lineArr + ([1.,1.] if len(lineArr)<23 else [])

    name=lineArr[2]
    nodename=lineArr[3]
    geom = _point_from_node(md, nodename)
    if geom is None:
        geom = _point(lineArr[4])

    z_up = float(lineArr[8]) if lineArr[8] else 0
    z_down = float(lineArr[17]) if lineArr[17] else 0

    type_map = {1:'valley', 2:'valley', 3: 'pipe', 4: 'channel', 5: 'ovoid', 6: 'circular'} # river is just a symetrical valley, type disapeared in new DB
    type_cs = {"up": type_map[int(lineArr[6][0])] if lineArr[6] else None,
               "down": type_map[int(lineArr[15][0])] if lineArr[15] else None}

    rk={'up':0,'down':0}
    rkmaj={'up':0,'down':0}
    rsinus={'up':0,'down':0}
    dn={'up':None,'down':None}
    topdiam={'up':None,'down':None}
    invertdiam={'up':None,'down':None}
    id_op_geom={'up':None,'down':None}
    id_cp_geom={'up':None,'down':None}
    id_vcs_geom={'up':None,'down':None}
    id_vcs_topo_sectgeom={'up':None,'down':None}

    for pos in ['up', 'down']:
        offset = 0 if pos == 'up' else 9
        if not lineArr[6+offset]:
            continue

        type_cs_ = type_cs[pos]
        idsect = lineArr[7+offset]

        dn[pos] = lineArr[9+offset] if type_cs_ in ['circular', 'ovoid'] else None
        topdiam[pos] = lineArr[10+offset] if type_cs_=='ovoid' else None
        invertdiam[pos] = lineArr[11+offset] if type_cs_=='ovoid' else None
        rk[pos] = lineArr[12+offset] if type_cs_ in ['pipe', 'channel', 'circular', 'ovoid'] else None

        if type_cs_ == 'valley':
            rk[pos] = max(0, float(lineArr[12+offset])) if lineArr[12+offset] else 0
            rkmaj[pos] = max(0, float(lineArr[13+offset])) if lineArr[13+offset] else 0
            rsinus[pos] = max(0, float(lineArr[14+offset])) if lineArr[14+offset] else 0

        attrib_open_geom = md.get_attrib_from_table_byname('open_parametric_geometry', idsect)
        attrib_closed_geom = md.get_attrib_from_table_byname('closed_parametric_geometry', idsect)
        attrib_valley_geom = md.get_attrib_from_table_byname('valley_cross_section_geometry', idsect)


        id_op_geom[pos] = attrib_open_geom[0][0] if type_cs_=='channel' and len(attrib_open_geom)>0 else None
        id_cp_geom[pos] = attrib_closed_geom[0][0] if type_cs_=='pipe' and len(attrib_closed_geom)>0 else None
        id_vcs_geom[pos] = attrib_valley_geom[0][0] if type_cs_=='valley' and len(attrib_valley_geom)>0 else None

        if type_cs_ == 'valley':
            topo_geomsect = md.get_attrib_from_table_byname('valley_cross_section_topo_geometry', idsect)
            if len(topo_geomsect)>0:
                id_vcs_topo_sectgeom[pos] = topo_geomsect[0][0]
            else:
                xzArr=[[-999.,-999.] for r in range(2)] # 2 points for topo geometry valley
                                                        # (see constraint with valley_cross_section)
                id_vcs_topo_sectgeom[pos] = md.add_valley_section_topo_geometry(xzArr, idsect)

    md.add_river_cross_section_profile(geom, z_up, z_down, type_cs["up"], type_cs["down"], rk['up'], rkmaj['up'], rsinus['up'], dn['up'], dn['up'], topdiam['up'], invertdiam['up'], id_cp_geom['up'],
                                                    id_op_geom['up'], id_vcs_geom['up'], id_vcs_topo_sectgeom['up'], rk['down'], rkmaj['down'], rsinus['down'], dn['down'], dn['down'], topdiam['down'],
                                                    invertdiam['down'],id_cp_geom['down'], id_op_geom['down'], id_vcs_geom['down'], id_vcs_topo_sectgeom['down'],name)

def multi_parse_topo(md, lineArr, csv_streamFile):
    name = lineArr[2]
    lineArr = _readline_to_array(csv_streamFile)
    xzArr = _parse_array(lineArr, 1, 2, csv_streamFile) # 150 rows max
    md.add_valley_section_topo_geometry(xzArr, name)

def multi_parse_sect(md, lineArr, csv_streamFile):
    name = lineArr[2]
    type_cs = {
            1: "river_cross_section_geometry",
            2: "valley_cross_section_geometry",
            3: "closed_parametric_geometry",
            4: "open_parametric_geometry"}[int(lineArr[3][0])]

    # 1st row: lamda, rmu1, rmu2, zw_lb, zw_rb
    if type_cs in ["river_cross_section_geometry", "valley_cross_section_geometry"]:
        lamda = float(lineArr[4]) if lineArr[4] else 1
        lamda = 0.01 if lamda<=0 else lamda
        rmu1 = float(lineArr[5]) if lineArr[5] else -99.
        rmu2 = float(lineArr[6]) if lineArr[6] else -99.

    if type_cs == "valley_cross_section_geometry":
        zw_lb = float(lineArr[7]) if lineArr[7] else -999.
        zw_rb = float(lineArr[8]) if lineArr[8] else -999.

    lineArr = _readline_to_array(csv_streamFile)
    zbminArr=[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]]
    zbtotArr=[[0,0],[0,0],[0,0],[0,0]]
    zbmajLbArr=[[0,0],[0,0],[0,0],[0,0]]
    zbmajRbArr=[[0,0],[0,0],[0,0],[0,0]]
    nbpt = 6 if type_cs in ["river_cross_section_geometry", "valley_cross_section_geometry"] else int(lineArr[1].split()[0])
    if lineArr[2]:
        zbminArr = [[float(lineArr[2]), float(lineArr[3])]]
    if type_cs == 'river_cross_section_geometry':
        if lineArr[5]: zbtotArr = [[float(lineArr[5]), float(lineArr[6])]]
    elif type_cs == 'valley_cross_section_geometry':
        if lineArr[5]: zbmajLbArr = [[float(lineArr[5]), float(lineArr[6])]]
        if lineArr[8]: zbmajRbArr = [[float(lineArr[8]), float(lineArr[9])]]

    for i in range(nbpt-1):
        lineArr = _readline_to_array(csv_streamFile)
        if lineArr[2]:
            zbminArr.append([float(lineArr[2]), float(lineArr[3])])
        if type_cs=="river_cross_section_geometry":
            if lineArr[5]:
                zbtotArr.append([float(lineArr[5]), float(lineArr[6])])
        elif type_cs=="valley_cross_section_geometry":
            if lineArr[5]:
                zbmajLbArr.append([float(lineArr[5]), float(lineArr[6])])
            if lineArr[8]:
                zbmajRbArr.append([float(lineArr[8]), float(lineArr[9])])

    already_exists = md.execute("""select 1 from $model.valley_cross_section_geometry where UPPER(name)=UPPER('{}')""".format(name)).fetchone()
    if already_exists:
        # doublon
        return
    if type_cs == "river_cross_section_geometry":
        md.add_valley_section_geometry(zbminArr, zbtotArr, zbtotArr, lamda, None, None, rmu1, rmu2, name)
    elif type_cs == "valley_cross_section_geometry":
        md.add_valley_section_geometry(zbminArr, zbmajLbArr, zbmajRbArr, lamda, rmu1, rmu2, zw_lb, zw_rb, name)
    elif type_cs == "open_parametric_geometry":
        md.add_pipe_channel_open_section_geometry(zbminArr, name)
    elif type_cs == "closed_parametric_geometry":
        md.add_pipe_channel_closed_section_geometry(zbminArr, name)

class LineCounter(object):
    """to count lines read in functions with readline"""
    def __init__(self, f):
        self.__f = f
        self.count = 0
    def readline(self):
        self.count += 1
        return self.__f.readline()

def log_info(info, info_form):
    if info_form is not None:
        info_form.textInfos.append(info)
        QApplication.processEvents()

def import_model_csv(filename, project, model, model_name, logger=LogManager(), infoform=None):
    progress = logger.progress("loading "+filename)
    log_info("loading "+filename, infoform)
    with codecs.open(filename, 'r', 'latin-1') as f:
        number_of_lines = len(f.readlines())
    model_name = model_name.lower()

    log_info("disable triggers...", infoform)
    project.execute("""update {}.metadata set(trigger_coverage, trigger_branch) = ('f', 'f');""".format(model_name))

    with codecs.open(filename, 'r', 'latin-1') as csv_streamFile:
        line_number = 0
        for line in csv_streamFile:
            line_number += 1
            csv_streamFile_counted = LineCounter(csv_streamFile)
            line = line.strip()
            progress.set_ratio(float(line_number)/number_of_lines)
            lineArr = [v[1:-1] if len(v) and v[0]=="'" and v[-1]=="'" else v
                    for v in line.split(';')]
            # /!\ Ne pas modifier les outputs en cas d'erreur svp /!\
            if lineArr[0].lower() == model_name:
                csv_kword = lineArr[1].lower()
                log_info("import "+line, infoform)
                try:
                    parser = "parse_"+csv_kword
                    multi_parser = "multi_parse_"+csv_kword
                    # find the function name in the list of defined functions
                    if parser in globals():
                        globals()[parser](model, lineArr)
                    elif multi_parser in globals():
                        globals()[multi_parser](model, lineArr, csv_streamFile_counted)
                    else:
                        logger.warning("\n{f}:{n}\n{l}\nunknown keyword: {k}".format(f=filename,
                                                                                    n=line_number,
                                                                                    l=u''.join(line).encode('utf-8').strip(),
                                                                                    k=csv_kword))
                    project.commit()
                except Exception as error:
                    logger.error("\n{f}:{n}\n{l}".format(f=filename, n=line_number, l=u''.join(line).encode('utf-8').strip()))
                    project.rollback()
                    raise
            elif lineArr[0].lower() == 'modele':
                pass
            elif line[0:2] == ';;':
                logger.warning("\n{f}:{n}\n{l}\nuseless line".format(f=filename,
                                                                    n=line_number,
                                                                    l=u''.join(line).strip()))
            elif line == '':
                logger.warning("\n{f}:{n}\n{l}\nempty line".format(f=filename,
                                                                    n=line_number,
                                                                    l=u''.join(line).strip()))
            else:
                logger.warning("unrecognised line {f}:{n} {l}".format(f=filename,
                                                                    n=line_number,
                                                                    l=u''.join(line).strip())
                        + (" (hint: does model name match file name ?)" if line_number < 3 else ""))
                project.rollback()
            line_number += csv_streamFile_counted.count
        # cleanup graphic river nodes
        log_info("cleanup graphic river nodes...", infoform)
        model.execute("""
            delete from $model.river_node where id in (
                select n.id from $model.river_node as n, $model._link as l
                where n.reach is null
                and ST_Intersects(n.geom, l.geom)
                and not ST_Intersects(n.geom, ST_StartPoint(l.geom))
                and not ST_Intersects(n.geom, ST_EndPoint(l.geom))
                and ST_NumPoints(l.geom)=4
                )""")

         # cleanup graphic river nodes
        log_info("check manholes and pipes connectivity...", infoform)
        model.execute("""update $model.manhole_node set geom=geom""")

        # reasign boundary conditions on unconnected river nodes to the
        # container in wich they lie
        # log_info("reasign boundary conditions... ", infoform)
        # model.execute("""
            # with storage as (
                # select id, name, contour, geom, 'elem_2d' as nt
                # from $model.elem_2d_node
                # union
                # select id, name, contour, geom, 'storage' as nt
                # from $model.storage_node
                # ),
            # sing_geom as (
                # select s.id as sid, c.id as nid, c.nt::hydra_node_type as nt
                # from $model._singularity as s
                # join $model.river_node as n on n.id=s.node
                # join storage as c on ST_Intersects(c.contour, n.geom)
                # where n.reach is null
                # )
            # update $model._singularity set node=nid, node_type=nt
            # from sing_geom where id=sid;
            # """)

        log_info("re-affecting river nodes to reaches...", infoform)
        misplaced_nodes = [id for id, in model.execute("""select id from $model.river_node where reach is null""").fetchall()]
        for node_id in misplaced_nodes:
            projection, = model.execute("""with reach as (select st_collect(geom) as geom from $model.reach)
                                            select st_astext(ST_SetSRID(ST_MakePoint(ST_X(st_closestpoint(reach.geom, n.geom)), ST_Y(st_closestpoint(reach.geom, n.geom)), project.altitude(st_closestpoint(reach.geom, n.geom))), $srid))
                                                from reach, $model.river_node as n where id={};""".format(node_id)).fetchone()
            if projection is not None:
                query = model.execute("""select id from $model.river_node where st_intersects(st_buffer(geom, 1),'SRID=$srid; {}'::geometry)""".format(projection)).fetchone()
                if query:
                    existing_node = query[0]
                    if node_id == existing_node:
                        #change node geom to its projection on reach
                        model.execute("""with reach as (select st_collect(geom) as geom from $model.reach)
                                update $model.river_node as n
                                    set geom=ST_SetSRID(ST_MakePoint(ST_X(st_closestpoint(reach.geom, n.geom)), ST_Y(st_closestpoint(reach.geom, n.geom)), project.altitude(st_closestpoint(reach.geom, n.geom))), $srid)
                                    from reach
                                    where id={n};
                                update $model.river_node as n
                                    set reach = (select id from $model.reach as r where st_intersects(n.geom, st_buffer(r.geom,1)) order by r.id asc limit 1)
                                    where id={n};""".format(n=node_id))
                    else:
                        #put links and geom on already existing node
                        delete = True
                        model.execute(f"update $model._link set up={existing_node} where up={node_id}")
                        model.execute(f"update $model._link set down={existing_node} where down={node_id}")
                        if not model.execute(f"select count(1) from $model._singularity where node={existing_node}").fetchone()[0]:
                            model.execute(f"update $model._singularity set node={existing_node} where node={node_id}")
                        else:
                            logger.warning(f"node {node_id} already has singularity, cannot move singularity from node {node_id}")
                            delete = False
                        model.execute(f"update $model._river_cross_section_profile set id={existing_node} where id={node_id}")
                        if delete:
                            model.execute(f"delete from $model.river_node where id={node_id}")

        log_info("cleanup unecessary river nodes...", infoform)
        model.execute("""
            with to_del as (
                select id from $model.river_node
                except
                select up from $model._link
                except
                select down from $model._link
                except
                select node from $model._singularity
                except
                select id from $model._river_cross_section_profile
                )
            delete from $model.river_node where reach is null and id in (select id from to_del)
        """)

        log_info("re-activating triggers...", infoform)
        project.execute("""update {}.metadata set(trigger_coverage, trigger_branch) = ('t', 't');""".format(model_name))
        project.execute("select {}.coverage_update();".format(model_name))
        project.execute("select {}.branch_update_fct();".format(model_name))

        log_info("saving changes", infoform)
        project.commit()
        progress.set_ratio(1)
        project.log.clear_progress()

if __name__=="__main__":
    import sys
    from hydra.database.database import project_exists, remove_project
    from hydra.project import Project

    project_name = sys.argv[1]
    srid = int(sys.argv[2])
    csv_file = sys.argv[3]
    assert csv_file[-4:]=='.csv'
    model_name = os.path.split(csv_file)[1][:-4]

    # remove_project(project_name)

    if not project_exists(project_name):
        Project.create_new_project(project_name, srid)
    project = Project.load_project(project_name)
    if model_name in project.get_models():
        project.delete_model(model_name)
    project.add_new_model(model_name)
    project.set_current_model(model_name)
    import_model_csv(csv_file, project, project.get_current_model(), model_name)
    project.commit()
