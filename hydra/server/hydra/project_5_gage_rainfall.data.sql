/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

create table project.gage_rainfall_data(
    rainfall integer not null references project._gage_rainfall(id) on delete cascade,
    rain_gage integer not null references project.rain_gage(id) on delete cascade,
    is_active boolean,
    t_mn_hcum_mm real[100][2] default '{{0,0}}' check(array_length(t_mn_hcum_mm, 1) <=1000 and array_length(t_mn_hcum_mm, 1)>=1 and array_length(t_mn_hcum_mm, 2)=2),
    constraint pk_id primary key (rainfall, rain_gage)
)
;;

