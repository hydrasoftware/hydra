# -*- coding: utf-8 -*-

"""
***************************************************************************
    areadinf.py
    ---------------------
    Date                 : January 2018
    Copyright            : (C) 2018-2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'January 2018'
__copyright__ = '(C) 2018-2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.core import (QgsVectorFileWriter,
                       QgsProcessing,
                       QgsProcessingException,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterBoolean,
                       QgsProcessingParameterRasterDestination
                      )

from .taudemAlgorithm import TauDemAlgorithm
from . import taudemUtils


class AreaDinf(TauDemAlgorithm):

    DINF_FLOWDIR = 'DINF_FLOWDIR'
    WEIGHT_GRID = 'WEIGHT_GRID'
    OUTLETS = 'OUTLETS'
    EDGE_CONTAMINATION = 'EDGE_CONTAMINATION'
    DINF_CONTRIB_AREA = 'DINF_CONTRIB_AREA'

    def name(self):
        return 'areadinf'

    def displayName(self):
        return self.tr('D-infinity contributing area')

    def group(self):
        return self.tr('Basic grid analysis')

    def groupId(self):
        return 'basicanalysis'

    def tags(self):
        return self.tr('dem,hydrology,d-infinity,contributing area,catchment area').split(',')

    def shortHelpString(self):
        return self.tr('Calculates a grid of specific catchment area which is '
                       'the contributing area per unit contour length using '
                       'the multiple flow direction D-infinity approach.')

    def helpUrl(self):
        return 'http://hydrology.usu.edu/taudem/taudem5/help53/DInfinityContributingArea.html'

    def __init__(self):
        super().__init__()

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterRasterLayer(self.DINF_FLOWDIR,
                                                            self.tr('D-infinity flow directions')))
        self.addParameter(QgsProcessingParameterFeatureSource(self.OUTLETS,
                                                              self.tr('Outlets'),
                                                              types=[QgsProcessing.TypeVectorPoint],
                                                              defaultValue=None,
                                                              optional=True))
        self.addParameter(QgsProcessingParameterRasterLayer(self.WEIGHT_GRID,
                                                            self.tr('Weight grid'),
                                                            defaultValue=None,
                                                            optional=True))
        self.addParameter(QgsProcessingParameterBoolean(self.EDGE_CONTAMINATION,
                                                        self.tr('Check for edge contamination'),
                                                        defaultValue=True))

        self.addParameter(QgsProcessingParameterRasterDestination(self.DINF_CONTRIB_AREA,
                                                                  self.tr('D-infinity specific catchment area')))

    def processAlgorithm(self, parameters, context, feedback):
        dinfLayer = self.parameterAsRasterLayer(parameters, self.DINF_FLOWDIR, context)
        if dinfLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.DINF_FLOWDIR))

        arguments = []
        arguments.append(self.command())

        arguments.append('-ang')
        arguments.append(dinfLayer.source())

        if self.OUTLETS in parameters and parameters[self.OUTLETS] is not None:
            outlets = self.parameterAsCompatibleSourceLayerPath(parameters, self.OUTLETS, context,
                                                                QgsVectorFileWriter.supportedFormatExtensions(),
                                                                feedback=feedback)
            arguments.append('-o')
            arguments.append(outlets)

        if self.WEIGHT_GRID in parameters and parameters[self.WEIGHT_GRID] is not None:
            weight = self.parameterAsRasterLayer(parameters, self.WEIGHT_GRID, context)
            arguments.append('-wg')
            arguments.append(weight.source())

        if not self.parameterAsBool(parameters, self.EDGE_CONTAMINATION, context):
            arguments.append('-nc')

        arguments.append('-sca')
        arguments.append(self.parameterAsOutputLayer(parameters, self.DINF_CONTRIB_AREA, context))

        taudemUtils.execute(arguments, feedback)

        return self.algorithmResults(parameters)
