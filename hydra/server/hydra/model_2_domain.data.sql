/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*                Flood areas                      */
/* ********************************************** */

create table $model.urban_flood_risk_area(
    id serial primary key,
    name varchar(16) unique not null default project.unique_name('UFRA_'),
    geom geometry('POLYGONZ',$srid)
)
;;

create table $model.bank(
    id serial primary key,
    name varchar(16) unique not null default project.unique_name('BANK_'),
    geom geometry('LINESTRINGZ',$srid) not null
)
;;

/* ********************************************** */
/*                Catchment                       */
/* ********************************************** */

create table $model.catchment(
    id serial primary key,
    name varchar(16) unique not null default project.unique_name('CATCHMENT_'),
    geom geometry('POLYGON',$srid) not null check(ST_IsValid(geom))
)
;;

/* ********************************************** */
/*                  Constrain lines               */
/* ********************************************** */

create table $model._constrain(
    id serial primary key,
    name varchar(16) unique not null,
    geom geometry('LINESTRINGZ',$srid) not null check(ST_IsValid(geom)),
    discretized geometry('LINESTRINGZ',$srid) not null check(ST_IsValid(geom)),
    elem_length real not null default 100,
    constrain_type hydra_constrain_type,
    link_attributes json,
    points_xyz integer references project.points_type(id) on delete set null,
    points_xyz_proximity real not null default 1,
    line_xyz integer references project.line_xyz(id) on delete set null,
    comment varchar
)
;;

/* ********************************************** */
/*                    Coverages                   */
/* ********************************************** */

create table $model.coverage(
    id serial primary key,
    domain_type hydra_coverage_type,
    geom geometry('POLYGONZ',$srid) not null check(ST_IsValid(geom))
)
;;

create table $model.coverage_marker(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('COV_NULL_'),
    comment varchar(256),
    protected boolean default 'f',
    geom geometry('POINTZ',$srid) not null
)
;;

/* ********************************************** */
/*                 2D domain                      */
/* ********************************************** */

create table $model.domain_2d(
    id serial primary key,
    name varchar(16) unique not null default project.unique_name('DOMAIN_2D_') check (name <> 'default_domain'),
    geom geometry('POLYGONZ',$srid),
    comment varchar(256)
)
;;

create table $model.domain_2d_marker(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('DOMAIN_MARKER_'),
    domain_2d integer references $model.domain_2d(id),
    geom geometry('POINTZ',$srid) not null,
    comment varchar(256)
)
;;

/* ********************************************** */
/*                     Reach                      */
/* ********************************************** */

create table $model.reach(
    id serial primary key,
    name varchar(16) unique not null default project.unique_name('REACH_'),
    pk0_km real not null default 0,
    dx real not null default 50,
    geom geometry('LINESTRINGZ',$srid) not null check(ST_IsValid(geom)),
    comment varchar(256)
)
;;

create index ${model}_reach_geom_idx on $model.reach using gist(geom)
;;

/* ********************************************** */
/*                     Branch                     */
/* ********************************************** */

create table $model.branch(
    id integer primary key,
    name varchar(24) unique,
    pk0_km real not null default 0,
    dx real not null default 50,
    geom geometry('LINESTRINGZ',$srid) not null check(ST_IsValid(geom))
)
;;

/* ********************************************** */
/*                      Street                    */
/* ********************************************** */

create table $model.street(
    id serial primary key,
    name varchar(16) unique not null default project.unique_name('STREET_'),
    width real not null default 0,
    width_invert real default null,
    elem_length real default 100,
    rk real not null default 40.,
    geom geometry('LINESTRINGZ',$srid) not null check(ST_IsValid(geom)),
    comment varchar(256)
)
;;

create index ${model}_street_geom_idx on $model.street using gist(geom)
;;

/* ********************************************** */
/*                      Station                   */
/* ********************************************** */

create table $model.station(
    id serial primary key,
    name varchar(16) unique not null default project.unique_name('STATION_'),
    geom geometry('POLYGONZ',$srid) not null check(ST_IsValid(geom)),
    comment varchar(256)
)
;;

