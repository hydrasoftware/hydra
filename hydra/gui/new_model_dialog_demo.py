# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

def demo():

    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.new_model_dialog import NewModelDialog
    from hydra.project import Project
    from hydra.utility.log import LogManager, ConsoleLogger
    from hydra.utility import empty_ui_manager as ui_manager
    from hydra.utility.string import normalized_name

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    sys.stderr.write("Project name: ")
    project_name = sys.stdin.readline()
    project_name = normalized_name(project_name)
    sys.stderr.write("Normalized project name:" + project_name)
    project = Project.load_project(project_name, LogManager(ConsoleLogger(), "Hydra"), ui_manager.EmptyUIManager())

    new_model_dialog = NewModelDialog(project)
    new_model_dialog.exec_()

