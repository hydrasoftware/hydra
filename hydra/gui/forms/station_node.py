# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..base_dialog import BaseDialog, tr
import hydra.utility.string as string
from qgis.PyQt import uic
import os

class StationNodeEditor(BaseDialog):
    def __init__(self, project, geom, id=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "station_node.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.__model = self.project.get_current_model()
        assert not self.__model == None
        self.__geom = geom
        if id==None:
            self.__id = self.__model.add_station_node(self.__geom)
        else:
            self.__id = id
            if self.__geom is None:
                geom,= self.project.execute("""
                select geom
                from {}.station_node
                where id={}""".format(self.__model.name, self.__id)).fetchone()

        name, area, z_invert, station_name, comment = self.project.execute("""
            select n.name, n.area, n.z_invert, s.name, n.comment
            from {model}.station_node as n, {model}.station as s
            where n.id={id} and n.station=s.id""".format(model=self.__model.name, id=self.__id)).fetchone()
        self.name.setText(name)
        self.area.setText(string.get_str(area))
        self.z_invert.setText(string.get_str(z_invert))

        self.station_name.setText(str(station_name))
        self.notes.setText(comment)

    def get_id(self):
        return self.__id

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.__model.update_station_node(self.__id, self.__geom, string.get_sql_float(self.z_invert.text()), string.get_sql_float(self.area.text()), self.name.text(), comment=self.notes.text())
        BaseDialog.save(self)
        self.close()
