# coding=UTF-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run all tests

USAGE

    test.py [-hk] [projet_name model1.csv [model2.csv ...]]

    if project name and model.csv are specified, import the model in the specified project (create it if needed)

OPTIONS

   -h, --help
        print this help

   -k, --keep
       don't remove the database after the test
"""

import os, sys
import getopt
from hydra.project import Project
from hydra.database.import_model import import_model_csv
from hydra.database.database import TestProject, project_exists, remove_project

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hk",
            ["help", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

keep =  "-k" in optlist or "--keep" in optlist

if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

data_dir = os.path.join(os.path.dirname(__file__), 'test_data', 'light_projects')

project_name, models = 'import_model_test', [os.path.join(data_dir, f)
        for f in os.listdir(data_dir) if f.endswith(".csv")]

if project_exists(project_name):
    remove_project(project_name)

if len(args) >= 2:
    project_name, models = args[0], args[1:]
    if not project_exists(project_name):
        Project.create_new_project(project_name, 2154)
else:
    test_project = TestProject(project_name, keep)

project = Project.load_project(project_name)

for model in models:
    model_name = os.path.split(model)[1][:-4]
    if model_name in project.get_models():
        project.delete_model(model_name)
    project.add_new_model(model_name)
    project.set_current_model(model_name)

    import_model_csv(model, project, project.get_current_model(), model_name)
    project.commit()

    model = project.get_current_model()
    invalidities = model.execute("""
        select id, name, subtype::varchar||'_'||type::varchar, reason from $model.invalid
        """).fetchall()
    if len(invalidities):
        sys.stderr.write("warning: invalidities in {}:\n".format(model_name))
        sys.stderr.write("\n".join(("\t"+"\t".join((str(j) for j in i)) for i in invalidities)))
        sys.stderr.write("\n")
print('ok')
