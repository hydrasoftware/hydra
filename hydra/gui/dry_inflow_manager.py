# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import csv
from qgis.PyQt import uic, QtGui, QtCore
from qgis.PyQt.QtWidgets import QTableWidgetItem, QFileDialog, QMessageBox
from hydra.gui.base_dialog import BaseDialog, tr
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
import hydra.utility.string as string

_desktop_dir = os.path.join(os.path.expanduser('~'), "Desktop")

class DryInflowManager(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "dry_inflow_manager.ui"), self)

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        #import class attributes
        self.new_sector_list = []
        self.first_line = None

        self.table_dry_inflow_scenario = HydraTableWidget(project,
            ("id", "name", "comment"),
            (tr("Id"), tr("Name"), tr("Comment")),
            "project", "dry_inflow_scenario",
            (self.new_dry_scenario, self.delete_dry_scenario), "", "id",
            self.table_dry_inflow_scenario_placeholder)
        self.table_dry_inflow_scenario.set_editable(True)
        self.table_dry_inflow_scenario.data_edited.connect(self.save_dry_scenario)
        self.table_dry_inflow_scenario.table.itemSelectionChanged.connect(self.set_active_dry_scenario)
        self.table_dry_inflow_scenario.table.resizeColumnsToContents()
        self.active_dry_scenario_id=None
        self.set_active_dry_scenario()

        self.import_btn.clicked.connect(self.__import)

        self.table_dry_inflow_scenario_settings.resizeColumnsToContents()
        self.table_dry_inflow_scenario_settings.horizontalHeader().setStretchLastSection(True)

    def __import(self):
        '''Import file into dry inflow sectors settings'''
        missing_sector_list = []
        valid_sector_list = []
        self.new_sector_list = []
        fileName, __ = QFileDialog.getOpenFileName(None, tr('Import from file'), _desktop_dir, tr('Csv files (*.csv)'))
        if fileName:
            missing_sector_list, valid_sector_list = self.import_file(fileName)

        if missing_sector_list :
            msg = QMessageBox(QMessageBox.Warning, tr('Non existing sector'), tr('There is some missing sector in the database, see details below. '), QMessageBox.Ok)
            msg.setDetailedText(tr('Missing sectors : ' + ', '.join(sector_name for sector_name in missing_sector_list)))
            msg.exec_()

    def import_file(self, fileName):
        '''Import specified file into dry inflow sectors settings'''
        valid_sector_list = []
        missing_sector_list = []

        if fileName:
            with open(fileName,'r') as dry_inflow_csv:
                dialect = csv.Sniffer().sniff(dry_inflow_csv.read(1024))
                dry_inflow_csv.seek(0)
                dry_inflow_reader = csv.reader(dry_inflow_csv,dialect=dialect)
                self.first_line = next(dry_inflow_csv)
                self.second_line = next(dry_inflow_csv)
                if int(self.second_line.replace("'","").replace(";","")) == 1 :
                    self.second_line = "weekday"
                else :
                    self.second_line = "weekend"
                for row in dry_inflow_reader :
                    self.new_sector_list.append(row)

        existing_sectors = self.project.execute(""" select name, id from project.dry_inflow_sector """).fetchall()

        for sectors in self.new_sector_list :
            if sectors[0].lower() not in [sector[0].lower() for sector in existing_sectors] :
                missing_sector_list.append(sectors[0])
            else :
                valid_sector_list.append(sectors)

        try:
            id_dry_scn = self.project.execute("""insert into project.dry_inflow_scenario (name, period) values ('{}','{}') returning id""".format(self.first_line.split(';')[0], self.second_line)).fetchone()[0]
        except:
            self.project.log.error("Error reading first line of {}".format(fileName))
            raise ImportDryInflowError("Error reading first line of {}".format(fileName))

        for sectors in valid_sector_list :
            self.project.execute("""insert into project.dry_inflow_scenario_sector_setting (dry_inflow_scenario,sector,volume_sewage_m3day,
            volume_clear_water_m3day,coef_volume_sewage_m3day,coef_volume_clear_water_m3day) values ('{}',{},'{}','{}','{}','{}')""".format(
            id_dry_scn,self.__get_id_sector_from_name(sectors[0]),sectors[1],sectors[2],sectors[3],sectors[4]))

        self.table_dry_inflow_scenario.update_data()

        return(missing_sector_list, valid_sector_list)

    def __get_id_sector_from_name(self, name):
        '''returns id of dry inflow sector with specified name'''
        id_sector = self.project.execute("""select id from project.dry_inflow_sector where LOWER(name) = LOWER('{name}')""".format(name=name)).fetchone()[0]
        return id_sector

    def select_by_name(self, name):
        for i in range(0, self.table_dry_inflow_scenario.table.rowCount()):
            if self.table_dry_inflow_scenario.table.item(i,1).text()==name:
                self.table_dry_inflow_scenario.table.setCurrentCell(i,0)

    def set_active_dry_scenario(self):
        '''populates dry inflow scn settings based on select dry inflow scn'''
        items = self.table_dry_inflow_scenario.table.selectedItems()
        if len(items)>0:
            if not self.active_dry_scenario_id is None:
                self.save_dry_scenario()

            self.active_dry_scenario_id = items[0].text()
            self.lbl_dry_scenario.setText(items[1].text())

            period = self.project.execute("""select period from project.dry_inflow_scenario where id={};""".format(self.active_dry_scenario_id)).fetchone()[0]
            self.radioWeekdays.setEnabled(True)
            self.radioWeekdays.setChecked(period=='weekday')
            self.radioWeekend.setEnabled(True)
            self.radioWeekend.setChecked(period=='weekend')
            self.radioAuto.setEnabled(True)
            self.radioAuto.setChecked(period=='auto')

            sectors_settings = self.project.execute("""select dis.id, dis.name, settings.volume_sewage_m3day,
                settings.volume_clear_water_m3day, settings.coef_volume_sewage_m3day, settings.coef_volume_clear_water_m3day
                from project.dry_inflow_sector as dis
                left join project.dry_inflow_scenario_sector_setting as settings on dis.id = settings.sector
                and settings.dry_inflow_scenario={}
                order by dis.name;""".format(self.active_dry_scenario_id)).fetchall()
            self.set_dry_inflow_scenario_settings_table_items(sectors_settings)
        else:
            self.active_dry_scenario_id = None
            self.lbl_dry_scenario.setText("")
            self.radioWeekdays.setEnabled(False)
            self.radioWeekend.setEnabled(False)
            self.radioAuto.setEnabled(False)
            self.set_dry_inflow_scenario_settings_table_items([])

    def new_dry_scenario(self):
        '''creates dry inflow scn'''
        self.table_dry_inflow_scenario.add_row(['default'])

    def delete_dry_scenario(self):
        '''delete selected dry inflow scn'''
        self.table_dry_inflow_scenario.del_selected_row()

    def save_dry_scenario(self):
        '''saves dry inflow scns'''
        self.table_dry_inflow_scenario.save_selected_row()
        if self.active_dry_scenario_id is not None:
            if self.radioWeekdays.isChecked():
                period = 'weekday'
            elif self.radioWeekend.isChecked():
                period = 'weekend'
            else:
                period = 'auto'

            self.project.execute(f"""update project.dry_inflow_scenario set period='{period}' where id={self.active_dry_scenario_id};""")

            items = self.get_dry_inflow_scenario_settings_table_items()
            if len(items)>0:
                values = list()
                for item in items:
                    values.append("({},{},{},{},{},{})".format(self.active_dry_scenario_id,
                        item[0], item[2], item[3], item[4], item[5]))
                self.project.execute("""insert into project.dry_inflow_scenario_sector_setting(dry_inflow_scenario, sector, volume_sewage_m3day,
                    volume_clear_water_m3day, coef_volume_sewage_m3day, coef_volume_clear_water_m3day)
                    values {} on conflict (dry_inflow_scenario, sector)
                    do update set sector=excluded.sector, volume_sewage_m3day=excluded.volume_sewage_m3day,
                    volume_clear_water_m3day=excluded.volume_clear_water_m3day, coef_volume_sewage_m3day=excluded.coef_volume_sewage_m3day,
                    coef_volume_clear_water_m3day=excluded.coef_volume_clear_water_m3day;
                    """.format(','.join(values)))

        items = self.table_dry_inflow_scenario.table.selectedItems()
        if len(items)>0:
            self.lbl_dry_scenario.setText(items[1].text())

    def set_dry_inflow_scenario_settings_table_items(self, sectors_settings):
        '''populates dry inflow scn settings'''
        self.table_dry_inflow_scenario_settings.setRowCount(0)
        n = len(sectors_settings)
        self.table_dry_inflow_scenario_settings.setRowCount(n)
        for row in range(0, n):
            for col in range(0, 6):
                self.table_dry_inflow_scenario_settings.setItem(row,col,QTableWidgetItem(string.get_str(sectors_settings[row][col])))
                if col in [0, 1]:
                    self.table_dry_inflow_scenario_settings.item(row,col).setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)

    def get_dry_inflow_scenario_settings_table_items(self):
        '''returns current dry inflow scn settings'''
        n = self.table_dry_inflow_scenario_settings.rowCount()
        result = list()
        index_list=0
        for i in range(0, n):
            result.append(list())
            for o in range(0,2):
                result[index_list].append(self.table_dry_inflow_scenario_settings.item(i,o).text())
            for o in range(2,6):
                result[index_list].append(string.get_sql_forced_float(self.table_dry_inflow_scenario_settings.item(i,o).text()))
            index_list = index_list + 1
        return result

    def save(self):
        '''saves form and commit'''
        if not self.active_dry_scenario_id is None:
            self.save_dry_scenario()
        BaseDialog.save(self)
        self.close()

class ImportDryInflowError(Exception):
    pass
