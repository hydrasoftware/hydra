/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update hydra.metadata set version = '3.4';
;;

do
$$$$
    begin
    if exists (
        select 1 from pg_catalog.pg_namespace where nspname = 'csv'
    )
    then
      execute 'insert into csv.hydra_compatible_versions(hydra_version) values (''3.4'')';
    end if;
end
$$$$;
;;

create or replace function project.model_instead_fct()
returns trigger
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_model
    for statement in create_model(TD['new']['name'], $srid, '$version'):
        plpy.execute(statement)
$$$$
;;

/* ********************************************** */
/*  Missing table (support feedback)              */
/* ********************************************** */

create table if not exists project.wind_scenario(
    id serial primary key,
    name varchar(24) not null default 'WIND_'||currval('project.wind_scenario_id_seq')::varchar,
    grid_connect_file varchar(256) not null default '',
    interpolation hydra_rainfall_interpolation_type not null default 'shortest_distance'
)
;;


/* ********************************************** */
/*  Workspace                                     */
/* ********************************************** */

update project.dem set source=replace(source, '$workspace', (select workspace from hydra.metadata));
;;

create or replace function project.altitude(x real, y real)
returns real
language plpython3u immutable
as
$$$$
    from $hydra import altitude
    import plpy
    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select source from project.dem order by priority asc")]
    return altitude(sources, x, y)
$$$$
;;

create or replace function project.set_altitude(geom geometry)
returns geometry
language plpython3u immutable
as
$$$$
    from $hydra import set_altitude
    import plpy
    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select source from project.dem order by priority asc")]
    return set_altitude(sources, geom, $srid)
$$$$
;;

create or replace function project._str_filling_curve(polygon geometry)
returns varchar
language plpython3u immutable
as
$$$$
    from $hydra import filling_curve
    import plpy
    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select source from project.dem order by priority asc")]
    return filling_curve(sources, polygon)
$$$$
;;

create or replace function project.crest_line(line geometry)
returns geometry
language plpython3u immutable
as
$$$$
    from $hydra import crest_line
    import plpy
    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select source from project.dem order by priority asc")]
    return crest_line(sources, line, $srid)
$$$$
;;


/* ********************************************** */
/*  Transect concepts                             */
/* ********************************************** */

create or replace function project.topo(line_ geometry)
returns geometry
language plpython3u
immutable
as
$$$$
    from $hydra import line_elevation, wkb_loads
    from shapely.geometry import LineString
    if line_ is None:
        return None
    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select source from project.dem order by priority desc")]
    assert(len(sources) == 1)
    return line_elevation(line_, sources[0])
$$$$
;;

create table project.line_xyz(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('L_XYZ_'),
    source varchar(24) default null,
    comment varchar(256) default null,
    geom geometry('LINESTRINGZ',$srid) not null
)
;;

create index project_line_xyz_geom_idx on project.line_xyz using gist(geom)
;;

create or replace function project.orient_transect(reach geometry, transect geometry)
returns geometry
language plpgsql immutable as
$$$$
declare
    a double precision;
    u geometry;
begin
    a := st_linelocatepoint(reach, st_geometryn(st_multi(st_intersection(reach, transect)), 1));
    u := st_linesubstring(reach, a, a+.1/st_length(reach));
    if (st_x(st_endpoint(u)) - st_x(st_startpoint(u)))*(st_y(st_endpoint(transect)) - st_y(st_startpoint(transect)))
        - (st_y(st_endpoint(u)) - st_y(st_startpoint(u)))*(st_x(st_endpoint(transect)) - st_x(st_startpoint(transect))) < 0 then
        return transect;
    else
        return st_reverse(transect);
    end if;
end
$$$$
;;

create or replace function project.fake_transect(reach geometry, node geometry, length_ double precision)
returns geometry
language plpgsql immutable as
$$$$
declare
    a double precision;
    l double precision;
    o geometry;
    r geometry;
begin
    r := st_force2d(reach);
    l := st_length(r);
    a := st_linelocatepoint(r, node);
    if (a*l + .1)/l > 1 then
        o := st_lineinterpolatepoint(r, (a*l - .1)/l);
        return st_rotate(st_scale(st_makeline(
                        o,
                        st_translate(node, st_x(node) - st_x(o), st_y(node) - st_y(o))
                        ), ('POINT('||5*length_||' '||5*length_||')')::geometry, node), -pi()/2, node);
    elsif (a*l - .1)/l < 0 then
        o := st_lineinterpolatepoint(r, (a*l + .1)/l);
        return st_rotate(st_scale(st_makeline(
                        st_translate(node, st_x(node) - st_x(o), st_y(node) - st_y(o)),
                        o
                        ), ('POINT('||5*length_||' '||5*length_||')')::geometry, node), -pi()/2, node);
    else
        return st_rotate(st_scale(st_makeline(
                        st_lineinterpolatepoint(r, (a*l - .1)/l),
                        st_lineinterpolatepoint(r, (a*l + .1)/l)
                        ), ('POINT('||5*length_||' '||5*length_||')')::geometry, node), -pi()/2, node);
    end if;
end
$$$$
;;


/* ********************************************** */
/*  Aerolics                                      */
/* ********************************************** */

--isolated--
alter type hydra_link_type add value if not exists 'air_duct'
;;
--isolated--
alter type hydra_link_type add value if not exists 'air_headloss'
;;
--isolated--
alter type hydra_link_type add value if not exists 'ventilator'
;;
--isolated--
alter type hydra_link_type add value if not exists 'jet_fan'
;;
--isolated--
alter type hydra_singularity_type add value if not exists 'air_flow_bc'
;;
--isolated--
alter type hydra_singularity_type add value if not exists 'air_pressure_bc'
;;

insert into hydra.link_type(name, id, abbreviation) values
 ('air_duct', 25, 'AD'),
 ('air_headloss', 26, 'HL'),
 ('ventilator', 27, 'V'),
 ('jet_fan', 28, 'JF')
;;

insert into hydra.singularity_type(name, id, abbreviation) values
 ('air_flow_bc', 26, 'AF'),
 ('air_pressure_bc', 27, 'AP')
;;

alter table project.scenario add column has_aeraulics boolean not null default false
;;
alter table project.scenario add column speed_of_sound_in_air real not null default 340
;;
alter table project.scenario add column absolute_ambiant_air_pressure real null default 1.e5
;;
alter table project.scenario add column average_altitude_msl real not null default 0
;;
alter table project.scenario add column air_water_skin_friction_coefficient real not null default .01
;;
alter table project.scenario add column air_pipe_friction_coefficient real not null default .01
;;
alter table project.scenario add column air_temperature_in_network_celcius real not null default 15
;;
alter table project.scenario add column ambiant_air_temperature_celcius real not null default 15
;;
alter table project.scenario add column leak_pressure real not null default 20000
;;
