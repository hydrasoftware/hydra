# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
from builtins import str
import os
from operator import itemgetter
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QWidget, QGridLayout, QMessageBox
from hydra.gui.widgets.combo_with_values import ComboWithValues
from hydra.gui.widgets.time_input_widget import TimeInputWidget

class ComputSettingsWidget(QWidget):

    def __init__(self, current_project, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "comput_settings.ui"), self)
        tmplayout = QGridLayout()
        tmplayout.addWidget(self,0,0)
        parent.setLayout(tmplayout)

        self.project = current_project
        self.__current_id_scn = None
        self.scenario_rstart = ComboWithValues(self.scenario_rstart_placeholder)
        self.flag_rstart.toggled.connect(self.flag_rstart_changed)
        self.flag_rest.toggled.connect(self.flag_rstart_changed)
        self.scenario_ref = ComboWithValues(self.scenario_ref_placeholder)
        self.scenario_ref_btn.stateChanged.connect(self.scenario_ref_statechanged)

        self.scenario_rstart.currentIndexChanged.connect(self.check_ref_and_restart_config)
        self.scenario_ref.currentIndexChanged.connect(self.check_ref_and_restart_config)

        self.tfin_hr = TimeInputWidget(self.tfin_hr_placeholder)
        self.tfin_hr.set_format(hr=99999, min=59, sec=-1)
        self.dt_hydrol_mn = TimeInputWidget(self.dt_hydrol_mn_placeholder)
        self.dt_hydrol_mn.set_format(hr=99, min=59, sec=59)

        self.dtmin_hydrau_hr = TimeInputWidget(self.dtmin_hydrau_hr_placeholder)
        self.dtmin_hydrau_hr.set_format(hr=99, min=59, sec=59)
        self.dtmin_hydrau_hr.setEnabled(False)
        self.dtmax_hydrau_hr = TimeInputWidget(self.dtmax_hydrau_hr_placeholder)
        self.dtmax_hydrau_hr.set_format(hr=99, min=59, sec=59)

        self.tsave_hr = TimeInputWidget(self.tsave_hr_placeholder)
        self.tsave_hr.set_format(hr=99999, min=59, sec=-1)

        self.tini_hydrau_hr = TimeInputWidget(self.tini_hydrau_hr_placeholder, no_zero=True)
        self.tini_hydrau_hr.set_format(hr=99, min=59, sec=-1)
        self.trstart_hr = TimeInputWidget(self.trstart_hr_placeholder)
        self.trstart_hr.set_format(hr=99999, min=59, sec=-1)

        self.dt_output_hr = TimeInputWidget(self.dt_output_hr_placeholder)
        self.dt_output_hr.set_format(hr=99999, min=59, sec=-1)
        self.tbegin_output_hr = TimeInputWidget(self.tbegin_output_hr_placeholder)
        self.tbegin_output_hr.set_format(hr=99999, min=59, sec=-1)
        self.tend_output_hr = TimeInputWidget(self.tend_output_hr_placeholder)
        self.tend_output_hr.set_format(hr=99999, min=59, sec=-1)

        self.show()

    def set(self, id_scn):
        if id_scn:
            self.scenario_rstart.currentIndexChanged.disconnect(self.check_ref_and_restart_config)
            self.scenario_ref.currentIndexChanged.disconnect(self.check_ref_and_restart_config)

            comput_mode, date0, tfin_hr, dt_hydrol_mn, dtmin_hydrau_hr, dtmax_hydrau_hr, dzmin_hydrau, \
            dzmax_hydrau, flag_save, tsave_hr, tini_hydrau_hr, dt_output_hr, scenario_ref,\
            tbegin_output_hr, tend_output_hr, flag_rstart, scenario_rstart, trstart_hr, flag_gfx_control = self.project.execute("""
            select comput_mode, date0, tfin_hr, dt_hydrol_mn, dtmin_hydrau_hr, dtmax_hydrau_hr, dzmin_hydrau,
                dzmax_hydrau, flag_save, tsave_hr, tini_hydrau_hr, dt_output_hr, scenario_ref,
                tbegin_output_hr, tend_output_hr, flag_rstart, scenario_rstart, trstart_hr, flag_gfx_control
            from project.scenario
            where id={} limit 1""".format(str(id_scn))).fetchone()

            self.flag_rstart.setChecked(flag_rstart)
            self.flag_rest.setChecked(not flag_rstart)
            self.flag_gfx_control.setChecked(flag_gfx_control)

            scn_id_name = self.project.get_scenarios()

            self.scenario_rstart.clear()
            self.scenario_rstart.addItem("None", None)
            for scn in sorted(scn_id_name, key=itemgetter(1)):
                if not float(scn[0])==float(id_scn):
                    self.scenario_rstart.addItem(scn[1], int(scn[0]))
            self.scenario_rstart.set_selected_value(scenario_rstart)

            self.scenario_ref_btn.setChecked(scenario_ref is not None)

            self.scenario_ref.clear()
            self.scenario_ref.addItem("None", None)
            for scn in sorted(scn_id_name, key=itemgetter(1)):
                if not float(scn[0])==float(id_scn):
                    self.scenario_ref.addItem(scn[1], int(scn[0]))
            self.scenario_ref.set_selected_value(scenario_ref)

            self.check_ref_and_restart_config()
            self.scenario_rstart.currentIndexChanged.connect(self.check_ref_and_restart_config)
            self.scenario_ref.currentIndexChanged.connect(self.check_ref_and_restart_config)

            self.date0.setDate(date0)
            self.date0.setTime(date0.time())
            self.tfin_hr.set_time(tfin_hr)
            self.dt_hydrol_mn.set_time(dt_hydrol_mn)
            self.dtmin_hydrau_hr.set_time(dtmin_hydrau_hr)
            self.dtmax_hydrau_hr.set_time(dtmax_hydrau_hr)
            self.dzmin_hydrau.setValue(dzmin_hydrau)
            self.dzmax_hydrau.setValue(dzmax_hydrau)
            self.flag_save.setChecked(flag_save)
            self.tsave_hr.set_time(tsave_hr)
            self.trstart_hr.set_time(trstart_hr)
            self.tini_hydrau_hr.set_time(tini_hydrau_hr)
            self.dt_output_hr.set_time(dt_output_hr)
            self.tbegin_output_hr.set_time(tbegin_output_hr)
            self.tend_output_hr.set_time(tend_output_hr)

            self.set_comput_mode(comput_mode)
            self.flag_rstart_changed()
            self.scenario_ref_statechanged()
            self.__current_id_scn = id_scn

    def save(self):
        if self.__current_id_scn:
            if self.hydrology.isChecked():
                comput_mode = "hydrology"
            elif self.hydraulic.isChecked():
                comput_mode = "hydraulics"
            elif self.hydrologic_and_hydraulic.isChecked():
                comput_mode = "hydrology_and_hydraulics"

            self.project.execute("""
            update project.scenario set
                comput_mode='{}',
                date0='{}', tfin_hr='{}', dt_hydrol_mn='{}', dtmin_hydrau_hr='{}', dtmax_hydrau_hr='{}',
                dzmin_hydrau='{}', dzmax_hydrau='{}', flag_save={}, tsave_hr='{}',
                tini_hydrau_hr='{}', dt_output_hr='{}', tbegin_output_hr='{}', tend_output_hr='{}',
                flag_rstart='{}', scenario_rstart={}, trstart_hr='{}', flag_gfx_control='{}', scenario_ref={}
            where id={}""".format(comput_mode, str(self.date0.dateTime().toString("yyyy-MM-dd hh:mm:ss")), self.tfin_hr.get_time(), \
                self.dt_hydrol_mn.get_time(), self.dtmin_hydrau_hr.get_time(), self.dtmax_hydrau_hr.get_time(), \
                str(self.dzmin_hydrau.value()), str(self.dzmax_hydrau.value()), self.flag_save.isChecked(), self.tsave_hr.get_time(), \
                self.tini_hydrau_hr.get_time(), self.dt_output_hr.get_time(), self.tbegin_output_hr.get_time(), self.tend_output_hr.get_time(),
                self.flag_rstart.isChecked(),
                self.scenario_rstart.get_selected_value() if self.scenario_rstart.get_selected_value() is not None else 'null',
                self.trstart_hr.get_time(),
                self.flag_gfx_control.isChecked(),
                self.scenario_ref.get_selected_value() if (self.scenario_ref_btn.isChecked() and self.scenario_ref.get_selected_value() is not None) else 'null',
                str(self.__current_id_scn)))

    def set_comput_mode(self, comput_mode):
        if comput_mode=="hydrology":
            self.hydrology.setChecked(True)
        elif comput_mode=="hydraulics":
            self.hydraulic.setChecked(True)
        elif comput_mode=="hydrology_and_hydraulics":
            self.hydrologic_and_hydraulic.setChecked(True)
        else:
            raise "Unknown comput mode"

    def flag_rstart_changed(self):
        self.scenario_rstart.setEnabled(self.flag_rstart.isChecked())
        self.trstart_hr.setEnabled(self.flag_rstart.isChecked())
        if not self.flag_rstart.isChecked():
            self.scenario_rstart.set_selected_value(None)

    def scenario_ref_statechanged(self):
        if self.scenario_ref_btn.isChecked():
            self.scenario_ref.setEnabled(True)
        elif not self.scenario_ref_btn.isChecked():
            self.scenario_ref.setEnabled(False)
            self.scenario_ref.set_selected_value(None)

    def check_ref_and_restart_config(self):
        if self.scenario_ref_btn.isChecked() and self.flag_rstart.isChecked():
            configs = self.project.execute("""
                with rkd as (
                    select model, configuration, rank() over (partition by model, configuration order by priority asc) as priority
                    from project.config_scenario cs
					join (values (%s), (%s)) t(id) on t.id=cs.scenario
                )
                select count(1), model, configuration, priority
                from rkd
                group by model, configuration, priority
                """, ( self.scenario_rstart.get_selected_value(), self.scenario_ref.get_selected_value())).fetchall()
            msg = ''
            for ct, model, configuration, priority in configs:
                if ct !=2:
                    config_name, = self.project.execute(f"""
                        select name from {model}.configuration where id=%s
                        """, (configuration,)).fetchone()
                    msg += f"configuration {config_name} differs between reference scenario {self.scenario_ref.currentText()} and hotstart scenario {self.scenario_rstart.currentText()}\n"
            if msg:
                QMessageBox.warning(self, "inconsistent configurations", msg)
                    


