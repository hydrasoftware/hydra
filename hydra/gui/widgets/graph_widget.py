
################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from qgis.PyQt.QtWidgets import QApplication, QWidget, QGridLayout, QMenu
from qgis.PyQt.QtGui import QCursor, QImage
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT, FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from hydra.gui.base_dialog import tr
import numpy
import io
import matplotlib
matplotlib.rc('xtick', labelsize=8)
matplotlib.rc('ytick', labelsize=8)

class GraphWidget(QWidget):

    def __getattr__(self, att):
        if att =='axe':
            return self.__axe
        else:
            raise AttributeError

    def __init__(self, parent=None, axis_equal=False, extend_zone=False, dpi=75):
        QWidget.__init__(self, parent)
        self.__dpi = dpi
        self.fig = Figure(dpi=self.__dpi)
        self.__axe = self.fig.add_subplot(111)
        self.__axe.grid(True)
        self.__axis_equal=axis_equal
        self.__extend_zone=extend_zone
        self.graph_title = self.fig.suptitle("")
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self)
        box = QGridLayout()
        box.addWidget(self.canvas)
        if parent is not None :
            parent.setLayout(box)
        self.__start_x = None
        self.__start_y = None
        self.__start_xlim = None
        self.__start_ylim = None
        self.__xratio = None
        self.fig.canvas.mpl_connect('button_press_event', self.onclick)
        self.canvas.mpl_connect('scroll_event', self.__on_scroll)
        self.canvas.mpl_connect('motion_notify_event', self.__on_move)
        self.canvas.setVisible(False)
        self.__active_zoom = False

    def set_zoom_enabled(self, enabled):
        self.__active_zoom = enabled

    def set_grid(self, enabled) :
        self.__axe.grid(enabled,which='both')

    def set_tick_params(self,axis,which='both',bottom=True,top=False,left=True,right=False,labelbottom=True,labelleft=True):
        self.__axe.tick_params(axis=axis,
                               which=which,
                               bottom=bottom,
                               top=top,
                               left=left,
                               right=right,
                               labelbottom=labelbottom,
                               labelleft=labelleft)

    def __on_scroll(self, event):
        "zoom plot on scroll"
        if not self.__active_zoom:
            return
        if event.xdata is None:
            return
        y_min, y_max = self.__axe.get_ylim()
        new_height = (y_max - y_min)*(1.1 if event.step < 0 else .9)
        self.__axe.set_ylim(
                [(y_max+y_min)*.5 - new_height*.5,
                 (y_max+y_min)*.5 + new_height*.5])

        x_min, x_max = self.__axe.get_xlim()
        new_width = (x_max - x_min)*(1.1 if event.step < 0 else .9)
        relx = (x_max - event.xdata) / (x_max - x_min)
        self.__axe.set_xlim(
                [event.xdata - new_width*(1-relx),
                 event.xdata + new_width*(relx)])

        self.canvas.draw()

    def __on_move(self, event):
        "move plot on drag"
        if event.button == 1 and self.__active_zoom:
            if event.x != None:
                self.__axe.set_xlim(self.__start_xlim + (self.__start_x - event.x)*self.__xratio)
            if event.y != None:
                self.__axe.set_ylim(self.__start_ylim + (self.__start_y - event.y)*self.__yratio)
            self.canvas.draw()

    def clear(self):
        self.__axe.clear()
        self.canvas.draw()

    def get_size(self):
        return self.fig.get_size_inches()*self.__dpi

    def get_zoomed_zone(self):
        y_min, y_max = self.__axe.get_ylim()
        x_min, x_max = self.__axe.get_xlim()
        return x_min, x_max , y_min, y_max

    def set_zoomed_zone(self, x_min, x_max, y_min, y_max):
        self.__axe.set_xlim(x_min, x_max)
        self.__axe.set_ylim(y_min, y_max)
        self.canvas.draw()

    def extend_zone(self):
        y_min, y_max = self.__axe.get_ylim()
        new_height = (y_max - y_min)*1.1
        self.__axe.set_ylim(
                [(y_max+y_min)*.5 - new_height*.5,
                 (y_max+y_min)*.5 + new_height*.5])
        x_min, x_max = self.__axe.get_xlim()
        new_width = (x_max - x_min)*1.1
        self.__axe.set_xlim(
                [(x_max+x_min)*.5 - new_width*.5,
                 (x_max+x_min)*.5 + new_width*.5])

    def render(self):
        self.fig.set_tight_layout({"rect": [0,0,1,0.95]})
        self.__axe.grid(True)
        #self.__axe.get_xaxis().get_major_formatter().set_useOffset(False)
        self.__axe.get_yaxis().get_major_formatter().set_useOffset(False)
        if self.__axis_equal: self.__axe.axis('equal')
        if self.__extend_zone: self.extend_zone()
        self.canvas.draw()
        self.canvas.setVisible(True)

    def add_filled_line(self, x_data, ydata, color, main_title=None, x_title=None, y_title=None):
        if main_title: self.graph_title.set_text(main_title)
        if x_title: self.__axe.set_xlabel(x_title)
        if y_title: self.__axe.set_ylabel(y_title)
        self.__axe.plot(x_data, ydata, linestyle="-", color=color)
        self.__axe.fill_between(x_data, ydata, color=color)
        for tick in self.__axe.get_xaxis().get_ticklabels() :
            tick.set_rotation(65)
        self.render()

    def add_fill_between_lines(self, x_data1, x_data2, ydata1, ydata2, color, main_title=None, x_title=None, y_title=None) :
        if main_title: self.graph_title.set_text(main_title)
        if x_title: self.__axe.set_xlabel(x_title)
        if y_title: self.__axe.set_ylabel(y_title)
        if len(ydata1) == len(ydata2) and len(ydata1) == len(x_data2) :
            self.__axe.fill_between(x_data2, ydata1, ydata2, linestyle="-", color=color)
        else :
            ydata1_interp = numpy.interp(x_data2,x_data1, ydata1)
            self.__axe.fill_between(x_data2, ydata1_interp, ydata2, linestyle="-", color=color)
        self.render()

    def add_filled_symetric_line(self, x_data, ydata, color, main_title=None, x_title=None, y_title=None, bottom=False, top=False):
        if main_title: self.graph_title.set_text(main_title)
        if x_title: self.__axe.set_xlabel(x_title)
        if y_title: self.__axe.set_ylabel(y_title)
        self.__axe.plot(x_data, ydata, linestyle="-", color=color)
        self.__axe.plot([-x for x in x_data], ydata, linestyle="-", color=color)
        self.__axe.fill_between(x_data, self.__axe.get_ylim()[1], ydata, color=color)
        self.__axe.fill_between([-x for x in x_data], self.__axe.get_ylim()[1], ydata, color=color)
        if bottom:
            index_y = ydata.index(min(ydata))
        if top:
            index_y = ydata.index(max(ydata))
        if top or bottom:
            self.__axe.plot([-x_data[index_y], x_data[index_y]], [ydata[index_y], ydata[index_y]], linestyle="-", color=color)
            self.__axe.fill_between([-x_data[index_y], x_data[index_y]], self.__axe.get_ylim()[1], [ydata[index_y], ydata[index_y]], color=color)
        self.render()

    def add_symetric_line(self, x_data, ydata, color, main_title=None, x_title=None, y_title=None, bottom=False, top=False,  marker="", center=0):
        if main_title: self.graph_title.set_text(main_title)
        if x_title: self.__axe.set_xlabel(x_title)
        if y_title: self.__axe.set_ylabel(y_title)
        self.__axe.plot([x + center for x in x_data], ydata, linestyle="-", color=color, marker=marker)
        self.__axe.plot([-x + center for x in x_data], ydata, linestyle="-", color=color, marker=marker)
        if bottom:
            index_y = ydata.index(min(ydata))
        if top:
            index_y = ydata.index(max(ydata))
        if top or bottom:
            self.__axe.plot([-x_data[index_y], x_data[index_y]], [ydata[index_y], ydata[index_y]], linestyle="-", color=color, marker=marker)
        self.render()

    def add_line(self, x_data, ydata, color, main_title=None, x_title=None, y_title=None, marker="", markersize=6, linestyle="-"):
        if x_title: self.__axe.set_xlabel(x_title)
        if y_title: self.__axe.set_ylabel(y_title)
        self.__axe.plot(x_data, ydata, linestyle=linestyle, color=color, marker=marker, markersize=markersize)
        if main_title: self.graph_title.set_text(main_title)
        self.render()

    def add_vline(self, x, color, linestyle="-", linewidth = 2):
        self.__axe.axvline(x, linestyle=linestyle, color=color, linewidth=linewidth)
        self.render()

    def add_fill_between(self,x_data,y_data1,y_data2,where=None,interpolate=False,facecolor=None):
        if len(x_data) == len(y_data1) and len(x_data) == len(y_data2):
            self.__axe.fill_between(x_data, y_data1, y_data2, where, interpolate, facecolor=facecolor)
        self.render()

    def onclick(self, event):
        if event.button == 3: #right-click
            menu = QMenu()
            pos = QCursor.pos()
            menu.move(pos.x(), pos.y())
            menu.addAction(tr("Copy image")).triggered.connect(self.export_image)
            menu.addAction(tr("Copy data")).triggered.connect(self.export_data)
            menu.exec_()

        if event.button == 1 and self.__active_zoom: #start drag for pan
            self.__start_x = event.x
            self.__start_y = event.y
            self.__start_xlim = numpy.array(self.__axe.get_xlim())
            self.__start_ylim = numpy.array(self.__axe.get_ylim())
            self.__xratio = (self.__start_xlim[1] -  self.__start_xlim[0])\
                         / self.canvas.width()
            self.__yratio = (self.__start_ylim[1] -  self.__start_ylim[0])\
                         / self.canvas.height()

    def export_image(self):
        buf = io.BytesIO()
        self.fig.savefig(buf)
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard )
        cb.setImage(QImage.fromData(buf.getvalue()), mode=cb.Clipboard)
        buf.close()

    def export_data(self):
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard )
        data_to_export = ""

        for line in self.fig.gca().lines:
            line_index = self.fig.gca().lines.index(line)
            data_to_export += f"X{line_index}\tY{line_index}\r\n"
            for i in range(len(line.get_xdata())):
                data_to_export += f"{line.get_xdata()[i]}\t{line.get_ydata()[i]}\r\n"
        cb.setText(data_to_export, mode=cb.Clipboard)
