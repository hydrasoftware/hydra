# -*- coding: utf-8 -*-

"""
***************************************************************************
    taudemAlgorithm.py
    ---------------------
    Date                 : May 2012
    Copyright            : (C) 2012-2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'May 2012'
__copyright__ = '(C) 2012-2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

import os

from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtGui import QIcon

from qgis.core import QgsProcessingAlgorithm

from . import taudemUtils

pluginPath = os.path.dirname(__file__)


class TauDemAlgorithm(QgsProcessingAlgorithm):

    def __init__(self):
        super().__init__()

    def createInstance(self):
        return type(self)()

    def icon(self):
        return QIcon(os.path.join(pluginPath, 'icons', 'taudem.svg'))

    def command(self):
        return os.path.join(taudemUtils.taudemDirectory(), self.name())

    def algorithmResults(self, parameters):
        results = {}
        for output in self.outputDefinitions():
            outputName = output.name()
            if outputName in parameters:
                results[outputName] = parameters[outputName]

        return results

    def tr(self, text):
        return QCoreApplication.translate(self.__class__.__name__, text)
