/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

update hydra.metadata set version = '3.8';
;;

--isolated--
alter type hydra_netflow_type add value if not exists 'hydra_permeable'
;;

do
$$$$
declare
    model text;
begin

    for model in select table_schema from information_schema.tables where table_name='_singularity' loop
        raise notice '%', format('alter table %s._catchment_node drop column hydra_aquifer_infiltration_type;', model);
        execute format('alter table %s._catchment_node drop column hydra_aquifer_infiltration_type;', model);
    end loop;
end;
$$$$
;;

update hydra.sor1_mode set description='Mode 1: Extended Hydra format' where name='two_files_extended'
;;
update hydra.sor1_mode set description='Mode 2: Excell format' where name='two_files_averaged'
;;
update hydra.sor1_mode set description='Mode 3: One file per object' where name='one_file'
;;

