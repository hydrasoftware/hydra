################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QWidget
import os
from .notes_expanded_dialog import NotesExpandedDialog

class NotesWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "notes_widget.ui"), self)
        self.btn.clicked.connect(self.__expand)

    def setText(self, txt):
        txt = txt or ''
        self.notes.setText(txt)
        self.notes.setEnabled(txt.find('\n') == -1)
        self.notes.setToolTip(txt)

    def __expand(self):
        d = NotesExpandedDialog(self.notes.text())
        if d.exec_():
            self.setText(d.notes.toPlainText())

    def text(self):
        return self.notes.text() if self.notes.text() != '' else None

    def domXml(self):
        return """
        <property name="geometry">
          <rect>
            <x>0</x>
            <y>0</y>
            <width>100</width>
            <height>100</height>
          </rect>
        </property>
        """

if __name__=='__main__':

    from qgis.PyQt.QtWidgets import QApplication
    import sys
    a = QApplication(sys.argv)
    w = NotesWidget('hello\nseconde ligne\nun texte vraiment long pour voir comment ça se comporte dans le champ commentaire')
    w.show()
    a.exec_()
    print(w.text())