/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  Update version                                */
/* ********************************************** */

update ${model}.metadata set version = '1.2.0';
;;

/* ********************************************** */
/*  Catchment nodes structural changes            */
/* ********************************************** */

alter table ${model}._catchment_node add column if not exists constant_runoff real;
alter table ${model}._catchment_node add column if not exists horner_ini_loss_coef real default 0.6;
alter table ${model}._catchment_node add column if not exists horner_recharge_coef real default 0.12;
alter table ${model}._catchment_node add column if not exists holtan_sat_inf_rate_mmh real default 2;
alter table ${model}._catchment_node add column if not exists holtan_dry_inf_rate_mmh real default 10;
alter table ${model}._catchment_node add column if not exists holtan_soil_storage_cap_mm real default 50;
alter table ${model}._catchment_node add column if not exists scs_j_mm real default 150;
alter table ${model}._catchment_node add column if not exists scs_soil_drainage_time_day real default 10;
alter table ${model}._catchment_node add column if not exists scs_rfu_mm real default 50;
alter table ${model}._catchment_node add column if not exists permeable_soil_j_mm real default 150;
alter table ${model}._catchment_node add column if not exists permeable_soil_rfu_mm real default 50;
alter table ${model}._catchment_node add column if not exists permeable_soil_ground_max_inf_rate real default 1;
alter table ${model}._catchment_node add column if not exists permeable_soil_ground_lag_time_day real default 1;
alter table ${model}._catchment_node add column if not exists permeable_soil_coef_river_exchange real default 1;
alter table ${model}._catchment_node add column if not exists socose_tc_mn real;
alter table ${model}._catchment_node add column if not exists socose_shape_param_beta real default 4;
alter table ${model}._catchment_node add column if not exists define_k_mn real;
;;

/* Set config to default */
update ${model}.metadata set configuration=1;
;;

/* Data transfer */
update ${model}._catchment_node set
    constant_runoff = (netflow_param->'constant_runoff'->>'cr')::real,
    horner_ini_loss_coef = (netflow_param->'horner'->>'ini_loss_coef')::real,
    horner_recharge_coef = (netflow_param->'horner'->>'recharge_coef')::real,
    holtan_sat_inf_rate_mmh = (netflow_param->'holtan'->>'sat_inf_rate_mmh')::real,
    holtan_dry_inf_rate_mmh = (netflow_param->'holtan'->>'dry_inf_rate_mmh')::real,
    holtan_soil_storage_cap_mm = (netflow_param->'holtan'->>'soil_storage_cap_mm')::real,
    scs_j_mm = (netflow_param->'scs'->>'j_mm_scs')::real,
    scs_soil_drainage_time_day = (netflow_param->'scs'->>'soil_drainage_time_day')::real,
    scs_rfu_mm = (netflow_param->'scs'->>'rfu_mm_scs')::real,
    permeable_soil_j_mm = (netflow_param->'permeable_soil'->>'j_mm_ps')::real,
    permeable_soil_rfu_mm = (netflow_param->'permeable_soil'->>'rfu_mm_ps')::real,
    permeable_soil_ground_max_inf_rate = (netflow_param->'permeable_soil'->>'ground_max_inf_rate')::real,
    permeable_soil_ground_lag_time_day = (netflow_param->'permeable_soil'->>'ground_lag_time_day')::real,
    permeable_soil_coef_river_exchange = (netflow_param->'permeable_soil'->>'coef_river_exchange')::real,
    socose_tc_mn = (runoff_param->'socose'->>'tc_mn')::real,
    socose_shape_param_beta = (runoff_param->'socose'->>'shape_param_beta')::real;
;;

/* configuration on catchments drop */
update ${model}._node set configuration=null where node_type='catchment';
;;

/* View drop and recreation */
drop view ${model}.catchment_node cascade;
;;

alter table ${model}._catchment_node drop column if exists netflow_param;
alter table ${model}._catchment_node drop column if exists runoff_param;
;;

create or replace view ${model}.catchment_node as
select p.id,
    p.name,
    c.area_ha,
    c.rl,
    c.slope,
    c.c_imp,
    c.netflow_type,
    c.constant_runoff,
    c.horner_ini_loss_coef,
    c.horner_recharge_coef,
    c.holtan_sat_inf_rate_mmh,
    c.holtan_dry_inf_rate_mmh,
    c.holtan_soil_storage_cap_mm,
    c.scs_j_mm,
    c.scs_soil_drainage_time_day,
    c.scs_rfu_mm,
    c.permeable_soil_j_mm,
    c.permeable_soil_rfu_mm,
    c.permeable_soil_ground_max_inf_rate,
    c.permeable_soil_ground_lag_time_day,
    c.permeable_soil_coef_river_exchange,
    c.runoff_type,
    c.socose_tc_mn,
    c.socose_shape_param_beta,
    c.define_k_mn,
    c.q_limit,
    c.q0,
    c.contour,
    c.catchment_pollution_mode,
    c.catchment_pollution_param::character varying as catchment_pollution_param,
    c.catchment_pollution_param as catchment_pollution_param_json,
    p.geom,
    p.configuration::character varying as configuration,
    p.generated,
    p.validity,
    p.configuration as configuration_json
   from $model._catchment_node c,
    $model._node p
  where p.id = c.id;
  ;;

create or replace function ${model}.catchment_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('catchment', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'catchment') where name = 'define_later' and id = id_;
            insert into $model._catchment_node
                values (id_, 'catchment', new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, coalesce(new.q_limit, 9999), coalesce(new.q0, 0), coalesce(new.contour, (select id from $model.catchment where st_intersects(geom, new.geom))), new.catchment_pollution_mode, new.catchment_pollution_param::json);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'catchment_node');

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'catchment' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_type!='constant_runoff' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)) and (netflow_type!='horner' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)) and (netflow_type!='horner' or (horner_recharge_coef is not null and horner_recharge_coef>=0)) and (netflow_type!='holtan' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)) and (netflow_type!='scs' or (scs_j_mm is not null and scs_j_mm>=0)) and (netflow_type!='scs' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)) and (netflow_type!='scs' or (scs_rfu_mm is not null and scs_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_j_mm is not null and permeable_soil_j_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_rfu_mm is not null and permeable_soil_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_max_inf_rate is not null and permeable_soil_ground_max_inf_rate>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_lag_time_day is not null and permeable_soil_ground_lag_time_day>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_coef_river_exchange is not null and permeable_soil_coef_river_exchange>=0 and permeable_soil_coef_river_exchange<=1)) and (runoff_type is not null) and (runoff_type!='socose' or (socose_tc_mn is not null and socose_tc_mn>0)) and (runoff_type!='socose' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6)) and (runoff_type!='define_k' or (define_k_mn is not null and define_k_mn>0)) and (q_limit is not null) and (q0 is not null) from  $model._catchment_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.catchment_pollution_mode, new.catchment_pollution_param) is distinct from (old.area_ha, old.rl, old.slope, old.c_imp, old.netflow_type, old.constant_runoff, old.horner_ini_loss_coef, old.horner_recharge_coef, old.holtan_sat_inf_rate_mmh, old.holtan_dry_inf_rate_mmh, old.holtan_soil_storage_cap_mm, old.scs_j_mm, old.scs_soil_drainage_time_day, old.scs_rfu_mm, old.permeable_soil_j_mm, old.permeable_soil_rfu_mm, old.permeable_soil_ground_max_inf_rate, old.permeable_soil_ground_lag_time_day, old.permeable_soil_coef_river_exchange, old.runoff_type, old.socose_tc_mn, old.socose_shape_param_beta, old.define_k_mn, old.q_limit, old.q0, old.contour, old.catchment_pollution_mode, old.catchment_pollution_param)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area_ha, old.rl, old.slope, old.c_imp, old.netflow_type, old.constant_runoff, old.horner_ini_loss_coef, old.horner_recharge_coef, old.holtan_sat_inf_rate_mmh, old.holtan_dry_inf_rate_mmh, old.holtan_soil_storage_cap_mm, old.scs_j_mm, old.scs_soil_drainage_time_day, old.scs_rfu_mm, old.permeable_soil_j_mm, old.permeable_soil_rfu_mm, old.permeable_soil_ground_max_inf_rate, old.permeable_soil_ground_lag_time_day, old.permeable_soil_coef_river_exchange, old.runoff_type, old.socose_tc_mn, old.socose_shape_param_beta, old.define_k_mn, old.q_limit, old.q0, old.contour, old.catchment_pollution_mode, old.catchment_pollution_param) as o, (select new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.catchment_pollution_mode, new.catchment_pollution_param) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.catchment_pollution_mode, new.catchment_pollution_param) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._catchment_node set area_ha=new.area_ha, rl=new.rl, slope=new.slope, c_imp=new.c_imp, netflow_type=new.netflow_type, constant_runoff=new.constant_runoff, horner_ini_loss_coef=new.horner_ini_loss_coef, horner_recharge_coef=new.horner_recharge_coef, holtan_sat_inf_rate_mmh=new.holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh=new.holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm=new.holtan_soil_storage_cap_mm, scs_j_mm=new.scs_j_mm, scs_soil_drainage_time_day=new.scs_soil_drainage_time_day, scs_rfu_mm=new.scs_rfu_mm, permeable_soil_j_mm=new.permeable_soil_j_mm, permeable_soil_rfu_mm=new.permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate=new.permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day=new.permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange=new.permeable_soil_coef_river_exchange, runoff_type=new.runoff_type, socose_tc_mn=new.socose_tc_mn, socose_shape_param_beta=new.socose_shape_param_beta, define_k_mn=new.define_k_mn, q_limit=new.q_limit, q0=new.q0, contour=new.contour, catchment_pollution_mode=new.catchment_pollution_mode, catchment_pollution_param=new.catchment_pollution_param::json where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'catchment_node');

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'catchment' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'catchment' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_type!='constant_runoff' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)) and (netflow_type!='horner' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)) and (netflow_type!='horner' or (horner_recharge_coef is not null and horner_recharge_coef>=0)) and (netflow_type!='holtan' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)) and (netflow_type!='scs' or (scs_j_mm is not null and scs_j_mm>=0)) and (netflow_type!='scs' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)) and (netflow_type!='scs' or (scs_rfu_mm is not null and scs_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_j_mm is not null and permeable_soil_j_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_rfu_mm is not null and permeable_soil_rfu_mm>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_max_inf_rate is not null and permeable_soil_ground_max_inf_rate>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_ground_lag_time_day is not null and permeable_soil_ground_lag_time_day>=0)) and (netflow_type!='permeable_soil' or (permeable_soil_coef_river_exchange is not null and permeable_soil_coef_river_exchange>=0 and permeable_soil_coef_river_exchange<=1)) and (runoff_type is not null) and (runoff_type!='socose' or (socose_tc_mn is not null and socose_tc_mn>0)) and (runoff_type!='socose' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6)) and (runoff_type!='define_k' or (define_k_mn is not null and define_k_mn>0)) and (q_limit is not null) and (q0 is not null) from  $model._catchment_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'storage' and (select trigger_coverage from $model.metadata) then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'catchment' = 'crossroad' and (select trigger_street_link from $model.metadata) then
                perform $model.update_street_links();
            end if;

            delete from $model._catchment_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

create trigger ${model}_catchment_node_trig
    instead of insert or update or delete on $model.catchment_node
       for each row execute procedure ${model}.catchment_node_fct()
;;

/* Recreate dropped invalid view (cascade on catchment_node) */
create or replace view ${model}.invalid as
    WITH node_invalidity_reason AS (
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.zb IS NOT NULL THEN ''::text
                    ELSE '   zb is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk > 0::double precision THEN ''::text
                    ELSE '   rk>0   '::text
                END AS reason
           FROM $model.elem_2d_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   (area > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h IS NOT NULL THEN ''::text
                    ELSE '   h is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h >= 0::double precision THEN ''::text
                    ELSE '   h>=0   '::text
                END AS reason
           FROM $model.crossroad_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END ||
                CASE
                    WHEN new_1.zini IS NOT NULL THEN ''::text
                    ELSE '   zini is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10    '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1) >=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END AS reason
           FROM $model.storage_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((
                CASE
                    WHEN new_1.area_ha IS NOT NULL THEN ''::text
                    ELSE '   area_ha is not null   '::text
                END ||
                CASE
                    WHEN new_1.area_ha > 0::double precision THEN ''::text
                    ELSE '   area_ha>0   '::text
                END) ||
                CASE
                    WHEN new_1.rl IS NOT NULL THEN ''::text
                    ELSE '   rl is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rl > 0::double precision THEN ''::text
                    ELSE '   rl>0   '::text
                END) ||
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp IS NOT NULL THEN ''::text
                    ELSE '   c_imp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp >= 0::double precision THEN ''::text
                    ELSE '   c_imp>=0   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp <= 1::double precision THEN ''::text
                    ELSE '   c_imp<=1   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type IS NOT NULL THEN ''::text
                    ELSE '   netflow_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type IS NOT NULL THEN ''::text
                    ELSE '   runoff_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q_limit IS NOT NULL THEN ''::text
                    ELSE '   q_limit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q0 IS NOT NULL THEN ''::text
                    ELSE '   q0 is not null   '::text
                END AS reason
           FROM $model.catchment_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END) ||
                CASE
                    WHEN new_1.station IS NOT NULL THEN ''::text
                    ELSE '   station is not null    '::text
                END) ||
                CASE
                    WHEN ( SELECT st_intersects(new_1.geom, station.geom) AS st_intersects
                       FROM $model.station
                      WHERE station.id = new_1.station) THEN ''::text
                    ELSE '   (select st_intersects(new.geom, geom) from $model.station where id=new.station)   '::text
                END AS reason
           FROM $model.station_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END) ||
                CASE
                    WHEN ( SELECT NOT (EXISTS ( SELECT 1
                               FROM $model.station
                              WHERE st_intersects(station.geom, new_1.geom)))) THEN ''::text
                    ELSE '   (select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))   '::text
                END) ||
                CASE
                    WHEN $model.check_connected_pipes(new_1.id) THEN ''::text
                    ELSE '   $model.check_connected_pipes(new.id)   '::text
                END AS reason
           FROM $model.manhole_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END ||
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END) ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.reach IS NOT NULL THEN ''::text
                    ELSE '   reach is not null   '::text
                END) ||
                CASE
                    WHEN ( SELECT NOT (EXISTS ( SELECT 1
                               FROM $model.station
                              WHERE st_intersects(station.geom, new_1.geom)))) THEN ''::text
                    ELSE '   (select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))   '::text
                END AS reason
           FROM $model.river_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   (area > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END AS reason
           FROM $model.manhole_hydrology_node new_1
          WHERE NOT new_1.validity
        ), link_invalidity_reason AS (
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.q_pump IS NOT NULL THEN ''::text
                    ELSE '   q_pump is not null   '::text
                END ||
                CASE
                    WHEN new_1.q_pump >= 0::double precision THEN ''::text
                    ELSE '   q_pump>= 0   '::text
                END) ||
                CASE
                    WHEN new_1.qz_array IS NOT NULL THEN ''::text
                    ELSE '   qz_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(qz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.deriv_pump_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.down_type = 'manhole_hydrology'::hydra_node_type OR (new_1.down_type = ANY (ARRAY['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type])) AND new_1.hydrograph IS NOT NULL THEN ''::text
                    ELSE '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'') and (hydrograph is not null))   '::text
                END ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.connector_hydrology_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null    '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.weir_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.transmitivity IS NOT NULL THEN ''::text
                    ELSE '   transmitivity is not null   '::text
                END) ||
                CASE
                    WHEN new_1.transmitivity >= 0::double precision THEN ''::text
                    ELSE '   transmitivity>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.porous_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.law_type IS NOT NULL THEN ''::text
                    ELSE '   law_type is not null   '::text
                END ||
                CASE
                    WHEN new_1.param IS NOT NULL THEN ''::text
                    ELSE '   param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.borda_headloss_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN new_1.z_crest1 IS NOT NULL THEN ''::text
                    ELSE '   z_crest1 is not null   '::text
                END ||
                CASE
                    WHEN new_1.width1 IS NOT NULL THEN ''::text
                    ELSE '   width1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width1 > 0::double precision THEN ''::text
                    ELSE '   width1>0   '::text
                END) ||
                CASE
                    WHEN new_1.length IS NOT NULL THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN new_1.length > 0::double precision THEN ''::text
                    ELSE '   length>0   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk >= 0::double precision THEN ''::text
                    ELSE '   rk>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 IS NOT NULL THEN ''::text
                    ELSE '   z_crest2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 > new_1.z_crest1 THEN ''::text
                    ELSE '   z_crest2>z_crest1   '::text
                END) ||
                CASE
                    WHEN new_1.width2 IS NOT NULL THEN ''::text
                    ELSE '   width2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width2 > new_1.width1 THEN ''::text
                    ELSE '   width2>width1   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.strickler_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= '0'::numeric::double precision THEN ''::text
                    ELSE '   width>=0.   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= '0'::numeric::double precision THEN ''::text
                    ELSE '   cc>=0.   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode IS NOT NULL THEN ''::text
                    ELSE '   break_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode OR new_1.z_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''zw_critical'' or z_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode OR new_1.t_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''time_critical'' or t_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or grp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp > 0 THEN ''::text
                    ELSE '   break_mode=''none'' or grp>0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp < 100 THEN ''::text
                    ELSE '   break_mode=''none'' or grp<100   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.dt_fracw_array IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or dt_fracw_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) <= 10 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) >= 1 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 2) = 2 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.fuse_spillway_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.cross_section IS NOT NULL THEN ''::text
                    ELSE '   cross_section is not null   '::text
                END ||
                CASE
                    WHEN new_1.cross_section >= 0::double precision THEN ''::text
                    ELSE '   cross_section>=0   '::text
                END) ||
                CASE
                    WHEN new_1.length IS NOT NULL THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN new_1.length >= 0::double precision THEN ''::text
                    ELSE '   length>=0   '::text
                END) ||
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type = 'manhole_hydrology'::hydra_node_type OR (new_1.down_type = ANY (ARRAY['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type])) AND new_1.hydrograph IS NOT NULL THEN ''::text
                    ELSE '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'') and (hydrograph is not null))   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.routing_hydrology_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling > new_1.z_invert THEN ''::text
                    ELSE '   z_ceiling>z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc <=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc >=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_valve IS NOT NULL THEN ''::text
                    ELSE '   mode_valve is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate IS NOT NULL THEN ''::text
                    ELSE '   z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate >= new_1.z_invert THEN ''::text
                    ELSE '   z_gate>=z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate <= new_1.z_ceiling THEN ''::text
                    ELSE '   z_gate<=z_ceiling   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.gate_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert_stop IS NOT NULL THEN ''::text
                    ELSE '   z_invert_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling_stop IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.dt_regul_hr IS NOT NULL THEN ''::text
                    ELSE '   dt_regul_hr is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_control_node IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_pid_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_tz_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR new_1.q_z_crit IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul OR new_1.nr_z_gate IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.regul_gate_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((((((((((((((((
                CASE
                    WHEN new_1.z_crest1 IS NOT NULL THEN ''::text
                    ELSE '   z_crest1 is not null   '::text
                END ||
                CASE
                    WHEN new_1.width1 IS NOT NULL THEN ''::text
                    ELSE '   width1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width1 >= 0::double precision THEN ''::text
                    ELSE '   width1>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 IS NOT NULL THEN ''::text
                    ELSE '   z_crest2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 >= new_1.z_crest1 THEN ''::text
                    ELSE '   z_crest2>=z_crest1   '::text
                END) ||
                CASE
                    WHEN new_1.width2 IS NOT NULL THEN ''::text
                    ELSE '   width2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width2 >= 0::double precision THEN ''::text
                    ELSE '   width2>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef IS NOT NULL THEN ''::text
                    ELSE '   lateral_contraction_coef is not null   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef <= 1::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef<=1   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef >= 0::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef>=0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode IS NOT NULL THEN ''::text
                    ELSE '   break_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode OR new_1.z_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''zw_critical'' or z_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode OR new_1.t_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''time_critical'' or t_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.width_breach IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or width_breach is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or z_invert is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or grp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp > 0 THEN ''::text
                    ELSE '   break_mode=''none'' or grp>0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp < 100 THEN ''::text
                    ELSE '   break_mode=''none'' or grp<100   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.dt_fracw_array IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or dt_fracw_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) <= 10 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) >= 1 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 2) = 2 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.overflow_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((
                CASE
                    WHEN new_1.z_invert_up IS NOT NULL THEN ''::text
                    ELSE '   z_invert_up is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_invert_down IS NOT NULL THEN ''::text
                    ELSE '   z_invert_down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h_sable IS NOT NULL THEN ''::text
                    ELSE '   h_sable is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'valley'::hydra_cross_section_type THEN ''::text
                    ELSE '   cross_section_type not in (''valley'')   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk > 0::double precision THEN ''::text
                    ELSE '   rk >0   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'circular'::hydra_cross_section_type OR new_1.circular_diameter IS NOT NULL AND new_1.circular_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''circular'' or (circular_diameter is not null and circular_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_top_diameter IS NOT NULL AND new_1.ovoid_top_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_invert_diameter IS NOT NULL AND new_1.ovoid_invert_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_height IS NOT NULL AND new_1.ovoid_height > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_height is not null and ovoid_height >0 )   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_height > ((new_1.ovoid_top_diameter + new_1.ovoid_invert_diameter) / 2::double precision) THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'pipe'::hydra_cross_section_type OR new_1.cp_geom IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type!=''pipe'' or cp_geom is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'channel'::hydra_cross_section_type OR new_1.op_geom IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type!=''channel'' or op_geom is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.pipe_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.length IS NOT NULL THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN new_1.length >= 0::double precision THEN ''::text
                    ELSE '   length>=0   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk > 0::double precision THEN ''::text
                    ELSE '   rk>0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.street_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN new_1.npump IS NOT NULL THEN ''::text
                    ELSE '   npump is not null   '::text
                END ||
                CASE
                    WHEN new_1.npump <= 10 THEN ''::text
                    ELSE '   npump<=10   '::text
                END) ||
                CASE
                    WHEN new_1.npump >= 1 THEN ''::text
                    ELSE '   npump>=1   '::text
                END) ||
                CASE
                    WHEN new_1.zregul_array IS NOT NULL THEN ''::text
                    ELSE '   zregul_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.hq_array IS NOT NULL THEN ''::text
                    ELSE '   hq_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zregul_array, 1) = new_1.npump THEN ''::text
                    ELSE '   array_length(zregul_array, 1)=npump   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zregul_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zregul_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 1) = new_1.npump THEN ''::text
                    ELSE '   array_length(hq_array, 1)=npump   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 2) <= 10 THEN ''::text
                    ELSE '   array_length(hq_array, 2)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 2) >= 1 THEN ''::text
                    ELSE '   array_length(hq_array, 2)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 3) = 2 THEN ''::text
                    ELSE '   array_length(hq_array, 3)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.pump_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((
                CASE
                    WHEN new_1.z_overflow IS NOT NULL THEN ''::text
                    ELSE '   z_overflow is not null   '::text
                END ||
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END) ||
                CASE
                    WHEN new_1.area >= 0::double precision THEN ''::text
                    ELSE '   area>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.network_overflow_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.lateral_contraction_coef IS NOT NULL THEN ''::text
                    ELSE '   lateral_contraction_coef is not null   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef <= 1::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef<=1   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef >= 0::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef>=0   '::text
                END) ||
                CASE
                    WHEN st_npoints(new_1.border) = 2 THEN ''::text
                    ELSE '   ST_NPoints(border)=2   '::text
                END) ||
                CASE
                    WHEN st_isvalid(new_1.border) THEN ''::text
                    ELSE '   ST_IsValid(border)   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.mesh_2d_link new_1
          WHERE NOT new_1.validity
        ), singularity_invalidity_reason AS (
         SELECT new_1.id,
            (((((
                CASE
                    WHEN new_1.downstream IS NOT NULL THEN ''::text
                    ELSE '   downstream is not null   '::text
                END ||
                CASE
                    WHEN new_1.split1 IS NOT NULL THEN ''::text
                    ELSE '   split1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.downstream_param IS NOT NULL THEN ''::text
                    ELSE '   downstream_param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1_law IS NOT NULL THEN ''::text
                    ELSE '   split1_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1_param IS NOT NULL THEN ''::text
                    ELSE '   split1_param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NULL OR new_1.split2_law IS NOT NULL THEN ''::text
                    ELSE '   split2 is null or split2_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NULL OR new_1.split2_param IS NOT NULL THEN ''::text
                    ELSE '   split2 is null or split2_param is not null   '::text
                END AS reason
           FROM $model.zq_split_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.law_type IS NOT NULL THEN ''::text
                    ELSE '   law_type is not null   '::text
                END ||
                CASE
                    WHEN new_1.param IS NOT NULL THEN ''::text
                    ELSE '   param is not null   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.borda_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((
                CASE
                    WHEN new_1.storage_area IS NOT NULL THEN ''::text
                    ELSE '   storage_area is not null   '::text
                END ||
                CASE
                    WHEN new_1.storage_area >= 0::double precision THEN ''::text
                    ELSE '   storage_area>=0   '::text
                END) ||
                CASE
                    WHEN new_1.constant_dry_flow IS NOT NULL THEN ''::text
                    ELSE '   constant_dry_flow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.constant_dry_flow >= 0::double precision THEN ''::text
                    ELSE '   constant_dry_flow>=0   '::text
                END) ||
                CASE
                    WHEN new_1.distrib_coef IS NULL OR new_1.distrib_coef >= 0::double precision AND new_1.distrib_coef <= 1::double precision THEN ''::text
                    ELSE '   distrib_coef is null or (distrib_coef>=0 and distrib_coef<=1)   '::text
                END) ||
                CASE
                    WHEN new_1.lag_time_hr IS NULL OR new_1.lag_time_hr >= 0::double precision THEN ''::text
                    ELSE '   lag_time_hr is null or (lag_time_hr>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.sector IS NULL OR new_1.distrib_coef IS NOT NULL AND new_1.lag_time_hr IS NOT NULL THEN ''::text
                    ELSE '   sector is null or (distrib_coef is not null and lag_time_hr is not null)   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR new_1.tq_array IS NOT NULL THEN ''::text
                    ELSE '   external_file_data or tq_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 1) <= 10 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 1) >= 1 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 2) = 2 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 2)=2   '::text
                END AS reason
           FROM $model.hydrograph_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling > new_1.z_invert THEN ''::text
                    ELSE '   z_ceiling>z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc <=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc >=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_valve IS NOT NULL THEN ''::text
                    ELSE '   mode_valve is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate IS NOT NULL THEN ''::text
                    ELSE '   z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate >= new_1.z_invert THEN ''::text
                    ELSE '   z_gate>=z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate <= new_1.z_ceiling THEN ''::text
                    ELSE '   z_gate<=z_ceiling   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.gate_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.cascade_mode IS NOT NULL THEN ''::text
                    ELSE '   cascade_mode is not null   '::text
                END ||
                CASE
                    WHEN new_1.cascade_mode = 'hydrograph'::hydra_model_connect_mode OR new_1.zq_array IS NOT NULL THEN ''::text
                    ELSE '   cascade_mode=''hydrograph'' or zq_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) <= 100 THEN ''::text
                    ELSE '   array_length(zq_array, 1)<=100   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zq_array, 2)=2   '::text
                END AS reason
           FROM $model.model_connect_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.d_abutment_l IS NOT NULL THEN ''::text
                    ELSE '   d_abutment_l is not null   '::text
                END ||
                CASE
                    WHEN new_1.d_abutment_r IS NOT NULL THEN ''::text
                    ELSE '   d_abutment_r is not null   '::text
                END) ||
                CASE
                    WHEN new_1.abutment_type IS NOT NULL THEN ''::text
                    ELSE '   abutment_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zw_array IS NOT NULL THEN ''::text
                    ELSE '   zw_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.bradley_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.q_dz_array IS NOT NULL THEN ''::text
                    ELSE '   q_dz_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(q_dz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(q_dz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(q_dz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.param_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null    '::text
                END ||
                CASE
                    WHEN new_1.z_regul IS NOT NULL THEN ''::text
                    ELSE '   z_regul is not null    '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul IS NOT NULL THEN ''::text
                    ELSE '   mode_regul is not null   '::text
                END) ||
                CASE
                    WHEN new_1.reoxy_law IS NOT NULL THEN ''::text
                    ELSE '   reoxy_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.reoxy_param IS NOT NULL THEN ''::text
                    ELSE '   reoxy_param is not null   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.zregul_weir_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((
                CASE
                    WHEN new_1.l_road IS NOT NULL THEN ''::text
                    ELSE '   l_road is not null   '::text
                END ||
                CASE
                    WHEN new_1.l_road >= 0::double precision THEN ''::text
                    ELSE '   l_road>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_road IS NOT NULL THEN ''::text
                    ELSE '   z_road is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zw_array IS NOT NULL THEN ''::text
                    ELSE '   zw_array is not null    '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.bridge_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.qq_array IS NOT NULL THEN ''::text
                    ELSE '   qq_array is not null   '::text
                END ||
                CASE
                    WHEN new_1.downstream IS NOT NULL THEN ''::text
                    ELSE '   downstream is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1 IS NOT NULL THEN ''::text
                    ELSE '   split1 is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qq_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NOT NULL AND array_length(new_1.qq_array, 2) = 3 OR new_1.split2 IS NULL AND array_length(new_1.qq_array, 2) = 2 THEN ''::text
                    ELSE '   (split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2)   '::text
                END AS reason
           FROM $model.qq_split_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((
                CASE
                    WHEN new_1.external_file_data OR new_1.tz_array IS NOT NULL THEN ''::text
                    ELSE '   external_file_data or tz_array is not null   '::text
                END ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 1) <= 10 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 1) >= 1 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 2) = 2 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 2)=2   '::text
                END AS reason
           FROM $model.tz_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((
                CASE
                    WHEN new_1.zq_array IS NOT NULL THEN ''::text
                    ELSE '   zq_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zq_array, 2)=2   '::text
                END AS reason
           FROM $model.zq_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.drainage IS NOT NULL THEN ''::text
                    ELSE '   drainage is not null   '::text
                END ||
                CASE
                    WHEN new_1.overflow IS NOT NULL THEN ''::text
                    ELSE '   overflow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ini IS NOT NULL THEN ''::text
                    ELSE '   z_ini is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zr_sr_qf_qs_array IS NOT NULL THEN ''::text
                    ELSE '   zr_sr_qf_qs_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.treatment_mode IS NOT NULL THEN ''::text
                    ELSE '   treatment_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.treatment_param IS NOT NULL THEN ''::text
                    ELSE '   treatment_param is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 2) = 4 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 2)=4   '::text
                END AS reason
           FROM $model.reservoir_rsp_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.pk0_km IS NOT NULL THEN ''::text
                    ELSE '   pk0_km is not null   '::text
                END ||
                CASE
                    WHEN new_1.dx IS NOT NULL THEN ''::text
                    ELSE '   dx is not null   '::text
                END) ||
                CASE
                    WHEN new_1.dx >= 0.1::double precision THEN ''::text
                    ELSE '   dx>=0.1   '::text
                END AS reason
           FROM $model.pipe_branch_marker_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
                CASE
                    WHEN new_1.q0 IS NOT NULL THEN ''::text
                    ELSE '   q0 is not null   '::text
                END AS reason
           FROM $model.constant_inflow_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END ||
                CASE
                    WHEN new_1.zini IS NOT NULL THEN ''::text
                    ELSE '   zini is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END AS reason
           FROM $model.tank_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.qz_array IS NOT NULL THEN ''::text
                    ELSE '   qz_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(qz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.hydraulic_cut_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.drainage IS NOT NULL THEN ''::text
                    ELSE '   drainage is not null   '::text
                END ||
                CASE
                    WHEN new_1.overflow IS NOT NULL THEN ''::text
                    ELSE '   overflow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q_drainage IS NOT NULL THEN ''::text
                    ELSE '   q_drainage is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q_drainage >= 0::double precision THEN ''::text
                    ELSE '   q_drainage>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_ini IS NOT NULL THEN ''::text
                    ELSE '   z_ini is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.treatment_mode IS NOT NULL THEN ''::text
                    ELSE '   treatment_mode is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END AS reason
           FROM $model.reservoir_rs_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END ||
                CASE
                    WHEN new_1.k IS NOT NULL THEN ''::text
                    ELSE '   k is not null   '::text
                END) ||
                CASE
                    WHEN new_1.k > 0::double precision THEN ''::text
                    ELSE '   k>0   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END AS reason
           FROM $model.strickler_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.z_weir IS NOT NULL THEN ''::text
                    ELSE '   z_weir is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= '0'::numeric::double precision THEN ''::text
                    ELSE '   cc>=0.   '::text
                END AS reason
           FROM $model.weir_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert_stop IS NOT NULL THEN ''::text
                    ELSE '   z_invert_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling_stop IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.dt_regul_hr IS NOT NULL THEN ''::text
                    ELSE '   dt_regul_hr is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_control_node IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_pid_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_tz_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR new_1.q_z_crit IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul OR new_1.nr_z_gate IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                END AS reason
           FROM $model.regul_sluice_gate_singularity new_1
          WHERE NOT new_1.validity
        )
    select CONCAT('node:', n.id) as
        id,
        validity,
        name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom,
        reason from $model._node as n join node_invalidity_reason as r on r.id=n.id
        where validity=FALSE
    union
    select CONCAT('link:', l.id) as
        id,
        validity,
        name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom,
        reason from $model._link as l join link_invalidity_reason as r on  r.id=l.id
        where validity=FALSE
    union
    select CONCAT('singularity:', s.id) as
        id,
        s.validity,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        reason from $model._singularity as s join $model._node as n on s.node = n.id join singularity_invalidity_reason as r on r.id=s.id
        where s.validity=FALSE
    union
    select CONCAT('profile:', p.id) as
        id,
        p.validity,
        p.name,
        'profile'::varchar as type,
        ''::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        ''::varchar as reason from $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where p.validity=FALSE
;;

-- Results view
create or replace view ${model}.results_catchment as
    select id, name, geom from ${model}.catchment_node
;;

/* ********************************************** */
/*  Config bug fix: trigger fct changes           */
/* ********************************************** */

create or replace function ${model}.storage_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('storage', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'storage') where name = 'define_later' and id = id_;
            insert into $model._storage_node
                values (id_, 'storage', new.zs_array, new.zini, coalesce(new.contour, (select id from $model.coverage where st_intersects(geom, new.geom))));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'storage_node');

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'storage' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1)<=10 ) and (array_length(zs_array, 1) >=1) and (array_length(zs_array, 2)=2) from  $model._storage_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.zs_array, new.zini) is distinct from (old.zs_array, old.zini)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.zs_array, old.zini) as o, (select new.zs_array, new.zini) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.zs_array, new.zini) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._storage_node set zs_array=new.zs_array, zini=new.zini, contour=new.contour where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'storage_node');

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'storage' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'storage' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1)<=10 ) and (array_length(zs_array, 1) >=1) and (array_length(zs_array, 2)=2) from  $model._storage_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' and (select trigger_coverage from $model.metadata) then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'storage' = 'crossroad' and (select trigger_street_link from $model.metadata) then
                perform $model.update_street_links();
            end if;

            delete from $model._storage_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

create or replace function ${model}.station_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('station', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'station') where name = 'define_later' and id = id_;
            insert into $model._station_node
                values (id_, 'station', coalesce(new.area, 1), coalesce(new.z_invert, (select project.altitude(new.geom))), coalesce(new.station, (select id from $model.station where ST_Intersects(new.geom, geom))));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'station_node');

            -- Lines to update specific nodes that works with associated contours
            if 'station' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'station' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and (area>0) and (z_invert is not null) and (station is not null ) and ((select st_intersects(new.geom, geom) from $model.station where id=new.station)) from  $model._station_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area, new.z_invert) is distinct from (old.area, old.z_invert)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.z_invert) as o, (select new.area, new.z_invert) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.z_invert) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._station_node set area=new.area, z_invert=new.z_invert, station=new.station where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'station_node');

            -- Lines to update specific nodes that works with associated contours
            if 'station' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'station' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'station' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and (area>0) and (z_invert is not null) and (station is not null ) and ((select st_intersects(new.geom, geom) from $model.station where id=new.station)) from  $model._station_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'station' = 'storage' and (select trigger_coverage from $model.metadata) then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'station' = 'crossroad' and (select trigger_street_link from $model.metadata) then
                perform $model.update_street_links();
            end if;

            delete from $model._station_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

create or replace function ${model}.elem_2d_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('elem_2d', coalesce(new.name, 'define_later'), ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), $srid), (select precision from hydra.metadata)), new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'elem_2d') where name = 'define_later' and id = id_;
            insert into $model._elem_2d_node
                values (id_, 'elem_2d', coalesce(new.area, ST_Area(new.contour)), coalesce(new.zb, (select ST_Z(ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), $srid), (select precision from hydra.metadata))))), coalesce(new.rk, 12), new.domain_2d, new.contour);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'elem_2d_node');

            -- Lines to update specific nodes that works with associated contours
            if 'elem_2d' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'elem_2d' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and (area>0) and (zb is not null) and (rk is not null) and (rk>0) from  $model._elem_2d_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area, new.zb, new.rk) is distinct from (old.area, old.zb, old.rk)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.zb, old.rk) as o, (select new.area, new.zb, new.rk) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.zb, new.rk) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), $srid), (select precision from hydra.metadata)), generated=new.generated where id=old.id;
            update $model._elem_2d_node set area=new.area, zb=new.zb, rk=new.rk, domain_2d=new.domain_2d, contour=new.contour where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'elem_2d_node');

            -- Lines to update specific nodes that works with associated contours
            if 'elem_2d' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'elem_2d' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'elem_2d' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and (area>0) and (zb is not null) and (rk is not null) and (rk>0) from  $model._elem_2d_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'elem_2d' = 'storage' and (select trigger_coverage from $model.metadata) then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'elem_2d' = 'crossroad' and (select trigger_street_link from $model.metadata) then
                perform $model.update_street_links();
            end if;

            delete from $model._elem_2d_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

create or replace function ${model}.river_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('river', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'river') where name = 'define_later' and id = id_;
            insert into $model._river_node
                values (id_, 'river', coalesce(new.reach, (select id from $model.reach where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) asc limit 1)), coalesce(new.z_ground, (select project.altitude(new.geom))), coalesce(new.area, 1));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'river_node');

            -- Lines to update specific nodes that works with associated contours
            if 'river' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'river' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (z_ground is not null) and (area is not null) and (area>0) and (reach is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) from  $model._river_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.z_ground, new.area) is distinct from (old.z_ground, old.area)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_ground, old.area) as o, (select new.z_ground, new.area) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_ground, new.area) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._river_node set reach=new.reach, z_ground=new.z_ground, area=new.area where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'river_node');

            -- Lines to update specific nodes that works with associated contours
            if 'river' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'river' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'river' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (z_ground is not null) and (area is not null) and (area>0) and (reach is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) from  $model._river_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'river' = 'storage' and (select trigger_coverage from $model.metadata) then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'river' = 'crossroad' and (select trigger_street_link from $model.metadata) then
                perform $model.update_street_links();
            end if;

            delete from $model._river_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

create or replace function ${model}.pipe_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('pipe', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'pipe') where name = 'define_later' and id = id_;
            insert into $model._pipe_link
                values (id_, 'pipe', new.comment, new.z_invert_up, new.z_invert_down, coalesce(new.cross_section_type, 'circular'), coalesce(new.h_sable, 0), new.branch, new.rk, coalesce(new.custom_length, null), new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom);
            if 'pipe' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'pipe_link');
            update $model._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (h_sable is not null) and (cross_section_type is not null) and (cross_section_type not in ('valley')) and (rk is not null) and (rk >0) and (cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 )) and (cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)) and (cross_section_type!='pipe' or cp_geom is not null) and (cross_section_type!='channel' or op_geom is not null) from  $model._pipe_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' then
                update $model.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.comment, new.z_invert_up, new.z_invert_down, new.cross_section_type, new.h_sable, new.rk, new.custom_length, new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom) is distinct from (old.comment, old.z_invert_up, old.z_invert_down, old.cross_section_type, old.h_sable, old.rk, old.custom_length, old.circular_diameter, old.ovoid_height, old.ovoid_top_diameter, old.ovoid_invert_diameter, old.cp_geom, old.op_geom)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.comment, old.z_invert_up, old.z_invert_down, old.cross_section_type, old.h_sable, old.rk, old.custom_length, old.circular_diameter, old.ovoid_height, old.ovoid_top_diameter, old.ovoid_invert_diameter, old.cp_geom, old.op_geom) as o, (select new.comment, new.z_invert_up, new.z_invert_down, new.cross_section_type, new.h_sable, new.rk, new.custom_length, new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.comment, new.z_invert_up, new.z_invert_down, new.cross_section_type, new.h_sable, new.rk, new.custom_length, new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._pipe_link set comment=new.comment, z_invert_up=new.z_invert_up, z_invert_down=new.z_invert_down, cross_section_type=new.cross_section_type, h_sable=new.h_sable, branch=new.branch, rk=new.rk, custom_length=new.custom_length, circular_diameter=new.circular_diameter, ovoid_height=new.ovoid_height, ovoid_top_diameter=new.ovoid_top_diameter, ovoid_invert_diameter=new.ovoid_invert_diameter, cp_geom=new.cp_geom, op_geom=new.op_geom where id=old.id;

            if 'pipe' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) and (select trigger_branch from $model.metadata) then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'pipe_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update $model.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (h_sable is not null) and (cross_section_type is not null) and (cross_section_type not in ('valley')) and (rk is not null) and (rk >0) and (cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 )) and (cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)) and (cross_section_type!='pipe' or cp_geom is not null) and (cross_section_type!='channel' or op_geom is not null) from  $model._pipe_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._pipe_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'pipe' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' then
                update $model.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;

create or replace function ${model}.routing_hydrology_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('routing_hydrology', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'routing_hydrology') where name = 'define_later' and id = id_;
            insert into $model._routing_hydrology_link
                values (id_, 'routing_hydrology', coalesce(new.cross_section, 1), coalesce(new.length, 0.1), coalesce(new.slope, .01), coalesce(new.hydrograph, (select hbc.id from $model.hydrograph_bc_singularity as hbc where ST_DWithin(ST_EndPoint(new.geom), hbc.geom, .1))));
            if 'routing_hydrology' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'routing_hydrology_link');
            update $model._link set validity = (select (cross_section is not null) and (cross_section>=0) and (length is not null) and (length>=0) and (slope is not null) and (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station') and (hydrograph is not null))) from  $model._routing_hydrology_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'routing_hydrology' = 'pipe' then
                update $model.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.cross_section, new.length, new.slope) is distinct from (old.cross_section, old.length, old.slope)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.cross_section, old.length, old.slope) as o, (select new.cross_section, new.length, new.slope) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.cross_section, new.length, new.slope) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._routing_hydrology_link set cross_section=new.cross_section, length=new.length, slope=new.slope, hydrograph=new.hydrograph where id=old.id;

            if 'routing_hydrology' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) and (select trigger_branch from $model.metadata) then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'routing_hydrology_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'routing_hydrology' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update $model.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (cross_section is not null) and (cross_section>=0) and (length is not null) and (length>=0) and (slope is not null) and (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station') and (hydrograph is not null))) from  $model._routing_hydrology_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._routing_hydrology_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'routing_hydrology' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'routing_hydrology' = 'pipe' then
                update $model.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;

create or replace function ${model}.mesh_2d_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('mesh_2d', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'mesh_2d') where name = 'define_later' and id = id_;
            insert into $model._mesh_2d_link
                values (id_, 'mesh_2d', new.z_invert, coalesce(new.lateral_contraction_coef, 1), coalesce(new.border, $model.inter_pave_border_fct(up_, down_)));
            if 'mesh_2d' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'mesh_2d_link');
            update $model._link set validity = (select (z_invert is not null) and (lateral_contraction_coef is not null) and (lateral_contraction_coef<=1) and (lateral_contraction_coef>=0) and (ST_NPoints(border)=2) and (ST_IsValid(border)) from  $model._mesh_2d_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'mesh_2d' = 'pipe' then
                update $model.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.z_invert, new.lateral_contraction_coef) is distinct from (old.z_invert, old.lateral_contraction_coef)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.lateral_contraction_coef) as o, (select new.z_invert, new.lateral_contraction_coef) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.lateral_contraction_coef) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._mesh_2d_link set z_invert=new.z_invert, lateral_contraction_coef=new.lateral_contraction_coef, border=new.border where id=old.id;

            if 'mesh_2d' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) and (select trigger_branch from $model.metadata) then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'mesh_2d_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'mesh_2d' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update $model.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_invert is not null) and (lateral_contraction_coef is not null) and (lateral_contraction_coef<=1) and (lateral_contraction_coef>=0) and (ST_NPoints(border)=2) and (ST_IsValid(border)) from  $model._mesh_2d_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._mesh_2d_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'mesh_2d' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'mesh_2d' = 'pipe' then
                update $model.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;

create or replace function ${model}.porous_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('porous', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'porous') where name = 'define_later' and id = id_;
            insert into $model._porous_link
                values (id_, 'porous', new.z_invert, new.width, new.transmitivity, new.border);
            if 'porous' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'porous_link');
            update $model._link set validity = (select (z_invert is not null) and (width is not null) and (width>=0) and (transmitivity is not null) and (transmitivity>=0) from  $model._porous_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'porous' = 'pipe' then
                update $model.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.z_invert, new.width, new.transmitivity) is distinct from (old.z_invert, old.width, old.transmitivity)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.width, old.transmitivity) as o, (select new.z_invert, new.width, new.transmitivity) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.width, new.transmitivity) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._porous_link set z_invert=new.z_invert, width=new.width, transmitivity=new.transmitivity, border=new.border where id=old.id;

            if 'porous' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) and (select trigger_branch from $model.metadata) then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'porous_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'porous' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update $model.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_invert is not null) and (width is not null) and (width>=0) and (transmitivity is not null) and (transmitivity>=0) from  $model._porous_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._porous_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'porous' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'porous' = 'pipe' then
                update $model.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;

create or replace function ${model}.overflow_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('overflow', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'overflow') where name = 'define_later' and id = id_;
            insert into $model._overflow_link
                values (id_, 'overflow', new.z_crest1, new.width1, coalesce(new.z_crest2, new.z_crest1), coalesce(new.width2, 0), coalesce(new.cc, .6), coalesce(new.lateral_contraction_coef, 1), coalesce(new.break_mode, 'none'), new.z_break, new.t_break, new.z_invert, new.width_breach, coalesce(new.grp, 1), new.dt_fracw_array, new.border);
            if 'overflow' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'overflow_link');
            update $model._link set validity = (select (z_crest1 is not null) and (width1 is not null) and (width1>=0) and (z_crest2 is not null) and (z_crest2>=z_crest1) and (width2 is not null) and (width2>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (lateral_contraction_coef is not null) and (lateral_contraction_coef<=1) and (lateral_contraction_coef>=0) and (break_mode is not null) and (break_mode!='zw_critical' or z_break is not null) and (break_mode!='time_critical' or t_break is not null) and (break_mode='none' or width_breach is not null) and (break_mode='none' or z_invert is not null) and (break_mode='none' or grp is not null) and (break_mode='none' or grp>0) and (break_mode='none' or grp<100) and (break_mode='none' or dt_fracw_array is not null) and (break_mode='none' or array_length(dt_fracw_array, 1)<=10) and (break_mode='none' or array_length(dt_fracw_array, 1)>=1) and (break_mode='none' or array_length(dt_fracw_array, 2)=2) from  $model._overflow_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'overflow' = 'pipe' then
                update $model.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.z_crest1, new.width1, new.z_crest2, new.width2, new.cc, new.lateral_contraction_coef, new.break_mode, new.z_break, new.t_break, new.z_invert, new.width_breach, new.grp, new.dt_fracw_array) is distinct from (old.z_crest1, old.width1, old.z_crest2, old.width2, old.cc, old.lateral_contraction_coef, old.break_mode, old.z_break, old.t_break, old.z_invert, old.width_breach, old.grp, old.dt_fracw_array)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_crest1, old.width1, old.z_crest2, old.width2, old.cc, old.lateral_contraction_coef, old.break_mode, old.z_break, old.t_break, old.z_invert, old.width_breach, old.grp, old.dt_fracw_array) as o, (select new.z_crest1, new.width1, new.z_crest2, new.width2, new.cc, new.lateral_contraction_coef, new.break_mode, new.z_break, new.t_break, new.z_invert, new.width_breach, new.grp, new.dt_fracw_array) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_crest1, new.width1, new.z_crest2, new.width2, new.cc, new.lateral_contraction_coef, new.break_mode, new.z_break, new.t_break, new.z_invert, new.width_breach, new.grp, new.dt_fracw_array) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._overflow_link set z_crest1=new.z_crest1, width1=new.width1, z_crest2=new.z_crest2, width2=new.width2, cc=new.cc, lateral_contraction_coef=new.lateral_contraction_coef, break_mode=new.break_mode, z_break=new.z_break, t_break=new.t_break, z_invert=new.z_invert, width_breach=new.width_breach, grp=new.grp, dt_fracw_array=new.dt_fracw_array, border=new.border where id=old.id;

            if 'overflow' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) and (select trigger_branch from $model.metadata) then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'overflow_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'overflow' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update $model.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_crest1 is not null) and (width1 is not null) and (width1>=0) and (z_crest2 is not null) and (z_crest2>=z_crest1) and (width2 is not null) and (width2>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (lateral_contraction_coef is not null) and (lateral_contraction_coef<=1) and (lateral_contraction_coef>=0) and (break_mode is not null) and (break_mode!='zw_critical' or z_break is not null) and (break_mode!='time_critical' or t_break is not null) and (break_mode='none' or width_breach is not null) and (break_mode='none' or z_invert is not null) and (break_mode='none' or grp is not null) and (break_mode='none' or grp>0) and (break_mode='none' or grp<100) and (break_mode='none' or dt_fracw_array is not null) and (break_mode='none' or array_length(dt_fracw_array, 1)<=10) and (break_mode='none' or array_length(dt_fracw_array, 1)>=1) and (break_mode='none' or array_length(dt_fracw_array, 2)=2) from  $model._overflow_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._overflow_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'overflow' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'overflow' = 'pipe' then
                update $model.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;

create or replace function ${model}.connector_hydrology_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('connector_hydrology', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'connector_hydrology') where name = 'define_later' and id = id_;
            insert into $model._connector_hydrology_link
                values (id_, 'connector_hydrology', coalesce(new.hydrograph, (select hbc.id from $model.hydrograph_bc_singularity as hbc where ST_DWithin(ST_EndPoint(new.geom), hbc.geom, .1))));
            if 'connector_hydrology' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'connector_hydrology_link');
            update $model._link set validity = (select (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station') and (hydrograph is not null))) from  $model._connector_hydrology_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'connector_hydrology' = 'pipe' then
                update $model.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if (('no_column') is distinct from ('no_column')) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select ) as o, (select ) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select ) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._connector_hydrology_link set hydrograph=new.hydrograph where id=old.id;

            if 'connector_hydrology' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) and (select trigger_branch from $model.metadata) then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'connector_hydrology_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'connector_hydrology' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update $model.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station') and (hydrograph is not null))) from  $model._connector_hydrology_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._connector_hydrology_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'connector_hydrology' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'connector_hydrology' = 'pipe' then
                update $model.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;

create or replace function ${model}.strickler_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('strickler', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'strickler') where name = 'define_later' and id = id_;
            insert into $model._strickler_link
                values (id_, 'strickler', new.z_crest1, new.width1, coalesce(new.length, ST_Length(new.geom)), coalesce(new.rk, 12), coalesce(new.z_crest2, new.z_crest1+0.001), coalesce(new.width2, new.width1+0.001), new.border);
            if 'strickler' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'strickler_link');
            update $model._link set validity = (select (z_crest1 is not null) and (width1 is not null) and (width1>0) and (length is not null) and (length>0) and (rk is not null) and (rk>=0) and (z_crest2 is not null) and (z_crest2>z_crest1) and (width2 is not null) and (width2>width1) from  $model._strickler_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'strickler' = 'pipe' then
                update $model.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.z_crest1, new.width1, new.length, new.rk, new.z_crest2, new.width2) is distinct from (old.z_crest1, old.width1, old.length, old.rk, old.z_crest2, old.width2)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_crest1, old.width1, old.length, old.rk, old.z_crest2, old.width2) as o, (select new.z_crest1, new.width1, new.length, new.rk, new.z_crest2, new.width2) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_crest1, new.width1, new.length, new.rk, new.z_crest2, new.width2) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._strickler_link set z_crest1=new.z_crest1, width1=new.width1, length=new.length, rk=new.rk, z_crest2=new.z_crest2, width2=new.width2, border=new.border where id=old.id;

            if 'strickler' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) and (select trigger_branch from $model.metadata) then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'strickler_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'strickler' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update $model.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_crest1 is not null) and (width1 is not null) and (width1>0) and (length is not null) and (length>0) and (rk is not null) and (rk>=0) and (z_crest2 is not null) and (z_crest2>z_crest1) and (width2 is not null) and (width2>width1) from  $model._strickler_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._strickler_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'strickler' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'strickler' = 'pipe' then
                update $model.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;
