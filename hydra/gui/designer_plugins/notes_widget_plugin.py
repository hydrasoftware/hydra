from PyQt5 import QtGui, QtDesigner
from hydra.gui.widgets.notes_widget import NotesWidget
import os

_logo_pixmap = QtGui.QPixmap(os.path.join(os.path.dirname(__file__), '..', '..', 'ressources', 'images', 'hydra.png'))

class NotesWidgetPlugin(QtDesigner.QPyDesignerCustomWidgetPlugin):
    def __init__(self, parent = None):
        QtDesigner.QPyDesignerCustomWidgetPlugin.__init__(self)
        self.initialized = False

    def initialize(self, core):
        if self.initialized:
            return
        self.initialized = True

    def isInitialized(self):
        return self.initialized

    def createWidget(self, parent):
        return NotesWidget(parent=parent)

    def name(self):
        return "NotesWidget"

    def group(self):
        return "Hydra"

    def icon(self):
        return QtGui.QIcon(_logo_pixmap)

    def toolTip(self):
        return ""

    def whatsThis(self):
        return ""

    def isContainer(self):
        return False

    def domXml(self):
        return (
               '<widget class="NotesWidget" name=\"notes\">\n'
               " <property name=\"toolTip\" >\n"
               "  <string>Notes</string>\n"
               " </property>\n"
               " <property name=\"whatsThis\" >\n"
               "  <string>Notes editor</string>\n"
               " </property>\n"
               "</widget>\n"
               )

    def includeFile(self):
        return "hydra.gui.widgets.notes_widget"





