/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update ${model}.metadata set version = '3.4';
;;

alter table ${model}.valley_cross_section_geometry add column sz_array real[]
;;


alter table ${model}._constrain add column line_xyz integer references project.line_xyz(id) on delete set null
;;

drop view if exists ${model}.constrain cascade
;;

drop trigger if exists ${model}_constrain_3d_after_trig on $model._constrain
;;

drop materialized view if exists $model.constrain_3d cascade
;;

alter table ${model}.metadata drop column trigger_constrain_3d
;;

alter table ${model}.valley_cross_section_geometry drop constraint valley_cross_section_geometry_zbmaj_lbank_array_check
;;

alter table ${model}.valley_cross_section_geometry drop constraint valley_cross_section_geometry_zbmaj_rbank_array_check
;;

alter table ${model}.valley_cross_section_geometry add constraint valley_cross_section_geometry_zbmaj_lbank_array_check
check(array_length(zbmaj_lbank_array, 1)<=8 and array_length(zbmaj_lbank_array, 1)>=1 and array_length(zbmaj_lbank_array, 2)=2)
;;

alter table ${model}.valley_cross_section_geometry add constraint valley_cross_section_geometry_zbmaj_rbank_array_check
check(array_length(zbmaj_rbank_array, 1)<=8 and array_length(zbmaj_rbank_array, 1)>=1 and array_length(zbmaj_rbank_array, 2)=2)
;;

alter table $model._river_cross_section_profile
drop constraint _river_cross_section_profile_down_cp_geom_fkey,
drop constraint _river_cross_section_profile_up_cp_geom_fkey,
drop constraint _river_cross_section_profile_down_op_geom_fkey,
drop constraint _river_cross_section_profile_up_op_geom_fkey,
drop constraint _river_cross_section_profile_down_vcs_geom_fkey,
drop constraint _river_cross_section_profile_up_vcs_geom_fkey,
add constraint _river_cross_section_profile_down_cp_geom_fkey foreign key (down_cp_geom) references $model.closed_parametric_geometry(id) on delete set null on update cascade,
add constraint _river_cross_section_profile_up_cp_geom_fkey foreign key (up_cp_geom) references $model.closed_parametric_geometry(id) on delete set null on update cascade,
add constraint _river_cross_section_profile_down_op_geom_fkey foreign key (down_op_geom) references $model.open_parametric_geometry(id) on delete set null on update cascade,
add constraint _river_cross_section_profile_up_op_geom_fkey foreign key (up_op_geom) references $model.open_parametric_geometry(id) on delete set null on update cascade,
add constraint _river_cross_section_profile_down_vcs_geom_fkey foreign key (down_vcs_geom) references $model.valley_cross_section_geometry(id) on delete set null on update cascade,
add constraint _river_cross_section_profile_up_vcs_geom_fkey foreign key (up_vcs_geom) references $model.valley_cross_section_geometry(id) on delete set null on update cascade
;;

alter table $model.valley_cross_section_geometry
drop constraint valley_cross_section_geometry_transect_fkey,
add constraint valley_cross_section_geometry_transect_fkey foreign key (transect) references $model._constrain(id) on delete set null on update cascade
;;


create view $model.constrain as
select c.id, c.name, c.geom, discretized, elem_length, constrain_type, link_attributes, points_xyz, points_xyz_proximity,
    exists (select 1
       from project.points_xyz as points
       where c.points_xyz = points.points_id
       and St_DWithin(points.geom, c.geom, c.points_xyz_proximity))  as _is_3d, line_xyz, l.geom as topo
from $model._constrain as c
left join project.line_xyz l on l.id = c.line_xyz
;;

alter view $model.constrain alter column id set default nextval('$model._constrain_id_seq'::regclass)
;;

create view $model.flood_plain_transect_candidate as
select l.id, l.name, l.id as line_xyz, project.orient_transect(r.geom, st_makeline(st_startpoint(l.geom), st_endpoint(l.geom))) as geom, 'ignored_for_coverages'::hydra_constrain_type as constrain_type,
    st_intersection(r.geom, st_makeline(st_startpoint(l.geom), st_endpoint(l.geom))) as reach_intersection, l.geom as topo
from project.line_xyz l
join $model.reach r on st_intersects(r.geom, l.geom)
where not exists (select 1 from $model._river_cross_section_profile p where p.name = l.name)
;;

-- create constrain, river cross section and associated geometry from a list of flood_plain_transect_candidate ids
create function $model.create_river_cross_section_from_line_xyz(candidate_id integer)
returns integer -- created cross section ids
language plpgsql volatile as
$$$$
declare
    res integer;
    z_invert real;
    top timestamp;
begin
    top := clock_timestamp();

    insert into $model.constrain(name, line_xyz, geom, constrain_type)
    select name, line_xyz, geom, constrain_type
    from $model.flood_plain_transect_candidate t
    where t.id = candidate_id and not exists (select 1 from $model.constrain c where c.name=t.name)
    ;

    select min(st_z(p.geom))
    from $model.flood_plain_transect_candidate t
    join lateral st_dumppoints(t.topo) p on true
    where t.id = candidate_id
    into z_invert
    ;

    insert into $model.valley_cross_section_geometry(name, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, transect, sz_array)
    select t.name, ('{{'||z_invert||', 0}}')::real[], ('{{'||z_invert||', 0}}')::real[], ('{{'||z_invert||', 0}}')::real[],
        c.id, array_agg(ARRAY[st_linelocatepoint(t.geom, p.geom)*st_length(t.geom), st_z(p.geom)] order by p.path)
    from $model.flood_plain_transect_candidate t
    join $model.constrain c on c.name = t.name
    join lateral st_dumppoints(c.topo) p on true
    where t.id = candidate_id and not exists (select 1 from $model.valley_cross_section_geometry g where g.name=t.name)
    group by t.name, c.id
    ;

    -- create river nodes where needed
    insert into $model.river_node(name, geom)
    select distinct t.name, t.reach_intersection
    from $model.flood_plain_transect_candidate t
    where t.id = candidate_id and not exists (select 1 from $model.river_node n where st_dwithin(t.reach_intersection, n.geom, 1.))
    ;

    insert into $model._river_cross_section_profile(name, id, type_cross_section_up, type_cross_section_down, down_vcs_geom, z_invert_down)
    select t.name, (select n.id from $model.river_node n order by n.geom <-> t.reach_intersection limit 1) id, null, 'valley', g.id down_vcs_geom, z_invert
    from $model.flood_plain_transect_candidate t
    join $model.valley_cross_section_geometry g on g.name = t.name
    where not exists (select 1 from $model.river_cross_section_profile p where st_dwithin(t.reach_intersection, p.geom, 1.))
    and t.id = candidate_id
    returning id into res
    ;

    return res;
end;
$$$$
;;

create or replace view $model.close_point as
    with allpoints as (
        select id, (st_dumppoints(geom)).geom as geom from $model.constrain where constrain_type is distinct from 'ignored_for_coverages'
    ),
    ends as (
        select id, st_startpoint(geom) as geom from $model.constrain where constrain_type is distinct from 'ignored_for_coverages'
        union
        select id, st_endpoint(geom) as geom from $model.constrain where constrain_type is distinct from 'ignored_for_coverages'
    ),
    cpoints as (
        select id, st_collect(geom) as geom from allpoints group by id
    ),
    clust as (
        select
          sqrt(ST_Area(ST_MinimumBoundingCircle(gc)) / pi()) as radius,
          ST_NumGeometries(gc) as nb,
          ST_Centroid(gc) as geom
        from (
          select unnest(ST_ClusterWithin(geom, 5)) gc
          from allpoints
        ) f
        union
        select
            1 as radius,
            0 as nb,
            st_force2d(e.geom) as geom
        from $model.constrain as c, cpoints as cp, ends as e
        where st_dwithin(c.geom, e.geom, 1)
        and not st_intersects(cp.geom, e.geom)
        and e.id != c.id
        and c.id = cp.id
        and c.constrain_type is distinct from 'ignored_for_coverages'
    )
select row_number() over () AS id, radius, nb, geom::geometry('POINT', $srid) from clust where radius > 0
;;

create or replace function project.interpolate_z(l double precision, a double precision[])
returns real
language plpgsql immutable as
$$$$
declare
    m   double precision[];
    lp double precision;
    zp double precision;
begin
    if l <= a[1][1] then
        return a[1][2];
    end if;
    foreach m slice 1 in array a
    loop
        if m[1] >= l then
            return zp + (m[2]-zp)*(l-lp)/(m[1]-lp);
        end if;
        lp := m[1];
        zp := m[2];
    end loop;
    return a[array_length(a,1)][2];
end
$$$$
;;


create view $model.constrain_3d as
with projected as (
    select c.id, st_linelocatepoint(c.geom, p.geom) as loc, c.geom,
        st_distance(c.geom, p.geom) as dist, p.z_ground as z
    from $model._constrain c
    join project.points_xyz p on st_dwithin(p.geom, c.geom, c.points_xyz_proximity) and c.points_xyz = p.points_id
),
distinct_projected as(
    select distinct on (loc) id, st_lineinterpolatepoint(geom, loc) as geom, loc, z
            from projected
            order by loc, dist -- dictin on + order by makes sure that if two poinst project
                            -- on the same spot (e.g. extremities) we keep only the closest
),
all_points as (
    select c.id, d.geom, st_linelocatepoint(c.geom, (d).geom) as loc, null as z
    from $model._constrain c
    cross join lateral st_dumppoints(c.geom) d
    where c.id in (select distinct id from projected)
    union all
    select * from distinct_projected
),
lz as (
    select id, array_agg(ARRAY[loc, z]) as a
    from distinct_projected
    where z is not null
    group by id
)
select i.id, st_setsrid(st_makeline(st_makepoint(
    st_x(i.geom), st_y(i.geom), coalesce(i.z, project.interpolate_z(i.loc, lz.a))) order by i.loc), $srid) as geom
from all_points i
join lz on lz.id = i.id
group by i.id
;;


create or replace function ${model}.constrain_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        prec_ real;
        snapped_ geometry;
        other_cst_ geometry;
        differenced_ geometry;
        oned_ geometry;
        discretized_ geometry;
        top timestamp;
    begin
        top := clock_timestamp();

        select precision from hydra.metadata into prec_;
        if tg_op = 'INSERT' then
            select st_snaptogrid(new.geom, prec_) into snapped_;
            if new.constrain_type != 'ignored_for_coverages' then
                select ST_Linemerge(ST_Collect(geom)) from $model._constrain where ST_Intersects(snapped_, geom) and constrain_type != 'ignored_for_coverages' into other_cst_;
                select ST_LineMerge(coalesce(ST_Difference(snapped_, other_cst_), snapped_)) into differenced_;
            else
                select snapped_ into differenced_;
            end if;
            if not ST_IsEmpty(differenced_) then
                select ST_GeometryN(differenced_, 1) into oned_;
                select st_snaptogrid(project.discretize_line(oned_, coalesce(new.elem_length, 100)), prec_) into discretized_;
                if not (select St_IsEmpty(st_snaptogrid(new.geom, prec_))) then
                    insert into $model._constrain(name, geom, discretized, elem_length, constrain_type, link_attributes, points_xyz, points_xyz_proximity, line_xyz)
                    values (coalesce(new.name, 'define_later'), oned_, discretized_, coalesce(new.elem_length, 100), new.constrain_type, new.link_attributes, new.points_xyz, coalesce(new.points_xyz_proximity, 1), new.line_xyz)
                    returning id, elem_length, points_xyz, points_xyz_proximity into new.id, new.elem_length, new.points_xyz, new.points_xyz_proximity ;
                    update $model._constrain set name = 'CONSTR'||new.id::varchar where name = 'define_later' and id = new.id
                    returning name into new.name;
                    return new;
                end if;
                return null;
            end if;
            return null;
        elsif tg_op = 'UPDATE' then
            select st_snaptogrid(new.geom, prec_) into snapped_;
            if new.constrain_type != 'ignored_for_coverages' then
                select ST_Linemerge(ST_Collect(geom)) from $model._constrain where ST_Intersects(snapped_, geom) and id!=old.id and constrain_type != 'ignored_for_coverages' into other_cst_;
                select ST_LineMerge(coalesce(ST_Difference(snapped_, other_cst_), snapped_)) into differenced_;
            else
                select snapped_ into differenced_;
            end if;
            if not ST_IsEmpty(differenced_) then
                select ST_GeometryN(differenced_, 1) into oned_;
                select st_snaptogrid(project.discretize_line(differenced_, coalesce(new.elem_length, 100)), prec_) into discretized_;
                update $model._constrain set name=new.name, geom=differenced_, discretized=discretized_,
                    elem_length=new.elem_length, constrain_type=new.constrain_type, link_attributes=new.link_attributes,
                    points_xyz=new.points_xyz, points_xyz_proximity=coalesce(new.points_xyz_proximity, 1),
                    line_xyz=new.line_xyz
                    where id = old.id;
                return new;
            end if;
            return old;
        elsif tg_op = 'DELETE' then
            delete from $model._constrain where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_constrain_trig
    instead of insert or update or delete on $model.constrain
       for each row execute procedure ${model}.constrain_fct()
;;



create view ${model}.flood_plain_bluff as
    with bluff as (
        select st_linemerge(st_difference(st_exteriorring(c.geom), st_collect(t.discretized))) as geom
        from ${model}.flood_plain c join ${model}.constrain t on st_intersects(c.geom, t.discretized)
        where t.constrain_type='flood_plain_transect'
        group by c.id, c.geom
    )
    select row_number() over() as id, geom from (select (st_dump(geom)).geom from bluff) as t
;;

create view ${model}.flood_plain_bluff_point as
    select row_number() over() as id, (d).geom, (d).path, id as bluff
    from (select id, st_dumppoints(geom) as d, st_numpoints(geom) as n from ${model}.flood_plain_bluff) as t
    where (d).path[1] != 1 and (d).path[1] != n
;;

create materialized view ${model}.flood_plain_bluff_point_transect as
    with fp as (
        select id, bluff, ${model}.interpolate_transect_at(geom) as geom, geom as pt from ${model}.flood_plain_bluff_point
    ),
    valley_section as (
        select ${model}.valley_section_at(st_pointn(fp.geom, 2)) as section, id from fp
    )
    select fp.id, fp.bluff, fp.geom,
        (valley_section.section).section as section,
        (valley_section.section).frac_maj_l as frac_maj_l,
        (valley_section.section).frac_min as frac_min,
        (valley_section.section).frac_maj_r as frac_maj_r,
    case when st_intersects(fp.pt, st_startpoint(fp.geom)) then 'left' else 'right' end as side, r.id as reach, st_linelocatepoint(r.geom, st_pointn(fp.geom, 2)) as reach_rel_abs
    from fp join ${model}.reach r on st_dwithin(r.geom, st_pointn(fp.geom, 2), .1)
    join valley_section on fp.id = valley_section.id
    where st_numpoints(fp.geom) = 3
    with no data
;;

create materialized view ${model}.flood_plain_flow_line as
    with line as (
    select id, 1 as ord, st_makeline(array(
                select ${model}.section_interpolate_point(t.geom, t.section,
                case when t.side = 'right' then (0.2*(t.frac_maj_r/(t.frac_maj_r+0.5*t.frac_min)))::real else (0.2*(t.frac_maj_l/(t.frac_maj_l+0.5*t.frac_min)))::real end,  t.side)
                from ${model}.flood_plain_bluff_point_transect as t where t.bluff=b.id
                order by st_linelocatepoint(b.geom,  case when t.side = 'left' then st_startpoint(t.geom) else st_endpoint(t.geom) end)
            )) as geom
    from ${model}.flood_plain_bluff as b
    union all
    select id, 2 as ord, st_makeline(array(
                select ${model}.section_interpolate_point(t.geom, t.section,
                case when t.side = 'right' then (0.4*(t.frac_maj_r/(t.frac_maj_r+0.5*t.frac_min)))::real else (0.4*(t.frac_maj_l/(t.frac_maj_l+0.5*t.frac_min)))::real end,  t.side)
                from ${model}.flood_plain_bluff_point_transect as t where t.bluff=b.id
                order by st_linelocatepoint(b.geom,  case when t.side = 'left' then st_startpoint(t.geom) else st_endpoint(t.geom) end)
            )) as geom
    from ${model}.flood_plain_bluff as b
    union all
    select id, 3 as ord, st_makeline(array(
                select ${model}.section_interpolate_point(t.geom, t.section,
                case when t.side = 'right' then (0.6*(t.frac_maj_r/(t.frac_maj_r+0.5*t.frac_min)))::real else (0.6*(t.frac_maj_l/(t.frac_maj_l+0.5*t.frac_min)))::real end,  t.side)
                from ${model}.flood_plain_bluff_point_transect as t where t.bluff=b.id
                order by st_linelocatepoint(b.geom,  case when t.side = 'left' then st_startpoint(t.geom) else st_endpoint(t.geom) end)
            )) as geom
    from ${model}.flood_plain_bluff as b
    union all
    select id, 4 as ord, st_makeline(array(
                select ${model}.section_interpolate_point(t.geom, t.section,
                case when t.side = 'right' then (0.8*(t.frac_maj_r/(t.frac_maj_r+0.5*t.frac_min)))::real else (0.8*(t.frac_maj_l/(t.frac_maj_l+0.5*t.frac_min)))::real end,  t.side)
                from ${model}.flood_plain_bluff_point_transect as t where t.bluff=b.id
                order by st_linelocatepoint(b.geom,  case when t.side = 'left' then st_startpoint(t.geom) else st_endpoint(t.geom) end)
            )) as geom
    from ${model}.flood_plain_bluff as b
    union all
    select id, 5 as ord, st_makeline(array(
                select ${model}.section_interpolate_point(t.geom, t.section,
                case when t.side = 'right' then (1.0*(t.frac_maj_r/(t.frac_maj_r+0.5*t.frac_min)))::real else (1.0*(t.frac_maj_l/(t.frac_maj_l+0.5*t.frac_min)))::real end,  t.side)
                from ${model}.flood_plain_bluff_point_transect as t where t.bluff=b.id
                order by st_linelocatepoint(b.geom,  case when t.side = 'left' then st_startpoint(t.geom) else st_endpoint(t.geom) end)
            )) as geom
    from ${model}.flood_plain_bluff as b
    ),
    segment as (
        select st_makeline(lag((pt).geom, 1, null) over (partition by id, ord order by id, (pt).path), (pt).geom) as geom
        from (select id, ord, ST_DumpPoints(geom) as pt from line) as dumps
    ),
    merged as (
        select st_linemerge(st_collect(s.geom))  as geom
        from segment s join ${model}.flood_plain p on st_covers(p.geom, s.geom)
        where not exists (select 1 from ${model}.reach as r where st_intersects(r.geom, s.geom))
    )
    select row_number() over() as id, geom from (select (st_dump(geom)).geom from merged) as t
    with no data
;;


create index ${model}_flood_plain_flow_line_geom_idx on ${model}.flood_plain_flow_line using gist(geom)
;;

create materialized view ${model}.reach_flow_line as
    with point as (
	    select t.id, ${model}.section_interpolate_point(t.geom, t.section, 0.999,  t.side) as geom
	    from ${model}.flood_plain_bluff_point_transect as t , ${model}.flood_plain_bluff as b
	    where t.bluff=b.id
    ),
    ordered as (
		select p.id, p.geom, r.id as reach, ST_LineLocatePoint(r.geom, p.geom) as pk
		from point as p
			join ${model}.reach as r on r.id = (select r.id from ${model}.reach as r
								order by ST_Distance(r.geom, p.geom) asc limit 1)
    ),
    segment as (
        select st_makeline(lag(geom, 1, null) over (partition by reach order by pk), geom) as geom
        from ordered
    ),
    merged as (
        select st_linemerge(st_collect(s.geom))  as geom
        from segment s join ${model}.flood_plain p on st_covers(p.geom, s.geom)
    )
    select row_number() over() as id, geom from (select (st_dump(geom)).geom from merged) as t
    with no data
;;

create index ${model}_reach_flow_line_geom_idx on ${model}.reach_flow_line using gist(geom)
;;


create materialized view ${model}.coted_point as
    with transect_start as (
        select ${model}.section_interpolate_point(t.geom, t.section, 0, t.side) as geom
        from ${model}.flood_plain_bluff_point_transect as t
    ),
    contour_point as (
        select (st_dumppoints(st_exteriorring(geom))).geom from ${model}.flood_plain
    ),
    missing_tr as (
        select (st_dumppoints(st_difference(ct.geom, trc.geom))).geom from
        (select st_collect(geom) as geom from contour_point) as ct,
        (select st_collect(geom) as geom from transect_start) as trc
    ),
    pt as (
        select (st_dumppoints(geom)).geom as geom from ${model}.flood_plain_flow_line
        union all
        select (st_dumppoints(geom)).geom as geom from ${model}.reach_flow_line
        union all
        select geom from transect_start
        union all
        select ${model}.set_reach_point_altitude(geom) from missing_tr
    )
    select row_number() over() as id, geom from pt
    with no data
;;

create index ${model}_coted_point_geom_idx on ${model}.coted_point using gist(geom)
;;

create materialized view ${model}.coted_point_with_interp as
    with easy as (
        select distinct on (cp.id) cp.id, cp.geom, ft.reach, ft.reach_rel_abs
        from ${model}.coted_point as cp join ${model}.flood_plain_bluff_point_transect as ft on st_dwithin(cp.geom, ft.geom, .1)
    ),
    hard as (
        select id, geom from ${model}.coted_point
        except
        select id, geom from easy
    ),
    hard_section as (
        select id, geom, ${model}.interpolate_transect_at(geom) as transect
        from hard
    ),
    interp as (
        select id, geom, reach, reach_rel_abs from easy
        union all
        select distinct on (h.id) h.id, h.geom, r.id as reach, st_linelocatepoint(r.geom, st_closestpoint(r.geom, h.transect)) as reach_rel_abs
        from hard_section as h join ${model}.reach as r on st_intersects(r.geom, ST_Buffer(h.transect, 0.01))
    ),
    reach_segment as (
        select n.reach, n.id as nd, st_linelocatepoint(r.geom, n.geom) as reach_rel_abs_nd,
        lag(n.id) over(w) as strt, lag(st_linelocatepoint(r.geom, n.geom)) over(w) as reach_rel_abs_strt,
        n.geom as nd_geom,
        lag(n.geom) over(w) as strt_geom
        from ${model}.river_node as n join ${model}.reach as r on r.id=n.reach
        window w as (partition by n.reach order by st_linelocatepoint(r.geom, n.geom))
    )
    select distinct on (i.id) i.id, i.geom, s.strt, s.nd, s.reach_rel_abs_strt, i.reach_rel_abs, s.reach_rel_abs_nd, st_makeline(array[s.strt_geom, i.geom, nd_geom]) as lnk, 1. - (i.reach_rel_abs - s.reach_rel_abs_strt)/(s.reach_rel_abs_nd - s.reach_rel_abs_strt) as weight_strt, (i.reach_rel_abs - s.reach_rel_abs_strt)/(s.reach_rel_abs_nd - s.reach_rel_abs_strt) as weight_nd
    from interp as i join reach_segment as s on (s.reach=i.reach and s.reach_rel_abs_strt <= i.reach_rel_abs and s.reach_rel_abs_nd >= i.reach_rel_abs)
    with no data
;;

create index ${model}_coted_point_with_interp_geom_idx on  ${model}.coted_point_with_interp using gist(geom)
;;

create materialized view ${model}.mesh_1d as
    with lines as (
        select geom
        from ${model}.flood_plain_flow_line
        union
        select geom
        from ${model}.reach_flow_line
    ),
    blade as (
        select ST_Union(ST_Buffer(geom, 0.001, 'endcap=square')) as geom
        from lines
    ),
    polyg as (
        select ST_Force2D(ST_Difference(p.geom, b.geom)) as geom
        from ${model}.flood_plain as p, blade as b
    ),
    triang as (
        select (ST_Dump(ST_Tesselate(geom))).geom from polyg
    )
    select row_number() over() as id, geom from triang
    with no data
;;

create index  ${model}_mesh_1d_geom_idx on  ${model}.mesh_1d using gist(geom)
;;

create materialized view ${model}.mesh_1d_coted as
    select row_number() over() as id,
    a.id as a, a.strt as a_strt, a.weight_strt as a_strt_w, a.nd as a_nd, a.weight_nd as a_nd_w,
    b.id as b, b.strt as b_strt, b.weight_strt as b_strt_w, b.nd as b_nd, b.weight_nd as b_nd_w,
    c.id as c, c.strt as c_strt, c.weight_strt as c_strt_w, c.nd as c_nd, c.weight_nd as c_nd_w,
    (st_z(a.geom) + st_z(b.geom) + st_z(c.geom))/3. z_mean,
    st_z(a.geom) as z_a, st_z(b.geom) as z_b, st_z(c.geom) as z_c,
    st_makepolygon(st_makeline(array[a.geom, b.geom, c.geom, a.geom])) as geom,
    st_makeline(array[st_startpoint(a.lnk), st_centroid(st_makepolygon(st_makeline(array[a.geom, b.geom, c.geom, a.geom]))), st_endpoint(a.lnk)]) as lnk,
    now() as last_refresh
    from ${model}.mesh_1d as e
    join ${model}.coted_point_with_interp as a on st_dwithin(st_pointn(st_exteriorring(e.geom), 1), a.geom, .1)
    join ${model}.coted_point_with_interp as b on st_dwithin(st_pointn(st_exteriorring(e.geom), 2), b.geom, .1)
    join ${model}.coted_point_with_interp as c on st_dwithin(st_pointn(st_exteriorring(e.geom), 3), c.geom, .1)
    where a.id!=b.id and b.id!= c.id and c.id!=a.id
    with no data
;;

--------------------------------------------
-- AERAULICS
--------------------------------------------

--
-- Name: _air_duct_link; Type: TABLE;
--

CREATE TABLE ${model}._air_duct_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    comment character varying(256),
    z_invert_up real,
    z_invert_down real,
    area real,
    perimeter real,
    friction_coefficient real,
    custom_length real,
    CONSTRAINT _air_duct_link_link_type_check CHECK ((link_type = 'air_duct'::public.hydra_link_type))
)
;;

--
-- Name: _air_flow_bc_singularity; Type: TABLE;
--

CREATE TABLE ${model}._air_flow_bc_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    flow real,
    is_inwards boolean,
    CONSTRAINT _air_flow_bc_singularity_singularity_type_check CHECK ((singularity_type = 'air_flow_bc'::public.hydra_singularity_type))
)
;;

--
-- Name: _air_headloss_link; Type: TABLE;
--

CREATE TABLE ${model}._air_headloss_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    comment character varying(256),
    area real,
    up_to_down_headloss_coefficient real,
    down_to_up_headloss_coefficient real,
    CONSTRAINT _air_headloss_link_link_type_check CHECK ((link_type = 'air_headloss'::public.hydra_link_type))
)
;;

--
-- Name: _air_pressure_bc_singularity; Type: TABLE;
--

CREATE TABLE ${model}._air_pressure_bc_singularity (
    id integer NOT NULL,
    singularity_type public.hydra_singularity_type NOT NULL,
    relative_pressure real,
    CONSTRAINT _air_pressure_bc_singularity_singularity_type_check CHECK ((singularity_type = 'air_pressure_bc'::public.hydra_singularity_type))
)
;;

--
-- Name: _ventilator_link; Type: TABLE;
--

CREATE TABLE ${model}._ventilator_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    comment character varying(256),
    q_dp_array real[],
    is_up_to_down boolean,
    CONSTRAINT _ventilator_link_link_type_check CHECK ((link_type = 'ventilator'::public.hydra_link_type))
)
;;

--
-- Name: _jet_fan_link; Type: TABLE;
--

CREATE TABLE ${model}._jet_fan_link (
    id integer NOT NULL,
    link_type public.hydra_link_type NOT NULL,
    comment character varying(256),
    unit_thrust_newton real,
    number_of_units integer,
    efficiency real,
    flow_velocity real,
    pipe_area real,
    is_up_to_down boolean,
    CONSTRAINT _jet_fan_link_link_type_check CHECK ((link_type = 'jet_fan'::public.hydra_link_type))
)
;;

--
-- Name: air_duct_link; Type: VIEW; Schema: ${model};
--

CREATE VIEW ${model}.air_duct_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.comment,
    c.z_invert_up,
    c.z_invert_down,
    c.area,
    c.perimeter,
    c.friction_coefficient,
    c.custom_length,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM ${model}._air_duct_link c,
    ${model}._link p
  WHERE (p.id = c.id)
;;

--
-- Name: air_flow_bc_singularity; Type: VIEW; Schema: ${model};
--

CREATE VIEW ${model}.air_flow_bc_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.flow,
    c.is_inwards,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM ${model}._air_flow_bc_singularity c,
    ${model}._singularity p,
    ${model}._node n
  WHERE ((p.id = c.id) AND (n.id = p.node))
;;

--
-- Name: air_headloss_link; Type: VIEW; Schema: ${model};
--

CREATE VIEW ${model}.air_headloss_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.comment,
    c.area,
    c.up_to_down_headloss_coefficient,
    c.down_to_up_headloss_coefficient,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM ${model}._air_headloss_link c,
    ${model}._link p
  WHERE (p.id = c.id)
;;

--
-- Name: air_pressure_bc_singularity; Type: VIEW; Schema: ${model};
--

CREATE VIEW ${model}.air_pressure_bc_singularity AS
 SELECT p.id,
    p.name,
    n.id AS node,
    c.relative_pressure,
    n.geom,
    (p.configuration)::character varying AS configuration,
    p.validity,
    p.configuration AS configuration_json
   FROM ${model}._air_pressure_bc_singularity c,
    ${model}._singularity p,
    ${model}._node n
  WHERE ((p.id = c.id) AND (n.id = p.node))
;;

--
-- Name: jet_fan_link; Type: VIEW; Schema: network;
--

CREATE VIEW ${model}.jet_fan_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.comment,
    c.unit_thrust_newton,
    c.number_of_units,
    c.efficiency,
    c.flow_velocity,
    c.pipe_area,
    c.is_up_to_down,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM ${model}._jet_fan_link c,
    ${model}._link p
  WHERE (p.id = c.id)
;;

--
-- Name: ventilator_link; Type: VIEW; Schema: network; Owner: vmo
--

CREATE VIEW ${model}.ventilator_link AS
 SELECT p.id,
    p.name,
    p.up,
    p.down,
    c.comment,
    c.q_dp_array,
    c.is_up_to_down,
    p.geom,
    (p.configuration)::character varying AS configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration AS configuration_json
   FROM ${model}._ventilator_link c,
    ${model}._link p
  WHERE (p.id = c.id)
;;

--
-- Name: air_duct_link_fct(); Type: FUNCTION; Schema: ${model};
--

CREATE FUNCTION ${model}.air_duct_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from ${model}.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;

            -- Creation of HY singularity if type = routing_hydrology_link or type = connector_hydrology_link
            if 'air_duct' = 'routing_hydrology' or 'air_duct' = 'connector_hydrology' then
                if down_type_ != 'manhole_hydrology' and not (select exists (select 1 from ${model}.hydrograph_bc_singularity where node=down_)) then
                    insert into ${model}.hydrograph_bc_singularity(geom) select geom from ${model}._node where id=down_;
                end if;
            end if;

            insert into ${model}._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('air_duct', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update ${model}._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'air_duct') where name = 'define_later' and id = id_;
            insert into ${model}._air_duct_link(id, link_type, comment, z_invert_up, z_invert_down, area, perimeter, friction_coefficient)
                values (id_, 'air_duct', new.comment, new.z_invert_up, new.z_invert_down, new.area, new.perimeter, coalesce(new.friction_coefficient, .008));
            if 'air_duct' = 'pipe' and (select trigger_branch from ${model}.metadata) then
                perform ${model}.branch_update_fct();
            end if;
            --perform ${model}.set_link_altitude(id_);
            perform ${model}.add_configuration_fct(new.configuration::json, id_, 'air_duct_link');
            update ${model}._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (area > 0) and (perimeter > 0) and (friction_coefficient > 0) from  ${model}._air_duct_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'air_duct' = 'pipe' then
                update ${model}.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.comment, new.z_invert_up, new.z_invert_down, new.area, new.perimeter, new.friction_coefficient) is distinct from (old.comment, old.z_invert_up, old.z_invert_down, old.area, old.perimeter, old.friction_coefficient)) then
                select is_switching from ${model}.config_switch into switching;
                if switching=false then
                    select name from ${model}.configuration as c, ${model}.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.comment, old.z_invert_up, old.z_invert_down, old.area, old.perimeter, old.friction_coefficient) as o, (select new.comment, new.z_invert_up, new.z_invert_down, new.area, new.perimeter, new.friction_coefficient, new.custom_length) as n into new_config;
                        update ${model}._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.comment, new.z_invert_up, new.z_invert_down, new.area, new.perimeter, new.friction_coefficient) n into new_config;
                        update ${model}._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from ${model}.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update ${model}._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update ${model}._air_duct_link set comment=new.comment, z_invert_up=new.z_invert_up, z_invert_down=new.z_invert_down, area=new.area, perimeter=new.perimeter, friction_coefficient=new.friction_coefficient where id=old.id;

            if 'air_duct' = 'pipe' and (select trigger_branch from ${model}.metadata) then
                if not ST_Equals(old.geom, new.geom) or (new.exclude_from_branch != old.exclude_from_branch) then
                    perform ${model}.branch_update_fct();
                end if;
            end if;
            perform ${model}.add_configuration_fct(new.configuration::json, old.id, 'air_duct_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'air_duct' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update ${model}.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update ${model}._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (area > 0) and (perimeter > 0) and (friction_coefficient > 0) from  ${model}._air_duct_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from ${model}._air_duct_link where id=old.id;
            delete from ${model}._link where id=old.id;
            if 'air_duct' = 'pipe' and (select trigger_branch from ${model}.metadata) then
                perform ${model}.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'air_duct' = 'pipe' then
                update ${model}.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;

--
-- Name: air_flow_bc_singularity_fct(); Type: FUNCTION; Schema: ${model};
--

CREATE FUNCTION ${model}.air_flow_bc_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',${srid});
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from ${model}._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from ${model}._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from ${model}._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into ${model}._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'air_flow_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update ${model}._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'air_flow_bc') where name = 'define_later' and id = id_;

            insert into ${model}._air_flow_bc_singularity(id, singularity_type, flow, is_inwards)
                values (id_, 'air_flow_bc', coalesce(new.flow, 0), coalesce(new.is_inwards, false));
            if 'air_flow_bc' = 'pipe_branch_marker' and (select trigger_branch from ${model}.metadata) then
                perform ${model}.branch_update_fct();
            end if;
            if 'air_flow_bc' = 'hydrograph_bc' then
                update ${model}.routing_hydrology_link set hydrograph=id_ where down=nid_;
                update ${model}.connector_hydrology_link set hydrograph=id_ where down=nid_;
            end if;
            perform ${model}.add_configuration_fct(new.configuration::json, id_, 'air_flow_bc_singularity');
            update ${model}._singularity set validity = (select (flow >= 0) and (is_inwards is not null) from  ${model}._air_flow_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.flow, new.is_inwards) is distinct from (old.flow, old.is_inwards)) then
                select is_switching from ${model}.config_switch into switching;
                if switching=false then
                    select name from ${model}.configuration as c, ${model}.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.flow, old.is_inwards) as o, (select new.flow, new.is_inwards) as n into new_config;
                        update ${model}._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.flow, new.is_inwards) n into new_config;
                        update ${model}._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from ${model}._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from ${model}._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from ${model}._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update ${model}._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update ${model}._air_flow_bc_singularity set flow=new.flow, is_inwards=new.is_inwards where id=old.id;
            if 'air_flow_bc' = 'pipe_branch_marker' and (select trigger_branch from ${model}.metadata) then
                perform ${model}.branch_update_fct();
            end if;
            if 'air_flow_bc' = 'hydrograph_bc' then
                update ${model}.routing_hydrology_link set hydrograph=null where down=old.node;
                update ${model}.connector_hydrology_link set hydrograph=null where down=old.node;
                update ${model}.routing_hydrology_link set hydrograph=new.id where down=new.node;
                update ${model}.connector_hydrology_link set hydrograph=new.id where down=new.node;
            end if;
            perform ${model}.add_configuration_fct(new.configuration::json, old.id, 'air_flow_bc_singularity');
            update ${model}._singularity set validity = (select (flow >= 0) and (is_inwards is not null) from  ${model}._air_flow_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            if 'air_flow_bc' = 'hydrograph_bc' then
                update ${model}.routing_hydrology_link set hydrograph=null where down=old.node;
                update ${model}.connector_hydrology_link set hydrograph=null where down=old.node;
            end if;
            delete from ${model}._air_flow_bc_singularity where id=old.id;
            delete from ${model}._singularity where id=old.id;
            if 'air_flow_bc' = 'pipe_branch_marker' and (select trigger_branch from ${model}.metadata) then
                perform ${model}.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$$$
;;

--
-- Name: air_headloss_link_fct(); Type: FUNCTION; Schema: ${model};
--

CREATE FUNCTION ${model}.air_headloss_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from ${model}.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;

            -- Creation of HY singularity if type = routing_hydrology_link or type = connector_hydrology_link
            if 'air_headloss' = 'routing_hydrology' or 'air_headloss' = 'connector_hydrology' then
                if down_type_ != 'manhole_hydrology' and not (select exists (select 1 from ${model}.hydrograph_bc_singularity where node=down_)) then
                    insert into ${model}.hydrograph_bc_singularity(geom) select geom from ${model}._node where id=down_;
                end if;
            end if;

            insert into ${model}._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('air_headloss', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update ${model}._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'air_headloss') where name = 'define_later' and id = id_;
            insert into ${model}._air_headloss_link(id, link_type, comment, area, up_to_down_headloss_coefficient, down_to_up_headloss_coefficient, custom_length)
                values (id_, 'air_headloss', new.comment, new.area, coalesce(new.up_to_down_headloss_coefficient, .008), coalesce(new.down_to_up_headloss_coefficient, .008), new.custom_length);
            if 'air_headloss' = 'pipe' and (select trigger_branch from ${model}.metadata) then
                perform ${model}.branch_update_fct();
            end if;
            --perform ${model}.set_link_altitude(id_);
            perform ${model}.add_configuration_fct(new.configuration::json, id_, 'air_headloss_link');
            update ${model}._link set validity = (select (area > 0) and (up_to_down_headloss_coefficient > 0) and (down_to_up_headloss_coefficient > 0) from  ${model}._air_headloss_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'air_headloss' = 'pipe' then
                update ${model}.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.comment, new.area, new.up_to_down_headloss_coefficient, new.down_to_up_headloss_coefficient, new.custom_length) is distinct from (old.comment, old.area, old.up_to_down_headloss_coefficient, old.down_to_up_headloss_coefficient, old.custom_length)) then
                select is_switching from ${model}.config_switch into switching;
                if switching=false then
                    select name from ${model}.configuration as c, ${model}.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.comment, old.area, old.up_to_down_headloss_coefficient, old.down_to_up_headloss_coefficient, old.custom_length) as o, (select new.comment, new.area, new.up_to_down_headloss_coefficient, new.down_to_up_headloss_coefficient, new.custom_length) as n into new_config;
                        update ${model}._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.comment, new.area, new.up_to_down_headloss_coefficient, new.down_to_up_headloss_coefficient, new.custom_length) n into new_config;
                        update ${model}._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from ${model}.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update ${model}._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update ${model}._air_headloss_link set comment=new.comment, area=new.area, up_to_down_headloss_coefficient=new.up_to_down_headloss_coefficient, down_to_up_headloss_coefficient=new.down_to_up_headloss_coefficient, custom_length=new.custom_length where id=old.id;

            if 'air_headloss' = 'pipe' and (select trigger_branch from ${model}.metadata) then
                if not ST_Equals(old.geom, new.geom) or (new.exclude_from_branch != old.exclude_from_branch) then
                    perform ${model}.branch_update_fct();
                end if;
            end if;
            perform ${model}.add_configuration_fct(new.configuration::json, old.id, 'air_headloss_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'air_headloss' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update ${model}.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update ${model}._link set validity = (select (area > 0) and (up_to_down_headloss_coefficient > 0) and (down_to_up_headloss_coefficient > 0) from  ${model}._air_headloss_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from ${model}._air_headloss_link where id=old.id;
            delete from ${model}._link where id=old.id;
            if 'air_headloss' = 'pipe' and (select trigger_branch from ${model}.metadata) then
                perform ${model}.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'air_headloss' = 'pipe' then
                update ${model}.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;

--
-- Name: air_pressure_bc_singularity_fct(); Type: FUNCTION; Schema: ${model};
--

CREATE FUNCTION ${model}.air_pressure_bc_singularity_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',${srid});
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from ${model}._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from ${model}._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from ${model}._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into ${model}._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'air_pressure_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update ${model}._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'air_pressure_bc') where name = 'define_later' and id = id_;

            insert into ${model}._air_pressure_bc_singularity(id, singularity_type, relative_pressure)
                values (id_, 'air_pressure_bc', coalesce(new.relative_pressure, 0));
            if 'air_pressure_bc' = 'pipe_branch_marker' and (select trigger_branch from ${model}.metadata) then
                perform ${model}.branch_update_fct();
            end if;
            if 'air_pressure_bc' = 'hydrograph_bc' then
                update ${model}.routing_hydrology_link set hydrograph=id_ where down=nid_;
                update ${model}.connector_hydrology_link set hydrograph=id_ where down=nid_;
            end if;
            perform ${model}.add_configuration_fct(new.configuration::json, id_, 'air_pressure_bc_singularity');
            update ${model}._singularity set validity = (select (relative_pressure is not null) from  ${model}._air_pressure_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.relative_pressure) is distinct from (old.relative_pressure)) then
                select is_switching from ${model}.config_switch into switching;
                if switching=false then
                    select name from ${model}.configuration as c, ${model}.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.relative_pressure) as o, (select new.relative_pressure) as n into new_config;
                        update ${model}._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.relative_pressure) n into new_config;
                        update ${model}._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from ${model}._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from ${model}._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from ${model}._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update ${model}._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update ${model}._air_pressure_bc_singularity set relative_pressure=new.relative_pressure where id=old.id;
            if 'air_pressure_bc' = 'pipe_branch_marker' and (select trigger_branch from ${model}.metadata) then
                perform ${model}.branch_update_fct();
            end if;
            if 'air_pressure_bc' = 'hydrograph_bc' then
                update ${model}.routing_hydrology_link set hydrograph=null where down=old.node;
                update ${model}.connector_hydrology_link set hydrograph=null where down=old.node;
                update ${model}.routing_hydrology_link set hydrograph=new.id where down=new.node;
                update ${model}.connector_hydrology_link set hydrograph=new.id where down=new.node;
            end if;
            perform ${model}.add_configuration_fct(new.configuration::json, old.id, 'air_pressure_bc_singularity');
            update ${model}._singularity set validity = (select (relative_pressure is not null) from  ${model}._air_pressure_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            if 'air_pressure_bc' = 'hydrograph_bc' then
                update ${model}.routing_hydrology_link set hydrograph=null where down=old.node;
                update ${model}.connector_hydrology_link set hydrograph=null where down=old.node;
            end if;
            delete from ${model}._air_pressure_bc_singularity where id=old.id;
            delete from ${model}._singularity where id=old.id;
            if 'air_pressure_bc' = 'pipe_branch_marker' and (select trigger_branch from ${model}.metadata) then
                perform ${model}.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$$$
;;

--
-- Name: jet_fan_link_fct(); Type: FUNCTION; Schema: ${model};
--

CREATE FUNCTION ${model}.jet_fan_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from ${model}.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;

            -- Creation of HY singularity if type = routing_hydrology_link or type = connector_hydrology_link
            if 'jet_fan' = 'routing_hydrology' or 'jet_fan' = 'connector_hydrology' then
                if down_type_ != 'manhole_hydrology' and not (select exists (select 1 from ${model}.hydrograph_bc_singularity where node=down_)) then
                    insert into ${model}.hydrograph_bc_singularity(geom) select geom from ${model}._node where id=down_;
                end if;
            end if;

            insert into ${model}._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('jet_fan', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update ${model}._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'jet_fan') where name = 'define_later' and id = id_;
            insert into ${model}._jet_fan_link(id, link_type, comment, unit_thrust_newton, number_of_units, efficiency, flow_velocity, pipe_area, is_up_to_down)
                values (id_, 'jet_fan', new.comment, coalesce(new.unit_thrust_newton, 1), coalesce(new.number_of_units, 1), coalesce(new.efficiency, .08), coalesce(new.flow_velocity, 30), new.pipe_area, coalesce(new.is_up_to_down, true));
            if 'jet_fan' = 'pipe' and (select trigger_branch from ${model}.metadata) then
                perform ${model}.branch_update_fct();
            end if;
            --perform ${model}.set_link_altitude(id_);
            perform ${model}.add_configuration_fct(new.configuration::json, id_, 'jet_fan_link');
            update ${model}._link set validity = (select (unit_thrust_newton > 0) and (number_of_units > 0) and (efficiency > 0) and (efficiency <= 1) and (flow_velocity > 0) and (pipe_area > 0) and (is_up_to_down is not null) from  ${model}._jet_fan_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'jet_fan' = 'pipe' then
                update ${model}.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.comment, new.unit_thrust_newton, new.number_of_units, new.efficiency, new.flow_velocity, new.pipe_area, new.is_up_to_down) is distinct from (old.comment, old.unit_thrust_newton, old.number_of_units, old.efficiency, old.flow_velocity, old.pipe_area, old.is_up_to_down)) then
                select is_switching from ${model}.config_switch into switching;
                if switching=false then
                    select name from ${model}.configuration as c, ${model}.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.comment, old.unit_thrust_newton, old.number_of_units, old.efficiency, old.flow_velocity, old.pipe_area, old.is_up_to_down) as o, (select new.comment, new.unit_thrust_newton, new.number_of_units, new.efficiency, new.flow_velocity, new.pipe_area, new.is_up_to_down) as n into new_config;
                        update ${model}._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.comment, new.unit_thrust_newton, new.number_of_units, new.efficiency, new.flow_velocity, new.pipe_area, new.is_up_to_down) n into new_config;
                        update ${model}._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from ${model}.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update ${model}._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update ${model}._jet_fan_link set comment=new.comment, unit_thrust_newton=new.unit_thrust_newton, number_of_units=new.number_of_units, efficiency=new.efficiency, flow_velocity=new.flow_velocity, pipe_area=new.pipe_area, is_up_to_down=new.is_up_to_down where id=old.id;

            if 'jet_fan' = 'pipe' and (select trigger_branch from ${model}.metadata) then
                if not ST_Equals(old.geom, new.geom) or (new.exclude_from_branch != old.exclude_from_branch) then
                    perform ${model}.branch_update_fct();
                end if;
            end if;
            perform ${model}.add_configuration_fct(new.configuration::json, old.id, 'jet_fan_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'jet_fan' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update ${model}.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update ${model}._link set validity = (select (unit_thrust_newton > 0) and (number_of_units > 0) and (efficiency > 0) and (efficiency <= 1) and (flow_velocity > 0) and (pipe_area > 0) and (is_up_to_down is not null) from  ${model}._jet_fan_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from ${model}._jet_fan_link where id=old.id;
            delete from ${model}._link where id=old.id;
            if 'jet_fan' = 'pipe' and (select trigger_branch from ${model}.metadata) then
                perform ${model}.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'jet_fan' = 'pipe' then
                update ${model}.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;

--
-- Name: ventilator_link_fct(); Type: FUNCTION; Schema: ${model};
--

CREATE FUNCTION ${model}.ventilator_link_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from ${model}.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;

            -- Creation of HY singularity if type = routing_hydrology_link or type = connector_hydrology_link
            if 'ventilator' = 'routing_hydrology' or 'ventilator' = 'connector_hydrology' then
                if down_type_ != 'manhole_hydrology' and not (select exists (select 1 from ${model}.hydrograph_bc_singularity where node=down_)) then
                    insert into ${model}.hydrograph_bc_singularity(geom) select geom from ${model}._node where id=down_;
                end if;
            end if;

            insert into ${model}._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('ventilator', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update ${model}._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'ventilator') where name = 'define_later' and id = id_;
            insert into ${model}._ventilator_link(id, link_type, comment, q_dp_array, is_up_to_down)
                values (id_, 'ventilator', new.comment, new.q_dp_array, coalesce(new.is_up_to_down, true));
            if 'ventilator' = 'pipe' and (select trigger_branch from ${model}.metadata) then
                perform ${model}.branch_update_fct();
            end if;
            --perform ${model}.set_link_altitude(id_);
            perform ${model}.add_configuration_fct(new.configuration::json, id_, 'ventilator_link');
            update ${model}._link set validity = (select (array_length(q_dp_array, 1) <= 10) and (array_length(q_dp_array, 2) <= 2) and (q_dp_array[1][2]=0) and (q_dp_array[array_length(q_dp_array, 1)][1]=0) and (is_up_to_down is not null) from  ${model}._ventilator_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'ventilator' = 'pipe' then
                update ${model}.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.comment, new.q_dp_array, new.is_up_to_down) is distinct from (old.comment, old.q_dp_array, old.is_up_to_down)) then
                select is_switching from ${model}.config_switch into switching;
                if switching=false then
                    select name from ${model}.configuration as c, ${model}.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.comment, old.q_dp_array, old.is_up_to_down) as o, (select new.comment, new.q_dp_array, new.is_up_to_down) as n into new_config;
                        update ${model}._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.comment, new.q_dp_array, new.is_up_to_down) n into new_config;
                        update ${model}._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from ${model}.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update ${model}._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update ${model}._ventilator_link set comment=new.comment, q_dp_array=new.q_dp_array, is_up_to_down=new.is_up_to_down where id=old.id;

            if 'ventilator' = 'pipe' and (select trigger_branch from ${model}.metadata) then
                if not ST_Equals(old.geom, new.geom) or (new.exclude_from_branch != old.exclude_from_branch) then
                    perform ${model}.branch_update_fct();
                end if;
            end if;
            perform ${model}.add_configuration_fct(new.configuration::json, old.id, 'ventilator_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'ventilator' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update ${model}.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update ${model}._link set validity = (select (array_length(q_dp_array, 1) <= 10) and (array_length(q_dp_array, 2) <= 2) and (q_dp_array[1][2]=0) and (q_dp_array[array_length(q_dp_array, 1)][1]=0) and (is_up_to_down is not null) from  ${model}._ventilator_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from ${model}._ventilator_link where id=old.id;
            delete from ${model}._link where id=old.id;
            if 'ventilator' = 'pipe' and (select trigger_branch from ${model}.metadata) then
                perform ${model}.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'ventilator' = 'pipe' then
                update ${model}.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;

--
-- Name: air_duct_link ${model}_air_duct_link_trig; Type: TRIGGER; Schema: ${model};
--

CREATE TRIGGER ${model}_air_duct_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON ${model}.air_duct_link FOR EACH ROW EXECUTE FUNCTION ${model}.air_duct_link_fct()
;;

--
-- Name: air_flow_bc_singularity ${model}_air_flow_bc_singularity_trig; Type: TRIGGER; Schema: ${model};
--

CREATE TRIGGER ${model}_air_flow_bc_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON ${model}.air_flow_bc_singularity FOR EACH ROW EXECUTE FUNCTION ${model}.air_flow_bc_singularity_fct()
;;

--
-- Name: air_headloss_link ${model}_air_headloss_link_trig; Type: TRIGGER; Schema: ${model};
--

CREATE TRIGGER ${model}_air_headloss_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON ${model}.air_headloss_link FOR EACH ROW EXECUTE FUNCTION ${model}.air_headloss_link_fct()
;;

--
-- Name: air_pressure_bc_singularity ${model}_air_pressure_bc_singularity_trig; Type: TRIGGER; Schema: ${model};
--

CREATE TRIGGER ${model}_air_pressure_bc_singularity_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON ${model}.air_pressure_bc_singularity FOR EACH ROW EXECUTE FUNCTION ${model}.air_pressure_bc_singularity_fct()
;;

--
-- Name: jet_fan_link ${model}_jet_fan_link_trig; Type: TRIGGER; Schema: ${model};
--

CREATE TRIGGER ${model}_jet_fan_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON ${model}.jet_fan_link FOR EACH ROW EXECUTE FUNCTION ${model}.jet_fan_link_fct()
;;

--
-- Name: ventilator_link ${model}_ventilator_link_trig; Type: TRIGGER; Schema: ${model};
--

CREATE TRIGGER ${model}_ventilator_link_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON ${model}.ventilator_link FOR EACH ROW EXECUTE FUNCTION ${model}.ventilator_link_fct()
;;

--
-- Name: node_invalidity_reason(integer); Type: FUNCTION; Schema: ${model};
--

CREATE FUNCTION ${model}.node_invalidity_reason(id_ integer) RETURNS character varying
    LANGUAGE plpgsql STABLE
    AS $$$$
        declare
            reason varchar;
        begin
            reason := '';
            if not (select area is not null from ${model}.crossroad_node as new where id=id_) then    reason := reason || '   area is not null   ';end if;

if not (select (area > 0) from ${model}.crossroad_node as new where id=id_) then    reason := reason || '   (area > 0)   ';end if;

if not (select z_ground is not null from ${model}.crossroad_node as new where id=id_) then    reason := reason || '   z_ground is not null   ';end if;

if not (select h is not null from ${model}.crossroad_node as new where id=id_) then    reason := reason || '   h is not null   ';end if;

if not (select h>=0 from ${model}.crossroad_node as new where id=id_) then    reason := reason || '   h>=0   ';end if;

if not (select area is not null from ${model}.manhole_hydrology_node as new where id=id_) then    reason := reason || '   area is not null   ';end if;

if not (select (area > 0) from ${model}.manhole_hydrology_node as new where id=id_) then    reason := reason || '   (area > 0)   ';end if;

if not (select z_ground is not null from ${model}.manhole_hydrology_node as new where id=id_) then    reason := reason || '   z_ground is not null   ';end if;

if not (select zs_array is not null  from ${model}.storage_node as new where id=id_) then    reason := reason || '   zs_array is not null    ';end if;

if not (select zini is not null from ${model}.storage_node as new where id=id_) then    reason := reason || '   zini is not null   ';end if;

if not (select array_length(zs_array, 1) <= 10  from ${model}.storage_node as new where id=id_) then    reason := reason || '   array_length(zs_array, 1) <= 10    ';end if;

if not (select array_length(zs_array, 1) >= 1 from ${model}.storage_node as new where id=id_) then    reason := reason || '   array_length(zs_array, 1) >= 1   ';end if;

if not (select array_length(zs_array, 2) = 2 from ${model}.storage_node as new where id=id_) then    reason := reason || '   array_length(zs_array, 2) = 2   ';end if;

if not (select contraction_coef is not null from ${model}.storage_node as new where id=id_) then    reason := reason || '   contraction_coef is not null   ';end if;

if not (select contraction_coef<=1 from ${model}.storage_node as new where id=id_) then    reason := reason || '   contraction_coef<=1   ';end if;

if not (select contraction_coef>=0 from ${model}.storage_node as new where id=id_) then    reason := reason || '   contraction_coef>=0   ';end if;

if not (select area_ha is not null from ${model}.catchment_node as new where id=id_) then    reason := reason || '   area_ha is not null   ';end if;

if not (select area_ha>0 from ${model}.catchment_node as new where id=id_) then    reason := reason || '   area_ha>0   ';end if;

if not (select rl is not null from ${model}.catchment_node as new where id=id_) then    reason := reason || '   rl is not null   ';end if;

if not (select rl>0 from ${model}.catchment_node as new where id=id_) then    reason := reason || '   rl>0   ';end if;

if not (select slope is not null from ${model}.catchment_node as new where id=id_) then    reason := reason || '   slope is not null   ';end if;

if not (select c_imp is not null from ${model}.catchment_node as new where id=id_) then    reason := reason || '   c_imp is not null   ';end if;

if not (select c_imp>=0 from ${model}.catchment_node as new where id=id_) then    reason := reason || '   c_imp>=0   ';end if;

if not (select c_imp<=1 from ${model}.catchment_node as new where id=id_) then    reason := reason || '   c_imp<=1   ';end if;

if not (select netflow_type is not null from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type is not null   ';end if;

if not (select netflow_type!='constant_runoff' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''constant_runoff'' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)   ';end if;

if not (select netflow_type!='horner' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''horner'' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)   ';end if;

if not (select netflow_type!='horner' or (horner_recharge_coef is not null and horner_recharge_coef>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''horner'' or (horner_recharge_coef is not null and horner_recharge_coef>=0)   ';end if;

if not (select netflow_type!='holtan' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''holtan'' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)   ';end if;

if not (select netflow_type!='holtan' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''holtan'' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)   ';end if;

if not (select netflow_type!='holtan' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''holtan'' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)   ';end if;

if not (select netflow_type!='scs' or (scs_j_mm is not null and scs_j_mm>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''scs'' or (scs_j_mm is not null and scs_j_mm>=0)   ';end if;

if not (select netflow_type!='scs' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''scs'' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)   ';end if;

if not (select netflow_type!='scs' or (scs_rfu_mm is not null and scs_rfu_mm>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''scs'' or (scs_rfu_mm is not null and scs_rfu_mm>=0)   ';end if;

if not (select netflow_type!='hydra' or (hydra_surface_soil_storage_rfu_mm is not null and hydra_surface_soil_storage_rfu_mm>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''hydra'' or (hydra_surface_soil_storage_rfu_mm is not null and hydra_surface_soil_storage_rfu_mm>=0)   ';end if;

if not (select netflow_type!='hydra' or (hydra_inf_rate_f0_mm_day is not null and hydra_inf_rate_f0_mm_day>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''hydra'' or (hydra_inf_rate_f0_mm_day is not null and hydra_inf_rate_f0_mm_day>=0)   ';end if;

if not (select netflow_type!='hydra' or (hydra_int_soil_storage_j_mm is not null and hydra_int_soil_storage_j_mm>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''hydra'' or (hydra_int_soil_storage_j_mm is not null and hydra_int_soil_storage_j_mm>=0)   ';end if;

if not (select netflow_type!='hydra' or (hydra_soil_drainage_time_qres_day is not null and hydra_soil_drainage_time_qres_day>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''hydra'' or (hydra_soil_drainage_time_qres_day is not null and hydra_soil_drainage_time_qres_day>=0)   ';end if;

if not (select netflow_type!='hydra_permeable' or (hydra_permeable_surface_soil_storage_rfu_mm is not null and hydra_permeable_surface_soil_storage_rfu_mm>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''hydra_permeable'' or (hydra_permeable_surface_soil_storage_rfu_mm is not null and hydra_permeable_surface_soil_storage_rfu_mm>=0)   ';end if;

if not (select netflow_type!='hydra_permeable' or (hydra_permeable_split_coefficient is not null and hydra_permeable_split_coefficient>=0 and hydra_permeable_split_coefficient<=1) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''hydra_permeable'' or (hydra_permeable_split_coefficient is not null and hydra_permeable_split_coefficient>=0 and hydra_permeable_split_coefficient<=1)   ';end if;

if not (select netflow_type!='hydra_permeable' or (hydra_permeable_soil_drainage_time_qres_day is not null and hydra_permeable_soil_drainage_time_qres_day>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''hydra_permeable'' or (hydra_permeable_soil_drainage_time_qres_day is not null and hydra_permeable_soil_drainage_time_qres_day>=0)   ';end if;

if not (select netflow_type!='hydra_permeable' or (hydra_permeable_catchment_connect_coef is not null and hydra_permeable_catchment_connect_coef>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''hydra_permeable'' or (hydra_permeable_catchment_connect_coef is not null and hydra_permeable_catchment_connect_coef>=0)   ';end if;

if not (select netflow_type!='hydra_permeable' or (hydra_permeable_catchment_connect_coef is not null and hydra_permeable_catchment_connect_coef<=1) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''hydra_permeable'' or (hydra_permeable_catchment_connect_coef is not null and hydra_permeable_catchment_connect_coef<=1)   ';end if;

if not (select netflow_type!='gr4' or (gr4_k1 is not null and gr4_k1>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''gr4'' or (gr4_k1 is not null and gr4_k1>=0)   ';end if;

if not (select netflow_type!='gr4' or (gr4_k2 is not null) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''gr4'' or (gr4_k2 is not null)   ';end if;

if not (select netflow_type!='gr4' or (gr4_k3 is not null and gr4_k3>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''gr4'' or (gr4_k3 is not null and gr4_k3>=0)   ';end if;

if not (select netflow_type!='gr4' or (gr4_k4 is not null and gr4_k4>=0) from ${model}.catchment_node as new where id=id_) then    reason := reason || '   netflow_type!=''gr4'' or (gr4_k4 is not null and gr4_k4>=0)   ';end if;

if not (select runoff_type is not null or netflow_type='gr4' from ${model}.catchment_node as new where id=id_) then    reason := reason || '   runoff_type is not null or netflow_type=''gr4''   ';end if;

if not (select runoff_type!='socose' or (socose_tc_mn is not null and socose_tc_mn>0) or netflow_type='gr4' from ${model}.catchment_node as new where id=id_) then    reason := reason || '   runoff_type!=''socose'' or (socose_tc_mn is not null and socose_tc_mn>0) or netflow_type=''gr4''   ';end if;

if not (select runoff_type!='socose' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6) or netflow_type='gr4' from ${model}.catchment_node as new where id=id_) then    reason := reason || '   runoff_type!=''socose'' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6) or netflow_type=''gr4''   ';end if;

if not (select runoff_type!='define_k' or (define_k_mn is not null and define_k_mn>0) or netflow_type='gr4' from ${model}.catchment_node as new where id=id_) then    reason := reason || '   runoff_type!=''define_k'' or (define_k_mn is not null and define_k_mn>0) or netflow_type=''gr4''   ';end if;

if not (select q_limit is not null from ${model}.catchment_node as new where id=id_) then    reason := reason || '   q_limit is not null   ';end if;

if not (select q0 is not null from ${model}.catchment_node as new where id=id_) then    reason := reason || '   q0 is not null   ';end if;

if not (select network_type is not null from ${model}.catchment_node as new where id=id_) then    reason := reason || '   network_type is not null   ';end if;

if not (select rural_land_use is null or rural_land_use>=0 from ${model}.catchment_node as new where id=id_) then    reason := reason || '   rural_land_use is null or rural_land_use>=0   ';end if;

if not (select industrial_land_use is null or industrial_land_use>=0 from ${model}.catchment_node as new where id=id_) then    reason := reason || '   industrial_land_use is null or industrial_land_use>=0   ';end if;

if not (select suburban_housing_land_use is null or suburban_housing_land_use>=0 from ${model}.catchment_node as new where id=id_) then    reason := reason || '   suburban_housing_land_use is null or suburban_housing_land_use>=0   ';end if;

if not (select dense_housing_land_use is null or dense_housing_land_use>=0 from ${model}.catchment_node as new where id=id_) then    reason := reason || '   dense_housing_land_use is null or dense_housing_land_use>=0   ';end if;

if not (select area is not null from ${model}.station_node as new where id=id_) then    reason := reason || '   area is not null   ';end if;

if not (select area>0 from ${model}.station_node as new where id=id_) then    reason := reason || '   area>0   ';end if;

if not (select z_invert is not null from ${model}.station_node as new where id=id_) then    reason := reason || '   z_invert is not null   ';end if;

if not (select station is not null  from ${model}.station_node as new where id=id_) then    reason := reason || '   station is not null    ';end if;

if not (select (select st_intersects(new.geom, geom) from ${model}.station where id=new.station) from ${model}.station_node as new where id=id_) then    reason := reason || '   (select st_intersects(new.geom, geom) from ${model}.station where id=new.station)   ';end if;

if not (select connection_law is not null from ${model}.manhole_node as new where id=id_) then    reason := reason || '   connection_law is not null   ';end if;

if not (select connection_law != 'manhole_cover' or cover_diameter is not null from ${model}.manhole_node as new where id=id_) then    reason := reason || '   connection_law != ''manhole_cover'' or cover_diameter is not null   ';end if;

if not (select connection_law != 'manhole_cover' or cover_diameter > 0 from ${model}.manhole_node as new where id=id_) then    reason := reason || '   connection_law != ''manhole_cover'' or cover_diameter > 0   ';end if;

if not (select connection_law != 'manhole_cover' or cover_critical_pressure is not null from ${model}.manhole_node as new where id=id_) then    reason := reason || '   connection_law != ''manhole_cover'' or cover_critical_pressure is not null   ';end if;

if not (select connection_law != 'manhole_cover' or cover_critical_pressure >0 from ${model}.manhole_node as new where id=id_) then    reason := reason || '   connection_law != ''manhole_cover'' or cover_critical_pressure >0   ';end if;

if not (select connection_law != 'drainage_inlet' or inlet_width is not null from ${model}.manhole_node as new where id=id_) then    reason := reason || '   connection_law != ''drainage_inlet'' or inlet_width is not null   ';end if;

if not (select connection_law != 'drainage_inlet' or inlet_width >0 from ${model}.manhole_node as new where id=id_) then    reason := reason || '   connection_law != ''drainage_inlet'' or inlet_width >0   ';end if;

if not (select connection_law != 'drainage_inlet' or inlet_height is not null from ${model}.manhole_node as new where id=id_) then    reason := reason || '   connection_law != ''drainage_inlet'' or inlet_height is not null   ';end if;

if not (select connection_law != 'drainage_inlet' or inlet_height >0 from ${model}.manhole_node as new where id=id_) then    reason := reason || '   connection_law != ''drainage_inlet'' or inlet_height >0   ';end if;

if not (select area is not null from ${model}.manhole_node as new where id=id_) then    reason := reason || '   area is not null   ';end if;

if not (select area>0 from ${model}.manhole_node as new where id=id_) then    reason := reason || '   area>0   ';end if;

if not (select z_ground is not null from ${model}.manhole_node as new where id=id_) then    reason := reason || '   z_ground is not null   ';end if;

if not (select (select not exists(select 1 from ${model}.station where st_intersects(geom, new.geom))) from ${model}.manhole_node as new where id=id_) then    reason := reason || '   (select not exists(select 1 from ${model}.station where st_intersects(geom, new.geom)))   ';end if;

if not (select (select exists (select 1 from ${model}.pipe_link where up=new.id or down=new.id)) from ${model}.manhole_node as new where id=id_) then    reason := reason || '   (select exists (select 1 from ${model}.pipe_link where up=new.id or down=new.id))   ';end if;

if not (select area is not null from ${model}.elem_2d_node as new where id=id_) then    reason := reason || '   area is not null   ';end if;

if not (select area>0 from ${model}.elem_2d_node as new where id=id_) then    reason := reason || '   area>0   ';end if;

if not (select zb is not null from ${model}.elem_2d_node as new where id=id_) then    reason := reason || '   zb is not null   ';end if;

if not (select rk is not null from ${model}.elem_2d_node as new where id=id_) then    reason := reason || '   rk is not null   ';end if;

if not (select rk>0 from ${model}.elem_2d_node as new where id=id_) then    reason := reason || '   rk>0   ';end if;

if not (select z_ground is not null from ${model}.river_node as new where id=id_) then    reason := reason || '   z_ground is not null   ';end if;

if not (select area is not null from ${model}.river_node as new where id=id_) then    reason := reason || '   area is not null   ';end if;

if not (select area>0 from ${model}.river_node as new where id=id_) then    reason := reason || '   area>0   ';end if;

if not (select reach is not null from ${model}.river_node as new where id=id_) then    reason := reason || '   reach is not null   ';end if;

if not (select (select not exists(select 1 from ${model}.station where st_intersects(geom, new.geom))) from ${model}.river_node as new where id=id_) then    reason := reason || '   (select not exists(select 1 from ${model}.station where st_intersects(geom, new.geom)))   ';end if;
            return reason;
        end;
        $$$$
;;


--
-- Name: link_invalidity_reason(integer); Type: FUNCTION; Schema: ${model};
--

CREATE FUNCTION ${model}.link_invalidity_reason(id_ integer) RETURNS character varying
    LANGUAGE plpgsql STABLE
    AS $$$$
        declare
            reason varchar;
        begin
            reason := '';
            if not (select z_invert_up is not null from ${model}.air_duct_link as new where id=id_) then    reason := reason || '   z_invert_up is not null   ';end if;

if not (select z_invert_down is not null from ${model}.air_duct_link as new where id=id_) then    reason := reason || '   z_invert_down is not null   ';end if;

if not (select area > 0 from ${model}.air_duct_link as new where id=id_) then    reason := reason || '   area > 0   ';end if;

if not (select perimeter > 0 from ${model}.air_duct_link as new where id=id_) then    reason := reason || '   perimeter > 0   ';end if;

if not (select friction_coefficient > 0 from ${model}.air_duct_link as new where id=id_) then    reason := reason || '   friction_coefficient > 0   ';end if;

if not (select up is not null from ${model}.air_duct_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.air_duct_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.air_duct_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.air_duct_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select area > 0 from ${model}.air_headloss_link as new where id=id_) then    reason := reason || '   area > 0   ';end if;

if not (select up_to_down_headloss_coefficient > 0 from ${model}.air_headloss_link as new where id=id_) then    reason := reason || '   up_to_down_headloss_coefficient > 0   ';end if;

if not (select down_to_up_headloss_coefficient > 0 from ${model}.air_headloss_link as new where id=id_) then    reason := reason || '   down_to_up_headloss_coefficient > 0   ';end if;

if not (select up is not null from ${model}.air_headloss_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.air_headloss_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.air_headloss_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.air_headloss_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select array_length(q_dp_array, 1) <= 10 from ${model}.ventilator_link as new where id=id_) then    reason := reason || '   array_length(q_dp_array, 1) <= 10   ';end if;

if not (select array_length(q_dp_array, 2) <= 2 from ${model}.ventilator_link as new where id=id_) then    reason := reason || '   array_length(q_dp_array, 2) <= 2   ';end if;

if not (select q_dp_array[1][2]=0 from ${model}.ventilator_link as new where id=id_) then    reason := reason || '   q_dp_array[1][2]=0   ';end if;

if not (select q_dp_array[array_length(q_dp_array, 1)][1]=0 from ${model}.ventilator_link as new where id=id_) then    reason := reason || '   q_dp_array[array_length(q_dp_array, 1)][1]=0   ';end if;

if not (select is_up_to_down is not null from ${model}.ventilator_link as new where id=id_) then    reason := reason || '   is_up_to_down is not null   ';end if;

if not (select up is not null from ${model}.ventilator_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.ventilator_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.ventilator_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.ventilator_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select unit_thrust_newton > 0 from ${model}.jet_fan_link as new where id=id_) then    reason := reason || '   unit_thrust_newton > 0   ';end if;

if not (select number_of_units > 0 from ${model}.jet_fan_link as new where id=id_) then    reason := reason || '   number_of_units > 0   ';end if;

if not (select efficiency > 0 from ${model}.jet_fan_link as new where id=id_) then    reason := reason || '   efficiency > 0   ';end if;

if not (select efficiency <= 1 from ${model}.jet_fan_link as new where id=id_) then    reason := reason || '   efficiency <= 1   ';end if;

if not (select flow_velocity > 0 from ${model}.jet_fan_link as new where id=id_) then    reason := reason || '   flow_velocity > 0   ';end if;

if not (select pipe_area > 0 from ${model}.jet_fan_link as new where id=id_) then    reason := reason || '   pipe_area > 0   ';end if;

if not (select is_up_to_down is not null from ${model}.jet_fan_link as new where id=id_) then    reason := reason || '   is_up_to_down is not null   ';end if;

if not (select up is not null from ${model}.jet_fan_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.jet_fan_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.jet_fan_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.jet_fan_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select z_invert is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   z_invert is not null   ';end if;

if not (select z_ceiling is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   z_ceiling is not null   ';end if;

if not (select width is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   width is not null   ';end if;

if not (select width>=0 from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   width>=0   ';end if;

if not (select cc is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   cc is not null   ';end if;

if not (select cc<=1 from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   cc<=1   ';end if;

if not (select cc>=0 from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   cc>=0   ';end if;

if not (select action_gate_type is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   action_gate_type is not null   ';end if;

if not (select z_invert_stop is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   z_invert_stop is not null   ';end if;

if not (select z_ceiling_stop is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   z_ceiling_stop is not null   ';end if;

if not (select v_max_cms is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   v_max_cms is not null   ';end if;

if not (select v_max_cms>=0 from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   v_max_cms>=0   ';end if;

if not (select dt_regul_hr is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   dt_regul_hr is not null   ';end if;

if not (select mode_regul!='elevation' or z_control_node is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   mode_regul!=''elevation'' or z_control_node is not null   ';end if;

if not (select mode_regul!='elevation' or z_pid_array is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   mode_regul!=''elevation'' or z_pid_array is not null   ';end if;

if not (select mode_regul!='elevation' or z_tz_array is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   mode_regul!=''elevation'' or z_tz_array is not null   ';end if;

if not (select mode_regul!='elevation' or array_length(z_tz_array, 1)<=10 from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   ';end if;

if not (select mode_regul!='elevation' or array_length(z_tz_array, 1)>=1 from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   ';end if;

if not (select mode_regul!='elevation' or array_length(z_tz_array, 2)=2 from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   ';end if;

if not (select mode_regul!='discharge' or q_z_crit is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   mode_regul!=''discharge'' or q_z_crit is not null   ';end if;

if not (select mode_regul!='discharge' or array_length(q_tq_array, 1)<=10 from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   ';end if;

if not (select mode_regul!='discharge' or array_length(q_tq_array, 1)>=1 from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   ';end if;

if not (select mode_regul!='discharge' or array_length(q_tq_array, 2)=2 from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   ';end if;

if not (select mode_regul!='no_regulation' or nr_z_gate is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   mode_regul!=''no_regulation'' or nr_z_gate is not null   ';end if;

if not (select cc_submerged >0 from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   cc_submerged >0   ';end if;

if not (select up is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.regul_gate_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select z_invert is not null from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   z_invert is not null   ';end if;

if not (select width is not null from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   width is not null   ';end if;

if not (select width>=0. from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   width>=0.   ';end if;

if not (select cc is not null from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   cc is not null   ';end if;

if not (select cc<=1 from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   cc<=1   ';end if;

if not (select cc>=0. from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   cc>=0.   ';end if;

if not (select break_mode is not null from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   break_mode is not null   ';end if;

if not (select break_mode!='zw_critical' or z_break is not null from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   break_mode!=''zw_critical'' or z_break is not null   ';end if;

if not (select break_mode!='time_critical' or t_break is not null from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   break_mode!=''time_critical'' or t_break is not null   ';end if;

if not (select break_mode='none' or grp is not null from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   break_mode=''none'' or grp is not null   ';end if;

if not (select break_mode='none' or grp>0 from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   break_mode=''none'' or grp>0   ';end if;

if not (select break_mode='none' or grp<100 from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   break_mode=''none'' or grp<100   ';end if;

if not (select break_mode='none' or dt_fracw_array is not null from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   break_mode=''none'' or dt_fracw_array is not null   ';end if;

if not (select break_mode='none' or array_length(dt_fracw_array, 1)<=10 from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   ';end if;

if not (select break_mode='none' or array_length(dt_fracw_array, 1)>=1 from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   ';end if;

if not (select break_mode='none' or array_length(dt_fracw_array, 2)=2 from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   ';end if;

if not (select up is not null from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.fuse_spillway_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select z_invert_up is not null from ${model}.pipe_link as new where id=id_) then    reason := reason || '   z_invert_up is not null   ';end if;

if not (select z_invert_down is not null from ${model}.pipe_link as new where id=id_) then    reason := reason || '   z_invert_down is not null   ';end if;

if not (select h_sable is not null from ${model}.pipe_link as new where id=id_) then    reason := reason || '   h_sable is not null   ';end if;

if not (select cross_section_type is not null from ${model}.pipe_link as new where id=id_) then    reason := reason || '   cross_section_type is not null   ';end if;

if not (select cross_section_type not in ('valley') from ${model}.pipe_link as new where id=id_) then    reason := reason || '   cross_section_type not in (''valley'')   ';end if;

if not (select rk is not null from ${model}.pipe_link as new where id=id_) then    reason := reason || '   rk is not null   ';end if;

if not (select rk >0 from ${model}.pipe_link as new where id=id_) then    reason := reason || '   rk >0   ';end if;

if not (select cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0) from ${model}.pipe_link as new where id=id_) then    reason := reason || '   cross_section_type!=''circular'' or (circular_diameter is not null and circular_diameter > 0)   ';end if;

if not (select cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0) from ${model}.pipe_link as new where id=id_) then    reason := reason || '   cross_section_type!=''ovoid'' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)   ';end if;

if not (select cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0) from ${model}.pipe_link as new where id=id_) then    reason := reason || '   cross_section_type!=''ovoid'' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)   ';end if;

if not (select cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 ) from ${model}.pipe_link as new where id=id_) then    reason := reason || '   cross_section_type!=''ovoid'' or (ovoid_height is not null and ovoid_height >0 )   ';end if;

if not (select cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2) from ${model}.pipe_link as new where id=id_) then    reason := reason || '   cross_section_type!=''ovoid'' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)   ';end if;

if not (select cross_section_type!='pipe' or cp_geom is not null from ${model}.pipe_link as new where id=id_) then    reason := reason || '   cross_section_type!=''pipe'' or cp_geom is not null   ';end if;

if not (select cross_section_type!='channel' or op_geom is not null from ${model}.pipe_link as new where id=id_) then    reason := reason || '   cross_section_type!=''channel'' or op_geom is not null   ';end if;

if not (select up is not null from ${model}.pipe_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.pipe_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.pipe_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.pipe_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select cross_section is not null from ${model}.routing_hydrology_link as new where id=id_) then    reason := reason || '   cross_section is not null   ';end if;

if not (select cross_section>=0 from ${model}.routing_hydrology_link as new where id=id_) then    reason := reason || '   cross_section>=0   ';end if;

if not (select length is not null from ${model}.routing_hydrology_link as new where id=id_) then    reason := reason || '   length is not null   ';end if;

if not (select length>=0 from ${model}.routing_hydrology_link as new where id=id_) then    reason := reason || '   length>=0   ';end if;

if not (select slope is not null from ${model}.routing_hydrology_link as new where id=id_) then    reason := reason || '   slope is not null   ';end if;

if not (select down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station', 'crossroad', 'storage', 'elem_2d') and (hydrograph is not null)) from ${model}.routing_hydrology_link as new where id=id_) then    reason := reason || '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'', ''crossroad'', ''storage'', ''elem_2d'') and (hydrograph is not null))   ';end if;

if not (select split_coef is not null from ${model}.routing_hydrology_link as new where id=id_) then    reason := reason || '   split_coef is not null   ';end if;

if not (select split_coef>=0 from ${model}.routing_hydrology_link as new where id=id_) then    reason := reason || '   split_coef>=0   ';end if;

if not (select split_coef<=1 from ${model}.routing_hydrology_link as new where id=id_) then    reason := reason || '   split_coef<=1   ';end if;

if not (select up is not null from ${model}.routing_hydrology_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.routing_hydrology_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.routing_hydrology_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.routing_hydrology_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select q_pump is not null from ${model}.deriv_pump_link as new where id=id_) then    reason := reason || '   q_pump is not null   ';end if;

if not (select q_pump>= 0 from ${model}.deriv_pump_link as new where id=id_) then    reason := reason || '   q_pump>= 0   ';end if;

if not (select qz_array is not null from ${model}.deriv_pump_link as new where id=id_) then    reason := reason || '   qz_array is not null   ';end if;

if not (select array_length(qz_array, 1)<=10 from ${model}.deriv_pump_link as new where id=id_) then    reason := reason || '   array_length(qz_array, 1)<=10   ';end if;

if not (select array_length(qz_array, 1)>=1 from ${model}.deriv_pump_link as new where id=id_) then    reason := reason || '   array_length(qz_array, 1)>=1   ';end if;

if not (select array_length(qz_array, 2)=2 from ${model}.deriv_pump_link as new where id=id_) then    reason := reason || '   array_length(qz_array, 2)=2   ';end if;

if not (select (/*   qz_array: 0<=q   */ select bool_and(bool) from (select 0<=unnest(new.qz_array[:][1]) as bool) as data_set) from ${model}.deriv_pump_link as new where id=id_) then    reason := reason || '   (/*   qz_array: 0<=q   */ select bool_and(bool) from (select 0<=unnest(new.qz_array[:][1]) as bool) as data_set)   ';end if;

if not (select (/*   qz_array: q<=1   */ select bool_and(bool) from (select unnest(new.qz_array[:][1])<=1 as bool) as data_set) from ${model}.deriv_pump_link as new where id=id_) then    reason := reason || '   (/*   qz_array: q<=1   */ select bool_and(bool) from (select unnest(new.qz_array[:][1])<=1 as bool) as data_set)   ';end if;

if not (select up is not null from ${model}.deriv_pump_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.deriv_pump_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.deriv_pump_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.deriv_pump_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select z_invert is not null from ${model}.porous_link as new where id=id_) then    reason := reason || '   z_invert is not null   ';end if;

if not (select width is not null from ${model}.porous_link as new where id=id_) then    reason := reason || '   width is not null   ';end if;

if not (select width>=0 from ${model}.porous_link as new where id=id_) then    reason := reason || '   width>=0   ';end if;

if not (select transmitivity is not null from ${model}.porous_link as new where id=id_) then    reason := reason || '   transmitivity is not null   ';end if;

if not (select transmitivity>=0 from ${model}.porous_link as new where id=id_) then    reason := reason || '   transmitivity>=0   ';end if;

if not (select up is not null from ${model}.porous_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.porous_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.porous_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.porous_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select z_invert is not null  from ${model}.weir_link as new where id=id_) then    reason := reason || '   z_invert is not null    ';end if;

if not (select width is not null from ${model}.weir_link as new where id=id_) then    reason := reason || '   width is not null   ';end if;

if not (select width>=0 from ${model}.weir_link as new where id=id_) then    reason := reason || '   width>=0   ';end if;

if not (select cc is not null from ${model}.weir_link as new where id=id_) then    reason := reason || '   cc is not null   ';end if;

if not (select cc<=1 from ${model}.weir_link as new where id=id_) then    reason := reason || '   cc<=1   ';end if;

if not (select cc>=0 from ${model}.weir_link as new where id=id_) then    reason := reason || '   cc>=0   ';end if;

if not (select v_max_cms is not null from ${model}.weir_link as new where id=id_) then    reason := reason || '   v_max_cms is not null   ';end if;

if not (select v_max_cms>=0 from ${model}.weir_link as new where id=id_) then    reason := reason || '   v_max_cms>=0   ';end if;

if not (select up is not null from ${model}.weir_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.weir_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.weir_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.weir_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select law_type is not null from ${model}.borda_headloss_link as new where id=id_) then    reason := reason || '   law_type is not null   ';end if;

if not (select param is not null from ${model}.borda_headloss_link as new where id=id_) then    reason := reason || '   param is not null   ';end if;

if not (select up is not null from ${model}.borda_headloss_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.borda_headloss_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.borda_headloss_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.borda_headloss_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select npump is not null from ${model}.pump_link as new where id=id_) then    reason := reason || '   npump is not null   ';end if;

if not (select npump<=10 from ${model}.pump_link as new where id=id_) then    reason := reason || '   npump<=10   ';end if;

if not (select npump>=1 from ${model}.pump_link as new where id=id_) then    reason := reason || '   npump>=1   ';end if;

if not (select zregul_array is not null from ${model}.pump_link as new where id=id_) then    reason := reason || '   zregul_array is not null   ';end if;

if not (select hq_array is not null from ${model}.pump_link as new where id=id_) then    reason := reason || '   hq_array is not null   ';end if;

if not (select array_length(zregul_array, 1)=npump from ${model}.pump_link as new where id=id_) then    reason := reason || '   array_length(zregul_array, 1)=npump   ';end if;

if not (select array_length(zregul_array, 2)=2 from ${model}.pump_link as new where id=id_) then    reason := reason || '   array_length(zregul_array, 2)=2   ';end if;

if not (select array_length(hq_array, 1)=npump from ${model}.pump_link as new where id=id_) then    reason := reason || '   array_length(hq_array, 1)=npump   ';end if;

if not (select array_length(hq_array, 2)<=10 from ${model}.pump_link as new where id=id_) then    reason := reason || '   array_length(hq_array, 2)<=10   ';end if;

if not (select array_length(hq_array, 2)>=1 from ${model}.pump_link as new where id=id_) then    reason := reason || '   array_length(hq_array, 2)>=1   ';end if;

if not (select array_length(hq_array, 3)=2 from ${model}.pump_link as new where id=id_) then    reason := reason || '   array_length(hq_array, 3)=2   ';end if;

if not (select up is not null from ${model}.pump_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.pump_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.pump_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.pump_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select z_invert is not null from ${model}.mesh_2d_link as new where id=id_) then    reason := reason || '   z_invert is not null   ';end if;

if not (select lateral_contraction_coef is not null from ${model}.mesh_2d_link as new where id=id_) then    reason := reason || '   lateral_contraction_coef is not null   ';end if;

if not (select lateral_contraction_coef<=1 from ${model}.mesh_2d_link as new where id=id_) then    reason := reason || '   lateral_contraction_coef<=1   ';end if;

if not (select lateral_contraction_coef>=0 from ${model}.mesh_2d_link as new where id=id_) then    reason := reason || '   lateral_contraction_coef>=0   ';end if;

if not (select ST_NPoints(border)=2 from ${model}.mesh_2d_link as new where id=id_) then    reason := reason || '   ST_NPoints(border)=2   ';end if;

if not (select ST_IsValid(border) from ${model}.mesh_2d_link as new where id=id_) then    reason := reason || '   ST_IsValid(border)   ';end if;

if not (select up is not null from ${model}.mesh_2d_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.mesh_2d_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.mesh_2d_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.mesh_2d_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select z_crest1 is not null from ${model}.overflow_link as new where id=id_) then    reason := reason || '   z_crest1 is not null   ';end if;

if not (select width1 is not null from ${model}.overflow_link as new where id=id_) then    reason := reason || '   width1 is not null   ';end if;

if not (select width1>=0 from ${model}.overflow_link as new where id=id_) then    reason := reason || '   width1>=0   ';end if;

if not (select z_crest2 is not null from ${model}.overflow_link as new where id=id_) then    reason := reason || '   z_crest2 is not null   ';end if;

if not (select z_crest2>=z_crest1 from ${model}.overflow_link as new where id=id_) then    reason := reason || '   z_crest2>=z_crest1   ';end if;

if not (select width2 is not null from ${model}.overflow_link as new where id=id_) then    reason := reason || '   width2 is not null   ';end if;

if not (select width2>=0 from ${model}.overflow_link as new where id=id_) then    reason := reason || '   width2>=0   ';end if;

if not (select cc is not null from ${model}.overflow_link as new where id=id_) then    reason := reason || '   cc is not null   ';end if;

if not (select cc<=1 from ${model}.overflow_link as new where id=id_) then    reason := reason || '   cc<=1   ';end if;

if not (select cc>=0 from ${model}.overflow_link as new where id=id_) then    reason := reason || '   cc>=0   ';end if;

if not (select lateral_contraction_coef is not null from ${model}.overflow_link as new where id=id_) then    reason := reason || '   lateral_contraction_coef is not null   ';end if;

if not (select lateral_contraction_coef<=1 from ${model}.overflow_link as new where id=id_) then    reason := reason || '   lateral_contraction_coef<=1   ';end if;

if not (select lateral_contraction_coef>=0 from ${model}.overflow_link as new where id=id_) then    reason := reason || '   lateral_contraction_coef>=0   ';end if;

if not (select break_mode is not null from ${model}.overflow_link as new where id=id_) then    reason := reason || '   break_mode is not null   ';end if;

if not (select break_mode!='zw_critical' or z_break is not null from ${model}.overflow_link as new where id=id_) then    reason := reason || '   break_mode!=''zw_critical'' or z_break is not null   ';end if;

if not (select break_mode!='time_critical' or t_break is not null from ${model}.overflow_link as new where id=id_) then    reason := reason || '   break_mode!=''time_critical'' or t_break is not null   ';end if;

if not (select break_mode='none' or width_breach is not null from ${model}.overflow_link as new where id=id_) then    reason := reason || '   break_mode=''none'' or width_breach is not null   ';end if;

if not (select break_mode='none' or z_invert is not null from ${model}.overflow_link as new where id=id_) then    reason := reason || '   break_mode=''none'' or z_invert is not null   ';end if;

if not (select break_mode='none' or grp is not null from ${model}.overflow_link as new where id=id_) then    reason := reason || '   break_mode=''none'' or grp is not null   ';end if;

if not (select break_mode='none' or grp>0 from ${model}.overflow_link as new where id=id_) then    reason := reason || '   break_mode=''none'' or grp>0   ';end if;

if not (select break_mode='none' or grp<100 from ${model}.overflow_link as new where id=id_) then    reason := reason || '   break_mode=''none'' or grp<100   ';end if;

if not (select break_mode='none' or dt_fracw_array is not null from ${model}.overflow_link as new where id=id_) then    reason := reason || '   break_mode=''none'' or dt_fracw_array is not null   ';end if;

if not (select break_mode='none' or array_length(dt_fracw_array, 1)<=10 from ${model}.overflow_link as new where id=id_) then    reason := reason || '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   ';end if;

if not (select break_mode='none' or array_length(dt_fracw_array, 1)>=1 from ${model}.overflow_link as new where id=id_) then    reason := reason || '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   ';end if;

if not (select break_mode='none' or array_length(dt_fracw_array, 2)=2 from ${model}.overflow_link as new where id=id_) then    reason := reason || '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   ';end if;

if not (select up is not null from ${model}.overflow_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.overflow_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.overflow_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.overflow_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select z_overflow is not null from ${model}.network_overflow_link as new where id=id_) then    reason := reason || '   z_overflow is not null   ';end if;

if not (select area is not null from ${model}.network_overflow_link as new where id=id_) then    reason := reason || '   area is not null   ';end if;

if not (select area>=0 from ${model}.network_overflow_link as new where id=id_) then    reason := reason || '   area>=0   ';end if;

if not (select up is not null from ${model}.network_overflow_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.network_overflow_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.network_overflow_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.network_overflow_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select z_invert is not null from ${model}.gate_link as new where id=id_) then    reason := reason || '   z_invert is not null   ';end if;

if not (select z_ceiling is not null from ${model}.gate_link as new where id=id_) then    reason := reason || '   z_ceiling is not null   ';end if;

if not (select z_ceiling>z_invert from ${model}.gate_link as new where id=id_) then    reason := reason || '   z_ceiling>z_invert   ';end if;

if not (select width is not null from ${model}.gate_link as new where id=id_) then    reason := reason || '   width is not null   ';end if;

if not (select cc is not null from ${model}.gate_link as new where id=id_) then    reason := reason || '   cc is not null   ';end if;

if not (select cc <=1 from ${model}.gate_link as new where id=id_) then    reason := reason || '   cc <=1   ';end if;

if not (select cc >=0 from ${model}.gate_link as new where id=id_) then    reason := reason || '   cc >=0   ';end if;

if not (select action_gate_type is not null from ${model}.gate_link as new where id=id_) then    reason := reason || '   action_gate_type is not null   ';end if;

if not (select mode_valve is not null from ${model}.gate_link as new where id=id_) then    reason := reason || '   mode_valve is not null   ';end if;

if not (select z_gate is not null from ${model}.gate_link as new where id=id_) then    reason := reason || '   z_gate is not null   ';end if;

if not (select z_gate>=z_invert from ${model}.gate_link as new where id=id_) then    reason := reason || '   z_gate>=z_invert   ';end if;

if not (select z_gate<=z_ceiling from ${model}.gate_link as new where id=id_) then    reason := reason || '   z_gate<=z_ceiling   ';end if;

if not (select v_max_cms is not null from ${model}.gate_link as new where id=id_) then    reason := reason || '   v_max_cms is not null   ';end if;

if not (select cc_submerged >0 from ${model}.gate_link as new where id=id_) then    reason := reason || '   cc_submerged >0   ';end if;

if not (select up is not null from ${model}.gate_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.gate_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.gate_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.gate_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station', 'storage', 'crossroad', 'elem_2d') and (hydrograph is not null)) from ${model}.connector_hydrology_link as new where id=id_) then    reason := reason || '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'', ''storage'', ''crossroad'', ''elem_2d'') and (hydrograph is not null))   ';end if;

if not (select up is not null from ${model}.connector_hydrology_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.connector_hydrology_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.connector_hydrology_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.connector_hydrology_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;

if not (select z_crest1 is not null from ${model}.strickler_link as new where id=id_) then    reason := reason || '   z_crest1 is not null   ';end if;

if not (select width1 is not null from ${model}.strickler_link as new where id=id_) then    reason := reason || '   width1 is not null   ';end if;

if not (select width1>0 from ${model}.strickler_link as new where id=id_) then    reason := reason || '   width1>0   ';end if;

if not (select length is not null from ${model}.strickler_link as new where id=id_) then    reason := reason || '   length is not null   ';end if;

if not (select length>0 from ${model}.strickler_link as new where id=id_) then    reason := reason || '   length>0   ';end if;

if not (select rk is not null from ${model}.strickler_link as new where id=id_) then    reason := reason || '   rk is not null   ';end if;

if not (select rk>=0 from ${model}.strickler_link as new where id=id_) then    reason := reason || '   rk>=0   ';end if;

if not (select z_crest2 is not null from ${model}.strickler_link as new where id=id_) then    reason := reason || '   z_crest2 is not null   ';end if;

if not (select z_crest2>z_crest1 from ${model}.strickler_link as new where id=id_) then    reason := reason || '   z_crest2>z_crest1   ';end if;

if not (select width2 is not null from ${model}.strickler_link as new where id=id_) then    reason := reason || '   width2 is not null   ';end if;

if not (select width2>width1 from ${model}.strickler_link as new where id=id_) then    reason := reason || '   width2>width1   ';end if;

if not (select up is not null from ${model}.strickler_link as new where id=id_) then    reason := reason || '   up is not null   ';end if;

if not (select down is not null from ${model}.strickler_link as new where id=id_) then    reason := reason || '   down is not null   ';end if;

if not (select up_type is not null from ${model}.strickler_link as new where id=id_) then    reason := reason || '   up_type is not null   ';end if;

if not (select down_type is not null from ${model}.strickler_link as new where id=id_) then    reason := reason || '   down_type is not null   ';end if;
            return reason;
        end;
        $$$$
;;

--
-- Name: singularity_invalidity_reason(integer); Type: FUNCTION; Schema: ${model};
--

CREATE FUNCTION ${model}.singularity_invalidity_reason(id_ integer) RETURNS character varying
    LANGUAGE plpgsql STABLE
    AS $$$$
        declare
            reason varchar;
        begin
            reason := '';
            if not (select flow >= 0 from ${model}.air_flow_bc_singularity as new where id=id_) then    reason := reason || '   flow >= 0   ';end if;

if not (select is_inwards is not null from ${model}.air_flow_bc_singularity as new where id=id_) then    reason := reason || '   is_inwards is not null   ';end if;

if not (select relative_pressure is not null from ${model}.air_pressure_bc_singularity as new where id=id_) then    reason := reason || '   relative_pressure is not null   ';end if;

if not (select zq_array is not null from ${model}.zq_bc_singularity as new where id=id_) then    reason := reason || '   zq_array is not null   ';end if;

if not (select array_length(zq_array, 1)<=20 from ${model}.zq_bc_singularity as new where id=id_) then    reason := reason || '   array_length(zq_array, 1)<=20   ';end if;

if not (select array_length(zq_array, 1)>=1 from ${model}.zq_bc_singularity as new where id=id_) then    reason := reason || '   array_length(zq_array, 1)>=1   ';end if;

if not (select array_length(zq_array, 2)=2 from ${model}.zq_bc_singularity as new where id=id_) then    reason := reason || '   array_length(zq_array, 2)=2   ';end if;

if not (select downstream is not null from ${model}.zq_split_hydrology_singularity as new where id=id_) then    reason := reason || '   downstream is not null   ';end if;

if not (select split1 is not null from ${model}.zq_split_hydrology_singularity as new where id=id_) then    reason := reason || '   split1 is not null   ';end if;

if not (select downstream_param is not null from ${model}.zq_split_hydrology_singularity as new where id=id_) then    reason := reason || '   downstream_param is not null   ';end if;

if not (select split1_law is not null from ${model}.zq_split_hydrology_singularity as new where id=id_) then    reason := reason || '   split1_law is not null   ';end if;

if not (select split1_param is not null from ${model}.zq_split_hydrology_singularity as new where id=id_) then    reason := reason || '   split1_param is not null   ';end if;

if not (select split2 is null or split2_law is not null from ${model}.zq_split_hydrology_singularity as new where id=id_) then    reason := reason || '   split2 is null or split2_law is not null   ';end if;

if not (select split2 is null or split2_param is not null from ${model}.zq_split_hydrology_singularity as new where id=id_) then    reason := reason || '   split2 is null or split2_param is not null   ';end if;

if not (select pk0_km is not null from ${model}.pipe_branch_marker_singularity as new where id=id_) then    reason := reason || '   pk0_km is not null   ';end if;

if not (select dx is not null from ${model}.pipe_branch_marker_singularity as new where id=id_) then    reason := reason || '   dx is not null   ';end if;

if not (select dx>=0.1 from ${model}.pipe_branch_marker_singularity as new where id=id_) then    reason := reason || '   dx>=0.1   ';end if;

if not (select slope is not null from ${model}.strickler_bc_singularity as new where id=id_) then    reason := reason || '   slope is not null   ';end if;

if not (select k is not null from ${model}.strickler_bc_singularity as new where id=id_) then    reason := reason || '   k is not null   ';end if;

if not (select k>0 from ${model}.strickler_bc_singularity as new where id=id_) then    reason := reason || '   k>0   ';end if;

if not (select width is not null from ${model}.strickler_bc_singularity as new where id=id_) then    reason := reason || '   width is not null   ';end if;

if not (select width>=0 from ${model}.strickler_bc_singularity as new where id=id_) then    reason := reason || '   width>=0   ';end if;

if not (select cascade_mode is not null from ${model}.model_connect_bc_singularity as new where id=id_) then    reason := reason || '   cascade_mode is not null   ';end if;

if not (select cascade_mode='hydrograph' or zq_array is not null or tz_array is not null from ${model}.model_connect_bc_singularity as new where id=id_) then    reason := reason || '   cascade_mode=''hydrograph'' or zq_array is not null or tz_array is not null   ';end if;

if not (select array_length(zq_array, 1)<=20 from ${model}.model_connect_bc_singularity as new where id=id_) then    reason := reason || '   array_length(zq_array, 1)<=20   ';end if;

if not (select array_length(zq_array, 1)>=1 from ${model}.model_connect_bc_singularity as new where id=id_) then    reason := reason || '   array_length(zq_array, 1)>=1   ';end if;

if not (select array_length(zq_array, 2)=2 from ${model}.model_connect_bc_singularity as new where id=id_) then    reason := reason || '   array_length(zq_array, 2)=2   ';end if;

if not (select array_length(tz_array, 1)<=20 from ${model}.model_connect_bc_singularity as new where id=id_) then    reason := reason || '   array_length(tz_array, 1)<=20   ';end if;

if not (select array_length(tz_array, 1)>=1 from ${model}.model_connect_bc_singularity as new where id=id_) then    reason := reason || '   array_length(tz_array, 1)>=1   ';end if;

if not (select array_length(tz_array, 2)=2 from ${model}.model_connect_bc_singularity as new where id=id_) then    reason := reason || '   array_length(tz_array, 2)=2   ';end if;

if not (select array_length(quality, 1)=9 from ${model}.model_connect_bc_singularity as new where id=id_) then    reason := reason || '   array_length(quality, 1)=9   ';end if;

if not (select d_abutment_l is not null from ${model}.bradley_headloss_singularity as new where id=id_) then    reason := reason || '   d_abutment_l is not null   ';end if;

if not (select d_abutment_r is not null from ${model}.bradley_headloss_singularity as new where id=id_) then    reason := reason || '   d_abutment_r is not null   ';end if;

if not (select abutment_type is not null from ${model}.bradley_headloss_singularity as new where id=id_) then    reason := reason || '   abutment_type is not null   ';end if;

if not (select zw_array is not null  from ${model}.bradley_headloss_singularity as new where id=id_) then    reason := reason || '   zw_array is not null    ';end if;

if not (select z_ceiling is not null from ${model}.bradley_headloss_singularity as new where id=id_) then    reason := reason || '   z_ceiling is not null   ';end if;

if not (select array_length(zw_array, 1)<=10 from ${model}.bradley_headloss_singularity as new where id=id_) then    reason := reason || '   array_length(zw_array, 1)<=10   ';end if;

if not (select array_length(zw_array, 1)>=1 from ${model}.bradley_headloss_singularity as new where id=id_) then    reason := reason || '   array_length(zw_array, 1)>=1   ';end if;

if not (select array_length(zw_array, 2)=2 from ${model}.bradley_headloss_singularity as new where id=id_) then    reason := reason || '   array_length(zw_array, 2)=2   ';end if;

if not (select not ${model}.check_on_branch_or_reach_endpoint(new.geom) from ${model}.bradley_headloss_singularity as new where id=id_) then    reason := reason || '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   ';end if;

if not (select l_road is not null from ${model}.bridge_headloss_singularity as new where id=id_) then    reason := reason || '   l_road is not null   ';end if;

if not (select l_road>=0 from ${model}.bridge_headloss_singularity as new where id=id_) then    reason := reason || '   l_road>=0   ';end if;

if not (select z_road is not null from ${model}.bridge_headloss_singularity as new where id=id_) then    reason := reason || '   z_road is not null   ';end if;

if not (select zw_array is not null  from ${model}.bridge_headloss_singularity as new where id=id_) then    reason := reason || '   zw_array is not null    ';end if;

if not (select array_length(zw_array, 1)<=10 from ${model}.bridge_headloss_singularity as new where id=id_) then    reason := reason || '   array_length(zw_array, 1)<=10   ';end if;

if not (select array_length(zw_array, 1)>=1 from ${model}.bridge_headloss_singularity as new where id=id_) then    reason := reason || '   array_length(zw_array, 1)>=1   ';end if;

if not (select array_length(zw_array, 2)=2 from ${model}.bridge_headloss_singularity as new where id=id_) then    reason := reason || '   array_length(zw_array, 2)=2   ';end if;

if not (select not ${model}.check_on_branch_or_reach_endpoint(new.geom) from ${model}.bridge_headloss_singularity as new where id=id_) then    reason := reason || '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   ';end if;

if not (select qz_array is not null from ${model}.hydraulic_cut_singularity as new where id=id_) then    reason := reason || '   qz_array is not null   ';end if;

if not (select array_length(qz_array, 1)<=10 from ${model}.hydraulic_cut_singularity as new where id=id_) then    reason := reason || '   array_length(qz_array, 1)<=10   ';end if;

if not (select array_length(qz_array, 1)>=1 from ${model}.hydraulic_cut_singularity as new where id=id_) then    reason := reason || '   array_length(qz_array, 1)>=1   ';end if;

if not (select array_length(qz_array, 2)=2 from ${model}.hydraulic_cut_singularity as new where id=id_) then    reason := reason || '   array_length(qz_array, 2)=2   ';end if;

if not (select not ${model}.check_on_branch_or_reach_endpoint(new.geom) from ${model}.hydraulic_cut_singularity as new where id=id_) then    reason := reason || '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   ';end if;

if not (select drainage is not null from ${model}.reservoir_rsp_hydrology_singularity as new where id=id_) then    reason := reason || '   drainage is not null   ';end if;

if not (select overflow is not null from ${model}.reservoir_rsp_hydrology_singularity as new where id=id_) then    reason := reason || '   overflow is not null   ';end if;

if not (select z_ini is not null from ${model}.reservoir_rsp_hydrology_singularity as new where id=id_) then    reason := reason || '   z_ini is not null   ';end if;

if not (select zr_sr_qf_qs_array is not null  from ${model}.reservoir_rsp_hydrology_singularity as new where id=id_) then    reason := reason || '   zr_sr_qf_qs_array is not null    ';end if;

if not (select treatment_mode is not null from ${model}.reservoir_rsp_hydrology_singularity as new where id=id_) then    reason := reason || '   treatment_mode is not null   ';end if;

if not (select treatment_param is not null from ${model}.reservoir_rsp_hydrology_singularity as new where id=id_) then    reason := reason || '   treatment_param is not null   ';end if;

if not (select array_length(zr_sr_qf_qs_array, 1)<=10 from ${model}.reservoir_rsp_hydrology_singularity as new where id=id_) then    reason := reason || '   array_length(zr_sr_qf_qs_array, 1)<=10   ';end if;

if not (select array_length(zr_sr_qf_qs_array, 1)>=1 from ${model}.reservoir_rsp_hydrology_singularity as new where id=id_) then    reason := reason || '   array_length(zr_sr_qf_qs_array, 1)>=1   ';end if;

if not (select array_length(zr_sr_qf_qs_array, 2)=4 from ${model}.reservoir_rsp_hydrology_singularity as new where id=id_) then    reason := reason || '   array_length(zr_sr_qf_qs_array, 2)=4   ';end if;

if not (select storage_area is not null from ${model}.hydrograph_bc_singularity as new where id=id_) then    reason := reason || '   storage_area is not null   ';end if;

if not (select storage_area>=0 from ${model}.hydrograph_bc_singularity as new where id=id_) then    reason := reason || '   storage_area>=0   ';end if;

if not (select constant_dry_flow is not null from ${model}.hydrograph_bc_singularity as new where id=id_) then    reason := reason || '   constant_dry_flow is not null   ';end if;

if not (select constant_dry_flow>=0 from ${model}.hydrograph_bc_singularity as new where id=id_) then    reason := reason || '   constant_dry_flow>=0   ';end if;

if not (select distrib_coef is null or distrib_coef>=0 from ${model}.hydrograph_bc_singularity as new where id=id_) then    reason := reason || '   distrib_coef is null or distrib_coef>=0   ';end if;

if not (select lag_time_hr is null or (lag_time_hr>=0) from ${model}.hydrograph_bc_singularity as new where id=id_) then    reason := reason || '   lag_time_hr is null or (lag_time_hr>=0)   ';end if;

if not (select sector is null or (distrib_coef is not null and lag_time_hr is not null) from ${model}.hydrograph_bc_singularity as new where id=id_) then    reason := reason || '   sector is null or (distrib_coef is not null and lag_time_hr is not null)   ';end if;

if not (select array_length(pollution_dryweather_runoff, 1)=2 from ${model}.hydrograph_bc_singularity as new where id=id_) then    reason := reason || '   array_length(pollution_dryweather_runoff, 1)=2   ';end if;

if not (select array_length(pollution_dryweather_runoff, 2)=4 from ${model}.hydrograph_bc_singularity as new where id=id_) then    reason := reason || '   array_length(pollution_dryweather_runoff, 2)=4   ';end if;

if not (select array_length(quality_dryweather_runoff, 1)=2 from ${model}.hydrograph_bc_singularity as new where id=id_) then    reason := reason || '   array_length(quality_dryweather_runoff, 1)=2   ';end if;

if not (select array_length(quality_dryweather_runoff, 2)=9 from ${model}.hydrograph_bc_singularity as new where id=id_) then    reason := reason || '   array_length(quality_dryweather_runoff, 2)=9   ';end if;

if not (select external_file_data or tq_array is not null from ${model}.hydrograph_bc_singularity as new where id=id_) then    reason := reason || '   external_file_data or tq_array is not null   ';end if;

if not (select external_file_data or array_length(tq_array, 1)<=10 from ${model}.hydrograph_bc_singularity as new where id=id_) then    reason := reason || '   external_file_data or array_length(tq_array, 1)<=10   ';end if;

if not (select external_file_data or array_length(tq_array, 1)>=1 from ${model}.hydrograph_bc_singularity as new where id=id_) then    reason := reason || '   external_file_data or array_length(tq_array, 1)>=1   ';end if;

if not (select external_file_data or array_length(tq_array, 2)=2 from ${model}.hydrograph_bc_singularity as new where id=id_) then    reason := reason || '   external_file_data or array_length(tq_array, 2)=2   ';end if;

if not (select z_invert is not null  from ${model}.zregul_weir_singularity as new where id=id_) then    reason := reason || '   z_invert is not null    ';end if;

if not (select z_regul is not null  from ${model}.zregul_weir_singularity as new where id=id_) then    reason := reason || '   z_regul is not null    ';end if;

if not (select width is not null from ${model}.zregul_weir_singularity as new where id=id_) then    reason := reason || '   width is not null   ';end if;

if not (select width>=0 from ${model}.zregul_weir_singularity as new where id=id_) then    reason := reason || '   width>=0   ';end if;

if not (select cc is not null from ${model}.zregul_weir_singularity as new where id=id_) then    reason := reason || '   cc is not null   ';end if;

if not (select cc<=1 from ${model}.zregul_weir_singularity as new where id=id_) then    reason := reason || '   cc<=1   ';end if;

if not (select cc>=0 from ${model}.zregul_weir_singularity as new where id=id_) then    reason := reason || '   cc>=0   ';end if;

if not (select mode_regul is not null from ${model}.zregul_weir_singularity as new where id=id_) then    reason := reason || '   mode_regul is not null   ';end if;

if not (select reoxy_law is not null from ${model}.zregul_weir_singularity as new where id=id_) then    reason := reason || '   reoxy_law is not null   ';end if;

if not (select reoxy_param is not null from ${model}.zregul_weir_singularity as new where id=id_) then    reason := reason || '   reoxy_param is not null   ';end if;

if not (select not ${model}.check_on_branch_or_reach_endpoint(new.geom) from ${model}.zregul_weir_singularity as new where id=id_) then    reason := reason || '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   ';end if;

if not (select qq_array is not null from ${model}.qq_split_hydrology_singularity as new where id=id_) then    reason := reason || '   qq_array is not null   ';end if;

if not (select downstream is not null from ${model}.qq_split_hydrology_singularity as new where id=id_) then    reason := reason || '   downstream is not null   ';end if;

if not (select split1 is not null from ${model}.qq_split_hydrology_singularity as new where id=id_) then    reason := reason || '   split1 is not null   ';end if;

if not (select array_length(qq_array, 1)<=10 from ${model}.qq_split_hydrology_singularity as new where id=id_) then    reason := reason || '   array_length(qq_array, 1)<=10   ';end if;

if not (select array_length(qq_array, 1)>=1 from ${model}.qq_split_hydrology_singularity as new where id=id_) then    reason := reason || '   array_length(qq_array, 1)>=1   ';end if;

if not (select (split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2) from ${model}.qq_split_hydrology_singularity as new where id=id_) then    reason := reason || '   (split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2)   ';end if;

if not (select zs_array is not null  from ${model}.tank_bc_singularity as new where id=id_) then    reason := reason || '   zs_array is not null    ';end if;

if not (select zini is not null from ${model}.tank_bc_singularity as new where id=id_) then    reason := reason || '   zini is not null   ';end if;

if not (select array_length(zs_array, 1)<=10 from ${model}.tank_bc_singularity as new where id=id_) then    reason := reason || '   array_length(zs_array, 1)<=10   ';end if;

if not (select array_length(zs_array, 1)>=1 from ${model}.tank_bc_singularity as new where id=id_) then    reason := reason || '   array_length(zs_array, 1)>=1   ';end if;

if not (select array_length(zs_array, 2)=2 from ${model}.tank_bc_singularity as new where id=id_) then    reason := reason || '   array_length(zs_array, 2)=2   ';end if;

if not (select treatment_mode is not null from ${model}.tank_bc_singularity as new where id=id_) then    reason := reason || '   treatment_mode is not null   ';end if;

if not (select drainage is not null from ${model}.reservoir_rs_hydrology_singularity as new where id=id_) then    reason := reason || '   drainage is not null   ';end if;

if not (select overflow is not null from ${model}.reservoir_rs_hydrology_singularity as new where id=id_) then    reason := reason || '   overflow is not null   ';end if;

if not (select q_drainage is not null from ${model}.reservoir_rs_hydrology_singularity as new where id=id_) then    reason := reason || '   q_drainage is not null   ';end if;

if not (select q_drainage>=0 from ${model}.reservoir_rs_hydrology_singularity as new where id=id_) then    reason := reason || '   q_drainage>=0   ';end if;

if not (select z_ini is not null from ${model}.reservoir_rs_hydrology_singularity as new where id=id_) then    reason := reason || '   z_ini is not null   ';end if;

if not (select zs_array is not null  from ${model}.reservoir_rs_hydrology_singularity as new where id=id_) then    reason := reason || '   zs_array is not null    ';end if;

if not (select treatment_mode is not null from ${model}.reservoir_rs_hydrology_singularity as new where id=id_) then    reason := reason || '   treatment_mode is not null   ';end if;

if not (select array_length(zs_array, 1)<=10 from ${model}.reservoir_rs_hydrology_singularity as new where id=id_) then    reason := reason || '   array_length(zs_array, 1)<=10   ';end if;

if not (select array_length(zs_array, 1)>=1 from ${model}.reservoir_rs_hydrology_singularity as new where id=id_) then    reason := reason || '   array_length(zs_array, 1)>=1   ';end if;

if not (select array_length(zs_array, 2)=2 from ${model}.reservoir_rs_hydrology_singularity as new where id=id_) then    reason := reason || '   array_length(zs_array, 2)=2   ';end if;

if not (select q_dz_array is not null from ${model}.param_headloss_singularity as new where id=id_) then    reason := reason || '   q_dz_array is not null   ';end if;

if not (select array_length(q_dz_array, 1)<=10 from ${model}.param_headloss_singularity as new where id=id_) then    reason := reason || '   array_length(q_dz_array, 1)<=10   ';end if;

if not (select array_length(q_dz_array, 1)>=1 from ${model}.param_headloss_singularity as new where id=id_) then    reason := reason || '   array_length(q_dz_array, 1)>=1   ';end if;

if not (select array_length(q_dz_array, 2)=2 from ${model}.param_headloss_singularity as new where id=id_) then    reason := reason || '   array_length(q_dz_array, 2)=2   ';end if;

if not (select q_dz_array[1][1]=0 from ${model}.param_headloss_singularity as new where id=id_) then    reason := reason || '   q_dz_array[1][1]=0   ';end if;

if not (select q_dz_array[1][2]=0 from ${model}.param_headloss_singularity as new where id=id_) then    reason := reason || '   q_dz_array[1][2]=0   ';end if;

if not (select not ${model}.check_on_branch_or_reach_endpoint(new.geom) from ${model}.param_headloss_singularity as new where id=id_) then    reason := reason || '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   ';end if;

if not (select external_file_data or tz_array is not null from ${model}.tz_bc_singularity as new where id=id_) then    reason := reason || '   external_file_data or tz_array is not null   ';end if;

if not (select external_file_data or array_length(tz_array, 1)<=10 from ${model}.tz_bc_singularity as new where id=id_) then    reason := reason || '   external_file_data or array_length(tz_array, 1)<=10   ';end if;

if not (select external_file_data or array_length(tz_array, 1)>=1 from ${model}.tz_bc_singularity as new where id=id_) then    reason := reason || '   external_file_data or array_length(tz_array, 1)>=1   ';end if;

if not (select external_file_data or array_length(tz_array, 2)=2 from ${model}.tz_bc_singularity as new where id=id_) then    reason := reason || '   external_file_data or array_length(tz_array, 2)=2   ';end if;

if not (select z_invert is not null from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   z_invert is not null   ';end if;

if not (select z_ceiling is not null from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   z_ceiling is not null   ';end if;

if not (select width is not null from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   width is not null   ';end if;

if not (select width >=0 from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   width >=0   ';end if;

if not (select cc is not null from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   cc is not null   ';end if;

if not (select cc <=1 from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   cc <=1   ';end if;

if not (select cc >=0 from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   cc >=0   ';end if;

if not (select action_gate_type is not null from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   action_gate_type is not null   ';end if;

if not (select z_invert_stop is not null from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   z_invert_stop is not null   ';end if;

if not (select z_ceiling_stop is not null from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   z_ceiling_stop is not null   ';end if;

if not (select v_max_cms is not null from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   v_max_cms is not null   ';end if;

if not (select v_max_cms>=0 from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   v_max_cms>=0   ';end if;

if not (select dt_regul_hr is not null from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   dt_regul_hr is not null   ';end if;

if not (select mode_regul!='elevation' or z_control_node is not null from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   mode_regul!=''elevation'' or z_control_node is not null   ';end if;

if not (select mode_regul!='elevation' or z_pid_array is not null from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   mode_regul!=''elevation'' or z_pid_array is not null   ';end if;

if not (select mode_regul!='elevation' or z_tz_array is not null from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   mode_regul!=''elevation'' or z_tz_array is not null   ';end if;

if not (select mode_regul!='elevation' or array_length(z_tz_array, 1)<=10 from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   ';end if;

if not (select mode_regul!='elevation' or array_length(z_tz_array, 1)>=1 from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   ';end if;

if not (select mode_regul!='elevation' or array_length(z_tz_array, 2)=2 from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   ';end if;

if not (select mode_regul!='discharge' or q_z_crit is not null from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   mode_regul!=''discharge'' or q_z_crit is not null   ';end if;

if not (select mode_regul!='discharge' or array_length(q_tq_array, 1)<=10 from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   ';end if;

if not (select mode_regul!='discharge' or array_length(q_tq_array, 1)>=1 from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   ';end if;

if not (select mode_regul!='discharge' or array_length(q_tq_array, 2)=2 from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   ';end if;

if not (select mode_regul!='no_regulation' or nr_z_gate is not null from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   mode_regul!=''no_regulation'' or nr_z_gate is not null   ';end if;

if not (select cc_submerged >0 from ${model}.regul_sluice_gate_singularity as new where id=id_) then    reason := reason || '   cc_submerged >0   ';end if;

if not (select z_invert is not null from ${model}.gate_singularity as new where id=id_) then    reason := reason || '   z_invert is not null   ';end if;

if not (select z_ceiling is not null from ${model}.gate_singularity as new where id=id_) then    reason := reason || '   z_ceiling is not null   ';end if;

if not (select z_ceiling>z_invert from ${model}.gate_singularity as new where id=id_) then    reason := reason || '   z_ceiling>z_invert   ';end if;

if not (select width is not null from ${model}.gate_singularity as new where id=id_) then    reason := reason || '   width is not null   ';end if;

if not (select cc is not null from ${model}.gate_singularity as new where id=id_) then    reason := reason || '   cc is not null   ';end if;

if not (select cc <=1 from ${model}.gate_singularity as new where id=id_) then    reason := reason || '   cc <=1   ';end if;

if not (select cc >=0 from ${model}.gate_singularity as new where id=id_) then    reason := reason || '   cc >=0   ';end if;

if not (select action_gate_type is not null from ${model}.gate_singularity as new where id=id_) then    reason := reason || '   action_gate_type is not null   ';end if;

if not (select mode_valve is not null from ${model}.gate_singularity as new where id=id_) then    reason := reason || '   mode_valve is not null   ';end if;

if not (select z_gate is not null from ${model}.gate_singularity as new where id=id_) then    reason := reason || '   z_gate is not null   ';end if;

if not (select z_gate>=z_invert from ${model}.gate_singularity as new where id=id_) then    reason := reason || '   z_gate>=z_invert   ';end if;

if not (select z_gate<=z_ceiling from ${model}.gate_singularity as new where id=id_) then    reason := reason || '   z_gate<=z_ceiling   ';end if;

if not (select v_max_cms is not null from ${model}.gate_singularity as new where id=id_) then    reason := reason || '   v_max_cms is not null   ';end if;

if not (select not ${model}.check_on_branch_or_reach_endpoint(new.geom) from ${model}.gate_singularity as new where id=id_) then    reason := reason || '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   ';end if;

if not (select cc_submerged >0 from ${model}.gate_singularity as new where id=id_) then    reason := reason || '   cc_submerged >0   ';end if;

if not (select law_type is not null from ${model}.borda_headloss_singularity as new where id=id_) then    reason := reason || '   law_type is not null   ';end if;

if not (select param is not null from ${model}.borda_headloss_singularity as new where id=id_) then    reason := reason || '   param is not null   ';end if;

if not (select not ${model}.check_on_branch_or_reach_endpoint(new.geom) from ${model}.borda_headloss_singularity as new where id=id_) then    reason := reason || '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   ';end if;

if not (select q0 is not null from ${model}.constant_inflow_bc_singularity as new where id=id_) then    reason := reason || '   q0 is not null   ';end if;

if not (select z_weir is not null from ${model}.weir_bc_singularity as new where id=id_) then    reason := reason || '   z_weir is not null   ';end if;

if not (select width is not null from ${model}.weir_bc_singularity as new where id=id_) then    reason := reason || '   width is not null   ';end if;

if not (select width>=0 from ${model}.weir_bc_singularity as new where id=id_) then    reason := reason || '   width>=0   ';end if;

if not (select cc is not null from ${model}.weir_bc_singularity as new where id=id_) then    reason := reason || '   cc is not null   ';end if;

if not (select cc<=1 from ${model}.weir_bc_singularity as new where id=id_) then    reason := reason || '   cc<=1   ';end if;

if not (select cc>=0. from ${model}.weir_bc_singularity as new where id=id_) then    reason := reason || '   cc>=0.   ';end if;
            return reason;
        end;
        $$$$
;;

--
-- Name: invalid; Type: VIEW; Schema: ${model};
--

drop view ${model}.invalid;

CREATE OR REPLACE VIEW ${model}.invalid AS
 SELECT concat('node:', n.id) AS id,
    n.validity,
    n.name,
    'node'::character varying AS type,
    (n.node_type)::character varying AS subtype,
    n.geom,
    ${model}.node_invalidity_reason(n.id)::text AS reason
   FROM ${model}._node n
  WHERE (n.validity = false)
UNION
 SELECT concat('link:', l.id) AS id,
    l.validity,
    l.name,
    'link'::character varying AS type,
    (l.link_type)::character varying AS subtype,
    (public.st_force3d(public.st_centroid(l.geom)))::public.geometry(PointZ,${srid}) AS geom,
    ${model}.link_invalidity_reason(l.id)::text AS reason
   FROM ${model}._link l
  WHERE (l.validity = false)
UNION
 SELECT concat('singularity:', s.id) AS id,
    s.validity,
    s.name,
    'singularity'::character varying AS type,
    (s.singularity_type)::character varying AS subtype,
    n.geom,
    ${model}.singularity_invalidity_reason(s.id)::text AS reason
   FROM (${model}._singularity s
     JOIN ${model}._node n ON ((s.node = n.id)))
  WHERE (s.validity = false)
UNION
 SELECT concat('profile:', p.id) AS id,
    p.validity,
    p.name,
    'profile'::character varying AS type,
    ''::character varying AS subtype,
    n.geom,
    ''::character varying AS reason
   FROM (${model}._river_cross_section_profile p
     JOIN ${model}._node n ON ((p.id = n.id)))
  WHERE (p.validity = false)
;;


--
-- drop and re-create constrain for link connectivity validity to include aeraulique type
--

alter table ${model}._link drop constraint ${model}_link_connectivity_check_cstr
;;

ALTER TABLE IF EXISTS ${model}._link
    ADD CONSTRAINT ${model}_link_connectivity_check_cstr
    CHECK (
       up_type = 'manhole_hydrology'::hydra_node_type AND down_type = 'station'::hydra_node_type
    OR up_type = 'manhole'::hydra_node_type AND down_type = 'manhole'::hydra_node_type
    OR up_type = 'manhole'::hydra_node_type AND down_type = 'station'::hydra_node_type
    OR link_type = 'air_duct'::hydra_link_type AND up_type = 'station'::hydra_node_type AND down_type = 'station'::hydra_node_type
    OR link_type = 'air_headloss'::hydra_link_type AND up_type = 'station'::hydra_node_type AND down_type = 'station'::hydra_node_type
    OR link_type = 'ventilator'::hydra_link_type AND up_type = 'station'::hydra_node_type AND down_type = 'station'::hydra_node_type
    OR link_type = 'jet_fan'::hydra_link_type AND up_type = 'station'::hydra_node_type AND down_type = 'station'::hydra_node_type
    OR link_type = 'regul_gate'::hydra_link_type AND (up_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
    OR link_type = 'connector'::hydra_link_type AND (up_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
    OR link_type = 'fuse_spillway'::hydra_link_type AND (up_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
    OR link_type = 'pipe'::hydra_link_type AND (up_type = 'manhole_hydrology'::hydra_node_type AND down_type = 'manhole_hydrology'::hydra_node_type OR up_type = 'manhole'::hydra_node_type AND down_type = 'manhole'::hydra_node_type)
    OR link_type = 'routing_hydrology'::hydra_link_type AND up_type = 'catchment'::hydra_node_type AND (down_type = ANY (ARRAY['manhole_hydrology'::hydra_node_type, 'manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'storage'::hydra_node_type, 'crossroad'::hydra_node_type, 'elem_2d'::hydra_node_type]))
    OR link_type = 'deriv_pump'::hydra_link_type AND (up_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
    OR link_type = 'porous'::hydra_link_type AND (up_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
    OR link_type = 'weir'::hydra_link_type AND (up_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
    OR link_type = 'borda_headloss'::hydra_link_type AND (up_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
    OR link_type = 'pump'::hydra_link_type AND (up_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
    OR link_type = 'mesh_2d'::hydra_link_type AND up_type = 'elem_2d'::hydra_node_type AND down_type = 'elem_2d'::hydra_node_type
    OR link_type = 'overflow'::hydra_link_type AND (up_type = ANY (ARRAY['river'::hydra_node_type, 'storage'::hydra_node_type, 'elem_2d'::hydra_node_type, 'crossroad'::hydra_node_type])) AND (down_type = ANY (ARRAY['river'::hydra_node_type, 'storage'::hydra_node_type, 'elem_2d'::hydra_node_type, 'crossroad'::hydra_node_type]))
    OR link_type = 'network_overflow'::hydra_link_type AND up_type = 'manhole'::hydra_node_type AND (down_type = ANY (ARRAY['crossroad'::hydra_node_type, 'elem_2d'::hydra_node_type, 'storage'::hydra_node_type, 'river'::hydra_node_type]))
    OR link_type = 'gate'::hydra_link_type AND (up_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
    OR link_type = 'connector_hydrology'::hydra_link_type AND up_type = 'manhole_hydrology'::hydra_node_type AND (down_type = ANY (ARRAY['manhole_hydrology'::hydra_node_type, 'manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'storage'::hydra_node_type, 'crossroad'::hydra_node_type, 'elem_2d'::hydra_node_type]))
    OR link_type = 'strickler'::hydra_link_type AND (up_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type])) AND (down_type <> ALL (ARRAY['catchment'::hydra_node_type, 'manhole_hydrology'::hydra_node_type]))
    )
;;


--
-- drop and re-create constrain for singularity connectivity validity to include aeraulique type
--

alter table ${model}._singularity drop constraint ${model}_singularity_connectivity_check_cstr
;;


ALTER TABLE IF EXISTS ${model}._singularity
    ADD CONSTRAINT ${model}_singularity_connectivity_check_cstr
    CHECK (
        singularity_type = 'air_flow_bc'::hydra_singularity_type AND node_type = 'station'::hydra_node_type
        OR singularity_type = 'air_pressure_bc'::hydra_singularity_type AND node_type = 'station'::hydra_node_type
        OR singularity_type = 'zq_bc'::hydra_singularity_type AND node_type <> 'catchment'::hydra_node_type AND node_type <> 'manhole_hydrology'::hydra_node_type
        OR singularity_type = 'zq_split_hydrology'::hydra_singularity_type AND node_type = 'manhole_hydrology'::hydra_node_type
        OR singularity_type = 'pipe_branch_marker'::hydra_singularity_type AND node_type = 'manhole'::hydra_node_type
        OR singularity_type = 'strickler_bc'::hydra_singularity_type AND node_type <> 'catchment'::hydra_node_type AND node_type <> 'manhole_hydrology'::hydra_node_type
        OR singularity_type = 'marker'::hydra_singularity_type AND (node_type = ANY (ARRAY['river'::hydra_node_type, 'manhole'::hydra_node_type]))
        OR singularity_type = 'model_connect_bc'::hydra_singularity_type AND node_type <> 'catchment'::hydra_node_type AND node_type <> 'manhole_hydrology'::hydra_node_type
        OR singularity_type = 'bradley_headloss'::hydra_singularity_type AND node_type = 'river'::hydra_node_type
        OR singularity_type = 'bridge_headloss'::hydra_singularity_type AND node_type = 'river'::hydra_node_type
        OR singularity_type = 'hydraulic_cut'::hydra_singularity_type AND (node_type = ANY (ARRAY['river'::hydra_node_type, 'manhole'::hydra_node_type]))
        OR singularity_type = 'reservoir_rsp_hydrology'::hydra_singularity_type AND node_type = 'manhole_hydrology'::hydra_node_type
        OR singularity_type = 'hydrograph_bc'::hydra_singularity_type AND node_type <> 'catchment'::hydra_node_type AND node_type <> 'manhole_hydrology'::hydra_node_type
        OR singularity_type = 'zregul_weir'::hydra_singularity_type AND (node_type = ANY (ARRAY['river'::hydra_node_type, 'manhole'::hydra_node_type]))
        OR singularity_type = 'qq_split_hydrology'::hydra_singularity_type AND node_type = 'manhole_hydrology'::hydra_node_type
        OR singularity_type = 'tank_bc'::hydra_singularity_type AND node_type <> 'catchment'::hydra_node_type AND node_type <> 'manhole_hydrology'::hydra_node_type
        OR singularity_type = 'reservoir_rs_hydrology'::hydra_singularity_type AND node_type = 'manhole_hydrology'::hydra_node_type
        OR singularity_type = 'param_headloss'::hydra_singularity_type AND (node_type = ANY (ARRAY['river'::hydra_node_type, 'manhole'::hydra_node_type]))
        OR singularity_type = 'froude_bc'::hydra_singularity_type AND (node_type = ANY (ARRAY['river'::hydra_node_type, 'manhole'::hydra_node_type]))
        OR singularity_type = 'tz_bc'::hydra_singularity_type AND node_type <> 'catchment'::hydra_node_type AND node_type <> 'manhole_hydrology'::hydra_node_type
        OR singularity_type = 'hydrology_bc'::hydra_singularity_type AND node_type = 'manhole_hydrology'::hydra_node_type
        OR singularity_type = 'regul_sluice_gate'::hydra_singularity_type AND (node_type = ANY (ARRAY['river'::hydra_node_type, 'manhole'::hydra_node_type]))
        OR singularity_type = 'gate'::hydra_singularity_type AND (node_type = ANY (ARRAY['river'::hydra_node_type, 'manhole'::hydra_node_type]))
        OR singularity_type = 'borda_headloss'::hydra_singularity_type AND (node_type = ANY (ARRAY['river'::hydra_node_type, 'manhole'::hydra_node_type]))
        OR singularity_type = 'constant_inflow_bc'::hydra_singularity_type AND node_type <> 'catchment'::hydra_node_type AND node_type <> 'manhole_hydrology'::hydra_node_type
        OR singularity_type = 'weir_bc'::hydra_singularity_type AND node_type <> 'catchment'::hydra_node_type AND node_type <> 'manhole_hydrology'::hydra_node_type
        )
;;

-- update trigger function for air_headloss (removing custom_length)

CREATE OR REPLACE FUNCTION ${model}.air_headloss_link_fct()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from ${model}.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;

            -- Creation of HY singularity if type = routing_hydrology_link or type = connector_hydrology_link
            if 'air_headloss' = 'routing_hydrology' or 'air_headloss' = 'connector_hydrology' then
                if down_type_ != 'manhole_hydrology' and not (select exists (select 1 from ${model}.hydrograph_bc_singularity where node=down_)) then
                    insert into ${model}.hydrograph_bc_singularity(geom) select geom from ${model}._node where id=down_;
                end if;
            end if;

            insert into ${model}._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('air_headloss', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update ${model}._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'air_headloss') where name = 'define_later' and id = id_;
            insert into ${model}._air_headloss_link(id, link_type, comment, area, up_to_down_headloss_coefficient, down_to_up_headloss_coefficient)
                values (id_, 'air_headloss', new.comment, new.area, coalesce(new.up_to_down_headloss_coefficient, .008), coalesce(new.down_to_up_headloss_coefficient, .008));
            if 'air_headloss' = 'pipe' and (select trigger_branch from ${model}.metadata) then
                perform ${model}.branch_update_fct();
            end if;
            --perform ${model}.set_link_altitude(id_);
            perform ${model}.add_configuration_fct(new.configuration::json, id_, 'air_headloss_link');
            update ${model}._link set validity = (select (area > 0) and (up_to_down_headloss_coefficient > 0) and (down_to_up_headloss_coefficient > 0) from  ${model}._air_headloss_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'air_headloss' = 'pipe' then
                update ${model}.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.comment, new.area, new.up_to_down_headloss_coefficient, new.down_to_up_headloss_coefficient) is distinct from (old.comment, old.area, old.up_to_down_headloss_coefficient, old.down_to_up_headloss_coefficient)) then
                select is_switching from ${model}.config_switch into switching;
                if switching=false then
                    select name from ${model}.configuration as c, ${model}.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.comment, old.area, old.up_to_down_headloss_coefficient, old.down_to_up_headloss_coefficient) as o, (select new.comment, new.area, new.up_to_down_headloss_coefficient, new.down_to_up_headloss_coefficient) as n into new_config;
                        update ${model}._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.comment, new.area, new.up_to_down_headloss_coefficient, new.down_to_up_headloss_coefficient) n into new_config;
                        update ${model}._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from ${model}.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update ${model}._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update ${model}._air_headloss_link set comment=new.comment, area=new.area, up_to_down_headloss_coefficient=new.up_to_down_headloss_coefficient, down_to_up_headloss_coefficient=new.down_to_up_headloss_coefficient where id=old.id;

            if 'air_headloss' = 'pipe' and (select trigger_branch from ${model}.metadata) then
                if not ST_Equals(old.geom, new.geom) or (new.exclude_from_branch != old.exclude_from_branch) then
                    perform ${model}.branch_update_fct();
                end if;
            end if;
            perform ${model}.add_configuration_fct(new.configuration::json, old.id, 'air_headloss_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'air_headloss' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update ${model}.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update ${model}._link set validity = (select (area > 0) and (up_to_down_headloss_coefficient > 0) and (down_to_up_headloss_coefficient > 0) from  ${model}._air_headloss_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from ${model}._air_headloss_link where id=old.id;
            delete from ${model}._link where id=old.id;
            if 'air_headloss' = 'pipe' and (select trigger_branch from ${model}.metadata) then
                perform ${model}.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'air_headloss' = 'pipe' then
                update ${model}.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$;

;;

create or replace function ${model}.crossroad_update()
returns integer
language plpgsql
as $$$$
    declare
        res_ integer;
    begin
        with nodes as (
            insert into $model.crossroad_node(geom)
                (select distinct s.geom
                    from (
                        select (st_dumppoints(geom)).geom
                        from $model.street
                            union
                        select (st_dumppoints(st_intersection(s1.geom, s2.geom))).geom as geom
                        from $model.street as s1, $model.street as s2
                        where ST_intersects(s1.geom,s2.geom)
                        and s1.id<s2.id
                    ) as s
                    left join (select st_force2d(geom) as geom from $model.crossroad_node) as c
                    on st_force2d(s.geom)=c.geom
                    where c.geom is null)
            returning id
        )
        select count(1) from nodes into res_;
        return res_;
    end;
$$$$
;;