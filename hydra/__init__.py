# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
 This script initializes the plugin, making it known to QGIS.
"""

import os
import json
import requests
import xml.etree.ElementTree as ET
import packaging
import importlib
import site
import subprocess
from pathlib import Path
from configparser import ConfigParser
from qgis.utils import pluginMetadata
from qgis.core import Qgis, QgsMessageLog
from qgis.PyQt.QtWidgets import QPushButton

if os.name == 'nt':
    # install wstar and dependency in local python directories
    qgis_version = str(Qgis.QGIS_VERSION_INT)[:3]

    if qgis_version == '322':
        requirements = Path(__file__).parent / f'requirements-{qgis_version}.txt'
    else:
        requirements = Path(__file__).parent / 'requirements.txt'

    try:
        import wstar
        with open(requirements) as r:
            for line in r:
                if line[:5] == 'wstar':
                    version = line.strip().split('wstar==')[1]
        if not hasattr(wstar, '__version__') or version != wstar.__version__:
            raise ModuleNotFoundError

    except ModuleNotFoundError:
        QgsMessageLog.logMessage("Missing wstar, installing with pip", level=Qgis.Warning)
        cwd = Path(__file__).parent / 'dist'
        cmd_whl = ["python", "-m", "pip", "install", "--user", "--find-links", str(cwd), str(requirements)]
        cmd_req = ["python", "-m", "pip", "install", "--user", "-r", str(requirements)]
        wheel_install_success = False
        if cwd.is_dir() and list(cwd.glob('*.whl')):
            # official distribution includes .whl in dist
            cmd = cmd_whl
            try:
                subprocess.check_output(cmd, creationflags=subprocess.CREATE_NO_WINDOW, cwd=str(cwd), universal_newlines=True)
                wheel_install_success = True
            except subprocess.CalledProcessError:
                pass

        if not wheel_install_success:
            # development version or uses install from wstar gitlab repository
            cmd = cmd_req
            cwd = Path(__file__).parent
            try:
                subprocess.check_output(cmd, creationflags=subprocess.CREATE_NO_WINDOW, cwd=str(cwd), stderr=subprocess.STDOUT, universal_newlines=True)
            except subprocess.CalledProcessError as exc:
                raise RuntimeError(f"{' '.join(cmd)}\n{exc.output}")

        # make sure new modules will be found from now on
        importlib.reload(site)
        importlib.invalidate_caches()


_hydra_dir = os.path.join(os.path.expanduser('~'), ".hydra")
_log_file = os.path.join(_hydra_dir, 'hydra.log')
_hydra_config_file = os.path.join(_hydra_dir, "hydra.config")
_pg_pass_file = os.path.join(os.getenv('APPDATA'), 'postgresql', 'pgpass.conf') if os.name == 'nt' else os.path.join(os.path.expanduser('~'), '.pgpass.conf')
_pg_service_file = os.path.join(os.path.expanduser('~'), ".pg_service.conf")

def open_plugin_manager():
    import pyplugin_installer
    pyplugin_installer.instance().showPluginManagerWhenReady()



def classFactory(iface):
    # create hydra directory
    if not os.path.isdir(_hydra_dir):
        os.makedirs(_hydra_dir)

    # reset log file
    open(_log_file, 'w').close()

    # default_pg_service pgservice file content
    DEFAULT_PG_SERVICE="""
    [hydra]
    host=127.0.0.1
    port=5454
    user=hydra
    password=hydra

    [hydra_remote]
    user=
    password=
    host=database.hydra-software.net
    port=5444
    """

    # SERVICE FILE
    if 'PGSERVICEFILE' not in os.environ: # .pg_service.conf not set in environment
        os.environ['PGSERVICEFILE'] = _pg_service_file
    # create service file if not exists
    if not os.path.isfile(_pg_service_file):
        with open(_pg_service_file, 'a+') as pgfile:
            pgfile.write(DEFAULT_PG_SERVICE)
    # reads services
    services = ConfigParser(default_section='hydra')
    services.read(_pg_service_file)

    # Check if plugin version is the latest one:
    try:
        hydra_plugins = ET.fromstring(requests.get('https://hydra-software.net/telechargement/hydra.xml', headers={'User-Agent': 'Mozilla'}, timeout=2).text)
        for plugin in hydra_plugins:
            # read each plugin's metadata from xml file
            if plugin.attrib['name'] == 'hydra':
                qgis_minimum_version = plugin.find('qgis_minimum_version').text
                # if plugin is hydra and to be used with qgis >3, check available version
                if packaging.version(qgis_minimum_version) > packaging.version('3.0'):
                    current_version = pluginMetadata('hydra', 'version')
                    available_version = plugin.attrib['version']
                    if packaging.version(current_version) < packaging.version(available_version):
                        widget = iface.messageBar().createMessage("New hydra version", f"Version {available_version} is available for hydra ! Open the QGIS plugin manager to download it.")
                        button = QPushButton(widget)
                        button.setText("Plugin manager")
                        button.clicked.connect(open_plugin_manager)
                        widget.layout().addWidget(button)
                        iface.messageBar().pushWidget(widget, level=Qgis.Info, duration=30)
                    break
    except:
        pass

    # reads old hydra config file & pgpass file to add data to service file
    if os.path.isfile(_hydra_config_file):
        with open(_hydra_config_file) as hydra_config:
            config = json.load(hydra_config)
            if 'hydra_remote' not in services and 'server_db' in config:
                    services['hydra_remote'] = config['server_db']
            if 'hydra_legacy' not in services and 'local_db' in config:
                if 'user' in config['local_db'] and 'password' in config['local_db']:
                    services['hydra_legacy'] = config['local_db']
                    services['hydra_legacy']['host'] = '127.0.0.1'
                elif os.path.exists(_pg_pass_file):
                    with open(_pg_pass_file) as pg_pass:
                        for line in pg_pass:
                            host, port, dbname, user, password = line.rstrip().split(':')
                            if host == '127.0.0.1' and port in ['*', '5432']:
                                services['hydra_legacy'] = {'host': host,
                                                            'port': 5432,
                                                            'user': user,
                                                            'password': password}

    with open(_pg_service_file, 'w', newline='\n') as service_file:
        services.write(service_file, space_around_delimiters=False)

    from .plugin import Hydra
    return Hydra(iface)
