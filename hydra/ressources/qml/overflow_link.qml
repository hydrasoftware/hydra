<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingTol="1" simplifyLocal="1" version="3.10.11-A Coruña" maxScale="0" labelsEnabled="0" readOnly="0" minScale="10000" simplifyMaxScale="1" hasScaleBasedVisibilityFlag="0" simplifyDrawingHints="1" styleCategories="AllStyleCategories" simplifyAlgorithm="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 type="singleSymbol" symbollevels="0" enableorderby="0" forceraster="0">
    <symbols>
      <symbol name="0" type="line" alpha="1" force_rhr="0" clip_to_extent="1">
        <layer locked="0" pass="0" enabled="1" class="SimpleLine">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,1,0.05,1,0.5" k="customdash_map_unit_scale"/>
          <prop v="MapUnit" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,158,0,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.5" k="line_width"/>
          <prop v="MapUnit" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,1,0.05,1,0.5" k="offset_map_unit_scale"/>
          <prop v="MapUnit" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,1,0.05,1,0.5" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Pie">
    <DiagramCategory maxScaleDenominator="1e+08" height="15" width="15" diagramOrientation="Up" rotationOffset="270" scaleBasedVisibility="0" lineSizeScale="3x:0,0,0,0,0,0" barWidth="5" sizeScale="3x:0,0,0,0,0,0" opacity="1" enabled="0" labelPlacementMethod="XHeight" backgroundAlpha="255" lineSizeType="MM" sizeType="MM" backgroundColor="#ffffff" penWidth="0" minScaleDenominator="1000" penAlpha="255" minimumSize="0" scaleDependency="Area" penColor="#000000">
      <fontProperties description="Arial,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute label="" color="#000000" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings dist="0" zIndex="0" placement="2" priority="0" linePlacementFlags="2" obstacle="0" showAll="1">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties" type="Map">
          <Option name="show" type="Map">
            <Option value="true" name="active" type="bool"/>
            <Option value="id" name="field" type="QString"/>
            <Option value="2" name="type" type="int"/>
          </Option>
        </Option>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="name">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="up">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="down">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="z_crest1">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="width1">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="z_crest2">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="width2">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="cc">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="lateral_contraction_coef">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="break_mode">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="z_break">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="t_break">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="z_invert">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="width_breach">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="grp">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="dt_fracw_array">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="border">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="configuration">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="generated">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="validity">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="up_type">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="down_type">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="0" name="IsMultiline" type="QString"/>
            <Option value="0" name="UseHtml" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="configuration_json">
      <editWidget type="KeyValue">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" index="0" field="id"/>
    <alias name="" index="1" field="name"/>
    <alias name="" index="2" field="up"/>
    <alias name="" index="3" field="down"/>
    <alias name="" index="4" field="z_crest1"/>
    <alias name="" index="5" field="width1"/>
    <alias name="" index="6" field="z_crest2"/>
    <alias name="" index="7" field="width2"/>
    <alias name="" index="8" field="cc"/>
    <alias name="" index="9" field="lateral_contraction_coef"/>
    <alias name="" index="10" field="break_mode"/>
    <alias name="" index="11" field="z_break"/>
    <alias name="" index="12" field="t_break"/>
    <alias name="" index="13" field="z_invert"/>
    <alias name="" index="14" field="width_breach"/>
    <alias name="" index="15" field="grp"/>
    <alias name="" index="16" field="dt_fracw_array"/>
    <alias name="" index="17" field="border"/>
    <alias name="" index="18" field="configuration"/>
    <alias name="" index="19" field="generated"/>
    <alias name="" index="20" field="validity"/>
    <alias name="" index="21" field="up_type"/>
    <alias name="" index="22" field="down_type"/>
    <alias name="" index="23" field="configuration_json"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="id" applyOnUpdate="0"/>
    <default expression="" field="name" applyOnUpdate="0"/>
    <default expression="" field="up" applyOnUpdate="0"/>
    <default expression="" field="down" applyOnUpdate="0"/>
    <default expression="" field="z_crest1" applyOnUpdate="0"/>
    <default expression="" field="width1" applyOnUpdate="0"/>
    <default expression="" field="z_crest2" applyOnUpdate="0"/>
    <default expression="" field="width2" applyOnUpdate="0"/>
    <default expression="" field="cc" applyOnUpdate="0"/>
    <default expression="" field="lateral_contraction_coef" applyOnUpdate="0"/>
    <default expression="" field="break_mode" applyOnUpdate="0"/>
    <default expression="" field="z_break" applyOnUpdate="0"/>
    <default expression="" field="t_break" applyOnUpdate="0"/>
    <default expression="" field="z_invert" applyOnUpdate="0"/>
    <default expression="" field="width_breach" applyOnUpdate="0"/>
    <default expression="" field="grp" applyOnUpdate="0"/>
    <default expression="" field="dt_fracw_array" applyOnUpdate="0"/>
    <default expression="" field="border" applyOnUpdate="0"/>
    <default expression="" field="configuration" applyOnUpdate="0"/>
    <default expression="" field="generated" applyOnUpdate="0"/>
    <default expression="" field="validity" applyOnUpdate="0"/>
    <default expression="" field="up_type" applyOnUpdate="0"/>
    <default expression="" field="down_type" applyOnUpdate="0"/>
    <default expression="" field="configuration_json" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" constraints="3" exp_strength="0" field="id" notnull_strength="1"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="name" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="up" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="down" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="z_crest1" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="width1" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="z_crest2" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="width2" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="cc" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="lateral_contraction_coef" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="break_mode" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="z_break" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="t_break" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="z_invert" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="width_breach" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="grp" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="dt_fracw_array" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="border" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="configuration" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="generated" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="validity" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="up_type" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="down_type" notnull_strength="0"/>
    <constraint unique_strength="0" constraints="0" exp_strength="0" field="configuration_json" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" desc="" field="id"/>
    <constraint exp="" desc="" field="name"/>
    <constraint exp="" desc="" field="up"/>
    <constraint exp="" desc="" field="down"/>
    <constraint exp="" desc="" field="z_crest1"/>
    <constraint exp="" desc="" field="width1"/>
    <constraint exp="" desc="" field="z_crest2"/>
    <constraint exp="" desc="" field="width2"/>
    <constraint exp="" desc="" field="cc"/>
    <constraint exp="" desc="" field="lateral_contraction_coef"/>
    <constraint exp="" desc="" field="break_mode"/>
    <constraint exp="" desc="" field="z_break"/>
    <constraint exp="" desc="" field="t_break"/>
    <constraint exp="" desc="" field="z_invert"/>
    <constraint exp="" desc="" field="width_breach"/>
    <constraint exp="" desc="" field="grp"/>
    <constraint exp="" desc="" field="dt_fracw_array"/>
    <constraint exp="" desc="" field="border"/>
    <constraint exp="" desc="" field="configuration"/>
    <constraint exp="" desc="" field="generated"/>
    <constraint exp="" desc="" field="validity"/>
    <constraint exp="" desc="" field="up_type"/>
    <constraint exp="" desc="" field="down_type"/>
    <constraint exp="" desc="" field="configuration_json"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column name="id" type="field" width="-1" hidden="0"/>
      <column name="name" type="field" width="-1" hidden="0"/>
      <column name="up" type="field" width="-1" hidden="0"/>
      <column name="down" type="field" width="-1" hidden="0"/>
      <column name="z_invert" type="field" width="-1" hidden="0"/>
      <column name="configuration" type="field" width="-1" hidden="0"/>
      <column name="generated" type="field" width="-1" hidden="0"/>
      <column name="validity" type="field" width="-1" hidden="0"/>
      <column name="up_type" type="field" width="-1" hidden="0"/>
      <column name="down_type" type="field" width="-1" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
      <column name="z_crest1" type="field" width="-1" hidden="0"/>
      <column name="width1" type="field" width="-1" hidden="0"/>
      <column name="z_crest2" type="field" width="-1" hidden="0"/>
      <column name="width2" type="field" width="-1" hidden="0"/>
      <column name="cc" type="field" width="-1" hidden="0"/>
      <column name="lateral_contraction_coef" type="field" width="-1" hidden="0"/>
      <column name="break_mode" type="field" width="-1" hidden="0"/>
      <column name="z_break" type="field" width="-1" hidden="0"/>
      <column name="t_break" type="field" width="-1" hidden="0"/>
      <column name="width_breach" type="field" width="-1" hidden="0"/>
      <column name="grp" type="field" width="-1" hidden="0"/>
      <column name="dt_fracw_array" type="field" width="-1" hidden="0"/>
      <column name="border" type="field" width="-1" hidden="0"/>
      <column name="configuration_json" type="field" width="-1" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">C:/Travail/46152_AUDIT_OF_RIVERS/Mission/Travail/Model/flacq/flacq/flacq</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>C:/Travail/46152_AUDIT_OF_RIVERS/Mission/Travail/Model/flacq/flacq/flacq</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ
"Fonction d'initialisation Python"
Voici un exemple à suivre:
"""
from PyQt4.QtGui import QWidget

def my_form_open(dialog, layer, feature):
⇥geom = feature.geometry()
⇥control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="border" editable="1"/>
    <field name="break_mode" editable="1"/>
    <field name="cc" editable="1"/>
    <field name="configuration" editable="1"/>
    <field name="configuration_json" editable="1"/>
    <field name="down" editable="1"/>
    <field name="down_type" editable="1"/>
    <field name="dt_fracw_array" editable="1"/>
    <field name="generated" editable="1"/>
    <field name="grp" editable="1"/>
    <field name="id" editable="1"/>
    <field name="lateral_contraction_coef" editable="1"/>
    <field name="name" editable="1"/>
    <field name="t_break" editable="1"/>
    <field name="up" editable="1"/>
    <field name="up_type" editable="1"/>
    <field name="validity" editable="1"/>
    <field name="width1" editable="1"/>
    <field name="width2" editable="1"/>
    <field name="width_breach" editable="1"/>
    <field name="z_break" editable="1"/>
    <field name="z_crest1" editable="1"/>
    <field name="z_crest2" editable="1"/>
    <field name="z_invert" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="border" labelOnTop="0"/>
    <field name="break_mode" labelOnTop="0"/>
    <field name="cc" labelOnTop="0"/>
    <field name="configuration" labelOnTop="0"/>
    <field name="configuration_json" labelOnTop="0"/>
    <field name="down" labelOnTop="0"/>
    <field name="down_type" labelOnTop="0"/>
    <field name="dt_fracw_array" labelOnTop="0"/>
    <field name="generated" labelOnTop="0"/>
    <field name="grp" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="lateral_contraction_coef" labelOnTop="0"/>
    <field name="name" labelOnTop="0"/>
    <field name="t_break" labelOnTop="0"/>
    <field name="up" labelOnTop="0"/>
    <field name="up_type" labelOnTop="0"/>
    <field name="validity" labelOnTop="0"/>
    <field name="width1" labelOnTop="0"/>
    <field name="width2" labelOnTop="0"/>
    <field name="width_breach" labelOnTop="0"/>
    <field name="z_break" labelOnTop="0"/>
    <field name="z_crest1" labelOnTop="0"/>
    <field name="z_crest2" labelOnTop="0"/>
    <field name="z_invert" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>"name"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
