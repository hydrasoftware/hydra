/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  Fix Z generation for links with 3D geom       */
/* ********************************************** */

create or replace function ${model}.set_link_z_and_altitude(link_id integer)
returns integer
language plpgsql
as $$$$
    declare
        _res integer;
    begin
        perform ${model}.set_link_ext_z(link_id);

        select ${model}.set_link_altitude(link_id) into _res;

        return _res;
    end;
$$$$
;;

create or replace function ${model}.set_link_ext_z(link_id integer)
returns boolean
language plpgsql
as $$$$
    begin

        update ${model}._link as l
        set geom=ST_SetPoint(
                    ST_SetPoint(
                        l.geom,
                        0,
                        ST_MakePoint(
                            ST_X(ST_StartPoint(l.geom)),
                            ST_Y(ST_StartPoint(l.geom)),
                            ST_Z(nup.geom)
                            )
                        ),
                    -1,
                    ST_MakePoint(
                        ST_X(ST_EndPoint(l.geom)),
                        ST_Y(ST_EndPoint(l.geom)),
                        ST_Z(ndown.geom)
                        )
                    )
        from ${model}._node as nup, ${model}._node as ndown
        where l.id=link_id and nup.id=l.up and ndown.id=l.down;

        return 't';
    end;
$$$$
;;

drop function if exists ${model}.update_z();
;;

/* ********************************************** */
/*  Change coverage trigger behaviour             */
/* ********************************************** */

alter table ${model}.metadata add column if not exists trigger_coverage boolean default 't';
;;

create or replace function ${model}.constrain_after_fct()
returns trigger
language plpgsql
as $$$$
    declare
        cov integer;
    begin
        if (select trigger_coverage from $model.metadata) then
            select $model.coverage_update() into cov;
        end if;

        if tg_op = 'INSERT' or tg_op = 'UPDATE'  then
            return new;
        else
            return old;
        end if;
    end;
$$$$
;;

create or replace function ${model}.mkc_after_fct()
returns trigger
language plpgsql
as $$$$
    declare
        cov_ integer;
    begin
        if (select trigger_coverage from $model.metadata) then
            select $model.coverage_update() into cov_;
        end if;

        if tg_op = 'INSERT' or tg_op = 'UPDATE'  then
            return new;
        else
            return old;
        end if;
    end;
$$$$
;;

create or replace function ${model}.storage_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('storage', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'storage') where name = 'define_later' and id = id_;
            insert into $model._storage_node
                values (id_, 'storage', new.zs_array, new.zini, coalesce(new.contour, (select id from $model.coverage where st_intersects(geom, new.geom))));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'storage_node');

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'storage' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1)<=10 ) and (array_length(zs_array, 1) >=1) and (array_length(zs_array, 2)=2) from  $model._storage_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.zs_array, old.zini, old.contour) as o, (select new.zs_array, new.zini, new.contour) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.zs_array, new.zini, new.contour) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._storage_node set zs_array=new.zs_array, zini=new.zini, contour=new.contour where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'storage_node');

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'storage' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'storage' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1)<=10 ) and (array_length(zs_array, 1) >=1) and (array_length(zs_array, 2)=2) from  $model._storage_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'storage' = 'crossroad' and (select trigger_street_link from $model.metadata) then
                perform $model.update_street_links();
            end if;

            delete from $model._storage_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

/* ********************************************** */
/*  Change branch trigger behaviour               */
/* ********************************************** */

alter table ${model}.metadata add column if not exists trigger_branch boolean default 't';
;;

create or replace function ${model}.pipe_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('pipe', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'pipe') where name = 'define_later' and id = id_;
            insert into $model._pipe_link
                values (id_, 'pipe', new.comment, new.z_invert_up, new.z_invert_down, coalesce(new.cross_section_type, 'circular'), coalesce(new.h_sable, 0), new.branch, new.rk, coalesce(new.custom_length, null), new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom);
            if 'pipe' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'pipe_link');
            update $model._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (h_sable is not null) and (cross_section_type is not null) and (cross_section_type not in ('valley')) and (rk is not null) and (rk >0) and (cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 )) and (cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)) and (cross_section_type!='pipe' or cp_geom is not null) and (cross_section_type!='channel' or op_geom is not null) from  $model._pipe_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' then
                update $model.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.comment, old.z_invert_up, old.z_invert_down, old.cross_section_type, old.h_sable, old.branch, old.rk, old.custom_length, old.circular_diameter, old.ovoid_height, old.ovoid_top_diameter, old.ovoid_invert_diameter, old.cp_geom, old.op_geom) as o, (select new.comment, new.z_invert_up, new.z_invert_down, new.cross_section_type, new.h_sable, new.branch, new.rk, new.custom_length, new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.comment, new.z_invert_up, new.z_invert_down, new.cross_section_type, new.h_sable, new.branch, new.rk, new.custom_length, new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._pipe_link set comment=new.comment, z_invert_up=new.z_invert_up, z_invert_down=new.z_invert_down, cross_section_type=new.cross_section_type, h_sable=new.h_sable, branch=new.branch, rk=new.rk, custom_length=new.custom_length, circular_diameter=new.circular_diameter, ovoid_height=new.ovoid_height, ovoid_top_diameter=new.ovoid_top_diameter, ovoid_invert_diameter=new.ovoid_invert_diameter, cp_geom=new.cp_geom, op_geom=new.op_geom where id=old.id;

            if 'pipe' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) and (select trigger_branch from $model.metadata) then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'pipe_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update $model.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (h_sable is not null) and (cross_section_type is not null) and (cross_section_type not in ('valley')) and (rk is not null) and (rk >0) and (cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 )) and (cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)) and (cross_section_type!='pipe' or cp_geom is not null) and (cross_section_type!='channel' or op_geom is not null) from  $model._pipe_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._pipe_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'pipe' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' then
                update $model.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;

create or replace function ${model}.pipe_branch_marker_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'pipe_branch_marker', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'pipe_branch_marker') where name = 'define_later' and id = id_;

            insert into $model._pipe_branch_marker_singularity
                values (id_, 'pipe_branch_marker', coalesce(new.pk0_km, 0), coalesce(new.dx, 50));
            if 'pipe_branch_marker' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'pipe_branch_marker_singularity');
            update $model._singularity set validity = (select (pk0_km is not null) and (dx is not null) and (dx>=0.1) from  $model._pipe_branch_marker_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.pk0_km, old.dx) as o, (select new.pk0_km, new.dx) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.pk0_km, new.dx) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._pipe_branch_marker_singularity set pk0_km=new.pk0_km, dx=new.dx where id=old.id;
            if 'pipe_branch_marker' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'pipe_branch_marker_singularity');
            update $model._singularity set validity = (select (pk0_km is not null) and (dx is not null) and (dx>=0.1) from  $model._pipe_branch_marker_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._pipe_branch_marker_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'pipe_branch_marker' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$$$
;;

/* ********************************************** */
/*  Change street links trigger behaviour         */
/* ********************************************** */

alter table ${model}.metadata add column if not exists trigger_street_link boolean default 't';
;;

create or replace function ${model}.crossroad_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('crossroad', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'crossroad') where name = 'define_later' and id = id_;
            insert into $model._crossroad_node
                values (id_, 'crossroad', coalesce(new.area, 1), coalesce(new.z_ground, (select project.altitude(new.geom))), coalesce(new.h, 0));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'crossroad_node');

            -- Lines to update specific nodes that works with associated contours
            if 'crossroad' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            if 'crossroad' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) and (h is not null) and (h>=0) from  $model._crossroad_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.z_ground, old.h) as o, (select new.area, new.z_ground, new.h) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.z_ground, new.h) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._crossroad_node set area=new.area, z_ground=new.z_ground, h=new.h where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'crossroad_node');

            -- Lines to update specific nodes that works with associated contours
            if 'crossroad' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'crossroad' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;

            if 'crossroad' = 'crossroad' and new.generated is null and (select trigger_street_link from $model.metadata) then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) and (h is not null) and (h>=0) from  $model._crossroad_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'crossroad' = 'storage' then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            if 'crossroad' = 'crossroad' and (select trigger_street_link from $model.metadata) then
                perform $model.update_street_links();
            end if;

            delete from $model._crossroad_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

/* ********************************************** */
/*  QCap bug on pipes                             */
/* ********************************************** */

create or replace function ${model}.cp_geometric_calc_p_fct(real[])
returns double precision
language plpgsql immutable
as
$$$$
    declare
        z_array alias for $$1;
        x real[];
        s double precision := 0.0;
        last_h double precision := 0.0;
        last_w double precision := 0.0;
        w1 double precision := 0.0;
    begin
        foreach x slice 1 in array z_array
        loop
            s:=s+2*sqrt(pow(x[2]-last_w,2)+pow(x[1]-last_h,2));
            last_h:=x[1];
            last_w:=x[2];
        end loop;
        s:=s+z_array[1][2]+z_array[array_length(z_array, 1)][2];
        return s;
    end;
$$$$
;;

create or replace function ${model}.op_geometric_calc_p_fct(real[])
returns double precision
language plpgsql immutable
as
$$$$
    declare
        z_array alias for $$1;
        x real[];
        s double precision := 0.0;
        last_h double precision := 0.0;
        last_w double precision := 0.0;
        w1 double precision := 0.0;
    begin
        foreach x slice 1 in array z_array
        loop
            s:=s+2*sqrt(pow(x[2]-last_w,2)+pow(x[1]-last_h,2));
            last_h:=x[1];
            last_w:=x[2];
        end loop;
        s:=s+z_array[1][2];
        return s;
    end;
$$$$
;;