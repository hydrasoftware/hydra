/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*              Connectivity  checks              */
/* ********************************************** */

create function ${model}.check_on_branch_or_reach_endpoint(geom geometry)
returns boolean
language plpgsql
as $$$$
    declare
        res_ boolean;
    begin
        with forbidden as (select ST_StartPoint(reach.geom) as point from $model.reach
                               union
                               select ST_EndPoint(reach.geom) as point from $model.reach
                               union
                               select ST_StartPoint(branch.geom) as point from $model.branch
                               union
                               select ST_EndPoint(branch.geom) as point from $model.branch
                               )
        select exists(select 1 from forbidden where ST_Intersects(geom, forbidden.point)) into res_;
        return res_;
    end;
$$$$
;;

/* ********************************************** */
/*            Constrain line spliting             */
/* ********************************************** */

create function $model.split_constrain(constrain_id integer, point geometry('POINTZ', $srid))
returns boolean
language plpgsql
as $$$$
    declare
        _geom geometry('LINESTRINGZ', $srid);
        _elem_length real;
        _constrain_type hydra_constrain_type ;
        _link_attributes json;
    begin
        update $model.metadata set trigger_coverage=False;

        select geom, elem_length, constrain_type, link_attributes from $model.constrain where id=constrain_id into _geom, _elem_length, _constrain_type, _link_attributes;

        delete from $model.constrain where id=constrain_id;

        insert into $model.constrain(geom, elem_length, constrain_type, link_attributes)
            values (
                    ST_SetSRID((
                        ST_Dump(
                            ST_Split(
                                ST_SetSRID(ST_Snap(_geom, point,0.0001), $srid),
                                ST_SetSRID(point, $srid)
                                )
                            )
                        ).geom, $srid),
                    _elem_length,
                    _constrain_type,
                    _link_attributes
                    );

        update $model.metadata set trigger_coverage=True;
        perform $model.coverage_update();
    return true;
    end;
$$$$
;;

/* ********************************************** */
/*            Coverage regeneration               */
/* ********************************************** */

create function $model.coverage_update()
returns integer
language plpgsql volatile security definer
as $$$$
    declare
        res_ integer;
    begin
        delete from $model.coverage ; -- where domain_2d is null;
        alter sequence $model.coverage_id_seq restart;

        with cst as (
            select ST_Node(ST_Collect(a.discretized)) as geom
            from $model.constrain as a
            where a.constrain_type is distinct from 'ignored_for_coverages'
            )
        insert into $model.coverage(geom, domain_type) select (st_dump(st_polygonize(geom))).geom, '2d' from cst;

        with oned as (
            select c.id
            from $model.coverage as c, $model.street as d
            where ST_Intersects(c.geom, d.geom)
            group by c.id
        )
        update $model.coverage set domain_type='street' where id in (select oned.id from oned);

        update $model.coverage as c set domain_type='reach'
        where exists (select 1 from $model.open_reach as r
        where ST_Intersects(c.geom, r.geom)
        and ST_Length(ST_CollectionExtract(ST_Intersection(c.geom, r.geom), 2)) > 0);

        with oned as (
            select distinct c.id
            from $model.coverage as c, $model.storage_node as d
            where ST_Intersects(c.geom, d.geom)
        )
        update $model.coverage set domain_type='storage' where id in (select oned.id from oned);

        with oned as (
            select c.id, count(1) as ct
            from $model.coverage as c, $model.coverage_marker as mkc
            where ST_Intersects(c.geom, mkc.geom)
            group by c.id
        )
        update $model.coverage set domain_type=null where id in (select oned.id from oned where ct=1);

        select count(*) from $model.coverage into res_;
        return res_;
    end;
$$$$
;;

/* ********************************************** */
/*              Street regeneration               */
/* ********************************************** */

create function ${model}.crossroad_update()
returns integer
language plpgsql
as $$$$
    declare
        res_ integer;
    begin
        with nodes as (
            insert into $model.crossroad_node(geom)
                (select distinct s.geom
                    from (
                        select (st_dumppoints(geom)).geom
                        from $model.street
                            union
                        select (st_dumppoints(st_intersection(s1.geom, s2.geom))).geom as geom
                        from $model.street as s1, $model.street as s2
                        where ST_intersects(s1.geom,s2.geom)
                        and s1.id<s2.id
                    ) as s
                    join hydra.metadata m on true
                    left join (select st_force2d(geom) as geom from $model.crossroad_node) as c
                    on st_dwithin(s.geom, c.geom, m.precision)
                    where c.geom is null)
            returning id
        )
        select count(1) from nodes into res_;
        return res_;
    end;
$$$$
;;

create function ${model}.gen_cst_street(street_id integer)
returns integer
language plpgsql
as $$$$
    declare
        _street_geom geometry;
        _street_width real;
        _street_el real;
        _trigger_state boolean;
    begin
        select geom, width, elem_length
        from $model.street
        where id=street_id
        into _street_geom, _street_width, _street_el;

        select trigger_coverage from $model.metadata into _trigger_state;
        update $model.metadata set trigger_coverage = 'f';

        with buf as (
                -- select ST_Buffer(ST_Force2d(_street_geom), _street_width/2, 'quad_segs=1 join=mitre mitre_limit='||(_street_width*2)::varchar) as geom
                -- workaroud for postgis 3
                select ST_Union(
                    ST_Difference(st_buffer(_street_geom, _street_width/2, 'quad_segs=1'), st_buffer(_street_geom, _street_width/2, 'quad_segs=1 endcap=flat')), st_buffer(_street_geom, _street_width/2, 'join=mitre endcap=flat mitre_limit='||(_street_width*2)::varchar)) as geom
            ),
            cst as (
                select ST_ExteriorRing(geom) as geom from buf
            ),
            spl as (
                select coalesce(ST_Split(cst.geom, (select ST_Collect(ST_Force2d(c.geom))
                                                    from $model.constrain as c
                                                    where ST_Intersects(c.geom, cst.geom) and c.constrain_type is distinct from 'ignored_for_coverages')
                                                    ), cst.geom) as geom
                from cst
            ),
            spld as (
                select ST_Force3d((st_dump(geom)).geom) as geom from spl
            ),
            diff as (
                select id, (ST_Dump(ST_CollectionExtract(ST_Split(c.geom, b.geom), 2))).geom as geom
                from $model.constrain as c, cst as b
                where ST_Intersects(b.geom, c.geom)
                and c.constrain_type is distinct from 'ignored_for_coverages'
            ),
            ins as (
                insert into $model.constrain(geom, elem_length, constrain_type)
                select d.geom, c.elem_length, c.constrain_type
                from $model.constrain as c, diff as d
                where c.id = d.id
                union
                select geom, _street_el, 'overflow'
                from spld
                returning id
            )
        delete from $model.constrain
        where id in (select id from diff);

        with cst_brut as (
                select a.id, (ST_Dump(coalesce(ST_Split(a.discretized,
                    (select ST_Collect(discretized) from $model.constrain as b
                        where a.id!=b.id
                        and ST_Intersects(a.discretized, b.discretized)
                        and b.constrain_type is distinct from 'ignored_for_coverages'
                        and ST_Dimension(ST_Intersection(a.discretized, b.discretized))=0)
                    ),
                a.discretized))).geom as geom
                from $model.constrain as a
                where a.constrain_type is distinct from 'ignored_for_coverages'
            ),
            cst as (
                select id, ST_Union(geom) over (partition by id) as geom from cst_brut
            ),
            polyg as (
                select (ST_Dump(ST_Polygonize(cst_brut.geom))).geom as geom from cst_brut
            ),
            inter as (
                select p.geom from polyg as p, $model.street as s
                where ST_Intersects(p.geom, s.geom)
            ),
            un as (
                select ST_Union(i.geom) as geom from inter as i
            ),
            boundary as (
                select ST_Boundary(u.geom) as geom from un as u
            ),
            to_del as (
                select id from cst as c
                where (select ST_Dimension(ST_Intersection(c.geom, b.geom))=0 from boundary as b)
                and (select ST_Contains(u.geom, c.geom) from un as u)
            )
            delete from $model.constrain as c
            where exists (select 1 from to_del as d where d.id=c.id limit 1)
            and not exists (select 1 from $model.valley_cross_section_geometry where transect=c.id limit 1)
            and c.constrain_type is distinct from 'ignored_for_coverages'
            and c.constrain_type is not null;


        if _trigger_state then
            perform $model.coverage_update();
        end if;

        update $model.metadata set trigger_coverage = _trigger_state;

        return 1;
    end;
$$$$
;;

/* ********************************************** */
/*                      Mesh tools                */
/* ********************************************** */

create function ${model}.create_links(up_domain_id integer, down_domain_id integer)
returns integer
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_links
    return create_links(plpy, up_domain_id, down_domain_id, '$model', $srid)
$$$$
;;

create function ${model}.create_boundary_links(up_type varchar, up_id integer, station_node_id integer)
returns integer
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_boundary_links
    return create_boundary_links(plpy, up_type, up_id, station_node_id, '$model', $srid)
$$$$
;;

create function ${model}.create_elem2d_links(elem_id integer, border geometry, generation_step integer default null)
returns integer
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_elem2d_links
    return create_elem2d_links(plpy, elem_id, border, '$model', $srid, generation_step)
$$$$
;;

create function ${model}.create_network_overflow_links(domain_id integer)
returns integer
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_network_overflow_links
    return create_network_overflow_links(plpy, domain_id, '$model', $srid)
$$$$
;;

create or replace function ${model}.mesh(domain_id integer)
returns integer
language plpgsql as
$$$$
declare
    cfg integer;
    lnk integer;
    rec record;
    domain_2d_id integer;
begin
    select configuration from ${model}.metadata into cfg;
    update ${model}.metadata set configuration=1;

    select m.domain_2d
    from $model.coverage c
    left join $model.domain_2d_marker m on st_intersects(c.geom, m.geom)
    where c.id=domain_id 
    into domain_2d_id;

    with meshable as (
        select
            p.geom as polygon,
            st_multi(st_difference(ct.geom, rg.geom)) as constraints,
            rg.geom as border
        from ${model}.coverage p
        cross join lateral (
            select st_union(geom) as geom from (
                select st_intersection(discretized, p.geom) as geom
                from ${model}.constrain c
                where st_intersects(p.geom, c.discretized)
                and (c.constrain_type is null or c.constrain_type != 'ignored_for_coverages')
                ) t
            ) ct
        cross join lateral (
            select st_collect(st_exteriorring(r.geom)) as geom from st_dumprings(p.geom) r
            ) as rg
        where domain_type='2d'
        and p.id = domain_id
        and not exists (select 1 from ${model}.elem_2d_node n where st_intersects(p.geom, n.geom))
    ),
    tess as (
        select vertex, face from meshable cross join lateral project.tesselate(polygon, constrains=>constraints, has_quads=>true)
    ),
    gen as (
        insert into ${model}.generation_step default values returning id
    ),
    msh as (
        insert into ${model}.elem_2d_node(contour, generated, domain_2d)
        select st_makepolygon(st_makeline(st_force3d(st_geometryn(tess.vertex, i), project.altitude(st_geometryn(tess.vertex, i))))), gen.id, domain_2d_id
        from tess
        cross join gen
        cross join lateral generate_series(1, array_length(tess.face, 1)) f
        cross join lateral unnest(tess.face[f:f][:]) i
        where i is not null
        group by f, gen.id
        returning id, contour, geom, zb
    ),
    msh_lnk as (
        select ${model}.create_elem2d_links(n1.id, (st_dump(st_collectionextract(st_intersection(n1.contour, n2.contour), 2))).geom, gen.id)
        from msh as n1
        , msh as n2
        , gen
        where st_intersects(n1.contour, n2.contour)
        and st_length(st_collectionextract(st_intersection(n1.contour, n2.contour), 2)) > 0
        and n1.id > n2.id
    ),
    brd_lnk as (
        select ${model}.create_elem2d_links(e.id, (st_dump(st_collectionextract(st_intersection(e.contour, b.border), 2))).geom, gen.id)
                from msh as e
                , meshable as b
                , gen
                where st_intersects(e.contour, b.border)
                and st_length(st_collectionextract(st_intersection(e.contour, b.border), 2)) > 0
    ),
    exterior as (
        select e.id as id, ST_Difference(e.contour, ST_buffer(e.contour, -sqrt(ST_Area(e.contour))/4.)) as geom
        from msh as e
    ),
    reach_segments as (
        select distinct on (elem_id, reach_id) e.id as elem_id, c.reach as reach_id, (st_dump(st_intersection(e.contour, c.geom))).geom as geom, (st_dump(st_intersection(ext.geom, c.geom))).geom as limited_geom
        from ${model}.channel_reach as c
        , msh as e
        join exterior as ext on e.id=ext.id
        where st_intersects(e.contour, c.geom)
    ),
    generated_node as (
        select elem_id, reach_id, ST_LineInterpolatePoint(limited_geom, .5) as geom
        from reach_segments as s
    ),
    new_node as (
        select n.elem_id, n.reach_id, project.set_altitude(st_force3d(n.geom)) as geom, e.geom as elem_geom
        from generated_node as n
        join msh as e on n.elem_id=e.id
        where not exists (select 1 from ${model}.river_node as r where r.reach=n.reach_id and ST_Contains(e.contour, r.geom))
    ),
    ins_node as (
        insert into ${model}.river_node(geom, generated, reach)
        select geom, gen.id, reach_id
        from new_node
        , gen
        returning id, geom, reach
    ),
    links_geom as (
        select distinct on (id, reach) n.geom as g1, e.geom as g2, e.id, n.reach
            from ${model}.river_node as n
            , msh as e
            where st_contains(e.contour, n.geom)
        union
        select i.geom as g1, e.geom as g2, e.id, i.reach
            from ins_node as i
            , msh as e
            where st_contains(e.contour, i.geom)
        order by id
    ),
    immersed_channel as (
        insert into ${model}.overflow_link(geom, generated, z_crest1, width1, z_crest2, width2)
        select st_makeline(ARRAY[l.g1, st_centroid(st_union(l.g1, l.g2))]),gen.id, e.zb, 0, e.zb, st_length(s.geom)*2
        from links_geom as l
        , msh as e
        , gen
        , reach_segments as s
        where l.id=e.id and l.id = s.elem_id
        and s.reach_id = l.reach
        returning id
    )
    select count(1) from (select 1 from brd_lnk union all select 1 from msh_lnk union select 1 from immersed_channel) t into lnk
    ;

    update ${model}.metadata set configuration=cfg;

    update ${model}.gate_link l set geom=l.geom
    from ${model}.coverage c
    where l.generated is null and st_intersects(c.geom, l.geom) and c.id = domain_id;

    update ${model}.overflow_link l set geom=l.geom
    from ${model}.coverage c
    where l.generated is null and st_intersects(c.geom, l.geom) and c.id = domain_id;

    -- fake update to re-attach links
    for rec in select unnest(enum_range(null::hydra_link_type)) as type_ loop
        execute 'update ${model}.'||rec.type_||'_link l set geom=l.geom '||
        'from ${model}.coverage c '||
        'where l.generated is null and st_intersects(c.geom, l.geom) and c.id = '||domain_id;
    end loop;

    return 0;
end;
$$$$
;;


/* ********************************************** */
/*                Cahtchment autofill             */
/* ********************************************** */


drop function if exists ${model}.auto_param_catchment
;;

drop type if exists hydra_catchment_param
;;

drop type if exists hydra.hydra_catchment_param
;;

create function ${model}.auto_param_catchment(catchment_id integer)
returns table(area real, rl real, slope real, c_imp real)
language plpgsql
as
$$$$
    begin
        select ST_Area(c.geom)
        from $model.catchment_node as n, $model.catchment as c
        where n.id=catchment_id and ST_Contains(c.geom, n.geom)
        into area;

        select ST_Length(l.geom), Greatest(( project.altitude(ST_StartPoint(l.geom)) - project.altitude(ST_EndPoint(l.geom)) ) / ST_Length(l.geom), 0.005)
        from $model.catchment_node as n, $model.routing_hydrology_link as l
        where n.id=catchment_id and l.up=n.id
        into rl, slope;

        select Sum((ST_Area(ST_Intersection(c.geom, lo.geom)) / ST_Area(c.geom)) * t.c_imp)
	    from $model.catchment_node as n, $model.catchment as c, project.land_occupation as lo
	    join project.land_type as t on t.land_type=lo.land_type
	    where n.id=catchment_id and ST_Contains(c.geom, n.geom) and ST_Intersects(c.geom, lo.geom)
	    into c_imp;

        return next;
        return;
    end;
$$$$
;;

/* ********************************************** */
/*                Hydrology checks                */
/* ********************************************** */

create function ${model}.check_hydrogramme_apport_on_down_route_fct()
returns boolean
language plpgsql
as $$$$
    declare
        res boolean;
    begin
        /* apres: routing with hydrograms at down */
        with routing_with_hydrogramme_apport_at_down as (
            select r.id as rid from $model._link as r, $model._hydrograph_bc_singularity as a
            where r.down=a.id and r.link_type=$routing_hydrology_link_type
        )
        select count(1)=0 from $model._link as r
        where r.down_type!=$manhole_hydrology_node_type and r.link_type=$routing_hydrology_link_type
        and r.id not in (select rid from routing_with_hydrogramme_apport_at_down) into res;
        return res;
    end;
$$$$
;;

create function ${model}.check_catchment_has_one_route_fct()
returns boolean
language plpgsql
as $$$$
    declare
        res boolean;
    begin
        with link as (
            select count(1) as count from $model.catchment_node as bv, $model._link as r
            where bv.id=r.up and r.link_type=$routing_hydrology_link_type
            group by r.id)
        select max(count)=min(count) and min(count)=1 from link into res;
        return res;
    end;
$$$$
;;

/* ********************************************** */
/*                    Profile                     */
/* ********************************************** */

create function ${model}.update_river_cross_section_pl1d(coverage_id integer)
returns integer
language plpgsql
as $$$$
    declare
        prec_ real;
        res_ integer;
        reach_id_ integer;
    begin
        select precision from hydra.metadata into prec_;

        select r.id
        from $model.coverage as c, $model.reach as r
        where ST_Intersects(r.geom, c.geom)
        and c.id=coverage_id
        and ST_Intersects(r.geom, c.geom)
        limit 1
        into reach_id_;

        delete from $model.river_cross_section_pl1d
        where generated is not null;

        -- create banks
        drop table if exists banks;
        create  table banks
        as
        with res as (
            select (st_dump(st_linemerge((ST_CollectionExtract(ST_Intersection(cs.discretized, cv.geom), 2))))).geom as geom
            from $model.coverage as cv, $model.constrain as cs, $model.reach as r
            where ST_Intersects(r.geom, cv.geom)
            and cv.id=coverage_id
            and r.id = reach_id_
            and not ST_Intersects(r.geom, cs.geom)
            and cs.constrain_type is distinct from 'ignored_for_coverages'
        )
        select row_number() over() as id, geom::geometry('LINESTRINGZ', $srid) from res
        ;


        -- river points that form the bedline are the one
        -- which are the closest point of their projection and the banks
        drop table if exists bedline;
        create  table bedline
        as
        with projected as (
            select b.id as bank, ST_LineInterpolatePoint(b.geom, ST_LineLocatePoint(b.geom, r.geom)) as geom
            from (
                select (st_dumppoints(geom)).geom as geom
                from $model.reach where id=reach_id_
                union
                select n.geom
                from $model.river_node as n
                where n.reach=reach_id_
                ) as r, banks as b
            union
            select b.id as bank, (st_dumppoints(b.geom)).geom as geom
            from banks as b
        ),
        closest as (
            select b.bank, ST_LineInterpolatePoint(r.geom, ST_LineLocatePoint(r.geom, b.geom)) as geom
            from $model.reach as r, projected as b
            where r.id=reach_id_
        )
        select row_number() over() as id, c.bank, ST_MakeLine(c.geom order by ST_LineLocatePoint(b.geom, c.geom)) as geom
        from closest as c, banks as b
        where c.bank=b.id
        group by c.bank
        ;

        drop table if exists midline;
        create  table midline
        as
        select row_number() over() as id, ST_MakeLine(st_centroid(ST_MakeLine(p.geom, ST_ClosestPoint(b.geom, p.geom)))) as geom, p.bank
        from banks as b, (select (st_dumppoints(geom)).geom as geom, bank from bedline) as p
        where b.id=p.bank
        group by p.bank
        ;

        drop table if exists pl1d;
        create  table pl1d
        as
        select  row_number() over() as id,
            ST_MakeLine(ST_LineInterpolatePoint((select geom from banks where id = 1), i::real/10),
                        ST_LineInterpolatePoint((select geom from banks where id = 2), i::real/10)) as geom,
                    1 as node
        from generate_series(1, 10) as i
        ;
        return 1;
    end;
$$$$
;;

-- plpython3u doesn't support multidim arrays yet, so we pass it as string and cast hence the use of _street_after_insert_trig , ignore_points=False, distance_point=100, discretization=150)
create function ${model}._str_profile_from_transect(flood_plain_transect geometry, interpolate boolean default 'f'::boolean, discretization integer default '150'::integer, ignore_points boolean default 'f'::boolean, distance_point integer default '100'::integer)
returns varchar
language plpython3u immutable
as
$$$$
    import plpy
    from $hydra import profile_from_transect
    return profile_from_transect(plpy, flood_plain_transect, '$model', $srid, interpolate, discretization, ignore_points, distance_point)
$$$$
;;

create function ${model}.profile_from_transect(flood_plain_transect geometry, interpolate boolean default 'f'::boolean, discretization integer default '150'::integer, ignore_points boolean default 'f'::boolean, distance_point integer default '100'::integer)
returns real[][]
language plpgsql immutable
as
$$$$
    begin
        return $model._str_profile_from_transect(flood_plain_transect, interpolate, discretization, ignore_points, distance_point)::real[][];
    end;
$$$$
;;

-- closest section on the same reach
create function $model.upstream_valley_section(transect geometry)
returns integer
language plpgsql immutable
as
$$$$
    begin
        return (
        with reach as (
            select r.id, ST_Intersection(r.geom, transect) as inter, geom, pk0_km
            from ${model}.reach as r
            where ST_Intersects(r.geom, transect)
        ),
        pk as (
            select ST_LineLocatePoint(geom, inter)*ST_Length(geom)/1000+pk0_km as pk_km from reach
        ),
        up_reach as (
            select nup.reach as id
            from ${model}.river_node as nup
            join ${model}.connector_link as l on l.up=nup.id
            join ${model}.river_node as ndown on ndown.id=l.down
            where l.main_branch
            and ndown.reach = (select id from reach)
        ),
        extended_reach_node as (
            select n.id, n.pk_km
            from ${model}.river_node as n, pk
            where n.reach in (select id from reach union select id from up_reach)
            and n.pk_km <= pk.pk_km
        ),
        section_up as (
            select c.id
            from extended_reach_node as n
            join ${model}.river_cross_section_profile as c on c.id = n.id,
            pk
            where ((c.type_cross_section_up='valley' and c.up_vcs_geom is not null)
                or (c.type_cross_section_down='valley' and c.down_vcs_geom is not null))
            order by n.pk_km desc
            limit 1
        )
        select id from section_up);
    end;
$$$$
;;

/* ********************************************** */
/*                Altitude handling               */
/* ********************************************** */

create function ${model}.set_link_z_and_altitude(link_id integer)
returns integer
language plpgsql
as $$$$
    declare
        _res integer;
    begin
        perform ${model}.set_link_ext_z(link_id);

        select ${model}.set_link_altitude(link_id) into _res;

        return _res;
    end;
$$$$
;;

create function ${model}.set_link_altitude(link_id integer)
returns integer
language plpython3u
as
$$$$
    import plpy
    from $hydra import set_link_altitude
    return set_link_altitude(plpy, link_id, '$model', $srid)
$$$$
;;

create function ${model}.set_link_altitude_topo(link_id integer)
returns integer
language plpython3u
as
$$$$
    import plpy
    from $hydra import set_link_altitude_topo
    return set_link_altitude_topo(plpy, link_id, '$model', $srid)
$$$$
;;

create function ${model}.set_link_ext_z(link_id integer)
returns boolean
language plpgsql
as $$$$
    begin

        update ${model}._link as l
        set geom=ST_SetPoint(
                    ST_SetPoint(
                        l.geom,
                        0,
                        ST_MakePoint(
                            ST_X(ST_StartPoint(l.geom)),
                            ST_Y(ST_StartPoint(l.geom)),
                            ST_Z(nup.geom)
                            )
                        ),
                    -1,
                    ST_MakePoint(
                        ST_X(ST_EndPoint(l.geom)),
                        ST_Y(ST_EndPoint(l.geom)),
                        ST_Z(ndown.geom)
                        )
                    )
        from ${model}._node as nup, ${model}._node as ndown
        where l.id=link_id and nup.id=l.up and ndown.id=l.down;

        return 't';
    end;
$$$$
;;

create function ${model}.line_set_z(
    in line geometry('LINESTRINGZ', $srid),
    in z_up real,
    in z_down real)
returns geometry('LINESTRINGZ', $srid)
language plpgsql
as $$$$
    declare
        geom geometry('LINESTRINGZ', $srid);
    begin
        with d as (select (ST_DumpPoints(line)).geom as geom),
        p as (select ST_X(d.geom) as x, ST_Y(d.geom) as y,
            ST_LineLocatePoint(line, d.geom) as a
            from d),
        i as (select x, y, (1-a)*z_up + a*z_down as z, a from p)
        select ST_SetSRID(ST_MakeLine(ST_MakePoint(x, y, z) order by a), $srid) as geom from i into geom;
        return geom;
    end;
$$$$
;;

/* ********************************************** */
/*                River transects                 */
/* ********************************************** */

-- for any point close to riverbank, give the transect interpolated at this point
-- for points on the border bewteen two reach coverage, you can specify the coverage id
create function ${model}.interpolate_transect_at(point geometry, coverage_id integer default null)
returns geometry('LINESTRINGZ', $srid)
language plpgsql
immutable
as $$$$
    declare
        l_ real;
        coverage geometry;
        reach geometry;
        res geometry;
        left_transects geometry;
        right_transects geometry;
        closest geometry;
        direction vector2;
        other_direction vector2;
        other_side geometry;
        side varchar;
    begin
        l_ := 10000;

        --raise notice 'point %', point;
        select ST_CollectionHomogenize(ST_Collect(ST_MakeLine(array[ST_StartPoint(discretized), ST_ClosestPoint(discretized, reach), ST_EndPoint(discretized)])))
        from ${model}.constrain
        where constrain_type='flood_plain_transect'
        and ST_Intersects(discretized, point)
        and constrain_type is distinct from 'ignored_for_coverages'
        into res;

        if ST_GeometryType(res) = 'ST_LineString' then
            return (
                select ST_MakeLine(array[ST_StartPoint(res), ST_ClosestPoint(r.geom, res), ST_EndPoint(res)])
                from ${model}.reach r
                order by r.geom <-> res
                limit 1
            );
        end if;

        -- closest coverage or specified one
        select coalesce(
            (select geom from ${model}.coverage where id=coverage_id),
            (select geom from ${model}.coverage where domain_type='reach' order by geom <-> point limit 1)
        ) into coverage;

        if not ST_DWithin(coverage, point, .1) then
            raise 'point % is not close enought to nearest coverage %', point, coverage ;
        end if;

        -- closest reach in coverage
        select geom from ${model}.reach where ST_Intersects(geom, coverage) order by geom <-> point limit 1 into reach;

        if reach is null then
            raise 'no reach in reach coverage %', coverage ;
        end if;

        with pt as (
            select ST_StartPoint(t.geom) as s, ST_ClosestPoint(t.geom, reach) as m, ST_EndPoint(t.geom) as e
            from ${model}.constrain as t
            where ST_Intersects(coverage, t.discretized)
            and t.constrain_type='flood_plain_transect'
            and ST_Length(ST_CollectionExtract(ST_Intersection(t.discretized, coverage), 2)) > 0
            order by ST_Distance(geom, reach)
            limit 2
        )
        select ST_Collect(ST_MakeLine(s, m)), ST_Collect(ST_MakeLine(e, m))
        from pt
        into left_transects, right_transects;

        if ST_NumGeometries(left_transects) = 0 then
            return ST_MakeLine(point, ST_ClosestPoint(reach, point));
        end if;

        select ST_ClosestPoint(reach, point) into closest;
        if dot(difference(ST_EndPoint(ST_GeometryN(right_transects,1)), ST_StartPoint(ST_GeometryN(right_transects,1))), difference(closest, point)) > 0 then
            side := 'right';
        else
            side := 'left';
        end if;

        if ST_NumGeometries(left_transects) = 1 then
            if side = 'right' then
                direction := difference(ST_EndPoint(ST_GeometryN(right_transects,1)), ST_StartPoint(ST_GeometryN(right_transects,1)));
            else
                direction := difference(ST_EndPoint(ST_GeometryN(left_transects,1)), ST_StartPoint(ST_GeometryN(left_transects,1)));
            end if;
            other_direction := direction;
        else
            if side = 'right' then
                direction := interpolate_direction( ST_GeometryN(right_transects,1), ST_GeometryN(right_transects,2), point);
            else
                direction := interpolate_direction( ST_GeometryN(left_transects,1), ST_GeometryN(left_transects,2), point);
            end if;
        end if;

        closest := coalesce(ST_ClosestPoint(ST_Intersection(ST_MakeLine(point, ST_Translate(point, l_*direction.x, l_*direction.y)), reach), point), closest);
        --raise notice 'point % closest %', point, closest;

        if ST_NumGeometries(left_transects) = 1 then
            other_direction := row(-direction.x, -direction.y)::vector2;
        else
            if side = 'right' then
                other_direction := interpolate_direction( ST_GeometryN(left_transects,1), ST_GeometryN(left_transects,2), closest);
            else
                other_direction := interpolate_direction( ST_GeometryN(right_transects,1), ST_GeometryN(right_transects,2), closest);
            end if;
        end if;

        other_side := ST_ClosestPoint(ST_Intersection(ST_MakeLine(closest, ST_Translate(closest, -l_*other_direction.x, -l_*other_direction.y)), ST_ExteriorRing(coverage)), closest);

        if side = 'right' then
            return ST_MakeLine(array[other_side, closest, point]);
        else
            return ST_MakeLine(array[point, closest, other_side]);
        end if;

    end;
$$$$
;;

drop function if exists ${model}.valley_section_at
;;

drop type if exists hydra_record_section
;;
drop type if exists hydra.hydra_record_section
;;

create function ${model}.valley_section_at(point geometry)
returns table(section real[], frac_maj_l real, frac_min real, frac_maj_r real)
language plpgsql
as
$$$$
begin
    return query
    with reach as (
        select id, geom, ST_LineLocatePoint(geom, point) t from  ${model}.reach where ST_DWithin(geom, point, .1)
    ),
    up_sect as (
        select zbmaj_lbank_array as z_maj_l, zbmin_array as z_min, zbmaj_rbank_array as z_maj_r, ST_LineLocatePoint(r.geom, p.geom) as t
        from  reach as r, ${model}.river_cross_section_profile as p -- TODO use z_invert_up
            join  ${model}.river_node as n on n.id=p.id
            join  ${model}.valley_cross_section_geometry as u on coalesce(p.down_vcs_geom, p.up_vcs_geom) = u.id
        where r.id=n.reach
        and ST_LineLocatePoint(r.geom, p.geom) <= ST_LineLocatePoint(r.geom, point)
        order by ST_LineLocatePoint(r.geom, p.geom) desc limit 1
    ),
    up_pt as (
        select zb_cat(z_maj_l, z_min, z_maj_r) as zb, t,
            z_maj_l[array_length(z_maj_l,1)][2] / (z_maj_l[array_length(z_maj_l,1)][2]+z_min[array_length(z_min,1)][2]+z_maj_r[array_length(z_maj_r,1)][2]) as frac_l,
            z_min  [array_length(z_min,1)][2]   / (z_maj_l[array_length(z_maj_l,1)][2]+z_min[array_length(z_min,1)][2]+z_maj_r[array_length(z_maj_r,1)][2]) as frac_0,
            z_maj_r[array_length(z_maj_r,1)][2] / (z_maj_l[array_length(z_maj_l,1)][2]+z_min[array_length(z_min,1)][2]+z_maj_r[array_length(z_maj_r,1)][2]) as frac_r
        from  up_sect
    ),
    down_sect as (
        select zbmaj_lbank_array as z_maj_l, zbmin_array as z_min, zbmaj_rbank_array as z_maj_r, ST_LineLocatePoint(r.geom, p.geom) as t
        from  reach as r, ${model}.river_cross_section_profile as p
            join  ${model}.river_node as n on n.id=p.id
            join  ${model}.valley_cross_section_geometry as d on coalesce(p.up_vcs_geom, p.down_vcs_geom) = d.id
        where r.id=n.reach
        and ST_LineLocatePoint(r.geom, p.geom) >= ST_LineLocatePoint(r.geom, point)
        order by ST_LineLocatePoint(r.geom, p.geom) desc limit 1
    ),
    down_pt as (
        select zb_cat(z_maj_l, z_min, z_maj_r) as zb, t,
            z_maj_l[array_length(z_maj_l,1)][2] / (z_maj_l[array_length(z_maj_l,1)][2]+z_min[array_length(z_min,1)][2]+z_maj_r[array_length(z_maj_r,1)][2]) as frac_l,
            z_min  [array_length(z_min,1)][2]   / (z_maj_l[array_length(z_maj_l,1)][2]+z_min[array_length(z_min,1)][2]+z_maj_r[array_length(z_maj_r,1)][2]) as frac_0,
            z_maj_r[array_length(z_maj_r,1)][2] / (z_maj_l[array_length(z_maj_l,1)][2]+z_min[array_length(z_min,1)][2]+z_maj_r[array_length(z_maj_r,1)][2]) as frac_r
        from down_sect
    )
    select
        coalesce(interpolate_array(u.zb, d.zb, case when d.t-u.t != 0 then (r.t-u.t)/(d.t-u.t) else 0 end), u.zb, d.zb)::real[],
        case when d.t-u.t != 0 then ((u.frac_l*(d.t-r.t)+d.frac_l*(r.t-u.t))/(d.t-u.t))::real else u.frac_l::real end as frac_l,
        case when d.t-u.t != 0 then ((u.frac_0*(d.t-r.t)+d.frac_0*(r.t-u.t))/(d.t-u.t))::real else u.frac_0::real end as frac_0,
        case when d.t-u.t != 0 then ((u.frac_r*(d.t-r.t)+d.frac_r*(r.t-u.t))/(d.t-u.t))::real else u.frac_r::real end as frac_r
    from reach r, up_pt u, down_pt d;
end;
$$$$
;;


-- if side is specified, line is only 3 points with the middle point on the reach, the fraction is a fraction of this half transect
-- if side is not specified the fraction is a fraction of the transect
create function ${model}.section_interpolate_point(line geometry, section real[], fraction real, side varchar default null)
returns geometry
language plpgsql
immutable
as
$$$$
declare
    l_ real;
    s_ real[];
    z_ real;
    p_ geometry;
    idx integer;
    alp real;
begin
    if side = 'left' then
        l_ := section[4][2] + section[10][2]/2.;
        s_ = array[array[0                                     , section[4][1]],
                   array[l_ - section[3][2] - section[10][2]/2., section[3][1]],
                   array[l_ - section[2][2] - section[10][2]/2., section[2][1]],
                   array[l_ - section[1][2] - section[10][2]/2., section[1][1]],
                   array[l_ - .5*section[10][2], section[10][1]],
                   array[l_ -  .5*section[9][2],  section[9][1]],
                   array[l_ -  .5*section[8][2],  section[8][1]],
                   array[l_ -  .5*section[7][2],  section[7][1]],
                   array[l_ -  .5*section[6][2],  section[6][1]],
                   array[l_ -  .5*section[5][2],  section[5][1]],
                   array[l_ , section[5][1]]
                ];
        p_ := ST_LineInterpolatePoint(ST_MakeLine(ST_StartPoint(line), st_pointn(line, 2)), fraction);
    elsif side = 'right' then
        l_ := section[14][2] + section[10][2]/2.;
        s_ = array[array[0                                      , section[14][1]],
                   array[l_ - section[13][2] - section[10][2]/2., section[13][1]],
                   array[l_ - section[12][2] - section[10][2]/2., section[12][1]],
                   array[l_ - section[11][2] - section[10][2]/2., section[11][1]],
                   array[l_ - .5*section[10][2], section[10][1]],
                   array[l_ -  .5*section[9][2],  section[9][1]],
                   array[l_ -  .5*section[8][2],  section[8][1]],
                   array[l_ -  .5*section[7][2],  section[7][1]],
                   array[l_ -  .5*section[6][2],  section[6][1]],
                   array[l_ -  .5*section[5][2],  section[5][1]],
                   array[l_ , section[5][1]]
                ];
        p_ := ST_LineInterpolatePoint(ST_MakeLine(ST_EndPoint(line), st_pointn(line, 2)), fraction);
    else
        l_ := section[4][2] + section[14][2] + section[10][2];
        s_ = array[array[0                                                   , section[4][1]],
                   array[l_ - section[3][2] - section[10][2] - section[14][2], section[3][1]],
                   array[l_ - section[2][2] - section[10][2] - section[14][2], section[2][1]],
                   array[l_ - section[1][2] - section[10][2] - section[14][2], section[1][1]],
                   array[l_ - section[10][2] - section[14][2], section[10][1]],
                   array[l_ - section[9][2] - section[14][2],  section[9][1]],
                   array[l_ - section[8][2] - section[14][2],  section[8][1]],
                   array[l_ - section[7][2] - section[14][2],  section[7][1]],
                   array[l_ - section[6][2] - section[14][2],  section[6][1]],
                   array[l_ - section[5][2] - section[14][2],  section[5][1]],
                   array[l_ - .5*section[5][2] - section[14][2], section[5][1]],
                   array[l_ - .5*section[6][2] - section[14][2], section[6][1]],
                   array[l_ - .5*section[7][2] - section[14][2], section[7][1]],
                   array[l_ - .5*section[8][2] - section[14][2], section[8][1]],
                   array[l_ - .5*section[9][2] - section[14][2], section[9][1]],
                   array[l_ - .5*section[10][2] - section[14][2], section[10][1]],
                   array[l_ - section[14][2],  section[11][1]],
                   array[l_ - section[14][2] + section[12][2],  section[12][1]],
                   array[l_ - section[14][2] + section[13][2],  section[13][1]],
                   array[l_ ,  section[14][1]]
                ];
        p_ := ST_LineInterpolatePoint(line, fraction);
    end if;

    if l_*fraction = s_[array_length(s_,1)][1] then
        return ST_SetSRID(ST_MakePoint(ST_X(p_), ST_Y(p_), s_[array_length(s_,1)][2]), ST_SRID(line));
    end if;

    select s_[i-1][2]*(1. - (l_*fraction - s_[i-1][1])/(s_[i][1] - s_[i-1][1])) + s_[i][2]*(l_*fraction - s_[i-1][1])/(s_[i][1] - s_[i-1][1]), i, (l_*fraction - s_[i-1][1])/(s_[i][1] - s_[i-1][1])
    from generate_subscripts(s_, 1) as i
    where i > 1
    and s_[i-1][1] <= l_*fraction
    and s_[i][1] > l_*fraction
    into z_, idx, alp;

    if z_ is null then
        raise 'we have a problem % %/% % % % % %', side, l_*fraction, l_, idx, alp, z_, s_, line;
    end if;
    --raise notice 's_ % %', l_*fraction, s_;
    --raise notice 'z_ % % %', z_, idx, alp;
    return ST_SetSRID(ST_MakePoint(ST_X(p_), ST_Y(p_), z_) ,ST_SRID(line));
end
$$$$
;;

create function ${model}.set_reach_point_altitude(point geometry)
returns geometry
language plpgsql
stable
as
$$$$
    declare
        transect geometry;
        section real[];
        reach geometry;
    begin
        transect :=  ${model}.interpolate_transect_at(point);
        select geom from ${model}.reach order by ST_Distance(transect, geom) limit 1 into reach;
        section := (${model}.valley_section_at(ST_ClosestPoint(reach, transect))).section;
        return ST_SetSRID(
            ST_MakePoint(
                ST_X(point),
                ST_Y(point),
                ST_Z(${model}.section_interpolate_point(transect, section, ST_LineLocatePoint(transect, point)::real))
            ),
            ST_SRID(point)
        );
    end;
$$$$
;;

/* ********************************************** */
/*          Hydrology functions to hydraulics     */
/* ********************************************** */

create function ${model}.find_nodes_network(node_array integer[])
returns integer[]
language plpgsql
stable
as
$$$$
    declare
        up_node integer;
        down_node integer;
        result_array integer array;
    begin
        for up_node in select unnest(node_array)
        loop
            with link as (
            select id from ${model}._link where up = up_node)
            select l.down into down_node from ${model}._link as l, link where l.id = link.id;
            if (select exists(select 1 from ${model}.manhole_hydrology_node where id = down_node)) then
                select array_append(result_array, down_node) into result_array;
            end if;
        end loop;
        select array_cat(result_array, node_array) into result_array;
        select array(select distinct unnest(result_array) order by 1) into result_array;

        return result_array;
    end;
$$$$
;;

create function ${model}.find_minimal_hydrology_network(node_array integer[])
returns integer[]
language plpgsql
stable
as
$$$$
    declare
        temp_array integer ARRAY;
    begin
	select ${model}.find_nodes_network(node_array) into temp_array;
	while temp_array != node_array loop
		select ${model}.find_nodes_network(node_array) into node_array;
		select ${model}.find_nodes_network(temp_array) into temp_array;
	end loop;
	return temp_array;
    end;
$$$$
;;
