# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtWidgets import QDialog, QMessageBox, QFileDialog, QDialogButtonBox
from qgis.gui import QgsProjectionSelectionDialog
from hydra.database import database as dbhydra
from hydra.utility.string import normalized_name
from hydra.utility.settings_properties import SettingsProperties

_hydra_dir = os.path.join(os.path.expanduser('~'), ".hydra")

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

class NewProjectDialog(QDialog):
    def __init__(self, parent=None, project_name='', srid='', working_dir=_hydra_dir):
        QDialog.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "new_project_dialog.ui"), self)

        self.existing_db = dbhydra.get_projects_list(all_db=True)

        self.info.text=''

        self.project_name.textChanged.connect(self.__project_name_changed)
        self.__project_name=project_name
        self.project_name.setText(project_name)

        self.srid.setText(str(srid))
        self.txt_working_dir.setText(str(working_dir))
        if not SettingsProperties.local():
            self.txt_working_dir.setEnabled(False)

        self.btn_workdir.clicked.connect(self.select_dir)
        self.btn_epsg.clicked.connect(self.select_epsg)
        self.buttonBox.accepted.connect(self.__save)

    def select_dir(self):
        title = tr('Select working dir path:')
        dir_name = QFileDialog.getExistingDirectory(None, title, self.txt_working_dir.text())
        if dir_name:
            self.txt_working_dir.setText(dir_name)

    def __project_name_changed(self):
        project_name = str(self.project_name.text())
        new_project_name = normalized_name(project_name)
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(new_project_name not in self.existing_db)
        self.info.setText(f"{new_project_name if project_name!=new_project_name else ''} {'name already taken' if new_project_name in self.existing_db else ''}""")

    def __save(self):
        self.__project_name= normalized_name(str(self.project_name.text()))

    def get_result(self):
        return self.__project_name, int(self.srid.text()), self.txt_working_dir.text()

    def select_epsg(self):
        projSelector = QgsProjectionSelectionDialog()
        projSelector.exec_()
        authId = projSelector.crs().authid()
        if authId[:5]=='EPSG:':
            self.srid.setText(authId[5:])
        else:
            QMessageBox(QMessageBox.Warning, tr('Not a valid SRID'), tr('Please select a SRID with code starting with "ESPG:..."'), QMessageBox.Ok).exec_()