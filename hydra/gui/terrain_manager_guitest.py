# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test

USAGE

   python -m hydra.gui.terrain_manager_guitest [-dh] [-g project_name model_name]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name model_name x y
        run in gui mode

"""

from __future__ import absolute_import # important to read the doc !
from __future__ import print_function
from builtins import str
from hydra.utility.string import normalized_name, normalized_model_name
import sys
import getopt

from hydra.project import Project

def gui_mode(project_name):
    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.terrain_manager import TerrainManager

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)
    obj_project = Project.load_project(project_name)
    test_dialog = TerrainManager(obj_project)
    test_dialog.exec_()

def test():
    pass

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdg",
            ["help", "debug", "gui"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    project_name = normalized_name(project_name)
    gui_mode(project_name)
    exit(0)

test()
# fix_print_with_import
print("ok")


