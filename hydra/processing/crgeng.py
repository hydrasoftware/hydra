from time import time
from pathlib import Path
import subprocess
import threading

from qgis.core import QgsProcessingException

CRGENG_EXE = str(Path(__file__).parent.parent / "advanced_tools" / "crgeng.exe")

# since crgeng write cannot handle some things concurrently
# exemple writing grid files for DEM
lock = threading.Lock()


def run_crgeng(args, feedback, workdir):
    """Run Crgeng command line tool

    Args:
        args (list[str]): list of crgeng arguments
        feedback (QgsProcessingFeedback): processing feedback instance
        workdir (str, optional): crgeng working dir

    Raises:
        IOError: when executable or arguments are not found
    """
    feedback.pushCommandInfo(f"Executing {CRGENG_EXE} {' '.join(args)} inside {workdir}")
    feedback.setProgress(0)

    start = time()
    try:
        with lock:
            subprocess.run(
                [CRGENG_EXE] + args,
                cwd=str(workdir),
                check=True,
                stdout=subprocess.PIPE
            )
        # crgeng doesn't exit with a code != 0 when it fails
        with (Path(workdir) / 'crgeng.err').open() as errfile:
            rc = int(errfile.readline().strip())
        if rc != 0:
            raise subprocess.CalledProcessError(rc, f"{CRGENG_EXE} {args}")

    except subprocess.CalledProcessError as exc:
        feedback.reportError("CRGENG failed", fatalError=True)
        if exc.stdout is not None:
            feedback.reportError(f"CRGENG stdout: {exc.stdout.decode(errors='replace')}")
        feedback.reportError("CRGENG err file content:")
        with (Path(workdir) / 'crgeng.log').open() as error_log:
            for line in error_log:
                if line.strip():
                    feedback.reportError(line)
        raise QgsProcessingException("Error in CRGENG execution")
    feedback.setProgress(100)
    feedback.pushCommandInfo(f"CRGENG processing for {args} took {time()-start:-.3f}s")
