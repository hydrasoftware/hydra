################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import numpy
import json
import pandas as pd
from .utility.result_decoder import W16Result
from .utility.parser import add_date
from datetime import timedelta, datetime

with open(os.path.join(os.path.dirname(__file__), "gui", "visu.json")) as j:
    visu = json.load(j)

class Branch:
    def __init__(self, name, w16, project, model, date0):
        self.name = name
        self.__w16 = w16
        self.__project = project
        self.model = model

    @property
    def max(self):
        df = self.__getitem__(self.__w16.nstep+2)
        df.rename(columns={ 'z(m)': 'z_max(m)', 'q(m3/s)': 'q_max(m3/s)' }, inplace=True)
        return df

    #@property
    #def singularities(self):
    #    self.__project.execute(f"""
    #        select name, singularity_type, st_linelocatepoint(n.geom, b.geom)/1000 as pk
    #        from {self.__model}._singularity
    #        where
    #        """)
    #    return None

    @property
    def dates(self):
        return [self.date0 + timedelta(hours=t) for t in self.__w16.get_step_times()]

    def __getitem__(self, i):
        # structure du tableau:
        # pk, z_invert, z_max, q_max
        data = numpy.array([
            [r[0] for r in self.__w16.get_geom(self.name)], # pks
            [r[1] for r in self.__w16.get_geom(self.name)], # pks
            [r[1][0] for r in self.__w16[self.name][i][0]], # z
            [r[1][1] for r in self.__w16[self.name][i][0]] # q
            ])
        return pd.DataFrame(data=data.T, columns=['pk', 'z_invert(m)', 'z(m)', 'q(m3/s)'])

class DataFrame:
    def __init__(self, name, table, path, date0, option):
        self.name = name.upper()
        self.table = table
        self.date0 = date0
        self.option = option

        r = visu[self.table]

        self.path = [p for p in path if p.endswith(r['file']) or ('files' in r and p[-3:] in r['files'])]

        if self.option is None:
            self.__columns = list(map(lambda x : x or '', r['results']))+['']*(8-len(r['results']))
        else:
            self.__columns = list(map(lambda x : x or '', r['results']))+['']*(4-len(r['results']))\
                    + list(map(lambda x : x or '', r[self.option]))+['']*(4-len(r[self.option]))

    @property
    def columns(self):
        return ['time(h)', 'date()']+[c for c in self.__columns if c]

    @property
    def dataframe(self):

        columns = ['time(h)']+self.__columns

        for file in self.path: # some pipes are in w13 instead of w14 for hydrology pipes
            if os.path.exists(file):
                with open(file, 'rb') as f :
                    f.seek(0)
                    nb_step, a, b = numpy.frombuffer(f.read(32), dtype=numpy.int32)[:3]
                    n = a + b
                    # find name index
                    for i in range(n):
                        name = f.read(24).decode('utf-8').rstrip()
                        f.read(8) # unused type
                        if self.name.upper() == name:
                            data = numpy.zeros((nb_step, 9), dtype=numpy.float32)
                            for t in range(nb_step):
                                f.seek(32*((1 + n)*(1 + t)))
                                data[t,0] = numpy.frombuffer(f.read(32), dtype=numpy.float32)[0]
                                f.seek(32*((1 + n)*(1 + t)+1+i))
                                data[t,1:] = numpy.frombuffer(f.read(32), dtype=numpy.float32)
                            df = pd.DataFrame(data=data, columns=columns)
                            df.drop(columns='', errors='ignore')
                            add_date(df, self.date0)
                            return df


class Result:
    # WARNING times and dates depend on the type of result (hydrology or hydraulics) and may even depend on the model
    # in case of cascade mode, therefore we do not compute result times in this class

    def __init__(self, project, model, scenario):
        self.__project = project
        self.model = model
        self.scenario = scenario
        self.date0 = None
        self.__date0 = None
        self.option = None

        optsor_path = os.path.join(project.directory, scenario.upper(), 'travail', scenario.upper()+'_optsor.dat')
        if os.path.exists(optsor_path):
            with open(optsor_path) as optsor:
                for line in optsor:
                    line = line.strip().upper()
                    if line == '*AERAULIQUE':
                        self.option = 'aeraulics'

        cmd_path = os.path.join(project.directory, scenario.upper(), 'travail', scenario.upper()+'.cmd')
        if os.path.exists(cmd_path):
            with open(cmd_path) as cmd:
                for line in cmd:
                    line = line.strip().upper()
                    if line == '*TIME':
                        yyyy, mm, dd, HH, MM = next(cmd).split()[:5]
                        self.date0 = datetime(int(yyyy), int(mm), int(dd), int(HH), int(MM))

                    elif line == '*OPTION_QUALITY1':
                        self.option = 'physico_chemical'

                    elif line == '*OPTION_QUALITY2':
                        self.option = 'bacteriological_quality'

                    elif line == '*OPTION_QUALITY3':
                        self.option = 'suspended_sediment_transport'

                    elif line == '*OPTION_POLLUTION':
                        self.option = 'pollution'

                    elif line == '*OPTION_SEDIMENT':
                        self.option = 'sediment'
        else:
            self.exists = False
            return

        hydraulique = os.path.join(
            self.__project.directory,
            self.scenario.upper(),
            "hydraulique",
            self.scenario.upper() + "_" + self.model.upper())
        hydrol = os.path.join(
            self.__project.directory,
            self.scenario.upper(),
            "hydrol",
            self.scenario.upper() + "_" + self.model.upper())


        self.__w16 = W16Result(hydraulique+'.w16') if os.path.exists(hydraulique+'.w16') else None

        self.path = [hydraulique+'.w14', hydraulique+'.w15', hydrol+'.w13']

        self.exists = os.path.exists(self.path[0]) or os.path.exists(self.path[2])

    @property
    def branches(self):
        return {n: Branch(n, self.__w16, self.__project, self.model, self.date0) for n in self.__w16.names} if self.__w16 else {}

    @property
    def nodes(self):
        return {n: DataFrame(n, table, self.path, self.date0, self.option) for n, table
            in self.__project.execute(f"select name, node_type||'_node' from {self.model}._node").fetchall()} if self.exists else {}

    @property
    def singularities(self):
        return {n: DataFrame(n, table, self.path, self.date0, self.option) for n, table
            in self.__project.execute(f"select name, singularity_type||'_singularity' from {self.model}._singularity").fetchall()} if self.exists else {}

    @property
    def links(self):
        return {n: DataFrame(n, table, self.path, self.date0, self.option) for n, table
            in self.__project.execute(f"select name, link_type||'_link' from {self.model}._link").fetchall()} if self.exists else {}

    def tables(self, table):
        return {n: DataFrame(n, table, self.path, self.date0, self.option) for n,
            in self.__project.execute(f"select upper(name) from {self.model}.{table}").fetchall()} if self.exists else {}

#if __name__=='__main__':
#    import sys
#    from .project import Project
#    result = Result(Project(sys.argv[1]), model=sys.argv[2], scenario=sys.argv[3])
#    if sys.argv[-1] in result.links:
#        print(result.links[sys.argv[-1]].dataframe)
#    if sys.argv[-1] in result.nodes:
#        print(result.nodes[sys.argv[-1]].dataframe)
#    if sys.argv[-1] in result.singularities:
#        print(result.singularities[sys.argv[-1]].dataframe)
