# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
string utility modules
"""
from __future__ import absolute_import # important to read the doc !
import os
import re
import string
import datetime
import unicodedata
from dateutil.parser import parse

__hydra_dir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
__meta_file = os.path.join(__hydra_dir, "metadata.txt")

def plugin_version():
    with open(__meta_file, 'r') as file:
        data = file.readlines()
    try:
        version = data[14].split('=')[1].strip()
    except:
        version = "!Version_not_found!"
    return version

def normalized_name(name, max_length=24, no_caps=True):
    '''removes accents, spaces and caps'''
    name = name.replace(u" ", u"_")
    if no_caps:
        name = name.lower()
    filter_ = string.ascii_letters+"_"+string.digits
    return ''.join(x for x in unicodedata.normalize('NFKD', name) \
            if x in filter_)[:max_length]

def normalized_caps_name(name, max_length=24):
    '''removes accents and spaces'''
    name = name.replace(u" ", u"_")
    filter_ = string.ascii_letters+"_"+string.digits
    return ''.join(x for x in unicodedata.normalize('NFKD', name) \
            if x in filter_)[:max_length]

def normalized_model_name(model_name):
    '''removes accents and spaces and limit length'''
    max_length=16
    starting_digit_cleaned_name = re.sub(r'^\d+', '', model_name)
    return normalized_name(starting_digit_cleaned_name, max_length)

def get_str(value):
    ''' get a str from a value if possible, else return "" '''
    if value is None:
        return ""
    elif type(value)==bytes:
        return value.decode('utf8')
    else:
        return str(value)

def isfloat(value):
    '''check if value can be converted to float'''
    try:
        float(value)
        return True
    except:
        return False

def isint(value):
    '''check if value can be converted to int [keeping the same value]'''
    try:
        int(value)
        assert float(value)==int(value)
        return True
    except:
        return False

def is_date(string):
    '''check if date'''
    try:
        parse(string)
        return True
    except ValueError:
        return False

def isascii(value):
    '''check if ascii'''
    try:
        value.decode('ascii')
        return True
    except:
        return False

def get_sql_int(value):
    '''converts to int if possible else None'''
    if isint(value):
        return int(value)
    else:
        return None

def get_sql_float(value):
    '''converts to int or float if possible else None'''
    if isint(value):
        return int(value)
    elif isfloat(value):
        return float(value)
    else:
        return None

def get_sql_forced_float(value):
    '''converts to float is possible else 0'''
    return float(value) if isfloat(value) else 0

def get_nullable_sql_float(value):
    if get_sql_float(value) is None:
        return "null"
    return get_sql_float(value)

def list_to_sql_array(py_list):
    '''converts list to string formated as sql array'''
    return str(list(map(str, py_list))).replace('[', '{').replace(']', '}').replace('\'', '') if py_list else None

def calc_slope(z_up, z_down, length):
    if isfloat(z_up) and isfloat(z_down) and isfloat(length):
        return ((float(z_up) - float(z_down)) / float(length))
    return None

def matrix_to_html(matrix, title=None, h=None, v=None, style=None, default="-"):
    ''' Transforms a square [[]] python array into a proper html string representing a table'''
    # check matrix is a square table
    irows = len(matrix)
    icols = len(matrix[0])
    for i in range(irows):
        assert len(matrix[i]) == icols
    # check headers length
    if h:
        assert len(h) == icols
    if v:
        assert len(v) == irows

    string = """<!DOCTYPE html><html>"""
    if style:
        string += """<head><style>
                        table, th, td {{
                            {};
                            text-align: right;
                        }}
                     </style></head>""".format(";".join(style))

    string += """<body>"""
    if title is not None:
        string += """<h2>{}</h2>""".format(str(title))
    string += """<table style="width:100%">"""
    if h:
        string += """<tr><th></th>{}</tr>""".format("""<th>{}</th>""".format("""</th><th>""".join(h)))
    for i in range(irows):
        string += """<tr>"""
        if v:
            string += """<th>{}</th>""".format(v[i])
        for j in range (icols):
            string += """<td>{}</td>""".format(matrix[i][j] if matrix[i][j] is not None else default)
        string += """</tr>"""
    string += """</table></body></html>"""

    return string

def read_file(filename):
    """usefull to avoid open(filename).read() syntax that leaves the file open"""
    with open(filename) as f:
        return f.read()

def read_lines(filename):
    """usefull to avoid open(filename).readlines() syntax that leaves the file open"""
    with open(filename) as f:
        return f.readlines()
