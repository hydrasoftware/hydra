# -*- coding: utf-8 -*-

"""
***************************************************************************
    streamdefdropanalysis.py
    ---------------------
    Date                 : April 2019
    Copyright            : (C) 2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'April 2019'
__copyright__ = '(C) 2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.core import (QgsProcessing,
                       QgsProcessingException,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterEnum,
                       QgsProcessingParameterRasterDestination,
                       QgsProcessingParameterFileDestination,
                      )


from .taudemAlgorithm import TauDemAlgorithm


class StreamDefDropAnalysis(TauDemAlgorithm):

    PIT_FILLED = 'PIT_FILLED'
    D8_FLOWDIR = 'D8_FLOWDIR'
    D8_CONTRIB_AREA = 'D8_CONTRIB_AREA'
    ACCUM_STREAM_SOURCE = 'ACCUM_STREAM_SOURCE'
    OUTLETS = 'OUTLETS'
    MASK_GRID = 'MASK_GRID'

    MIN_TRESHOLD = 'MIN_TRESHOLD'
    MAX_THRESHOLD = 'MAX_THRESHOLD'
    DROP_TRESHOLDS = 'DROP_TRESHOLDS'
    STEP = 'STEP'

    STREAM_RASTER = 'STREAM_RASTER'
    DROP_ANALYSIS = 'DROP_ANALYSIS'

    def name(self):
        return 'streamdefdropanalysis'

    def displayName(self):
        return self.tr('Stream definition with drop analysis')

    def group(self):
        return self.tr('Stream network analysis')

    def groupId(self):
        return 'streamanalysis'

    def tags(self):
        return self.tr('dem,hydrology,stream,drop,threshold').split(',')

    def shortHelpString(self):
        return self.tr('Generates a stream indicator grid (1,0) using '
                       'optimal threshold as determined from the stream '
                       'drop statistics.')

    def helpUrl(self):
        return 'http://hydrology.usu.edu/taudem/taudem5/help53/StreamDefinitionWithDropAnalysis.html'

    def __init__(self):
        super().__init__()

    def initAlgorithm(self, config=None):
        self.steps = [self.tr('Logarithmic'),
                      self.tr('Arithmetic')
                     ]

        self.addParameter(QgsProcessingParameterRasterLayer(self.PIT_FILLED,
                                                            self.tr('Pit filled elevation')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.D8_FLOWDIR,
                                                            self.tr('D8 flow directions')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.D8_CONTRIB_AREA,
                                                            self.tr('D8 contributing area')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.ACCUM_STREAM_SOURCE,
                                                            self.tr('Accumulated stream source')))
        self.addParameter(QgsProcessingParameterFeatureSource(self.OUTLETS,
                                                              self.tr('Outlets'),
                                                              types=[QgsProcessing.TypeVectorPoint]))
        self.addParameter(QgsProcessingParameterRasterLayer(self.MASK_GRID,
                                                            self.tr('Mask grid'),
                                                            defaultValue=None,
                                                            optional=True))
        self.addParameter(QgsProcessingParameterNumber(self.MIN_TRESHOLD,
                                                       self.tr('Minimum threshold'),
                                                       QgsProcessingParameterNumber.Double,
                                                       defaultValue=5))
        self.addParameter(QgsProcessingParameterNumber(self.MAX_THRESHOLD,
                                                       self.tr('Maximum threshold'),
                                                       QgsProcessingParameterNumber.Double,
                                                       defaultValue=500))
        self.addParameter(QgsProcessingParameterNumber(self.DROP_TRESHOLDS,
                                                       self.tr('Number of drop thresholds'),
                                                       QgsProcessingParameterNumber.Double,
                                                       defaultValue=10))
        self.addParameter(QgsProcessingParameterEnum(self.STEP,
                                                     self.tr('Type of threshold step'),
                                                     options=self.steps,
                                                     defaultValue=0))

        self.addParameter(QgsProcessingParameterRasterDestination(self.STREAM_RASTER,
                                                                  self.tr('Stream raster')))
        self.addParameter(QgsProcessingParameterFileDestination(self.DROP_ANALYSIS,
                                                                self.tr('Drop analysis'),
                                                                self.tr('Text files (*.txt)')))


    def processAlgorithm(self, parameters, context, alg_feedback):
        demLayer = self.parameterAsRasterLayer(parameters, self.PIT_FILLED, context)
        if demLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.PIT_FILLED))

        flowLayer = self.parameterAsRasterLayer(parameters, self.D8_FLOWDIR, context)
        if flowLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.D8_FLOWDIR))

        contribLayer = self.parameterAsRasterLayer(parameters, self.D8_CONTRIB_AREA, context)
        if contribLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.D8_CONTRIB_AREA))

        accumLayer = self.parameterAsRasterLayer(parameters, self.ACCUM_STREAM_SOURCE, context)
        if accumLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.ACCUM_STREAM_SOURCE))

        feedback = QgsProcessingMultiStepFeedback(2, alg_feedback)

        outputs = {}
        results = {}

        # DropAnalysis
        params = {'PIT_FILLED': parameters[self.PIT_FILLED],
                  'D8_FLOWDIR': parameters[self.D8_FLOWDIR],
                  'D8_CONTRIB_AREA': parameters[self.D8_CONTRIB_AREA],
                  'ACCUM_STREAM_SOURCE': parameters[self.ACCUM_STREAM_SOURCE],
                  'OUTLETS': parameters[self.OUTLETS],
                  'MIN_THRESHOLD': parameters[self.MIN_THRESHOLD],
                  'MAX_THRESHOLD': parameters[self.MAX_THRESHOLD],
                  'DROP_TRESHOLDS': parameters[self.DROP_TRESHOLDS],
                  'STEP': parameters[self.STEP],
                  'DROP_ANALYSIS': parameters[self.DROP_ANALYSIS],
                 }
        outputs['DROP_ANALYSIS'] = processing.run('taudem:dropanalysis', params, context=context, feedback=feedback, is_child_algorithm=True)
        results[self.DROP_ANALYSIS] = outputs['DROP_ANALYSIS']['DROP_ANALYSIS']

        # read threshold from the drop analysis text file
        threshold = 0.0
        with open(dropFile, 'r') as f:
            for line in f:
                pass
            threshold = float(line.split(':')[1].strip())

        feedback.setCurrentStep(1)
        if feedback.isCanceled():
            return {}

        # Threshold
        params = {'ACCUM_STREAM_SOURCE': parameters[self.ACCUM_STREAM_SOURCE],
                  'MASK_GRID': parameters[self.MASK_GRID],
                  'THRESHOLD': threshold,
                  'STREAM_RASTER': parameters[self.STREAM_RASTER],
                 }
        outputs['THRESHOLD'] = processing.run('taudem:threshold', params, context=context, feedback=feedback, is_child_algorithm=True)
        results[self.STREAM_RASTER] = outputs['THRESHOLD']['STREAM_RASTER']

        feedback.setCurrentStep(2)
        if feedback.isCanceled():
            return {}

        return results
