from builtins import str
from builtins import range
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from qgis.PyQt.QtWidgets import QListWidgetItem, QMessageBox, QFileDialog
from qgis.PyQt.QtPrintSupport import QPrinter
from qgis.PyQt.QtGui import QPainter
from qgis.PyQt.QtCore import QRect, Qt
from hydra.gui.base_dialog import BaseDialog, tr
from hydra.gui.section_viewer import GraphViewer
from qgis.PyQt import uic
import os

_desktop_dir = os.path.join(os.path.expanduser("~"), "Desktop")

class SectionExporter(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "section_export.ui"), self)

        self.btnFolder.setEnabled(False)
        self.btnPdf.setEnabled(False)

        self.project = project

        if not self.project.terrain.has_data() :
            self.chk_show_DEM.setChecked(False)
            self.chk_show_combined.setChecked(False)
            self.chk_show_DEM.setEnabled(False)
            self.chk_show_combined.setEnabled(False)

        self.btnFolder.clicked.connect(lambda: self.process_image("folder"))
        self.btnPdf.clicked.connect(lambda: self.process_image("pdf"))
        self.btnClose.clicked.connect(self.close)
        self.comboPrint.addItems(["1", "2", "4"])

        for scn in self.project.get_scenarios():
            if self.project.scn_has_run(scn[0]):
                self.qtlist_scenario.addItem(QListWidgetItem(tr(scn[1])))
            else:
                item = QListWidgetItem(tr(scn[1]))
                item.setFlags(Qt.NoItemFlags)
                self.qtlist_scenario.addItem(item)
        self.qtlist_scenario.addItem(QListWidgetItem(tr('No scenario')))

        models = self.project.get_models()

        for model in models:
            temp_list = self.project.execute("""select name from {model}.reach""".format(model=model)).fetchall()
            for reach in [[name, model] for (name,) in temp_list]:
                self.qtlist_reach.addItem(QListWidgetItem(tr(reach[0] + ";" + reach[1])))
                self.btnFolder.setEnabled(True)
                self.btnPdf.setEnabled(True)

    def process_image(self, mode):
        list_image = []
        list_tittle = []

        selected_scn, selected_reach = self.get_selected_items()

        if len(selected_scn) == 0 or len(selected_reach) == 0:
            return

        title = tr('Directory for .pdf file or folder')
        dirpath = QFileDialog.getExistingDirectory(None, title, _desktop_dir, QFileDialog.ShowDirsOnly)
        if not dirpath:
            return

        progress = self.project.log.progress(tr("Exporting section"))
        progress.set_ratio(0/(len(selected_reach)*len(selected_scn)))
        i = 0

        for scn in selected_scn:
            for reach in selected_reach:
                temp_tittle, temp_image = self.__extract_reach(reach, scn=scn)
                list_image.append([temp_image, "_".join(temp_tittle[0].split(" - ")[0:3])])
                list_tittle.append(temp_tittle)
                i+=1
                progress.set_ratio(float(i)/float((len(selected_reach)*len(selected_scn))))

        if dirpath and mode == "pdf" :
            self.__create_pdf(dirpath, list_image)
        elif dirpath and mode == "folder" :
            self.__export_png(dirpath, list_image, list_tittle)
        progress.set_ratio(1)
        self.project.log.clear_progress()

    def get_selected_items(self) :
        selected_scn = []
        selected_reach = []

        for item in self.qtlist_scenario.selectedItems() :
            if item.text() == 'No scenario' :
                selected_scn.append([0,"No scenario"])
            else :
                scn = [[id,name] for (id,name) in self.project.get_scenarios() if name == item.text()][0]
                selected_scn.append(scn)

        for item in self.qtlist_reach.selectedItems() :
            reach = [item.text().split(";")[0],item.text().split(";")[1] if item is not None else None]
            selected_reach.append(reach)

        return (selected_scn, selected_reach)

    def __extract_reach(self, reach, scn=None):
        ''' extract a reach, create the dico of the reach, then make the image and save it in self.__list_image '''

        temp_list_image = []
        temp_list_tittle = []

        id_reach = self.project.execute("""select id from {model}.reach where reach.name = '{name}' """.format(model=self.project.get_current_model().name,name=reach[0])).fetchone()[0]

        self.dico_reach_cross_section_valley = self.project.get_current_model().get_section_reach_valley(id_reach, scn)

        cross_section_id_updown = list(self.dico_reach_cross_section_valley.items())[0][0] if len(self.dico_reach_cross_section_valley) else []

        transect, valley_geom_id, water_level = self.__change_section(cross_section_id_updown, scn)

        self.section_viewer = GraphViewer(self.project, transect, valley_geom_id, id_reach, water_level, silent_mode=True, dpi=300)
        self.section_viewer.show_graph(self.chk_show_points_xyz.isChecked(), self.chk_show_DEM.isChecked(), self.chk_show_hydra.isChecked(), self.chk_show_combined.isChecked())

        temp_list_tittle.append(self.__write_and_set_tittle(cross_section_id_updown, reach, scn))

        temp_list_image.append(self.section_viewer.render_image())

        for i in range(1,len(self.dico_reach_cross_section_valley)) :
            cross_section_id_updown = list(self.dico_reach_cross_section_valley.items())[i][0]

            self.section_viewer.transect_id, self.section_viewer.valley_geom_id, self.section_viewer.water_level = self.__change_section(cross_section_id_updown, scn)

            self.section_viewer.load_graph(reach[1])
            self.section_viewer.show_graph(self.chk_show_points_xyz.isChecked(), self.chk_show_DEM.isChecked(), self.chk_show_hydra.isChecked(), self.chk_show_combined.isChecked())

            temp_list_tittle.append(self.__write_and_set_tittle(cross_section_id_updown, reach, scn))
            temp_list_image.append(self.section_viewer.render_image())

        return (temp_list_tittle, temp_list_image)

    def __change_section(self, cross_section_id_updown, scn):
        water_level = None
        transect = self.dico_reach_cross_section_valley[cross_section_id_updown]["transect"]
        valley_geom_id = self.dico_reach_cross_section_valley[cross_section_id_updown]["valley_geom_id"]

        if scn[0] > 0:
            water_level = self.dico_reach_cross_section_valley[cross_section_id_updown]["water_level"]

        return(transect, valley_geom_id, water_level)

    def __write_and_set_tittle(self, cross_section_id_updown, reach, scn):
        water_label = "No water"

        if scn[0] > 0:
                water_label = "Water level : "+str(self.dico_reach_cross_section_valley[cross_section_id_updown]['water_level'])+" m"

        tittle = " - ".join([scn[1],reach[1],reach[0],
        self.dico_reach_cross_section_valley[cross_section_id_updown]["name"],self.dico_reach_cross_section_valley[cross_section_id_updown]["updown"],
        water_label])

        self.section_viewer.graph_title.set_text(tittle)
        return (tittle)

    def __create_pdf(self, dirpath, list_image) :

        for pkg in list_image :

            if os.path.exists(os.path.join(dirpath,pkg[1].replace(" ","_")+".pdf")) :
                try :
                    os.remove(os.path.join(dirpath,pkg[1].replace(" ","_")+".pdf"))
                except :
                    QMessageBox.information(self, tr("File open"),
                    tr("File is actually open : {} , close it before the generation of the section's report".format(os.path.join(dirpath,pkg[1].replace(" ","_")+".pdf"))), QMessageBox.Ok)
                    return

            printer = QPrinter()
            printer.setOutputFileName(os.path.join(dirpath,pkg[1]+".pdf"))
            printer.setOutputFormat(QPrinter.PdfFormat)
            if self.comboPrint.currentText() in ["1","4"] :
                printer.setOrientation(QPrinter.Landscape)

            painter = QPainter()
            painter.setRenderHint(QPainter.Antialiasing, True)
            painter.begin(printer)

            if self.comboPrint.currentText() == "1" :
                for image in pkg[0] :
                    painter.drawImage(QRect(0,0,printer.pageRect().width(),printer.pageRect().height()),image)
                    printer.newPage()

            elif self.comboPrint.currentText() == "2" :
                for image in pkg[0] :
                    y_picture = printer.pageRect().height()/2 if pkg[0].index(image)%2==1 else 0
                    painter.drawImage(QRect(0,y_picture,printer.pageRect().width(),printer.pageRect().height()/2),image)
                    if pkg[0].index(image)%2 == 0 and pkg[0].index(image) != 0:
                        printer.newPage()

            elif self.comboPrint.currentText() == "4" :
                for image in pkg[0] :
                    x_picture = printer.pageRect().width()/2 if pkg[0].index(image)%4 in [1, 3] else 0
                    y_picture = printer.pageRect().height()/2 if pkg[0].index(image)%4 in [2, 3] else 0
                    painter.drawImage(QRect(x_picture, y_picture, printer.pageRect().width()/2, printer.pageRect().height()/2), image)
                    if pkg[0].index(image)%4 == 0 and pkg[0].index(image) != 0:
                        printer.newPage()

            painter.end()


    def __export_png(self, dirpath, list_image, list_tittle) :

        for pkg in list_image :
            if not os.path.exists(os.path.join(dirpath,pkg[1].replace(" ","_"))):
                os.makedirs(os.path.join(dirpath,pkg[1].replace(" ","_")))
            else :
                content=os.listdir(os.path.join(dirpath,pkg[1].replace(" ","_")))
                for x in content:
                    os.remove(os.path.join(dirpath,pkg[1].replace(" ","_"),x))

            for i in range(len(pkg[0])) :
                filename = '_'.join(list_tittle[list_image.index(pkg)][i].split(' - ')[:-1]) + ".png"
                pkg[0][i].save(os.path.join(dirpath,pkg[1].replace(" ","_"),filename))
