from .bake import Bake
from .blurred import Blurred
from .hazard_vectorisation import HazardVectorisation
from .simulate import Simulate
from .rendering_mesh import RenderingMesh
from .w15_to_slf import W15ToSlf
from .slf_to_tif import SlfToTif
from .out_to_gpkg import OutToGpkg
from qgis.core import QgsProcessingProvider


class HydraProvider(QgsProcessingProvider):
    def loadAlgorithms(self, *args, **kwargs):
        self.addAlgorithm(Simulate())
        self.addAlgorithm(OutToGpkg())

        #self.addAlgorithm(DepthRaster())
        #self.addAlgorithm(DepthEnvelopeRaster())
        #self.addAlgorithm(VelocityRaster())
        #self.addAlgorithm(VelocityEnvelopeRaster())

        self.addAlgorithm(RenderingMesh())
        self.addAlgorithm(W15ToSlf())
        self.addAlgorithm(SlfToTif())
        self.addAlgorithm(HazardVectorisation())

        self.addAlgorithm(Bake())
        self.addAlgorithm(Blurred())

    def id(self, *args, **kwargs):
        return 'hydra'

    def name(self, *args, **kwargs):
        return 'hydra'

    def icon(self):
        return QgsProcessingProvider.icon(self)

