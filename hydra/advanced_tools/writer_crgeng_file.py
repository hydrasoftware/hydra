from builtins import str
from builtins import range
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from qgis.PyQt.QtWidgets import QDialog, QCheckBox, QVBoxLayout, QSpinBox, QDoubleSpinBox, QLabel, QWidget, QHBoxLayout, QTableWidgetItem, QFileDialog
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QObject, pyqtSignal
from hydra.gui.base_dialog import tr
from qgis.gui import QgsProjectionSelectionDialog
from qgis.core import QgsCoordinateReferenceSystem
import os

chk_dict_setting = {
    'TRZ_MIF': {'name': 'TRZ_MIF', 'label': "triangulation des cotes d'eau au format .shp"},
    'NVZ_MIF': {'name': 'NVZ_MIF', 'label': "courbes iso-cotes au format .shp"},
    'NVN_MIF': {'name': 'NVN_MIF', 'label': "classes de hauteurs au format .shp"},
    'VIT_MIF': {'name': 'VIT_MIF', 'label': "classes de vitesses au format .shp"},
    'ALEA_MIF': {'name': 'ALEA_MIF', 'label': "classes d'alaas au format .shp"},
    'NVZ_ASC': {'name': 'NVZ_ASC', 'label': "grid des cotes d'eau au format asc"},
    'NVN_ASC': {'name': 'NVN_ASC', 'label': "grid des hauteurs d'eau au format asc"},
    'VIT_ASC': {'name': 'VIT_ASC', 'label': "grid des vitesses d'ecoulement au format asc"},
    'VSUB_ASC': {'name': 'VSUB_ASC', 'label': "grid des vitesses d'ecoulement au format .asc"},
    'ALEA_ASC': {'name': 'ALEA_ASC', 'label': "grid des classes d'alea au format .asc"}
}

chk_dict_method = {
    'OPTION_1D': {'name': 'OPTION_1D', 'label': "delimitation lit mineur", 'type': "chk"},
    'SURFMIN': {'name': 'SURFMIN', 'label': "surface minimum (4 m2)", 'type': "spinbox"},
    'OPTION_ZF2': {'name': 'OPTION_ZF2', 'label': "hauteur calculee/ZF2 dans mailles 2D", 'type': "chk"},
    'OPTION_CALCULV': {'name': 'OPTION_CALCULV', 'label': "methode par agregation de pixels", 'type': "chk"},
    'OPTION_1D_DX': {'name': 'OPTION_1D_DX', 'label': "dx maillage coverage 1D", 'type': "spinbox"},
    'OPTION_1D_DY': {'name': 'OPTION_1D_DY', 'label': "dy maillage coverage 1D", 'type': "spinbox"},
    'DC_EXPAND': {'name': 'DC_EXPAND', 'label': "écartement du contour extérieur", 'type': "spinbox"}
}

chk_dict_mode = {
    'DALLES': {'methods': [], 'settings': [], 'scenario_ref': False, 'spec_value_tab': False, 'class_tab_alea': False, 'header': '', 'output_name': False},
    'CONTOUR': {'methods': ['OPTION_1D', 'SURFMIN', 'OPTION_ZF2', 'OPTION_CALCULV'], 'settings': ['TRZ_MIF', 'NVN_MIF', 'VIT_MIF', 'NVN_ASC', 'VIT_ASC', 'VSUB_ASC'], 'scenario_ref': True, 'spec_value_tab': True,'class_tab_alea': False, 'header': 'Hauteur (m)', 'output_name': False},
    'CONTOURZ': {'methods': ['OPTION_1D', 'SURFMIN', 'OPTION_ZF2', 'OPTION_CALCULV'], 'settings': ['TRZ_MIF', 'NVZ_MIF', 'VIT_MIF', 'NVZ_ASC'], 'scenario_ref': True, 'spec_value_tab': False, 'class_tab_alea': False, 'header': '', 'output_name': False},
    'GAIN': {'methods': ['OPTION_1D', 'SURFMIN', 'OPTION_ZF2', 'OPTION_CALCULV'], 'settings': ['NVN_MIF', 'NVN_ASC'], 'scenario_ref': True, 'spec_value_tab': True, 'class_tab_alea': False, 'header': 'Gain dH (m)', 'output_name': True},
    'ENVELOP': {'methods': ['OPTION_1D', 'SURFMIN', 'OPTION_ZF2', 'OPTION_CALCULV'], 'settings': ['NVN_MIF', 'NVN_ASC', 'VIT_ASC', 'VSUB_ASC'], 'scenario_ref': True, 'spec_value_tab': True, 'class_tab_alea': False, 'header': 'Hauteur (m)', 'output_name': True},
    'ENVELOPZ': {'methods': ['OPTION_1D', 'SURFMIN', 'OPTION_ZF2', 'OPTION_CALCULV'], 'settings': ['NVZ_MIF', 'NVN_ASC'], 'scenario_ref': True, 'spec_value_tab': False, 'class_tab_alea': False, 'header': 'Hauteur (m)', 'output_name': True},
    'ALEA': {'methods': ['OPTION_1D', 'SURFMIN', 'OPTION_ZF2', 'OPTION_CALCULV'], 'settings': ['TRZ_MIF', 'NVN_MIF', 'VIT_MIF', 'ALEA_MIF', 'NVN_ASC', 'VIT_ASC', 'VSUB_ASC', 'ALEA_ASC'], 'scenario_ref': True, 'spec_value_tab': True, 'class_tab_alea': True, 'header': "1", 'output_name': False},
    'ALEA_ENV': {'methods': ['OPTION_1D', 'SURFMIN', 'OPTION_ZF2', 'OPTION_CALCULV'], 'settings': ['NVN_MIF', 'ALEA_MIF', 'NVN_ASC', 'VIT_ASC', 'VSUB_ASC', 'ALEA_ASC'], 'scenario_ref': True, 'spec_value_tab': False, 'class_tab_alea': False, 'output_name': False},
    'MESH_LAYER': {'methods': ['OPTION_1D', 'SURFMIN', 'OPTION_ZF2', 'OPTION_CALCULV'], 'settings': [], 'scenario_ref': True, 'spec_value_tab': False, 'class_tab_alea': False, 'output_name': False}
}

class WriterCrgeng(QDialog):
    signal_file = pyqtSignal(str)
    def __init__(self, project, parent=None):
        QDialog.__init__(self, parent)
        self.project = project

        uic.loadUi(os.path.join(os.path.dirname(__file__), 'writer_crgeng_file.ui'), self)
        self.settings_gb.setLayout(QVBoxLayout())
        self.methods_gb.setLayout(QVBoxLayout())

        self.modes = ['Dalles', 'Contour', 'Contourz', 'Gain', 'Envelop', 'Envelopz', 'Alea', 'Alea_env', 'Mesh_layer']

        for mode in self.modes:
            self.combo_mode.addItem(mode)

        for chk in chk_dict_setting.values():
            self.settings_gb.layout().addWidget(self.generate_chk(chk['name'], chk['label']))

        for chk in chk_dict_method.values():
            if chk['type'] == 'chk':
                widget = self.generate_chk(chk['name'], chk['label'])
            elif chk['type'] == 'spinbox':
                widget = self.generate_spinbox(chk['name'], chk['label'])
            self.methods_gb.layout().addWidget(widget)

        scenarios = [scn[1] for scn in self.project.get_scenarios()]
        scenarios.sort()
        for scenario in scenarios:
            self.combo_scn.addItem(scenario)

        for model in self.project.get_models():
            self.combo_model.addItem(model)

        for terrain in self.project.terrain.dems():
            self.combo_terrain.addItem(terrain)

        self.lb_output_epsg.setText(str(self.project.srid))
        self.combo_mode.currentIndexChanged.connect(self.refresh)
        self.btn_rowAdd.clicked.connect(self.add_row)
        self.btn_rowRemove.clicked.connect(self.remove_row)
        self.btn_row_valueAdd.clicked.connect(self.add_value_row)
        self.btn_row_valueRemove.clicked.connect(self.remove_value_row)
        self.btn_epsg.clicked.connect(self.select_epsg)
        self.buttonBox.accepted.connect(self.generate_dict)
        self.sb_nb_hauteur.valueChanged.connect(self.refresh)
        self.sb_nb_vitesse.valueChanged.connect(self.refresh)

        self.refresh()

    def select_epsg(self):
        prj_select = QgsProjectionSelectionDialog()
        prj_select.exec_()
        if prj_select.crs():
            crs_core = prj_select.crs()
            if crs_core.isValid():
                self.qpj_string = str(crs_core.toWkt())
                if crs_core.authid()[:5] == "EPSG:":
                    self.lb_output_epsg.setText(crs_core.authid()[5:])

    def refresh(self):
        ''' refresh UI '''
        mode = str(self.combo_mode.currentText()).upper()

        crs = QgsCoordinateReferenceSystem(int(self.lb_output_epsg.text()), QgsCoordinateReferenceSystem.EpsgCrsId)
        self.qpj_string = str(crs.toWkt())

        #hide all gui
        for chk in chk_dict_setting.values():
            vars(self)[chk['name'].lower()].setVisible(False)

        for chk in chk_dict_method.values():
            vars(self)[chk['name'].lower()].setVisible(False)

        #show good part of UI considering mode

        for item in chk_dict_mode[mode]['settings']:
            vars(self)[item.lower()].setVisible(True)

        for item in chk_dict_mode[mode]['methods']:
            vars(self)[item.lower()].setVisible(True)

        self.scenario_ref_gb.setVisible(chk_dict_mode[mode]['scenario_ref'])
        if mode == "GAIN" and self.scenario_table.rowCount() > 2:
            self.scenario_table.setRowCount(2)

        if mode == "CONTOURZ" or mode == "ENVELOPZ":
            self.gb_contourz.setVisible(True)
        else:
            self.gb_contourz.setVisible(False)

        self.lb_output_name.setVisible(chk_dict_mode[mode]['output_name'])
        self.sb_output_time.setVisible(chk_dict_mode[mode]['output_name'])
        self.lb_output_label.setVisible(chk_dict_mode[mode]['output_name'])
        self.sb_output_time.setRange(-999, 999)
        self.sb_output_time.setValue(-999)

        self.spec_value_table.setVisible(chk_dict_mode[mode]['spec_value_tab'])
        if chk_dict_mode[mode]['spec_value_tab']:
            self.spec_value_table.setColumnCount(0)
            self.spec_value_table.setRowCount(0)
            self.spec_value_table.setColumnCount(1)
            self.spec_value_table.setHorizontalHeaderLabels([chk_dict_mode[mode]['header']])
            self.btn_row_valueAdd.setVisible(True)
            self.btn_row_valueRemove.setVisible(True)
        else:
            self.btn_row_valueAdd.setVisible(False)
            self.btn_row_valueRemove.setVisible(False)
            self.spec_value_table.setColumnCount(0)

        if chk_dict_mode[mode]['class_tab_alea']:
            self.btn_row_valueAdd.setVisible(False)
            self.btn_row_valueRemove.setVisible(False)
            self.gb_alea.setVisible(True)
            self.spec_value_table.setColumnCount(0)
            self.spec_value_table.setRowCount(0)
            self.spec_value_table.setColumnCount(self.sb_nb_vitesse.value()+1)
            self.spec_value_table.setRowCount(self.sb_nb_hauteur.value()+1)
            self.tb_hauteur.setRowCount(self.sb_nb_hauteur.value())
            self.tb_vitesse.setRowCount(self.sb_nb_vitesse.value())
        else:
            self.gb_alea.setVisible(False)

    def add_row(self):
        row = self.scenario_table.rowCount()
        if self.combo_scn.currentIndex() == -1:
            return
        if self.combo_model.currentIndex() == -1:
            return
        if self.combo_mode.currentText() == "Gain" and row > 1:
            return
        self.scenario_table.insertRow(row)
        self.scenario_table.setItem(row, 0, QTableWidgetItem(self.combo_scn.currentText()))
        self.scenario_table.setItem(row, 1, QTableWidgetItem(self.combo_model.currentText()))
        spinbox_time = QDoubleSpinBox()
        spinbox_time.setRange(-999, 999)
        spinbox_time.setValue(self.sb_output_time.value())
        self.scenario_table.setCellWidget(row, 2, spinbox_time)

    def remove_row(self):
        row = self.scenario_table.currentRow()
        if row != -1:
            self.scenario_table.removeRow(row)

    def add_value_row(self):
        row = self.spec_value_table.rowCount()
        self.spec_value_table.insertRow(row)
        spinbox = QDoubleSpinBox()
        spinbox.setSingleStep(0.01)
        self.spec_value_table.setCellWidget(row, 0, spinbox)

    def remove_value_row(self):
        row = self.spec_value_table.currentRow()
        if row != -1:
            self.spec_value_table.removeRow(row)

    def generate_chk(self, name, label):
        vars(self)[name.lower()] = QCheckBox("{} : {}".format(name, label))
        return vars(self)[name.lower()]

    def generate_spinbox(self, name, label):
        vars(self)[name.lower() + "_spin"] = QSpinBox()
        vars(self)[name.lower() + "_chk"] = QCheckBox("{} : {}".format(name, label))
        if name == 'SURFMIN':
            vars(self)[name.lower() + "_spin"].setRange(4, 999999)
        layout = QHBoxLayout()
        layout.addWidget(vars(self)[name.lower() + "_chk"])
        layout.addWidget(vars(self)[name.lower() + "_spin"])
        vars(self)[name.lower()] = QWidget()
        vars(self)[name.lower()].setLayout(layout)
        return vars(self)[name.lower()]

    def generate_dict(self):
        mode = str(self.combo_mode.currentText()).upper()
        self.dict_file = {}

        self.dict_file['*WRKSPACE'] = "\n" + os.path.dirname(self.project.directory)
        self.dict_file["*MODE"] = mode
        self.dict_file["*GRID_VRT"] = "\n" + str(self.combo_terrain.currentText())
        self.dict_file["output_name"] = self.lb_output_name.text()
        self.dict_file["output_time"] = str(self.sb_output_time.value())
        self.dict_file["epsg"] = str(self.lb_output_epsg.text())
        self.dict_file["qpj"] = self.qpj_string
        self.dict_file["contourz_z1"] = str(self.lb_z1.text())
        self.dict_file["contourz_z2"] = str(self.lb_z2.text())
        self.dict_file["contourz_dz"] = str(self.lb_dz.text())
        self.dict_file["vsub"] = str(self.v_sub.value())

        if self.surfmin_chk.checkState():
            self.dict_file["*SURFMIN"] = self.surfmin_spin.value()

        if self.option_zf2.checkState():
            self.dict_file["*OPTION_ZF2"] = ""

        if self.option_calculv.checkState():
            self.dict_file["*OPTION_CALCULV"] = "\n1"

        if self.option_1d.checkState():
            self.dict_file["*OPTION_1D"] = ""

        values = []
        for row in range(self.spec_value_table.rowCount()):
            if self.spec_value_table.cellWidget(row, 0) :
                values.append(str(self.spec_value_table.cellWidget(row, 0).value()))
        self.dict_file["values"] = values

        values_height = []
        for row in range(self.tb_hauteur.rowCount()):
            values_height.append(str(self.tb_hauteur.item(row, 0).text()))
        self.dict_file["values_height"] = values_height

        values_speed = []
        for row in range(self.tb_vitesse.rowCount()):
            values_speed.append(str(self.tb_vitesse.item(row, 0).text()))
        self.dict_file["values_speed"] = values_speed

        values_as_array = []
        for row in range(self.spec_value_table.rowCount()):
            row_values = []
            for col in range(self.spec_value_table.columnCount()):
                if not self.spec_value_table.cellWidget(row, 0) :
                    row_values.append(str(self.spec_value_table.item(row, col).text()))
            values_as_array.append(row_values)
        self.dict_file["values_as_array"] = values_as_array

        projects = []
        for row in range(self.scenario_table.rowCount()):
            projects.append([str(self.scenario_table.item(row, 1).text()), str(self.scenario_table.item(row, 0).text()),
            str(self.scenario_table.cellWidget(row, 2).value())])
        self.dict_file["projects"] = projects

        output_settings = []
        for item in chk_dict_mode[mode]['settings']:
            if vars(self)[item.lower()].checkState():
                output_settings.append(item)
        self.dict_file["*OPTION_RES"] = output_settings
        self.write_file()

    def write_file(self):
        fileName, __ = QFileDialog.getSaveFileName(None, tr('Save control file'), self.project.carto_dir, tr('Ctl files (*.ctl)'))
        if fileName:
            with open(fileName, "w") as file:
                for k, v in self.dict_file.items():
                    space = True
                    if k == "projects":
                        file.write('{} {}\n'.format("*MODE", self.dict_file["*MODE"]))
                        if self.dict_file["*MODE"] in ["GAIN", "ALEA_ENV"]:
                            file.write("1\n")
                        elif self.dict_file["*MODE"] in ["ENVELOP", "ENVELOPZ"]:
                            file.write(str(len(v)) + "\n")
                            file.write(self.project.name + " " + v[0][0] + "\n")
                        else:
                            file.write(str(len(v)) + "\n")
                        for scn in v:
                            if self.dict_file["*MODE"] in ["GAIN", "ALEA_ENV"]:
                                file.write(self.project.name + " " + " ".join(scn[:-1]))
                            elif self.dict_file["*MODE"] in ["ENVELOP", "ENVELOPZ"]:
                                if scn == v[0] :
                                    file.write(self.project.name + " "+ " ".join([elem[1] for elem in v]))
                            else:
                                file.write(self.project.name + " " + " ".join(scn))
                            file.write('\n')
                        if self.dict_file["*MODE"] in ["GAIN", "ALEA_ENV", "ENVELOP", "ENVELOPZ"]:
                            file.write(self.dict_file["output_name"] + " " + self.dict_file["output_time"] + "\n")
                        if self.dict_file["*MODE"] in ["CONTOURZ", "ENVELOPZ"]:
                            file.write(self.dict_file["contourz_z1"] + " " + self.dict_file["contourz_z2"] + " " + self.dict_file["contourz_dz"] + "\n")
                        if self.dict_file["*MODE"] in ["ALEA"]:
                            file.write(" ".join(self.dict_file["values_height"]))
                            file.write('\n')
                            file.write(" ".join(self.dict_file["values_speed"]))
                            file.write('\n')
                            file.write(self.dict_file["vsub"])
                            file.write('\n')
                            for row in self.dict_file["values_as_array"] :
                                file.write(" ".join(row))
                                file.write('\n')
                        if self.dict_file["*MODE"] not in ["CONTOURZ", "ENVELOPZ", "ALEA"]:
                            file.write(str(len(self.dict_file["values"])) + "\n")
                            for val in self.dict_file["values"]:
                                file.write(val)
                                file.write('\n')
                    elif k == "*OPTION_RES":
                        file.write(k + "\n")
                        if self.dict_file["qpj"]:
                            file.write(self.dict_file["qpj"] + "\n")
                        else:
                            file.write("EPSG" + self.dict_file["epsg"] + "\n")
                        for opt in v:
                            file.write(opt + "\n")
                    elif k == "*MODE":
                        if not self.dict_file["projects"]:
                            file.write('{} {}'.format(k, v))
                        else:
                            pass
                    elif k in ["values", "output_name", "output_time", "qpj", "epsg", "contourz_z1", "contourz_z2", "contourz_dz", "vsub", "values_height", "values_speed", "values_as_array"]:
                        space = False
                        pass
                    else:
                        file.write('{} {}'.format(k, v))
                        file.write('\n')
                    if space:
                        file.write('\n')
        self.signal_file.emit(fileName)
