
/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

create table $model._$tablename(
    id integer unique not null,
    singularity_type hydra_singularity_type not null,
        foreign key (id, singularity_type) references $model._singularity(id, singularity_type),
        check(singularity_type=$tableid)$tabledeclare
);;
