# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_link_dialog import SimpleLinkDialog, tr
import hydra.utility.string as string
from qgis.PyQt import uic
import os

class RoutingHydrologyLinkEditor(SimpleLinkDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleLinkDialog.__init__(self, project, geom, "routing_hydrology_link", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "routing_hydrology_link.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.split_coef.textChanged.connect(self.__split_coef_changed)

        name, cross_section, length, slope, split_coef, catchment_name, comment = self.project.execute("""
            select l.name, l.cross_section, l.length, l.slope, l.split_coef, n.name, l.comment
            from {m}.routing_hydrology_link as l
            left join {m}.catchment_node as n on l.up=n.id
            where l.id={i}""".format(m=self.model.name, i=self.id)).fetchone()

        self.name.setText(name)
        self.notes.setText(comment)
        self.cross_section.setText(string.get_str(cross_section))
        self.length.setText(string.get_str(length))
        self.slope.setText(string.get_str(slope))
        self.split_coef.setText(string.get_str(split_coef))

        if catchment_name:
            self.label_split_weight.setText("Split weight on {}".format(catchment_name))

    def __split_coef_changed(self):
        if string.get_sql_float(self.split_coef.text()):
            self.project.execute("""
                update {}.routing_hydrology_link
                set split_coef={}
                where id={}""".format(self.model.name, string.get_sql_float(self.split_coef.text()), self.id))

            split_weight, = self.project.execute("""
                select split_coef/(select SUM(split_coef) from {m}.routing_hydrology_link where up=l.up)
                from {m}.routing_hydrology_link as l
                where id={i}""".format(m=self.model.name, i=self.id)).fetchone()

            self.split_weight.setText(string.get_str(split_weight))

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.model.update_routing_hydrology_link(self.id, self.geom,
                                                    string.get_sql_float(self.cross_section.text()),string.get_sql_float(self.slope.text()),
                                                    string.get_sql_float(self.length.text()), string.get_sql_float(self.split_coef.text()), self.name.text(), comment=self.notes.text())
        SimpleLinkDialog.save(self)
        self.close()
