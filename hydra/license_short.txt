Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand of Setec Hydratec, Paris.<br>
<br>
Contact: <a href='mailto:contact@hydra-software.net'>contact@hydra-software.net</a><br>
Support: <a href='mailto:support@hydra-software.net'>support@hydra-software.net</a><br>
Website: <a href='http://hydra-software.net/'>http://hydra-software.net/</a><br>
<br>
You can use this program under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.<br>
<br>
You should have received a copy of the GNU General Public License along with this program. If not, see <a href='http://www.gnu.org/licenses/'>GNU website</a>.