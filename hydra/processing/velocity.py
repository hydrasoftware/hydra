import shutil
from pathlib import Path
from multiprocessing import cpu_count

from qgis.PyQt.QtCore import QCoreApplication
from qgis import processing
from qgis.core import (
    QgsRasterLayer,
    QgsProject,
    QgsProcessing,
    QgsProcessingException,
    QgsProcessingAlgorithm,
    QgsProcessingParameterRasterDestination,
    QgsProcessingParameterRasterLayer,
    QgsProcessingParameterNumber,
    QgsProcessingParameterString,
    QgsCoordinateReferenceSystem
)
from .crgeng import run_crgeng
from .utility import ProjectWidget, ScenarioWidget


QML_DIR = Path(__file__).parent.parent / "ressources" / "qml"

CONTOUR_CTL_CONTENT = """*WRKSPACE
{workspace}

*GRID_VRT
{terrain}

*MODE CONTOUR
1
{project_name} {model} {scenario} {time_step}
0

*OPTION_RES
LAMB93
VIT_ASC

"""


class VelocityRaster(QgsProcessingAlgorithm):
    """
    Algorithm creating a Velocity raster from terrain and crgeng processing
    """

    INPUT = "INPUT"
    OUTPUT = "OUTPUT"
    PROJECT = "PROJECT"
    SCENARIO = "SCENARIO"
    TIME_STEP = 'TIME_STEP'

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return VelocityRaster()

    def name(self):
        return "velocity_raster"

    def displayName(self):
        return self.tr("crgeng Velocity raster")

    def group(self):
        return self.tr("River and free surface flow mapping")

    def groupId(self):
        return "surfaceflowmapping"

    def shortHelpString(self):
        return self.tr("Velocity Raster Output")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT,
                self.tr("Terrain Layer"),
                [QgsProcessing.TypeRaster],
            )
        )

        param = QgsProcessingParameterString(self.PROJECT, "Project")
        param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.SCENARIO, "Scenario")
        param.setMetadata({ 'widget_wrapper': {'class': ScenarioWidget}})
        self.addParameter(param)


        self.addParameter(
            QgsProcessingParameterNumber(
                self.TIME_STEP, "Time step",
                QgsProcessingParameterNumber.Double, defaultValue=-999.0
            )
        )

        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.OUTPUT, "Output velocity raster"
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        self.project = Projet(self.parameterAsString(parameters, self.PROJECT, context))
        self.srid = self.project.srid
        self.project_name = self.project.name
        self.workspace_dir = Path(self.project.directory).parent
        self.carto_dir = self.project.carto_dir
        scenario = self.parameterAsString(parameters, self.SCENARIO, context)

        input = self.parameterAsRasterLayer(parameters, self.INPUT, context)
        self.output = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        time_step = self.parameterAsDouble(parameters, self.TIME_STEP, context)


        velocity_folder = Path(self.carto_dir) / 'velocity'
        velocity_folder.mkdir(parents=True, exist_ok=True)

        ctl_file_path = velocity_folder / "vit_asc.ctl"

        # all models ion current scenario
        models = self.project.get_models_from_scenario(scenario)

        if not models:
            raise QgsProcessingException(
                "velocity: No models associated with current scenario, please check scenario's configuration"
            )

        # use to store temp files to be removed after processing
        self.tmpfiles = []
        # velocity_models = tif output files for each model
        velocity_models = []

        for model in models:
            velocity_models.append(
                Path(self.carto_dir) / f"velocity_{scenario.upper()}_{model.upper()}_{time_step}.tif"
            )
            feedback.pushInfo(f"Writing control file to {ctl_file_path}")
            with ctl_file_path.open("wt", encoding="utf-8") as ctl_file:
                ctl_file.write(CONTOUR_CTL_CONTENT.format(
                    workspace=self.workspace_dir,
                    terrain=str(Path(input.source())),
                    project_name=self.project_name,
                    model=model.upper(),
                    scenario=scenario.upper(),
                    time_step=time_step
                ))

            run_crgeng([str(ctl_file_path)], feedback, velocity_folder)

            asc_rasters = list(Path(self.carto_dir).glob(f"{scenario.upper()}_{model.upper()}_{time_step}_vit*.asc"))
            self.tmpfiles.extend(asc_rasters)

            feedback.pushInfo("merging source files")
            processing.run(
                "gdal:merge",
                {
                    "INPUT": [str(p) for p in asc_rasters],
                    "OUTPUT": str(velocity_models[-1]),
                    "NODATA_INPUT": -999,  # don't treat this bad nodata value
                    "NODATA_OUTPUT": -9999,
                    "OPTIONS": f"PREDICTOR=3|COMPRESS=DEFLATE|NUM_THREADS={cpu_count()}"
                },
                # WARNING do not pass context since input files will be kept opened !
            )

        processing.run(
            "gdal:buildvirtualraster",
            {
                "INPUT": [str(v) for v in velocity_models],
                "OUTPUT": self.output,
                "ASSIGN_CRS": f"EPSG:{self.srid}",
                "SEPARATE": False,
            },
        )

        # prepare style for output layer
        shutil.copy(QML_DIR / "raster_vitesses.qml", Path(self.output).with_suffix('.qml'))

        return {self.OUTPUT: self.output}

    def postProcessAlgorithm(self, context, feedback):

        layer = QgsRasterLayer(self.output, Path(self.output).name)
        layer.setCrs(QgsCoordinateReferenceSystem.fromEpsgId(self.srid))
        QgsProject.instance().addMapLayer(layer)

        # cleanup temp files
        for vraster in self.tmpfiles:
            try:
                vraster.unlink()
            except PermissionError as err:
                feedback.pushWarning(f'source raster not deleted, reason: {err}')

        # cleanup gdal metadata if exists
        for xml_metadata in Path(self.carto_dir).glob("*vit*.asc.aux.xml"):
            xml_metadata.unlink()

        return {self.OUTPUT: self.output}
