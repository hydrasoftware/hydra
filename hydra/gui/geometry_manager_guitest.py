# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.geometry_manager_guitest [-dhs] [-g project_name model_name]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name
        run in gui mode

   -s  --solo
        run test in solo mode (create database)
"""

from hydra.utility.string import normalized_name, normalized_model_name
import sys
import getopt
import os
from hydra.project import Project
from qgis.PyQt.QtWidgets import QApplication
from qgis.PyQt.QtCore import QCoreApplication, QTranslator
from hydra.gui.geometry_manager import GeometryManager
from ..database.database import TestProject, project_exists

def gui_mode(project_name):
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)

    test_dialog = GeometryManager(obj_project)
    test_dialog.exec_()
    del app


def test():
    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)

    test_dialog = GeometryManager(obj_project)

    test_dialog.new_cp_geom()
    test_dialog.new_op_geom()
    test_dialog.new_vcs_geom()

    test_dialog.geom_tab.setCurrentIndex(0)
    test_dialog.delete_cp_geom()
    test_dialog.geom_tab.setCurrentIndex(1)
    test_dialog.delete_op_geom()
    test_dialog.geom_tab.setCurrentIndex(2)
    test_dialog.delete_vcs_geom()

    test_file = os.path.join(os.path.dirname(__file__), "test_data", "section_pipe.csv")
    test_dialog.import_csv_file(test_file, "closed")

    test_file = os.path.join(os.path.dirname(__file__), "test_data", "section_channel.csv")
    test_dialog.import_csv_file(test_file, "open")

    test_dialog.save()

    nb_section = obj_project.execute("""select count(*) from model.closed_parametric_geometry""").fetchone()[0]
    assert nb_section == 5

    nb_section = obj_project.execute("""select count(*) from model.open_parametric_geometry""").fetchone()[0]
    assert nb_section == 8

    section1_value = obj_project.execute("""select zbmin_array[1][2] from model.closed_parametric_geometry where name = 'section 1'""").fetchone()[0]
    assert section1_value == 0.3

    section2_value = obj_project.execute("""select zbmin_array[3][2] from model.closed_parametric_geometry where name = 'section 3'""").fetchone()[0]
    assert section2_value == 1.2
    del app

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name, with_model=True)
    test()
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    model_name = args[1]
    geom = [float(a) for a in args[2:]]
    id =geom[2] if len(geom)==3 else None
    project_name = normalized_name(project_name)
    model_name = normalized_model_name(model_name)
    gui_mode(project_name)
    exit(0)

test()
# fix_print_with_import

# fix_print_with_import
print("ok")
exit(0)

