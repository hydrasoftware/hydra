# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run update tests on a project from V0.0.27

Warning : you need to have hydra postgresql in version 0.0.27 installed !

USAGE

   dump_restore_project_test.py [-hk]

OPTIONS

   -h, --help
        print this help

   -k, --keep
        does not delete test database after update

"""

assert __name__ == "__main__"

import sys
import os
import getopt
import psycopg2
import tempfile
from hydra.versions.update import update_project
from hydra.versions.dump_restore import create_db, run_psql, dump_project, copy_project
from hydra.database import database as dbhydra
from hydra.project import Project
from hydra.utility.empty_ui_manager import EmptyUIManager
from hydra.utility.log import LogManager, ConsoleLogger
from hydra.utility.settings_properties import SettingsProperties

__tempdir = tempfile.gettempdir()

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hk",
            ["help", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist
keep = "-k" in optlist or "--keep" in optlist

log = LogManager(ConsoleLogger(), "Hydra")

__currendir = os.path.dirname(__file__)

for project in ['restore_project_test', 'restore_model_test', 'copy_project_test']:
    if dbhydra.project_exists(project):
        dbhydra.remove_project(project)

log.notice("Import project")
create_db(log, 'restore_project_test')

current_dir = os.path.abspath(os.path.dirname(__file__))
run_psql(log, os.path.join(current_dir, "test_data", "restore_project_test.sql"), 'restore_project_test', strict=True)

dbhydra.insert_project_metadata('restore_project_test', 3944)

test_project = Project('restore_project_test', log, EmptyUIManager())
update_project(test_project)

project_1 = Project.load_project('restore_project_test')
assert all(model in project_1.get_models() for model in ['alc', 'mrs', 'pdc'])

manholes, pipes = project_1.execute("select (select count(n.*) from alc.manhole_node as n), (select count(p.*) from alc.pipe_link as p)").fetchone()
assert manholes == 946
assert pipes == 919

log.notice("Export project")
project_temp_file = os.path.join(__tempdir, "_dump_test.sql")
dump_project(log, project_temp_file, 'restore_project_test')

log.notice("Export model 'mrs'")
model_temp_file = os.path.join(__tempdir, "_dump_model_test.sql")
dump_project(log, model_temp_file, 'restore_project_test', 'mrs')

log.notice("Import model 'mrs' into another DB")
dbhydra.create_project('restore_model_test', 3944)
run_psql(log, model_temp_file, 'restore_model_test')

project_2 = Project.load_project('restore_model_test')
assert project_2.get_models() == ['mrs']

manholes, pipes = project_2.execute("select (select count(n.*) from mrs.manhole_node as n), (select count(p.*) from mrs.pipe_link as p)").fetchone()
assert manholes == 7620
assert pipes == 7561

log.notice("Copy project")
copy_project(log, 'restore_project_test', 'copy_project_test')
dbhydra.insert_project_metadata('copy_project_test', 3944)

project_3 = Project.load_project('copy_project_test')
assert all(model in project_3.get_models() for model in ['alc', 'mrs', 'pdc'])

manholes, pipes = project_3.execute("select (select count(n.*) from pdc.manhole_node as n), (select count(p.*) from pdc.pipe_link as p)").fetchone()
assert manholes == 429
assert pipes == 429

if not keep:
    for project in ['restore_project_test', 'copy_project_test']:
        dbhydra.remove_project(project)
        log.notice("Deleting database {}...".format(project))

sys.stdout.write("ok\n")
