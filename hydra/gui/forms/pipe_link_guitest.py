# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.forms.domain.froude_bc_test [-dh] [-g project_name model_name x y x y]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name model_name x y
        run in gui mode

"""

from hydra.database.database import TestProject, remove_project, project_exists
from hydra.utility.string import normalized_name, normalized_model_name
import sys
import getopt

from hydra.project import Project

def gui_mode(project_name, model_name, geom, id):

    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.pipe_link import PipeLinkEditor
    from hydra.utility.string import normalized_name, normalized_model_name

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)

    test_dialog = PipeLinkEditor(obj_project, geom, id)
    test_dialog.exec_()


def test():
    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.pipe_link import PipeLinkEditor
    from hydra.database.database import TestProject

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)

    obj_project.set_current_model(model_name)
    model = obj_project.get_current_model()

    model.add_manhole([-1, 1])
    model.add_manhole([-2, 2])

    test_dialog = PipeLinkEditor(obj_project, [[-1,1],[-2,2]])
    test_dialog.name.setText("Test_pipe_link")
    test_dialog.z_invert_up.setText('12')
    test_dialog.z_invert_down.setText('10')
    test_dialog.h_sable.setText('1')
    test_dialog.ovoid_top_diameter.setText('100')
    test_dialog.ovoid_invert_diameter.setText('101')
    test_dialog.ovoid_height.setText('102')
    test_dialog.combo_cross_section.setCurrentIndex(test_dialog.combo_cross_section.findText('channel'))
    test_dialog.cross_section_changed() # simulates click on combo box
    test_dialog.rk.setText('65')
    test_dialog.comment.setText("EP\ntesté")
    id = test_dialog.get_id()
    test_dialog.save()

    test_dialog = PipeLinkEditor(obj_project, None, id)
    assert test_dialog.name.text()=="Test_pipe_link"
    assert float(test_dialog.z_invert_up.text()) == 12
    assert float(test_dialog.z_invert_down.text()) ==10
    assert float(test_dialog.h_sable.text()) == 1
    assert float(test_dialog.ovoid_top_diameter.text()) == 100
    assert float(test_dialog.ovoid_invert_diameter.text()) == 101
    assert float(test_dialog.ovoid_height.text()) == 102
    assert test_dialog.combo_cross_section.currentText() == 'channel'
    assert float(test_dialog.rk.text()) == 65
    assert test_dialog.comment.toPlainText()=="EP\ntesté"
    test_dialog.close()

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])

except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name, with_model=True)
    test()
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    model_name = args[1]
    geom = [float(a) for a in args[2:]]
    id =geom[2] if len(geom)==3 else None
    project_name = normalized_name(project_name)
    model_name = normalized_model_name(model_name)
    gui_mode(project_name, model_name, geom, id)
    exit(0)

test()
# fix_print_with_import
print("ok")


