# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.radar_grid [-dhs] [-g project_name]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name
        run in gui mode

   -s  --solo
        run test in solo mode (create database)
"""

import sys
import getopt
from hydra.utility.string import normalized_name, normalized_model_name
from hydra.project import Project
from qgis.PyQt.QtWidgets import QApplication, QTableWidgetItem
from qgis.PyQt.QtCore import QCoreApplication, QTranslator
from hydra.gui.radar_grid import RadarGridManager
from hydra.database.database import TestProject, remove_project, project_exists
import hydra.utility.string as string

def gui_mode(project_name):
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    obj_project = Project.load_project(project_name)

    test_dialog = RadarGridManager(obj_project)
    test_dialog.exec_()


def test():
    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)

    test_dialog = RadarGridManager(obj_project)

    # check default values are loaded
    assert float(test_dialog.x0.text()) == 0
    assert float(test_dialog.y0.text()) == 0
    assert float(test_dialog.dx.text()) == 1000
    assert float(test_dialog.dy.text()) == 1000
    assert test_dialog.nx.value() == 0
    assert test_dialog.ny.value() == 0

    # change values
    test_dialog.x0.setText('1617923.93')
    test_dialog.y0.setText('8211996.4')
    test_dialog.dx.setText('1000.0')
    test_dialog.dy.setText('1000.00')
    test_dialog.nx.setValue(62)
    test_dialog.ny.setValue(69)
    test_dialog.save()

    # check values were saved
    assert float(test_dialog.x0.text()) == 1617923.93
    assert float(test_dialog.y0.text()) == 8211996.4
    assert float(test_dialog.dx.text()) == 1000
    assert float(test_dialog.dy.text()) == 1000
    assert test_dialog.nx.value() == 62
    assert test_dialog.ny.value() ==69

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name)
    test()
    # fix_print_with_import
    print("ok")
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    project_name = normalized_name(project_name)
    gui_mode(project_name)
    exit(0)

test()
# fix_print_with_import

# fix_print_with_import
print("ok")
exit(0)
