# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.forms.domain.zq_bc_test [-dh] [-g project_name model_name x y]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name model_name x y

"""

from hydra.database.database import TestProject, remove_project, project_exists
from hydra.utility.string import normalized_name, normalized_model_name
import sys
import getopt

from hydra.project import Project

def gui_mode(project_name, model_name, geom):

    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.reservoir_rs_hydrology_singularity import ReservoirRsHydrologySingularityEditor
    from hydra.utility.string import normalized_name, normalized_model_name

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)

    test_dialog = ReservoirRsHydrologySingularityEditor(obj_project, geom)
    test_dialog.exec_()


def test():
    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.reservoir_rs_hydrology_singularity import ReservoirRsHydrologySingularityEditor
    from hydra.database.database import TestProject

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)
    model = obj_project.get_current_model()

    model.add_manhole_hydrology([22,58])
    model.add_manhole_hydrology([29,55])
    model.add_manhole_hydrology([18,49])
    model.add_manhole_hydrology([15,46])
    model.add_manhole_hydrology([29,32])

    model.add_pipe_link([[22,58], [29,55]], name='pipe1')
    model.add_pipe_link([[22,58], [18,49]], name='pipe2')
    model.add_simple_link('connector_hydrology_link', [[22,58], [15,46]], name='connector1')
    model.add_simple_link('connector_hydrology_link', [[22,58], [29,32]], name='connector2')


    test_dialog = ReservoirRsHydrologySingularityEditor(obj_project, [22,58])
    test_dialog.name.setText('reservoir_rs_test')
    test_dialog.q_drainage.setText("15")
    test_dialog.z_ini.setText("19")
    test_dialog.drainage.setText('pipe1')
    test_dialog.overflow.setText('pipe2')
    test_dialog.treatment_mode.setCurrentIndex(test_dialog.treatment_mode.findText('Efficiency'))
    test_dialog.mes_efficiency_factor.setText('12')
    test_dialog.ntk_efficiency_factor.setText('13')
    test_dialog.dbo5_efficiency_factor.setText('14')
    test_dialog.dco_efficiency_factor.setText('15')
    test_dialog.array.set_table_items([[1,2], [4,5]])
    id_reservoir_rs = test_dialog.get_id()
    test_dialog.save()

    test_dialog = ReservoirRsHydrologySingularityEditor(obj_project, None, id_reservoir_rs)
    assert test_dialog.name.text() =='reservoir_rs_test'
    assert float(test_dialog.q_drainage.text()) == 15
    assert float(test_dialog.z_ini.text()) == 19
    assert test_dialog.drainage.text() == 'pipe1'
    assert test_dialog.overflow.text() == 'pipe2'
    assert test_dialog.treatment_mode.currentText() == 'Efficiency'
    assert float(test_dialog.mes_efficiency_factor.text()) == 12
    assert float(test_dialog.ntk_efficiency_factor.text()) == 13
    assert float(test_dialog.dbo5_efficiency_factor.text()) == 14
    assert float(test_dialog.dco_efficiency_factor.text()) == 15

    def list_equals(list1, list2):
        for val in list1:
            if not val in list2:
                return False
        return True

    assert list_equals(test_dialog.array.get_table_items(),[[1,2], [4,5]])

    test_dialog.drainage.setText('connector1')
    test_dialog.overflow.setText('connector2')
    test_dialog.save()

    test_dialog = ReservoirRsHydrologySingularityEditor(obj_project, None, id_reservoir_rs)
    assert test_dialog.drainage.text() == 'connector1'
    assert test_dialog.overflow.text() == 'connector2'
    test_dialog.reject()

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])

except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name, with_model=True)
    test()
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    model_name = args[1]
    geom = [float(a) for a in args[2:]]
    id =geom[2] if len(geom)==3 else None
    project_name = normalized_name(project_name)
    model_name = normalized_model_name(model_name)
    gui_mode(project_name, model_name, geom, id)
    exit(0)

test()
# fix_print_with_import
print("ok")
