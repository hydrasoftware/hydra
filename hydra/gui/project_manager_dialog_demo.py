# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

def demo():
    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.utility.log import LogManager, ConsoleLogger
    from hydra.utility.empty_ui_manager import EmptyUIManager
    from hydra.gui.project_manager_dialog import ProjectManagerDialog

    import sys


    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)
    pm_dialog = ProjectManagerDialog(LogManager(ConsoleLogger(), "Hydra"), EmptyUIManager())

    pm_dialog.exec_()
    if pm_dialog.new_current_project==None:
       sys.stderr.write("No current project")
    else:
       sys.stderr.write("New current project to open: "+str(pm_dialog.new_current_project))

demo()
