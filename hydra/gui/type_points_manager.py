# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os

from qgis.PyQt import uic
from hydra.gui.base_dialog import BaseDialog, tr
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget

class PointManager(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "type_points_manager.ui"), self)
        self.buttonBox.accepted.connect(self.sav)
        self.buttonBox.rejected.connect(self.close)

        self.project = project

        self.type_points_table = HydraTableWidget(self.project, ("id", "name", "comment"),(tr("Id"), tr("Name"), tr("Comment")),
                                "project", "points_type",
                                (self.new_type_points, self.delete_type_points), "", "id",
                                self.place_holder_table)

        self.type_points_table.set_editable(True)
        self.type_points_table.data_edited.connect(self.save_row)

    def save_row(self):
        self.type_points_table.save_selected_row()

    def new_type_points(self):
        self.type_points_table.add_row(['default', ''])

    def delete_type_points(self):
        self.type_points_table.del_selected_row()

    def sav(self):
        self.save()
        self.close()