/* ********************************************************************************** */
/* Create flood plain transects, river cross section profiles and associated          */
/* valley geometries from line xyz                                                    */
/* ********************************************************************************** */

select $model.create_river_cross_section_from_line_xyz(id) from $model.flood_plain_transect_candidate
;;
