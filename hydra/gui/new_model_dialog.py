# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from qgis.PyQt.QtWidgets import QDialog
from qgis.PyQt import uic
import re
import os
from ..utility.string import normalized_model_name

class NewModelDialog(QDialog):
    def __init__(self, project, parent=None):
        QDialog.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "new_model_dialog.ui"), self)
        self.lblInfo.text=''
        self.buttonBox.accepted.connect(self.save)
        self.edit_project_name.textChanged.connect(self.__edit_project_name_changed)
        self.project = project

    def __edit_project_name_changed(self):
        modelname = str(self.edit_project_name.text())
        new_modelname = normalized_model_name(modelname)
        self.lblInfo.setText("" if modelname==new_modelname else new_modelname)

    def save(self):
        if self.edit_project_name.text() != '':
            self.project.add_new_model(normalized_model_name(str(self.edit_project_name.text())))
            self.project.set_current_model(normalized_model_name(str(self.edit_project_name.text())))
