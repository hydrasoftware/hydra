# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


assert __name__ == "__main__"

import sys
from qgis.PyQt.QtWidgets import QApplication
from qgis.PyQt.QtCore import QCoreApplication, QTranslator
from hydra.project import Project
from hydra.gui.new_model_dialog import NewModelDialog
from hydra.database.database import TestProject, project_exists, remove_project

try:
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "new_model_dialog_test"

    if project_exists(project_name):
        remove_project(project_name)

    model_name = "model"

    test_project = TestProject(project_name)

    obj_project = Project.load_project(project_name)

    if model_name in obj_project.get_models():
        obj_project.delete_model(model_name)

    new_project_dialog = NewModelDialog(obj_project)
    new_project_dialog.edit_project_name.setText(model_name)
    new_project_dialog.save()

except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

# fix_print_with_import
print("ok")
