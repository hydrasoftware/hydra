# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import re
from qgis.PyQt.QtWidgets import QWidget, QHBoxLayout, QSpacerItem, QSizePolicy

from numpy import array
from ..widgets.array_widget import ArrayWidget
from .geometry import Geometry

def sz(zbmin_array):
    return [[-.5*b,  z] for z, b in reversed(zbmin_array)] + [[.5*b,  z] for z, b in zbmin_array] + [[-0.5*zbmin_array[-1][1], zbmin_array[-1][0]]]

class PipeGeometry(Geometry):
    def __init__(self, zbmin_array, dz=0, transect=None, terrain=None, parent=None):
        super().__init__(sz(zbmin_array), dz, transect, terrain, parent=parent)
        l = QHBoxLayout()
        w = QWidget()
        l.addWidget(w)
        l.addItem(QSpacerItem(1,1,vPolicy=QSizePolicy.Expanding))
        self.setLayout(l)
        self.__zbmin_array = zbmin_array
        self.zbmin_array = ArrayWidget(["h[m]", "b [m]"], [20,2], zbmin_array, parent=w)
        self.zbmin_array.data_edited.connect(self.__zb_changed)

    def __zb_changed(self, zbmin_array):
        self.set_sz(sz(zbmin_array))
        self.__zbmin_array = zbmin_array
        self.geometry_changed.emit({'zbmin_array': zbmin_array})

    def __getattr__(self, name):
        if name == 'geom':
            return self.__geometry
        elif name == 'param':
            return {
            "zbmin_array": self.__zbmin_array
            }
        else:
            return super().__getattr__(name)
