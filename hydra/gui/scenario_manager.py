from builtins import str
from builtins import range
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import csv
import datetime
from qgis.PyQt import uic, QtCore
from qgis.PyQt.QtWidgets import QFileDialog, QMessageBox, QApplication
from hydra.gui.base_dialog import BaseDialog, tr
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from hydra.gui.scn_widgets.comput_options import ComputOptionsWidget
from hydra.gui.scn_widgets.comput_settings import ComputSettingsWidget
from hydra.gui.scn_widgets.hydrology_settings import HydrologySettingsWidget
from hydra.gui.scn_widgets.model_ordering import ModelOrderingWidget
from hydra.gui.scn_widgets.transport import TransportWidget
from hydra.gui.scn_widgets.aeraulics import AeraulicsWidget
from hydra.gui.scn_widgets.regulation_and_configuration import RegulationConfigurationWidget
from hydra.utility.system import open_external

_desktop_dir = os.path.join(os.path.expanduser('~'), "Desktop")

def _quote(value):
    return "'"+value+"'" \
            if (isinstance(value, str) or isinstance(value, str))\
            and ( value == "" or value[0] != "'") and value != "null" else value

class ScenarioManager(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "scenario_manager.ui"), self)
        self.setWindowFlags(self.windowFlags() |
                              QtCore.Qt.WindowSystemMenuHint |
                              QtCore.Qt.WindowMinMaxButtonsHint)
        self.setWindowState(QtCore.Qt.WindowMaximized)

        self.is_loading = True

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.clone.clicked.connect(self.clone_scenario)
        self.import_btn.clicked.connect(self.__import)
        self.btnHelp.clicked.connect(self.help_import_file)

        self.comput_options = ComputOptionsWidget(project, self.comput_options_placeholder)
        self.comput_settings = ComputSettingsWidget(project, self.comput_settings_placeholder)
        self.hydrology_settings = HydrologySettingsWidget(project, self.hydrology_settings_placeholder)
        self.model_ordering = ModelOrderingWidget(project, self.model_ordering_placeholder)
        self.transport = TransportWidget(project, self.transport_placeholder)
        self.aeraulics = AeraulicsWidget(project, self.aeraulics_placeholder)
        self.regulation_and_configuration = RegulationConfigurationWidget(project, self.regulation_and_configuration_placeholder)


        self.table_scenario = HydraTableWidget(project,
            ("id", "name", "comment"),
            (tr("Id"), tr("Name"), tr("Comment")),
            "project", "scenario",
            (self.new_scenario, self.delete_scenario), "", "id",
            self.table_scenario_placeholder, autoInit=False)
        self.table_scenario.update_data("name")
        self.table_scenario.table.resizeColumnToContents(1)
        self.table_scenario.set_editable(True)
        self.table_scenario.data_edited.connect(self.update_scenario_data)
        self.table_scenario.setDelegate("LineEdit", 1, {'maxlen': 24})

        self.active_scn_id = None
        self.set_active_scenario()
        self.table_scenario.table.itemSelectionChanged.connect(self.set_active_scenario)

        self.comput_settings.hydraulic.toggled.connect(self.hydraulic_mode_changed)

        self.comput_settings.flag_rest.toggled.connect(self.source_scn_activated)
        self.comput_settings.flag_rstart.toggled.connect(self.source_scn_activated)
        self.comput_settings.scenario_rstart.currentIndexChanged.connect(self.source_scn_activated)
        self.comput_settings.scenario_ref_btn.stateChanged.connect(self.source_scn_activated)
        self.comput_settings.scenario_ref.currentIndexChanged.connect(self.source_scn_activated)

        self.hydraulic_mode_changed()
        self.refresh_source_scn()

        self.is_loading = False

    def __import(self):
        '''Import file into scenario'''
        fileName, __ = QFileDialog.getOpenFileName(None, tr('Import from file'), _desktop_dir, tr('Csv files (*.csv)'))
        if fileName:
            nb_scn = self.import_file(fileName)
            if nb_scn > 0 :
                self.table_scenario.update_data("name")
                self.table_scenario.table.resizeColumnToContents(1)
                msg = QMessageBox(QMessageBox.Information, tr('Imported scenarios'), tr('Import successfull, {} scenarios imported'.format(nb_scn)), QMessageBox.Ok)
                msg.exec_()

    def import_file(self,fileName):
        '''Reads file into line arrays and handle import'''
        content = []
        if fileName :
            with open(fileName,'r') as scenario_csv :
                scenario_reader = csv.reader(scenario_csv,delimiter=";")
                header = next(scenario_reader)
                for row in scenario_reader :
                    content.append(row)
            if header[0].strip() not in ['! Scenario','! scenario','!Scenario'] :
                msg = QMessageBox(QMessageBox.Warning, tr('Not the right key word'), tr("Expected the keyword : '! Scenario' at the beginning of the file, found  {}.\tCheck the format of your input file.".format(header[0])), QMessageBox.Ok)
                msg.exec_()
                return 0
            else :
                cmpt = 0
                self.current_line = 0
                while self.current_line < len(content):
                    try :
                        if content[self.current_line][0] != "" and content[self.current_line][0][0] != "!":
                            scn_fields, scn_external = self.__parse_file(content)
                            self.__import_scn(scn_fields, scn_external)
                            cmpt += 1
                        else :
                            self.current_line += 1
                    except :
                        self.project.log.error("An error as occured while trying to import line {} of file {}.".format(self.current_line, fileName))
                        raise
                return cmpt

    def __parse_file(self, content) :
        '''Parses file's content to order data for 1 scenario'''
        # Fields written in scenario table
        scn_fields = {}
        scn_fields['name'] = content[self.current_line][0]
        scn_fields['scenario_ref'] = content[self.current_line][1]
        scn_fields['soil_cr_alp'] = content[self.current_line+1][4]
        scn_fields['soil_horner_sat'] = content[self.current_line+1][4]
        scn_fields['soil_holtan_sat'] = content[self.current_line][4]
        scn_fields['soil_scs_sat'] = content[self.current_line][4]
        scn_fields['soil_hydra_psat'] = content[self.current_line][4]
        scn_fields['soil_hydra_rsat'] = content[self.current_line][4]
        scn_fields['soil_gr4_psat'] = content[self.current_line][4]
        scn_fields['soil_gr4_rsat'] = content[self.current_line][4]
        scn_fields['dt_hydrol_mn'] = str(datetime.timedelta(seconds = float(content[self.current_line][7])*60))
        scn_fields['date0'] = content[self.current_line][8]
        scn_fields['tfin_hr'] = str(datetime.timedelta(seconds = float(content[self.current_line][9])*60*60))
        scn_fields['tini_hydrau_hr'] = str(datetime.timedelta(seconds = float(content[self.current_line][10])*60*60))
        scn_fields['dtmin_hydrau_hr'] = str(datetime.timedelta(seconds = float(content[self.current_line][11])*60*60))
        scn_fields['dtmax_hydrau_hr'] = str(datetime.timedelta(seconds = float(content[self.current_line][12])*60*60))
        scn_fields['dzmin_hydrau'] = content[self.current_line][13]
        scn_fields['dzmax_hydrau'] = content[self.current_line][14]
        scn_fields['flag_save'] = "t" if content[self.current_line][15]==1 else "f"
        scn_fields['tsave_hr'] = str(datetime.timedelta(seconds = float(content[self.current_line+1][15])*60*60))
        scn_fields['dt_output_hr'] = str(datetime.timedelta(seconds = float(content[self.current_line][16])*60*60))
        scn_fields['tend_output_hr'] = scn_fields['tfin_hr']
        scn_fields['flag_gfx_control'] = "t" if content[self.current_line][17]==1 else "f"
        scn_fields['scenario_rstart'] = content[self.current_line][19]
        scn_fields['trstart_hr'] = str(datetime.timedelta(seconds = float(content[self.current_line][20])*60*60))

        # Fields wwritten in parameter_XXX.dat file
        scn_external = {}
        if content[self.current_line][2] != "" :
            scn_external['F_QTS'] = [content[self.current_line][2]]
        if content[self.current_line][3] != "" :
            scn_external['F_PLUVIO'] = [content[self.current_line][3]]
        if content[self.current_line][24] != "" :
            scn_external['F_SOR'] = [content[self.current_line][24]]
        if content[self.current_line][21] != "" :
            scn_external['F_HY'] = [content[self.current_line][21]]
        if content[self.current_line][22] != "" :
            scn_external['F_REGUL'] = [content[self.current_line][22]]
        if content[self.current_line][23] != "" :
            scn_external['F_MES'] = [content[self.current_line][23]]
        if content[self.current_line][25] != "" :
            scn_external['F_DEROUT'] = [content[self.current_line][25]]
        self.current_line += 1

        while self.current_line < len(content) \
                and content[self.current_line][0] == "" \
                and any(s!="" for s in content[self.current_line]) :
            if content[self.current_line][21] != "" :
                scn_external['F_HY'].append(content[self.current_line][21])
            if content[self.current_line][22] != "" :
                scn_external['F_REGUL'].append(content[self.current_line][22])
            if content[self.current_line][23] != "" :
                scn_external['F_MES'].append(content[self.current_line][23])
            if content[self.current_line][25] != "" :
                scn_external['F_DEROUT'].append(content[self.current_line][25])
            self.current_line += 1

        return scn_fields, scn_external

    def __import_scn(self, scn_fields, scn_external):
        '''Inserts scenario in database
           Creates parameter_xxx.dat associated file'''

        ref_id = self.project.execute("""select id from project.scenario where scenario.name = '{}'""".format(scn_fields['scenario_ref'])).fetchone()
        if ref_id is not None:
            scn_fields['scenario_ref'] = str(ref_id[0])
        else:
            scn_fields.pop('scenario_ref')
        rstart_id = self.project.execute("""select id from project.scenario where scenario.name = '{}'""".format(scn_fields['scenario_rstart'])).fetchone()
        if rstart_id is not None:
            scn_fields['flag_rstart'] = "t"
            scn_fields['scenario_rstart'] = str(rstart_id[0])
        else:
            scn_fields.pop('scenario_rstart')

        param_dat_file = os.path.join(self.project.data_dir, 'parameter_'+scn_fields['name']+'.dat')
        scn_fields['option_file'] = "'t'"
        scn_fields['option_file_path'] = param_dat_file

        sql = ("""insert into project.scenario({}) values({}) returning id""".format(
                ",".join(list(scn_fields.keys())),
                ",".join(_quote(str(value)) for value in list(scn_fields.values()))))
        new_id, = self.project.execute(sql).fetchone()

        with open(param_dat_file, 'w') as f:
            for keyword, files in sorted(list(scn_external.items())):
                f.write('*'+keyword+'\n')
                for file in files:
                    f.write(file+'\n')
                f.write('\n')

    def help_import_file(self):
        file = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'ressources', 'notes_techniques', 'notice_scn_import.pdf'))
        open_external(file)

    def hydraulic_mode_changed(self):
        self.hydrology_settings.hydro_from_other_scn.setEnabled(self.comput_settings.hydraulic.isChecked())
        if self.comput_settings.hydraulic.isChecked():
            scenario_hydrol_id = self.project.execute("""select scenario_hydrol from project.scenario where id={}""".format(self.active_scn_id)).fetchone()[0]
            if scenario_hydrol_id is not None:
                self.hydrology_settings.hydro_from_other_scn.set_selected_value(scenario_hydrol_id)
            else:
                self.hydrology_settings.hydro_from_other_scn.setCurrentIndex(0)
        else:
            self.hydrology_settings.hydro_from_other_scn.set_selected_value(None)

    def source_scn_activated(self):
        if self.active_scn_id and not self.is_loading:
            self.refresh_source_scn()

    def refresh_source_scn(self):
        '''disable useless parts of the scenario parameters UI based
        on the activation of a Reference Scenario or Hot-start scenario'''
        if self.active_scn_id:
            hot_start_scenario = self.comput_settings.scenario_rstart.get_selected_value()
            ref_scenario = self.comput_settings.scenario_ref.get_selected_value()

            hot_start_active = hot_start_scenario and self.comput_settings.flag_rstart.isChecked()
            ref_scenario_active = ref_scenario and self.comput_settings.scenario_ref_btn.isChecked()

            if hot_start_active or ref_scenario_active:
                self.regulation_and_configuration.gb_config.setEnabled(False)
                #self.model_ordering.setEnabled(False)
                if self.active_scn_id:
                    self.project.execute("""delete from project.config_scenario where scenario={};""".format(self.active_scn_id))
                    #self.__copy_model_ordering(hot_start_scenario or ref_scenario)

                if hot_start_active:
                    self.comput_options.domains.setEnabled(False)
                    self.comput_options.runoff.setEnabled(False)
                    self.comput_options.kernelVersion.setEnabled(False)
                    self.transport.setEnabled(False)
                    if self.active_scn_id:
                        self.__copy_config(hot_start_scenario)
                        self.__copy_comput_option(hot_start_scenario)
                        self.__copy_transport(hot_start_scenario)

            else:
                self.regulation_and_configuration.gb_config.setEnabled(True)
                #self.model_ordering.setEnabled(True)
                if not hot_start_active:
                    self.comput_options.domains.setEnabled(True)
                    self.comput_options.runoff.setEnabled(True)
                    self.comput_options.kernelVersion.setEnabled(True)
                    self.transport.setEnabled(True)

            self.regulation_and_configuration.set(self.active_scn_id)
            #self.model_ordering.set(self.active_scn_id)
            self.comput_options.set(self.active_scn_id)
            self.transport.set(self.active_scn_id)
            self.aeraulics.set(self.active_scn_id)


    def __copy_config(self, source_scn_id):
        ''' sets configuration parameters to a
        copy of those of the source scenario'''
        self.project.execute("""delete from project.config_scenario where scenario={};""".format(self.active_scn_id))
        if source_scn_id:
            self.project.execute("""insert into project.config_scenario(scenario, model, configuration, priority)
                                        select {}, model, configuration, priority
                                        from project.config_scenario where scenario={}
                                        """.format(self.active_scn_id, source_scn_id))

    def __copy_model_ordering(self, source_scn_id):
        ''' sets model_ordering parameters to a
        copy of those of the source scenario'''
        self.project.execute("""update project.scenario set model_connect_settings='cascade' where id={};""".format(self.active_scn_id))

        if source_scn_id:
            self.project.execute("""update project.scenario
                set model_connect_settings= (select model_connect_settings from project.scenario where id={})
                where id={};""".format(source_scn_id, self.active_scn_id))

            self.project.execute("""
                do
                $$
                    declare
                        old_group project.mixed%rowtype;
                    begin
                        for old_group in select * from project.mixed where scenario={old_scn} loop
                            with new_group as (
                                insert into project.grp default values returning id
                            ),
                            grp_mod as (
                                insert into project.grp_model(grp, model)
                                select n_grp.id, grp_model.model
                                from new_group as n_grp, project.grp_model
                                where grp_model.grp=old_group.grp
                            )
                            insert into project.mixed (scenario, ord, grp)
                            select {scn}, mx.ord, n_grp.id
                            from project.mixed as mx, new_group as n_grp
                            where mx.scenario={old_scn} and mx.grp=old_group.grp
                            on conflict do nothing;
                        end loop;
                    end;
                $$;
                ;;
                """.format(scn=self.active_scn_id, old_scn=source_scn_id))

    def __copy_comput_option(self, source_scn_id):
        ''' sets computation options parameters to a
        copy of those of the source scenario'''
        self.project.execute("""update project.scenario
            set (c_affin_param, domain_2d_param, kernel_version) =
                ('f', 'f', null)
            where id={};""".format(self.active_scn_id))

        self.project.execute("""delete from project.c_affin_param where scenario={};""".format(self.active_scn_id))
        self.project.execute("""delete from project.domain_2d_param where scenario={};""".format(self.active_scn_id))
        if source_scn_id:
            self.project.execute("""update project.scenario
                set (c_affin_param, domain_2d_param, t_affin_start_hr, t_affin_min_surf_ddomains, kernel_version) =
                    (select c_affin_param, domain_2d_param, t_affin_start_hr, t_affin_min_surf_ddomains, kernel_version from project.scenario where id={})
                where id={};""".format(source_scn_id, self.active_scn_id))

            self.project.execute("""
                insert into project.c_affin_param (scenario, model, type_domain, element, affin_option)
                select {}, model, type_domain, element, affin_option
                from project.c_affin_param where scenario={}
                """.format(self.active_scn_id, source_scn_id))

            self.project.execute("""
                insert into project.domain_2d_param (scenario, model, domain_2d)
                select {}, model, domain_2d
                from project.domain_2d_param where scenario={}
                """.format(self.active_scn_id, source_scn_id))

            # self.project.execute("""
                # insert into project.output_option (scenario, model, type_element, element)
                # select {}, model, type_element, element
                # from project.output_option where scenario={}
                # """.format(self.active_scn_id, source_scn_id))

            # self.project.execute("""
                # insert into project.strickler_param (scenario, model, type_domain, element, rkmin, rkmaj, pk1, pk2)
                # select {}, model, type_domain, element, rkmin, rkmaj, pk1, pk2
                # from project.strickler_param where scenario={}
                # """.format(self.active_scn_id, source_scn_id))

    def __copy_transport(self, source_scn_id):
        ''' sets transport parameters to a
        copy of those of the source scenario'''
        self.project.execute("""update project.scenario set transport_type = 'no_transport' where id={};""".format(self.active_scn_id))

        self.project.execute("""delete from project.param_measure where param_scenario={};""".format(self.active_scn_id))

        if source_scn_id:
            self.project.execute("""update project.scenario
                set (transport_type, iflag_mes, iflag_dbo5, iflag_dco, iflag_ntk, quality_type, diffusion, dispersion_coef, longit_diffusion_coef,
                     lateral_diff_coef, water_temperature_c, min_o2_mgl, kdbo5_j_minus_1, koxygen_j_minus_1, knr_j_minus_1, kn_j_minus_1, bdf_mgl,
                     o2sat_mgl, iflag_ecoli, iflag_enti, iflag_ad1, iflag_ad2, ecoli_decay_rate_jminus_one, enti_decay_rate_jminus_one,
                     ad1_decay_rate_jminus_one, ad2_decay_rate_jminus_one, sst_mgl, sediment_file, suspended_sediment_fall_velocity_mhr) =
                    (select transport_type, iflag_mes, iflag_dbo5, iflag_dco, iflag_ntk, quality_type, diffusion, dispersion_coef, longit_diffusion_coef,
                     lateral_diff_coef, water_temperature_c, min_o2_mgl, kdbo5_j_minus_1, koxygen_j_minus_1, knr_j_minus_1, kn_j_minus_1, bdf_mgl,
                     o2sat_mgl, iflag_ecoli, iflag_enti, iflag_ad1, iflag_ad2, ecoli_decay_rate_jminus_one, enti_decay_rate_jminus_one,
                     ad1_decay_rate_jminus_one, ad2_decay_rate_jminus_one, sst_mgl, sediment_file, suspended_sediment_fall_velocity_mhr
                     from project.scenario where id={})
                where id={};""".format(source_scn_id, self.active_scn_id))

            self.project.execute("""
                insert into project.param_measure (param_scenario, measure_file)
                select {}, measure_file
                from project.param_measure where param_scenario={}
                """.format(self.active_scn_id, self.active_scn_id))

    def clone_scenario(self):
        if self.active_scn_id is None:
            return
        else:
            self.save_active_scn()
            scn_fields = """comment, comput_mode, dry_inflow, rainfall, dt_hydrol_mn,
                            soil_cr_alp, soil_horner_sat, soil_holtan_sat, soil_scs_sat,
                            soil_hydra_psat, soil_hydra_rsat, soil_gr4_psat, soil_gr4_rsat,
                            option_dim_hydrol_network, date0, tfin_hr, tini_hydrau_hr, dtmin_hydrau_hr, dtmax_hydrau_hr,
                            dzmin_hydrau, dzmax_hydrau, flag_save, tsave_hr, flag_rstart, flag_gfx_control, scenario_rstart,
                            flag_hydrology_rstart, scenario_hydrology_rstart, trstart_hr, graphic_control, model_connect_settings,
                            tbegin_output_hr, tend_output_hr, dt_output_hr, c_affin_param, domain_2d_param, t_affin_start_hr, t_affin_min_surf_ddomains,
                            strickler_param, option_file, option_file_path, output_option, sor1_mode, dt_sor1_hr, transport_type, iflag_mes, iflag_dbo5, iflag_dco, iflag_ntk,
                            quality_type, diffusion, dispersion_coef, longit_diffusion_coef, lateral_diff_coef, water_temperature_c,
                            min_o2_mgl, kdbo5_j_minus_1, koxygen_j_minus_1, knr_j_minus_1, kn_j_minus_1, bdf_mgl, o2sat_mgl, iflag_ecoli, iflag_enti, iflag_ad1, iflag_ad2,
                            ecoli_decay_rate_jminus_one, enti_decay_rate_jminus_one, ad1_decay_rate_jminus_one, ad2_decay_rate_jminus_one, sst_mgl, sediment_file,
                            suspended_sediment_fall_velocity_mhr, scenario_ref, scenario_hydrol, kernel_version"""

            id_new_scn, = self.project.execute("""
                insert into project.scenario(name, {fields})
                select name||'_copy', {fields}
                from project.scenario where id={scn_id}
                returning id;""".format(
                fields=scn_fields, scn_id=self.active_scn_id)).fetchone()

            c_aff_fields = """model, type_domain, element, affin_option"""
            inflow_fields = """model_from, hydrograph_from, model_to, hydrograph_to, option, parameter"""
            out_fields = """model, type_element, element"""
            ext_hy_fields = """external_file"""
            hy_fields = """model, hydrograph_from, hydrograph_to"""
            domain_2d_fields = """model, domain_2d"""
            measure_fields = """measure_file"""
            regulation_fields = """control_file, rank_"""
            strickler_fields = """model, type_domain, element, rkmin, rkmaj, pk1, pk2"""
            config_fields = """model, configuration, priority"""

            self.project.execute("""
                insert into project.c_affin_param (scenario, {fields})
                select {scn}, {fields}
                from project.c_affin_param where scenario={old_scn}
                """.format(fields=c_aff_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.inflow_rerouting (scenario, {fields})
                select {scn}, {fields}
                from project.inflow_rerouting where scenario={old_scn}
                """.format(fields=inflow_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                do
                $$
                    declare
                        old_group project.mixed%rowtype;
                    begin
                        for old_group in select * from project.mixed where scenario={old_scn} loop
                        with new_group as (
                            insert into project.grp default values returning id
                        ),
                        grp_mod as (
                            insert into project.grp_model(grp, model)
                            select n_grp.id, grp_model.model
                            from new_group as n_grp, project.grp_model
                            where grp_model.grp=old_group.grp
                        )
                        insert into project.mixed (scenario, ord, grp)
                        select {scn}, mx.ord, n_grp.id
                        from project.mixed as mx, new_group as n_grp
                        where mx.scenario={old_scn} and mx.grp=old_group.grp;
                        end loop;
                    end;
                $$;
                ;;
                """.format(scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.output_option (scenario, {fields})
                select {scn}, {fields}
                from project.output_option where scenario={old_scn}
                """.format(fields=out_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.param_external_hydrograph (param_scenario, {fields})
                select {scn}, {fields}
                from project.param_external_hydrograph where param_scenario={old_scn}
                """.format(fields=ext_hy_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.param_hydrograph (param_scenario, {fields})
                select {scn}, {fields}
                from project.param_hydrograph where param_scenario={old_scn}
                """.format(fields=hy_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.domain_2d_param (scenario, {fields})
                select {scn}, {fields}
                from project.domain_2d_param where scenario={old_scn}
                """.format(fields=domain_2d_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.param_measure (param_scenario, {fields})
                select {scn}, {fields}
                from project.param_measure where param_scenario={old_scn}
                """.format(fields=measure_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.param_regulation (param_scenario, {fields})
                select {scn}, {fields}
                from project.param_regulation where param_scenario={old_scn}
                """.format(fields=regulation_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.strickler_param (scenario, {fields})
                select {scn}, {fields}
                from project.strickler_param where scenario={old_scn}
                """.format(fields=strickler_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.project.execute("""
                insert into project.config_scenario (scenario, {fields})
                select {scn}, {fields}
                from project.config_scenario where scenario={old_scn}
                """.format(fields=config_fields, scn=id_new_scn, old_scn=self.active_scn_id))

            self.table_scenario.update_data("name")
            self.table_scenario.table.resizeColumnToContents(1)
            self.select_scenario_by_id(id_new_scn)

    def delete_scenario(self):
        if self.active_scn_id is not None:
            self.project.execute("""delete from project.scenario where id={0}""".format(self.active_scn_id))
            self.active_scn_id = None
            self.table_scenario.update_data("name")
            self.table_scenario.table.resizeColumnToContents(1)

    def new_scenario(self):
        self.save_active_scn()
        self.active_scn_id=None
        id_scn = self.project.add_new_scenario()
        self.table_scenario.update_data("name")
        self.table_scenario.table.resizeColumnToContents(1)
        self.select_scenario_by_id(id_scn)

    def select_scenario_by_name(self, scenario_name):
        for i in range(0, self.table_scenario.table.rowCount()):
            if self.table_scenario.table.item(i,1).text()==scenario_name:
                self.table_scenario.table.setCurrentCell(i,0)

    def select_scenario_by_id(self, scenario_id):
        for i in range(0, self.table_scenario.table.rowCount()):
            if self.table_scenario.table.item(i,0).text()==str(scenario_id):
                self.table_scenario.table.setCurrentCell(i,0)

    def set_active_scenario(self):
        items = self.table_scenario.table.selectedItems()
        self.is_loading = True
        if len(items)>0:
            if self.active_scn_id is not None:
                self.save_active_scn()
            self.active_scn_id = items[0].text()
            self.labelSelectedScn.setText(items[1].text())
            self.init_ui_scn_settings()
        else:
            self.active_scn_id = None
            self.labelSelectedScn.setText("")
            self.gbxScenarioSettings.setEnabled(False)
        self.is_loading = False

    def init_ui_scn_settings(self):
        self.gbxScenarioSettings.setEnabled(True)
        if self.active_scn_id is not None:
            self.comput_options.set(self.active_scn_id)
            self.comput_settings.set(self.active_scn_id)
            self.model_ordering.set(self.active_scn_id)
            self.hydrology_settings.set(self.active_scn_id)
            self.transport.set(self.active_scn_id)
            self.aeraulics.set(self.active_scn_id)
            self.regulation_and_configuration.set(self.active_scn_id)

            self.hydraulic_mode_changed()
            self.refresh_source_scn()

    def update_scenario_data(self):
        self.table_scenario.table.resizeColumnToContents(1)
        self.save_active_scn()

    def save_active_scn(self):
        if self.active_scn_id is not None:
            values_to_update=[]
            for i in range(0, self.table_scenario.table.rowCount()):
                if int(self.active_scn_id) == int(self.table_scenario.table.item(i,0).text()):
                    if self.table_scenario.table.item(i, 1) is not None:
                        values_to_update.append("name={}".format(_quote(str(self.table_scenario.table.item(i, 1).text()))))
                    if self.table_scenario.table.item(i, 2) is not None:
                        values_to_update.append("comment={}".format(_quote(str(self.table_scenario.table.item(i, 2).text()))))

            self.project.execute("""update project.scenario set {} where id={}""".format( ", ".join(values_to_update), self.active_scn_id))

            self.comput_settings.save()
            self.comput_options.save()
            self.hydrology_settings.save()
            self.model_ordering.save()
            self.transport.save()
            self.aeraulics.save()
            self.regulation_and_configuration.save()

    def save(self):
        self.save_active_scn()
        if self.active_scn_id:
            self.project.set_current_scenario(self.active_scn_id)
        BaseDialog.save(self)
        self.close()

if __name__=='__main__':
    from hydra.project import Project
    from qgis.PyQt.QtWidgets import QApplication
    import sys

    app = QApplication(sys.argv)
    project = Project(sys.argv[1])
    manager = ScenarioManager(project)
    manager.exec_()


