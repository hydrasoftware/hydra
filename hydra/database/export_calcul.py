# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

# 2017 : creation
# December 2024 / CHV : export of computing options reviewed according to note NT82

"""
export database for computation

USAGE

   python -m hydra.database.export_calcul <project> [<scenario>]

   if scenario is not supplied, a default one named scn1 is created
"""

import os
import sys
import re
import numpy
import struct
import datetime
from hydra.model import Model, topo_error_dict
from hydra.utility import string
from hydra.utility.sql_json import instances
from hydra.utility.log import ExportLogManager


_current_dir = os.path.abspath(os.path.dirname(__file__))

class ExportError(Exception):
    pass

class ExportCalcul(object):

    def __init__(self, project, log_manager=None):
        self.instances = instances
        self.__record_length = {"LI2": 1024, "SECTION.DAT": 256}
        self.project = project
        self.__node_max = 0
        self.__icount_hydrol = 0
        self.log = log_manager or ExportLogManager(project.log.logger, project.log.title)

    def __writ_parametres_generaux(self, stream_, res, itype, npt, coment):
        """ writing first line of a bloc (general parameters) """
        numel=len(res) # number of elements
        if (numel>0):
            stream_.write("$ "+coment+"\n")
            if (npt>=0):
                stream_.write("%5d %9d %5d\n" % (itype,numel,npt))
            else:
                stream_.write("%5d %9d \n" % (itype,numel))
        return

    def __link_request(self, nommodel, srequete, stream_, itypehydra, npt, coment):
        """ query the database and obtain the links of the group
            common writings: list of the links of the group  """
        # (ecritures d'export communes à tous les groupes de liaisons)
        res = self.project.execute(srequete).fetchall()

        if (len(res)>0):
            self.__writ_parametres_generaux(stream_, res, itypehydra, npt, coment)
        return res

    def __writ_one_singularity(self, name, node, i, stream_, streamtmp): # , node_new):
        """ writing node information """
        self.__node_max+=1
        stream_.write("%5d %9d %9d    '%-17s'\n" %(i+1, node, self.__node_max, name))
        streamtmp.write(" %9d %9d\n" %(node, self.__node_max))
        return

    def __writ_tableau(self, rec, stream_, sformat):
        """ writing an array with 2 columns (loi z-s d'un casier, q-z d'une liaison QDZ, ...)
            first the length of this array """
        cleared_rec=[[a,b] for [a,b] in rec if a is not None and b is not None]
        np=len(cleared_rec) # longueur du tableau
        stream_.write("%5d\n" % np)
        for i in range(np):
                stream_.write((sformat+"\n") % (cleared_rec[i][0],cleared_rec[i][1]))
        return

    def __add_array_to_list(self, lst,rec,nmax):
        try:
            lgrec=len(rec)
        except TypeError:
            lgrec=0

        for j in range(lgrec):
            lst.append(rec[j])
        for j in range(nmax-lgrec):
            lst.append(0)
        return
        #@todo à voir: "tu peux concaténer des listes avec l'opérateur +:  en gros ta fonction est équivalente à:  lst += rec + [0]*(nmax-len(rec))

    def __add_transpose_array_to_list(self, lst,r,nmax):
        a=numpy.array(r,numpy.float32)
        b=numpy.transpose(a)
        self.__add_array_to_list(lst,b[0],nmax)
        self.__add_array_to_list(lst,b[1],nmax)
        return

    def __update_node_max(self, res):
        self.__node_max
        n=len(res)
        if (n>0):
            if (res[n-1][0]>self.__node_max):
                self.__node_max=res[n-1][0]
        return

    # ------------------------------------------------------------------------------------
    # export function for kernel version file
    # ------------------------------------------------------------------------------------
    def export_metadata(self, file):
        file.write('{:<43}: {}\n'.format('computation start', datetime.datetime.now().strftime('%H:%M - %d/%m/%Y')))
        file.write('{:<43}: {:>6}\n'.format('plugin HYDRA version', string.plugin_version()))
        file.write('{:<43}: {:>6}\n'.format('project {} version'.format(self.project.name), self.project.version))
        file.write('\n')

    # ------------------------------------------------------------------------------------
    # export functions for settings files (fichiers de paramétrages)
    # ------------------------------------------------------------------------------------
    def exportsettings_gen_fexterne(self, id_param_scen, nomtable, fexterne):
        """ called by exportsettings_gen """
        #  fbloc              fexterne (column)   nomtable (name of table)
        #  --------------     ----------------    -------------------------
        #  *f_hy_externe      external_file       param_external_hydrograph
        #  *f_regul           control_file        param_regulation
        #  *f_mesures         measure_file        param_measure
        res = self.project.execute("""
            select param_scenario, {fil}
            from project.{tab}
            where param_scenario={id}
            order by rank_, regexp_replace({fil}, '^.+[/\\\\]', '')
            """.format(fil=fexterne, tab=nomtable, id=id_param_scen)).fetchall()

        res_unpacked = [self.project.unpack_path(f[1]) for f in res]

        return res_unpacked

    def write_exportsettings_gen_fexterne(self, res_unpacked, fbloc, stream1):

        if (len(res_unpacked) > 0):
            stream1.write("%s\n" % fbloc)
            for rec in res_unpacked:
                self.log.notice("external file", rec)
                stream1.write("%s\n"%rec)
            stream1.write("\n")

    def textwrite_exportsettings_gen_fexterne(self, res_unpacked, fbloc):
        if (len(res_unpacked) > 0):
            text = fbloc+"\n"
            for rec in res_unpacked:
                self.log.notice("external file", rec)
                text += rec+"\n"
        else:
            text=None
        return text

    #@todo à voir: alternative suggérée par VM:
    def __exportsettings_gen_fexterne_bis(self, id_param_scen, nomtable, fexterne, fbloc, stream1):
        count, = self.project.execute("""
            select count(1)
            from project.%s
            where param_scenario=%d
            """ %(nomtable, id_param_scen)).fetchone()

        return '\n'.join([fbloc]+[fexterne]*count)+'\n' if count else None

    def exportsettings_interlink(self, stream):
        self.log.notice("interlinks")
        interlinks = self.project.execute("""select name, model_up, node_up, model_down, node_down from project.interlink""").fetchall()
        for name, model_up, node_up, model_down, node_down in interlinks:
            stream.write("%s %s %s %s %s\n" %(name, model_down, node_down, model_up, node_up))

    def exportsettings_land_type(self, stream):
        self.log.notice("land types")
        land_types = self.project.execute("""select land_type, c_imp, netflow_type, c_runoff, curve, f0_mm_h, fc_mm_h, soil_cap_mm from project.land_type""").fetchall()
        for land_type, c_imp, netflow_type, c_runoff, curve, f0_mm_h, fc_mm_h, soil_cap_mm in land_types:
            netflow_id = self.instances['netflow_type'][netflow_type]['id_hydra_export']
            constant_runoff = c_runoff if netflow_type=='constant_runoff' else -999
            scs_curve = curve if netflow_type=='scs' else -999
            holtan_f0 = f0_mm_h if netflow_type=='holtan' else -999
            holtan_fc = fc_mm_h if netflow_type=='holtan' else -999
            holtan_soil_cap = soil_cap_mm if netflow_type=='holtan' else -999
            stream.write("%s %5d %9.3f %9.3f %9.3f %9.3f %9.3f\n" %(land_type, netflow_id, constant_runoff, scs_curve, holtan_f0, holtan_fc, holtan_soil_cap))

    #2.1 - file scenario.nom
    def __exportsettings_scen(self, id_scenario, name_scenario, name_ref_scenario, name_hydrol_scenario, stream1):

        data_exportsetting_scen = self.project.execute("""
            select comput_mode, output_option, model_connect_settings
            from project.scenario
            where id={}
            """.format(id_scenario)).fetchone()

        groups = self.project.execute("""
            select id, name, ord
            from project.mixed as mixed, project.grp as grp
            where mixed.grp=grp.id
            and mixed.scenario=%i
            order by mixed.ord"""%(id_scenario)).fetchall()

        lstmodel = self.write_exportsetting_scen(data_exportsetting_scen, name_scenario, name_ref_scenario, name_hydrol_scenario, groups, stream1)

        return(lstmodel)

    def write_exportsetting_scen(self, data_exportsetting_scen, name_scenario, name_ref_scenario, name_hydrol_scenario, groups, stream1) :

        comput_mode, output_option, model_connect_settings = data_exportsetting_scen

        stream1.write("*scenario\n")
        stream1.write("%s\n\n" % name_scenario.upper())

        id_comput_option, = self.project.execute("""
            select id from hydra.computation_mode
            where name='{}'
            """.format(comput_mode)).fetchone()

        #--- BLOC SCN_REF
        if name_ref_scenario:
            self.log.notice("option *SCNREF")
            stream1.write("*scnref\n")
            stream1.write("%s\n\n" % name_ref_scenario.upper())

        #--- BLOC HYDROL
        if comput_mode == 'hydraulics' and name_hydrol_scenario:
            self.log.notice("option *HYDROL")
            stream1.write("*hydrol\n")
            stream1.write("%s\n\n" % name_hydrol_scenario.upper())

        #--- BLOC MODELES    ajout  TL 18-11-2017
        self.log.notice("*MODELES")
        stream1.write("*modeles\n")
        lstmodel = self.project.execute("""select name from project.model order by name""").fetchall()
        for model in lstmodel:
            stream1.write("%s    " % model[0].upper())
        stream1.write("\n\n")

        #--- BLOC COMPUTATION_MODE
        self.log.notice("*COMPUTATION MODE")
        stream1.write("*computation_mode\n")
        stream1.write("%5d\n\n" % id_comput_option)

        #--- BLOC GROUPAGE
        self.log.notice("*GROUPAGE")
        stream1.write("*groupage\n")
        lstmodel = [name for name, in self.project.execute("""
            select name
            from project.model_config
            order by cascade_order asc
            """).fetchall()]
        if model_connect_settings=="global":
            self.log.notice("global")
            for model in lstmodel:
                stream1.write("%s    " % model.upper())
            self.log.notice("models: {}".format(', '.join(lstmodel)))
            stream1.write("\n\n")
        elif model_connect_settings=="cascade":
            self.log.notice("cascade")
            for model in lstmodel:
                stream1.write("%s\n" % model.upper())
            self.log.notice("models: {}".format(', '.join(lstmodel)))
            stream1.write("\n")
        elif model_connect_settings=="mixed":
            self.log.notice("mixed")
            lstmodel = []

            for group in groups:
                tmpmodel = [name for name, in self.project.execute("""
                    select name
                    from project.model_config as model
                    where model.id in (
                        select model from project.grp_model
                        where grp = %i)
                    """%(int(group[0]))).fetchall()]
                for model in tmpmodel:
                    lstmodel.append(model)
                    stream1.write("%s    " % model.upper())
                self.log.notice("models: {}".format(', '.join(tmpmodel)))
                stream1.write("\n")
            stream1.write("\n")
        return lstmodel

    #2.2 - file X.cmd
    def __exportsettings_gen(self, id_scenario, stream1):

        # Query the table "parametrage_scenario" and obtain data as Python objects

        data_exportsettings_gen = self.project.execute("""
            select comput_mode, scenario, dry_inflow, rainfall, soil_cr_alp,
            soil_horner_sat, soil_holtan_sat, soil_scs_sat,
            soil_hydra_psat, soil_hydra_rsat, soil_gr4_psat,
            soil_gr4_rsat, option_dim_hydrol_network, date0,
            tfin_hr, tini_hydrau_hr, dtmin_hydrau_hr, dtmax_hydrau_hr,
            dzmin_hydrau, dzmax_hydrau, flag_save, tsave_hr, flag_rstart,
            scenario_rstart, trstart_hr, flag_hydrology_rstart, scenario_hydrology_rstart,
            flag_hydrology_auto_dimensioning, graphic_control, model_connect_settings,
            tbegin_output_hr, tend_output_hr, dt_output_hr,
            output_option, transport_type, quality_type,
            dt_hydrol_mn,
            has_aeraulics
            from project.scenario
            where id={} """.format(id_scenario)).fetchone()

        comput_mode, scenario, dry_inflow, rainfall, soil_cr_alp, \
            soil_horner_sat, soil_holtan_sat, soil_scs_sat, \
            soil_hydra_psat, soil_hydra_rsat, soil_gr4_psat, \
            soil_gr4_rsat, option_dim_hydrol_network, date0, \
            tfin_hr, tini_hydrau_hr, dtmin_hydrau_hr, dtmax_hydrau_hr, \
            dzmin_hydrau, dzmax_hydrau, flag_save, tsave_hr, flag_rstart, \
            scenario_rstart, trstart_hr, flag_hydrology_rstart, scenario_hydrology_rstart, \
            flag_hydrology_auto_dimensioning, graphic_control, model_connect_settings, \
            tbegin_output_hr, tend_output_hr, dt_output_hr, \
            output_option, transport_type, quality_type, \
            dt_hydrol_mn, \
            has_aeraulics = data_exportsettings_gen

        res1, res2, res3, res4, res5 = [], [], [], [], []

        #--- BLOC RSTART
        if (flag_rstart):
            res1 = self.project.execute("""
                select id,name
                from project.scenario
                where id=%d
                """%(scenario_rstart)).fetchall()

        #--- BLOC HYDROL RSTART
        if (flag_hydrology_rstart):
            res2 = self.project.execute("""
                select id,name
                from project.scenario
                where id=%d
                """%(scenario_hydrology_rstart)).fetchall()

        #--- BLOCS F_HY_EXTERNE, F_REGUL, F_MESURE        name of table  , name of column ,  name of bloc  #  4/12/2024/CHV : export transféré dans _optsor.dat selon NT82
        res_ext = []
        # res_ext.append(self.exportsettings_gen_fexterne(id_scenario,"param_external_hydrograph", "external_file"))
        # res_ext.append(self.exportsettings_gen_fexterne(id_scenario,"param_regulation"         , "control_file" ))
        # res_ext.append(self.exportsettings_gen_fexterne(id_scenario,"param_measure"            , "measure_file" ))

        #--- BLOC DEROUTAGE_APPORT - 4/12/2024/CHV : transféré dans _optsor.dat selon NT82

        sediment_file = ""
        #--- BLOC OPTION_POLLUTION
        if (transport_type=="pollution"):
            #                    0  1       2         3         4         5
            res4 = self.project.execute("""
                select id, id, iflag_mes, iflag_dbo5, iflag_dco, iflag_ntk
                from project.scenario
                where id=%d
                """%(id_scenario)).fetchall()

        #--- BLOC OPTION_QUALITY1/2
        elif (transport_type=="quality"):
            #           0  1    2       3          4               5                       6
            res4 = self.project.execute("""
                select id, id, id, diffusion, dispersion_coef, longit_diffusion_coef, lateral_diff_coef
                from project.scenario
                where id=%d
                """%(id_scenario)).fetchall()

            if (len(res4)>0):
                if (quality_type=="physico_chemical"):   # class=1 : fixed parameters: DBO5 NH4 NO3 O2
                    #           0       1                 2          3                4                   5             6            7        8
                    res5 = self.project.execute("""
                        select id, water_temperature_c, min_o2_mgl, kdbo5_j_minus_1, koxygen_j_minus_1, knr_j_minus_1, kn_j_minus_1, bdf_mgl, o2sat_mgl
                        from project.scenario
                        where id=%d
                        """%(id_scenario)).fetchall()

                elif (quality_type=="bacteriological_quality"): # class=2
                    #           0       1         2         3         4             5                           6                            7                      8
                    res5 = self.project.execute("""
                        select id, iflag_ecoli, iflag_enti, iflag_ad1, iflag_ad2, ecoli_decay_rate_jminus_one, enti_decay_rate_jminus_one,ad1_decay_rate_jminus_one,ad2_decay_rate_jminus_one
                        from project.scenario
                        where id=%d
                        """ % (id_scenario)).fetchall()

                elif (quality_type=="suspended_sediment_transport"): # class=3
                    #          0   1
                    res5 = self.project.execute("""
                        select id, suspended_sediment_fall_velocity_mhr
                        from project.scenario
                        where id=%d
                        """ % (id_scenario)).fetchall()

        #--- BLOC OPTION_SEDIMENT
        elif (transport_type=="sediment"):
            sediment_file = self.project.execute("""
                select sediment_file
                from  project.scenario
                where id=%d
                """%(id_scenario)).fetchone()

        # self.write_exportsetting_gen(data_exportsettings_gen, res1, res2, res3, res4, res5, sediment_file, res_ext, aeraulics, stream1)
        self.write_exportsetting_gen(data_exportsettings_gen, res1, res2, res3, res4, res5, sediment_file, res_ext, stream1)

    # def write_exportsetting_gen(self, data_exportsettings_gen, res1, res2, res3, res4, res5, sediment_file, res_ext, aeraulics, stream1) :
    def write_exportsetting_gen(self, data_exportsettings_gen, res1, res2, res3, res4, res5, sediment_file, res_ext, stream1) :

        comput_mode, scenario, dry_inflow, rainfall, soil_cr_alp, \
            soil_horner_sat, soil_holtan_sat, soil_scs_sat, \
            soil_hydra_psat, soil_hydra_rsat, soil_gr4_psat, \
            soil_gr4_rsat, option_dim_hydrol_network, date0, \
            tfin_hr, tini_hydrau_hr, dtmin_hydrau_hr, dtmax_hydrau_hr, \
            dzmin_hydrau, dzmax_hydrau, flag_save, tsave_hr, flag_rstart, \
            scenario_rstart, trstart_hr, flag_hydrology_rstart, scenario_hydrology_rstart, \
            flag_hydrology_auto_dimensioning, graphic_control, model_connect_settings, \
            tbegin_output_hr, tend_output_hr, dt_output_hr, \
            output_option, transport_type, quality_type, \
            dt_hydrol_mn, has_aeraulics = data_exportsettings_gen

        #--- BLOC TIME
        self.log.notice("option *time")
        stream1.write("*time\n")
        stream1.write("%s\n" % date0.strftime("%Y %m %d %H %M"))   # date0_hydrau,
        stream1.write("%12.3f\n" % (tfin_hr.total_seconds()/3600))                           # tfin_hydrau_hours
        # tbegin_ouput_hr,tend_output_hr,dt_output_hr
        tdeb=0.
        if (tbegin_output_hr is not None):
            tdeb=tbegin_output_hr.total_seconds()/3600
        tfin=tfin_hr.total_seconds()/3600
        if (tend_output_hr is not None):
            tfin=tend_output_hr.total_seconds()/3600
        dt=0.
        if (dt_output_hr is not None):
            dt=dt_output_hr.total_seconds()/3600
        stream1.write("%12.3f %12.3f% 12.5f\n\n" %(tdeb,tfin,dt))

        #--- BLOC HYDROL
        if (rainfall is None):
            rainfall=0
        if (dry_inflow is None):
            dry_inflow=0  #TL

        if not comput_mode=="hydraulics":
            self.log.notice("option *hydrol")
            stream1.write("*hydrol\n")
            stream1.write("%5d%5d\n"          % (dry_inflow,rainfall))  #TL
            stream1.write("%12.4f\n"          % (dt_hydrol_mn.total_seconds()/60))
            stream1.write("%12.4f %12.4f %12.4f %12.4f %12.4f %12.4f %12.4f %12.4f\n"   % (soil_cr_alp, soil_horner_sat, \
                            soil_holtan_sat, soil_scs_sat, soil_hydra_psat, soil_hydra_rsat, soil_gr4_psat, soil_gr4_rsat))       # coef_humidite_sol,modulation_coef_ruissel
            i=0
            if (option_dim_hydrol_network):
                i=1
            stream1.write("%5d\n\n"           %  i)                          #  option_dim_reseau_hydrol

        #--- BLOC HYDRAU
        self.log.notice("option *hydrau")
        stream1.write("*hydrau\n")
        stream1.write("%12.3f\n"          % (tini_hydrau_hr.total_seconds()/3600))
        stream1.write("%12.6f %12.6f\n"   % (dtmin_hydrau_hr.total_seconds()/3600,dtmax_hydrau_hr.total_seconds()/3600))
        stream1.write("%12.4f %12.4f\n\n" % (dzmin_hydrau,dzmax_hydrau))

        #--- BLOC SAVE
        if (flag_save):
            self.log.notice("option *save")
            stream1.write("*save\n %5d\n %12.4f\n\n" % (1,tsave_hr.total_seconds()/3600))

        #--- BLOC DESSIN_CALCUL
        if (graphic_control):
            self.log.notice("option *dessin_calcul")
            stream1.write("*dessin_calcul\n %5d\n\n" % 1)              # option_dessin_calcull

        #--- BLOC RSTART
        if (flag_rstart):
            self.log.notice("option *rstart")
            stream1.write("*rstart\n")
            stream1.write("%5d\n" % 1)
            stream1.write("%s\n %12.4f\n\n" % (res1[0][1],trstart_hr.total_seconds()/3600))      # scenario_rstart,trsart_hours

        #--- BLOC HYDROL RSTART
        if (flag_hydrology_rstart):

            self.log.notice("option *hydrol_rstart")
            stream1.write("*hydrol_rstart\n")
            stream1.write("%5d\n" % 1)
            stream1.write("%s\n\n" % (res2[0][1]))      # scenario_rstart,trsart_hours

        #--- BLOC HYDROL AUTO DIMENSIONING
        if (flag_hydrology_auto_dimensioning):

            self.log.notice("option *dim_hydrol")
            stream1.write("*dim_hydrol\n\n")

        #--- BLOCS F_HY_EXTERNE, F_REGUL, F_MESURE  -  4/12/2024/CHV : transféré dans _optsor.dat selon NT82

        #--- BLOC DEROUTAGE_APPORT - 4/12/2024/CHV : transféré dans _optsor.dat selon NT82

        #--- BLOC OPTION_POLLUTION
        if (transport_type=="pollution"):
            #                    0  1       2         3         4         5
            if (len(res4)>0):
                self.log.notice("option *option_pollution")
                stream1.write("*option_pollution\n")
                nparam=0
                for i in range(4):
                    if (res4[0][2+i]):
                        nparam+=1
                stream1.write("%5d\n" % nparam)
                if (res4[0][2]):
                    stream1.write("MES\n")
                if (res4[0][3]):
                    stream1.write("DBO5\n")
                if (res4[0][4]):
                    stream1.write("DCO\n")
                if (res4[0][5]):
                    stream1.write("NTK\n")
                stream1.write("\n")


        #--- BLOC OPTION_QUALITY1/2
        elif (transport_type=="quality"):
            #           0  1    2       3          4               5                       6

            if (len(res4)>0):
                if (quality_type=="physico_chemical"):   # class=1 : fixed parameters: DBO5 NH4 NO3 O2
                    self.log.notice("option *option_quality1")
                    stream1.write("*option_quality1\n")
                    #           0       1                 2          3                4                   5             6            7        8

                    stream1.write("%10.2f %9.3g %9.3g %9.3g %9.3g %9.3g %9.3g %9.3g\n" % (res5[0][1],res5[0][2],res5[0][3],res5[0][4],res5[0][5],res5[0][6],res5[0][7],res5[0][8]))  #
                    stream1.write("%12d %12.3g %12.3g %12.3g\n\n"     % (res4[0][3],res4[0][4],res4[0][5],res4[0][6]))  #  diffusion,coef_dispersion coef_diffusiv_long,coef_diffusiv_lat

                elif (quality_type=="bacteriological_quality"): # class=2
                    name_param=["unused","EC","EI","Ad1","Ad2"]
                    self.log.notice("option *option_quality2")
                    stream1.write("*option_quality2\n")
                    #           0       1         2         3         4             5                           6                            7                      8

                    nparam=0
                    for i in range(4):
                        if (res5[0][i+1]):
                            nparam+=1
                    stream1.write("%5d\n" % nparam)

                    for i in range(4):
                        if (res5[0][i+1]):
                            stream1.write("%-8s  %9.3g\n" %(name_param[i+1],res5[0][5+i]))  #@todo: paramètre "decay_rate" à mettre en place pour chaque paramètre
                    stream1.write("%12d %12.3g %12.3g %12.3g\n\n"     % (res4[0][3],res4[0][4],res4[0][5],res4[0][6]))  #  diffusion,coef_dispersion coef_diffusiv_long,coef_diffusiv_lat

                elif (quality_type=="suspended_sediment_transport"): # class=3
                    self.log.notice("option *option_quality3")
                    stream1.write("*option_quality3\n")
                    #          0   1

                    stream1.write("%9.3f\n" % (res5[0][1]))


        #--- BLOC OPTION_SEDIMENT
        elif (transport_type=="sediment"):

            self.log.notice("option *option_sediment")
            stream1.write("*option_sediment\n%s\n\n" % (self.project.unpack_path(sediment_file[0])))


    #2.3
    def __exportsettings_optsor(self, id_scenario, stream_optsor):

        data_exportsettings_optsor= self.project.execute("""
            select option_file, option_file_path, c_affin_param, t_affin_start_hr, t_affin_min_surf_ddomains,
            output_option, sor1_mode, dt_sor1_hr, strickler_param, domain_2d_param,
            flag_wind_option, air_density, skin_friction_coef, wind_data_file, rainfall,
            (select rainfall_type from project._rainfall where id=s.rainfall),
            kernel_version, has_aeraulics
            from project.scenario as s
            where id={} limit 1;""".format(id_scenario)).fetchone()

        option_file, option_file_path, c_affin_param, t_affin_start_hr, t_affin_min_surf_ddomains, \
            output_option, sor1_mode, dt_sor1_hr, strickler_param, domain_2d_param, flag_wind_option, \
            air_density, skin_friction_coef, wind_data_file, rainfall, rainfall_type, kernel_version, has_aeraulics = data_exportsettings_optsor

        c_affin_list = []
        strickler_list = []
        domain_2d_list= []
        output_list = []
        wind_gage_list = []
        rain_file = None

        if c_affin_param:
            c_affin_list = self.project.execute("""
                select model, type_domain, element, affin_option
                from project.c_affin_param
                where scenario={} order by model, type_domain, element;""".format(id_scenario)).fetchall()

        if strickler_param:
            #strickl
            strickler_list = self.project.execute("""
                select model, element, rkmin, rkmaj, pk1, pk2
                from project.strickler_param
                where scenario={} order by model, element;""".format(id_scenario)).fetchall()

        if domain_2d_param:
            # runoff_2D
            list = self.project.execute("""
                select model, domain_2d
                from project.domain_2d_param
                where scenario={} order by model, domain_2d;""".format(id_scenario)).fetchall()
            for model, domain_id in list:
                res = self.project.execute("""
                    select name
                    from {}.domain_2d
                    where id={};""".format(model, domain_id)).fetchone()
                if res:
                    domain_name = res[0]
                    domain_2d_list.append((model, domain_name))

        if output_option:
            #sor1
            output_list = self.project.execute("""
                select model, element
                from project.output_option
                where scenario={} order by model, element;""".format(id_scenario)).fetchall()

        if flag_wind_option:
            #vent
            wind_gage_list = self.project.execute("""
                select name, ST_X(geom), ST_Y(geom)
                from project.wind_gage
                order by name;""").fetchall()

        if rainfall_type=='gage_file':
            rain_file, = self.project.execute("""
                select file
                from project.gage_file_rainfall
                where id={}
                """.format(rainfall)).fetchone()

        #--- BLOC DEROUTAGE_APPORT - 4/12/2024/CHV : transféré dans _optsor.dat selon NT82
        # Query the table "deroutage_apport"
        #                              0     1          2           3           4      5
        res3 = self.project.execute("""
            select model_from, hydrograph_from, model_to, hydrograph_to, option, parameter
            from project.inflow_rerouting
            where scenario={}
            """.format(id_scenario)).fetchall()

        #--- BLOC F_HY_EXTERNE - 4/12/2024/CHV : transféré dans _optsor.dat selon NT82
        res_ext=[]
        res_ext.append(self.exportsettings_gen_fexterne(id_scenario,"param_external_hydrograph", "external_file"))
        res_ext.append(self.exportsettings_gen_fexterne(id_scenario,"param_regulation"         , "control_file" ))
        res_ext.append(self.exportsettings_gen_fexterne(id_scenario,"param_measure"            , "measure_file" ))

        #--- Bloc AERAULICS - 4/12/2024/CHV : transféré dans _optsor.dat selon NT82
        if has_aeraulics:
            aeraulics = self.project.execute("""
                select speed_of_sound_in_air, absolute_ambiant_air_pressure,
                    average_altitude_msl, air_water_skin_friction_coefficient, air_pipe_friction_coefficient,
                    air_temperature_in_network_celcius, ambiant_air_temperature_celcius, leak_pressure
                from project.scenario
                where id=%d
                """%(id_scenario)).fetchone()
        else:
            aeraulics = None

        dir_scen = self.project.get_senario_path(id_scenario)
        dir_travail = os.path.join(dir_scen, "travail")
        self.write_exportsettings_optsor(data_exportsettings_optsor, c_affin_list, strickler_list, domain_2d_list, output_list, wind_gage_list, rain_file, stream_optsor, dir_travail, res3, res_ext, aeraulics)

    #                                                                                                                                                                     - 4/12/2024/CHV ----------------------
    def write_exportsettings_optsor(self, data_exportsettings_optsor, c_affin_list, strickler_list, domain_2d_list, output_list, wind_gage_list, rain_file, stream_optsor, dir_travail, res3, res_ext, aeraulics):

        option_file, option_file_path, c_affin_param, t_affin_start_hr, t_affin_min_surf_ddomains, \
            output_option, sor1_mode, dt_sor1_hr, strickler_param, domain_2d_param, flag_wind_option, \
            air_density, skin_friction_coef, wind_data_file, rainfall, rainfall_type, kernel_version, has_aeraulics = data_exportsettings_optsor

        #--- 9/12/2024/CHV - Prévalence modifiée : l'export des options définies dans le fichier externe est reporté en fin de procédure.

        if c_affin_param:
            # c affin
            self.log.notice("option *c_affin")
            stream_optsor.write("\n*c_affin\n")
            stream_optsor.write("%12.6f %12.6f\n" % (t_affin_start_hr.total_seconds()/3600, t_affin_min_surf_ddomains))

            for item in c_affin_list:
                stream_optsor.write("%s %s %s %i\n" % (str(item[0]), str(item[1]), str(item[2]), self.instances['affin_option'][item[3]]['id']))
            stream_optsor.write("\n")

        if strickler_param:
            # strickl
            self.log.notice("option *strickl")
            stream_optsor.write("\n*strickl\n")

            for item in strickler_list:
                stream_optsor.write("%s %s %12.2f %12.2f %12.4f %12.4f\n" %(str(item[0]), str(item[1]), item[2], item[3], item[4], item[5]))
            stream_optsor.write("\n")

        if domain_2d_param:
            # runoff_2D
            self.log.notice("option *runoff_2D")
            stream_optsor.write("\n*runoff_2D\n")
            for item in domain_2d_list:
                stream_optsor.write("%s %s \n" %(str(item[0]), str(item[1])))
            stream_optsor.write("\n")

        if output_option:
            # sor1
            self.log.notice("option *sor1")
            stream_optsor.write("\n*sor1\n")
            stream_optsor.write("%i %12.6f \n" %(self.instances['sor1_mode'][sor1_mode]['id'], dt_sor1_hr.total_seconds()/3600))
            for item in output_list:
                stream_optsor.write("%s %s \n" %(str(item[0]), str(item[1])))
            stream_optsor.write("\n")

        if flag_wind_option:
            # vent
            self.log.notice("option *vent")
            stream_optsor.write("\n*vent\n")
            stream_optsor.write("%s \n" % self.project.unpack_path(wind_data_file))
            stream_optsor.write("%i \n" % len(wind_gage_list))
            stream_optsor.write("%9.3f %9.3f \n" % (air_density, skin_friction_coef))
            for item in wind_gage_list:
                stream_optsor.write("%s %9.3f %9.3f \n" % (str(item[0]), item[1], item[2]))
            stream_optsor.write("\n")

        if kernel_version:
            self.log.notice("option *VERSION_KERNEL")
            stream_optsor.write("\n*VERSION_KERNEL\n")
            stream_optsor.write(f"{kernel_version}\n\n")

        #--- BLOC DEROUTAGE_APPORT - 4/12/2024/CHV : transféré dans _optsor.dat selon NT82
        if len(res3)>0:
            self.log.notice("option *deroutage_apport")
            fdatname = os.path.join(dir_travail, 'deroutage.dat')
            stream_optsor.write("*F_DEROUT\n")
            stream_optsor.write(f"{fdatname}\n\n")
            with open(fdatname, 'w') as stream1:
                for rec in res3:
                    name_from = self.project.execute("""select name from {}.hydrograph_bc_singularity where id={}""".format(rec[0], rec[1])).fetchone()[0]
                    if rec[2]!='' and rec[3] is not None:
                        name_to = self.project.execute("""select name from {}.hydrograph_bc_singularity where id={}""".format(rec[2], rec[3])).fetchone()[0]
                    else:
                        name_to = 'null'
                    stream1.write("%20s %20s %20s %20s %i %10.4f\n" % (rec[0],rec[2] if name_to!='null' else 'null',name_from, name_to,self.instances['rerouting_option'][rec[4]]['id'],rec[5]))
                stream1.write("\n")

        #--- 4/12/2024/CHV - BLOC F_HY anciennenemt F_HY_EXTERNE transféré dans _optsor.dat selon NT82
        self.write_exportsettings_gen_fexterne(res_ext[0], "*f_hy"   , stream_optsor) #  -  4/12/2024/CHV : transféré dans _optsor.dat selon NT82
        self.write_exportsettings_gen_fexterne(res_ext[1], "*f_regul", stream_optsor) #  -  4/12/2024/CHV : transféré dans _optsor.dat selon NT82
        self.write_exportsettings_gen_fexterne(res_ext[2], "*f_mesures", stream_optsor) #  -  4/12/2024/CHV : transféré dans _optsor.dat selon NT82

        #--- BLOC AERAULICS - 4/12/2024/CHV : transféré dans _optsor.dat selon NT82
        if has_aeraulics:
            speed_of_sound_in_air, absolute_ambiant_air_pressure, \
                    average_altitude_msl, air_water_skin_friction_coefficient, air_pipe_friction_coefficient, \
                    air_temperature_in_network_celcius, ambiant_air_temperature_celcius, leak_pressure = aeraulics
            air_densitity = 1.2# TODO compute air density
            self.log.notice("option *aeraulics")
            stream_optsor.write(f"*aeraulique\n {air_densitity} "
                f"{air_pipe_friction_coefficient} {air_water_skin_friction_coefficient} "
                f" {air_temperature_in_network_celcius} {ambiant_air_temperature_celcius} "
                f" {speed_of_sound_in_air} {leak_pressure}\n\n")

        #--- 9/12/2024/CHV - Désormais les options présentes en BDD prévalent sur celles du fichier externe en cas de conflit
        if option_file:
            self.log.notice("option *external file")
            file = self.project.unpack_path(option_file_path)
            if os.path.isfile(file):
                stream_optsor.write("\n!---------------------------------------------------\n")
                stream_optsor.write("!  computing options from external settings file \n")
                stream_optsor.write("!---------------------------------------------------\n\n")
                try:
                    with open(file) as f:
                        lines = f.readlines()
                        stream_optsor.writelines(lines)
                        
                except Exception:
                    self.log.error("Error handling file {}".format(file))
                    raise
            else:
                self.log.error("File not found {}".format(file))
                raise ExportError("File not found {}".format(file))

    #2.4
    # 4/12/2024/CHV : modifié pour production bloc *F_QTS_NEW selon NT82, UNIQUEMENT s'il y a des données de temps sec
    # def __exportsettings_dry_inflow(self, id_scenario, qts_stream, cts_stream, sector_stream):
    def __exportsettings_dry_inflow(self, id_scenario, qts_file, cts_file, sector_file, optsor_dat_stream, directory_work):

        id_dry_scenario, =   self.project.execute("""select dry_inflow from project.scenario
           where id={}""".format(id_scenario)).fetchone()

        # self.write_exportsettings_dry_inflow(id_dry_scenario, qts_stream, cts_stream, sector_stream)
        self.write_exportsettings_dry_inflow(id_dry_scenario, qts_file, cts_file, sector_file, optsor_dat_stream, directory_work)

    # def write_exportsettings_dry_inflow(self, id_dry_scenario, qts_stream, cts_stream, sector_stream) :
    def write_exportsettings_dry_inflow(self, id_dry_scenario, qts_file, cts_file, sector_file, optsor_dat_stream, directory_work) :

        if id_dry_scenario is None:
            return

        with open(qts_file, "w") as qts_stream, open(cts_file, "w") as cts_stream, open(sector_file, "w") as sector_stream:
            # File .QTS
            period, = self.project.execute("""select period from project.dry_inflow_scenario where id={}""".format(id_dry_scenario)).fetchone()
            sectors_settings = self.project.execute("""
                select dis.id, dis.name, settings.volume_sewage_m3day, settings.volume_clear_water_m3day,
                settings.coef_volume_sewage_m3day, settings.coef_volume_clear_water_m3day
                from project.dry_inflow_sector as dis
                left join project.dry_inflow_scenario_sector_setting as settings on dis.id = settings.sector
                and settings.dry_inflow_scenario={}
                order by dis.name;
                """.format(id_dry_scenario)).fetchall()

            nsectors = len(sectors_settings)
            qts_stream.write("comment\n")  #TL 20181021
            qts_stream.write("%i %i\n"%(nsectors, self.instances['dry_inflow_period'][period]['id_hydra_export']))
            for settings in sectors_settings:
                self.log.notice(" dry inflow sector", settings[1])
                qts_stream.write("%s %F %F %F %F\n"%(settings[1],
                string.get_sql_forced_float(settings[2]), string.get_sql_forced_float(settings[3]),
                string.get_sql_forced_float(settings[4]), string.get_sql_forced_float(settings[5])))

            # File .CTS
            modulations = self.project.execute("""
                select name, hv_array
                from project.dry_inflow_hourly_modulation
                order by name;
                """).fetchall()

            modulations.insert(0, ['CST', [[0, 0, 0], [24, 1, 1]]])

            for modulation in modulations:
                cts_stream.write("%s\n"%(modulation[0]))
                n = len (modulation[1])
                for j in range(n):
                    cts_stream.write("%F %F %F\n"%(string.get_sql_forced_float(modulation[1][j][0]), string.get_sql_forced_float(modulation[1][j][1]), string.get_sql_forced_float(modulation[1][j][2])))
                cts_stream.write("\n")

            # File secteurs.txt
            sectors = self.project.execute("""
                select dis.name, dis.comment, dihm.name
                from project.dry_inflow_sector as dis
                left join project.dry_inflow_hourly_modulation as dihm on dis.vol_curve = dihm.id
                order by dis.name;
                """).fetchall()
            for sector in sectors:
                name, cmt, curve = sector
                if cmt is None:
                    cmt = ''
                if curve is None:
                    curve = 'CST'
                sector_stream.write("""%s "%s" %s\n"""%(name, cmt, curve))

        # 4/12/2024/CHV - production bloc *F_QTS_NEW selon NT82. Cette écriture est faite ici car elle doit être faite UNIQUEMENT s'il y a des données de temps sec
        optsor_dat_stream.write("*f_qts_new\n")
        for fname in (qts_file, sector_file, cts_file):
            frelname = os.path.relpath(fname, directory_work)
            optsor_dat_stream.write(f"{frelname}\n")
        optsor_dat_stream.write("\n")
        # optsor_dat_stream.write(f"{qts_file}\n")
        # optsor_dat_stream.write(f"{sector_file}\n")
        # optsor_dat_stream.write(f"{cts_file}\n\n")

    #2.5
    def __exportsettings_rainfall_library(self, id_scenario, stream, stream_optsor, plu_file):
        id_rainfall, =  self.project.execute("""select rainfall from project.scenario
            where id={};""".format(id_scenario)).fetchone()

        self.write_exportsetting_rainfall_library(id_rainfall, stream, stream_optsor, plu_file)

    def write_exportsetting_rainfall_library(self, id_rainfall, stream, stream_optsor, plu_file):

        if id_rainfall is None:
            return

        stream_optsor.write("*f_pluvio_new\n") # 4/12/2024/CHV - ajout selon NT82

        type_rainfall, = self.project.execute("""select rainfall_type from project._rainfall
            where id={};""".format(id_rainfall)).fetchone()

        if type_rainfall=="caquot":
            self.log.notice("rainfall - caquot", id_rainfall)
            id_montana, = self.project.execute("""select montana from project.caquot_rainfall
                where id={}""".format(id_rainfall)).fetchone()
            if id_montana is None:
                raise ExportError("Selected caquot rainfall has no montana coefficient")

            a_montana, b_montana = self.project.execute("""select coef_a, coef_b from project.coef_montana
                where id={};""".format(id_montana)).fetchone()

            stream_optsor.write("CAQ\n")  # 4/12/2024/CHV : deplacé dans _optsor.dat selon NT82
            stream.write("%F %F\n"%(string.get_sql_forced_float(a_montana), string.get_sql_forced_float(b_montana)))

        elif type_rainfall=="single_triangular":
            self.log.notice("rainfall - single triangular", id_rainfall)
            id_montana, total_duration_mn, peak_time_mn = self.project.execute("""
                select montana, total_duration_mn, peak_time_mn
                from project.single_triangular_rainfall
                where id={};""".format(id_rainfall)).fetchone()
            if id_montana is None:
                raise ExportError("Selected single triangular rainfall has no montana coefficient")

            a_montana, b_montana = self.project.execute("""select coef_a, coef_b from project.coef_montana
                where id={};""".format(id_montana)).fetchone()

            stream_optsor.write("PPST\n")  # 4/12/2024/CHV : deplacé dans _optsor.dat selon NT82
            stream.write("0 0 10e10\n")   #@todo : capture et export de l'épicentre et du rayon d'action de la pluie
            stream.write("%F %F\n"%(string.get_sql_forced_float(a_montana), string.get_sql_forced_float(b_montana)))
            stream.write("%F %F\n"%(string.get_sql_forced_float(total_duration_mn), string.get_sql_forced_float(peak_time_mn)))

        elif type_rainfall=="double_triangular":
            self.log.notice("rainfall - double triangular", id_rainfall)
            montana_total, montana_peak, total_duration_mn, peak_duration_mn, peak_time_mn = self.project.execute("""
                select montana_total, montana_peak, total_duration_mn, peak_duration_mn, peak_time_mn
                from project.double_triangular_rainfall
                where id={};""".format(id_rainfall)).fetchone()
            if montana_total is None:
                raise ExportError("Selected double triangular rainfall has no montana_total coefficient")
            if montana_peak is None:
                raise ExportError("Selected double triangular rainfall has no montana_peak coefficient")

            a_montana_tot, b_montana_tot = self.project.execute("""select coef_a, coef_b from project.coef_montana
                where id={};""".format(montana_total)).fetchone()
            a_montana_pk, b_montana_pk = self.project.execute("""select coef_a, coef_b from project.coef_montana
                where id={};""".format(montana_peak)).fetchone()

            stream_optsor.write("PPDT\n")  # 4/12/2024/CHV : deplacé dans _optsor.dat selon NT82
            stream.write("0 0 10e10\n")   #@todo : capture et export de l'épicentre et du rayon d'action de la pluie
            stream.write("%F %F\n"%(string.get_sql_forced_float(a_montana_tot), string.get_sql_forced_float(b_montana_tot)))
            stream.write("%F %F\n"%(string.get_sql_forced_float(a_montana_pk), string.get_sql_forced_float(b_montana_pk)))
            stream.write("%F %F %F\n"%(string.get_sql_forced_float(total_duration_mn),
                string.get_sql_forced_float(peak_duration_mn), string.get_sql_forced_float(peak_time_mn)))

        elif type_rainfall=="intensity_curve":
            self.log.notice("rainfall - intensity curve", id_rainfall)
            t_mn_intensity_mmh, = self.project.execute("""
                select t_mn_intensity_mmh from project.intensity_curve_rainfall where id={};
                """.format(id_rainfall)).fetchone()

            # correcting t_mn_intensity_mmh (needs Qvalues of 0 for first and last point)
            if t_mn_intensity_mmh[0][1] != 0:
                t_mn_intensity_mmh.insert(0, [t_mn_intensity_mmh[0][0]-0.01, 0])
            if t_mn_intensity_mmh[-1][1] != 0:
                t_mn_intensity_mmh.append([t_mn_intensity_mmh[-1][0]+0.01, 0])

            stream_optsor.write("PPHY\n")  # 4/12/2024/CHV : deplacé dans _optsor.dat selon NT82
            stream.write("0 0 10e10\n")   #@todo : capture et export de l'épicentre et du rayon d'action de la pluie
            n = len(t_mn_intensity_mmh)
            stream.write("%i\n"%(n))
            for row in range(0, n):
                stream.write("%F %F\n"%(string.get_sql_forced_float(t_mn_intensity_mmh[row][0]),
                    string.get_sql_forced_float(t_mn_intensity_mmh[row][1])))

        elif type_rainfall=="gage":
            self.log.notice("rainfall - gage", id_rainfall)
            interpolation, = self.project.execute("""
                select interpolation from project.gage_rainfall where id={};
                """.format(id_rainfall)).fetchone()

            rain_gages = self.project.execute(f"""select rg.id, st_x(rg.geom) as x, st_y(rg.geom)  as y, coalesce(grd.t_mn_hcum_mm, '{{{{0,0}}}}'::real[][]), rg.name, grd.is_active
                from project.rain_gage as rg
                left join project.gage_rainfall_data as grd on rg.id = grd.rain_gage
                where grd.rainfall={id_rainfall}
                order by rg.name;"""
                ).fetchall()
            n_rg = len(rain_gages)

            stream_optsor.write("PR\n")  # 4/12/2024/CHV : deplacé dans _optsor.dat selon NT82
            stream_optsor.write(f"{plu_file}\n\n")
            stream.write("%i %i\n\n"%(n_rg, self.instances['rainfall_interpolation_type'][interpolation]['id_hydra_export']))
            for i in range(0, n_rg):
                datas = rain_gages[i][3]
                npoints = len(datas)
                stream.write("'%s'\n"%(rain_gages[i][4]))
                stream.write("%F %F %i %i\n"%(float(rain_gages[i][1]), float(rain_gages[i][2]), npoints, int(rain_gages[i][5])))
                for j in range(0, npoints):
                    stream.write("%F %F\n"%(float(datas[j][0]), float(datas[j][1])))
                stream.write("\n")

            if interpolation == 'thiessen':
                stream_optsor.write("*PR_BV\n")
                for model in self.model_to_export:
                    file_name = model+'_thiessen.csv'
                    if os.path.exists(os.path.join(self.project.data_dir, file_name)):
                        # stream.write("%s %s\n"%(model, file_name))
                        stream_optsor.write("%s %s\n"%(model,   os.path.join(self.project.data_dir, file_name)))
                    else:
                        raise ExportError("Selected Thiessen coefficients has not been generated (missing .csv files in /data folder)")
                stream_optsor.write("\n")

        elif type_rainfall=="gage_file":
            self.log.notice("rainfall - gage (file)", id_rainfall)
            interpolation, file = self.project.execute("""
                select interpolation, file from project.gage_file_rainfall where id={};
                """.format(id_rainfall)).fetchone()

            rain_gages = self.project.execute("""select id, ST_X(geom) as x, ST_Y(geom)  as y, name
                from project.rain_gage as rg
                order by rg.name;""").fetchall()
            n_rg = len(rain_gages)

            stream_optsor.write("PR\n")  # 4/12/2024/CHV : deplacé dans _optsor.dat selon NT82
            stream_optsor.write(f"{plu_file}\n")                         # fichier ".plu" des RG avec leurs coordonnees
            stream_optsor.write(f"{self.project.unpack_path(file)}\n\n") # fichier des données de pluie externe
            stream.write("%i %i\n\n"%(n_rg, self.instances['rainfall_interpolation_type'][interpolation]['id_hydra_export']))
            for i in range(0, n_rg):
                stream.write("'%s'\n"%(rain_gages[i][3]))
                stream.write("%F %F\n"%(float(rain_gages[i][1]), float(rain_gages[i][2])))
                stream.write("\n")

            if interpolation == 'thiessen':
                stream_optsor.write("*PR_BV\n")
                for model in self.model_to_export:
                    file_name = model+'_thiessen.csv'
                    if os.path.exists(os.path.join(self.project.data_dir, file_name)):
                        # stream.write("%s %s\n"%(model, file_name))
                        stream_optsor.write("%s %s\n"%(model,   os.path.join(self.project.data_dir, file_name)))
                    else:
                        raise ExportError("Selected Thiessen coefficients has not been generated (missing .csv files in /data folder)")
                stream_optsor.write("\n")

        elif type_rainfall=="radar":
            self.log.notice("rainfall - radar", id_rainfall)
            rain_folder = os.path.join(self.project.directory, 'rain')
            rain = self.project.execute("""
                select id, name from project.radar_rainfall where id={}
                """.format(id_rainfall)).fetchone()

            stream_optsor.write("PRAD\n")  # 4/12/2024/CHV : deplacé dans _optsor.dat selon NT82
            for model in self.model_to_export:
                file_name = rain[1]+'_'+model+'.rad'
                if os.path.exists(os.path.join(rain_folder, file_name)):
                    stream_optsor.write("%s %s\n"%(model, file_name))
                else:
                    raise ExportError("Selected radar rainfall has not been generated (missing .rad files in /rain folder)")
            stream_optsor.write("\n")

        elif type_rainfall=="mages":
            self.log.notice("rainfall - MAGES", id_rainfall)
            id, rain_file = self.project.execute("""
                select id, rain_file from project.mages_rainfall where id={}
                """.format(id_rainfall)).fetchone()

            stream_optsor.write("PRAD2\n")  # 4/12/2024/CHV : deplacé dans _optsor.dat selon NT82
            crb_file = os.path.join(self.project.data_dir, self.project.name+".crb")
            if os.path.exists(crb_file):
                stream_optsor.write("%s\n"%(crb_file))
            else:
                raise ExportError("Selected MAGES rainfall has not been generated (missing .crb files in /data folder)")
            stream_optsor.write("%s\n"%(self.project.unpack_path(rain_file)))
            stream_optsor.write("\n")

        if not type_rainfall in ("radar","mages","gage_file","gage") : stream_optsor.write(f"{plu_file}\n\n") # 4/12/2024/CHV - ajout selon NT82

    #2.7
    def __export_model_connections(self, namemodel, stream1):
        res = self.project.execute("""
            select id, name, node, cascade_mode, zq_array, tz_array, quality
            from %s.model_connect_bc_singularity
            order by id
            """%(namemodel)).fetchall()
        for id, name, node, cascade_mode, zq_array, tz_array, quality in res:
            self.log.notice("model connection", name)
            stream1.write("%s   %s  %d  %d   \n" % (namemodel, name, node, self.instances['model_connect_mode'][cascade_mode]['id']))

            if (cascade_mode=="zq_downstream_condition"):
                array = zq_array
                n  = min(len(array),20)
            elif (cascade_mode=="tz_downstream_condition"):
                array = tz_array
                n  = min(len(array),20)
            else:
                array = [[0, 0], [0, 0]]
                n=0

            for i in range(n):
                stream1.write(" %F " % array[i][0])
            for i in range(20-n):
                stream1.write(" %F " % 0.)  # pad with 0 up to 20
            stream1.write("\n")
            for i in range(n):
                stream1.write(" %F " % array[i][1])
            for i in range(20-n):
                stream1.write(" %F " % 0.)  # pad with 0 up to 20
            stream1.write("\n")
            for q in quality:
                stream1.write(" %F " % q)
            stream1.write("\n")

    def exportsettings_pollution_land_accumulation(self, stream):
        sds_stock, cds_stock, recovery_rate, a_coef, b_coef = self.project.execute("""
            select sewage_storage_kgha, unitary_storage_kgha, regeneration_time_days, a_extraction_coef, b_extraction_coef
            from project.pollution_land_accumulation
            where id=1;
            """).fetchone()

        self.log.notice("Exporting settings: pollution land accumulation")
        for i in range(4):
            for j in range(4):
                stream.write("%9.3f "%sds_stock[i][j])
            stream.write("\n")
        stream.write("\n")
        for i in range(4):
            for j in range(4):
                stream.write("%9.3f "%cds_stock[i][j])
            stream.write("\n")
        stream.write("\n")
        stream.write("%9.3f %9.3f\n"%(a_coef, b_coef))
        stream.write("%9.3f\n"%recovery_rate)
        return

    # ------------------------------------------------------------------------------------
    # export functions for hydrologic comptuations (files .LI1  .LI2)
    #   file .LI1 : ascii. each line includes: i (rang de l'objet) nom itype_hydrol node_amont node_aval.
    #   file .LI2 : binary. each record is 1024 bytes long and includes up to 256 parameters (integer or real, size=4 bytes),
    #                depending of the type of object (bassin_versant, hydrology_routage, etc ...).
    #               if there are less than 256 parameters, the record must be completed with zeros.
    # ------------------------------------------------------------------------------------

    #3.2.1
    def __exporthydrol_manhole(self, nommodel, stream1, stream2):
        itypehydra=self.instances["node_type"]["manhole_hydrology"]["group_hydra_no"]
        lgrecord=self.__record_length["LI2"]
        #n4byte=lgrecord/4 # number of 4-bytes words in LI2-record
        # Query the database and obtain data as Python objects
        #                    0  1     2    3
        res = self.project.execute("""
            select id, name, area, z_ground
            from %s.manhole_hydrology_node
            order by id
            """%(nommodel)).fetchall()

        self.__update_node_max(res)
        # format for ascii file LI1
        sformat1="%10d   %-17s %4d %9d %9d\n"
        #srecord=ctypes.create_string_buffer(lgrecord)

        for i, rec in enumerate(res):
            self.log.notice("hydrology manhole", rec[1])
            self.__icount_hydrol += 1
            # writing in LI1 ascii file
            stream1.write(sformat1 % (self.__icount_hydrol,rec[1],itypehydra,rec[0],0))

            # writing in LI2 binary file
            start = stream2.tell()
            stream2.write(struct.pack('ff', rec[2], rec[3] ))    # area, z_ground
            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros
        return

    #3.2.2
    def __exporthydrol_catchment(self, nommodel, stream1, stream2):
        itype_hydrol = self.instances["node_type"]["catchment"]["group_hydra_no"]
        lgrecord = self.__record_length["LI2"]
        #n4byte=lgrecord/4 # number of 4-bytes words in LI2-record

        # Query the database and obtain data as Python objects
        res = self.project.execute("""
            select n.id, n.name, area_ha, rl, slope, c_imp, netflow_type,
            constant_runoff, horner_ini_loss_coef, horner_recharge_coef,
            holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm,
            scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm,
            hydra_surface_soil_storage_rfu_mm, hydra_inf_rate_f0_mm_hr, hydra_int_soil_storage_j_mm, hydra_soil_drainage_time_qres_day,
            hydra_split_coefficient, hydra_catchment_connect_coef, hydra_aquifer_infiltration_rate,
            hydra_soil_infiltration_type,
            gr4_k1, gr4_k2, gr4_k3, gr4_k4, runoff_type, _tc_mn, socose_shape_param_beta, _k_mn, q_limit, q0,
            network_type, rural_land_use, industrial_land_use,
            suburban_housing_land_use, dense_housing_land_use, ST_X(n.geom), ST_Y(n.geom), c.name
            from {m}.catchment_node as n left join {m}.catchment as c on n.contour = c.id
            order by n.id
            """.format(m=nommodel)).fetchall()
        self.__update_node_max(res)

        for i, (id_, name, area_ha, rl, slope, c_imp,
                netflow_type, constant_runoff, horner_ini_loss_coef, horner_recharge_coef,
                holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm,
                scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm,
                hydra_surface_soil_storage_rfu_mm, hydra_inf_rate_f0_mm_hr, hydra_int_soil_storage_j_mm, hydra_soil_drainage_time_qres_day,
                hydra_split_coefficient, hydra_catchment_connect_coef, hydra_aquifer_infiltration_rate,
                hydra_soil_infiltration_type,
                gr4_k1, gr4_k2, gr4_k3, gr4_k4, runoff_type, _tc_mn, socose_shape_param_beta, _k_mn, q_limit, q0,
                network_type, rural_land_use, industrial_land_use,
                suburban_housing_land_use, dense_housing_land_use, xcord, ycord, cbv) in enumerate(res):
            self.log.notice("hydrology catchment", name)
            self.__icount_hydrol += 1
            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n"%(self.__icount_hydrol, name, itype_hydrol, id_, 0))

            # Netflow
            netflow_id = self.instances['netflow_type'][netflow_type]['id_hydra_export']
            if netflow_type=='constant_runoff':
                netflow_params=[constant_runoff]
            elif netflow_type=='horner':
                netflow_params=[horner_ini_loss_coef, horner_recharge_coef]
            elif netflow_type=='holtan':
                netflow_params=[holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm]
            elif netflow_type=='scs':
                netflow_params=[scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm]
            elif netflow_type=='hydra':
                netflow_params=[
                    hydra_surface_soil_storage_rfu_mm,
                    1 if hydra_soil_infiltration_type == 'split' else 2,
                    hydra_inf_rate_f0_mm_hr or 0,
                    hydra_int_soil_storage_j_mm or 0,
                    hydra_split_coefficient or 0,
                    hydra_soil_drainage_time_qres_day,
                    1.,
                    hydra_catchment_connect_coef or 0,
                    hydra_aquifer_infiltration_rate or 0]
            elif netflow_type=='gr4':
                netflow_params=[gr4_k1, gr4_k2, gr4_k3, gr4_k4]
            else:
                assert False

            netflow_params = netflow_params + [0]*(10*len(netflow_params))

            # Runoff
            if runoff_type in ['Define Tc', 'Passini', 'Giandotti', 'Turraza', 'Ventura', 'Kirpich', 'Cemagref', 'Mockus']:
                runoff_id = 1
                runoff_params=[_tc_mn, socose_shape_param_beta, self.instances['runoff_type'][runoff_type]['id']]
            elif runoff_type in ['Define K', 'Desbordes 1 Cr', 'Desbordes 1 Cimp', 'Desbordes 2 Cimp', 'Krajewski']:
                runoff_id = 2
                runoff_params=[_k_mn, 0, self.instances['runoff_type'][runoff_type]['id']]
            elif netflow_type=='gr4' and runoff_type is None:
                pass # all goog
            else:
                assert False

            # Quality parameters
            pp=[]
            for param in [rural_land_use, industrial_land_use, suburban_housing_land_use, dense_housing_land_use]:
                pp += [param] if param is not None else [0]
            pp += [0]*4

            cbv = '' if cbv is None else cbv

            start = stream2.tell()
            # writing in LI2 binary file
            stream2.write(struct.pack('ffffiffffffffffifffffiiffffffffff24s',
                area_ha, rl, slope, c_imp,
                netflow_id, netflow_params[0], netflow_params[1], netflow_params[2], netflow_params[3], netflow_params[4], netflow_params[5], netflow_params[6], netflow_params[7], netflow_params[8], netflow_params[9],
                runoff_id, runoff_params[0], runoff_params[1], runoff_params[2], q_limit, q0, 2,
                self.instances['network_type'][network_type]['id'],
                pp[0], pp[1], pp[2], pp[3], pp[4], pp[5], pp[6], pp[7],
                xcord, ycord, cbv.encode('utf-8')
                ))

            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros
        return

    #3.2.3
    def __exporthydrol_routage(self, nommodel, stream1, stream2):
        itypehydra=self.instances["link_type"]["routing_hydrology"]["group_hydra_no"]
        lgrecord=self.__record_length["LI2"]
        #n4byte=lgrecord/4

        # Query the database and obtain data as Python objects
        res = self.project.execute("""
            select id, name, up, down, cross_section, length, slope, split_coef/(select SUM(split_coef) from {m}.routing_hydrology_link where up=l.up)
            from {m}.routing_hydrology_link as l
            order by id
            """.format(m=nommodel)).fetchall()

        for i, (id, name, up, down, cross_section, length, slope, split_weight) in enumerate(res):
            self.log.notice("hydrology routing", name)
            self.__icount_hydrol += 1
            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,name,itypehydra,up,down))

            # writing in LI2 binary file
            start = stream2.tell()
            stream2.write(struct.pack('ffff', cross_section, length, slope, split_weight))
            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros
        return

    #3.2.4
    def __exporthydrol_translation(self, nommodel, stream1, stream2):
        itypehydra=self.instances["link_type"]["connector_hydrology"]["group_hydra_no"]
        lgrecord=self.__record_length["LI2"]
        #n4byte=lgrecord/4
        # Query the database and obtain data as Python objects
        #                   0   1    2  3   4
        res = self.project.execute("""
            select id, name, id, up, down
            from %s.connector_hydrology_link
            order by id
            """%(nommodel)).fetchall()

        for i, rec in enumerate(res):
            self.log.notice("hydrology connector", rec[1])
            self.__icount_hydrol += 1
            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,rec[1],itypehydra,rec[3],rec[4]))

            # writing in LI2 binary file
            stream2.write(bytearray(lgrecord)) # pad with zeros
        return

    #3.2.5
    def __exporthydrol_qqsplit(self, nommodel, stream1, stream2):
        itypehydra=self.instances["singularity_type"]["qq_split_hydrology"]["group_hydra_no"] #120
        lgrecord=self.__record_length["LI2"]
        res = self.project.execute("""
            select id, name, node, qq_array, downstream, split1, split2
            from %s.qq_split_hydrology_singularity
            order by id
            """%(nommodel)).fetchall()

        for i, rec in enumerate(res):
            id,name,node,qq_array,downstream,split1,split2 = rec
            self.log.notice("hydrology qq-split", name)
            self.__icount_hydrol += 1

            downstream = self.project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,downstream)).fetchone()[0]
            if (split1 is not None):
                split1 = self.project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,split1)).fetchone()[0]
            if (split2 is not None and split2!=0):
                split2 = self.project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,split2)).fetchone()[0]

            if (split2 is None):
                split2=0

            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,name,itypehydra,node,0))

            # writing in LI2 binary file
            lg=len(qq_array)
            p = []
            for q in qq_array:
                p+= [q[0]]
            p += [0.]*(10-lg) # pad with zeros
            for q in qq_array:
                p+= [q[1]]
            p += [0.]*(10-lg) # pad with zeros
            if (len(qq_array[0]))==3:
                for q in qq_array:
                    p+= [q[2]]
                p += [0.]*(10-lg) # pad with zeros

            start = stream2.tell()
            stream2.write(struct.pack('iii', downstream, split1, split2))
            for p1 in p:
                stream2.write(struct.pack('f', p1))
            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros

        return

    def __convert_to_array(self, mystring):
        return [[float(v) for v in level1.split(',')] for level1 in mystring.replace(' ', '').replace('{{', '').replace('}}', '').split('},{') ]



    #3.2.6
    def __exporthydrol_zqsplit(self, nommodel, stream1, stream2):
        #itypehydra=21
        itypehydra=self.instances["singularity_type"]["zq_split_hydrology"]["group_hydra_no"]

        lgrecord=self.__record_length["LI2"]
        res = self.project.execute("""
            select id, name, node, downstream, split1, split2, downstream_law, downstream_param_json->downstream_law::varchar, split1_law, split1_param_json->split1_law::varchar, split2_law, split2_param_json->split2_law::varchar
            from %s.zq_split_hydrology_singularity
            order by id
            """%(nommodel)).fetchall()

        for i, (id_, name, node, downstream, split1, split2,  downstream_law,
                downstream_param, split1_law, split1_param, split2_law, split2_param
                ) in enumerate(res):
            self.log.notice("hydrology zq-split", name)
            self.__icount_hydrol += 1

            downstream = self.project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,downstream)).fetchone()[0]
            if (split1 is not None):
                split1 = self.project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,split1)).fetchone()[0]
            if (split2 is not None):
                split2 = self.project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,split2)).fetchone()[0]
            else :
                split2_law = None

            if (split2 is None):
                split2=0

            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,name,itypehydra,node,0))

            # writing in LI2 binary file
            total_p = []
            for (law, param) in [(downstream_law, downstream_param), (split1_law, split1_param), (split2_law, split2_param)]:
                p=[]
                if (law=="zq"):
                    for v in param["zq_array"]:
                        p += [v[0]]
                    p += [0.]*(10-len(p)) # pad with zeros (for 10 first values)
                    for v in param["zq_array"]:
                        p += [v[1]]
                elif law is not None:
                    p += [float(param[n]) for n in sorted(self.instances['split_law_type'][law]['params'].keys(), key=lambda k: self.instances['split_law_type'][law]['params'][k]['place'])]
                p += [0.]*(20-len(p)) # pad with zeros (20 values total)

                total_p += p # append into final array (length 60: 3 x 20)

            start = stream2.tell()
            stream2.write(struct.pack('iiiiii', downstream, split1, split2 or 0, self.instances['split_law_type'][downstream_law]["id"], self.instances['split_law_type'][split1_law]["id"], self.instances['split_law_type'][split2_law]["id"] if split2_law else 0  ))
            for n in range(60):
                stream2.write(struct.pack('f', total_p[n]))
            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros

        return

    #3.2.7
    def __exporthydrol_rs(self, nommodel, stream1, stream2):
        #itypehydra=22
        itypehydra=self.instances["singularity_type"]["reservoir_rs_hydrology"]["group_hydra_no"]

        lgrecord=self.__record_length["LI2"]
        res = self.project.execute("""
            select id, name, node, drainage, overflow, q_drainage, z_ini, zs_array, treatment_mode, treatment_param_json->treatment_mode::varchar
            from %s.reservoir_rs_hydrology_singularity
            order by id
            """%(nommodel)).fetchall()

        for i, rec in enumerate(res):
            id, name, node, drainage, overflow, q_drainage, z_ini, zs_array, treatment_mode, treatment_param = rec
            self.log.notice("hydrology rs-reservoir", name)
            self.__icount_hydrol += 1

            drainage = self.project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,drainage)).fetchone()[0]
            overflow = self.project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,overflow)).fetchone()[0]

            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,name,itypehydra,node,0))

            # writing in LI2 binary file
            start = stream2.tell()
            stream2.write(struct.pack('iiff', drainage, overflow, q_drainage, z_ini ))

            p = []
            for zs in zs_array:
                p+= [zs[0]]
            p += [0.]*(10-len(zs_array)) # pad with zeros
            for zs in zs_array:
                p+= [zs[1]]
            p += [0.]*(10-len(zs_array)) # pad with zeros
            for p1 in p:
                stream2.write(struct.pack('f', p1))

            p = [float(treatment_param[n]) for n in sorted(self.instances['pollution_treatment_mode'][treatment_mode]['params'].keys(), key=lambda k: self.instances['pollution_treatment_mode'][treatment_mode]['params'][k]['place'])]
            stream2.write(struct.pack('i', self.instances['pollution_treatment_mode'][treatment_mode]["id"]  ))
            for p1 in p:
                stream2.write(struct.pack('f', p1))

            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros

        return


    #3.2.8
    def __exporthydrol_rsp(self, nommodel, stream1, stream2):
        #itypehydra=23
        itypehydra=self.instances["singularity_type"]["reservoir_rsp_hydrology"]["group_hydra_no"]

        lgrecord=self.__record_length["LI2"]
        res = self.project.execute("""
            select id, name, node, drainage, overflow, z_ini, zr_sr_qf_qs_array, treatment_mode, treatment_param_json->treatment_mode::varchar
            from %s.reservoir_rsp_hydrology_singularity
            order by id
            """%(nommodel)).fetchall()

        for i, rec in enumerate(res):
            id, name, node, drainage, overflow, z_ini, zr_sr_qf_qs_array, treatment_mode, treatment_param = rec
            self.log.notice("hydrology rsp-reservoir", name)
            self.__icount_hydrol += 1

            drainage = self.project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,drainage)).fetchone()[0]
            overflow = self.project.execute("""select down  from  %s._link    where   id=%d """ % (nommodel,overflow)).fetchone()[0]

            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,name,itypehydra,node,0))

            # writing in LI2 binary file
            start = stream2.tell()
            stream2.write(struct.pack('iif', drainage, overflow, z_ini ))

            p = []
            for j in range(4):
                for zs in zr_sr_qf_qs_array:
                    p+= [zs[j]]
                p += [0.]*(10-len(zr_sr_qf_qs_array)) # pad with zeros
            for p1 in p:
                stream2.write(struct.pack('f', p1))

            p = [float(treatment_param[n]) for n in sorted(self.instances['pollution_treatment_mode'][treatment_mode]['params'].keys(), key=lambda k: self.instances['pollution_treatment_mode'][treatment_mode]['params'][k]['place'])]
            stream2.write(struct.pack('i', self.instances['pollution_treatment_mode'][treatment_mode]["id"]  ))
            for p1 in p:
                stream2.write(struct.pack('f', p1))

            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros
        return


    #3.2.9
    def __exporthydrol_bcsingularity(self, nommodel, stream1, stream2):
        itypehydra=self.instances["singularity_type"]["hydrology_bc"]["group_hydra_no"]
        lgrecord=self.__record_length["LI2"]
        #n4byte=lgrecord/4
        # Query the database and obtain data as Python objects
        res = self.project.execute("""
             select id, name, node
             from %s.hydrology_bc_singularity
             order by id
             """%(nommodel)).fetchall()

        for i, rec in enumerate(res):
            self.log.notice("hydrology BC", rec[1])
            self.__icount_hydrol += 1

            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,rec[1],itypehydra,rec[2],0))

            # writing in LI2 binary file
            stream2.write(bytearray(lgrecord)) # pad with zeros
        return

    #3.2.10
    def __exporthydrol_pipe(self, nommodel, stream1, stream2):
        itypehydra=self.instances["link_type"]["pipe"]["group_hydra_no"]
        lgrecord=self.__record_length["LI2"]
        #n4byte=lgrecord/4
        # Query the database and obtain data as Python objects
        #             0      1    2        3             4                5        6                          7             8              9               10             11                  12               13                      14                     15          16
        res = self.project.execute("""
            select l.id, l.name, l.up, l.down, l.z_invert_up, l.z_invert_down, -- 0-5
            hydra.cross_section_type.id, l.h_sable, l.custom_length, ST_Length(l.geom), l.rk, -- 6-10
            l.cross_section_type,
            l.circular_diameter,
            l.ovoid_height, l.ovoid_top_diameter, l.ovoid_invert_diameter, -- 13-15
            l.cp_geom,
            l.op_geom, -- 17
            g.with_flood_plain,
            g.flood_plain_width,
            g.flood_plain_lateral_slope,
            l.rk_maj
            from {model}.pipe_link as l
            join hydra.cross_section_type on hydra.cross_section_type.name = l.cross_section_type
            left join {model}.open_parametric_geometry g on g.id=l.op_geom
            where branch is null and l.up_type='manhole_hydrology' and l.down_type='manhole_hydrology'
            order by l.id
            """.format(model=nommodel)).fetchall()

        for i, rec in enumerate(res):
            self.log.notice("hydrology pipe", rec[1])
            self.__icount_hydrol += 1
            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,rec[1],itypehydra,rec[2],rec[3]))

            # writing in LI2 binary file    corr TL 03/03/2017
            start = stream2.tell()
            length = rec[8] if rec[8] is not None else rec[9]
            if rec[11]=='circular':
                stream2.write(struct.pack('fffifff', rec[4],rec[5],length,rec[6],rec[10],rec[7],rec[12]))
            elif rec[11]=='ovoid':
                stream2.write(struct.pack('fffifffff', rec[4],rec[5],length,rec[6],rec[10],rec[7],rec[13],rec[14],rec[15]))
            elif rec[11]=='pipe':
                stream2.write(struct.pack('fffiffi', rec[4],rec[5],length,rec[6],rec[10],rec[7],rec[16]))
            elif rec[11]=='channel':
                stream2.write(struct.pack('fffiffiifff', rec[4],rec[5],length,rec[6],rec[10],rec[7],rec[17], rec[18], rec[19], rec[20], rec[21]))
            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros

        return

    #3.2.11
    def __exporthydrol_hydrograph_bc(self, nommodel, stream1, stream2):
        itypehydra=self.instances["singularity_type"]["hydrograph_bc"]["group_hydra_no"]
        lgrecord=self.__record_length["LI2"]
        # Query the database and obtain data as Python objects
        res = self.project.execute("""
            select s.id, s.name, s.node, s.constant_dry_flow, s.tq_array, s.sector, s.hourly_modulation, s.distrib_coef, s.lag_time_hr, s.pollution_dryweather_runoff, s.quality_dryweather_runoff, external_file_data
            from %s.hydrograph_bc_singularity as s
            order by s.id
            """%(nommodel)).fetchall()

        # get sum of total distrib coefs by sectors
        sectors_total_contributions={}
        sectors = [sect for sect, in self.project.execute("""select id from project.dry_inflow_sector""").fetchall()]
        for s in sectors:
            total=0.0
            for model in self.project.get_models():
                sum = self.project.execute("""select sum(hy.distrib_coef)
                                            from {}.hydrograph_bc_singularity as hy
                                            where hy.sector={} and hy.distrib_coef>0""".format(model, s)).fetchone()
                if sum is not None and sum[0] is not None:
                    total += sum[0]
            if total==0:
                sectors_total_contributions[s]=1
            else:
                sectors_total_contributions[s]=total

        for i, rec in enumerate(res):
            self.log.notice("hydrograph BC", rec[1])
            id,name,node,constant_dry_flow,tq_array,sector,hourly_modulation,distrib_coef,lag_time_hr, param_pollution, param_quality, external_file_data = rec
            self.__icount_hydrol += 1

            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,name,itypehydra,node,0))

            # writing in LI2 binary file
            if (external_file_data):
                imode_external=1
            else:
                imode_external=0

            if (tq_array is None):
                ltq=0
            else:
                ltq=len(tq_array)

            if (sector is None):
                imode_qts=0
                name_sector=" "*24   # 24 spaces
                contribution = 0
            else:
                imode_qts=1
                name_sector = self.project.execute("""select  name from project.dry_inflow_sector where id=%d """ % (sector)).fetchone()[0]
                name_sector+=" "*(24-len(name_sector)) # in order to make it padded with spaces, not null char
                contribution = distrib_coef/sectors_total_contributions[sector] if distrib_coef else 0

            start = stream2.tell()
            stream2.write(struct.pack('ifii', imode_external, constant_dry_flow, ltq, imode_qts))
            stream2.write(name_sector.encode('utf8'))
            stream2.write(struct.pack('ff', contribution, lag_time_hr))

            for i in range(ltq):
                stream2.write(struct.pack('f', tq_array[i][0] ))
            for i in range(20-ltq):
                stream2.write(struct.pack('f', 0. ))
            for i in range(ltq):
                stream2.write(struct.pack('f', tq_array[i][1] ))
            for i in range(20-ltq):
                stream2.write(struct.pack('f', 0. ))

            p = []
            p += param_pollution[0] if param_pollution is not None else [0,0,0,0]
            p += param_pollution[1] if param_pollution is not None else [0,0,0,0]
            p += param_quality[0] if param_quality is not None else [0,0,0,0,0,0,0,0,0]
            p += param_quality[1] if param_quality is not None else [0,0,0,0,0,0,0,0,0]
            # total length 26

            for pp in p:
                stream2.write(struct.pack('f', pp))

            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros
        return

    #3.2.12
    def __exporthydrol_tz_bc(self, nommodel, stream1, stream2):
        itypehydra=self.instances["singularity_type"]["tz_bc"]["group_hydra_no"]
        lgrecord=self.__record_length["LI2"]
        # Query the database and obtain data as Python objects
        res = self.project.execute("""
            select id, name, node, tz_array, cyclic, external_file_data
            from %s.tz_bc_singularity as s
            order by id
            """%(nommodel)).fetchall()

        for i, rec in enumerate(res):
            self.log.notice("tz BC", rec[1])
            id, name, node, tz_array, cyclic, external_file_data = rec
            self.__icount_hydrol += 1

            if (external_file_data):
                imode_external=1
            else:
                imode_external=0

            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,name,itypehydra,node,0))

            # writing in LI2 binary file
            if (tz_array is None):
                ltz=0
            else:
                ltz=len(tz_array)
            start = stream2.tell()
            stream2.write(struct.pack('ii', imode_external, ltz ))
            for i in range(ltz):
                stream2.write(struct.pack('f', tz_array[i][0] ))
            for i in range(20-ltz):
                stream2.write(struct.pack('f', 0. ))
            for i in range(ltz):
                stream2.write(struct.pack('f', tz_array[i][1] ))
            for i in range(20-ltz):
                stream2.write(struct.pack('f', 0. ))
            #for i in range(ltz):
            #    stream2.write(struct.pack('ff', tz_array[i][0], tz_array[i][1] ))
            stream2.write(bytearray(lgrecord - stream2.tell() + start)) # pad with zeros
        return

    #3.2.13   #TL 20180318
    def __exporthydrol_marker(self, nommodel, stream1, stream2):
        itypehydra=self.instances["singularity_type"]["marker"]["group_hydra_no"]
        self.__record_length["LI2"]
        # Query the database and obtain data as Python objects
        res = self.project.execute("""
            select id, name,node
            from %s.marker_singularity as s
            order by id
            """%(nommodel)).fetchall()
        for i, rec in enumerate(res):
            self.log.notice("marker", rec[1])
            id, name,node = rec
            self.__icount_hydrol += 1

            # writing in LI1 ascii file
            stream1.write("%10d   %-17s %4d %9d %9d\n" % (self.__icount_hydrol,name,itypehydra,node,0))
        return


    # ------------------------------------------------------------------------------------
    # export functions for tables sections
    # ------------------------------------------------------------------------------------
    def __add_sgeom_to_list(self, lst,r,n,z0,z_invert):
        z=numpy.zeros(n,dtype=numpy.float32)
        b=numpy.zeros(n,dtype=numpy.float32)
        for i in range(n):
            if (i<len(r)):
                zi=r[i][0]
            else:
                zi=None

            if (zi is None):
                z[i]=z[i-1]
                b[i]=b[i-1]
            elif (z_invert is None):
                z[i]=r[i][0]
                b[i]=r[i][1]
            else:
                z[i]=r[i][0]-z0+z_invert
                b[i]=r[i][1]
        self.__add_array_to_list(lst,z,n)
        self.__add_array_to_list(lst,b,n)
        return

    #3.3
    def __export_section(self, nommodel, streamfsec):
        #lgrecord=self.__record_length["SECTION.DAT"]
        #n4byte=lgrecord/4 # number of 4-bytes words in LI2-record

        # pipe

        itype=self.instances["cross_section_type"]["pipe"]["group_hydra_no"]
        #srecord=ctypes.create_string_buffer(lgrecord)
        #      sc = pipe or channel      sg = closed or open                     sgeom = cp_geom or op_geom
        #      0     1
        res = self.project.execute(f"""
        select id, name, zbmin_array
        from {nommodel}.closed_parametric_geometry
        where id in (select cp_geom from {nommodel}.pipe_link where cross_section_type='pipe')
        union
        select id, name, zbmin_array
        from {nommodel}.closed_parametric_geometry
        where id in (select up_cp_geom from {nommodel}.river_cross_section_profile where type_cross_section_up='pipe')
        union
        select id, name, zbmin_array
        from {nommodel}.closed_parametric_geometry
        where id in (select down_cp_geom from {nommodel}.river_cross_section_profile where type_cross_section_down='pipe')
        order by id;
        """).fetchall()

        for rec in res:
            self.log.notice("closed section", rec[1])
            streamfsec.write("%4d %4d  %24s\n" % (rec[0], itype, rec[1]))
            self.__writ_tableau(rec[2], streamfsec, "%10.3f %9.3f")
            if (len(rec)>0):
                streamfsec.write("\n")

        itype=self.instances["cross_section_type"]["channel"]["group_hydra_no"]
        #srecord=ctypes.create_string_buffer(lgrecord)
        #      sc = pipe or channel      sg = closed or open                     sgeom = cp_geom or op_geom
        #      0     1
        res = self.project.execute(f"""
        select id, name, zbmin_array, with_flood_plain, flood_plain_width, flood_plain_lateral_slope
        from {nommodel}.open_parametric_geometry
        where id in (select op_geom from {nommodel}.pipe_link where cross_section_type='channel')
        union
        select id, name, zbmin_array, with_flood_plain, flood_plain_width, flood_plain_lateral_slope
        from {nommodel}.open_parametric_geometry
        where id in (select up_op_geom from {nommodel}.river_cross_section_profile where type_cross_section_up='channel')
        union
        select id, name, zbmin_array, with_flood_plain, flood_plain_width, flood_plain_lateral_slope
        from {nommodel}.open_parametric_geometry
        where id in (select down_op_geom from {nommodel}.river_cross_section_profile where type_cross_section_down='channel')
        order by id;
        """).fetchall()

        for rec in res:
            self.log.notice("open section", rec[1])
            streamfsec.write("%4d %4d  %24s\n" % (rec[0], itype, rec[1]))
            self.__writ_tableau(rec[2], streamfsec, "%10.3f %9.3f")
            streamfsec.write("%s %9.3f %9.3f\n" % ('.true.' if rec[3] else '.false.', rec[4], rec[5]))
            if (len(rec)>0):
                streamfsec.write("\n")


        #
        #--- Section VL ---
        #
        itype=self.instances["cross_section_type"]["valley"]["group_hydra_no"]
        #          0    1     2             3                 4                  5      6      7       8          9
        res = self.project.execute("""
            select id, name, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, rlambda, rmu1, rmu2, zlevee_lb, zlevee_rb
            from {model}.valley_cross_section_geometry
            where id in (select up_vcs_geom from {model}.river_cross_section_profile where type_cross_section_up='valley')
            union
            select id, name, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, rlambda, rmu1, rmu2, zlevee_lb, zlevee_rb
            from {model}.valley_cross_section_geometry
            where id in (select down_vcs_geom from {model}.river_cross_section_profile where type_cross_section_down='valley')
            order by id;
            """.format(model=nommodel)).fetchall()

        for rec in res:
            self.log.notice("valley section", rec[0])
            streamfsec.write("%4d %4d %24s \n" % (rec[0],itype,rec[1]))
            streamfsec.write("%9.3f %9.3f %9.3f %9.3f %9.3f\n" % (rec[5], rec[6], rec[7], rec[8], rec[9]))
            self.__writ_tableau(rec[2], streamfsec, "%10.3f %9.3f")
            self.__writ_tableau(rec[3], streamfsec, "%10.3f %9.3f")
            self.__writ_tableau(rec[4], streamfsec, "%10.3f %9.3f")
            if (len(rec)>0):
                streamfsec.write("\n")

        streamfsec.close()
        return

    # ------------------------------------------------------------------------------------
    # export functions for tables CONTAINER
    # ------------------------------------------------------------------------------------
    #4.2.1
    def __exportnode_station(self, nommodel, stream_):
        itypehydra=self.instances["node_type"]["station"]["group_hydra_no"]

        resStn = self.project.execute("""
            select id, name
            from %s.station
            order by id
            """%(nommodel)).fetchall()

        resNod = self.project.execute("""select id    from %s.station_node    order by id"""%(nommodel)).fetchall()
        self.__update_node_max(resNod)
        for recStn in resStn:
            #                    0    1      2      3        4
            res = self.project.execute("""
                select id, name, area, z_invert, station
                from  %s.station_node
                where station=%d
                """%(nommodel,recStn[0])).fetchall()

            comment="Element NDSG '%s'" % (recStn[1])
            self.__writ_parametres_generaux(stream_, res, itypehydra, 0, comment)

            for i, rec in enumerate(res):
                self.log.notice("station", rec[1])
                stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
                stream_.write("%10.3f %10.3f\n" % (rec[3],rec[2]))
            stream_.write("\n")
        return

    #4.2.2
    def __exportnode_storage(self, nommodel, stream_, nomtable, element):
        itypehydra=self.instances["node_type"]["storage"]["group_hydra_no"]  #37
        res = self.project.execute("""
            select id, name, zs_array, zini, contraction_coef
            from %s.%s
            order by id
            """%(nommodel,nomtable)).fetchall()

        self.__update_node_max(res)
        self.__writ_parametres_generaux(stream_, res, itypehydra, 10, "Element "+element)

        for i, rec in enumerate(res):
            self.log.notice("storage", rec[1])
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            #stream_.write("%10.3f %10.2f %10.3f" % (rec[2][0][0],rec[2][np-1][1],rec[3]))
            stream_.write("%10.3f %10.3f" % (rec[3], rec[4]))
            self.__writ_tableau(rec[2], stream_, "%10.3f %19.2f")
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.2.3
    def __exportnode_2d(self, nommodel, stream_):

        itypehydra=self.instances["node_type"]["elem_2d"]["group_hydra_no"]  #36

        resDomain = self.project.execute("""
            select id, name
            from %s.domain_2d
            order by id
            """%(nommodel)).fetchall()

        resDomain.insert(0,[0,'default_domain'])

        for recDomain in resDomain :
            #                    0   1      2   3     4     5     6
            res = self.project.execute("""
            with brut as (
                select n.id, n.name, n.area, n.zb, n.rk, ST_X(n.geom) as x, ST_Y(n.geom) as y, coalesce(l.land_type, 'no_land_type_found') as land_type
                from {}.elem_2d_node as n
                left join project.land_occupation as l on ST_Contains(l.geom, n.geom)
                where coalesce(domain_2d,0)={}
            )
            select distinct on (id) id, name, area, zb, rk, x, y, land_type
            from brut
            order by id, nullif(land_type, 'no_land_type_found') nulls last
            """.format(nommodel, recDomain[0])).fetchall()


            self.__update_node_max(res)

            self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Element PAV  '%s'" % (recDomain[1]))
            for i, (id_, name, area, zb, rk, x, y, land_type) in enumerate(res):
                self.log.notice("element 2D", name)
                zb_min = zb #@todo: à capturer à partir des LPAV adjacents
                stream_.write("%5d %9d     '%s'\n" %(i+1, id_, name))
                stream_.write("%10.2f %10.4f %10.2f %10.4f %12.2f %12.2f %9d %s\n" % (area, zb, rk, zb_min, x, y, recDomain[0], land_type))
            if (len(res)>0):
                stream_.write("\n")
        return

    #4.2.4
    def __exportnode_crossroad(self, nommodel, stream_):
        itypehydra=self.instances["node_type"]["crossroad"]["group_hydra_no"]  #38
        res = self.project.execute("""
            select id, name, area, z_ground, h, ST_X(geom), ST_Y(geom)
            from %s.crossroad_node
            order by id
            """%(nommodel)).fetchall()

        self.__update_node_max(res)
        self.__writ_parametres_generaux(stream_, res, itypehydra, 1, "Element CAR ")
        for i, rec in enumerate(res):
            self.log.notice("crossroad", rec[1])
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            stream_.write("%10.4f %10.4f %10.3f  %12.2f %12.2f\n" % (rec[3], rec[4], rec[2], rec[5], rec[6]))
        if (len(res)>0):
            stream_.write("\n")

    # ------------------------------------------------------------------------------------
    # export functions for tables SINGULARITIES
    # ------------------------------------------------------------------------------------
    def __initialize_new_node(self, nommodel):
        res = self.project.execute(
        "select id  from %s._node order by id"%(nommodel)
        ).fetchall()

        n=len(res)
        return res[n-1][0]+1

    #4.2.6
    def __exportsing_gate(self, nommodel, stream_, streamtmp):
        itypehydra=self.instances["singularity_type"]["gate"]["group_hydra_no"]
        #            0    1         2       3              4         5      6      7                     8                        9                 10
        res = self.project.execute("""
            select s.id, s.name, s.node, s.z_invert, s.z_ceiling, s.width, s.cc, s.cc_submerged, hydra.valve_mode.id, hydra.action_gate_type.id, s.z_gate, s.v_max_cms, s.full_section_discharge_for_headloss
            from %s.gate_singularity as s
            join hydra.valve_mode on hydra.valve_mode.name = s.mode_valve
            join hydra.action_gate_type on hydra.action_gate_type.name = s.action_gate_type
            order by s.id
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, 1, "Element EORIF")
        for i, rec in enumerate(res):
            self.log.notice("singularity gate", rec[1])
            self.__writ_one_singularity(rec[1], rec[2], i, stream_, streamtmp)
            id_section_discharge = 0 if rec[12] else 1
            cc_submerged = rec[7] if rec[7] is not None and rec[7]>0 else -1
            stream_.write("%9.3f %9.3f %9.3f %9.3f" % (rec[3:7]))
            stream_.write("%9.3f" % (cc_submerged))
            stream_.write("%4d %4d %9.3f %9.3f" % (rec[8:12]))
            stream_.write("%4d\n" % (id_section_discharge))
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.2.7  &  4.2.9   -    export function for hydraulic_cut_singularity (4.2.7) and param_headloss_singularity (4.2.9)
    def __exportsing_param(self, nommodel, stream_, streamtmp, tablesing, name_array, code_elem, code_hydra):
        # tablesing                     name_array    code_elem  code_hydra
        # hydraulic_cut_singularity     qz_array      CPR        ECPR(23)
        # param_headloss_singularity    q_dz_array    DH         ESPRM(10)
        itypehydra=self.instances["singularity_type"][tablesing.replace("_singularity", "")]["group_hydra_no"]
        res = self.project.execute("""
            select id, name, node, %s, full_section_discharge_for_headloss
            from %s.%s
            order by id
            """%(name_array,nommodel,tablesing)).fetchall()

        self.__writ_parametres_generaux(stream_, res, itypehydra, 1, "Element %s"%(code_hydra))
        for i, rec in enumerate(res):
            self.log.notice("singularity {}".format(tablesing), rec[1])
            self.__writ_one_singularity(rec[1], rec[2], i, stream_, streamtmp)
            id_section_discharge = 0 if rec[4] else 1
            stream_.write("%5d" %(id_section_discharge))
            self.__writ_tableau(rec[3], stream_, "%10.3f %9.4f")
        if (len(res)>0):
            stream_.write("\n")
        return


    #4.2.8
    def __exportsing_borda_headloss(self, nommodel, stream_, streamtmp):
        itypehydra=self.instances["singularity_type"]["borda_headloss"]["group_hydra_no"]
        res = self.project.execute("""
            select id, name, node, law_type, param_json->law_type::varchar, full_section_discharge_for_headloss
            from %s.borda_headloss_singularity
            order by id
            """%(nommodel)).fetchall()

        self.__writ_parametres_generaux(stream_, res, itypehydra, 1, "Element ESMK")
        for i, (id_, name, node, law_type, param, full_section_discharge_for_headloss) in enumerate(res):
            self.log.notice("singularity borda", name)
            self.__writ_one_singularity(name, node, i, stream_, streamtmp)

            num_law_type = self.instances['borda_headloss_singularity_type'][law_type]['id']
            id_section_discharge = 0 if full_section_discharge_for_headloss else 1

            stream_.write("%5d %5d" %(num_law_type, id_section_discharge))
            stream_.write("       headloss type: %s" %(law_type))
            stream_.write("\n")
            if (law_type=="x"):
                self.log.warning("type %s: ne marche pas, ignoré" % law_type)
                #@todo: à voir, a priori fichier json
                continue

            np=0
            for n in sorted(self.instances['borda_headloss_singularity_type'][law_type]['params'].keys(),
                    key=lambda k:
                        self.instances['borda_headloss_singularity_type'][law_type]['params'][k]['place']):
                param_type = self.instances['borda_headloss_singularity_type'][law_type]['params'][n]['type']
                np+=1

                if n == 'middle_option':
                    if param['middle_option'] == 'no':
                        param['middle_cs_area'] = 1

                if n == 'connector_no':
                    if param['connector_no'] == 0:
                        param['ls'] = 1

                if (param_type=="real"):
                    if law_type == "sharp_bend_rectangular" and (n == "param3" or n == "param4"):
                        if param['bend_shape'] == 'u_shaped_elbow':
                            stream_.write("  %f" % float(param[n]))
                        else:
                            np-=1
                    else:
                        stream_.write("  %f" % float(param[n]))  # corr TL 20171123

                elif (param_type=="integer"):
                    stream_.write("  %10d" % param[n])
                elif (param_type[0:5]=="real["):
                    param_cleared = [[q,h] for [q,h] in param[n] if q is not None and h is not None]
                    # case concerned : parametric
                    for v in param_cleared:
                        stream_.write("  %10.3f  %10.3f" % (v[0],v[1]))
                    np+=2*len(param_cleared)-1
                else:
                    # type enumeration expected. conversion into integer
                    try:
                        m = re.match(r'enum\((.*)\)', self.instances['borda_headloss_singularity_type'][law_type]['params'][n]['type'])
                        enum_map = {v: i+1 for i, v in enumerate(m.group(1).replace("'","").split(', '))}
                        stream_.write("  %10d" % enum_map[param[n]])
                    except AttributeError:
                        self.log.error("Unexpected case. AttributeError on:"
                            +"n:"+str(n)
                            +"param[n]:"+str(param[n])
                            +"type(param[n]):"+type(param[n])
                            +str(self.instances['borda_headloss_singularity_type'][law_type]['params'][n]['type']))
                        return

            for n in range(12-np):
                stream_.write("  %10.1f" % 0.) # pad with zeros
            stream_.write("\n")


        if (len(res)>0):
            stream_.write("\n")
        return

    #4.2.10
    def __exportsing_zregul_weir(self, nommodel, stream_, streamtmp):
        itypehydra=self.instances["singularity_type"]["zregul_weir"]["group_hydra_no"]
        res = self.project.execute("""
            select id, name, node, z_invert, z_regul, width, cc, mode_regul, reoxy_law, reoxy_param_json->reoxy_law::varchar, full_section_discharge_for_headloss
            from %s.zregul_weir_singularity
            order by id
            """%(nommodel)).fetchall()

        self.__writ_parametres_generaux(stream_, res, itypehydra, 1, "Element EDEVM")
        for i, rec in enumerate(res):
            self.log.notice("singularity weir", rec[1])
            self.__writ_one_singularity(rec[1], rec[2], i, stream_, streamtmp)

            id_mode_regul = self.instances['weir_mode_regul'][rec[7]]['id']
            id_law = self.instances['weir_mode_reoxy'][rec[8]]['id']
            id_section_discharge = 0 if rec[10] else 1
            p = [float(rec[9][n]) for n in sorted(self.instances['weir_mode_reoxy'][rec[8]]['params'].keys(), key=lambda k: self.instances['weir_mode_reoxy'][rec[8]]['params'][k]['place'])]
            p += [0.]*(3-len(p)) # pad with zeros

            stream_.write("%10.3f %9.3f %9.2f %9.3f %4d %4d\n" % (rec[3], rec[4], rec[5], rec[6], id_mode_regul, id_section_discharge))
            stream_.write("%5d %9.3f %9.3f %9.3f\n" % (id_law, p[0], p[1], p[2]))

        if (len(res)>0):
            stream_.write("\n")
        return

    #4.2.12  et #4.2.14  :  modif TL -  les objets markers sont exportés désormais comme des élments BC  ( cf % 4.3.29 et 4.3.30)

    #4.2.11
    def __exportsing_regul_gate(self, nommodel, stream_, streamtmp):
        itypehydra=self.instances["singularity_type"]["regul_sluice_gate"]["group_hydra_no"]
        #           0    1    2           3      4        5          6     7     8                9                10            11          12
        res = self.project.execute("""
            select id, name, mode_regul, node, z_invert, z_ceiling, width, cc, cc_submerged, action_gate_type, z_invert_stop, z_ceiling_stop, v_max_cms, dt_regul_hr,
            z_control_node, z_pid_array, z_tz_array, q_tq_array, q_z_crit, nr_z_gate, full_section_discharge_for_headloss, h_open, h_close, hq_array from %s.regul_sluice_gate_singularity
            order by id
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, 1, "Element EACTA")
        for i, rec in enumerate(res):
            self.log.notice("singularity gate", rec[1])
            self.__writ_one_singularity(rec[1], rec[3], i, stream_, streamtmp)

            num_action = self.instances['action_gate_type'][rec[9]]['id']
            regul = self.instances['gate_mode_regul'][rec[2]]['id']
            id_section_discharge = 0 if rec[20] else 1
            cc_submerged = rec[8] if rec[8] is not None and rec[8]>0 else -1

            stream_.write("%10.3f %9.3f %9.3f %9.3f" % (rec[4:8]))
            stream_.write("%9.3f" % (cc_submerged))
            stream_.write("%4d" % num_action)
            stream_.write("%10.3f %9.3f %9.3f" % (rec[10:13]))
            stream_.write("%9.3f %4d %4d\n" % (rec[13], regul, id_section_discharge))
            if rec[2]=='elevation':
                stream_.write("%10d %9.3f %9.3f %9.3f " %(rec[14], rec[15][0], rec[15][1], rec[15][2]))
                self.__writ_tableau(rec[16], stream_, "%10.3f %9.4f")
            if rec[2]=='discharge':
                stream_.write("%9.3f " % rec[18])
                self.__writ_tableau(rec[17], stream_, "%10.3f %9.4f")
            if rec[2]=='no_regulation':
                stream_.write("%9.3f\n" % rec[19])
                # for other options, __write_tableau includes line-ends
                # for no_regulation, force line-end :
            if rec[2]=='relief':
                stream_.write("%9.3f %9.3f\n"%(rec[21], rec[22]))
            if rec[2]=='head':
                stream_.write("%9.3f %9.3f %9.3f %9.3f\n"%(rec[15][0], rec[15][1], rec[15][2], rec[21]))
            if rec[2]=='q(h)':
                stream_.write("%9.3f %9.3f %9.3f " %(rec[15][0], rec[15][1], rec[15][2]))
                self.__writ_tableau(rec[23], stream_, "%10.3f %9.4f")
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.2.13
    def __exportsing_bradley_headloss(self, nommodel, stream_, streamtmp):
        itypehydra=self.instances["singularity_type"]["bradley_headloss"]["group_hydra_no"]
        #            0     1        2       3              4                 5                       6            7
        res = self.project.execute("""
            select s.id, s.name, s.node, s.d_abutment_l, s.d_abutment_r, hydra.abutment_type.id, s.zw_array, s.z_ceiling
            from %s.bradley_headloss_singularity as s
            join hydra.abutment_type on hydra.abutment_type.name = s.abutment_type
            order by s.id
            """%(nommodel)).fetchall()

        self.__writ_parametres_generaux(stream_, res, itypehydra, 1, "Element EBRD")
        for i, rec in enumerate(res):
            self.log.notice("singularity bradley", rec[1])
            self.__writ_one_singularity(rec[1], rec[2], i, stream_, streamtmp)
            stream_.write("%10.3f %9.3f %4d %9.3f" % (rec[3],rec[4],rec[5],rec[7]))
            self.__writ_tableau(rec[6], stream_, "%10.3f %9.2f")
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.2.15
    def __exportsing_bridge_headloss(self, nommodel, stream_, streamtmp):
        itypehydra=self.instances["singularity_type"]["bridge_headloss"]["group_hydra_no"]
        #          0    1    2      3        4       5
        res = self.project.execute("""
            select id, name, node, z_road, l_road, zw_array, full_section_discharge_for_headloss
            from %s.bridge_headloss_singularity
            order by id
            """%(nommodel)).fetchall()

        self.__writ_parametres_generaux(stream_, res, itypehydra, 1, "Element EBRDG")
        for i, rec in enumerate(res):
            self.log.notice("singularity bridge", rec[1])
            self.__writ_one_singularity(rec[1], rec[2], i, stream_, streamtmp)
            id_section_discharge = 0 if rec[6] else 1
            stream_.write("%10.3f %9.3f %4d" % (rec[3],rec[4], id_section_discharge))
            self.__writ_tableau(rec[5], stream_, "%10.3f %9.2f")
        if (len(res)>0):
            stream_.write("\n")
        return

    # ------------------------------------------------------------------------------------
    # export functions for tables NODES river & manhole
    # ------------------------------------------------------------------------------------

    #4.3.1
    def __exportnode_manhole(self, nommodel, stream_):
        # Query the database and obtain data as Python objects
        # itypehydra=28
        itypehydra=self.instances["node_type"]["manhole"]["group_hydra_no"]
        res = self.project.execute("""
            select n.id, n.name, n.area, n.z_ground, ST_X(n.geom), ST_Y(n.geom), hydra.manhole_connection_type.id, n.cover_diameter, n.cover_critical_pressure, n.inlet_width , n.inlet_height
            from %s.manhole_node as n
            join hydra.manhole_connection_type on hydra.manhole_connection_type.name = n.connection_law
            order by n.id
            """%(nommodel)).fetchall()
        self.__update_node_max(res)
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Liaison LCLNOD manhole_node")
        for i, rec in enumerate(res):
            self.log.notice("node manhole", rec[1])
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            stream_.write("%10.3f %10.4f %14.3f %14.3f %4d" % (rec[2],rec[3],rec[4],rec[5],rec[6]))
            if rec[6] == 1 : #cover
                stream_.write("%10.4f %10.4f\n"% (rec[7],rec[8]))
            elif rec[6] == 2 : #inlet
                stream_.write("%10.4f %10.4f\n"% (rec[9],rec[10]))
            else :
                raise Exception("Unexpected case. AttributeError on: manhole_connection_type")
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.2
    def __exportnode_river(self, nommodel, stream_):
        # Query the database and obtain data as Python objects
        # itypehydra=28
        itypehydra=self.instances["node_type"]["river"]["group_hydra_no"]
        res = self.project.execute("""
            select id, name, area, z_ground, ST_X(geom), ST_Y(geom)
            from %s.river_node
            order by id
            """%(nommodel)).fetchall()
        self.__update_node_max(res)
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Liaison LCLNOD river_node")
        for i, rec in enumerate(res):
            self.log.notice("node river", rec[1])
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            stream_.write("%10.3f %10.4f %14.3f %14.3f\n" % (rec[2],rec[3],rec[4],rec[5]))
        if (len(res)>0):
            stream_.write("\n")
        return

    # ------------------------------------------------------------------------------------
    # export functions for tables LINKS
    # ------------------------------------------------------------------------------------

    #4.3.5
    def __exportlink_derivation_pump(self, nommodel, stream_):
        itypehydra=self.instances["link_type"]["deriv_pump"]["group_hydra_no"]   #12
        srequete = """
            select id, name, up, down, q_pump, qz_array
            from %s.deriv_pump_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, 10, "Liaison LQDZ_pomp")
        for i, rec in enumerate(res):
            self.log.notice("link deriv pump", rec[1])
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[2], rec[3], rec[1]))
            loipomp=list()
            for j in range(len(rec[5])):
                loipomp.append([rec[4]*rec[5][j][0], rec[5][j][1]])
            self.__writ_tableau(loipomp, stream_, "%10.3f %9.3f")
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.6
    def __exportlink_weir(self, nommodel, stream_):
        itypehydra=self.instances["link_type"]["weir"]["group_hydra_no"]   #3
        srequete = """
            select id, name, up, down, z_invert, width, cc, z_weir, v_max_cms
            from %s.weir_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison LDEV")
        for i, rec in enumerate(res):
            self.log.notice("link weir", rec[1])
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[2], rec[3], rec[1]))
            stream_.write("%10.3f %9.2f %9.3f %9.3f %9.3f\n" % (rec[4],rec[5],rec[6],rec[7],rec[8]))
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.7
    def __exportlink_gate(self, nommodel, stream_):
        itypehydra=self.instances["link_type"]["gate"]["group_hydra_no"]   #4
        #           0    1      2         3          4           5         6    7            8               9               10                       11         12
        srequete = """
            select l.id, l.name, l.up, l.down, l.z_invert, l.z_ceiling, l.width, l.cc, l.cc_submerged, hydra.valve_mode.id, hydra.action_gate_type.id, l.z_gate, l.v_max_cms
            from %s.gate_link as l
            join hydra.valve_mode on hydra.valve_mode.name = l.mode_valve
            join hydra.action_gate_type on hydra.action_gate_type.name = l.action_gate_type
            order by l.id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison LORF")
        for i, rec in enumerate(res):
            self.log.notice("link gate", rec[1])
            cc_submerged = rec[8] if rec[8] is not None and rec[8]>0 else -1
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[2], rec[3], rec[1]))

            stream_.write("%10.3f %9.3f %9.3f %9.3f" % (rec[4:8]))
            stream_.write("%9.3f" % (cc_submerged))
            stream_.write("%4d %4d %9.3f %9.3f \n" % (rec[9:13]))
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.8
    def __exportlink_pump(self, nommodel, stream_):
        itypehydra=self.instances["link_type"]["pump"]["group_hydra_no"]   #10
        srequete = """
            select id, name, up, down, npump, zregul_array, hq_array, response_time_sec
            from %s.pump_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison LPOMP")
        for i, rec in enumerate(res):
            self.log.notice("link pump", rec[1])
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[2], rec[3], rec[1]))
            id, name, up, down, npump, zregul_array, hq_array, response_time_sec = rec
            stream_.write("%5d %9.3f\n" % (npump, response_time_sec))
            for j in range(npump):
                stream_.write(" %9.3f %9.3f\n" % (zregul_array[j][0],zregul_array[j][1]))

                npt=len(hq_array[j])
                number_None = hq_array[j].count([None,None])
                stream_.write("%5d\n" % (npt-number_None))
                for k in range(npt):
                    if [hq_array[j][k][0],hq_array[j][k][1]] != [None, None]:
                        stream_.write(" %9.3f %9.3f\n" % (hq_array[j][k][0],hq_array[j][k][1]))
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.9
    def __exportlink_regul_gate(self, nommodel, stream_):  # liaisons RGZ et RGQ
        itypehydra= 8


        srequete = """
            select
            --  0    1    2           3    4      5          6        7     8       9                10            11          12               13            14 
                id, name, mode_regul, up, down, z_invert, z_ceiling, width, cc, cc_submerged, action_gate_type, z_invert_stop, z_ceiling_stop, v_max_cms, dt_regul_hr,
            --   15                 16           17          18        19           20      21       22        23
                z_control_node, z_pid_array, z_tz_array, q_tq_array, q_z_crit, nr_z_gate, h_open, h_close, hq_array
            from %s.regul_gate_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison RGZ ou RGQ")
        for i, rec in enumerate(res):
            self.log.notice("link regul gate", rec[1])
            num_action = self.instances['action_gate_type'][rec[10]]['id']
            regul = self.instances['gate_mode_regul'][rec[2]]['id']
            cc_submerged = rec[9] if rec[9] is not None and rec[9]>0 else -1

            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[3], rec[4], rec[1]))
            stream_.write("%10.3f %9.3f %9.3f %9.3f" % (rec[5:9]))
            stream_.write("%9.3f" % (cc_submerged))
            stream_.write("%5d" % num_action)
            stream_.write("%10.3f %9.3f %9.3f" % (rec[11:14]))
            stream_.write("%9.3f %4d\n" % (rec[14], regul))
            if rec[2]=='elevation':
                stream_.write("%10d %9.3f %9.3f %9.3f " %(rec[15], rec[16][0], rec[16][1], rec[16][2]))
                self.__writ_tableau(rec[17], stream_, "%10.3f %9.4f")
            if rec[2]=='discharge':
                stream_.write("%9.3f" % rec[19])
                self.__writ_tableau(rec[18], stream_, "%10.3f %9.4f")
            if rec[2]=='no_regulation':
                stream_.write("%9.3f\n" % rec[20])
                # for other options, __write_tableau includes line-ends
                # for no_regulation, force line-end :
            if rec[2]=='relief':
                stream_.write("%9.3f %9.3f\n" % (rec[21], rec[22]))
            if rec[2]=='head':
                stream_.write("%9.3f %9.3f %9.3f %9.3f\n" % (rec[16][0], rec[16][1], rec[16][2], rec[20]))
            if rec[2]=='q(h)':
                stream_.write("%9.3f %9.3f %9.3f " %(rec[16][0], rec[16][1], rec[16][2]))
                self.__writ_tableau(rec[23], stream_, "%10.3f %9.4f")

        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.11
    def __exportlink_connector(self, nommodel, stream_):
        itypehydra=self.instances["link_type"]["connector"]["group_hydra_no"]   #19
        srequete = """
            select id, name, up, down
            from %s.connector_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison LMTR")
        for i, rec in enumerate(res):
            self.log.notice("link connector", rec[1])
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[2], rec[3], rec[1]))
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.12
    def __exportlink_borda_headloss(self, nommodel, stream_):
        itypehydra=self.instances["link_type"]["borda_headloss"]["group_hydra_no"]   #20
        srequete = """
            select id, name, up, down, law_type, param_json->law_type::varchar
            from %s.borda_headloss_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison LMK")
        for i, (id_, name, up, down, law_type, param) in enumerate(res):
            self.log.notice("link borda", name)
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, up, down, name))

            num_law_type = self.instances['borda_headloss_link_type'][law_type]['id'],
            stream_.write("%5d" %(num_law_type))
            stream_.write("       headloss type: %s" %(law_type))
            stream_.write("\n")
            if (law_type=="x" or law_type=="xx"):
                self.log.warning("type %s: ne marche pas, ignoré" % law_type)
                #@todo: à voir, a priori fichier json
                continue

            # Conversion enum en entiers.
            np=0
            for n in sorted(self.instances['borda_headloss_link_type'][law_type]['params'].keys(), key=lambda k: self.instances['borda_headloss_link_type'][law_type]['params'][k]['place']):
                param_type = self.instances['borda_headloss_link_type'][law_type]['params'][n]['type']
                np+=1

                if n == 'middle_option':
                    if param['middle_option'] == 'no':
                        param['middle_cs_area'] = 1

                if n == 'connector_no':
                    if param['connector_no'] == 0:
                        param['ls'] = 1

                if '_cs_area' in n or '_cs_zinvert' in n:
                    side, type = n.split('_cs_')
                    if side in ['up', 'down']:
                        if param[side+'_geom'] == 'computed':
                            param[n] = 1
                        if param[side+'_geom'] == 'circular' and '_cs_zinvert' in n:
                            param[n] = 1

                if (param_type=="real"):
                    if law_type == "sharp_bend_rectangular" and (n == "param3" or n == "param4"):
                        if param['bend_shape'] == 'u_shaped_elbow':
                            stream_.write("  %f" % float(param[n]))
                        else:
                            np-=1
                    else:
                        stream_.write("  %f" % float(param[n]))  # corr TL 20171123

                elif (param_type=="integer"):
                    stream_.write("  %10d" % param[n])
                elif (param_type[0:5]=="real["):
                    param_cleared = [[q,h] for [q,h] in param[n] if q is not None and h is not None]
                    # case concerned : parametric
                    for v in param_cleared:
                        stream_.write("  %10.3f  %10.3f" % (v[0],v[1]))
                    np+=2*len(param_cleared)-1
                else:
                    # type enumeration expected. conversion into integer
                    try:
                        m = re.match(r'enum\((.*)\)', self.instances['borda_headloss_link_type'][law_type]['params'][n]['type'])
                        enum_map = {v: i+1 for i, v in enumerate(m.group(1).replace("'","").split(', '))}
                    except AttributeError:
                        raise Exception("Unexpected case. AttributeError on:"
                            +"n:"+str(n
                            +"param[n]:"+str(param[n]))
                            +"type(param[n]):"+type(param[n])
                            +str(self.instances['borda_headloss_link_type'][law_type]['params'][n]['type']))

                    stream_.write("  %10d" % enum_map[param[n]])

            for n in range(12-np):
                stream_.write("  %10.1f" % 0.) # pad with zeros
            stream_.write("\n")

        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.13
    def __exportlink_2d_mesh(self, nommodel, stream_):
        res = self.project.execute("""
            select id, name, up, down, z_invert, lateral_contraction_coef,
            ST_X(ST_StartPoint(border)),
            ST_Y(ST_StartPoint(border)),
            ST_X(ST_EndPoint(border)),
            ST_Y(ST_EndPoint(border))
            from %s.mesh_2d_link
            order by id
            """%(nommodel)).fetchall()

        # numbers of LPAV
        nlpav=len(res)

        # links LPAV
        if (nlpav>0):
            itypehydra=self.instances["link_type"]["mesh_2d"]["group_hydra_no"]   #5
            stream_.write("$ Liaison LPAV\n")
            stream_.write("%5d %5d \n" % (itypehydra,nlpav))

            # self.__writ_parametres_generaux(stream_, res, itypehydra, -1, coment)
            # res = self.__link_request(nommodel, srequete, stream_, itypehydra, "Liaison LPAV")
            for i, rec in enumerate(res):
                self.log.notice("link 2D", rec[1])
                id, name, up, down, z_invert, lateral_contraction_coef = rec[:6]
                border = rec[6:]
                stream_.write("%5d %10d %10d    '%-17s'\n" %(i+1, up, down, name))
                stream_.write("%f %f %f %f %f %f\n" % (z_invert, border[0], border[1], border[2], border[3], lateral_contraction_coef))
            stream_.write("\n")


    #4.3.14
    def __exportlink_strickler(self, nommodel, stream_):
        itypehydra=self.instances["link_type"]["strickler"]["group_hydra_no"]   #1
        srequete = """
            select id, name, up, down, z_crest1, width1, length, rk, z_crest2, width2
            from %s.strickler_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison LSTK")
        for i, rec in enumerate(res):
            self.log.notice("link strickler", rec[1])
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[2], rec[3], rec[1]))
            stream_.write("%10.3f %9.2f %9.2f %9.2f %9.3f %9.2f\n" % (rec[4:10]))
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.15
    def __exportlink_porous(self, nommodel, stream_):
        itypehydra=self.instances["link_type"]["porous"]["group_hydra_no"]   #2
        srequete = """
            select id, name, up, down, z_invert, width, transmitivity
            from %s.porous_link
            order by id
            """%(nommodel)  # s désigne zs
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison LPOR")
        for i, rec in enumerate(res):
            self.log.notice("link porous", rec[1])
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[2], rec[3], rec[1]))
            stream_.write("%10.3f %9.2f %10.3g\n" % (rec[4:7]))
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.16
    def __exportlink_overflow(self, nommodel, stream_):
        itypehydra=self.instances["link_type"]["overflow"]["group_hydra_no"]   #6
        #                     0    1    2    3     4        5       6          7     8      9                       10          11        12           13     14       15        16
        srequete = """select id, name, up, down, z_crest1, width1, z_crest2, width2, cc, lateral_contraction_coef, break_mode, z_invert, width_breach, grp, z_break, t_break, dt_fracw_array
            from %s.overflow_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison OVFL")
        for i, rec in enumerate(res):
            self.log.notice("link overflow", rec[1])
            break_mode = self.instances["fuse_spillway_break_mode"][rec[10]]["id"]

            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, rec[2], rec[3], rec[1]))
            stream_.write("%10.3f %9.2f %10.3f %9.2f %9.2f %9.2f" % (rec[4:10]))
            stream_.write("%4d\n" % (break_mode))
            if rec[10] != 'none':
                stream_.write("%10.3f %10.3f %10d" %(rec[11:14]))
                if rec[10]=='zw_critical':  # coor format TL 20171129
                    stream_.write("%9.3f" %(rec[14]))
                elif rec[10]=='time_critical':
                    stream_.write("%9.3f" %(rec[15]))
                self.__writ_tableau(rec[16], stream_, "%10.3f %9.3f")
        if (len(res)>0):
            stream_.write("\n")
        return

    def __exportlink_network_overflow(self, nommodel, stream_):
        itypehydra=self.instances["link_type"]["network_overflow"]["group_hydra_no"]   #6
        srequete = """
            select id, name, up, down, z_overflow, area
            from %s.network_overflow_link
            order by id
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison NOVFL")
        for i, (id_, name, up, down, z_overflow, area) in enumerate(res):
            self.log.notice("link network overflow", name)
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, up, down, name))
            stream_.write("%10.3f %9.2f\n" % (
                z_overflow, area))
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.18
    def __exportlink_street(self, nommodel, stream_):
        itypehydra = 21 # Street links stored as view, so cannot access properties in hydra.instances.json
        srequete = """
            select id, name, up, down, width, width_invert, length, rk
            from %s.street_link
            order by id
            """%(nommodel)

        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison LRUE")
        for i, (id_, name, up, down, width, width_invert, length, rk) in enumerate(res):
            self.log.notice("link street", name)
            stream_.write("%5d %10d %10d    '%-17s'\n" %(i+1, up, down, name))
            stream_.write("%9.2f %9.2f %9.3f %9.2f\n" % (width, length, rk, width_invert if width_invert else width))
        if (len(res)>0):
            stream_.write("\n")
        return

    # Pipes excluded from branches (treated as links in LI file)
    def __exportlink_pipe(self, nommodel, stream_):
        itypehydra = 36
        srequete = """
            select p.id, p.name, p.up, p.down, p.z_invert_up, p.z_invert_down, p.custom_length, ST_Length(p.geom), hydra.cross_section_type.id, p.h_sable, p.rk, p.cross_section_type, p.circular_diameter, p.ovoid_height, p.ovoid_top_diameter, p.ovoid_invert_diameter, p.cp_geom, p.op_geom, p.qcap
            from %s.pipe_link as p
            join hydra.cross_section_type on (hydra.cross_section_type.name = p.cross_section_type)
            where p.exclude_from_branch and p.up_type='manhole' and p.down_type='manhole'
            """%(nommodel)
        res = self.__link_request(nommodel, srequete, stream_, itypehydra, -1, "Liaison LPIPE")
        for i, (id_, name, up, down, zup, zdown, cl, ll, i_cs, h_sable, rk, cs_type, ci_diameter, ov_height, ov_top_diameter, ov_invert_diameter, cp_geom, op_geom, qcap) in enumerate(res):
            self.log.notice("link pipe", name)
            stream_.write("%5d %9d %9d    '%s'\n" %(i+1, up, down, name))
            length = cl if cl is not None else ll
            stream_.write("%9.3f %9.3f %9.2f %4d %9.3f %15.6f\n" % (zup, zdown, length, i_cs, h_sable, qcap))
            if i_cs==self.instances["cross_section_type"]['circular']["group_hydra_no"]:
                stream_.write("%9.3f %9.3f\n" % (ci_diameter, rk))
            elif i_cs==self.instances["cross_section_type"]['ovoid']["group_hydra_no"]:
                stream_.write("%9.3f %9.3f %9.3f %9.3f\n" % (ov_height, ov_top_diameter, ov_invert_diameter, rk))
            elif i_cs==self.instances["cross_section_type"]['pipe']["group_hydra_no"]:
                stream_.write("%9.3f %4d\n" % (cp_geom, rk))
            elif i_cs==self.instances["cross_section_type"]['channel']["group_hydra_no"]:
                stream_.write("%9.3f %4d\n" % (op_geom, rk))
        if (len(res)>0):
            stream_.write("\n")
        return

    # ------------------------------------------------------------------------------------
    # export functions for BOUNDARY CONDITIONS
    # ------------------------------------------------------------------------------------

    #4.3.20
    def __exportbc_weir(self, nommodel, stream_):
        itypehydra=self.instances["singularity_type"]["weir_bc"]["group_hydra_no"]
        res = self.project.execute("""
            select node, name, z_weir, width, cc
            from %s.weir_bc_singularity
            order by id
            """%(nommodel)).fetchall()

        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Element QDL1")
        for i, rec in enumerate(res):
            self.log.notice("weir BC", rec[1])
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            stream_.write(" %9.3f %9.3f %9.3f\n" % (rec[2],rec[3],rec[4]))
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.21
    def __exportbc_strickler(self, nommodel, stream_):
        itypehydra=self.instances["singularity_type"]["strickler_bc"]["group_hydra_no"]
        res = self.project.execute("""
            select node, name, slope, k, width
            from %s.strickler_bc_singularity
            order by id
            """%(nommodel)).fetchall()

        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Element LCRK (CL Strickler)")
        for i, rec in enumerate(res):
            self.log.notice("strickler BC", rec[1])
            mode=0
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            stream_.write("%5d %9.6f %9.2f %9.2f\n" % (mode,rec[2],rec[3],rec[4]))
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.22
    def __exportbc_zq(self, nommodel, stream_):
        itypehydra=self.instances["singularity_type"]["zq_bc"]["group_hydra_no"]
        res = self.project.execute("""
            select node, name, zq_array
            from %s.zq_bc_singularity
            order by id
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, 10, "Element LCLQZ")
        for i, rec in enumerate(res):
            self.log.notice("zq BC", rec[1])
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            self.__writ_tableau(rec[2], stream_, "%14.3f %14.3f")
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.23
    def __exportbc_tz(self, nommodel, stream_):
        itypehydra=self.instances["singularity_type"]["tz_bc"]["group_hydra_no"]
        res = self.project.execute("""
            select node, name, cyclic, tz_array
            from %s.tz_bc_singularity
            order by id
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, 0, "Element LCLZT")
        for i, rec in enumerate(res):
            self.log.notice("tz BC", rec[1])
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            icyclic=0
            if (rec[2]):
                icyclic=1
            stream_.write("%5d \n" %(icyclic))
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.24
    def __exportbc_froude(self, nommodel, stream_):
        itypehydra=self.instances["singularity_type"]["froude_bc"]["group_hydra_no"]
        res = self.project.execute("""
            select node, name
            from %s.froude_bc_singularity
            order by id
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Element LCLZF (CL Froude)")
        for i, rec in enumerate(res):
            self.log.notice("froude BC", rec[1])
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.25
    def __exportbc_tank(self, nommodel, stream_):
        itypehydra=self.instances["singularity_type"]["tank_bc"]["group_hydra_no"]
        res = self.project.execute("""
            select node, name, zs_array, zini, treatment_mode, treatment_param_json->treatment_mode::varchar
            from %s.tank_bc_singularity
            order by id
            """%(nommodel)).fetchall()

        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Element LCLBO")
        for i, rec in enumerate(res):
            self.log.notice("tank", rec[1])
            id_treatment_mode = self.instances['pollution_treatment_mode'][rec[4]]['id']
            if rec[5]: # Treatment param
                p = [float(rec[5][n]) for n in sorted(self.instances['pollution_treatment_mode'][rec[4]]['params'].keys(), key=lambda k: self.instances['pollution_treatment_mode'][rec[4]]['params'][k]['place'])]
            else:
                p=[0.]*4
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            stream_.write("%10.3f %5d %9.3f %9.3f %9.3f %9.3f" % (rec[3], id_treatment_mode, p[0], p[1], p[2], p[3]))
            self.__writ_tableau(rec[2], stream_, "%14.3f %14.3f")

        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.26
    def __exportbc_hydrograph(self, nommodel, stream_):
        itypehydra=self.instances["singularity_type"]["hydrograph_bc"]["group_hydra_no"]
        res = self.project.execute("""
            select node, name, storage_area
            from %s.hydrograph_bc_singularity
            order by id
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Element LCLQT (Hydrogrammes)")
        for i, rec in enumerate(res):
            self.log.notice("hydrograph", rec[1])
            stream_.write("%5d %9d '%s'\n" %(i+1, rec[0], rec[1]))
            stream_.write("%10.2f\n" %(rec[2]))
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.27
    def __exportbc_constant_inflow(self, nommodel, stream_):
        itypehydra=self.instances["singularity_type"]["constant_inflow_bc"]["group_hydra_no"]
        res = self.project.execute("""
            select node, name, q0
            from %s.constant_inflow_bc_singularity
            order by id
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Element LQCON")
        for i, rec in enumerate(res):
            self.log.notice("constant inflow", rec[1])
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[0], rec[1]))
            stream_.write("%10.4f\n" % (rec[2]))
        if (len(res)>0):
            stream_.write("\n")
        return

    #4.3.29    modif TL le 04/07/2017   : l'objet MRKB est exporté désormais comme un élément BC
    def __exportbc_branch_marker(self, nommodel, stream_):
        itypehydra=self.instances["singularity_type"]["pipe_branch_marker"]["group_hydra_no"]
        res = self.project.execute("""
            select id, name, node
            from %s.pipe_branch_marker_singularity
            order by name asc
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Liaison LTRNS(MRKP)")
        for i, rec in enumerate(res):
            self.log.notice("MRKB", rec[1])
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[2], rec[1]))
        if (len(res)>0):
            stream_.write("\n")
        return

     #4.3.30    modif TL le 04/07/2017   : l'objet MRKP est exporté désormais comme un élément BC
    def __exportbc_marker(self, nommodel, stream_):
        itypehydra=self.instances["singularity_type"]["marker"]["group_hydra_no"]
        res = self.project.execute("""
            select id, name, node
            from %s.marker_singularity
            order by name asc
            """%(nommodel)).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Liaison LTRNS(MRKP)")
        for i, rec in enumerate(res):
            self.log.notice("MRKP", rec[1])
            stream_.write("%5d %9d     '%s'\n" %(i+1, rec[2], rec[1]))
        if (len(res)>0):
            stream_.write("\n")
        return

    def __exportbc_air_flow(self, nommodel, stream_):
        itypehydra=self.instances["singularity_type"]["air_flow_bc"]["group_hydra_no"]
        res = self.project.execute(f"""
            select id, name, node, flow, is_inwards
            from {nommodel}.air_flow_bc_singularity as s
            order by id
            """).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Air Flow BC")
        for i, (id, name, node, flow, is_inwards) in enumerate(res):
            self.log.notice("air flow BC", id)
            stream_.write(f"{i+1} {node} '{name}'\n{1 if is_inwards else 2} {flow}\n")
        return

    def __exportbc_air_pressure(self, nommodel, stream_):
        itypehydra=self.instances["singularity_type"]["air_pressure_bc"]["group_hydra_no"]
        res = self.project.execute(f"""
            select id, name, node, relative_pressure
            from {nommodel}.air_pressure_bc_singularity as s
            order by id
            """).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Air Pressure BC")
        for i, (id, name, node, relative_pressure) in enumerate(res):
            self.log.notice("air pressure BC", id)
            stream_.write(f"{i+1} {node} '{name}'\n{relative_pressure}\n")
        return

    def __exportlink_air_duct(self, nommodel, stream_):
        itypehydra=self.instances["link_type"]["air_duct"]["group_hydra_no"]
        res = self.project.execute(f"""
            select id, up, down, name, z_invert_up, z_invert_down,
                coalesce(custom_length, st_length(geom)),
                area, perimeter, friction_coefficient
            from {nommodel}.air_duct_link
            order by id
            """).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Air Duct")
        for i, (id, up, down, name, z_invert_up, z_invert_down,
                length, area, perimeter, friction_coefficient) in enumerate(res):
            self.log.notice("air duct", id)
            stream_.write(f"{i+1} {up} {down} '{name}'\n"
                f"{z_invert_up} {z_invert_down} {length} {area} {perimeter} {friction_coefficient}\n")
        return

    def __exportlink_air_headloss(self, nommodel, stream_):
        itypehydra=self.instances["link_type"]["air_headloss"]["group_hydra_no"]
        res = self.project.execute(f"""
            select id, up, down, name,
                area, up_to_down_headloss_coefficient, down_to_up_headloss_coefficient
            from {nommodel}.air_headloss_link
            order by id
            """).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Air Headloss")
        for i, (id, up, down, name, area, up_to_down_headloss_coefficient,
                down_to_up_headloss_coefficient) in enumerate(res):
            self.log.notice("air headloss", id)
            stream_.write(f"{i+1} {up} {down} '{name}'\n"
                f"{area} {up_to_down_headloss_coefficient} {down_to_up_headloss_coefficient}\n")
        return

    def __exportlink_jet_fan(self, nommodel, stream_):
        itypehydra=self.instances["link_type"]["jet_fan"]["group_hydra_no"]
        res = self.project.execute(f"""
            select id, up, down, name,
                is_up_to_down, number_of_units, unit_thrust_newton, efficiency, flow_velocity, pipe_area
            from {nommodel}.jet_fan_link
            order by id
            """).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Jet Fan")
        for i, (id, up, down, name, is_up_to_down, number_of_units, unit_thrust_newton,
                efficiency, flow_velocity, pipe_area) in enumerate(res):
            self.log.notice("jet fan", id)
            stream_.write(f"{i+1} {up} {down} '{name}'\n"
                f"{1 if is_up_to_down else 2} {number_of_units} "
                f"{unit_thrust_newton} {efficiency} "
                f"{flow_velocity} {pipe_area}\n")
        return

    def __exportlink_ventilator(self, nommodel, stream_):
        itypehydra=self.instances["link_type"]["ventilator"]["group_hydra_no"]
        res = self.project.execute(f"""
            select id, up, down, name,
                q_dp_array, is_up_to_down
            from {nommodel}.ventilator_link
            order by id
            """).fetchall()
        self.__writ_parametres_generaux(stream_, res, itypehydra, -1, "Ventilator")
        for i, (id, up, down, name, q_dp_array, is_up_to_down) in enumerate(res):
            self.log.notice("ventilator", id)
            stream_.write(f"{i+1} {up} {down} '{name}'\n"
                f"{1 if is_up_to_down else 2} {len(q_dp_array)}\n"
                +"\n".join([f'{q} {dp}' for q, dp in q_dp_array])+
                "\n")
        return


    # ------------------------------------------------------------------------------------
    # export functions for 1D domains
    # ------------------------------------------------------------------------------------

    #4.4.1   modif TL  3 mars 2017  + modif TL  01 mai 2017 : ajout du recnod(17) en écriture
    def __export_branch(self, name_model, stream1):
        res = self.project.execute("""
            select id, name, pk0_km, dx
            from %s.branch
            order by id
            """ % (name_model)).fetchall()
        for rec in res:
            self.log.notice("branch", rec[1])
            stream1.write("$\n'%s' %12.5f %9.2f\n" % (rec[1],rec[2],rec[3]))
            #          0     1     2       3              4                 5                  6                           7          8         9     10                    11                  12                  13                  14                      15          16          17
            resnod = self.project.execute("""
                select
                    p.id,
                    p.up,
                    p.down,
                    p.z_invert_up,
                    p.z_invert_down,
                    p.custom_length,
                    ST_Length(p.geom),
                    hydra.cross_section_type.id,
                    p.h_sable,
                    p.branch,
                    p.rk,
                    p.cross_section_type,
                    p.circular_diameter,
                    p.ovoid_height,
                    p.ovoid_top_diameter,
                    p.ovoid_invert_diameter,
                    p.cp_geom,
                    p.op_geom,
                    p.name,
                    p.rk_maj,
                    p.qcap
                from {model}.pipe_link as p
                join {model}.branch as b on p.branch=b.id
                join hydra.cross_section_type on (hydra.cross_section_type.name = p.cross_section_type)
                where p.branch={b} and not p.exclude_from_branch and p.up_type='manhole' and p.down_type='manhole'
                order by st_linelocatepoint(b.geom, st_startpoint(p.geom))
                """.format(model=name_model,b=rec[0])).fetchall()

            for recnod in resnod:
                length = recnod[5] if recnod[5] is not None else recnod[6]
                stream1.write("%10d %10d %9.3f %9.3f %9.2f %4d %9.3f %15.6f  %s \n" % (recnod[1], recnod[2], recnod[3], recnod[4], length, recnod[7], recnod[8], recnod[20], recnod[18]))
                if recnod[7]==self.instances["cross_section_type"]['circular']["group_hydra_no"]:
                    stream1.write("%9.3f %9.3f\n" % (recnod[12], recnod[10]))
                elif recnod[7]==self.instances["cross_section_type"]['ovoid']["group_hydra_no"]:
                    stream1.write("%9.3f %9.3f %9.3f %9.3f\n" % (recnod[13], recnod[14], recnod[15], recnod[10]))
                elif recnod[7]==self.instances["cross_section_type"]['pipe']["group_hydra_no"]:
                    stream1.write("%4d %9.3f\n" % (recnod[16], recnod[10]))
                elif recnod[7]==self.instances["cross_section_type"]['channel']["group_hydra_no"]:
                    stream1.write("%4d %9.3f %9.3f\n" % (recnod[17], recnod[10], recnod[19]))
            stream1.write("\n")
        stream1.close()
        return

    #4.4.2
    def __export_reach(self, name_model, stream1):
        res = self.project.execute("""
            select id, name, pk0_km, dx
            from %s.reach
            order by id
            """ % (name_model)).fetchall()

        for rec in res:
            self.log.notice("reach", rec[1])
            stream1.write("$\n'%s' %12.5f %9.2f\n" % (rec[1],rec[2],rec[3]))
            resnod = self.project.execute("""
                select id, pk_km
                from %s.river_node
                where reach=%d
                order by pk_km
                """ % (name_model,rec[0])).fetchall()

            for recnod in resnod:
                stream1.write("%10d%12.5f\n" % (recnod[0],recnod[1]))
            stream1.write("\n")
        stream1.close()
        return

    #4.4.3
    def __export_pt(self, name_model, stream1):
        #          0    1            2                    3                     4            5
        res = self.project.execute(f"""
            select id, name, type_cross_section_up, type_cross_section_down, z_invert_up, z_invert_down, -- 0-5
            up_rk, up_rk_maj, up_sinuosity, -- 6-8
            up_circular_diameter, -- 9
            up_ovoid_height, up_ovoid_top_diameter, up_ovoid_invert_diameter, -- 10-12
            up_cp_geom, -- 13
            up_op_geom, -- 14
            up_vcs_geom, -- 15
            down_rk, down_rk_maj, down_sinuosity, -- 16-18
            down_circular_diameter, -- 19
            down_ovoid_height, -- 20
            down_ovoid_top_diameter, down_ovoid_invert_diameter, -- 21-22
            down_cp_geom, -- 23
            down_op_geom, -- 24
            down_vcs_geom -- 25
            from {name_model}.river_cross_section_profile
            order by id
            """).fetchall()

        for rec in res:
            self.log.notice("river profile", rec[1])
            it_csup=self.instances["cross_section_type"][rec[2]]["group_hydra_no"] if rec[2] is not None else 0
            it_csdo=self.instances["cross_section_type"][rec[3]]["group_hydra_no"] if rec[3] is not None else 0
            zup=rec[4] if rec[4] is not None else -999. #@todo: à vérifier avec TL
            zdo=rec[5] if rec[5] is not None else -999. #@todo: à vérifier avec TL
            stream1.write("%10d %4d %4d %9.3f %9.3f\n" % (rec[0],it_csup,it_csdo,zup,zdo))
            # up
            if rec[2] is None:
                stream1.write("%4d\n" % (0))
            elif rec[2]=='circular':
                stream1.write("%9.3f %9.3f\n" %(rec[9], rec[6]))
            elif rec[2]=='ovoid':
                stream1.write("%9.3f %9.3f %9.3f %9.3f\n" % (rec[10], rec[11], rec[12], rec[6]))
            elif rec[2]=='pipe':
                stream1.write("%4d %9.3f\n" % (rec[13], rec[6]))
            elif rec[2]=='channel':
                stream1.write("%4d %9.3f\n" % (rec[14], rec[6]))
            elif rec[2]=='valley':
                stream1.write("%4d %9.3f  %9.3f %9.3f\n" % (rec[15], rec[6], rec[7], rec[8]))   # corr TL
            # down
            if rec[3] is None:
                stream1.write("0\n")
            elif rec[3]=='circular':
                stream1.write("%9.3f %9.3f\n" % (rec[19], rec[16]))
            elif rec[3]=='ovoid':
                stream1.write("%9.3f %9.3f %9.3f %9.3f\n" % (rec[20], rec[21], rec[22], rec[16]))
            elif rec[3]=='pipe':
                stream1.write("%4d %9.3f\n" % (rec[23], rec[16]))
            elif rec[3]=='channel':
                stream1.write("%4d %9.3f\n" % (rec[24], rec[16]))
            elif rec[3]=='valley':
                stream1.write("%4d %9.3f %9.3f %9.3f\n" % (rec[25], rec[16], rec[17], rec[18]))   # corr TL
            stream1.write("\n")    # corr TL
        stream1.close()
        return
    #4.4.4 : cf 3.3 (sections)

    #4.4.6
    def __export_sup(self, name_model, node_user_max, streamtmp, stream1):
        streamtmp.seek(0)

        # bloc *NUMNP
        self.log.notice("option *NUMNP")
        stream1.write("*NUMNP\n")
        stream1.write("%10d\n\n" % (node_user_max))

        # bloc *SING
        self.log.notice("option *SING")
        stream1.write("*SING\n")
        str = streamtmp.readline()
        while str != "":
            stream1.write("%s" % str)
            str = streamtmp.readline()
        stream1.write("\n")

        # bloc *JUNCTION
        self.log.notice("option *JUNCTION")
        stream1.write("*JUNCTION\n")
        res = self.project.execute("""
            select id, z_ground, ST_X(geom), ST_Y(geom)
            from  %s.manhole_node
            order by id
            """ % (name_model)).fetchall()
        for rec in res:
            npipe_on_downstream = len(self.project.execute("""
                select  id  from  %s.pipe_link
                where  up=%d
                """ % (name_model,rec[0])).fetchall())
            npipe_on_upstream = len(self.project.execute("""
                select  id
                from  %s.pipe_link
                where  down=%d
                """ % (name_model,rec[0])).fetchall())
            if (npipe_on_downstream>=2 or npipe_on_upstream>=2):
                stream1.write("%10d %9.4f %19.6f %19.6f\n"%(rec[0],rec[1],rec[2],rec[3]))
        stream1.write("\n")
        stream1.close()
        return

    def exportmodel(self, nomscenario, model, directory):
        """
        export module for calculation
        nomscenario: name of a scenario
        model: name of a model
        """
        self.__node_max=0
        self.__icount_hydrol=0
        nomfichier=os.path.join(directory, nomscenario.upper()+"_"+model.upper())

        model_object = Model(self.project, self.project.srid, model)
        topo_check = model_object.topo_check()
        if topo_check:
            raise ExportError("Topological errors in model {}: {}. See hydra.log file for details.".format(
                        model, ', '.join([topo_error_dict[i] for i in topo_check.keys()])))

        #
        #--- export for hydrologic comptuations (files .LI1  .LI2)
        #
        with open(nomfichier+"_hydrol.li1","w") as streamfileLI1, \
             open(nomfichier+"_hydrol.li2","wb") as streamfileLI2:
            self.__exporthydrol_manhole(model, streamfileLI1, streamfileLI2)  #3.2.1
            self.__exporthydrol_catchment(model, streamfileLI1, streamfileLI2)
            self.__exporthydrol_routage(model, streamfileLI1, streamfileLI2)
            self.__exporthydrol_translation(model, streamfileLI1, streamfileLI2)
            self.__exporthydrol_qqsplit(model, streamfileLI1, streamfileLI2)   #3.2.5
            self.__exporthydrol_zqsplit(model, streamfileLI1, streamfileLI2)   #3.2.6
            self.__exporthydrol_rs(model, streamfileLI1, streamfileLI2)   #3.2.7
            self.__exporthydrol_rsp(model, streamfileLI1, streamfileLI2)   #3.2.8
            self.__exporthydrol_bcsingularity(model, streamfileLI1, streamfileLI2)
            self.__exporthydrol_pipe(model, streamfileLI1, streamfileLI2) #3.2.10
            self.__exporthydrol_hydrograph_bc(model, streamfileLI1, streamfileLI2) #3.2.11
            self.__exporthydrol_tz_bc(model, streamfileLI1, streamfileLI2) #3.2.12
            self.__exporthydrol_marker(model, streamfileLI1, streamfileLI2) #3.2.13 #TL 20180318
        #
        #--- export for hydrologic comptuations (files .EL  .LI)
        #

        with open(nomfichier+"_sing.tmp","w+") as streamtmp, \
             open(nomfichier+"_sup.dat", "w") as streamsup, \
             open(nomfichier+".el","w") as streamfichierEL, \
             open(nomfichier+".li","w") as streamfichierLI:
            # note _sing.tmp : tempory file for singularities with additional node

            #
            #--- Tables CONTAINER: writing in file .EL
            self.__exportnode_station(model,streamfichierEL)   #4.2.1
            self.__exportnode_storage(model,streamfichierEL,"storage_node","CAS") # export des casiers
            self.__exportnode_2d(model,streamfichierEL)
            self.__exportnode_crossroad(model,streamfichierEL)
            #
            #--- Tables NODES 1D: writing in file .LI
            self.__exportnode_manhole(model,streamfichierLI) #4.3.1
            self.__exportnode_river(model,streamfichierLI) #4.3.2
            node_user_max=self.__node_max
            #
            #--- Tables SINGULARITIES: writing in file .EL
            self.__exportsing_gate(model, streamfichierEL, streamtmp)  #4.2.6
            self.__exportsing_param(model, streamfichierEL, streamtmp,"hydraulic_cut_singularity","qz_array","CPR","ECPR")    #4.2.7
            self.__exportsing_borda_headloss(model, streamfichierEL, streamtmp)  #4.2.8
            self.__exportsing_param(model, streamfichierEL, streamtmp,"param_headloss_singularity","q_dz_array","DH","ESPRM") #4.2.9
            self.__exportsing_zregul_weir(model, streamfichierEL, streamtmp)  #4.2.10
            self.__exportsing_regul_gate(model, streamfichierEL, streamtmp)  #4.2.11
            self.__exportsing_bradley_headloss(model, streamfichierEL, streamtmp) #4.2.13
            self.__exportsing_bridge_headloss(model, streamfichierEL, streamtmp) #4.2.15
            #
            #--- Tables LINKS: (fichier .LI) : liaisons
            self.__exportlink_derivation_pump(model, streamfichierLI)
            self.__exportlink_weir(model, streamfichierLI) #4.3.6
            self.__exportlink_gate(model, streamfichierLI)
            self.__exportlink_pump(model, streamfichierLI)
            self.__exportlink_regul_gate(model, streamfichierLI)
            self.__exportlink_connector(model, streamfichierLI)  #4.3.11
            self.__exportlink_borda_headloss(model, streamfichierLI)
            self.__exportlink_2d_mesh(model, streamfichierLI)
            self.__exportlink_strickler(model, streamfichierLI)
            self.__exportlink_porous(model, streamfichierLI) #4.3.15
            self.__exportlink_overflow(model, streamfichierLI)
            self.__exportlink_network_overflow(model, streamfichierLI)
            self.__exportlink_street(model, streamfichierLI)
            self.__exportlink_pipe(model, streamfichierLI)
            self.__exportlink_air_duct(model, streamfichierLI)
            self.__exportlink_air_headloss(model, streamfichierLI)
            self.__exportlink_jet_fan(model, streamfichierLI)
            self.__exportlink_ventilator(model, streamfichierLI)
            self.__exportbc_air_flow(model, streamfichierLI)
            self.__exportbc_air_pressure(model, streamfichierLI)
            self.__exportbc_weir(model, streamfichierLI)   #4.3.20
            self.__exportbc_strickler(model, streamfichierLI)   #4.3.21
            self.__exportbc_zq(model, streamfichierLI)
            self.__exportbc_tz(model, streamfichierLI)
            self.__exportbc_froude(model, streamfichierLI)      #4.3.24
            self.__exportbc_tank(model, streamfichierLI)        #4.3.25
            self.__exportbc_hydrograph(model, streamfichierLI)
            self.__exportbc_constant_inflow(model, streamfichierLI)
            self.__exportbc_branch_marker(model, streamfichierLI)             #4.2.12
            self.__exportbc_marker(model, streamfichierLI)             #4.2.14

            self.__export_sup(model, node_user_max, streamtmp, streamsup)

        #--- specific files for 1D-domains
        with open(nomfichier+"_geom_section.dat","w") as stream_geom:
            self.__export_section(model, stream_geom)
        with open(nomfichier+"_branch.dat", "w") as stream_branch:
            self.__export_branch(model, stream_branch)
        with open(nomfichier+"_reach.dat", "w") as stream_reach:
            self.__export_reach(model, stream_reach)
        with open(nomfichier+"_pt.dat", "w") as stream_pt:
            self.__export_pt(model, stream_pt)


        # export geom.dat
        geom_dat_file = nomfichier+"_geom.dat"
        try:
            with open(geom_dat_file, "w") as f:
                for res in self.project.execute("""
                        select {model}.geom_dat('reach', id) as exp
                        from {model}.reach
                            union all
                        select {model}.geom_dat('branch', id) as exp
                        from {model}.branch
                            union all
                        select {model}.geom_dat('2d', id) as exp
                        from {model}.coverage where domain_type='2d'
                            union all
                        select {model}.geom_dat('street', id) as exp
                        from {model}.coverage where domain_type='street'
                            union all
                        select {model}.geom_dat('storage', id) as exp
                        from {model}.coverage where domain_type='storage'
                            union all
                        select
                            case when m.protected='t'
                                then {model}.geom_dat('protected', c.id)
                                else {model}.geom_dat('unprotected', c.id)
                                end as exp
                        from {model}.coverage as c, {model}.coverage_marker as m
                        where c.domain_type is null and ST_Intersects(c.geom, m.geom)
                            order by exp
                        """.format(model=model)):
                    f.write(res[0])
        except Exception:
            self.log.error("Error writing geom.dat file: {}".format(geom_dat_file))
            raise

        # export link.dat
        link_dat_file = nomfichier+"_link.dat"
        try:
            with open(link_dat_file,"w") as f:
                for id, name in self.project.execute("""
                        select id, UPPER(name) as name
                        from {model}._link
                            union all
                        select id, UPPER(name) as name
                        from {model}.street_link
                        order by name asc
                        """.format(model=model)):
                    f.write("{} {}\n".format(id, name))
        except Exception:
            self.log.error("Error writing link.dat file: {}".format(link_dat_file))
            raise

        # export ctrz.dat
        ctrz_dat_file = nomfichier+"_ctrz.dat"
        try:
            with open(ctrz_dat_file,"w") as f:
                for res in self.project.execute("""
                        select {model}.ctrz_dat()
                        """.format(model=model)):
                    f.write(res[0])
        except Exception:
            self.log.error("Error writing ctrz.dat file: {}".format(ctrz_dat_file))
            raise

        # export l2d.dat
        l2d_dat_file = nomfichier+"_l2d.dat"
        try:
            with open(l2d_dat_file,"w") as f:
                for name, xa, ya, xb, yb in self.project.execute("""
                        select name, xa, ya, xb, yb from {model}.l2d
                        """.format(model=model)):
                    f.write("{} {} {} {} {}\n".format(name, xa, ya, xb, yb))
        except Exception:
            self.log.error("Error writing l2d.dat file: {}".format(l2d_dat_file))
            raise

    def exportrac(self, model, nomscenario, directory):
        rac_file = os.path.join(directory, nomscenario.upper()+".rac")
        try:
            with open(rac_file,"a+") as f:
                self.__export_model_connections(model, f)  #2.7
        except Exception:
            self.log.error("Error writing .rac file: {}".format(rac_file))
            raise

    def export(self, id_scenario):
        """
        export module for calculation: main function
        id_scenario: id of a scenario
        """
        try:
            if not isinstance(id_scenario, int):
                id_scenario, =  self.project.execute("""
                    select id from project.scenario where name='{}'
                    """.format(id_scenario)).fetchone()

            name_scenario, comput_mode, transport_type, name_ref_scenario, name_hydrol_scenario =  self.project.execute("""
                select s.name, s.comput_mode, s.transport_type, r.name, h.name
                from project.scenario as s
                full outer join project.scenario as r on s.scenario_ref=r.id
                full outer join project.scenario as h on s.scenario_hydrol=h.id
                where s.id={}
                """.format(id_scenario)).fetchone()
        except TypeError:
            self.log.error("No scenario (id:{})".format(id_scenario))
            raise ExportError("No scenario (id:{})".format(id_scenario))

        if comput_mode == 'hydraulics' and not name_hydrol_scenario:
            self.log.error("No other scenario specified for hydrographs with 'hydraulics' mode.")
            raise ExportError("No other scenario specified for hydrographs with 'hydraulics' mode.")

        # If reference scenario :
        #  - Don't write file .rac,
        #  - Don't write files .LI1/.LI2,
        #  - Don't write files _sing.tmp, .el, .li, _geom.dat, _link.dat, _ctrz.dat, _l2d.dat
        #    -> No model export (is the same as previous points)
        #  + No topo update (branches, coverages,...)

        #  create directories if needed
        directory_scenario = self.project.get_senario_path(id_scenario)
        directory_calculation = os.path.join(directory_scenario, "travail")
        directory_work = os.path.join(directory_scenario, "hydraulique")
        directory_hydrol = os.path.join(directory_scenario, "hydrol")
        directory_calculation_rel = os.path.relpath(directory_calculation, directory_work)

        self.init_folder(directory_scenario)
        self.init_folder(directory_calculation)
        self.init_folder(directory_work)
        self.init_folder(directory_hydrol)

        # Export kernel version file
        kernel_version_file = os.path.join(directory_scenario, "scenario.info")
        try:
            with open(kernel_version_file, "w") as f_kernel_version:
                self.export_metadata(f_kernel_version)
        except Exception:
            self.log.error("Error writing kernel version file: {}".format(kernel_version_file))
            raise

        # Export settings files (Génération des fichiers de paramétrages - note NT1 §2)
        scenario_nom_file = os.path.join(directory_scenario, "scenario.nom")
        try:
            with open(scenario_nom_file, "w") as scenario_nom_stream:
                self.model_to_export = self.__exportsettings_scen(id_scenario, name_scenario, name_ref_scenario, name_hydrol_scenario, scenario_nom_stream)
        except Exception:
            self.log.error("Error writing scenario file: {}".format(scenario_nom_file))
            raise

        # Export command file
        cmd_file = os.path.join(directory_calculation, name_scenario.upper()+".cmd")
        try:
            with open(cmd_file, "w") as cmd_stream:
                self.__exportsettings_gen(id_scenario, cmd_stream)
        except Exception:
            self.log.error("Error writing command file: {}".format(cmd_file))
            raise

        # Open optsor file - 4/12/2024/CHV - According to NT82 optsor file recieve now and first computing options from database
        optsor_dat_file = os.path.join(directory_calculation, name_scenario.upper()+"_optsor.dat")
        optsor_dat_stream = open(optsor_dat_file, "w")  # 4/12/2024/CHV adaptation selon NT82 : _optsor.dat doit rester ouvert pour les donnés de temps sec et de pluie
        optsor_dat_stream.write("!---------------------------------------------------\n")
        optsor_dat_stream.write("!  computing options from database \n")
        optsor_dat_stream.write("!---------------------------------------------------\n\n")

        if comput_mode!='hydraulics':
            qts_file = os.path.join(directory_calculation, name_scenario.upper()+".qts")
            cts_file = os.path.join(directory_calculation, name_scenario.upper()+".cts")
            secteurs_file = os.path.join(directory_calculation, "secteurs.txt")
            try:
                # 4/12/2024/CHV : modifié pour production bloc *F_QTS_NEW selon NT82, UNIQUEMENT s'il y a des données de temps sec
                # with open(qts_file, "w") as qts_stream, open(cts_file, "w") as cts_stream, open(secteurs_file, "w") as secteurs_stream:
                    # self.__exportsettings_dry_inflow(id_scenario, qts_stream, cts_stream, secteurs_stream)
                self.__exportsettings_dry_inflow(id_scenario, qts_file, cts_file, secteurs_file, optsor_dat_stream, directory_work)
            except Exception:
                self.log.error("Error writing dry inflow files: {}".format(', '.join([qts_file, cts_file, secteurs_file])))
                raise
            
            plu_file = os.path.join(directory_calculation, name_scenario.upper()+".plu")
            try:
                with open(plu_file, "w") as plu_stream:
                    self.__exportsettings_rainfall_library(id_scenario, plu_stream, optsor_dat_stream, plu_file)
            except Exception:
                self.log.error("Error writing rain file: {}".format(plu_file))
                raise
            # optsor_dat_stream.write(f"{plu_file}\n\n") # 4/12/2024/CHV - ajout selon NT82

        # Export optsor file
        try:
            # with open(optsor_dat_file, "w") as optsor_dat_stream: # 4/12/2024/CHV adaptation selon NT82 : _optsor.dat doit rester ouvert pour les donnés de temps sec et de pluie
            self.__exportsettings_optsor(id_scenario, optsor_dat_stream)
        except Exception:
            self.log.error("Error writing optsor file: {}".format(optsor_dat_file))
            raise

        # Export project interlinks
        interlink_rac_file = os.path.join(directory_calculation, name_scenario.upper()+"_interlink.rac")
        if self.project.execute("""select exists (select 1 from project.interlink limit 1)""").fetchone()[0]:
            try:
                with open(interlink_rac_file, "w") as interlink_stream:
                    self.exportsettings_interlink(interlink_stream)
            except Exception:
                self.log.error("Error writing interlink file: {}".format(interlink_rac_file))
                raise

        # Export land use
        land_type_file = os.path.join(directory_calculation, name_scenario.upper()+"_land_use.dat")
        if self.project.execute("""select exists (select 1 from project.land_type limit 1)""").fetchone()[0]:
            try:
                with open(land_type_file, "w") as land_type_stream:
                    self.exportsettings_land_type(land_type_stream)
            except Exception:
                self.log.error("Error writing land_type file: {}".format(land_type_file))
                raise

        if transport_type=='pollution':
            stock_pollution_bv_file = os.path.join(directory_calculation, "stock_pollution.bv")
            try:
                with open(stock_pollution_bv_file, "w") as stock_pollution_bv_stream:
                    self.exportsettings_pollution_land_accumulation(stock_pollution_bv_stream)
            except Exception:
                self.log.error("Error writing stock pollution file: {}".format(stock_pollution_bv_file))
                raise

        if not name_ref_scenario:
            for name_model in self.model_to_export:
                initial_config = self.project.execute("""select configuration from {}.metadata""".format(name_model)).fetchone()[0]
                try:
                    configs_brut = self.project.execute("""select configuration from project.config_scenario where scenario={} and model='{}' order by priority desc;""".format(id_scenario, name_model)).fetchall()
                    # Set scenario configurations
                    configs = [string for (string,) in configs_brut]
                    self.log.classic_notice('Configuration handling')
                    if initial_config != 1:
                        # Switch to default config
                        self.project.execute("""update {}.metadata set configuration=1""".format(name_model))
                        self.log.continuous_notice('Configuration changed to 1')
                    for config in configs:
                        # For each config, highest (1) priority last, unpack configured items that exists in this config
                        self.project.execute("""select {}.unpack_config({});""".format(name_model, config))
                        self.log.continuous_notice('Configuration {} applied'.format(config))
                    self.log.continuous_notice('Configuration switches successfull')

                    invalid = self.project.execute("""select name, subtype from {}.invalid;""".format(name_model)).fetchall()
                    if invalid:
                        self.log.classic_error("Found {} invalidities exporting scenario: {} / model: {}.".format(len(invalid), name_scenario, name_model))
                        raise ExportError("Invalidities in model {} for selected configurations in scenario {}.".format(name_model, name_scenario))

                    # Topo: update coverages
                    self.project.execute("""select {}.coverage_update()""".format(name_model))
                    # Topo: update branches
                    self.project.execute("""select {}.branch_update_fct()""".format(name_model))

                    # Export model files
                    self.exportrac(name_model, name_scenario, directory_calculation)
                    self.exportmodel(name_scenario, name_model, directory_calculation)
                    self.log.classic_notice("Model {} exported to {}".format(name_model, directory_scenario))

                    if initial_config!=1 or configs:
                        # Switch back to initial config
                        self.project.execute("""update {}.metadata set configuration={}""".format(name_model, initial_config))
                        self.log.classic_notice('Configuration changed back to {}'.format(initial_config))

                except Exception:
                    # Switch to default config
                    self.project.execute("""update {}.metadata set configuration={}""".format(name_model, initial_config))
                    self.log.error("Error exporting scenario: {} / model: {}".format(name_scenario, name_model))
                    raise

        self.project.commit()
        optsor_dat_stream.close()

    # ------------------------------------------------------------------------------------

    def init_folder(self, folder):
        if not os.path.isdir(folder):
            os.mkdir(folder)
        else:
            for file in os.listdir(folder):
                file_path = os.path.join(folder, file)
                if os.path.isfile(file_path):
                    try:
                        os.remove(file_path)
                    except Exception:
                        self.log.error("Error: cannot remove file {f} from directory {dir}".format(f=os.path.basename(file_path), dir=os.path.dirname(file_path)))
                        raise


# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#
# Export calcul for series
#
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------


class ExportCalculSerie(ExportCalcul) :

    def __init__(self, project):
        ExportCalcul.__init__(self, project)

    def export_scenario_serie(self, id_scn_serie):
        """
        export module for calculation: main function
        id_scenario: id of a serie scenario
        """
        try:
            if not isinstance(id_scn_serie, int):
                id_scn_serie, id_b, id_cs =  self.project.execute("""
                    select id, id_b, id_cs from project.serie_scenario where name='{}'
                    """.format(id_scn_serie)).fetchone()
            else:
                id_b, id_cs =  self.project.execute("""
                    select id_b, id_cs from project.serie_scenario where id='{}'
                    """.format(id_scn_serie)).fetchone()

            name_scenario, comput_mode, transport_type, name_ref_scenario, name_hydrol_scenario =  self.project.execute("""
                select s.name, s.comput_mode, s.transport_type, r.name, h.name
                from project.serie_scenario as s
                full outer join project.serie_scenario as r on r.id = (select id
                from project.serie_scenario where
                date0 = (select min(date0) from project.serie_scenario where id_cs = {id_cs})
                and id_cs = {id_cs})
                full outer join project.serie_scenario as h on s.scenario_hydrol=h.id
                where s.id={id}
                """.format(id = id_scn_serie, id_cs = id_cs)).fetchone()

            first_scn = self.project.execute("""select id from project.serie_scenario where id_cs = {} order by date0 asc limit 1 """.format(id_cs)).fetchone()[0]

            if first_scn == id_scn_serie :
                name_ref_scenario = None

        except TypeError:
            self.log.error("No serie scenario (id:{})".format(id_scn_serie))
            raise ExportError("No serie scenario (id:{})".format(id_scn_serie))

        if comput_mode == 'hydraulics' and not name_hydrol_scenario:
            self.log.error("No other scenario specified for hydrographs with 'hydraulics' mode.")
            raise ExportError("No other scenario specified for hydrographs with 'hydraulics' mode.")

        #  create directories if needed
        directory_scenario = self.project.get_serie_scenario_path(id_scn_serie)
        directory_calculation = os.path.join(directory_scenario, "travail")
        directory_work = os.path.join(directory_scenario, "hydraulique")
        directory_hydrol = os.path.join(directory_scenario, "hydrol")
        directory_calculation_rel = os.path.relpath(directory_calculation, directory_work)

        self.init_folder(directory_scenario)
        self.init_folder(directory_calculation)
        self.init_folder(directory_work)
        self.init_folder(directory_hydrol)

        # Export kernel version file
        kernel_version_file = os.path.join(directory_scenario, "scenario.info")
        try:
            with open(kernel_version_file, "w") as f_kernel_version:
                 self.export_metadata(f_kernel_version)
        except Exception:
            self.log.error("Error writing kernel version file: {}".format(kernel_version_file))
            raise

        # Export settings files (Génération des fichiers de paramétrages - note NT1 §2)
        scenario_nom_file = os.path.join(directory_scenario, "scenario.nom")
        try:
            with open(scenario_nom_file, "w") as scenario_nom_stream:
                self.model_to_export = self.__exportsettings_serie_scen(id_scn_serie, name_scenario, name_ref_scenario, name_hydrol_scenario, scenario_nom_stream)
        except Exception:
            self.log.error("Error writing scenario file: {}".format(scenario_nom_file))
            raise

        # Export scenario files
        cmd_file = os.path.join(directory_calculation, name_scenario.upper()+".cmd")
        try:
            with open(cmd_file, "w") as cmd_stream:
                self.__exportsettings_gen_serie(id_scn_serie, cmd_stream)
        except Exception:
            self.log.error("Error writing command file: {}".format(cmd_file))
            raise

        # Export optsor file
        optsor_dat_file = os.path.join(directory_calculation, name_scenario.upper()+"_optsor.dat")
        optsor_dat_stream = open(optsor_dat_file, "w")
        optsor_dat_stream.write("!---------------------------------------------------\n")
        optsor_dat_stream.write("!  computing options from database \n")
        optsor_dat_stream.write("!---------------------------------------------------\n\n")
        try:
            # with open(optsor_dat_file, "w") as optsor_dat_stream:
            self.__exportsettings_optsor_serie(id_scn_serie, optsor_dat_stream)
        except Exception:
            self.log.error("Error writing optsor file: {}".format(optsor_dat_file))
            raise

        # Export project interlinks
        interlink_rac_file = os.path.join(directory_calculation, name_scenario.upper()+"_interlink.rac")
        if self.project.execute("""select exists (select 1 from project.interlink limit 1)""").fetchone()[0]:
            try:
                with open(interlink_rac_file, "w") as interlink_stream:
                    self.exportsettings_interlink(interlink_stream)
            except Exception:
                self.log.error("Error writing interlink file: {}".format(interlink_rac_file))
                raise

        # Export land use
        land_type_file = os.path.join(directory_calculation, name_scenario.upper()+"_land_use.dat")
        if self.project.execute("""select exists (select 1 from project.land_type limit 1)""").fetchone()[0]:
            try:
                with open(land_type_file, "w") as land_type_stream:
                    self.exportsettings_land_type(land_type_stream)
            except Exception:
                self.log.error("Error writing land_type file: {}".format(land_type_file))
                raise

        if comput_mode!='hydraulics':
            qts_file = os.path.join(directory_calculation, name_scenario.upper()+".qts")
            cts_file = os.path.join(directory_calculation, name_scenario.upper()+".cts")
            secteurs_file = os.path.join(directory_calculation, "secteurs.txt")
            try:
                # with open(qts_file, "w") as qts_stream, open(cts_file, "w") as cts_stream, open(secteurs_file, "w") as secteurs_stream:
                    # self.__exportsettings_dry_inflow_serie(id_scn_serie, qts_stream, cts_stream, secteurs_stream)
                self.__exportsettings_dry_inflow_serie(id_scn_serie, qts_file, cts_file, secteurs_file, optsor_dat_stream, directory_work)
            except Exception:
                self.log.error("Error writing dry inflow files: {}".format(', '.join([qts_file, cts_file, secteurs_file])))
                raise
            plu_file = os.path.join(directory_calculation, name_scenario.upper()+".plu")
            try:
                with open(plu_file, "w") as plu_stream:
                    self.__exportsettings_rainfall_library_serie(id_scn_serie, plu_stream, optsor_dat_stream, plu_file)
            except Exception:
                self.log.error("Error writing rain file: {}".format(plu_file))
                raise

        if transport_type=='pollution':
            stock_pollution_bv_file = os.path.join(directory_calculation, "stock_pollution.bv")
            try:
                with open(stock_pollution_bv_file, "w") as stock_pollution_bv_stream:
                    self.exportsettings_pollution_land_accumulation(stock_pollution_bv_stream)
            except Exception:
                self.log.error("Error writing stock pollution file: {}".format(stock_pollution_bv_file))
                raise

        optsor_dat_stream.close()
        
        if not name_ref_scenario:
            for name_model in self.model_to_export:
                initial_config = self.project.execute("""select configuration from {}.metadata""".format(name_model)).fetchone()[0]
                try:
                    configs_brut = self.project.execute("""select configuration from project.config_serie where serie={} and model='{}' order by priority desc;""".format(id_cs, name_model)).fetchall()
                    # Set scenario configurations
                    configs = [string for (string,) in configs_brut]
                    self.log.classic_notice('Configuration handling')
                    if initial_config != 1:
                        # Switch to default config
                        self.project.execute("""update {}.metadata set configuration=1""".format(name_model))
                        self.log.continuous_notice('Configuration changed to 1')
                    for config in configs:
                        # For each config, highest (1) priority last, unpack configured items that exists in this config
                        self.project.execute("""select {}.unpack_config({});""".format(name_model, config))
                        self.log.continuous_notice('Configuration {} applied'.format(config))
                    self.log.continuous_notice('Configuration switches successfull')

                    invalid = self.project.execute("""select name, subtype from {}.invalid;""".format(name_model)).fetchall()
                    if invalid:
                        self.log.classic_error("Found {} invalidities exporting scenario: {} / model: {}.".format(len(invalid), name_scenario, name_model))
                        raise ExportError("Invalidities in model {} for selected configurations in scenario {}.".format(name_model, name_scenario))

                    # Topo: update coverages
                    self.project.execute("""select {}.coverage_update()""".format(name_model))
                    # Topo: update branches
                    self.project.execute("""select {}.branch_update_fct()""".format(name_model))

                    # Export model files
                    self.exportrac(name_model, name_scenario, directory_calculation)
                    self.exportmodel(name_scenario, name_model, directory_calculation)
                    self.log.classic_notice("Model {} exported to {}".format(name_model, directory_scenario))

                    if initial_config!=1 or configs:
                        # Switch back to initial config
                        self.project.execute("""update {}.metadata set configuration={}""".format(name_model, initial_config))
                        self.log.classic_notice('Configuration changed back to {}'.format(initial_config))
                    self.project.commit()
                except Exception:
                    # Switch to default config
                    self.project.execute("""update {}.metadata set configuration=1""".format(name_model, ))
                    self.log.error("Error exporting scenario: {} / model: {}".format(name_scenario, name_model))
                    self.project.commit()
                    raise

    def exportsettings_gen_fexterne_serie(self, id_param_scen, nomtable, fexterne):
        """ called by exportsettings_gen """
        # fbloc              fexterne (column)   nomtable (name of table)
        # --------------     ----------------    -------------------------
        # *f_hy_externe      external_file       param_external_hydrograph
        # *f_regul           control_file        param_regulation
        # *f_mesures         measure_file        param_measure
        res = self.project.execute("""
            select param_bloc, %s
            from project.%s
            where param_bloc=%d
            """ % (fexterne,nomtable,id_param_scen)).fetchall()

        res_unpacked = [self.project.unpack_path(f[1]) for f in res]

        return res_unpacked

    def __exportsettings_serie_scen(self, id_scenario, name_scenario, name_ref_scenario, name_hydrol_scenario, stream1):

        data_exportsetting_scen = self.project.execute("""
            select comput_mode, output_option, model_connect_settings
            from project.serie_scenario
            where id={}
            """.format(id_scenario)).fetchone()

        groups = self.project.execute("""
            select id, name, ord
            from project.mixed_serie as mixed, project.grp as grp
            where mixed.grp=grp.id
            and mixed.serie=(select id_cs from project.serie_scenario where id = %i)
            order by mixed.ord"""%(id_scenario)).fetchall()

        lstmodel = self.write_exportsetting_scen(data_exportsetting_scen, name_scenario, name_ref_scenario, name_hydrol_scenario, groups, stream1)

        return (lstmodel)

    def __exportsettings_gen_serie(self, id_scenario, stream1):

        data_exportsettings_gen = self.project.execute("""
            select comput_mode, serie_scenario, dry_inflow, rainfall, soil_cr_alp,
            soil_horner_sat, soil_holtan_sat, soil_scs_sat,
            soil_hydra_psat, soil_hydra_rsat, soil_gr4_psat,
            soil_gr4_rsat, option_dim_hydrol_network, date0,
            tfin_hr, tini_hydrau_hr, dtmin_hydrau_hr, dtmax_hydrau_hr,
            dzmin_hydrau, dzmax_hydrau, flag_save, tsave_hr, flag_rstart,
            scenario_rstart_array, trstart_hr, flag_hydrology_rstart, scenario_hydrology_rstart,
            flag_hydrology_auto_dimensioning, graphic_control, model_connect_settings,
            tbegin_output_hr, tend_output_hr, dt_output_hr,
            output_option, transport_type, quality_type,
            dt_hydrol_mn, false as has_aeraulics
            from project.serie_scenario
            where id={} """.format(id_scenario)).fetchone()

        id_b, id_cs = self.project.execute("""
            select id_b, id_cs
            from project.serie_scenario
            where id={} """.format(id_scenario)).fetchone()

        comput_mode, serie_scenario, dry_inflow, rainfall, soil_cr_alp, \
            soil_horner_sat, soil_holtan_sat, soil_scs_sat, \
            soil_hydra_psat, soil_hydra_rsat, soil_gr4_psat, \
            soil_gr4_rsat, option_dim_hydrol_network, date0, \
            tfin_hr, tini_hydrau_hr, dtmin_hydrau_hr, dtmax_hydrau_hr, \
            dzmin_hydrau, dzmax_hydrau, flag_save, tsave_hr, flag_rstart, \
            scenario_rstart_array, trstart_hr, flag_hydrology_rstart, scenario_hydrology_rstart, \
            flag_hydrology_auto_dimensioning, graphic_control, model_connect_settings, \
            tbegin_output_hr, tend_output_hr, dt_output_hr, \
            output_option, transport_type, quality_type, \
            dt_hydrol_mn, has_aeraulics = data_exportsettings_gen

        res1, res2, res3, res4, res5 = [], [], [], [], []
        sediment_file = ""

        #--- BLOC RSTART
        if (flag_rstart):
            res1 = self.project.execute("""
                select id,name
                from project.serie_scenario
                where id_cs = {}
                and id_b = {}
                and index = {}
                """.format(scenario_rstart_array[0],scenario_rstart_array[1],scenario_rstart_array[2])).fetchall()

        #--- BLOC HYDROL RSTART
        if (flag_hydrology_rstart):
            res2 = self.project.execute("""
                select id,name
                from project.serie_scenario
                where id=%d
                """%(scenario_hydrology_rstart)).fetchall()

        #  4/12/2024/CHV : exports transférés dans _optsor.dat selon NT82
        res_ext = []
        # res_ext.append(self.exportsettings_gen_fexterne_serie(id_b,"param_external_hydrograph_bloc", "external_file"))
        # res_ext.append(self.exportsettings_gen_fexterne_serie(id_b,"param_regulation_bloc"         , "control_file" ))
        # res_ext.append(self.exportsettings_gen_fexterne_serie(id_b,"param_measure_bloc"            , "measure_file" ))

        #--- BLOC OPTION_POLLUTION
        if (transport_type=="pollution"):
            #                    0  1       2         3         4         5
            res4 = self.project.execute("""
                select id, id, iflag_mes, iflag_dbo5, iflag_dco, iflag_ntk
                from project.serie_scenario
                where id=%d
                """%(id_scenario)).fetchall()

        #--- BLOC OPTION_QUALITY1/2
        elif (transport_type=="quality"):
            #           0  1    2       3          4               5                       6
            res4 = self.project.execute("""
                select id, id, id, diffusion, dispersion_coef, longit_diffusion_coef, lateral_diff_coef
                from project.serie_scenario
                where id=%d
                """%(id_scenario)).fetchall()

            if (len(res2)>0):
                if (quality_type=="physico_chemical"):   # class=1 : fixed parameters: DBO5 NH4 NO3 O2
                    #           0       1                 2          3                4                   5             6            7        8
                    res5 = self.project.execute("""
                        select id, water_temperature_c, min_o2_mgl, kdbo5_j_minus_1, koxygen_j_minus_1, knr_j_minus_1, kn_j_minus_1, bdf_mgl, o2sat_mgl
                        from project.serie_scenario
                        where id=%d
                        """%(id_scenario)).fetchall()

                elif (quality_type=="bacteriological_quality"): # class=2
                    #           0       1         2         3         4             5                           6                            7                      8
                    res5 = self.project.execute("""
                        select id, iflag_ecoli, iflag_enti, iflag_ad1, iflag_ad2, ecoli_decay_rate_jminus_one, enti_decay_rate_jminus_one,ad1_decay_rate_jminus_one,ad2_decay_rate_jminus_one
                        from project.serie_scenario
                        where id=%d
                        """ % (id_scenario)).fetchall()

                elif (quality_type=="suspended_sediment_transport"): # class=3
                    #          0   1
                    res5 = self.project.execute("""
                        select id, suspended_sediment_fall_velocity_mhr
                        from project.serie_scenario
                        where id=%d
                        """ % (id_scenario)).fetchall()

        #--- BLOC OPTION_SEDIMENT
        elif (transport_type=="sediment"):
            sediment_file = self.project.execute("""
                select sediment_file
                from  project.serie_scenario
                where id=%d
                """%(id_scenario)).fetchone()

        assert(not has_aeraulics) # Not available for serie_scenario
        self.write_exportsetting_gen( data_exportsettings_gen, res1, res2, res3, res4, res5, sediment_file, res_ext, stream1)

    def __exportsettings_optsor_serie(self, id_scenario, stream_optsor):

        id_b, id_cs = self.project.execute("""
            select id_b, id_cs
            from project.serie_scenario
            where id={} """.format(id_scenario)).fetchone()

        data_exportsettings_optsor = self.project.execute("""
            select option_file, option_file_path, c_affin_param, t_affin_start_hr, t_affin_min_surf_ddomains,
            output_option, sor1_mode, dt_sor1_hr, strickler_param, domain_2d_param,
            flag_wind_option, air_density, skin_friction_coef, wind_data_file, rainfall,
            (select rainfall_type from project._rainfall where id=s.rainfall),
            kernel_version, False
            from project.serie_scenario as s
            where id={} limit 1;""".format(id_scenario)).fetchone()

        option_file, option_file_path, c_affin_param, t_affin_start_hr, t_affin_min_surf_ddomains, \
            output_option, sor1_mode, dt_sor1_hr, strickler_param, domain_2d_param, \
            flag_wind_option, air_density, skin_friction_coef, wind_data_file, rainfall, rainfall_type, \
            kernel_version, has_aeraulics = data_exportsettings_optsor

        c_affin_list = []
        strickler_list = []
        domain_2d_list = []
        output_list = []
        wind_gage_list = []
        rain_file = None

        if c_affin_param:
            # c affin
            c_affin_list = self.project.execute("""
                select model, type_domain, element, affin_option
                from project.c_affin_param_serie
                where serie={} order by model, type_domain, element;""".format(id_cs)).fetchall()

        if strickler_param:
            #strickl
            strickler_list = self.project.execute("""
                select model, element, rkmin, rkmaj, pk1, pk2
                from project.strickler_param_serie
                where serie={} order by model, element;""".format(id_cs)).fetchall()

        if domain_2d_param:
            #runoff_D
            domain_2d_list = self.project.execute("""
                select model, domain_2d
                from project.domain_2d_param_serie
                where serie={} order by model, domain_2d;""".format(id_cs)).fetchall()

        if output_option:
            #sor1
            output_list = self.project.execute("""
                select model, element
                from project.output_option_serie
                where serie={} order by model, element;""".format(id_cs)).fetchall()

        if flag_wind_option:
            #vent
            wind_gage_list = self.project.execute("""
                select name, ST_X(geom), ST_Y(geom)
                from project.wind_gage
                order by name;""").fetchall()

        if rainfall_type=='gage_file':
            rain_file, = self.project.execute("""
                select file
                from project.gage_file_rainfall
                where id={}
                """.format(rainfall)).fetchone()

        #--- BLOC F_HY - 4/12/2024/CHV : transféré dans _optsor.dat selon NT82
        res_ext = []
        res_ext.append(self.exportsettings_gen_fexterne_serie(id_b,"param_external_hydrograph_bloc", "external_file"))
        res_ext.append(self.exportsettings_gen_fexterne_serie(id_b,"param_regulation_bloc"         , "control_file" ))
        res_ext.append(self.exportsettings_gen_fexterne_serie(id_b,"param_measure_bloc"            , "measure_file" ))

        #--- BLOC DEROUTAGE_APPORT - 4/12/2024/CHV : transféré dans _optsor.dat selon NT82
        # Query the table "deroutage_apport"
        #                              0     1          2           3           4      5
        res3 = self.project.execute("""
            select model_from, hydrograph_from, model_to, hydrograph_to, option, parameter
            from project.inflow_rerouting_bloc
            where bloc={}
            """.format(id_scenario)).fetchall()

        dir_scen = self.project.get_senario_path(id_scenario)
        dir_travail = os.path.join(dir_scen, "travail")
        self.write_exportsettings_optsor(data_exportsettings_optsor, c_affin_list, strickler_list, domain_2d_list, output_list, wind_gage_list, rain_file, stream_optsor,   dir_travail, res3, res_ext, None)
        
    #2.4
    # 4/12/2024/CHV : modifié pour production bloc *F_QTS_NEW selon NT82, UNIQUEMENT s'il y a des données de temps sec
    def __exportsettings_dry_inflow_serie(self, id_scenario, qts_file, cts_file, sector_file, optsor_dat_stream, directory_work):

        id_dry_scenario, =   self.project.execute("""select dry_inflow from project.serie_scenario
           where id={}""".format(id_scenario)).fetchone()

        self.write_exportsettings_dry_inflow(id_dry_scenario, qts_file, cts_file, sector_file, optsor_dat_stream, directory_work)

    def __exportsettings_rainfall_library_serie(self, id_scenario, stream, stream_optsor, plu_file):

        id_rainfall, =  self.project.execute("""select rainfall from project.serie_scenario
            where id={};""".format(id_scenario)).fetchone()

        self.write_exportsetting_rainfall_library(id_rainfall, stream, stream_optsor, plu_file)


    # ------------------------------------------------------------------------------------
    # ------------------------------------------------------------------------------------


# ------------------------------------------------------------------------------------


# ------------------------------------------------------------------------------------
# script de test
# ------------------------------------------------------------------------------------
if __name__ == "__main__":
    import os
    import sys
    import getopt
    from hydra.project import Project
    from .export_carto_data import ExportCartoData

    try:
        optlist, args = getopt.getopt(sys.argv[1:],
                "htd",
                ["help", "timing", "debug"])
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        exit(1)

    optlist = dict(optlist)

    if "-h" in optlist or "--help" in optlist:
        help(sys.modules[__name__])
        exit(0)

    project = args[0]
    obj_project = Project.load_project(project)

    if len(args) == 2:
        try:
            idscn = int(args[-1])
        except ValueError:
            idscn, = obj_project.execute(f"select id from project.scenario where name='{args[1]}'").fetchone()
    else:
        idscn = obj_project.add_new_scenario()
        obj_project.commit()



    timing = "-t" in optlist or "--timing" in optlist
    debug = "-d" in optlist or "--debug" in optlist

    exporter = ExportCalcul(obj_project)
    exporter.export(int(idscn))

    exporter = ExportCartoData(obj_project)
    exporter.export(int(idscn))
