# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QTableWidgetItem
from hydra.gui.widgets.graph_widget import GraphWidget
from hydra.gui.widgets.array_widget import ArrayWidget
from hydra.gui.simple_singularity_dialog import SimpleSingularityDialog, tr
import hydra.utility.string as string

pollution_quality_dict = {'pollution':['mes_mgl', 'dbo5_mgl', 'dco_mgl', 'ntk_mgl'],
                          'quality'  :['dbo5_mgl', 'nh4_mgl', 'no3_mgl', 'o2_mgl', 'ecoli_npp100ml', 'enti_npp100ml', 'ad3_npp100ml', 'ad4_npp100ml', 'mes_mgl']}

class HydrographBcEditor(SimpleSingularityDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleSingularityDialog.__init__(self, project, geom, "hydrograph_bc_singularity", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "hydrograph_bc.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        name, storage_area, tq_array, external_file_data, constant_dry_flow, sector, pollution_dryweather_runoff, quality_dryweather_runoff, comment = self.project.execute("""
            select name, storage_area, tq_array, external_file_data, constant_dry_flow, sector, pollution_dryweather_runoff, quality_dryweather_runoff, comment
            from {}.hydrograph_bc_singularity
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.name.setText(name)
        self.constant_dry_flow.setText(string.get_str(constant_dry_flow))
        self.area.setText(string.get_str(storage_area))

        if not tq_array:
            tq_array = [[0, 0]]

        self.graph = GraphWidget(self.graph_placeholder, extend_zone = True)
        self.array = ArrayWidget(["Time (h)", "Discharge (m3/s)"], [10,2], tq_array,
                draw_graph_func=self.__draw_graph, help_func=None, parent=self.array_placeholder, not_nullable=True)

        combo_value = 'Yes' if external_file_data else 'No'
        self.combo_external_file_data.setCurrentIndex(self.combo_external_file_data.findText(combo_value))
        self.combo_external_file_data.activated.connect(self.__external_data_changed)
        self.__external_data_changed()

        containing_sector = self.project.execute("""select id, name
            from project.dry_inflow_sector where
                st_contains(geom, '{}')""".format(self.geom)).fetchone()
        if not containing_sector:
            self.defined_by_sector.setEnabled(False)
            self.sector = None
            self.sector_name.setText(tr("Not in a sector"))
            self.defined_by_sector.setChecked(False)
        else:
            self.defined_by_sector.setEnabled(True)
            if sector is not None:
                self.defined_by_sector.setChecked(True)
            self.sector = containing_sector
            self.sector_name.setText(tr("In sector: ")+ self.sector[1])

        self.defined_by_sector.stateChanged.connect(self.__defined_by_sector_changed)
        self.__defined_by_sector_changed()

        # pollution and quality
        for type, params in pollution_quality_dict.items():
            data_array = locals()['_'.join([type, 'dryweather', 'runoff'])]
            if data_array and len(data_array)==2:
                for mode, i in {'dryweather':0, 'runoff':1}.items():
                    if data_array[i] and len(data_array[i])==len(params):
                        for j in range(len(params)):
                            getattr(self, '_'.join([type, mode, params[j]])).setText(string.get_str(data_array[i][j]))

        self.btn_quality.clicked.connect(self.__quality_clicked)
        self.quality_groupbox.setVisible(False)
        self.btn_pollution.clicked.connect(self.__pollution_clicked)
        self.pollution_groupbox.setVisible(False)
        self.notes.setText(comment)

    def __quality_clicked(self):
        self.quality_groupbox.setVisible(not self.quality_groupbox.isVisible())
        self.quality_groupbox.setMaximumHeight(16777215 if self.quality_groupbox.isVisible() else 0)

    def __pollution_clicked(self):
        self.pollution_groupbox.setVisible(not self.pollution_groupbox.isVisible())
        self.pollution_groupbox.setMaximumHeight(16777215 if self.pollution_groupbox.isVisible() else 0)

    def __defined_by_sector_changed(self):
        self.constant_dry_flow.setEnabled(not self.defined_by_sector.isChecked())
        self.dry_label.setEnabled(not self.defined_by_sector.isChecked())

    def __external_data_changed(self):
        self.array.setEnabled(True)
        if self.combo_external_file_data.currentText() == 'Yes':
            self.array.setEnabled(False)

    def __draw_graph(self, datas):
        if len(datas)>0:
            self.graph.clear()
            self.graph.add_filled_line(
                [r[0] for r in datas],
                [r[1] for r in datas],
                "b",
                self.name.text(),
                tr("T (h)"), tr("Q (m3/s)"))
        else:
            self.graph.canvas.setVisible(False)

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))

        bool = True if self.combo_external_file_data.currentText() == 'Yes' else False

        if self.defined_by_sector.isChecked():
            self.project.execute("""update {}.hydrograph_bc_singularity set sector={} where id={}""".format(self.model.name,self.sector[0], self.id))
        else:
            self.project.execute("""update {}.hydrograph_bc_singularity set sector=null where id={}""".format(self.model.name, self.id))

        # tq_array
        tq_array = self.array.get_table_items()

        # pollution, quality
        pollution_dryweather_runoff = []
        quality_dryweather_runoff = []
        for type, params in pollution_quality_dict.items():
            for mode in ['dryweather', 'runoff']:
                data_array = []
                for j in range(len(params)):
                    data_array.append(string.get_sql_forced_float(getattr(self, '_'.join([type, mode, params[j]])).text()))
                locals()['_'.join([type, 'dryweather', 'runoff'])].append(data_array)

        self.model.update_hy_singularity(self.id, self.geom, self.name.text(), string.get_sql_float(self.area.text()),
            tq_array,constant_dry_flow=self.constant_dry_flow.text(), external_file_data=bool,
            pollution_dryweather_runoff=string.list_to_sql_array(pollution_dryweather_runoff),
            quality_dryweather_runoff=string.list_to_sql_array(quality_dryweather_runoff), comment=self.notes.text())

        SimpleSingularityDialog.save(self)
        self.close()
