from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterString,
    QgsProcessingParameterNumber,
    QgsProcessingException,
    QgsMeshLayer,
    QgsProcessingParameterVectorDestination,
    QgsProcessing,
    QgsProject
)
from qgis import processing
from ..project import Project
from .utility import ProjectWidget, ScenarioWidget
from pathlib import Path
from collections import defaultdict
from osgeo import ogr, osr

from qgis.utils import iface

class OutToGpkg(QgsProcessingAlgorithm):
    """
    Converts hydra out file to geopackage format to show where computation slows down
    """

    PROJECT = "project"
    SCENARIO = "scenario"
    OUTPUT = "output"

    def tr(self, string):
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return OutToGpkg()

    def name(self):
        return "out_to_gpkg"

    def displayName(self):
        return self.tr("Slowdown")

    def group(self):
        return ""

    def groupId(self):
        return ""

    def shortHelpString(self):
        return self.tr("Creates gpkg analysis file from hydra output to show nodes limiting the timestep.")

    def initAlgorithm(self, config=None):

        param = QgsProcessingParameterString(self.PROJECT, "Project")
        param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.SCENARIO, "Scenario")
        param.setMetadata({ 'widget_wrapper': {'class': ScenarioWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterVectorDestination(self.OUTPUT, self.tr("slowdown"), QgsProcessing.TypeVectorPoint)
        self.addParameter(param)


    def processAlgorithm(self, parameters, context, feedback):

        project = Project(self.parameterAsString(parameters, self.PROJECT, context))
        scenario = self.parameterAsString(parameters, self.SCENARIO, context)
        self.output = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)

        scn_ref, = project.fetchone(f"""
            select coalesce(sr.name, s.name) 
            from project.scenario s
            left join project.scenario sr on sr.id=s.scenario_ref
            where s.name=%s
            """, (scenario,))
        print(scn_ref)

        nodes = {}
        reaches = defaultdict(list)
        cas = {}
        mdlm = {}
        hyds = list((Path(project.directory) / scn_ref.upper() / 'hydraulique').glob('*.hyd'))
        for if_, f in enumerate(hyds):
            feedback.setProgress(100*if_/len(hyds))
            feedback.pushInfo(str(f))
            with open(f) as hyd:
                model = f.stem.replace(scn_ref.upper()+'_', '')
                model_, = project.fetchone(f"select name from project.model where name ilike %s", (model,))

                for i, l in enumerate(hyd):
                    l = l.strip().rstrip('\x00')
                    if l.startswith('$  liste des noeuds - branche de collecteur'):
                        reach = next(hyd).strip().split()[-1].replace("'", "")
                        for l in hyd:
                            l = l.strip().rstrip('\x00')
                            if l == '':
                                break
                            arr = l.split()
                            reaches[(model, reach)].append({'node':int(arr[0]), 'pk': float(arr[1])})

                    if l.startswith('$ Element CAS'):
                        nb_cas = int(next(hyd).strip().split()[1])
                        for c in range(nb_cas):
                            arr = next(hyd).strip().split()
                            nodes[(model, int(arr[1]))] = project.fetchone(f"""
                                select  st_x(geom), st_y(geom) from {model_}.storage_node where name ilike %s
                                """, (arr[2].replace("'", ""),))
                            assert nodes[(model, int(arr[1]))]
                            nbp = int(next(hyd).strip().split()[-1])
                            for p in range(nbp):
                                next(hyd)

                    if l.startswith('$ Element PAV'):
                        nb_pav = int(next(hyd).strip().split()[1])
                        for p in range(nb_pav):
                            arr = next(hyd).strip().split()
                            nodes[(model, int(arr[1]))] = tuple((float(v) for v in next(hyd).strip().split()[4:6]))

                    if l.startswith('$ Element CAR'):
                        nb_car = int(next(hyd).strip().split()[1])
                        for p in range(nb_car):
                            arr = next(hyd).strip().split()
                            nodes[(model, int(arr[1]))] = tuple((float(v) for v in next(hyd).strip().split()[3:5]))

                    if l.startswith('$ Liaison LCLNOD'):
                        nb_li = int(next(hyd).strip().split()[1])
                        for p in range(nb_li):
                            arr = next(hyd).strip().split()
                            nodes[(model, int(arr[1]))] = tuple((float(v) for v in next(hyd).strip().split()[2:4]))
                    
                    if l.startswith('$ Element NDSG'):
                        nb_sg = int(next(hyd).strip().split()[1])
                        for c in range(nb_sg):
                            arr = next(hyd).strip().split()
                            nodes[(model, int(arr[1]))] = project.fetchone(f"""
                                select  st_x(geom), st_y(geom) from {model_}.station_node where name ilike %s
                                """, (arr[2].replace("'", ""),))
                            assert nodes[(model, int(arr[1]))]
                            next(hyd)


        for model, reach in reaches.keys():
            model_, = project.fetchone(f"select name from project.model where name ilike %s", (model,))
            for n, x, y in project.fetchall(f"""
                    select p.node, st_x(v.geom), st_y(v.geom)
                    from {model_}.reach r
                    cross join (values {','.join([f'({v["node"]}, {v["pk"]}::float)' for v in reaches[(model, reach)]])}) p(node, pk)
                    cross join lateral (
                        select st_lineinterpolatepoint(r.geom, least(1., greatest(0., 1000*(p.pk -r.pk0_km)/st_length(r.geom)))) as geom) v
                    where r.name ilike %s
                    """, (reach,)):
                nodes[(model, n)] = (x, y)
            for n, x, y in project.fetchall(f"""
                    select p.node, st_x(v.geom), st_y(v.geom)
                    from {model_}.branch r
                    cross join (values {','.join([f'({v["node"]}, {v["pk"]}::float)' for v in reaches[(model, reach)]])}) p(node, pk)
                    cross join lateral (
                        select st_lineinterpolatepoint(r.geom, least(1., greatest(0., 1000*(p.pk -r.pk0_km)/st_length(r.geom)))) as geom) v
                    where r.name ilike %s
                    """, (reach,)):
                nodes[(model, n)] = (x, y)

        time_steps = defaultdict(list)
        total_occurences = 0
        outs = list((Path(project.directory) / scenario.upper() / 'hydraulique').glob('*.out')) 
        for if_, f in enumerate(outs):
            feedback.setProgress(100*if_/len(outs))
            feedback.pushInfo(str(f))
            with open(f) as out:
                for l in out:
                    l = l.strip().rstrip('\x00')
                    if l.startswith('hydraul time_hr:'):
                        arr = l.replace(':', ' ').split()
                        assert arr[7] == 'nod'
                        assert arr[3] == 'dt_sec'
                        time_steps[(arr[5], int(arr[8]))].append(float(arr[4]))
                        total_occurences += 1

        osr.UseExceptions()
        drv = ogr.GetDriverByName("GPKG")
        if Path(self.output).exists():
           drv.DeleteDataSource(self.output)
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(project.srid)
        ds = drv.CreateDataSource(self.output) #file creation


        slowdown_layer = ds.CreateLayer('slowdown', srs, ogr.wkbPoint)
        slowdown_layer.CreateField(ogr.FieldDefn("fid", ogr.OFTInteger))
        slowdown_layer.CreateField(ogr.FieldDefn("nod", ogr.OFTInteger))
        #f = ogr.FieldDefn("name", ogr.OFTString)
        #f.SetWidth(24)
        #slowdown_layer.CreateField(f)
        f = ogr.FieldDefn("model", ogr.OFTString)
        f.SetWidth(24)
        slowdown_layer.CreateField(f)
        slowdown_layer.CreateField(ogr.FieldDefn("penalty", ogr.OFTReal))
        slowdown_layer.CreateField(ogr.FieldDefn("occurences", ogr.OFTInteger))
        slowdown_layer.CreateField(ogr.FieldDefn("percent_occurences", ogr.OFTReal))

        i = 0
        for (model, nod), ts in time_steps.items():
            occurences = len(ts)
            penalty = sum([1/dt for dt in ts])
            feature = ogr.Feature(slowdown_layer.GetLayerDefn())
            i += 1
            feature.SetField("fid", i)
            feature.SetField("nod", nod)
            feature.SetField("model", model)
            feature.SetField("penalty", penalty)
            feature.SetField("occurences", occurences)
            feature.SetField("percent_occurences", 100*occurences/total_occurences)
            geom = ogr.Geometry(ogr.wkbPoint)
            x, y = nodes[(model, nod)]
            geom.AddPoint(x, y)
            feature.SetGeometry(geom)
            slowdown_layer.CreateFeature(feature)
            feature.Destroy()

        # layer_style
        layer_styles = ds.CreateLayer('layer_styles', None, ogr.wkbNone, options=['FID=id'])
        layer_styles.CreateField(ogr.FieldDefn("f_table_catalog", ogr.OFTString))
        layer_styles.CreateField(ogr.FieldDefn("f_table_schema", ogr.OFTString))
        layer_styles.CreateField(ogr.FieldDefn("f_table_name", ogr.OFTString))
        layer_styles.CreateField(ogr.FieldDefn("f_geometry_column", ogr.OFTString))
        layer_styles.CreateField(ogr.FieldDefn("styleName", ogr.OFTString))
        layer_styles.CreateField(ogr.FieldDefn("styleQML", ogr.OFTString))
        layer_styles.CreateField(ogr.FieldDefn("styleSLD", ogr.OFTString))
        layer_styles.CreateField(ogr.FieldDefn("useAsDefault", ogr.OFTInteger))
        layer_styles.CreateField(ogr.FieldDefn("description", ogr.OFTString))
        layer_styles.CreateField(ogr.FieldDefn("owner", ogr.OFTString))
        layer_styles.CreateField(ogr.FieldDefn("ui", ogr.OFTString))
        layer_styles.CreateField(ogr.FieldDefn("update_time", ogr.OFTDateTime))

        with open(Path(__file__).parent / 'slowdown.qml') as qml:
            ds.ExecuteSQL(f"""
                insert into layer_styles(f_table_schema, f_table_name, f_geometry_column, styleName, useAsDefault, styleQML)
                values ('', 'slowdown', 'geom', 'slowdown', 1, '{qml.read().replace("'","''")}')
                """)

        return {self.OUTPUT: self.output}


    def __del__(self):
        for l in QgsProject.instance().mapLayersByName(self.tr("slowdown")):
            l.loadNamedStyle('slowdown')
