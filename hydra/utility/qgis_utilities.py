# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
QGisUIManager manage iface interactions with gqis
"""

import os
import re
import json
from qgis.core import (QgsProject, QgsVectorLayer,
    QgsLayerTreeGroup, QgsRasterLayer, QgsColorRampShader,
    QgsLayerTreeLayer, QgsVectorLayerJoinInfo, Qgis, QgsWkbTypes,
    QgsMapLayerType, QgsDataSourceUri, QgsSnappingConfig, QgsTolerance)
from qgis.PyQt.QtCore import QCoreApplication, Qt, QSettings
from qgis.PyQt.QtWidgets import QProgressBar, QWidget
from hydra.utility.tables_properties import TablesProperties
from hydra.utility.settings_properties import SettingsProperties
from hydra.utility.surface_result_layer import SurfaceResultLayer

_plugin_dir = os.path.dirname(os.path.dirname(__file__))
_qml_dir = os.path.join(_plugin_dir, "ressources", "qml")
_utility_dir = os.path.join(_plugin_dir, "utility")

with open(os.path.join(os.path.dirname(__file__), "csv_data.json")) as csv_file:
    __csv_keys__ = json.load(csv_file)


def tr(msg):
    return QCoreApplication.translate("Hydra", msg)


class QGisProgressDisplay(object):
    """QProgressBar wrapper to provide minimal interface"""
    def __init__(self):
        self.__widget = QProgressBar()
        self.__widget.setMaximum(100)
        self.__widget.setAlignment(Qt.AlignLeft|Qt.AlignVCenter)

    def widget(self):
        return self.__widget

    def set_ratio(self, ratio):
        self.__widget.setValue(int(ratio*100))

    def __del__(self):
        del self.__widget

class QGisLogger(object):
    def __init__(self, iface):
        self.__iface = iface

    def error(self, title, message):
        self.__iface.messageBar().pushCritical(title, message.replace('\n', ' '))

    def warning(self, title, message):
        self.__iface.messageBar().pushWarning(title, message.replace('\n', ' '))

    def notice(self, title, message):
        # self.__iface.messageBar().pushInfo(title, message.replace('\n', ' '), duration=3)
        pass

    def progress(self, title, message):
        progressMessageBar = self.__iface.messageBar().createMessage(message)
        progress = QGisProgressDisplay()
        progressMessageBar.layout().addWidget(progress.widget())
        self.__iface.messageBar().pushWidget(progressMessageBar, Qgis.Info)
        return progress

    def clear_progress(self):
        self.__iface.messageBar().clearWidgets()


class QGisUIManager(object):
    def __init__(self, iface):
        self.__iface = iface
        self.__w15_layer_id = None

    def new_project(self):
        self.__iface.newProject()

    def save_project(self, project_filename):
        project = QgsProject.instance()
        project.write(project_filename)

    def open_project(self, project_filename):
        self.__w15_layer_id = None
        self.__iface.newProject()
        project = QgsProject.instance()
        project.read(project_filename)

        # adds connection to database if not already existing
        hydra_dbname = project.baseName()
        settings = QSettings()
        # check if connection settings for the opened project are set
        if settings.value(f'/PostgreSQL/connections/{hydra_dbname}/database') != hydra_dbname:
            # if not set, writes them
            settings.setValue(f'/PostgreSQL/connections/{hydra_dbname}/database', hydra_dbname)
            settings.setValue(f'/PostgreSQL/connections/{hydra_dbname}/service', SettingsProperties.get_service())
            # refresh QGIS data browser to display the project's DB
            self.__iface.mainWindow().findChildren(QWidget, 'Browser')[0].refresh()
            self.__iface.mainWindow().findChildren(QWidget, 'Browser2')[0].refresh()

    def reset_snapping_config(self):
        tolerance = 12
        snap_config = QgsSnappingConfig()
        snap_config.setEnabled(False)
        snap_config.setType(QgsSnappingConfig.Vertex)
        snap_config.setUnits(QgsTolerance.Pixels)
        snap_config.setTolerance(tolerance)
        snap_config.setIntersectionSnapping(False)
        snap_config.setMode(QgsSnappingConfig.SnappingMode.AdvancedConfiguration)

        for layer in QgsProject.instance().mapLayers().values():
            if layer.type() == QgsMapLayerType.VectorLayer :
                table = QgsDataSourceUri(layer.dataProvider().dataSourceUri()).table()
                snap = table.endswith('_node') or table in ('reach', 'constrain', 'catchment')
                lyr_settings = QgsSnappingConfig.IndividualLayerSettings(snap, QgsSnappingConfig.SnappingType.Vertex, tolerance, QgsTolerance.Pixels)
                snap_config.setIndividualLayerSettings(layer, lyr_settings)
        QgsProject.instance().setSnappingConfig(snap_config)


    def add_vector_layers(self, project_name, model_name, json_file, srid):
        uri = QgsDataSourceUri()
        uri.setConnection(SettingsProperties.get_service(), project_name, '', '')
        uri.setSrid(str(srid))
        __currendir = os.path.dirname(__file__)
        with open(os.path.join(__currendir, json_file)) as j:
            layer_map = json.load(j)
        properties = TablesProperties.get_properties()
        self.__layermap_process(layer_map['objects'], properties, uri, model_name, self.__add_layer_group(model_name, None))
        self.reset_snapping_config()
        QgsProject.instance().write(QgsProject.instance().fileName())

    def add_terrain_layers(self, project_name, files):
        root = QgsProject.instance().layerTreeRoot()
        terrain_group = root.addGroup(tr('terrain'))
        for terrain in files:
            raster = QgsRasterLayer(terrain, re.sub(r".*/", "", re.sub(r".*\\", "" , terrain)))
            raster.loadNamedStyle(os.path.join(_qml_dir, 'hillshade.qml'))
            QgsProject.instance().addMapLayer(raster, False)
            terrain_group.addLayer(raster)
        QgsProject.instance().write(QgsProject.instance().fileName())

    def add_rain_layers(self, vrt_rains):
        root = QgsProject.instance().layerTreeRoot()
        rain_group = root.addGroup(tr('rain'))
        for rain in vrt_rains:
            # Charge un style de pluie radar par défaut
            raster = QgsRasterLayer(rain.file, re.sub(r".*/", "", re.sub(r".*\\", "" , rain.file)))
            raster.loadNamedStyle(os.path.join(_qml_dir, 'radar_rain_5min_01_vrt.qml'))
            # Paramètres de la pluie (lame d'eau)
            dt_0 = 300.
            cc_0 = .01
            #cc, = self.execute("""
            #    SELECT unit_correction_coef FROM project.radar_rainfall WHERE name ='{}'
            #    """.format(vrt_name)).fetchone()
            dt = rain.dt
            cc = rain.correction_coef
            # Adapte le style au paramètres de la pluie sélectionnée
            renderer = raster.renderer()
            shader = renderer.shader()
            color_ramp_shader = shader.rasterShaderFunction()
            items = color_ramp_shader.colorRampItemList()
            new_items = []
            for item in items:
                new_value = item.value * (cc_0 / cc) * (dt / dt_0)
                new_items.append(QgsColorRampShader.ColorRampItem(new_value, item.color, item.label))
            max_val = new_value # dernière valeur de liste en principe
            color_ramp_shader.setColorRampItemList(new_items)
            color_ramp_shader.setMaximumValue(max_val)
            renderer.setClassificationMax(max_val)
            legend_settings = color_ramp_shader.legendSettings()
            legend_settings.setUseContinuousLegend(False)
            color_ramp_shader.setLegendSettings(legend_settings)

            QgsProject.instance().addMapLayer(raster, False)
            rain_group.addLayer(raster)

            if self.__iface:
                self.__iface.layerTreeView().refreshLayerSymbology(raster.id())

    QgsProject.instance().write(QgsProject.instance().fileName())

    def add_work_layer(self, table_name, layer_name, project_name, column_id, column_geom):
        root = QgsProject.instance().layerTreeRoot()
        if root.findGroup('work') :
            work_group = root.findGroup('work')
        else :
            work_group = root.addGroup('work')

        uri = QgsDataSourceUri()
        uri.setConnection(SettingsProperties.get_service(), project_name, '', '')
        uri.setDataSource('work', table_name, str(column_geom))

        layer = QgsVectorLayer(uri.uri(), tr(layer_name), "postgres")

        QgsProject.instance().addMapLayer(layer, False)
        work_group.addLayer(layer)
        QgsProject.instance().write(QgsProject.instance().fileName())
        return layer.isValid()

    def add_result_layers(self, project_path, project_name, model_name, scn_name, layer_names):
        root = QgsProject.instance().layerTreeRoot()
        if scn_name and scn_name != '':
            if root.findGroup('res: '+scn_name):
                scn_group = root.findGroup('res: '+scn_name)
            else:
                scn_group = root.insertGroup(0, 'res: '+scn_name)

            if model_name and model_name != '':
                if scn_group.findGroup('res: '+model_name):
                    self.remove_group_layers('res: '+model_name, scn_group)
                model_group = scn_group.addGroup('res: '+model_name)

                uri = QgsDataSourceUri()
                uri.setConnection(SettingsProperties.get_service(), project_name, '', '')

                csv_group = model_group.insertGroup(0, tr('CSV Files'))
                csv_group.setExpanded(False)
                # Load corresponding layer
                for layer_name in layer_names:
                    if layer_name in __csv_keys__.keys():
                        file_uri = os.path.join(project_path, scn_name.upper(), __csv_keys__[layer_name]['folder'], (('_').join([scn_name.upper(), model_name.upper(), __csv_keys__[layer_name]['csv']])+'.csv'))
                        if os.path.isfile(file_uri):
                            # Add vector layer
                            uri.setDataSource(str(model_name), str(__csv_keys__[layer_name]['table']), str('geom'), "", str('id'))
                            uri.hasParam("checkPrimaryKeyUnicity") or uri.setParam("checkPrimaryKeyUnicity", "0")
                            target_layer = self.__add_layer(uri.uri(), "{}_{}".format(layer_name.title(), scn_name.upper()), os.path.join(_qml_dir, __csv_keys__[layer_name]['style']), model_group)
                            # Add CSV as layer
                            csv_uri = """file:///{}?delimiter=;""".format(file_uri)
                            csv = QgsVectorLayer(csv_uri, layer_name, 'delimitedtext')
                            QgsProject.instance().addMapLayer(csv, False)
                            csv_group.addLayer(csv)
                            # Join to table
                            if target_layer is not None:
                                joinObject = QgsVectorLayerJoinInfo()
                                joinObject.setJoinFieldName(__csv_keys__[layer_name]['join_name'])
                                joinObject.setTargetFieldName(__csv_keys__[layer_name]['target_name'])
                                joinObject.setJoinLayerId(csv.id())
                                joinObject.setUsingMemoryCache(True)
                                joinObject.setJoinLayer(csv)
                                target_layer.addJoin(joinObject)
                QgsProject.instance().write(QgsProject.instance().fileName())

    def remove_group_layers(self, group_name, root = QgsProject.instance().layerTreeRoot()):
        group = root.findGroup(group_name)
        if group is not None:
            for child in group.children():
                if isinstance(child, QgsLayerTreeLayer):
                    QgsProject.instance().removeMapLayer(child.layerId())
                else:
                    self.remove_group_layers(child.name(), group)
            root.removeChildNode(group)
        QgsProject.instance().write(QgsProject.instance().fileName())

    def add_w15_layer(self, project, w14_result, w15_result):
        layer = SurfaceResultLayer(project, w14_result, w15_result)
        self.__w15_layer_id = layer.id()
        QgsProject.instance().addMapLayer(layer, False)
        QgsProject.instance().layerTreeRoot().insertLayer(0, layer)
        return layer

    def remove_w15_layer(self, group=None):
        if self.__w15_layer_id is not None:
            QgsProject.instance().removeMapLayer(self.__w15_layer_id)
            self.__w15_layer_id = None
            self.__iface.mapCanvas().refresh()

    def set_model_layers_expanded(self, model_name, expanded=True, checked=True):
        root = QgsProject.instance().layerTreeRoot()
        for child in root.children():
            if isinstance(child, QgsLayerTreeGroup) and child.name() == model_name:
                child.setExpanded(expanded)
                transparency = 0 if expanded or model_name=='project' else 50
                self.__change_group_transparency(child, transparency)
                self.__change_group_visibility(child, checked)
        QgsProject.instance().write(QgsProject.instance().fileName())

    def __change_group_transparency(self, group, transparency):
        for child in group.children():
            if isinstance(child, QgsLayerTreeLayer):
                if child.layer().type() == 0: #QgsVectorLayer
                    child.layer().setLayerTransparency(int(transparency))
                else:
                    child.layer().renderer().setOpacity(transparency/100.0)
                child.layer().triggerRepaint()
            else:
                self.__change_group_transparency(child, transparency)

    def __change_group_visibility(self, group, visibility):
        for child in group.children():
            if isinstance(child, QgsLayerTreeLayer):
                if child.layer().type() == 0: #QgsVectorLayer
                    self.__iface.legendInterface().setLayerVisible(child.layer(), visibility)
                child.layer().triggerRepaint()
            else:
                self.__change_group_visibility(child, visibility)

    def layer_group_exists(self, group_name='project'):
        tree = QgsProject.instance().layerTreeRoot()
        for leaf in tree.children():
            if isinstance(leaf, QgsLayerTreeGroup) and leaf.name() == group_name:
                return True
        return False

    def __layermap_process(self, items, properties, uri, model_name, current_group):
        for object in items:
            if object['type']=="layer":
                uri.setDataSource(str(model_name), str(object['table']), str(properties[object['table']]['geom']), "", str(properties[object['table']]['key']))
                uri.hasParam("checkPrimaryKeyUnicity") or uri.setParam("checkPrimaryKeyUnicity", "0")
                if properties[object['table']]['type'] == "point":
                    # uri.setWkbType(1)
                    uri.setWkbType(QgsWkbTypes.Point)
                elif properties[object['table']]['type'] in ['segment', 'line']:
                    # uri.setWkbType(2)
                    uri.setWkbType(QgsWkbTypes.LineString)
                elif properties[object['table']]['type'] == 'pointz':
                    # uri.setWkbType(1001)
                    uri.setWkbType(QgsWkbTypes.PointZ)
                elif properties[object['table']]['type'] in ['segmentz', 'linez']:
                    # uri.setWkbType(1002)
                    uri.setWkbType(QgsWkbTypes.LineStringZ)
                elif properties[object['table']]['type'] == "polygon":
                    # uri.setWkbType(3)
                    uri.setWkbType(QgsWkbTypes.Polygon)
                elif properties[object['table']]['type'] == "polygonz":
                    uri.setWkbType(QgsWkbTypes.PolygonZ)
                else:
                    raise AttributeError("layer {} has unspecified geometry type".format(str(object['table'])))
                self.__add_layer(uri.uri(), tr(properties[object['table']]['name']), properties[object['table']]['style'],current_group)
            if object['type']=="group":
                self.__layermap_process(object['objects'], properties, uri, model_name, self.__add_layer_group(object['name'], current_group))

    def __add_layer_group(self, group_name, parent):
        if parent is None:
            root = QgsProject.instance().layerTreeRoot()
            group = root.addGroup(tr(group_name))
        else:
            group = QgsLayerTreeGroup(tr(group_name))
            parent.addChildNode(group)
        return group

    def __add_layer(self, path, layer_name, layer_style, group):
        ''' adds a layer to QGIS layer manager. Returns the layer added'''
        if group is None:
            layer = QgsVectorLayer(path, tr(layer_name), "postgres")
            if layer_style is not None:
                layer.loadNamedStyle(os.path.join(_qml_dir, layer_style))
            # if not layer.isValid():
                # raise RuntimeError("invalid postgres layer (uri: {})".format(path))
            QgsProject.instance().addMapLayer(layer)
            return layer
        else:
            layer = QgsVectorLayer(path, tr(layer_name), "postgres")
            if layer_style is not None:
                layer.loadNamedStyle(os.path.join(_qml_dir, layer_style))
            # if not layer.isValid():
                # raise RuntimeError("invalid postgres layer (uri: {})".format(path))
            QgsProject.instance().addMapLayer(layer, False)
            group.addLayer(layer)
            return layer
