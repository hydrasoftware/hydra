/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  Update version                                */
/* ********************************************** */

update hydra.metadata set version = '1.3';
;;

create or replace function project.model_instead_fct()
returns trigger
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_model
    for statement in create_model(TD['new']['name'], $srid, '$version'):
        plpy.execute(statement)
$$$$
;;

/* ********************************************** */
/*  Scenario REF                                  */
/* ********************************************** */

alter table project.scenario add column if not exists scenario_ref integer references project.scenario(id);
;;

/* ********************************************** */
/*              add a schema work                 */
/* ********************************************** */

create schema if not exists work;
;;

/* ********************************************** */
/*  Rerouting options                             */
/* ********************************************** */

--isolated--
alter type hydra_rerouting_option add value if not exists 'qlimit';
;;

--isolated--
alter type hydra_rerouting_option add value if not exists 'fraction';
;;

update hydra.rerouting_option set name='qlimit', description='Q limit (m3/s)' where id=2;
update hydra.rerouting_option set name='fraction', description='Fraction (between 0 et 1)' where id=1;
;;

drop table if exists project.inflow_rerouting;
;;

create table project.inflow_rerouting(
    id serial primary key,
    scenario integer not null references project.scenario(id) on delete cascade,
    model varchar(24) not null,
    hydrograph_from integer not null,
    hydrograph_to integer,
    option hydra_rerouting_option not null default 'fraction',
    parameter real not null default 0,
        unique(scenario, model, hydrograph_from, hydrograph_to)
)
;;

/* ********************************************** */
/*              Utilities                         */
/* ********************************************** */

create table if not exists project.extract(
    id serial primary key,
    ctrl_file varchar(256) not null default ''
)
;;

create table if not exists project.animeau(
    id serial primary key,
    param_file varchar(256) not null default ''
)
;;

create table if not exists project.crgeng(
    id serial primary key,
    ctrl_file varchar(256) not null default ''
)
;;

/* ********************************************** */
/*  Update plpy functions                         */
/* ********************************************** */

create or replace function project.altitude(x real, y real)
returns real
language plpython3u immutable
as
$$$$
    from $hydra import altitude
    import plpy
    workspace = plpy.execute("select workspace from hydra.metadata")[0]['workspace']
    sources = [res['source'].replace('\\','\\\\').replace('$workspace', workspace) for res in plpy.execute("select source from project.dem order by priority asc")]
    return altitude(sources, x, y)
$$$$
;;

create or replace function project.set_altitude(geom geometry)
returns geometry
language plpython3u immutable
as
$$$$
    from $hydra import set_altitude
    import plpy
    workspace = plpy.execute("select workspace from hydra.metadata")[0]['workspace']
    sources = [res['source'].replace('\\','\\\\').replace('$workspace', workspace) for res in plpy.execute("select source from project.dem order by priority asc")]
    return set_altitude(sources, geom, $srid)
$$$$
;;

create or replace function project._str_filling_curve(polygon geometry)
returns varchar
language plpython3u immutable
as
$$$$
    from $hydra import filling_curve
    import plpy
    workspace = plpy.execute("select workspace from hydra.metadata")[0]['workspace']
    sources = [res['source'].replace('\\','\\\\').replace('$workspace', workspace) for res in plpy.execute("select source from project.dem order by priority asc")]
    return filling_curve(sources, polygon)
$$$$
;;

create or replace function project.crest_line(line geometry)
returns geometry
language plpython3u immutable
as
$$$$
    from $hydra import crest_line
    import plpy
    workspace = plpy.execute("select workspace from hydra.metadata")[0]['workspace']
    sources = [res['source'].replace('\\','\\\\').replace('$workspace', workspace) for res in plpy.execute("select source from project.dem order by priority asc")]
    return crest_line(sources, line, $srid)
$$$$
;;

create or replace function project.discretize_line(geom geometry, elem_length float)
returns geometry
language plpython3u
as $$$$
    from $hydra import discretize_line
    return discretize_line(geom, elem_length)
$$$$
;;

/* ********************************************** */
/*            Configuration update                */
/* ********************************************** */

select nextval('project.config_scenario_id_seq');
alter table project.config_scenario add column if not exists priority integer not null default currval('project.config_scenario_id_seq');
;;

/* ********************************************** */
/*            Scenario date0 chnage               */
/* ********************************************** */

alter table only project.scenario alter column date0 set default '2000-01-01 00:00:00';
;;

