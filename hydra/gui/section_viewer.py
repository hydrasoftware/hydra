from builtins import str
from builtins import range
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QApplication, QWidget, QMessageBox, QGridLayout
from qgis.PyQt.QtGui import QImage
from qgis.PyQt.QtCore import QCoreApplication, pyqtSignal
from hydra.gui.widgets.graph_widget import GraphWidget
from hydra.gui.base_dialog import BaseDialog
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT, FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from shapely import wkt
from shapely.geometry import LineString, Point
from math import sqrt
import os
import io
import numpy

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

class ViewerManager (BaseDialog) :
    def __init__(self,project,id,updown):
        BaseDialog.__init__(self,project,parent=None)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "section_viewer.ui"), self)

        self.project = project
        self.cross_section_id_updown = str(id)+"_"+updown
        self.__topo_profile = None
        self.current_position = None

        self.chk_show_DEM.clicked.connect(self.refresh_checkbox)
        self.chk_show_points_xyz.clicked.connect(self.refresh_checkbox)
        self.chk_show_combined.clicked.connect(self.refresh_checkbox)
        self.chk_show_hydra_section.clicked.connect(self.refresh_checkbox)
        self.btn_next.clicked.connect(lambda: self.change_cross_section("next"))
        self.btn_previous.clicked.connect(lambda: self.change_cross_section("previous"))

        #chargement du dictionnaire du manager, contient tout les cross_sections ordonnées par pk croissant avec les infos utiles.
        #fonction de la classe model appelé avec en argument l'id de reach.

        self.reach, self.reach_name = self.project.execute("""select reach.id, reach.name
                                                      from {model}.reach where
                                                      reach.id = (
                                                        select reach
                                                        from {model}.river_node
                                                        where id = {node_id}
                                                        )""".format(model=self.project.get_current_model().name,
                                                        node_id=self.cross_section_id_updown.split("_")[0])).fetchall()[0]

        self.dico_reach_cross_section_valley = self.project.get_current_model().get_section_reach_valley(self.reach)

        self.current_position = list(self.dico_reach_cross_section_valley.keys()).index(self.cross_section_id_updown)

        #chargement du reach
        # self.reach = self.list_cross_section[0][6]

        #chargement de la cross_section actuelle
        self.transect = self.dico_reach_cross_section_valley[self.cross_section_id_updown]["transect"]
        self.water_level = self.dico_reach_cross_section_valley[self.cross_section_id_updown]["water_level"]
        self.valley_geom = self.dico_reach_cross_section_valley[self.cross_section_id_updown]["valley_geom_id"]

        #initialisation des graphs
        self.graph = GraphViewer(self.project, self.transect, self.valley_geom, self.reach, self.water_level, self.graph_placeholder, False, True)
        self.init_checkbox()
        self.route_pk_graph = GraphRoute(self.pk_route_placeholder)
        self.btn_copy_data.clicked.connect(self.graph.copy_data)

        #fonction pour initialiser les graphs et les labels.
        self.init_pk_route()
        self.make_graph_title()
        self.graph.show_graph(self.chk_show_points_xyz.isChecked(), self.chk_show_DEM.isChecked(), self.chk_show_hydra_section.isChecked(), self.chk_show_combined.isChecked())

    def refresh_checkbox(self) :
        self.graph.show_graph( self.chk_show_points_xyz.isChecked(), self.chk_show_DEM.isChecked(), self.chk_show_hydra_section.isChecked(), self.chk_show_combined.isChecked())

    def change_cross_section(self,direction) :
        if self.current_position != 0 and direction == "previous":
            self.current_position += -1
        if self.current_position != (len(self.dico_reach_cross_section_valley)-1) and direction == "next":
            self.current_position += 1
        self.cross_section_id_updown = list(self.dico_reach_cross_section_valley.items())[self.current_position][0]
        self.graph.transect_id = self.dico_reach_cross_section_valley[self.cross_section_id_updown]['transect']
        self.graph.valley_geom_id = self.dico_reach_cross_section_valley[self.cross_section_id_updown]['valley_geom_id']
        self.graph.water_level = self.dico_reach_cross_section_valley[self.cross_section_id_updown]['water_level']
        self.graph.load_graph(self.project.get_current_model().name)
        self.init_checkbox()
        if self.chk_show_points_xyz.isChecked() or self.chk_show_combined.isChecked() :
            self.label_distance.setText("Point distance : {} m".format(self.graph.t_distance_pt))
        else :
            self.label_distance.setText("")
        self.make_graph_title()
        self.init_pk_route()
        self.graph.show_graph(self.chk_show_points_xyz.isChecked(), self.chk_show_DEM.isChecked(), self.chk_show_hydra_section.isChecked(), self.chk_show_combined.isChecked())

    def init_checkbox(self):
        if self.graph.topo_profile != []:
            self.chk_show_DEM.setEnabled(True)
            self.chk_show_DEM.setChecked(True)
        else :
            self.chk_show_DEM.setEnabled(False)
        if self.graph.topo_final != []:
            self.chk_show_combined.setEnabled(True)
            self.chk_show_combined.setChecked(True)
        else :
            self.chk_show_combined.setEnabled(False)
        if self.graph.hydra_section != []:
            self.chk_show_hydra_section.setEnabled(True)
            self.chk_show_hydra_section.setChecked(True)
        else :
            self.chk_show_hydra_section.setEnabled(False)
        if self.graph.points_profile != []:
            self.chk_show_points_xyz.setEnabled(True)
            self.chk_show_points_xyz.setChecked(True)
        else :
            self.chk_show_points_xyz.setEnabled(False)

    def make_graph_title(self) :
        cross_section_name = self.dico_reach_cross_section_valley[str(self.cross_section_id_updown)]['name']
        pk = str(round(self.dico_reach_cross_section_valley[str(self.cross_section_id_updown)]['pk'],3))
        updown = self.dico_reach_cross_section_valley[str(self.cross_section_id_updown)]['updown']

        main_title = ' - '.join([self.project.name+' / '+self.project.get_current_model().name, self.reach_name, 'pk: '+pk, cross_section_name+' ('+updown+')'])

        self.graph.graph_title.set_text(main_title)

    def init_pk_route(self) :
        self.route_pk_graph.clear()
        self.pk_route = []
        for i in range(len(self.dico_reach_cross_section_valley)) :
            self.pk_route.append([list(self.dico_reach_cross_section_valley.items())[i][1]["pk"],0])

        self.route_pk_graph.add_route_line([pk[0] for pk in self.pk_route] , [pk[1] for pk in self.pk_route] , color='#0000ff', marker="|" ,mec='#e31a1c')
        self.route_pk_graph.set_zoomed_zone(0 - self.pk_route[-1][0]*0.05 , self.pk_route[-1][0]*1.05, -2, 2)
        self.route_pk_graph.set_tick_params('y', which='both', left=False, labelleft=False)

        self.marker_pk_route = [list(self.dico_reach_cross_section_valley.items())[self.current_position][1]["pk"],0]

        self.route_pk_graph.add_line(self.marker_pk_route[0], self.marker_pk_route[1], color=None, marker=[(-1,-8),(1,-8),(1,8),(-1,8)], markersize=32, mfc='#e31a1c', mec='#000000')

        data = [[item[1]['name'],item[1]['pk']] for item in list(self.dico_reach_cross_section_valley.items())]
        self.route_pk_graph.set_labels(data)

        self.route_pk_graph.canvas.draw()

class  GraphViewer(GraphWidget) :

    class NavigationToolbar(NavigationToolbar2QT):
        # only display the buttons we need
        toolitems = [t for t in NavigationToolbar2QT.toolitems if
                     t[0] in ('Pan', 'Zoom')]

    def __getattr__(self, att):
        if att =='axe':
            return self.__axe
        elif att =='topo_profile':
            return self.__topo_profile
        elif att =='topo_final':
            return self.__topo_final
        elif att =='hydra_section':
            return self.__hydra_section
        elif att =='points_profile':
            return self.__points_profile
        else:
            raise AttributeError

    def __init__(self, project, transect_id, valley_geom_id, reach, water_level=None, parent=None, axis_equal=False, extend_zone=False, silent_mode=False, dpi=75):
        GraphWidget.__init__(self, parent, axis_equal, extend_zone, dpi)

        self.mpl_toolbar = GraphViewer.NavigationToolbar(self.canvas, parent)
        self.project = project
        self.transect_id = transect_id
        self.valley_geom_id = valley_geom_id
        self.reach = reach
        self.water_level = water_level
        self.error_message = None
        self.t_distance_pt = 0
        self.silent_mode = silent_mode

        self.load_graph(self.project.get_current_model().name)

    def load_graph(self,model):
        self.__water_profile = []
        self.__hydra_section = []

        self.__topo_profile, transect_geom = self.load_topo_profile(model)

        self.__points_profile, start, end = self.load_points_profile(model, transect_geom)

        self.__topo_final = self.load_topo_final(self.__topo_profile, self.__points_profile, start, end)

        #load hydra section

        if self.valley_geom_id is not None:

            points_valley_geom = self.project.execute("""
            select zbmaj_lbank_array,
            zbmin_array,
            zbmaj_rbank_array,
            t_distance_pt
            from {model}.valley_cross_section_geometry
            where id = {valley_id}""".format(model=model,valley_id=self.valley_geom_id)).fetchall()

            if transect_geom:
                reach_position = self.project.execute("""select ST_LineLocatePoint(
                    (select geom from {model}.constrain
                        where id={transect_id})
                        ,
                    (select St_Intersection(
                        (select geom from {model}.reach
                        where id = {reach}),
                        (select geom from {model}.constrain
                        where id={transect_id}))
                        )
                )
                *St_Length((select geom from {model}.constrain
                where id ={transect_id}))""".format(model=model, transect_id=self.transect_id, reach=self.reach)).fetchone()[0]

            else:
                reach_position = self.project.execute("""select ST_LineLocatePoint(
                    (select geom from {model}.constrain
                        where id={transect_id})
                        ,
                    (select St_ClosestPoint(
                        (select geom from {model}.constrain
                        where id={transect_id}),
                        (select geom from {model}.reach
                        where id = {reach}))
                        )
                )
                *St_Length((select geom from {model}.constrain
                where id ={transect_id}))""".format(model=model, transect_id=self.transect_id, reach=self.reach)).fetchone()[0]

            self.t_distance_pt = points_valley_geom[0][3]

            profil = []

            for XZpoint in points_valley_geom[0][1] :
                profil.append([reach_position+XZpoint[1]/2,XZpoint[0]])
                profil.append([reach_position-XZpoint[1]/2,XZpoint[0]])

            river_bedlimit_left = reach_position-points_valley_geom[0][1][-1][1]/2
            river_bedlimit_right = reach_position+points_valley_geom[0][1][-1][1]/2

            for XZpoint in points_valley_geom[0][0] :
                profil.append([river_bedlimit_left-XZpoint[1],XZpoint[0]])

            for XZpoint in points_valley_geom[0][2] :
                profil.append([river_bedlimit_right+XZpoint[1],XZpoint[0]])

            profil.sort(key = lambda x: x[0])

            self.__hydra_section = profil

    def load_topo_profile(self, model) :
        inter = self.project.execute("""
                with transect as ( select geom from {model}.constrain where id={id}
                ),
                reach as (
                    select r.id, st_intersection(r.geom, transect.geom) as inter, r.geom
                    from {model}.reach as r, transect
                    where st_intersects(r.geom, transect.geom)
                ),
                topo_transect as (
                    select project.set_altitude(
                        project.discretize_line(transect.geom, st_length(transect.geom)/{discret})
                        ) as geom from transect
                )
                select r.id, r.inter, t.geom, transect.geom
                from reach as r, topo_transect as t, transect
                """.format(
                    id=self.transect_id,
                    model=model,
                    discret=str(150)
                    )).fetchall()

        if inter:
            # self.inter_reach = wkt.loads((self.project.execute("""select wkb_loads('{}')""".format(inter[0][1])).fetchone())[0])
            transect_geom = wkt.loads(self.project.execute("""select wkb_loads('{}')""".format(inter[0][3])).fetchone()[0])

            if len(transect_geom.coords) == 2:
                coords = transect_geom.coords
                new_coords_start, new_coords_end = self.__calc_new_start_end(coords[0], coords[1])
                geom = [new_coords_start]
                geom.extend(coords)
                geom.append(new_coords_end)
                transect_geom = LineString(geom)

            return (numpy.array([(transect_geom.project(Point(p)), p[2]) for p in wkt.loads((self.project.execute("""select wkb_loads('{}')""".format(inter[0][2])).fetchone())[0]).coords]), transect_geom)

        else:
            if not self.silent_mode:
                self.project.log.error("No profile found, check that your constrain line crosses a reach and that you have terrain data")
            return ([], [])

    def load_points_profile(self, model, transect_geom):
        if transect_geom != []:
            points_profile_geom = self.project.execute("""
                            select p.id,
                            p.z_ground as z,
                            st_linelocatepoint('SRID={srid};{transect}'::geometry, p.geom)*st_length('SRID={srid};{transect}'::geometry) as pk
                            from project.points_xyz as p
                            where st_intersects(ST_Buffer('SRID={srid};{transect}'::geometry, {distance}, 'endcap=flat join=round'),p.geom )=true
                            or st_startpoint('SRID={srid};{transect}'::geometry)=p.geom
                            or st_endpoint('SRID={srid};{transect}'::geometry)=p.geom
                            order by pk""".format(model=model, transect=transect_geom, distance=self.t_distance_pt, srid= self.project.srid)).fetchall()
        else:
            points_profile_geom = []

        if len(points_profile_geom)>1:
            points_profile = numpy.array([(p[2], p[1]) for p in points_profile_geom])
            start = points_profile[0][0]
            end = points_profile[-1][0]

            return (points_profile, start, end)
        return ([], 0, 0)

    def load_topo_final(self, topo_profile, points_profile, start, end) :
        if len(points_profile)> 1 :
            topo_final = [(p[0],p[1]) for p in topo_profile if p[0]<start]
            if len(topo_final)>1:
                topo_final=numpy.concatenate((topo_final, points_profile))
            else:
                topo_final=points_profile
            for p in topo_profile:
                if p[0]>end:
                    topo_final=numpy.concatenate((topo_final,[p]))

            return topo_final
        return []

    def copy_data(self):
        ''' copy the data for the current graph and put in clipboard'''
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard)
        data_to_export = ""
        data_to_export = data_to_export + tr("topo MNT Z(m)")
        data_to_export = data_to_export + '\t' + tr("topo MNT X(m)")
        data_to_export = data_to_export + '\t' + tr("topo terrain point Z(m)")
        data_to_export = data_to_export + '\t' + tr("topo terrain point X(m)")
        data_to_export = data_to_export + '\t' + tr("combined topo Z(m)")
        data_to_export = data_to_export + '\t' + tr("combined topo X(m)")
        data_to_export = data_to_export + '\t' + tr("Hydra section Z(m)")
        data_to_export = data_to_export + '\t' + tr("Hydra section X(m)")

        data_to_export = data_to_export +'\r\n'

        for i in range(0, max(len(self.__topo_profile),len(self.__points_profile),len(self.__topo_final),len(self.__hydra_section))):
            data_to_export = data_to_export + str(self.__topo_profile[i][1] if i<len(self.__topo_profile) else "-")
            data_to_export = data_to_export + '\t' + str(self.__topo_profile[i][0] if i<len(self.__topo_profile) else "-")
            data_to_export = data_to_export + '\t' + (str(self.__points_profile[i][1]) if i<len(self.__points_profile) else "-")
            data_to_export = data_to_export + '\t' + (str(self.__points_profile[i][0]) if i<len(self.__points_profile) else "-")
            data_to_export = data_to_export + '\t' + (str(self.__topo_final[i][1]) if i<len(self.__topo_final) else "-")
            data_to_export = data_to_export + '\t' + (str(self.__topo_final[i][0]) if i<len(self.__topo_final) else "-")
            data_to_export = data_to_export + '\t' + (str(self.__hydra_section[i][1]) if i<len(self.__hydra_section) else "-")
            data_to_export = data_to_export + '\t' + (str(self.__hydra_section[i][0]) if i<len(self.__hydra_section) else "-")
            data_to_export = data_to_export + '\t'
            data_to_export = data_to_export + '\r\n'
        cb.setText(data_to_export, mode=cb.Clipboard)

    def __calc_new_start_end(self, coordstart, coordend):
        inc_x = (coordend[0] - coordstart[0])/(coordend[1]-coordstart[1])
        inc_y = (coordend[1]-coordstart[1])/(coordend[0] - coordstart[0])
        dist = sqrt((coordend[0] - coordstart[0])**2 + (coordend[1]-coordstart[1])**2)
        inc_x = (coordend[0] - coordstart[0])/dist
        inc_y = (coordend[1]-coordstart[1])/dist

        new_coords_start = (coordstart[0]-inc_x, coordstart[1]-inc_y, coordstart[2])
        new_coords_end = (coordend[0]+inc_x, coordend[1]+inc_y, coordend[2])
        return new_coords_start, new_coords_end

    def add_water(self,topo,water) :
        x=[r[0] for r in topo]
        y1=[r[1] for r in topo]
        y2=[r[1] for r in water]
        where_array = []
        for i in range(len(y1)) :
            where_array.append(y1[i]<y2[i])
        self.add_fill_between(x,y1,y2,where = where_array ,interpolate = True, facecolor='lightblue')

    def show_graph(self,show_points_xyz,show_DEM,show_hydra_section,show_combined):
        ''' fonction to show the graph, call it to refresh the display'''
        self.clear()

        if self.__topo_profile != [] and show_DEM :
            self.add_line([r[0] for r in self.__topo_profile], [r[1] for r in self.__topo_profile], '#c3c3c3', y_title= tr("Z (m)"), x_title = tr("X (m)"))
        if self.__points_profile != [] and show_points_xyz :
            self.add_line([r[0] for r in self.__points_profile], [r[1] for r in self.__points_profile], '#C60800', y_title= tr("Z (m)"), x_title = tr("X (m)"))
        if self.__topo_final != [] and show_combined :
            self.add_line([r[0] for r in self.__topo_final], [r[1] for r in self.__topo_final], '#77B5FE', y_title= tr("Z (m)"), x_title = tr("X (m)"))
        if self.__hydra_section != [] and show_hydra_section :
            self.add_line([r[0] for r in self.__hydra_section], [r[1] for r in self.__hydra_section], '#32CD32', y_title= tr("Z (m)"), x_title = tr("X (m)"))

        if self.water_level is not None:
            self.__water_profile = []
            if show_hydra_section and self.__hydra_section != [] :
                for p in self.__hydra_section :
                    self.__water_profile.append([round(p[0],2),round(self.water_level,2)])
                self.add_water(self.__hydra_section,self.__water_profile)
            elif show_combined and self.__topo_final != [] :
                for p in self.__topo_final :
                    self.__water_profile.append([round(p[0],2),round(self.water_level,2)])
                self.add_water(self.__topo_final,self.__water_profile)
            elif show_DEM and self.__topo_profile != [] :
                for p in self.__topo_profile :
                    self.__water_profile.append([round(p[0],2),round(self.water_level,2)])
                self.add_water(self.__topo_profile,self.__water_profile)

        self.render()

    def render_image(self):
        ''' render the graph window as a Qimage, use for export section'''
        buf = io.BytesIO()
        self.fig.savefig(buf)
        image = QImage.fromData(buf.getvalue())
        return(image)


class  GraphRoute(QWidget) :
    clicked_pt = pyqtSignal(object)
    hover_pt = pyqtSignal(str)

    def __init__(self, parent=None, axis_equal=False, extend_zone=False):
        QWidget.__init__(self, parent)
        self.__dpi = 75
        self.__fig = Figure(dpi=self.__dpi)
        self.__axe = self.__fig.add_subplot(111)
        self.__axe.grid(False)
        self.__fig.patch.set_visible(False)
        self.__axe.axis("off")

        self.canvas = FigureCanvas(self.__fig)
        self.canvas.setParent(self)
        box = QGridLayout()
        box.addWidget(self.canvas)
        parent.setLayout(box)

        self.__start_x = None
        self.__start_y = None
        self.__start_xlim = None
        self.__start_ylim = None

        self.route_line = None

        self.__labels = []

        self.canvas.mpl_connect('motion_notify_event', self.__hover)
        self.canvas.mpl_connect('button_press_event', self.onclick)
        # self.labels_route = self.HoverLabels(self.canvas,self.__axe)

    def reset_labels(self):
        self.__labels = []

    def set_labels(self, labels):
        self.__labels = labels

    def present_label(self, ind, line) :
        if ind != -1 :
            x,y = line.get_data()
            text = "Name : {}, Pk : {} km".format([self.__labels[n][0] for n in ind["ind"]][0], [round(self.__labels[n][1],3) for n in ind["ind"]][0])
            self.hover_pt.emit(text)
        else :
            self.hover_pt.emit("")

    def add_line(self, x_data, ydata, color, main_title=None, x_title=None, y_title=None, marker="", markersize=6, mfc=None, mec=None):
        if x_title : self.__axe.set_xlabel(x_title)
        if y_title : self.__axe.set_ylabel(y_title)
        self.__axe.plot(x_data, ydata, linestyle="-", color=color, marker=marker, markersize=markersize, mfc=mfc, mec=mec)

        if main_title: self.graph_title.set_text(main_title)
        self.canvas.draw()

    def add_route_line(self, x_data, ydata, color, main_title=None, x_title=None, y_title=None, marker="", markersize=6, mfc=None, mec=None):
        if x_title: self.__axe.set_xlabel(x_title)
        if y_title: self.__axe.set_ylabel(y_title)
        self.route_line, = self.__axe.plot(x_data, ydata, linestyle="-", color=color, marker=marker, markersize=markersize, mfc=mfc, mec=mec)

        if main_title: self.graph_title.set_text(main_title)
        self.canvas.draw()

    def set_tick_params(self,axis,which='both',bottom=True,top=False,left=True,right=False,labelbottom=True,labelleft=True):
        self.__axe.tick_params(axis=axis, which=which, bottom=bottom, top=top, left=left, right=right, labelbottom=labelbottom, labelleft=labelleft)

    def set_zoomed_zone(self, x_min, x_max, y_min, y_max):
        self.__axe.set_xlim(x_min, x_max)
        self.__axe.set_ylim(y_min, y_max)
        self.canvas.draw()

    def clear(self):
        self.__axe.clear()
        self.canvas.draw()

    def onclick(self, event) :
        if self.route_line:
            line = self.route_line
            if event.inaxes == self.__axe and line is not None:
                cont, ind = line.contains(event)
                if cont :
                    x,y = line.get_data()
                    (round(x[ind["ind"][0]],2), y[ind["ind"][0]])
                    self.clicked_pt.emit(x[ind["ind"][0]])

    #NB : la mecanique du hover est en place mais l'annotation ne s'affiche pas à cause d'un bug localisé, a resoudre.
    # il est impossible d'annoter le graph pour une raison inconnue (canvas particulier, layout, je ne sais pas)
    # Resolution par ajout d'un signal qui permet d'update un label externe au graph (hover_pt)
    def __hover(self, event):
        # there is a pb with inaxes if 2 axes overlap
        if self.route_line:
            line = self.route_line

            if event.inaxes == self.__axe and line is not None:
                cont, ind = line.contains(event)
                if cont:
                    self.present_label(ind,line)
                else:
                    self.present_label(-1,line)