<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="100000000" styleCategories="AllStyleCategories" simplifyDrawingTol="1" symbologyReferenceScale="-1" version="3.22.7-Białowieża" maxScale="0" simplifyMaxScale="1" labelsEnabled="0" simplifyAlgorithm="0" simplifyLocal="1" simplifyDrawingHints="1" hasScaleBasedVisibilityFlag="0" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal durationField="" durationUnit="min" accumulate="0" startField="" limitMode="0" mode="0" startExpression="" fixedDuration="0" endField="" enabled="0" endExpression="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 forceraster="0" symbollevels="0" referencescale="-1" enableorderby="0" type="RuleRenderer">
    <rules key="{f9be31c7-d395-4485-b920-9119f0a716d3}">
      <rule symbol="0" key="{9c5b6a14-56cf-465f-92a7-9a642f375f3a}" label="Conduite"/>
      <rule scalemaxdenom="15000" scalemindenom="10" symbol="1" key="{41839918-70ca-4231-8ff8-b6bcca55a393}" label="Sens"/>
    </rules>
    <symbols>
      <symbol force_rhr="0" clip_to_extent="1" alpha="1" name="0" type="line">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" class="SimpleLine" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" name="align_dash_pattern" type="QString"/>
            <Option value="square" name="capstyle" type="QString"/>
            <Option value="5;2" name="customdash" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale" type="QString"/>
            <Option value="MM" name="customdash_unit" type="QString"/>
            <Option value="0" name="dash_pattern_offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="dash_pattern_offset_unit" type="QString"/>
            <Option value="0" name="draw_inside_polygon" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="0,0,0,255" name="line_color" type="QString"/>
            <Option value="solid" name="line_style" type="QString"/>
            <Option value="0.39" name="line_width" type="QString"/>
            <Option value="MM" name="line_width_unit" type="QString"/>
            <Option value="0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="0" name="ring_filter" type="QString"/>
            <Option value="0" name="trim_distance_end" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale" type="QString"/>
            <Option value="MM" name="trim_distance_end_unit" type="QString"/>
            <Option value="0" name="trim_distance_start" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale" type="QString"/>
            <Option value="MM" name="trim_distance_start_unit" type="QString"/>
            <Option value="0" name="tweak_dash_pattern_on_corners" type="QString"/>
            <Option value="0" name="use_custom_dash" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="width_map_unit_scale" type="QString"/>
          </Option>
          <prop v="0" k="align_dash_pattern"/>
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="dash_pattern_offset"/>
          <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
          <prop v="MM" k="dash_pattern_offset_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0,0,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.39" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="trim_distance_end"/>
          <prop v="3x:0,0,0,0,0,0" k="trim_distance_end_map_unit_scale"/>
          <prop v="MM" k="trim_distance_end_unit"/>
          <prop v="0" k="trim_distance_start"/>
          <prop v="3x:0,0,0,0,0,0" k="trim_distance_start_map_unit_scale"/>
          <prop v="MM" k="trim_distance_start_unit"/>
          <prop v="0" k="tweak_dash_pattern_on_corners"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" alpha="1" name="1" type="line">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" class="MarkerLine" enabled="1" locked="0">
          <Option type="Map">
            <Option value="4" name="average_angle_length" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="average_angle_map_unit_scale" type="QString"/>
            <Option value="MM" name="average_angle_unit" type="QString"/>
            <Option value="3" name="interval" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="interval_map_unit_scale" type="QString"/>
            <Option value="MM" name="interval_unit" type="QString"/>
            <Option value="0" name="offset" type="QString"/>
            <Option value="0" name="offset_along_line" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_along_line_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_along_line_unit" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="centralpoint" name="placement" type="QString"/>
            <Option value="0" name="ring_filter" type="QString"/>
            <Option value="1" name="rotate" type="QString"/>
          </Option>
          <prop v="4" k="average_angle_length"/>
          <prop v="3x:0,0,0,0,0,0" k="average_angle_map_unit_scale"/>
          <prop v="MM" k="average_angle_unit"/>
          <prop v="3" k="interval"/>
          <prop v="3x:0,0,0,0,0,0" k="interval_map_unit_scale"/>
          <prop v="MM" k="interval_unit"/>
          <prop v="0" k="offset"/>
          <prop v="0" k="offset_along_line"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_along_line_map_unit_scale"/>
          <prop v="MM" k="offset_along_line_unit"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="centralpoint" k="placement"/>
          <prop v="0" k="ring_filter"/>
          <prop v="1" k="rotate"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" clip_to_extent="1" alpha="1" name="@1@0" type="marker">
            <data_defined_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </data_defined_properties>
            <layer pass="0" class="SimpleMarker" enabled="1" locked="0">
              <Option type="Map">
                <Option value="0" name="angle" type="QString"/>
                <Option value="square" name="cap_style" type="QString"/>
                <Option value="255,0,0,255" name="color" type="QString"/>
                <Option value="1" name="horizontal_anchor_point" type="QString"/>
                <Option value="bevel" name="joinstyle" type="QString"/>
                <Option value="arrowhead" name="name" type="QString"/>
                <Option value="0,0" name="offset" type="QString"/>
                <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
                <Option value="MM" name="offset_unit" type="QString"/>
                <Option value="35,35,35,255" name="outline_color" type="QString"/>
                <Option value="solid" name="outline_style" type="QString"/>
                <Option value="0.2" name="outline_width" type="QString"/>
                <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
                <Option value="MM" name="outline_width_unit" type="QString"/>
                <Option value="diameter" name="scale_method" type="QString"/>
                <Option value="2" name="size" type="QString"/>
                <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
                <Option value="MM" name="size_unit" type="QString"/>
                <Option value="1" name="vertical_anchor_point" type="QString"/>
              </Option>
              <prop v="0" k="angle"/>
              <prop v="square" k="cap_style"/>
              <prop v="255,0,0,255" k="color"/>
              <prop v="1" k="horizontal_anchor_point"/>
              <prop v="bevel" k="joinstyle"/>
              <prop v="arrowhead" k="name"/>
              <prop v="0,0" k="offset"/>
              <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
              <prop v="MM" k="offset_unit"/>
              <prop v="35,35,35,255" k="outline_color"/>
              <prop v="solid" k="outline_style"/>
              <prop v="0.2" k="outline_width"/>
              <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
              <prop v="MM" k="outline_width_unit"/>
              <prop v="diameter" k="scale_method"/>
              <prop v="2" k="size"/>
              <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
              <prop v="MM" k="size_unit"/>
              <prop v="1" k="vertical_anchor_point"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" name="name" type="QString"/>
                  <Option name="properties"/>
                  <Option value="collection" name="type" type="QString"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option value="0" name="embeddedWidgets/count" type="QString"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Pie" attributeLegend="1">
    <DiagramCategory backgroundAlpha="255" maxScaleDenominator="1e+08" penAlpha="255" minScaleDenominator="1000" labelPlacementMethod="XHeight" spacing="0" scaleDependency="Area" opacity="1" rotationOffset="270" lineSizeType="MM" spacingUnitScale="3x:0,0,0,0,0,0" penColor="#000000" barWidth="5" penWidth="0" diagramOrientation="Up" scaleBasedVisibility="0" showAxis="0" sizeType="MM" width="15" minimumSize="0" lineSizeScale="3x:0,0,0,0,0,0" spacingUnit="MM" backgroundColor="#ffffff" height="15" direction="1" enabled="0" sizeScale="3x:0,0,0,0,0,0">
      <fontProperties description="Arial,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute field="" label="" color="#000000"/>
      <axisSymbol>
        <symbol force_rhr="0" clip_to_extent="1" alpha="1" name="" type="line">
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
          <layer pass="0" class="SimpleLine" enabled="1" locked="0">
            <Option type="Map">
              <Option value="0" name="align_dash_pattern" type="QString"/>
              <Option value="square" name="capstyle" type="QString"/>
              <Option value="5;2" name="customdash" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale" type="QString"/>
              <Option value="MM" name="customdash_unit" type="QString"/>
              <Option value="0" name="dash_pattern_offset" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale" type="QString"/>
              <Option value="MM" name="dash_pattern_offset_unit" type="QString"/>
              <Option value="0" name="draw_inside_polygon" type="QString"/>
              <Option value="bevel" name="joinstyle" type="QString"/>
              <Option value="35,35,35,255" name="line_color" type="QString"/>
              <Option value="solid" name="line_style" type="QString"/>
              <Option value="0.26" name="line_width" type="QString"/>
              <Option value="MM" name="line_width_unit" type="QString"/>
              <Option value="0" name="offset" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
              <Option value="MM" name="offset_unit" type="QString"/>
              <Option value="0" name="ring_filter" type="QString"/>
              <Option value="0" name="trim_distance_end" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale" type="QString"/>
              <Option value="MM" name="trim_distance_end_unit" type="QString"/>
              <Option value="0" name="trim_distance_start" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale" type="QString"/>
              <Option value="MM" name="trim_distance_start_unit" type="QString"/>
              <Option value="0" name="tweak_dash_pattern_on_corners" type="QString"/>
              <Option value="0" name="use_custom_dash" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="width_map_unit_scale" type="QString"/>
            </Option>
            <prop v="0" k="align_dash_pattern"/>
            <prop v="square" k="capstyle"/>
            <prop v="5;2" k="customdash"/>
            <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
            <prop v="MM" k="customdash_unit"/>
            <prop v="0" k="dash_pattern_offset"/>
            <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
            <prop v="MM" k="dash_pattern_offset_unit"/>
            <prop v="0" k="draw_inside_polygon"/>
            <prop v="bevel" k="joinstyle"/>
            <prop v="35,35,35,255" k="line_color"/>
            <prop v="solid" k="line_style"/>
            <prop v="0.26" k="line_width"/>
            <prop v="MM" k="line_width_unit"/>
            <prop v="0" k="offset"/>
            <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
            <prop v="MM" k="offset_unit"/>
            <prop v="0" k="ring_filter"/>
            <prop v="0" k="trim_distance_end"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_end_map_unit_scale"/>
            <prop v="MM" k="trim_distance_end_unit"/>
            <prop v="0" k="trim_distance_start"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_start_map_unit_scale"/>
            <prop v="MM" k="trim_distance_start_unit"/>
            <prop v="0" k="tweak_dash_pattern_on_corners"/>
            <prop v="0" k="use_custom_dash"/>
            <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings priority="0" obstacle="0" dist="0" placement="2" linePlacementFlags="2" showAll="1" zIndex="0">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties" type="Map">
          <Option name="show" type="Map">
            <Option value="true" name="active" type="bool"/>
            <Option value="id" name="field" type="QString"/>
            <Option value="2" name="type" type="int"/>
          </Option>
        </Option>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="None" name="id">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="up">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="down">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="z_invert_up">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="z_invert_down">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cross_section_type">
      <editWidget type="Enumeration">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="h_sable">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="branch">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="exclude_from_branch">
      <editWidget type="CheckBox">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="rk">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="custom_length">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="circular_diameter">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ovoid_height">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ovoid_top_diameter">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ovoid_invert_diameter">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cp_geom">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="op_geom">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="rk_maj">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="configuration">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="generated">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="validity">
      <editWidget type="CheckBox">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="up_type">
      <editWidget type="Enumeration">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="down_type">
      <editWidget type="Enumeration">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="configuration_json">
      <editWidget type="JsonEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="comment">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="qcap">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="z_vault_up">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="z_vault_down">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="z_tn_up">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="z_tn_down">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" index="0" name=""/>
    <alias field="name" index="1" name=""/>
    <alias field="up" index="2" name=""/>
    <alias field="down" index="3" name=""/>
    <alias field="z_invert_up" index="4" name=""/>
    <alias field="z_invert_down" index="5" name=""/>
    <alias field="cross_section_type" index="6" name=""/>
    <alias field="h_sable" index="7" name=""/>
    <alias field="branch" index="8" name=""/>
    <alias field="exclude_from_branch" index="9" name=""/>
    <alias field="rk" index="10" name=""/>
    <alias field="custom_length" index="11" name=""/>
    <alias field="circular_diameter" index="12" name=""/>
    <alias field="ovoid_height" index="13" name=""/>
    <alias field="ovoid_top_diameter" index="14" name=""/>
    <alias field="ovoid_invert_diameter" index="15" name=""/>
    <alias field="cp_geom" index="16" name=""/>
    <alias field="op_geom" index="17" name=""/>
    <alias field="rk_maj" index="18" name=""/>
    <alias field="configuration" index="19" name=""/>
    <alias field="generated" index="20" name=""/>
    <alias field="validity" index="21" name=""/>
    <alias field="up_type" index="22" name=""/>
    <alias field="down_type" index="23" name=""/>
    <alias field="configuration_json" index="24" name=""/>
    <alias field="comment" index="25" name=""/>
    <alias field="qcap" index="26" name=""/>
    <alias field="z_vault_up" index="27" name=""/>
    <alias field="z_vault_down" index="28" name=""/>
    <alias field="z_tn_up" index="29" name=""/>
    <alias field="z_tn_down" index="30" name=""/>
  </aliases>
  <defaults>
    <default field="id" expression="" applyOnUpdate="0"/>
    <default field="name" expression="" applyOnUpdate="0"/>
    <default field="up" expression="" applyOnUpdate="0"/>
    <default field="down" expression="" applyOnUpdate="0"/>
    <default field="z_invert_up" expression="" applyOnUpdate="0"/>
    <default field="z_invert_down" expression="" applyOnUpdate="0"/>
    <default field="cross_section_type" expression="" applyOnUpdate="0"/>
    <default field="h_sable" expression="" applyOnUpdate="0"/>
    <default field="branch" expression="" applyOnUpdate="0"/>
    <default field="exclude_from_branch" expression="" applyOnUpdate="0"/>
    <default field="rk" expression="" applyOnUpdate="0"/>
    <default field="custom_length" expression="" applyOnUpdate="0"/>
    <default field="circular_diameter" expression="" applyOnUpdate="0"/>
    <default field="ovoid_height" expression="" applyOnUpdate="0"/>
    <default field="ovoid_top_diameter" expression="" applyOnUpdate="0"/>
    <default field="ovoid_invert_diameter" expression="" applyOnUpdate="0"/>
    <default field="cp_geom" expression="" applyOnUpdate="0"/>
    <default field="op_geom" expression="" applyOnUpdate="0"/>
    <default field="rk_maj" expression="" applyOnUpdate="0"/>
    <default field="configuration" expression="" applyOnUpdate="0"/>
    <default field="generated" expression="" applyOnUpdate="0"/>
    <default field="validity" expression="" applyOnUpdate="0"/>
    <default field="up_type" expression="" applyOnUpdate="0"/>
    <default field="down_type" expression="" applyOnUpdate="0"/>
    <default field="configuration_json" expression="" applyOnUpdate="0"/>
    <default field="comment" expression="" applyOnUpdate="0"/>
    <default field="qcap" expression="" applyOnUpdate="0"/>
    <default field="z_vault_up" expression="" applyOnUpdate="0"/>
    <default field="z_vault_down" expression="" applyOnUpdate="0"/>
    <default field="z_tn_up" expression="" applyOnUpdate="0"/>
    <default field="z_tn_down" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint field="id" constraints="3" exp_strength="0" unique_strength="1" notnull_strength="1"/>
    <constraint field="name" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="up" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="down" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="z_invert_up" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="z_invert_down" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="cross_section_type" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="h_sable" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="branch" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="exclude_from_branch" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="rk" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="custom_length" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="circular_diameter" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="ovoid_height" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="ovoid_top_diameter" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="ovoid_invert_diameter" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="cp_geom" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="op_geom" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="rk_maj" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="configuration" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="generated" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="validity" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="up_type" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="down_type" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="configuration_json" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="comment" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="qcap" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="z_vault_up" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="z_vault_down" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="z_tn_up" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
    <constraint field="z_tn_down" constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" exp="" desc=""/>
    <constraint field="name" exp="" desc=""/>
    <constraint field="up" exp="" desc=""/>
    <constraint field="down" exp="" desc=""/>
    <constraint field="z_invert_up" exp="" desc=""/>
    <constraint field="z_invert_down" exp="" desc=""/>
    <constraint field="cross_section_type" exp="" desc=""/>
    <constraint field="h_sable" exp="" desc=""/>
    <constraint field="branch" exp="" desc=""/>
    <constraint field="exclude_from_branch" exp="" desc=""/>
    <constraint field="rk" exp="" desc=""/>
    <constraint field="custom_length" exp="" desc=""/>
    <constraint field="circular_diameter" exp="" desc=""/>
    <constraint field="ovoid_height" exp="" desc=""/>
    <constraint field="ovoid_top_diameter" exp="" desc=""/>
    <constraint field="ovoid_invert_diameter" exp="" desc=""/>
    <constraint field="cp_geom" exp="" desc=""/>
    <constraint field="op_geom" exp="" desc=""/>
    <constraint field="rk_maj" exp="" desc=""/>
    <constraint field="configuration" exp="" desc=""/>
    <constraint field="generated" exp="" desc=""/>
    <constraint field="validity" exp="" desc=""/>
    <constraint field="up_type" exp="" desc=""/>
    <constraint field="down_type" exp="" desc=""/>
    <constraint field="configuration_json" exp="" desc=""/>
    <constraint field="comment" exp="" desc=""/>
    <constraint field="qcap" exp="" desc=""/>
    <constraint field="z_vault_up" exp="" desc=""/>
    <constraint field="z_vault_down" exp="" desc=""/>
    <constraint field="z_tn_up" exp="" desc=""/>
    <constraint field="z_tn_down" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" sortExpression="" actionWidgetStyle="dropDown">
    <columns>
      <column hidden="0" name="id" type="field" width="-1"/>
      <column hidden="0" name="name" type="field" width="-1"/>
      <column hidden="0" name="up" type="field" width="-1"/>
      <column hidden="0" name="down" type="field" width="-1"/>
      <column hidden="0" name="z_invert_up" type="field" width="-1"/>
      <column hidden="0" name="z_invert_down" type="field" width="-1"/>
      <column hidden="0" name="cross_section_type" type="field" width="-1"/>
      <column hidden="0" name="h_sable" type="field" width="-1"/>
      <column hidden="0" name="branch" type="field" width="-1"/>
      <column hidden="0" name="configuration" type="field" width="-1"/>
      <column hidden="0" name="generated" type="field" width="-1"/>
      <column hidden="0" name="validity" type="field" width="-1"/>
      <column hidden="0" name="up_type" type="field" width="-1"/>
      <column hidden="0" name="down_type" type="field" width="-1"/>
      <column hidden="0" name="exclude_from_branch" type="field" width="-1"/>
      <column hidden="0" name="rk" type="field" width="-1"/>
      <column hidden="0" name="custom_length" type="field" width="-1"/>
      <column hidden="0" name="circular_diameter" type="field" width="-1"/>
      <column hidden="0" name="ovoid_height" type="field" width="-1"/>
      <column hidden="0" name="ovoid_top_diameter" type="field" width="-1"/>
      <column hidden="0" name="ovoid_invert_diameter" type="field" width="-1"/>
      <column hidden="0" name="cp_geom" type="field" width="-1"/>
      <column hidden="0" name="op_geom" type="field" width="-1"/>
      <column hidden="0" name="rk_maj" type="field" width="-1"/>
      <column hidden="0" name="configuration_json" type="field" width="-1"/>
      <column hidden="0" name="comment" type="field" width="-1"/>
      <column hidden="0" name="qcap" type="field" width="-1"/>
      <column hidden="0" name="z_vault_up" type="field" width="-1"/>
      <column hidden="0" name="z_vault_down" type="field" width="-1"/>
      <column hidden="0" name="z_tn_up" type="field" width="-1"/>
      <column hidden="0" name="z_tn_down" type="field" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">C:/Users/thomas.bienvenu/Documents</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>C:/Users/thomas.bienvenu/Documents</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ
"Fonction d'initialisation Python"
Voici un exemple à suivre:
"""
from PyQt4.QtGui import QWidget

def my_form_open(dialog, layer, feature):
⇥geom = feature.geometry()
⇥control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="branch"/>
    <field editable="1" name="circular_diameter"/>
    <field editable="1" name="comment"/>
    <field editable="1" name="configuration"/>
    <field editable="1" name="configuration_json"/>
    <field editable="1" name="cp_geom"/>
    <field editable="1" name="cross_section_type"/>
    <field editable="1" name="custom_length"/>
    <field editable="1" name="down"/>
    <field editable="1" name="down_type"/>
    <field editable="1" name="exclude_from_branch"/>
    <field editable="1" name="generated"/>
    <field editable="1" name="h_sable"/>
    <field editable="1" name="id"/>
    <field editable="1" name="name"/>
    <field editable="1" name="op_geom"/>
    <field editable="1" name="ovoid_height"/>
    <field editable="1" name="ovoid_invert_diameter"/>
    <field editable="1" name="ovoid_top_diameter"/>
    <field editable="1" name="qcap"/>
    <field editable="1" name="rk"/>
    <field editable="1" name="rk_maj"/>
    <field editable="1" name="up"/>
    <field editable="1" name="up_type"/>
    <field editable="1" name="validity"/>
    <field editable="1" name="z_invert_down"/>
    <field editable="1" name="z_invert_up"/>
    <field editable="1" name="z_tn_down"/>
    <field editable="1" name="z_tn_up"/>
    <field editable="1" name="z_vault_down"/>
    <field editable="1" name="z_vault_up"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="branch"/>
    <field labelOnTop="0" name="circular_diameter"/>
    <field labelOnTop="0" name="comment"/>
    <field labelOnTop="0" name="configuration"/>
    <field labelOnTop="0" name="configuration_json"/>
    <field labelOnTop="0" name="cp_geom"/>
    <field labelOnTop="0" name="cross_section_type"/>
    <field labelOnTop="0" name="custom_length"/>
    <field labelOnTop="0" name="down"/>
    <field labelOnTop="0" name="down_type"/>
    <field labelOnTop="0" name="exclude_from_branch"/>
    <field labelOnTop="0" name="generated"/>
    <field labelOnTop="0" name="h_sable"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="op_geom"/>
    <field labelOnTop="0" name="ovoid_height"/>
    <field labelOnTop="0" name="ovoid_invert_diameter"/>
    <field labelOnTop="0" name="ovoid_top_diameter"/>
    <field labelOnTop="0" name="qcap"/>
    <field labelOnTop="0" name="rk"/>
    <field labelOnTop="0" name="rk_maj"/>
    <field labelOnTop="0" name="up"/>
    <field labelOnTop="0" name="up_type"/>
    <field labelOnTop="0" name="validity"/>
    <field labelOnTop="0" name="z_invert_down"/>
    <field labelOnTop="0" name="z_invert_up"/>
    <field labelOnTop="0" name="z_tn_down"/>
    <field labelOnTop="0" name="z_tn_up"/>
    <field labelOnTop="0" name="z_vault_down"/>
    <field labelOnTop="0" name="z_vault_up"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="branch" reuseLastValue="0"/>
    <field name="circular_diameter" reuseLastValue="0"/>
    <field name="comment" reuseLastValue="0"/>
    <field name="configuration" reuseLastValue="0"/>
    <field name="configuration_json" reuseLastValue="0"/>
    <field name="cp_geom" reuseLastValue="0"/>
    <field name="cross_section_type" reuseLastValue="0"/>
    <field name="custom_length" reuseLastValue="0"/>
    <field name="down" reuseLastValue="0"/>
    <field name="down_type" reuseLastValue="0"/>
    <field name="exclude_from_branch" reuseLastValue="0"/>
    <field name="generated" reuseLastValue="0"/>
    <field name="h_sable" reuseLastValue="0"/>
    <field name="id" reuseLastValue="0"/>
    <field name="name" reuseLastValue="0"/>
    <field name="op_geom" reuseLastValue="0"/>
    <field name="ovoid_height" reuseLastValue="0"/>
    <field name="ovoid_invert_diameter" reuseLastValue="0"/>
    <field name="ovoid_top_diameter" reuseLastValue="0"/>
    <field name="qcap" reuseLastValue="0"/>
    <field name="rk" reuseLastValue="0"/>
    <field name="rk_maj" reuseLastValue="0"/>
    <field name="up" reuseLastValue="0"/>
    <field name="up_type" reuseLastValue="0"/>
    <field name="validity" reuseLastValue="0"/>
    <field name="z_invert_down" reuseLastValue="0"/>
    <field name="z_invert_up" reuseLastValue="0"/>
    <field name="z_tn_down" reuseLastValue="0"/>
    <field name="z_tn_up" reuseLastValue="0"/>
    <field name="z_vault_down" reuseLastValue="0"/>
    <field name="z_vault_up" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"name"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
