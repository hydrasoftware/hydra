/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update $model.metadata set version = '2.3';
;;

/* ********************************************** */
/*                Cahtchment autofill             */
/* ********************************************** */

create or replace function ${model}.auto_param_catchment(catchment_id integer)
returns hydra_catchment_param
language plpgsql
as
$$$$
    declare
	area real;
	rl real;
	slope real;
	c_imp real;
    begin
        select ST_Area(c.geom)
        from $model.catchment_node as n, $model.catchment as c
        where n.id=catchment_id and ST_Contains(c.geom, n.geom)
        into area;

        select ST_Length(l.geom), Greatest(( project.altitude(ST_StartPoint(l.geom)) - project.altitude(ST_EndPoint(l.geom)) ) / ST_Length(l.geom), 0.005)
        from $model.catchment_node as n, $model.routing_hydrology_link as l
        where n.id=catchment_id and l.up=n.id
        into rl, slope;

        select Sum((ST_Area(ST_Intersection(c.geom, so.geom)) / ST_Area(c.geom)) * t.c_imp)
	    from $model.catchment_node as n, $model.catchment as c, project.soil_occupation as so
	    join project.soil_occupation_type as t on t.id=so.soil_occupation_type
	    where n.id=catchment_id and ST_Contains(c.geom, n.geom) and ST_Intersects(c.geom, so.geom)
	    into c_imp;

        return (area, rl, slope, c_imp);
    end;
$$$$
;;

/* ********************************************** */
/*  Constrain update                              */
/* ********************************************** */

create or replace view $model.constrain_points as(
with points as (
    select p.id,
    p.points_id,
    p.geom as geom,
    p.name,
    p.z_ground,
    st_linelocatepoint(c.geom, p.geom) as loc,
    st_lineinterpolatepoint(c.geom,st_linelocatepoint(c.geom, p.geom)) as geom_on_line,
    c.id as id_line
    from $model.constrain c,
    project.points_xyz p
    where st_distance(c.geom, p.geom) < c.points_xyz_proximity
    and c.points_xyz = p.points_id
    and c.constrain_type is distinct from 'ignored_for_coverages'
    order by c.id, loc)
,z_line as (
    select st_setsrid(st_makeline(st_makepoint(st_x(points.geom_on_line), st_y(points.geom_on_line), points.z_ground::double precision))::geometry(linestringz), $srid) as geom,
    points.id_line as id_cons
    from points
    group by points.id_line
)
,vertex as (
    select (st_dumppoints(c.geom)).geom as geom,
    c.id as id_line,
    (st_dumppoints(c.geom)).path[1] as id,
    st_linelocatepoint(c.geom, (st_dumppoints(c.geom)).geom) as loc
    from $model.constrain c
    where c.constrain_type is distinct from 'ignored_for_coverages')
,points_on_line as (
    select points.id_line as id_cons,
    points.geom_on_line as geom,
    points.z_ground as z_ground,
    st_linelocatepoint(c.geom, points.geom) as loc
    from points, $model.constrain as c
    where points.id_line = c.id
    and c.constrain_type is distinct from 'ignored_for_coverages'
union
    select vertex.id_line as id_cons,
    vertex.geom as geom,
    st_z(st_lineinterpolatepoint(z_line.geom,vertex.loc)) as z_ground,
    vertex.loc as loc
    from vertex, z_line
    where z_line.id_cons = vertex.id_line)
,ord_points as (
    select *
    from points_on_line as points
    order by id_cons, loc)
select st_setsrid(st_makeline(st_makepoint(st_x(points.geom), st_y(points.geom), points.z_ground::double precision))::geometry(linestringz), $srid) as geom,
points.id_cons as id
from ord_points as points
group by id_cons
)
;;

create or replace view $model.close_point as
    with allpoints as (
        select id, (st_dumppoints(geom)).geom as geom from $model.constrain where constrain_type is distinct from 'ignored_for_coverages'
    ),
    ends as (
        select id, st_startpoint(geom) as geom from $model.constrain where constrain_type is distinct from 'ignored_for_coverages'
        union
        select id, st_endpoint(geom) as geom from $model.constrain where constrain_type is distinct from 'ignored_for_coverages'
    ),
    cpoints as (
        select id, st_collect(geom) as geom from allpoints group by id
    ),
    clust as (
        select
          sqrt(ST_Area(ST_MinimumBoundingCircle(gc)) / pi()) as radius,
          ST_NumGeometries(gc) as nb,
          ST_Centroid(gc) as geom
        from (
          select unnest(ST_ClusterWithin(geom, 5)) gc
          from allpoints
        ) f
        union
        select
            1 as radius,
            0 as nb,
            st_force2d(e.geom) as geom
        from $model.constrain as c, cpoints as cp, ends as e
        where st_dwithin(c.geom, e.geom, 1)
        and not st_intersects(cp.geom, e.geom)
        and e.id != c.id
        and c.id = cp.id
        and c.constrain_type is distinct from 'ignored_for_coverages'
    )
select row_number() over () AS id, radius, nb, geom::geometry('POINT', $srid) from clust where radius > 0
;;

create or replace function $model.coverage_update()
returns integer
language plpgsql
as $$$$
    declare
        res_ integer;
    begin
        delete from $model.coverage ; -- where domain_2d is null;
        alter sequence $model.coverage_id_seq restart;

        with cst as (
            select (ST_Dump(coalesce(ST_Split(a.discretized,
                    (select ST_Collect(discretized) from $model.constrain as b where a.id!=b.id and ST_Intersects(a.discretized, b.discretized) and b.constrain_type is distinct from 'ignored_for_coverages')
                    ),
                a.discretized))).geom as geom
            from $model.constrain as a
            where a.constrain_type is distinct from 'ignored_for_coverages'
            )
        insert into $model.coverage(geom, domain_type) select (st_dump(st_polygonize(geom))).geom, '2d' from cst;


        with oned as (
            select c.id
            from $model.coverage as c, $model.street as d
            where ST_Intersects(c.geom, d.geom)
            group by c.id
        )
        update $model.coverage set domain_type='street' where id in (select oned.id from oned);

        update $model.coverage as c set domain_type='reach'
        where exists (select 1 from $model.open_reach as r
        where ST_Intersects(c.geom, r.geom)
        and ST_Length(ST_CollectionExtract(ST_Intersection(c.geom, r.geom), 2)) > 0);

        with oned as (
            select distinct c.id
            from $model.coverage as c, $model.storage_node as d
            where ST_Intersects(c.geom, d.geom)
        )
        update $model.coverage set domain_type='storage' where id in (select oned.id from oned);

        with oned as (
            select c.id, count(1) as ct
            from $model.coverage as c, $model.coverage_marker as mkc
            where ST_Intersects(c.geom, mkc.geom)
            group by c.id
        )
        update $model.coverage set domain_type=null where id in (select oned.id from oned where ct=1);

        select count(*) from $model.coverage into res_;
        return res_;
    end;
$$$$
;;

create or replace function ${model}.gen_cst_street(street_id integer)
returns integer
language plpgsql
as $$$$
    declare
        _street_geom geometry;
        _street_width real;
        _street_el real;
        _trigger_state boolean;
    begin
        select geom, width, elem_length
        from $model.street
        where id=street_id
        into _street_geom, _street_width, _street_el;

        select trigger_coverage from $model.metadata into _trigger_state;
        update $model.metadata set trigger_coverage = 'f';

        with buf as (
                select ST_Buffer(ST_Force2d(_street_geom), _street_width/2, 'quad_segs=1 join=mitre mitre_limit='||(_street_width*2)::varchar) as geom
            ),
            cst as (
                select ST_ExteriorRing(geom) as geom from buf
            ),
            spl as (
                select coalesce(ST_Split(cst.geom, (select ST_Collect(ST_Force2d(c.geom))
                                                    from $model.constrain as c
                                                    where ST_Intersects(c.geom, cst.geom) and c.constrain_type is distinct from 'ignored_for_coverages')
                                                    ), cst.geom) as geom
                from cst
            ),
            spld as (
                select ST_Force3d((st_dump(geom)).geom) as geom from spl
            ),
            diff as (
                select id, (ST_Dump(ST_CollectionExtract(ST_Split(c.geom, b.geom), 2))).geom as geom
                from $model.constrain as c, cst as b
                where ST_Intersects(b.geom, c.geom)
                and c.constrain_type is distinct from 'ignored_for_coverages'
            ),
            ins as (
                insert into $model.constrain(geom, elem_length, constrain_type)
                select d.geom, c.elem_length, c.constrain_type
                from $model.constrain as c, diff as d
                where c.id = d.id
                union
                select geom, _street_el, 'overflow'
                from spld
                returning id
            )
        delete from $model.constrain
        where id in (select id from diff);

        with cst_brut as (
                select a.id, (ST_Dump(coalesce(ST_Split(a.discretized,
                    (select ST_Collect(discretized) from $model.constrain as b
                        where a.id!=b.id
                        and ST_Intersects(a.discretized, b.discretized)
                        and b.constrain_type is distinct from 'ignored_for_coverages'
                        and ST_Dimension(ST_Intersection(a.discretized, b.discretized))=0)
                    ),
                a.discretized))).geom as geom
                from $model.constrain as a
                where a.constrain_type is distinct from 'ignored_for_coverages'
            ),
            cst as (
                select id, ST_Union(geom) over (partition by id) as geom from cst_brut
            ),
            polyg as (
                select (ST_Dump(ST_Polygonize(cst_brut.geom))).geom as geom from cst_brut
            ),
            inter as (
                select p.geom from polyg as p, $model.street as s
                where ST_Intersects(p.geom, s.geom)
            ),
            un as (
                select ST_Union(i.geom) as geom from inter as i
            ),
            boundary as (
                select ST_Boundary(u.geom) as geom from un as u
            ),
            to_del as (
                select id from cst as c
                where (select ST_Dimension(ST_Intersection(c.geom, b.geom))=0 from boundary as b)
                and (select ST_Contains(u.geom, c.geom) from un as u)
            )
            delete from $model.constrain as c
            where exists (select 1 from to_del as d where d.id=c.id limit 1)
            and not exists (select 1 from $model.valley_cross_section_geometry where transect=c.id limit 1)
            and c.constrain_type is distinct from 'ignored_for_coverages';


        if _trigger_state then
            perform $model.coverage_update();
        end if;

        update $model.metadata set trigger_coverage = _trigger_state;

        return 1;
    end;
$$$$
;;

create or replace function ${model}.update_river_cross_section_pl1d(coverage_id integer)
returns integer
language plpgsql
as $$$$
    declare
        prec_ real;
        res_ integer;
        reach_id_ integer;
    begin
        select precision from hydra.metadata into prec_;

        select r.id
        from $model.coverage as c, $model.reach as r
        where ST_Intersects(r.geom, c.geom)
        and c.id=coverage_id
        and ST_Intersects(r.geom, c.geom)
        limit 1
        into reach_id_;

        delete from $model.river_cross_section_pl1d
        where generated is not null;

        -- create banks
        drop table if exists banks;
        create  table banks
        as
        with res as (
            select (st_dump(st_linemerge((ST_CollectionExtract(ST_Intersection(cs.discretized, cv.geom), 2))))).geom as geom
            from $model.coverage as cv, $model.constrain as cs, $model.reach as r
            where ST_Intersects(r.geom, cv.geom)
            and cv.id=coverage_id
            and r.id = reach_id_
            and not ST_Intersects(r.geom, cs.geom)
            and cs.constrain_type is distinct from 'ignored_for_coverages'
        )
        select row_number() over() as id, geom::geometry('LINESTRINGZ', $srid) from res
        ;


        -- river points that form the bedline are the one
        -- which are the closest point of their projection and the banks
        drop table if exists bedline;
        create  table bedline
        as
        with projected as (
            select b.id as bank, ST_LineInterpolatePoint(b.geom, ST_LineLocatePoint(b.geom, r.geom)) as geom
            from (
                select (st_dumppoints(geom)).geom as geom
                from $model.reach where id=reach_id_
                union
                select n.geom
                from $model.river_node as n
                where n.reach=reach_id_
                ) as r, banks as b
            union
            select b.id as bank, (st_dumppoints(b.geom)).geom as geom
            from banks as b
        ),
        closest as (
            select b.bank, ST_LineInterpolatePoint(r.geom, ST_LineLocatePoint(r.geom, b.geom)) as geom
            from $model.reach as r, projected as b
            where r.id=reach_id_
        )
        select row_number() over() as id, c.bank, ST_MakeLine(c.geom order by ST_LineLocatePoint(b.geom, c.geom)) as geom
        from closest as c, banks as b
        where c.bank=b.id
        group by c.bank
        ;

        drop table if exists midline;
        create  table midline
        as
        select row_number() over() as id, ST_MakeLine(st_centroid(ST_MakeLine(p.geom, ST_ClosestPoint(b.geom, p.geom)))) as geom, p.bank
        from banks as b, (select (st_dumppoints(geom)).geom as geom, bank from bedline) as p
        where b.id=p.bank
        group by p.bank
        ;

        drop table if exists pl1d;
        create  table pl1d
        as
        select  row_number() over() as id,
            ST_MakeLine(ST_LineInterpolatePoint((select geom from banks where id = 1), i::real/10),
                        ST_LineInterpolatePoint((select geom from banks where id = 2), i::real/10)) as geom,
                    1 as node
        from generate_series(1, 10) as i
        ;
        return 1;
    end;
$$$$
;;

-- for any point close to riverbank, give the transect interpolated at this point
-- for points on the border bewteen two reach coverage, you can specify the coverage id
create or replace function ${model}.interpolate_transect_at(point geometry, coverage_id integer default null)
returns geometry('LINESTRINGZ', $srid)
language plpgsql
immutable
as $$$$
    declare
        l_ real;
        coverage geometry;
        reach geometry;
        res geometry;
        left_transects geometry;
        right_transects geometry;
        closest geometry;
        direction vector2;
        other_direction vector2;
        other_side geometry;
        side varchar;
    begin
        l_ := 10000;

        --raise notice 'point %', point;
        select ST_CollectionHomogenize(ST_Collect(ST_MakeLine(array[ST_StartPoint(discretized), ST_ClosestPoint(discretized, reach), ST_EndPoint(discretized)])))
        from ${model}.constrain
        where constrain_type='flood_plain_transect'
        and ST_Intersects(discretized, point)
        and constrain_type is distinct from 'ignored_for_coverages'
        into res;

        if ST_GeometryType(res) = 'ST_LineString' then
            return (
                select ST_MakeLine(array[ST_StartPoint(res), ST_ClosestPoint(r.geom, res), ST_EndPoint(res)])
                from ${model}.reach r
                order by r.geom <-> res
                limit 1
            );
        end if;

        -- closest coverage or specified one
        select coalesce(
            (select geom from ${model}.coverage where id=coverage_id),
            (select geom from ${model}.coverage where domain_type='reach' order by geom <-> point limit 1)
        ) into coverage;

        if not ST_DWithin(coverage, point, .1) then
            raise 'point % is not close enought to nearest coverage %', point, coverage ;
        end if;

        -- closest reach in coverage
        select geom from ${model}.reach where ST_Intersects(geom, coverage) order by geom <-> point limit 1 into reach;

        if reach is null then
            raise 'no reach in reach coverage %', coverage ;
        end if;

        with pt as (
            select ST_StartPoint(t.geom) as s, ST_ClosestPoint(t.geom, reach) as m, ST_EndPoint(t.geom) as e
            from ${model}.constrain as t
            where ST_Intersects(coverage, t.discretized)
            and t.constrain_type='flood_plain_transect'
            and ST_Length(ST_CollectionExtract(ST_Intersection(t.discretized, coverage), 2)) > 0
            order by ST_Distance(geom, reach)
            limit 2
        )
        select ST_Collect(ST_MakeLine(s, m)), ST_Collect(ST_MakeLine(e, m))
        from pt
        into left_transects, right_transects;

        if ST_NumGeometries(left_transects) = 0 then
            return ST_MakeLine(point, ST_ClosestPoint(reach, point));
        end if;

        select ST_ClosestPoint(reach, point) into closest;
        if dot(difference(ST_EndPoint(ST_GeometryN(right_transects,1)), ST_StartPoint(ST_GeometryN(right_transects,1))), difference(closest, point)) > 0 then
            side := 'right';
        else
            side := 'left';
        end if;

        if ST_NumGeometries(left_transects) = 1 then
            if side = 'right' then
                direction := difference(ST_EndPoint(ST_GeometryN(right_transects,1)), ST_StartPoint(ST_GeometryN(right_transects,1)));
            else
                direction := difference(ST_EndPoint(ST_GeometryN(left_transects,1)), ST_StartPoint(ST_GeometryN(left_transects,1)));
            end if;
            other_direction := direction;
        else
            if side = 'right' then
                direction := interpolate_direction( ST_GeometryN(right_transects,1), ST_GeometryN(right_transects,2), point);
            else
                direction := interpolate_direction( ST_GeometryN(left_transects,1), ST_GeometryN(left_transects,2), point);
            end if;
        end if;

        closest := coalesce(ST_ClosestPoint(ST_Intersection(ST_MakeLine(point, ST_Translate(point, l_*direction.x, l_*direction.y)), reach), point), closest);
        --raise notice 'point % closest %', point, closest;

        if ST_NumGeometries(left_transects) = 1 then
            other_direction := row(-direction.x, -direction.y)::vector2;
        else
            if side = 'right' then
                other_direction := interpolate_direction( ST_GeometryN(left_transects,1), ST_GeometryN(left_transects,2), closest);
            else
                other_direction := interpolate_direction( ST_GeometryN(right_transects,1), ST_GeometryN(right_transects,2), closest);
            end if;
        end if;

        other_side := ST_ClosestPoint(ST_Intersection(ST_MakeLine(closest, ST_Translate(closest, -l_*other_direction.x, -l_*other_direction.y)), ST_ExteriorRing(coverage)), closest);

        if side = 'right' then
            return ST_MakeLine(array[other_side, closest, point]);
        else
            return ST_MakeLine(array[point, closest, other_side]);
        end if;

    end;
$$$$
;;

-- Note that the discretized is not editable
create or replace function ${model}.constrain_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        prec_ real;
        snapped_ geometry;
        other_cst_ geometry;
        differenced_ geometry;
        oned_ geometry;
        discretized_ geometry;
    begin
        select precision from hydra.metadata into prec_;
        if tg_op = 'INSERT' then
            select st_snaptogrid(new.geom, prec_) into snapped_;
            select ST_Linemerge(ST_Collect(geom)) from $model._constrain where ST_Intersects(snapped_, geom) into other_cst_;
            select ST_LineMerge(coalesce(ST_Difference(snapped_, other_cst_), snapped_)) into differenced_;
            if differenced_ is not null then
                select ST_GeometryN(differenced_, 1) into oned_;
                select st_snaptogrid(project.discretize_line(oned_, coalesce(new.elem_length, 100)), prec_) into discretized_;
                if not (select St_IsEmpty(st_snaptogrid(new.geom, prec_))) then
                    insert into $model._constrain(name, geom, discretized, elem_length, constrain_type, link_attributes, points_xyz, points_xyz_proximity)
                    values (coalesce(new.name, 'define_later'), oned_, discretized_, coalesce(new.elem_length, 100), new.constrain_type, new.link_attributes, new.points_xyz, coalesce(new.points_xyz_proximity, 1))
                    returning id, elem_length, points_xyz, points_xyz_proximity into new.id, new.elem_length, new.points_xyz, new.points_xyz_proximity ;
                    update $model._constrain set name = 'CONSTR'||new.id::varchar where name = 'define_later' and id = new.id
                    returning name into new.name;
                    return new;
                end if;
                return null;
            end if;
            return null;
        elsif tg_op = 'UPDATE' then
            select st_snaptogrid(new.geom, prec_) into snapped_;
            select ST_Linemerge(ST_Collect(geom)) from $model._constrain where ST_Intersects(snapped_, geom) and id!=old.id into other_cst_;
            select ST_LineMerge(coalesce(ST_Difference(snapped_, other_cst_), snapped_)) into differenced_;
            if differenced_ is not null then
                select ST_GeometryN(differenced_, 1) into oned_;
                select st_snaptogrid(project.discretize_line(differenced_, coalesce(new.elem_length, 100)), prec_) into discretized_;
                update $model._constrain set name=new.name, geom=differenced_, discretized=discretized_,
                    elem_length=new.elem_length, constrain_type=new.constrain_type, link_attributes=new.link_attributes,
                    points_xyz=new.points_xyz, points_xyz_proximity=coalesce(new.points_xyz_proximity, 1)
                    where id = old.id;
                return new;
            end if;
            return old;
        elsif tg_op = 'DELETE' then
            delete from $model._constrain where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

/* ********************************************** */
/*              Interlinks                        */
/* ********************************************** */

create or replace function ${model}.elem_2d_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('elem_2d', coalesce(new.name, 'define_later'), ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), $srid), (select precision from hydra.metadata)), new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'elem_2d') where name = 'define_later' and id = id_;
            insert into $model._elem_2d_node(id, node_type, area, zb, rk, domain_2d, contour)
                values (id_, 'elem_2d', coalesce(new.area, ST_Area(new.contour)), coalesce(new.zb, (select ST_Z(ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), $srid), (select precision from hydra.metadata))))), coalesce(new.rk, 12), new.domain_2d, new.contour);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'elem_2d_node');

            update $model._node set validity = (select (area is not null) and (area>0) and (zb is not null) and (rk is not null) and (rk>0) from  $model._elem_2d_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area, new.zb, new.rk) is distinct from (old.area, old.zb, old.rk)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.zb, old.rk) as o, (select new.area, new.zb, new.rk) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.zb, new.rk) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=ST_SnapToGrid(ST_SetSRID(ST_MakePoint(ST_X(ST_Centroid(new.contour)), ST_Y(ST_Centroid(new.contour)), project.altitude(ST_Centroid(new.contour))), $srid), (select precision from hydra.metadata)), generated=new.generated where id=old.id;
            update $model._elem_2d_node set area=new.area, zb=new.zb, rk=new.rk, domain_2d=new.domain_2d, contour=new.contour where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'elem_2d_node');

            update $model._node set validity = (select (area is not null) and (area>0) and (zb is not null) and (rk is not null) and (rk>0) from  $model._elem_2d_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from project.interlink where (model_up='$model' and node_up=old.id) or (model_down='$model' and node_down=old.id);

            delete from $model._elem_2d_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create or replace function ${model}.manhole_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('manhole', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'manhole') where name = 'define_later' and id = id_;
            insert into $model._manhole_node(id, node_type, area, z_ground, cover_diameter, cover_critical_pressure, inlet_width, inlet_height, connection_law)
                values (id_, 'manhole', coalesce(new.area, 1), coalesce(new.z_ground, (select project.altitude(new.geom))), coalesce(new.cover_diameter, 0.6), coalesce(new.cover_critical_pressure, 2.0), coalesce(new.inlet_width, 1.0), coalesce(new.inlet_height, 0.25), coalesce(new.connection_law, 'manhole_cover'));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'manhole_node');

            update $model._node set validity = (select (connection_law is not null) and (connection_law != 'manhole_cover' or cover_diameter is not null) and (connection_law != 'manhole_cover' or cover_diameter > 0) and (connection_law != 'manhole_cover' or cover_critical_pressure is not null) and (connection_law != 'manhole_cover' or cover_critical_pressure >0) and (connection_law != 'drainage_inlet' or inlet_width is not null) and (connection_law != 'drainage_inlet' or inlet_width >0) and (connection_law != 'drainage_inlet' or inlet_height is not null) and (connection_law != 'drainage_inlet' or inlet_height >0) and (area is not null) and (area>0) and (z_ground is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) and ((select exists (select 1 from $model.pipe_link where up=new.id or down=new.id))) from  $model._manhole_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area, new.z_ground, new.cover_diameter, new.cover_critical_pressure, new.inlet_width, new.inlet_height, new.connection_law) is distinct from (old.area, old.z_ground, old.cover_diameter, old.cover_critical_pressure, old.inlet_width, old.inlet_height, old.connection_law)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.z_ground, old.cover_diameter, old.cover_critical_pressure, old.inlet_width, old.inlet_height, old.connection_law) as o, (select new.area, new.z_ground, new.cover_diameter, new.cover_critical_pressure, new.inlet_width, new.inlet_height, new.connection_law) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.z_ground, new.cover_diameter, new.cover_critical_pressure, new.inlet_width, new.inlet_height, new.connection_law) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._manhole_node set area=new.area, z_ground=new.z_ground, cover_diameter=new.cover_diameter, cover_critical_pressure=new.cover_critical_pressure, inlet_width=new.inlet_width, inlet_height=new.inlet_height, connection_law=new.connection_law where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'manhole_node');

            update $model._node set validity = (select (connection_law is not null) and (connection_law != 'manhole_cover' or cover_diameter is not null) and (connection_law != 'manhole_cover' or cover_diameter > 0) and (connection_law != 'manhole_cover' or cover_critical_pressure is not null) and (connection_law != 'manhole_cover' or cover_critical_pressure >0) and (connection_law != 'drainage_inlet' or inlet_width is not null) and (connection_law != 'drainage_inlet' or inlet_width >0) and (connection_law != 'drainage_inlet' or inlet_height is not null) and (connection_law != 'drainage_inlet' or inlet_height >0) and (area is not null) and (area>0) and (z_ground is not null) and ((select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))) and ((select exists (select 1 from $model.pipe_link where up=new.id or down=new.id))) from  $model._manhole_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from project.interlink where (model_up='$model' and node_up=old.id) or (model_down='$model' and node_down=old.id);

            delete from $model._manhole_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

create or replace function ${model}.crossroad_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('crossroad', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'crossroad') where name = 'define_later' and id = id_;
            insert into $model._crossroad_node(id, node_type, area, z_ground, h)
                values (id_, 'crossroad', coalesce(new.area, 1), coalesce(new.z_ground, (select project.altitude(new.geom))), coalesce(new.h, 0));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'crossroad_node');

            update $model._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) and (h is not null) and (h>=0) from  $model._crossroad_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area, new.z_ground, new.h) is distinct from (old.area, old.z_ground, old.h)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area, old.z_ground, old.h) as o, (select new.area, new.z_ground, new.h) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area, new.z_ground, new.h) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._crossroad_node set area=new.area, z_ground=new.z_ground, h=new.h where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'crossroad_node');

            update $model._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) and (h is not null) and (h>=0) from  $model._crossroad_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            delete from project.interlink where (model_up='$model' and node_up=old.id) or (model_down='$model' and node_down=old.id);

            delete from $model._crossroad_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create or replace function ${model}.storage_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('storage', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'storage') where name = 'define_later' and id = id_;
            insert into $model._storage_node(id, node_type, zs_array, zini, contour)
                values (id_, 'storage', new.zs_array, new.zini, coalesce(new.contour, (select id from $model.coverage where st_intersects(geom, new.geom))));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'storage_node');

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            update $model._node set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1) <= 10 ) and (array_length(zs_array, 1) >= 1) and (array_length(zs_array, 2) = 2) from  $model._storage_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.zs_array, new.zini) is distinct from (old.zs_array, old.zini)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.zs_array, old.zini) as o, (select new.zs_array, new.zini) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.zs_array, new.zini) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._storage_node set zs_array=new.zs_array, zini=new.zini, contour=new.contour where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'storage_node');

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                if (select trigger_coverage from $model.metadata) then
                    update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                    update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            update $model._node set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1) <= 10 ) and (array_length(zs_array, 1) >= 1) and (array_length(zs_array, 2) = 2) from  $model._storage_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' and (select trigger_coverage from $model.metadata) then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            delete from project.interlink where (model_up='$model' and node_up=old.id) or (model_down='$model' and node_down=old.id);

            delete from $model._storage_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;
    end;
$$$$
;;




/* ********************************************** */
/*  Pipes excluded from branch                    */
/* ********************************************** */

drop view if exists $model.pipe_link cascade;
;;

alter table $model._pipe_link add column exclude_from_branch boolean not null default False;
;;

do
$$$$
    declare
        row $model.configuration%rowtype;
    begin
        for row in select * from $model.configuration loop
            update $model._link
            set configuration=(configuration::jsonb||jsonb_build_object(row.name, configuration::jsonb->row.name||jsonb_build_object('exclude_from_branch', 'false')))::json
            where link_type='pipe'
            and configuration::jsonb->row.name != 'null';

            update $model._link
            set configuration=(configuration::jsonb - row.name)::json
            where link_type='pipe'
            and configuration::jsonb->row.name = 'null';
        end loop;
    end;
$$$$;
;;

create or replace view ${model}.pipe_link as
 select p.id,
    p.name,
    p.up,
    p.down,
    c.comment,
    c.z_invert_up,
    c.z_invert_down,
    c.cross_section_type,
    c.h_sable,
    c.branch,
    c.exclude_from_branch,
    c.rk,
    c.custom_length,
    c.circular_diameter,
    c.ovoid_height,
    c.ovoid_top_diameter,
    c.ovoid_invert_diameter,
    c.cp_geom,
    c.op_geom,
    p.geom,
    p.configuration::character varying as configuration,
    p.generated,
    p.validity,
    p.up_type,
    p.down_type,
    p.configuration as configuration_json,
    (select case
    when (c.z_invert_up-c.z_invert_down)*st_length(p.geom)>0
    and st_length(p.geom)<>0
    then
        (select case when c.cross_section_type='circular' then
            (select case when c.circular_diameter>0 then
                (with n as (select
                    (pi()/4*pow(c.circular_diameter, 2.0)) as s,
                    (pi()*c.circular_diameter) as p,
                    c.rk as k)
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n))
            else -999
            end)
        when c.cross_section_type='ovoid' then
            (with n as (select
                (pi()/8*(c.ovoid_top_diameter*c.ovoid_top_diameter+c.ovoid_invert_diameter*c.ovoid_invert_diameter)+0.5*(c.ovoid_height-0.5*(c.ovoid_top_diameter+c.ovoid_invert_diameter))*(c.ovoid_top_diameter+c.ovoid_invert_diameter)) as s,
                (pi()/2*(c.ovoid_top_diameter+c.ovoid_invert_diameter)+2*(c.ovoid_height-0.5*(c.ovoid_top_diameter+c.ovoid_invert_diameter))) as p,
                c.rk as k)
            (select case when n.s>0 and n.p>0 then
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
            else -999
            end from n))
        when c.cross_section_type='pipe' then
            (with n as (select ${model}.geometric_calc_s_fct(cpg.zbmin_array) as s,
                    ${model}.cp_geometric_calc_p_fct(cpg.zbmin_array) as p,
                    c.rk as k
                from ${model}.closed_parametric_geometry cpg
                where c.cp_geom = cpg.id)
            (select case when n.s>0 and n.p>0 then
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
            else -999
            end from n))
        when c.cross_section_type='channel' then
            (with n as (select ${model}.geometric_calc_s_fct(opg.zbmin_array) as s,
                    ${model}.op_geometric_calc_p_fct(opg.zbmin_array) as p,
                    c.rk as k
                from ${model}.open_parametric_geometry opg
                where c.op_geom = opg.id)
            (select case when n.s>0 and n.p>0 then
                (select n.k*n.s*pow(n.s/n.p, 2.0/3.0)*sqrt((c.z_invert_up-c.z_invert_down)/st_length(p.geom)) from n)
            else -999
            end from n))
        else 0.0
        end)
    else -999
    end) as qcap,
    (select case when c.cross_section_type='circular' then
        (select (c.z_invert_up+c.circular_diameter))
    when c.cross_section_type='ovoid' then
        (select (c.z_invert_up+c.ovoid_height))
    when c.cross_section_type='pipe' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.closed_parametric_geometry cpg
            where c.cp_geom = cpg.id)
        (select (c.z_invert_up + n.height) from n))
    when c.cross_section_type='channel' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.open_parametric_geometry opg
            where c.op_geom = opg.id)
        (select (c.z_invert_up + n.height) from n))
    else null
    end) as z_vault_up,
    (select case when c.cross_section_type='circular' then
        (select (c.z_invert_down+c.circular_diameter))
    when c.cross_section_type='ovoid' then
        (select (c.z_invert_down+c.ovoid_height))
    when c.cross_section_type='pipe' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.closed_parametric_geometry cpg
            where c.cp_geom = cpg.id)
        (select (c.z_invert_down + n.height) from n))
    when c.cross_section_type='channel' then
        (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
            from ${model}.open_parametric_geometry opg
            where c.op_geom = opg.id)
        (select (c.z_invert_down + n.height) from n))
    else null
    end) as z_vault_down,
    (select case when c.cross_section_type='channel' then
        (select (c.z_invert_up))::double precision
    else (select z_ground from ${model}.manhole_node where p.up = id)::double precision
    end) as z_tn_up,
    (select case when c.cross_section_type='channel' then
        (select (c.z_invert_down))::double precision
    else (select z_ground from ${model}.manhole_node where p.down = id)::double precision
    end) as z_tn_down

    from ${model}._pipe_link c,
        ${model}._link p
where p.id = c.id
;;

create or replace function ${model}.pipe_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('pipe', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'pipe') where name = 'define_later' and id = id_;
            insert into $model._pipe_link(id, link_type, comment, z_invert_up, z_invert_down, cross_section_type, h_sable, branch, exclude_from_branch, rk, custom_length, circular_diameter, ovoid_height, ovoid_top_diameter, ovoid_invert_diameter, cp_geom, op_geom)
                values (id_, 'pipe', new.comment, new.z_invert_up, new.z_invert_down, coalesce(new.cross_section_type, 'circular'), coalesce(new.h_sable, 0), new.branch, coalesce(new.exclude_from_branch, False), new.rk, coalesce(new.custom_length, null), new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom);
            if 'pipe' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'pipe_link');
            update $model._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (h_sable is not null) and (cross_section_type is not null) and (cross_section_type not in ('valley')) and (rk is not null) and (rk >0) and (cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 )) and (cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)) and (cross_section_type!='pipe' or cp_geom is not null) and (cross_section_type!='channel' or op_geom is not null) from  $model._pipe_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' then
                update $model.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.comment, new.z_invert_up, new.z_invert_down, new.cross_section_type, new.h_sable, new.exclude_from_branch, new.rk, new.custom_length, new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom) is distinct from (old.comment, old.z_invert_up, old.z_invert_down, old.cross_section_type, old.h_sable, old.exclude_from_branch, old.rk, old.custom_length, old.circular_diameter, old.ovoid_height, old.ovoid_top_diameter, old.ovoid_invert_diameter, old.cp_geom, old.op_geom)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.comment, old.z_invert_up, old.z_invert_down, old.cross_section_type, old.h_sable, old.exclude_from_branch, old.rk, old.custom_length, old.circular_diameter, old.ovoid_height, old.ovoid_top_diameter, old.ovoid_invert_diameter, old.cp_geom, old.op_geom) as o, (select new.comment, new.z_invert_up, new.z_invert_down, new.cross_section_type, new.h_sable, new.exclude_from_branch, new.rk, new.custom_length, new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.comment, new.z_invert_up, new.z_invert_down, new.cross_section_type, new.h_sable, new.exclude_from_branch, new.rk, new.custom_length, new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._pipe_link set comment=new.comment, z_invert_up=new.z_invert_up, z_invert_down=new.z_invert_down, cross_section_type=new.cross_section_type, h_sable=new.h_sable, branch=new.branch, exclude_from_branch=new.exclude_from_branch, rk=new.rk, custom_length=new.custom_length, circular_diameter=new.circular_diameter, ovoid_height=new.ovoid_height, ovoid_top_diameter=new.ovoid_top_diameter, ovoid_invert_diameter=new.ovoid_invert_diameter, cp_geom=new.cp_geom, op_geom=new.op_geom where id=old.id;

            if 'pipe' = 'pipe' and (select trigger_branch from $model.metadata) then
                if not ST_Equals(old.geom, new.geom) or (new.exclude_from_branch != old.exclude_from_branch) then
                    perform $model.branch_update_fct();
                end if;
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'pipe_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update $model.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (h_sable is not null) and (cross_section_type is not null) and (cross_section_type not in ('valley')) and (rk is not null) and (rk >0) and (cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 )) and (cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)) and (cross_section_type!='pipe' or cp_geom is not null) and (cross_section_type!='channel' or op_geom is not null) from  $model._pipe_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._pipe_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'pipe' = 'pipe' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' then
                update $model.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_pipe_link_trig
    instead of insert or update or delete on $model.pipe_link
       for each row execute procedure ${model}.pipe_link_fct()
;;

create view $model.invalid as
 with node_invalidity_reason as (
         select new_1.id,
            (((
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   area>0   '::text
                end) ||
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end) ||
                case
                    when new_1.station is not null then ''::text
                    else '   station is not null    '::text
                end) ||
                case
                    when ( select st_intersects(new_1.geom, station.geom) as st_intersects
                       from ${model}.station
                      where station.id = new_1.station) then ''::text
                    else '   (select st_intersects(new.geom, geom) from ${model}.station where id=new.station)   '::text
                end as reason
           from ${model}.station_node new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   (area > 0)   '::text
                end) ||
                case
                    when new_1.z_ground is not null then ''::text
                    else '   z_ground is not null   '::text
                end) ||
                case
                    when new_1.h is not null then ''::text
                    else '   h is not null   '::text
                end) ||
                case
                    when new_1.h >= 0::double precision then ''::text
                    else '   h>=0   '::text
                end as reason
           from ${model}.crossroad_node new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.zs_array is not null then ''::text
                    else '   zs_array is not null    '::text
                end ||
                case
                    when new_1.zini is not null then ''::text
                    else '   zini is not null   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) <= 10 then ''::text
                    else '   array_length(zs_array, 1) <= 10    '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) >= 1 then ''::text
                    else '   array_length(zs_array, 1) >= 1   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 2) = 2 then ''::text
                    else '   array_length(zs_array, 2) = 2   '::text
                end as reason
           from ${model}.storage_node new_1
          where not new_1.validity
        union
         select new_1.id,
            (
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   (area > 0)   '::text
                end) ||
                case
                    when new_1.z_ground is not null then ''::text
                    else '   z_ground is not null   '::text
                end as reason
           from ${model}.manhole_hydrology_node new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((((((((((((((((((((((((
                case
                    when new_1.area_ha is not null then ''::text
                    else '   area_ha is not null   '::text
                end ||
                case
                    when new_1.area_ha > 0::double precision then ''::text
                    else '   area_ha>0   '::text
                end) ||
                case
                    when new_1.rl is not null then ''::text
                    else '   rl is not null   '::text
                end) ||
                case
                    when new_1.rl > 0::double precision then ''::text
                    else '   rl>0   '::text
                end) ||
                case
                    when new_1.slope is not null then ''::text
                    else '   slope is not null   '::text
                end) ||
                case
                    when new_1.c_imp is not null then ''::text
                    else '   c_imp is not null   '::text
                end) ||
                case
                    when new_1.c_imp >= 0::double precision then ''::text
                    else '   c_imp>=0   '::text
                end) ||
                case
                    when new_1.c_imp <= 1::double precision then ''::text
                    else '   c_imp<=1   '::text
                end) ||
                case
                    when new_1.netflow_type is not null then ''::text
                    else '   netflow_type is not null   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'constant_runoff'::hydra_netflow_type or new_1.constant_runoff is not null and new_1.constant_runoff >= 0::double precision and new_1.constant_runoff <= 1::double precision then ''::text
                    else '   netflow_type!=''constant_runoff'' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'horner'::hydra_netflow_type or new_1.horner_ini_loss_coef is not null and new_1.horner_ini_loss_coef >= 0::double precision then ''::text
                    else '   netflow_type!=''horner'' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'horner'::hydra_netflow_type or new_1.horner_recharge_coef is not null and new_1.horner_recharge_coef >= 0::double precision then ''::text
                    else '   netflow_type!=''horner'' or (horner_recharge_coef is not null and horner_recharge_coef>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'holtan'::hydra_netflow_type or new_1.holtan_sat_inf_rate_mmh is not null and new_1.holtan_sat_inf_rate_mmh >= 0::double precision then ''::text
                    else '   netflow_type!=''holtan'' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'holtan'::hydra_netflow_type or new_1.holtan_dry_inf_rate_mmh is not null and new_1.holtan_dry_inf_rate_mmh >= 0::double precision then ''::text
                    else '   netflow_type!=''holtan'' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'holtan'::hydra_netflow_type or new_1.holtan_soil_storage_cap_mm is not null and new_1.holtan_soil_storage_cap_mm >= 0::double precision then ''::text
                    else '   netflow_type!=''holtan'' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'scs'::hydra_netflow_type or new_1.scs_j_mm is not null and new_1.scs_j_mm >= 0::double precision then ''::text
                    else '   netflow_type!=''scs'' or (scs_j_mm is not null and scs_j_mm>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'scs'::hydra_netflow_type or new_1.scs_soil_drainage_time_day is not null and new_1.scs_soil_drainage_time_day >= 0::double precision then ''::text
                    else '   netflow_type!=''scs'' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'scs'::hydra_netflow_type or new_1.scs_rfu_mm is not null and new_1.scs_rfu_mm >= 0::double precision then ''::text
                    else '   netflow_type!=''scs'' or (scs_rfu_mm is not null and scs_rfu_mm>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'hydra'::hydra_netflow_type or new_1.hydra_surface_soil_storage_rfu_mm is not null and new_1.hydra_surface_soil_storage_rfu_mm >= 0::double precision then ''::text
                    else '   netflow_type!=''hydra'' or (hydra_surface_soil_storage_rfu_mm is not null and hydra_surface_soil_storage_rfu_mm>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'hydra'::hydra_netflow_type or new_1.hydra_inf_rate_f0_mm_day is not null and new_1.hydra_inf_rate_f0_mm_day >= 0::double precision then ''::text
                    else '   netflow_type!=''hydra'' or (hydra_inf_rate_f0_mm_day is not null and hydra_inf_rate_f0_mm_day>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'hydra'::hydra_netflow_type or new_1.hydra_int_soil_storage_j_mm is not null and new_1.hydra_int_soil_storage_j_mm >= 0::double precision then ''::text
                    else '   netflow_type!=''hydra'' or (hydra_int_soil_storage_j_mm is not null and hydra_int_soil_storage_j_mm>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'hydra'::hydra_netflow_type or new_1.hydra_soil_drainage_time_qres_day is not null and new_1.hydra_soil_drainage_time_qres_day >= 0::double precision then ''::text
                    else '   netflow_type!=''hydra'' or (hydra_soil_drainage_time_qres_day is not null and hydra_soil_drainage_time_qres_day>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'gr4'::hydra_netflow_type or new_1.gr4_k1 is not null and new_1.gr4_k1 >= 0::double precision then ''::text
                    else '   netflow_type!=''gr4'' or (gr4_k1 is not null and gr4_k1>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'gr4'::hydra_netflow_type or new_1.gr4_k2 is not null then ''::text
                    else '   netflow_type!=''gr4'' or (gr4_k2 is not null)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'gr4'::hydra_netflow_type or new_1.gr4_k3 is not null and new_1.gr4_k3 >= 0::double precision then ''::text
                    else '   netflow_type!=''gr4'' or (gr4_k3 is not null and gr4_k3>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'gr4'::hydra_netflow_type or new_1.gr4_k4 is not null and new_1.gr4_k4 >= 0::double precision then ''::text
                    else '   netflow_type!=''gr4'' or (gr4_k4 is not null and gr4_k4>=0)   '::text
                end) ||
                case
                    when new_1.runoff_type is not null or new_1.netflow_type = 'gr4'::hydra_netflow_type then ''::text
                    else '   runoff_type is not null or netflow_type=''gr4''   '::text
                end) ||
                case
                    when new_1.runoff_type <> 'socose'::hydra_runoff_type or new_1.socose_tc_mn is not null and new_1.socose_tc_mn > 0::double precision or new_1.netflow_type = 'gr4'::hydra_netflow_type then ''::text
                    else '   runoff_type!=''socose'' or (socose_tc_mn is not null and socose_tc_mn>0) or netflow_type=''gr4''   '::text
                end) ||
                case
                    when new_1.runoff_type <> 'socose'::hydra_runoff_type or new_1.socose_shape_param_beta is not null and new_1.socose_shape_param_beta >= 1::double precision and new_1.socose_shape_param_beta <= 6::double precision or new_1.netflow_type = 'gr4'::hydra_netflow_type then ''::text
                    else '   runoff_type!=''socose'' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6) or netflow_type=''gr4''   '::text
                end) ||
                case
                    when new_1.runoff_type <> 'define_k'::hydra_runoff_type or new_1.define_k_mn is not null and new_1.define_k_mn > 0::double precision or new_1.netflow_type = 'gr4'::hydra_netflow_type then ''::text
                    else '   runoff_type!=''define_k'' or (define_k_mn is not null and define_k_mn>0) or netflow_type=''gr4''   '::text
                end) ||
                case
                    when new_1.q_limit is not null then ''::text
                    else '   q_limit is not null   '::text
                end) ||
                case
                    when new_1.q0 is not null then ''::text
                    else '   q0 is not null   '::text
                end) ||
                case
                    when new_1.network_type is not null then ''::text
                    else '   network_type is not null   '::text
                end) ||
                case
                    when new_1.rural_land_use is null or new_1.rural_land_use >= 0::double precision then ''::text
                    else '   rural_land_use is null or rural_land_use>=0   '::text
                end) ||
                case
                    when new_1.industrial_land_use is null or new_1.industrial_land_use >= 0::double precision then ''::text
                    else '   industrial_land_use is null or industrial_land_use>=0   '::text
                end) ||
                case
                    when new_1.suburban_housing_land_use is null or new_1.suburban_housing_land_use >= 0::double precision then ''::text
                    else '   suburban_housing_land_use is null or suburban_housing_land_use>=0   '::text
                end) ||
                case
                    when new_1.dense_housing_land_use is null or new_1.dense_housing_land_use >= 0::double precision then ''::text
                    else '   dense_housing_land_use is null or dense_housing_land_use>=0   '::text
                end as reason
           from ${model}.catchment_node new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   area>0   '::text
                end) ||
                case
                    when new_1.zb is not null then ''::text
                    else '   zb is not null   '::text
                end) ||
                case
                    when new_1.rk is not null then ''::text
                    else '   rk is not null   '::text
                end) ||
                case
                    when new_1.rk > 0::double precision then ''::text
                    else '   rk>0   '::text
                end as reason
           from ${model}.elem_2d_node new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.z_ground is not null then ''::text
                    else '   z_ground is not null   '::text
                end ||
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end) ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   area>0   '::text
                end) ||
                case
                    when new_1.reach is not null then ''::text
                    else '   reach is not null   '::text
                end) ||
                case
                    when ( select not (exists ( select 1
                               from ${model}.station
                              where st_intersects(station.geom, new_1.geom)))) then ''::text
                    else '   (select not exists(select 1 from ${model}.station where st_intersects(geom, new.geom)))   '::text
                end as reason
           from ${model}.river_node new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((((
                case
                    when new_1.connection_law is not null then ''::text
                    else '   connection_law is not null   '::text
                end ||
                case
                    when new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type or new_1.cover_diameter is not null then ''::text
                    else '   connection_law != ''manhole_cover'' or cover_diameter is not null   '::text
                end) ||
                case
                    when new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type or new_1.cover_diameter > 0::double precision then ''::text
                    else '   connection_law != ''manhole_cover'' or cover_diameter > 0   '::text
                end) ||
                case
                    when new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type or new_1.cover_critical_pressure is not null then ''::text
                    else '   connection_law != ''manhole_cover'' or cover_critical_pressure is not null   '::text
                end) ||
                case
                    when new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type or new_1.cover_critical_pressure > 0::double precision then ''::text
                    else '   connection_law != ''manhole_cover'' or cover_critical_pressure >0   '::text
                end) ||
                case
                    when new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type or new_1.inlet_width is not null then ''::text
                    else '   connection_law != ''drainage_inlet'' or inlet_width is not null   '::text
                end) ||
                case
                    when new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type or new_1.inlet_width > 0::double precision then ''::text
                    else '   connection_law != ''drainage_inlet'' or inlet_width >0   '::text
                end) ||
                case
                    when new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type or new_1.inlet_height is not null then ''::text
                    else '   connection_law != ''drainage_inlet'' or inlet_height is not null   '::text
                end) ||
                case
                    when new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type or new_1.inlet_height > 0::double precision then ''::text
                    else '   connection_law != ''drainage_inlet'' or inlet_height >0   '::text
                end) ||
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end) ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   area>0   '::text
                end) ||
                case
                    when new_1.z_ground is not null then ''::text
                    else '   z_ground is not null   '::text
                end) ||
                case
                    when ( select not (exists ( select 1
                               from ${model}.station
                              where st_intersects(station.geom, new_1.geom)))) then ''::text
                    else '   (select not exists(select 1 from ${model}.station where st_intersects(geom, new.geom)))   '::text
                end) ||
                case
                    when ( select (exists ( select 1
                               from ${model}.pipe_link
                              where pipe_link.up = new_1.id or pipe_link.down = new_1.id)) as "exists") then ''::text
                    else '   (select exists (select 1 from ${model}.pipe_link where up=new.id or down=new.id))   '::text
                end as reason
           from ${model}.manhole_node new_1
          where not new_1.validity
        ), link_invalidity_reason as (
         select new_1.id,
            (((((((((((((((((((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.z_ceiling is not null then ''::text
                    else '   z_ceiling is not null   '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc>=0   '::text
                end) ||
                case
                    when new_1.action_gate_type is not null then ''::text
                    else '   action_gate_type is not null   '::text
                end) ||
                case
                    when new_1.z_invert_stop is not null then ''::text
                    else '   z_invert_stop is not null   '::text
                end) ||
                case
                    when new_1.z_ceiling_stop is not null then ''::text
                    else '   z_ceiling_stop is not null   '::text
                end) ||
                case
                    when new_1.v_max_cms is not null then ''::text
                    else '   v_max_cms is not null   '::text
                end) ||
                case
                    when new_1.v_max_cms >= 0::double precision then ''::text
                    else '   v_max_cms>=0   '::text
                end) ||
                case
                    when new_1.dt_regul_hr is not null then ''::text
                    else '   dt_regul_hr is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_control_node is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_pid_array is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_tz_array is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 1) <= 10 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 1) >= 1 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 2) = 2 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or new_1.q_z_crit is not null then ''::text
                    else '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 1) <= 10 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 1) >= 1 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 2) = 2 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul or new_1.nr_z_gate is not null then ''::text
                    else '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                end) ||
                case
                    when new_1.cc_submerged > 0::double precision then ''::text
                    else '   cc_submerged >0   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.regul_gate_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((
                case
                    when new_1.z_crest1 is not null then ''::text
                    else '   z_crest1 is not null   '::text
                end ||
                case
                    when new_1.width1 is not null then ''::text
                    else '   width1 is not null   '::text
                end) ||
                case
                    when new_1.width1 > 0::double precision then ''::text
                    else '   width1>0   '::text
                end) ||
                case
                    when new_1.length is not null then ''::text
                    else '   length is not null   '::text
                end) ||
                case
                    when new_1.length > 0::double precision then ''::text
                    else '   length>0   '::text
                end) ||
                case
                    when new_1.rk is not null then ''::text
                    else '   rk is not null   '::text
                end) ||
                case
                    when new_1.rk >= 0::double precision then ''::text
                    else '   rk>=0   '::text
                end) ||
                case
                    when new_1.z_crest2 is not null then ''::text
                    else '   z_crest2 is not null   '::text
                end) ||
                case
                    when new_1.z_crest2 > new_1.z_crest1 then ''::text
                    else '   z_crest2>z_crest1   '::text
                end) ||
                case
                    when new_1.width2 is not null then ''::text
                    else '   width2 is not null   '::text
                end) ||
                case
                    when new_1.width2 > new_1.width1 then ''::text
                    else '   width2>width1   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.strickler_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((
                case
                    when new_1.npump is not null then ''::text
                    else '   npump is not null   '::text
                end ||
                case
                    when new_1.npump <= 10 then ''::text
                    else '   npump<=10   '::text
                end) ||
                case
                    when new_1.npump >= 1 then ''::text
                    else '   npump>=1   '::text
                end) ||
                case
                    when new_1.zregul_array is not null then ''::text
                    else '   zregul_array is not null   '::text
                end) ||
                case
                    when new_1.hq_array is not null then ''::text
                    else '   hq_array is not null   '::text
                end) ||
                case
                    when array_length(new_1.zregul_array, 1) = new_1.npump then ''::text
                    else '   array_length(zregul_array, 1)=npump   '::text
                end) ||
                case
                    when array_length(new_1.zregul_array, 2) = 2 then ''::text
                    else '   array_length(zregul_array, 2)=2   '::text
                end) ||
                case
                    when array_length(new_1.hq_array, 1) = new_1.npump then ''::text
                    else '   array_length(hq_array, 1)=npump   '::text
                end) ||
                case
                    when array_length(new_1.hq_array, 2) <= 10 then ''::text
                    else '   array_length(hq_array, 2)<=10   '::text
                end) ||
                case
                    when array_length(new_1.hq_array, 2) >= 1 then ''::text
                    else '   array_length(hq_array, 2)>=1   '::text
                end) ||
                case
                    when array_length(new_1.hq_array, 3) = 2 then ''::text
                    else '   array_length(hq_array, 3)=2   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.pump_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((
                case
                    when new_1.q_pump is not null then ''::text
                    else '   q_pump is not null   '::text
                end ||
                case
                    when new_1.q_pump >= 0::double precision then ''::text
                    else '   q_pump>= 0   '::text
                end) ||
                case
                    when new_1.qz_array is not null then ''::text
                    else '   qz_array is not null   '::text
                end) ||
                case
                    when array_length(new_1.qz_array, 1) <= 10 then ''::text
                    else '   array_length(qz_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.qz_array, 1) >= 1 then ''::text
                    else '   array_length(qz_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.qz_array, 2) = 2 then ''::text
                    else '   array_length(qz_array, 2)=2   '::text
                end) ||
                case
                    when ( select bool_and(data_set.bool) as bool_and
                       from ( select 0::double precision <= unnest(new_1.qz_array[:][1:1]) as bool) data_set) then ''::text
                    else '   (/*   qz_array: 0<=q   */ select bool_and(bool) from (select 0<=unnest(new.qz_array[:][1]) as bool) as data_set)   '::text
                end) ||
                case
                    when ( select bool_and(data_set.bool) as bool_and
                       from ( select unnest(new_1.qz_array[:][1:1]) <= 1::double precision as bool) data_set) then ''::text
                    else '   (/*   qz_array: q<=1   */ select bool_and(bool) from (select unnest(new.qz_array[:][1])<=1 as bool) as data_set)   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.deriv_pump_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((((((((((((((((
                case
                    when new_1.z_crest1 is not null then ''::text
                    else '   z_crest1 is not null   '::text
                end ||
                case
                    when new_1.width1 is not null then ''::text
                    else '   width1 is not null   '::text
                end) ||
                case
                    when new_1.width1 >= 0::double precision then ''::text
                    else '   width1>=0   '::text
                end) ||
                case
                    when new_1.z_crest2 is not null then ''::text
                    else '   z_crest2 is not null   '::text
                end) ||
                case
                    when new_1.z_crest2 >= new_1.z_crest1 then ''::text
                    else '   z_crest2>=z_crest1   '::text
                end) ||
                case
                    when new_1.width2 is not null then ''::text
                    else '   width2 is not null   '::text
                end) ||
                case
                    when new_1.width2 >= 0::double precision then ''::text
                    else '   width2>=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc>=0   '::text
                end) ||
                case
                    when new_1.lateral_contraction_coef is not null then ''::text
                    else '   lateral_contraction_coef is not null   '::text
                end) ||
                case
                    when new_1.lateral_contraction_coef <= 1::double precision then ''::text
                    else '   lateral_contraction_coef<=1   '::text
                end) ||
                case
                    when new_1.lateral_contraction_coef >= 0::double precision then ''::text
                    else '   lateral_contraction_coef>=0   '::text
                end) ||
                case
                    when new_1.break_mode is not null then ''::text
                    else '   break_mode is not null   '::text
                end) ||
                case
                    when new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode or new_1.z_break is not null then ''::text
                    else '   break_mode!=''zw_critical'' or z_break is not null   '::text
                end) ||
                case
                    when new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode or new_1.t_break is not null then ''::text
                    else '   break_mode!=''time_critical'' or t_break is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.width_breach is not null then ''::text
                    else '   break_mode=''none'' or width_breach is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.z_invert is not null then ''::text
                    else '   break_mode=''none'' or z_invert is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp is not null then ''::text
                    else '   break_mode=''none'' or grp is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp > 0 then ''::text
                    else '   break_mode=''none'' or grp>0   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp < 100 then ''::text
                    else '   break_mode=''none'' or grp<100   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.dt_fracw_array is not null then ''::text
                    else '   break_mode=''none'' or dt_fracw_array is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 1) <= 10 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 1) >= 1 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 2) = 2 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.overflow_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null    '::text
                end ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc>=0   '::text
                end) ||
                case
                    when new_1.v_max_cms is not null then ''::text
                    else '   v_max_cms is not null   '::text
                end) ||
                case
                    when new_1.v_max_cms >= 0::double precision then ''::text
                    else '   v_max_cms>=0   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.weir_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((
                case
                    when new_1.cross_section is not null then ''::text
                    else '   cross_section is not null   '::text
                end ||
                case
                    when new_1.cross_section >= 0::double precision then ''::text
                    else '   cross_section>=0   '::text
                end) ||
                case
                    when new_1.length is not null then ''::text
                    else '   length is not null   '::text
                end) ||
                case
                    when new_1.length >= 0::double precision then ''::text
                    else '   length>=0   '::text
                end) ||
                case
                    when new_1.slope is not null then ''::text
                    else '   slope is not null   '::text
                end) ||
                case
                    when new_1.down_type = 'manhole_hydrology'::hydra_node_type or (new_1.down_type = any (array['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'crossroad'::hydra_node_type, 'storage'::hydra_node_type, 'elem_2d'::hydra_node_type])) and new_1.hydrograph is not null then ''::text
                    else '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'', ''crossroad'', ''storage'', ''elem_2d'') and (hydrograph is not null))   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.routing_hydrology_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end) ||
                case
                    when new_1.transmitivity is not null then ''::text
                    else '   transmitivity is not null   '::text
                end) ||
                case
                    when new_1.transmitivity >= 0::double precision then ''::text
                    else '   transmitivity>=0   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.porous_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.down_type = 'manhole_hydrology'::hydra_node_type or (new_1.down_type = any (array['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'storage'::hydra_node_type, 'crossroad'::hydra_node_type, 'elem_2d'::hydra_node_type])) and new_1.hydrograph is not null then ''::text
                    else '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'', ''storage'', ''crossroad'', ''elem_2d'') and (hydrograph is not null))   '::text
                end ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.connector_hydrology_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.lateral_contraction_coef is not null then ''::text
                    else '   lateral_contraction_coef is not null   '::text
                end) ||
                case
                    when new_1.lateral_contraction_coef <= 1::double precision then ''::text
                    else '   lateral_contraction_coef<=1   '::text
                end) ||
                case
                    when new_1.lateral_contraction_coef >= 0::double precision then ''::text
                    else '   lateral_contraction_coef>=0   '::text
                end) ||
                case
                    when st_npoints(new_1.border) = 2 then ''::text
                    else '   st_npoints(border)=2   '::text
                end) ||
                case
                    when st_isvalid(new_1.border) then ''::text
                    else '   st_isvalid(border)   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.mesh_2d_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= '0'::numeric::double precision then ''::text
                    else '   width>=0.   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= '0'::numeric::double precision then ''::text
                    else '   cc>=0.   '::text
                end) ||
                case
                    when new_1.break_mode is not null then ''::text
                    else '   break_mode is not null   '::text
                end) ||
                case
                    when new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode or new_1.z_break is not null then ''::text
                    else '   break_mode!=''zw_critical'' or z_break is not null   '::text
                end) ||
                case
                    when new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode or new_1.t_break is not null then ''::text
                    else '   break_mode!=''time_critical'' or t_break is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp is not null then ''::text
                    else '   break_mode=''none'' or grp is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp > 0 then ''::text
                    else '   break_mode=''none'' or grp>0   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp < 100 then ''::text
                    else '   break_mode=''none'' or grp<100   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.dt_fracw_array is not null then ''::text
                    else '   break_mode=''none'' or dt_fracw_array is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 1) <= 10 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 1) >= 1 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 2) = 2 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.fuse_spillway_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.z_ceiling is not null then ''::text
                    else '   z_ceiling is not null   '::text
                end) ||
                case
                    when new_1.z_ceiling > new_1.z_invert then ''::text
                    else '   z_ceiling>z_invert   '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc <=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc >=0   '::text
                end) ||
                case
                    when new_1.action_gate_type is not null then ''::text
                    else '   action_gate_type is not null   '::text
                end) ||
                case
                    when new_1.mode_valve is not null then ''::text
                    else '   mode_valve is not null   '::text
                end) ||
                case
                    when new_1.z_gate is not null then ''::text
                    else '   z_gate is not null   '::text
                end) ||
                case
                    when new_1.z_gate >= new_1.z_invert then ''::text
                    else '   z_gate>=z_invert   '::text
                end) ||
                case
                    when new_1.z_gate <= new_1.z_ceiling then ''::text
                    else '   z_gate<=z_ceiling   '::text
                end) ||
                case
                    when new_1.v_max_cms is not null then ''::text
                    else '   v_max_cms is not null   '::text
                end) ||
                case
                    when new_1.cc_submerged > 0::double precision then ''::text
                    else '   cc_submerged >0   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.gate_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((
                case
                    when new_1.law_type is not null then ''::text
                    else '   law_type is not null   '::text
                end ||
                case
                    when new_1.param is not null then ''::text
                    else '   param is not null   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.borda_headloss_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((((((((
                case
                    when new_1.z_invert_up is not null then ''::text
                    else '   z_invert_up is not null   '::text
                end ||
                case
                    when new_1.z_invert_down is not null then ''::text
                    else '   z_invert_down is not null   '::text
                end) ||
                case
                    when new_1.h_sable is not null then ''::text
                    else '   h_sable is not null   '::text
                end) ||
                case
                    when new_1.cross_section_type is not null then ''::text
                    else '   cross_section_type is not null   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'valley'::hydra_cross_section_type then ''::text
                    else '   cross_section_type not in (''valley'')   '::text
                end) ||
                case
                    when new_1.rk is not null then ''::text
                    else '   rk is not null   '::text
                end) ||
                case
                    when new_1.rk > 0::double precision then ''::text
                    else '   rk >0   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'circular'::hydra_cross_section_type or new_1.circular_diameter is not null and new_1.circular_diameter > 0::double precision then ''::text
                    else '   cross_section_type!=''circular'' or (circular_diameter is not null and circular_diameter > 0)   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type or new_1.ovoid_top_diameter is not null and new_1.ovoid_top_diameter > 0::double precision then ''::text
                    else '   cross_section_type!=''ovoid'' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type or new_1.ovoid_invert_diameter is not null and new_1.ovoid_invert_diameter > 0::double precision then ''::text
                    else '   cross_section_type!=''ovoid'' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type or new_1.ovoid_height is not null and new_1.ovoid_height > 0::double precision then ''::text
                    else '   cross_section_type!=''ovoid'' or (ovoid_height is not null and ovoid_height >0 )   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type or new_1.ovoid_height > ((new_1.ovoid_top_diameter + new_1.ovoid_invert_diameter) / 2::double precision) then ''::text
                    else '   cross_section_type!=''ovoid'' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'pipe'::hydra_cross_section_type or new_1.cp_geom is not null then ''::text
                    else '   cross_section_type!=''pipe'' or cp_geom is not null   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'channel'::hydra_cross_section_type or new_1.op_geom is not null then ''::text
                    else '   cross_section_type!=''channel'' or op_geom is not null   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.pipe_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((
                case
                    when new_1.z_overflow is not null then ''::text
                    else '   z_overflow is not null   '::text
                end ||
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end) ||
                case
                    when new_1.area >= 0::double precision then ''::text
                    else '   area>=0   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.network_overflow_link new_1
          where not new_1.validity
        ), singularity_invalidity_reason as (
         select new_1.id,
            ((
                case
                    when new_1.zq_array is not null then ''::text
                    else '   zq_array is not null   '::text
                end ||
                case
                    when array_length(new_1.zq_array, 1) <= 20 then ''::text
                    else '   array_length(zq_array, 1)<=20   '::text
                end) ||
                case
                    when array_length(new_1.zq_array, 1) >= 1 then ''::text
                    else '   array_length(zq_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zq_array, 2) = 2 then ''::text
                    else '   array_length(zq_array, 2)=2   '::text
                end as reason
           from ${model}.zq_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((
                case
                    when new_1.q_dz_array is not null then ''::text
                    else '   q_dz_array is not null   '::text
                end ||
                case
                    when array_length(new_1.q_dz_array, 1) <= 10 then ''::text
                    else '   array_length(q_dz_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.q_dz_array, 1) >= 1 then ''::text
                    else '   array_length(q_dz_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.q_dz_array, 2) = 2 then ''::text
                    else '   array_length(q_dz_array, 2)=2   '::text
                end) ||
                case
                    when new_1.q_dz_array[1][1] = 0::double precision then ''::text
                    else '   q_dz_array[1][1]=0   '::text
                end) ||
                case
                    when new_1.q_dz_array[1][2] = 0::double precision then ''::text
                    else '   q_dz_array[1][2]=0   '::text
                end) ||
                case
                    when not ${model}.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from ${model}.param_headloss_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((
                case
                    when new_1.cascade_mode is not null then ''::text
                    else '   cascade_mode is not null   '::text
                end ||
                case
                    when new_1.cascade_mode = 'hydrograph'::hydra_model_connect_mode or new_1.zq_array is not null then ''::text
                    else '   cascade_mode=''hydrograph'' or zq_array is not null   '::text
                end) ||
                case
                    when array_length(new_1.zq_array, 1) <= 20 then ''::text
                    else '   array_length(zq_array, 1)<=20   '::text
                end) ||
                case
                    when array_length(new_1.zq_array, 1) >= 1 then ''::text
                    else '   array_length(zq_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zq_array, 2) = 2 then ''::text
                    else '   array_length(zq_array, 2)=2   '::text
                end) ||
                case
                    when array_length(new_1.quality, 1) = 9 then ''::text
                    else '   array_length(quality, 1)=9   '::text
                end as reason
           from ${model}.model_connect_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((
                case
                    when new_1.zs_array is not null then ''::text
                    else '   zs_array is not null    '::text
                end ||
                case
                    when new_1.zini is not null then ''::text
                    else '   zini is not null   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) <= 10 then ''::text
                    else '   array_length(zs_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) >= 1 then ''::text
                    else '   array_length(zs_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 2) = 2 then ''::text
                    else '   array_length(zs_array, 2)=2   '::text
                end) ||
                case
                    when new_1.treatment_mode is not null then ''::text
                    else '   treatment_mode is not null   '::text
                end as reason
           from ${model}.tank_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((
                case
                    when new_1.storage_area is not null then ''::text
                    else '   storage_area is not null   '::text
                end ||
                case
                    when new_1.storage_area >= 0::double precision then ''::text
                    else '   storage_area>=0   '::text
                end) ||
                case
                    when new_1.constant_dry_flow is not null then ''::text
                    else '   constant_dry_flow is not null   '::text
                end) ||
                case
                    when new_1.constant_dry_flow >= 0::double precision then ''::text
                    else '   constant_dry_flow>=0   '::text
                end) ||
                case
                    when new_1.distrib_coef is null or new_1.distrib_coef >= 0::double precision then ''::text
                    else '   distrib_coef is null or distrib_coef>=0   '::text
                end) ||
                case
                    when new_1.lag_time_hr is null or new_1.lag_time_hr >= 0::double precision then ''::text
                    else '   lag_time_hr is null or (lag_time_hr>=0)   '::text
                end) ||
                case
                    when new_1.sector is null or new_1.distrib_coef is not null and new_1.lag_time_hr is not null then ''::text
                    else '   sector is null or (distrib_coef is not null and lag_time_hr is not null)   '::text
                end) ||
                case
                    when array_length(new_1.pollution_dryweather_runoff, 1) = 2 then ''::text
                    else '   array_length(pollution_dryweather_runoff, 1)=2   '::text
                end) ||
                case
                    when array_length(new_1.pollution_dryweather_runoff, 2) = 4 then ''::text
                    else '   array_length(pollution_dryweather_runoff, 2)=4   '::text
                end) ||
                case
                    when array_length(new_1.quality_dryweather_runoff, 1) = 2 then ''::text
                    else '   array_length(quality_dryweather_runoff, 1)=2   '::text
                end) ||
                case
                    when array_length(new_1.quality_dryweather_runoff, 2) = 9 then ''::text
                    else '   array_length(quality_dryweather_runoff, 2)=9   '::text
                end) ||
                case
                    when new_1.external_file_data or new_1.tq_array is not null then ''::text
                    else '   external_file_data or tq_array is not null   '::text
                end) ||
                case
                    when new_1.external_file_data or array_length(new_1.tq_array, 1) <= 10 then ''::text
                    else '   external_file_data or array_length(tq_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.external_file_data or array_length(new_1.tq_array, 1) >= 1 then ''::text
                    else '   external_file_data or array_length(tq_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.external_file_data or array_length(new_1.tq_array, 2) = 2 then ''::text
                    else '   external_file_data or array_length(tq_array, 2)=2   '::text
                end as reason
           from ${model}.hydrograph_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((
                case
                    when new_1.drainage is not null then ''::text
                    else '   drainage is not null   '::text
                end ||
                case
                    when new_1.overflow is not null then ''::text
                    else '   overflow is not null   '::text
                end) ||
                case
                    when new_1.z_ini is not null then ''::text
                    else '   z_ini is not null   '::text
                end) ||
                case
                    when new_1.zr_sr_qf_qs_array is not null then ''::text
                    else '   zr_sr_qf_qs_array is not null    '::text
                end) ||
                case
                    when new_1.treatment_mode is not null then ''::text
                    else '   treatment_mode is not null   '::text
                end) ||
                case
                    when new_1.treatment_param is not null then ''::text
                    else '   treatment_param is not null   '::text
                end) ||
                case
                    when array_length(new_1.zr_sr_qf_qs_array, 1) <= 10 then ''::text
                    else '   array_length(zr_sr_qf_qs_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.zr_sr_qf_qs_array, 1) >= 1 then ''::text
                    else '   array_length(zr_sr_qf_qs_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zr_sr_qf_qs_array, 2) = 4 then ''::text
                    else '   array_length(zr_sr_qf_qs_array, 2)=4   '::text
                end as reason
           from ${model}.reservoir_rsp_hydrology_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((
                case
                    when new_1.d_abutment_l is not null then ''::text
                    else '   d_abutment_l is not null   '::text
                end ||
                case
                    when new_1.d_abutment_r is not null then ''::text
                    else '   d_abutment_r is not null   '::text
                end) ||
                case
                    when new_1.abutment_type is not null then ''::text
                    else '   abutment_type is not null   '::text
                end) ||
                case
                    when new_1.zw_array is not null then ''::text
                    else '   zw_array is not null    '::text
                end) ||
                case
                    when new_1.z_ceiling is not null then ''::text
                    else '   z_ceiling is not null   '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 1) <= 10 then ''::text
                    else '   array_length(zw_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 1) >= 1 then ''::text
                    else '   array_length(zw_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 2) = 2 then ''::text
                    else '   array_length(zw_array, 2)=2   '::text
                end) ||
                case
                    when not ${model}.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from ${model}.bradley_headloss_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((
                case
                    when new_1.l_road is not null then ''::text
                    else '   l_road is not null   '::text
                end ||
                case
                    when new_1.l_road >= 0::double precision then ''::text
                    else '   l_road>=0   '::text
                end) ||
                case
                    when new_1.z_road is not null then ''::text
                    else '   z_road is not null   '::text
                end) ||
                case
                    when new_1.zw_array is not null then ''::text
                    else '   zw_array is not null    '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 1) <= 10 then ''::text
                    else '   array_length(zw_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 1) >= 1 then ''::text
                    else '   array_length(zw_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 2) = 2 then ''::text
                    else '   array_length(zw_array, 2)=2   '::text
                end) ||
                case
                    when not ${model}.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from ${model}.bridge_headloss_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (
                case
                    when new_1.law_type is not null then ''::text
                    else '   law_type is not null   '::text
                end ||
                case
                    when new_1.param is not null then ''::text
                    else '   param is not null   '::text
                end) ||
                case
                    when not ${model}.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from ${model}.borda_headloss_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((
                case
                    when new_1.downstream is not null then ''::text
                    else '   downstream is not null   '::text
                end ||
                case
                    when new_1.split1 is not null then ''::text
                    else '   split1 is not null   '::text
                end) ||
                case
                    when new_1.downstream_param is not null then ''::text
                    else '   downstream_param is not null   '::text
                end) ||
                case
                    when new_1.split1_law is not null then ''::text
                    else '   split1_law is not null   '::text
                end) ||
                case
                    when new_1.split1_param is not null then ''::text
                    else '   split1_param is not null   '::text
                end) ||
                case
                    when new_1.split2 is null or new_1.split2_law is not null then ''::text
                    else '   split2 is null or split2_law is not null   '::text
                end) ||
                case
                    when new_1.split2 is null or new_1.split2_param is not null then ''::text
                    else '   split2 is null or split2_param is not null   '::text
                end as reason
           from ${model}.zq_split_hydrology_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.z_ceiling is not null then ''::text
                    else '   z_ceiling is not null   '::text
                end) ||
                case
                    when new_1.z_ceiling > new_1.z_invert then ''::text
                    else '   z_ceiling>z_invert   '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc <=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc >=0   '::text
                end) ||
                case
                    when new_1.action_gate_type is not null then ''::text
                    else '   action_gate_type is not null   '::text
                end) ||
                case
                    when new_1.mode_valve is not null then ''::text
                    else '   mode_valve is not null   '::text
                end) ||
                case
                    when new_1.z_gate is not null then ''::text
                    else '   z_gate is not null   '::text
                end) ||
                case
                    when new_1.z_gate >= new_1.z_invert then ''::text
                    else '   z_gate>=z_invert   '::text
                end) ||
                case
                    when new_1.z_gate <= new_1.z_ceiling then ''::text
                    else '   z_gate<=z_ceiling   '::text
                end) ||
                case
                    when new_1.v_max_cms is not null then ''::text
                    else '   v_max_cms is not null   '::text
                end) ||
                case
                    when not ${model}.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end) ||
                case
                    when new_1.cc_submerged > 0::double precision then ''::text
                    else '   cc_submerged >0   '::text
                end as reason
           from ${model}.gate_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
                case
                    when new_1.q0 is not null then ''::text
                    else '   q0 is not null   '::text
                end as reason
           from ${model}.constant_inflow_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((
                case
                    when new_1.qq_array is not null then ''::text
                    else '   qq_array is not null   '::text
                end ||
                case
                    when new_1.downstream is not null then ''::text
                    else '   downstream is not null   '::text
                end) ||
                case
                    when new_1.split1 is not null then ''::text
                    else '   split1 is not null   '::text
                end) ||
                case
                    when array_length(new_1.qq_array, 1) <= 10 then ''::text
                    else '   array_length(qq_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.qq_array, 1) >= 1 then ''::text
                    else '   array_length(qq_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.split2 is not null and array_length(new_1.qq_array, 2) = 3 or new_1.split2 is null and array_length(new_1.qq_array, 2) = 2 then ''::text
                    else '   (split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2)   '::text
                end as reason
           from ${model}.qq_split_hydrology_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((
                case
                    when new_1.drainage is not null then ''::text
                    else '   drainage is not null   '::text
                end ||
                case
                    when new_1.overflow is not null then ''::text
                    else '   overflow is not null   '::text
                end) ||
                case
                    when new_1.q_drainage is not null then ''::text
                    else '   q_drainage is not null   '::text
                end) ||
                case
                    when new_1.q_drainage >= 0::double precision then ''::text
                    else '   q_drainage>=0   '::text
                end) ||
                case
                    when new_1.z_ini is not null then ''::text
                    else '   z_ini is not null   '::text
                end) ||
                case
                    when new_1.zs_array is not null then ''::text
                    else '   zs_array is not null    '::text
                end) ||
                case
                    when new_1.treatment_mode is not null then ''::text
                    else '   treatment_mode is not null   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) <= 10 then ''::text
                    else '   array_length(zs_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) >= 1 then ''::text
                    else '   array_length(zs_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 2) = 2 then ''::text
                    else '   array_length(zs_array, 2)=2   '::text
                end as reason
           from ${model}.reservoir_rs_hydrology_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((
                case
                    when new_1.z_weir is not null then ''::text
                    else '   z_weir is not null   '::text
                end ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= '0'::numeric::double precision then ''::text
                    else '   cc>=0.   '::text
                end as reason
           from ${model}.weir_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (
                case
                    when new_1.pk0_km is not null then ''::text
                    else '   pk0_km is not null   '::text
                end ||
                case
                    when new_1.dx is not null then ''::text
                    else '   dx is not null   '::text
                end) ||
                case
                    when new_1.dx >= 0.1::double precision then ''::text
                    else '   dx>=0.1   '::text
                end as reason
           from ${model}.pipe_branch_marker_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null    '::text
                end ||
                case
                    when new_1.z_regul is not null then ''::text
                    else '   z_regul is not null    '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc>=0   '::text
                end) ||
                case
                    when new_1.mode_regul is not null then ''::text
                    else '   mode_regul is not null   '::text
                end) ||
                case
                    when new_1.reoxy_law is not null then ''::text
                    else '   reoxy_law is not null   '::text
                end) ||
                case
                    when new_1.reoxy_param is not null then ''::text
                    else '   reoxy_param is not null   '::text
                end) ||
                case
                    when not ${model}.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from ${model}.zregul_weir_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.qz_array is not null then ''::text
                    else '   qz_array is not null   '::text
                end ||
                case
                    when array_length(new_1.qz_array, 1) <= 10 then ''::text
                    else '   array_length(qz_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.qz_array, 1) >= 1 then ''::text
                    else '   array_length(qz_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.qz_array, 2) = 2 then ''::text
                    else '   array_length(qz_array, 2)=2   '::text
                end) ||
                case
                    when not ${model}.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from ${model}.hydraulic_cut_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.slope is not null then ''::text
                    else '   slope is not null   '::text
                end ||
                case
                    when new_1.k is not null then ''::text
                    else '   k is not null   '::text
                end) ||
                case
                    when new_1.k > 0::double precision then ''::text
                    else '   k>0   '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end as reason
           from ${model}.strickler_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.z_ceiling is not null then ''::text
                    else '   z_ceiling is not null   '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width >=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc <=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc >=0   '::text
                end) ||
                case
                    when new_1.action_gate_type is not null then ''::text
                    else '   action_gate_type is not null   '::text
                end) ||
                case
                    when new_1.z_invert_stop is not null then ''::text
                    else '   z_invert_stop is not null   '::text
                end) ||
                case
                    when new_1.z_ceiling_stop is not null then ''::text
                    else '   z_ceiling_stop is not null   '::text
                end) ||
                case
                    when new_1.v_max_cms is not null then ''::text
                    else '   v_max_cms is not null   '::text
                end) ||
                case
                    when new_1.v_max_cms >= 0::double precision then ''::text
                    else '   v_max_cms>=0   '::text
                end) ||
                case
                    when new_1.dt_regul_hr is not null then ''::text
                    else '   dt_regul_hr is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_control_node is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_pid_array is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_tz_array is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 1) <= 10 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 1) >= 1 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 2) = 2 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or new_1.q_z_crit is not null then ''::text
                    else '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 1) <= 10 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 1) >= 1 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 2) = 2 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul or new_1.nr_z_gate is not null then ''::text
                    else '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                end) ||
                case
                    when new_1.cc_submerged > 0::double precision then ''::text
                    else '   cc_submerged >0   '::text
                end as reason
           from ${model}.regul_sluice_gate_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((
                case
                    when new_1.external_file_data or new_1.tz_array is not null then ''::text
                    else '   external_file_data or tz_array is not null   '::text
                end ||
                case
                    when new_1.external_file_data or array_length(new_1.tz_array, 1) <= 10 then ''::text
                    else '   external_file_data or array_length(tz_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.external_file_data or array_length(new_1.tz_array, 1) >= 1 then ''::text
                    else '   external_file_data or array_length(tz_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.external_file_data or array_length(new_1.tz_array, 2) = 2 then ''::text
                    else '   external_file_data or array_length(tz_array, 2)=2   '::text
                end as reason
           from ${model}.tz_bc_singularity new_1
          where not new_1.validity
        )
    select CONCAT('node:', n.id) as
        id,
        validity,
        name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom,
        reason from $model._node as n join node_invalidity_reason as r on r.id=n.id
        where validity=FALSE
    union
    select CONCAT('link:', l.id) as
        id,
        validity,
        name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom,
        reason from $model._link as l join link_invalidity_reason as r on  r.id=l.id
        where validity=FALSE
    union
    select CONCAT('singularity:', s.id) as
        id,
        s.validity,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        reason from $model._singularity as s join $model._node as n on s.node = n.id join singularity_invalidity_reason as r on r.id=s.id
        where s.validity=FALSE
    union
    select CONCAT('profile:', p.id) as
        id,
        p.validity,
        p.name,
        'profile'::varchar as type,
        ''::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        ''::varchar as reason from $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where p.validity=FALSE
;;

-- Pipes
create materialized view ${model}.results_pipe as
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.pipe_link
    where up_type='manhole' and down_type='manhole'
with no data;
;;

-- Hydrol pipes
create materialized view ${model}.results_pipe_hydrol as
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.pipe_link
    where up_type='manhole_hydrology' and down_type='manhole_hydrology'
with no data;
;;

create or replace function $model.branch_update_fct()
returns boolean
language plpgsql
as $$$$
    begin
        update $model._pipe_link set branch = null;
        delete from $model.branch;

        -- count up and down node connectivity on pipes
        -- select pass_through manhole nodes that have one up and one down
        -- line merge pipes that have pass_through at both ends
        -- add pipes that are touching

        with up as (
            select n.id, count(1) as ct, n.geom
            from $model.pipe_link as l join $model.manhole_node as n on n.id=l.up
            where not l.exclude_from_branch
            group by n.id, n.geom
        ),
        down as (
            select n.id, count(1) as ct
            from $model.pipe_link as l join $model.manhole_node as n on n.id=l.down
            where not l.exclude_from_branch
            group by n.id
        ),
        blade as (
            select st_collect(n.geom) as geom
            from $model.manhole_node as n
            where id in (
            select id from $model.manhole_node
            except
            select up.id
            from up join down on up.id=down.id
            where up.ct = 1
            and down.ct = 1)
        ),
        branch as (
            select (st_dump(st_split(st_linemerge(st_collect(l.geom)), b.geom))).geom as geom
            from $model.pipe_link as l, blade as b
            where l.up_type='manhole' and not l.exclude_from_branch
            group by b.geom
        ),
        oriented as (
            select (st_dump(st_linemerge(st_collect(p.geom)))).geom as geom
            from branch as b,  $model.pipe_link as p
            where st_covers(b.geom, p.geom) and not p.exclude_from_branch
            group by b.geom
        )
        insert into $model.branch(id, name, geom)
        select row_number() over(), 'BRANCH_'||(row_number() over())::varchar, geom
        from oriented
        ;

        update $model.branch as b set name=m.name, dx=m.dx, pk0_km=m.pk0_km
        from $model.pipe_branch_marker_singularity as m where st_intersects(m.geom, b.geom);

        update $model._pipe_link as p set branch = b.id
        from $model._link as l, $model.branch as b
        where p.id = l.id and st_covers(b.geom, l.geom) and not p.exclude_from_branch;

        return 't';
    end;
$$$$
;;

/* ********************************************** */
/*              Regulated items                   */
/* ********************************************** */

create or replace view $model.regulated as
    select distinct on (name)
        CONCAT(type, ':', r.id) as orig_id,
        r.type,
        r.subtype,
        r.name,
        r.geom
    from project.regulated() as r
    where model = '$model'
;;

create or replace view $model.regulated_detailed as
    select
        row_number() over() as id,
        r.scenario,
        r.file,
        r.iline,
        CONCAT(type, ':', r.id) as orig_id,
        r.type,
        r.subtype,
        r.name,
        r.geom
    from project.regulated() as r
    where model = '$model'
;;
