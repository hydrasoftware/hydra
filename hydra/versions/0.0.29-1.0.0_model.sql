/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  Update triggers at reach creation             */
/* ********************************************** */

create or replace function ${model}.reach_after_insert_fct()
returns trigger
language plpgsql
as $$$$
    declare
        _up boolean;
        _down boolean;
    begin
        select exists(select 1 from $model.river_node as n where ST_Intersects(n.geom, ST_Buffer(ST_StartPoint(new.geom), 0.1))) into _up;
        select exists(select 1 from $model.river_node as n where ST_Intersects(n.geom, ST_Buffer(ST_EndPoint(new.geom), 0.1))) into _down;

        if not _up then
            insert into $model.river_node(geom) values(ST_StartPoint(new.geom));
        end if;
        if not _down then
            insert into $model.river_node(geom) values(ST_EndPoint(new.geom));
        end if;
        return new;
    end;
$$$$
;;

create or replace function ${model}.reach_after_update_fct()
returns trigger
language plpgsql
as $$$$
    begin
        update $model.river_node set
                geom = (select ST_LineInterpolatePoint(new.geom, ST_LineLocatePoint(new.geom, geom)))
                where reach=new.id
                    and id not in (select id from $model.river_node as n where ST_Intersects(n.geom, ST_LineInterpolatePoint(new.geom, ST_LineLocatePoint(new.geom, n.geom))));
        return new;
    end;
$$$$
;;

/* ********************************************** */
/*  Street mechanics update                       */
/* ********************************************** */

ALTER TABLE ONLY $model.street ALTER COLUMN width SET DEFAULT 0;
;;

ALTER TABLE ONLY $model.street ALTER COLUMN elem_length SET DEFAULT 100;
;;

-- remove ueless trigger
drop trigger if exists ${model}_street_after_insert_trig on $model.street;
;;

-- remove useless associated func
drop function if exists ${model}.street_after_insert_fct();
;;

create or replace function ${model}.street_after_fct()
returns trigger
language plpgsql
as $$$$
    declare
        res integer;
    begin
        if new.width != 0 then
            if tg_op = 'INSERT' or (tg_op = 'UPDATE' and old.width=0) then
                perform $model.gen_cst_street(new.id);
            end if;
        end if;

        perform $model.crossroad_update();

        return new;
    end;
$$$$
;;

create trigger ${model}_street_after_trig
    after insert or update of width, geom on $model.street
       for each row execute procedure ${model}.street_after_fct();
;;

create or replace function ${model}.crossroad_update()
returns integer
language plpgsql
as $$$$
    declare
        res_ integer;
    begin
        with nodes as (
            insert into $model.crossroad_node(geom)
                (select distinct s.geom
                    from (select (st_dumppoints(geom)).geom as geom from $model.street) as s
                    left join (select geom from $model.crossroad_node) as c
                    on s.geom=c.geom
                    where c.geom is null)
            returning id
        )
        select count(1) from nodes into res_;
        return res_;
    end;
$$$$
;;

create or replace function ${model}.gen_cst_street(street_id integer)
returns integer
language plpgsql
as $$$$
    declare
        _street_geom geometry;
        _street_width real;
        _street_el real;
    begin
        select geom, width, elem_length
        from $model.street
        where id=street_id
        into _street_geom, _street_width, _street_el;

        with buf as (
                select st_buffer(st_force2d(_street_geom), _street_width/2, 'quad_segs=1 join=mitre mitre_limit='||(_street_width*2)::varchar) as geom
            ),
            cst as (
                select st_exteriorring(geom) as geom from buf
            ),
            snp as (
                update $model.constrain set geom = st_snap(geom, _street_geom, 1)
            ),
            spl as (
                select coalesce(
                    st_split(cst.geom, (select st_collect(st_force2d(c.geom))
                            from $model.constrain as c
                            where st_intersects(c.geom, cst.geom)
                            )), cst.geom) as geom
                from cst
            ),
            spld as (
                select st_force3d((st_dump(geom)).geom) as geom from spl
            ),
            diff as (
                select id, (st_dump(st_collectionextract(st_split(c.geom, b.geom), 2))).geom as geom
                from $model.constrain as c, cst as b
                where st_intersects(b.geom, c.geom)
            ),
            ins as (
                insert into $model.constrain(geom, elem_length, constrain_type)
                select d.geom, c.elem_length, c.constrain_type
                from $model.constrain as c, diff as d
                where c.id = d.id
                union
                select geom, _street_el, 'overflow'
                from spld
                -- where not st_intersects(spld.geom, new.geom)
                returning id
            )
            delete from $model.constrain where id in (select id from diff);
            return 1;
    end;
$$$$
;;

/* update crossroad node view */
create or replace function ${model}.crossroad_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('crossroad', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'crossroad') where name = 'define_later' and id = id_;
            insert into $model._crossroad_node
                values (id_, 'crossroad', coalesce(new.area, 1), coalesce(new.z_ground, (select project.altitude(new.geom))), coalesce(new.h, 0));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'crossroad_node');

            -- Lines to update specific nodes that works with associated contours
            if 'crossroad' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'crossroad' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) and (h is not null) and (h>=0) from  $model._crossroad_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update $model._node set name=new.name, geom=new.geom, configuration=new.configuration::json, generated=new.generated where id=old.id;
            update $model._crossroad_node set area=new.area, z_ground=new.z_ground, h=new.h where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'crossroad_node');

            -- Lines to update specific nodes that works with associated contours
            if 'crossroad' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            if 'crossroad' = 'storage' then
                update $model._storage_node set contour=(select id from $model.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                update $model.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
            end if;
            if 'crossroad' = 'crossroad' and new.generated is null then -- update streets if not group insert from mesh
                perform $model.update_street_links();
            end if;

            update $model._node set validity = (select (area is not null) and ((area > 0)) and (z_ground is not null) and (h is not null) and (h>=0) from  $model._crossroad_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'crossroad' = 'storage' then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;
            if 'crossroad' = 'crossroad' then
                perform $model.update_street_links();
            end if;

            delete from $model._crossroad_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

/* ********************************************** */
/*  Views for csv results display                 */
/* ********************************************** */

-- Pipes
create or replace view ${model}.results_pipe as
    select id, name, geom from ${model}.pipe_link
    where up_type='manhole' and down_type='manhole'
;;

-- Hydrol pipes
create or replace view ${model}.results_pipe_hydrol as
    select id, name, geom from ${model}.pipe_link
    where up_type='manhole_hydrology' and down_type='manhole_hydrology'
;;

-- Catchments
create or replace view ${model}.results_catchment as
    select id, name, geom from ${model}.catchment_node
;;

-- Links
create or replace view ${model}.results_link as
    select id, name, geom from ${model}.gate_link
        union
    select id, name, geom from ${model}.regul_gate_link
        union
    select id, name, geom from ${model}.weir_link
        union
    select id, name, geom from ${model}.borda_headloss_link
        union
    select id, name, geom from ${model}.pump_link
        union
    select id, name, geom from ${model}.deriv_pump_link
        union
    select id, name, geom from ${model}.fuse_spillway_link
        union
    select id, name, geom from ${model}.connector_link
        union
    select id, name, geom from ${model}.strickler_link
        union
    select id, name, geom from ${model}.porous_link
        union
    select id, name, geom from ${model}.overflow_link
        union
    select id, name, geom from ${model}.network_overflow_link
        union
    select id, name, geom from ${model}.mesh_2d_link
        union
    select id, name, geom from ${model}.street_link
;;

-- Surface elements
create or replace view ${model}.results_surface as
    with
        dump as (
            select st_collect(n.geom) as points, s.geom as envelope, s.id
            from ${model}.coverage as s, ${model}.crossroad_node as n
            where s.domain_type='street' and st_intersects(s.geom, n.geom)
            group by s.id
        ),
        voronoi as (
            select st_intersection(st_setsrid((st_dump(st_VoronoiPolygons(dump.points, 0, dump.envelope))).geom, ${srid}), dump.envelope) as geom
            from dump
        )
    select e.id as id, e.name as name, e.contour as geom from ${model}.elem_2d_node as e
        union
    select n.id as id, n.name as name, c.geom as geom from ${model}.storage_node as n join ${model}.coverage as c on n.contour=c.id
        union
    select n.id as id, n.name as name, voronoi.geom as geom from voronoi, ${model}.crossroad_node as n where st_intersects(voronoi.geom, n.geom)
        union
    select n.id as id, n.name as name, c.geom as geom from ${model}.crossroad_node as n, ${model}.coverage as c where c.domain_type='crossroad' and st_intersects(n.geom, c.geom)
    order by id asc;
;;

/* ********************************************** */
/*  Swap A and B in view L2D                      */
/* ********************************************** */

drop view if exists ${model}.l2d;
;;

create or replace view ${model}.l2d
as
with link as (
    select l.name, project.crossed_border(l.geom, n.contour) as geom
    from ${model}._link as l, ${model}.elem_2d_node as n
    where (n.id = l.up or n.id=l.down)
    and l.link_type!='mesh_2d'
    and l.link_type!='network_overflow'
    and st_intersects(l.geom, st_exteriorring(n.contour)))
select name, st_x(st_startpoint(geom)) as xb, st_y(st_startpoint(geom)) as yb, st_x(st_endpoint(geom)) as xa, st_y(st_endpoint(geom)) as ya from link
;;