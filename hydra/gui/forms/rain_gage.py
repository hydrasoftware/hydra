# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..base_dialog import BaseDialog, tr
from qgis.PyQt import uic
from inspect import getargvalues, currentframe
import os

def _quote(value):
    return "'"+value+"'" \
            if (isinstance(value, str) or isinstance(value, str))\
            and ( value == "" or value[0] != "'") and value != "null" else value

class RainGageEditor(BaseDialog):
    def __init__(self, project, geom, id=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "rain_gage.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.__geom = geom
        self.__project = project
        if id==None:
            self.__id = self.add_rain_gage(self.__project, self.__geom,)
        else:
            self.__id = id
            if self.__geom is None:
                geom = self.project.execute("""
                select ST_X(geom), ST_Y(geom), ST_Z(geom)
                from {}.rain_gage
                where id={}""".format("project", self.__id)).fetchone()
        name, comment = self.project.execute("""
            select name, comment
            from {}.rain_gage
            where id={}""".format("project", self.__id)).fetchone()
        self.name.setText(name)
        self.comment.setText(comment)
        self.xcoordinate.setText(str(geom[0]))
        self.ycoordinate.setText(str(geom[1]))
        espg = str(self.__project.srid)
        self.espg.setText("ESPG :" + espg)

    def get_id(self):
        return self.__id

    def save(self):
        self.project.log.notice("saved")
        self.update_rain_gage(self.__project,
                                self.__id,
                                (float(self.xcoordinate.text()),float(self.ycoordinate.text())),
                                self.name.text(), self.comment.toPlainText())
        BaseDialog.save(self)
        self.close()

    def add_rain_gage(self, project, geom, name=None, comment=None):
        geom = "'srid={}; POINTZ({} {} {})'::geometry".format(
                str(project.srid),
                str(geom[0]),
                str(geom[1]),
                str(geom[2]) if len(geom)==3 else '9999'
                ) if geom else None
        argmap = {key: value for key, value in {'geom': geom, 'name': name, 'comment': comment}.items() if value is not None}
        if len(argmap):
            project.execute("""
                    insert into project.rain_gage({}) values({})""".format(
                    ",".join(list(argmap.keys())),
                    ",".join(_quote(str(value)) for value in list(argmap.values()))))
        else:
            raise "geom is None."
        return project.execute( "select max(id) from project.rain_gage").fetchone()[0]

    def update_rain_gage(self, project, id, geom, name=None, comment=None):
        geom = "'srid={}; POINTZ({} {} {})'::geometry".format(
                str(project.srid),
                str(geom[0]),
                str(geom[1]),
                str(geom[2]) if len(geom)==3 else '9999'
                ) if geom else None
        argmap = {'geom': geom, 'name': name, 'comment': comment}
        for key, value in argmap.items():
            if value is not None:
                sql = """update project.rain_gage set {}={} where id={}""".format(key, _quote(str(value)), id)
                project.execute(sql)

