from __future__ import absolute_import
from builtins import str
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
from operator import itemgetter
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QTableWidgetItem
from qgis.PyQt.QtGui import QFont
from hydra.gui.widgets.combo_with_values import ComboWithValues
from hydra.utility.system import open_external

from .base_dialog import BaseDialog, tr


class VisuRegulation(BaseDialog):
    def __init__(self, project, reg_id, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "visu_regulation.ui"), self)

        self.project = project
        self.reg_id = reg_id

        self.name = self.project.execute("""select distinct name from {model}.regulated where id='{reg_id}'""".format(
            model=self.project.get_current_model().name, reg_id=self.reg_id)).fetchone()[0]

        self.btn_openfiles.clicked.connect(self.open_files)

        self.scn_combo = ComboWithValues(self.scn_combo_placeholder)
        scn_id_name = self.project.get_scenarios()
        for scn in sorted(scn_id_name, key=itemgetter(1)):
            self.scn_combo.addItem(scn[1], scn[0])
        self.scn_combo.addItem("All Scenarios", 0)
        if self.project.get_current_scenario():
            self.scn_combo.set_selected_value(self.project.get_current_scenario()[0])
        else:
            self.scn_combo.set_selected_value(0)

        self.scn_combo.currentIndexChanged.connect(self.refresh)

        self.setWindowTitle("Regulation files for : " + self.name)

        self.refresh()

    def refresh(self):
        self.regul_table.setRowCount(0)
        self.regul_table.setColumnCount(4)
        title_columns = [tr('Regulation file'), tr('Scenario'), tr('Line number'), tr('Name')]
        self.regul_table.setHorizontalHeaderLabels(title_columns)

        if self.scn_combo.get_selected_value() == 0:
            where_scn = ""
        else:
            where_scn = " and scenario='{}'".format(self.scn_combo.currentText())

        self.matchs = self.project.execute("""select file, scenario, iline, name from {model}.regulated_detailed
                                        where name='{name}' {where}
                                        order by file, scenario, name , iline
                                        """.format(model=self.project.get_current_model().name,
                                        name=self.name,
                                        where=where_scn
                                        )).fetchall()

        for match in self.matchs:
            row_position = self.regul_table.rowCount()
            self.regul_table.insertRow(row_position)
            self.regul_table.setItem(row_position, 0, QTableWidgetItem(os.path.basename(match[0])))
            self.regul_table.setItem(row_position, 1, QTableWidgetItem(match[1]))
            self.regul_table.setItem(row_position, 2, QTableWidgetItem(str(match[2])))
            self.regul_table.setItem(row_position, 3, QTableWidgetItem(match[3]))

            bold = QFont()
            bold.setBold(True)
            self.regul_table.itemAt(row_position, 0).setFont(bold)
            if self.project.get_current_scenario() and self.project.get_current_scenario()[1] == match[1]:
                self.regul_table.itemAt(row_position, 1).setFont(bold)
        self.regul_table.resizeColumnsToContents()

    def open_files(self):
        ''' open regulation files for the given object'''

        full_path_files = []
        for match in self.matchs:
            if match[0] not in full_path_files:
                full_path_files.append(match[0])

        for files in full_path_files:
            if os.path.isfile(files):
                open_external(files)