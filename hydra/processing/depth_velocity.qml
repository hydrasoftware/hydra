<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" minScale="1e+08" version="3.22.16-Białowieża" maxScale="0" hasScaleBasedVisibilityFlag="0">
  <renderer-3d type="mesh" layer="SCENARIO_1_MODEL1_ecdfc9e3_25ad_4ef7_84e5_55d74daccb9b">
    <symbol type="mesh">
      <data add-back-faces="0" height="0" alt-clamping="relative"/>
      <material ambient="26,26,26,255" specular="255,255,255,255" diffuse="178,178,178,255" shininess="0">
        <data-defined-properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data-defined-properties>
      </material>
      <advanced-settings min-color-ramp-shader="0" wireframe-enabled="0" texture-type="0" wireframe-line-width="1" max-color-ramp-shader="255" vertical-scale="1" renderer-3d-enabled="0" level-of-detail="-99" arrows-spacing="25" smoothed-triangle="0" arrows-fixed-size="0" wireframe-line-color="128,128,128,255" vertical-group-index="-1" arrows-enabled="0" vertical-relative="0" texture-single-color="0,128,0,255">
        <colorrampshader maximumValue="255" classificationMode="1" colorRampType="INTERPOLATED" clip="0" labelPrecision="6" minimumValue="0">
          <rampLegendSettings maximumLabel="" useContinuousLegend="1" direction="0" minimumLabel="" orientation="2" prefix="" suffix="">
            <numericFormat id="basic">
              <Option type="Map">
                <Option type="QChar" name="decimal_separator" value=""/>
                <Option type="int" name="decimals" value="6"/>
                <Option type="int" name="rounding_type" value="0"/>
                <Option type="bool" name="show_plus" value="false"/>
                <Option type="bool" name="show_thousand_separator" value="true"/>
                <Option type="bool" name="show_trailing_zeros" value="false"/>
                <Option type="QChar" name="thousand_separator" value=""/>
              </Option>
            </numericFormat>
          </rampLegendSettings>
        </colorrampshader>
      </advanced-settings>
      <data-defined-properties>
        <Option type="Map">
          <Option type="QString" name="name" value=""/>
          <Option name="properties"/>
          <Option type="QString" name="type" value="collection"/>
        </Option>
      </data-defined-properties>
    </symbol>
  </renderer-3d>
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal end-time-extent="2000-01-01T23:58:47Z" reference-time="2000-01-01T00:00:00Z" matching-method="0" temporal-active="1" start-time-extent="2000-01-01T00:10:58Z"/>
  <customproperties>
    <Option/>
  </customproperties>
  <mesh-renderer-settings>
    <active-dataset-group vector="1" scalar="2"/>
    <scalar-settings group="0" opacity="1" max-val="406.95736694335938" min-val="398.0069580078125" interpolation-method="none">
      <colorrampshader maximumValue="406.95736694335938" classificationMode="1" colorRampType="INTERPOLATED" clip="0" labelPrecision="6" minimumValue="398.0069580078125">
        <colorramp type="gradient" name="[source]">
          <Option type="Map">
            <Option type="QString" name="color1" value="13,8,135,255"/>
            <Option type="QString" name="color2" value="240,249,33,255"/>
            <Option type="QString" name="discrete" value="0"/>
            <Option type="QString" name="rampType" value="gradient"/>
            <Option type="QString" name="stops" value="0.0196078;27,6,141,255:0.0392157;38,5,145,255:0.0588235;47,5,150,255:0.0784314;56,4,154,255:0.0980392;65,4,157,255:0.117647;73,3,160,255:0.137255;81,2,163,255:0.156863;89,1,165,255:0.176471;97,0,167,255:0.196078;105,0,168,255:0.215686;113,0,168,255:0.235294;120,1,168,255:0.254902;128,4,168,255:0.27451;135,7,166,255:0.294118;142,12,164,255:0.313725;149,17,161,255:0.333333;156,23,158,255:0.352941;162,29,154,255:0.372549;168,34,150,255:0.392157;174,40,146,255:0.411765;180,46,141,255:0.431373;186,51,136,255:0.45098;191,57,132,255:0.470588;196,62,127,255:0.490196;201,68,122,255:0.509804;205,74,118,255:0.529412;210,79,113,255:0.54902;214,85,109,255:0.568627;218,91,105,255:0.588235;222,97,100,255:0.607843;226,102,96,255:0.627451;230,108,92,255:0.647059;233,114,87,255:0.666667;237,121,83,255:0.686275;240,127,79,255:0.705882;243,133,75,255:0.72549;245,140,70,255:0.745098;247,147,66,255:0.764706;249,154,62,255:0.784314;251,161,57,255:0.803922;252,168,53,255:0.823529;253,175,49,255:0.843137;254,183,45,255:0.862745;254,190,42,255:0.882353;253,198,39,255:0.901961;252,206,37,255:0.921569;251,215,36,255:0.941176;248,223,37,255:0.960784;246,232,38,255:0.980392;243,240,39,255"/>
          </Option>
          <prop k="color1" v="13,8,135,255"/>
          <prop k="color2" v="240,249,33,255"/>
          <prop k="discrete" v="0"/>
          <prop k="rampType" v="gradient"/>
          <prop k="stops" v="0.0196078;27,6,141,255:0.0392157;38,5,145,255:0.0588235;47,5,150,255:0.0784314;56,4,154,255:0.0980392;65,4,157,255:0.117647;73,3,160,255:0.137255;81,2,163,255:0.156863;89,1,165,255:0.176471;97,0,167,255:0.196078;105,0,168,255:0.215686;113,0,168,255:0.235294;120,1,168,255:0.254902;128,4,168,255:0.27451;135,7,166,255:0.294118;142,12,164,255:0.313725;149,17,161,255:0.333333;156,23,158,255:0.352941;162,29,154,255:0.372549;168,34,150,255:0.392157;174,40,146,255:0.411765;180,46,141,255:0.431373;186,51,136,255:0.45098;191,57,132,255:0.470588;196,62,127,255:0.490196;201,68,122,255:0.509804;205,74,118,255:0.529412;210,79,113,255:0.54902;214,85,109,255:0.568627;218,91,105,255:0.588235;222,97,100,255:0.607843;226,102,96,255:0.627451;230,108,92,255:0.647059;233,114,87,255:0.666667;237,121,83,255:0.686275;240,127,79,255:0.705882;243,133,75,255:0.72549;245,140,70,255:0.745098;247,147,66,255:0.764706;249,154,62,255:0.784314;251,161,57,255:0.803922;252,168,53,255:0.823529;253,175,49,255:0.843137;254,183,45,255:0.862745;254,190,42,255:0.882353;253,198,39,255:0.901961;252,206,37,255:0.921569;251,215,36,255:0.941176;248,223,37,255:0.960784;246,232,38,255:0.980392;243,240,39,255"/>
        </colorramp>
        <item alpha="255" color="#0d0887" label="398.01" value="398.0069580078125"/>
        <item alpha="255" color="#1b068d" label="398.18" value="398.1824558361389"/>
        <item alpha="255" color="#260591" label="398.36" value="398.3579545595062"/>
        <item alpha="255" color="#2f0596" label="398.53" value="398.53345238783265"/>
        <item alpha="255" color="#38049a" label="398.71" value="398.70895111119995"/>
        <item alpha="255" color="#41049d" label="398.88" value="398.88444893952635"/>
        <item alpha="255" color="#4903a0" label="399.06" value="399.0599467678528"/>
        <item alpha="255" color="#5102a3" label="399.24" value="399.235446386261"/>
        <item alpha="255" color="#5901a5" label="399.41" value="399.4109460046692"/>
        <item alpha="255" color="#6100a7" label="399.59" value="399.5864456230774"/>
        <item alpha="255" color="#6900a8" label="399.76" value="399.76193629107667"/>
        <item alpha="255" color="#7100a8" label="399.94" value="399.93743590948486"/>
        <item alpha="255" color="#7801a8" label="400.11" value="400.11293552789306"/>
        <item alpha="255" color="#8004a8" label="400.29" value="400.28843514630125"/>
        <item alpha="255" color="#8707a6" label="400.46" value="400.46393476470945"/>
        <item alpha="255" color="#8e0ca4" label="400.64" value="400.6394343831177"/>
        <item alpha="255" color="#9511a1" label="400.81" value="400.8149250511169"/>
        <item alpha="255" color="#9c179e" label="400.99" value="400.99042466952517"/>
        <item alpha="255" color="#a21d9a" label="401.17" value="401.16592428793336"/>
        <item alpha="255" color="#a82296" label="401.34" value="401.34142390634156"/>
        <item alpha="255" color="#ae2892" label="401.52" value="401.51692352474976"/>
        <item alpha="255" color="#b42e8d" label="401.69" value="401.69242314315795"/>
        <item alpha="255" color="#ba3388" label="401.87" value="401.86792276156615"/>
        <item alpha="255" color="#bf3984" label="402.04" value="402.0434134295654"/>
        <item alpha="255" color="#c43e7f" label="402.22" value="402.2189130479736"/>
        <item alpha="255" color="#c9447a" label="402.39" value="402.3944126663818"/>
        <item alpha="255" color="#cd4a76" label="402.57" value="402.56991228479006"/>
        <item alpha="255" color="#d24f71" label="402.75" value="402.74541190319826"/>
        <item alpha="255" color="#d6556d" label="402.92" value="402.92091152160646"/>
        <item alpha="255" color="#da5b69" label="403.1" value="403.0964021896057"/>
        <item alpha="255" color="#de6164" label="403.27" value="403.2719018080139"/>
        <item alpha="255" color="#e26660" label="403.45" value="403.4474014264221"/>
        <item alpha="255" color="#e66c5c" label="403.62" value="403.6229010448303"/>
        <item alpha="255" color="#e97257" label="403.8" value="403.7984006632385"/>
        <item alpha="255" color="#ed7953" label="403.97" value="403.9739002816467"/>
        <item alpha="255" color="#f07f4f" label="404.15" value="404.14939990005496"/>
        <item alpha="255" color="#f3854b" label="404.32" value="404.3248905680542"/>
        <item alpha="255" color="#f58c46" label="404.5" value="404.5003901864624"/>
        <item alpha="255" color="#f79342" label="404.68" value="404.6758898048706"/>
        <item alpha="255" color="#f99a3e" label="404.85" value="404.8513894232788"/>
        <item alpha="255" color="#fba139" label="405.03" value="405.026889041687"/>
        <item alpha="255" color="#fca835" label="405.2" value="405.2023886600952"/>
        <item alpha="255" color="#fdaf31" label="405.38" value="405.3778793280945"/>
        <item alpha="255" color="#feb72d" label="405.55" value="405.5533789465027"/>
        <item alpha="255" color="#febe2a" label="405.73" value="405.7288785649109"/>
        <item alpha="255" color="#fdc627" label="405.9" value="405.90437818331907"/>
        <item alpha="255" color="#fcce25" label="406.08" value="406.0798778017273"/>
        <item alpha="255" color="#fbd724" label="406.26" value="406.2553774201355"/>
        <item alpha="255" color="#f8df25" label="406.43" value="406.4308680881348"/>
        <item alpha="255" color="#f6e826" label="406.61" value="406.606367706543"/>
        <item alpha="255" color="#f3f027" label="406.78" value="406.7818673249512"/>
        <item alpha="255" color="#f0f921" label="406.96" value="406.9573669433594"/>
        <rampLegendSettings maximumLabel="" useContinuousLegend="1" direction="0" minimumLabel="" orientation="2" prefix="" suffix="">
          <numericFormat id="basic">
            <Option type="Map">
              <Option type="QChar" name="decimal_separator" value=""/>
              <Option type="int" name="decimals" value="6"/>
              <Option type="int" name="rounding_type" value="0"/>
              <Option type="bool" name="show_plus" value="false"/>
              <Option type="bool" name="show_thousand_separator" value="true"/>
              <Option type="bool" name="show_trailing_zeros" value="false"/>
              <Option type="QChar" name="thousand_separator" value=""/>
            </Option>
          </numericFormat>
        </rampLegendSettings>
      </colorrampshader>
      <edge-settings stroke-width-unit="0">
        <mesh-stroke-width ignore-out-of-range="0" minimum-value="398.0069580078125" maximum-width="3" use-absolute-value="0" maximum-value="406.95736694335938" width-varying="0" fixed-width="0.26000000000000001" minimum-width="0.26000000000000001"/>
      </edge-settings>
    </scalar-settings>
    <scalar-settings group="1" opacity="1" max-val="4.2804564505674687" min-val="0" interpolation-method="none">
      <colorrampshader maximumValue="4.2804564505674687" classificationMode="1" colorRampType="INTERPOLATED" clip="0" labelPrecision="6" minimumValue="0">
        <colorramp type="gradient" name="[source]">
          <Option type="Map">
            <Option type="QString" name="color1" value="13,8,135,255"/>
            <Option type="QString" name="color2" value="240,249,33,255"/>
            <Option type="QString" name="discrete" value="0"/>
            <Option type="QString" name="rampType" value="gradient"/>
            <Option type="QString" name="stops" value="0.0196078;27,6,141,255:0.0392157;38,5,145,255:0.0588235;47,5,150,255:0.0784314;56,4,154,255:0.0980392;65,4,157,255:0.117647;73,3,160,255:0.137255;81,2,163,255:0.156863;89,1,165,255:0.176471;97,0,167,255:0.196078;105,0,168,255:0.215686;113,0,168,255:0.235294;120,1,168,255:0.254902;128,4,168,255:0.27451;135,7,166,255:0.294118;142,12,164,255:0.313725;149,17,161,255:0.333333;156,23,158,255:0.352941;162,29,154,255:0.372549;168,34,150,255:0.392157;174,40,146,255:0.411765;180,46,141,255:0.431373;186,51,136,255:0.45098;191,57,132,255:0.470588;196,62,127,255:0.490196;201,68,122,255:0.509804;205,74,118,255:0.529412;210,79,113,255:0.54902;214,85,109,255:0.568627;218,91,105,255:0.588235;222,97,100,255:0.607843;226,102,96,255:0.627451;230,108,92,255:0.647059;233,114,87,255:0.666667;237,121,83,255:0.686275;240,127,79,255:0.705882;243,133,75,255:0.72549;245,140,70,255:0.745098;247,147,66,255:0.764706;249,154,62,255:0.784314;251,161,57,255:0.803922;252,168,53,255:0.823529;253,175,49,255:0.843137;254,183,45,255:0.862745;254,190,42,255:0.882353;253,198,39,255:0.901961;252,206,37,255:0.921569;251,215,36,255:0.941176;248,223,37,255:0.960784;246,232,38,255:0.980392;243,240,39,255"/>
          </Option>
          <prop k="color1" v="13,8,135,255"/>
          <prop k="color2" v="240,249,33,255"/>
          <prop k="discrete" v="0"/>
          <prop k="rampType" v="gradient"/>
          <prop k="stops" v="0.0196078;27,6,141,255:0.0392157;38,5,145,255:0.0588235;47,5,150,255:0.0784314;56,4,154,255:0.0980392;65,4,157,255:0.117647;73,3,160,255:0.137255;81,2,163,255:0.156863;89,1,165,255:0.176471;97,0,167,255:0.196078;105,0,168,255:0.215686;113,0,168,255:0.235294;120,1,168,255:0.254902;128,4,168,255:0.27451;135,7,166,255:0.294118;142,12,164,255:0.313725;149,17,161,255:0.333333;156,23,158,255:0.352941;162,29,154,255:0.372549;168,34,150,255:0.392157;174,40,146,255:0.411765;180,46,141,255:0.431373;186,51,136,255:0.45098;191,57,132,255:0.470588;196,62,127,255:0.490196;201,68,122,255:0.509804;205,74,118,255:0.529412;210,79,113,255:0.54902;214,85,109,255:0.568627;218,91,105,255:0.588235;222,97,100,255:0.607843;226,102,96,255:0.627451;230,108,92,255:0.647059;233,114,87,255:0.666667;237,121,83,255:0.686275;240,127,79,255:0.705882;243,133,75,255:0.72549;245,140,70,255:0.745098;247,147,66,255:0.764706;249,154,62,255:0.784314;251,161,57,255:0.803922;252,168,53,255:0.823529;253,175,49,255:0.843137;254,183,45,255:0.862745;254,190,42,255:0.882353;253,198,39,255:0.901961;252,206,37,255:0.921569;251,215,36,255:0.941176;248,223,37,255:0.960784;246,232,38,255:0.980392;243,240,39,255"/>
        </colorramp>
        <item alpha="255" color="#0d0887" label="0" value="0"/>
        <item alpha="255" color="#1b068d" label="0.0839" value="0.0839303339914368"/>
        <item alpha="255" color="#260591" label="0.168" value="0.167861096028519"/>
        <item alpha="255" color="#2f0596" label="0.252" value="0.251791430019956"/>
        <item alpha="255" color="#38049a" label="0.336" value="0.335722192057037"/>
        <item alpha="255" color="#41049d" label="0.42" value="0.419652526048474"/>
        <item alpha="255" color="#4903a0" label="0.504" value="0.503582860039911"/>
        <item alpha="255" color="#5102a3" label="0.588" value="0.587514050122638"/>
        <item alpha="255" color="#5901a5" label="0.671" value="0.671445240205365"/>
        <item alpha="255" color="#6100a7" label="0.755" value="0.755376430288092"/>
        <item alpha="255" color="#6900a8" label="0.839" value="0.839303339914368"/>
        <item alpha="255" color="#7100a8" label="0.923" value="0.923234529997095"/>
        <item alpha="255" color="#7801a8" label="1.01" value="1.007165720079822"/>
        <item alpha="255" color="#8004a8" label="1.09" value="1.091096910162549"/>
        <item alpha="255" color="#8707a6" label="1.18" value="1.175028100245276"/>
        <item alpha="255" color="#8e0ca4" label="1.26" value="1.258959290328003"/>
        <item alpha="255" color="#9511a1" label="1.34" value="1.342886199954279"/>
        <item alpha="255" color="#9c179e" label="1.43" value="1.426817390037006"/>
        <item alpha="255" color="#a21d9a" label="1.51" value="1.510748580119733"/>
        <item alpha="255" color="#a82296" label="1.59" value="1.59467977020246"/>
        <item alpha="255" color="#ae2892" label="1.68" value="1.678610960285187"/>
        <item alpha="255" color="#b42e8d" label="1.76" value="1.762542150367914"/>
        <item alpha="255" color="#ba3388" label="1.85" value="1.84647334045064"/>
        <item alpha="255" color="#bf3984" label="1.93" value="1.930400250076917"/>
        <item alpha="255" color="#c43e7f" label="2.01" value="2.014331440159644"/>
        <item alpha="255" color="#c9447a" label="2.1" value="2.098262630242371"/>
        <item alpha="255" color="#cd4a76" label="2.18" value="2.182193820325098"/>
        <item alpha="255" color="#d24f71" label="2.27" value="2.266125010407825"/>
        <item alpha="255" color="#d6556d" label="2.35" value="2.350056200490552"/>
        <item alpha="255" color="#da5b69" label="2.43" value="2.433983110116828"/>
        <item alpha="255" color="#de6164" label="2.52" value="2.517914300199555"/>
        <item alpha="255" color="#e26660" label="2.6" value="2.601845490282282"/>
        <item alpha="255" color="#e66c5c" label="2.69" value="2.68577668036501"/>
        <item alpha="255" color="#e97257" label="2.77" value="2.769707870447736"/>
        <item alpha="255" color="#ed7953" label="2.85" value="2.853639060530463"/>
        <item alpha="255" color="#f07f4f" label="2.94" value="2.93757025061319"/>
        <item alpha="255" color="#f3854b" label="3.02" value="3.021497160239466"/>
        <item alpha="255" color="#f58c46" label="3.11" value="3.105428350322193"/>
        <item alpha="255" color="#f79342" label="3.19" value="3.18935954040492"/>
        <item alpha="255" color="#f99a3e" label="3.27" value="3.273290730487647"/>
        <item alpha="255" color="#fba139" label="3.36" value="3.357221920570373"/>
        <item alpha="255" color="#fca835" label="3.44" value="3.441153110653101"/>
        <item alpha="255" color="#fdaf31" label="3.53" value="3.525080020279377"/>
        <item alpha="255" color="#feb72d" label="3.61" value="3.609011210362104"/>
        <item alpha="255" color="#febe2a" label="3.69" value="3.69294240044483"/>
        <item alpha="255" color="#fdc627" label="3.78" value="3.776873590527558"/>
        <item alpha="255" color="#fcce25" label="3.86" value="3.860804780610285"/>
        <item alpha="255" color="#fbd724" label="3.94" value="3.944735970693011"/>
        <item alpha="255" color="#f8df25" label="4.03" value="4.028662880319288"/>
        <item alpha="255" color="#f6e826" label="4.11" value="4.112594070402015"/>
        <item alpha="255" color="#f3f027" label="4.2" value="4.196525260484742"/>
        <item alpha="255" color="#f0f921" label="4.28" value="4.28045645056747"/>
        <rampLegendSettings maximumLabel="" useContinuousLegend="1" direction="0" minimumLabel="" orientation="2" prefix="" suffix="">
          <numericFormat id="basic">
            <Option type="Map">
              <Option type="QChar" name="decimal_separator" value=""/>
              <Option type="int" name="decimals" value="6"/>
              <Option type="int" name="rounding_type" value="0"/>
              <Option type="bool" name="show_plus" value="false"/>
              <Option type="bool" name="show_thousand_separator" value="true"/>
              <Option type="bool" name="show_trailing_zeros" value="false"/>
              <Option type="QChar" name="thousand_separator" value=""/>
            </Option>
          </numericFormat>
        </rampLegendSettings>
      </colorrampshader>
      <edge-settings stroke-width-unit="0">
        <mesh-stroke-width ignore-out-of-range="0" minimum-value="0" maximum-width="3" use-absolute-value="0" maximum-value="4.2804564505674687" width-varying="0" fixed-width="0.26000000000000001" minimum-width="0.26000000000000001"/>
      </edge-settings>
    </scalar-settings>
    <scalar-settings group="2" opacity="1" max-val="4.2804564505674687" min-val="0" interpolation-method="none">
      <colorrampshader maximumValue="4.2804564505674687" classificationMode="1" colorRampType="INTERPOLATED" clip="0" labelPrecision="6" minimumValue="0">
        <colorramp type="gradient" name="[source]">
          <Option type="Map">
            <Option type="QString" name="color1" value="13,8,135,255"/>
            <Option type="QString" name="color2" value="240,249,33,255"/>
            <Option type="QString" name="discrete" value="0"/>
            <Option type="QString" name="rampType" value="gradient"/>
            <Option type="QString" name="stops" value="0.0196078;27,6,141,255:0.0392157;38,5,145,255:0.0588235;47,5,150,255:0.0784314;56,4,154,255:0.0980392;65,4,157,255:0.117647;73,3,160,255:0.137255;81,2,163,255:0.156863;89,1,165,255:0.176471;97,0,167,255:0.196078;105,0,168,255:0.215686;113,0,168,255:0.235294;120,1,168,255:0.254902;128,4,168,255:0.27451;135,7,166,255:0.294118;142,12,164,255:0.313725;149,17,161,255:0.333333;156,23,158,255:0.352941;162,29,154,255:0.372549;168,34,150,255:0.392157;174,40,146,255:0.411765;180,46,141,255:0.431373;186,51,136,255:0.45098;191,57,132,255:0.470588;196,62,127,255:0.490196;201,68,122,255:0.509804;205,74,118,255:0.529412;210,79,113,255:0.54902;214,85,109,255:0.568627;218,91,105,255:0.588235;222,97,100,255:0.607843;226,102,96,255:0.627451;230,108,92,255:0.647059;233,114,87,255:0.666667;237,121,83,255:0.686275;240,127,79,255:0.705882;243,133,75,255:0.72549;245,140,70,255:0.745098;247,147,66,255:0.764706;249,154,62,255:0.784314;251,161,57,255:0.803922;252,168,53,255:0.823529;253,175,49,255:0.843137;254,183,45,255:0.862745;254,190,42,255:0.882353;253,198,39,255:0.901961;252,206,37,255:0.921569;251,215,36,255:0.941176;248,223,37,255:0.960784;246,232,38,255:0.980392;243,240,39,255"/>
          </Option>
          <prop k="color1" v="13,8,135,255"/>
          <prop k="color2" v="240,249,33,255"/>
          <prop k="discrete" v="0"/>
          <prop k="rampType" v="gradient"/>
          <prop k="stops" v="0.0196078;27,6,141,255:0.0392157;38,5,145,255:0.0588235;47,5,150,255:0.0784314;56,4,154,255:0.0980392;65,4,157,255:0.117647;73,3,160,255:0.137255;81,2,163,255:0.156863;89,1,165,255:0.176471;97,0,167,255:0.196078;105,0,168,255:0.215686;113,0,168,255:0.235294;120,1,168,255:0.254902;128,4,168,255:0.27451;135,7,166,255:0.294118;142,12,164,255:0.313725;149,17,161,255:0.333333;156,23,158,255:0.352941;162,29,154,255:0.372549;168,34,150,255:0.392157;174,40,146,255:0.411765;180,46,141,255:0.431373;186,51,136,255:0.45098;191,57,132,255:0.470588;196,62,127,255:0.490196;201,68,122,255:0.509804;205,74,118,255:0.529412;210,79,113,255:0.54902;214,85,109,255:0.568627;218,91,105,255:0.588235;222,97,100,255:0.607843;226,102,96,255:0.627451;230,108,92,255:0.647059;233,114,87,255:0.666667;237,121,83,255:0.686275;240,127,79,255:0.705882;243,133,75,255:0.72549;245,140,70,255:0.745098;247,147,66,255:0.764706;249,154,62,255:0.784314;251,161,57,255:0.803922;252,168,53,255:0.823529;253,175,49,255:0.843137;254,183,45,255:0.862745;254,190,42,255:0.882353;253,198,39,255:0.901961;252,206,37,255:0.921569;251,215,36,255:0.941176;248,223,37,255:0.960784;246,232,38,255:0.980392;243,240,39,255"/>
        </colorramp>
        <item alpha="255" color="#0d0887" label="0" value="0"/>
        <item alpha="255" color="#1b068d" label="0.0839" value="0.0839303339914368"/>
        <item alpha="255" color="#260591" label="0.168" value="0.167861096028519"/>
        <item alpha="255" color="#2f0596" label="0.252" value="0.251791430019956"/>
        <item alpha="255" color="#38049a" label="0.336" value="0.335722192057037"/>
        <item alpha="255" color="#41049d" label="0.42" value="0.419652526048474"/>
        <item alpha="255" color="#4903a0" label="0.504" value="0.503582860039911"/>
        <item alpha="255" color="#5102a3" label="0.588" value="0.587514050122638"/>
        <item alpha="255" color="#5901a5" label="0.671" value="0.671445240205365"/>
        <item alpha="255" color="#6100a7" label="0.755" value="0.755376430288092"/>
        <item alpha="255" color="#6900a8" label="0.839" value="0.839303339914368"/>
        <item alpha="255" color="#7100a8" label="0.923" value="0.923234529997095"/>
        <item alpha="255" color="#7801a8" label="1.01" value="1.007165720079822"/>
        <item alpha="255" color="#8004a8" label="1.09" value="1.091096910162549"/>
        <item alpha="255" color="#8707a6" label="1.18" value="1.175028100245276"/>
        <item alpha="255" color="#8e0ca4" label="1.26" value="1.258959290328003"/>
        <item alpha="255" color="#9511a1" label="1.34" value="1.342886199954279"/>
        <item alpha="255" color="#9c179e" label="1.43" value="1.426817390037006"/>
        <item alpha="255" color="#a21d9a" label="1.51" value="1.510748580119733"/>
        <item alpha="255" color="#a82296" label="1.59" value="1.59467977020246"/>
        <item alpha="255" color="#ae2892" label="1.68" value="1.678610960285187"/>
        <item alpha="255" color="#b42e8d" label="1.76" value="1.762542150367914"/>
        <item alpha="255" color="#ba3388" label="1.85" value="1.84647334045064"/>
        <item alpha="255" color="#bf3984" label="1.93" value="1.930400250076917"/>
        <item alpha="255" color="#c43e7f" label="2.01" value="2.014331440159644"/>
        <item alpha="255" color="#c9447a" label="2.1" value="2.098262630242371"/>
        <item alpha="255" color="#cd4a76" label="2.18" value="2.182193820325098"/>
        <item alpha="255" color="#d24f71" label="2.27" value="2.266125010407825"/>
        <item alpha="255" color="#d6556d" label="2.35" value="2.350056200490552"/>
        <item alpha="255" color="#da5b69" label="2.43" value="2.433983110116828"/>
        <item alpha="255" color="#de6164" label="2.52" value="2.517914300199555"/>
        <item alpha="255" color="#e26660" label="2.6" value="2.601845490282282"/>
        <item alpha="255" color="#e66c5c" label="2.69" value="2.68577668036501"/>
        <item alpha="255" color="#e97257" label="2.77" value="2.769707870447736"/>
        <item alpha="255" color="#ed7953" label="2.85" value="2.853639060530463"/>
        <item alpha="255" color="#f07f4f" label="2.94" value="2.93757025061319"/>
        <item alpha="255" color="#f3854b" label="3.02" value="3.021497160239466"/>
        <item alpha="255" color="#f58c46" label="3.11" value="3.105428350322193"/>
        <item alpha="255" color="#f79342" label="3.19" value="3.18935954040492"/>
        <item alpha="255" color="#f99a3e" label="3.27" value="3.273290730487647"/>
        <item alpha="255" color="#fba139" label="3.36" value="3.357221920570373"/>
        <item alpha="255" color="#fca835" label="3.44" value="3.441153110653101"/>
        <item alpha="255" color="#fdaf31" label="3.53" value="3.525080020279377"/>
        <item alpha="255" color="#feb72d" label="3.61" value="3.609011210362104"/>
        <item alpha="255" color="#febe2a" label="3.69" value="3.69294240044483"/>
        <item alpha="255" color="#fdc627" label="3.78" value="3.776873590527558"/>
        <item alpha="255" color="#fcce25" label="3.86" value="3.860804780610285"/>
        <item alpha="255" color="#fbd724" label="3.94" value="3.944735970693011"/>
        <item alpha="255" color="#f8df25" label="4.03" value="4.028662880319288"/>
        <item alpha="255" color="#f6e826" label="4.11" value="4.112594070402015"/>
        <item alpha="255" color="#f3f027" label="4.2" value="4.196525260484742"/>
        <item alpha="255" color="#f0f921" label="4.28" value="4.28045645056747"/>
        <rampLegendSettings maximumLabel="" useContinuousLegend="1" direction="0" minimumLabel="" orientation="2" prefix="" suffix="">
          <numericFormat id="basic">
            <Option type="Map">
              <Option type="QChar" name="decimal_separator" value=""/>
              <Option type="int" name="decimals" value="6"/>
              <Option type="int" name="rounding_type" value="0"/>
              <Option type="bool" name="show_plus" value="false"/>
              <Option type="bool" name="show_thousand_separator" value="true"/>
              <Option type="bool" name="show_trailing_zeros" value="false"/>
              <Option type="QChar" name="thousand_separator" value=""/>
            </Option>
          </numericFormat>
        </rampLegendSettings>
      </colorrampshader>
      <edge-settings stroke-width-unit="0">
        <mesh-stroke-width ignore-out-of-range="0" minimum-value="0" maximum-width="3" use-absolute-value="0" maximum-value="4.2804564505674687" width-varying="0" fixed-width="0.26000000000000001" minimum-width="0.26000000000000001"/>
      </edge-settings>
    </scalar-settings>
    <scalar-settings group="3" opacity="1" max-val="5" min-val="0" interpolation-method="none">
      <colorrampshader maximumValue="5" classificationMode="1" colorRampType="INTERPOLATED" clip="1" labelPrecision="6" minimumValue="0">
        <colorramp type="gradient" name="[source]">
          <Option type="Map">
            <Option type="QString" name="color1" value="247,251,255,255"/>
            <Option type="QString" name="color2" value="8,48,107,255"/>
            <Option type="QString" name="discrete" value="0"/>
            <Option type="QString" name="rampType" value="gradient"/>
            <Option type="QString" name="stops" value="0.13;222,235,247,255:0.26;198,219,239,255:0.39;158,202,225,255:0.52;107,174,214,255:0.65;66,146,198,255:0.78;33,113,181,255:0.9;8,81,156,255"/>
          </Option>
          <prop k="color1" v="247,251,255,255"/>
          <prop k="color2" v="8,48,107,255"/>
          <prop k="discrete" v="0"/>
          <prop k="rampType" v="gradient"/>
          <prop k="stops" v="0.13;222,235,247,255:0.26;198,219,239,255:0.39;158,202,225,255:0.52;107,174,214,255:0.65;66,146,198,255:0.78;33,113,181,255:0.9;8,81,156,255"/>
        </colorramp>
        <item alpha="255" color="#f7fbff" label="0.000000" value="0"/>
        <item alpha="255" color="#deebf7" label="0.650000" value="0.65"/>
        <item alpha="255" color="#c6dbef" label="1.300000" value="1.3"/>
        <item alpha="255" color="#9ecae1" label="1.950000" value="1.95"/>
        <item alpha="255" color="#6baed6" label="2.600000" value="2.6"/>
        <item alpha="255" color="#4292c6" label="3.250000" value="3.25"/>
        <item alpha="255" color="#2171b5" label="3.900000" value="3.9"/>
        <item alpha="255" color="#08519c" label="4.500000" value="4.5"/>
        <item alpha="255" color="#08306b" label="5.000000" value="5"/>
        <rampLegendSettings maximumLabel="" useContinuousLegend="1" direction="0" minimumLabel="" orientation="2" prefix="" suffix="">
          <numericFormat id="basic">
            <Option type="Map">
              <Option type="QChar" name="decimal_separator" value=""/>
              <Option type="int" name="decimals" value="6"/>
              <Option type="int" name="rounding_type" value="0"/>
              <Option type="bool" name="show_plus" value="false"/>
              <Option type="bool" name="show_thousand_separator" value="true"/>
              <Option type="bool" name="show_trailing_zeros" value="false"/>
              <Option type="QChar" name="thousand_separator" value=""/>
            </Option>
          </numericFormat>
        </rampLegendSettings>
      </colorrampshader>
      <edge-settings stroke-width-unit="0">
        <mesh-stroke-width ignore-out-of-range="0" minimum-value="0" maximum-width="3" use-absolute-value="0" maximum-value="10" width-varying="0" fixed-width="0.26000000000000001" minimum-width="0.26000000000000001"/>
      </edge-settings>
    </scalar-settings>
    <scalar-settings group="4" opacity="1" max-val="5" min-val="0" interpolation-method="none">
      <colorrampshader maximumValue="5" classificationMode="1" colorRampType="INTERPOLATED" clip="1" labelPrecision="6" minimumValue="0">
        <colorramp type="gradient" name="[source]">
          <Option type="Map">
            <Option type="QString" name="color1" value="247,251,255,255"/>
            <Option type="QString" name="color2" value="8,48,107,255"/>
            <Option type="QString" name="discrete" value="0"/>
            <Option type="QString" name="rampType" value="gradient"/>
            <Option type="QString" name="stops" value="0.13;222,235,247,255:0.26;198,219,239,255:0.39;158,202,225,255:0.52;107,174,214,255:0.65;66,146,198,255:0.78;33,113,181,255:0.9;8,81,156,255"/>
          </Option>
          <prop k="color1" v="247,251,255,255"/>
          <prop k="color2" v="8,48,107,255"/>
          <prop k="discrete" v="0"/>
          <prop k="rampType" v="gradient"/>
          <prop k="stops" v="0.13;222,235,247,255:0.26;198,219,239,255:0.39;158,202,225,255:0.52;107,174,214,255:0.65;66,146,198,255:0.78;33,113,181,255:0.9;8,81,156,255"/>
        </colorramp>
        <item alpha="255" color="#f7fbff" label="0.000000" value="0"/>
        <item alpha="255" color="#deebf7" label="0.650000" value="0.65"/>
        <item alpha="255" color="#c6dbef" label="1.300000" value="1.3"/>
        <item alpha="255" color="#9ecae1" label="1.950000" value="1.95"/>
        <item alpha="255" color="#6baed6" label="2.600000" value="2.6"/>
        <item alpha="255" color="#4292c6" label="3.250000" value="3.25"/>
        <item alpha="255" color="#2171b5" label="3.900000" value="3.9"/>
        <item alpha="255" color="#08519c" label="4.500000" value="4.5"/>
        <item alpha="255" color="#08306b" label="5.000000" value="5"/>
        <rampLegendSettings maximumLabel="" useContinuousLegend="1" direction="0" minimumLabel="" orientation="2" prefix="" suffix="">
          <numericFormat id="basic">
            <Option type="Map">
              <Option type="QChar" name="decimal_separator" value=""/>
              <Option type="int" name="decimals" value="6"/>
              <Option type="int" name="rounding_type" value="0"/>
              <Option type="bool" name="show_plus" value="false"/>
              <Option type="bool" name="show_thousand_separator" value="true"/>
              <Option type="bool" name="show_trailing_zeros" value="false"/>
              <Option type="QChar" name="thousand_separator" value=""/>
            </Option>
          </numericFormat>
        </rampLegendSettings>
      </colorrampshader>
      <edge-settings stroke-width-unit="0">
        <mesh-stroke-width ignore-out-of-range="0" minimum-value="0" maximum-width="3" use-absolute-value="0" maximum-value="10" width-varying="0" fixed-width="0.26000000000000001" minimum-width="0.26000000000000001"/>
      </edge-settings>
    </scalar-settings>
    <vector-settings group="1" color="0,0,0,255" coloring-method="0" filter-max="-1" user-grid-height="10" user-grid-width="10" line-width="0.26000000000000001" filter-min="-1" user-grid-enabled="0" symbology="0">
      <colorrampshader maximumValue="4.2800000000000002" classificationMode="1" colorRampType="INTERPOLATED" clip="0" labelPrecision="6" minimumValue="0">
        <colorramp type="gradient" name="[source]">
          <Option type="Map">
            <Option type="QString" name="color1" value="13,8,135,255"/>
            <Option type="QString" name="color2" value="240,249,33,255"/>
            <Option type="QString" name="discrete" value="0"/>
            <Option type="QString" name="rampType" value="gradient"/>
            <Option type="QString" name="stops" value="0.0196078;27,6,141,255:0.0392157;38,5,145,255:0.0588235;47,5,150,255:0.0784314;56,4,154,255:0.0980392;65,4,157,255:0.117647;73,3,160,255:0.137255;81,2,163,255:0.156863;89,1,165,255:0.176471;97,0,167,255:0.196078;105,0,168,255:0.215686;113,0,168,255:0.235294;120,1,168,255:0.254902;128,4,168,255:0.27451;135,7,166,255:0.294118;142,12,164,255:0.313725;149,17,161,255:0.333333;156,23,158,255:0.352941;162,29,154,255:0.372549;168,34,150,255:0.392157;174,40,146,255:0.411765;180,46,141,255:0.431373;186,51,136,255:0.45098;191,57,132,255:0.470588;196,62,127,255:0.490196;201,68,122,255:0.509804;205,74,118,255:0.529412;210,79,113,255:0.54902;214,85,109,255:0.568627;218,91,105,255:0.588235;222,97,100,255:0.607843;226,102,96,255:0.627451;230,108,92,255:0.647059;233,114,87,255:0.666667;237,121,83,255:0.686275;240,127,79,255:0.705882;243,133,75,255:0.72549;245,140,70,255:0.745098;247,147,66,255:0.764706;249,154,62,255:0.784314;251,161,57,255:0.803922;252,168,53,255:0.823529;253,175,49,255:0.843137;254,183,45,255:0.862745;254,190,42,255:0.882353;253,198,39,255:0.901961;252,206,37,255:0.921569;251,215,36,255:0.941176;248,223,37,255:0.960784;246,232,38,255:0.980392;243,240,39,255"/>
          </Option>
          <prop k="color1" v="13,8,135,255"/>
          <prop k="color2" v="240,249,33,255"/>
          <prop k="discrete" v="0"/>
          <prop k="rampType" v="gradient"/>
          <prop k="stops" v="0.0196078;27,6,141,255:0.0392157;38,5,145,255:0.0588235;47,5,150,255:0.0784314;56,4,154,255:0.0980392;65,4,157,255:0.117647;73,3,160,255:0.137255;81,2,163,255:0.156863;89,1,165,255:0.176471;97,0,167,255:0.196078;105,0,168,255:0.215686;113,0,168,255:0.235294;120,1,168,255:0.254902;128,4,168,255:0.27451;135,7,166,255:0.294118;142,12,164,255:0.313725;149,17,161,255:0.333333;156,23,158,255:0.352941;162,29,154,255:0.372549;168,34,150,255:0.392157;174,40,146,255:0.411765;180,46,141,255:0.431373;186,51,136,255:0.45098;191,57,132,255:0.470588;196,62,127,255:0.490196;201,68,122,255:0.509804;205,74,118,255:0.529412;210,79,113,255:0.54902;214,85,109,255:0.568627;218,91,105,255:0.588235;222,97,100,255:0.607843;226,102,96,255:0.627451;230,108,92,255:0.647059;233,114,87,255:0.666667;237,121,83,255:0.686275;240,127,79,255:0.705882;243,133,75,255:0.72549;245,140,70,255:0.745098;247,147,66,255:0.764706;249,154,62,255:0.784314;251,161,57,255:0.803922;252,168,53,255:0.823529;253,175,49,255:0.843137;254,183,45,255:0.862745;254,190,42,255:0.882353;253,198,39,255:0.901961;252,206,37,255:0.921569;251,215,36,255:0.941176;248,223,37,255:0.960784;246,232,38,255:0.980392;243,240,39,255"/>
        </colorramp>
        <item alpha="255" color="#0d0887" label="0.000000" value="0"/>
        <item alpha="255" color="#1b068d" label="0.083921" value="0.083921384"/>
        <item alpha="255" color="#260591" label="0.167843" value="0.167843196"/>
        <item alpha="255" color="#2f0596" label="0.251765" value="0.25176458"/>
        <item alpha="255" color="#38049a" label="0.335686" value="0.335686392"/>
        <item alpha="255" color="#41049d" label="0.419608" value="0.419607776"/>
        <item alpha="255" color="#4903a0" label="0.503529" value="0.50352916"/>
        <item alpha="255" color="#5102a3" label="0.587451" value="0.5874514"/>
        <item alpha="255" color="#5901a5" label="0.671374" value="0.67137364"/>
        <item alpha="255" color="#6100a7" label="0.755296" value="0.75529588"/>
        <item alpha="255" color="#6900a8" label="0.839214" value="0.83921384"/>
        <item alpha="255" color="#7100a8" label="0.923136" value="0.92313608"/>
        <item alpha="255" color="#7801a8" label="1.007058" value="1.00705832"/>
        <item alpha="255" color="#8004a8" label="1.090981" value="1.09098056"/>
        <item alpha="255" color="#8707a6" label="1.174903" value="1.1749028"/>
        <item alpha="255" color="#8e0ca4" label="1.258825" value="1.25882504"/>
        <item alpha="255" color="#9511a1" label="1.342743" value="1.342743"/>
        <item alpha="255" color="#9c179e" label="1.426665" value="1.42666524"/>
        <item alpha="255" color="#a21d9a" label="1.510587" value="1.51058748"/>
        <item alpha="255" color="#a82296" label="1.594510" value="1.59450972"/>
        <item alpha="255" color="#ae2892" label="1.678432" value="1.67843196"/>
        <item alpha="255" color="#b42e8d" label="1.762354" value="1.7623542"/>
        <item alpha="255" color="#ba3388" label="1.846276" value="1.84627644"/>
        <item alpha="255" color="#bf3984" label="1.930194" value="1.9301944"/>
        <item alpha="255" color="#c43e7f" label="2.014117" value="2.01411664"/>
        <item alpha="255" color="#c9447a" label="2.098039" value="2.09803888"/>
        <item alpha="255" color="#cd4a76" label="2.181961" value="2.18196112"/>
        <item alpha="255" color="#d24f71" label="2.265883" value="2.26588336"/>
        <item alpha="255" color="#d6556d" label="2.349806" value="2.3498056"/>
        <item alpha="255" color="#da5b69" label="2.433724" value="2.43372356"/>
        <item alpha="255" color="#de6164" label="2.517646" value="2.5176458"/>
        <item alpha="255" color="#e26660" label="2.601568" value="2.60156804"/>
        <item alpha="255" color="#e66c5c" label="2.685490" value="2.68549028"/>
        <item alpha="255" color="#e97257" label="2.769413" value="2.76941252"/>
        <item alpha="255" color="#ed7953" label="2.853335" value="2.85333476"/>
        <item alpha="255" color="#f07f4f" label="2.937257" value="2.937257"/>
        <item alpha="255" color="#f3854b" label="3.021175" value="3.02117496"/>
        <item alpha="255" color="#f58c46" label="3.105097" value="3.1050972"/>
        <item alpha="255" color="#f79342" label="3.189019" value="3.18901944"/>
        <item alpha="255" color="#f99a3e" label="3.272942" value="3.27294168"/>
        <item alpha="255" color="#fba139" label="3.356864" value="3.35686392"/>
        <item alpha="255" color="#fca835" label="3.440786" value="3.44078616"/>
        <item alpha="255" color="#fdaf31" label="3.524704" value="3.52470412"/>
        <item alpha="255" color="#feb72d" label="3.608626" value="3.60862636"/>
        <item alpha="255" color="#febe2a" label="3.692549" value="3.6925486"/>
        <item alpha="255" color="#fdc627" label="3.776471" value="3.77647084"/>
        <item alpha="255" color="#fcce25" label="3.860393" value="3.86039308"/>
        <item alpha="255" color="#fbd724" label="3.944315" value="3.94431532"/>
        <item alpha="255" color="#f8df25" label="4.028233" value="4.02823328"/>
        <item alpha="255" color="#f6e826" label="4.112156" value="4.11215552"/>
        <item alpha="255" color="#f3f027" label="4.196078" value="4.196077760000001"/>
        <item alpha="255" color="#f0f921" label="4.280000" value="4.28"/>
        <rampLegendSettings maximumLabel="" useContinuousLegend="1" direction="0" minimumLabel="" orientation="2" prefix="" suffix="">
          <numericFormat id="basic">
            <Option type="Map">
              <Option type="QChar" name="decimal_separator" value=""/>
              <Option type="int" name="decimals" value="6"/>
              <Option type="int" name="rounding_type" value="0"/>
              <Option type="bool" name="show_plus" value="false"/>
              <Option type="bool" name="show_thousand_separator" value="true"/>
              <Option type="bool" name="show_trailing_zeros" value="false"/>
              <Option type="QChar" name="thousand_separator" value=""/>
            </Option>
          </numericFormat>
        </rampLegendSettings>
      </colorrampshader>
      <vector-arrow-settings arrow-head-width-ratio="0.14999999999999999" arrow-head-length-ratio="0.40000000000000002">
        <shaft-length max="10" method="minmax" min="0.80000000000000004"/>
      </vector-arrow-settings>
      <vector-streamline-settings seeding-method="0" seeding-density="0.14999999999999999"/>
      <vector-traces-settings particles-count="1000" maximum-tail-length-unit="0" maximum-tail-length="100"/>
    </vector-settings>
    <vector-settings group="2" color="0,0,0,255" coloring-method="0" filter-max="-1" user-grid-height="10" user-grid-width="10" line-width="0.26000000000000001" filter-min="-1" user-grid-enabled="0" symbology="0">
      <colorrampshader maximumValue="4.2800000000000002" classificationMode="1" colorRampType="INTERPOLATED" clip="0" labelPrecision="6" minimumValue="0">
        <colorramp type="gradient" name="[source]">
          <Option type="Map">
            <Option type="QString" name="color1" value="13,8,135,255"/>
            <Option type="QString" name="color2" value="240,249,33,255"/>
            <Option type="QString" name="discrete" value="0"/>
            <Option type="QString" name="rampType" value="gradient"/>
            <Option type="QString" name="stops" value="0.0196078;27,6,141,255:0.0392157;38,5,145,255:0.0588235;47,5,150,255:0.0784314;56,4,154,255:0.0980392;65,4,157,255:0.117647;73,3,160,255:0.137255;81,2,163,255:0.156863;89,1,165,255:0.176471;97,0,167,255:0.196078;105,0,168,255:0.215686;113,0,168,255:0.235294;120,1,168,255:0.254902;128,4,168,255:0.27451;135,7,166,255:0.294118;142,12,164,255:0.313725;149,17,161,255:0.333333;156,23,158,255:0.352941;162,29,154,255:0.372549;168,34,150,255:0.392157;174,40,146,255:0.411765;180,46,141,255:0.431373;186,51,136,255:0.45098;191,57,132,255:0.470588;196,62,127,255:0.490196;201,68,122,255:0.509804;205,74,118,255:0.529412;210,79,113,255:0.54902;214,85,109,255:0.568627;218,91,105,255:0.588235;222,97,100,255:0.607843;226,102,96,255:0.627451;230,108,92,255:0.647059;233,114,87,255:0.666667;237,121,83,255:0.686275;240,127,79,255:0.705882;243,133,75,255:0.72549;245,140,70,255:0.745098;247,147,66,255:0.764706;249,154,62,255:0.784314;251,161,57,255:0.803922;252,168,53,255:0.823529;253,175,49,255:0.843137;254,183,45,255:0.862745;254,190,42,255:0.882353;253,198,39,255:0.901961;252,206,37,255:0.921569;251,215,36,255:0.941176;248,223,37,255:0.960784;246,232,38,255:0.980392;243,240,39,255"/>
          </Option>
          <prop k="color1" v="13,8,135,255"/>
          <prop k="color2" v="240,249,33,255"/>
          <prop k="discrete" v="0"/>
          <prop k="rampType" v="gradient"/>
          <prop k="stops" v="0.0196078;27,6,141,255:0.0392157;38,5,145,255:0.0588235;47,5,150,255:0.0784314;56,4,154,255:0.0980392;65,4,157,255:0.117647;73,3,160,255:0.137255;81,2,163,255:0.156863;89,1,165,255:0.176471;97,0,167,255:0.196078;105,0,168,255:0.215686;113,0,168,255:0.235294;120,1,168,255:0.254902;128,4,168,255:0.27451;135,7,166,255:0.294118;142,12,164,255:0.313725;149,17,161,255:0.333333;156,23,158,255:0.352941;162,29,154,255:0.372549;168,34,150,255:0.392157;174,40,146,255:0.411765;180,46,141,255:0.431373;186,51,136,255:0.45098;191,57,132,255:0.470588;196,62,127,255:0.490196;201,68,122,255:0.509804;205,74,118,255:0.529412;210,79,113,255:0.54902;214,85,109,255:0.568627;218,91,105,255:0.588235;222,97,100,255:0.607843;226,102,96,255:0.627451;230,108,92,255:0.647059;233,114,87,255:0.666667;237,121,83,255:0.686275;240,127,79,255:0.705882;243,133,75,255:0.72549;245,140,70,255:0.745098;247,147,66,255:0.764706;249,154,62,255:0.784314;251,161,57,255:0.803922;252,168,53,255:0.823529;253,175,49,255:0.843137;254,183,45,255:0.862745;254,190,42,255:0.882353;253,198,39,255:0.901961;252,206,37,255:0.921569;251,215,36,255:0.941176;248,223,37,255:0.960784;246,232,38,255:0.980392;243,240,39,255"/>
        </colorramp>
        <item alpha="255" color="#0d0887" label="0.000000" value="0"/>
        <item alpha="255" color="#1b068d" label="0.083921" value="0.083921384"/>
        <item alpha="255" color="#260591" label="0.167843" value="0.167843196"/>
        <item alpha="255" color="#2f0596" label="0.251765" value="0.25176458"/>
        <item alpha="255" color="#38049a" label="0.335686" value="0.335686392"/>
        <item alpha="255" color="#41049d" label="0.419608" value="0.419607776"/>
        <item alpha="255" color="#4903a0" label="0.503529" value="0.50352916"/>
        <item alpha="255" color="#5102a3" label="0.587451" value="0.5874514"/>
        <item alpha="255" color="#5901a5" label="0.671374" value="0.67137364"/>
        <item alpha="255" color="#6100a7" label="0.755296" value="0.75529588"/>
        <item alpha="255" color="#6900a8" label="0.839214" value="0.83921384"/>
        <item alpha="255" color="#7100a8" label="0.923136" value="0.92313608"/>
        <item alpha="255" color="#7801a8" label="1.007058" value="1.00705832"/>
        <item alpha="255" color="#8004a8" label="1.090981" value="1.09098056"/>
        <item alpha="255" color="#8707a6" label="1.174903" value="1.1749028"/>
        <item alpha="255" color="#8e0ca4" label="1.258825" value="1.25882504"/>
        <item alpha="255" color="#9511a1" label="1.342743" value="1.342743"/>
        <item alpha="255" color="#9c179e" label="1.426665" value="1.42666524"/>
        <item alpha="255" color="#a21d9a" label="1.510587" value="1.51058748"/>
        <item alpha="255" color="#a82296" label="1.594510" value="1.59450972"/>
        <item alpha="255" color="#ae2892" label="1.678432" value="1.67843196"/>
        <item alpha="255" color="#b42e8d" label="1.762354" value="1.7623542"/>
        <item alpha="255" color="#ba3388" label="1.846276" value="1.84627644"/>
        <item alpha="255" color="#bf3984" label="1.930194" value="1.9301944"/>
        <item alpha="255" color="#c43e7f" label="2.014117" value="2.01411664"/>
        <item alpha="255" color="#c9447a" label="2.098039" value="2.09803888"/>
        <item alpha="255" color="#cd4a76" label="2.181961" value="2.18196112"/>
        <item alpha="255" color="#d24f71" label="2.265883" value="2.26588336"/>
        <item alpha="255" color="#d6556d" label="2.349806" value="2.3498056"/>
        <item alpha="255" color="#da5b69" label="2.433724" value="2.43372356"/>
        <item alpha="255" color="#de6164" label="2.517646" value="2.5176458"/>
        <item alpha="255" color="#e26660" label="2.601568" value="2.60156804"/>
        <item alpha="255" color="#e66c5c" label="2.685490" value="2.68549028"/>
        <item alpha="255" color="#e97257" label="2.769413" value="2.76941252"/>
        <item alpha="255" color="#ed7953" label="2.853335" value="2.85333476"/>
        <item alpha="255" color="#f07f4f" label="2.937257" value="2.937257"/>
        <item alpha="255" color="#f3854b" label="3.021175" value="3.02117496"/>
        <item alpha="255" color="#f58c46" label="3.105097" value="3.1050972"/>
        <item alpha="255" color="#f79342" label="3.189019" value="3.18901944"/>
        <item alpha="255" color="#f99a3e" label="3.272942" value="3.27294168"/>
        <item alpha="255" color="#fba139" label="3.356864" value="3.35686392"/>
        <item alpha="255" color="#fca835" label="3.440786" value="3.44078616"/>
        <item alpha="255" color="#fdaf31" label="3.524704" value="3.52470412"/>
        <item alpha="255" color="#feb72d" label="3.608626" value="3.60862636"/>
        <item alpha="255" color="#febe2a" label="3.692549" value="3.6925486"/>
        <item alpha="255" color="#fdc627" label="3.776471" value="3.77647084"/>
        <item alpha="255" color="#fcce25" label="3.860393" value="3.86039308"/>
        <item alpha="255" color="#fbd724" label="3.944315" value="3.94431532"/>
        <item alpha="255" color="#f8df25" label="4.028233" value="4.02823328"/>
        <item alpha="255" color="#f6e826" label="4.112156" value="4.11215552"/>
        <item alpha="255" color="#f3f027" label="4.196078" value="4.196077760000001"/>
        <item alpha="255" color="#f0f921" label="4.280000" value="4.28"/>
        <rampLegendSettings maximumLabel="" useContinuousLegend="1" direction="0" minimumLabel="" orientation="2" prefix="" suffix="">
          <numericFormat id="basic">
            <Option type="Map">
              <Option type="QChar" name="decimal_separator" value=""/>
              <Option type="int" name="decimals" value="6"/>
              <Option type="int" name="rounding_type" value="0"/>
              <Option type="bool" name="show_plus" value="false"/>
              <Option type="bool" name="show_thousand_separator" value="true"/>
              <Option type="bool" name="show_trailing_zeros" value="false"/>
              <Option type="QChar" name="thousand_separator" value=""/>
            </Option>
          </numericFormat>
        </rampLegendSettings>
      </colorrampshader>
      <vector-arrow-settings arrow-head-width-ratio="0.14999999999999999" arrow-head-length-ratio="0.40000000000000002">
        <shaft-length max="10" method="minmax" min="0.80000000000000004"/>
      </vector-arrow-settings>
      <vector-streamline-settings seeding-method="0" seeding-density="0.14999999999999999"/>
      <vector-traces-settings particles-count="1000" maximum-tail-length-unit="0" maximum-tail-length="100"/>
    </vector-settings>
    <mesh-settings-native color="0,0,0,255" line-width-unit="MM" line-width="0.26000000000000001" enabled="0"/>
    <mesh-settings-edge color="0,0,0,255" line-width-unit="MM" line-width="0.26000000000000001" enabled="0"/>
    <mesh-settings-triangular color="0,0,0,255" line-width-unit="MM" line-width="0.26000000000000001" enabled="0"/>
    <averaging-3d method="0">
      <multi-vertical-layers-settings end-layer-index="1" start-layer-index="1"/>
    </averaging-3d>
  </mesh-renderer-settings>
  <mesh-simplify-settings reduction-factor="10" mesh-resolution="5" enabled="0"/>
  <blendMode>0</blendMode>
  <layerOpacity>1</layerOpacity>
</qgis>
