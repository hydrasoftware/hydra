# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import io
import json
from qgis.PyQt.QtWidgets import QWidget, QTreeWidgetItem, QDialog, QTableWidgetItem
from qgis.PyQt import uic
from hydra.gui.base_dialog import tr

with open(os.path.join(os.path.dirname(__file__), "regulation_command.json")) as j:
    elements = json.load(j)

class AdvancedRegulationEditor(QDialog):

    def __init__(self, current_project, file, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "advanced_regulation_editor.ui"), self)
        self.file = file
        self.project = current_project
        self.loading = False

        self.init_tree()

        self.current_model.addItems(self.project.get_models())

        self.tree_toolbox.itemDoubleClicked.connect(self.write_command)
        self.tree_toolbox.itemClicked.connect(self.hover_command)

        with open(self.file, "r") as file_stream:
            self.edit_regulation.setText(file_stream.read())
        file_stream.close()

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.edit_regulation.selectionChanged.connect(self.make_keylist)
        self.data_tree.itemDoubleClicked.connect(self.replace_completion)

    def init_tree(self):

        for elem in elements :
            root = QTreeWidgetItem(self.tree_toolbox)
            root.setExpanded(False)
            root.setText(0,tr(elem))
            for item in elements[elem]["commands"] :
                node = QTreeWidgetItem(root)
                node.setText(0, tr(elements[elem]["commands"][item]["name"]))

    def write_command(self):
        item = self.tree_toolbox.currentItem()
        if item.parent() != self.tree_toolbox and item.parent() is not None:
            action_node = item.parent()
            command = [ command for command in elements[item.parent().text(0)]['commands'] if elements[item.parent().text(0)]['commands'][command]['name'] == item.text(0)]
            self.edit_regulation.insertPlainText(elements[action_node.text(0)]["commands"][command[0]]["command"])

    def hover_command(self):
        self.help_regulation.setText('')
        item = self.tree_toolbox.currentItem()
        if item.parent() != self.tree_toolbox and item.parent() is not None:
            action_node = item.parent()
            help_list = [ help_elem for help_elem in elements[item.parent().text(0)]['commands'] if elements[item.parent().text(0)]['commands'][help_elem]['name'] == item.text(0)]
            for e_help in elements[action_node.text(0)]["commands"][help_list[0]]["help"]:
                self.help_regulation.append(e_help)

    def make_keylist(self):
        if self.loading == False :

            self.data_tree.clear()
            with open(os.path.join(os.path.dirname(__file__), "advanced_regulation_object.json")) as j:
                self.objects = json.load(j)["object"]
            with open(os.path.join(os.path.dirname(__file__), "param_action.json")) as j:
                self.param_actionneur = json.load(j)

            if self.edit_regulation.textCursor().selectedText() == 'model' :
                models = self.project.get_models()

                model_root = QTreeWidgetItem(self.data_tree)
                model_root.setExpanded(True)
                model_root.setText(0,tr("model"))

                for model in models :
                    node = QTreeWidgetItem(model_root)
                    node.setText(0, tr(model))

            elif self.edit_regulation.textCursor().selectedText() == 'actionneur' :

                model=self.current_model.currentText()

                for table in self.objects["actionneur_list"] :
                    node_table = QTreeWidgetItem(self.data_tree)
                    node_table.setText(0, tr(table[0]))

                    objects_1 = self.project.execute(
                    """select name from {model}.{table}""".format(model=model, table=table[0])
                    ).fetchall()

                    objects_1 = [obj for (obj,) in objects_1]

                    for obj in objects_1 :
                        node_elem = QTreeWidgetItem(node_table)
                        node_elem.setText(0,table[1] +" "+ tr(obj))
                        node_elem.setDisabled(False)

            elif self.edit_regulation.textCursor().selectedText() == 'actionneur_reaction_rapide' :

                model=self.current_model.currentText()

                for table in self.objects["actionneur_reaction_rapide_list"]:
                    node_table = QTreeWidgetItem(self.data_tree)
                    node_table.setText(0, tr(table[0]))

                    objects_1 = self.project.execute(
                    """select name from {model}.{table}""".format(model=model, table=table[0])
                    ).fetchall()

                    objects_1 = [obj for (obj,) in objects_1]

                    for obj in objects_1 :
                        node_elem = QTreeWidgetItem(node_table)
                        node_elem.setText(0,table[1] +" "+ tr(obj))
                        node_elem.setDisabled(False)

            elif self.edit_regulation.textCursor().selectedText() == 'param' :

                self._make_paramlist()

            elif self.edit_regulation.textCursor().selectedText() == 'param_hydrau' :

                self._make_param_hydraulist()

            elif self.edit_regulation.textCursor().selectedText() == 'variable' :

                param_root = QTreeWidgetItem(self.data_tree)

                self._add_variable_from_txt("%var", "%", param_root, "%%")

                self._add_variable_from_txt("%%tab", "%%", param_root, "")

            elif self.edit_regulation.textCursor().selectedText() == 'action_param' :

                for elem in self.param_actionneur:
                    root = QTreeWidgetItem(self.data_tree)
                    root.setExpanded(False)
                    root.setText(0,tr(elem))
                    for item in self.param_actionneur[elem]["param"] :
                        node = QTreeWidgetItem(root)
                        node.setText(0, tr(self.param_actionneur[elem]["param"][item]["name"]))

            elif self.edit_regulation.textCursor().selectedText() == 'action_value' :

                self._add_variable_from_txt("%var", "%", self.data_tree, "%%")

            elif self.edit_regulation.textCursor().selectedText() == 'fonction' :
                for elem in objects["fonction_list"]:
                    nod = QTreeWidgetItem(self.data_tree)
                    nod.setExpanded(False)
                    nod.setText(0,tr(elem[0]))

    def _make_param_hydraulist(self):
        ''' add Qtreewidget items to the "param_hydrau" node Qtreewidget if param_hydrau is highlighted '''
        param_root = QTreeWidgetItem(self.data_tree)

        for param in self.objects["param_hydro"]:
            node_param = QTreeWidgetItem(param_root)
            node_param.setText(0, tr(param[0]))

            if "link" in param[1]:
                self._add_table("link", node_param, param)
            if "manhole_node" in param[1]:
                self._add_table("manhole_node", node_param, param)
            if "river_node" in param[1]:
                self._add_table("river_node", node_param, param)


    def _make_paramlist(self):
        ''' add Qtreewidget items to the "param" node Qtreewidget if param is highlighted '''

        self._add_variable_from_txt("%var", "%", self.data_tree, "%%")

        self._add_variable_from_txt("%%tab", "%%", self.data_tree, "")

        self._add_variable_from_txt("&&var", "&&", self.data_tree, "")

        param_root = QTreeWidgetItem(self.data_tree)
        param_root.setText(0,tr("param_hydrau"))
        for param in self.objects["param_hydro"]:
            node_param = QTreeWidgetItem(param_root)
            node_param.setText(0, tr(param[0]))

            if "link" in param[1]:
                self._add_table("link", node_param, param)
            if "bc" in param[1]:
                self._add_table("bc", node_param, param)
            if "sing" in param[1]:
                self._add_table("sing", node_param, param)
            if "node" in param[1]:
                self._add_table("node", node_param, param)
            if "river_node" in param[1]:
                self._add_table("river_node", node_param, param)
            if "manhole_node" in param[1]:
                self._add_table("manhole_node", node_param, param)

        param_root = QTreeWidgetItem(self.data_tree)
        param_root.setText(0,tr("position_actionneur"))
        for table in self.objects["position_actionneur_list"]:
            root = QTreeWidgetItem(param_root)
            root.setExpanded(False)
            root.setText(0,tr(table[0]))

            model=self.current_model.currentText()
            objects_1= self.project.execute(
            """select name from {model}.{table}""".format(model=model, table=table[0])
            ).fetchall()

            #if objects :
                #objects = objects[0]
            objects_1 = [obj for (obj,) in objects_1]
            for obj in objects_1 :
                node_elem = QTreeWidgetItem(root)
                node_elem.setText(0,table[1] +" "+ tr(obj) +" " + table[4] )
                # node_elem.setDisabled(False)

    def _add_variable_from_txt(self, name_cat, marker_cat, root_node, exclude_marker_list):
        ''' read current control files and add categorie in given node for given marker of categorie'''
        param_root = QTreeWidgetItem(root_node)
        param_root.setText(0,tr(name_cat))
        text = self.edit_regulation.toPlainText()
        vars_text = set([word for word in text.split() if (word.startswith(marker_cat) and exclude_marker_list not in word)])

        for var in vars_text:
            node_var = QTreeWidgetItem(param_root)
            node_var.setText(0, tr(var))


    def _add_table(self, type_table, root_node, param):
        ''' add all table to treewidget from a given type of table ( _bc, _node, _sing, _link, manhole_node <=> manhole_node, river_node <=> river_node) '''
        for table in self.objects[type_table]:
            objects_1 = self.project.execute(""" select name from {model}.{table} order by name""".format(model=self.current_model.currentText(), table=table[0])).fetchall()
            objects_1 = [obj for (obj,) in objects_1]
            for obj in objects_1:
                node_obj = QTreeWidgetItem(root_node)
                node_obj.setText(0, tr(param[0] + obj))

    def replace_completion(self, column):
        self.loading = True

        if self.data_tree.currentItem() and self.data_tree.currentItem().childCount() == 0 :

            self.edit_regulation.textCursor().removeSelectedText()
            self.edit_regulation.textCursor().insertText(self.data_tree.currentItem().text(0))

        self.loading = False

    def save(self):
        with io.open(self.file, "w", encoding = 'utf-8') as file_stream:
            file_stream.write(self.edit_regulation.toPlainText())
        file_stream.close()
        self.close()