-- dissociate kernel_version from type since kernel version listing is handled by kernel
alter table project.scenario alter column kernel_version type varchar using kernel_version::varchar
;;
alter table project.serie alter column kernel_version type varchar using kernel_version::varchar
;;
do 
$$$$
begin
    if exists (select 1 from pg_depend d where d.deptype = 'e' and objid='hydra.kernel_version'::regclass::oid) then
        alter extension hydra drop table  hydra.kernel_version;
        alter extension hydra drop type hydra_kernel_version;
    end if;
end;
$$$$
;;
drop table if exists hydra.kernel_version 
;;
drop type if exists hydra_kernel_version
;;
