# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
packaging script for the hydra project

USAGE
    package.py [-h, -i, -u] [directory] [-d] [version],

OPTIONS
    -h, --help
        print this help

    -i, --install [directory]
        install the package in the .qgis2 directory, if directory is ommited,
        install in the QGis plugin directory
        deploys dll (postgresql and python3)

    -u, --uninstall
        uninstall (remove) the package from .qgis2 directory

    -d, --deploy
        deploy the package to qgis repository directory
        possibility to add version: python -m scripts.package -d 1.0
"""

import os
import sys
import zipfile
import re
import shutil
import getpass
import time
import subprocess

qgis_plugin_dir = os.path.abspath(os.path.join(os.path.expanduser('~'), "AppData", "Roaming", "QGIS", "QGIS3", "profiles", "default", "python", "plugins"))
plugin_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'hydra'))
repository_dir = os.path.abspath("L:/Logiciels/Hydra/repository/")

metadata_file = os.path.abspath(os.path.join(os.path.dirname(__file__), "hydra", "metadata.txt"))
changelog_file = os.path.abspath(os.path.join(os.path.dirname(__file__), "CHANGELOG"))

zip_name = "hydra"
zip_ext = ".zip"
zip_file = os.path.join(os.path.dirname(__file__), zip_name+zip_ext)

def run_tests():
    '''Runs hydra plugin tests script. Exits if error happens'''
    out, err = subprocess.Popen([sys.executable, "-m", "hydra.test"]).communicate()
    if err:
        print("Tests failed.", err)
        exit(1)

def deploy(plugin_zip, new_version):
    '''Creates a copy of hydra.zip to REMOTE DIR and updates associated xml file'''
    #Copy Plugin archive to REPOSITORY
    target_file = os.path.join(repository_dir, zip_name + "_" + str(new_version) + zip_ext)
    __drop(target_file)
    shutil.copyfile(plugin_zip, target_file)
    print("Copied ZIP in {}".format(target_file))
    #Updates associated xml file
    xmlfile = os.path.join(repository_dir, "hydra.xml")
    with open(xmlfile, 'r') as file:
        data = file.readlines()
    data[3]= "  <pyqgis_plugin name='hydra' version='" + str(new_version) + "'>\n"
    data[5]= "    <version>" + str(new_version) + "</version>\n"
    data[10]= "    <download_url>file:///" + target_file +  "</download_url>\n"
    data[11]= "    <uploaded_by>" + getpass.getuser() + "</uploaded_by>\n"
    data[12]= "    <create_date>" + time.strftime("%x") + "</create_date>\n"
    with open(xmlfile, 'w') as file:
        file.writelines( data )
    print("Updated XML file in {}".format(repository_dir))
    print("Deployed version {} in {}".format(new_version,repository_dir))

def update_metadata_file(new_version):
    '''Updates plugin version in metadata.txt'''
    with open(metadata_file, 'r') as file:
        data = file.readlines()
    if new_version is None:
        version_split = data[14].split('=')[1].split('.')
        version_split[-1] = str(int(version_split[-1])+1)
        new_version = '.'.join(version_split)
    data[14]= "version="+str(new_version)+"\n"
    data[31]= "  "+str(new_version)+"\n"
    with open(metadata_file, 'w') as file:
        file.writelines( data )
    print("Metadata file updated to {}".format(data[14].rstrip().replace("="," ")))
    return new_version

def update_changelog():
    '''Updates file CHANGELOG with updated changelog from metadata.txt'''
    with open(metadata_file, 'r') as file:
        data = file.readlines()
    with open(changelog_file, 'w') as file:
        file.writelines(data[31:-1])
    print("Changelog for version {} updated".format(data[31].strip()))

def uninstall(install_dir):
    ''' Removes plugin archive from USER/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugin/'''
    target_dir = os.path.join(install_dir, "hydra")
    __drop(target_dir)
    print("Removed plugin from {}".format(install_dir))

def install(install_dir, source_file):
    ''' Extract plugin archive from hydra.zip into USER/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugin/'''
    uninstall(install_dir)
    with zipfile.ZipFile(source_file, "r") as z:
        z.extractall(install_dir)
    print("Installed plugin in {}".format(install_dir))

def zip_(target_file):
    ''' Compresses plugin into archive target_file"
    The zip file doesn't include tests, demos or docs'''
    print("Packaging plugin into {}".format(target_file))
    with zipfile.ZipFile(target_file, 'w') as package:
        for root, dirs, files in os.walk(plugin_dir):
            if (not re.match(r".*(test_data|.*.egg-info).*", root)):
                for file_ in files:
                    if re.match(r'CHANGELOG', file_) or (re.match(r".*\.(plu|py|sql|ui|json|qml|png|svg|txt|hvc|exe|dll|whl)$", file_) \
                            and not re.match(r".*(_test|_guitest|_demo|_solotest)\.py", file_) and not re.match(r".*(_test)\.sql", file_)):
                        fake_root = root.replace(plugin_dir, "hydra")
                        package.write(os.path.join(root, file_),
                                      os.path.join(fake_root, file_))
    print("Compressed zip file {}".format(target_file))

def __drop(name):
    '''Deletes file or dir at path "name"'''
    if os.path.isfile(name):
        os.remove(name)
        print("Del file {}".format(name))
    if os.path.isdir(name):
        shutil.rmtree(name)
        print("Del folder {}".format(name))

if __name__ == "__main__":

    import getopt
    import sys
    from hydra.utility.system import deploy_dependencies

    try:
        optlist, args = getopt.getopt(sys.argv[1:],
                "hiud",
                ["help", "install", "uninstall", "deploy"])
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        exit(1)

    optlist = dict(optlist)

    if "-h" in optlist or "--help" in optlist:
        help(sys.modules[__name__])
        exit(0)

    if "-d" in optlist or "--deploy" in optlist:
        if len(args) == 1:
            new_version = args[0]
        else:
            new_version = None
        run_tests()
        new_version = update_metadata_file(new_version)
        update_changelog()

    zip_(zip_file)

    if "-u" in optlist or "--uninstall" in optlist or "-i" in optlist or "--install" in optlist:
        install_dir = qgis_plugin_dir if len(args)==0 else args[0]

    if "-u" in optlist or "--uninstall" in optlist:
        uninstall(install_dir)

    if "-i" in optlist or "--install" in optlist:
        install(install_dir, zip_file)
        if os.name == 'nt':
            deploy_dependencies(plugin_dir)

    if "-d" in optlist or "--deploy" in optlist:
        deploy(zip_file, new_version)
