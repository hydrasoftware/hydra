/* ************************************************** */
/* Update river cross section profiles set z_invert   */
/* to first Z of river bed geometry array where       */
/* z_invert is null or 0                              */
/* ************************************************** */

update $model.river_cross_section_profile as cs
set z_invert_down = (
    select g.zbmin_array[1][1]
    from $model.valley_cross_section_geometry as g
    where cs.down_vcs_geom=g.id
    )
where cs.z_invert_down = 0
   or cs.z_invert_down is null
;;

update $model.river_cross_section_profile as cs
set z_invert_up = (
    select g.zbmin_array[1][1]
    from $model.valley_cross_section_geometry as g
    where cs.up_vcs_geom=g.id
    )
where cs.z_invert_up = 0
   or cs.z_invert_up is null
;;