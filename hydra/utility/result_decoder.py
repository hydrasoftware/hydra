# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
smart access to hydra binary files

HydraResult implement __getitem__(self, keys) operator
If keys arg is a string or a list of string then access is made
by element name and return a ResultFromElem object.
If keys arg is a int or a list of int or a slice then access is made
by time step number and return a ResultFromTimeStep object.

ResultFromElem and ResultFromTimeStep also implement __getitem__(self, keys) operator
ResultFromElem accept slice, int and list of int to indicates the desired time steps of the selected elements

ResultFromTimeStep accept string, list of string, slice, int and list of int to indicates the desired elements for the selected time steps

Results are return in the following format: [time, [list of 8 floats]]

USAGE

    results = HydraResult(file_path)

    Access by time steps:
        results[0]["NOD_1"]                 --> return the result of "NOD_1" at the first time step
        results[0][["NOD_1", "NOD_2"]]      --> return the result of "NOD_1" and "NOD_2" for the first time step
        results[0:3][["NOD_1", "NOD_2"]]    --> return the result of "NOD_1" and "NOD_2" for the first 3 time steps
        results[[0,5]][["NOD_1", "NOD_2"]]  --> return the result of "NOD_1" and "NOD_2" for the first time step 0 and 5
        results[0][:]                       --> return the result of all elements for the first time step
        results[:][:]                       --> return the result of all elements for all time steps

    Access by elements:
        results["NOD_1"][:]                 --> return the result of all time steps for the element "NOD_1"
        results[["NOD_1", "NOD_2"]][:]      --> return the result of all time steps for the element "NOD_1" and "NOD_2"

"""

import struct
import os
import numpy
import collections
import json

def isint(x):
    return isinstance(x, int) or isinstance(x, long) or isinstance(x, numpy.int32) or isinstance(x, numpy.int64)

with open(os.path.join(os.path.dirname(__file__), "..", "gui", "visu.json")) as j:
    _visu_ = json.load(j)

class HydraResult(object):
    @staticmethod
    def read_binary(file_stream, time_step, i_elem, nb_elem):
        start = (long(nb_elem)+1)*32 + time_step*(long(nb_elem)+1)*32
        file_stream.seek(start)
        time = numpy.frombuffer(file_stream.read(32), dtype=numpy.float32)[0]
        file_stream.seek(32*(long(i_elem)), 1)
        results = numpy.frombuffer(file_stream.read(32), dtype=numpy.float32)
        return [time, results]

    class ResultFromElem(object):
        def __init__(self, path, i_elem, nb_step, nb_elem):
            self.__path = path
            self.__nb_step = nb_step
            self.__i_elem = i_elem
            self.__nb_elem = nb_elem

        def __getitem__(self, keys):
            with open(self.__path, 'rb') as f :
                if isinstance(keys, slice):
                    start = 0 if (keys.start is None or keys.start<0) else keys.start if keys.start<self.__nb_step else self.__nb_step
                    stop = self.__nb_step if (keys.stop is None or keys.stop>self.__nb_step) else keys.stop if keys.stop>0 else 0
                    iter_values = list(range(start, stop))
                elif isint(keys):
                    start = 0 if keys<0 else keys if keys<self.__nb_step else self.__nb_step
                    stop = start+1
                    iter_values = list(range(start, stop))
                elif isinstance(keys, list):
                    iter_values = keys
                else:
                    raise Exception("Key type exception in getitem operator.")

                values = list()
                if isinstance(self.__i_elem, list):
                    for elem in self.__i_elem:
                        for i in iter_values:
                            values.append(HydraResult.read_binary(f, i, elem, self.__nb_elem))
                else:
                    for i in iter_values:
                        values.append(HydraResult.read_binary(f, i, self.__i_elem, self.__nb_elem))

            return values

    class ResultFromTimeStep(object):
        def __init__(self, path, i_step, nb_step, names_elem):
            self.__path = path
            self.__nb_step = nb_step
            self.__i_step = i_step
            self.__names_elem = names_elem

        def __getitem__(self, keys):
            with open(self.__path, 'rb') as f :
                iter_values = list()
                if isinstance(keys, str):
                    iter_values.append(int(self.__names_elem.index(keys.upper())))
                elif isint(keys):
                    iter_values = [keys]
                elif isinstance(keys, list):
                    for key in keys:
                        if isint(key):
                            iter_values.append(key)
                        else:
                            iter_values.append(int(self.__names_elem.index(key.upper())))
                elif isinstance(keys, slice):
                    nb_elem = len(self.__names_elem)
                    start = 0 if keys.start<0 else keys.start if keys.start<nb_elem else nb_elem
                    stop = 0 if keys.stop<0 else keys.stop if keys.stop<nb_elem else nb_elem
                    iter_values = list(range(start, stop))
                else:
                    raise Exception("Key type exception in getitem operator.")

                values = list()
                if isinstance(self.__i_step, list):
                    for step in self.__i_step:
                        for i_elem in iter_values:
                            values.append(HydraResult.read_binary(f, step, i_elem, len(self.__names_elem)))
                elif isinstance(self.__i_step, slice):
                    istep_st = 0 if self.__i_step.start<0 else self.__i_step.start if self.__i_step.start<self.__nb_step else self.__nb_step
                    istep_ed = 0 if self.__i_step.stop<0 else self.__i_step.stop if self.__i_step.stop<self.__nb_step else self.__nb_step
                    for istep in range(istep_st, istep_ed):
                        for i_elem in iter_values:
                            values.append(HydraResult.read_binary(f, istep, i_elem, len(self.__names_elem)))
                else:
                    for i_elem in iter_values:
                        values.append(HydraResult.read_binary(f, self.__i_step, i_elem, len(self.__names_elem)))

                return values

    def __len__(self):
        return self.__nb_step

    def __init__(self, file_path):
        self.path = file_path

    def __getitem__(self, keys):
        with open(self.path, 'rb') as f :
            f.seek(0)
            nb_step, nb_elem, nb_link = numpy.frombuffer(f.read(32), dtype=numpy.int32)[:3]
            names = list()
            for i in range(nb_elem + nb_link):
                names.append(b''.join(struct.unpack('s'*24, f.read(24))).strip().upper().decode('utf-8'))
                f.seek(8, 1)

            if isinstance(keys, slice) or isint(keys): # from time step
                return HydraResult.ResultFromTimeStep(f, keys, nb_step, names)
            elif isinstance(keys, str): # from elem name
                i_elem = int(names.index(keys.upper()))
                return HydraResult.ResultFromElem(self.path, i_elem, nb_step, nb_elem + nb_link)
            elif isinstance(keys, list):
                if len(keys)>0:
                    if isint(keys[0]): # from time step
                        return HydraResult.ResultFromTimeStep(self.path, keys, nb_step, names)
                    elif isinstance(keys[0], str): # from elem name
                        indices = list()
                        for name in keys:
                            indices.append(int(names.index(name.upper())))
                        return HydraResult.ResultFromElem(self.path, indices, nb_step, nb_elem + nb_link)
                    else:
                        raise Exception("Key type exception in getitem operator.")
            else:
                raise Exception("Key type exception in getitem operator.")
            return None

    def __setitem__(self, key, item):
        raise Exception("Set item is not allowed")

class W14Result(object):
    def __init__(self, filename):
        self.path = filename
        self.__nb_step = 0
        self.__nb_lias = 0
        self.__names = []

        self.__init_file()

    def __init_file(self) :
        with open(self.path, 'rb') as f :
            f.seek(0)
            nb_step, nb_umps, nb_lias = numpy.frombuffer(f.read(32), dtype=numpy.int32)[:3]

            names = []
            for i in range(nb_umps+nb_lias):
                name = f.read(24).decode('utf-8').rstrip()
                itype, id_ =  numpy.frombuffer(f.read(8), dtype=numpy.int32)[:2]
                names.append(name)

            self.__nb_step = nb_step
            self.__nb_lias = nb_umps + nb_lias
            self.__names = names

    def __getattr__(self, name):
        if "names" == name:
            return self.__names
        elif "nb_steps" == name:
            return self.__nb_step
        raise AttributeError

    def read(self, step):
        with open(self.path, 'rb') as f :
            f.seek(32*((1 + long(self.__nb_lias))*(1 + long(step))+1))
            return numpy.frombuffer(f.read(32*self.__nb_lias), dtype=numpy.float32).reshape((self.__nb_lias, -1))

class W15Result(object):

    values_for_type = {
        0:  None,
        1: _visu_['manhole_node']['results'],
        2: _visu_['river_node']['results'],
        3: _visu_['station_node']['results'],
        4: _visu_['storage_node']['results'],
        5: _visu_['elem_2d_node']['results'],
        6: _visu_['crossroad_node']['results'],
        '': None,
        'NODA': _visu_['manhole_node']['results'],
        'NODR': _visu_['river_node']['results'],
        'NODS': _visu_['station_node']['results'],
        'CAS' : _visu_['storage_node']['results'],
        'PAV' : _visu_['elem_2d_node']['results'],
        'CAR' : _visu_['crossroad_node']['results']
        }

    abbrv = set((v["abbreviation"] for v in _visu_.values()))

    class Step(object):
        def __init__(self, values, name_idx, type_ids):
            self.values = values
            self.__name_idx = name_idx
            self.__type_ids = type_ids

        def __getitem__(self, node_name_or_idx):
            if type(node_name_or_idx) == str:
                idx = self.__name_idx[node_name_or_idx]
                typ = self.__type_ids[idx]
                val = [float(self.values[idx, i]) for i in range(8)]
                key = W15Result.values_for_type[typ]
                return dict(zip(key, val))
            else:
                return float(self.values[node_name_or_idx[0], node_name_or_idx[1]])

    def __init__(self, filename):
        self.path = filename
        with open(self.path, 'rb') as f :
            f.seek(0)
            nb_step, nb_nod, nb_lias = numpy.frombuffer(f.read(32), dtype=numpy.int32)[:3]
            name_idx = {}
            names = []
            type_ids = []
            for i in range(nb_nod+nb_lias):
                name = f.read(24).decode('utf-8').rstrip()
                names.append(name)
                name_idx[name] = i
                buf = f.read(8)
                #itype, id_ = numpy.frombuffer(buf, dtype=numpy.int32)[:2]
                typename = buf[:4].decode('ascii').strip()
                if typename in W15Result.abbrv:
                    type_ids.append(typename)
                else:
                    itype = numpy.frombuffer(buf, dtype=numpy.int16)[1]
                    type_ids.append(itype)

        self.__name_idx = name_idx
        self.__nb_step = nb_step
        self.__nb_nod = nb_nod+nb_lias
        self.__names = names
        self.__type_ids = type_ids

    def offset(self, node_name, value_name):
        idx = self.__name_idx[node_name]
        return (idx, W15Result.values_for_type[self.__type_ids[idx]].index(value_name)) \
            if value_name in W15Result.values_for_type[self.__type_ids[idx]] else (idx, -1)

    def __getattr__(self, name):
        if "names" == name:
            return self.__names
        elif "nb_steps" == name:
            return self.__nb_step
        raise AttributeError

    def step(self, step):
        return W15Result.Step(self.read(step), self.__name_idx, self.__type_ids)

    def read(self, step):
        with open(self.path, 'rb') as f :
            f.seek(32*((1 + long(self.__nb_nod))*(1 + long(step))+1))
            return numpy.frombuffer(f.read(32*self.__nb_nod), dtype=numpy.float32).reshape((self.__nb_nod, -1))

    def get_step_times(self):
        with open(self.path, 'rb') as f :
            times = []
            for i in range(self.__nb_step):
                f.seek(32*((1 + long(self.__nb_nod)) + i*(long(self.__nb_nod)+1)))
                times.append(float(numpy.frombuffer(f.read(32), dtype=numpy.float32)[0]))
            return times

class W16Result(object):
    class W16TimeResult(object):
        def __init__(self, parent, ielem):
            self.__parent = parent
            self.ielem = ielem

        def __getitem__(self, keys):
            if isinstance(keys, slice):
                start = 0 if keys.start<0 else keys.start if keys.start<self.__parent.nstep+2 else self.__parent.nstep
                stop = 0 if keys.stop<0 else keys.stop if keys.stop<self.__parent.nstep+2 else self.__parent.nstep
                iter_values = list(range(start, stop))
            elif isint(keys):
                start = 0 if keys<0 else keys if keys<self.__parent.nstep+2 else self.__parent.nstep
                stop = start+1
                iter_values = list(range(start, stop))
            else:
                raise Exception(f"Key type should be int or slice not {str(type(keys))}.")

            values = list()
            for i in iter_values:
                values.append(self.result_at_time(i))

            return values

        def result_at_time(self, time):
            with open(self.__parent.path, 'rb') as f :
                result = list()
                start = long(self.__parent.nrec[self.ielem-1]) if self.ielem>0 else 0
                end = self.__parent.nrec[self.ielem]
                f.seek(32*(1 + long(self.__parent.nb) + long(self.__parent.ntot)) + 32*time*(long(self.__parent.ntot)+1))
                time = numpy.frombuffer(f.read(32), dtype=numpy.float32)[0]
                f.seek(32*start,1)
                for n in range(start, end):
                    result.append([time, numpy.frombuffer(f.read(32), dtype=numpy.float32)])
                return result

    def __len__(self):
        return self.nb

    def __init__(self, file_path):
        self.path = file_path
        self.param_header = None
        self.nstep = 0
        self.ntot = 0
        self.nb = 0
        self.names = []
        self.nrec = []
        self.read_header()

    def read_header(self):
        with open(self.path, 'rb') as f :
            f.seek(0)
            f.seek(16)
            self.param_header = []
            for i in range (4) :
                name = b''.join(struct.unpack('s'*4, f.read(4))).decode('utf-8')
                self.param_header.append(name)
            if self.param_header == ['    ','    ','    ','    '] :
                self.param_header = None
            f.seek(0)
            self.nstep, self.ntot, self.nb = numpy.frombuffer(f.read(32), dtype=numpy.int32)[:3]
            self.names = []
            self.nrec = []
            for i in range(self.nb):
                name = b''.join(struct.unpack('s'*16, f.read(16))).strip().upper().decode('utf-8')
                nrec = struct.unpack('i'*4, f.read(16))[0]
                self.names.append(name)
                self.nrec.append(nrec)

            self.elem_geom = []
            for i in range(self.ntot):
                self.elem_geom.append(numpy.frombuffer(f.read(32), dtype=numpy.float32))

    def get_geom(self, name):
        indice = self.names.index(name.strip().upper())
        start = self.nrec[indice-1] if indice>0 else 0
        end = self.nrec[indice]
        return self.elem_geom[start:end]

    def get_step_times(self):
        with open(self.path, 'rb') as f :
            times = []
            for i in range(self.nstep):
                f.seek(32*(1 + long(self.nb) + long(self.ntot)) + 32*i*(long(self.ntot)+1))
                times.append(float(numpy.frombuffer(f.read(32), dtype=numpy.float32)[0]))
            return times

    def __getitem__(self, keys):
        if not isinstance(keys, str):
            raise Exception("Key should be str elem name.")
        indice = self.names.index(keys.strip().upper())
        return W16Result.W16TimeResult(self, indice)

    def __setitem__(self, key, item):
        raise Exception("Set item is not allowed")

def get_results(elem_name, result_file_path):
    assert os.path.isfile(result_file_path)
    result = [[], [], [], [], []]
    with open(result_file_path, 'rb') as file:
        file.seek(0)
        data = struct.unpack('i'*8, file.read(32))
        nStep = data[0]
        nElem = data[1] + data[2]

        index = -1
        for i in range(0,nElem):
            name = b''.join(struct.unpack('s'*24, file.read(24))).strip().decode('utf-8')
            id = struct.unpack('i'*2, file.read(8))
            if (name.upper()==elem_name.strip().upper()):
                index = i
                file.seek(32*(long(nElem)-long(index)-1),1)
                break

        if index==-1:
            raise RuntimeError("Elem not found.")

        for i in range(0, nStep):
            rdata = file.read(32)
            date = struct.unpack('f'*8, rdata)[0]
            file.seek(32*long(index),1)
            rdata = file.read(32)

            data = struct.unpack('f'*8, rdata)
            result[0].append(date)
            result[1].append(data[0])
            result[2].append(data[1])
            result[3].append(data[2])
            result[4].append(data[3])
            result[5].append(data[4])
            result[6].append(data[5])
            result[7].append(data[6])
            result[8].append(data[7])

            file.seek(32*(nElem-index-1),1)

    return result

if __name__=="__main__":
    import sys
    w15 = W15Result(sys.argv[1])
    print("ok")
