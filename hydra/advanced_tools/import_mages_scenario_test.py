# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
test import MAGES scenario/

USAGE

   import_mages_scenario_test.py [-hk]

OPTIONS

   -h, --help
        print this help

   -k, --keep
        does not delete test database after test

    -d, --debug
        debug mode, more feedback
"""

import re
import os
import sys
import getopt
import filecmp
from shutil import rmtree
from hydra.project import Project
from hydra.utility import empty_ui_manager
from hydra.utility.log import LogManager, ConsoleLogger
from hydra.database import database as dbhydra
from hydra.advanced_tools.import_mages_scenario import import_mages_scenario


if __name__ == '__main__':

    try:
        optlist, args = getopt.getopt(sys.argv[1:],
                "hkd",
                ["help", "keep", "debug"])
    except Exception as e:
        sys.stderr.write("export_csv_test.py [-hkd]\n")
        exit(1)

    keep = False
    debug = False

    for opt, arg in optlist:
        if opt in ("-h", "--help"):
            help(sys.modules[__name__])
            exit(0)
        elif opt in ("-k", "--keep"):
            keep=True
        elif opt in ("-d", "--debug"):
            debug = True

    log = LogManager(ConsoleLogger(), "Hydra")

    project_name = 'mages_scenario_test'
    if dbhydra.project_exists(project_name):
        dbhydra.remove_project(project_name)
    test_project = dbhydra.TestProject(project_name, keep)
    project = Project.load_project(project_name)

    current_dir = os.path.dirname(__file__)
    scenario_dirs = [os.path.join(current_dir, "test_data", "mages_scenarios", "Data_superviseur", "2010_06_12_00h00"),
                     os.path.join(current_dir, "test_data", "Simu000", "data-000-REF", "2022_07_29_09h05")]

    project = Project(project_name, LogManager(ConsoleLogger(), 'Hydra'), empty_ui_manager.EmptyUIManager(), debug)
    rmtree(project.directory)

    for scenario_dir in scenario_dirs:
        scenario_name = os.path.basename(scenario_dir)
        project.execute(f"delete from project.scenario where name='{scenario_name}';")

        import_mages_scenario(project, scenario_dir)
        project.commit()

        ref_dir = os.path.join(current_dir, "test_data", f"ref_{scenario_name}")

        no_error = True
        for root, dirs, files in os.walk(ref_dir):
            for file_ in files:
                ref_file = os.path.abspath(os.path.join(root, file_))
                generated_file = ref_file.replace(ref_dir, project.directory)

                if not os.path.isfile(generated_file):
                    print(f"Missing file : {generated_file}")
                    no_error = False
                if not filecmp.cmp(ref_file, generated_file):
                    print(f"Created file {generated_file} content not matching ref file {ref_file} content")
                    no_error = False

        if not no_error:
            raise Exception("Errors comparing generated files to refs")

    sys.stdout.write("ok\n")
