# -*- coding: utf-8 -*-

"""
***************************************************************************
    aread8.py
    ---------------------
    Date                 : January 2018
    Copyright            : (C) 2018-2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'January 2018'
__copyright__ = '(C) 2018-2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.core import (QgsVectorFileWriter,
                       QgsProcessing,
                       QgsProcessingException,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterBoolean,
                       QgsProcessingParameterRasterDestination
                      )

from .taudemAlgorithm import TauDemAlgorithm
from . import taudemUtils


class AreaD8(TauDemAlgorithm):

    D8_FLOWDIR = 'D8_FLOWDIR'
    WEIGHT_GRID = 'WEIGHT_GRID'
    OUTLETS = 'OUTLETS'
    EDGE_CONTAMINATION = 'EDGE_CONTAMINATION'
    D8_CONTRIB_AREA = 'D8_CONTRIB_AREA'

    def name(self):
        return 'aread8'

    def displayName(self):
        return self.tr('D8 contributing area')

    def group(self):
        return self.tr('Basic grid analysis')

    def groupId(self):
        return 'basicanalysis'

    def tags(self):
        return self.tr('dem,hydrology,d8,contributing area,catchment area').split(',')

    def shortHelpString(self):
        return self.tr('Calculates a grid of contributing areas using the '
                       'single direction D8 flow model.')

    def helpUrl(self):
        return 'http://hydrology.usu.edu/taudem/taudem5/help53/D8ContributingArea.html'

    def __init__(self):
        super().__init__()

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterRasterLayer(self.D8_FLOWDIR,
                                                            self.tr('D8 flow directions')))
        self.addParameter(QgsProcessingParameterFeatureSource(self.OUTLETS,
                                                              self.tr('Outlets'),
                                                              types=[QgsProcessing.TypeVectorPoint],
                                                              defaultValue=None,
                                                              optional=True))
        self.addParameter(QgsProcessingParameterRasterLayer(self.WEIGHT_GRID,
                                                            self.tr('Weight grid'),
                                                            defaultValue=None,
                                                            optional=True))
        self.addParameter(QgsProcessingParameterBoolean(self.EDGE_CONTAMINATION,
                                                        self.tr('Check for edge contamination'),
                                                        defaultValue=True))

        self.addParameter(QgsProcessingParameterRasterDestination(self.D8_CONTRIB_AREA,
                                                                  self.tr('D8 specific catchment area')))

    def processAlgorithm(self, parameters, context, feedback):
        d8Layer = self.parameterAsRasterLayer(parameters, self.D8_FLOWDIR, context)
        if d8Layer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.D8_FLOWDIR))

        arguments = []
        arguments.append(self.command())

        arguments.append('-p')
        arguments.append(d8Layer.source())

        if self.OUTLETS in parameters and parameters[self.OUTLETS] is not None:
            outlets = self.parameterAsCompatibleSourceLayerPath(parameters, self.OUTLETS, context,
                                                                QgsVectorFileWriter.supportedFormatExtensions(),
                                                                feedback=feedback)
            arguments.append('-o')
            arguments.append(outlets)

        if self.WEIGHT_GRID in parameters and parameters[self.WEIGHT_GRID] is not None:
            weight = self.parameterAsRasterLayer(parameters, self.WEIGHT_GRID, context)
            arguments.append('-wg')
            arguments.append(weight.source())

        if not self.parameterAsBool(parameters, self.EDGE_CONTAMINATION, context):
            arguments.append('-nc')

        arguments.append('-ad8')
        arguments.append(self.parameterAsOutputLayer(parameters, self.D8_CONTRIB_AREA, context))

        taudemUtils.execute(arguments, feedback)

        return self.algorithmResults(parameters)
