/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update $model.metadata set version = '1.7';
;;

/* ********************************************** */
/*  Results on river nodes                        */
/* ********************************************** */

-- River nodes
create or replace view ${model}.results_river as
    select
        n.id,
        UPPER(n.name)::varchar(24) as name,
        n.geom,
        case when v.id is not null then 'valley'
             when o.id is not null then 'channel'
             else 'closed'
        end as section_type
    from ${model}.river_node as n
    left join ${model}.open_reach as v on ST_DWithin(v.geom, n.geom, 0.001)
    left join ${model}.channel_reach as o on ST_DWithin(o.geom, n.geom, 0.001);
;;

-- Manholes nodes
create or replace view ${model}.results_manhole as
    select
        n.id,
        UPPER(n.name)::varchar(24) as name,
        n.geom
    from ${model}.manhole_node as n;
;;

/* ********************************************** */
/*  hotfix for z_invert on cross_section          */
/* ********************************************** */

create or replace view $model.river_cross_section_profile as
    select g.id,
        c.name,
        case
            when c.type_cross_section_up is not null then c.z_invert_up
            else null
        end as z_invert_up,
        case
            when c.type_cross_section_down is not null then c.z_invert_down
            else null
        end as z_invert_down,
        c.type_cross_section_up,
        c.type_cross_section_down,
        c.up_rk,
        c.up_rk_maj,
        c.up_sinuosity,
        c.up_circular_diameter,
        c.up_ovoid_height,
        c.up_ovoid_top_diameter,
        c.up_ovoid_invert_diameter,
        c.up_cp_geom,
        c.up_op_geom,
        c.up_vcs_geom,
        c.up_vcs_topo_geom,
        c.down_rk,
        c.down_rk_maj,
        c.down_sinuosity,
        c.down_circular_diameter,
        c.down_ovoid_height,
        c.down_ovoid_top_diameter,
        c.down_ovoid_invert_diameter,
        c.down_cp_geom,
        c.down_op_geom,
        c.down_vcs_geom,
        c.down_vcs_topo_geom,
        c.configuration,
        (select case when (c.type_cross_section_up='channel' or c.type_cross_section_up='valley') then
            (select (c.z_invert_up))
        else (select st_z(st_startpoint(g.geom)))
        end) as z_tn_up,
        (select case when (c.type_cross_section_down='channel' or c.type_cross_section_down='valley') then
            (select (c.z_invert_down))
        else (select st_z(st_endpoint(g.geom)))
        end) as z_tn_down,
        (select case when c.type_cross_section_up='circular' then
            (select (c.z_invert_up+c.up_circular_diameter))
        when c.type_cross_section_up='ovoid' then
            (select (c.z_invert_up+c.up_ovoid_height))
        when c.type_cross_section_up='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.up_cp_geom = cpg.id)
            (select (c.z_invert_up + n.height) from n))
        when c.type_cross_section_up='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.up_op_geom = opg.id)
            (select (c.z_invert_up + n.height) from n))
        when c.type_cross_section_up='valley' then
            (with n as (select (zbmaj_lbank_array[1][1] - zbmin_array[1][1]) as height
                from ${model}.valley_cross_section_geometry opg
                where c.up_vcs_geom = opg.id)
            (select (c.z_invert_up + n.height) from n))
        else null
        end) as z_lbank_up,
        (select case when c.type_cross_section_down='circular' then
            (select (c.z_invert_down+c.down_circular_diameter))
        when c.type_cross_section_down='ovoid' then
            (select (c.z_invert_down+c.down_ovoid_height))
        when c.type_cross_section_down='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.down_cp_geom = cpg.id)
            (select (c.z_invert_down + n.height) from n))
        when c.type_cross_section_down='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.down_op_geom = opg.id)
            (select (c.z_invert_down + n.height) from n))
        when c.type_cross_section_down='valley' then
            (with n as (select (zbmaj_lbank_array[1][1] - zbmin_array[1][1]) as height
                from ${model}.valley_cross_section_geometry opg
                where c.down_vcs_geom = opg.id)
            (select (c.z_invert_down + n.height) from n))
        else null
        end) as z_lbank_down,
        (select case when c.type_cross_section_up='circular' then
            (select (c.z_invert_up+c.up_circular_diameter))
        when c.type_cross_section_up='ovoid' then
            (select (c.z_invert_up+c.up_ovoid_height))
        when c.type_cross_section_up='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.up_cp_geom = cpg.id)
            (select (c.z_invert_up + n.height) from n))
        when c.type_cross_section_up='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.up_op_geom = opg.id)
            (select (c.z_invert_up + n.height) from n))
        when c.type_cross_section_up='valley' then
            (with n as (select (zbmaj_rbank_array[1][1] - zbmin_array[1][1]) as height
                from ${model}.valley_cross_section_geometry opg
                where c.up_vcs_geom = opg.id)
            (select (c.z_invert_up + n.height) from n))
        else null
        end) as z_rbank_up,
        (select case when c.type_cross_section_down='circular' then
            (select (c.z_invert_down+c.down_circular_diameter))
        when c.type_cross_section_down='ovoid' then
            (select (c.z_invert_down+c.down_ovoid_height))
        when c.type_cross_section_down='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.down_cp_geom = cpg.id)
            (select (c.z_invert_down + n.height) from n))
        when c.type_cross_section_down='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.down_op_geom = opg.id)
            (select (c.z_invert_down + n.height) from n))
        when c.type_cross_section_down='valley' then
            (with n as (select (zbmaj_rbank_array[1][1] - zbmin_array[1][1]) as height
                from ${model}.valley_cross_section_geometry opg
                where c.down_vcs_geom = opg.id)
            (select (c.z_invert_down + n.height) from n))
        else null
        end) as z_rbank_down,
        (select case when c.type_cross_section_down='valley' then
            null
        when c.type_cross_section_down='channel' then
            null
        else
            (with n as (select zbmin_array[array_length(zbmin_array, 1)][1] as height
                from ${model}.closed_parametric_geometry opg
                where c.down_op_geom = opg.id)
            select (c.z_invert_up + n.height) from n)
        end) as z_ceiling_up,
        (select case when c.type_cross_section_down='valley' then
            null
        when c.type_cross_section_down='channel' then
            null
        else
            (with n as (select zbmin_array[array_length(zbmin_array, 1)][1] as height
                from ${model}.closed_parametric_geometry opg
                where c.down_op_geom = opg.id)
            select (c.z_invert_down + n.height) from n)
        end) as z_ceiling_down,
        c.validity,
        g.geom
    from $model._river_cross_section_profile as c, $model._node as g
    where g.id = c.id
;;