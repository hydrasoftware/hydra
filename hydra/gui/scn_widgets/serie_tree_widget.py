# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
from builtins import str
import os
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QCoreApplication, Qt, pyqtSignal
from qgis.PyQt.QtWidgets import QWidget, QGridLayout, QTreeWidget, QTreeWidgetItem
from qgis.PyQt.QtGui import QFont
from hydra.gui.base_dialog import tr

bold = QFont()
bold.setWeight(QFont.Bold)

class SerieTreeWidget(QWidget):
    serie_changed_signal = pyqtSignal()

    def __init__(self, project, parent) :
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        self.__project = project
        uic.loadUi(os.path.join(current_dir, "serie_tree_widget.ui"), self)
        self.__reloading = False
        self.active_serie = None
        self.active_bloc = None
        self.serie_list = None
        self.bloc_list = None

        self.btn_create_serie.clicked.connect(self.create_serie)
        self.serie_tree.itemSelectionChanged.connect(self.selection_changed)

        self.btn_add.clicked.connect(self.__add)
        self.btn_remove.clicked.connect(self.__remove)

        self.refresh_serie_tree()

    def selection_changed(self):
        if not self.__reloading :
            if self.serie_tree.selectedItems()[0].parent() is None :
                self.active_serie = [self.serie_tree.selectedItems()[0].text(0),self.serie_tree.selectedItems()[0].text(1)]
                self.active_bloc = None
            else :
                self.active_bloc = [self.serie_tree.selectedItems()[0].text(0),self.serie_tree.selectedItems()[0].text(1)]
                for item in self.bloc_list :
                    if int(item[0]) == int(self.active_bloc[0]) :
                        id_cs = item[1]
                for item in self.serie_list :
                    if int(item[0]) == int(id_cs) :
                        name_serie = item[1]
                self.active_serie = [id_cs,name_serie]
            self.serie_changed_signal.emit()

    def refresh_serie_tree(self):
        self.__reloading = True

        self.serie_tree.clear()

        self.serie_list = [[id,name] for id,name in self.__project.execute("""select id, name from project.serie;""").fetchall()]
        self.bloc_list = [[id,id_cs,name] for id,id_cs,name in self.__project.execute("""select id, id_cs, name from project.serie_bloc;""").fetchall()]

        for serie in self.serie_list:
            serie_node = QTreeWidgetItem(self.serie_tree)
            serie_node.setExpanded(True)
            serie_node.setText(0,tr(str(serie[0])))
            serie_node.setText(1,tr(serie[1]))
            for bloc in self.bloc_list:
                if bloc[1] == serie[0] :
                    children_node = QTreeWidgetItem(serie_node)
                    children_node.setExpanded(True)
                    children_node.setText(0,tr(str(bloc[0])))
                    children_node.setText(1,tr(bloc[2]))

        self.__reloading = False

    def __add_list(self, parent, title, table):
        parent_node = QTreeWidgetItem(parent)
        parent_node.setExpanded(True)
        parent_node.setText(0,title)
        list = [name for name, in self.__project.execute("""select name from project.{};""".format(table)).fetchall()]
        for item in sorted(list, key=str.lower):
            children_node = QTreeWidgetItem(parent_node)
            children_node.setFont(0,bold)
            children_node.setText(0,item)

    def find_bloc_time(self, id_serie) :
        t_bloc_max=self.__project.execute("""select max(t_till_start) from project.serie_bloc where id_cs = {id_cs}""".format(id_cs = id_serie)).fetchone()

        t_fin_serie = self.__project.execute("""select tfin from project.serie where id = {id}""".format(id = id_serie)).fetchone()[0]

        t_bloc_max = t_bloc_max[0] if t_bloc_max is not None else None

        if t_bloc_max == None :
            return("'0 days'")
        else :
            return("'"+str(((t_fin_serie+t_bloc_max)/2).days)+" days'")

    def create_serie(self):
        self.__project.execute("""insert into project.serie default values""")
        self.refresh_serie_tree()

    def __add(self):
        if self.active_serie is not None :
            t_till_start = self.find_bloc_time(self.active_serie[0])
            self.__project.execute("""insert into project.serie_bloc (id_cs,t_till_start) values ({id_cs},{time})""".format(id_cs = self.active_serie[0], time = t_till_start))
        self.refresh_serie_tree()

    def __remove(self) :
        if self.active_bloc is not None :
            self.__project.execute("""delete from project.serie_bloc where id = {id} """.format(id = self.active_bloc[0]))
        elif self.active_serie is not None :
            self.__project.execute("""delete from project.serie where id = {id} """.format(id = self.active_serie[0]))
        self.refresh_serie_tree()