# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QTableWidgetItem, QFileDialog, QMessageBox
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from hydra.gui.base_dialog import BaseDialog, tr

class ConfigManager(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "config_manager.ui"), self)

        self.project = project
        self.table_config = None

        models = project.get_models()
        if len(models)==0:
            project.log.notice(tr("No model in project"))
            self.close()

        for model in models :
            self.models_combo.addItem(model)
        self.models_combo.setCurrentIndex(self.models_combo.findText(self.project.get_current_model().name))

        self.table_config = HydraTableWidget(project,
            ("id", "name", "description"),
            (tr("Id"), tr("Name"), tr("Comment")),
            self.models_combo.currentText(), "configuration",
            (self.new_config, self.delete_config, None, None, self.copy_config), "", "id",
            self.table_config_placeholder, showCopy=True)
        self.table_config.set_editable(True)
        self.table_config.set_fixed_first_row(True)
        self.table_config.data_edited.connect(self.save_config)
        self.table_config.setDelegate("LineEdit", 1, {'maxlen': 24})
        self.table_config.table.resizeColumnsToContents()

        self.models_combo.activated.connect(self.model_changed)
        self.model_changed()

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

    def model_changed(self):
        self.table_config.set_dbname(self.models_combo.currentText())

    def new_config(self):
        self.project.execute("""insert into {}.configuration default values;""".format(self.models_combo.currentText()))
        self.table_config.update_data()

    def copy_config(self):
        items = self.table_config.table.selectedItems()
        if len(items)>0:
            source_config = items[1].text()
            target_config = self.project.execute("""insert into {}.configuration default values returning name;""".format(self.models_combo.currentText())).fetchone()[0]

            self.project.execute("""
                update {m}._node
                    set configuration=(configuration::jsonb || jsonb_build_object('{t}', (configuration->'{s}')))::json
                    where configuration is not null and configuration::jsonb ? '{s}';
                update {m}._link
                    set configuration=(configuration::jsonb || jsonb_build_object('{t}', (configuration->'{s}')))::json
                    where configuration is not null and configuration::jsonb ? '{s}';
                update {m}._singularity
                    set configuration=(configuration::jsonb || jsonb_build_object('{t}', (configuration->'{s}')))::json
                    where configuration is not null and configuration::jsonb ? '{s}';
                update {m}._river_cross_section_profile
                    set configuration=(configuration::jsonb || jsonb_build_object('{t}', (configuration->'{s}')))::json
                    where configuration is not null and configuration::jsonb ? '{s}';
                    """.format(m=self.models_combo.currentText(), t=target_config, s=source_config))

            self.table_config.update_data()

    def delete_config(self):
        items = self.table_config.table.selectedItems()
        if len(items)>0:
            id = int(items[0].text())
            if id>1:
                model_current_config = self.project.execute("""select configuration from {}.metadata;""".format(self.models_combo.currentText())).fetchone()
                if model_current_config[0] != id:
                    self.table_config.del_selected_row()
                else:
                    QMessageBox(QMessageBox.Warning, tr('Configuration error'), tr("Cannot delete active configuration {} on model {}.".format(items[1].text(), self.models_combo.currentText())), QMessageBox.Ok).exec_()

    def save_config(self):
        self.table_config.save_selected_row()

    def save(self):
        BaseDialog.save(self)
        self.close()
