
################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import re

"""
the database version corresponding to plugin MAJOR.MINOR.PATCH is MAJOR.MINOR
except for old plugins
"""

old_databse_plugin_version = {
    "1.9b": "1.91",
    "1.9": "1.90",
    "1.8": "1.82",
    "1.7": "1.70",
    "1.6": "1.60",
    "1.5": "1.24",
    "1.4": "1.23",
    "1.31": "1.23",
    "1.3": "1.21",
    "test.1.2.2": "1.21",
    "1.2.1": "1.20",
    "1.2.0": "1.06",
    "1.1.2": "1.05",
    "1.1.1": "1.04",
    "1.1.0": "1.03",
    "1.0.1": "1.02",
    "1.0.0": "0.77",
    "0.0.29": "0.73",
    "0.0.28": "0.71",
    "0.0.27": "0.70",
    "0.0.26": "0.68",
}

def latest():
    recent_versions = []
    for f in os.listdir(os.path.dirname(__file__)):
        m = re.match(r'(.+)-(\d+)\.(\d+)\.(\d+).sql', f)
        if m:
            recent_versions.append((int(m.group(2)), int(m.group(3)), int(m.group(4))))
    return '{}.{}.{}'.format(*max(recent_versions))

def history():
    h = {
        "current_version": latest(),
        "0.0.26": {
        "target": "null",
        "plugin_version": "0.68",
        "scripts": {
            "prj": "null",
            "model": "null"
            }
        }
    }

    for f in os.listdir(os.path.dirname(__file__)):
        m = re.match(r'(.+)-(.+).sql', f)
        if m and not f.endswith('_model.sql') and not f.startswith('.'):
            src, tgt = m.group(1), m.group(2)

            splt = src.split('.')
            if src in old_databse_plugin_version:
                plugin_version = old_databse_plugin_version[src]
            elif len(splt) == 2:
                plugin_version = src+'.0'
            else:
                assert len(splt) == 3
                # next line tests int conversion for new versions
                major, minor, patch = int(splt[0]), int(splt[1]), int(splt[2])
                plugin_version = src

            h[src] = {
                'target': tgt,
                'plugin_version': plugin_version,
                'scripts': {
                    'prj': f'{src}-{tgt}.sql',
                    'model': f'{src}-{tgt}_model.sql'
                    }
                }
            assert os.path.exists(os.path.join(os.path.dirname(__file__), h[src]['scripts']['prj']))
            assert os.path.exists(os.path.join(os.path.dirname(__file__), h[src]['scripts']['model']))
    return h

def version_tuple(version):
    "return tuple(major, minor, patch) or (major, minor)"
    return tuple((int(v) for v in version.replace('b','').replace('test', '').split('.')))

__version__ = latest()
__version_history__ = history()

