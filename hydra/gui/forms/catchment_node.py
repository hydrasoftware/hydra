# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import json
import html
from math import sqrt, exp, log
from qgis.PyQt import uic
from hydra.gui.base_dialog import BaseDialog, tr
import hydra.utility.string as string
import hydra.utility.sql_json as sql_json

class CatchmentNodeEditor(BaseDialog):
    def __init__(self, project, geom, id=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "catchment_node.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.__model = self.project.get_current_model()
        assert not self.__model == None
        self.__geom = geom
        if id==None:
            self.__id = self.__model.add_catchment_node(geom)
        else:
            self.__id = id
            if self.__geom is None:
                geom, = self.project.execute("""
                select geom
                from {}.catchment_node
                where id={}""".format(self.__model.name, self.__id)).fetchone()

        name, area_ha, rl, slope, c_imp, \
            netflow_type, constant_runoff, horner_ini_loss_coef, horner_recharge_coef, \
            holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm, \
            scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm, \
            hydra_surface_soil_storage_rfu_mm, hydra_inf_rate_f0_mm_hr, hydra_int_soil_storage_j_mm, hydra_soil_drainage_time_qres_day, \
            hydra_split_coefficient, hydra_catchment_connect_coef, hydra_aquifer_infiltration_rate, \
            hydra_soil_infiltration_type, \
            gr4_k1, gr4_k2, gr4_k3, gr4_k4, runoff_type, _tc_mn, socose_shape_param_beta, _k_mn, q_limit, q0, \
            network_type, rural_land_use, industrial_land_use, suburban_housing_land_use, dense_housing_land_use, comment = self.project.execute("""
            select name, area_ha, rl, slope, c_imp,
            netflow_type, constant_runoff, horner_ini_loss_coef, horner_recharge_coef,
            holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm,
            scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm,
            hydra_surface_soil_storage_rfu_mm, hydra_inf_rate_f0_mm_hr, hydra_int_soil_storage_j_mm, hydra_soil_drainage_time_qres_day,
            hydra_split_coefficient, hydra_catchment_connect_coef, hydra_aquifer_infiltration_rate,
            hydra_soil_infiltration_type,
            gr4_k1, gr4_k2, gr4_k3, gr4_k4, runoff_type, _tc_mn, socose_shape_param_beta, _k_mn, q_limit, q0,
            network_type, rural_land_use, industrial_land_use, suburban_housing_land_use, dense_housing_land_use, comment
            from {}.catchment_node
            where id={}""".format(self.__model.name, self.__id)).fetchone()

        contour_id, = self.project.execute("""select contour from {m}.catchment_node where id={i}""".format(m=self.__model.name, i=self.__id)).fetchone()


        self.name.setText(name)
        self.area_ha.setText(string.get_str(area_ha))
        self.rl.setText(string.get_str(rl))
        self.slope.setText(string.get_str(slope))
        self.c_imp.setText(string.get_str(c_imp))
        self.btn_autofill.clicked.connect(self.autofill)

        self.area_ha.textChanged.connect(self.__compute_runoff_param)
        self.rl.textChanged.connect(self.__compute_runoff_param)
        self.slope.textChanged.connect(self.__compute_runoff_param)
        self.c_imp.textChanged.connect(self.__compute_runoff_param)

        self.q0.setText(string.get_str(q0))
        self.q_limit.setText(string.get_str(q_limit))

        #initialize netflow
        self.netflow_types = self.populate_combo_from_sqlenum(self.netflow_type,
            "netflow_type", netflow_type)

        self.netflow_type.activated.connect(self.__netflowchanged)
        #self.stacked_netflow.setVisible(False)
        self.__netflowchanged()

        self.hydra_soil_infiltration_type.currentTextChanged.connect(self.__soil_infiltration_type_changed)

        #initialize netflow parameters
        self.cr.setText(string.get_str(constant_runoff))
        self.horner_ini_loss_coef.setText(string.get_str(horner_ini_loss_coef))
        self.horner_recharge_coef.setText(string.get_str(horner_recharge_coef))
        self.holtan_sat_inf_rate_mmh.setText(string.get_str(holtan_sat_inf_rate_mmh))
        self.holtan_dry_inf_rate_mmh.setText(string.get_str(holtan_dry_inf_rate_mmh))
        self.holtan_soil_storage_cap_mm.setText(string.get_str(holtan_soil_storage_cap_mm))
        self.scs_j_mm.setText(string.get_str(scs_j_mm))
        self.scs_soil_drainage_time_day.setText(string.get_str(scs_soil_drainage_time_day))
        self.scs_rfu_mm.setText(string.get_str(scs_rfu_mm))
        self.hydra_surface_soil_storage_rfu_mm.setText(string.get_str(hydra_surface_soil_storage_rfu_mm))
        self.hydra_inf_rate_f0_mm_hr.setText(string.get_str(hydra_inf_rate_f0_mm_hr))
        self.hydra_int_soil_storage_j_mm.setText(string.get_str(hydra_int_soil_storage_j_mm))
        self.hydra_soil_drainage_time_qres_day.setText(string.get_str(hydra_soil_drainage_time_qres_day))
        self.hydra_split_coefficient.setText(string.get_str(hydra_split_coefficient))
        self.hydra_catchment_connect_coef.setText(string.get_str(hydra_catchment_connect_coef))
        self.hydra_aquifer_infiltration_rate.setText(string.get_str(hydra_aquifer_infiltration_rate))
        self.hydra_soil_infiltration_type.setCurrentText(string.get_str(hydra_soil_infiltration_type))
        self.gr4_k1.setText(string.get_str(gr4_k1))
        self.gr4_k2.setText(string.get_str(gr4_k2))
        self.gr4_k3.setText(string.get_str(gr4_k3))
        self.gr4_k4.setText(string.get_str(gr4_k4))

        self.runoff_type_urban.currentTextChanged.connect(self.__runoff_type_urban_changed)
        self.runoff_type_rural.currentTextChanged.connect(self.__runoff_type_rural_changed)


        # set tooltips
        desc = self.project.execute("select name, description from hydra.runoff_type").fetchall()
        tooltip_urban = ("<html><head/><body><p>"+
            "<br/>\n".join([f"<b>{l}</b> {html.escape(d)}\n"
            for l, d in desc if self.runoff_type_urban.findText(l) != -1]) +
            "</p></body></html>")
        tooltip_rural = ("<html><head/><body><p>" +
            "<br/>\n".join([f"<b>{l}</b> {html.escape(d)}\n"
            for l, d in desc if self.runoff_type_rural.findText(l) != -1]) +
            "</p></body></html>")

        self.runoff_type_urban.setToolTip(tooltip_urban)
        self.runoff_type_rural.setToolTip(tooltip_rural)

        # set selection active on runoff type
        if self.runoff_type_rural.findText(runoff_type) != -1 :
            self.socose.setChecked(True)
            self.runoff_type_rural.setCurrentText(runoff_type)
            self.socose_clicked(True)
        elif self.runoff_type_urban.findText(runoff_type) != -1:
            self.linear_reservoir.setChecked(True)
            self.runoff_type_urban.setCurrentText(runoff_type)
            self.linear_reservoir_clicked(True)

        # initialize runoff
        self.linear_reservoir.toggled.connect(self.linear_reservoir_clicked)
        self.socose.toggled.connect(self.socose_clicked)

        #initialize runoff parameters
        self.define_k_mn.setText(string.get_str(_k_mn))
        self.socose_tc_mn.setText(string.get_str(_tc_mn))
        self.socose_shape_param_beta.setText(string.get_str(socose_shape_param_beta))
        self.notes.setText(comment)

        #init contour
        if contour_id:
            contour, =self.project.execute("""select catchment.name
                                        from {m}.catchment, {m}.catchment_node
                                        where catchment.id={i}""".format(m=self.__model.name, i=contour_id)).fetchone()
            self.contour_name.setText(contour)

        #initialize pollution
        self.network_types = self.populate_combo_from_sqlenum(self.network_type,
            "network_type", network_type)
        self.rural_land_use.setText(string.get_str(rural_land_use))
        self.industrial_land_use.setText(string.get_str(industrial_land_use))
        self.suburban_housing_land_use.setText(string.get_str(suburban_housing_land_use))
        self.dense_housing_land_use.setText(string.get_str(dense_housing_land_use))

        self.rural_land_use.textChanged.connect(self.__refresh_total_land_use)
        self.industrial_land_use.textChanged.connect(self.__refresh_total_land_use)
        self.suburban_housing_land_use.textChanged.connect(self.__refresh_total_land_use)
        self.dense_housing_land_use.textChanged.connect(self.__refresh_total_land_use)
        self.__refresh_total_land_use()

        self.btn_pollution.clicked.connect(self.__pollution_clicked)
        self.pollution_groupbox.setVisible(False)

    def autofill(self):
        area_ha, rl, slope, c_imp, cr = self.project.execute("""select area*0.0001, rl, slope, c_imp, c_imp*0.7 from {m}.auto_param_catchment({id})""".format(m=self.__model.name, id=self.__id)).fetchone()

        self.area_ha.setText(string.get_str(area_ha))
        self.rl.setText(string.get_str(rl))
        self.slope.setText(string.get_str(slope))
        self.c_imp.setText(string.get_str(c_imp))
        self.cr.setText(string.get_str(cr))

    def __pollution_clicked(self):
        self.pollution_groupbox.setVisible(not self.pollution_groupbox.isVisible())
        self.pollution_groupbox.setMaximumHeight(16777215 if self.pollution_groupbox.isVisible() else 0)

    def __refresh_total_land_use(self):
        total = sum([string.get_sql_forced_float(line_edit.text()) for line_edit in [self.rural_land_use, self.industrial_land_use, self.suburban_housing_land_use, self.dense_housing_land_use]])
        if total == 100:
            self.label_total.setText('<span style="color:black">'+str(total)+' %</span>')
        else:
            self.label_total.setText('<span style="color:red">'+str(total)+' %</span>')

    def __netflowchanged(self):
        self.stacked_netflow.setCurrentIndex(6) # empty
        stacked_widget_index = {"constant_runoff":0,
                                "horner":1,
                                "holtan":2,
                                "scs":3,
                                "hydra":4,
                                "gr4":5}
        if self.netflow_type.currentIndex() != -1:
            for netflow in self.netflow_types:
                if netflow[0] == self.netflow_type.currentText():
                    self.stacked_netflow.setCurrentIndex(stacked_widget_index[netflow[1]])
                    # no runoff needed if selected netflow is GR4
                    self.runoff.setEnabled(netflow[1]!='gr4')
        self.__compute_runoff_param()

    def __soil_infiltration_type_changed(self, txt):
        self.hydra_split_coefficient.setEnabled(txt=='split')
        self.hydra_inf_rate_f0_mm_hr.setEnabled(txt=='rate')
        self.hydra_int_soil_storage_j_mm.setEnabled(txt=='rate')


    def linear_reservoir_clicked(self, enabled):
        if enabled:
            self.runoff_type_urban.setEnabled(True)
            self.define_k_mn.setEnabled(self.runoff_type_urban.currentText() == 'Define K')
            self.runoff_type_rural.setEnabled(False)
            self.socose_tc_mn.setEnabled(False)
            self.socose_shape_param_beta.setEnabled(False)

    def socose_clicked(self, enabled):
        if enabled:
            self.runoff_type_urban.setEnabled(False)
            self.define_k_mn.setEnabled(False)
            self.runoff_type_rural.setEnabled(True)
            self.socose_tc_mn.setEnabled(self.runoff_type_rural.currentText() == 'Define Tc')
            self.socose_shape_param_beta.setEnabled(True)

    def __runoff_type_rural_changed(self, txt):
        self.socose_tc_mn.setEnabled(txt == 'Define Tc')
        self.__compute_runoff_param()


    def __runoff_type_urban_changed(self, txt):
        self.define_k_mn.setEnabled(txt == 'Define K')
        self.__compute_runoff_param()

    def __compute_runoff_param(self):

        try:
            A = float(self.area_ha.text())
            L = float(self.rl.text())
            I = float(self.slope.text())
            C = float(self.c_imp.text())
            Cr= float(self.cr.text()) if self.netflow_type.currentText().lower() == 'constant runoff' else None
            RFU = 1e-6
            if self.netflow_type.currentText().lower() == 'hydra':
                RFU = float(self.hydra_surface_soil_storage_rfu_mm.text())
            elif self.netflow_type.currentText().lower() == 'scs':
                RFU = float(self.scs_rfu_mm.text())
        except ValueError as e:
            return

        if self.socose.isChecked(): # Rural
            type_ = self.runoff_type_rural.currentText()
            if type_ != 'Define Tc':
                Tc = str(self.project.execute("select project.socose_tc_mn(%s, %s, %s, %s, %s, %s)", (type_, A, L, I, C, RFU)).fetchone()[0])
            else:
                Tc = str(self.project.execute(f"select socose_tc_mn from {self.__model.name}.catchment_node where id=%s", (self.__id,)).fetchone()[0])
            self.socose_tc_mn.setText(Tc)


        else: # Urban
            type_ = self.runoff_type_urban.currentText()
            if type_ != 'Define K':
                K = str(self.project.execute("select project.define_k_mn(%s, %s, %s, %s, %s, %s)", (type_, A, L, I, C, Cr)).fetchone()[0])
            else:
                K = str(self.project.execute(f"select define_k_mn from {self.__model.name}.catchment_node where id=%s", (self.__id,)).fetchone()[0])
            self.define_k_mn.setText(K)

    def get_id(self):
        return self.__id

    def get_runoff_type(self):
        res_runoff_type = self.project.execute("""
                    select name
                    from hydra.runoff_type
                    where description='{}'""".format(self.runoff_type.currentText())).fetchone()
        if res_runoff_type:
            return res_runoff_type[0]
        else:
            return None

    def save(self):
        self.project.log.notice(tr("Saved"))

        netflow_type = None
        res_netflow_type = self.project.execute("""
            select name
            from hydra.netflow_type
            where description='{}'""".format(self.netflow_type.currentText())).fetchone()
        if res_netflow_type:
            netflow_type = res_netflow_type[0]

        if self.runoff_type_rural.currentText() == 'Define Tc':
            socose_tc_mn = string.get_sql_float(self.socose_tc_mn.text())
        else:
            socose_tc_mn, = self.project.execute(f"select socose_tc_mn from {self.__model.name}.catchment_node where id=%s", (self.__id,)).fetchone()

        if self.runoff_type_urban.currentText() == 'Define K':
            define_k_mn = string.get_sql_float(self.define_k_mn.text())
        else:
            define_k_mn, = self.project.execute(f"select define_k_mn from {self.__model.name}.catchment_node where id=%s", (self.__id,)).fetchone()

        runoff_type = None
        if self.socose.isChecked():
            runoff_type = self.runoff_type_rural.currentText()
        elif self.linear_reservoir.isChecked():
            runoff_type = self.runoff_type_urban.currentText()

        network_type = None
        res_network_type = self.project.execute("""
            select name
            from hydra.network_type
            where description='{}'""".format(self.network_type.currentText())).fetchone()
        if res_network_type:
            network_type = res_network_type[0]

        self.__model.update_catchment_node(self.__id, self.__geom, self.name.text(),
            string.get_sql_float(self.area_ha.text()), string.get_sql_float(self.rl.text()), string.get_sql_float(self.slope.text()), string.get_sql_float(self.c_imp.text()),
            netflow_type, string.get_sql_float(self.cr.text()), string.get_sql_float(self.horner_ini_loss_coef.text()), string.get_sql_float(self.horner_recharge_coef.text()),
            string.get_sql_float(self.holtan_sat_inf_rate_mmh.text()), string.get_sql_float(self.holtan_dry_inf_rate_mmh.text()), string.get_sql_float(self.holtan_soil_storage_cap_mm.text()),
            string.get_sql_float(self.scs_j_mm.text()), string.get_sql_float(self.scs_soil_drainage_time_day.text()), string.get_sql_float(self.scs_rfu_mm.text()),
            string.get_sql_float(self.hydra_surface_soil_storage_rfu_mm.text()), string.get_sql_float(self.hydra_inf_rate_f0_mm_hr.text()), string.get_sql_float(self.hydra_int_soil_storage_j_mm.text()), string.get_sql_float(self.hydra_soil_drainage_time_qres_day.text()),
            string.get_sql_float(self.hydra_split_coefficient.text()), string.get_sql_float(self.hydra_catchment_connect_coef.text()), string.get_sql_float(self.hydra_aquifer_infiltration_rate.text()),
            self.hydra_soil_infiltration_type.currentText(),
            string.get_sql_float(self.gr4_k1.text()), string.get_sql_float(self.gr4_k2.text()), string.get_sql_float(self.gr4_k3.text()), string.get_sql_float(self.gr4_k4.text()),
            runoff_type, socose_tc_mn, string.get_sql_float(self.socose_shape_param_beta.text()),
            define_k_mn, string.get_sql_float(self.q_limit.text()), string.get_sql_float(self.q0.text()), None, network_type, string.get_sql_float(self.rural_land_use.text()),
            string.get_sql_float(self.industrial_land_use.text()), string.get_sql_float(self.suburban_housing_land_use.text()), string.get_sql_float(self.dense_housing_land_use.text()), comment=self.notes.text())

        BaseDialog.save(self)
        self.close()
