################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import json
import os
if os.name == 'nt':
    pass
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QCoreApplication, Qt
from qgis.PyQt.QtWidgets import QMessageBox, QWidget, QTableWidgetItem, QComboBox, QVBoxLayout, QDialogButtonBox, QInputDialog
from qgis.core import QgsDataSourceUri
from db_manager.dlg_import_vector import DlgImportVector
from db_manager.db_plugins.postgis.plugin import PostGisDBPlugin, PGDatabase
from hydra.gui.widgets.default_value_widget import DefaultValueWidget
from hydra.gui.base_dialog import BaseDialog
from hydra.utility.settings_properties import SettingsProperties

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

with open(os.path.join(os.path.dirname(__file__), "import_shapefile.json")) as keys_file:
    __json_keys__ = json.load(keys_file)

class WorkManager(BaseDialog):
    def __init__(self, project, json_keys=__json_keys__, parent=None, source=None, dest=None):
        BaseDialog.__init__(self, project, parent)
        self.json_keys = json_keys
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "work_manager.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.__isloading = False
        self.__connection_table_row = {}

        self.dlg = None

        # init the table source selection in schema work and its geom type
        self.table_source= None
        self.geomtype = ""

        # content of schema work as a list of table's name
        self.work_content = None

        # init list for knowing table's name in schema work, preventing user to re-use it
        self.name_table_in_db = []
        self.work_table_details_list = []

        # connect event for change in work schema table selection
        self.work_schema_table.itemSelectionChanged.connect(self.refresh_connection_table)
        self.work_schema_table.itemSelectionChanged.connect(self.refresh_work_table_details)

        # set table's details for selected table in schema work, hide label for number of line in the init
        self.work_table_details.setColumnCount(2)
        self.work_table_details.setHorizontalHeaderLabels(['Column','Type'])

        self.label_lines.setVisible(False)

        # Combo for project/model choice
        self.comboSchema.addItem("project")
        models = self.project.get_models()
        for model in models :
            self.comboSchema.addItem(model)
        current_model = self.project.get_current_model()
        if current_model:
            self.comboSchema.setCurrentIndex(self.comboSchema.findText(self.project.get_current_model().name))

        # connect the event of combo change for target schema and table for import (project/model), (pipe,manhole,point_terrain)
        self.comboSchema.currentIndexChanged.connect(self.__schema_changed)
        self.comboTable.currentIndexChanged.connect(self.__work_table_changed)

        self.__schema_changed()

        # connect the button setup
        self.btnAdd.clicked.connect(self.add)
        self.btnDel.clicked.connect(self.delete)
        self.btn_import.clicked.connect(self.import_hydra)
        self.btn_preview.clicked.connect(self.open_layer)
        self.advanced_chk.stateChanged.connect(self.change_advanced_mode)

        # call first refresh to init the work schema table and the import
        self.refresh_connection_table()
        self.refresh_work_schema_table()

        if source is not None:
            self.work_schema_table.setCurrentItem(self.work_schema_table.findItems(source, Qt.MatchExactly)[0])

        if dest is not None:
            schema, table = dest.split('.')
            self.comboSchema.setCurrentIndex(self.comboSchema.findText(schema))
            self.comboTable.setCurrentIndex(self.comboTable.findText(table))

    def set_default_value(self, name, value):
        for i in range(self.connection_table.rowCount()):
            if self.connection_table.item(i,0).text() == name:
                self.connection_table.cellWidget(i,3).activate_checkbox.setCheckState(True)
                self.connection_table.cellWidget(i,3).default_value.setText(value)

    def set_connection(self, name, field):
        for i in range(self.connection_table.rowCount()):
            if self.connection_table.item(i,0).text() == name:
                self.connection_table.cellWidget(i,1).layout().itemAt(0).widget().setCurrentIndex(
                        self.connection_table.cellWidget(i,1).layout().itemAt(0).widget().findText(field))

    def auto_complete(self):
        for i in range(self.connection_table.rowCount()):
            for col, def_ in self.json_keys[self.comboTable.currentText()]['columns'].items():
                if def_['name'] == self.connection_table.item(i, 0).text():
                    widget =  self.connection_table.cellWidget(i, 1).layout().itemAt(0).widget()
                    if type(widget) == QComboBox:
                        for j in range(widget.count()):
                            if col.find(widget.itemText(j)) != -1:
                                self.connection_table.cellWidget(i, 1).layout().itemAt(0).widget().setCurrentIndex(j)

    def change_advanced_mode(self):
        self.__schema_changed()

    def open_layer(self):
        if self.table_source :
            layer_name, ok = QInputDialog.getText(None, tr('Preview data'),tr('Chose a name for the previewed layer'), text=self.table_source)
            if layer_name and ok :
                list_geom_col = [name for name, in
                self.project.execute(
                """select f_geometry_column
                from geometry_columns
                where f_table_schema = 'work'
                and f_table_name = '{}'
                """.format(self.table_source)).fetchall()
                ]

                if len(list_geom_col) > 1:
                    geom, ok = QInputDialog.getItem(None, tr('Geometry column'),tr('confirm the geometry column name'), list_geom_col, 0, False)
                    if geom and ok :
                        self.project.load_work_layer(self.table_source, layer_name, geom)
                else :
                    self.project.load_work_layer(self.table_source, layer_name, list_geom_col[0])

    def refresh_connection_table(self):
        if not self.__isloading :
            self.__isloading = True
            if self.work_schema_table.selectedItems():
                self.table_source = self.work_schema_table.selectedItems()[0].text()
                self.gb_import.setEnabled(True)
            else:
                self.gb_import.setEnabled(False)

            if self.table_source :
                self.label_workname.setText("Select parameters to import table {}".format(self.table_source))
            else:
                self.label_workname.setText("")

            self.connection_table.setColumnCount(4)
            headerH=["Target column","Source column","Data type","Default value ?"]
            self.connection_table.setHorizontalHeaderLabels(headerH)
            if self.table_source is not None and self.table_source != ""  :
                self.geomtype = self.project.execute( """select type from geometry_columns where f_table_name = '{}' """.format(self.table_source)).fetchone()[0]
            else :
                self.geomtype = ""

            self.__schema_changed()
            self.__isloading = False

    def refresh_work_schema_table(self):
        self.work_content = [ name for (name,) in self.project.execute("""select distinct f_table_name from geometry_columns where f_table_schema = 'work'""").fetchall()]

        for name in self.work_content:
            if name not in self.name_table_in_db:
                self.name_table_in_db.append(name)

        self.work_schema_table.clear()
        self.work_schema_table.setRowCount(len(self.work_content))
        self.work_schema_table.setColumnCount(1)

        for i in range (len(self.work_content)) :
            item = QTableWidgetItem(self.work_content[i])
            comment_query = f"""SELECT obj_description('work.{self.work_content[i]}'::regclass, 'pg_class');"""
            comment = self.project.execute(comment_query).fetchone()[0]
            if comment:
                item.setToolTip(comment)
            self.work_schema_table.setItem(i,0,item)


    def refresh_work_table_details(self):
        if self.table_source :
            self.work_table_details_list = [[name,type] for (name,type) in self.project.execute("""select column_name, data_type from information_schema.columns where table_name = '{table}' and table_schema='work'""".format(table=self.table_source)).fetchall()]

        self.work_table_details.setRowCount(len(self.work_table_details_list))

        for i in range(len(self.work_table_details_list)) :

            if self.work_table_details_list[i][1] == "USER-DEFINED":
                type_geom = self.project.execute("""select type from geometry_columns
                            where f_table_name = '{table}'
                            and f_table_schema = 'work'
                            and f_geometry_column = '{col}'""".format(table=self.table_source,col=self.work_table_details_list[i][0])).fetchone()

                if type_geom is not None :
                    self.work_table_details_list[i][1] = type_geom[0]

            self.work_table_details.setItem(i,0,QTableWidgetItem(self.work_table_details_list[i][0]))
            self.work_table_details.setItem(i,1,QTableWidgetItem(self.work_table_details_list[i][1]))

        if self.table_source and self.project.execute("""select f_geometry_column from geometry_columns where f_table_name = '{}'""".format(self.table_source)).fetchall() != [] :
            #geom_col = [geom_col for (geom_col,) in self.project.execute("""select f_geometry_column from geometry_columns where f_table_name = '{}'""".format(self.table_source)).fetchall()][0]

            nb_lines = int(self.project.execute(''' select count(*) from work."{}"'''.format(self.table_source)).fetchone()[0])
            self.label_lines.setText("Number of items: " + str(nb_lines))
            self.label_lines.setVisible(True)
        else:
            self.label_lines.setVisible(False)


    def add(self):
        uri = QgsDataSourceUri()
        uri.setConnection(SettingsProperties.get_service(), self.project.name, '', '')
        
        dbplugin = PostGisDBPlugin(self.project.name)
        db = PGDatabase(dbplugin, uri)
        
        self.dlg = DlgImportVector(None, db, uri, None)
        self.dlg.cboInputLayer.setCurrentIndex(-1)
        self.dlg.chkLowercaseFieldNames.setChecked(True)

        # hide and preselect schema work , you cannot disable or it won't import
        self.dlg.cboSchema.setCurrentText('work')
        self.dlg.cboSchema.setVisible(False)

        # hide and check the checkbox to set a spatial index on import
        self.dlg.chkSpatialIndex.setChecked(True)
        self.dlg.chkSpatialIndex.setVisible(False)

        if self.dlg.exec_():
            file_path = self.dlg.cboInputLayer.currentText()
            table_name = self.dlg.cboTable.currentText()
            #table_file = os.path.splitext(os.path.basename(file_path))[0]
            if table_name.lower() != table_name:
                self.rename_table(table_name, table_name.lower())
        
            self.add_comment(table_name, file_path)

            self.refresh_work_schema_table()
            self.dlg = None

    def rename_table(self, old_name, new_name):
        query = f'ALTER TABLE "work"."{old_name}" RENAME TO "{new_name}";'
        self.project.execute(query)

    def add_comment(self, table_name, file_path):
        query = f"""
        COMMENT ON TABLE work.{table_name} IS '{file_path}';
        """
        self.project.execute(query)

    def delete(self):
        self.__isloading = True
        table_name = None
        if self.work_schema_table.selectedItems() :
            table_name = self.work_schema_table.selectedItems()[0].text()
        if table_name is not None :
            confirm = QMessageBox.question(self, tr('Confirm'), tr(f"Are you sure you want to delete work.{table_name} ?"), QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if confirm == QMessageBox.Yes:
                self.project.execute('''drop table work."{}"'''.format(table_name))
                BaseDialog.save(self)

        self.refresh_work_schema_table()
        self.__isloading = False

    def __schema_changed(self):
        schema = self.comboSchema.currentText() if self.comboSchema.currentText() =='project' else 'model'
        self.comboTable.clear()
        for item in self.json_keys:
            if str(self.geomtype) in str(self.json_keys[item]["geom"]["typeimport"]) and schema==str(self.json_keys[item]["schema"]):
                self.comboTable.addItem(str(item))
        self.__work_table_changed()

    def __work_table_changed(self):
        self.__connection_table_row = {}
        table = self.comboTable.currentText()
        self.connection_table.setRowCount(0)
        if table and self.table_source:
            if self.advanced_chk.isChecked():
                self.connection_table.setRowCount(len(self.json_keys[table]["columns"])+len(self.json_keys[table]["advanced_columns"])+1)
            else:
                self.connection_table.setRowCount(len(self.json_keys[table]["columns"])+1)

            if self.advanced_chk.isChecked():
                list_column_tab = [self.json_keys[table]["geom"]["namecolumn"]] + [item for item in self.json_keys[table]["columns"]] + [item for item in self.json_keys[table]["advanced_columns"]]
            else:
                list_column_tab = [self.json_keys[table]["geom"]["namecolumn"]] + [item for item in self.json_keys[table]["columns"]]

            headerV = [self.json_keys[table]["geom"]["namecolumn"]]

            for key in list(self.json_keys[table]["columns"].keys()):
                headerV.append(self.json_keys[table]["columns"][key]["name"])

            if self.advanced_chk.isChecked():
                for key in list(self.json_keys[table]["advanced_columns"].keys()):
                    headerV.append(self.json_keys[table]["advanced_columns"][key]["name"])

            columnlistrequest = self.project.execute( "select column_name, data_type from information_schema.columns WHERE table_name = '{}' and table_schema='work'".format(self.table_source)).fetchall()
            columnlist=[]
            columnlisttype=[]

            for column in columnlistrequest:
                columnlist.append(str(column[0]))
                columnlisttype.append(str(column[1]))

            listtype = [self.json_keys[table]["geom"]["type"]]
            for key in list(self.json_keys[table]["columns"].keys()):
                listtype.append(self.json_keys[table]["columns"][key]["type"])
            if self.advanced_chk.isChecked():
                for key in list(self.json_keys[table]["advanced_columns"].keys()):
                    listtype.append(self.json_keys[table]["advanced_columns"][key]["type"])

            for i in range (self.connection_table.rowCount()):
                #Target column
                label = QTableWidgetItem()
                label.setText(headerV[i])
                label.setTextAlignment(Qt.AlignCenter)
                self.connection_table.setItem(i,0,label)

                # Source column , combo box
                for key in list(self.json_keys[table]["columns"].keys()):
                    if self.connection_table.item(i,0).text() == self.json_keys[table]["columns"][key]["name"]:
                        type = self.json_keys[table]["columns"][list_column_tab[i]]["type"]
                        combo = self.__create_combo(columnlist,columnlisttype,type,True)
                        vlayout = QVBoxLayout()
                        vlayout.addWidget(combo)
                        widget = QWidget()
                        widget.setLayout(vlayout)
                        self.connection_table.setCellWidget(i,1,widget)
                        self.__connection_table_row[key] = i

                if self.advanced_chk.isChecked():
                    for key in list(self.json_keys[table]["advanced_columns"].keys()):
                        if self.connection_table.item(i,0).text() == self.json_keys[table]["advanced_columns"][key]["name"]:
                            type = self.json_keys[table]["advanced_columns"][list_column_tab[i]]["type"]
                            combo = self.__create_combo(columnlist,columnlisttype,type,True)
                            vlayout = QVBoxLayout()
                            vlayout.addWidget(combo)
                            widget = QWidget()
                            widget.setLayout(vlayout)
                            self.connection_table.setCellWidget(i,1,widget)
                            self.__connection_table_row[key] = i

                #Source column geom, combo box
                if self.connection_table.item(i,0).text() == self.json_keys[table]["geom"]["namecolumn"] :
                    columnlistgeom=[]
                    columnlisttypegeom=[]
                    columnlistrequestgeom = self.project.execute( "select column_name, data_type from information_schema.columns WHERE table_name = '{t}' and table_schema='work' and column_name IN (SELECT f_geometry_column FROM public.geometry_columns WHERE f_table_name = '{t}')".format(t=self.table_source)).fetchall()
                    for column in columnlistrequestgeom:
                        columnlistgeom.append(str(column[0]))
                        columnlisttypegeom.append(str(column[1]))

                    type = self.json_keys[table]["geom"]["type"]
                    combo = self.__create_combo(columnlistgeom,columnlisttypegeom,type)
                    vlayoutgeom = QVBoxLayout()
                    vlayoutgeom.addWidget(combo)
                    widgetgeom = QWidget()
                    widgetgeom.setLayout(vlayoutgeom)
                    self.connection_table.setCellWidget(i,1,widgetgeom)
                    self.__connection_table_row['geom'] = i

                #data type column
                label2 = QTableWidgetItem()
                label2.setText(listtype[i])
                label2.setTextAlignment(Qt.AlignCenter)
                self.connection_table.setItem(i,2,label2)

                #default value column
                for key in list(self.json_keys[table]["columns"].keys()):
                    if self.json_keys[table]["columns"][key]["default"] == "True" and list_column_tab[i] == key:
                        defaultwidget = DefaultValueWidget(self.connection_table)
                        self.connection_table.setCellWidget(i,3,defaultwidget)

                if self.advanced_chk.isChecked():
                    for key in list(self.json_keys[table]["advanced_columns"].keys()):
                        if self.json_keys[table]["advanced_columns"][key]["default"] == "True" and list_column_tab[i] == key:
                            defaultwidget = DefaultValueWidget(self.connection_table)
                            self.connection_table.setCellWidget(i,3,defaultwidget)

            self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True)
            self.btn_import.setEnabled(True)
        else:
            self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)
            self.btn_import.setEnabled(False)

        self.auto_complete()

    def __create_combo(self, list, listtype, type, nullable=False):
        combobox = QComboBox()
        if nullable:
            combobox.addItem("")
        for i in range(0, len(list)):
            if type == "real" :
                if listtype[i] in ["numeric","integer",'decimal','double precision'] :
                    temp = tr(list[i])
                    combobox.addItem("{}".format(temp))
            elif type == "string" :
                if listtype[i] == "character varying" or listtype[i] == "text" :
                    temp = tr(list[i])
                    combobox.addItem("{}".format(temp))
            elif type == "string|real" :
                if listtype[i] in ["numeric","integer",'decimal','double precision','character varying','text'] :
                    temp = tr(list[i])
                    combobox.addItem("{}".format(temp))
            else :
                temp = tr(list[i])
                combobox.addItem("{}".format(temp))
        if not nullable :
            combobox.setCurrentIndex(combobox.findText("geom"))

        return combobox

    def __connection_table_text_for(self, key):
        return self.connection_table.cellWidget(self.__connection_table_row[key], 1).layout().itemAt(0).widget().currentText()

    def __connection_table_has(self, key):
        return key in self.__connection_table_row

    def check_manholes_for_pipes(self):

        geom_column_source_table=self.__connection_table_text_for('geom')

        node_missing = self.project.execute("""
        with link_limits_points_temp (geom) as (
        select ST_SetSRID(ST_StartPoint((St_Dump(work."{table_source}".{geom_column})).geom),{srid}) as geom from work."{table_source}"
        UNION
        select ST_SetSRID(ST_EndPoint((St_Dump(work."{table_source}".{geom_column})).geom),{srid}) as geom from work."{table_source}"
        )
        select exists (select 1 from link_limits_points_temp as l where st_force2d(l.geom) not in (
            SELECT st_force2d({model}.manhole_node.geom) from {model}.manhole_node
            union
            SELECT st_force2d({model}.manhole_hydrology_node.geom) from {model}.manhole_hydrology_node
            ))
        """.format(
            model=self.comboSchema.currentText(),
            geom_column=geom_column_source_table,
            table_source=self.table_source,
            srid=self.project.srid)).fetchone()[0]

        if node_missing :
            confirm = QMessageBox(QMessageBox.Question, tr('Pipes with no manholes'), tr('Missing manholes will be generated at start and end of pipes. Proceed ?'), QMessageBox.Ok | QMessageBox.Cancel).exec_()
            if confirm == QMessageBox.Ok:
                self.project.execute("""with link_limits_points_temp (geom) as (
                select st_force2d(ST_SetSRID(ST_StartPoint((St_Dump(work."{table_source}".{geom_column})).geom),{srid})) as geom from work."{table_source}"
                UNION
                select st_force2d(ST_SetSRID(ST_EndPoint((St_Dump(work."{table_source}".{geom_column})).geom),{srid})) as geom from work."{table_source}"
                )
                insert into {model}.manhole_node (geom)
                select distinct ST_SetSRID(St_MakePoint(ST_X(geom),ST_Y(geom),project.altitude(geom)),ST_SRID(geom)) from link_limits_points_temp as l
                where l.geom not in (
                SELECT st_force2d({model}.manhole_node.geom)
                from {model}.manhole_node
                union
                SELECT st_force2d({model}.manhole_hydrology_node.geom)
                from {model}.manhole_hydrology_node)""".format(model=self.comboSchema.currentText(),
                                                    table_source=self.table_source,
                                                    geom_column=geom_column_source_table,
                                                    srid=self.project.srid)
                                                    )
                return(True)
            else :
                return (False)
        return(True)

    def check_node_for_hydrograph(self):

        geom_column_source_table=self.__connection_table_text_for('geom')

        manholes_missing = self.project.execute("""
        with points_in_table (geom) as (
        select {geom_column} from work."{table_source}"
        )
        select exists (select 1 from points_in_table as l where l.geom not in ( SELECT {model}._node.geom from {model}._node))
        """.format(
            model=self.comboSchema.currentText(),
            geom_column=geom_column_source_table,
            table_source=self.table_source)).fetchone()[0]

        if manholes_missing :
            confirm = QMessageBox(QMessageBox.Question, tr('Singularity with no nodes'), tr('You need to add nodes. Proceed ?'), QMessageBox.Ok | QMessageBox.Cancel).exec_()
            if confirm == QMessageBox.Ok:
                type_node, ok = QInputDialog.getItem(None, tr("Insert nodes"), tr("node type :"), ["manhole", "river"], 0, False)
                if type_node and ok :
                    return(True, type_node)
        return(False, None)

    def insert_node_for_hydrograph(self, type_node):

        geom_column_source_table=self.__connection_table_text_for('geom')

        self.project.execute("""
        insert into {model}.{type_node}_node (geom)
        select ST_SetSRID(St_MakePoint(ST_X({geom_column}),ST_Y({geom_column}),project.altitude({geom_column})),ST_SRID({geom_column})) from work."{table_source}" as t
        where t.geom not in (
        SELECT {model}.{type_node}_node.geom
        from {model}.{type_node}_node)""".format(model=self.comboSchema.currentText(),
                                            table_source=self.table_source,
                                            geom_column=geom_column_source_table,
                                            type_node=type_node))

    def import_new_terrain_group(self):
        ''' import new terrain points group if doesn't exist'''

        group_name_col = self.__connection_table_text_for('points_type')

        if group_name_col:

            group_name = self.project.execute(""" select distinct {group_name_col}::text from work."{table_source}" """.format(group_name_col=group_name_col, table_source=self.table_source)).fetchall()

            group_name = [elem.lower().replace(' ','_') for (elem,) in group_name if elem is not None]

            group_name_in_db = [elem for (elem,) in self.project.execute("""select name from project.points_type""").fetchall()]

            group_name_not_in_db = [elem for elem in group_name if elem not in group_name_in_db]

            new_groups = []

            for name in group_name_not_in_db:
                name_id = self.project.execute(""" insert into project.points_type (name)
                values ('{name}') returning name, id""".format(name=name)).fetchall()
                new_groups.append(name_id)

        return


    def insert_coverage_marker(self):
        ''' add coverage_marker from either points or polygon layer '''

        schema = self.comboSchema.currentText()

        if 'POINT' in self.geomtype:
            if self.geomtype == 'ZPOINT' or self.geomtype == 'ZMPOINT':
                self.project.execute("""insert into {model}."coverage_marker" (geom)
                select St_setsrid(St_MakePoint(ST_X(geom),ST_Y(geom),ST_Z(geom)),St_srid(geom)) from work."{table}" """.format(model=schema, table=self.table_source))
            if self.geomtype == 'POINT':
                self.project.execute("""insert into {model}."coverage_marker" (geom)
                select st_force3d(geom) from work."{table}" """.format(model=schema, table=self.table_source))

        if 'POLYGON' in self.geomtype:
            self.project.execute("""insert into {model}."coverage_marker" (geom)
            select st_force3d(St_PointOnSurface(geom))  from work."{table}" """.format(model=schema, table=self.table_source))

    def fuse_reach(self):
        ''' look for 2 reachs that have match at the extremity, then fuse them'''

        reachs = self.project.execute(
            """ with couple as (
            select row_number() OVER() as id_couple,
            up.id as id_up,
            down.id as id_down
            from {model}.river_node as nod, {model}.reach as up, {model}.reach as down
            where St_equals(nod.geom, St_StartPoint(down.geom))
            and St_equals(nod.geom, St_EndPoint(up.geom))
            ),

            bad_couple as (
            select distinct c1.id_couple
            from couple as c1, couple as c2
            where (c1.id_up = c2.id_up or c1.id_down = c2.id_down)
            and c1.id_couple != c2.id_couple
            )

            select distinct couple.id_up, couple.id_down
            from couple
            where couple.id_couple
            not in
            (select id_couple from bad_couple)
            limit 1 """.format(model=self.comboSchema.currentText())).fetchall()

        if reachs:
            self.project.set_current_model(self.comboSchema.currentText())
            self.project.get_current_model().fuse_reach(reachs[0][0], reachs[0][1])
            self.fuse_reach()

    def requete_insert(self, schema, table):
        statement = ""
        statementinsert = []
        statementselect = []
        statementwhere = []
        #list_column = [item for (item,) in self.project.execute( "select column_name from information_schema.columns WHERE table_name = '{}' and table_schema='work'".format(self.table_source)).fetchall()]
        statement += "insert into {}.{}".format(schema,table)
        if self.json_keys[table]["properties"]["type"] != "" :
            statement += "_{}".format(self.json_keys[table]["properties"]["type"])
        statement += " ("
        for i in range(self.connection_table.rowCount()):
            default = self.connection_table.cellWidget(i,3).get_default_value() if self.connection_table.cellWidget(i,3) else False
            table_source_column = self.connection_table.cellWidget(i,1).layout().itemAt(0).widget().currentText()

            table_target_column = None
            for key in list(self.json_keys[table]["columns"].keys()):
                if self.json_keys[table]["columns"][key]["name"] == self.connection_table.item(i,0).text():
                    table_target_column = key
            if self.advanced_chk.isChecked():
                for key in list(self.json_keys[table]["advanced_columns"].keys()):
                    if self.json_keys[table]["advanced_columns"][key]["name"] == self.connection_table.item(i,0).text():
                        table_target_column = key
            if table_target_column is None:
                table_target_column = "geom"

            table_datatype_column = self.connection_table.item(i,2).text().lower().replace(' ','_')

            if (table_target_column == self.json_keys[table]["geom"]["namecolumn"]):
                # geometry part of query
                geomstatement = self.create_geom_statement(table)
                if geomstatement:
                    statementinsert.append(table_target_column)
                    statementselect.append(geomstatement)
            elif default:
                statementinsert.append(table_target_column)
                statementselect.append("'" + default + "'" )
            elif table_source_column != '' and table_target_column == 'cross_section_type' :
                statementinsert.append(table_target_column)
                statementselect.append(table_source_column+'::hydra_cross_section_type')
            elif table_source_column != '' and table_target_column == 'points_type' :
                statementinsert.append("points_id")
                statementselect.append("(select id from project.points_type where lower(name) = lower({}.{}))".format(self.table_source, table_source_column))
            elif table_source_column != '' and table_target_column == 'constrain_type' :
                statementinsert.append(table_target_column)
                statementselect.append(table_source_column+'::hydra_constrain_type')
            elif table_source_column != '' and table_datatype_column == 'pollution_treatment_mode' :
                statementinsert.append(table_target_column)
                statementselect.append(table_source_column+'::hydra_pollution_treatment_mode')
            elif table_source_column != '' and table_datatype_column == 'link_type' :
                statementinsert.append(table_target_column)
                statementselect.append(table_source_column+'::hydra_link_type')
            elif table_source_column != '' and table_datatype_column == 'hydra_split_law_type' :
                statementinsert.append(table_target_column)
                statementselect.append(table_source_column+'::hydra_split_law_type')
            elif table_source_column != '' and table_datatype_column == 'hydra_runoff_type' :
                statementinsert.append(table_target_column)
                statementselect.append(table_source_column+'::hydra_runoff_type')
            elif table_source_column != '' and table_datatype_column == 'hydra_netflow_type' :
                statementinsert.append(table_target_column)
                statementselect.append(table_source_column+'::hydra_netflow_type')
            elif table_source_column != '' and table_target_column == 'cp_geom' :
                statementinsert.append(table_target_column)
                statementselect.append(table_source_column)
                statementwhere.append(" and coalesce(cp_geom, 0) in (select id from {}.closed_parametric_geometry union select 0)".format(schema))
            elif table_source_column != '' and table_target_column == 'op_geom' :
                statementinsert.append(table_target_column)
                statementselect.append(table_source_column)
                statementwhere.append(" and coalesce(op_geom, 0) in (select id from {}.open_parametric_geometry union select 0)".format(schema))
            elif table_source_column != '' and table_datatype_column == 'array_real' :
                statementinsert.append(table_target_column)
                statementselect.append(table_source_column+'::real[]')
            elif table_source_column == '' and table_target_column == 'z_ground':
                statementinsert.append(table_target_column)
                statementselect.append("project.altitude(ST_MakePoint(ST_X({point}),ST_Y({point})))".format(point = self.__connection_table_text_for('geom')))
            elif table_source_column != '':
                statementinsert.append(table_target_column)
                statementselect.append('"'+table_source_column+'"')

        statement += (', ').join(statementinsert)
        statement += ") select "
        statement += (', ').join(statementselect)
        statement += " from {}.work.{}".format(self.project.get_current_project(),self.table_source)
        statement += " where ST_IsValid(St_MakeValid({geom})) and ST_NumGeometries({geom}) = 1".format(geom=self.__connection_table_text_for('geom'))

        for where in statementwhere :
            statement += where

        return statement

    def create_geom_statement(self, table):
        geomstatement = ""
        # specific geom statement for manhode_node insert
        if table == "manhole" or table == "manhole_hydrology":
            if self.__connection_table_has('z_ground'):
                if self.__connection_table_text_for('z_ground'):
                    geomstatement = """ case
                                        when "{z_data}" is not null then ST_SetSRID(ST_MakePoint(ST_X({point}),ST_Y({point}),"{z_data}"),ST_SRID({point}))
                                        when ST_Z({point}) is not null then {point}
                                        else project.set_altitude({point})
                                            end """.format(point=self.__connection_table_text_for('geom'),
                                                           z_data=self.__connection_table_text_for('z_ground'))
                else:
                    geomstatement = """ case
                                        when ST_Z({point}) is not null then {point}
                                        else project.set_altitude({point})
                                            end """.format(point=self.__connection_table_text_for('geom'))
            return geomstatement


        if self.json_keys[table]['geom']['typeimport'] == 'MULTIPOINTZ' :
            if self.geomtype == 'ZPOINT' or self.geomtype == 'ZMPOINT' :
                geomstatement = "ST_SetSRID(ST_MakePoint(ST_X({point}),ST_Y({point}),ST_Z({point})),ST_SRID({point}))".format(point = self.__connection_table_text_for('geom'))
            elif self.geomtype == 'MULTIPOINT' :
                # TO DO : Expand to whole multi point ? St_dump ?
                if self.__connection_table_has('z_ground'):
                    if self.__connection_table_text_for('z_ground') :
                        geomstatement = """ST_SetSRID(ST_MakePoint(ST_X(ST_GeometryN({point},1)),ST_Y(ST_GeometryN({point},1)),"{z_data}"),ST_SRID({point}))""".format(point = self.__connection_table_text_for('geom'),z_data=self.__connection_table_text_for('z_ground'))
                    else :
                        geomstatement = "ST_SetSRID(ST_MakePoint(ST_X(ST_GeometryN({point},1)),ST_Y(ST_GeometryN({point},1)),project.altitude(ST_MakePoint(ST_X({point}),ST_Y({point})))),ST_SRID({point}))".format(point = self.__connection_table_text_for('geom'))
            elif self.geomtype == 'POINT' :
                if self.__connection_table_has('z_ground'):
                    if self.__connection_table_text_for('z_ground') :
                        geomstatement = """ST_SetSRID(ST_MakePoint(ST_X({point}),ST_Y({point}),"{z_data}"),ST_SRID({point}))""".format(point = self.__connection_table_text_for('geom'),z_data=self.__connection_table_text_for('z_ground') )
                    else :
                        geomstatement = "ST_SetSRID(ST_MakePoint(ST_X({point}),ST_Y({point}),project.altitude(ST_MakePoint(ST_X({point}),ST_Y({point})))),ST_SRID({point}))".format(point = self.__connection_table_text_for('geom'))
                else :
                    geomstatement = "ST_SetSRID(ST_Force3D(ST_MakePoint(ST_X({point}),ST_Y({point}))),ST_SRID({point}))".format(point = self.__connection_table_text_for('geom'))
        elif self.json_keys[table]['geom']['typeimport'] == 'MULTILINESTRINGZ' :
            geomstatement = "ST_Force3D(St_MakeValid((ST_Dump("+ self.__connection_table_text_for('geom') +")).geom))"
        elif self.json_keys[table]['geom']['typeimport'] == 'MULTIPOLYGONZ' and table in ["catchment", "land_occupation"] :
            geomstatement = "ST_Force2D(St_MakeValid((ST_Dump("+ self.__connection_table_text_for('geom') +")).geom))"
        elif self.json_keys[table]['geom']['typeimport'] == 'MULTIPOLYGONZ' and table in ["station"] :
            geomstatement = "ST_Force3D(St_MakeValid((ST_Dump("+ self.__connection_table_text_for('geom') +")).geom))"
        return geomstatement

    def import_hydra(self):
        schema = self.comboSchema.currentText()
        table = self.comboTable.currentText()
        if self.json_keys[table]['properties']['type']:
            table = "_".join([table, self.json_keys[table]['properties']['type']])
        count_before = self.project.execute('''select count(*) from {}."{}"'''.format(schema.replace(" ","_"),table.replace(" ","_"))).fetchone()[0]
        schema, table = self.import_from_work()
        if self.json_keys[table]['properties']['type']:
            table = "_".join([table, self.json_keys[table]['properties']['type']])
        count_imported = self.project.execute('''select count(*) from {}."{}"'''.format(schema.replace(" ","_"),table.replace(" ","_"))).fetchone()[0]
        count_raw = self.project.execute('''select count(*) from work."{}"'''.format(self.table_source)).fetchone()[0]

        comment_query = f"""SELECT obj_description('work.{self.table_source}'::regclass, 'pg_class');"""
        comment = self.project.execute(comment_query).fetchone()[0]
        if comment:
            update_comment_query = f"""UPDATE {schema}."{table}" SET comment = '{comment}' WHERE comment IS NULL;"""
            self.project.execute(update_comment_query)
            self.project.commit()

        QMessageBox.information(None,tr('Data added in Hydra project'),
        tr("Imported data from schema work into table {}.{}, \n {} rows imported out of {} ( {} invalid geometries)".format(schema, table, count_imported - count_before, count_raw, (count_raw-(count_imported-count_before)))),QMessageBox.Ok)

    def import_from_work(self) :
        table = self.comboTable.currentText()

        insert_node, node_type = False, None

        if table in ['hydrograph_bc_singularity','constant_inflow_bc_singularity'] :
            insert_node, node_type = self.check_node_for_hydrograph()

        return self.run_import_from_work(insert_node, node_type)

    def run_import_from_work(self, insert_node, node_type):
        schema = self.comboSchema.currentText()
        table = self.comboTable.currentText()
        query = self.requete_insert(schema, table)

        if table == 'points_xyz':
            self.import_new_terrain_group()

        if table in ['hydrograph_bc_singularity','constant_inflow_bc_singularity']:
            if insert_node and node_type:
                self.insert_node_for_hydrograph(node_type)
            self.project.execute(query)

        if table in ['coverage_marker', 'constrain']:
            triggerstatement = "select trigger_coverage from {}.metadata;".format(schema)
            bool_state = self.project.execute(triggerstatement).fetchone()[0]
            if bool_state :
                self.project.execute("update {}.metadata set trigger_coverage = False;".format(schema))
            if table == 'coverage_marker':
                self.insert_coverage_marker()

        if table == 'pipe' :
        # prevent update trigger for pipe to fire
            triggerstatement = "select trigger_branch from {}.metadata;".format(schema)
            bool_state = self.project.execute(triggerstatement).fetchone()[0]
            if bool_state :
                self.project.execute("update {}.metadata set trigger_branch = False;".format(schema))

            if self.check_manholes_for_pipes() :
                self.project.execute(query)

        if table == 'reach' :
            self.project.execute(query)
            self.fuse_reach()

        if table not in ['pipe','hydrograph_bc_singularity','constant_inflow_bc_singularity','reach','coverage_marker'] :
            self.project.execute(query)

        # reinit trigger
        if table in ['coverage_marker', 'constrain'] and bool_state :
            self.project.execute("update {}.metadata set trigger_coverage = True;".format(schema))
            self.project.execute("""select {}.coverage_update();""".format(schema))
        if table == 'pipe' and bool_state :
            self.project.execute("update {}.metadata set trigger_branch = True;".format(schema))
            self.project.execute("""select {}.branch_update_fct();""".format(schema))
        self.project.log.notice(tr("Imported data from schema work into table {}.{};".format(schema, table)))

        return(schema, table)

    def save(self):
        BaseDialog.save(self)
        self.close()
