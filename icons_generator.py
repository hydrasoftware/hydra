# coding:UTF-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
creates .png icons for each layer from a given QGIS project

USAGE

    icons_generator [qgisproject path, icons target directory]
"""
import os
import sys
from qgis.core import *
from qgis.gui import *
from qgis.core.contextmanagers import qgisapp
from qgis.PyQt.QtCore import Qt
from hydra.utility.tables_properties import TablesProperties
from hydra.utility.empty_ui_manager import EmptyUIManager

project_name = sys.argv[1]
icon_dir = sys.argv[2]

app = QgsApplication([], True)
app.setPrefixPath(os.path.join(os.environ['PYTHONHOME'], '..', 'qgis-ltr'), True)
app.initQgis()

# load project out of qgis API
project = QgsProject.instance()
project.read(project_name)

def leafs(tree):
    """generator that explores the layertree recursively"""
    for child in tree.children():
        if isinstance(child, QgsLayerTreeGroup):
            for leaf in leafs(child):
                yield leaf
        elif isinstance(child, QgsLayerTreeLayer):
            yield child

root = project.layerTreeRoot()
model = QgsLayerTreeModel(root)

properties = TablesProperties.get_properties()

for l in leafs(root):
    geom_name = ["point", "line", "polygon"][l.layer().geometryType()]
    for table in properties:
        if properties[table]['name'] == l.layerName():
            name = table
    model.layerLegendNodes(l)[0].data(Qt.DecorationRole).save( os.path.join(icon_dir, name+"_"+geom_name+".png"))

app.exitQgis()
