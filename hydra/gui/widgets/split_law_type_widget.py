################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import json
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QApplication, QWidget, QGridLayout
from .graph_widget import GraphWidget
from .array_widget import ArrayWidget
from hydra.gui.base_dialog import tr
import hydra.utility.sql_json as sql_json

class SplitLawTypeWidget(QWidget):
    def __init__(self, project, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "split_law_type_widget.ui"), self)
        self.project = project

        tmplayout = QGridLayout()
        tmplayout.addWidget(self)
        parent.setLayout(tmplayout)

        self.law_type_combo.activated.connect(self.__law_changed)
        self.stackedWidget.setVisible(False)
        self.__law_changed()

        self.zq_array = ArrayWidget(["Discharge (m3/s)", "Elevation (m)"], [10,2], None,
            draw_graph_func=self.__draw_graph, help_func=None, parent=self.widget_zq_tab)
        self.zq_graph = GraphWidget(self.widget_zq_graph, True, True)

    def __draw_graph(self, datas):
        if len(datas)>0:
            self.zq_graph.clear()
            self.zq_graph.add_line(
                [r[0] for r in datas],
                [r[1] for r in datas],
                "r",
                None,
                tr("Q (m3/s)"), tr("Z (m)"))
        else:
            self.zq_graph.canvas.setVisible(False)

    def __law_changed(self):
        if self.law_type_combo.currentIndex()!=-1:
            self.stackedWidget.setVisible(True)
            self.stackedWidget.setCurrentIndex(int(self.law_type_combo.currentText().split(':')[0])-1)

    def __selected_law(self):
        if self.law_type_combo.currentIndex()!=-1:
            return self.project.execute("""select name from hydra.split_law_type where id={}""".format(self.law_type_combo.currentText().split(':')[0])).fetchone()[0]
        else:
            return None

    def set_values(self, law_type=None, law_params_json=None):
        enum_values = self.project.execute("""
            select description, name, id
            from hydra.split_law_type
            order by id""").fetchall()
        for value in enum_values:
            corrected_value = value if value[0] is not None else [value[1],value[1],value[2]]
            self.law_type_combo.addItem(str(corrected_value[2])+': '+corrected_value[0])
            if law_type is not None:
                if corrected_value[1] == law_type:
                    self.law_type_combo.setCurrentIndex(self.law_type_combo.findText(str(corrected_value[2])+': '+corrected_value[0]))
            else:
                self.law_type_combo.setCurrentIndex(-1)
        sql_json.set_fields_to_json(self, 'split_law_type', law_params_json)
        self.__law_changed()

        if law_params_json is not None:
            params_dict = json.loads(law_params_json)
            if "zq" in list(params_dict.keys()):
                if "zq_array"in list(params_dict["zq"].keys()):
                    array = json.loads(law_params_json)["zq"]["zq_array"]
                    if array is not None:
                        self.zq_array.set_table_items(json.loads(law_params_json)["zq"]["zq_array"])

    def get_values(self):
        return self.__selected_law(), sql_json.build_json_from_fields(self, 'split_law_type').replace('{"zq_array": null}', json.dumps({"zq_array":self.zq_array.get_table_items()}))
