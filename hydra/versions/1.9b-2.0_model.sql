/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update $model.metadata set version = '2.0';
;;

/* ********************************************** */
/*            River node Z_INVERT                 */
/* ********************************************** */

create or replace function ${model}.river_node_pk(river_node_id integer)
returns double precision
language plpgsql
as $$$$
    declare
        _res real;
    begin
        select st_linelocatepoint(r.geom, n.geom) * st_length(r.geom) / 1000::double precision + r.pk0_km
            from ${model}.reach as r, ${model}._river_node as rn, ${model}._node as n
            where r.id = rn.reach and  n.id=rn.id and rn.id = river_node_id
            into _res;
        return _res;
    end;
$$$$
;;

create or replace function ${model}.river_node_z_invert(river_node_id integer)
returns real
language plpgsql
as $$$$
    declare
        _res real;
    begin
        with node as (
                select ${model}.river_node_pk(river_node_id) as pk_km, reach from ${model}._river_node where id=river_node_id
            ),
            pts_up as (
                select p.id, ${model}.river_node_pk(n.id) as pk_km, COALESCE(z_invert_down, z_invert_up) as z_invert
                from ${model}._river_node as n, node, ${model}.river_cross_section_profile as p
                where n.reach=node.reach and p.id=n.id and ${model}.river_node_pk(n.id) <= node.pk_km
                ),
            pts_down as (
                select p.id, ${model}.river_node_pk(n.id) as pk_km, COALESCE(z_invert_up, z_invert_down) as z_invert
                from ${model}._river_node as n, node, ${model}.river_cross_section_profile as p
                where n.reach=node.reach and p.id=n.id and node.pk_km <= ${model}.river_node_pk(n.id)
                ),
            up as (
                select z_invert, pk_km, id from pts_up
                where z_invert is not null
                order by pk_km desc limit 1
                ),
            down as (
                select z_invert, pk_km, id from pts_down
                where z_invert is not null
                order by pk_km asc limit 1
                ),
            cote_minmax as (
                select up.z_invert as cote, COALESCE(NULLIF(down.pk_km - node.pk_km, 0), 1) as weight from up, down, node
                union all
                select down.z_invert as cote, COALESCE(NULLIF(node.pk_km - up.pk_km, 0), 1) as weight from up, down, node
            ),
            z_invert as (
                select SUM(cote * weight)/SUM(weight) as z
                from cote_minmax
                having bool_and(cote is not null)
            )
            select case
                when not exists (select 1 from z_invert) then null
                else (select z from z_invert) end
            into _res;
        return _res;
    end;
$$$$
;;

create or replace view ${model}.river_node as
    select
        p.id,
        p.name,
        c.reach,
        c.z_ground,
        c.area,
        ${model}.river_node_pk(p.id) as pk_km,
        p.geom,
        p.configuration::character varying AS configuration,
        p.generated,
        p.validity,
        p.configuration AS configuration_json,
        ${model}.river_node_z_invert(p.id) as z_invert
    from ${model}._river_node c,
        ${model}._node p
    where p.id = c.id
;;
