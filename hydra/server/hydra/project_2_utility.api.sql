/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

create function project.to_obj(multipoly geometry)
returns varchar
language plpython3u
immutable
as
$$$$
    from shapely import wkb
    if multipoly is None:
        return ''
    m = wkb.loads(bytes.fromhex(multipoly))
    res = ""
    node_map = {}
    elem = []
    n = 0
    for p in m:
        elem.append([])
        for c in p.exterior.coords[:-1]:
            sc = "%f %f %f" % (tuple(c))
            if sc not in node_map:
                res += "v {}\n".format(sc)
                n += 1
                node_map[sc] = n
                elem[-1].append(str(n))
            else:
                elem[-1].append(str(node_map[sc]))
    for e in elem:
        res += "f {}\n".format(" ".join(e))
    return res
$$$$
;;

/* ********************************************** */
/*                      Models                    */
/* ********************************************** */

create view project.interlink as
    select id, name, model_up, node_up, model_down, node_down, geom
    from project._interlink
;;

create function project.interlink_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        down_ integer;
    begin
        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            execute 'select id from ' || new.model_up || '._node where ST_DWithin(geom, ST_StartPoint(''' || ST_AsEWKT(new.geom) || '''::geometry), 0.01) and node_type in (''elem_2d'', ''crossroad'', ''storage'')' into up_;
            execute 'select id from ' || new.model_down || '._node where ST_DWithin(geom, ST_EndPoint(''' || ST_AsEWKT(new.geom) || '''::geometry), 0.01) and node_type = ''manhole''' into down_;
            if (up_ is null or down_ is null) then
                raise exception 'link % not touching nodes from model % to model %', ST_AsText(new.geom), new.model_up, new.model_down;
            end if;
        end if;
        if tg_op = 'INSERT' then
            insert into project._interlink(name, model_up, node_up, model_down, node_down, geom)
                values(coalesce(new.name, 'define_later'), new.model_up, up_, new.model_down, down_, new.geom) returning id into id_;
            update project._interlink set name = 'INT_'||id_::varchar where name = 'define_later' and id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update project._interlink set name=new.name, model_up=new.model_up, up=up_, model_down=new.model_down, down=down_, geom=new.geom where id=old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from project._interlink where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger project_interlink_trig
    instead of insert or update or delete on project.interlink
       for each row execute procedure project.interlink_fct()
;;

/* ********************************************** */
/*              Terrain & altitude                */
/* ********************************************** */

-- Source files

create function project.check_source(source varchar)
returns bool
language plpython3u immutable
as
$$$$
    try:
        from osgeo import gdal
    except ImportError:
        import gdal

    src_ds = gdal.Open(source)
    if src_ds is None:
        return False
    else:
        return True
$$$$
;;

-- Altitude

create function project.altitude(x double precision, y double precision)
returns real
language plpython3u immutable
as
$$$$
    from $hydra import altitude
    import plpy
    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select source from project.dem order by priority asc")]
    return altitude(sources, x, y)
$$$$
;;

create function project.altitude(point geometry)
returns real
language plpgsql immutable
as
$$$$
    begin
        return project.altitude(st_x(point)::double precision, st_y(point)::double precision);
    end;
$$$$
;;

create function project.set_altitude(geom geometry)
returns geometry
language plpython3u immutable
as
$$$$
    from $hydra import set_altitude
    import plpy
    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select source from project.dem order by priority asc")]
    return set_altitude(sources, geom, $srid)
$$$$
;;

-- Filling curves

create function project._str_filling_curve(polygon geometry)
returns varchar
language plpython3u immutable
as
$$$$
    from $hydra import filling_curve
    import plpy
    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select source from project.dem order by priority asc")]
    return filling_curve(sources, polygon)
$$$$
;;

create function project.filling_curve(polygon geometry)
returns real[][]
language plpgsql immutable
as
$$$$
    begin
        return project._str_filling_curve(polygon)::real[][];
    end;
$$$$
;;

-- Geometry tools

create function project.crossed_border(line geometry, polygon geometry)
returns geometry
language plpython3u immutable
as
$$$$
    from $hydra import crossed_border
    return crossed_border(line, polygon)
$$$$
;;

create function project.crest_line(line geometry)
returns geometry
language plpython3u immutable
as
$$$$
    from $hydra import crest_line
    import plpy
    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select source from project.dem order by priority asc")]
    return crest_line(sources, line, $srid)
$$$$
;;

create function project.discretize_line(geom geometry, elem_length float)
returns geometry
language plpython3u
as $$$$
    from $hydra import discretize_line
    return discretize_line(geom, elem_length)
$$$$
;;

/* ********************************************** */
/*              Land use                          */
/* ********************************************** */

create function project.land_type_before_insert_fct()
returns trigger
language plpgsql
as $$$$
    begin
        new.c_runoff := 0.7 * new.c_imp;
        return new;
    end;
$$$$
;;

create trigger project_land_type_before_trig
    before insert on project.land_type
    for each row when (new.c_runoff is null and new.c_imp is not null)
    execute procedure project.land_type_before_insert_fct();
;;

create view project.land_occupation as
    select o.id, o.geom, t.land_type, t.c_imp, t.netflow_type
    from project._land_occupation as o
    left join project.land_type as t on t.id=o.land_type_id
;;

create function project.land_occupation_after_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        land_type_id_ integer;
    begin
        if tg_op = 'INSERT' then
            -- Creates polygon with geom
            insert into project._land_occupation(geom) values(new.geom) returning id into id_;
            -- Check if new land_type_occupation exists, creates if needed
            if nullif(new.land_type, '') is not null then
                select id from project.land_type where land_type=new.land_type into land_type_id_;
                if land_type_id_ is null then
                    insert into project.land_type(land_type, c_imp, netflow_type) values (new.land_type, coalesce(new.c_imp, 0.5), coalesce(new.netflow_type, 'constant_runoff')) returning id into land_type_id_;
                end if;
                -- Update polygon with land type foreign key
                update project._land_occupation set land_type_id=land_type_id_ where id=id_;
            end if;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Update polygon geometry
            update project._land_occupation set geom=new.geom where id=old.id;
            -- If updated land_type is null or ''
            if nullif(new.land_type, '') is null then
                -- Update polygon land type foreign key to null
                update project._land_occupation set land_type_id=null where id=old.id;
            -- Check if updated land_type exists, creates if needed
            elsif nullif(new.land_type, '') is not null then
                select id from project.land_type where land_type=new.land_type into land_type_id_;
                if land_type_id_ is null then
                    insert into project.land_type(land_type, c_imp, netflow_type) values (new.land_type, coalesce(new.c_imp, 0.5), coalesce(new.netflow_type, 'constant_runoff')) returning id into land_type_id_;
                end if;
                -- Update polygon land type foreign key
                update project._land_occupation set land_type_id=land_type_id_ where id=old.id;
            end if;
            return new;
        elsif tg_op = 'DELETE' then
            delete from project._land_occupation where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger project_land_occupation_after_trig
    instead of insert or update or delete on project.land_occupation
    for each row execute procedure project.land_occupation_after_fct()
;;

/* ********************************************** */
/*              Transcription                     */
/* ********************************************** */

create function hydra_to_json(tabl varchar, geom varchar, variadic cols varchar[])
returns varchar
language plpgsql
as $$$$
    declare
        res varchar;
        ct integer;
    begin
        execute 'select count(1) from '||tabl into ct;

        if ct>0 and geom is null then
            execute 'select row_to_json(everything)
            from
                (select ''FeatureCollection'' as type,
                        array_to_json(array_agg(feat)) as features
                 from (select ''Feature'' as type,
                           null As geometry,
                           row_to_json((select l from (select '||(select array_to_string(cols, ','))||') as l)) as properties
                       from '||tabl||'
                      ) as feat
                 ) as everything;' into res;
        elsif ct>0 then
            execute 'select row_to_json(everything)
            from
                (select ''FeatureCollection'' as type,
                        (select row_to_json(l) from (select ''name'' as type,
                            (select row_to_json(m)
                                from (select ''urn:ogc:def:crs:EPSG::''||(select ST_SRID('||geom||') from '||tabl ||' limit 1)::varchar as name) as m)
                            as properties) as l)
                        as crs,
                        array_to_json(array_agg(feat)) as features
                 from (select ''Feature'' as type,
                           ST_AsGeoJSON('||geom||')::json As geometry,
                           row_to_json((select l from (select '||(select array_to_string(cols, ','))||') as l)) as properties
                       from '||tabl||'
                      ) as feat
                 ) as everything;' into res;
        else
            select '' into res;
        end if;
        return res;
    end;
$$$$
;;

create function wkb_loads(geom varchar)
returns varchar
language plpython3u
as
$$$$
    from $hydra import wkb_loads
    return wkb_loads(geom)
$$$$
;;

create function project.topo(line_ geometry)
returns geometry
language plpython3u
immutable
as
$$$$
    from $hydra import line_elevation, wkb_loads
    from shapely.geometry import LineString
    if line_ is None:
        return None
    sources = [res['source'].replace('\\','\\\\') for res in plpy.execute("select source from project.dem order by priority desc")]
    assert(len(sources) == 1)
    return line_elevation(line_, sources[0])
$$$$
;;


create function project.orient_transect(reach geometry, transect geometry)
returns geometry
language plpgsql immutable as
$$$$
declare
    a double precision;
    u geometry;
begin
    a := st_linelocatepoint(reach, st_geometryn(st_multi(st_intersection(reach, transect)), 1));
    u := st_linesubstring(reach, a, a+.1/st_length(reach));
    if (st_x(st_endpoint(u)) - st_x(st_startpoint(u)))*(st_y(st_endpoint(transect)) - st_y(st_startpoint(transect)))
        - (st_y(st_endpoint(u)) - st_y(st_startpoint(u)))*(st_x(st_endpoint(transect)) - st_x(st_startpoint(transect))) < 0 then
        return transect;
    else
        return st_reverse(transect);
    end if;
end
$$$$
;;

create function project.fake_transect(reach geometry, node geometry, length_ double precision)
returns geometry
language plpgsql immutable as
$$$$
declare
    a double precision;
    l double precision;
    o geometry;
    r geometry;
begin
    r := st_force2d(reach);
    l := st_length(r);
    a := st_linelocatepoint(r, node);
    if (a*l + .1)/l > 1 then
        o := st_lineinterpolatepoint(r, (a*l - .1)/l);
        return st_rotate(st_scale(st_makeline(
                        o,
                        st_translate(node, st_x(node) - st_x(o), st_y(node) - st_y(o))
                        ), ('POINT('||5*length_||' '||5*length_||')')::geometry, node), -pi()/2, node);
    elsif (a*l - .1)/l < 0 then
        o := st_lineinterpolatepoint(r, (a*l + .1)/l);
        return st_rotate(st_scale(st_makeline(
                        st_translate(node, st_x(node) - st_x(o), st_y(node) - st_y(o)),
                        o
                        ), ('POINT('||5*length_||' '||5*length_||')')::geometry, node), -pi()/2, node);
    else
        return st_rotate(st_scale(st_makeline(
                        st_lineinterpolatepoint(r, (a*l - .1)/l),
                        st_lineinterpolatepoint(r, (a*l + .1)/l)
                        ), ('POINT('||5*length_||' '||5*length_||')')::geometry, node), -pi()/2, node);
    end if;
end
$$$$
;;

create function project.interpolate_z(l double precision, a double precision[])
returns real
language plpgsql immutable as
$$$$
declare
    m   double precision[];
    lp double precision;
    zp double precision;
begin
    if l <= a[1][1] then
        return a[1][2];
    end if;
    foreach m slice 1 in array a
    loop
        if m[1] >= l then
            return zp + (m[2]-zp)*(l-lp)/(m[1]-lp);
        end if;
        lp := m[1];
        zp := m[2];
    end loop;
    return a[array_length(a,1)][2];
end
$$$$
;;

drop function if exists project.regulated()
;;

create function project.regulated()
returns table(id integer,
    scenario text,
    file  text,
    iline integer,
    model text,
    type text,
    subtype text,
    name text,
    geom geometry)
language plpython3u
as
$$$$
    import plpy
    from $hydra import regulated_items
    return regulated_items(plpy)
$$$$
;;

create function project.socose_tc_mn(runoff_type hydra_runoff_type, area_ha real, rl real, slope real, c_imp real, rfu_mm real)
returns float
language plpgsql immutable as
$$$$
begin
    if runoff_type = 'Passini' then
        return 0.14 * (area_ha * rl)^(1./3)/sqrt(slope);
    elsif runoff_type = 'Giandotti' then
        return 60 * (0.4*sqrt(area_ha) + 0.0015*rl)/(0.8*sqrt(slope*rl));
    elsif runoff_type = 'Turraza' then
        return 6.51*sqrt(area_ha);
    elsif runoff_type = 'Ventura' then
        return 0.762 * sqrt(area_ha/slope);
    elsif runoff_type = 'Kirpich' then
        return 0.0195 * (rl/sqrt(slope))^(0.77);
    elsif runoff_type = 'Cemagref' then
        return exp(0.375 * ln(area_ha/100) + 3.729);
    elsif runoff_type = 'Mockus' then
        return 60 * (rl^(0.8) * (1000/(1000/(10 + rfu_mm/25.4)) -9)^(1.67)) / (2083 * sqrt(100 * slope));
    else
        raise 'function project.socose_tc_mn called with %',runoff_type;
    end if;
end;
$$$$
;;


create function project.define_k_mn(runoff_type hydra_runoff_type, area_ha real, rl real, slope real, c_imp real, c_r real)
returns float
language plpgsql immutable as
$$$$
begin
    if runoff_type = 'Desbordes 1 Cr' then
        return 5.07 * area_ha^(0.18) * (slope*100)^(-0.36) * (1 + c_r)^(-1.9) * rl^(0.15) * 30^(0.21) * 10^(-0.07);
    elsif runoff_type = 'Desbordes 1 Cimp' then
        return 5.07 * area_ha^(0.18) * (slope*100)^(-0.36) * (1 + c_imp)^(-1.9) * rl^(0.15) * 30^(0.21) * 10^(-0.07);
    elsif runoff_type = 'Desbordes 2 Cimp' then
        return 5.3 * area_ha^(0.3) * (slope*100)^(-0.38) * c_imp^(-0.45);
    elsif runoff_type = 'Krajewski' then
        return 2.436 * area_ha^(0.455) * c_imp^(-0.57) * (slope*100)^(-0.127);
    else
        raise 'function project.define_k_mn called with %',runoff_type;
    end if;
end;
$$$$
;;

create function project.area_circular(d real, h_sable real)
returns float
language plpgsql immutable as
$$$$
begin
    return pi()*d^2/4 - (acos(1 - 2*h_sable/d) * d^2/4 + sqrt(d^2/4 - (d/2 - h_sable)^2));
end;
$$$$
;;

create function project.area_ovoid(h real, d_bottom real, d_top real, h_sable real)
returns float
language plpgsql immutable as
$$$$
begin
    if h_sable < d_bottom/2. then
       return pi()*d_top^2/8.
       + (d_top + d_bottom)/2. * (h - (d_top + d_bottom)/2.)
       + pi()*d_bottom^2/8. - (acos(1. - 2.*h_sable/d_bottom) * d_bottom^2/4. + sqrt(d_bottom^2/4. - (d_bottom/2. - h_sable)^2));
    elsif h_sable < h - d_top/2. then
       return pi()*d_top^2/8.
       + (d_bottom + (d_top - d_bottom)*((h_sable - d_bottom/2.)/(h - (d_top + d_bottom)/2.))) * (h - h_sable - d_top/2 - d_top/2.);
    else
       return pi()*d_top^2/8. - (acos(1. - 2.*(h_sable - h + d_top/2.)/d_top) * d_top^2/4. + sqrt(d_top^2/4. - (d_top/2. - (h_sable - h + d_top/2.))^2));
    end if;
end;
$$$$
;;

create function project.area_pipe(zb real[], h_sable real)
returns float
language plpgsql immutable as
$$$$
begin
    return (select (sum(case
    when zb[i][1] < h_sable then
        0
    when zb[i-1][1] >= h_sable then
        (zb[i][1] - zb[i-1][1])*(zb[i][2] + zb[i-1][2])/2.
    else
        (zb[i][1] - h_sable)*(zb[i][2] + zb[i-1][2] + (zb[i][2] - zb[i-1][2])*(h_sable - zb[i-1][1])/(zb[i][1] - zb[i-1][1]))/2.
    end))
    from generate_subscripts(zb, 1) i
    where i > 1);
end;
$$$$
;;


create function project.area_channel(zb real[], h_sable real)
returns float
language sql immutable as
$$$$
    select project.area_pipe($$1, $$2);
$$$$
;;

create function project.tesselate(
    polygon geometry,
    constrains geometry default null,
    discontinuities geometry default null,
    points geometry default null,
    has_quads boolean default false)
returns table (vertex geometry(MULTIPOINT, $srid), face integer[])
language plpython3u immutable as
$$$$
    from $hydra import tesselate
    return tesselate(polygon, constrains, discontinuities, points, has_quads);
$$$$
;;


create or replace function project.line_trim_extend(line_ geometry, start_ geometry, end_ geometry)
returns geometry
language plpgsql immutable as
$$$$
declare
    sa double precision;
    ea double precision;
    s geometry;
begin
    sa := st_linelocatepoint(line_, start_);
    ea := st_linelocatepoint(line_, end_);
    s := st_linesubstring(line_, sa, ea);
    return (
        select st_makeline(ARRAY[start_] || array_agg(p.geom)  || ARRAY[end_])
        from st_dumppoints(s) p
        where st_linelocatepoint(s, p.geom) < 1 and st_linelocatepoint(s, p.geom) > 0
        );
end;
$$$$
;;

create function project.line_offset(line_ geometry, start_ geometry, end_ geometry, domain_ geometry default null, dist_ double precision default 1.)
returns geometry
language plpgsql immutable SET SEARCH_PATH to project, public
as
$$$$
declare
    p geometry;
    pp geometry;
    nx double precision;
    ny double precision;
    pnx double precision;
    pny double precision;
    l double precision;
    total_length double precision;
    current_length double precision;
    start_distance double precision;
    end_distance double precision;
    pnl double precision[]; -- points normals and location, normal length is corrected for angle
    sn double precision;
    g geometry;
    de geometry;
    border geometry[];
    cov geometry;
    side_sign double precision;
    ssign double precision;
begin
    line_ := st_force2d(line_);
    start_ := st_force2d(start_);
    end_ := st_force2d(end_);
    domain_ := st_force2d(domain_);

    current_length := 0;
    total_length := st_length(line_);

    cov := st_buffer(domain_, -dist_);

    for p in select (st_dumppoints(st_removerepeatedpoints(line_, 0.1))).geom loop
        if pp is null then
            pp = p;
            continue;
        end if;
        l := st_distance(p, pp);

        nx := -(st_y(p) - st_y(pp))/l;
        ny := (st_x(p) - st_x(pp))/l;
        if pnx is not null then
            sn := sqrt((nx+pnx)^2 + (ny+pny)^2)*cos(0.5*acos(greatest(least(nx*pnx + ny*pny, 1.), -1.)));
            pnl := pnl || ARRAY[ARRAY[st_x(pp), st_y(pp), (nx+pnx)/sn, (ny+pny)/sn, current_length/total_length]];

        else
            pnl := ARRAY[ARRAY[st_x(pp), st_y(pp), nx, ny, 0]];
            start_distance := (st_x(start_) - st_x(pp))*nx + (st_y(start_) - st_y(pp))*ny;
        end if;

        pp := p;
        pnx := nx;
        pny := ny;
        current_length := current_length + l;
    end loop;
    pnl := pnl || ARRAY[ARRAY[st_x(pp), st_y(pp), nx, ny, current_length/total_length]];
    end_distance := (st_x(end_) - st_x(pp))*nx + (st_y(end_) - st_y(pp))*ny;

    border := (
        select array_agg(d.geom)
        from
            (select
            project.line_trim_extend(
                st_makeline(
                    --project.force_inside(
                        st_setsrid(
                            st_makepoint(pnl[i][1] + (start_distance*(1-pnl[i][5]) + end_distance*pnl[i][5])*pnl[i][3]
                                        ,pnl[i][2] + (start_distance*(1-pnl[i][5]) + end_distance*pnl[i][5])*pnl[i][4])
                        , $srid)
                    --, cov)
                )
                , start_, end_) as geom
            from generate_series(1, array_length(pnl, 1)) i
            ) t
        cross join lateral st_dump(st_unaryunion(t.geom)) d
        where not st_isclosed(d.geom)
        );


    side_sign := case when st_ispolygonccw(st_makepolygon(st_makeline(ARRAY[start_, st_pointn(line_, 1), st_pointn(line_, 2), start_]))) then -1 else 1 end;

    g := st_intersection(
        cov,
        st_polygonize(st_node(st_collect(
            ARRAY[
            st_reverse(line_),
            st_makeline(st_startpoint(line_), start_),
            st_makeline(border),
            st_makeline(end_, st_endpoint(line_))
            ])))
        );

    if side_sign = 1 then
        g := st_exteriorring(ST_ForcePolygonCCW(g));
    else
        g := st_exteriorring(ST_ForcePolygonCW(g));
    end if;

    g := (
        with pts as (
            select i, st_pointn(g, i)
            from generate_series(1, st_numpoints(g)) i
            order by st_distance(st_pointn(g, i), start_) asc limit 1
        ),
        pte as (
            select i, st_pointn(g, i)
            from generate_series(1, st_numpoints(g)) i
            order by st_distance(st_pointn(g, i), end_) asc limit 1
        )
        select st_makeline(ARRAY[start_] || array_agg(st_pointn(g, (j+s.i)%st_numpoints(g))) || ARRAY[end_])
        from pts s
        cross join pte e
        cross join lateral generate_series(0, case when s.i < e.i then e.i-s.i else e.i+st_numpoints(g)-s.i end) j
        );

    g := st_simplify(st_makeline(ARRAY[start_, g, end_]), dist_);

    return g;
end;
$$$$
;;



