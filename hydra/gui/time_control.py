from builtins import str
# coding = UTF-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from datetime import datetime
from qgis.PyQt import uic
from qgis.PyQt.QtCore import pyqtSignal, QTimer, QSettings
from qgis.PyQt.QtWidgets import QWidget
from qgis.PyQt.QtGui import QIcon
from hydra.utility.settings_properties import SettingsProperties

_plugin_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
_icons_dir = os.path.join(_plugin_dir, "ressources", "images")

class TimeControl(QWidget):
    timeChanged = pyqtSignal(int)

    def __init__(self, blocker=None, parent=None):
        """the blocker prevents time update, if initialized to True, it will wait
        for the nextFrame function to be called"""
        QWidget.__init__(self, parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), 'time_control.ui'), self)
        self.timer = QTimer(None)
        self.timer.timeout.connect(self.__animate)
        self.timeSlider.valueChanged.connect(self.timeChanged.emit)
        self.playButton.toggled.connect(self.play)
        self.__blocker = blocker

    def timestep(self):
        return self.timeSlider.value()

    def nextFrame(self):
        self.__blocker = False

    def setTimes(self, times):
        self.dateComboBox.clear()
        self.dateComboBox.addItems(\
            [str(time) for time in times])
        self.timeSlider.setMaximum(len(times)-1)

    def getTime(self, idx):
        return self.dateComboBox.itemText(idx) if idx else 0

    def __animate(self):
        if self.__blocker is None or not self.__blocker:
            self.__blocker = True if self.__blocker is not None else None
            self.timeSlider.setValue(
                    (self.timeSlider.value() + self.stride.value())
                    % (self.timeSlider.maximum() + self.stride.value()) )

    def play(self, checked):
        timestep = int(float(self.timestepEdit.text()) * 1000)
        if checked and not self.timer.isActive():
            self.timer.start(timestep)
            self.playButton.setIcon(QIcon(os.path.join(_icons_dir, "pause.svg")))
        elif self.timer.isActive():
            self.timer.stop()
            self.playButton.setIcon(QIcon(os.path.join(_icons_dir, "play.svg")))

    def suspendPlayFunction(self, flag=None):
        self.playButton.setChecked(False)
        self.playButton.toggled.disconnect(self.play)

    def restorePlayFunction(self, flag=None):
        self.playButton.toggled.connect(self.play)

    def value(self):
        return self.timeSlider.value()

    def setValue(self, idx):
        self.timeSlider.setValue(idx)

    def maximum(self):
        return self.timeSlider.maximum()

    def text(self):
        return self.dateComboBox.currentText()

    def set_active(self, active):
        if active:
            self.playButton.setEnabled(True)
            self.timeSlider.setEnabled(True)
            self.dateComboBox.setEnabled(True)
        elif not active:
            self.play(False)
            self.playButton.setChecked(False)
            self.playButton.setEnabled(False)
            self.timeSlider.setValue(0)
            self.timeSlider.setEnabled(False)
            self.dateComboBox.clear()
            self.dateComboBox.setEnabled(False)
