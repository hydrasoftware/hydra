# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import json
import glob
from math import sqrt
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QHBoxLayout, QDockWidget, QInputDialog, QMessageBox
from qgis.PyQt.QtGui import QColor
from qgis.PyQt.QtCore import QCoreApplication, QObject
from hydra.utility.map_point_tool import create_point
from hydra.gui.long_profil_interface import LongProfilInterface
from hydra.gui.select_menu import SelectTool
from qgis.gui import QgsRubberBand
from qgis.core import QgsGeometry, QgsWkbTypes, QgsPoint, QgsPointXY

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

_plugin_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
_layer_icons_dir = os.path.join(_plugin_dir, "ressources", "layer_icons")
_icons_dir = os.path.join(_plugin_dir, "ressources", "images")

_list_checkbox_ = {
    "links":["Links","Show links"],
    "singularities":["Singularities","Show singularities"],
    "cross_sections":["Cross sections", "Show cross sections"],
    "r_bank":["Reach right bank", "Show right bank"],
    "l_bank":["Reach left bank", "Show left bank"],
    "water_line":["Water level Z", "Show water level"],
    "water_flow":["Water flow Q", "Show water's debit"],
    "water_minmax":["Water min max Z", "Show water min/max z"],
    "water_speed":["Water speed V", "Show water speed"],
    "energy":["Energy E", "Show equivalent Z energy"],
    "critical":["Critical Zc", "Show critical Z"],
    "solid_flow":["Solid flow Qs", "Show solid flow Qs"],
    "h_sable":["Silting hight Hs", "Show silting hight"],
    "quality":["Quality", "Show quality_param"]
    }


class LongProfilBranch(QObject):

    def __init__(self,id,name,table,schema):
        self.id = id
        self.name = name
        self.table = table
        self.schema = schema

class Highlighter(QObject):

    def __init__(self, map_canvas, line_wkt, pk0=0, parent=None):
        QObject.__init__(self, parent)

        self.__map_canvas = map_canvas
        self.__rb = QgsRubberBand(map_canvas, QgsWkbTypes.LineGeometry)
        self.__line = QgsGeometry.fromWkt(line_wkt)
        self.__rb.addGeometry(self.__line, None)
        self.__rb.setWidth(5)
        color = QColor(255, 0, 0, 128)
        self.__rb.setColor(color)
        self.__pk0 = pk0

    def __del__(self):
        self.__rb.reset()
        self.__map_canvas.scene().removeItem(self.__rb)

    def show(self):
        self.__rb.show()

    def hide(self):
        self.__rb.hide()

    def set_extent_km(self, pk_min, pk_max):
        ''' function that handle the position of the red rubber band on the Qgis map canvas'''
        # section visibility bounds
        self.__rb.reset()
        km_to_m = 1000
        pk0 = self.__pk0
        start = min(max(0, (pk_min - pk0)*km_to_m), self.__line.length())
        end = max(0, min((pk_max - pk0)*km_to_m, self.__line.length()))

        vertices = [QgsPoint(self.__line.interpolate(start).asPoint())]
        vertex_count = len(self.__line.asPolyline())
        distance = 0
        for i in range(1, vertex_count):
            vertex_i = self.__line.vertexAt(i)
            distance += sqrt(self.__line.sqrDistToVertexAt(QgsPointXY(vertex_i), i-1))

            if distance <= start:
                pass
            elif distance < end:
                vertices += [vertex_i]
            else:
                break

        vertices += [QgsPoint(self.__line.interpolate(end).asPoint())]

        self.__rb.addGeometry(QgsGeometry.fromPolyline(vertices), None)

class LongProfilDock(QDockWidget):

    def __init__(self, title, current_project, iface, time_control_panneau):
        QDockWidget.__init__(self, title)

        self.__isreloading = False

        self.__currentproject = current_project
        self.__log_manager = current_project.log
        self.__iface = iface
        self.__currentDir = os.path.abspath(os.path.dirname(__file__))
        uic.loadUi(os.path.join(self.__currentDir,"long_profil_dock.ui"),self)
        self.__time_control = time_control_panneau
        layout_time = QHBoxLayout()
        layout_time.addWidget(self.__time_control)
        self.time_control_box.setLayout(layout_time)
        self.__time_control.set_active(False)
        self.__panneauwindow = None
        self.results_box.setEnabled(False)
        self.branchs=[]
        self.list_scenarios=[]
        self.highlighters = []

        uic.loadUi(os.path.join(self.__currentDir, "widgets", "branch_manager.ui"), self.branch_manager_widget)
        uic.loadUi(os.path.join(self.__currentDir, "widgets", "scenario_comparator_manager.ui"), self.scenario_comparator_widget)

        self.__connect()

        self.checkbox_refresh_scn_changed()

    def __getattr__(self, att):
        if att=='display_r_bank':
            return self.chk_r_bank.isChecked()
        elif att=='display_l_bank':
            return self.chk_l_bank.isChecked()
        elif att=='display_singularities':
            return self.chk_singularities.isChecked()
        elif att=='display_links':
            return self.chk_links.isChecked()
        elif att=='display_cross_sections':
            return self.chk_cross_sections.isChecked()
        elif att=='display_water_line':
            return self.chk_water_line.isChecked()
        elif att=='display_water_flow':
            return self.chk_water_flow.isChecked()
        elif att=='display_water_minmax':
            return self.chk_water_minmax.isChecked()
        elif att=='display_water_speed':
            return self.chk_water_speed.isChecked()
        elif att=='display_energy':
            return self.chk_energy.isChecked()
        elif att=='display_critical':
            return self.chk_critical.isChecked()
        elif att=='display_solid_flow':
            return self.chk_solid_flow.isChecked()
        elif att=='display_h_sable':
            return self.chk_h_sable.isChecked()
        elif att=='display_quality':
            return self.chk_quality.isChecked()
        elif att=='display_min_water':
            return self.btn_display_min.isChecked()
        elif att=='display_max_water':
            return self.btn_display_max.isChecked()
        else:
            raise AttributeError

    def __connect(self) :
        #widgets
        self.branch_manager_widget.btnAdd.clicked.connect(self.add_branch)
        self.branch_manager_widget.btnDel.clicked.connect(self.delete_branch)
        self.branch_manager_widget.btnUp.clicked.connect(self.move_up_selected)
        self.branch_manager_widget.btnDown.clicked.connect(self.move_down_selected)

        self.scenario_comparator_widget.btnAdd.clicked.connect(self.add_scenario_comparator)
        self.scenario_comparator_widget.btnDel.clicked.connect(self.delete_scenario_comparator)

        #premier layout : toolbox
        self.btn_copy_data.clicked.connect(self.copy_data)
        self.btn_copy_image.clicked.connect(self.copy_image)
        self.btn_export_settings.clicked.connect(self.export_setting)
        self.btn_import_settings.clicked.connect(self.import_setting)


        #2ème layout : timebox
        self.btn_display_max.clicked.connect(self.max_pressed)
        self.btn_display_min.clicked.connect(self.min_pressed)

        #connect checkboxes
        for key in list(_list_checkbox_.keys()) :
            getattr(self,'chk_'+key).stateChanged.connect(self.checkbox_refresh)

        self.btn_display_graph.clicked.connect(self.display_graph)

    def display_graph(self):
        ''' affiche le graph '''
        if self.__panneauwindow is None:
            self.__panneauwindow = LongProfilInterface(self.__iface,self.__currentproject,self.__time_control,parent = self)
        if self.__panneauwindow.isVisible() :
            self.__panneauwindow.activateWindow()
        else:
            self.__panneauwindow.display()
        if self.list_scenarios and not self.display_min_water and not self.display_max_water:
            self.__time_control.set_active(True)

    def delete_branch(self):
        ''' function to remove a branch in the manager'''
        if self.branch_manager_widget.list_branch_widget.count()!=0:
            branch_selected_rows = [row.row() for row in self.branch_manager_widget.list_branch_widget.selectedIndexes()]
            self.branchs = [x for ind, x in enumerate(self.branchs) if ind not in branch_selected_rows]
            self.reload_branchmanager()
            if self.__panneauwindow:
                self.__panneauwindow.reload()

    def move_up_selected(self) :
        ''' function used to find index of selected object in branch_manger and call move up for them'''
        for index in sorted([row.row() for row in self.branch_manager_widget.list_branch_widget.selectedIndexes()]) :
            self.move_up(index)
        self.reload_branchmanager()
        if self.__panneauwindow:
            self.__panneauwindow.reload()

    def move_up(self, index):
        ''' function to move up a branch in the manager '''
        if not self.__isreloading:
            self.__isreloading = True
            if self.branch_manager_widget.list_branch_widget.count()!=0:
                if index>0:
                    self.branchs.insert(index-1,self.branchs.pop(index))
            self.__isreloading = False

    def move_down_selected(self) :
        ''' function used to find index of selected object in branch_manger and call move down for them'''
        for index in sorted([row.row() for row in self.branch_manager_widget.list_branch_widget.selectedIndexes()],reverse=True) :
            self.move_down(index)
        self.reload_branchmanager()
        if self.__panneauwindow:
            self.__panneauwindow.reload()

    def move_down(self, index):
        ''' function to move down a branch in the manager '''
        if not self.__isreloading:
            self.__isreloading = True
            if self.branch_manager_widget.list_branch_widget.count()!=0:
                if index != (self.branch_manager_widget.list_branch_widget.count()-1) and index != -1:
                    self.branchs.insert(index+1,self.branchs.pop(index))
            self.__isreloading = False

    def reload_branchmanager(self):
        ''' reload the manager and set the background list uptodate'''
        self.branch_manager_widget.list_branch_widget.clear()
        self.highlighters = []
        for branch in self.branchs:
            self.branch_manager_widget.list_branch_widget.addItem(branch.name)

            line_wkt, = self.__currentproject .execute("""
                    select ST_AsText(geom) from {model}.{table}
                    where id={id}
                    """.format(
                    model=branch.schema,
                    table=branch.table,
                    id=branch.id)).fetchone()

            self.highlighters.append(Highlighter(self.__iface.mapCanvas(), line_wkt, 0))

        for highlighter in self.highlighters:
            highlighter.show()

    def reload_scn_comparator(self):
        ''' reload the scenario comparator '''
        self.scenario_comparator_widget.list_scenarios.clear()
        for scn in self.list_scenarios:
            self.scenario_comparator_widget.list_scenarios.addItem(scn[1])

    def add_branch(self):
        ''' add a branch into the manager'''
        #start = time.time()
        #print(start,'start')
        self.__idx = 0
        point = create_point(self.__iface.mapCanvas(), self.__log_manager)
        if point:
            #print("select_tool",time.time()-start)
            size = self.__iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel()*10
            model = self.__currentproject.get_current_model().name

            self.__currentproject.execute("select {}.branch_update_fct()".format(model))
            self.__currentproject.commit()

            selected_item = SelectTool(self.__currentproject, point, size, subset=[(model, 'reach'), (model, 'branch')])
            if selected_item.name:
                selected_branch = LongProfilBranch(selected_item.id, selected_item.name, selected_item.table, selected_item.schema)
                #print("reload_labels",time.time()-start)

                self.branchs.append(selected_branch)
                #print("branch append",time.time()-start)

                self.branch_manager_widget.list_branch_widget.addItem(selected_branch.name)

                self.reload_branchmanager()

                if self.__panneauwindow :
                    self.__panneauwindow.reload()


    def copy_image (self):
        if self.__panneauwindow is not None:
            self.__panneauwindow.copy_image()

    def copy_data (self):
        if self.__panneauwindow is not None:
            self.__panneauwindow.copy_data()

    def import_setting(self):
        setting_folder = os.path.join(self.__currentproject.directory,"graphs")
        if not os.path.isdir(setting_folder):
            os.mkdir(setting_folder)

        profil_files = glob.glob(setting_folder+"/*.hpf")
        filenames = [os.path.basename(n).replace('.hpf','') for n in profil_files]
        file=None
        if len(filenames)==1:
            file=filenames[0]
        elif len(filenames)>1:
            f, ok = QInputDialog.getItem(None, "Setting selection", "Select the setting you want to display.", filenames, 0, False)
            if ok and f:
                file=f
        if file:
            self.__load_setting_file(profil_files[filenames.index(file)])

    def __load_setting_file(self,fileName):
        if self.__panneauwindow is not None :
            self.__panneauwindow.hide()

        with open(fileName) as fp:
            data = json.load(fp)
            self.__reset()
            for key in list(_list_checkbox_.keys()) :
                getattr(self,'chk_'+key).setChecked(data['checkboxes'][key])
            for branch in data["branchs"]:
                self.branchs.append(LongProfilBranch(branch['id'],branch['name'],branch['table'],branch['schema']))
            for scn in data["scn_comparator"]:
                self.list_scenarios.append(scn)
            if data["time"] == "max" :
                self.btn_display_max.setChecked(True)
            elif data["time"] == "min" :
                self.btn_display_min.setChecked(True)
            self.__time_control.set_active(False)

        self.reload_branchmanager()
        self.reload_scn_comparator()
        self.__panneauwindow.reload(override=True)

    def export_setting(self):
        setting_folder = os.path.join(self.__currentproject.directory, "graphs")

        if not os.path.isdir(setting_folder):
            os.mkdir(setting_folder)
        name_setting, ok = QInputDialog.getText(None, "setting creator", "Give a name to the setting file.")

        if ok and name_setting :
            fileName = os.path.join(setting_folder, str(name_setting) +".hpf")

            if os.path.isfile(fileName):
                reply = QMessageBox.question(self, tr('long profil'), str(tr("The file {f} already exists,\ndo you want overwrite?")).format(f=fileName), QMessageBox.Yes, QMessageBox.No)
                if reply == QMessageBox.No:
                    return

            time = 'notime'
            if self.display_max_water :
                time = 'max'
            elif self.display_min_water :
                time = 'min'

            data = dict()
            data["branchs"]=[{"id":branch.id,"name":branch.name,"table":branch.table,"schema":branch.schema} for branch in self.branchs]
            data["scn_comparator"]= [scn for scn in self.list_scenarios]
            data["checkboxes"] =  {key: getattr(self,'display_'+key) for key in list(_list_checkbox_.keys())}
            data["time"] = time

            with open(fileName, 'w') as fp:
                json.dump(data, fp, indent=4)

            QMessageBox.information(self, tr('Setting saved'),
            str(tr('Setting successfully saved to:\n {f}')).format(f=fileName), QMessageBox.Ok)

    def max_pressed(self):
        ''' handle the action when the max button is pressed '''
        if self.__panneauwindow is not None :
            self.__time_control.set_active(True)

        self.btn_display_min.setChecked(False)
        self.chk_water_minmax.setChecked(not self.btn_display_max.isChecked())
        self.chk_water_minmax.setEnabled(not self.btn_display_max.isChecked())
        self.__time_control.set_active(not self.btn_display_max.isChecked())

        if self.__panneauwindow is not None:
            self.__panneauwindow.reload()

    def min_pressed(self):
        ''' handle the action when the min button is pressed '''
        if self.__panneauwindow is not None :
            self.__time_control.set_active(True)

        self.btn_display_max.setChecked(False)
        self.chk_water_minmax.setChecked(not self.btn_display_min.isChecked())
        self.chk_water_minmax.setEnabled(not self.btn_display_min.isChecked())
        self.__time_control.set_active(not self.btn_display_min.isChecked())

        if self.__panneauwindow is not None:
            self.__panneauwindow.reload()

    def add_current_scn(self):
        if self.__currentproject.get_current_scenario() :
            current_scn = self.__currentproject.get_current_scenario()
            if self.__currentproject.scn_has_run_from_name(current_scn[1]) and len(self.list_scenarios)<2 :
                self.scenario_comparator_widget.list_scenarios.clear()
                self.list_scenarios = []
                self.scenario_comparator_widget.list_scenarios.addItem(current_scn[1])
                self.list_scenarios.append([current_scn[0],current_scn[1],""])
                if self.__panneauwindow:
                    self.__panneauwindow.init_scn()
                    self.__panneauwindow.reload()
            if self.list_scenarios :
                self.results_box.setEnabled(True)

    def add_scenario_comparator(self):
        ''' fonction used to add a scenario in the comparator of secondary scenario'''
        scenarios = [scn for scn in self.__currentproject.get_scenarios() if self.__currentproject.scn_has_run_from_name(scn[1])] + [scn for scn in
            self.__currentproject.fetchall("select id, name from project.serie_scenario")
            if self.__currentproject.scn_has_run_from_name(scn[1])]

        if scenarios:
            input_dialog, ok = QInputDialog.getItem(None,"Add scenario","Add a scenario",  [scn[1] for scn in scenarios], 0, False)
            if input_dialog and ok :
                self.scenario_comparator_widget.list_scenarios.addItem(input_dialog)
                for scenario in scenarios :
                    if scenario[1] == input_dialog :
                        self.list_scenarios.append([scenario[0],scenario[1],""])
                        if self.__panneauwindow:
                            self.__panneauwindow.init_scn()
                            self.__panneauwindow.reload()
            if self.list_scenarios :
                self.results_box.setEnabled(True)

    def delete_scenario_comparator(self):
        ''' fonction used to remove a scenario in the comparator of secondary scenario'''
        if self.scenario_comparator_widget.list_scenarios.count()!=0:
            deleted_scn = self.list_scenarios[self.scenario_comparator_widget.list_scenarios.currentRow()]
            if self.scenario_comparator_widget.list_scenarios.currentRow() != -1 :
                self.__panneauwindow.remove_secondary_scn(deleted_scn)
                self.list_scenarios.pop(self.scenario_comparator_widget.list_scenarios.currentRow())
                if not self.list_scenarios :
                    self.__time_control.set_active(False)
                    self.results_box.setEnabled(False)
                self.reload_scn_comparator()

    def checkbox_refresh(self):
        if self.__panneauwindow is None:
            self.__panneauwindow = LongProfilInterface(self.__iface,self.__currentproject,self.__time_control,parent = self)
        self.__panneauwindow.reload()

    def checkbox_refresh_scn_changed(self):
        if self.__panneauwindow is None:
            self.__panneauwindow = LongProfilInterface(self.__iface,self.__currentproject,self.__time_control,parent = self)
        self.__panneauwindow.reload(override=True)
        self.chk_quality.setEnabled(False)
        temp_quality_bool =  self.chk_quality.isChecked()
        self.chk_quality.setChecked(False)

        self.result_box.setEnabled(True)
        self.result_box.setChecked(False)

        self.result_box.setEnabled(self.__panneauwindow.param_header is None)

        self.chk_quality.setEnabled(self.__panneauwindow.param_header is not None)
        self.chk_quality.setChecked(temp_quality_bool)
        self.__panneauwindow.reload()

    def __reset(self) :
        for key in list(_list_checkbox_.keys()) :
            getattr(self,'chk_'+key).setChecked(False)
        self.branchs=[]
        self.list_scenarios=[]
        self.reload_branchmanager()
        self.reload_scn_comparator()
        self.btn_display_min.setChecked(False)
        self.btn_display_max.setChecked(False)
