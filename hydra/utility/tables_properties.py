from builtins import object
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import json
import os

_utility_dir = os.path.join(os.path.dirname(__file__))

class TablesProperties(object):
    __properties = None

    @staticmethod
    def get_properties():
        if TablesProperties.__properties is None:
            with open(os.path.join(_utility_dir, "tables_properties.json")) as f:
                TablesProperties.__properties = json.load(f)
        return TablesProperties.__properties

    @staticmethod
    def reset_properties():
        TablesProperties.__properties = None
