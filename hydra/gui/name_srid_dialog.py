# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtWidgets import QDialog, QMessageBox
from qgis.gui import QgsProjectionSelectionDialog

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

class NameSridDialog(QDialog):
    def __init__(self, parent=None, name='', srid=2154):
        QDialog.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "name_srid_dialog.ui"), self)

        self.name.setText(name)
        self.srid.setText(str(srid))

        self.btn_epsg.clicked.connect(self.select_epsg)

    def select_epsg(self):
        projSelector = QgsProjectionSelectionDialog()
        projSelector.exec_()
        authId = projSelector.crs().authid()
        if authId[:5]=='EPSG:':
            self.srid.setText(authId[5:])
        else:
            QMessageBox(QMessageBox.Warning, tr('Not a valid SRID'), tr('Please select a SRID with code starting with "ESPG:..."'), QMessageBox.Ok).exec_()

    def get_result(self):
        return self.name.text(), int(self.srid.text())
