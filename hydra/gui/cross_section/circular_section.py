# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from qgis.PyQt.QtWidgets import QWidget
from qgis.PyQt.QtCore import pyqtSignal
from qgis.PyQt import uic
import os
from shapely import wkb
from math import cos, sin, pi

from .geometry import Geometry

class CircularSection(QWidget):

    geometry_changed = pyqtSignal(dict)
    transect_changed = pyqtSignal(list)
    section_changed = pyqtSignal(dict)
    new_geometry = pyqtSignal()

    def __init__(self, z_invert, rk, diameter, transect, terrain, parent=None):
        QWidget.__init__(self, parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "circular_section.ui"), self)

        self.__geometry = None

        self.z_invert.setValue(z_invert if z_invert is not None else -999)
        self.rk.setValue(rk if rk is not None else  0)
        self.diameter.setValue(diameter if diameter is not None else  0)

        self.z_invert.valueChanged.connect(self.__section_parameters_changed)
        self.rk.valueChanged.connect(self.__section_parameters_changed)
        self.diameter.valueChanged.connect(self.__section_parameters_changed)

        self.__transect = wkb.loads(transect, True) if transect is not None else None
        self.__terrain = terrain

        self.__set_geometry()


    def __getattr__(self, name):
        if name == 'geom':
            return self.__geometry
        elif name == 'param':
            return {
            "z_invert": self.z_invert.value(),
            "rk": self.rk.value(),
            "circular_diameter": self.diameter.value()
            }
        else:
            raise AttributeError(name)

    def __set_geometry(self):
        diameter = self.diameter.value()
        dz = self.z_invert.value() + .5*diameter
        self.__geometry = Geometry(
            [[.5*diameter*cos(pi*t/15),  .5*diameter*(sin(pi*t/15))] for t in range(30+1)],
            dz,
            self.__transect,
            self.__terrain)

    def __section_parameters_changed(self):
        self.__set_geometry()
        self.section_changed.emit(self.param)
