# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from qgis.PyQt.QtWidgets import QWidget, QInputDialog, QLineEdit
from qgis.PyQt.QtCore import pyqtSignal
from qgis.PyQt import uic
import os
from .valley_geometry import ValleyGeometry
from shapely import wkb

class ValleySection(QWidget):
    geometry_changed = pyqtSignal(dict)
    transect_changed = pyqtSignal(list)
    section_changed = pyqtSignal(dict)
    new_geometry = pyqtSignal()

    def __init__(self, z_invert, rk, rk_maj, sinuosity, geometry, fake_transect, transects, geometries, terrain, parent=None):
        QWidget.__init__(self, parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "valley_section.ui"), self)
        self.__geometry = None
        self.geometry_layout.addWidget(QWidget())
        # TODO completes data if no geometry exists
        self.z_invert.setValue(z_invert if z_invert is not None else -9999)
        self.rk.setValue(rk if rk is not None else  0)
        self.rk_maj.setValue(rk_maj if rk_maj is not None else  0)
        self.sinuosity.setValue(sinuosity if sinuosity is not None else  0)

        self.invert_altitude_control.toggled.connect(self.__set_invert_altitude_control)

        self.z_invert.valueChanged.connect(self.__z_invertChanged)
        self.rk.valueChanged.connect(self.__section_parameters_changed)
        self.rk_maj.valueChanged.connect(self.__section_parameters_changed)
        self.sinuosity.valueChanged.connect(self.__section_parameters_changed)

        self.new_geom.clicked.connect(self.new_geometry.emit)
        self.rename_geom.clicked.connect(self.__rename_geometry)

        self.__geometries = geometries
        self.__transects = transects
        self.__fake_transect = wkb.loads(fake_transect, True) if fake_transect is not None else None
        self.__terrain = terrain

        assert(None not in geometries)
        idx = None
        for i, g in sorted(geometries.items(), key=lambda x: x[1]['name']):
            self.geometry_combo.addItem(g['name'], userData=i)
            if geometry == i:
                idx = self.geometry_combo.count()-1
        self.geometry_combo.addItem('')
        if idx is None:
            idx = self.geometry_combo.count()-1
        else:
            self.__set_geometry(idx)
        self.geometry_combo.setCurrentIndex(idx)
        self.geometry_combo.currentIndexChanged.connect(self.__set_geometry)

    def __rename_geometry(self):
        g_id = self.geometry_combo.itemData(self.geometry_combo.currentIndex())
        if g_id is not None:
            t = QInputDialog.getText(self, 'Rename', "New name", QLineEdit.Normal, self.__geometries[g_id]['name'])
            if t[1]:
                self.__geometries[g_id]['name'] = t[0]
                self.geometry_combo.setItemText(self.geometry_combo.currentIndex(), t[0])

    def __getattr__(self, name):
        if name == 'geom':
            return self.__geometry
        elif name == 'param':
            return {
            "z_invert": self.z_invert.value(),
            "rk": self.rk.value(),
            "rk_maj": self.rk_maj.value(),
            "sinuosity": self.sinuosity.value(),
            "vcs_geom": self.geometry_combo.itemData(self.geometry_combo.currentIndex())
            }
        else:
            raise AttributeError(name)

    def __set_geometry(self, idx):
        w = self.geometry_layout.takeAt(0)
        w and w.widget().setParent(None)

        g_id = self.geometry_combo.itemData(idx)

        if g_id is not None:

            vcs_geom = self.__geometries[g_id]

            if self.z_invert.value() == -9999 or vcs_geom['zbmin_array'] is None or abs(self.z_invert.value() - vcs_geom['zbmin_array'][0][0]) < 0.01:
                self.invert_altitude_control.setChecked(False)
                dz = 0
            else:
                self.invert_altitude_control.setChecked(True)
                dz = self.z_invert.value() - vcs_geom['zbmin_array'][0][0] if vcs_geom['zbmin_array'] is not None else 0

            self.__geometry = ValleyGeometry(
                vcs_geom['zbmin_array'], vcs_geom['zbmaj_lbank_array'], vcs_geom['zbmaj_rbank_array'],
                vcs_geom['rlambda'], vcs_geom['rmu1'], vcs_geom['rmu2'],
                vcs_geom['zlevee_lb'], vcs_geom['zlevee_rb'],
                vcs_geom['sz_array'], vcs_geom['transect'],
                self.__transects, self.__terrain, dz, self.__fake_transect, parent=self)

            if not self.invert_altitude_control.isChecked():
                self.z_invert.blockSignals(True)
                self.z_invert.setValue(self.__geometry.z_invert)
                self.z_invert.blockSignals(False)
                self.__set_z_invert_tooltip()

            self.__set_z_invert_tooltip()

            self.__geometry.geometry_changed.connect(self.__geometry_changed)
            self.__geometry.transect_changed.connect(self.transect_changed.emit)

            self.z_invert.blockSignals(True)
            self.z_invert.setValue(self.__geometry.z_invert)
            self.z_invert.blockSignals(False)

        else:
            self.__geometry = None

        self.section_changed.emit(self.param)
        self.geometry_layout.addWidget(self.__geometry)

    def __set_invert_altitude_control(self, flag):
        if not flag and self.__geometry is not None:
            self.__geometry.set_dz(0)
            self.z_invert.blockSignals(True)
            self.z_invert.setValue(self.__geometry.z_invert)
            self.z_invert.blockSignals(False)
            self.geometry_changed.emit(self.__geometry.param)
            self.section_changed.emit(self.param)

    def __geometry_changed(self, param):
        self.z_invert.blockSignals(True)
        self.z_invert.setValue(self.__geometry.z_invert)
        self.z_invert.blockSignals(False)
        self.__set_z_invert_tooltip()
        self.geometry_changed.emit(param)
        self.section_changed.emit(self.param)

    def __section_parameters_changed(self):
        self.section_changed.emit(self.param)

    def __z_invertChanged(self, x):
        self.__geometry.set_dz(x - self.__geometry.param['zbmin_array'][0][0])
        self.__set_z_invert_tooltip()
        self.__section_parameters_changed()

    def __set_z_invert_tooltip(self):
        if abs(self.__geometry.dz) > 0.01:
            self.z_invert.setToolTip(f"altitude offset {self.__geometry.dz:.2f}m")
            #self.z_invert.setStyleSheet("background-color: lightgrey;")
        else:
            self.z_invert.setToolTip("")
            #self.z_invert.setStyleSheet("background-color: white;")
