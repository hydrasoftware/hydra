# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from __future__ import unicode_literals
from builtins import str
from builtins import range
import os
import webbrowser
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QWidget, QGridLayout, QFileDialog, QTableWidgetItem, QInputDialog
from hydra.gui.base_dialog import tr
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from hydra.utility.settings_properties import SettingsProperties
from hydra.utility.system import open_external


class SerieRegulationConfigurationWidget(QWidget):

    def __init__(self, current_project, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "regulation_and_configuration.ui"), self)
        tmplayout = QGridLayout()
        tmplayout.addWidget(self,0,0)
        parent.setLayout(tmplayout)
        self.active_bloc_id = None
        self.active_serie_id = None
        self.project = current_project

        self.btnAdd.clicked.connect(self.add)
        self.btnDel.clicked.connect(self.delete)
        self.btnUp.clicked.connect(self.up)
        self.btnDown.clicked.connect(self.down)
        self.label_info.setText("")

        self.table_reg_files = HydraTableWidget(self.project,
            ("id", "control_file"),
            (tr("Id"), tr("Path")),
            "project", "param_regulation_bloc",
            (self.__ask_and_add_reg_file, self.del_reg_file, self.edit_reg_file, self.help_reg_file), "", "id",
            self.table_reg_files_files_placeholder, autoInit=False, showEdit=True, showHelp=True, file_columns=[1])
        self.table_reg_files.set_editable(True)

        models = self.project.get_models()
        for model in models:
            self.combo_model.addItem(model)
        self.combo_model.activated.connect(self.refresh_config)

        self.show()

    def add(self):
        if self.project.get_models():
            query = self.project.execute("""select name from {m}.configuration
                                            where id not in (select configuration
                                                                from project.config_serie
                                                                where serie={s} and model='{m}')
                                            and id!=1;""".format(m=self.combo_model.currentText(), s=self.active_serie_id)).fetchall()
            configs = [string for (string,) in query]
            if configs:
                config, ok = QInputDialog.getItem(self, "Configuration selection", "Select the configuration to add", configs, 0, False)
                if ok and config:
                    config_id = self.__get_config_id(config)
                    self.project.execute("""insert into project.config_serie(serie, model, configuration) values({}, '{}', {});""".format(
                                            self.active_serie_id, self.combo_model.currentText(), config_id))
                    self.update_priorities()
                    self.refresh_config()

    def delete(self):
        current_index = self.table_config.currentRow()
        if current_index==-1:
            return
        config_id = self.__get_config_id(self.table_config.item(current_index,0).text())
        self.project.execute("""delete from project.config_serie
                                where configuration={} and model='{}' and serie={};""".format(
                                config_id, self.combo_model.currentText(), self.active_serie_id))
        self.update_priorities()
        self.refresh_config()

    def up(self):
        current_index = self.table_config.currentRow()
        if current_index==-1:
            return
        item1 = self.table_config.takeItem(current_index, 0)
        rowPosition = current_index - 1
        self.table_config.insertRow(rowPosition)
        self.table_config.setItem(rowPosition , 0, item1)
        self.table_config.removeRow(current_index+1)
        self.update_priorities()
        self.refresh_config()

    def down(self):
        current_index = self.table_config.currentRow()
        if current_index==-1:
            return
        if current_index==self.table_config.rowCount()-1:
            return
        item1 = self.table_config.takeItem(current_index, 0)
        rowPosition = current_index + 2
        self.table_config.insertRow(rowPosition)
        self.table_config.setItem(rowPosition , 0, item1)
        self.table_config.removeRow(current_index)
        self.update_priorities()
        self.refresh_config()

    def refresh_config(self):
        if  not self.project.get_models():
            self.btnAdd.setEnabled(False)
        elif self.active_bloc_id is not None and self.combo_model!=-1:
            self.table_config.setRowCount(0)
            model = self.combo_model.currentText()
            configs_brut = self.project.execute("""select configuration from project.config_serie where model='{}' and serie={} order by priority asc;""".format(model, self.active_serie_id)).fetchall()
            configs = [string for (string,) in configs_brut]
            for config in configs:
                config_name = self.__get_config_name(config)
                rowPosition = self.table_config.rowCount()
                self.table_config.insertRow(rowPosition)
                self.table_config.setItem(rowPosition , 0, QTableWidgetItem(str(config_name)))

            self.table_config.resizeColumnsToContents()
            self.table_config.horizontalHeader().setStretchLastSection(True)

            other_configs_exists = self.project.execute("""select exists(select 1 from {m}.configuration
                                        where id not in (select configuration
                                                            from project.config_serie
                                                            where serie={s} and model='{m}')
                                        and id!=1);""".format(m=self.combo_model.currentText(), s=self.active_serie_id)).fetchone()[0]
            self.btnAdd.setEnabled(other_configs_exists)

            if len(configs)>1:
                self.label_info.setText("For items in multiple configurations, the top-most\nconfiguration will be used in computation.")
            else:
                self.label_info.setText("")

    def update_priorities(self):
        if self.active_serie_id is not None and self.combo_model!=-1:
            for i in range(0, self.table_config.rowCount()):
                self.project.execute("""update project.config_serie
                                        set priority={}
                                        where configuration={} and serie={} and model='{}';""".format(
                                        i+1, self.__get_config_id(self.table_config.item(i,0).text()), self.active_serie_id, self.combo_model.currentText()))

    def __get_config_id(self, config_name):
        if self.combo_model!=-1:
            config_id, = self.project.execute("""select id from {}.configuration where name='{}';""".format(self.combo_model.currentText(), config_name)).fetchone()
            return config_id

    def __get_config_name(self, config_id):
        if self.combo_model!=-1:
            config_name, = self.project.execute("""select name from {}.configuration where id={};""".format(self.combo_model.currentText(), config_id)).fetchone()
            return config_name

    def __ask_and_add_reg_file(self):
        if self.active_bloc_id is not None:
            fileName, __ = QFileDialog.getSaveFileName(self, tr('Add file'), SettingsProperties.get_memorized_dir() or self.project.data_dir, options=QFileDialog.DontConfirmOverwrite)
            if fileName is not None and fileName!='':
                # creates file empty if not exists
                open(fileName, 'a').close()
                self.add_reg_file(fileName)
                # saves directory in settings for other files openings (for current session)
                SettingsProperties.set_memorized_dir(os.path.dirname(fileName))

    def add_reg_file(self, filePath):
        if self.active_bloc_id is not None:
            self.table_reg_files.add_row((filePath, self.active_bloc_id), ["param_bloc"])

    def del_reg_file(self):
        if self.active_bloc_id is not None:
            self.table_reg_files.del_selected_row()

    def edit_reg_file(self):
        items = self.table_reg_files.table.selectedItems()
        if len(items)>0:
            file = items[1].text()
            open_external(file)

    def help_reg_file(self):
        webbrowser.open('http://hydra-software.net/docs/notes_techniques/NT22.pdf', new=0)

    def set_bloc_serie(self, id_bloc, id_serie):
        self.gb_config.setEnabled(False)
        self.table_reg_files.setEnabled(False)
        if id_serie :
            self.active_serie_id = int(id_serie)
            self.gb_config.setEnabled(True)
            self.refresh_config()
        if id_bloc :
            self.active_bloc_id = int(id_bloc)
            self.table_reg_files.set_filter("where param_bloc={}".format(self.active_bloc_id))
            self.refresh_config()
            self.table_reg_files.setEnabled(True)
            self.gb_config.setEnabled(False)


    def save(self):
        # nothing to save, everything is dynamically inserted/updated in DB for this widget
        return
