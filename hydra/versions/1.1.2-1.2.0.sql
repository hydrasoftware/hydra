/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  Update version                                */
/* ********************************************** */

update hydra.metadata set version = '1.2.0';
;;

create or replace function project.model_instead_fct()
returns trigger
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_model
    for statement in create_model(TD['new']['name'], $srid, '$version'):
        plpy.execute(statement)
$$$$
;;

/* ********************************************** */
/*  Catchment runoff_types structural changes     */
/* ********************************************** */

--isolated--
alter type hydra_runoff_type add value if not exists 'define_k';
;;

insert into hydra.runoff_type (name, id, description) values ('define_k', 5, 'Defined K and linear reservoir');
;;

/* ********************************************** */
/*  Radar rainfall new unit_correction_coef       */
/* ********************************************** */

alter table project._radar_rainfall add column if not exists unit_correction_coef real not null default 1;
;;

drop view project.radar_rainfall cascade;
;;

create or replace view project.radar_rainfall as
    select p.id,
        c.file,
        c.unit_correction_coef,
        p.name
    from project._radar_rainfall c,
        project._rainfall p
    where p.id = c.id;
;;

create trigger project_radar_rainfall_trig
  instead of insert or update or delete on project.radar_rainfall
  for each row execute procedure project.radar_rainfall_fct();
;;

create or replace function project.radar_rainfall_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into project._rainfall(rainfall_type, name)
                values('radar', coalesce(new.name, 'define_later')) returning id into id_;
            update project._rainfall set name = 'rain_'||id_::varchar
                where name = 'define_later' and id = id_;
            insert into project._radar_rainfall
                values (id_, 'radar', new.file, coalesce(new.unit_correction_coef, 1));
            update project._rainfall set validity = (select (file is not null) from  project._radar_rainfall where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update project._rainfall set name=new.name where id=old.id;
            update project._radar_rainfall set file=new.file, unit_correction_coef=new.unit_correction_coef where id=old.id;
            update project._rainfall set validity = (select (file is not null) from  project._radar_rainfall where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from project._radar_rainfall where id=old.id;
            delete from project._rainfall where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

/* ********************************************** */
/*  Scenario REF                                  */
/* ********************************************** */

alter table project.scenario add column scenario_ref integer references project.scenario(id);
;;
