# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import copy
from numpy import array
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QMessageBox, QLabel, QDialogButtonBox, QShortcut, QWidget
from qgis.PyQt.QtCore import Qt, QSize
from qgis.PyQt.QtGui import QKeySequence
from qgis.core import QgsProject, QgsDataSourceUri

from hydra.gui.section_viewer import GraphRoute
from hydra.gui.simple_singularity_dialog import SimpleSingularityDialog, tr

from hydra.gui.cross_section.circular_section import CircularSection
from hydra.gui.cross_section.ovoid_section import OvoidSection
from hydra.gui.cross_section.valley_section import ValleySection
from hydra.gui.cross_section.pipe_section import PipeSection
from hydra.gui.cross_section.channel_section import ChannelSection
from hydra.gui.cross_section.section_widget import SectionWidget
from hydra.gui.cross_section.minimap import MiniMap
from hydra.utility.timer import Timer

from shapely.geometry import LineString, Point
from shapely import wkb, geos


from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas

_table_info = {'pipe':    {'index': 0, 'table': 'closed_parametric_geometry'},
               'channel': {'index': 1, 'table': 'open_parametric_geometry'},
               'valley':  {'index': 2, 'table': 'valley_cross_section_geometry'}}

_forms_dir = os.path.dirname(__file__)
_ressource_dir = os.path.join(os.path.dirname(os.path.dirname(_forms_dir)), "ressources")
_current_dir = os.path.dirname(__file__)
_section_types = ['', 'valley', 'channel', 'pipe', 'ovoid', 'circular']
_references = ['up_cp_geom', 'up_op_geom', 'up_vcs_geom', 'down_cp_geom', 'down_op_geom', 'down_vcs_geom']

class RiverCrossSectionProfileEditor(SimpleSingularityDialog):
    def __init__(self, project, geom, id=None, parent=None, iface=None):
        SimpleSingularityDialog.__init__(self, project, geom, "river_cross_section_profile", id, parent)
        uic.loadUi(os.path.join(_current_dir, "river_cross_section_profile.ui"), self)

        self.__iface = iface
        self.buttonBox.rejected.connect(self.close)
        self.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(self.save_and_close)
        self.buttonBox.button(QDialogButtonBox.Save).clicked.connect(self.save)

        self.__fig_l = Figure()
        self.__canvas_l = FigureCanvas(self.__fig_l)
        self.longiProfileLayout.addWidget(self.__canvas_l)
        self.__minimap = MiniMap(None if iface is None else iface.mapCanvas().mapSettings())
        self.__minimap.setMaximumWidth(270)
        self.__minimap.setMaximumHeight(150)
        self.longiProfileLayout.addWidget(self.__minimap)

        self.__fig_t = Figure()

        self.upSectionType.addItems(_section_types)
        self.downSectionType.addItems(_section_types)
        reach, type_cs_up, type_cs_down, reach_geom, pk0_km = self.project.execute(f"""
                                        select n.reach, p.type_cross_section_up, p.type_cross_section_down, r.geom, r.pk0_km
                                        from {self.model.name}.river_cross_section_profile p
                                        left join {self.model.name}.river_node n on n.id = p.id
                                        left join {self.model.name}.reach r on r.id = n.reach
                                        where p.id={self.id}
                                        """).fetchone()
        if reach is None:
            # TODO warn that reach cannot be None
            self.close()

        dem = self.project.execute("select source from project.dem order by priority desc").fetchone()
        self.__dem = dem[0] if dem is not None and len(dem) else None

        # Get cross section data for all profiles on the reach and store them
        fields = [
            'id', 'name', 'z_invert_up', 'z_invert_down', 'type_cross_section_up', 'type_cross_section_down',
            'up_rk', 'down_rk', 'up_circular_diameter', 'down_circular_diameter', 'up_ovoid_top_diameter', 'down_ovoid_top_diameter',
            'up_ovoid_invert_diameter', 'down_ovoid_invert_diameter', 'up_ovoid_height', 'down_ovoid_height',
            'up_rk_maj', 'down_rk_maj', 'up_sinuosity', 'down_sinuosity', 'up_cp_geom', 'up_op_geom', 'up_vcs_geom',
            'down_cp_geom', 'down_op_geom', 'down_vcs_geom', 'geom']
        self.__cross_sections = [{**{f: row[i] for i, f in enumerate(fields)}, **{'pk': row[-2], 'transect': row[-1]}}
            for row in self.project.execute(f"""
                    select {' ,'.join([f'p.{f}' for f in fields])}, n.pk_km,
                    project.fake_transect(r.geom, n.geom, 100)
                    from {self.model.name}.river_cross_section_profile p
                    join {self.model.name}.river_node n on n.id = p.id
                    join {self.model.name}.reach r on r.id = n.reach
                    where n.reach={reach} order by n.pk_km asc""").fetchall()]

        self.__cross_sections_idx = [r['id'] for r in self.__cross_sections].index(int(self.id))

        if self.current_section['type_cross_section_up'] is not None:
            self.upRadio.setChecked(True)
        else:
            self.downRadio.setChecked(True)

        self.__pipe_geom = {id_:{"name": name, "zbmin_array": zbmin_array}
            for id_, name, zbmin_array in project.execute(f"select id, name, zbmin_array from {self.model.name}.closed_parametric_geometry").fetchall()}
        self.__initial_pipe_geom = copy.deepcopy(self.__pipe_geom)

        self.__channel_geom = {id_:{"name": name, "zbmin_array": zbmin_array}
            for id_, name, zbmin_array in project.execute(f"select id, name, zbmin_array from {self.model.name}.open_parametric_geometry").fetchall()}
        self.__initial_channel_geom = copy.deepcopy(self.__channel_geom)

        self.__valley_geom = {id_:{"name": name, "zbmin_array": zbmin_array, 'zbmaj_lbank_array': zbmaj_lbank_array,
                'zbmaj_rbank_array': zbmaj_rbank_array, 'rlambda': rlambda, 'rmu1': rmu1, 'rmu2': rmu2,
                'zlevee_lb': zlevee_lb, 'zlevee_rb': zlevee_rb, 'transect': transect, 'sz_array': sz_array}
            for id_, name, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, rlambda, rmu1, rmu2, zlevee_lb, zlevee_rb, transect, sz_array
                in project.execute(f"""
                select id, name, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, rlambda, rmu1, rmu2, zlevee_lb, zlevee_rb, transect, sz_array
                from {self.model.name}.valley_cross_section_geometry""").fetchall()}
        self.__initial_valley_geom = copy.deepcopy(self.__valley_geom)

        sql = f"""
            with transect as (
                select c.id, c.name, c.geom, st_intersection(r.geom, c.geom) as inter, r.geom as reach_geom, r.name as reach_name, r.pk0_km,
                    st_collect(st_setsrid(st_makepoint(st_x(p.geom), st_y(p.geom), p.z_ground), {self.project.srid})) as points, c.topo
                from {self.model.name}.constrain c
                join {self.model.name}.reach r on st_intersects(r.geom, c.geom)
                left join project.points_xyz p on st_dwithin(c.geom, p.geom, c.points_xyz_proximity) and p.points_id = c.points_xyz
                where st_intersects(r.geom, c.geom)
                and constrain_type in ('flood_plain_transect', 'ignored_for_coverages')
                and r.id={reach}
                group by c.id, c.name, c.geom, r.geom, r.name, r.pk0_km, c.topo
            )
            select t.id, t.name, t.geom, st_linelocatepoint(reach_geom, t.inter)*st_length(reach_geom)/1000 + pk0_km as pk, inter, points, topo
            from transect t
            order by pk asc
            """

        self.__transects = {id_: {'name': name, 'geom': geom, 'pk': pk, 'inter': inter, 'points': points, 'topo': topo}
            for id_, name, geom, pk, inter, points, topo in self.project.execute(sql).fetchall()}
        self.__initial_transects = copy.deepcopy(self.__transects)


        self.__initial_cross_sections = copy.deepcopy(self.__cross_sections)

        self.__axes_l = self.__fig_l.add_subplot(111)
        self.__fig_l.subplots_adjust(left=0.03, bottom=0.20, right=0.99, top=0.99)
        self.__sline = None
        self.__bottom_line = None
        self.__terrain_line = None
        self.__terrain_line_fill = None

        reach_g = wkb.loads(reach_geom, True)
        topo_points = self.project.terrain.line_elevation(reach_g.coords)
        topo = array([[reach_g.project(Point(p))/1000, p[2]] for p in topo_points])
        self.__terrain_line, = self.__axes_l.plot(topo[:,0] + pk0_km, topo[:,1], color='grey', linewidth=.5)
        self.__terrain_line_fill = self.__axes_l.fill_between(topo[:,0] + pk0_km, topo[:,1], -999, color='goldenrod', linewidth=.5, alpha=.1)

        self.__draw_l()

        self.__section_widget = SectionWidget() # FigureCanvas(self.__fig_t)
        self.graphLayout.addWidget(self.__section_widget)
        self.__section_widget.transect_changed.connect(self.__transect_changed)
        self.__section_widget.section_changed.connect(self.__section_parameters_changed)
        self.__section_widget.geometry_changed.connect(self.__geometry_parameters_changed)
        self.__section_widget.new_geometry.connect(self.__new_geometry)

        self.__section_changed()

        self.upRadio.toggled.connect(self.__section_changed)
        self.next.clicked.connect(self.__next)
        self.previous.clicked.connect(self.__previous)
        self.upSectionType.currentTextChanged.connect(self.__upSectionTypeChanged)
        self.downSectionType.currentTextChanged.connect(self.__downSectionTypeChanged)
        self.select_and_close.clicked.connect(self.__select_and_close)

        self.__shortcuts = [
            (QShortcut(QKeySequence("Left"), self), self.__previous),
            (QShortcut(QKeySequence("Right"), self), self.__next)]
        for s, f in self.__shortcuts:
            s.activated.connect(f)

        self.name.textChanged.connect(self.__name_changed)

    def __name_changed(self, name):
        self.current_section['name'] = name

    def __select_and_close(self):
        layers = [l
            for l in QgsProject.instance().mapLayers().values()
            if QgsDataSourceUri(l.dataProvider().dataSourceUri()).schema() == self.model.name
            and QgsDataSourceUri(l.dataProvider().dataSourceUri()).table() == 'river_cross_section_profile'
            ]

        layer, = layers
        self.__iface.setActiveLayer(layer)
        layer.removeSelection()
        layer.select(self.current_section['id'])
        self.__iface.actionPanToSelected().trigger()
        self.close()

    def __transect_changed(self, l):
        cs = self.current_section
        t = None
        if self.upRadio.isChecked() and cs['type_cross_section_up'] == 'valley':
            t_id = self.__valley_geom[cs['up_vcs_geom']]['transect']
            if t_id is not None:
                t = self.__transects[t_id]
        elif self.downRadio.isChecked() and cs['type_cross_section_down'] == 'valley':
            t_id = self.__valley_geom[cs['down_vcs_geom']]['transect']
            if t_id is not None:
                t = self.__transects[t_id]

        if t is not None:
            t['geom'] = wkb.dumps(LineString(l), hex=True, srid=self.project.srid)

        self.__minimap.set_transect(l)

    def __draw_l(self):
        x = []
        y = []
        for p in self.__cross_sections:
            if p['type_cross_section_up']:
                x.append(p['pk'])
                y.append(p['z_invert_up'] or -99)
            if p['type_cross_section_down']:
                x.append(p['pk'])
                y.append(p['z_invert_down'] or -99)
        self.__bottom_line and self.__bottom_line.remove()
        self.__sline and self.__sline.remove()
        ylim = [(min(y)-1) if len(y) else -1, (max(y)+1) if len(y) else 1]
        self.__axes_l.set_ylim(ylim)
        self.__bottom_line, = self.__axes_l.plot(x, y, '-+', ms=2, lw=.5, color='royalblue')

        self.__sline, = self.__axes_l.plot([self.current_section['pk']]*3,
            ylim[0:1]
            + ([self.current_section['z_invert_up'] if self.current_section['z_invert_up'] is not None else -99]
                if self.upRadio.isChecked() else [self.current_section['z_invert_down'] if self.current_section['z_invert_down'] is not None else -99])
            + ylim[1:], '-o', color='red')
        self.__fig_l.canvas.draw()


    def __getattr__(self, name):
        if name == 'current_section':
            return self.__cross_sections[self.__cross_sections_idx]
        else:
            raise AttributeError(name)

    def __next(self):
        if self.upRadio.isChecked() and self.current_section['type_cross_section_down'] is not None:
            self.downRadio.setChecked(True)
        else:
            self.__cross_sections_idx = (self.__cross_sections_idx+1)%len(self.__cross_sections)
            if self.current_section['type_cross_section_up'] is not None:
                if self.upRadio.isChecked():
                    self.__section_changed()
                else:
                    self.upRadio.setChecked(True)
            else:
                if self.downRadio.isChecked():
                    self.__section_changed()
                else:
                    self.downRadio.setChecked(True)

    def __previous(self):
        if self.downRadio.isChecked() and self.current_section['type_cross_section_up'] is not None:
            self.upRadio.setChecked(True)
        else:
            self.__cross_sections_idx = self.__cross_sections_idx-1 if self.__cross_sections_idx !=0 else len(self.__cross_sections)-1
            if self.current_section['type_cross_section_down'] is not None:
                if self.downRadio.isChecked():
                    self.__section_changed()
                else:
                    self.downRadio.setChecked(True)
            else:
                if self.upRadio.isChecked():
                    self.__section_changed()
                else:
                    self.upRadio.setChecked(True)

    def __new_geometry(self):
        if self.upRadio.isChecked():
            self.current_section['up_cp_geom'] = None
            self.current_section['up_op_geom'] = None
            self.current_section['up_vcs_geom'] = None
            self.__upSectionTypeChanged(self.current_section['type_cross_section_up'], 'new geometry')
        else:
            self.current_section['down_cp_geom'] = None
            self.current_section['down_op_geom'] = None
            self.current_section['down_vcs_geom'] = None
            self.__downSectionTypeChanged(self.current_section['type_cross_section_down'], 'new geometry')


    def __upSectionTypeChanged(self, type_, geometry_name=None):
        self.current_section['type_cross_section_up'] = type_ or None
        new_name = self.current_section['name']+'_up' if geometry_name is None else geometry_name
        if type_ == 'pipe' and self.current_section['up_cp_geom'] is None:
            new_id = (max(self.__pipe_geom.keys()) + 1) if len(self.__pipe_geom) else 1
            self.__pipe_geom[new_id] = {'name': new_name,'zbmin_array':[[0,0]]}
            self.current_section['up_cp_geom'] = new_id
            self.current_section['z_invert_up'] = 0
        if type_ == 'channel' and self.current_section['up_op_geom'] is None:
            new_id = (max(self.__channel_geom.keys()) + 1)  if len(self.__channel_geom) else 1
            self.__channel_geom[new_id] = {'name': new_name,'zbmin_array':[[0,0]]}
            self.current_section['up_op_geom'] = new_id
            self.current_section['z_invert_up'] = 0
        if type_ == 'valley' and self.current_section['up_vcs_geom'] is None:
            d = float('inf')
            trsc = None
            for id_, t in self.__transects.items():
                if t['name'] == self.current_section['name']:
                    trsc = id_
                    break
                if abs(t['pk'] - self.current_section['pk']) < d:
                    trsc = id_
                    d = abs(t['pk'] - self.current_section['pk'])
            new_id = (max(self.__valley_geom.keys()) + 1) if len(self.__valley_geom) else 1
            self.__valley_geom[new_id] = {'name': new_name,
                'sz_array': None,
                'zbmin_array': [[0,0]],
                'zbmaj_lbank_array': [[0,0]],
                'zbmaj_rbank_array': [[0,0]],
                'transect': trsc,
                'rlambda': 1,
                'rmu2': 0,
                'rmu1': 0,
                'zlevee_lb': -9999,
                'zlevee_rb': -9999
                }
            self.current_section['up_vcs_geom'] = new_id
            self.current_section['z_invert_up'] = None
        self.__change_widget()

    def __downSectionTypeChanged(self, type_, geometry_name=None):
        self.current_section['type_cross_section_down'] = type_ or None
        new_name = self.current_section['name']+'_down' if geometry_name is None else geometry_name
        if type_ == 'pipe' and self.current_section['down_cp_geom'] is None:
            new_id = (max(self.__pipe_geom.keys()) + 1) if len(self.__pipe_geom) else 1
            self.__pipe_geom[new_id] = {'name': new_name,'zbmin_array':[[0,0]]}
            self.current_section['down_cp_geom'] = new_id
            self.current_section['z_invert_down'] = 0
        if type_ == 'channel' and self.current_section['up_op_geom'] is None:
            new_id = (max(self.__channel_geom.keys()) + 1)  if len(self.__channel_geom) else 1
            self.__channel_geom[new_id] = {'name': new_name,'zbmin_array':[[0,0]]}
            self.current_section['down_op_geom'] = new_id
            self.current_section['z_invert_down'] = 0
        if type_ == 'valley' and self.current_section['down_vcs_geom'] is None:
            d = float('inf')
            trsc = None
            for id_, t in self.__transects.items():
                if t['name'] == self.current_section['name']:
                    trsc = id_
                    break
                if abs(t['pk'] - self.current_section['pk']) < d:
                    trsc = id_
                    d = abs(t['pk'] - self.current_section['pk'])
            new_id = (max(self.__valley_geom.keys()) + 1) if len(self.__valley_geom) else 1
            self.__valley_geom[new_id] = {'name': new_name,
                'sz_array': None,
                'zbmin_array': [[0,0]],
                'zbmaj_lbank_array': [[0,0]],
                'zbmaj_rbank_array': [[0,0]],
                'transect': trsc,
                'rlambda': 1,
                'rmu2': 0,
                'rmu1': 0,
                'zlevee_lb': -9999,
                'zlevee_rb': -9999
                }
            self.current_section['down_vcs_geom'] = new_id
            self.current_section['z_invert_down'] = None
        self.__change_widget()

    def __section_changed(self):
        self.upSectionType.blockSignals(True)
        self.upSectionType.setCurrentText(self.current_section['type_cross_section_up'] or '')
        self.upSectionType.blockSignals(False)
        self.downSectionType.blockSignals(True)
        self.downSectionType.setCurrentText(self.current_section['type_cross_section_down'] or '')
        self.downSectionType.blockSignals(False)
        self.name.setText(self.current_section['name'])

        self.__draw_l()
        self.__change_widget()

    def __change_widget(self):
        cs = self.current_section
        if self.upRadio.isChecked() and cs['type_cross_section_up'] == 'circular':
            s = CircularSection(
                cs['z_invert_up'],
                cs['up_rk'],
                cs['up_circular_diameter'],
                cs['transect'],
                self.project.terrain
                )
        elif self.downRadio.isChecked() and cs['type_cross_section_down'] == 'circular':
            s = CircularSection(
                cs['z_invert_down'],
                cs['down_rk'],
                cs['down_circular_diameter'],
                cs['transect'],
                self.project.terrain
                )
        elif self.upRadio.isChecked() and cs['type_cross_section_up'] == 'ovoid':
            s = OvoidSection(
                cs['z_invert_up'],
                cs['up_rk'],
                cs['up_ovoid_top_diameter'],
                cs['up_ovoid_invert_diameter'],
                cs['up_ovoid_height'],
                cs['transect'],
                self.project.terrain
                )
        elif self.downRadio.isChecked() and cs['type_cross_section_down'] == 'ovoid':
            s = OvoidSection(
                cs['z_invert_down'],
                cs['down_rk'],
                cs['down_ovoid_top_diameter'],
                cs['down_ovoid_invert_diameter'],
                cs['down_ovoid_height'],
                cs['transect'],
                self.project.terrain
                )
        elif self.upRadio.isChecked() and cs['type_cross_section_up'] == 'pipe':
            s = PipeSection(
                cs['z_invert_up'],
                cs['up_rk'],
                cs['up_cp_geom'],
                self.__pipe_geom,
                cs['transect'],
                self.project.terrain
                )
        elif self.downRadio.isChecked() and cs['type_cross_section_down'] == 'pipe':
            s = PipeSection(
                cs['z_invert_down'],
                cs['down_rk'],
                cs['down_cp_geom'],
                self.__pipe_geom,
                cs['transect'],
                self.project.terrain
                )
        elif self.upRadio.isChecked() and cs['type_cross_section_up'] == 'channel':
            s = ChannelSection(
                cs['z_invert_up'],
                cs['up_rk'],
                cs['up_rk_maj'],
                cs['up_op_geom'],
                self.__channel_geom,
                cs['transect'],
                self.project.terrain
                )
        elif self.downRadio.isChecked() and cs['type_cross_section_down'] == 'channel':
            s = ChannelSection(
                cs['z_invert_down'],
                cs['down_rk'],
                cs['down_rk_maj'],
                cs['down_op_geom'],
                self.__channel_geom,
                cs['transect'],
                self.project.terrain
                )
        elif self.upRadio.isChecked() and cs['type_cross_section_up'] == 'valley':
            s = ValleySection(
                cs['z_invert_up'],
                cs['up_rk'],
                cs['up_rk_maj'],
                cs['up_sinuosity'],
                cs['up_vcs_geom'],
                cs['transect'],
                self.__transects,
                self.__valley_geom,
                self.project.terrain
                )
        elif self.downRadio.isChecked() and cs['type_cross_section_down'] == 'valley':
            s = ValleySection(
                cs['z_invert_down'],
                cs['down_rk'],
                cs['down_rk_maj'],
                cs['down_sinuosity'],
                cs['down_vcs_geom'],
                cs['transect'],
                self.__transects,
                self.__valley_geom,
                self.project.terrain
                )
        else:
            s = None

        self.__section_widget.set_section(s)

    def __section_parameters_changed(self, param):
        cs = self.current_section
        if self.upRadio.isChecked() and cs['type_cross_section_up'] == 'circular':
            cs['z_invert_up'] = param['z_invert']
            cs['up_rk'] = param['rk']
            cs['up_circular_diameter'] = param['circular_diameter']
            cs['up_cp_geom'] = None
            cs['up_op_geom'] = None
            cs['up_vcs_geom'] = None
        elif self.downRadio.isChecked() and cs['type_cross_section_down'] == 'circular':
            cs['z_invert_down'] = param['z_invert']
            cs['down_rk'] = param['rk']
            cs['down_circular_diameter'] = param['circular_diameter']
            cs['down_cp_geom'] = None
            cs['down_op_geom'] = None
            cs['down_vcs_geom'] = None
        elif self.upRadio.isChecked() and cs['type_cross_section_up'] == 'ovoid':
            cs['z_invert_up'] = param['z_invert']
            cs['up_rk'] = param['rk']
            cs['up_ovoid_top_diameter'] = param['ovoid_top_diameter']
            cs['up_ovoid_invert_diameter'] = param['ovoid_invert_diameter']
            cs['up_ovoid_height'] = param['ovoid_height']
            cs['up_cp_geom'] = None
            cs['up_op_geom'] = None
            cs['up_vcs_geom'] = None
        elif self.downRadio.isChecked() and cs['type_cross_section_down'] == 'ovoid':
            cs['z_invert_down'] = param['z_invert']
            cs['down_rk'] = param['rk']
            cs['down_ovoid_top_diameter'] = param['ovoid_top_diameter']
            cs['down_ovoid_invert_diameter'] = param['ovoid_invert_diameter']
            cs['down_ovoid_height'] = param['ovoid_height']
            cs['down_cp_geom'] = None
            cs['down_op_geom'] = None
            cs['down_vcs_geom'] = None
        elif self.upRadio.isChecked() and cs['type_cross_section_up'] == 'pipe':
            cs['z_invert_up'] = param['z_invert']
            cs['up_rk'] = param['rk']
            cs['up_cp_geom'] = param['cp_geom']
            cs['up_op_geom'] = None
            cs['up_vcs_geom'] = None
        elif self.downRadio.isChecked() and cs['type_cross_section_down'] == 'pipe':
            cs['z_invert_down'] = param['z_invert']
            cs['down_rk'] = param['rk']
            cs['down_cp_geom'] = param['cp_geom']
            cs['down_op_geom'] = None
            cs['down_vcs_geom'] = None
        elif self.upRadio.isChecked() and cs['type_cross_section_up'] == 'channel':
            cs['z_invert_up'] = param['z_invert']
            cs['up_rk'] = param['rk']
            cs['up_rk_maj'] = param['rk_maj']
            cs['up_op_geom'] = param['op_geom']
            cs['up_cp_geom'] = None
            cs['up_vcs_geom'] = None
        elif self.downRadio.isChecked() and cs['type_cross_section_down'] == 'channel':
            cs['z_invert_down'] = param['z_invert']
            cs['down_rk'] = param['rk']
            cs['down_rk_maj'] = param['rk_maj']
            cs['down_op_geom'] = param['op_geom']
            cs['down_cp_geom'] = None
            cs['down_vcs_geom'] = None
        elif self.upRadio.isChecked() and cs['type_cross_section_up'] == 'valley':
            cs['z_invert_up'] = param['z_invert']
            cs['up_rk'] = param['rk']
            cs['up_rk_maj'] = param['rk_maj']
            cs['up_sinuosity'] = param['sinuosity']
            cs['up_vcs_geom'] = param['vcs_geom']
            cs['up_op_geom'] = None
            cs['up_cp_geom'] = None
        elif self.downRadio.isChecked() and cs['type_cross_section_down'] == 'valley':
            before = copy.deepcopy(cs)
            cs['z_invert_down'] = param['z_invert']
            cs['down_rk'] = param['rk']
            cs['down_rk_maj'] = param['rk_maj']
            cs['down_sinuosity'] = param['sinuosity']
            cs['down_vcs_geom'] = param['vcs_geom']
            cs['down_op_geom'] = None
            cs['down_cp_geom'] = None

    def __geometry_parameters_changed(self, param):
        cs = self.current_section
        if self.upRadio.isChecked() and cs['type_cross_section_up'] == 'pipe':
            self.__pipe_geom[cs['up_cp_geom']].update(param)
        elif self.downRadio.isChecked() and cs['type_cross_section_down'] == 'pipe':
            self.__pipe_geom[cs['down_cp_geom']].update(param)
        elif self.upRadio.isChecked() and cs['type_cross_section_up'] == 'channel':
            self.__channel_geom[cs['up_op_geom']].update(param)
        elif self.downRadio.isChecked() and cs['type_cross_section_down'] == 'channel':
            self.__channel_geom[cs['down_op_geom']].update(param)
        elif self.upRadio.isChecked() and cs['type_cross_section_up'] == 'valley':
            self.__valley_geom[cs['up_vcs_geom']].update(param)
        elif self.downRadio.isChecked() and cs['type_cross_section_down'] == 'valley':
            self.__valley_geom[cs['down_vcs_geom']].update(param)

    def save(self):
        # update transects
        # TODO update also touching constrains
        for id_, t in self.__transects.items():
            if t != self.__initial_transects[id_]:
                self.project.execute(f"""
                    update {self.model.name}.constrain set geom=%s, name=%s where id=%s
                    returning name""", (t['geom'], t['name'], id_))
        self.__initial_transects = copy.deepcopy(self.__transects)

        # update op
        op_id_map = {}
        for id_, op in self.__channel_geom.items():
            if id_ not in self.__initial_channel_geom:
                if id_ not in [cs['up_op_geom'] for cs in self.__cross_sections] + [cs['down_op_geom'] for cs in self.__cross_sections]:
                    op_id_map[id_] = None
                else:
                    op_id_map[id_], = self.project.execute(f"""
                        insert into {self.model.name}.open_parametric_geometry(name, zbmin_array)
                        values(%s, %s) returning id""", (op['name'], op['zbmin_array'])).fetchone()
            elif op != self.__initial_channel_geom[id_]:
                self.project.execute(f"""
                    update {self.model.name}.open_parametric_geometry set
                    name=%s, zbmin_array=%s
                    where id=%s""", (op['name'], op['zbmin_array'], id_))

        # update cp
        cp_id_map = {}
        for id_, cp in self.__pipe_geom.items():
            if id_ not in self.__initial_pipe_geom:
                if id_ not in [cs['up_cp_geom'] for cs in self.__cross_sections] + [cs['down_cp_geom'] for cs in self.__cross_sections]:
                    cp_id_map[id_] = None
                else:
                    cp_id_map[id_], = self.project.execute(f"""
                        insert into {self.model.name}.closed_parametric_geometry(name, zbmin_array)
                        values(%s, %s) returning id""", (cp['name'], cp['zbmin_array'])).fetchone()
            elif cp != self.__initial_pipe_geom[id_]:
                self.project.execute(f"""
                    update {self.model.name}.closed_parametric_geometry set
                    name=%s, zbmin_array=%s
                    where id=%s""", (cp['name'], cp['zbmin_array'], id_))

        # update vcs
        vcs_id_map = {}
        for id_, vcs in self.__valley_geom.items():
            if id_ not in self.__initial_valley_geom:
                if id_ not in [cs['up_vcs_geom'] for cs in self.__cross_sections] + [cs['down_vcs_geom'] for cs in self.__cross_sections]:
                    vcs_id_map[id_] = None
                else:
                    vcs_id_map[id_], = self.project.execute(f"""
                        insert into {self.model.name}.valley_cross_section_geometry(
                            name, zbmin_array, zbmaj_lbank_array,
                            zbmaj_rbank_array, rlambda,
                            rmu1, rmu2, zlevee_lb,
                            zlevee_rb, transect, sz_array)
                        values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) returning id""", (
                            vcs['name'], vcs['zbmin_array'], vcs['zbmaj_lbank_array'],
                            vcs['zbmaj_rbank_array'], vcs['rlambda'],
                            vcs['rmu1'], vcs['rmu2'], vcs['zlevee_lb'],
                            vcs['zlevee_rb'], vcs['transect'],
                            vcs['sz_array'])).fetchone()
            elif vcs != self.__initial_valley_geom[id_]:
                self.project.execute(f"""
                    update {self.model.name}.valley_cross_section_geometry set
                    name=%s, zbmin_array=%s, zbmaj_lbank_array=%s,
                    zbmaj_rbank_array=%s, rlambda=%s,
                    rmu1=%s, rmu2=%s, zlevee_lb=%s,
                    zlevee_rb=%s, transect=%s, sz_array=%s
                    where id=%s""", (
                        vcs['name'], vcs['zbmin_array'], vcs['zbmaj_lbank_array'],
                        vcs['zbmaj_rbank_array'], vcs['rlambda'],
                        vcs['rmu1'], vcs['rmu2'], vcs['zlevee_lb'],
                        vcs['zlevee_rb'], vcs['transect'],
                        vcs['sz_array'],
                        id_))
        # TODO BE SMART HERE ! INSTEAD OF STUPID
        new_channel_geom = {target : self.__channel_geom[source] for source, target in op_id_map.items() if target is not None}
        new_pipe_geom = {target : self.__pipe_geom[source] for source, target in cp_id_map.items() if target is not None}
        new_valley_geom = {target : self.__valley_geom[source] for source, target in vcs_id_map.items() if target is not None}

        for source, target in op_id_map.items():
            del self.__channel_geom[source]

        for source, target in cp_id_map.items():
            del self.__pipe_geom[source]

        for source, target in vcs_id_map.items():
            del self.__valley_geom[source]

        self.__channel_geom.update(new_channel_geom)
        self.__pipe_geom.update(new_pipe_geom)
        self.__valley_geom.update(new_valley_geom)

        for i in range(len(self.__cross_sections)):
            cs = self.__cross_sections[i]
            for pos in ['up', 'down']:
                if cs[f'{pos}_op_geom'] in op_id_map:
                    cs[f'{pos}_op_geom'] = op_id_map[cs[f'{pos}_op_geom']]
                if cs[f'{pos}_cp_geom'] in cp_id_map:
                    cs[f'{pos}_cp_geom'] = cp_id_map[cs[f'{pos}_cp_geom']]
                if cs[f'{pos}_vcs_geom'] in vcs_id_map:
                    cs[f'{pos}_vcs_geom'] = vcs_id_map[cs[f'{pos}_vcs_geom']]

        self.__initial_channel_geom = copy.deepcopy(self.__channel_geom)
        self.__initial_pipe_geom = copy.deepcopy(self.__pipe_geom)
        self.__initial_valley_geom = copy.deepcopy(self.__valley_geom)

        for i in range(len(self.__cross_sections)):
            if self.__cross_sections[i] != self.__initial_cross_sections[i]:
                s = self.__cross_sections[i]
                fields = [k for k in s.keys() if k not in ('id', 'pk', 'geom', 'transect')]
                self.project.execute(f"""
                    update {self.model.name}.river_cross_section_profile set
                    {", ".join([f"{k }=%s" for k in fields])}
                    where id={s['id']}
                """, [s[k]  for k in fields])
        self.__initial_cross_sections = copy.deepcopy(self.__cross_sections)

        self.__change_widget()

        SimpleSingularityDialog.save(self)

    def __get_transects_by_name(self, name):
        for t in self.__transects:
            if t['name'] == name:
                return t

    def save_and_close(self):
        self.save()
        self.close()

    def close(self):
        # if there are unsaved changes, popup and save if needed
        unsaved = [i for i in range(len(self.__cross_sections))
            if self.__cross_sections[i] != self.__initial_cross_sections[i]]

        if len(unsaved):
            # TODO popup warning
            pass

        SimpleSingularityDialog.close(self)


if __name__ == '__main__':
    from qgis.core import QgsProject, QgsApplication, QgsMapRendererTask, QgsMapSettings, QgsRectangle, QgsMapRendererCustomPainterJob
    import sys
    from qgis.gui import QgsMapCanvas
    from qgis.PyQt.QtGui import QImage, QPainter, QColor, QPixmap
    from qgis.PyQt.QtCore import QSize
    from hydra.project import Project

    qgs = QgsApplication([], False)
    qgs.initQgis()

    project = Project.load_project(sys.argv[1])
    model = sys.argv[2]
    project.set_current_model(model)
    timer = Timer()
    print(project.qgs)
    if len(sys.argv) == 5:
        QgsProject.instance().read(project.qgs)
        print(timer.reset("read project"))
    id_, = project.execute(f"select id from {model}.river_cross_section_profile where name='{sys.argv[3]}'").fetchone()
    RiverCrossSectionProfileEditor(project, None, id=id_).exec()
