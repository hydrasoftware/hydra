# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
run test to check the writing of crgeng file

USAGE

   python -m hydra.gui.writer_crgeng_file_guitest [-hs]

OPTIONS

   -h, --help
        print this help

   -g  --gui project_name
        run in gui mode

"""
from qgis.PyQt.QtWidgets import QApplication
from qgis.PyQt.QtCore import QCoreApplication, QTranslator
from hydra.advanced_tools.writer_crgeng_file import WriterCrgeng
from hydra.utility.string import normalized_name, normalized_model_name
from hydra.project import Project

import sys
import os
import getopt

def gui_mode(project_name):
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)
    project = Project.load_project(project_name)
    test_dialog = WriterCrgeng(project)
    test_dialog.exec_()

try:
    optlist, args = getopt.getopt(sys.argv[1:],
                    "hg",
                    ["help" "gui"])
except Exception as e:
    sys.stderr.write(str(e) + "\n")
    exit(1)

optlist = dict(optlist)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    project_name = normalized_name(project_name)
    gui_mode(project_name)
    exit(0)
