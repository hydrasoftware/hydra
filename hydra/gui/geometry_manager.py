# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import hydra.utility.string as string

from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QFileDialog, QPushButton, QDialogButtonBox

from hydra.gui.base_dialog import BaseDialog, tr
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from .cross_section.geometry_widget import GeometryWidget
from .cross_section.valley_geometry import ValleyGeometry
from .cross_section.channel_geometry import ChannelGeometry
from .cross_section.pipe_geometry import PipeGeometry
from hydra.utility.string import list_to_sql_array
import copy

from shapely.geometry import LineString
from shapely import wkb
import shapely
if int(shapely.__version__.split('.')[0]) >=2:
    from shapely import get_srid
else:
    from shapely import geos
    def get_srid(geom):
        return geos.lgeos.GEOSGetSRID(geom._geom)

_desktop_dir = os.path.join(os.path.expanduser('~'), "Desktop")

class GeometryManager(BaseDialog):
    def __init__(self, project, parent=None, iface=None):
        BaseDialog.__init__(self, project, parent)
        self.hide()
        current_dir = os.path.dirname(__file__)
        self.project = project
        self.model = self.project.get_current_model()
        uic.loadUi(os.path.join(current_dir, "geometry_manager.ui"), self)

        self.buttonBox.rejected.connect(self.close)
        self.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(self.save_and_close)
        self.buttonBox.button(QDialogButtonBox.Save).clicked.connect(self.save)

        self.__iface = iface

        self.geom_widget = GeometryWidget(parent=self)
        self.geometry_layout.addWidget(self.geom_widget)
        self.geom_widget.geometry_changed.connect(self.geometry_changed)
        self.geom_widget.transect_changed.connect(self.transect_changed)

        sql = f"""
            with transect as (
                select c.id, c.name, c.geom, st_intersection(r.geom, c.geom) as inter, r.geom as reach_geom, r.name as reach_name, r.pk0_km,
                    st_collect(st_setsrid(st_makepoint(st_x(p.geom), st_y(p.geom), p.z_ground), {self.project.srid})) as points, c.topo
                from {self.model.name}.constrain c
                join {self.model.name}.reach r on st_intersects(r.geom, c.geom)
                left join project.points_xyz p on st_dwithin(c.geom, p.geom, c.points_xyz_proximity) and p.points_id = c.points_xyz
                where st_intersects(r.geom, c.geom)
                and constrain_type in ('flood_plain_transect', 'ignored_for_coverages')
                group by c.id, c.name, c.geom, r.geom, r.name, r.pk0_km, c.topo
            )
            select t.id, t.name, t.geom, st_linelocatepoint(reach_geom, t.inter)*st_length(reach_geom)/1000 + pk0_km as pk, inter, points, topo
            from transect t
            order by pk asc
            """

        self.__transects = {id_: {'name': name, 'geom': geom, 'pk': pk, 'inter': inter, 'points': points, 'topo': topo}
            for id_, name, geom, pk, inter, points, topo in self.project.execute(sql).fetchall()}
        self.__initial_transects = copy.deepcopy(self.__transects)

        #valley parametric geometries init :
        self.__valley_geom = {id_:{"name": name, "zbmin_array": zbmin_array, 'zbmaj_lbank_array': zbmaj_lbank_array,
                'zbmaj_rbank_array': zbmaj_rbank_array, 'rlambda': rlambda, 'rmu1': rmu1, 'rmu2': rmu2,
                'zlevee_lb': zlevee_lb, 'zlevee_rb': zlevee_rb, 'transect': transect, 'sz_array': sz_array}
            for id_, name, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, rlambda, rmu1, rmu2, zlevee_lb, zlevee_rb, transect, sz_array
                in project.execute(f"""
                select id, name, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, rlambda, rmu1, rmu2, zlevee_lb, zlevee_rb, transect, sz_array
                from {self.model.name}.valley_cross_section_geometry""").fetchall()}
        self.__initial_valley_geom = copy.deepcopy(self.__valley_geom)

        self.table_vcs_geom = HydraTableWidget(self.project, ("id", "name"),(tr("Id"), tr("Name")),
                                self.model.name, "valley_cross_section_geometry",
                                (self.new_vcs_geom, self.delete_vcs_geom), "", "id")
        self.active_vcs_geom_id = None
        self.table_vcs_geom.set_editable(True)
        self.table_vcs_geom.table.setSortingEnabled(True)
        self.table_vcs_geom.table.selectRow(0)
        self.table_vcs_geom.data_edited.connect(self.name_changed)

        btn_duplicate_vcs = QPushButton("Duplicate geometry")
        btn_duplicate_vcs.clicked.connect(self.duplicate_current_geom)
        self.valley_layout.addWidget(self.table_vcs_geom)
        self.valley_layout.addWidget(btn_duplicate_vcs)

        #closed parametric geometries init :
        self.__pipe_geom = {id_:{"name": name, "zbmin_array": zbmin_array}
            for id_, name, zbmin_array in project.execute(f"select id, name, zbmin_array from {self.model.name}.closed_parametric_geometry").fetchall()}
        self.__initial_pipe_geom = copy.deepcopy(self.__pipe_geom)

        self.table_cp_geom = HydraTableWidget(self.project, ("id", "name"),(tr("Id"), tr("Name")),
                                self.model.name, "closed_parametric_geometry",
                                (self.new_cp_geom, self.delete_cp_geom), "", "id")
        self.active_cp_geom_id = None
        self.table_cp_geom.table.setSortingEnabled(True)
        self.table_cp_geom.table.selectRow(0)
        self.table_cp_geom.set_editable(True)
        self.table_cp_geom.data_edited.connect(self.name_changed)

        btn_import_pipe = QPushButton("Import from .csv")
        btn_import_pipe.clicked.connect(lambda: self.__import_from_csv("closed"))
        self.pipe_layout.addWidget(self.table_cp_geom)
        self.pipe_layout.addWidget(btn_import_pipe)


        #open parametric geometries init :
        self.__channel_geom = {id_:{
            "name": name,
            "zbmin_array": zbmin_array,
            "with_flood_plain": with_flood_plain,
            "flood_plain_width": flood_plain_width,
            "flood_plain_lateral_slope": flood_plain_lateral_slope}
            for id_, name, zbmin_array, with_flood_plain, flood_plain_width, flood_plain_lateral_slope
            in project.execute(f"select id, name, zbmin_array, with_flood_plain, flood_plain_width, flood_plain_lateral_slope from {self.model.name}.open_parametric_geometry").fetchall()}
        self.__initial_channel_geom = copy.deepcopy(self.__channel_geom)

        self.table_op_geom = HydraTableWidget(self.project, ("id", "name"),(tr("Id"), tr("Name")),
                                self.model.name, "open_parametric_geometry",
                                (self.new_op_geom, self.delete_op_geom), "", "id")
        self.active_op_geom_id = None
        self.table_op_geom.table.setSortingEnabled(True)
        self.table_op_geom.table.selectRow(0)
        self.table_op_geom.set_editable(True)
        self.table_op_geom.data_edited.connect(self.name_changed)

        btn_import_channel = QPushButton("Import from .csv")
        btn_import_channel.clicked.connect(lambda: self.__import_from_csv("open"))
        self.channel_layout.addWidget(self.table_op_geom)
        self.channel_layout.addWidget(btn_import_channel)

        self.geom_tab.currentChanged.connect(self.tab_changed)
        self.table_op_geom.table.itemSelectionChanged.connect(self.set_active_op_geom)
        self.table_cp_geom.table.itemSelectionChanged.connect(self.set_active_cp_geom)
        self.table_vcs_geom.table.itemSelectionChanged.connect(self.set_active_vcs_geom)
        self.set_active_vcs_geom()

        self.show()

    def tab_changed(self, i):
        if i == 0:
            self.set_active_cp_geom()
        elif i == 1:
            self.set_active_op_geom()
        elif i == 2:
            self.set_active_vcs_geom()

    def duplicate_current_geom(self):
        if self.active_vcs_geom_id is not None:
            cg = self.__valley_geom[self.active_vcs_geom_id]
            zbmin_array= cg['zbmin_array']
            zbmaj_lbank_array= cg['zbmaj_lbank_array']
            zbmaj_rbank_array= cg['zbmaj_rbank_array']
        self.table_vcs_geom.add_row(['default', string.list_to_sql_array(zbmin_array),
                                        string.list_to_sql_array(zbmaj_lbank_array), string.list_to_sql_array(zbmaj_rbank_array)],
                                        ['zbmin_array','zbmaj_lbank_array','zbmaj_rbank_array'])
        last_row = self.table_vcs_geom.rowCount-1
        self.active_vcs_geom_id = int(self.table_vcs_geom.table.item(last_row,0).text())
        self.table_vcs_geom.table.selectRow(last_row)
        selected_row = self.table_vcs_geom.table.row(self.table_vcs_geom.table.selectedItems()[0])
        self.__valley_geom[self.active_vcs_geom_id] = {'name': self.table_vcs_geom.table.item(selected_row, 1).text(),
                    'zbmin_array': zbmin_array, 'zbmaj_lbank_array': zbmaj_lbank_array, 'zbmaj_rbank_array': zbmaj_rbank_array,
                    'rlambda': cg['rlambda'], 'rmu1': cg['rmu1'], 'rmu2': cg['rmu2'],
                    'zlevee_lb': cg['zlevee_lb'], 'zlevee_rb': cg['zlevee_rb'], 'transect':  cg['transect'], 'sz_array': cg['sz_array']}
        self.geom_widget.set_geometry(ValleyGeometry(cg['zbmin_array'], cg['zbmaj_lbank_array'], cg['zbmaj_rbank_array'],
                cg['rlambda'], cg['rmu1'], cg['rmu2'], cg['zlevee_lb'], cg['zlevee_rb'], cg['sz_array'], cg['transect'], self.__transects, self.project.terrain))

    def geometry_changed(self, param):
        if self.active_cp_geom_id is not None:
            self.__pipe_geom[self.active_cp_geom_id].update(param)
        elif self.active_op_geom_id is not None:
            self.__channel_geom[self.active_op_geom_id].update(param)
        elif self.active_vcs_geom_id is not None:
            self.__valley_geom[self.active_vcs_geom_id].update(param)

    def transect_changed(self, ll):
        assert(self.active_vcs_geom_id is not None)
        t_id = self.__valley_geom[self.active_vcs_geom_id]['transect']
        if t_id is not None:
            t = self.__transects[t_id]
            geom = wkb.loads(t['geom'], hex=True)
            srid = get_srid(geom)
            t['geom'] = wkb.dumps(LineString(ll), hex=True, srid=srid)

    def name_changed(self):
        for i in range(self.table_cp_geom.table.rowCount()):
            self.__pipe_geom[int(self.table_cp_geom.table.item(i,0).text())]['name'] = self.table_cp_geom.table.item(i,1).text()
        for i in range(self.table_op_geom.table.rowCount()):
            self.__channel_geom[int(self.table_op_geom.table.item(i,0).text())]['name'] = self.table_op_geom.table.item(i,1).text()
        for i in range(self.table_vcs_geom.table.rowCount()):
            self.__valley_geom[int(self.table_vcs_geom.table.item(i,0).text())]['name'] = self.table_vcs_geom.table.item(i,1).text()

    def new_cp_geom(self):
        zbmin_array=[[0,0]]
        self.table_cp_geom.add_row(['default', string.list_to_sql_array(zbmin_array)], ['zbmin_array'])
        self.set_active_cp_geom()

    def delete_cp_geom(self):
        if self.active_cp_geom_id is not None:
            self.table_cp_geom.del_selected_row()
            self.set_active_cp_geom()

    def set_active_cp_geom(self):
        self.active_op_geom_id = None
        self.active_vcs_geom_id = None
        items = self.table_cp_geom.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_cp_geom.table.row(items[0])
            self.active_cp_geom_id = int(self.table_cp_geom.table.item(selected_row,0).text())
            if self.active_cp_geom_id not in self.__pipe_geom:
                self.__pipe_geom[self.active_cp_geom_id] = {'name': self.table_cp_geom.table.item(selected_row, 1).text(), 'zbmin_array': [[0,0]]}
            self.geom_widget.set_geometry(PipeGeometry(self.__pipe_geom[self.active_cp_geom_id]['zbmin_array']))
        else:
            self.active_cp_geom_id=None
            self.geom_widget.set_geometry(None)

    # open parametric geometry piloting
    def new_op_geom(self):
        zbmin_array=[[0,0]]
        self.table_op_geom.add_row(['default', string.list_to_sql_array(zbmin_array)], ['zbmin_array'])
        self.set_active_op_geom()

    def delete_op_geom(self):
        if self.active_op_geom_id is not None:
            self.table_op_geom.del_selected_row()
            self.set_active_op_geom()

    def set_active_op_geom(self):
        self.active_cp_geom_id = None
        self.active_vcs_geom_id = None
        items = self.table_op_geom.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_op_geom.table.row(items[0])
            self.active_op_geom_id = int(self.table_op_geom.table.item(selected_row,0).text())
            if self.active_op_geom_id not in self.__channel_geom:
                self.__channel_geom[self.active_op_geom_id] = {
                    'name': self.table_op_geom.table.item(selected_row, 1).text(),
                    'zbmin_array': [[0,0]],
                    'with_flood_plain': False,
                    'flood_plain_width': 0,
                    'flood_plain_lateral_slope': 0}
            self.geom_widget.set_geometry(ChannelGeometry(self.__channel_geom[self.active_op_geom_id]['zbmin_array'],
                with_flood_plain=self.__channel_geom[self.active_op_geom_id]['with_flood_plain'],
                flood_plain_width=self.__channel_geom[self.active_op_geom_id]['flood_plain_width'],
                flood_plain_lateral_slope=self.__channel_geom[self.active_op_geom_id]['flood_plain_lateral_slope']
                ))
        else:
            self.active_op_geom_id=None
            self.geom_widget.set_geometry(None)

    # valley cross section geometry piloting
    def new_vcs_geom(self):
        zbmin_array=[[0,0]]
        zbmaj_lbank_array=[[0,0]]
        zbmaj_rbank_array=[[0,0]]
        self.table_vcs_geom.add_row(['default', string.list_to_sql_array(zbmin_array),
                                        string.list_to_sql_array(zbmaj_lbank_array), string.list_to_sql_array(zbmaj_rbank_array)],
                                        ['zbmin_array','zbmaj_lbank_array','zbmaj_rbank_array'])
        self.set_active_vcs_geom()

    def delete_vcs_geom(self):
        if self.active_vcs_geom_id is not None:
            self.table_vcs_geom.del_selected_row()
            self.set_active_vcs_geom()

    def set_active_vcs_geom(self):
        self.active_cp_geom_id = None
        self.active_op_geom_id = None
        if len(self.table_vcs_geom.table.selectedItems())>0:
            selected_row = self.table_vcs_geom.table.row(self.table_vcs_geom.table.selectedItems()[0])
            self.active_vcs_geom_id = int(self.table_vcs_geom.table.item(selected_row,0).text())
            if self.active_vcs_geom_id not in self.__valley_geom:
                self.__valley_geom[self.active_vcs_geom_id] = {'name': self.table_vcs_geom.table.item(selected_row, 1).text(),
                    'zbmin_array': [[0,0]], 'zbmaj_lbank_array': [[0,0]], 'zbmaj_rbank_array': [[0,0]],
                    'rlambda': None, 'rmu1': None, 'rmu2': None,
                    'zlevee_lb': None, 'zlevee_rb': None, 'transect': None, 'sz_array': None}
            cg = self.__valley_geom[self.active_vcs_geom_id]
            self.geom_widget.set_geometry(ValleyGeometry(cg['zbmin_array'], cg['zbmaj_lbank_array'], cg['zbmaj_rbank_array'],
                cg['rlambda'], cg['rmu1'], cg['rmu2'], cg['zlevee_lb'], cg['zlevee_rb'], cg['sz_array'], cg['transect'], self.__transects, self.project.terrain))
        else:
            self.active_vcs_geom_id=None
            self.geom_widget.set_geometry(None)

    # all
    # def get_table_items(self, array, formated = True):
    #     n = array.rowCount()
    #     result = list()
    #     index_list=0
    #     for i in range(0, n):
    #         if not array.item(i,0) == None and not array.item(i,1) == None:
    #             result.append(list())
    #             m = array.columnCount()
    #             result[index_list].append(string.get_sql_float(array.item(i,0).text()))
    #             result[index_list].append(string.get_sql_float(array.item(i,1).text()))
    #             index_list = index_list + 1
    #     if not formated:
    #         return result
    #     return string.list_to_sql_array(result)

    def __import_from_csv(self, section_type):
        assert section_type in ["open", "closed"]
        title = tr('Import sections from csv file')
        filter = tr('(*.csv)')
        filepath, __ = QFileDialog.getOpenFileName(self, title, _desktop_dir, filter)
        if filepath and os.path.isfile(filepath):
            self.import_csv_file(filepath, section_type)

    def import_csv_file(self, filepath, section_type):
        with open(filepath) as f:
            hb = []
            name = None
            for ll in f:
                r = ll.strip().split(';')
                if len(r) == 1 or (len(r)==2 and r[1].strip()==""):
                    if len(hb) and name:
                        id_, = self.project.execute("""
                            insert into {model}.{section_type}_parametric_geometry (zbmin_array, name)
                            values ('{tab}', '{name}') returning id""".format(
                            model=self.model.name, section_type=section_type, tab=list_to_sql_array(hb), name=name)).fetchone()
                        if section_type == "closed":
                            self.__pipe_geom[id_] = {'name': name, 'zbmin_array': hb}
                        if section_type == "open":
                            self.__channel_geom[id_] = {'name': name, 'zbmin_array': hb, 'with_flood_plain': False, 'flood_plain_width': 0, 'flood_plain_lateral_slope': 0}
 
                    name = r[0]
                    hb = []
                elif len(r) == 2:
                    hb.append([float(r[0]), float(r[1])])
 
            if len(hb) and name:
                id_, = self.project.execute("""
                    insert into {model}.{section_type}_parametric_geometry (zbmin_array, name)
                    values ('{tab}'::real[], '{name}') returning id""".format(
                    model=self.model.name, section_type=section_type, tab=list_to_sql_array(hb), name=name)).fetchone()
                if section_type == "closed":
                    self.__pipe_geom[id_] = {'name': name, 'zbmin_array': hb}
                if section_type == "open":
                    self.__channel_geom[id_] = {'name': name, 'zbmin_array': hb, 'with_flood_plain': False, 'flood_plain_width': 0, 'flood_plain_lateral_slope': 0}
 
        if section_type == "closed":
            self.table_cp_geom.update_data()
        if section_type == "open":
            self.table_op_geom.update_data()

    def save(self):
        # update transects
        for id_, t in self.__transects.items():
            if t != self.__initial_transects[id_]:
                self.project.execute(f"""
                    update {self.model.name}.constrain set geom=%s, name=%s where id=%s
                    returning name""", (t['geom'], t['name'], id_))
        self.__initial_transects = copy.deepcopy(self.__transects)

        # update op
        for id_, op in self.__channel_geom.items():
            if id_ not in self.__initial_channel_geom or op != self.__initial_channel_geom[id_]:
                self.project.execute(f"""
                    update {self.model.name}.open_parametric_geometry set
                    name=%s, zbmin_array=%s,
                    with_flood_plain=%s, flood_plain_width=%s, flood_plain_lateral_slope=%s
                    where id=%s""", (
                        op['name'], op['zbmin_array'],
                        op['with_flood_plain'], op['flood_plain_width'], op['flood_plain_lateral_slope'],
                        id_))
        self.__initial_channel_geom = copy.deepcopy(self.__channel_geom)

        # update cp
        for id_, cp in self.__pipe_geom.items():
            if id_ not in self.__initial_pipe_geom or cp != self.__initial_pipe_geom[id_]:
                self.project.execute(f"""
                    update {self.model.name}.closed_parametric_geometry set
                    name=%s, zbmin_array=%s
                    where id=%s""", (cp['name'], cp['zbmin_array'], id_))
        self.__initial_pipe_geom = copy.deepcopy(self.__pipe_geom)

        # update vcs
        for id_, vcs in self.__valley_geom.items():
            if id_ not in self.__initial_valley_geom or vcs != self.__initial_valley_geom[id_]:
                self.project.execute(f"""
                    update {self.model.name}.valley_cross_section_geometry set
                    name=%s,
                    zbmin_array=%s,
                    zbmaj_lbank_array=%s,
                    zbmaj_rbank_array=%s,
                    rlambda=%s,
                    rmu1=%s,
                    rmu2=%s,
                    zlevee_lb=%s,
                    zlevee_rb=%s,
                    transect=%s,
                    sz_array=%s
                    where id=%s""", (
                        vcs['name'],
                        vcs['zbmin_array'],
                        vcs['zbmaj_lbank_array'],
                        vcs['zbmaj_rbank_array'],
                        vcs['rlambda'],
                        vcs['rmu1'],
                        vcs['rmu2'],
                        vcs['zlevee_lb'],
                        vcs['zlevee_rb'],
                        vcs['transect'],
                        vcs['sz_array'],
                        id_))
        self.__initial_valley_geom = copy.deepcopy(self.__valley_geom)

        BaseDialog.save(self)

    def save_and_close(self):
        self.save()
        self.close()

if __name__ == '__main__':
    from qgis.core import QgsApplication
    import sys
    from hydra.project import Project

    qgs = QgsApplication([], False)
    qgs.initQgis()

    project = Project.load_project(sys.argv[1])
    model = sys.argv[2]
    project.set_current_model(model)
    d = GeometryManager(project)
    d.exec_()
