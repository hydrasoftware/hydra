# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.forms.jet_fan_guitest [-dh] [-g project_name model_name x y]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name model_name x y

"""

from hydra.gui.forms.jet_fan_link import JetFanLinkEditor
from hydra.utility.tables_properties import TablesProperties

from qgis.PyQt.QtWidgets import QApplication
from qgis.PyQt.QtCore import QCoreApplication, QTranslator
from hydra.database.database import TestProject, remove_project, project_exists
import sys
import getopt

from hydra.project import Project

app = QApplication(sys.argv)
translator = QTranslator()
translator.load("i18n/fr", "../..")
QCoreApplication.installTranslator(translator)
guitranslator = QTranslator()
guitranslator.load("/usr/share/qt4/translations/qt_fr")
QCoreApplication.installTranslator(guitranslator)

project_name = "gui_test"
model_name = "model"

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])

except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name)
    obj_project = Project.load_project(project_name)
    obj_project.add_new_model(model_name)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    model_name = args[1]
    geom = [float(a) for a in args[2:]]
    id =geom[2] if len(geom)==3 else None
    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)
    test_dialog = JetFanLinkEditor(obj_project, geom)
    test_dialog.exec_()
    exit(0)

obj_project = Project.load_project(project_name)
obj_project.set_current_model(model_name)
model = obj_project.get_current_model()

model.add_station([[[2000,5000],[3000,5000],[3000,6000],[2000,6000],[2000,5000]]])
model.add_station_node([2169, 5169])
model.add_station_node([2369, 5169])

test_dialog = JetFanLinkEditor(obj_project, [[2169, 5169],[2369, 5169]])
test_dialog.unit_thrust_newton.setText('10.5')
test_dialog.number_of_units.setValue(3)
test_dialog.efficiency.setText('.55')
test_dialog.flow_velocity.setText('44')
test_dialog.pipe_area.setText('33.5')
test_dialog.is_up_to_down.setChecked(False)
id_ = test_dialog.get_id()
test_dialog.save()

test_dialog = JetFanLinkEditor(obj_project, None, id_)
assert(float(test_dialog.unit_thrust_newton.text())==10.5)
assert(test_dialog.number_of_units.value() == 3)
assert(float(test_dialog.efficiency.text()) == .55)
assert(float(test_dialog.flow_velocity.text()) == 44.)
assert(float(test_dialog.pipe_area.text()) == 33.5)
assert(test_dialog.is_up_to_down.isChecked() == False)

test_dialog.reject()

print("ok")


