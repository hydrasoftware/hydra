# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..base_dialog import BaseDialog, tr
from qgis.PyQt import uic
import os

class InComboSelector(BaseDialog):
    def __init__(self, project, label, title, combo_texts_ids, default_value=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "select_in_combo.ui"), self)

        self.__combo_values_ids = combo_texts_ids
        self.setWindowTitle(title)
        self.label.setText(label)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.__is_cancelled = True
        for i in range(0,len(combo_texts_ids)):
            self.combo.addItem(str(combo_texts_ids[i][0]))
            if default_value is not None and default_value==combo_texts_ids[i][1]:
                self.combo.setCurrentIndex(i)
        if default_value is None:
            self.combo.setCurrentIndex(-1)

    def save(self):
        self.__is_cancelled = False
        BaseDialog.save(self)
        self.close()

    def is_cancelled(self):
        return self.__is_cancelled;

    def get_selected_id(self):
        if self.combo.currentIndex() != -1:
            return self.__combo_values_ids[self.combo.currentIndex()][1]
        else:
            return None

    def get_selected_text(self):
        if self.combo.currentIndex() != -1:
            return self.__combo_values_ids[self.combo.currentIndex()][0]
        else:
            return None

