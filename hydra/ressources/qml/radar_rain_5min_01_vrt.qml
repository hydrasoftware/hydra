<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="1e+08" hasScaleBasedVisibilityFlag="0" maxScale="0" styleCategories="AllStyleCategories" version="3.22.7-Białowieża">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal enabled="0" mode="0" fetchMode="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <Option type="Map">
      <Option name="WMSBackgroundLayer" type="bool" value="false"/>
      <Option name="WMSPublishDataSourceUrl" type="bool" value="false"/>
      <Option name="embeddedWidgets/count" type="int" value="0"/>
      <Option name="identify/format" type="QString" value="Value"/>
    </Option>
  </customproperties>
  <pipe-data-defined-properties>
    <Option type="Map">
      <Option name="name" type="QString" value=""/>
      <Option name="properties"/>
      <Option name="type" type="QString" value="collection"/>
    </Option>
  </pipe-data-defined-properties>
  <pipe>
    <provider>
      <resampling zoomedOutResamplingMethod="nearestNeighbour" enabled="false" maxOversampling="2" zoomedInResamplingMethod="nearestNeighbour"/>
    </provider>
    <rasterrenderer band="1" alphaBand="-1" classificationMin="0" nodataColor="" classificationMax="833" type="singlebandpseudocolor" opacity="1">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Exact</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
        <colorrampshader classificationMode="1" colorRampType="INTERPOLATED" labelPrecision="6" maximumValue="833" clip="0" minimumValue="0">
          <colorramp name="[source]" type="gradient">
            <Option type="Map">
              <Option name="color1" type="QString" value="255,0,255,0"/>
              <Option name="color2" type="QString" value="241,130,44,128"/>
              <Option name="discrete" type="QString" value="0"/>
              <Option name="rampType" type="QString" value="gradient"/>
              <Option name="stops" type="QString" value="0.0144058;99,166,221,128:0.0204082;72,124,188,128:0.030012;70,92,131,128:0.0696279;59,69,97,128:0.0996399;150,71,162,128:0.20048;211,41,149,128:0.30012;248,162,164,128:0.69988;233,240,32,128"/>
            </Option>
            <prop v="255,0,255,0" k="color1"/>
            <prop v="241,130,44,128" k="color2"/>
            <prop v="0" k="discrete"/>
            <prop v="gradient" k="rampType"/>
            <prop v="0.0144058;99,166,221,128:0.0204082;72,124,188,128:0.030012;70,92,131,128:0.0696279;59,69,97,128:0.0996399;150,71,162,128:0.20048;211,41,149,128:0.30012;248,162,164,128:0.69988;233,240,32,128" k="stops"/>
          </colorramp>
          <item label="0 mm/h" alpha="0" value="0" color="#ff00ff"/>
          <item label="1.5 mm/h" alpha="128" value="12" color="#63a6dd"/>
          <item label="2 mm/h" alpha="128" value="17" color="#487cbc"/>
          <item label="3 mm/h" alpha="128" value="25" color="#465c83"/>
          <item label="7 mm/h" alpha="128" value="58" color="#3b4561"/>
          <item label="10 mm/h" alpha="128" value="83" color="#9647a2"/>
          <item label="20 mm/h" alpha="128" value="167" color="#d32995"/>
          <item label="30 mm/h" alpha="128" value="250" color="#f8a2a4"/>
          <item label="70 mm/h" alpha="128" value="583" color="#e9f020"/>
          <item label="100 mm/h" alpha="128" value="833" color="#f1822c"/>
          <rampLegendSettings useContinuousLegend="1" direction="0" minimumLabel="" suffix="" prefix="" orientation="2" maximumLabel="">
            <numericFormat id="basic">
              <Option type="Map">
                <Option name="decimal_separator" type="QChar" value=""/>
                <Option name="decimals" type="int" value="6"/>
                <Option name="rounding_type" type="int" value="0"/>
                <Option name="show_plus" type="bool" value="false"/>
                <Option name="show_thousand_separator" type="bool" value="true"/>
                <Option name="show_trailing_zeros" type="bool" value="false"/>
                <Option name="thousand_separator" type="QChar" value=""/>
              </Option>
            </numericFormat>
          </rampLegendSettings>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0" gamma="1"/>
    <huesaturation invertColors="0" colorizeOn="0" colorizeRed="255" saturation="0" grayscaleMode="0" colorizeGreen="128" colorizeBlue="128" colorizeStrength="100"/>
    <rasterresampler maxOversampling="2"/>
    <resamplingStage>resamplingFilter</resamplingStage>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
