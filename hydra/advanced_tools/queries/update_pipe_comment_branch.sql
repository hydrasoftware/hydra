/* ************************************************************************************* */
/* Update all pipes set comment to branch name                                           */
/* ************************************************************************************* */

update $model.pipe_link p
set comment = (select b.name from $model.branch as b where p.branch=b.id)
;;