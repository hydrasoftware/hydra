/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*       Abstract entities: CROSS-SECTION         */
/* ********************************************** */

create table $model._river_cross_section_profile(
    id integer unique not null references $model._river_node(id),
    name varchar(24) unique not null,
    z_invert_up real,
    z_invert_down real,
    type_cross_section_up hydra_cross_section_type default 'valley',
    type_cross_section_down hydra_cross_section_type default 'valley',
    up_rk real,
    up_rk_maj real,
    up_sinuosity real,
    up_circular_diameter real,
    up_ovoid_height real,
    up_ovoid_top_diameter real,
    up_ovoid_invert_diameter real,
    up_cp_geom integer references $model.closed_parametric_geometry(id) on delete set null on update cascade,
    up_op_geom integer references $model.open_parametric_geometry(id) on delete set null on update cascade,
    up_vcs_geom integer references $model.valley_cross_section_geometry(id) on delete set null on update cascade,
    up_vcs_topo_geom integer references $model.valley_cross_section_topo_geometry(id) on delete set null on update cascade,
    down_rk real,
    down_rk_maj real,
    down_sinuosity real,
    down_circular_diameter real,
    down_ovoid_height real,
    down_ovoid_top_diameter real,
    down_ovoid_invert_diameter real,
    down_cp_geom integer references $model.closed_parametric_geometry(id) on delete set null on update cascade,
    down_op_geom integer references $model.open_parametric_geometry(id) on delete set null on update cascade,
    down_vcs_geom integer references $model.valley_cross_section_geometry(id) on delete set null on update cascade,
    down_vcs_topo_geom integer references $model.valley_cross_section_topo_geometry(id) on delete set null on update cascade,
    configuration json,
    validity bool,
    comment varchar
)
;;

/* ********************************************** */
/*                     PL1D                       */
/* ********************************************** */

create table $model._river_cross_section_pl1d(
    id integer unique not null references $model._river_node(id),
    name varchar(24) unique not null,
    profile geometry('LINESTRINGZ',$srid) not null check(ST_NPoints(profile)=4 and ST_IsValid(profile)),
    generated integer references $model.generation_step(id) on delete cascade
)
;;

