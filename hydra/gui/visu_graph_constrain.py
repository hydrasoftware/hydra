from builtins import str
from builtins import range
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import io
import os
import numpy as np
from qgis.PyQt.QtCore import QCoreApplication, Qt
from qgis.PyQt.QtWidgets import QDialog, QTreeWidgetItem, QWidget, QVBoxLayout, QApplication
from qgis.PyQt.QtGui import QImage
from qgis.PyQt import uic
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from hydra.utility.result_decoder import HydraResult
from hydra.gui.visu_graph_line import Line


def tr(msg):
    return QCoreApplication.translate("Hydra", msg)


files_dir = {
    "w15": "hydraulique",
    "w14": "hydraulique",
    "w13": "hydrol"
}


titles = {
    "qam": 'Flow(m3/s)',
    "zam": 'Level(m)'
}

dico_color = {
    "porous": "#e31a1c",
    "overflow": "#009e00",
    "strickler": "#ff7f00",
    "connector": "#000000",
    "mesh_2d": "#ff0000",
    "network_overflow": "#1c5717",
    "crossroad_node": "#d569ff",
    "river_node": "#0000ff",
    "total": "#f60eef"
}


class VisuGraphConstrain(QDialog):

    class HydraResultLine(Line):
        ''' line for result of hydra'''
        def __init__(self, name, line_name, data, color, data_index, style, table=None, direction=1):
            assert direction == 1 or direction == -1
            Line.__init__(self, [elem[0] for elem in data], [elem[1][data_index] * direction for elem in data], line_name, color=color)
            self.style = style

    def __init__(self, current_project, name_item, table_item, name_scn, parent=None):
        QDialog.__init__(self, parent)
        self.__isinit = True
        uic.loadUi(os.path.join(os.path.dirname(__file__), "visu_graph_constrain.ui"), self)

        self.project = current_project
        self.constrain_name = name_item
        self.table = table_item
        self.name_scn = name_scn
        self.list_lines = []

        self.graph_title.textEdited.connect(self.__refresh_graph_text)
        self.left_axis_title.textEdited.connect(self.__refresh_graph_text)

        self.chk_disp_links.clicked.connect(self.load_graph)
        self.chk_all.clicked.connect(self.__checkall)
        self.chk_reverse.stateChanged.connect(self.__reverse_direction_links)
        self.btn_copy_image.clicked.connect(self.__copy_image)
        self.btn_copy_data.clicked.connect(self.__copy_data)

        self.graph_title.setText(self.name_scn + " " + self.constrain_name)
        self.left_axis_title.setText("Q (m3)")

        self.check_cross_reach_or_street()
        self.init_tree()
        self.__create_main_frame()

        self.show()
        self.__isinit = False

    def check_cross_reach_or_street(self):
        self.node_list = []

        list_reach = self.project.execute("""select r.id, St_AsText( St_intersection(r.geom, c.geom)) as geomtext
        from {model}.reach as r, {model}.constrain as c
        where St_intersects(r.geom, c.geom)
        and c.name = '{constrain_name}'""".format(model=self.project.get_current_model().name,
        constrain_name=self.constrain_name)).fetchall()

        list_street = self.project.execute("""select s.id, St_AsText( St_intersection(s.geom, c.geom)) as geomtext
        from {model}.street as s, {model}.constrain as c
        where St_intersects(s.geom, c.geom)
        and c.name = '{constrain_name}'""".format(model=self.project.get_current_model().name,
        constrain_name=self.constrain_name)).fetchall()

        for intersect in list_street:
            get_street_point = self.project.execute("""select cn.id, cn.geom, cn.name, cn.generated, St_distance(ST_GeomFromText('{geomtext}', {srid}), cn.geom) as dist
            from {model}.crossroad_node as cn, {model}.street as s
            where St_intersects(cn.geom, st_buffer(s.geom,0.01))
            and s.id = {id_intersect}
            and St_linelocatePoint(s.geom, St_closestPoint(s.geom, cn.geom)) > St_linelocatePoint(s.geom, ST_GeomFromText('{geomtext}', {srid}))
            order by dist
            limit 1""".format(model=self.project.get_current_model().name, id_intersect=intersect[0], geomtext=intersect[1], srid=self.project.srid)).fetchone()

            # [id , type, name, generated]

            self.node_list.append([get_street_point[0], "crossroad_node", get_street_point[2], get_street_point[3]])

        for intersect in list_reach:
            get_reach_point = self.project.execute("""select rn.id, rn.geom, rn.name, rn.generated, St_distance(ST_GeomFromText('{geomtext}', {srid}), rn.geom) as dist
            from {model}.river_node as rn, {model}.reach as r
            where St_intersects(rn.geom, st_buffer(r.geom,0.01))
            and r.id = {id_intersect}
            and St_linelocatePoint(r.geom, St_closestPoint(r.geom, rn.geom)) > St_linelocatePoint(r.geom, ST_GeomFromText('{geomtext}', {srid}))
            order by dist
            limit 1""".format(model=self.project.get_current_model().name, id_intersect=intersect[0], geomtext=intersect[1], srid=self.project.srid)).fetchone()

            # [id , type, name, generated]

            self.node_list.append([get_reach_point[0], "river_node", get_reach_point[2], get_reach_point[3]])

    def init_tree(self):
        ''' create and format the tree of links in the right panel to handle the graph'''
        self.constrain_tree.setHeaderLabels(["id", "name"])

        self.links_list = self.project.execute("""select l.id, l.link_type, l.name, l.generated
        from {model}._link as l, {model}._constrain as c
        where St_intersects(l.geom,c.geom)
        and c.name='{constrain_name}'""".format(model=self.project.get_current_model().name, constrain_name=self.constrain_name)).fetchall()

        self.dict_data = {'generated': {},
                          'not_generated': {}
                          }

        if self.links_list:
            self.name_for_init = str(self.links_list[0][2])
        elif self.node_list:
            self.name_for_init = str(self.node_list[0][2].split(':')[0])

        self.constrain_tree.itemChanged.connect(lambda x: self.__check_state(x))

        # add links in the Qtreewidget (overflow, mesh_2d, ...)
        # add node to the dict_data (Q of node in downstream)
        for links in self.links_list + self.node_list:
            if links[3] is not None:
                if links[1] in self.dict_data['generated']:
                    self.dict_data['generated'][links[1]][links[0]] = links[2]
                else:
                    self.dict_data['generated'][links[1]] = {}
                    self.dict_data['generated'][links[1]][links[0]] = links[2]
            else:
                if links[1] in self.dict_data['not_generated']:
                    self.dict_data['not_generated'][links[1]][links[0]] = links[2]
                else:
                    self.dict_data['not_generated'][links[1]] = {}
                    self.dict_data['not_generated'][links[1]][links[0]] = links[2]

        # create branch of tree "generated" if there is generated links
        # and "not_generated" if there is manually generated links across the constrain.
        generation = []
        if self.dict_data["generated"]:
            generation.append('generated')
            generated = QTreeWidgetItem(self.constrain_tree)
            generated.setText(0, "Automatic links")
            generated.setCheckState(0, False)
        if self.dict_data["not_generated"]:
            generation.append('not_generated')
            not_generated = QTreeWidgetItem(self.constrain_tree)
            not_generated.setText(0, "Manual links")
            not_generated.setCheckState(0, False)

        # generate branch for each link_type in the data dict
        # and then add the tree branch in its "generated" or "manual" branch.
        for gen in generation:
            for link_type in self.dict_data[gen]:
                node_type = QTreeWidgetItem()
                node_type.setText(0, self.__text_to_display(link_type))
                node_type.setCheckState(0, False)
                for links in self.dict_data[gen][link_type]:
                    link_node = QTreeWidgetItem()
                    link_node.setText(0, str(links))
                    link_node.setText(1, self.dict_data[gen][link_type][links])
                    link_node.setCheckState(0, False)
                    node_type.addChild(link_node)
                if gen == 'generated':
                    generated.addChild(node_type)
                else:
                    not_generated.addChild(node_type)

        # create a dict with all vectorial product of the constrain
        # and the link geom to use the right direction for calcul
        self.init_dico_direction_links()

    def init_dico_direction_links(self):
        self.dico_direction = {}

        raw_data = self.project.execute('''
                                    with raw_data as
                                        (select l.id, l.link_type, l.name, l.generated, degrees(St_azimuth(St_StartPoint(l.geom), St_EndPoint(l.geom))) as link_az,
                                        St_LineLocatePoint(c.geom, St_intersection(l.geom, c.geom)) as loc,
                                        degrees(St_azimuth(St_intersection(l.geom,c.geom), St_LineInterpolatePoint(c.geom, St_LineLocatePoint(c.geom, St_intersection(l.geom, c.geom)) + 0.001))) as c_az
                                        from {model}._link as l, {model}._constrain as c
                                        where St_intersects(l.geom, c.geom)
                                        and c.name= '{constrain_name}')
                                    select (link_az - c_az) as az,
                                    raw_data.name,
                                    raw_data.id,
                                    raw_data.link_type,
                                    case
                                        when (link_az - c_az)>0 and  (link_az - c_az)<180 then -1
                                        when (link_az - c_az)>180 then 1
                                        when (link_az - c_az)<0 and  (link_az - c_az)>-180 then 1
                                        when (link_az - c_az)<-180 then -1
                                    end as vect
                                    from raw_data'''.format(model=self.project.get_current_model().name, constrain_name=self.constrain_name)).fetchall()

        for elem in raw_data:
            self.dico_direction[elem[1].lower()+":"+elem[3]] = elem[4]

        for node in self.node_list:
            if node[1] == 'crossroad_node':
                self.dico_direction[node[2].lower()+":crossroad_node"] = 1
            elif node[1] == 'river_node':
                self.dico_direction[node[2].lower()+":river_node"] = 1

    def __reverse_direction_links(self):
        ''' function to inverse the direction of links in the graph (reverse the dict and reload graph)'''
        for key, value in list(self.dico_direction.items()):
            self.dico_direction[key] = -value

        self.load_graph()

    def load_graph(self):
        # clearing graph
        self.__axe1.clear()
        self.__axe1.grid(True)
        self.__axe1.legend()

        self.list_lines = []
        self.__plot_list = []
        if self.__dot_on_curve is not None:
            ligne = self.__dot_on_curve.pop(0)
            self.__dot_on_curve = None

        # scenario data and results
        scn_path = self.project.get_senario_path_from_name(self.name_scn)
        subdir_w14 = files_dir["w14"]
        result_path_w14 = os.path.join(scn_path, subdir_w14, self.name_scn + "_" + self.project.get_current_model().name + ".w14")
        resultfile_w14 = HydraResult(result_path_w14)

        subdir_w15 = files_dir["w15"]
        result_path_w15 = os.path.join(scn_path, subdir_w15, self.name_scn + "_" + self.project.get_current_model().name + ".w15")
        resultfile_w15 = HydraResult(result_path_w15)

        # init variables
        data = {}
        any_link_checked = False
        if self.links_list:
            sum_tot = [[res[0], np.array([0] * len(res[1]))] for res in resultfile_w14[self.name_for_init][:]]
        elif self.node_list:
            sum_tot = [[res[0], np.array([0] * len(res[1]))] for res in resultfile_w15[self.name_for_init][:]]
        # get a blank array with good format for sum_tot

        # on présente ici le total de toutes les valeurs cochées et les valeurs cochées.

        for i in range(self.constrain_tree.topLevelItemCount()):
            top_group = self.constrain_tree.topLevelItem(i)
            for j in range(top_group.childCount()):
                group = top_group.child(j)
                for ligne in range(group.childCount()):
                    link = group.child(ligne)
                    cat = self.__display_to_text(group.text(0))
                    if cat in ['crossroad_node', 'river_node']:
                        res_file = 'w15'
                    else:
                        res_file = 'w14'
                    if link.checkState(0):
                        any_link_checked = True
                        name = self.__display_to_text(str(link.text(1)))
                        if res_file == "w14":
                            sum_tot = self.__cumul_res(sum_tot, resultfile_w14[name][:], self.dico_direction[name + ":" + cat])
                        elif res_file == "w15":
                            sum_tot = self.__cumul_res(sum_tot, resultfile_w15[name][:], self.dico_direction[name + ":" + cat])
                        if self.chk_disp_links.isChecked():
                            if res_file == "w14":
                                data[name + ":" + cat] = resultfile_w14[name][:]
                            elif res_file == "w15":
                                data[name + ":" + cat] = resultfile_w15[name][:]

        if any_link_checked:
            # Do stuff with graph only when at least a link was checked
            data['total'] = sum_tot

            self.__refresh_graph_text(draw=False)

            # [id , type, name, generated]

            for name_cat in data:
                if ":" in name_cat:
                    name, cat = name_cat.split(':')
                else:
                    name = name_cat
                if self.__display_to_text(name) in dico_color:
                    line = VisuGraphConstrain.HydraResultLine(name, name, data[name_cat], dico_color[name], 0, "-")
                    self.list_lines.append(line)
                else:
                    for elem in self.node_list + self.links_list:
                        if self.__display_to_text(elem[2]) == self.__display_to_text(name):
                            line = VisuGraphConstrain.HydraResultLine(name, name, data[name_cat], dico_color[elem[1]], 0, ":", direction=self.dico_direction[name_cat])
                            self.list_lines.append(line)

            for line in self.list_lines:
                self.__plot_list.append(self.__axe1.plot(line.get_levels(), line.get_values(), linestyle=line.style, marker="", color=line.color, label=self.__text_to_display(line.name)))

        self.canvas.draw()

    def __refresh_graph_text(self, draw=True):
        self.__axe1.set_title(self.graph_title.text())
        self.__axe1.set_ylabel(self.left_axis_title.text())
        self.__axe1.set_xlabel("Time (h)")
        if draw:
            self.canvas.draw()

    def __check_state(self, item):
        ''' call back fonction to check children if a parent is checked and unchecked parent if a children is unchecked '''
        if not self.__isinit:
            self.constrain_tree.blockSignals(True)

            # coche tous les enfants disponibles du Widget
            for i in range(item.childCount()):
                item.child(i).setCheckState(0, item.checkState(0))
                for j in range(item.child(i).childCount()):
                    item.child(i).child(j).setCheckState(0, item.checkState(0))

            # regarde si il existe des parents pour le widget, si oui met son état en accord avec les enfants :
            # si un enfant est décoché alors le parent est décoché.
            if item.parent():
                item.parent().setCheckState(0, 2 if all(item.parent().child(i).checkState(0) for i in range(item.parent().childCount())) else 0)
                if item.parent().parent():
                    item.parent().parent().setCheckState(0, 2 if all(item.parent().parent().child(i).checkState(0) for i in range(item.parent().parent().childCount())) else 0)

            self.load_graph()
            self.constrain_tree.blockSignals(False)

    def __checkall(self):
        for i in range(self.constrain_tree.topLevelItemCount()):
            self.constrain_tree.topLevelItem(i).setCheckState(0, 2 if self.chk_all.isChecked() else 0)

    def __cumul_res(self, result1, result2, direction):
        ''' fonction to cumulate two given result'''
        assert len(result1) == len(result2)
        assert direction == 1 or direction == -1

        for i in range(len(result1)):
            result1[i][1] = result1[i][1] + [elem * direction for elem in result2[i][1]]

        return(result1)

    def __text_to_display(self, text):
        '''use this fonction to have good format to print data'''
        return (text.replace("_", " ").title())

    def __display_to_text(self, display):
        '''use this fonction to formatdata'''
        return (display.replace(" ", "_").lower())

    def __copy_image(self):
        ''' export an image '''
        buf = io.BytesIO()
        self.__fig.savefig(buf)
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard)
        cb.setImage(QImage.fromData(buf.getvalue()), mode=cb.Clipboard)
        buf.close()

    def __copy_data(self):
        ''' copy the data for the current graph and put in clipboard'''
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard)

        if not self.list_lines:
            return

        data_to_export = ""

        for i in range(len(self.list_lines)):
            line = self.list_lines[i]
            if i == 0:
                # first line, add time
                data_to_export += "H(Hours)\t" + "\t".join([str(elem) for elem in line.get_levels()]) + '\r\n'
            data_to_export += "Q {}(m3)\t".format(self.__text_to_display(line.name)) + "\t".join([str(elem) for elem in line.get_values()]) + '\r\n'

        cb.setText(data_to_export, mode=cb.Clipboard)

    def __create_main_frame(self):
        ''' init the main frame for the graph '''
        self.main_frame = QWidget()
        self.__dpi = 75
        self.__fig = Figure(dpi=self.__dpi)
        self.canvas = FigureCanvas(self.__fig)
        self.canvas.setParent(self.main_frame)
        self.__axe1 = self.__fig.add_subplot(111)
        self.__axe1.grid(True)
        self.__plot_list = []
        self.__dot_on_curve = None

        vbox = QVBoxLayout()
        vbox.addWidget(self.canvas)
        self.graph_placeholder.setLayout(vbox)

        self.canvas.mpl_connect('motion_notify_event', self.__hover_curves)

    def __hover_curves(self, event):
        ''' fonction called when hovering a point in the canvas graph'''
        for line in self.__plot_list:
            cont, ind = line[0].contains(event)
            if cont:
                if self.__dot_on_curve is not None:
                    ligne = self.__dot_on_curve.pop(0)
                    if ligne:
                        ligne.remove()
                        del ligne
                    self.__dot_on_curve = None
                x, y = line[0].get_data()
                self.mouse_tracker_lb.setText("Curve : {name} / coord :   {x} Hour ,  {y} Q (m3)".format(x=round(x[ind["ind"][0]], 2), y=round(y[ind["ind"][0]], 2), name=line[0].get_label()))
                self.__dot_on_curve = self.__axe1.plot(x[ind["ind"][0]], y[ind["ind"][0]], linestyle=None, marker=".", color='red')
                self.canvas.draw()
            else:
                self.canvas.draw()