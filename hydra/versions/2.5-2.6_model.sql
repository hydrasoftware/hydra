/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update ${model}.metadata set version = '2.6';
;;

/* ********************************************** */
/*  3D constrain                                  */
/* ********************************************** */

alter table ${model}.metadata add column trigger_constrain_3d boolean not null default 't';
;;

create or replace view ${model}.constrain as
select c.id, name, geom, discretized, elem_length, constrain_type, link_attributes, points_xyz, points_xyz_proximity, coalesce(is_3d, false) as _is_3d
from $model._constrain as c
full join (
	select c.id as id, true as is_3d
	from project.points_xyz as points,
	$model._constrain as c
	where c.points_xyz = points.points_id
	and St_distance(points.geom, c.geom) < c.points_xyz_proximity
	group by c.id) as j on j.id=c.id
;;

create materialized view $model.constrain_3d as
    with points_from_constrain as (
        select (st_dumppoints(c.geom)).path[1] as p_id,
            c.points_xyz as points_id,
            (st_dumppoints(c.geom)).geom as geom,
            9999 as z_ground,
            st_linelocatepoint(c.geom, (st_dumppoints(c.geom)).geom) as loc,
            (st_dumppoints(c.geom)).geom as geom_on_line,
            c.id as id_line
            from $model.constrain c
            where c.constrain_type is distinct from 'ignored_for_coverages'::hydra_constrain_type
            order by c.id, (st_linelocatepoint(c.geom, (st_dumppoints(c.geom)).geom))
            ),
        z_points as (
        select p.id,
            p.points_id,
            p.geom,
            p.z_ground,
            st_linelocatepoint(c.geom, p.geom) as loc,
            st_lineinterpolatepoint(c.geom, st_linelocatepoint(c.geom, p.geom)) as geom_on_line,
            c.points_xyz_proximity as proximity,
            c.id as id_line
            from $model.constrain c,
            project.points_xyz p
            where st_distance(c.geom, p.geom) < c.points_xyz_proximity and c.points_xyz = p.points_id and c.constrain_type is distinct from 'ignored_for_coverages'::hydra_constrain_type
            order by c.id, (st_linelocatepoint(c.geom, p.geom))
            ),
        dist_comp as (
        select pfc.id_line as c_id,
            pfc.p_id,
            pfc.geom,
            pfc.points_id,
            pfc.loc as pfc_loc,
            closest_zp.loc as zp_loc,
            closest_zp.id as zp_id,
            closest_zp.dist,
            closest_zp.z_ground
            from points_from_constrain pfc
                cross join lateral (
                select z_points.id,
                    z_points.z_ground,
                    z_points.loc,
                    st_distance(z_points.geom, pfc.geom) as dist
                    from z_points
                    where pfc.points_id = z_points.points_id
                    and pfc.id_line = z_points.id_line
                    and pfc.loc > z_points.loc
                    order by abs(pfc.loc - z_points.loc)
                    limit 1) closest_zp
	    union
	    select pfc.id_line as c_id,
            pfc.p_id,
            pfc.geom,
            pfc.points_id,
            pfc.loc as pfc_loc,
            closest_zp.loc as zp_loc,
            closest_zp.id as zp_id,
            closest_zp.dist,
            closest_zp.z_ground
            from points_from_constrain pfc
                cross join lateral (
                select z_points.id,
                    z_points.z_ground,
                    z_points.loc,
                    st_distance(z_points.geom, pfc.geom) as dist
                    from z_points
                    where pfc.points_id = z_points.points_id
                    and pfc.id_line = z_points.id_line
                    and pfc.loc <= z_points.loc
                    order by abs(pfc.loc - z_points.loc)
                    limit 1) closest_zp),
        count_points as (
		select count(*) as nb,
            c_id,
            p_id
            from dist_comp
            group by c_id, p_id
		),
	    solo_points as (
		select c_id,
            p_id
            from count_points
		    where nb = 1
	    ),
        weighted_raw as (
        select sum(abs(dist_comp.pfc_loc-dist_comp.zp_loc)) as weight,
            dist_comp.p_id,
            dist_comp.c_id
            from dist_comp
            group by dist_comp.p_id, dist_comp.c_id
        ),
        weighted as (
        select 0 as weight,
            weighted_raw.p_id as p_id,
            weighted_raw.c_id as c_id
            from weighted_raw ,
            solo_points
            where solo_points.p_id = weighted_raw.p_id
            and solo_points.c_id = weighted_raw.c_id
        union
        select weight as weight,
            weighted_raw.p_id as p_id,
            weighted_raw.c_id as c_id
            from weighted_raw ,
            solo_points
            where (weighted_raw.p_id,weighted_raw.c_id) not in (select p_id, c_id from solo_points)
        ),
        contrib as (
        select dist_comp.c_id as c_id,
            dist_comp.p_id,
            (case
                when weighted.weight = 0 then dist_comp.z_ground
                when weighted.weight != 0 then dist_comp.z_ground * ((weighted.weight - abs(pfc_loc-zp_loc)) / weighted.weight)
            end) as contrib_z,
            dist_comp.geom,
            dist_comp.pfc_loc
            from dist_comp,
            weighted
            where dist_comp.p_id = weighted.p_id and dist_comp.c_id = weighted.c_id
        ),
        vertex_with_z as (
        select st_setsrid(st_makepoint(st_x(contrib.geom), st_y(contrib.geom), sum(contrib.contrib_z)), $srid) as geom,
            contrib.c_id,
            contrib.p_id,
            contrib.pfc_loc as loc
            from contrib
            group by contrib.c_id, contrib.p_id, contrib.geom, contrib.pfc_loc
        ),
        all_points as (
        select vz.geom,
            vz.c_id,
            vz.loc
            from vertex_with_z vz
        union
        select st_setsrid(st_makepoint(st_x(zp.geom_on_line), st_y(zp.geom_on_line), zp.z_ground::double precision), $srid) as geom,
            zp.id_line as c_id,
            zp.loc
            from z_points zp
        ),
        ord_points as (
        select ap.c_id,
            ap.geom,
            ap.loc
            from all_points ap
            order by ap.c_id, ap.loc
        )
    select st_setsrid(st_makeline(points.geom)::geometry(linestringz), $srid) as geom,
        points.c_id as id
        from ord_points points
        group by points.c_id
;;

create or replace function ${model}.constrain_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        prec_ real;
        snapped_ geometry;
        other_cst_ geometry;
        differenced_ geometry;
        oned_ geometry;
        discretized_ geometry;
    begin
        select precision from hydra.metadata into prec_;
        if tg_op = 'INSERT' then
            select st_snaptogrid(new.geom, prec_) into snapped_;
            select ST_Linemerge(ST_Collect(geom)) from $model._constrain where ST_Intersects(snapped_, geom) into other_cst_;
            select ST_LineMerge(coalesce(ST_Difference(snapped_, other_cst_), snapped_)) into differenced_;
            if not ST_IsEmpty(differenced_) then
                select ST_GeometryN(differenced_, 1) into oned_;
                select st_snaptogrid(project.discretize_line(oned_, coalesce(new.elem_length, 100)), prec_) into discretized_;
                if not (select St_IsEmpty(st_snaptogrid(new.geom, prec_))) then
                    insert into $model._constrain(name, geom, discretized, elem_length, constrain_type, link_attributes, points_xyz, points_xyz_proximity)
                    values (coalesce(new.name, 'define_later'), oned_, discretized_, coalesce(new.elem_length, 100), new.constrain_type, new.link_attributes, new.points_xyz, coalesce(new.points_xyz_proximity, 1))
                    returning id, elem_length, points_xyz, points_xyz_proximity into new.id, new.elem_length, new.points_xyz, new.points_xyz_proximity ;
                    update $model._constrain set name = 'CONSTR'||new.id::varchar where name = 'define_later' and id = new.id
                    returning name into new.name;
                    return new;
                end if;
                return null;
            end if;
            return null;
        elsif tg_op = 'UPDATE' then
            select st_snaptogrid(new.geom, prec_) into snapped_;
            select ST_Linemerge(ST_Collect(geom)) from $model._constrain where ST_Intersects(snapped_, geom) and id!=old.id into other_cst_;
            select ST_LineMerge(coalesce(ST_Difference(snapped_, other_cst_), snapped_)) into differenced_;
            if not ST_IsEmpty(differenced_) then
                select ST_GeometryN(differenced_, 1) into oned_;
                select st_snaptogrid(project.discretize_line(differenced_, coalesce(new.elem_length, 100)), prec_) into discretized_;
                update $model._constrain set name=new.name, geom=differenced_, discretized=discretized_,
                    elem_length=new.elem_length, constrain_type=new.constrain_type, link_attributes=new.link_attributes,
                    points_xyz=new.points_xyz, points_xyz_proximity=coalesce(new.points_xyz_proximity, 1)
                    where id = old.id;
                return new;
            end if;
            return old;
        elsif tg_op = 'DELETE' then
            delete from $model._constrain where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create or replace function ${model}.constrain_3d_after_fct()
returns trigger
language plpgsql
as $$$$
    declare
        res integer;
    begin
        if (select trigger_constrain_3d from $model.metadata) then
            refresh materialized view $model.constrain_3d;
            select count(*) from $model.constrain_3d into res;
        end if;

        if tg_op = 'INSERT' or tg_op = 'UPDATE'  then
            return new;
        else
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_constrain_3d_after_trig
    after insert or update of points_xyz, points_xyz_proximity or delete on $model._constrain
       execute procedure ${model}.constrain_3d_after_fct()
;;

create or replace function $model.constrain_3d_refresh_fct()
returns integer
language plpgsql
as $$$$
    declare
        res integer;
    begin
        refresh materialized view $model.constrain_3d;
        select count(*) from $model.constrain_3d into res;
        return res;
    end;
$$$$
;;

/* ********************************************** */
/*  Domain markers deletion bug                   */
/* ********************************************** */

create or replace function ${model}.mkd_after_fct()
returns trigger
language plpgsql
as $$$$
    declare
        old_cov integer;
        new_cov integer;
    begin
        if tg_op = 'INSERT' then
            select id from $model.coverage as c where st_intersects(c.geom, new.geom) into new_cov;
            update $model.elem_2d_node as n set domain_2d = new.domain_2d from $model.coverage as c where st_intersects(n.geom, c.geom) and c.id = new_cov;
            return new;
        else
            if tg_op = 'UPDATE' then
                select id from $model.coverage as c where st_intersects(c.geom, old.geom) into old_cov;
                update $model.elem_2d_node as n set domain_2d = new.domain_2d from $model.coverage as c where st_intersects(n.geom, c.geom) and c.id = old_cov;
                select id from $model.coverage as c where st_intersects(c.geom, new.geom) into new_cov;
                update $model.elem_2d_node as n set domain_2d = new.domain_2d from $model.coverage as c where st_intersects(n.geom, c.geom) and c.id = new_cov;
                return new;
            else
                select id from $model.coverage as c where st_intersects(c.geom, old.geom) into old_cov;
                update $model.elem_2d_node as n set domain_2d = null from $model.coverage as c where st_intersects(n.geom, c.geom) and c.id = old_cov;
                return old;
            end if;
        end if;
    end;
$$$$
;;

/* ********************************************** */
/*  River cross section profiles hotfix           */
/* ********************************************** */

create or replace function ${model}.river_cross_section_profile_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        select (
            (new.type_cross_section_up is null or (
                new.z_invert_up is not null
                and new.up_rk is not null
                and (new.type_cross_section_up!='circular' or (new.up_circular_diameter is not null and new.up_circular_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_top_diameter is not null and new.up_ovoid_top_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_invert_diameter is not null and new.up_ovoid_invert_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_height is not null and new.up_ovoid_height >0 ))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_height > (new.up_ovoid_top_diameter+new.up_ovoid_invert_diameter)/2))
                and (new.type_cross_section_up!='pipe' or new.up_cp_geom is not null)
                and (new.type_cross_section_up!='channel' or new.up_op_geom is not null)
                and (new.type_cross_section_up!='valley'
                        or (new.up_rk = 0 and new.up_rk_maj = 0 and new.up_sinuosity = 0)
                        or (new.up_rk >0 and new.up_rk_maj is not null and new.up_rk_maj > 0 and new.up_sinuosity is not null and new.up_sinuosity > 0))
                and (new.type_cross_section_up!='valley' or new.up_vcs_geom is not null)
                )
            )
            and
            (new.type_cross_section_down is null or (
                new.z_invert_down is not null
                and new.down_rk is not null
                and (new.type_cross_section_down!='circular' or (new.down_circular_diameter is not null and new.down_circular_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_top_diameter is not null and new.down_ovoid_top_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_invert_diameter is not null and new.down_ovoid_invert_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_height is not null and new.down_ovoid_height >0 ))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_height > (new.down_ovoid_top_diameter+new.down_ovoid_invert_diameter)/2))
                and (new.type_cross_section_down!='pipe' or new.down_cp_geom is not null)
                and (new.type_cross_section_down!='channel' or new.down_op_geom is not null)
                and (new.type_cross_section_down!='valley'
                        or (new.down_rk = 0 and new.down_rk_maj = 0 and new.down_sinuosity = 0)
                        or (new.down_rk >0 and new.down_rk_maj is not null and new.down_rk_maj > 0 and new.down_sinuosity is not null and new.down_sinuosity > 0))
                and (new.type_cross_section_down!='valley' or new.down_vcs_geom is not null)
                )
            )
            and
            (new.type_cross_section_up is null or new.type_cross_section_down is null or (
                new.z_invert_up is not null and new.z_invert_down is not null
                )
            )
        ) into new.validity;

        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id from $model.river_node where ST_DWithin(new.geom, geom, .1) into new.id;
            select coalesce(new.name, 'CP'||new.id::varchar) into new.name;
            if new.id is null then
                raise exception 'no river node nearby river_cross_section_profile %', st_astext(new.geom);
            end if;

            insert into $model._river_cross_section_profile (
                    id,
                    name,
                    z_invert_up,
                    z_invert_down,
                    type_cross_section_up,
                    type_cross_section_down,
                    up_rk,
                    up_rk_maj,
                    up_sinuosity,
                    up_circular_diameter,
                    up_ovoid_height,
                    up_ovoid_top_diameter,
                    up_ovoid_invert_diameter,
                    up_cp_geom,
                    up_op_geom,
                    up_vcs_geom,
                    up_vcs_topo_geom,
                    down_rk,
                    down_rk_maj,
                    down_sinuosity,
                    down_circular_diameter,
                    down_ovoid_height,
                    down_ovoid_top_diameter,
                    down_ovoid_invert_diameter,
                    down_cp_geom,
                    down_op_geom,
                    down_vcs_geom,
                    down_vcs_topo_geom,
                    configuration,
                    validity
                )
                values (
                    new.id,
                    new.name,
                    new.z_invert_up,
                    new.z_invert_down,
                    new.type_cross_section_up,
                    new.type_cross_section_down,
                    new.up_rk,
                    new.up_rk_maj,
                    new.up_sinuosity,
                    new.up_circular_diameter,
                    new.up_ovoid_height,
                    new.up_ovoid_top_diameter,
                    new.up_ovoid_invert_diameter,
                    new.up_cp_geom,
                    new.up_op_geom,
                    new.up_vcs_geom,
                    new.up_vcs_topo_geom,
                    new.down_rk,
                    new.down_rk_maj,
                    new.down_sinuosity,
                    new.down_circular_diameter,
                    new.down_ovoid_height,
                    new.down_ovoid_top_diameter,
                    new.down_ovoid_invert_diameter,
                    new.down_cp_geom,
                    new.down_op_geom,
                    new.down_vcs_geom,
                    new.down_vcs_topo_geom,
                    new.configuration::json,
                    new.validity
                );
            if new.z_invert_up is not null or new.z_invert_down is not null then
                update $model.river_node
                    set geom=ST_SetSRID(ST_MakePoint(ST_X(geom), ST_Y(geom), coalesce($model.river_node_z_invert(geom), project.altitude(geom))), $srid)
                    where reach=(select reach from $model.river_node where id=new.id);
            end if;

            perform $model.add_configuration_fct(new.configuration::json, new.id, 'river_cross_section_profile');

            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id from $model.river_node where ST_DWithin(new.geom, geom, .1) into new.id;
            select coalesce(new.name, 'CP'||new.id::varchar) into new.name;
            if new.id is null then
                raise exception 'no river node nearby river_cross_section_profile %', st_astext(new.geom);
            end if;

            -- Handle configurations
            if ((new.z_invert_up, new.z_invert_down,
                    new.type_cross_section_up, new.type_cross_section_down,
                    new.up_rk, new.up_rk_maj, new.up_sinuosity, new.up_circular_diameter,
                    new.up_ovoid_height, new.up_ovoid_top_diameter, new.up_ovoid_invert_diameter,
                    new.up_cp_geom, new.up_op_geom, new.up_vcs_geom, new.up_vcs_topo_geom,
                    new.down_rk, new.down_rk_maj, new.down_sinuosity, new.down_circular_diameter,
                    new.down_ovoid_height, new.down_ovoid_top_diameter, new.down_ovoid_invert_diameter,
                    new.down_cp_geom, new.down_op_geom, new.down_vcs_geom, new.down_vcs_topo_geom)
            is distinct from (old.z_invert_up, old.z_invert_down,
                    old.type_cross_section_up, old.type_cross_section_down,
                    old.up_rk, old.up_rk_maj, old.up_sinuosity, old.up_circular_diameter,
                    old.up_ovoid_height, old.up_ovoid_top_diameter, old.up_ovoid_invert_diameter,
                    old.up_cp_geom, old.up_op_geom, old.up_vcs_geom, old.up_vcs_topo_geom,
                    old.down_rk, old.down_rk_maj, old.down_sinuosity, old.down_circular_diameter,
                    old.down_ovoid_height, old.down_ovoid_top_diameter, old.down_ovoid_invert_diameter,
                    old.down_cp_geom, old.down_op_geom, old.down_vcs_geom, old.down_vcs_topo_geom))
            then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}'
                            from (select old.z_invert_up, old.z_invert_down,
                                old.type_cross_section_up, old.type_cross_section_down,
                                old.up_rk, old.up_rk_maj, old.up_sinuosity, old.up_circular_diameter,
                                old.up_ovoid_height, old.up_ovoid_top_diameter, old.up_ovoid_invert_diameter,
                                old.up_cp_geom, old.up_op_geom, old.up_vcs_geom, old.up_vcs_topo_geom,
                                old.down_rk, old.down_rk_maj, old.down_sinuosity, old.down_circular_diameter,
                                old.down_ovoid_height, old.down_ovoid_top_diameter, old.down_ovoid_invert_diameter,
                                old.down_cp_geom, old.down_op_geom, old.down_vcs_geom, old.down_vcs_topo_geom) as o,
                                (select new.z_invert_up, new.z_invert_down,
                                new.type_cross_section_up, new.type_cross_section_down,
                                new.up_rk, new.up_rk_maj, new.up_sinuosity, new.up_circular_diameter,
                                new.up_ovoid_height, new.up_ovoid_top_diameter, new.up_ovoid_invert_diameter,
                                new.up_cp_geom, new.up_op_geom, new.up_vcs_geom, new.up_vcs_topo_geom,
                                new.down_rk, new.down_rk_maj, new.down_sinuosity, new.down_circular_diameter,
                                new.down_ovoid_height, new.down_ovoid_top_diameter, new.down_ovoid_invert_diameter,
                                new.down_cp_geom, new.down_op_geom, new.down_vcs_geom, new.down_vcs_topo_geom) as n
                            into new_config;
                        update $model._river_cross_section_profile set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}'
                            from (select new.z_invert_up, new.z_invert_down,
                                new.type_cross_section_up, new.type_cross_section_down,
                                new.up_rk, new.up_rk_maj, new.up_sinuosity, new.up_circular_diameter,
                                new.up_ovoid_height, new.up_ovoid_top_diameter, new.up_ovoid_invert_diameter,
                                new.up_cp_geom, new.up_op_geom, new.up_vcs_geom, new.up_vcs_topo_geom,
                                new.down_rk, new.down_rk_maj, new.down_sinuosity, new.down_circular_diameter,
                                new.down_ovoid_height, new.down_ovoid_top_diameter, new.down_ovoid_invert_diameter,
                                new.down_cp_geom, new.down_op_geom, new.down_vcs_geom, new.down_vcs_topo_geom) n
                            into new_config;
                        update $model._river_cross_section_profile set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._river_cross_section_profile set
                id=new.id,
                name=new.name,
                z_invert_up=new.z_invert_up,
                z_invert_down=new.z_invert_down,
                type_cross_section_up=new.type_cross_section_up,
                type_cross_section_down=new.type_cross_section_down,
                up_rk=new.up_rk,
                up_rk_maj=new.up_rk_maj,
                up_sinuosity=new.up_sinuosity,
                up_circular_diameter=new.up_circular_diameter,
                up_ovoid_height=new.up_ovoid_height,
                up_ovoid_top_diameter=new.up_ovoid_top_diameter,
                up_ovoid_invert_diameter=new.up_ovoid_invert_diameter,
                up_cp_geom=new.up_cp_geom,
                up_op_geom=new.up_op_geom,
                up_vcs_geom=new.up_vcs_geom,
                up_vcs_topo_geom=new.up_vcs_topo_geom,
                down_rk=new.down_rk,
                down_rk_maj=new.down_rk_maj,
                down_sinuosity=new.down_sinuosity,
                down_circular_diameter=new.down_circular_diameter,
                down_ovoid_height=new.down_ovoid_height,
                down_ovoid_top_diameter=new.down_ovoid_top_diameter,
                down_ovoid_invert_diameter=new.down_ovoid_invert_diameter,
                down_cp_geom=new.down_cp_geom,
                down_op_geom=new.down_op_geom,
                down_vcs_geom=new.down_vcs_geom,
                down_vcs_topo_geom=new.down_vcs_topo_geom,
                validity=new.validity
            where id=old.id;

            if new.z_invert_up != old.z_invert_down or new.z_invert_down != old.z_invert_down then
                update $model.river_node
                    set geom=ST_SetSRID(ST_MakePoint(ST_X(geom), ST_Y(geom), coalesce($model.river_node_z_invert(geom), project.altitude(geom))), $srid)
                    where reach=(select reach from $model.river_node where id=new.id);
            end if;

            perform $model.add_configuration_fct(new.configuration::json, new.id, 'river_cross_section_profile');

            return new;
        end if;
    end;
$$$$
;;

/* ********************************************** */
/*  Thiessen / Rain gage hotfix                   */
/* ********************************************** */

create or replace view ${model}.thiessen_rain_gage as
    with
        vertices as (select ST_Union(geom) as geom from project.rain_gage),
        extend as (select ST_Buffer(ST_Union(ST_Buffer(geom, 1)), -1) as geom from ${model}.catchment),
        voronoi as (select ST_VoronoiPolygons(v.geom, 0, e.geom) as geom from vertices as v, extend as e),
        polygons as (select (ST_Dump(geom)).geom from voronoi)
    select rg.id, rg.name, ST_Intersection(p.geom, e.geom) as geom
    from project.rain_gage as rg, polygons as p, extend as e
    where ST_Contains(p.geom, rg.geom) and ST_intersects(p.geom, e.geom)
;;


/* ********************************************** */
/*  Catchment evolution                           */
/* ********************************************** */

drop view if exists $model.catchment_node cascade;
;;

alter table $model._catchment_node add column hydra_permeable_surface_soil_storage_rfu_mm real;
alter table $model._catchment_node add column hydra_permeable_split_coefficient real;
alter table $model._catchment_node add column hydra_permeable_soil_drainage_time_qres_day real;
alter table $model._catchment_node add column hydra_permeable_catchment_connect_coef real;
;;

create or replace view $model.catchment_node as
    select p.id,
        p.name,
        c.area_ha,
        c.rl,
        c.slope,
        c.c_imp,
        c.netflow_type,
        c.constant_runoff,
        c.horner_ini_loss_coef,
        c.horner_recharge_coef,
        c.holtan_sat_inf_rate_mmh,
        c.holtan_dry_inf_rate_mmh,
        c.holtan_soil_storage_cap_mm,
        c.scs_j_mm,
        c.scs_soil_drainage_time_day,
        c.scs_rfu_mm,
        c.permeable_soil_j_mm,
        c.permeable_soil_rfu_mm,
        c.permeable_soil_ground_max_inf_rate,
        c.permeable_soil_ground_lag_time_day,
        c.permeable_soil_coef_river_exchange,
        c.hydra_surface_soil_storage_rfu_mm,
        c.hydra_inf_rate_f0_mm_day,
        c.hydra_int_soil_storage_j_mm,
        c.hydra_soil_drainage_time_qres_day,
        c.hydra_permeable_surface_soil_storage_rfu_mm,
        c.hydra_permeable_split_coefficient,
        c.hydra_permeable_soil_drainage_time_qres_day,
        c.hydra_permeable_catchment_connect_coef,
        c.gr4_k1,
        c.gr4_k2,
        c.gr4_k3,
        c.gr4_k4,
        c.runoff_type,
        c.socose_tc_mn,
        c.socose_shape_param_beta,
        c.define_k_mn,
        c.q_limit,
        c.q0,
        c.contour,
        c.network_type,
        c.rural_land_use,
        c.industrial_land_use,
        c.suburban_housing_land_use,
        c.dense_housing_land_use,
        p.geom,
        p.configuration::character varying AS configuration,
        p.generated,
        p.validity,
        p.configuration AS configuration_json
    from $model._catchment_node c,
        $model._node p
    where p.id = c.id
;;

/* note: id and node_type are not updatable */
create or replace function ${model}.catchment_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('catchment', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'catchment') where name = 'define_later' and id = id_;
            insert into $model._catchment_node(id, node_type, area_ha, rl, slope, c_imp, netflow_type, constant_runoff, horner_ini_loss_coef, horner_recharge_coef, holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm, scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm, permeable_soil_j_mm, permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange, hydra_surface_soil_storage_rfu_mm, hydra_inf_rate_f0_mm_day, hydra_int_soil_storage_j_mm, hydra_soil_drainage_time_qres_day, hydra_permeable_surface_soil_storage_rfu_mm, hydra_permeable_split_coefficient, hydra_permeable_soil_drainage_time_qres_day, hydra_permeable_catchment_connect_coef, gr4_k1, gr4_k2, gr4_k3, gr4_k4, runoff_type, socose_tc_mn, socose_shape_param_beta, define_k_mn, q_limit, q0, contour, network_type, rural_land_use, industrial_land_use, suburban_housing_land_use, dense_housing_land_use)
                values (id_, 'catchment', new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.hydra_surface_soil_storage_rfu_mm, new.hydra_inf_rate_f0_mm_day, new.hydra_int_soil_storage_j_mm, new.hydra_soil_drainage_time_qres_day, new.hydra_permeable_surface_soil_storage_rfu_mm, new.hydra_permeable_split_coefficient, new.hydra_permeable_soil_drainage_time_qres_day, new.hydra_permeable_catchment_connect_coef, new.gr4_k1, new.gr4_k2, new.gr4_k3, new.gr4_k4, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, coalesce(new.q_limit, 9999), coalesce(new.q0, 0), coalesce(new.contour, (select id from $model.catchment where st_intersects(geom, new.geom))), coalesce(new.network_type, 'separative'), new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'catchment_node');

            update $model._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_type!='constant_runoff' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)) and (netflow_type!='horner' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)) and (netflow_type!='horner' or (horner_recharge_coef is not null and horner_recharge_coef>=0)) and (netflow_type!='holtan' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)) and (netflow_type!='scs' or (scs_j_mm is not null and scs_j_mm>=0)) and (netflow_type!='scs' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)) and (netflow_type!='scs' or (scs_rfu_mm is not null and scs_rfu_mm>=0)) and (netflow_type!='hydra' or (hydra_surface_soil_storage_rfu_mm is not null and hydra_surface_soil_storage_rfu_mm>=0)) and (netflow_type!='hydra' or (hydra_inf_rate_f0_mm_day is not null and hydra_inf_rate_f0_mm_day>=0)) and (netflow_type!='hydra' or (hydra_int_soil_storage_j_mm is not null and hydra_int_soil_storage_j_mm>=0)) and (netflow_type!='hydra' or (hydra_soil_drainage_time_qres_day is not null and hydra_soil_drainage_time_qres_day>=0)) and (netflow_type!='hydra_permeable' or (hydra_permeable_surface_soil_storage_rfu_mm is not null and hydra_permeable_surface_soil_storage_rfu_mm>=0)) and (netflow_type!='hydra_permeable' or (hydra_permeable_split_coefficient is not null and hydra_permeable_split_coefficient>=0 and hydra_permeable_split_coefficient<=1)) and (netflow_type!='hydra_permeable' or (hydra_permeable_soil_drainage_time_qres_day is not null and hydra_permeable_soil_drainage_time_qres_day>=0)) and (netflow_type!='hydra_permeable' or (hydra_permeable_catchment_connect_coef is not null and hydra_permeable_catchment_connect_coef>=0)) and (netflow_type!='hydra_permeable' or (hydra_permeable_catchment_connect_coef is not null and hydra_permeable_catchment_connect_coef<=1)) and (netflow_type!='gr4' or (gr4_k1 is not null and gr4_k1>=0)) and (netflow_type!='gr4' or (gr4_k2 is not null)) and (netflow_type!='gr4' or (gr4_k3 is not null and gr4_k3>=0)) and (netflow_type!='gr4' or (gr4_k4 is not null and gr4_k4>=0)) and (runoff_type is not null or netflow_type='gr4') and (runoff_type!='socose' or (socose_tc_mn is not null and socose_tc_mn>0) or netflow_type='gr4') and (runoff_type!='socose' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6) or netflow_type='gr4') and (runoff_type!='define_k' or (define_k_mn is not null and define_k_mn>0) or netflow_type='gr4') and (q_limit is not null) and (q0 is not null) and (network_type is not null) and (rural_land_use is null or rural_land_use>=0) and (industrial_land_use is null or industrial_land_use>=0) and (suburban_housing_land_use is null or suburban_housing_land_use>=0) and (dense_housing_land_use is null or dense_housing_land_use>=0) from  $model._catchment_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.hydra_surface_soil_storage_rfu_mm, new.hydra_inf_rate_f0_mm_day, new.hydra_int_soil_storage_j_mm, new.hydra_soil_drainage_time_qres_day, new.hydra_permeable_surface_soil_storage_rfu_mm, new.hydra_permeable_split_coefficient, new.hydra_permeable_soil_drainage_time_qres_day, new.hydra_permeable_catchment_connect_coef, new.gr4_k1, new.gr4_k2, new.gr4_k3, new.gr4_k4, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.network_type, new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use) is distinct from (old.area_ha, old.rl, old.slope, old.c_imp, old.netflow_type, old.constant_runoff, old.horner_ini_loss_coef, old.horner_recharge_coef, old.holtan_sat_inf_rate_mmh, old.holtan_dry_inf_rate_mmh, old.holtan_soil_storage_cap_mm, old.scs_j_mm, old.scs_soil_drainage_time_day, old.scs_rfu_mm, old.permeable_soil_j_mm, old.permeable_soil_rfu_mm, old.permeable_soil_ground_max_inf_rate, old.permeable_soil_ground_lag_time_day, old.permeable_soil_coef_river_exchange, old.hydra_surface_soil_storage_rfu_mm, old.hydra_inf_rate_f0_mm_day, old.hydra_int_soil_storage_j_mm, old.hydra_soil_drainage_time_qres_day, old.hydra_permeable_surface_soil_storage_rfu_mm, old.hydra_permeable_split_coefficient, old.hydra_permeable_soil_drainage_time_qres_day, old.hydra_permeable_catchment_connect_coef, old.gr4_k1, old.gr4_k2, old.gr4_k3, old.gr4_k4, old.runoff_type, old.socose_tc_mn, old.socose_shape_param_beta, old.define_k_mn, old.q_limit, old.q0, old.contour, old.network_type, old.rural_land_use, old.industrial_land_use, old.suburban_housing_land_use, old.dense_housing_land_use)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area_ha, old.rl, old.slope, old.c_imp, old.netflow_type, old.constant_runoff, old.horner_ini_loss_coef, old.horner_recharge_coef, old.holtan_sat_inf_rate_mmh, old.holtan_dry_inf_rate_mmh, old.holtan_soil_storage_cap_mm, old.scs_j_mm, old.scs_soil_drainage_time_day, old.scs_rfu_mm, old.permeable_soil_j_mm, old.permeable_soil_rfu_mm, old.permeable_soil_ground_max_inf_rate, old.permeable_soil_ground_lag_time_day, old.permeable_soil_coef_river_exchange, old.hydra_surface_soil_storage_rfu_mm, old.hydra_inf_rate_f0_mm_day, old.hydra_int_soil_storage_j_mm, old.hydra_soil_drainage_time_qres_day, old.hydra_permeable_surface_soil_storage_rfu_mm, old.hydra_permeable_split_coefficient, old.hydra_permeable_soil_drainage_time_qres_day, old.hydra_permeable_catchment_connect_coef, old.gr4_k1, old.gr4_k2, old.gr4_k3, old.gr4_k4, old.runoff_type, old.socose_tc_mn, old.socose_shape_param_beta, old.define_k_mn, old.q_limit, old.q0, old.contour, old.network_type, old.rural_land_use, old.industrial_land_use, old.suburban_housing_land_use, old.dense_housing_land_use) as o, (select new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.hydra_surface_soil_storage_rfu_mm, new.hydra_inf_rate_f0_mm_day, new.hydra_int_soil_storage_j_mm, new.hydra_soil_drainage_time_qres_day, new.hydra_permeable_surface_soil_storage_rfu_mm, new.hydra_permeable_split_coefficient, new.hydra_permeable_soil_drainage_time_qres_day, new.hydra_permeable_catchment_connect_coef, new.gr4_k1, new.gr4_k2, new.gr4_k3, new.gr4_k4, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.network_type, new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.hydra_surface_soil_storage_rfu_mm, new.hydra_inf_rate_f0_mm_day, new.hydra_int_soil_storage_j_mm, new.hydra_soil_drainage_time_qres_day, new.hydra_permeable_surface_soil_storage_rfu_mm, new.hydra_permeable_split_coefficient, new.hydra_permeable_soil_drainage_time_qres_day, new.hydra_permeable_catchment_connect_coef, new.gr4_k1, new.gr4_k2, new.gr4_k3, new.gr4_k4, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.network_type, new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._catchment_node set area_ha=new.area_ha, rl=new.rl, slope=new.slope, c_imp=new.c_imp, netflow_type=new.netflow_type, constant_runoff=new.constant_runoff, horner_ini_loss_coef=new.horner_ini_loss_coef, horner_recharge_coef=new.horner_recharge_coef, holtan_sat_inf_rate_mmh=new.holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh=new.holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm=new.holtan_soil_storage_cap_mm, scs_j_mm=new.scs_j_mm, scs_soil_drainage_time_day=new.scs_soil_drainage_time_day, scs_rfu_mm=new.scs_rfu_mm, permeable_soil_j_mm=new.permeable_soil_j_mm, permeable_soil_rfu_mm=new.permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate=new.permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day=new.permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange=new.permeable_soil_coef_river_exchange, hydra_surface_soil_storage_rfu_mm=new.hydra_surface_soil_storage_rfu_mm, hydra_inf_rate_f0_mm_day=new.hydra_inf_rate_f0_mm_day, hydra_int_soil_storage_j_mm=new.hydra_int_soil_storage_j_mm, hydra_soil_drainage_time_qres_day=new.hydra_soil_drainage_time_qres_day, hydra_permeable_surface_soil_storage_rfu_mm=new.hydra_permeable_surface_soil_storage_rfu_mm, hydra_permeable_split_coefficient=new.hydra_permeable_split_coefficient, hydra_permeable_soil_drainage_time_qres_day=new.hydra_permeable_soil_drainage_time_qres_day, hydra_permeable_catchment_connect_coef=new.hydra_permeable_catchment_connect_coef, gr4_k1=new.gr4_k1, gr4_k2=new.gr4_k2, gr4_k3=new.gr4_k3, gr4_k4=new.gr4_k4, runoff_type=new.runoff_type, socose_tc_mn=new.socose_tc_mn, socose_shape_param_beta=new.socose_shape_param_beta, define_k_mn=new.define_k_mn, q_limit=new.q_limit, q0=new.q0, contour=new.contour, network_type=new.network_type, rural_land_use=new.rural_land_use, industrial_land_use=new.industrial_land_use, suburban_housing_land_use=new.suburban_housing_land_use, dense_housing_land_use=new.dense_housing_land_use where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'catchment_node');

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;

            update $model._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_type!='constant_runoff' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)) and (netflow_type!='horner' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)) and (netflow_type!='horner' or (horner_recharge_coef is not null and horner_recharge_coef>=0)) and (netflow_type!='holtan' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)) and (netflow_type!='scs' or (scs_j_mm is not null and scs_j_mm>=0)) and (netflow_type!='scs' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)) and (netflow_type!='scs' or (scs_rfu_mm is not null and scs_rfu_mm>=0)) and (netflow_type!='hydra' or (hydra_surface_soil_storage_rfu_mm is not null and hydra_surface_soil_storage_rfu_mm>=0)) and (netflow_type!='hydra' or (hydra_inf_rate_f0_mm_day is not null and hydra_inf_rate_f0_mm_day>=0)) and (netflow_type!='hydra' or (hydra_int_soil_storage_j_mm is not null and hydra_int_soil_storage_j_mm>=0)) and (netflow_type!='hydra' or (hydra_soil_drainage_time_qres_day is not null and hydra_soil_drainage_time_qres_day>=0)) and (netflow_type!='hydra_permeable' or (hydra_permeable_surface_soil_storage_rfu_mm is not null and hydra_permeable_surface_soil_storage_rfu_mm>=0)) and (netflow_type!='hydra_permeable' or (hydra_permeable_split_coefficient is not null and hydra_permeable_split_coefficient>=0 and hydra_permeable_split_coefficient<=1)) and (netflow_type!='hydra_permeable' or (hydra_permeable_soil_drainage_time_qres_day is not null and hydra_permeable_soil_drainage_time_qres_day>=0)) and (netflow_type!='hydra_permeable' or (hydra_permeable_catchment_connect_coef is not null and hydra_permeable_catchment_connect_coef>=0)) and (netflow_type!='gr4' or (gr4_k1 is not null and gr4_k1>=0)) and (netflow_type!='gr4' or (gr4_k2 is not null)) and (netflow_type!='gr4' or (gr4_k3 is not null and gr4_k3>=0)) and (netflow_type!='gr4' or (gr4_k4 is not null and gr4_k4>=0)) and (runoff_type is not null or netflow_type='gr4') and (runoff_type!='socose' or (socose_tc_mn is not null and socose_tc_mn>0) or netflow_type='gr4') and (runoff_type!='socose' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6) or netflow_type='gr4') and (runoff_type!='define_k' or (define_k_mn is not null and define_k_mn>0) or netflow_type='gr4') and (q_limit is not null) and (q0 is not null) and (network_type is not null) and (rural_land_use is null or rural_land_use>=0) and (industrial_land_use is null or industrial_land_use>=0) and (suburban_housing_land_use is null or suburban_housing_land_use>=0) and (dense_housing_land_use is null or dense_housing_land_use>=0) from  $model._catchment_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'storage' and (select trigger_coverage from $model.metadata) then
                update $model.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            delete from project.interlink where (model_up='$model' and node_up=old.id) or (model_down='$model' and node_down=old.id);

            delete from $model._catchment_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

create trigger ${model}_catchment_node_trig
    instead of insert or update or delete on $model.catchment_node
       for each row execute procedure ${model}.catchment_node_fct()
;;

create materialized view ${model}.results_catchment as
    select id, UPPER(name)::varchar(24) as name, geom, now() as last_refresh from ${model}.catchment_node
with no data;
;;

/* ********************************************** */
/*  Routing - split_coef                          */
/* ********************************************** */

alter table ${model}._routing_hydrology_link add column if not exists split_coef real;
;;

drop view if exists $model.routing_hydrology_link cascade;
;;

 create or replace view $model.routing_hydrology_link as
    select p.id,
        p.name,
        p.up,
        p.down,
        c.cross_section,
        c.length,
        c.slope,
        c.hydrograph,
        c.split_coef,
        p.geom,
        p.configuration::character varying AS configuration,
        p.generated,
        p.validity,
        p.up_type,
        p.down_type,
        p.configuration as configuration_json
    from $model._routing_hydrology_link c,
        $model._link p
    where p.id = c.id;
;;

create or replace function ${model}.routing_hydrology_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;

            -- Creation of HY singularity if type = routing_hydrology_link or type = connector_hydrology_link
            if 'routing_hydrology' = 'routing_hydrology' or 'routing_hydrology' = 'connector_hydrology' then
                if down_type_ != 'manhole_hydrology' and not (select exists (select 1 from $model.hydrograph_bc_singularity where node=down_)) then
                    insert into $model.hydrograph_bc_singularity(geom) select geom from $model._node where id=down_;
                end if;
            end if;

            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('routing_hydrology', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'routing_hydrology') where name = 'define_later' and id = id_;
            insert into $model._routing_hydrology_link(id, link_type, cross_section, length, slope, hydrograph, split_coef)
                values (id_, 'routing_hydrology', coalesce(new.cross_section, 1), coalesce(new.length, 0.1), coalesce(new.slope, .01), coalesce(new.hydrograph, (select hbc.id from $model.hydrograph_bc_singularity as hbc where ST_DWithin(ST_EndPoint(new.geom), hbc.geom, .1))), coalesce(new.split_coef, 1));

            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'routing_hydrology_link');
            update $model._link set validity = (select (cross_section is not null) and (cross_section>=0) and (length is not null) and (length>=0) and (slope is not null) and (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station', 'crossroad', 'storage', 'elem_2d') and (hydrograph is not null))) and (split_coef is not null) and (split_coef>=0) and (split_coef<=1) from  $model._routing_hydrology_link where id = id_) where id = id_;
            new.id = id_;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.cross_section, new.length, new.slope, new.split_coef) is distinct from (old.cross_section, old.length, old.slope, old.split_coef)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.cross_section, old.length, old.slope, old.split_coef) as o, (select new.cross_section, new.length, new.slope, new.split_coef) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.cross_section, new.length, new.slope, new.split_coef) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._routing_hydrology_link set cross_section=new.cross_section, length=new.length, slope=new.slope, hydrograph=new.hydrograph, split_coef=new.split_coef where id=old.id;

            perform $model.add_configuration_fct(new.configuration::json, old.id, 'routing_hydrology_link');

            update $model._link set validity = (select (cross_section is not null) and (cross_section>=0) and (length is not null) and (length>=0) and (slope is not null) and (down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station', 'crossroad', 'storage', 'elem_2d') and (hydrograph is not null))) and (split_coef is not null) and (split_coef>=0) and (split_coef<=1) from  $model._routing_hydrology_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._routing_hydrology_link where id=old.id;
            delete from $model._link where id=old.id;

            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_routing_hydrology_link_trig
    instead of insert or update or delete on $model.routing_hydrology_link
    for each row execute procedure $model.routing_hydrology_link_fct();
;;

update $model.routing_hydrology_link set split_coef=1.;
;;

/* ********************************************** */
/*              Validity handling                 */
/* ********************************************** */

create view $model.invalid as
    with
        node_invalidity_reason as (select id, case when area is not null then '' else '   area is not null   ' end||case when (area > 0) then '' else '   (area > 0)   ' end||case when z_ground is not null then '' else '   z_ground is not null   ' end||case when h is not null then '' else '   h is not null   ' end||case when h>=0 then '' else '   h>=0   ' end as reason from ${model}.crossroad_node as new where not validity
union
select id, case when area is not null then '' else '   area is not null   ' end||case when (area > 0) then '' else '   (area > 0)   ' end||case when z_ground is not null then '' else '   z_ground is not null   ' end as reason from ${model}.manhole_hydrology_node as new where not validity
union
select id, case when zs_array is not null  then '' else '   zs_array is not null    ' end||case when zini is not null then '' else '   zini is not null   ' end||case when array_length(zs_array, 1) <= 10  then '' else '   array_length(zs_array, 1) <= 10    ' end||case when array_length(zs_array, 1) >= 1 then '' else '   array_length(zs_array, 1) >= 1   ' end||case when array_length(zs_array, 2) = 2 then '' else '   array_length(zs_array, 2) = 2   ' end||case when contraction_coef is not null then '' else '   contraction_coef is not null   ' end||case when contraction_coef<=1 then '' else '   contraction_coef<=1   ' end||case when contraction_coef>=0 then '' else '   contraction_coef>=0   ' end as reason from ${model}.storage_node as new where not validity
union
select id, case when area_ha is not null then '' else '   area_ha is not null   ' end||case when area_ha>0 then '' else '   area_ha>0   ' end||case when rl is not null then '' else '   rl is not null   ' end||case when rl>0 then '' else '   rl>0   ' end||case when slope is not null then '' else '   slope is not null   ' end||case when c_imp is not null then '' else '   c_imp is not null   ' end||case when c_imp>=0 then '' else '   c_imp>=0   ' end||case when c_imp<=1 then '' else '   c_imp<=1   ' end||case when netflow_type is not null then '' else '   netflow_type is not null   ' end||case when netflow_type!='constant_runoff' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1) then '' else '   netflow_type!=''constant_runoff'' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)   ' end||case when netflow_type!='horner' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0) then '' else '   netflow_type!=''horner'' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)   ' end||case when netflow_type!='horner' or (horner_recharge_coef is not null and horner_recharge_coef>=0) then '' else '   netflow_type!=''horner'' or (horner_recharge_coef is not null and horner_recharge_coef>=0)   ' end||case when netflow_type!='holtan' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0) then '' else '   netflow_type!=''holtan'' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)   ' end||case when netflow_type!='holtan' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0) then '' else '   netflow_type!=''holtan'' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)   ' end||case when netflow_type!='holtan' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0) then '' else '   netflow_type!=''holtan'' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)   ' end||case when netflow_type!='scs' or (scs_j_mm is not null and scs_j_mm>=0) then '' else '   netflow_type!=''scs'' or (scs_j_mm is not null and scs_j_mm>=0)   ' end||case when netflow_type!='scs' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0) then '' else '   netflow_type!=''scs'' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)   ' end||case when netflow_type!='scs' or (scs_rfu_mm is not null and scs_rfu_mm>=0) then '' else '   netflow_type!=''scs'' or (scs_rfu_mm is not null and scs_rfu_mm>=0)   ' end||case when netflow_type!='hydra' or (hydra_surface_soil_storage_rfu_mm is not null and hydra_surface_soil_storage_rfu_mm>=0) then '' else '   netflow_type!=''hydra'' or (hydra_surface_soil_storage_rfu_mm is not null and hydra_surface_soil_storage_rfu_mm>=0)   ' end||case when netflow_type!='hydra' or (hydra_inf_rate_f0_mm_day is not null and hydra_inf_rate_f0_mm_day>=0) then '' else '   netflow_type!=''hydra'' or (hydra_inf_rate_f0_mm_day is not null and hydra_inf_rate_f0_mm_day>=0)   ' end||case when netflow_type!='hydra' or (hydra_int_soil_storage_j_mm is not null and hydra_int_soil_storage_j_mm>=0) then '' else '   netflow_type!=''hydra'' or (hydra_int_soil_storage_j_mm is not null and hydra_int_soil_storage_j_mm>=0)   ' end||case when netflow_type!='hydra' or (hydra_soil_drainage_time_qres_day is not null and hydra_soil_drainage_time_qres_day>=0) then '' else '   netflow_type!=''hydra'' or (hydra_soil_drainage_time_qres_day is not null and hydra_soil_drainage_time_qres_day>=0)   ' end||case when netflow_type!='hydra_permeable' or (hydra_permeable_surface_soil_storage_rfu_mm is not null and hydra_permeable_surface_soil_storage_rfu_mm>=0) then '' else '   netflow_type!=''hydra_permeable'' or (hydra_permeable_surface_soil_storage_rfu_mm is not null and hydra_permeable_surface_soil_storage_rfu_mm>=0)   ' end||case when netflow_type!='hydra_permeable' or (hydra_permeable_split_coefficient is not null and hydra_permeable_split_coefficient>=0 and hydra_permeable_split_coefficient<=1) then '' else '   netflow_type!=''hydra_permeable'' or (hydra_permeable_split_coefficient is not null and hydra_permeable_split_coefficient>=0 and hydra_permeable_split_coefficient<=1)   ' end||case when netflow_type!='hydra_permeable' or (hydra_permeable_soil_drainage_time_qres_day is not null and hydra_permeable_soil_drainage_time_qres_day>=0) then '' else '   netflow_type!=''hydra_permeable'' or (hydra_permeable_soil_drainage_time_qres_day is not null and hydra_permeable_soil_drainage_time_qres_day>=0)   ' end||case when netflow_type!='hydra_permeable' or (hydra_permeable_catchment_connect_coef is not null and hydra_permeable_catchment_connect_coef>=0) then '' else '   netflow_type!=''hydra_permeable'' or (hydra_permeable_catchment_connect_coef is not null and hydra_permeable_catchment_connect_coef>=0)   ' end||case when netflow_type!='hydra_permeable' or (hydra_permeable_catchment_connect_coef is not null and hydra_permeable_catchment_connect_coef<=1) then '' else '   netflow_type!=''hydra_permeable'' or (hydra_permeable_catchment_connect_coef is not null and hydra_permeable_catchment_connect_coef<=1)   ' end||case when netflow_type!='gr4' or (gr4_k1 is not null and gr4_k1>=0) then '' else '   netflow_type!=''gr4'' or (gr4_k1 is not null and gr4_k1>=0)   ' end||case when netflow_type!='gr4' or (gr4_k2 is not null) then '' else '   netflow_type!=''gr4'' or (gr4_k2 is not null)   ' end||case when netflow_type!='gr4' or (gr4_k3 is not null and gr4_k3>=0) then '' else '   netflow_type!=''gr4'' or (gr4_k3 is not null and gr4_k3>=0)   ' end||case when netflow_type!='gr4' or (gr4_k4 is not null and gr4_k4>=0) then '' else '   netflow_type!=''gr4'' or (gr4_k4 is not null and gr4_k4>=0)   ' end||case when runoff_type is not null or netflow_type='gr4' then '' else '   runoff_type is not null or netflow_type=''gr4''   ' end||case when runoff_type!='socose' or (socose_tc_mn is not null and socose_tc_mn>0) or netflow_type='gr4' then '' else '   runoff_type!=''socose'' or (socose_tc_mn is not null and socose_tc_mn>0) or netflow_type=''gr4''   ' end||case when runoff_type!='socose' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6) or netflow_type='gr4' then '' else '   runoff_type!=''socose'' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6) or netflow_type=''gr4''   ' end||case when runoff_type!='define_k' or (define_k_mn is not null and define_k_mn>0) or netflow_type='gr4' then '' else '   runoff_type!=''define_k'' or (define_k_mn is not null and define_k_mn>0) or netflow_type=''gr4''   ' end||case when q_limit is not null then '' else '   q_limit is not null   ' end||case when q0 is not null then '' else '   q0 is not null   ' end||case when network_type is not null then '' else '   network_type is not null   ' end||case when rural_land_use is null or rural_land_use>=0 then '' else '   rural_land_use is null or rural_land_use>=0   ' end||case when industrial_land_use is null or industrial_land_use>=0 then '' else '   industrial_land_use is null or industrial_land_use>=0   ' end||case when suburban_housing_land_use is null or suburban_housing_land_use>=0 then '' else '   suburban_housing_land_use is null or suburban_housing_land_use>=0   ' end||case when dense_housing_land_use is null or dense_housing_land_use>=0 then '' else '   dense_housing_land_use is null or dense_housing_land_use>=0   ' end as reason from ${model}.catchment_node as new where not validity
union
select id, case when area is not null then '' else '   area is not null   ' end||case when area>0 then '' else '   area>0   ' end||case when z_invert is not null then '' else '   z_invert is not null   ' end||case when station is not null  then '' else '   station is not null    ' end||case when (select st_intersects(new.geom, geom) from ${model}.station where id=new.station) then '' else '   (select st_intersects(new.geom, geom) from ${model}.station where id=new.station)   ' end as reason from ${model}.station_node as new where not validity
union
select id, case when connection_law is not null then '' else '   connection_law is not null   ' end||case when connection_law != 'manhole_cover' or cover_diameter is not null then '' else '   connection_law != ''manhole_cover'' or cover_diameter is not null   ' end||case when connection_law != 'manhole_cover' or cover_diameter > 0 then '' else '   connection_law != ''manhole_cover'' or cover_diameter > 0   ' end||case when connection_law != 'manhole_cover' or cover_critical_pressure is not null then '' else '   connection_law != ''manhole_cover'' or cover_critical_pressure is not null   ' end||case when connection_law != 'manhole_cover' or cover_critical_pressure >0 then '' else '   connection_law != ''manhole_cover'' or cover_critical_pressure >0   ' end||case when connection_law != 'drainage_inlet' or inlet_width is not null then '' else '   connection_law != ''drainage_inlet'' or inlet_width is not null   ' end||case when connection_law != 'drainage_inlet' or inlet_width >0 then '' else '   connection_law != ''drainage_inlet'' or inlet_width >0   ' end||case when connection_law != 'drainage_inlet' or inlet_height is not null then '' else '   connection_law != ''drainage_inlet'' or inlet_height is not null   ' end||case when connection_law != 'drainage_inlet' or inlet_height >0 then '' else '   connection_law != ''drainage_inlet'' or inlet_height >0   ' end||case when area is not null then '' else '   area is not null   ' end||case when area>0 then '' else '   area>0   ' end||case when z_ground is not null then '' else '   z_ground is not null   ' end||case when (select not exists(select 1 from ${model}.station where st_intersects(geom, new.geom))) then '' else '   (select not exists(select 1 from ${model}.station where st_intersects(geom, new.geom)))   ' end||case when (select exists (select 1 from ${model}.pipe_link where up=new.id or down=new.id)) then '' else '   (select exists (select 1 from ${model}.pipe_link where up=new.id or down=new.id))   ' end as reason from ${model}.manhole_node as new where not validity
union
select id, case when area is not null then '' else '   area is not null   ' end||case when area>0 then '' else '   area>0   ' end||case when zb is not null then '' else '   zb is not null   ' end||case when rk is not null then '' else '   rk is not null   ' end||case when rk>0 then '' else '   rk>0   ' end as reason from ${model}.elem_2d_node as new where not validity
union
select id, case when z_ground is not null then '' else '   z_ground is not null   ' end||case when area is not null then '' else '   area is not null   ' end||case when area>0 then '' else '   area>0   ' end||case when reach is not null then '' else '   reach is not null   ' end||case when (select not exists(select 1 from ${model}.station where st_intersects(geom, new.geom))) then '' else '   (select not exists(select 1 from ${model}.station where st_intersects(geom, new.geom)))   ' end as reason from ${model}.river_node as new where not validity),
        link_invalidity_reason as (select id, case when z_invert is not null then '' else '   z_invert is not null   ' end||case when z_ceiling is not null then '' else '   z_ceiling is not null   ' end||case when width is not null then '' else '   width is not null   ' end||case when width>=0 then '' else '   width>=0   ' end||case when cc is not null then '' else '   cc is not null   ' end||case when cc<=1 then '' else '   cc<=1   ' end||case when cc>=0 then '' else '   cc>=0   ' end||case when action_gate_type is not null then '' else '   action_gate_type is not null   ' end||case when z_invert_stop is not null then '' else '   z_invert_stop is not null   ' end||case when z_ceiling_stop is not null then '' else '   z_ceiling_stop is not null   ' end||case when v_max_cms is not null then '' else '   v_max_cms is not null   ' end||case when v_max_cms>=0 then '' else '   v_max_cms>=0   ' end||case when dt_regul_hr is not null then '' else '   dt_regul_hr is not null   ' end||case when mode_regul!='elevation' or z_control_node is not null then '' else '   mode_regul!=''elevation'' or z_control_node is not null   ' end||case when mode_regul!='elevation' or z_pid_array is not null then '' else '   mode_regul!=''elevation'' or z_pid_array is not null   ' end||case when mode_regul!='elevation' or z_tz_array is not null then '' else '   mode_regul!=''elevation'' or z_tz_array is not null   ' end||case when mode_regul!='elevation' or array_length(z_tz_array, 1)<=10 then '' else '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   ' end||case when mode_regul!='elevation' or array_length(z_tz_array, 1)>=1 then '' else '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   ' end||case when mode_regul!='elevation' or array_length(z_tz_array, 2)=2 then '' else '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   ' end||case when mode_regul!='discharge' or q_z_crit is not null then '' else '   mode_regul!=''discharge'' or q_z_crit is not null   ' end||case when mode_regul!='discharge' or array_length(q_tq_array, 1)<=10 then '' else '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   ' end||case when mode_regul!='discharge' or array_length(q_tq_array, 1)>=1 then '' else '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   ' end||case when mode_regul!='discharge' or array_length(q_tq_array, 2)=2 then '' else '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   ' end||case when mode_regul!='no_regulation' or nr_z_gate is not null then '' else '   mode_regul!=''no_regulation'' or nr_z_gate is not null   ' end||case when cc_submerged >0 then '' else '   cc_submerged >0   ' end||case when up is not null then '' else '   up is not null   ' end||case when down is not null then '' else '   down is not null   ' end||case when up_type is not null then '' else '   up_type is not null   ' end||case when down_type is not null then '' else '   down_type is not null   ' end as reason from ${model}.regul_gate_link as new where not validity
union
select id, case when z_invert is not null then '' else '   z_invert is not null   ' end||case when width is not null then '' else '   width is not null   ' end||case when width>=0. then '' else '   width>=0.   ' end||case when cc is not null then '' else '   cc is not null   ' end||case when cc<=1 then '' else '   cc<=1   ' end||case when cc>=0. then '' else '   cc>=0.   ' end||case when break_mode is not null then '' else '   break_mode is not null   ' end||case when break_mode!='zw_critical' or z_break is not null then '' else '   break_mode!=''zw_critical'' or z_break is not null   ' end||case when break_mode!='time_critical' or t_break is not null then '' else '   break_mode!=''time_critical'' or t_break is not null   ' end||case when break_mode='none' or grp is not null then '' else '   break_mode=''none'' or grp is not null   ' end||case when break_mode='none' or grp>0 then '' else '   break_mode=''none'' or grp>0   ' end||case when break_mode='none' or grp<100 then '' else '   break_mode=''none'' or grp<100   ' end||case when break_mode='none' or dt_fracw_array is not null then '' else '   break_mode=''none'' or dt_fracw_array is not null   ' end||case when break_mode='none' or array_length(dt_fracw_array, 1)<=10 then '' else '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   ' end||case when break_mode='none' or array_length(dt_fracw_array, 1)>=1 then '' else '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   ' end||case when break_mode='none' or array_length(dt_fracw_array, 2)=2 then '' else '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   ' end||case when up is not null then '' else '   up is not null   ' end||case when down is not null then '' else '   down is not null   ' end||case when up_type is not null then '' else '   up_type is not null   ' end||case when down_type is not null then '' else '   down_type is not null   ' end as reason from ${model}.fuse_spillway_link as new where not validity
union
select id, case when z_invert_up is not null then '' else '   z_invert_up is not null   ' end||case when z_invert_down is not null then '' else '   z_invert_down is not null   ' end||case when h_sable is not null then '' else '   h_sable is not null   ' end||case when cross_section_type is not null then '' else '   cross_section_type is not null   ' end||case when cross_section_type not in ('valley') then '' else '   cross_section_type not in (''valley'')   ' end||case when rk is not null then '' else '   rk is not null   ' end||case when rk >0 then '' else '   rk >0   ' end||case when cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0) then '' else '   cross_section_type!=''circular'' or (circular_diameter is not null and circular_diameter > 0)   ' end||case when cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0) then '' else '   cross_section_type!=''ovoid'' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)   ' end||case when cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0) then '' else '   cross_section_type!=''ovoid'' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)   ' end||case when cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 ) then '' else '   cross_section_type!=''ovoid'' or (ovoid_height is not null and ovoid_height >0 )   ' end||case when cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2) then '' else '   cross_section_type!=''ovoid'' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)   ' end||case when cross_section_type!='pipe' or cp_geom is not null then '' else '   cross_section_type!=''pipe'' or cp_geom is not null   ' end||case when cross_section_type!='channel' or op_geom is not null then '' else '   cross_section_type!=''channel'' or op_geom is not null   ' end||case when up is not null then '' else '   up is not null   ' end||case when down is not null then '' else '   down is not null   ' end||case when up_type is not null then '' else '   up_type is not null   ' end||case when down_type is not null then '' else '   down_type is not null   ' end as reason from ${model}.pipe_link as new where not validity
union
select id, case when cross_section is not null then '' else '   cross_section is not null   ' end||case when cross_section>=0 then '' else '   cross_section>=0   ' end||case when length is not null then '' else '   length is not null   ' end||case when length>=0 then '' else '   length>=0   ' end||case when slope is not null then '' else '   slope is not null   ' end||case when down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station', 'crossroad', 'storage', 'elem_2d') and (hydrograph is not null)) then '' else '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'', ''crossroad'', ''storage'', ''elem_2d'') and (hydrograph is not null))   ' end||case when split_coef is not null then '' else '   split_coef is not null   ' end||case when split_coef>=0 then '' else '   split_coef>=0   ' end||case when split_coef<=1 then '' else '   split_coef<=1   ' end||case when up is not null then '' else '   up is not null   ' end||case when down is not null then '' else '   down is not null   ' end||case when up_type is not null then '' else '   up_type is not null   ' end||case when down_type is not null then '' else '   down_type is not null   ' end as reason from ${model}.routing_hydrology_link as new where not validity
union
select id, case when q_pump is not null then '' else '   q_pump is not null   ' end||case when q_pump>= 0 then '' else '   q_pump>= 0   ' end||case when qz_array is not null then '' else '   qz_array is not null   ' end||case when array_length(qz_array, 1)<=10 then '' else '   array_length(qz_array, 1)<=10   ' end||case when array_length(qz_array, 1)>=1 then '' else '   array_length(qz_array, 1)>=1   ' end||case when array_length(qz_array, 2)=2 then '' else '   array_length(qz_array, 2)=2   ' end||case when (/*   qz_array: 0<=q   */ select bool_and(bool) from (select 0<=unnest(new.qz_array[:][1]) as bool) as data_set) then '' else '   (/*   qz_array: 0<=q   */ select bool_and(bool) from (select 0<=unnest(new.qz_array[:][1]) as bool) as data_set)   ' end||case when (/*   qz_array: q<=1   */ select bool_and(bool) from (select unnest(new.qz_array[:][1])<=1 as bool) as data_set) then '' else '   (/*   qz_array: q<=1   */ select bool_and(bool) from (select unnest(new.qz_array[:][1])<=1 as bool) as data_set)   ' end||case when up is not null then '' else '   up is not null   ' end||case when down is not null then '' else '   down is not null   ' end||case when up_type is not null then '' else '   up_type is not null   ' end||case when down_type is not null then '' else '   down_type is not null   ' end as reason from ${model}.deriv_pump_link as new where not validity
union
select id, case when z_invert is not null then '' else '   z_invert is not null   ' end||case when width is not null then '' else '   width is not null   ' end||case when width>=0 then '' else '   width>=0   ' end||case when transmitivity is not null then '' else '   transmitivity is not null   ' end||case when transmitivity>=0 then '' else '   transmitivity>=0   ' end||case when up is not null then '' else '   up is not null   ' end||case when down is not null then '' else '   down is not null   ' end||case when up_type is not null then '' else '   up_type is not null   ' end||case when down_type is not null then '' else '   down_type is not null   ' end as reason from ${model}.porous_link as new where not validity
union
select id, case when z_invert is not null  then '' else '   z_invert is not null    ' end||case when width is not null then '' else '   width is not null   ' end||case when width>=0 then '' else '   width>=0   ' end||case when cc is not null then '' else '   cc is not null   ' end||case when cc<=1 then '' else '   cc<=1   ' end||case when cc>=0 then '' else '   cc>=0   ' end||case when v_max_cms is not null then '' else '   v_max_cms is not null   ' end||case when v_max_cms>=0 then '' else '   v_max_cms>=0   ' end||case when up is not null then '' else '   up is not null   ' end||case when down is not null then '' else '   down is not null   ' end||case when up_type is not null then '' else '   up_type is not null   ' end||case when down_type is not null then '' else '   down_type is not null   ' end as reason from ${model}.weir_link as new where not validity
union
select id, case when law_type is not null then '' else '   law_type is not null   ' end||case when param is not null then '' else '   param is not null   ' end||case when up is not null then '' else '   up is not null   ' end||case when down is not null then '' else '   down is not null   ' end||case when up_type is not null then '' else '   up_type is not null   ' end||case when down_type is not null then '' else '   down_type is not null   ' end as reason from ${model}.borda_headloss_link as new where not validity
union
select id, case when npump is not null then '' else '   npump is not null   ' end||case when npump<=10 then '' else '   npump<=10   ' end||case when npump>=1 then '' else '   npump>=1   ' end||case when zregul_array is not null then '' else '   zregul_array is not null   ' end||case when hq_array is not null then '' else '   hq_array is not null   ' end||case when array_length(zregul_array, 1)=npump then '' else '   array_length(zregul_array, 1)=npump   ' end||case when array_length(zregul_array, 2)=2 then '' else '   array_length(zregul_array, 2)=2   ' end||case when array_length(hq_array, 1)=npump then '' else '   array_length(hq_array, 1)=npump   ' end||case when array_length(hq_array, 2)<=10 then '' else '   array_length(hq_array, 2)<=10   ' end||case when array_length(hq_array, 2)>=1 then '' else '   array_length(hq_array, 2)>=1   ' end||case when array_length(hq_array, 3)=2 then '' else '   array_length(hq_array, 3)=2   ' end||case when up is not null then '' else '   up is not null   ' end||case when down is not null then '' else '   down is not null   ' end||case when up_type is not null then '' else '   up_type is not null   ' end||case when down_type is not null then '' else '   down_type is not null   ' end as reason from ${model}.pump_link as new where not validity
union
select id, case when z_invert is not null then '' else '   z_invert is not null   ' end||case when lateral_contraction_coef is not null then '' else '   lateral_contraction_coef is not null   ' end||case when lateral_contraction_coef<=1 then '' else '   lateral_contraction_coef<=1   ' end||case when lateral_contraction_coef>=0 then '' else '   lateral_contraction_coef>=0   ' end||case when ST_NPoints(border)=2 then '' else '   ST_NPoints(border)=2   ' end||case when ST_IsValid(border) then '' else '   ST_IsValid(border)   ' end||case when up is not null then '' else '   up is not null   ' end||case when down is not null then '' else '   down is not null   ' end||case when up_type is not null then '' else '   up_type is not null   ' end||case when down_type is not null then '' else '   down_type is not null   ' end as reason from ${model}.mesh_2d_link as new where not validity
union
select id, case when z_crest1 is not null then '' else '   z_crest1 is not null   ' end||case when width1 is not null then '' else '   width1 is not null   ' end||case when width1>=0 then '' else '   width1>=0   ' end||case when z_crest2 is not null then '' else '   z_crest2 is not null   ' end||case when z_crest2>=z_crest1 then '' else '   z_crest2>=z_crest1   ' end||case when width2 is not null then '' else '   width2 is not null   ' end||case when width2>=0 then '' else '   width2>=0   ' end||case when cc is not null then '' else '   cc is not null   ' end||case when cc<=1 then '' else '   cc<=1   ' end||case when cc>=0 then '' else '   cc>=0   ' end||case when lateral_contraction_coef is not null then '' else '   lateral_contraction_coef is not null   ' end||case when lateral_contraction_coef<=1 then '' else '   lateral_contraction_coef<=1   ' end||case when lateral_contraction_coef>=0 then '' else '   lateral_contraction_coef>=0   ' end||case when break_mode is not null then '' else '   break_mode is not null   ' end||case when break_mode!='zw_critical' or z_break is not null then '' else '   break_mode!=''zw_critical'' or z_break is not null   ' end||case when break_mode!='time_critical' or t_break is not null then '' else '   break_mode!=''time_critical'' or t_break is not null   ' end||case when break_mode='none' or width_breach is not null then '' else '   break_mode=''none'' or width_breach is not null   ' end||case when break_mode='none' or z_invert is not null then '' else '   break_mode=''none'' or z_invert is not null   ' end||case when break_mode='none' or grp is not null then '' else '   break_mode=''none'' or grp is not null   ' end||case when break_mode='none' or grp>0 then '' else '   break_mode=''none'' or grp>0   ' end||case when break_mode='none' or grp<100 then '' else '   break_mode=''none'' or grp<100   ' end||case when break_mode='none' or dt_fracw_array is not null then '' else '   break_mode=''none'' or dt_fracw_array is not null   ' end||case when break_mode='none' or array_length(dt_fracw_array, 1)<=10 then '' else '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   ' end||case when break_mode='none' or array_length(dt_fracw_array, 1)>=1 then '' else '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   ' end||case when break_mode='none' or array_length(dt_fracw_array, 2)=2 then '' else '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   ' end||case when up is not null then '' else '   up is not null   ' end||case when down is not null then '' else '   down is not null   ' end||case when up_type is not null then '' else '   up_type is not null   ' end||case when down_type is not null then '' else '   down_type is not null   ' end as reason from ${model}.overflow_link as new where not validity
union
select id, case when z_overflow is not null then '' else '   z_overflow is not null   ' end||case when area is not null then '' else '   area is not null   ' end||case when area>=0 then '' else '   area>=0   ' end||case when up is not null then '' else '   up is not null   ' end||case when down is not null then '' else '   down is not null   ' end||case when up_type is not null then '' else '   up_type is not null   ' end||case when down_type is not null then '' else '   down_type is not null   ' end as reason from ${model}.network_overflow_link as new where not validity
union
select id, case when z_invert is not null then '' else '   z_invert is not null   ' end||case when z_ceiling is not null then '' else '   z_ceiling is not null   ' end||case when z_ceiling>z_invert then '' else '   z_ceiling>z_invert   ' end||case when width is not null then '' else '   width is not null   ' end||case when cc is not null then '' else '   cc is not null   ' end||case when cc <=1 then '' else '   cc <=1   ' end||case when cc >=0 then '' else '   cc >=0   ' end||case when action_gate_type is not null then '' else '   action_gate_type is not null   ' end||case when mode_valve is not null then '' else '   mode_valve is not null   ' end||case when z_gate is not null then '' else '   z_gate is not null   ' end||case when z_gate>=z_invert then '' else '   z_gate>=z_invert   ' end||case when z_gate<=z_ceiling then '' else '   z_gate<=z_ceiling   ' end||case when v_max_cms is not null then '' else '   v_max_cms is not null   ' end||case when cc_submerged >0 then '' else '   cc_submerged >0   ' end||case when up is not null then '' else '   up is not null   ' end||case when down is not null then '' else '   down is not null   ' end||case when up_type is not null then '' else '   up_type is not null   ' end||case when down_type is not null then '' else '   down_type is not null   ' end as reason from ${model}.gate_link as new where not validity
union
select id, case when down_type='manhole_hydrology' or (down_type in ('manhole', 'river', 'station', 'storage', 'crossroad', 'elem_2d') and (hydrograph is not null)) then '' else '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'', ''storage'', ''crossroad'', ''elem_2d'') and (hydrograph is not null))   ' end||case when up is not null then '' else '   up is not null   ' end||case when down is not null then '' else '   down is not null   ' end||case when up_type is not null then '' else '   up_type is not null   ' end||case when down_type is not null then '' else '   down_type is not null   ' end as reason from ${model}.connector_hydrology_link as new where not validity
union
select id, case when z_crest1 is not null then '' else '   z_crest1 is not null   ' end||case when width1 is not null then '' else '   width1 is not null   ' end||case when width1>0 then '' else '   width1>0   ' end||case when length is not null then '' else '   length is not null   ' end||case when length>0 then '' else '   length>0   ' end||case when rk is not null then '' else '   rk is not null   ' end||case when rk>=0 then '' else '   rk>=0   ' end||case when z_crest2 is not null then '' else '   z_crest2 is not null   ' end||case when z_crest2>z_crest1 then '' else '   z_crest2>z_crest1   ' end||case when width2 is not null then '' else '   width2 is not null   ' end||case when width2>width1 then '' else '   width2>width1   ' end||case when up is not null then '' else '   up is not null   ' end||case when down is not null then '' else '   down is not null   ' end||case when up_type is not null then '' else '   up_type is not null   ' end||case when down_type is not null then '' else '   down_type is not null   ' end as reason from ${model}.strickler_link as new where not validity),
        singularity_invalidity_reason as (select id, case when zq_array is not null then '' else '   zq_array is not null   ' end||case when array_length(zq_array, 1)<=20 then '' else '   array_length(zq_array, 1)<=20   ' end||case when array_length(zq_array, 1)>=1 then '' else '   array_length(zq_array, 1)>=1   ' end||case when array_length(zq_array, 2)=2 then '' else '   array_length(zq_array, 2)=2   ' end as reason from ${model}.zq_bc_singularity as new where not validity
union
select id, case when downstream is not null then '' else '   downstream is not null   ' end||case when split1 is not null then '' else '   split1 is not null   ' end||case when downstream_param is not null then '' else '   downstream_param is not null   ' end||case when split1_law is not null then '' else '   split1_law is not null   ' end||case when split1_param is not null then '' else '   split1_param is not null   ' end||case when split2 is null or split2_law is not null then '' else '   split2 is null or split2_law is not null   ' end||case when split2 is null or split2_param is not null then '' else '   split2 is null or split2_param is not null   ' end as reason from ${model}.zq_split_hydrology_singularity as new where not validity
union
select id, case when pk0_km is not null then '' else '   pk0_km is not null   ' end||case when dx is not null then '' else '   dx is not null   ' end||case when dx>=0.1 then '' else '   dx>=0.1   ' end as reason from ${model}.pipe_branch_marker_singularity as new where not validity
union
select id, case when slope is not null then '' else '   slope is not null   ' end||case when k is not null then '' else '   k is not null   ' end||case when k>0 then '' else '   k>0   ' end||case when width is not null then '' else '   width is not null   ' end||case when width>=0 then '' else '   width>=0   ' end as reason from ${model}.strickler_bc_singularity as new where not validity
union
select id, case when cascade_mode is not null then '' else '   cascade_mode is not null   ' end||case when cascade_mode='hydrograph' or zq_array is not null or tz_array is not null then '' else '   cascade_mode=''hydrograph'' or zq_array is not null or tz_array is not null   ' end||case when array_length(zq_array, 1)<=20 then '' else '   array_length(zq_array, 1)<=20   ' end||case when array_length(zq_array, 1)>=1 then '' else '   array_length(zq_array, 1)>=1   ' end||case when array_length(zq_array, 2)=2 then '' else '   array_length(zq_array, 2)=2   ' end||case when array_length(tz_array, 1)<=20 then '' else '   array_length(tz_array, 1)<=20   ' end||case when array_length(tz_array, 1)>=1 then '' else '   array_length(tz_array, 1)>=1   ' end||case when array_length(tz_array, 2)=2 then '' else '   array_length(tz_array, 2)=2   ' end||case when array_length(quality, 1)=9 then '' else '   array_length(quality, 1)=9   ' end as reason from ${model}.model_connect_bc_singularity as new where not validity
union
select id, case when d_abutment_l is not null then '' else '   d_abutment_l is not null   ' end||case when d_abutment_r is not null then '' else '   d_abutment_r is not null   ' end||case when abutment_type is not null then '' else '   abutment_type is not null   ' end||case when zw_array is not null  then '' else '   zw_array is not null    ' end||case when z_ceiling is not null then '' else '   z_ceiling is not null   ' end||case when array_length(zw_array, 1)<=10 then '' else '   array_length(zw_array, 1)<=10   ' end||case when array_length(zw_array, 1)>=1 then '' else '   array_length(zw_array, 1)>=1   ' end||case when array_length(zw_array, 2)=2 then '' else '   array_length(zw_array, 2)=2   ' end||case when not ${model}.check_on_branch_or_reach_endpoint(new.geom) then '' else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   ' end as reason from ${model}.bradley_headloss_singularity as new where not validity
union
select id, case when l_road is not null then '' else '   l_road is not null   ' end||case when l_road>=0 then '' else '   l_road>=0   ' end||case when z_road is not null then '' else '   z_road is not null   ' end||case when zw_array is not null  then '' else '   zw_array is not null    ' end||case when array_length(zw_array, 1)<=10 then '' else '   array_length(zw_array, 1)<=10   ' end||case when array_length(zw_array, 1)>=1 then '' else '   array_length(zw_array, 1)>=1   ' end||case when array_length(zw_array, 2)=2 then '' else '   array_length(zw_array, 2)=2   ' end||case when not ${model}.check_on_branch_or_reach_endpoint(new.geom) then '' else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   ' end as reason from ${model}.bridge_headloss_singularity as new where not validity
union
select id, case when qz_array is not null then '' else '   qz_array is not null   ' end||case when array_length(qz_array, 1)<=10 then '' else '   array_length(qz_array, 1)<=10   ' end||case when array_length(qz_array, 1)>=1 then '' else '   array_length(qz_array, 1)>=1   ' end||case when array_length(qz_array, 2)=2 then '' else '   array_length(qz_array, 2)=2   ' end||case when not ${model}.check_on_branch_or_reach_endpoint(new.geom) then '' else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   ' end as reason from ${model}.hydraulic_cut_singularity as new where not validity
union
select id, case when drainage is not null then '' else '   drainage is not null   ' end||case when overflow is not null then '' else '   overflow is not null   ' end||case when z_ini is not null then '' else '   z_ini is not null   ' end||case when zr_sr_qf_qs_array is not null  then '' else '   zr_sr_qf_qs_array is not null    ' end||case when treatment_mode is not null then '' else '   treatment_mode is not null   ' end||case when treatment_param is not null then '' else '   treatment_param is not null   ' end||case when array_length(zr_sr_qf_qs_array, 1)<=10 then '' else '   array_length(zr_sr_qf_qs_array, 1)<=10   ' end||case when array_length(zr_sr_qf_qs_array, 1)>=1 then '' else '   array_length(zr_sr_qf_qs_array, 1)>=1   ' end||case when array_length(zr_sr_qf_qs_array, 2)=4 then '' else '   array_length(zr_sr_qf_qs_array, 2)=4   ' end as reason from ${model}.reservoir_rsp_hydrology_singularity as new where not validity
union
select id, case when storage_area is not null then '' else '   storage_area is not null   ' end||case when storage_area>=0 then '' else '   storage_area>=0   ' end||case when constant_dry_flow is not null then '' else '   constant_dry_flow is not null   ' end||case when constant_dry_flow>=0 then '' else '   constant_dry_flow>=0   ' end||case when distrib_coef is null or distrib_coef>=0 then '' else '   distrib_coef is null or distrib_coef>=0   ' end||case when lag_time_hr is null or (lag_time_hr>=0) then '' else '   lag_time_hr is null or (lag_time_hr>=0)   ' end||case when sector is null or (distrib_coef is not null and lag_time_hr is not null) then '' else '   sector is null or (distrib_coef is not null and lag_time_hr is not null)   ' end||case when array_length(pollution_dryweather_runoff, 1)=2 then '' else '   array_length(pollution_dryweather_runoff, 1)=2   ' end||case when array_length(pollution_dryweather_runoff, 2)=4 then '' else '   array_length(pollution_dryweather_runoff, 2)=4   ' end||case when array_length(quality_dryweather_runoff, 1)=2 then '' else '   array_length(quality_dryweather_runoff, 1)=2   ' end||case when array_length(quality_dryweather_runoff, 2)=9 then '' else '   array_length(quality_dryweather_runoff, 2)=9   ' end||case when external_file_data or tq_array is not null then '' else '   external_file_data or tq_array is not null   ' end||case when external_file_data or array_length(tq_array, 1)<=10 then '' else '   external_file_data or array_length(tq_array, 1)<=10   ' end||case when external_file_data or array_length(tq_array, 1)>=1 then '' else '   external_file_data or array_length(tq_array, 1)>=1   ' end||case when external_file_data or array_length(tq_array, 2)=2 then '' else '   external_file_data or array_length(tq_array, 2)=2   ' end as reason from ${model}.hydrograph_bc_singularity as new where not validity
union
select id, case when z_invert is not null  then '' else '   z_invert is not null    ' end||case when z_regul is not null  then '' else '   z_regul is not null    ' end||case when width is not null then '' else '   width is not null   ' end||case when width>=0 then '' else '   width>=0   ' end||case when cc is not null then '' else '   cc is not null   ' end||case when cc<=1 then '' else '   cc<=1   ' end||case when cc>=0 then '' else '   cc>=0   ' end||case when mode_regul is not null then '' else '   mode_regul is not null   ' end||case when reoxy_law is not null then '' else '   reoxy_law is not null   ' end||case when reoxy_param is not null then '' else '   reoxy_param is not null   ' end||case when not ${model}.check_on_branch_or_reach_endpoint(new.geom) then '' else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   ' end as reason from ${model}.zregul_weir_singularity as new where not validity
union
select id, case when qq_array is not null then '' else '   qq_array is not null   ' end||case when downstream is not null then '' else '   downstream is not null   ' end||case when split1 is not null then '' else '   split1 is not null   ' end||case when array_length(qq_array, 1)<=10 then '' else '   array_length(qq_array, 1)<=10   ' end||case when array_length(qq_array, 1)>=1 then '' else '   array_length(qq_array, 1)>=1   ' end||case when (split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2) then '' else '   (split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2)   ' end as reason from ${model}.qq_split_hydrology_singularity as new where not validity
union
select id, case when zs_array is not null  then '' else '   zs_array is not null    ' end||case when zini is not null then '' else '   zini is not null   ' end||case when array_length(zs_array, 1)<=10 then '' else '   array_length(zs_array, 1)<=10   ' end||case when array_length(zs_array, 1)>=1 then '' else '   array_length(zs_array, 1)>=1   ' end||case when array_length(zs_array, 2)=2 then '' else '   array_length(zs_array, 2)=2   ' end||case when treatment_mode is not null then '' else '   treatment_mode is not null   ' end as reason from ${model}.tank_bc_singularity as new where not validity
union
select id, case when drainage is not null then '' else '   drainage is not null   ' end||case when overflow is not null then '' else '   overflow is not null   ' end||case when q_drainage is not null then '' else '   q_drainage is not null   ' end||case when q_drainage>=0 then '' else '   q_drainage>=0   ' end||case when z_ini is not null then '' else '   z_ini is not null   ' end||case when zs_array is not null  then '' else '   zs_array is not null    ' end||case when treatment_mode is not null then '' else '   treatment_mode is not null   ' end||case when array_length(zs_array, 1)<=10 then '' else '   array_length(zs_array, 1)<=10   ' end||case when array_length(zs_array, 1)>=1 then '' else '   array_length(zs_array, 1)>=1   ' end||case when array_length(zs_array, 2)=2 then '' else '   array_length(zs_array, 2)=2   ' end as reason from ${model}.reservoir_rs_hydrology_singularity as new where not validity
union
select id, case when q_dz_array is not null then '' else '   q_dz_array is not null   ' end||case when array_length(q_dz_array, 1)<=10 then '' else '   array_length(q_dz_array, 1)<=10   ' end||case when array_length(q_dz_array, 1)>=1 then '' else '   array_length(q_dz_array, 1)>=1   ' end||case when array_length(q_dz_array, 2)=2 then '' else '   array_length(q_dz_array, 2)=2   ' end||case when q_dz_array[1][1]=0 then '' else '   q_dz_array[1][1]=0   ' end||case when q_dz_array[1][2]=0 then '' else '   q_dz_array[1][2]=0   ' end||case when not ${model}.check_on_branch_or_reach_endpoint(new.geom) then '' else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   ' end as reason from ${model}.param_headloss_singularity as new where not validity
union
select id, case when external_file_data or tz_array is not null then '' else '   external_file_data or tz_array is not null   ' end||case when external_file_data or array_length(tz_array, 1)<=10 then '' else '   external_file_data or array_length(tz_array, 1)<=10   ' end||case when external_file_data or array_length(tz_array, 1)>=1 then '' else '   external_file_data or array_length(tz_array, 1)>=1   ' end||case when external_file_data or array_length(tz_array, 2)=2 then '' else '   external_file_data or array_length(tz_array, 2)=2   ' end as reason from ${model}.tz_bc_singularity as new where not validity
union
select id, case when z_invert is not null then '' else '   z_invert is not null   ' end||case when z_ceiling is not null then '' else '   z_ceiling is not null   ' end||case when width is not null then '' else '   width is not null   ' end||case when width >=0 then '' else '   width >=0   ' end||case when cc is not null then '' else '   cc is not null   ' end||case when cc <=1 then '' else '   cc <=1   ' end||case when cc >=0 then '' else '   cc >=0   ' end||case when action_gate_type is not null then '' else '   action_gate_type is not null   ' end||case when z_invert_stop is not null then '' else '   z_invert_stop is not null   ' end||case when z_ceiling_stop is not null then '' else '   z_ceiling_stop is not null   ' end||case when v_max_cms is not null then '' else '   v_max_cms is not null   ' end||case when v_max_cms>=0 then '' else '   v_max_cms>=0   ' end||case when dt_regul_hr is not null then '' else '   dt_regul_hr is not null   ' end||case when mode_regul!='elevation' or z_control_node is not null then '' else '   mode_regul!=''elevation'' or z_control_node is not null   ' end||case when mode_regul!='elevation' or z_pid_array is not null then '' else '   mode_regul!=''elevation'' or z_pid_array is not null   ' end||case when mode_regul!='elevation' or z_tz_array is not null then '' else '   mode_regul!=''elevation'' or z_tz_array is not null   ' end||case when mode_regul!='elevation' or array_length(z_tz_array, 1)<=10 then '' else '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   ' end||case when mode_regul!='elevation' or array_length(z_tz_array, 1)>=1 then '' else '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   ' end||case when mode_regul!='elevation' or array_length(z_tz_array, 2)=2 then '' else '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   ' end||case when mode_regul!='discharge' or q_z_crit is not null then '' else '   mode_regul!=''discharge'' or q_z_crit is not null   ' end||case when mode_regul!='discharge' or array_length(q_tq_array, 1)<=10 then '' else '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   ' end||case when mode_regul!='discharge' or array_length(q_tq_array, 1)>=1 then '' else '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   ' end||case when mode_regul!='discharge' or array_length(q_tq_array, 2)=2 then '' else '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   ' end||case when mode_regul!='no_regulation' or nr_z_gate is not null then '' else '   mode_regul!=''no_regulation'' or nr_z_gate is not null   ' end||case when cc_submerged >0 then '' else '   cc_submerged >0   ' end as reason from ${model}.regul_sluice_gate_singularity as new where not validity
union
select id, case when z_invert is not null then '' else '   z_invert is not null   ' end||case when z_ceiling is not null then '' else '   z_ceiling is not null   ' end||case when z_ceiling>z_invert then '' else '   z_ceiling>z_invert   ' end||case when width is not null then '' else '   width is not null   ' end||case when cc is not null then '' else '   cc is not null   ' end||case when cc <=1 then '' else '   cc <=1   ' end||case when cc >=0 then '' else '   cc >=0   ' end||case when action_gate_type is not null then '' else '   action_gate_type is not null   ' end||case when mode_valve is not null then '' else '   mode_valve is not null   ' end||case when z_gate is not null then '' else '   z_gate is not null   ' end||case when z_gate>=z_invert then '' else '   z_gate>=z_invert   ' end||case when z_gate<=z_ceiling then '' else '   z_gate<=z_ceiling   ' end||case when v_max_cms is not null then '' else '   v_max_cms is not null   ' end||case when not ${model}.check_on_branch_or_reach_endpoint(new.geom) then '' else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   ' end||case when cc_submerged >0 then '' else '   cc_submerged >0   ' end as reason from ${model}.gate_singularity as new where not validity
union
select id, case when law_type is not null then '' else '   law_type is not null   ' end||case when param is not null then '' else '   param is not null   ' end||case when not ${model}.check_on_branch_or_reach_endpoint(new.geom) then '' else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   ' end as reason from ${model}.borda_headloss_singularity as new where not validity
union
select id, case when q0 is not null then '' else '   q0 is not null   ' end as reason from ${model}.constant_inflow_bc_singularity as new where not validity
union
select id, case when z_weir is not null then '' else '   z_weir is not null   ' end||case when width is not null then '' else '   width is not null   ' end||case when width>=0 then '' else '   width>=0   ' end||case when cc is not null then '' else '   cc is not null   ' end||case when cc<=1 then '' else '   cc<=1   ' end||case when cc>=0. then '' else '   cc>=0.   ' end as reason from ${model}.weir_bc_singularity as new where not validity)
    select CONCAT('node:', n.id) as
        id,
        validity,
        name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom,
        reason from $model._node as n join node_invalidity_reason as r on r.id=n.id
        where validity=FALSE
    union
    select CONCAT('link:', l.id) as
        id,
        validity,
        name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom,
        reason from $model._link as l join link_invalidity_reason as r on  r.id=l.id
        where validity=FALSE
    union
    select CONCAT('singularity:', s.id) as
        id,
        s.validity,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        reason from $model._singularity as s join $model._node as n on s.node = n.id join singularity_invalidity_reason as r on r.id=s.id
        where s.validity=FALSE
    union
    select CONCAT('profile:', p.id) as
        id,
        p.validity,
        p.name,
        'profile'::varchar as type,
        ''::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        ''::varchar as reason from $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where p.validity=FALSE
;;