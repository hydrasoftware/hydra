/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  Utility                                       */
/* ********************************************** */

create function csv.geometry_to_array(g geometry)
returns numeric[]
language plpgsql
as $$$$
    declare
        _res numeric[][];
    begin
        with vertices as (
            select ST_DumpPoints(g) as dump
        )
        select array_agg(ARRAY[ST_X((dump).geom), ST_Y((dump).geom)])
        from vertices
        into _res;
        return _res;
    end;
$$$$
;;

create function csv.reverse_columns(anyarray)
returns anyarray
language sql
as $$$$
    select ARRAY(
        select ARRAY[$$1[i][2], $$1[i][1]]
        from generate_subscripts($$1,1) as s(i)
        order by i asc);
$$$$
;;

/* ********************************************** */
/*  Export functions: PROJECT ELEMENTS            */
/* ********************************************** */

create function csv.dry_inflow_hourly_modulation()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    tvv_arr real[]
)
language plpgsql
as $$$$
    begin
        return query execute
            'SELECT
                ''PROJET''::varchar as modele,
                ''CRBTSEC''::varchar as element,
                name::varchar as identifiant,
                hv_array as tvv_arr
            FROM
                project.dry_inflow_hourly_modulation
            ORDER by name';
    end;
$$$$
;;

create function csv.dry_inflow_scenario()
returns table (
    modele varchar,
    element varchar,
    id_scenario varchar,
    nb_secteur integer,
    id_secteur varchar,
    courbe_adimensionnelle varchar,
    vcum real,
    vepp real,
    coefcum real,
    coefepp real,
    day varchar
)
language plpgsql
as $$$$
    begin
        return query execute
            'SELECT
                ''PROJECT''::varchar as modele,
                ''SCNTSEC''::varchar as element,
                UPPER(scn.name)::varchar as id_scenario,
                (select count(1) from project.dry_inflow_scenario_sector_setting as d where d.dry_inflow_scenario=scn.id)::integer as nb_secteur,
                UPPER(sect.name)::varchar as id_secteur,
                crv.name::varchar as courbe_adimensionnelle,
                settings.volume_sewage_m3day::real as vcum,
                settings.volume_clear_water_m3day::real as vepp,
                settings.coef_volume_sewage_m3day::real as coefcum,
                settings.coef_volume_clear_water_m3day::real as coefepp,
                scn.period::varchar as day
            FROM project.dry_inflow_scenario_sector_setting as settings
            LEFT JOIN project.dry_inflow_scenario as scn ON scn.id=settings.dry_inflow_scenario
            LEFT JOIN project.dry_inflow_sector as sect ON sect.id=settings.sector
            LEFT JOIN project.dry_inflow_hourly_modulation as crv ON crv.id=sect.vol_curve
            ORDER by scn.name, sect.name';
    end;
$$$$
;;

create function csv.rain_gage()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    x numeric,
    y numeric
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        return query execute
            'SELECT
                ''PROJECT''::varchar as modele,
                ''PLV''::varchar as element,
                UPPER(r.name)::varchar as identifiant,
                (ST_X(r.geom))::numeric as x,
                (ST_Y(r.geom))::numeric as y
            FROM project.rain_gage as r
            ORDER by r.name';
    end;
$$$$
;;

/* ********************************************** */
/*  Export functions: HYDROLOGY                   */
/* ********************************************** */

create function csv.connector_hydrology_link()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    noeud_amont varchar,
    noeud_aval varchar,
    id_hydrograph varchar
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''HYCON''::varchar as element,
                        UPPER(l.name)::varchar as identifiant,
                        UPPER(amn.name)::varchar as noeud_amont,
                        UPPER(avn.name)::varchar as noeud_aval,
                        UPPER(h.name)::varchar as id_hydrograph
                    FROM %1$$I.connector_hydrology_link as l
                    LEFT JOIN %1$$I._node as amn on amn.id=l.up
                    LEFT JOIN %1$$I._node as avn on avn.id=l.down
                    left join %1$$I.hydrograph_bc_singularity as h on h.id=l.hydrograph
                    ORDER by l.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.routing_hydrology_link()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    noeud_amont varchar,
    noeud_aval varchar,
    section real,
    longueur real,
    pente real,
    id_hydrograph varchar
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''ROUT''::varchar as element,
                        UPPER(l.name)::varchar as identifiant,
                        UPPER(amn.name)::varchar as noeud_amont,
                        UPPER(avn.name)::varchar as noeud_aval,
                        l.cross_section::real as section,
                        l.length::real as longueur,
                        l.slope::real as pente,
                        UPPER(h.name)::varchar as id_hydrograph
                    FROM %1$$I.routing_hydrology_link as l
                    LEFT JOIN %1$$I._node as amn on amn.id=l.up
                    LEFT JOIN %1$$I._node as avn on avn.id=l.down
                    left join %1$$I.hydrograph_bc_singularity as h on h.id=l.hydrograph
                    ORDER by l.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.hydrology_bc_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    x numeric,
    y numeric
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''CLAV''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        (ST_X(n.geom))::numeric as x,
                        (ST_Y(n.geom))::numeric as y
                    FROM %1$$I.hydrology_bc_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.qq_split_hydrology_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    id_aval varchar,
    type_aval varchar,
    id_split1 varchar,
    type_split1 varchar,
    id_split2 varchar,
    type_split2 varchar,
    qq_arr real[]
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''QQSP''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        UPPER(la.name)::varchar as id_aval,
                        UPPER(la.link_type::varchar)::varchar as type_aval,
                        UPPER(l1.name)::varchar as id_split1,
                        UPPER(l1.link_type::varchar)::varchar as type_split1,
                        UPPER(l2.name)::varchar as id_split2,
                        UPPER(l2.link_type::varchar)::varchar as type_split2,
                        qq_array as qq_arr
                    FROM %1$$I.qq_split_hydrology_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    LEFT JOIN %1$$I._link as la ON la.id=s.downstream
                    LEFT JOIN %1$$I._link as l1 ON l1.id=s.split1
                    LEFT JOIN %1$$I._link as l2 ON l2.id=s.split2
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.zq_split_hydrology_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    id_aval varchar,
    type_aval varchar,
    mode_aval integer,
    aval_coef real,
    aval_z real,
    aval_l_d real,
    aval_zq_arr real[],
    id_split1 varchar,
    type_split1 varchar,
    mode_split1 integer,
    split1_coef real,
    split1_z real,
    split1_l_d real,
    split1_zq_arr real[],
    id_split2 varchar,
    type_split2 varchar,
    mode_split2 integer,
    split2_coef real,
    split2_z real,
    split2_l_d real,
    split2_zq_arr real[]
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''ZQSP''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        UPPER(la.name)::varchar as id_aval,
                        UPPER(la.link_type::varchar)::varchar as type_aval,
                        ma.id::integer as mode_aval,
                        CASE
                            WHEN s.downstream_law::varchar=''weir'' THEN (s.downstream_param::json->''weir''->>''weir_cc'')::real
                            WHEN s.downstream_law::varchar=''gate'' THEN (s.downstream_param::json->''gate''->>''gate_cc'')::real
                            ELSE null::real END as aval_coef,
                        CASE
                            WHEN s.downstream_law::varchar=''weir'' THEN (s.downstream_param::json->''weir''->>''weir_z_invert'')::real
                            WHEN s.downstream_law::varchar=''gate'' THEN (s.downstream_param::json->''gate''->>''gate_z_invert'')::real
                            ELSE null::real END as aval_z,
                        CASE
                            WHEN s.downstream_law::varchar=''weir'' THEN (s.downstream_param::json->''weir''->>''weir_width'')::real
                            WHEN s.downstream_law::varchar=''gate'' THEN (s.downstream_param::json->''gate''->>''gate_diameter'')::real
                            ELSE null::real END as aval_l_d,
                        CASE
                            WHEN s.downstream_law::varchar=''zq'' THEN translate(s.downstream_param::json->''zq''->>''zq_array'', ''[]'', ''{}'')::real[]
                            ELSE null::real[] END as aval_zq_arr,
                        UPPER(l1.name)::varchar as id_split1,
                        UPPER(l1.link_type::varchar)::varchar as type_split1,
                        m1.id::integer as mode_split1,
                        CASE
                            WHEN s.split1_law::varchar=''weir'' THEN (s.split1_param::json->''weir''->>''weir_cc'')::real
                            WHEN s.split1_law::varchar=''gate'' THEN (s.split1_param::json->''gate''->>''gate_cc'')::real
                            ELSE null::real END as split1_coef,
                        CASE
                            WHEN s.split1_law::varchar=''weir'' THEN (s.split1_param::json->''weir''->>''weir_z_invert'')::real
                            WHEN s.split1_law::varchar=''gate'' THEN (s.split1_param::json->''gate''->>''gate_z_invert'')::real
                            ELSE null::real END as split1_z,
                        CASE
                            WHEN s.split1_law::varchar=''weir'' THEN (s.split1_param::json->''weir''->>''weir_width'')::real
                            WHEN s.split1_law::varchar=''gate'' THEN (s.split1_param::json->''gate''->>''gate_diameter'')::real
                            ELSE null::real END as split1_l_d,
                        CASE
                            WHEN s.split1_law::varchar=''zq'' THEN translate(s.split1_param::json->''zq''->>''zq_array'', ''[]'', ''{}'')::real[]
                            ELSE null::real[] END as split1_zq_arr,
                        UPPER(l2.name)::varchar as id_split2,
                        UPPER(l2.link_type::varchar)::varchar as type_split2,
                        m2.id::integer as mode_split2,
                        CASE
                            WHEN s.split2_law::varchar=''weir'' THEN (s.split2_param::json->''weir''->>''weir_cc'')::real
                            WHEN s.split2_law::varchar=''gate'' THEN (s.split2_param::json->''gate''->>''gate_cc'')::real
                            ELSE null::real END as split2_coef,
                        CASE
                            WHEN s.split2_law::varchar=''weir'' THEN (s.split2_param::json->''weir''->>''weir_z_invert'')::real
                            WHEN s.split2_law::varchar=''gate'' THEN (s.split2_param::json->''gate''->>''gate_z_invert'')::real
                            ELSE null::real END as split2_z,
                        CASE
                            WHEN s.split2_law::varchar=''weir'' THEN (s.split2_param::json->''weir''->>''weir_width'')::real
                            WHEN s.split2_law::varchar=''gate'' THEN (s.split2_param::json->''gate''->>''gate_diameter'')::real
                            ELSE null::real END as split2_l_d,
                        CASE
                            WHEN s.split2_law::varchar=''zq'' THEN translate(s.split2_param::json->''zq''->>''zq_array'', ''[]'', ''{}'')::real[]
                            ELSE null::real[] END as split2_zq_arr
                    FROM %1$$I.zq_split_hydrology_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    LEFT JOIN %1$$I._link as la ON la.id=s.downstream
                    LEFT JOIN hydra.split_law_type as ma ON ma.name=s.downstream_law
                    LEFT JOIN %1$$I._link as l1 ON l1.id=s.split1
                    LEFT JOIN hydra.split_law_type as m1 ON m1.name=s.split1_law
                    LEFT JOIN %1$$I._link as l2 ON l2.id=s.split2
                    LEFT JOIN hydra.split_law_type as m2 ON m2.name=s.split2_law
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.reservoir_rs_hydrology_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    id_drainage varchar,
    type_drainage varchar,
    id_overflow varchar,
    type_overflow varchar,
    q_drainage real,
    z_ini real,
    zs_arr real[]
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''RS''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        UPPER(ld.name)::varchar as id_drainage,
                        UPPER(ld.link_type::varchar)::varchar as type_drainage,
                        UPPER(lo.name)::varchar as id_overflow,
                        UPPER(lo.link_type::varchar)::varchar as type_overflow,
                        s.q_drainage::real as q_drainage,
                        s.z_ini::real as z_ini,
                        s.zs_array as zs_arr
                    FROM %1$$I.reservoir_rs_hydrology_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    LEFT JOIN %1$$I._link as ld ON ld.id=s.drainage
                    LEFT JOIN %1$$I._link as lo ON lo.id=s.overflow
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.reservoir_rsp_hydrology_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    id_drainage varchar,
    type_drainage varchar,
    id_overflow varchar,
    type_overflow varchar,
    z_ini real,
    zr_sr_qf_qs_arr real[]
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''RSP''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        UPPER(ld.name)::varchar as id_drainage,
                        UPPER(ld.link_type::varchar)::varchar as type_drainage,
                        UPPER(lo.name)::varchar as id_overflow,
                        UPPER(lo.link_type::varchar)::varchar as type_overflow,
                        s.z_ini::real as z_ini,
                        s.zr_sr_qf_qs_array as zr_sr_qf_qs_arr
                    FROM %1$$I.reservoir_rsp_hydrology_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    LEFT JOIN %1$$I._link as ld ON ld.id=s.drainage
                    LEFT JOIN %1$$I._link as lo ON lo.id=s.overflow
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;


/* ********************************************** */
/*  Export functions: NODES & MARKERS             */
/* ********************************************** */

create function csv.node()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    x numeric,
    y numeric,
    z_tn real,
    area real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        CASE
                            WHEN n.node_type::varchar=''manhole'' THEN ''NODA''::varchar
                            WHEN n.node_type::varchar=''river'' THEN ''NODR''::varchar
                            WHEN n.node_type::varchar=''station'' THEN ''NODS''::varchar
                            WHEN n.node_type::varchar=''storage'' THEN ''CAS''::varchar
                            WHEN n.node_type::varchar=''elem_2d'' THEN ''PAV''::varchar
                            WHEN n.node_type::varchar=''crossroad'' THEN ''CAR''::varchar
                            WHEN n.node_type::varchar=''manhole_hydrology'' THEN ''NODH''::varchar
                            WHEN n.node_type::varchar=''catchment'' THEN ''BV''::varchar
                            END as element,
                        UPPER(n.name)::varchar as identifiant,
                        (ST_X(n.geom))::numeric as x,
                        (ST_Y(n.geom))::numeric as y,
                        CASE
                            WHEN n.node_type::varchar=''manhole'' THEN (select z_ground from %1$$I.manhole_node where id=n.id)::real
                            WHEN n.node_type::varchar=''river'' THEN (select z_ground from %1$$I.river_node where id=n.id)::real
                            WHEN n.node_type::varchar=''station'' THEN (select z_invert from %1$$I.station_node where id=n.id)::real
                            WHEN n.node_type::varchar=''storage'' THEN (select zs_array[1][1] from %1$$I.storage_node where id=n.id)::real
                            WHEN n.node_type::varchar=''elem_2d'' THEN (select zb from %1$$I.elem_2d_node where id=n.id)::real
                            WHEN n.node_type::varchar=''crossroad'' THEN (select z_ground from %1$$I.crossroad_node where id=n.id)::real
                            WHEN n.node_type::varchar=''manhole_hydrology'' THEN (select z_ground from %1$$I.manhole_hydrology_node where id=n.id)::real
                            WHEN n.node_type::varchar=''catchment'' THEN null
                            END as z_tn,
                        CASE
                            WHEN n.node_type::varchar=''manhole'' THEN (select area from %1$$I.manhole_node where id=n.id)::real
                            WHEN n.node_type::varchar=''river'' THEN (select area from %1$$I.river_node where id=n.id)::real
                            WHEN n.node_type::varchar=''station'' THEN (select area from %1$$I.station_node where id=n.id)::real
                            WHEN n.node_type::varchar=''storage'' THEN (select ST_Area(geom) from %1$$I.coverage where id=(select contour from %1$$I.storage_node where id=n.id))::real
                            WHEN n.node_type::varchar=''elem_2d'' THEN (select area from %1$$I.elem_2d_node where id=n.id)::real
                            WHEN n.node_type::varchar=''crossroad'' THEN (select area from %1$$I.crossroad_node where id=n.id)::real
                            WHEN n.node_type::varchar=''manhole_hydrology'' THEN (select area from %1$$I.manhole_hydrology_node where id=n.id)::real
                            WHEN n.node_type::varchar=''catchment'' THEN (select area_ha*10000 from %1$$I.catchment_node where id=n.id)::real
                            END as area
                    FROM %1$$I._node as n
                    ORDER by element, n.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.marker_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''MRKP''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud
                    FROM %1$$I.marker_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.pipe_branch_marker_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    pk0 real,
    dx real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''MRKB''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        s.pk0_km::real as pk0,
                        s.dx::real as dx
                    FROM %1$$I.pipe_branch_marker_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

/* ********************************************** */
/*  Export functions: CONTAINERS & NODES          */
/* ********************************************** */

-- Manholes + branchs
create function csv.branch()
returns table (
    modele varchar,
    element varchar,
    id_branche varchar,
    numero_branche integer,
    nb_regard integer,
    id_regard varchar,
    pk_regard real,
    id_ouvrage varchar,
    type_ouvrage varchar,
    x_regard numeric,
    y_regard numeric,
    surface_regard real,
    z_regard real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''BRN''::varchar as element,
                        UPPER(b.name)::varchar as id_branche,
                        b.id::integer as numero_branche,
                        (select count(1) from %1$$I.manhole_node as nod where ST_DWithin(nod.geom, b.geom, 0.1))::integer as nb_regard,
                        UPPER(n.name)::varchar as id_regard,
                        (ST_LineLocatePoint(b.geom, n.geom)*ST_Length(b.geom))::real as pk_regard,
                        UPPER(s.name)::varchar as id_ouvrage,
                        UPPER(s.singularity_type::varchar)::varchar as type_ouvrage,
                        (ST_X(n.geom))::numeric as x_regard,
                        (ST_Y(n.geom))::numeric as y_regard,
                        n.area::real as surface_regard,
                        n.z_ground::real as z_regard
                    FROM %1$$I.manhole_node as n
                    LEFT JOIN %1$$I.branch as b ON ST_DWithin(n.geom, b.geom, 0.1)
                    LEFT JOIN %1$$I._singularity as s ON s.node=n.id
                    ORDER by b.name, ST_LineLocatePoint(b.geom, n.geom)
                ', model.name);
        END LOOP;
    end;
$$$$
;;

-- River_nodes + reachs
create function csv.reach()
returns table (
    modele varchar,
    element varchar,
    id_bief varchar,
    numero_bief integer,
    pk0_bief real,
    nb_noeud integer,
    id_noeud varchar,
    pk_noeud real,
    x_noeud numeric,
    y_noeud numeric,
    id_profil varchar,
    id_ouvrage varchar,
    type_ouvrage varchar
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''BIEF''::varchar as element,
                        UPPER(r.name)::varchar as id_bief,
                        r.id::integer as numero_bief,
                        (r.pk0_km*1000)::real as pk0_bief,
                        (select count(1) from %1$$I.river_node as nod where ST_DWithin(nod.geom, r.geom, 0.1))::integer as nb_noeud,
                        UPPER(n.name)::varchar as id_noeud,
                        (r.pk0_km*1000 + ST_LineLocatePoint(r.geom, n.geom)*ST_Length(r.geom))::real as pk_noeud,
                        (ST_X(n.geom))::numeric as x_noeud,
                        (ST_Y(n.geom))::numeric as y_noeud,
                        UPPER(p.name)::varchar as id_profil,
                        UPPER(s.name)::varchar as id_ouvrage,
                        UPPER(s.singularity_type::varchar)::varchar as type_ouvrage
                    FROM %1$$I.river_node as n
                    LEFT JOIN %1$$I.reach as r ON ST_DWithin(n.geom, r.geom, 0.1)
                    LEFT JOIN %1$$I._singularity as s ON s.node=n.id
                    LEFT JOIN %1$$I._river_cross_section_profile as p ON p.id=n.id
                    ORDER by r.name, ST_LineLocatePoint(r.geom, n.geom)
                ', model.name);
        END LOOP;
    end;
$$$$
;;

-- Station_nodes + stations
create function csv.station()
returns table (
    modele varchar,
    element varchar,
    id_station varchar,
    numero_station integer,
    xc_station numeric,
    yc_station numeric,
    nb_noeud integer,
    id_noeud varchar,
    x_noeud numeric,
    y_noeud numeric,
    id_ouvrage varchar,
    type_ouvrage varchar
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''STN''::varchar as element,
                        UPPER(st.name)::varchar as id_station,
                        st.id::integer as numero_station,
                        (ST_X(ST_Centroid(st.geom)))::numeric as xc_station,
                        (ST_Y(ST_Centroid(st.geom)))::numeric as yc_station,
                        (select count(1) from %1$$I.station_node as nod where nod.station=st.id)::integer as nb_noeud,
                        UPPER(n.name)::varchar as id_noeud,
                        (ST_X(n.geom))::numeric as x_noeud,
                        (ST_Y(n.geom))::numeric as y_noeud,
                        UPPER(s.name)::varchar as id_ouvrage,
                        UPPER(s.singularity_type::varchar)::varchar as type_ouvrage
                    FROM %1$$I.station_node as n
                    JOIN %1$$I.station as st ON st.id=n.station
                    INNER JOIN %1$$I._singularity as s ON s.node=n.id
                    UNION
                    SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''STN''::varchar as element,
                        UPPER(st.name)::varchar as id_station,
                        st.id::integer as numero_station,
                        (ST_X(ST_Centroid(st.geom)))::numeric as xc_station,
                        (ST_Y(ST_Centroid(st.geom)))::numeric as yc_station,
                        (select count(1) from %1$$I.station_node as nod where nod.station=st.id)::integer as nb_noeud,
                        UPPER(n.name)::varchar as id_noeud,
                        (ST_X(n.geom))::numeric as x_noeud,
                        (ST_Y(n.geom))::numeric as y_noeud,
                        UPPER(l.name)::varchar as id_ouvrage,
                        UPPER(l.link_type::varchar)::varchar as type_ouvrage
                    FROM %1$$I.station_node as n
                    JOIN %1$$I.station as st ON st.id=n.station
                    INNER JOIN %1$$I._link as l ON l.up=n.id
                    UNION
                    SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''STN''::varchar as element,
                        UPPER(st.name)::varchar as id_station,
                        st.id::integer as numero_station,
                        (ST_X(ST_Centroid(st.geom)))::numeric as xc_station,
                        (ST_Y(ST_Centroid(st.geom)))::numeric as yc_station,
                        (select count(1) from %1$$I.station_node as nod where nod.station=st.id)::integer as nb_noeud,
                        UPPER(n.name)::varchar as id_noeud,
                        (ST_X(n.geom))::numeric as x_noeud,
                        (ST_Y(n.geom))::numeric as y_noeud,
                        ''''::varchar as id_ouvrage,
                        ''''::varchar as type_ouvrage
                    FROM %1$$I.station_node as n
                    JOIN %1$$I.station as st ON st.id=n.station
                    WHERE n.id not in (select id from %1$$I._singularity) and n.id not in (select up from %1$$I._link)
                    ORDER by id_station, id_noeud, id_ouvrage
                ', model.name);
        END LOOP;
    end;
$$$$
;;

-- 2D
create function csv.elem_2d()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    x_noeud numeric,
    y_noeud numeric,
    surface real,
    z_noeud real,
    rk real,
    contour numeric[]
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''PAV''::varchar as element,
                        UPPER(n.name)::varchar as identifiant,
                        (ST_X(n.geom))::numeric as x_noeud,
                        (ST_Y(n.geom))::numeric as y_noeud,
                        n.area::real as surface_regard,
                        n.zb::real as z_noeud,
                        n.rk::real as rk,
                        csv.geometry_to_array(contour) as contour
                    FROM %1$$I.elem_2d_node as n
                    ORDER by n.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

-- Crossroads + streets
create function csv.street()
returns table (
    modele varchar,
    element varchar,
    id_rue varchar,
    numero_rue integer,
    largeur_rue real,
    rk_rue real,
    nb_carrefour integer,
    id_carrefour varchar,
    pk_carrefour real,
    x_carrefour numeric,
    y_carrefour numeric,
    id_ouvrage varchar,
    type_ouvrage varchar
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''RUE''::varchar as element,
                        UPPER(st.name)::varchar as id_rue,
                        st.id::integer as numero_rue,
                        st.width::real as largeur_rue,
                        st.rk::real as rk_rue,
                        (select count(1) from %1$$I.manhole_node as nod where ST_DWithin(nod.geom, st.geom, 0.1))::integer as nb_carrefour,
                        UPPER(n.name)::varchar as id_carrefour,
                        (ST_LineLocatePoint(st.geom, n.geom)*ST_Length(st.geom))::real as pk_carrefour,
                        (ST_X(n.geom))::numeric as x_carrefour,
                        (ST_Y(n.geom))::numeric as y_carrefour,
                        UPPER(s.name)::varchar as id_ouvrage,
                        UPPER(s.singularity_type::varchar)::varchar as type_ouvrage
                    FROM %1$$I.crossroad_node as n
                    LEFT JOIN %1$$I.street as st ON ST_DWithin(n.geom, st.geom, 0.1)
                    LEFT JOIN %1$$I._singularity as s ON s.node=n.id
                    ORDER by st.name, ST_LineLocatePoint(st.geom, n.geom)
                ', model.name);
        END LOOP;
    end;
$$$$
;;

-- Catchments contours + nodes
create function csv.catchment()
returns table (
    modele varchar,
    element varchar,
    id_contour varchar,
    nb_noeud integer,
    id_noeud varchar,
    x_noeud numeric,
    y_noeud numeric,
    superficie_ha real,
    longueur_m real,
    coefficient_d_impermeabilisation real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''BV''::varchar as element,
                        UPPER(c.name)::varchar as id_contour,
                        (select count(1) from %1$$I.catchment_node as nod where nod.contour=c.id)::integer as nb_noeud,
                        UPPER(n.name)::varchar as id_noeud,
                        (ST_X(n.geom))::numeric as x_noeud,
                        (ST_Y(n.geom))::numeric as y_noeud,
                        n.area_ha::real as superficie_ha,
                        n.rl::real as longueur_m,
                        n.c_imp::real as coefficient_d_impermeabilisation
                    FROM %1$$I.catchment_node as n
                    LEFT JOIN %1$$I.catchment as c ON c.id=n.contour
                    ORDER by c.name, n.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.storage()
returns table (
    modele varchar,
    element varchar,
    id_noeud varchar,
    x_noeud numeric,
    y_noeud numeric,
    z_initial real,
    zs_arr real[]
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''CAS''::varchar as element,
                        UPPER(n.name)::varchar as id_noeud,
                        (ST_X(n.geom))::numeric as x_noeud,
                        (ST_Y(n.geom))::numeric as y_noeud,
                        n.zini::real as z_initial,
                        n.zs_array as zs_arr
                    FROM %1$$I.storage_node as n
                    ORDER by n.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

/* ********************************************** */
/*  Export functions: GEOMETRIES                  */
/* ********************************************** */

create function csv.closed_parametric_geometry()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    zb_arr real[]
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''SECT_PF''::varchar as element,
                        UPPER(g.name)::varchar as identifiant,
                        g.zbmin_array as zb_arr
                    FROM %1$$I.closed_parametric_geometry as g
                    ORDER by g.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.open_parametric_geometry()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    zb_arr real[]
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''SECT_PO''::varchar as element,
                        UPPER(g.name)::varchar as identifiant,
                        g.zbmin_array as zb_arr
                    FROM %1$$I.open_parametric_geometry as g
                    ORDER by g.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.valley_cross_section_geometry()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    rmu1 real,
    rmu2 real,
    zlevee_gauche real,
    zlevee_droite real,
    zbmaj_lbank_array real[],
    zbmin_array real[],
    zbmaj_rbank_array real[]
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''SECT_VA''::varchar as element,
                        UPPER(g.name)::varchar as identifiant,
                        g.rmu1::real as rmu1,
                        g.rmu2::real as rmu2,
                        g.zlevee_lb::real as zlevee_gauche,
                        g.zlevee_rb::real as zlevee_droite,
                        g.zbmaj_lbank_array,
                        g.zbmin_array,
                        g.zbmaj_rbank_array
                    FROM %1$$I.valley_cross_section_geometry as g
                    ORDER by g.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

/* ********************************************** */
/*  Export functions: BOUNDARY CONDITIONS         */
/* ********************************************** */

create function csv.hydrograph_bc_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    surface_reseau_amont real,
    fichier_externe integer,
    mode_d_apport integer,
    q_sec real,
    id_crb_adimensionnel varchar,
    coef_multiplicateur real,
    temps_de_transfert real,
    id_secteur_apport varchar,
    tq_arr real[],
    modulation_horaire real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''HY''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        s.storage_area::real as surface_reseau_amont,
                        s.external_file_data::integer as fichier_externe,
                        null::integer as mode_d_apport,
                        s.constant_dry_flow::real as q_sec,
                        ''-''::varchar as id_crb_adimensionnel,
                        s.distrib_coef::real as coef_multiplicateur,
                        s.lag_time_hr::real as temps_de_transfert,
                        UPPER(dis.name)::varchar as id_secteur_apport,
                        s.tq_array as tq_arr,
                        s.hourly_modulation::real as modulation_horaire
                    FROM %1$$I.hydrograph_bc_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    LEFT JOIN project.dry_inflow_sector as dis ON dis.id=s.sector
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.constant_inflow_bc_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    debit_constant real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''HL''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        s.q0 as debit_constant
                    FROM %1$$I.constant_inflow_bc_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.tank_bc_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    zs_arr real[],
    zini real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''CLBO''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        s.zs_array as zs_arr,
                        s.zini::real as zini
                    FROM %1$$I.tank_bc_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.model_connect_bc_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    mode integer,
    qz_arr real[]
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''RACC''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        m.id::integer as mode,
                        csv.reverse_columns(s.zq_array) as qz_arr
                    FROM %1$$I.model_connect_bc_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    LEFT JOIN hydra.model_connect_mode as m ON m.name=s.cascade_mode
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.froude_bc_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''CLZF''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud
                    FROM %1$$I.froude_bc_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.strickler_bc_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    pente real,
    strickler real,
    largeur real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''CLZK''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        s.slope::real as pente,
                        s.k::real as strickler,
                        s.width::real as largeur
                    FROM %1$$I.strickler_bc_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.zq_bc_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    qz_arr real[]
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''CLZQ''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        csv.reverse_columns(s.zq_array) as qz_arr
                    FROM %1$$I.zq_bc_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.tz_bc_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    cyclique integer,
    tz_arr real[],
    fichier_externe integer
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''CLZT''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        s.cyclic::integer as cyclique,
                        s.tz_array as tz_arr,
                        s.external_file_data::integer as fichier_externe
                    FROM %1$$I.tz_bc_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.weir_bc_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    cote_seuil real,
    largeur real,
    coefficient_de_contraction real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''DL''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        s.z_weir::real as cote_seuil,
                        s.width::real as largeur,
                        s.cc::real as coefficient_de_contraction
                    FROM %1$$I.weir_bc_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;


/* ********************************************** */
/*  Export functions: SINGULARITIES               */
/* ********************************************** */

create function csv.bradley_headloss_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    butee_type varchar,
    dist_butee_gauche real,
    dist_butee_droite real,
    z_butee real,
    zw_arr real[]
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''BRAD''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        s.abutment_type::varchar as butee_type,
                        s.d_abutment_l::real as dist_butee_gauche,
                        s.d_abutment_r::real as dist_butee_droite,
                        s.z_ceiling::real as z_butee,
                        s.zw_array as zw_arr
                    FROM %1$$I.bradley_headloss_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.bridge_headloss_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    longueur_route real,
    z_route real,
    section_complete integer,
    zw_arr real[]
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''PONT''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        s.l_road::real as longueur_route,
                        s.z_road::real as z_route,
                        s.full_section_discharge_for_headloss::integer as section_complete,
                        s.zw_array as zw_arr
                    FROM %1$$I.bridge_headloss_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.hydraulic_cut_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    qz_arr real[],
    section_complete integer
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''CPR''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        s.qz_array as qz_arr,
                        s.full_section_discharge_for_headloss::integer as section_complete
                    FROM %1$$I.hydraulic_cut_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.param_headloss_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    qdz_arr real[],
    section_complete integer
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''DH''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        s.q_dz_array as qdz_arr,
                        s.full_section_discharge_for_headloss::integer as section_complete
                    FROM %1$$I.param_headloss_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.gate_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    z real,
    z_voute real,
    largeur real,
    mu_denoye real,
    mu_noye real,
    mode_d_action integer,
    mode_de_valve integer,
    z_vanne real,
    vitesse_max real,
    section_complete integer
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''VA''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        s.z_invert::real as z,
                        s.z_ceiling::real as z_voute,
                        s.width::real as largeur,
                        s.cc::real as mu_denoye,
                        s.cc_submerged::real as mu_noye,
                        ma.id::integer as mode_d_action,
                        mv.id::integer as mode_de_valve,
                        s.z_gate::real as z_vanne,
                        s.v_max_cms::real as vitesse_max,
                        s.full_section_discharge_for_headloss::integer as section_complete
                    FROM %1$$I.gate_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    LEFT JOIN hydra.action_gate_type as ma ON ma.name=s.action_gate_type
                    LEFT JOIN hydra.valve_mode as mv ON mv.name=s.mode_valve
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.regul_sluice_gate_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    z_radier real,
    z_voute real,
    largeur real,
    mu_denoye real,
    mu_noye real,
    mode_d_action integer,
    vitesse_max real,
    dt_regulation real,
    type_regulation integer,
    z_vanne real,
    noeud_controle varchar,
    z_critique real,
    z_radier_stop real,
    z_voute_stop real,
    p real,
    i real,
    d real,
    tz_arr real[],
    tq_arr real[],
    section_complete integer
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''RGB''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        s.z_invert::real as z_radier,
                        s.z_ceiling::real as z_voute,
                        s.width::real as largeur,
                        s.cc::real as mu_denoye,
                        s.cc_submerged::real as mu_noye,
                        ma.id::integer as mode_d_action,
                        s.v_max_cms::real as vitesse_max,
                        s.dt_regul_hr::real as dt_regulation,
                        mr.id::integer as type_regulation,
                        CASE
                            WHEN s.mode_regul::varchar=''no_regul'' THEN s.nr_z_gate::real
                            ELSE null END as z_vanne,
                        CASE
                            WHEN s.mode_regul::varchar=''elevation'' THEN UPPER(cn.name)::varchar
                            ELSE null END as noeud_controle,
                        CASE
                            WHEN s.mode_regul::varchar=''discharge'' THEN s.q_z_crit::real
                            ELSE null END as z_critique,
                        s.z_invert_stop::real as z_radier_stop,
                        s.z_ceiling_stop::real as z_voute_stop,
                        CASE
                            WHEN s.mode_regul::varchar=''elevation'' THEN s.z_pid_array[1]::real
                            ELSE null END as p,
                        CASE
                            WHEN s.mode_regul::varchar=''elevation'' THEN s.z_pid_array[2]::real
                            ELSE null END as i,
                        CASE
                            WHEN s.mode_regul::varchar=''elevation'' THEN s.z_pid_array[3]::real
                            ELSE null END as d,
                        CASE
                            WHEN s.mode_regul::varchar=''elevation'' THEN s.z_tz_array
                            ELSE null END as tz_arr,
                        CASE
                            WHEN s.mode_regul::varchar=''discharge'' THEN s.q_tq_array
                            ELSE null END as tq_arr,
                        s.full_section_discharge_for_headloss::integer as section_complete
                    FROM %1$$I.regul_sluice_gate_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    LEFT JOIN %1$$I._node as cn on cn.id=s.z_control_node
                    LEFT JOIN hydra.action_gate_type as ma ON ma.name=s.action_gate_type
                    LEFT JOIN hydra.gate_mode_regul as mr ON mr.name=s.mode_regul
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.zregul_weir_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    z_seuil real,
    z_regulation real,
    largeur real,
    coefficient_de_contraction real,
    mode_regulation integer,
    section_complete integer
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''DE''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        s.z_invert::real as z_seuil,
                        s.z_regul::real as z_regulation,
                        s.width::real as largeur,
                        s.cc::real as coefficient_de_contraction,
                        m.id::integer as mode_regulation,
                        s.full_section_discharge_for_headloss::integer as section_complete
                    FROM %1$$I.zregul_weir_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    LEFT JOIN hydra.weir_mode_regul as m ON m.name=s.mode_regul
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.borda_headloss_singularity()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    id_noeud varchar,
    loi integer,
    coef_kq2 real,
    reference_cs varchar,
    coef_kv_12 real,
    coef_kv_21 real,
    middle_option varchar,
    middle_cs_area real,
    transition_option varchar,
    half_angle real,
    curve_radius real,
    curve_angle real,
    wall_friction_option varchar,
    angle real,
    connector_no integer,
    ls real,
    blocage_coef real,
    v_angle real,
    deposition_option varchar,
    grid_shape varchar,
    length real,
    ks real,
    bend_shape varchar,
    a0b0 real,
    b1b0 real,
    l0b0 real,
    bkb0 real,
    qh_arr real[],
    section_complete integer
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''SMK''::varchar as element,
                        UPPER(s.name)::varchar as identifiant,
                        UPPER(n.name)::varchar as id_noeud,
                        b.id::integer as loi,
                        CASE
                            WHEN s.law_type::varchar=''q'' THEN (s.param::json->(s.law_type::varchar)->>''coef_kq2'')::real
                            ELSE null::real END as coef_kq2,
                        CASE
                            WHEN s.law_type::varchar=''v'' THEN (s.param::json->(s.law_type::varchar)->>''reference_cs'')::varchar
                            ELSE ''-''::varchar END as reference_cs,
                        CASE
                            WHEN s.law_type::varchar=''v'' THEN (s.param::json->(s.law_type::varchar)->>''coef_kv_12'')::real
                            ELSE null::real END as coef_kv_12,
                        CASE
                            WHEN s.law_type::varchar=''v'' THEN (s.param::json->(s.law_type::varchar)->>''coef_kv_21'')::real
                            ELSE null::real END as coef_kv_21,
                        CASE
                            WHEN s.law_type::varchar=''sharp_transition_geometry'' THEN (s.param::json->(s.law_type::varchar)->>''middle_option'')::varchar
                            ELSE ''-''::varchar END as middle_option,
                        CASE
                            WHEN s.law_type::varchar=''sharp_transition_geometry'' THEN (s.param::json->(s.law_type::varchar)->>''middle_cs_area'')::real
                            ELSE null::real END as middle_cs_area,
                        CASE
                            WHEN s.law_type::varchar in (''contraction_with_transition'', ''enlargment_with_transition'')
                            THEN (s.param::json->(s.law_type::varchar)->>''transition_option'')::varchar
                            ELSE ''-''::varchar END as transition_option,
                        CASE
                            WHEN s.law_type::varchar in (''contraction_with_transition'', ''enlargment_with_transition'')
                            THEN (s.param::json->(s.law_type::varchar)->>''half_angle'')::real
                            ELSE null::real END as half_angle,
                        CASE
                            WHEN s.law_type::varchar=''circular_bend'' THEN (s.param::json->(s.law_type::varchar)->>''curve_radius'')::real
                            ELSE null::real END as curve_radius,
                        CASE
                            WHEN s.law_type::varchar=''circular_bend'' THEN (s.param::json->(s.law_type::varchar)->>''curve_angle'')::real
                            ELSE null::real END as curve_angle,
                        CASE
                            WHEN s.law_type::varchar in (''circular_bend'', ''sharp_angle_bend'')
                            THEN (s.param::json->(s.law_type::varchar)->>''wall_friction_option'')::varchar
                            ELSE ''-''::varchar END as wall_friction_option,
                        CASE
                            WHEN s.law_type::varchar=''sharp_angle_bend'' THEN (s.param::json->(s.law_type::varchar)->>''angle'')::real
                            ELSE null::real END as angle,
                        CASE
                            WHEN s.law_type::varchar=''sharp_angle_bend'' THEN (s.param::json->(s.law_type::varchar)->>''connector_no'')::integer
                            ELSE null::integer END as connector_no,
                        CASE
                            WHEN s.law_type::varchar=''sharp_angle_bend'' THEN (s.param::json->(s.law_type::varchar)->>''ls'')::real
                            ELSE null::real END as ls,
                        CASE
                            WHEN s.law_type::varchar in (''screen_normal_to_flow'', ''screen_oblique_to_flow'')
                            THEN (s.param::json->(s.law_type::varchar)->>''blocage_coef'')::real
                            ELSE null::real END as blocage_coef,
                        CASE
                            WHEN s.law_type::varchar in (''screen_normal_to_flow'', ''screen_oblique_to_flow'')
                            THEN (s.param::json->(s.law_type::varchar)->>''v_angle'')::real
                            ELSE null::real END as v_angle,
                        CASE
                            WHEN s.law_type::varchar in (''screen_normal_to_flow'', ''screen_oblique_to_flow'')
                            THEN (s.param::json->(s.law_type::varchar)->>''deposition_option'')::varchar
                            ELSE ''-''::varchar END as deposition_option,
                        CASE
                            WHEN s.law_type::varchar=''screen_normal_to_flow'' THEN (s.param::json->(s.law_type::varchar)->>''grid_shape'')::varchar
                            ELSE ''-''::varchar END as grid_shape,
                        CASE
                            WHEN s.law_type::varchar=''friction'' THEN (s.param::json->(s.law_type::varchar)->>''length'')::real
                            ELSE null::real END as length,
                        CASE
                            WHEN s.law_type::varchar=''friction'' THEN (s.param::json->(s.law_type::varchar)->>''ks'')::real
                            ELSE null::real END as ks,
                        CASE
                            WHEN s.law_type::varchar=''sharp_bend_rectangular'' THEN (s.param::json->(s.law_type::varchar)->>''bend_shape'')::varchar
                            ELSE ''-''::varchar END as bend_shape,
                        CASE
                            WHEN s.law_type::varchar=''sharp_bend_rectangular'' THEN (s.param::json->(s.law_type::varchar)->>''param1'')::real
                            ELSE null::real END as a0b0,
                        CASE
                            WHEN s.law_type::varchar=''sharp_bend_rectangular''
                            AND (s.param::json->(s.law_type::varchar)->>''bend_shape'')::varchar=''elbow_90_deg''
                            THEN (s.param::json->(s.law_type::varchar)->>''param2'')::real
                            WHEN s.law_type::varchar=''sharp_bend_rectangular''
                            AND (s.param::json->(s.law_type::varchar)->>''bend_shape'')::varchar=''u_shaped_elbow''
                            THEN (s.param::json->(s.law_type::varchar)->>''param4'')::real
                            ELSE null::real END as b1b0,
                        CASE
                            WHEN s.law_type::varchar=''sharp_bend_rectangular''
                            AND (s.param::json->(s.law_type::varchar)->>''bend_shape'')::varchar in (''z_shaped_elbow'', ''u_shaped_elbow'', ''two_elbows_in_distinct_planes'')
                            THEN (s.param::json->(s.law_type::varchar)->>''param2'')::real
                            ELSE null::real END as l0b0,
                        CASE
                            WHEN s.law_type::varchar=''sharp_bend_rectangular''
                            AND (s.param::json->(s.law_type::varchar)->>''bend_shape'')::varchar=''u_shaped_elbow''
                            THEN (s.param::json->(s.law_type::varchar)->>''param3'')::real
                            ELSE null::real END as bkb0,
                        CASE
                            WHEN s.law_type::varchar=''parametric'' THEN translate(s.param::json->(s.law_type::varchar)->>''qh_array'', ''[]'', ''{}'')::real[]
                            ELSE null::real[] END as qh_arr,
                        s.full_section_discharge_for_headloss::integer as section_complete
                    FROM %1$$I.borda_headloss_singularity as s
                    LEFT JOIN %1$$I._node as n ON s.node=n.id
                    LEFT JOIN hydra.borda_headloss_singularity_type as b ON b.name=s.law_type
                    ORDER by s.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

/* ********************************************** */
/*  Export functions: LINKS                       */
/* ********************************************** */

--Pipe_links
--Circular
create function csv.pipe_link_ci()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    noeud_amont varchar,
    noeud_aval varchar,
    zam real,
    zav real,
    longueur real,
    rk real,
    diametre real,
    type varchar,
    branche integer,
    h_sable real,
    v_utile real,
    v_sediment real,
    v_total real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''CI''::varchar as element,
                        UPPER(l.name)::varchar as identifiant,
                        UPPER(amn.name)::varchar as noeud_amont,
                        UPPER(avn.name)::varchar as noeud_aval,
                        l.z_invert_up::real as zam,
                        l.z_invert_down::real as zav,
                        CASE
                            WHEN l.custom_length is null THEN ST_length(l.geom)::real
                            ELSE custom_length::real END as longueur,
                        l.rk::real as rk,
                        l.circular_diameter::real as diametre,
                        ''-''::varchar as type,
                        l.branch::integer as branche,
                        l.h_sable::real as h_sable,
                        (project.area_circular(l.circular_diameter, l.h_sable)*coalesce(l.custom_length, st_length(l.geom)))::real v_utile,
                        ((project.area_circular(l.circular_diameter, 0) - project.area_circular(l.circular_diameter, l.h_sable))*coalesce(l.custom_length, st_length(l.geom)))::real v_sediment,
                        (project.area_circular(l.circular_diameter, 0)*coalesce(l.custom_length, st_length(l.geom)))::real v_total
                    FROM %1$$I.pipe_link as l
                    LEFT JOIN %1$$I._node as amn on amn.id=l.up
                    LEFT JOIN %1$$I._node as avn on avn.id=l.down
                    WHERE l.cross_section_type=''circular''
                        and amn.node_type=''manhole''
                        and avn.node_type=''manhole''
                    ORDER by l.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

--Ovoid
create function csv.pipe_link_ov()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    noeud_amont varchar,
    noeud_aval varchar,
    zam real,
    zav real,
    longueur real,
    rk real,
    h_sable real,
    diametre_sup real,
    hauteur real,
    diametre_inf real,
    type varchar,
    branche integer,
    v_utile real,
    v_sediment real,
    v_total real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''OV''::varchar as element,
                        UPPER(l.name)::varchar as identifiant,
                        UPPER(amn.name)::varchar as noeud_amont,
                        UPPER(avn.name)::varchar as noeud_aval,
                        l.z_invert_up::real as zam,
                        l.z_invert_down::real as zav,
                        CASE
                            WHEN l.custom_length is null THEN ST_length(l.geom)::real
                            ELSE custom_length::real END as longueur,
                        l.rk::real as rk,
                        l.h_sable::real as h_sable,
                        l.ovoid_top_diameter::real as diametre_sup,
                        l.ovoid_height::real as hauteur,
                        l.ovoid_invert_diameter::real as diametre_inf,
                        ''-''::varchar as type,
                        l.branch::integer as branche,
                        (project.area_ovoid(l.ovoid_height, l.ovoid_invert_diameter, l.ovoid_top_diameter, l.h_sable)*coalesce(l.custom_length, st_length(l.geom)))::real v_utile,
                        ((project.area_ovoid(l.ovoid_height, l.ovoid_invert_diameter, l.ovoid_top_diameter, 0) - project.area_ovoid(l.ovoid_height, l.ovoid_invert_diameter, l.ovoid_top_diameter, l.h_sable))*coalesce(l.custom_length, st_length(l.geom)))::real v_sediment,
                        (project.area_ovoid(l.ovoid_height, l.ovoid_invert_diameter, l.ovoid_top_diameter, 0)*coalesce(l.custom_length, st_length(l.geom)))::real v_total
                    FROM %1$$I.pipe_link as l
                    LEFT JOIN %1$$I._node as amn on amn.id=l.up
                    LEFT JOIN %1$$I._node as avn on avn.id=l.down
                    WHERE l.cross_section_type=''ovoid''
                        and amn.node_type=''manhole''
                        and avn.node_type=''manhole''
                    ORDER by l.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

--Closed parametric geometry
create function csv.pipe_link_cp()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    noeud_amont varchar,
    noeud_aval varchar,
    zam real,
    zav real,
    longueur real,
    rk real,
    h_sable real,
    id_section varchar,
    branche integer,
    v_utile real,
    v_sediment real,
    v_total real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''PF''::varchar as element,
                        UPPER(l.name)::varchar as identifiant,
                        UPPER(amn.name)::varchar as noeud_amont,
                        UPPER(avn.name)::varchar as noeud_aval,
                        l.z_invert_up::real as zam,
                        l.z_invert_down::real as zav,
                        CASE
                            WHEN l.custom_length is null THEN ST_length(l.geom)::real
                            ELSE custom_length::real END as longueur,
                        l.rk::real as rk,
                        l.h_sable::real as h_sable,
                        (select cp.name from %1$$I.closed_parametric_geometry as cp where cp.id=l.cp_geom)::varchar as id_section,
                        l.branch::integer as branche,
                        (project.area_pipe(g.zbmin_array, l.h_sable)*coalesce(l.custom_length, st_length(l.geom)))::real v_utile,
                        ((project.area_pipe(g.zbmin_array, 0) - project.area_pipe(g.zbmin_array, l.h_sable))*coalesce(l.custom_length, st_length(l.geom)))::real v_sediment,
                        (project.area_pipe(g.zbmin_array, 0)*coalesce(l.custom_length, st_length(l.geom)))::real v_total
                    FROM %1$$I.pipe_link as l
                    LEFT JOIN %1$$I._node as amn on amn.id=l.up
                    LEFT JOIN %1$$I._node as avn on avn.id=l.down
                    LEFT JOIN %1$$I.closed_parametric_geometry as g on g.id=l.cp_geom
                    WHERE l.cross_section_type=''pipe''
                        and amn.node_type=''manhole''
                        and avn.node_type=''manhole''
                    ORDER by l.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

--Open parametric geometry
create function csv.pipe_link_op()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    noeud_amont varchar,
    noeud_aval varchar,
    zam real,
    zav real,
    longueur real,
    rk real,
    h_sable real,
    nom_geometrie varchar,
    branche integer,
    v_utile real,
    v_sediment real,
    v_total real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''PO''::varchar as element,
                        UPPER(l.name)::varchar as identifiant,
                        UPPER(amn.name)::varchar as noeud_amont,
                        UPPER(avn.name)::varchar as noeud_aval,
                        l.z_invert_up::real as zam,
                        l.z_invert_down::real as zav,
                        CASE
                            WHEN l.custom_length is null THEN ST_length(l.geom)::real
                            ELSE custom_length::real END as longueur,
                        l.rk::real as rk,
                        l.h_sable::real as h_sable,
                        (select op.name from %1$$I.open_parametric_geometry as op where op.id=l.op_geom)::varchar as nom_geometrie,
                        l.branch::integer as branche,
                        (project.area_channel(g.zbmin_array, l.h_sable)*coalesce(l.custom_length, st_length(l.geom)))::real v_utile,
                        ((project.area_channel(g.zbmin_array, 0) - project.area_channel(g.zbmin_array, l.h_sable))*coalesce(l.custom_length, st_length(l.geom)))::real v_sediment,
                        (project.area_channel(g.zbmin_array, 0)*coalesce(l.custom_length, st_length(l.geom)))::real v_total
                    FROM %1$$I.pipe_link as l
                    LEFT JOIN %1$$I._node as amn on amn.id=l.up
                    LEFT JOIN %1$$I._node as avn on avn.id=l.down
                    LEFT JOIN %1$$I.open_parametric_geometry as g on g.id=l.cp_geom
                    WHERE l.cross_section_type=''channel''
                        and amn.node_type=''manhole''
                        and avn.node_type=''manhole''
                    ORDER by l.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.deriv_pump_link()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    noeud_amont varchar,
    noeud_aval varchar,
    qini real,
    zq_arr real[]
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''QD''::varchar as element,
                        UPPER(l.name)::varchar as identifiant,
                        UPPER(amn.name)::varchar as noeud_amont,
                        UPPER(avn.name)::varchar as noeud_aval,
                        q_pump::real as qini,
                        qz_array as zq_arr
                    FROM %1$$I.deriv_pump_link as l
                    LEFT JOIN %1$$I._node as amn on amn.id=l.up
                    LEFT JOIN %1$$I._node as avn on avn.id=l.down
                    ORDER by l.name asc
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.gate_link()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    noeud_amont varchar,
    noeud_aval varchar,
    z real,
    z_voute real,
    largeur real,
    mu_denoye real,
    mu_noye real,
    mode_d_action integer,
    mode_de_valve integer,
    z_vanne real,
    vitesse_max real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''QMV''::varchar as element,
                        UPPER(l.name)::varchar as identifiant,
                        UPPER(amn.name)::varchar as noeud_amont,
                        UPPER(avn.name)::varchar as noeud_aval,
                        l.z_invert::real as z,
                        l.z_ceiling::real as z_voute,
                        l.width::real as largeur,
                        l.cc::real as mu_denoye,
                        l.cc_submerged::real as mu_noye,
                        ma.id::integer as mode_d_action,
                        mv.id::integer as mode_de_valve,
                        l.z_gate::real as z_vanne,
                        l.v_max_cms::real as vitesse_max
                    FROM %1$$I.gate_link as l
                    LEFT JOIN %1$$I._node as amn on amn.id=l.up
                    LEFT JOIN %1$$I._node as avn on avn.id=l.down
                    LEFT JOIN hydra.action_gate_type as ma ON ma.name=l.action_gate_type
                    LEFT JOIN hydra.valve_mode as mv ON mv.name=l.mode_valve
                    ORDER by l.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.regul_gate_link()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    noeud_amont varchar,
    noeud_aval varchar,
    z real,
    z_voute real,
    largeur real,
    mu_denoye real,
    mu_noye real,
    mode_d_action integer,
    vitesse_max real,
    dt_regulation real,
    type_regulation integer,
    z_vanne real,
    noeud_controle varchar,
    z_critique real,
    z_radier_stop real,
    z_voute_stop real,
    p real,
    i real,
    d real,
    tz_arr real[],
    tq_arr real[]
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''RG''::varchar as element,
                        UPPER(l.name)::varchar as identifiant,
                        UPPER(amn.name)::varchar as noeud_amont,
                        UPPER(avn.name)::varchar as noeud_aval,
                        l.z_invert::real as z,
                        l.z_ceiling::real as z_voute,
                        l.width::real as largeur,
                        l.cc::real as mu_denoye,
                        l.cc_submerged::real as mu_noye,
                        ma.id::integer as mode_d_action,
                        l.v_max_cms::real as vitesse_max,
                        l.dt_regul_hr::real as dt_regulation,
                        mr.id::integer as type_regulation,
                        CASE
                            WHEN l.mode_regul::varchar=''no_regul'' THEN l.nr_z_gate::real
                            ELSE null END as z_vanne,
                        CASE
                            WHEN l.mode_regul::varchar=''elevation'' THEN UPPER(cn.name)::varchar
                            ELSE null END as noeud_controle,
                        CASE
                            WHEN l.mode_regul::varchar=''discharge'' THEN l.q_z_crit::real
                            ELSE null END as z_critique,
                        l.z_invert_stop::real as z_radier_stop,
                        l.z_ceiling_stop::real as z_voute_stop,
                        CASE
                            WHEN l.mode_regul::varchar=''elevation'' THEN l.z_pid_array[1]::real
                            ELSE null END as p,
                        CASE
                            WHEN l.mode_regul::varchar=''elevation'' THEN l.z_pid_array[2]::real
                            ELSE null END as i,
                        CASE
                            WHEN l.mode_regul::varchar=''elevation'' THEN l.z_pid_array[3]::real
                            ELSE null END as d,
                        CASE
                            WHEN l.mode_regul::varchar=''elevation'' THEN l.z_tz_array
                            ELSE null END as tz_arr,
                        CASE
                            WHEN l.mode_regul::varchar=''discharge'' THEN l.q_tq_array
                            ELSE null END as tq_arr
                    FROM %1$$I.regul_gate_link as l
                    LEFT JOIN %1$$I._node as amn on amn.id=l.up
                    LEFT JOIN %1$$I._node as avn on avn.id=l.down
                    LEFT JOIN %1$$I._node as cn on cn.id=l.z_control_node
                    LEFT JOIN hydra.action_gate_type as ma ON ma.name=l.action_gate_type
                    LEFT JOIN hydra.gate_mode_regul as mr ON mr.name=l.mode_regul
                    ORDER by l.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.weir_link()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    noeud_amont varchar,
    noeud_aval varchar,
    z_butee_basse real,
    largeur_seuil real,
    mu_denoye real,
    z_seuil real,
    vitesse_max real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''QMS''::varchar as element,
                        UPPER(l.name)::varchar as identifiant,
                        UPPER(amn.name)::varchar as noeud_amont,
                        UPPER(avn.name)::varchar as noeud_aval,
                        z_invert::real as z_butee_basse,
                        width::real as largeur_seuil,
                        cc::real as mu_denoye,
                        z_weir::real as z_seuil,
                        v_max_cms::real as vitesse_max
                    FROM %1$$I.weir_link as l
                    LEFT JOIN %1$$I._node as amn on amn.id=l.up
                    LEFT JOIN %1$$I._node as avn on avn.id=l.down
                    ORDER by l.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.pump_link()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    noeud_amont varchar,
    noeud_aval varchar,
    n_pompe integer,
    zz_arr real[],
    hq_arr real[]
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''QMP''::varchar as element,
                        UPPER(l.name)::varchar as identifiant,
                        UPPER(amn.name)::varchar as noeud_amont,
                        UPPER(avn.name)::varchar as noeud_aval,
                        l.npump::integer as n_pompe,
                        l.zregul_array as zz_arr,
                        l.hq_array as hq_arr
                    FROM %1$$I.pump_link as l
                    LEFT JOIN %1$$I._node as amn on amn.id=l.up
                    LEFT JOIN %1$$I._node as avn on avn.id=l.down
                    ORDER by l.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.connector_link()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    noeud_amont varchar,
    noeud_aval varchar
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''QMTR''::varchar as element,
                        UPPER(l.name)::varchar as identifiant,
                        UPPER(amn.name)::varchar as noeud_amont,
                        UPPER(avn.name)::varchar as noeud_aval
                    FROM %1$$I.connector_link as l
                    LEFT JOIN %1$$I._node as amn on amn.id=l.up
                    LEFT JOIN %1$$I._node as avn on avn.id=l.down
                    ORDER by l.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.mesh_2d_link()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    noeud_amont varchar,
    noeud_aval varchar,
    cote real,
    cc real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''LPAV''::varchar as element,
                        UPPER(l.name)::varchar as identifiant,
                        UPPER(amn.name)::varchar as noeud_amont,
                        UPPER(avn.name)::varchar as noeud_aval,
                        l.z_invert::real as cote,
                        l.lateral_contraction_coef::real as cc
                    FROM %1$$I.mesh_2d_link as l
                    LEFT JOIN %1$$I._node as amn on amn.id=l.up
                    LEFT JOIN %1$$I._node as avn on avn.id=l.down
                    ORDER by l.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.porous_link()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    noeud_amont varchar,
    noeud_aval varchar,
    cote real,
    largeur real,
    transmitivite real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''LPOR''::varchar as element,
                        UPPER(l.name)::varchar as identifiant,
                        UPPER(amn.name)::varchar as noeud_amont,
                        UPPER(avn.name)::varchar as noeud_aval,
                        l.z_invert::real as cote,
                        l.width::real as largeur,
                        l.transmitivity::real as transmitivite
                    FROM %1$$I.porous_link as l
                    LEFT JOIN %1$$I._node as amn on amn.id=l.up
                    LEFT JOIN %1$$I._node as avn on avn.id=l.down
                    ORDER by l.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.overflow_link()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    noeud_amont varchar,
    noeud_aval varchar,
    cote1 real,
    largeur1 real,
    cote2 real,
    largeur2 real,
    mu_denoye real,
    coefficient_de_contraction real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''LOV''::varchar as element,
                        UPPER(l.name)::varchar as identifiant,
                        UPPER(amn.name)::varchar as noeud_amont,
                        UPPER(avn.name)::varchar as noeud_aval,
                        l.z_crest1::real as cote1,
                        l.width1::real as largeur1,
                        l.z_crest2::real as cote2,
                        l.width2::real as largeur2,
                        l.cc::real as mu_denoye,
                        l.lateral_contraction_coef::real as coefficient_de_contraction
                    FROM %1$$I.overflow_link as l
                    LEFT JOIN %1$$I._node as amn on amn.id=l.up
                    LEFT JOIN %1$$I._node as avn on avn.id=l.down
                    ORDER by l.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.network_overflow_link()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    noeud_amont varchar,
    noeud_aval varchar,
    cote real,
    surface real
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''LNOV''::varchar as element,
                        UPPER(l.name)::varchar as identifiant,
                        UPPER(amn.name)::varchar as noeud_amont,
                        UPPER(avn.name)::varchar as noeud_aval,
                        l.z_overflow::real as cote,
                        l.area::real as surface
                    FROM %1$$I.network_overflow_link as l
                    LEFT JOIN %1$$I._node as amn on amn.id=l.up
                    LEFT JOIN %1$$I._node as avn on avn.id=l.down
                    ORDER by l.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;

create function csv.borda_headloss_link()
returns table (
    modele varchar,
    element varchar,
    identifiant varchar,
    noeud_amont varchar,
    noeud_aval varchar,
    loi integer,
    coef_kq2 real,
    up_geom varchar,
    up_cs_area real,
    up_cs_zinvert real,
    down_geom varchar,
    down_cs_area real,
    down_cs_zinvert real,
    reference_cs varchar,
    coef_kv_12 real,
    coef_kv_21 real,
    outlet_cs_area real,
    outlet_shape varchar,
    inlet_cs_area real,
    inlet_shape varchar,
    angle real,
    entrance_shape varchar,
    length real,
    half_angle real,
    diffus_geom varchar,
    middle_option varchar,
    middle_cs_area real,
    transition_option varchar,
    curve_radius real,
    curve_angle real,
    wall_friction_option varchar,
    ls real,
    connector_no integer,
    obstruction_rate real,
    blocage_coef real,
    v_angle real,
    deposition_option varchar,
    grid_shape varchar,
    grid_shape_oblique varchar,
    ks real,
    bend_shape varchar,
    a0b0 real,
    b1b0 real,
    l0b0 real,
    bkb0 real,
    qh_arr real[]
)
language plpgsql
as $$$$
    declare
        model record;
    begin
        for model in select name from project.model
        loop
          return query execute
            format('SELECT
                        UPPER(%1$$L)::varchar as modele,
                        ''QMK''::varchar as element,
                        UPPER(l.name)::varchar as identifiant,
                        UPPER(amn.name)::varchar as noeud_amont,
                        UPPER(avn.name)::varchar as noeud_aval,
                        b.id::integer as loi,
                        CASE
                            WHEN l.law_type::varchar=''q'' THEN (l.param::json->(l.law_type::varchar)->>''coef_kq2'')::real
                            ELSE null::real END as coef_kq2,
                        CASE
                            WHEN l.law_type::varchar in (''v'', ''lateral_outlet'', ''bifurcation'', ''junction'', ''diffuser'', ''sharp_transition_geometry'',
                                ''contraction_with_transition'', ''enlargment_with_transition'', ''circular_bend'', ''sharp_angle_bend'', ''screen_normal_to_flow'',
                                ''friction'', ''sharp_bend_rectangular'', ''t_shape_bifurcation'', ''t_shape_junction'')
                            THEN (l.param::json->(l.law_type::varchar)->>''up_geom'')::varchar
                            ELSE ''-''::varchar END as up_geom,
                        CASE
                            WHEN l.law_type::varchar in (''v'', ''lateral_outlet'', ''bifurcation'', ''junction'', ''diffuser'', ''sharp_transition_geometry'',
                                ''contraction_with_transition'', ''enlargment_with_transition'', ''circular_bend'', ''sharp_angle_bend'', ''screen_normal_to_flow'',
                                ''friction'', ''sharp_bend_rectangular'', ''t_shape_bifurcation'', ''t_shape_junction'')
                            THEN (l.param::json->(l.law_type::varchar)->>''up_cs_area'')::real
                            ELSE null::real END as up_cs_area,
                        CASE
                            WHEN l.law_type::varchar in (''v'', ''lateral_outlet'', ''bifurcation'', ''junction'', ''diffuser'', ''sharp_transition_geometry'',
                                ''contraction_with_transition'', ''enlargment_with_transition'', ''circular_bend'', ''sharp_angle_bend'', ''screen_normal_to_flow'',
                                ''friction'', ''sharp_bend_rectangular'', ''t_shape_bifurcation'', ''t_shape_junction'')
                            THEN (l.param::json->(l.law_type::varchar)->>''up_cs_z_invert'')::real
                            ELSE null::real END as up_cs_zinvert,
                        CASE
                            WHEN l.law_type::varchar in (''v'', ''lateral_inlet'', ''bifurcation'', ''junction'', ''transition_edge_inlet'', ''sharp_transition_geometry'',
                                ''contraction_with_transition'', ''enlargment_with_transition'', ''sharp_bend_rectangular'', ''t_shape_bifurcation'', ''t_shape_junction'')
                            THEN (l.param::json->(l.law_type::varchar)->>''down_geom'')::varchar
                            ELSE ''-''::varchar END as down_geom,
                        CASE
                            WHEN l.law_type::varchar in (''v'', ''lateral_inlet'', ''bifurcation'', ''junction'', ''transition_edge_inlet'', ''sharp_transition_geometry'',
                                ''contraction_with_transition'', ''enlargment_with_transition'', ''sharp_bend_rectangular'', ''t_shape_bifurcation'', ''t_shape_junction'')
                            THEN (l.param::json->(l.law_type::varchar)->>''down_cs_area'')::real
                            ELSE null::real END as down_cs_area,
                        CASE
                            WHEN l.law_type::varchar in (''v'', ''lateral_inlet'', ''bifurcation'', ''junction'', ''transition_edge_inlet'', ''sharp_transition_geometry'',
                                ''contraction_with_transition'', ''enlargment_with_transition'', ''sharp_bend_rectangular'', ''t_shape_bifurcation'', ''t_shape_junction'')
                            THEN (l.param::json->(l.law_type::varchar)->>''down_cs_zinvert'')::real
                            ELSE null::real END as down_cs_zinvert,
                        CASE
                            WHEN l.law_type::varchar=''v'' THEN (l.param::json->(l.law_type::varchar)->>''reference_cs'')::varchar
                            ELSE ''-''::varchar END as reference_cs,
                        CASE
                            WHEN l.law_type::varchar=''v'' THEN (l.param::json->(l.law_type::varchar)->>''coef_kv_12'')::real
                            ELSE null::real END as coef_kv_12,
                        CASE
                            WHEN l.law_type::varchar=''v'' THEN (l.param::json->(l.law_type::varchar)->>''coef_kv_21'')::real
                            ELSE null::real END as coef_kv_21,
                        CASE
                            WHEN l.law_type::varchar=''lateral_outlet'' THEN (l.param::json->(l.law_type::varchar)->>''outlet_cs_area'')::real
                            ELSE null::real END as outlet_cs_area,
                        CASE
                            WHEN l.law_type::varchar=''lateral_outlet'' THEN (l.param::json->(l.law_type::varchar)->>''outlet_shape'')::varchar
                            ELSE ''-''::varchar END as outlet_shape,
                        CASE
                            WHEN l.law_type::varchar=''lateral_inlet'' THEN (l.param::json->(l.law_type::varchar)->>''inlet_cs_area'')::real
                            ELSE null::real END as inlet_cs_area,
                        CASE
                            WHEN l.law_type::varchar=''lateral_inlet'' THEN (l.param::json->(l.law_type::varchar)->>''inlet_shape'')::varchar
                            ELSE ''-''::varchar END as inlet_shape,
                        CASE
                            WHEN l.law_type::varchar in (''bifurcation'', ''junction'', ''transition_edge_inlet'')
                            THEN (l.param::json->(l.law_type::varchar)->>''angle'')::real
                            ELSE null::real END as angle,
                        CASE
                            WHEN l.law_type::varchar=''transition_edge_inlet'' THEN (l.param::json->(l.law_type::varchar)->>''entrance_shape'')::varchar
                            ELSE ''-''::varchar END as entrance_shape,
                        CASE
                            WHEN l.law_type::varchar in (''diffuser'', ''friction'')
                            THEN (l.param::json->(l.law_type::varchar)->>''length'')::real
                            ELSE null::real END as length,
                        CASE
                            WHEN l.law_type::varchar in (''diffuser'', ''contraction_with_transition'', ''enlargment_with_transition'')
                            THEN (l.param::json->(l.law_type::varchar)->>''half_angle'')::real
                            ELSE null::real END as half_angle,
                        CASE
                            WHEN l.law_type::varchar=''diffuser'' THEN (l.param::json->(l.law_type::varchar)->>''diffus_geom'')::varchar
                            ELSE ''-''::varchar END as diffus_geom,
                        CASE
                            WHEN l.law_type::varchar=''sharp_transition_geometry'' THEN (l.param::json->(l.law_type::varchar)->>''middle_option'')::varchar
                            ELSE ''-''::varchar END as middle_option,
                        CASE
                            WHEN l.law_type::varchar=''sharp_transition_geometry'' THEN (l.param::json->(l.law_type::varchar)->>''middle_cs_area'')::real
                            ELSE null::real END as middle_cs_area,
                        CASE
                            WHEN l.law_type::varchar in (''contraction_with_transition'', ''enlargment_with_transition'')
                            THEN (l.param::json->(l.law_type::varchar)->>''transition_option'')::varchar
                            ELSE ''-''::varchar END as transition_option,
                        CASE
                            WHEN l.law_type::varchar=''circular_bend'' THEN (l.param::json->(l.law_type::varchar)->>''curve_radius'')::real
                            ELSE null::real END as curve_radius,
                        CASE
                            WHEN l.law_type::varchar=''circular_bend'' THEN (l.param::json->(l.law_type::varchar)->>''curve_angle'')::real
                            ELSE null::real END as curve_angle,
                        CASE
                            WHEN l.law_type::varchar in (''circular_bend'', ''sharp_angle_bend'')
                            THEN (l.param::json->(l.law_type::varchar)->>''wall_friction_option'')::varchar
                            ELSE ''-''::varchar END as wall_friction_option,
                        CASE
                            WHEN l.law_type::varchar=''sharp_angle_bend'' THEN (l.param::json->(l.law_type::varchar)->>''ls'')::real
                            ELSE null::real END as ls,
                        CASE
                            WHEN l.law_type::varchar=''sharp_angle_bend'' THEN (l.param::json->(l.law_type::varchar)->>''connector_no'')::integer
                            ELSE null::integer END as connector_no,
                        CASE
                            WHEN l.law_type::varchar=''screen_normal_to_flow'' THEN (l.param::json->(l.law_type::varchar)->>''obstruction_rate'')::real
                            ELSE null::real END as obstruction_rate,
                        CASE
                            WHEN l.law_type::varchar=''screen_oblique_to_flow'' THEN (l.param::json->(l.law_type::varchar)->>''blocage_coef'')::real
                            ELSE null::real END as blocage_coef,
                        CASE
                            WHEN l.law_type::varchar=''screen_normal_to_flow'' THEN (l.param::json->(l.law_type::varchar)->>''v_angle'')::real
                            ELSE null::real END as v_angle,
                        CASE
                            WHEN l.law_type::varchar=''screen_normal_to_flow'' THEN (l.param::json->(l.law_type::varchar)->>''deposition_option'')::varchar
                            ELSE ''-''::varchar END as deposition_option,
                        CASE
                            WHEN l.law_type::varchar=''screen_normal_to_flow'' THEN (l.param::json->(l.law_type::varchar)->>''grid_shape'')::varchar
                            ELSE ''-''::varchar END as grid_shape,
                        CASE
                            WHEN l.law_type::varchar=''screen_oblique_to_flow'' THEN (l.param::json->(l.law_type::varchar)->>''grid_shape_oblique'')::varchar
                            ELSE ''-''::varchar END as grid_shape_oblique,
                        CASE
                            WHEN l.law_type::varchar=''friction'' THEN (l.param::json->''friction''->>''ks'')::real
                            ELSE null::real END as ks,
                        CASE
                            WHEN l.law_type::varchar=''sharp_bend_rectangular'' THEN (l.param::json->(l.law_type::varchar)->>''bend_shape'')::varchar
                            ELSE ''-''::varchar END as bend_shape,
                        CASE
                            WHEN l.law_type::varchar=''sharp_bend_rectangular'' THEN (l.param::json->(l.law_type::varchar)->>''param1'')::real
                            ELSE null::real END as a0b0,
                        CASE
                            WHEN l.law_type::varchar=''sharp_bend_rectangular''
                            AND (l.param::json->(l.law_type::varchar)->>''bend_shape'')::varchar=''elbow_90_deg''
                            THEN (l.param::json->(l.law_type::varchar)->>''param2'')::real
                            WHEN l.law_type::varchar=''sharp_bend_rectangular''
                            AND (l.param::json->(l.law_type::varchar)->>''bend_shape'')::varchar=''u_shaped_elbow''
                            THEN (l.param::json->(l.law_type::varchar)->>''param4'')::real
                            ELSE null::real END as b1b0,
                        CASE
                            WHEN l.law_type::varchar=''sharp_bend_rectangular''
                            AND (l.param::json->(l.law_type::varchar)->>''bend_shape'')::varchar in (''z_shaped_elbow'', ''u_shaped_elbow'', ''two_elbows_in_distinct_planes'')
                            THEN (l.param::json->(l.law_type::varchar)->>''param2'')::real
                            ELSE null::real END as l0b0,
                        CASE
                            WHEN l.law_type::varchar=''sharp_bend_rectangular''
                            AND (l.param::json->(l.law_type::varchar)->>''bend_shape'')::varchar=''u_shaped_elbow''
                            THEN (l.param::json->(l.law_type::varchar)->>''param3'')::real
                            ELSE null::real END as bkb0,
                        CASE
                            WHEN l.law_type::varchar=''parametric'' THEN translate(l.param::json->(l.law_type::varchar)->>''qh_array'', ''[]'', ''{}'')::real[]
                            ELSE null::real[] END as qh_arr
                    FROM %1$$I.borda_headloss_link as l
                    LEFT JOIN %1$$I._node as amn on amn.id=l.up
                    LEFT JOIN %1$$I._node as avn on avn.id=l.down
                    LEFT JOIN hydra.borda_headloss_link_type as b ON b.name=l.law_type
                    ORDER by l.name
                ', model.name);
        END LOOP;
    end;
$$$$
;;
