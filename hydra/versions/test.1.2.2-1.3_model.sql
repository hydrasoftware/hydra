/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  Update version                                */
/* ********************************************** */

update $model.metadata set version = '1.3';
;;

/* ********************************************** */
/*            Constrain line spliting             */
/* ********************************************** */

create or replace function $model.split_constrain(constrain_id integer, point geometry('POINTZ', $srid))
returns boolean
language plpgsql
as $$$$
    declare
        _geom geometry('LINESTRINGZ', $srid);
        _elem_length real;
        _constrain_type hydra_constrain_type ;
        _link_attributes json;
    begin
        update $model.metadata set trigger_coverage=False;

        select geom, elem_length, constrain_type, link_attributes from $model.constrain where id=constrain_id into _geom, _elem_length, _constrain_type, _link_attributes;

        delete from $model.constrain where id=constrain_id;

        insert into $model.constrain(geom, elem_length, constrain_type, link_attributes)
            values (
                    ST_SetSRID((
                        ST_Dump(
                            ST_Split(
                                ST_SetSRID(ST_Snap(_geom, point,0.0001), $srid),
                                ST_SetSRID(point, $srid)
                                )
                            )
                        ).geom, $srid),
                    _elem_length,
                    _constrain_type,
                    _link_attributes
                    );

        update $model.metadata set trigger_coverage=True;
        perform $model.coverage_update();
    return true;
    end;
$$$$
;;

/* ********************************************** */
/*  Configuration update - River Cross sections   */
/* ********************************************** */

drop view if exists $model.river_cross_section_profile cascade;
;;

alter table $model._river_cross_section_profile add column if not exists configuration json;
;;

create or replace view $model.river_cross_section_profile as
    select g.id,
        c.name,
        c.z_invert_up,
        c.z_invert_down,
        c.type_cross_section_up,
        c.type_cross_section_down,
        c.up_rk,
        c.up_rk_maj,
        c.up_sinuosity,
        c.up_circular_diameter,
        c.up_ovoid_height,
        c.up_ovoid_top_diameter,
        c.up_ovoid_invert_diameter,
        c.up_cp_geom,
        c.up_op_geom,
        c.up_vcs_geom,
        c.up_vcs_topo_geom,
        c.down_rk,
        c.down_rk_maj,
        c.down_sinuosity,
        c.down_circular_diameter,
        c.down_ovoid_height,
        c.down_ovoid_top_diameter,
        c.down_ovoid_invert_diameter,
        c.down_cp_geom,
        c.down_op_geom,
        c.down_vcs_geom,
        c.down_vcs_topo_geom,
        c.configuration,
        (select case when (c.type_cross_section_up='channel' or c.type_cross_section_up='valley') then
            (select (c.z_invert_up))
        else (select st_z(st_startpoint(g.geom)))
        end) as z_tn_up,
        (select case when (c.type_cross_section_down='channel' or c.type_cross_section_down='valley') then
            (select (c.z_invert_down))
        else (select st_z(st_endpoint(g.geom)))
        end) as z_tn_down,
        (select case when c.type_cross_section_up='circular' then
            (select (c.z_invert_up+c.up_circular_diameter))
        when c.type_cross_section_up='ovoid' then
            (select (c.z_invert_up+c.up_ovoid_height))
        when c.type_cross_section_up='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.up_cp_geom = cpg.id)
            (select (c.z_invert_up + n.height) from n))
        when c.type_cross_section_up='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.up_op_geom = opg.id)
            (select (c.z_invert_up + n.height) from n))
        when c.type_cross_section_up='valley' then
            (with n as (select (zbmaj_lbank_array[1][1] - zbmin_array[1][1]) as height
                from ${model}.valley_cross_section_geometry opg
                where c.up_vcs_geom = opg.id)
            (select (c.z_invert_up + n.height) from n))
        else null
        end) as z_lbank_up,
        (select case when c.type_cross_section_down='circular' then
            (select (c.z_invert_down+c.down_circular_diameter))
        when c.type_cross_section_down='ovoid' then
            (select (c.z_invert_down+c.down_ovoid_height))
        when c.type_cross_section_down='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.down_cp_geom = cpg.id)
            (select (c.z_invert_down + n.height) from n))
        when c.type_cross_section_down='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.down_op_geom = opg.id)
            (select (c.z_invert_down + n.height) from n))
        when c.type_cross_section_down='valley' then
            (with n as (select (zbmaj_lbank_array[1][1] - zbmin_array[1][1]) as height
                from ${model}.valley_cross_section_geometry opg
                where c.down_vcs_geom = opg.id)
            (select (c.z_invert_down + n.height) from n))
        else null
        end) as z_lbank_down,
        (select case when c.type_cross_section_up='circular' then
            (select (c.z_invert_up+c.up_circular_diameter))
        when c.type_cross_section_up='ovoid' then
            (select (c.z_invert_up+c.up_ovoid_height))
        when c.type_cross_section_up='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.up_cp_geom = cpg.id)
            (select (c.z_invert_up + n.height) from n))
        when c.type_cross_section_up='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.up_op_geom = opg.id)
            (select (c.z_invert_up + n.height) from n))
        when c.type_cross_section_up='valley' then
            (with n as (select (zbmaj_rbank_array[1][1] - zbmin_array[1][1]) as height
                from ${model}.valley_cross_section_geometry opg
                where c.up_vcs_geom = opg.id)
            (select (c.z_invert_up + n.height) from n))
        else null
        end) as z_rbank_up,
        (select case when c.type_cross_section_down='circular' then
            (select (c.z_invert_down+c.down_circular_diameter))
        when c.type_cross_section_down='ovoid' then
            (select (c.z_invert_down+c.down_ovoid_height))
        when c.type_cross_section_down='pipe' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.closed_parametric_geometry cpg
                where c.down_cp_geom = cpg.id)
            (select (c.z_invert_down + n.height) from n))
        when c.type_cross_section_down='channel' then
            (with n as (select (zbmin_array[array_length(zbmin_array, 1)][1] - zbmin_array[1][1]) as height
                from ${model}.open_parametric_geometry opg
                where c.down_op_geom = opg.id)
            (select (c.z_invert_down + n.height) from n))
        when c.type_cross_section_down='valley' then
            (with n as (select (zbmaj_rbank_array[1][1] - zbmin_array[1][1]) as height
                from ${model}.valley_cross_section_geometry opg
                where c.down_vcs_geom = opg.id)
            (select (c.z_invert_down + n.height) from n))
        else null
        end) as z_rbank_down,
        c.validity,
        g.geom
    from $model._river_cross_section_profile as c, $model._node as g
    where g.id = c.id
;;

create or replace function ${model}.river_cross_section_profile_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        select (
            (new.type_cross_section_up is null or (
                new.z_invert_up is not null
                and new.up_rk is not null
                and (new.type_cross_section_up!='circular' or (new.up_circular_diameter is not null and new.up_circular_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_top_diameter is not null and new.up_ovoid_top_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_invert_diameter is not null and new.up_ovoid_invert_diameter > 0))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_height is not null and new.up_ovoid_height >0 ))
                and (new.type_cross_section_up!='ovoid' or (new.up_ovoid_height > (new.up_ovoid_top_diameter+new.up_ovoid_invert_diameter)/2))
                and (new.type_cross_section_up!='pipe' or new.up_cp_geom is not null)
                and (new.type_cross_section_up!='channel' or new.up_op_geom is not null)
                and (new.type_cross_section_up!='valley'
                        or (new.up_rk = 0 and new.up_rk_maj = 0 and new.up_sinuosity = 0)
                        or (new.up_rk >0 and new.up_rk_maj is not null and new.up_rk_maj > 0 and new.up_sinuosity is not null and new.up_sinuosity > 0))
                and (new.type_cross_section_up!='valley' or new.up_vcs_geom is not null)
                )
            )
            and
            (new.type_cross_section_down is null or (
                new.z_invert_down is not null
                and new.down_rk is not null
                and (new.type_cross_section_down!='circular' or (new.down_circular_diameter is not null and new.down_circular_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_top_diameter is not null and new.down_ovoid_top_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_invert_diameter is not null and new.down_ovoid_invert_diameter > 0))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_height is not null and new.down_ovoid_height >0 ))
                and (new.type_cross_section_down!='ovoid' or (new.down_ovoid_height > (new.down_ovoid_top_diameter+new.down_ovoid_invert_diameter)/2))
                and (new.type_cross_section_down!='pipe' or new.down_cp_geom is not null)
                and (new.type_cross_section_down!='channel' or new.down_op_geom is not null)
                and (new.type_cross_section_down!='valley'
                        or (new.down_rk = 0 and new.down_rk_maj = 0 and new.down_sinuosity = 0)
                        or (new.down_rk >0 and new.down_rk_maj is not null and new.down_rk_maj > 0 and new.down_sinuosity is not null and new.down_sinuosity > 0))
                and (new.type_cross_section_down!='valley' or new.down_vcs_geom is not null)
                )
            )
            and
            (new.type_cross_section_up is null or new.type_cross_section_down is null or (
                new.z_invert_up is not null and new.z_invert_down is not null
                )
            )
        ) into new.validity;

        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id from $model.river_node where ST_DWithin(new.geom, geom, .1) into new.id;
            select coalesce(new.name, 'CP'||new.id::varchar) into new.name;
            if new.id is null then
                raise exception 'no river node nearby river_cross_section_profile %', st_astext(new.geom);
            end if;

            insert into $model._river_cross_section_profile (
                    id,
                    name,
                    z_invert_up,
                    z_invert_down,
                    type_cross_section_up,
                    type_cross_section_down,
                    up_rk,
                    up_rk_maj,
                    up_sinuosity,
                    up_circular_diameter,
                    up_ovoid_height,
                    up_ovoid_top_diameter,
                    up_ovoid_invert_diameter,
                    up_cp_geom,
                    up_op_geom,
                    up_vcs_geom,
                    up_vcs_topo_geom,
                    down_rk,
                    down_rk_maj,
                    down_sinuosity,
                    down_circular_diameter,
                    down_ovoid_height,
                    down_ovoid_top_diameter,
                    down_ovoid_invert_diameter,
                    down_cp_geom,
                    down_op_geom,
                    down_vcs_geom,
                    down_vcs_topo_geom,
                    configuration,
                    validity
                )
                values (
                    new.id,
                    new.name,
                    new.z_invert_up,
                    new.z_invert_down,
                    new.type_cross_section_up,
                    new.type_cross_section_down,
                    new.up_rk,
                    new.up_rk_maj,
                    new.up_sinuosity,
                    new.up_circular_diameter,
                    new.up_ovoid_height,
                    new.up_ovoid_top_diameter,
                    new.up_ovoid_invert_diameter,
                    new.up_cp_geom,
                    new.up_op_geom,
                    new.up_vcs_geom,
                    new.up_vcs_topo_geom,
                    new.down_rk,
                    new.down_rk_maj,
                    new.down_sinuosity,
                    new.down_circular_diameter,
                    new.down_ovoid_height,
                    new.down_ovoid_top_diameter,
                    new.down_ovoid_invert_diameter,
                    new.down_cp_geom,
                    new.down_op_geom,
                    new.down_vcs_geom,
                    new.down_vcs_topo_geom,
                    new.configuration::json,
                    new.validity
                );

            perform $model.add_configuration_fct(new.configuration::json, new.id, 'river_cross_section_profile');

            return new;
        elsif tg_op = 'UPDATE' then
            /* find the node if not specified */
            select id from $model.river_node where ST_DWithin(new.geom, geom, .1) into new.id;
            select coalesce(new.name, 'CP'||new.id::varchar) into new.name;
            if new.id is null then
                raise exception 'no river node nearby river_cross_section_profile %', st_astext(new.geom);
            end if;

            -- Handle configurations
            if ((new.z_invert_up, new.z_invert_down,
                    new.type_cross_section_up, new.type_cross_section_down,
                    new.up_rk, new.up_rk_maj, new.up_sinuosity, new.up_circular_diameter,
                    new.up_ovoid_height, new.up_ovoid_top_diameter, new.up_ovoid_invert_diameter,
                    new.up_cp_geom, new.up_op_geom, new.up_vcs_geom, new.up_vcs_topo_geom,
                    new.down_rk, new.down_rk_maj, new.down_sinuosity, new.down_circular_diameter,
                    new.down_ovoid_height, new.down_ovoid_top_diameter, new.down_ovoid_invert_diameter,
                    new.down_cp_geom, new.down_op_geom, new.down_vcs_geom, new.down_vcs_topo_geom)
            is distinct from (old.z_invert_up, old.z_invert_down,
                    old.type_cross_section_up, old.type_cross_section_down,
                    old.up_rk, old.up_rk_maj, old.up_sinuosity, old.up_circular_diameter,
                    old.up_ovoid_height, old.up_ovoid_top_diameter, old.up_ovoid_invert_diameter,
                    old.up_cp_geom, old.up_op_geom, old.up_vcs_geom, old.up_vcs_topo_geom,
                    old.down_rk, old.down_rk_maj, old.down_sinuosity, old.down_circular_diameter,
                    old.down_ovoid_height, old.down_ovoid_top_diameter, old.down_ovoid_invert_diameter,
                    old.down_cp_geom, old.down_op_geom, old.down_vcs_geom, old.down_vcs_topo_geom))
            then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}'
                            from (select old.z_invert_up, old.z_invert_down,
                                old.type_cross_section_up, old.type_cross_section_down,
                                old.up_rk, old.up_rk_maj, old.up_sinuosity, old.up_circular_diameter,
                                old.up_ovoid_height, old.up_ovoid_top_diameter, old.up_ovoid_invert_diameter,
                                old.up_cp_geom, old.up_op_geom, old.up_vcs_geom, old.up_vcs_topo_geom,
                                old.down_rk, old.down_rk_maj, old.down_sinuosity, old.down_circular_diameter,
                                old.down_ovoid_height, old.down_ovoid_top_diameter, old.down_ovoid_invert_diameter,
                                old.down_cp_geom, old.down_op_geom, old.down_vcs_geom, old.down_vcs_topo_geom) as o,
                                (select new.z_invert_up, new.z_invert_down,
                                new.type_cross_section_up, new.type_cross_section_down,
                                new.up_rk, new.up_rk_maj, new.up_sinuosity, new.up_circular_diameter,
                                new.up_ovoid_height, new.up_ovoid_top_diameter, new.up_ovoid_invert_diameter,
                                new.up_cp_geom, new.up_op_geom, new.up_vcs_geom, new.up_vcs_topo_geom,
                                new.down_rk, new.down_rk_maj, new.down_sinuosity, new.down_circular_diameter,
                                new.down_ovoid_height, new.down_ovoid_top_diameter, new.down_ovoid_invert_diameter,
                                new.down_cp_geom, new.down_op_geom, new.down_vcs_geom, new.down_vcs_topo_geom) as n
                            into new_config;
                        update $model._river_cross_section_profile set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}'
                            from (select new.z_invert_up, new.z_invert_down,
                                new.type_cross_section_up, new.type_cross_section_down,
                                new.up_rk, new.up_rk_maj, new.up_sinuosity, new.up_circular_diameter,
                                new.up_ovoid_height, new.up_ovoid_top_diameter, new.up_ovoid_invert_diameter,
                                new.up_cp_geom, new.up_op_geom, new.up_vcs_geom, new.up_vcs_topo_geom,
                                new.down_rk, new.down_rk_maj, new.down_sinuosity, new.down_circular_diameter,
                                new.down_ovoid_height, new.down_ovoid_top_diameter, new.down_ovoid_invert_diameter,
                                new.down_cp_geom, new.down_op_geom, new.down_vcs_geom, new.down_vcs_topo_geom) n
                            into new_config;
                        update $model._river_cross_section_profile set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._river_cross_section_profile set
                id=new.id,
                name=new.name,
                z_invert_up=new.z_invert_up,
                z_invert_down=new.z_invert_down,
                type_cross_section_up=new.type_cross_section_up,
                type_cross_section_down=new.type_cross_section_down,
                up_rk=new.up_rk,
                up_rk_maj=new.up_rk_maj,
                up_sinuosity=new.up_sinuosity,
                up_circular_diameter=new.up_circular_diameter,
                up_ovoid_height=new.up_ovoid_height,
                up_ovoid_top_diameter=new.up_ovoid_top_diameter,
                up_ovoid_invert_diameter=new.up_ovoid_invert_diameter,
                up_cp_geom=new.up_cp_geom,
                up_op_geom=new.up_op_geom,
                up_vcs_geom=new.up_vcs_geom,
                up_vcs_topo_geom=new.up_vcs_topo_geom,
                down_rk=new.down_rk,
                down_rk_maj=new.down_rk_maj,
                down_sinuosity=new.down_sinuosity,
                down_circular_diameter=new.down_circular_diameter,
                down_ovoid_height=new.down_ovoid_height,
                down_ovoid_top_diameter=new.down_ovoid_top_diameter,
                down_ovoid_invert_diameter=new.down_ovoid_invert_diameter,
                down_cp_geom=new.down_cp_geom,
                down_op_geom=new.down_op_geom,
                down_vcs_geom=new.down_vcs_geom,
                down_vcs_topo_geom=new.down_vcs_topo_geom,
                validity=new.validity
            where id=old.id;

            perform $model.add_configuration_fct(new.configuration::json, new.id, 'river_cross_section_profile');

            return new;
        end if;
    end;
$$$$
;;

drop trigger if exists ${model}_river_cross_section_profile_trig on $model.river_cross_section_profile;
;;

create trigger ${model}_river_cross_section_profile_trig
    instead of insert or update on $model.river_cross_section_profile
       for each row execute procedure ${model}.river_cross_section_profile_fct()
;;

create or replace function ${model}.river_cross_section_profile_del_fct()
returns trigger
language plpgsql
as $$$$
    begin
        delete from $model._river_cross_section_profile where id=old.id;
        return old;
    end;
$$$$
;;

drop trigger if exists ${model}_river_cross_section_profile_del_trig on $model.river_cross_section_profile;
;;

create trigger ${model}_river_cross_section_profile_del_trig
    instead of delete on $model.river_cross_section_profile
       for each row execute procedure ${model}.river_cross_section_profile_del_fct()
;;

/* ********************************************************** */
/*  Configurations - Trigger on geom deletion                 */
/* ********************************************************** */

create or replace function $model.closed_parametric_geometry_before_del_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config record;
    begin
        for config in select name from $model.configuration loop
            update $model._link
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', cp_geom}')::text[], 'null'::jsonb))::json
            where link_type='pipe' and (configuration->config.name->'cp_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', up_cp_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'up_cp_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', down_cp_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'down_cp_geom')::text=old.id::text;
        end loop;
        return old;
    end;
$$$$
;;

drop trigger if exists ${model}_closed_parametric_geometry_trig on $model.closed_parametric_geometry;
;;

create trigger ${model}_closed_parametric_geometry_trig
    before delete on $model.closed_parametric_geometry
       for each row execute procedure ${model}.closed_parametric_geometry_before_del_fct()
;;

create or replace function $model.open_parametric_geometry_before_del_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config record;
    begin
        for config in select name from $model.configuration loop
            update $model._link
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', op_geom}')::text[], 'null'::jsonb))::json
            where link_type='pipe' and (configuration->config.name->'op_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', up_op_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'up_op_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', down_op_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'down_op_geom')::text=old.id::text;
        end loop;
        return old;
    end;
$$$$
;;

drop trigger if exists ${model}_open_parametric_geometry_trig on $model.open_parametric_geometry;
;;

create trigger ${model}_open_parametric_geometry_trig
    before delete on $model.open_parametric_geometry
       for each row execute procedure ${model}.open_parametric_geometry_before_del_fct()
;;

create or replace function $model.valley_cross_section_geometry_before_del_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config record;
    begin
        for config in select name from $model.configuration loop
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', up_vcs_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'up_vcs_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', down_vcs_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'down_vcs_geom')::text=old.id::text;
        end loop;
        return old;
    end;
$$$$
;;

drop trigger if exists ${model}_valley_cross_section_geometry_trig on $model.valley_cross_section_geometry;
;;

create trigger ${model}_valley_cross_section_geometry_trig
    before delete on $model.valley_cross_section_geometry
       for each row execute procedure ${model}.valley_cross_section_geometry_before_del_fct()
;;

create or replace function $model.valley_cross_section_topo_geometry_before_del_fct()
returns trigger
language plpgsql
as $$$$
    declare
        config record;
    begin
        for config in select name from $model.configuration loop
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', up_vcs_topo_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'up_vcs_topo_geom')::text=old.id::text;
            update $model._river_cross_section_profile
            set configuration=(jsonb_set(configuration::jsonb, ('{'||config.name||', down_vcs_topo_geom}')::text[], 'null'::jsonb))::json
            where (configuration->config.name->'down_vcs_topo_geom')::text=old.id::text;
        end loop;
        return old;
    end;
$$$$
;;

drop trigger if exists ${model}_valley_cross_section_topo_geometry_trig on $model.valley_cross_section_topo_geometry;
;;

create trigger ${model}_valley_cross_section_topo_geometry_trig
    before delete on $model.valley_cross_section_topo_geometry
       for each row execute procedure ${model}.valley_cross_section_topo_geometry_before_del_fct()
;;

/* ********************************************** */
/*     Discharge for headloss computation         */
/* ********************************************** */

-- Borda headloss
drop view if exists $model.borda_headloss_singularity cascade;
;;

alter table $model._borda_headloss_singularity add column if not exists full_section_discharge_for_headloss boolean default 't';
;;

create or replace view $model.borda_headloss_singularity as
    select p.id,
        p.name,
        n.id as node,
        c.law_type,
        c.param::character varying as param,
        c.full_section_discharge_for_headloss,
        c.param as param_json,
        n.geom,
        p.configuration::character varying as configuration,
        p.validity,
        p.configuration as configuration_json
    from $model._borda_headloss_singularity c,
        $model._singularity p,
        $model._node n
    where p.id = c.id and n.id = p.node;
;;

create or replace function $model.borda_headloss_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'borda_headloss', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'borda_headloss') where name = 'define_later' and id = id_;

            insert into $model._borda_headloss_singularity
                values (id_, 'borda_headloss', new.law_type, new.param::json, coalesce(new.full_section_discharge_for_headloss, 't'));
            if 'borda_headloss' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'borda_headloss_singularity');
            update $model._singularity set validity = (select (law_type is not null) and (param is not null) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._borda_headloss_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.law_type, new.param, new.full_section_discharge_for_headloss) is distinct from (old.law_type, old.param, old.full_section_discharge_for_headloss)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.law_type, old.param, old.full_section_discharge_for_headloss) as o, (select new.law_type, new.param, new.full_section_discharge_for_headloss) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.law_type, new.param, new.full_section_discharge_for_headloss) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._borda_headloss_singularity set law_type=new.law_type, param=new.param::json, full_section_discharge_for_headloss=new.full_section_discharge_for_headloss where id=old.id;
            if 'borda_headloss' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'borda_headloss_singularity');
            update $model._singularity set validity = (select (law_type is not null) and (param is not null) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._borda_headloss_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._borda_headloss_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'borda_headloss' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$$$
;;

drop trigger if exists ${model}_borda_headloss_singularity_trig on $model.borda_headloss_singularity;
;;

create trigger ${model}_borda_headloss_singularity_trig
    instead of insert or update or delete on $model.borda_headloss_singularity
       for each row execute procedure ${model}.borda_headloss_singularity_fct()
;;

-- Bridge headloss
drop view if exists $model.bridge_headloss_singularity cascade;
;;

alter table $model._bridge_headloss_singularity add column if not exists full_section_discharge_for_headloss boolean default 't';
;;

create or replace view $model.bridge_headloss_singularity as
    select p.id,
        p.name,
        n.id as node,
        c.l_road,
        c.z_road,
        c.zw_array,
        c.full_section_discharge_for_headloss,
        n.geom,
        p.configuration::character varying as configuration,
        p.validity,
        p.configuration as configuration_json
    from $model._bridge_headloss_singularity c,
        $model._singularity p,
        $model._node n
    where p.id = c.id and n.id = p.node;
;;

create or replace function $model.bridge_headloss_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'bridge_headloss', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'bridge_headloss') where name = 'define_later' and id = id_;

            insert into $model._bridge_headloss_singularity
                values (id_, 'bridge_headloss', new.l_road, new.z_road, new.zw_array, coalesce(new.full_section_discharge_for_headloss, 't'));
            if 'bridge_headloss' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'bridge_headloss_singularity');
            update $model._singularity set validity = (select (l_road is not null) and (l_road>=0) and (z_road is not null) and (zw_array is not null ) and (array_length(zw_array, 1)<=10) and (array_length(zw_array, 1)>=1) and (array_length(zw_array, 2)=2) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._bridge_headloss_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.l_road, new.z_road, new.zw_array, new.full_section_discharge_for_headloss) is distinct from (old.l_road, old.z_road, old.zw_array, old.full_section_discharge_for_headloss)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.l_road, old.z_road, old.zw_array, old.full_section_discharge_for_headloss) as o, (select new.l_road, new.z_road, new.zw_array, new.full_section_discharge_for_headloss) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.l_road, new.z_road, new.zw_array, new.full_section_discharge_for_headloss) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._bridge_headloss_singularity set l_road=new.l_road, z_road=new.z_road, zw_array=new.zw_array, full_section_discharge_for_headloss=new.full_section_discharge_for_headloss where id=old.id;
            if 'bridge_headloss' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'bridge_headloss_singularity');
            update $model._singularity set validity = (select (l_road is not null) and (l_road>=0) and (z_road is not null) and (zw_array is not null ) and (array_length(zw_array, 1)<=10) and (array_length(zw_array, 1)>=1) and (array_length(zw_array, 2)=2) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._bridge_headloss_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._bridge_headloss_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'bridge_headloss' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$$$
;;

drop trigger if exists ${model}_bridge_headloss_singularity_trig on $model.bridge_headloss_singularity;
;;

create trigger ${model}_bridge_headloss_singularity_trig
    instead of insert or update or delete on $model.bridge_headloss_singularity
       for each row execute procedure ${model}.bridge_headloss_singularity_fct()
;;

-- Gate
drop view if exists $model.gate_singularity cascade;
;;

alter table $model._gate_singularity add column if not exists full_section_discharge_for_headloss boolean default 't';
;;

create or replace view $model.gate_singularity as
    select p.id,
        p.name,
        n.id as node,
        c.z_invert,
        c.z_ceiling,
        c.width,
        c.cc,
        c.action_gate_type,
        c.mode_valve,
        c.z_gate,
        c.v_max_cms,
        c.full_section_discharge_for_headloss,
        n.geom,
        p.configuration::character varying as configuration,
        p.validity,
        p.configuration as configuration_json
    from $model._gate_singularity c,
        $model._singularity p,
        $model._node n
    where p.id = c.id and n.id = p.node;
;;

create or replace function $model.gate_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'gate', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'gate') where name = 'define_later' and id = id_;

            insert into $model._gate_singularity
                values (id_, 'gate', new.z_invert, new.z_ceiling, new.width, coalesce(new.cc, .6), coalesce(new.action_gate_type, 'upward_opening'), coalesce(new.mode_valve, 'no_valve'), coalesce(new.z_gate, new.z_ceiling), coalesce(new.v_max_cms, .2), coalesce(new.full_section_discharge_for_headloss, 't'));
            if 'gate' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'gate_singularity');
            update $model._singularity set validity = (select (z_invert is not null) and (z_ceiling is not null) and (z_ceiling>z_invert) and (width is not null) and (cc is not null) and (cc <=1) and (cc >=0) and (action_gate_type is not null) and (mode_valve is not null) and (z_gate is not null) and (z_gate>=z_invert) and (z_gate<=z_ceiling) and (v_max_cms is not null) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._gate_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.mode_valve, new.z_gate, new.v_max_cms, new.full_section_discharge_for_headloss) is distinct from (old.z_invert, old.z_ceiling, old.width, old.cc, old.action_gate_type, old.mode_valve, old.z_gate, old.v_max_cms, old.full_section_discharge_for_headloss)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.z_ceiling, old.width, old.cc, old.action_gate_type, old.mode_valve, old.z_gate, old.v_max_cms, old.full_section_discharge_for_headloss) as o, (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.mode_valve, new.z_gate, new.v_max_cms, new.full_section_discharge_for_headloss) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.mode_valve, new.z_gate, new.v_max_cms, new.full_section_discharge_for_headloss) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._gate_singularity set z_invert=new.z_invert, z_ceiling=new.z_ceiling, width=new.width, cc=new.cc, action_gate_type=new.action_gate_type, mode_valve=new.mode_valve, z_gate=new.z_gate, v_max_cms=new.v_max_cms, full_section_discharge_for_headloss=new.full_section_discharge_for_headloss where id=old.id;
            if 'gate' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'gate_singularity');
            update $model._singularity set validity = (select (z_invert is not null) and (z_ceiling is not null) and (z_ceiling>z_invert) and (width is not null) and (cc is not null) and (cc <=1) and (cc >=0) and (action_gate_type is not null) and (mode_valve is not null) and (z_gate is not null) and (z_gate>=z_invert) and (z_gate<=z_ceiling) and (v_max_cms is not null) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._gate_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._gate_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'gate' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$$$
;;

drop trigger if exists ${model}_gate_singularity_trig on $model.gate_singularity;
;;

create trigger ${model}_gate_singularity_trig
    instead of insert or update or delete on $model.gate_singularity
       for each row execute procedure ${model}.gate_singularity_fct()
;;

-- Hydraulic cut headloss
drop view if exists $model.hydraulic_cut_singularity cascade;
;;

alter table $model._hydraulic_cut_singularity add column if not exists full_section_discharge_for_headloss boolean default 't';
;;

create or replace view $model.hydraulic_cut_singularity as
    select p.id,
        p.name,
        n.id AS node,
        c.qz_array,
        c.full_section_discharge_for_headloss,
        n.geom,
        p.configuration::character varying as configuration,
        p.validity,
        p.configuration as configuration_json
    from $model._hydraulic_cut_singularity c,
        $model._singularity p,
        $model._node n
    where p.id = c.id and n.id = p.node;
;;

create or replace function $model.hydraulic_cut_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'hydraulic_cut', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'hydraulic_cut') where name = 'define_later' and id = id_;

            insert into $model._hydraulic_cut_singularity
                values (id_, 'hydraulic_cut', new.qz_array, coalesce(new.full_section_discharge_for_headloss, 't'));
            if 'hydraulic_cut' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'hydraulic_cut_singularity');
            update $model._singularity set validity = (select (qz_array is not null) and (array_length(qz_array, 1)<=10) and (array_length(qz_array, 1)>=1) and (array_length(qz_array, 2)=2) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._hydraulic_cut_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.qz_array, new.full_section_discharge_for_headloss) is distinct from (old.qz_array, old.full_section_discharge_for_headloss)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.qz_array, old.full_section_discharge_for_headloss) as o, (select new.qz_array, new.full_section_discharge_for_headloss) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.qz_array, new.full_section_discharge_for_headloss) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._hydraulic_cut_singularity set qz_array=new.qz_array, full_section_discharge_for_headloss=new.full_section_discharge_for_headloss where id=old.id;
            if 'hydraulic_cut' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'hydraulic_cut_singularity');
            update $model._singularity set validity = (select (qz_array is not null) and (array_length(qz_array, 1)<=10) and (array_length(qz_array, 1)>=1) and (array_length(qz_array, 2)=2) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._hydraulic_cut_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._hydraulic_cut_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'hydraulic_cut' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$$$
;;

drop trigger if exists ${model}_hydraulic_cut_singularity_trig on $model.hydraulic_cut_singularity;
;;

create trigger ${model}_hydraulic_cut_singularity_trig
    instead of insert or update or delete on $model.hydraulic_cut_singularity
       for each row execute procedure ${model}.hydraulic_cut_singularity_fct()
;;

-- Param headloss
drop view if exists $model.param_headloss_singularity cascade;
;;

alter table $model._param_headloss_singularity add column if not exists full_section_discharge_for_headloss boolean default 't';
;;

create or replace view $model.param_headloss_singularity as
    select p.id,
        p.name,
        n.id as node,
        c.q_dz_array,
        c.full_section_discharge_for_headloss,
        n.geom,
        p.configuration::character varying as configuration,
        p.validity,
        p.configuration as configuration_json
    from $model._param_headloss_singularity c,
        $model._singularity p,
        $model._node n
    where p.id = c.id and n.id = p.node;
;;

create or replace function $model.param_headloss_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'param_headloss', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'param_headloss') where name = 'define_later' and id = id_;

            insert into $model._param_headloss_singularity
                values (id_, 'param_headloss', new.q_dz_array, coalesce(new.full_section_discharge_for_headloss, 't'));
            if 'param_headloss' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'param_headloss_singularity');
            update $model._singularity set validity = (select (q_dz_array is not null) and (array_length(q_dz_array, 1)<=10) and (array_length(q_dz_array, 1)>=1) and (array_length(q_dz_array, 2)=2) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._param_headloss_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.q_dz_array, new.full_section_discharge_for_headloss) is distinct from (old.q_dz_array, old.full_section_discharge_for_headloss)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.q_dz_array, old.full_section_discharge_for_headloss) as o, (select new.q_dz_array, new.full_section_discharge_for_headloss) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.q_dz_array, new.full_section_discharge_for_headloss) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._param_headloss_singularity set q_dz_array=new.q_dz_array, full_section_discharge_for_headloss=new.full_section_discharge_for_headloss where id=old.id;
            if 'param_headloss' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'param_headloss_singularity');
            update $model._singularity set validity = (select (q_dz_array is not null) and (array_length(q_dz_array, 1)<=10) and (array_length(q_dz_array, 1)>=1) and (array_length(q_dz_array, 2)=2) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._param_headloss_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._param_headloss_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'param_headloss' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$$$
;;

drop trigger if exists ${model}_param_headloss_singularity_trig on $model.param_headloss_singularity;
;;

create trigger ${model}_param_headloss_singularity_trig
    instead of insert or update or delete on $model.param_headloss_singularity
       for each row execute procedure ${model}.param_headloss_singularity_fct()
;;

-- Regul gate
drop view if exists $model.regul_sluice_gate_singularity cascade;
;;

alter table $model._regul_sluice_gate_singularity add column if not exists full_section_discharge_for_headloss boolean default 't';
;;

create or replace view $model.regul_sluice_gate_singularity as
    select p.id,
        p.name,
        n.id as node,
        c.z_invert,
        c.z_ceiling,
        c.width,
        c.cc,
        c.action_gate_type,
        c.z_invert_stop,
        c.z_ceiling_stop,
        c.v_max_cms,
        c.dt_regul_hr,
        c.mode_regul,
        c.z_control_node,
        c.z_pid_array,
        c.z_tz_array,
        c.q_z_crit,
        c.q_tq_array,
        c.nr_z_gate,
        c.full_section_discharge_for_headloss,
        n.geom,
        p.configuration::character varying as configuration,
        p.validity,
        p.configuration as configuration_json
    from $model._regul_sluice_gate_singularity c,
        $model._singularity p,
        $model._node n
    where p.id = c.id and n.id = p.node;
;;

create or replace function $model.regul_sluice_gate_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'regul_sluice_gate', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'regul_sluice_gate') where name = 'define_later' and id = id_;

            insert into $model._regul_sluice_gate_singularity
                values (id_, 'regul_sluice_gate', new.z_invert, new.z_ceiling, new.width, coalesce(new.cc, .6), coalesce(new.action_gate_type, 'upward_opening'), coalesce(new.z_invert_stop, new.z_invert), coalesce(new.z_ceiling_stop, new.z_ceiling), coalesce(new.v_max_cms, .5), coalesce(new.dt_regul_hr, 0), coalesce(new.mode_regul, 'elevation'), new.z_control_node, coalesce(new.z_pid_array, '{1, 0, 0}'::real[]), new.z_tz_array, new.q_z_crit, new.q_tq_array, coalesce(new.nr_z_gate, new.z_ceiling), coalesce(new.full_section_discharge_for_headloss, 't'));
            if 'regul_sluice_gate' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'regul_sluice_gate_singularity');
            update $model._singularity set validity = (select (z_invert is not null) and (z_ceiling is not null) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (action_gate_type is not null) and (z_invert_stop is not null) and (z_ceiling_stop is not null) and (v_max_cms is not null) and (v_max_cms>=0) and (dt_regul_hr is not null) and (mode_regul!='elevation' or z_control_node is not null) and (mode_regul!='elevation' or z_pid_array is not null) and (mode_regul!='elevation' or z_tz_array is not null) and (mode_regul!='elevation' or array_length(z_tz_array, 1)<=10) and (mode_regul!='elevation' or array_length(z_tz_array, 1)>=1) and (mode_regul!='elevation' or array_length(z_tz_array, 2)=2) and (mode_regul!='discharge' or q_z_crit is not null) and (mode_regul!='discharge' or array_length(q_tq_array, 1)<=10) and (mode_regul!='discharge' or array_length(q_tq_array, 1)>=1) and (mode_regul!='discharge' or array_length(q_tq_array, 2)=2) and (mode_regul!='no_regulation' or nr_z_gate is not null) from  $model._regul_sluice_gate_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.z_invert_stop, new.z_ceiling_stop, new.v_max_cms, new.dt_regul_hr, new.mode_regul, new.z_control_node, new.z_pid_array, new.z_tz_array, new.q_z_crit, new.q_tq_array, new.nr_z_gate, new.full_section_discharge_for_headloss) is distinct from (old.z_invert, old.z_ceiling, old.width, old.cc, old.action_gate_type, old.z_invert_stop, old.z_ceiling_stop, old.v_max_cms, old.dt_regul_hr, old.mode_regul, old.z_control_node, old.z_pid_array, old.z_tz_array, old.q_z_crit, old.q_tq_array, old.nr_z_gate, old.full_section_discharge_for_headloss)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.z_ceiling, old.width, old.cc, old.action_gate_type, old.z_invert_stop, old.z_ceiling_stop, old.v_max_cms, old.dt_regul_hr, old.mode_regul, old.z_control_node, old.z_pid_array, old.z_tz_array, old.q_z_crit, old.q_tq_array, old.nr_z_gate, old.full_section_discharge_for_headloss) as o, (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.z_invert_stop, new.z_ceiling_stop, new.v_max_cms, new.dt_regul_hr, new.mode_regul, new.z_control_node, new.z_pid_array, new.z_tz_array, new.q_z_crit, new.q_tq_array, new.nr_z_gate, new.full_section_discharge_for_headloss) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.z_invert_stop, new.z_ceiling_stop, new.v_max_cms, new.dt_regul_hr, new.mode_regul, new.z_control_node, new.z_pid_array, new.z_tz_array, new.q_z_crit, new.q_tq_array, new.nr_z_gate, new.full_section_discharge_for_headloss) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._regul_sluice_gate_singularity set z_invert=new.z_invert, z_ceiling=new.z_ceiling, width=new.width, cc=new.cc, action_gate_type=new.action_gate_type, z_invert_stop=new.z_invert_stop, z_ceiling_stop=new.z_ceiling_stop, v_max_cms=new.v_max_cms, dt_regul_hr=new.dt_regul_hr, mode_regul=new.mode_regul, z_control_node=new.z_control_node, z_pid_array=new.z_pid_array, z_tz_array=new.z_tz_array, q_z_crit=new.q_z_crit, q_tq_array=new.q_tq_array, nr_z_gate=new.nr_z_gate, full_section_discharge_for_headloss=new.full_section_discharge_for_headloss where id=old.id;
            if 'regul_sluice_gate' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'regul_sluice_gate_singularity');
            update $model._singularity set validity = (select (z_invert is not null) and (z_ceiling is not null) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (action_gate_type is not null) and (z_invert_stop is not null) and (z_ceiling_stop is not null) and (v_max_cms is not null) and (v_max_cms>=0) and (dt_regul_hr is not null) and (mode_regul!='elevation' or z_control_node is not null) and (mode_regul!='elevation' or z_pid_array is not null) and (mode_regul!='elevation' or z_tz_array is not null) and (mode_regul!='elevation' or array_length(z_tz_array, 1)<=10) and (mode_regul!='elevation' or array_length(z_tz_array, 1)>=1) and (mode_regul!='elevation' or array_length(z_tz_array, 2)=2) and (mode_regul!='discharge' or q_z_crit is not null) and (mode_regul!='discharge' or array_length(q_tq_array, 1)<=10) and (mode_regul!='discharge' or array_length(q_tq_array, 1)>=1) and (mode_regul!='discharge' or array_length(q_tq_array, 2)=2) and (mode_regul!='no_regulation' or nr_z_gate is not null) from  $model._regul_sluice_gate_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._regul_sluice_gate_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'regul_sluice_gate' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$$$
;;

drop trigger if exists ${model}_regul_sluice_gate_singularity_trig on $model.regul_sluice_gate_singularity;
;;

create trigger ${model}_regul_sluice_gate_singularity_trig
    instead of insert or update or delete on $model.regul_sluice_gate_singularity
       for each row execute procedure ${model}.regul_sluice_gate_singularity_fct()
;;

-- Z regul weir
drop view if exists $model.zregul_weir_singularity cascade;
;;

alter table $model._zregul_weir_singularity add column if not exists full_section_discharge_for_headloss boolean default 't';
;;

create or replace view $model.zregul_weir_singularity as
    select p.id,
        p.name,
        n.id as node,
        c.z_invert,
        c.z_regul,
        c.width,
        c.cc,
        c.mode_regul,
        c.reoxy_law,
        c.reoxy_param::character varying as reoxy_param,
        c.full_section_discharge_for_headloss,
        c.reoxy_param as reoxy_param_json,
        n.geom,
        p.configuration::character varying as configuration,
        p.validity,
        p.configuration as configuration_json
    from $model._zregul_weir_singularity c,
        $model._singularity p,
        $model._node n
    where p.id = c.id and n.id = p.node;
;;

create or replace function $model.zregul_weir_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'zregul_weir', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'zregul_weir') where name = 'define_later' and id = id_;

            insert into $model._zregul_weir_singularity
                values (id_, 'zregul_weir', new.z_invert, new.z_regul, new.width, coalesce(new.cc, .6), coalesce(new.mode_regul, 'elevation'), coalesce(new.reoxy_law, 'gameson'), new.reoxy_param::json, coalesce(new.full_section_discharge_for_headloss, 't'));
            if 'zregul_weir' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, 'zregul_weir_singularity');
            update $model._singularity set validity = (select (z_invert is not null ) and (z_regul is not null ) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (mode_regul is not null) and (reoxy_law is not null) and (reoxy_param is not null) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._zregul_weir_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.z_invert, new.z_regul, new.width, new.cc, new.mode_regul, new.reoxy_law, new.reoxy_param, new.full_section_discharge_for_headloss) is distinct from (old.z_invert, old.z_regul, old.width, old.cc, old.mode_regul, old.reoxy_law, old.reoxy_param, old.full_section_discharge_for_headloss)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.z_regul, old.width, old.cc, old.mode_regul, old.reoxy_law, old.reoxy_param, old.full_section_discharge_for_headloss) as o, (select new.z_invert, new.z_regul, new.width, new.cc, new.mode_regul, new.reoxy_law, new.reoxy_param, new.full_section_discharge_for_headloss) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.z_regul, new.width, new.cc, new.mode_regul, new.reoxy_law, new.reoxy_param, new.full_section_discharge_for_headloss) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._zregul_weir_singularity set z_invert=new.z_invert, z_regul=new.z_regul, width=new.width, cc=new.cc, mode_regul=new.mode_regul, reoxy_law=new.reoxy_law, reoxy_param=new.reoxy_param::json, full_section_discharge_for_headloss=new.full_section_discharge_for_headloss where id=old.id;
            if 'zregul_weir' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'zregul_weir_singularity');
            update $model._singularity set validity = (select (z_invert is not null ) and (z_regul is not null ) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (mode_regul is not null) and (reoxy_law is not null) and (reoxy_param is not null) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) from  $model._zregul_weir_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._zregul_weir_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            if 'zregul_weir' = 'pipe_branch_marker' and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$$$
;;

drop trigger if exists ${model}_zregul_weir_singularity_trig on $model.zregul_weir_singularity;
;;

create trigger ${model}_zregul_weir_singularity_trig
    instead of insert or update or delete on $model.zregul_weir_singularity
       for each row execute procedure ${model}.zregul_weir_singularity_fct()
;;

/* ********************************************** */
/*  Configurations                                */
/* ********************************************** */

create or replace function $model.metadata_configuration_after_update_fct()
returns trigger
language plpgsql
as $$$$
    begin
        -- Unpack all configured items in default config
        perform $model.unpack_config(1);
        -- Unpack configured items for new config only (on top of all items in default)
        perform $model.unpack_config(new.configuration);
        return new;
    end;
$$$$
;;

create or replace function ${model}.unpack_config(config_id integer)
returns integer
language plpgsql
as $$$$
    declare
        _res integer := 0;
        node record;
        singularity record;
        link record;
        profile record;
        update_fields varchar;
        update_arrays varchar;
        json_fields varchar;
        config_name varchar;
    begin
        update $model.config_switch set is_switching=true;

        select name from $model.configuration where id=config_id into config_name;

        for node in select * from $model._node where configuration is not null loop
            if config_name in (select k from json_object_keys(node.configuration) as k) then
                -- json switch must be done first to handle trigger on other fields with correct values
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||node.node_type::varchar||'_node'
                and data_type = 'json'
                and column_name not in ('id', 'node_type', 'name', 'geom', 'reach', 'contour', 'station', 'domain_2d', 'configuration')
                into json_fields;

                if json_fields is not null then
                    execute 'update $model.'||node.node_type::varchar||'_node '||
                                 'set '||json_fields||' where id='||node.id::varchar||';';
                end if;

                -- user defined types
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||udt_name, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||node.node_type::varchar||'_node'
                and data_type = 'USER-DEFINED'
                and column_name not in ('id', 'node_type', 'name', 'geom', 'reach', 'contour', 'station', 'domain_2d', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model.'||node.node_type::varchar||'_node '||
                                 'set '||update_fields||' where id='||node.id::varchar||';';
                end if;

                -- common fields
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||node.node_type::varchar||'_node'
                and data_type != 'USER-DEFINED'
                and data_type != 'ARRAY'
                and data_type != 'json'
                and column_name not in ('id', 'node_type', 'name', 'geom', 'reach', 'contour', 'station', 'domain_2d', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model.'||node.node_type::varchar||'_node '||
                                 'set '||update_fields||' where id='||node.id::varchar||';';
                end if;

                -- assume that all arrays are real[] (it is the case in v1.0.0)
                select string_agg(column_name||'=replace(replace(configuration_json->'''||config_name||'''->>'''||column_name||''', ''['' , ''{''), '']'', ''}'')::real[]', ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||node.node_type::varchar||'_node'
                and data_type = 'ARRAY'
                and column_name not in ('id', 'node_type', 'name', 'geom', 'reach', 'contour', 'station', 'domain_2d', 'configuration')
                into update_arrays;

                if update_arrays is not null then
                    execute 'update $model.'||node.node_type::varchar||'_node '||
                                 'set '||update_arrays||' where id='||node.id::varchar||';';
                end if;
                _res := _res +1;
            end if;
        end loop;

        -- Update singularities that have a configuration
        for singularity in select * from $model._singularity where configuration is not null loop
            if config_name in (select k from json_object_keys(singularity.configuration) as k) then
                -- json switch must be done first to handle trigger on other fields with correct values
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||singularity.singularity_type::varchar||'_singularity'
                and data_type = 'json'
                and column_name not in ('id', 'singularity_type', 'name', 'geom', 'sector', 'configuration')
                into json_fields;

                if json_fields is not null then
                    execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                                 'set '||json_fields||' where id='||singularity.id::varchar||';';
                end if;

                -- user defined types
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||udt_name, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||singularity.singularity_type::varchar||'_singularity'
                and data_type = 'USER-DEFINED'
                and column_name not in ('id', 'singularity_type', 'name', 'geom', 'sector', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                                 'set '||update_fields||' where id='||singularity.id::varchar||';';
                end if;

                -- common fields
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||singularity.singularity_type::varchar||'_singularity'
                and data_type != 'USER-DEFINED'
                and data_type != 'ARRAY'
                and data_type != 'json'
                and column_name not in ('id', 'singularity_type', 'name', 'geom', 'sector', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                                 'set '||update_fields||' where id='||singularity.id::varchar||';';
                end if;

                -- assume that all arrays are real[] (it is the case in v1.0.0)
                select string_agg(column_name||'=replace(replace(configuration_json->'''||config_name||'''->>'''||column_name||''', ''['' , ''{''), '']'', ''}'')::real[]', ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||singularity.singularity_type::varchar||'_singularity'
                and data_type = 'ARRAY'
                and column_name not in ('id', 'singularity_type', 'name', 'geom', 'sector', 'configuration')
                into update_arrays;

                if update_arrays is not null then
                    execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                                 'set '||update_arrays||' where id='||singularity.id::varchar||';';
                end if;
                _res := _res +1;
            end if;
        end loop;

        -- Update links that have a configuration
        for link in select * from $model._link where configuration is not null loop
            if config_name in (select k from json_object_keys(link.configuration) as k) then
                -- json switch must be done first to handle trigger on other fields with correct values
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||link.link_type::varchar||'_link'
                and data_type = 'json'
                and column_name not in ('id', 'link_type', 'name', 'geom', 'branch', 'border', 'hydrograph', 'configuration')
                into json_fields;

                if json_fields is not null then
                    execute 'update $model.'||link.link_type::varchar||'_link '||
                                 'set '||json_fields||' where id='||link.id::varchar||';';
                end if;

                -- user defined types
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||udt_name, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||link.link_type::varchar||'_link'
                and data_type = 'USER-DEFINED'
                and column_name not in ('id', 'link_type', 'name', 'geom', 'branch', 'border', 'hydrograph', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model.'||link.link_type::varchar||'_link '||
                                 'set '||update_fields||' where id='||link.id::varchar||';';
                end if;

                -- common fields
                select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||link.link_type::varchar||'_link'
                and data_type != 'USER-DEFINED'
                and data_type != 'ARRAY'
                and data_type != 'json'
                and column_name not in ('id', 'link_type', 'name', 'geom', 'branch', 'border', 'hydrograph', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model.'||link.link_type::varchar||'_link '||
                                 'set '||update_fields||' where id='||link.id::varchar||';';
                end if;

                -- assume that all arrays are real[] (it is the case in v1.0.0)
                select string_agg(column_name||'=replace(replace(configuration_json->'''||config_name||'''->>'''||column_name||''', ''['' , ''{''), '']'', ''}'')::real[]', ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_'||link.link_type::varchar||'_link'
                and data_type = 'ARRAY'
                and column_name not in ('id', 'link_type', 'name', 'geom', 'branch', 'border', 'hydrograph', 'configuration')
                into update_arrays;

                if update_arrays is not null then
                    execute 'update $model.'||link.link_type::varchar||'_link '||
                                 'set '||update_arrays||' where id='||link.id::varchar||';';
                end if;
                _res := _res +1;
            end if;
        end loop;

        for profile in select * from $model._river_cross_section_profile where configuration is not null loop
            if config_name in (select k from json_object_keys(profile.configuration) as k) then
                -- foreign keys
                select string_agg(column_name||'=(configuration->'''||config_name||'''->>'''||column_name||''')::'||udt_name, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_river_cross_section_profile'
                and column_name  in ('up_cp_geom', 'down_cp_geom', 'up_op_geom', 'down_op_geom', 'up_vcs_geom', 'up_vcs_topo_geom', 'down_vcs_geom', 'down_vcs_topo_geom')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model._river_cross_section_profile set '||update_fields||' where id='||profile.id::varchar||';';
                end if;

                -- user defined types
                select string_agg(column_name||'=(configuration->'''||config_name||'''->>'''||column_name||''')::'||udt_name, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_river_cross_section_profile'
                and data_type = 'USER-DEFINED'
                and column_name not in ('id', 'name', 'geom', 'up_cp_geom', 'down_cp_geom', 'up_op_geom', 'down_op_geom', 'up_vcs_geom', 'up_vcs_topo_geom', 'down_vcs_geom', 'down_vcs_topo_geom', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model._river_cross_section_profile set '||update_fields||' where id='||profile.id::varchar||';';
                end if;

                -- common fields
                select string_agg(column_name||'=(configuration->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_river_cross_section_profile'
                and data_type != 'USER-DEFINED'
                and data_type != 'ARRAY'
                and data_type != 'json'
                and column_name not in ('id', 'name', 'geom', 'up_cp_geom', 'down_cp_geom', 'up_op_geom', 'down_op_geom', 'up_vcs_geom', 'up_vcs_topo_geom', 'down_vcs_geom', 'down_vcs_topo_geom', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model._river_cross_section_profile set '||update_fields||' where id='||profile.id::varchar||';';
                end if;

                -- assume that all arrays are real[] (it is the case in v1.0.0)
                select string_agg(column_name||'=replace(replace(configuration->'''||config_name||'''->>'''||column_name||''', ''['' , ''{''), '']'', ''}'')::real[]', ', ')
                from information_schema.columns
                where table_schema='$model'
                and table_name='_river_cross_section_profile'
                and data_type = 'ARRAY'
                and column_name not in ('id', 'name', 'geom', 'up_cp_geom', 'down_cp_geom', 'up_op_geom', 'down_op_geom', 'up_vcs_geom', 'up_vcs_topo_geom', 'down_vcs_geom', 'down_vcs_topo_geom', 'configuration')
                into update_fields;

                if update_fields is not null then
                    execute 'update $model._river_cross_section_profile set '||update_fields||' where id='||profile.id::varchar||';';
                end if;
                _res := _res +1;
            end if;
        end loop;

        update $model.config_switch set is_switching=false;

        return _res;
    end;
$$$$
;;

create or replace view $model.configured as
    select CONCAT('node:', n.id) as id,
        configuration,
        name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom
        from $model._node as n
        where configuration is not null
    union all
    select CONCAT('link:', l.id) as id,
        configuration,
        name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom
        from $model._link as l
        where configuration is not null
    union all
    select CONCAT('singularity:', s.id) as id,
        s.configuration,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom
        from $model._singularity as s join $model._node as n on s.node = n.id
        where s.configuration is not null
    union all
    select CONCAT('river_cross_section_profile:', p.id) as id,
        p.configuration,
        p.name,
        'profile'::varchar as type,
        'river_cross_section'::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom
        from $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where p.configuration is not null;
;;

create or replace view $model.configured_current as
    with config as (select c.name from $model.configuration as c, $model.metadata as m where m.configuration=c.id)
    select CONCAT('node:', n.id) as id,
        configuration,
        n.name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom
        from $model._node as n, config as c
        where exists (select 1 from json_object_keys(n.configuration) where json_object_keys = c.name limit 1) and c.name != 'default'
    union all
    select CONCAT('link:', l.id) as id,
        configuration,
        l.name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom
        from $model._link as l, config as c
        where exists (select 1 from json_object_keys(l.configuration) where json_object_keys = c.name limit 1) and c.name != 'default'
    union all
    select CONCAT('singularity:', s.id) as id,
        s.configuration,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom
        from config as c, $model._singularity as s join $model._node as n on s.node = n.id
        where exists (select 1 from json_object_keys(s.configuration) where json_object_keys = c.name limit 1) and c.name != 'default'
    union all
    select CONCAT('river_cross_section_profile:', p.id) as id,
        p.configuration,
        p.name,
        'profile'::varchar as type,
        'river_cross_section'::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom
        from config as c, $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where exists (select 1 from json_object_keys(p.configuration) where json_object_keys = c.name limit 1) and c.name != 'default';
;;

/* ********************************************************* */
/* Recreate dropped views (cascade drop)                     */
/* ********************************************************* */

create or replace view $model.invalid as
    WITH node_invalidity_reason AS (
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.zb IS NOT NULL THEN ''::text
                    ELSE '   zb is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk > 0::double precision THEN ''::text
                    ELSE '   rk>0   '::text
                END AS reason
           FROM $model.elem_2d_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   (area > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h IS NOT NULL THEN ''::text
                    ELSE '   h is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h >= 0::double precision THEN ''::text
                    ELSE '   h>=0   '::text
                END AS reason
           FROM $model.crossroad_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END ||
                CASE
                    WHEN new_1.zini IS NOT NULL THEN ''::text
                    ELSE '   zini is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10    '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1) >=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END AS reason
           FROM $model.storage_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((
                CASE
                    WHEN new_1.area_ha IS NOT NULL THEN ''::text
                    ELSE '   area_ha is not null   '::text
                END ||
                CASE
                    WHEN new_1.area_ha > 0::double precision THEN ''::text
                    ELSE '   area_ha>0   '::text
                END) ||
                CASE
                    WHEN new_1.rl IS NOT NULL THEN ''::text
                    ELSE '   rl is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rl > 0::double precision THEN ''::text
                    ELSE '   rl>0   '::text
                END) ||
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp IS NOT NULL THEN ''::text
                    ELSE '   c_imp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp >= 0::double precision THEN ''::text
                    ELSE '   c_imp>=0   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp <= 1::double precision THEN ''::text
                    ELSE '   c_imp<=1   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type IS NOT NULL THEN ''::text
                    ELSE '   netflow_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type IS NOT NULL THEN ''::text
                    ELSE '   runoff_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q_limit IS NOT NULL THEN ''::text
                    ELSE '   q_limit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q0 IS NOT NULL THEN ''::text
                    ELSE '   q0 is not null   '::text
                END AS reason
           FROM $model.catchment_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END) ||
                CASE
                    WHEN new_1.station IS NOT NULL THEN ''::text
                    ELSE '   station is not null    '::text
                END) ||
                CASE
                    WHEN ( SELECT st_intersects(new_1.geom, station.geom) AS st_intersects
                       FROM $model.station
                      WHERE station.id = new_1.station) THEN ''::text
                    ELSE '   (select st_intersects(new.geom, geom) from $model.station where id=new.station)   '::text
                END AS reason
           FROM $model.station_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END) ||
                CASE
                    WHEN ( SELECT NOT (EXISTS ( SELECT 1
                               FROM $model.station
                              WHERE st_intersects(station.geom, new_1.geom)))) THEN ''::text
                    ELSE '   (select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))   '::text
                END) ||
                CASE
                    WHEN $model.check_connected_pipes(new_1.id) THEN ''::text
                    ELSE '   $model.check_connected_pipes(new.id)   '::text
                END AS reason
           FROM $model.manhole_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END ||
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END) ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.reach IS NOT NULL THEN ''::text
                    ELSE '   reach is not null   '::text
                END) ||
                CASE
                    WHEN ( SELECT NOT (EXISTS ( SELECT 1
                               FROM $model.station
                              WHERE st_intersects(station.geom, new_1.geom)))) THEN ''::text
                    ELSE '   (select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))   '::text
                END AS reason
           FROM $model.river_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   (area > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END AS reason
           FROM $model.manhole_hydrology_node new_1
          WHERE NOT new_1.validity
        ), link_invalidity_reason AS (
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.q_pump IS NOT NULL THEN ''::text
                    ELSE '   q_pump is not null   '::text
                END ||
                CASE
                    WHEN new_1.q_pump >= 0::double precision THEN ''::text
                    ELSE '   q_pump>= 0   '::text
                END) ||
                CASE
                    WHEN new_1.qz_array IS NOT NULL THEN ''::text
                    ELSE '   qz_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(qz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.deriv_pump_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.down_type = 'manhole_hydrology'::hydra_node_type OR (new_1.down_type = ANY (ARRAY['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type])) AND new_1.hydrograph IS NOT NULL THEN ''::text
                    ELSE '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'') and (hydrograph is not null))   '::text
                END ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.connector_hydrology_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null    '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.weir_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.transmitivity IS NOT NULL THEN ''::text
                    ELSE '   transmitivity is not null   '::text
                END) ||
                CASE
                    WHEN new_1.transmitivity >= 0::double precision THEN ''::text
                    ELSE '   transmitivity>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.porous_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.law_type IS NOT NULL THEN ''::text
                    ELSE '   law_type is not null   '::text
                END ||
                CASE
                    WHEN new_1.param IS NOT NULL THEN ''::text
                    ELSE '   param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.borda_headloss_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN new_1.z_crest1 IS NOT NULL THEN ''::text
                    ELSE '   z_crest1 is not null   '::text
                END ||
                CASE
                    WHEN new_1.width1 IS NOT NULL THEN ''::text
                    ELSE '   width1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width1 > 0::double precision THEN ''::text
                    ELSE '   width1>0   '::text
                END) ||
                CASE
                    WHEN new_1.length IS NOT NULL THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN new_1.length > 0::double precision THEN ''::text
                    ELSE '   length>0   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk >= 0::double precision THEN ''::text
                    ELSE '   rk>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 IS NOT NULL THEN ''::text
                    ELSE '   z_crest2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 > new_1.z_crest1 THEN ''::text
                    ELSE '   z_crest2>z_crest1   '::text
                END) ||
                CASE
                    WHEN new_1.width2 IS NOT NULL THEN ''::text
                    ELSE '   width2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width2 > new_1.width1 THEN ''::text
                    ELSE '   width2>width1   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.strickler_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= '0'::numeric::double precision THEN ''::text
                    ELSE '   width>=0.   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= '0'::numeric::double precision THEN ''::text
                    ELSE '   cc>=0.   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode IS NOT NULL THEN ''::text
                    ELSE '   break_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode OR new_1.z_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''zw_critical'' or z_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode OR new_1.t_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''time_critical'' or t_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or grp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp > 0 THEN ''::text
                    ELSE '   break_mode=''none'' or grp>0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp < 100 THEN ''::text
                    ELSE '   break_mode=''none'' or grp<100   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.dt_fracw_array IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or dt_fracw_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) <= 10 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) >= 1 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 2) = 2 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.fuse_spillway_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.cross_section IS NOT NULL THEN ''::text
                    ELSE '   cross_section is not null   '::text
                END ||
                CASE
                    WHEN new_1.cross_section >= 0::double precision THEN ''::text
                    ELSE '   cross_section>=0   '::text
                END) ||
                CASE
                    WHEN new_1.length IS NOT NULL THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN new_1.length >= 0::double precision THEN ''::text
                    ELSE '   length>=0   '::text
                END) ||
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type = 'manhole_hydrology'::hydra_node_type OR (new_1.down_type = ANY (ARRAY['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type])) AND new_1.hydrograph IS NOT NULL THEN ''::text
                    ELSE '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'') and (hydrograph is not null))   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.routing_hydrology_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling > new_1.z_invert THEN ''::text
                    ELSE '   z_ceiling>z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc <=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc >=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_valve IS NOT NULL THEN ''::text
                    ELSE '   mode_valve is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate IS NOT NULL THEN ''::text
                    ELSE '   z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate >= new_1.z_invert THEN ''::text
                    ELSE '   z_gate>=z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate <= new_1.z_ceiling THEN ''::text
                    ELSE '   z_gate<=z_ceiling   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.gate_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert_stop IS NOT NULL THEN ''::text
                    ELSE '   z_invert_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling_stop IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.dt_regul_hr IS NOT NULL THEN ''::text
                    ELSE '   dt_regul_hr is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_control_node IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_pid_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_tz_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR new_1.q_z_crit IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul OR new_1.nr_z_gate IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.regul_gate_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((((((((((((((((
                CASE
                    WHEN new_1.z_crest1 IS NOT NULL THEN ''::text
                    ELSE '   z_crest1 is not null   '::text
                END ||
                CASE
                    WHEN new_1.width1 IS NOT NULL THEN ''::text
                    ELSE '   width1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width1 >= 0::double precision THEN ''::text
                    ELSE '   width1>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 IS NOT NULL THEN ''::text
                    ELSE '   z_crest2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 >= new_1.z_crest1 THEN ''::text
                    ELSE '   z_crest2>=z_crest1   '::text
                END) ||
                CASE
                    WHEN new_1.width2 IS NOT NULL THEN ''::text
                    ELSE '   width2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width2 >= 0::double precision THEN ''::text
                    ELSE '   width2>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef IS NOT NULL THEN ''::text
                    ELSE '   lateral_contraction_coef is not null   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef <= 1::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef<=1   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef >= 0::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef>=0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode IS NOT NULL THEN ''::text
                    ELSE '   break_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode OR new_1.z_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''zw_critical'' or z_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode OR new_1.t_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''time_critical'' or t_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.width_breach IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or width_breach is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or z_invert is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or grp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp > 0 THEN ''::text
                    ELSE '   break_mode=''none'' or grp>0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp < 100 THEN ''::text
                    ELSE '   break_mode=''none'' or grp<100   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.dt_fracw_array IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or dt_fracw_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) <= 10 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) >= 1 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 2) = 2 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.overflow_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((
                CASE
                    WHEN new_1.z_invert_up IS NOT NULL THEN ''::text
                    ELSE '   z_invert_up is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_invert_down IS NOT NULL THEN ''::text
                    ELSE '   z_invert_down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h_sable IS NOT NULL THEN ''::text
                    ELSE '   h_sable is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'valley'::hydra_cross_section_type THEN ''::text
                    ELSE '   cross_section_type not in (''valley'')   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk > 0::double precision THEN ''::text
                    ELSE '   rk >0   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'circular'::hydra_cross_section_type OR new_1.circular_diameter IS NOT NULL AND new_1.circular_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''circular'' or (circular_diameter is not null and circular_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_top_diameter IS NOT NULL AND new_1.ovoid_top_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_invert_diameter IS NOT NULL AND new_1.ovoid_invert_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_height IS NOT NULL AND new_1.ovoid_height > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_height is not null and ovoid_height >0 )   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_height > ((new_1.ovoid_top_diameter + new_1.ovoid_invert_diameter) / 2::double precision) THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'pipe'::hydra_cross_section_type OR new_1.cp_geom IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type!=''pipe'' or cp_geom is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'channel'::hydra_cross_section_type OR new_1.op_geom IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type!=''channel'' or op_geom is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.pipe_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.length IS NOT NULL THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN new_1.length >= 0::double precision THEN ''::text
                    ELSE '   length>=0   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk > 0::double precision THEN ''::text
                    ELSE '   rk>0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.street_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN new_1.npump IS NOT NULL THEN ''::text
                    ELSE '   npump is not null   '::text
                END ||
                CASE
                    WHEN new_1.npump <= 10 THEN ''::text
                    ELSE '   npump<=10   '::text
                END) ||
                CASE
                    WHEN new_1.npump >= 1 THEN ''::text
                    ELSE '   npump>=1   '::text
                END) ||
                CASE
                    WHEN new_1.zregul_array IS NOT NULL THEN ''::text
                    ELSE '   zregul_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.hq_array IS NOT NULL THEN ''::text
                    ELSE '   hq_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zregul_array, 1) = new_1.npump THEN ''::text
                    ELSE '   array_length(zregul_array, 1)=npump   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zregul_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zregul_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 1) = new_1.npump THEN ''::text
                    ELSE '   array_length(hq_array, 1)=npump   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 2) <= 10 THEN ''::text
                    ELSE '   array_length(hq_array, 2)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 2) >= 1 THEN ''::text
                    ELSE '   array_length(hq_array, 2)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 3) = 2 THEN ''::text
                    ELSE '   array_length(hq_array, 3)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.pump_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((
                CASE
                    WHEN new_1.z_overflow IS NOT NULL THEN ''::text
                    ELSE '   z_overflow is not null   '::text
                END ||
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END) ||
                CASE
                    WHEN new_1.area >= 0::double precision THEN ''::text
                    ELSE '   area>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.network_overflow_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.lateral_contraction_coef IS NOT NULL THEN ''::text
                    ELSE '   lateral_contraction_coef is not null   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef <= 1::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef<=1   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef >= 0::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef>=0   '::text
                END) ||
                CASE
                    WHEN st_npoints(new_1.border) = 2 THEN ''::text
                    ELSE '   ST_NPoints(border)=2   '::text
                END) ||
                CASE
                    WHEN st_isvalid(new_1.border) THEN ''::text
                    ELSE '   ST_IsValid(border)   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM $model.mesh_2d_link new_1
          WHERE NOT new_1.validity
        ), singularity_invalidity_reason AS (
         SELECT new_1.id,
            (((((
                CASE
                    WHEN new_1.downstream IS NOT NULL THEN ''::text
                    ELSE '   downstream is not null   '::text
                END ||
                CASE
                    WHEN new_1.split1 IS NOT NULL THEN ''::text
                    ELSE '   split1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.downstream_param IS NOT NULL THEN ''::text
                    ELSE '   downstream_param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1_law IS NOT NULL THEN ''::text
                    ELSE '   split1_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1_param IS NOT NULL THEN ''::text
                    ELSE '   split1_param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NULL OR new_1.split2_law IS NOT NULL THEN ''::text
                    ELSE '   split2 is null or split2_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NULL OR new_1.split2_param IS NOT NULL THEN ''::text
                    ELSE '   split2 is null or split2_param is not null   '::text
                END AS reason
           FROM $model.zq_split_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.law_type IS NOT NULL THEN ''::text
                    ELSE '   law_type is not null   '::text
                END ||
                CASE
                    WHEN new_1.param IS NOT NULL THEN ''::text
                    ELSE '   param is not null   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.borda_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((
                CASE
                    WHEN new_1.storage_area IS NOT NULL THEN ''::text
                    ELSE '   storage_area is not null   '::text
                END ||
                CASE
                    WHEN new_1.storage_area >= 0::double precision THEN ''::text
                    ELSE '   storage_area>=0   '::text
                END) ||
                CASE
                    WHEN new_1.constant_dry_flow IS NOT NULL THEN ''::text
                    ELSE '   constant_dry_flow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.constant_dry_flow >= 0::double precision THEN ''::text
                    ELSE '   constant_dry_flow>=0   '::text
                END) ||
                CASE
                    WHEN new_1.distrib_coef IS NULL OR new_1.distrib_coef >= 0::double precision AND new_1.distrib_coef <= 1::double precision THEN ''::text
                    ELSE '   distrib_coef is null or (distrib_coef>=0 and distrib_coef<=1)   '::text
                END) ||
                CASE
                    WHEN new_1.lag_time_hr IS NULL OR new_1.lag_time_hr >= 0::double precision THEN ''::text
                    ELSE '   lag_time_hr is null or (lag_time_hr>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.sector IS NULL OR new_1.distrib_coef IS NOT NULL AND new_1.lag_time_hr IS NOT NULL THEN ''::text
                    ELSE '   sector is null or (distrib_coef is not null and lag_time_hr is not null)   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR new_1.tq_array IS NOT NULL THEN ''::text
                    ELSE '   external_file_data or tq_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 1) <= 10 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 1) >= 1 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 2) = 2 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 2)=2   '::text
                END AS reason
           FROM $model.hydrograph_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling > new_1.z_invert THEN ''::text
                    ELSE '   z_ceiling>z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc <=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc >=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_valve IS NOT NULL THEN ''::text
                    ELSE '   mode_valve is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate IS NOT NULL THEN ''::text
                    ELSE '   z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate >= new_1.z_invert THEN ''::text
                    ELSE '   z_gate>=z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate <= new_1.z_ceiling THEN ''::text
                    ELSE '   z_gate<=z_ceiling   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.gate_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.cascade_mode IS NOT NULL THEN ''::text
                    ELSE '   cascade_mode is not null   '::text
                END ||
                CASE
                    WHEN new_1.cascade_mode = 'hydrograph'::hydra_model_connect_mode OR new_1.zq_array IS NOT NULL THEN ''::text
                    ELSE '   cascade_mode=''hydrograph'' or zq_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) <= 100 THEN ''::text
                    ELSE '   array_length(zq_array, 1)<=100   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zq_array, 2)=2   '::text
                END AS reason
           FROM $model.model_connect_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.d_abutment_l IS NOT NULL THEN ''::text
                    ELSE '   d_abutment_l is not null   '::text
                END ||
                CASE
                    WHEN new_1.d_abutment_r IS NOT NULL THEN ''::text
                    ELSE '   d_abutment_r is not null   '::text
                END) ||
                CASE
                    WHEN new_1.abutment_type IS NOT NULL THEN ''::text
                    ELSE '   abutment_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zw_array IS NOT NULL THEN ''::text
                    ELSE '   zw_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.bradley_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.q_dz_array IS NOT NULL THEN ''::text
                    ELSE '   q_dz_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(q_dz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(q_dz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(q_dz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.param_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null    '::text
                END ||
                CASE
                    WHEN new_1.z_regul IS NOT NULL THEN ''::text
                    ELSE '   z_regul is not null    '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul IS NOT NULL THEN ''::text
                    ELSE '   mode_regul is not null   '::text
                END) ||
                CASE
                    WHEN new_1.reoxy_law IS NOT NULL THEN ''::text
                    ELSE '   reoxy_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.reoxy_param IS NOT NULL THEN ''::text
                    ELSE '   reoxy_param is not null   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.zregul_weir_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((
                CASE
                    WHEN new_1.l_road IS NOT NULL THEN ''::text
                    ELSE '   l_road is not null   '::text
                END ||
                CASE
                    WHEN new_1.l_road >= 0::double precision THEN ''::text
                    ELSE '   l_road>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_road IS NOT NULL THEN ''::text
                    ELSE '   z_road is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zw_array IS NOT NULL THEN ''::text
                    ELSE '   zw_array is not null    '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.bridge_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.qq_array IS NOT NULL THEN ''::text
                    ELSE '   qq_array is not null   '::text
                END ||
                CASE
                    WHEN new_1.downstream IS NOT NULL THEN ''::text
                    ELSE '   downstream is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1 IS NOT NULL THEN ''::text
                    ELSE '   split1 is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qq_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NOT NULL AND array_length(new_1.qq_array, 2) = 3 OR new_1.split2 IS NULL AND array_length(new_1.qq_array, 2) = 2 THEN ''::text
                    ELSE '   (split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2)   '::text
                END AS reason
           FROM $model.qq_split_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((
                CASE
                    WHEN new_1.external_file_data OR new_1.tz_array IS NOT NULL THEN ''::text
                    ELSE '   external_file_data or tz_array is not null   '::text
                END ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 1) <= 10 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 1) >= 1 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 2) = 2 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 2)=2   '::text
                END AS reason
           FROM $model.tz_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((
                CASE
                    WHEN new_1.zq_array IS NOT NULL THEN ''::text
                    ELSE '   zq_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zq_array, 2)=2   '::text
                END AS reason
           FROM $model.zq_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.drainage IS NOT NULL THEN ''::text
                    ELSE '   drainage is not null   '::text
                END ||
                CASE
                    WHEN new_1.overflow IS NOT NULL THEN ''::text
                    ELSE '   overflow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ini IS NOT NULL THEN ''::text
                    ELSE '   z_ini is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zr_sr_qf_qs_array IS NOT NULL THEN ''::text
                    ELSE '   zr_sr_qf_qs_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.treatment_mode IS NOT NULL THEN ''::text
                    ELSE '   treatment_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.treatment_param IS NOT NULL THEN ''::text
                    ELSE '   treatment_param is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 2) = 4 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 2)=4   '::text
                END AS reason
           FROM $model.reservoir_rsp_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.pk0_km IS NOT NULL THEN ''::text
                    ELSE '   pk0_km is not null   '::text
                END ||
                CASE
                    WHEN new_1.dx IS NOT NULL THEN ''::text
                    ELSE '   dx is not null   '::text
                END) ||
                CASE
                    WHEN new_1.dx >= 0.1::double precision THEN ''::text
                    ELSE '   dx>=0.1   '::text
                END AS reason
           FROM $model.pipe_branch_marker_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
                CASE
                    WHEN new_1.q0 IS NOT NULL THEN ''::text
                    ELSE '   q0 is not null   '::text
                END AS reason
           FROM $model.constant_inflow_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END ||
                CASE
                    WHEN new_1.zini IS NOT NULL THEN ''::text
                    ELSE '   zini is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END AS reason
           FROM $model.tank_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.qz_array IS NOT NULL THEN ''::text
                    ELSE '   qz_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(qz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT $model.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM $model.hydraulic_cut_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.drainage IS NOT NULL THEN ''::text
                    ELSE '   drainage is not null   '::text
                END ||
                CASE
                    WHEN new_1.overflow IS NOT NULL THEN ''::text
                    ELSE '   overflow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q_drainage IS NOT NULL THEN ''::text
                    ELSE '   q_drainage is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q_drainage >= 0::double precision THEN ''::text
                    ELSE '   q_drainage>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_ini IS NOT NULL THEN ''::text
                    ELSE '   z_ini is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.treatment_mode IS NOT NULL THEN ''::text
                    ELSE '   treatment_mode is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END AS reason
           FROM $model.reservoir_rs_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END ||
                CASE
                    WHEN new_1.k IS NOT NULL THEN ''::text
                    ELSE '   k is not null   '::text
                END) ||
                CASE
                    WHEN new_1.k > 0::double precision THEN ''::text
                    ELSE '   k>0   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END AS reason
           FROM $model.strickler_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.z_weir IS NOT NULL THEN ''::text
                    ELSE '   z_weir is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= '0'::numeric::double precision THEN ''::text
                    ELSE '   cc>=0.   '::text
                END AS reason
           FROM $model.weir_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert_stop IS NOT NULL THEN ''::text
                    ELSE '   z_invert_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling_stop IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.dt_regul_hr IS NOT NULL THEN ''::text
                    ELSE '   dt_regul_hr is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_control_node IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_pid_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_tz_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR new_1.q_z_crit IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul OR new_1.nr_z_gate IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                END AS reason
           FROM $model.regul_sluice_gate_singularity new_1
          WHERE NOT new_1.validity
        )
    select CONCAT('node:', n.id) as
        id,
        validity,
        name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom,
        reason from $model._node as n join node_invalidity_reason as r on r.id=n.id
        where validity=FALSE
    union
    select CONCAT('link:', l.id) as
        id,
        validity,
        name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom,
        reason from $model._link as l join link_invalidity_reason as r on  r.id=l.id
        where validity=FALSE
    union
    select CONCAT('singularity:', s.id) as
        id,
        s.validity,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        reason from $model._singularity as s join $model._node as n on s.node = n.id join singularity_invalidity_reason as r on r.id=s.id
        where s.validity=FALSE
    union
    select CONCAT('profile:', p.id) as
        id,
        p.validity,
        p.name,
        'profile'::varchar as type,
        ''::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        ''::varchar as reason from $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where p.validity=FALSE
;;

create or replace view ${model}.open_reach
as with
    ends as (
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'valley' and p.type_cross_section_down != 'valley' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up != 'valley' and p.type_cross_section_down = 'valley' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'valley' and st_dwithin(p.geom, r.geom, .1) and st_linelocatepoint(r.geom, p.geom) > .99
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_down = 'valley' and st_dwithin(p.geom, r.geom, .1) and st_linelocatepoint(r.geom, p.geom) < .01
    )
    ,
    bounds as (
        select FIRST_VALUE(reach) over (partition by reach order by alpha), typ, LAG(alpha) over (partition by reach order by alpha) as start, alpha as end, id, reach from ends
    )
    select row_number() over() as id, st_linesubstring(r.geom, b.start, b.end) as geom, r.id as reach from bounds as b join ${model}.reach as r on r.id=b.reach where b.typ = 'down'
;;

create or replace view ${model}.channel_reach
as with
    ends as (
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'channel' and p.type_cross_section_down != 'channel' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up != 'channel' and p.type_cross_section_down = 'channel' and st_dwithin(p.geom, r.geom, .1)
        union
        select p.id, p.geom, 'down' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_up = 'channel' and st_dwithin(p.geom, r.geom, .1) and st_linelocatepoint(r.geom, p.geom) > .99
        union
        select p.id, p.geom, 'up' as typ, r.id as reach, st_linelocatepoint(r.geom, p.geom) as alpha
        from ${model}.river_cross_section_profile as p, ${model}.reach as r
        where p.type_cross_section_down = 'channel' and st_dwithin(p.geom, r.geom, .1) and st_linelocatepoint(r.geom, p.geom) < .01
    )
    ,
    bounds as (
        select FIRST_VALUE(reach) over (partition by reach order by alpha), typ, LAG(alpha) over (partition by reach order by alpha) as start, alpha as end, id, reach from ends
    )
    select row_number() over() as id, st_linesubstring(r.geom, b.start, b.end) as geom, r.id as reach from bounds as b join ${model}.reach as r on r.id=b.reach where b.typ = 'down'
;;

