<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="en">
<context>
    <name>@default</name>
    <message>
        <location filename="hydra.py" line="49"/>
        <source>&amp;Project</source>
        <translation type="obsolete">&amp;Projet</translation>
    </message>
    <message>
        <location filename="hydra.py" line="51"/>
        <source>&amp;Open</source>
        <translation type="obsolete">&amp;Ouvrir</translation>
    </message>
    <message>
        <location filename="hydra.py" line="52"/>
        <source>&amp;New</source>
        <translation type="obsolete">&amp;Nouveau</translation>
    </message>
    <message>
        <location filename="hydra.py" line="71"/>
        <source>&amp;Save As</source>
        <translation type="obsolete">&amp;Enregister sous</translation>
    </message>
    <message>
        <location filename="hydra.py" line="54"/>
        <source>&amp;Import topographic data</source>
        <translation type="obsolete">&amp;Impoter des données topographiques</translation>
    </message>
    <message>
        <location filename="hydra.py" line="55"/>
        <source>&amp;Raster</source>
        <translation type="obsolete">&amp;Raster</translation>
    </message>
    <message>
        <location filename="hydra.py" line="56"/>
        <source>&amp;Points</source>
        <translation type="obsolete">&amp;Points</translation>
    </message>
    <message>
        <location filename="hydra.py" line="57"/>
        <source>&amp;Lines</source>
        <translation type="obsolete">&amp;Lignes</translation>
    </message>
    <message>
        <location filename="hydra.py" line="59"/>
        <source>&amp;Quit</source>
        <translation type="obsolete">&amp;Quitter</translation>
    </message>
    <message>
        <location filename="hydra.py" line="61"/>
        <source>&amp;Model</source>
        <translation type="obsolete">&amp;Modèle</translation>
    </message>
    <message>
        <location filename="../plugin.py" line="222"/>
        <source>&amp;Configurations</source>
        <translation>&amp;Configurations</translation>
    </message>
    <message>
        <location filename="hydra.py" line="69"/>
        <source>&amp;Add</source>
        <translation type="obsolete">&amp;Ajouter</translation>
    </message>
    <message>
        <location filename="hydra.py" line="70"/>
        <source>&amp;Import</source>
        <translation type="obsolete">&amp;Importer</translation>
    </message>
    <message>
        <location filename="../plugin.py" line="295"/>
        <source>&amp;Hydrology</source>
        <translation>&amp;Hydrologie</translation>
    </message>
    <message>
        <location filename="hydra.py" line="75"/>
        <source>Dry weather apports</source>
        <translation type="obsolete">&amp;Apport de temps sec</translation>
    </message>
    <message>
        <location filename="hydra.py" line="77"/>
        <source>Internal Rain</source>
        <translation type="obsolete">Pluviométrie interne</translation>
    </message>
    <message>
        <location filename="hydra.py" line="78"/>
        <source>External Rain</source>
        <translation type="obsolete">Pluviométrie externe</translation>
    </message>
    <message>
        <location filename="hydra.py" line="79"/>
        <source>Rain list</source>
        <translation type="obsolete">Liste des pluies</translation>
    </message>
    <message>
        <location filename="hydra.py" line="81"/>
        <source>Pollution in waste water...</source>
        <translation type="obsolete">Pollution en assainissement...</translation>
    </message>
    <message>
        <location filename="hydra.py" line="83"/>
        <source>Rain grid...</source>
        <translation type="obsolete">Grille pluvioétrique</translation>
    </message>
    <message>
        <location filename="hydra.py" line="85"/>
        <source>Rain radar...</source>
        <translation type="obsolete">Pluie radar</translation>
    </message>
    <message>
        <location filename="hydra.py" line="87"/>
        <source>&amp;Scenario</source>
        <translation type="obsolete">&amp;Scénario</translation>
    </message>
    <message>
        <location filename="../project.py" line="58"/>
        <source>Project created.</source>
        <translation>Prijet créé</translation>
    </message>
    <message>
        <location filename="../project.py" line="66"/>
        <source>Project loaded.</source>
        <translation>Projet Chargé</translation>
    </message>
    <message>
        <location filename="../project.py" line="131"/>
        <source>Database </source>
        <translation>Base de données</translation>
    </message>
    <message>
        <location filename="../project.py" line="131"/>
        <source> not found. You should use create_new_projet/open_project instead of constructor.</source>
        <translation>non trouvé. Vous devriez utiliser reate_new_projet/open_project à la place du constructeur.</translation>
    </message>
    <message>
        <location filename="../project.py" line="157"/>
        <source>New project file created.</source>
        <translation>Nouveau fichier projet créé.</translation>
    </message>
    <message>
        <location filename="../project.py" line="166"/>
        <source>Data model in %s is version %s while the database module is version %s</source>
        <translation>Le modèle de données %s est en version %s alors que le module de base de données est en version %s</translation>
    </message>
    <message>
        <location filename="../project.py" line="178"/>
        <source>Project {0} opened.</source>
        <translation>Projet {0] ouvert.</translation>
    </message>
    <message>
        <location filename="../project.py" line="190"/>
        <source>Error with terrain file {}: {}</source>
        <translation>Erreur avec le fichier terrai {}: {}</translation>
    </message>
    <message>
        <location filename="../project.py" line="194"/>
        <source>File {filename} saved.</source>
        <translation>Fichier {filename} sauvegardé.</translation>
    </message>
    <message>
        <location filename="../project.py" line="198"/>
        <source>Project needed. You should use create_new_projet to create a new project.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project.py" line="205"/>
        <source>Model {0} created.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project.py" line="234"/>
        <source>Layer is not valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="122"/>
        <source>Hydra</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="157"/>
        <source>Hydra licence activation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="157"/>
        <source>Enter your licence number:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="162"/>
        <source>Licence activated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="163"/>
        <source>Activation successfull</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="163"/>
        <source>Your licence has successfully been activated!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_rain.py" line="58"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="167"/>
        <source>There has been an error during your licence activation. Please contact technical support.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="170"/>
        <source>No licence activated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="221"/>
        <source>&amp;Terrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="216"/>
        <source>&amp;Hydra</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="218"/>
        <source>&amp;New project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="219"/>
        <source>Manage &amp;projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="224"/>
        <source>&amp;Advanced tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="225"/>
        <source>&amp;Extract</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="226"/>
        <source>&amp;Anim-Eau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="227"/>
        <source>&amp;Crgeng</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="229"/>
        <source>Manage external &amp;tables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="230"/>
        <source>&amp;Pre-composed SQL queries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="231"/>
        <source>Create &amp;river profile album</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="232"/>
        <source>Export as CSV &amp;files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="233"/>
        <source>Launch notebook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="235"/>
        <source>&amp;Reload project layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="238"/>
        <source>&amp;Documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="239"/>
        <source>&amp;Users manuel (online)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="240"/>
        <source>&amp;Analysis: hydraulics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="241"/>
        <source>Analysis: &amp;hydrology</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="242"/>
        <source>Analysis: &amp;quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="243"/>
        <source>Analysis: &amp;sedimentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="245"/>
        <source>Change&amp;log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="246"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="247"/>
        <source>Licence activation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="911"/>
        <source>About Hydra</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="272"/>
        <source>&amp;Models</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="273"/>
        <source>&amp;Set current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="284"/>
        <source>&amp;Add model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="285"/>
        <source>&amp;Delete current model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="286"/>
        <source>&amp;Import model (sql)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="287"/>
        <source>&amp;Export model (sql)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="288"/>
        <source>Import model (&amp;csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="289"/>
        <source>&amp;Reload current model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="296"/>
        <source>&amp;Dryflow sector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="297"/>
        <source>Dryflow &amp;scenario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="298"/>
        <source>&amp;Rain scenario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="299"/>
        <source>Radar &amp;grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="300"/>
        <source>&amp;Pollution land accumulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="301"/>
        <source>&amp;Land occupation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="307"/>
        <source>&amp;Scenarios</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="308"/>
        <source>&amp;Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="309"/>
        <source>&amp;Grouping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="310"/>
        <source>&amp;Time series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="311"/>
        <source>&amp;Import scenario (MAGES)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="318"/>
        <source>Project items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="336"/>
        <source>Longitudinal profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="343"/>
        <source>Hydra toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="481"/>
        <source>You need a new a project before adding a new model.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="495"/>
        <source>Delete model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="495"/>
        <source>This will delete model {}. Proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="505"/>
        <source>No model to delete found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="515"/>
        <source>You need an open project to reload its layers.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="525"/>
        <source>You need a project and a model to reload a model.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="575"/>
        <source>Select directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="586"/>
        <source>We need to install Jupyter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="586"/>
        <source>we will try to perform auto install of Jupyter!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="591"/>
        <source>We did not install jupyter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="591"/>
        <source>Please do a manual installation of Jupyter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="594"/>
        <source>Jupyter installed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="594"/>
        <source>Jupyter has been installed, please restart Qgis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="663"/>
        <source>Import hydra dump file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="748"/>
        <source>Sql files (*.sql)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="796"/>
        <source>Import model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="713"/>
        <source>This will import model {} from file {} into project {}. Proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="453"/>
        <source>Import error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="722"/>
        <source>Error importing model {} into {}. See hydra.log file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="461"/>
        <source>Import successfull</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="732"/>
        <source>Model {} successfully imported into {}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="748"/>
        <source>Export model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="271"/>
        <source>Export error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="757"/>
        <source>Error exporting model {}. See hydra.log file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="258"/>
        <source>Export successfull</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="761"/>
        <source>Model {} successfully exported in file {}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="813"/>
        <source>Import csv file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.py" line="69"/>
        <source>Csv files (*.csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="796"/>
        <source>Importation of model {} finished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="828"/>
        <source>Import project data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="828"/>
        <source>Importation of datas from file {} finished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="839"/>
        <source>You need an open project to access work schema.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="846"/>
        <source>Changelog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="873"/>
        <source>Licence already activated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugin.py" line="873"/>
        <source>A licence has already been activated. Do you want to activate it again?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/geometry_selector.py" line="81"/>
        <source>Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/crgeng.py" line="48"/>
        <source>Control File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/extract.py" line="82"/>
        <source>Select extract control file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/extract.py" line="138"/>
        <source>Extract successful</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/extract.py" line="138"/>
        <source>Extract run successfully with control file {}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/extract.py" line="140"/>
        <source>Extract error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/extract.py" line="140"/>
        <source>An error occured during extract with file {}.
See hydra.log file for extract logs.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/crgeng.py" line="88"/>
        <source>Select crgeng control file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/crgeng.py" line="143"/>
        <source>Crgeng successful</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/crgeng.py" line="143"/>
        <source>Crgeng run successfully with control file {}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/crgeng.py" line="145"/>
        <source>Crgeng error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/crgeng.py" line="145"/>
        <source>An error occured during crgeng with file {}.
See hydra.log file for crgeng logs.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/animeau.py" line="63"/>
        <source>Parameters File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/animeau.py" line="86"/>
        <source>Select animeau control file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/pre_composed_queries.py" line="36"/>
        <source>Target model: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/pre_composed_queries.py" line="37"/>
        <source>Table in work: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/pre_composed_queries.py" line="38"/>
        <source>Geometry column of work table: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/pre_composed_queries.py" line="39"/>
        <source>Liste d&apos;identifiants</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/pre_composed_queries.py" line="86"/>
        <source>Notice : You must have at least one model to perform queries.&lt;br/&gt;&lt;br/&gt;Selected SQL query effect:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/pre_composed_queries.py" line="244"/>
        <source>Create result file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/pre_composed_queries.py" line="244"/>
        <source>CSV (*.csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/pre_composed_queries.py" line="250"/>
        <source>SQL query successful</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/pre_composed_queries.py" line="250"/>
        <source>Queries from file {} succesfully run.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/writer_crgeng_file.py" line="305"/>
        <source>Save control file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/writer_crgeng_file.py" line="305"/>
        <source>Ctl files (*.ctl)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../database/radar_rain.py" line="96"/>
        <source>Processing rain data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../database/terrain.py" line="72"/>
        <source>Processing terrain data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../database/mesh_interface.py" line="134"/>
        <source>Meshed coverage {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../database/mesh_interface.py" line="108"/>
        <source>Mesh error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../database/mesh_interface.py" line="182"/>
        <source>Regenerate all meshes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../database/mesh_interface.py" line="182"/>
        <source>This will unmesh all 2D coverages tehn regenerate mesh in all of them. Proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../database/mesh_interface.py" line="202"/>
        <source>Mesh all unmeshed coverages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../database/mesh_interface.py" line="202"/>
        <source>This will mesh all 2D coverages not currently meshed. Proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../database/mesh_interface.py" line="297"/>
        <source>you must select two domains</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../versions/update.py" line="150"/>
        <source>Selected project is in version {}.
This is already the last available data model version.
No update is needed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../versions/update.py" line="153"/>
        <source>Selected project is in version {}.
This version does not support automatic update.
Please contact Hydra support.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../versions/update.py" line="156"/>
        <source>Selected project seems to be in version {}.
This version is not compatible with automatic update.
Please contact Hydra support.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utility/qgis_utilities.py" line="135"/>
        <source>terrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utility/qgis_utilities.py" line="145"/>
        <source>rain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utility/qgis_utilities.py" line="187"/>
        <source>CSV Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/geometry_selector.py" line="81"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="60"/>
        <source>Return period (years)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="60"/>
        <source>a</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="60"/>
        <source>b</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.py" line="46"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="95"/>
        <source>Montana</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="95"/>
        <source>Total duration (min)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="107"/>
        <source>Peak time (min)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="107"/>
        <source>Total event
 Duration (min)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="107"/>
        <source>Total event
 Montana coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="107"/>
        <source>Peak
 Duration (min)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="107"/>
        <source>Peak
 Montana coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="145"/>
        <source>Interpolation mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="145"/>
        <source>Rainfall file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="165"/>
        <source>Pluvio file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="310"/>
        <source>t (min)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="310"/>
        <source>i (mm/h)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="381"/>
        <source>Time (mn)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="381"/>
        <source>Cumulated height (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="522"/>
        <source>Exporting Thiessen coefficients</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="692"/>
        <source>Init...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="703"/>
        <source>Catchment {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="564"/>
        <source>Thiessen coefficients file generated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="720"/>
        <source>Successfully created files {} in folder {}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="739"/>
        <source>Select a rain file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="657"/>
        <source>Hydra rain (*.vrt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="661"/>
        <source>Radar rain error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="661"/>
        <source>Error importing file {}. Required file {} not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/terrain_manager.py" line="170"/>
        <source>Select one or more files to open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/terrain_manager.py" line="170"/>
        <source>Images (*.asc *.tiff *.tif)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="691"/>
        <source>Exporting rain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.py" line="720"/>
        <source>Rain file generated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/config_manager.py" line="40"/>
        <source>No model in project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/config_manager.py" line="104"/>
        <source>Configuration error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/config_manager.py" line="104"/>
        <source>Cannot delete active configuration {} on model {}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="106"/>
        <source>Run...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="117"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="118"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="119"/>
        <source>Search objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="120"/>
        <source>Geometries library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="121"/>
        <source>List invalid objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="122"/>
        <source>List configured objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="123"/>
        <source>List regulated objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="124"/>
        <source>Check current model topology</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="125"/>
        <source>Produce velocity and depth rasters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="129"/>
        <source>Show graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="130"/>
        <source>Show serie graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="131"/>
        <source>Show min/max table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="132"/>
        <source>Show hydro data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="133"/>
        <source>Synthetic results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="135"/>
        <source>Animation map results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="136"/>
        <source>Animate rain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="280"/>
        <source>Current model set to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="283"/>
        <source>No current model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="295"/>
        <source>Current scenario set to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="299"/>
        <source>No current scenario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="313"/>
        <source>Current configuration set to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="319"/>
        <source>Starting {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="319"/>
        <source>Proceed with computation for {}?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="575"/>
        <source>Processing Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="575"/>
        <source>Unable to detect DEM source, please check Terrain configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="616"/>
        <source>Topology OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="616"/>
        <source>Topology of model {} is fine.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="618"/>
        <source>Topological errors found in model {}:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="621"/>
        <source>Topology error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="638"/>
        <source>No supporting layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="638"/>
        <source>No layers found to display results. Open &quot;Hydra&quot; =&gt; &quot;Settings&quot; =&gt; &quot;Synthetic results settings&quot; and refresh some layers.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/delete.py" line="54"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_run.py" line="171"/>
        <source>There are still some invalidities in models {}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_run.py" line="217"/>
        <source>Error during computation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_run.py" line="217"/>
        <source>An error occured during {}:
    {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="768"/>
        <source>Computation complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="807"/>
        <source>Computation starting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.py" line="222"/>
        <source>Add file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/terrain_manager.py" line="153"/>
        <source>Terrain files (*.vrt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/domain_manager.py" line="101"/>
        <source>No active model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/configuration.py" line="94"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/configuration.py" line="94"/>
        <source>Parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/configuration.py" line="94"/>
        <source>Default value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_group_run.py" line="232"/>
        <source>Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/new_project_dialog.py" line="84"/>
        <source>Not a valid SRID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/new_project_dialog.py" line="84"/>
        <source>Please select a SRID with code starting with &quot;ESPG:...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="402"/>
        <source>pk (km)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="314"/>
        <source>baseline (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="314"/>
        <source>topo (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="314"/>
        <source>vault (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="314"/>
        <source>left bank (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="314"/>
        <source>rigth bank (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="331"/>
        <source>waterline z {minmax} (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="332"/>
        <source>flow minor riverbed Q {minmax} (m3/s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="333"/>
        <source>flow major river bed Q {minmax} (m3/s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="334"/>
        <source>speed V {minmax} (m3/s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="323"/>
        <source>energy E {minmax} (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="324"/>
        <source>criticalZ Zc {minmax} (m/s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="325"/>
        <source>silting hight Hs {minmax} (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="326"/>
        <source>solid flow Qs {minmax} (m3/s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="400"/>
        <source>Objects ( , </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="402"/>
        <source> : Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="402"/>
        <source>Cumulated pk (km)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/bridge_headloss_singularity.py" line="70"/>
        <source>Z (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="402"/>
        <source>Direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="1684"/>
        <source>pk(km)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="1675"/>
        <source>Flow(Qs)(m3/s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_interface.py" line="1685"/>
        <source>Concentration(C)(npp/100ml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/simple_link_dialog.py" line="53"/>
        <source> - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/simple_singularity_dialog.py" line="59"/>
        <source> (</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/simple_singularity_dialog.py" line="57"/>
        <source>: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/simple_singularity_dialog.py" line="57"/>
        <source> km)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/simple_singularity_dialog.py" line="59"/>
        <source>)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_sector_settings.py" line="246"/>
        <source>Constant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tz_bc.py" line="75"/>
        <source>t (h)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_sector_settings.py" line="290"/>
        <source>V/Vtot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.py" line="69"/>
        <source>Import from file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.py" line="115"/>
        <source>Imported scenarios</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.py" line="115"/>
        <source>Import successfull, {} scenarios imported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.py" line="128"/>
        <source>Not the right key word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.py" line="128"/>
        <source>Expected the keyword : &apos;! Scenario&apos; at the beginning of the file, found  {}.	Check the format of your input file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/graph_widget.py" line="224"/>
        <source>Copy data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/graph_widget.py" line="223"/>
        <source>Copy image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.py" line="183"/>
        <source>Axe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.py" line="183"/>
        <source>Curves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.py" line="341"/>
        <source>Axe 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.py" line="344"/>
        <source>Axe 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.py" line="386"/>
        <source>Visu tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.py" line="385"/>
        <source>The file {f} already exists,
do you want overwrite?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.py" line="411"/>
        <source>Graphic saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.py" line="411"/>
        <source>Graphic successfully saved to:
 {f}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.py" line="479"/>
        <source>Load graphic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.py" line="672"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.py" line="672"/>
        <source>Time(hr)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_rain.py" line="58"/>
        <source>Please enter a name and a template containing exactly 12 consecutive &apos;&apos;?&apos;&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/simple_link_dialog.py" line="53"/>
        <source> =&gt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.py" line="74"/>
        <source>Non existing sector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.py" line="74"/>
        <source>There is some missing sector in the database, see details below. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.py" line="75"/>
        <source>Missing sectors : , </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="150"/>
        <source>Bad SRID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="155"/>
        <source>Not current version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="162"/>
        <source>No creation date found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="169"/>
        <source>Project folder not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="192"/>
        <source>Update project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="192"/>
        <source>Do you want to update project {} from version {} to version {} ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="214"/>
        <source>Delete project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="214"/>
        <source>This will delete project {}. Proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="223"/>
        <source>Project {0} deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="240"/>
        <source>Successful update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="230"/>
        <source>Project {} updated to version {}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="234"/>
        <source>Update all projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="234"/>
        <source>This will update all you projects to version {}. Proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="240"/>
        <source>All projects updated to version {}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="245"/>
        <source>Export project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="245"/>
        <source>SQL file(*.sql)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="271"/>
        <source>File {f} has special characters in path. Choose a simpler directory path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="258"/>
        <source>Project {p} successfully exported to {f}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="261"/>
        <source>Error exporting project {p} to {f}. See hydra.log file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="267"/>
        <source>Export project with data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="347"/>
        <source>Hydra project file(*.hyp)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="317"/>
        <source>Export with data successfull</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="317"/>
        <source>Project {p} successfully exported to {f} with folder {d}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="320"/>
        <source>Export with data error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="320"/>
        <source>Error exporting project {p} to {f} with folder {d}. See hydra.log file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="323"/>
        <source>Import project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="323"/>
        <source>SQL files(*.sql)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="351"/>
        <source>File {f} has special characters in path. Move it to a simpler directory path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="371"/>
        <source>File {f} data version do not match available versions ({av}).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="375"/>
        <source>Error importing file {f}. See hydra.log file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="343"/>
        <source>Project {p} successfully imported from {f}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="347"/>
        <source>Import project with data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="363"/>
        <source>Error handling file {f}. No project found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="399"/>
        <source>Project {p} with data successfully imported from {f}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="453"/>
        <source>Error while duplicating project {}. See hydra.log file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="461"/>
        <source>Project {} successfully copied into project {}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="465"/>
        <source>Select workspace path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/base_dialog.py" line="48"/>
        <source>Canceled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/manhole_node.py" line="76"/>
        <source>Saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/geometry_manager.py" line="325"/>
        <source>Import sections from csv file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/geometry_manager.py" line="326"/>
        <source>(*.csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.py" line="42"/>
        <source>Model dock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.py" line="334"/>
        <source>No points found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.py" line="334"/>
        <source>Please select layer Terrain points and some points to affect them to a group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.py" line="378"/>
        <source>Selection is not complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.py" line="378"/>
        <source>The Hydrology network is not complete, you need to select more nodes : {}, do you wish to continue with this extended selection ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.py" line="386"/>
        <source>Singularities detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.py" line="388"/>
        <source>Please remove singularities from nodes with ids : [{}]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.py" line="396"/>
        <source>Hydrology Converted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.py" line="102"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.py" line="685"/>
        <source>This will merge {} end point and {} startpoint. Confirm ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.py" line="713"/>
        <source>This will split {} at {} km. Confirm ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.py" line="759"/>
        <source>Are you sure you want to delete this item?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="249"/>
        <source>Montanas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="255"/>
        <source>Caquot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="257"/>
        <source>Single triangular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="259"/>
        <source>Double triangular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="261"/>
        <source>Hyetographs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="263"/>
        <source>Gaged rainfalls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="265"/>
        <source>Gaged rainfalls (file)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="267"/>
        <source>Radar rainfalls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="269"/>
        <source>MAGES rainfalls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="239"/>
        <source>Models</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="241"/>
        <source>Scenarios</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="243"/>
        <source>Time series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="271"/>
        <source>Dry inflow scenarios</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="247"/>
        <source>Hydrology</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="253"/>
        <source>Rainfalls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/land_type.py" line="51"/>
        <source>Land type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/land_type.py" line="51"/>
        <source>Impervious
Coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/land_type.py" line="51"/>
        <source>Production
Law type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/land_type.py" line="51"/>
        <source>Runoff
Coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/land_type.py" line="51"/>
        <source>SCS
Soil storage J (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/land_type.py" line="51"/>
        <source>Holtan
Dry infiltration rate (mm/h)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/land_type.py" line="51"/>
        <source>Holtan
Saturation infiltration rate (mm/h)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/land_type.py" line="51"/>
        <source>Holtan
Soil storage capacity (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/land_type.py" line="155"/>
        <source>Delete current classification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/land_type.py" line="155"/>
        <source>This will delete the current classification of land occupation layer. Proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/land_type.py" line="167"/>
        <source>Preset land type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/land_type.py" line="167"/>
        <source>Choose a preset classification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.py" line="111"/>
        <source>Select {} path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.py" line="255"/>
        <source>Empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.py" line="194"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.py" line="283"/>
        <source>Generation successfull</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.py" line="226"/>
        <source>Generation of {} for model {} successful. {} items created.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.py" line="285"/>
        <source>Generation failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.py" line="229"/>
        <source>see log file for more details.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.py" line="293"/>
        <source>Clear successfull</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.py" line="240"/>
        <source>{} of model {} successfully cleared</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.py" line="295"/>
        <source>Clear failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.py" line="295"/>
        <source>See log file for more details.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.py" line="270"/>
        <source>Warning: triangulation can take some time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.py" line="283"/>
        <source>Model {} successfully meshed, {} triangles created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.py" line="293"/>
        <source>Triangles of model {} successfully cleared</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.py" line="143"/>
        <source>Preview data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.py" line="143"/>
        <source>Chose a name for the previewed layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.py" line="155"/>
        <source>Geometry column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.py" line="155"/>
        <source>confirm the geometry column name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.py" line="266"/>
        <source>Are you sure you want to delete work.{table_name} ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.py" line="449"/>
        <source>Pipes with no manholes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.py" line="449"/>
        <source>Manholes will be generated at start and end of pipes. Proceed ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.py" line="488"/>
        <source>Singularity with no nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.py" line="488"/>
        <source>You need to add nodes. Proceed ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.py" line="490"/>
        <source>Insert nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.py" line="490"/>
        <source>node type :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.py" line="730"/>
        <source>Data added in Hydra project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.py" line="730"/>
        <source>Imported data from schema work into table {}.{}, 
 {} rows imported out of {} ( {} invalid geometries)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.py" line="791"/>
        <source>Imported data from schema work into table {}.{};</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_grid.py" line="105"/>
        <source>Starting CRB file generation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_grid.py" line="109"/>
        <source>Generating CRB file: model {}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_grid.py" line="132"/>
        <source>Model {} written in CRB file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_grid.py" line="133"/>
        <source>CRB file generated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/delete.py" line="50"/>
        <source>Are you sure you want to delete {} ({})?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/delete.py" line="54"/>
        <source>/! This is a {}, removing it will remove linked object or create domain issue, are you sure ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/delete.py" line="60"/>
        <source>Multiple objects will be deleted or modified, continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/new_project_dialog.py" line="60"/>
        <source>Select working dir path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/geom_dialog.py" line="77"/>
        <source>Geometry is not valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/geom_dialog.py" line="78"/>
        <source>Entered valley geometry is not valid, click details below to show more information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_export.py" line="67"/>
        <source>No scenario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_export.py" line="87"/>
        <source>Directory for .pdf file or folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_export.py" line="92"/>
        <source>Exporting section</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_export.py" line="193"/>
        <source>File open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_export.py" line="193"/>
        <source>File is actually open : {} , close it before the generation of the section&apos;s report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.py" line="385"/>
        <source>long profil</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.py" line="404"/>
        <source>Setting saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.py" line="404"/>
        <source>Setting successfully saved to:
 {f}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_viewer.py" line="358"/>
        <source>topo MNT Z(m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_viewer.py" line="359"/>
        <source>topo MNT X(m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_viewer.py" line="360"/>
        <source>topo terrain point Z(m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_viewer.py" line="361"/>
        <source>topo terrain point X(m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_viewer.py" line="362"/>
        <source>combined topo Z(m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_viewer.py" line="363"/>
        <source>combined topo X(m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_viewer.py" line="364"/>
        <source>Hydra section Z(m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_viewer.py" line="365"/>
        <source>Hydra section X(m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_viewer.py" line="413"/>
        <source>X (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_regulation.py" line="68"/>
        <source>Regulation file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_regulation.py" line="68"/>
        <source>Scenario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_regulation.py" line="68"/>
        <source>Line number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.py" line="78"/>
        <source>Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.py" line="169"/>
        <source>Select wind data file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_comput_options.py" line="128"/>
        <source>All file (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_transport.py" line="59"/>
        <source>Select sediment file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.py" line="162"/>
        <source>Bloc starting at same date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.py" line="162"/>
        <source>there is already a bloc starting in : {} , please check your input.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_comput_options.py" line="50"/>
        <source>Links</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_comput_options.py" line="68"/>
        <source>Singularities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_comput_options.py" line="81"/>
        <source>Boundary conditions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_comput_options.py" line="128"/>
        <source>Select options file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_comput_options.py" line="198"/>
        <source>Branch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_comput_options.py" line="200"/>
        <source>Reach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_comput_options.py" line="202"/>
        <source>Domain 2D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/param_headloss_singularity.py" line="69"/>
        <source>Q (m3/s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/advanced_regulation_editor.py" line="98"/>
        <source>model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/advanced_regulation_editor.py" line="204"/>
        <source>param_hydrau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/advanced_regulation_editor.py" line="223"/>
        <source>position_actionneur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.py" line="36"/>
        <source>Riverbed Z not monotonously increasing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.py" line="37"/>
        <source>Riverbed width not monotonously increasing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.py" line="38"/>
        <source>Left flood plain Z not monotonously increasing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.py" line="39"/>
        <source>Left flood plain width not monotonously increasing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.py" line="40"/>
        <source>Right flood plain Z not monotonously increasing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.py" line="41"/>
        <source>Right flood plain width not monotonously increasing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.py" line="42"/>
        <source>Left flood plain start beneath riverbed end</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.py" line="43"/>
        <source>Right flood plain start beneath riverbed end</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.py" line="381"/>
        <source>Geometry generation failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.py" line="347"/>
        <source>No profile found, check that your constrain line crosses a reach.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.py" line="380"/>
        <source>Geometry generation failed, click details below to show more information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/channel_geom_widget.py" line="96"/>
        <source>Width (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pump_link.py" line="77"/>
        <source>H (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.py" line="126"/>
        <source>Incorrect law</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.py" line="126"/>
        <source>{} law is not available any more. Please select another one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.py" line="102"/>
        <source>This will exclude pipe {} from branch generation. It can create errors in results. Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/storage.py" line="69"/>
        <source>Elevation (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tank_bc.py" line="72"/>
        <source>Area (m2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/overflow_link.py" line="79"/>
        <source>dt (h)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/overflow_link.py" line="73"/>
        <source>Fract. ratio (0-1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/overflow_link.py" line="79"/>
        <source>Fractured width (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/deriv_pump_link.py" line="56"/>
        <source>Q/Qmax</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/bradley_headloss_singularity.py" line="63"/>
        <source>Piers width (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_connect_bc.py" line="86"/>
        <source>t (hr)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/reservoir_rs_hydrology_singularity.py" line="95"/>
        <source>S (m2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/hydrograph_bc.py" line="71"/>
        <source>Not in a sector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/hydrograph_bc.py" line="78"/>
        <source>In sector: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/hydrograph_bc.py" line="118"/>
        <source>T (h)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/ventilator_link.py" line="54"/>
        <source>Q [m3/s]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/ventilator_link.py" line="54"/>
        <source>dp [Pa]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/param_headloss_singularity.py" line="69"/>
        <source>dH (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/bridge_headloss_singularity.py" line="70"/>
        <source>Empty width (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="764"/>
        <source>&amp;lt;center&amp;gt;Les calculs en licence &amp;lt;b&amp;gt;STARTER&amp;lt;/b&amp;gt; ont duré : &amp;lt;b&amp;gt;{}&amp;lt;/b&amp;gt;&amp;lt;/center&amp;gt;
                         &amp;lt;center&amp;gt;Durée des calculs si vous étiez en version &amp;lt;b&amp;gt;PRO: {}&amp;lt;/b&amp;gt;&amp;lt;/center&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="803"/>
        <source>&amp;lt;center&amp;gt;La version PRO vous permettrait d&apos;accélérer les calculs&amp;lt;/center&amp;gt;
                                  &amp;lt;center&amp;gt;et d&apos;accéder à des options complémentaires.&amp;lt;/center&amp;gt;
                                {&amp;quot;&amp;lt;center&amp;gt;Voir message en fin de calcul estimant les temps de calcul avec la version PRO.&amp;lt;/center&amp;gt;&amp;quot; if manholes_in_models else &apos;&apos;}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.py" line="215"/>
        <source>Also delete project workspace (project directory and all its content)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Advanced_regulation_editor</name>
    <message>
        <location filename="../gui/widgets/advanced_regulation_editor.ui" line="17"/>
        <source>Advanced regulation editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/advanced_regulation_editor.ui" line="92"/>
        <source>Actions</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AreaD8</name>
    <message>
        <location filename="../processing/taudem/aread8.py" line="53"/>
        <source>D8 contributing area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/aread8.py" line="56"/>
        <source>Basic grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/aread8.py" line="62"/>
        <source>dem,hydrology,d8,contributing area,catchment area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/aread8.py" line="65"/>
        <source>Calculates a grid of contributing areas using the single direction D8 flow model.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/aread8.py" line="75"/>
        <source>D8 flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/aread8.py" line="77"/>
        <source>Outlets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/aread8.py" line="82"/>
        <source>Weight grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/aread8.py" line="86"/>
        <source>Check for edge contamination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/aread8.py" line="90"/>
        <source>D8 specific catchment area</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AreaDinf</name>
    <message>
        <location filename="../processing/taudem/areadinf.py" line="53"/>
        <source>D-infinity contributing area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/areadinf.py" line="56"/>
        <source>Basic grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/areadinf.py" line="62"/>
        <source>dem,hydrology,d-infinity,contributing area,catchment area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/areadinf.py" line="65"/>
        <source>Calculates a grid of specific catchment area which is the contributing area per unit contour length using the multiple flow direction D-infinity approach.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/areadinf.py" line="76"/>
        <source>D-infinity flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/areadinf.py" line="78"/>
        <source>Outlets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/areadinf.py" line="83"/>
        <source>Weight grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/areadinf.py" line="87"/>
        <source>Check for edge contamination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/areadinf.py" line="91"/>
        <source>D-infinity specific catchment area</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CatchmentContourEditor</name>
    <message>
        <location filename="../gui/forms/catchment_contour.py" line="82"/>
        <source>hyetogram file </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_contour.py" line="82"/>
        <source> not found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConnectDown</name>
    <message>
        <location filename="../processing/taudem/connectdown.py" line="52"/>
        <source>Connect down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/connectdown.py" line="55"/>
        <source>Stream network analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/connectdown.py" line="61"/>
        <source>dem,hydrology,connect,outlet,downflow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/connectdown.py" line="64"/>
        <source>For each zone in a raster entered (e.g. HUC converted to grid) it identifies the point with largest area D8.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/connectdown.py" line="74"/>
        <source>D8 flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/connectdown.py" line="76"/>
        <source>D8 contributing area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/connectdown.py" line="78"/>
        <source>Watershed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/connectdown.py" line="80"/>
        <source>Grid cells move to downstream</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/connectdown.py" line="86"/>
        <source>Outlets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/connectdown.py" line="89"/>
        <source>Moved outlets</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>D8FlowDir</name>
    <message>
        <location filename="../processing/taudem/d8flowdir.py" line="77"/>
        <source>D8 flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8flowdir.py" line="50"/>
        <source>Basic grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8flowdir.py" line="56"/>
        <source>dem,hydrology,d8,flow directions,slope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8flowdir.py" line="59"/>
        <source>Calculates 2 grids. The first contains the D8 Flow directions which are defined, for each cell, as the direction of one of its eight adjacent or diagonal neighbors with the steepest downward slope. The second contains the slope, as evaluated in the direction of steepest descent and is reported as drop/distance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8flowdir.py" line="74"/>
        <source>Pit filled elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8flowdir.py" line="79"/>
        <source>D8 slope</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>D8FlowPathExtremeUp</name>
    <message>
        <location filename="../processing/taudem/d8flowpathextremeup.py" line="54"/>
        <source>D8 Extreme Upslope Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8flowpathextremeup.py" line="57"/>
        <source>Stream network analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8flowpathextremeup.py" line="63"/>
        <source>dem,hydrology,d8,upslope,extreme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8flowpathextremeup.py" line="66"/>
        <source>Evaluates the extreme (either maximum or minimum) upslope value from an input grid based on the D8 flow model.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8flowpathextremeup.py" line="77"/>
        <source>D8 flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8flowpathextremeup.py" line="79"/>
        <source>Slope area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8flowpathextremeup.py" line="81"/>
        <source>Outlets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8flowpathextremeup.py" line="86"/>
        <source>Calculate minimum upslope value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8flowpathextremeup.py" line="89"/>
        <source>Check for edge contamination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8flowpathextremeup.py" line="93"/>
        <source>Extreme value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>D8HDistToStrm</name>
    <message>
        <location filename="../processing/taudem/d8hdisttostrm.py" line="50"/>
        <source>D8 distance to streams</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8hdisttostrm.py" line="53"/>
        <source>Specialized grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8hdisttostrm.py" line="59"/>
        <source>dem,hydrology,d8,stream,distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8hdisttostrm.py" line="62"/>
        <source>Computes the horizontal distance to stream for each grid cell, moving downslope according to the D8 flow model, until a stream grid cell is encountered.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8hdisttostrm.py" line="73"/>
        <source>D8 flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8hdisttostrm.py" line="75"/>
        <source>Stream raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8hdisttostrm.py" line="77"/>
        <source>Threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/d8hdisttostrm.py" line="82"/>
        <source>Distance to streams</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DepthEnvelopeRaster</name>
    <message>
        <location filename="../processing/envelope.py" line="312"/>
        <source>Maximum Depth of a group of scenarios</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/envelope.py" line="309"/>
        <source>crgeng Depth raster envelope</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DepthRaster</name>
    <message>
        <location filename="../processing/depth.py" line="82"/>
        <source>Depth Raster Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/depth.py" line="90"/>
        <source>Terrain Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/depth.py" line="73"/>
        <source>crgeng Depth raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/depth.py" line="76"/>
        <source>River and free surface flow mapping</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../gui/widgets/notes_expanded_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/writer_crgeng_file.ui" line="20"/>
        <source>Output settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/writer_crgeng_file.ui" line="33"/>
        <source>Z2 (m NGF)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/writer_crgeng_file.ui" line="40"/>
        <source>dz (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/writer_crgeng_file.ui" line="47"/>
        <source>Z1 (m NGF)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/writer_crgeng_file.ui" line="79"/>
        <source>Method settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/writer_crgeng_file.ui" line="187"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/writer_crgeng_file.ui" line="96"/>
        <source>EPSG:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/writer_crgeng_file.ui" line="209"/>
        <source>Remove row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/writer_crgeng_file.ui" line="149"/>
        <source>Output name and output duration :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/writer_crgeng_file.ui" line="156"/>
        <source>EPSG selector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_regulation.ui" line="49"/>
        <source>Scenario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/writer_crgeng_file.ui" line="246"/>
        <source>Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/writer_crgeng_file.ui" line="251"/>
        <source>Temps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/writer_crgeng_file.ui" line="287"/>
        <source>nb hauteur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/writer_crgeng_file.ui" line="334"/>
        <source>nb vitesse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/writer_crgeng_file.ui" line="355"/>
        <source>v_sub</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/csvloader_on_axis.ui" line="86"/>
        <source>Axe 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/csvloader_on_axis.ui" line="145"/>
        <source>Axe 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_regulation.ui" line="30"/>
        <source>Open Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_regulation.ui" line="44"/>
        <source>Regulation file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_regulation.ui" line="54"/>
        <source>Line number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_regulation.ui" line="59"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DinfAvalanche</name>
    <message>
        <location filename="../processing/taudem/dinfavalanche.py" line="53"/>
        <source>D-infinity avalanche runout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfavalanche.py" line="56"/>
        <source>Specialized grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfavalanche.py" line="62"/>
        <source>dem,hydrology,d-infinity,avalanche</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfavalanche.py" line="65"/>
        <source>Identifies an avalanche&apos;s affected area and the flow path length to each cell in that affected area.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfavalanche.py" line="75"/>
        <source>Pit filled elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfavalanche.py" line="77"/>
        <source>D-infinity flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfavalanche.py" line="79"/>
        <source>Avalanche source site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfavalanche.py" line="81"/>
        <source>Proportion threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfavalanche.py" line="86"/>
        <source>Alpha angle threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfavalanche.py" line="91"/>
        <source>Measure distance as a straight line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfavalanche.py" line="95"/>
        <source>Runout zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfavalanche.py" line="97"/>
        <source>Path distance</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DinfConcLimAccum</name>
    <message>
        <location filename="../processing/taudem/dinfconclimaccum.py" line="57"/>
        <source>D-infinity concentration limited acccumulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfconclimaccum.py" line="60"/>
        <source>Specialized grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfconclimaccum.py" line="66"/>
        <source>dem,hydrology,d-infinity,concentration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfconclimaccum.py" line="69"/>
        <source>Creates a grid of the concentration of a substance at each location in the domain, where the supply of substance from a supply area is loaded into the flow at a concentration or solubility threshold.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfconclimaccum.py" line="81"/>
        <source>D-infinity flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfconclimaccum.py" line="83"/>
        <source>Decay multiplier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfconclimaccum.py" line="85"/>
        <source>Disturbance indicator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfconclimaccum.py" line="87"/>
        <source>Effective runoff weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfconclimaccum.py" line="89"/>
        <source>Outlets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfconclimaccum.py" line="94"/>
        <source>Concentration threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfconclimaccum.py" line="99"/>
        <source>Check for edge contamination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfconclimaccum.py" line="103"/>
        <source>Concentration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DinfDecayAccum</name>
    <message>
        <location filename="../processing/taudem/dinfdecayaccum.py" line="53"/>
        <source>D-infinity decaying accumulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdecayaccum.py" line="56"/>
        <source>Specialized grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdecayaccum.py" line="62"/>
        <source>dem,hydrology,d8,stream,distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdecayaccum.py" line="65"/>
        <source>Creates a grid of the accumulated quantity at each location in the domain where the quantity accumulates with the D-infinity flow field, but is subject to first order decay in moving from cell to cell.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdecayaccum.py" line="77"/>
        <source>D-infinity flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdecayaccum.py" line="79"/>
        <source>Decay multiplier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdecayaccum.py" line="81"/>
        <source>Weight grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdecayaccum.py" line="85"/>
        <source>Outlets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdecayaccum.py" line="90"/>
        <source>Check for edge contamination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdecayaccum.py" line="94"/>
        <source>Decayed specific catchment area</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DinfDistDown</name>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="53"/>
        <source>D-infinity distance down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="56"/>
        <source>Specialized grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="62"/>
        <source>dem,hydrology,d-infinity,downslope,distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="65"/>
        <source>Calculates the distance downslope to a stream using the D-infinity flow model.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="75"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="75"/>
        <source>Minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="75"/>
        <source>Maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="79"/>
        <source>Horizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="79"/>
        <source>Vertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="79"/>
        <source>Pythagoras</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="79"/>
        <source>Surface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="84"/>
        <source>Pit filled elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="86"/>
        <source>D-infinity flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="88"/>
        <source>Stream raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="90"/>
        <source>Weight grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="93"/>
        <source>Statistical method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="97"/>
        <source>Distance method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="101"/>
        <source>Check for edge contamination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistdown.py" line="105"/>
        <source>D-infinity drop to stream</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DinfDistUp</name>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="108"/>
        <source>D-infinity distance up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="57"/>
        <source>Specialized grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="63"/>
        <source>dem,hydrology,d-infinity,upslope,distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="66"/>
        <source>Calculates the distance from each grid cell up to the ridge cells along the reverse D-infinity flow directions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="76"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="76"/>
        <source>Minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="76"/>
        <source>Maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="80"/>
        <source>Horizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="80"/>
        <source>Vertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="80"/>
        <source>Pythagoras</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="80"/>
        <source>Surface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="85"/>
        <source>Pit filled elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="87"/>
        <source>D-infinity flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="89"/>
        <source>Slope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="91"/>
        <source>Proportion threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="96"/>
        <source>Statistical method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="100"/>
        <source>Distance method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfdistup.py" line="104"/>
        <source>Check for edge contamination</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DinfFlowDir</name>
    <message>
        <location filename="../processing/taudem/dinfflowdir.py" line="76"/>
        <source>D-infinity flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfflowdir.py" line="50"/>
        <source>Basic grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfflowdir.py" line="56"/>
        <source>dem,hydrology,d-infinity,flow directions,slope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfflowdir.py" line="59"/>
        <source>Assigns a flow direction based on the D-infinity flow method using the steepest slope of a triangular facet (Tarboton, 1997, &quot;A New Method for the Determination of Flow Directions and Contributing Areas in Grid Digital Elevation Models&quot;, Water Resources Research, 33(2): 309-319)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfflowdir.py" line="73"/>
        <source>Pit filled elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfflowdir.py" line="78"/>
        <source>D-infinity slope</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DinfRevAccum</name>
    <message>
        <location filename="../processing/taudem/dinfrevaccum.py" line="47"/>
        <source>D-infinity reverse accumulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfrevaccum.py" line="50"/>
        <source>Specialized grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfrevaccum.py" line="56"/>
        <source>dem,hydrology,d-infinity,accumulation,reverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfrevaccum.py" line="59"/>
        <source>Works in a similar way to evaluation of weighted contributing area, except that the accumulation is by propagating the weight loadings upslope along the reverse of the flow directions to accumulate the quantity of weight loading downslope from each grid cell.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfrevaccum.py" line="73"/>
        <source>D-infinity flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfrevaccum.py" line="75"/>
        <source>Weight grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfrevaccum.py" line="78"/>
        <source>Reverse accumulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfrevaccum.py" line="80"/>
        <source>Maximum downslope</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DinfTransLimAccum</name>
    <message>
        <location filename="../processing/taudem/dinftranslimaccum.py" line="57"/>
        <source>D-infinity transport limited accumulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinftranslimaccum.py" line="60"/>
        <source>Specialized grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinftranslimaccum.py" line="66"/>
        <source>dem,hydrology,d-infinity,transport,deposition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinftranslimaccum.py" line="69"/>
        <source>Calculates the transport and deposition of a substance (e.g. sediment) that may be limited by both supply and the capacity of the flow field to transport it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinftranslimaccum.py" line="80"/>
        <source>D-infinity flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinftranslimaccum.py" line="82"/>
        <source>Transport supply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinftranslimaccum.py" line="84"/>
        <source>Transport capacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinftranslimaccum.py" line="86"/>
        <source>Input concentration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinftranslimaccum.py" line="90"/>
        <source>Outlets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinftranslimaccum.py" line="95"/>
        <source>Check for edge contamination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinftranslimaccum.py" line="99"/>
        <source>Transport limited accumulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinftranslimaccum.py" line="101"/>
        <source>Deposition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinftranslimaccum.py" line="103"/>
        <source>Output concentration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinftranslimaccum.py" line="141"/>
        <source>Output concentration is not set.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DinfUpDependence</name>
    <message>
        <location filename="../processing/taudem/dinfupdependence.py" line="47"/>
        <source>D-infinity upslope dependence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfupdependence.py" line="50"/>
        <source>Specialized grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfupdependence.py" line="56"/>
        <source>dem,hydrology,d8,stream,distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfupdependence.py" line="59"/>
        <source>Quantifies the amount each grid cell in the domain contributes to a destination set of grid cells.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfupdependence.py" line="69"/>
        <source>D-infinity flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfupdependence.py" line="71"/>
        <source>Disturbance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dinfupdependence.py" line="74"/>
        <source>Upslope dependence</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DropAnalysis</name>
    <message>
        <location filename="../processing/taudem/dropanalysis.py" line="59"/>
        <source>Stream drop analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dropanalysis.py" line="62"/>
        <source>Stream network analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dropanalysis.py" line="68"/>
        <source>dem,hydrology,stream,drop,statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dropanalysis.py" line="71"/>
        <source>Applies a series of thresholds (determined from the input parameters) to the input accumulated stream source grid and outputs the results in the stream drop statistics table.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dropanalysis.py" line="83"/>
        <source>Logarithmic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dropanalysis.py" line="84"/>
        <source>Arithmetic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dropanalysis.py" line="87"/>
        <source>Pit filled elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dropanalysis.py" line="89"/>
        <source>D8 flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dropanalysis.py" line="91"/>
        <source>D8 contributing area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dropanalysis.py" line="93"/>
        <source>Accumulated stream source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dropanalysis.py" line="95"/>
        <source>Outlets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dropanalysis.py" line="98"/>
        <source>Minimum threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dropanalysis.py" line="102"/>
        <source>Maximum threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dropanalysis.py" line="106"/>
        <source>Number of drop thresholds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dropanalysis.py" line="110"/>
        <source>Type of threshold step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dropanalysis.py" line="115"/>
        <source>Drop analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/dropanalysis.py" line="115"/>
        <source>Text files (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DryScenarioMenu</name>
    <message>
        <location filename="../gui/project_tree.py" line="157"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="158"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EnvelopeProcessing</name>
    <message>
        <location filename="../processing/envelope.py" line="125"/>
        <source>Terrain Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/envelope.py" line="118"/>
        <source>River and free surface flow mapping</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../advanced_tools/extract.ui" line="91"/>
        <source>Extract</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/crgeng.ui" line="37"/>
        <source>Open log file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/extract.ui" line="78"/>
        <source>Wexpdess</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/crgeng.ui" line="14"/>
        <source>Crgeng</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/animeau.ui" line="30"/>
        <source>Go !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/crgeng.ui" line="57"/>
        <source>Generate File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/animeau.ui" line="14"/>
        <source>Anim-Eau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../advanced_tools/animeau.ui" line="40"/>
        <source>Use param file (optional)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/text_info.ui" line="14"/>
        <source>Info form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.ui" line="20"/>
        <source>Dry inflow manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.ui" line="26"/>
        <source>Import from file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.ui" line="40"/>
        <source>Dry inflow scenarios</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.ui" line="46"/>
        <source>Dry inflow scenario settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.ui" line="52"/>
        <source>Weekdays</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.ui" line="75"/>
        <source>#DRY SECTOR NAME#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.ui" line="62"/>
        <source>Weekend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="315"/>
        <source>Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.ui" line="98"/>
        <source>Sector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.ui" line="103"/>
        <source>VEU (m3/d)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.ui" line="108"/>
        <source>VECPP (m3/d)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.ui" line="113"/>
        <source>EU mod. coef.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.ui" line="118"/>
        <source>ECPP mod. coef.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_sector_settings.ui" line="26"/>
        <source>Dry inflow sectors settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_sector_settings.ui" line="39"/>
        <source>Dry inflow sectors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_sector_settings.ui" line="79"/>
        <source>Nouvelle ligne</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_sector_settings.ui" line="89"/>
        <source>Sectors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_sector_settings.ui" line="94"/>
        <source>Mod. curve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_sector_settings.ui" line="99"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_sector_settings.ui" line="117"/>
        <source>Hydrographs in sector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_sector_settings.ui" line="129"/>
        <source>#SECTOR NAME#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="605"/>
        <source>Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_sector_settings.ui" line="150"/>
        <source>Hydrograph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_sector_settings.ui" line="155"/>
        <source>Def by sector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_sector_settings.ui" line="160"/>
        <source>Contribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_sector_settings.ui" line="165"/>
        <source>Distrib. Coef.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_sector_settings.ui" line="170"/>
        <source>Constant flow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_sector_settings.ui" line="175"/>
        <source>Lag time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_sector_settings.ui" line="195"/>
        <source>Dry inflow modulation curves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/branch_manager.ui" line="14"/>
        <source>Terrain manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/terrain_manager.ui" line="24"/>
        <source>DEM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/terrain_manager.ui" line="114"/>
        <source>Priority</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/terrain_manager.ui" line="119"/>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/terrain_manager.ui" line="131"/>
        <source>Terrain points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_grid.ui" line="26"/>
        <source>Radar grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_grid.ui" line="32"/>
        <source>X0 (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_grid.ui" line="39"/>
        <source>Y0 (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_grid.ui" line="46"/>
        <source>dx (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_grid.ui" line="53"/>
        <source>dy (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_grid.ui" line="60"/>
        <source>nx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_grid.ui" line="67"/>
        <source>ny</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_grid.ui" line="74"/>
        <source>CRB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="14"/>
        <source>Visu tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="48"/>
        <source>Simulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="57"/>
        <source>Scenario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="67"/>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="80"/>
        <source>filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="93"/>
        <source>Object type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="100"/>
        <source>Object name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="124"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="131"/>
        <source>Time format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="138"/>
        <source>Attribute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="146"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="151"/>
        <source>Decimal hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="172"/>
        <source>Time origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="182"/>
        <source>yyyy-MM-dd HH:mm:ss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="206"/>
        <source>Show legend on graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="213"/>
        <source>Remove curve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph_constrain.ui" line="118"/>
        <source>Graph title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph_constrain.ui" line="132"/>
        <source>Left axis title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="268"/>
        <source>Right axis title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="299"/>
        <source>Date format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="306"/>
        <source>Decimal Hour format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="334"/>
        <source>Add step right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="341"/>
        <source>Add line right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="348"/>
        <source>Add line left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="355"/>
        <source>Add step left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="364"/>
        <source>Reset Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="373"/>
        <source>&lt;&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="380"/>
        <source>Save graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="387"/>
        <source>Load graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph.ui" line="394"/>
        <source>&gt;&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="14"/>
        <source>Rains</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="31"/>
        <source>Synthetic rainfall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="37"/>
        <source>Montana coefficients</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="66"/>
        <source>Caquot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="79"/>
        <source>Simple triangular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="92"/>
        <source>Double triangular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="109"/>
        <source>Hyetograph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="132"/>
        <source>Gage rainfall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="151"/>
        <source>Gage rainfall data:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/time_control.ui" line="124"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="207"/>
        <source>Rain gage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="212"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="241"/>
        <source>Gage rainfall list:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="257"/>
        <source>Create Thiessen coefficient files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="265"/>
        <source>Radar rainfall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="307"/>
        <source>Radar rainfall described by one raster files by time step, giving accumulated rainfall for time step (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="202"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="325"/>
        <source>Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/rain_manager.ui" line="433"/>
        <source>MAGES rainfall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.ui" line="14"/>
        <source>External tables manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.ui" line="26"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.ui" line="32"/>
        <source>#nblines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.ui" line="79"/>
        <source>Loaded tables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.ui" line="193"/>
        <source>Import table to project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.ui" line="199"/>
        <source>#table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.ui" line="231"/>
        <source>Target table :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.ui" line="241"/>
        <source>Target schema :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.ui" line="251"/>
        <source>Advanced mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/work_manager.ui" line="285"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/land_type.ui" line="34"/>
        <source>Land types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/land_type.ui" line="27"/>
        <source>Load preset land type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_viewer.ui" line="29"/>
        <source>Section viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_viewer.ui" line="35"/>
        <source>Export data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_viewer.ui" line="51"/>
        <source>Terrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_viewer.ui" line="221"/>
        <source>Z points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_viewer.ui" line="240"/>
        <source>Combined topo (Terrain and points)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_viewer.ui" line="253"/>
        <source>Hydra section</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/type_points_manager.ui" line="14"/>
        <source>Terrain points type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/pollution_land_accumulation.ui" line="26"/>
        <source>Pollution land accumulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/pollution_land_accumulation.ui" line="32"/>
        <source>Separative network storage (kg/ha)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/pollution_land_accumulation.ui" line="110"/>
        <source>Rural</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/pollution_land_accumulation.ui" line="115"/>
        <source>Industrial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/pollution_land_accumulation.ui" line="120"/>
        <source>Suburban housing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/pollution_land_accumulation.ui" line="125"/>
        <source>Dense housing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/pollution_land_accumulation.ui" line="130"/>
        <source>MES</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/pollution_land_accumulation.ui" line="135"/>
        <source>DBO5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/pollution_land_accumulation.ui" line="140"/>
        <source>DCO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/pollution_land_accumulation.ui" line="145"/>
        <source>NTK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/pollution_land_accumulation.ui" line="94"/>
        <source>Combined network storage (kg/ha)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/pollution_land_accumulation.ui" line="156"/>
        <source>Pollution storage parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/pollution_land_accumulation.ui" line="162"/>
        <source>Regeneration time (days)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/pollution_land_accumulation.ui" line="169"/>
        <source>Stripping coefficient A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/pollution_land_accumulation.ui" line="176"/>
        <source>Stripping coefficient B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_domain_2d.ui" line="14"/>
        <source>2D domain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/time_control.ui" line="14"/>
        <source>Time control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/time_control.ui" line="111"/>
        <source>delay (sec)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/time_control.ui" line="131"/>
        <source>stride</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/geometry_selector.ui" line="29"/>
        <source>Geometry manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/geometry_manager.ui" line="60"/>
        <source>Pipe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/geometry_manager.ui" line="95"/>
        <source>Channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/geometry_manager.ui" line="130"/>
        <source>Valley</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph_constrain.ui" line="14"/>
        <source>Constrain Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph_constrain.ui" line="55"/>
        <source>Reverse links</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph_constrain.ui" line="62"/>
        <source>Check all links</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph_constrain.ui" line="69"/>
        <source>Copy data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph_constrain.ui" line="139"/>
        <source>Q(m³)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph_constrain.ui" line="149"/>
        <source>Show each link line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/visu_graph_constrain.ui" line="156"/>
        <source>Copy image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/regulation_and_configuration.ui" line="48"/>
        <source>Configurations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/model_ordering.ui" line="170"/>
        <source>Models</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/pipe_section.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_tree_widget.ui" line="64"/>
        <source>New serie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_tree_widget.ui" line="136"/>
        <source>Current bloc&apos;s scenarios</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/aeraulics.ui" line="20"/>
        <source>Enable aeraulics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/aeraulics.ui" line="39"/>
        <source>Speed of sound in air</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/aeraulics.ui" line="49"/>
        <source>Absolute ambient air presure [Pa]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/aeraulics.ui" line="59"/>
        <source>Average altitude [msl]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/aeraulics.ui" line="69"/>
        <source>Air water skin friction coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/aeraulics.ui" line="79"/>
        <source>Air pipe friction coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/aeraulics.ui" line="89"/>
        <source>Air temperature in network [°C]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/aeraulics.ui" line="99"/>
        <source>Ambient air temperature [°C]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/aeraulics.ui" line="109"/>
        <source>Leak pressure [Pa]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="20"/>
        <source>Initial conditions settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="35"/>
        <source>Start from rest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="48"/>
        <source>Duration of initial conditions settings simulation (h:m):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="55"/>
        <source>Rstart time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="75"/>
        <source>[PREMIUM] Hot start scenario:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="98"/>
        <source>Reference scenario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="107"/>
        <source>[PREMIUM] Set reference scenario:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="88"/>
        <source>Computation output time intervals:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="97"/>
        <source>Output time step:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="152"/>
        <source>t max:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="159"/>
        <source>t min:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="232"/>
        <source>Min:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="259"/>
        <source>Water surface variation between 2 time steps (m):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="239"/>
        <source>Max:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="173"/>
        <source>Hydraulic computation time step:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="299"/>
        <source>Hydrologic computation time step:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="313"/>
        <source>Computation starting date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="302"/>
        <source>Computation duration:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="342"/>
        <source>[PREMIUM] Output time for hotstart option:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="33"/>
        <source>Computation mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="39"/>
        <source>Hydraulic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="52"/>
        <source>Hydrologic and hydraulic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="65"/>
        <source>Hydrology</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="454"/>
        <source>Graphic control during computation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/regulation_and_configuration.ui" line="32"/>
        <source>[PREMIUM] Regulation settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/regulation_and_configuration.ui" line="74"/>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/regulation_and_configuration.ui" line="85"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/notes_widget.ui" line="58"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="29"/>
        <source>[PREMIUM] Upgrade computation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="407"/>
        <source>Remove row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="65"/>
        <source>Branch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="70"/>
        <source>Reach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="75"/>
        <source>Domain 2D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/array_widget.ui" line="72"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="106"/>
        <source>Starting time (for upgraded computation)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="113"/>
        <source>Minimum surface for 2D domains (m²):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="165"/>
        <source>Domain type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="243"/>
        <source>Domain name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="394"/>
        <source>Option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="186"/>
        <source>[PREMIUM] Domain 2D runoff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="201"/>
        <source>Activate automatic runoff computation in 2D elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="287"/>
        <source>OUTPUT-FILE : external file for computation options modifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="358"/>
        <source>[PREMIUM] VERSION: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="383"/>
        <source># kernel info #</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="398"/>
        <source>[PREMIUM] Strickler coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="413"/>
        <source>Strickler coefficient adjustement (Kmin, Kmaj, PK1, PK2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="610"/>
        <source>Element type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="615"/>
        <source>Element name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="480"/>
        <source>Kmin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="485"/>
        <source>Kmaj</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="490"/>
        <source>PK1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="495"/>
        <source>PK2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="524"/>
        <source>[PREMIUM] Output option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="582"/>
        <source>Additionnal output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/model_ordering.ui" line="20"/>
        <source>Model connexion method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/model_ordering.ui" line="26"/>
        <source>[PREMIUM] Global</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/model_ordering.ui" line="39"/>
        <source>Cascade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/model_ordering.ui" line="49"/>
        <source>[PREMIUM] Mixed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/model_ordering.ui" line="83"/>
        <source>Cascade ordering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/model_ordering.ui" line="183"/>
        <source>Groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/array_widget.ui" line="104"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="29"/>
        <source>Rainfall scenario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="44"/>
        <source>Dry inflow scenario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="59"/>
        <source>Hydrographs from other scenario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="74"/>
        <source>Initial soil conditions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="86"/>
        <source>From previous scenario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="96"/>
        <source>Horner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="106"/>
        <source>Constant runoff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="119"/>
        <source>Holtan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="126"/>
        <source>GR4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="133"/>
        <source>Hydra</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="140"/>
        <source>SCS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="178"/>
        <source>Rain data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="185"/>
        <source>[PREMIUM] Hydrology network auto-dimensioning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="192"/>
        <source>Wind computation option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="204"/>
        <source>Air density</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="211"/>
        <source>Skin friction coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="218"/>
        <source>Wind data file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="322"/>
        <source>External files hydrographs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="332"/>
        <source>[PREMIUM] Hydrograph derouting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="374"/>
        <source>Model from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="379"/>
        <source>From...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="384"/>
        <source>Model to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/hydrology_settings.ui" line="389"/>
        <source>To...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="741"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="25"/>
        <source>[PREMIUM] Transport type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="33"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="38"/>
        <source>Pollution generation and network transport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="43"/>
        <source>Physicochemical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="48"/>
        <source>Tracer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="53"/>
        <source>Sediment transport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="61"/>
        <source>Integration of dispersion / diffusion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="119"/>
        <source>Settings file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="186"/>
        <source>Total Kjeldahl Nitrogen (TKN)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="193"/>
        <source>Chemical oxygen demand (COD)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="200"/>
        <source>Total suspended sediments (TSS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="207"/>
        <source>Biochemical oxygen demand in 5 days (BOD5)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="338"/>
        <source>Tp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="367"/>
        <source>MinO2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="396"/>
        <source>K1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="425"/>
        <source>K2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="454"/>
        <source>KNR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="483"/>
        <source>KN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="308"/>
        <source>BGN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="541"/>
        <source>O2sat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="318"/>
        <source>Parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="746"/>
        <source>Unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="751"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="346"/>
        <source>20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="351"/>
        <source>Celcius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="359"/>
        <source>Water temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="375"/>
        <source>1.5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="554"/>
        <source>mg/L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="388"/>
        <source>Dissolved 02 minimum value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="404"/>
        <source>0.2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="496"/>
        <source>1/j</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="417"/>
        <source>BOD5 biodegradation constant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="433"/>
        <source>0.45</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="446"/>
        <source>Open-channel water reoxygenation constant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="462"/>
        <source>0.09</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="475"/>
        <source>Ammonia biodegradation constant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="491"/>
        <source>0.035</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="504"/>
        <source>Ammonia nitrification constant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="512"/>
        <source>BDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="520"/>
        <source>1.50</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="533"/>
        <source>BOD5 background noise due to the microorganisms and algae activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="549"/>
        <source>9.07</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="562"/>
        <source>Dissolved oxygen in water at saturation at Tp °C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="583"/>
        <source>Decay rate (1/j)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="590"/>
        <source>Eschericia Coli (E. Coli)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="600"/>
        <source>Intestinal Enteroques (I. Ent.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="607"/>
        <source>Ad1 bacterio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="623"/>
        <source>Ad2 bacterio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="664"/>
        <source>Fall velocity (m/hr)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="772"/>
        <source>1D domain : dispersion coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="796"/>
        <source>2D domain : longitudinal diffusion coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="820"/>
        <source>2D domain :  lateral diffusion coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="756"/>
        <source>20.000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="812"/>
        <source>m²/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="780"/>
        <source>41.200</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/transport.ui" line="804"/>
        <source>0.4500</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="104"/>
        <source>Days per scenario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="289"/>
        <source>Duration of initial conditions settings simulation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="306"/>
        <source>Computation duration (days):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="333"/>
        <source>#min date#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="365"/>
        <source>Bloc&apos;s settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="371"/>
        <source>Start date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/serie_settings.ui" line="404"/>
        <source>Bloc1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/split_law_type_widget.ui" line="73"/>
        <source>Contraction coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/split_law_type_widget.ui" line="80"/>
        <source>Z invert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/split_law_type_widget.ui" line="53"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/split_law_type_widget.ui" line="87"/>
        <source>Diameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.ui" line="66"/>
        <source>constrain :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.ui" line="75"/>
        <source>distance:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.ui" line="85"/>
        <source>discretisation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.ui" line="95"/>
        <source>Ignore terrain points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/pipe_section.ui" line="95"/>
        <source>Geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.ui" line="142"/>
        <source>Exchange coefficient : Flood plain -&gt; Storage area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.ui" line="179"/>
        <source>Exchange coefficient : Storage area -&gt; Flood plain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/valley_geom_widget.ui" line="195"/>
        <source>Active flood plain ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/regfile_table_widget.ui" line="64"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/array_widget.ui" line="182"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/regfile_table_widget.ui" line="176"/>
        <source>Duplicate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/branch_manager.ui" line="29"/>
        <source>clic and select a branch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/branch_manager.ui" line="43"/>
        <source>Remove the selected branch from the manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/branch_manager.ui" line="94"/>
        <source>Reach to display :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/time_input_widget.ui" line="51"/>
        <source>h :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/time_input_widget.ui" line="77"/>
        <source>m :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/time_input_widget.ui" line="103"/>
        <source>s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/channel_geometry.ui" line="35"/>
        <source>with flood plain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/channel_geometry.ui" line="53"/>
        <source>Width (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/channel_geometry.ui" line="60"/>
        <source>Slope (m/m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="263"/>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/scenario_comparator_manager.ui" line="14"/>
        <source>Scenario Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/scenario_comparator_manager.ui" line="62"/>
        <source>Scenarios to display :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/array_widget.ui" line="157"/>
        <source>column_1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/widgets/array_widget.ui" line="162"/>
        <source>column_2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="14"/>
        <source>Constrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="22"/>
        <source>Points proximity (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="54"/>
        <source>#Nb Points#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="76"/>
        <source>Update links</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="89"/>
        <source>Links Generation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="95"/>
        <source>Standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="102"/>
        <source>Flood plain transect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="109"/>
        <source>Non topological</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="116"/>
        <source>Strickler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="123"/>
        <source>Overflow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="130"/>
        <source>Connector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="137"/>
        <source>Porous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="144"/>
        <source>Boundary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="188"/>
        <source>Points type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="218"/>
        <source>Element length (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="225"/>
        <source>Create cross section </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="247"/>
        <source>Terrain line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_constrain.ui" line="266"/>
        <source>Notes editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/circular_section.ui" line="60"/>
        <source>Diameter [m]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/pipe_section.ui" line="86"/>
        <source>Strickler coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/pipe_section.ui" line="45"/>
        <source>Invert altitude [m]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/pipe_section.ui" line="54"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/pipe_section.ui" line="61"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/channel_section.ui" line="96"/>
        <source>Strickler coefficient Rk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/channel_section.ui" line="122"/>
        <source>Flood plain Rk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/ovoid_section.ui" line="73"/>
        <source>Top diameter [m]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/ovoid_section.ui" line="80"/>
        <source>Height [m]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/ovoid_section.ui" line="148"/>
        <source>Invert diameter [m]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_section.ui" line="38"/>
        <source>Sinusoity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_section.ui" line="80"/>
        <source>Strickler coefficients</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_section.ui" line="98"/>
        <source>Riverbed </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_section.ui" line="127"/>
        <source>Flood plain </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_geometry.ui" line="41"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_geometry.ui" line="59"/>
        <source>Transect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_geometry.ui" line="78"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set target profile from hydra (simplified) profile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_geometry.ui" line="81"/>
        <source>Simplify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_geometry.ui" line="100"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reset target profile profile to topography.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_geometry.ui" line="103"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_geometry.ui" line="112"/>
        <source>Levee altitudes [m]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_geometry.ui" line="143"/>
        <source>Left bank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_geometry.ui" line="150"/>
        <source>Right bank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_geometry.ui" line="199"/>
        <source>Autofit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_geometry.ui" line="291"/>
        <source>Active flood plain </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_geometry.ui" line="304"/>
        <source>Exchange coefficients</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_geometry.ui" line="347"/>
        <source>To storage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_geometry.ui" line="354"/>
        <source>From storage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/cross_section/valley_geometry.ui" line="392"/>
        <source>ZB ARRAYS with very long </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/dry_inflow_manager.ui" line="82"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GageWatershed</name>
    <message>
        <location filename="../processing/taudem/gagewatershed.py" line="78"/>
        <source>Gage watershed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/gagewatershed.py" line="55"/>
        <source>Stream network analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/gagewatershed.py" line="61"/>
        <source>dem,hydrology,gage,watershed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/gagewatershed.py" line="64"/>
        <source>Calculates Gage watersheds grid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/gagewatershed.py" line="73"/>
        <source>D8 flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/gagewatershed.py" line="75"/>
        <source>Outlets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/gagewatershed.py" line="80"/>
        <source>Watershed downslope connectivity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/gagewatershed.py" line="80"/>
        <source>Text files (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GridNet</name>
    <message>
        <location filename="../processing/taudem/gridnet.py" line="55"/>
        <source>Grid Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/gridnet.py" line="58"/>
        <source>Basic grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/gridnet.py" line="64"/>
        <source>dem,hydrology,strahler,order,path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/gridnet.py" line="67"/>
        <source>Creates 3 grids that contain for each grid cell: 1) the longest upslope path length, 2) the total upslope path length, and 3) the Strahler order number.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/gridnet.py" line="78"/>
        <source>D9 flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/gridnet.py" line="80"/>
        <source>Mask grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/gridnet.py" line="83"/>
        <source>Mask threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/gridnet.py" line="88"/>
        <source>Outlets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/gridnet.py" line="93"/>
        <source>Longest upslope length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/gridnet.py" line="95"/>
        <source>Total upslope length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/gridnet.py" line="97"/>
        <source>Strahler network order</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HazardVectorisation</name>
    <message>
        <location filename="../processing/hazard_vectorisation.py" line="47"/>
        <source>Hazard vectorisation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/hazard_vectorisation.py" line="50"/>
        <source>River and free surface flow mapping</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LengthArea</name>
    <message>
        <location filename="../processing/taudem/lengtharea.py" line="50"/>
        <source>Length area stream source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/lengtharea.py" line="53"/>
        <source>Stream network analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/lengtharea.py" line="59"/>
        <source>dem,hydrology,dem,threshold,compare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/lengtharea.py" line="73"/>
        <source>Maximum upslope length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/lengtharea.py" line="75"/>
        <source>Contributing area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/lengtharea.py" line="77"/>
        <source>Multiplier (M)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/lengtharea.py" line="81"/>
        <source>Exponent (y)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/lengtharea.py" line="86"/>
        <source>Stream source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/lengtharea.py" line="62"/>
        <source>Creates an indicator grid (1, 0) that evaluates A &amp;gt;= M·(L^y) based on upslope path length, D8 contributing area grid inputs, and parameters M and y.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../gui/settings.ui" line="26"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.ui" line="36"/>
        <source>Connection settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.ui" line="42"/>
        <source>Local dependencies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.ui" line="65"/>
        <source>Browse...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.ui" line="55"/>
        <source>PATH to PostgreSQL root folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.ui" line="72"/>
        <source>PATH to Python3 executable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.ui" line="88"/>
        <source>Service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.ui" line="94"/>
        <source>Service name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.ui" line="128"/>
        <source>Map settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.ui" line="134"/>
        <source>Map parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.ui" line="143"/>
        <source>Snap distance (px)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.ui" line="180"/>
        <source>Synthetic results settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.ui" line="186"/>
        <source>Current model:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.ui" line="199"/>
        <source>#current_model#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.ui" line="235"/>
        <source>Item count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.ui" line="240"/>
        <source>Last refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.ui" line="245"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/settings.ui" line="250"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/regulation.ui" line="26"/>
        <source>Regulated objects manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/regulation.ui" line="54"/>
        <source>Item name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/regulation.ui" line="59"/>
        <source>Type item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/regulation.ui" line="64"/>
        <source>File name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/regulation.ui" line="69"/>
        <source>Line number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/regulation.ui" line="74"/>
        <source>Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/regulation.ui" line="79"/>
        <source>Scenario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/regulation.ui" line="84"/>
        <source>Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/regulation.ui" line="92"/>
        <source>Current scenario:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/validity.ui" line="126"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/configuration.ui" line="146"/>
        <source>Copy data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/regulation.ui" line="123"/>
        <source>Open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/regulation.ui" line="130"/>
        <source>#count_regul_object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/validity.ui" line="26"/>
        <source>Validity manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/search.ui" line="75"/>
        <source>Current model :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/configuration.ui" line="26"/>
        <source>Configured objects manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/configuration.ui" line="39"/>
        <source>Number of configured objects :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/validity.ui" line="39"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/configuration.ui" line="66"/>
        <source>Undo configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/validity.ui" line="102"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/configuration.ui" line="103"/>
        <source>Table Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/validity.ui" line="77"/>
        <source>Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/river_cross_section_profile.ui" line="55"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/configuration.ui" line="118"/>
        <source>Parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/configuration.ui" line="123"/>
        <source>Default value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/configuration.ui" line="128"/>
        <source>Config value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/configuration.ui" line="139"/>
        <source>Current configuration:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/validity.ui" line="49"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/validity.ui" line="72"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/validity.ui" line="87"/>
        <source>Invalidity reason (rule broken)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/validity.ui" line="95"/>
        <source>Number of invalid objects :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/validity.ui" line="119"/>
        <source>(n nodes, s singularities and l links)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/validity.ui" line="133"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_rain.ui" line="26"/>
        <source>Radar rain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_rain.ui" line="32"/>
        <source>Template:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_rain.ui" line="42"/>
        <source>Rain name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_rain.ui" line="49"/>
        <source>Source files ESPG:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_rain.ui" line="59"/>
        <source>Select EPSG...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/radar_rain.ui" line="73"/>
        <source>Template must consist of a characters string with at least 12 consecutives &apos;?&apos; (Used to get the timestamp in tiff files names, format YYYYMMDDhhmm).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/search.ui" line="26"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/search.ui" line="35"/>
        <source>Search for object named:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/search.ui" line="58"/>
        <source>Search / Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/search.ui" line="65"/>
        <source>Search / Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/search.ui" line="82"/>
        <source>info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/results_selector.ui" line="26"/>
        <source>Results manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/results_selector.ui" line="39"/>
        <source>Hydraulics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/results_selector.ui" line="45"/>
        <source>Pipes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/results_selector.ui" line="52"/>
        <source>Links</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/results_selector.ui" line="59"/>
        <source>Surface elements (Streets, mesh, storages)
    /!\ May take some time to load (heavy data)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/results_selector.ui" line="70"/>
        <source>River flood plain
    /!\ May take some time to load (heavy data)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/results_selector.ui" line="81"/>
        <source>Hydrology</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/results_selector.ui" line="87"/>
        <source>Hydrology pipes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/results_selector.ui" line="94"/>
        <source>Catchments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/hydrograph_bc.ui" line="26"/>
        <source>Hydrograph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tank_bc.ui" line="164"/>
        <source>Pollution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_connect_bc.ui" line="131"/>
        <source>DBO5 (mg/L)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/hydrograph_bc.ui" line="66"/>
        <source>DCO (mg/L)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_connect_bc.ui" line="215"/>
        <source>MES (mg/L)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/hydrograph_bc.ui" line="86"/>
        <source>NTK (mg/L)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/hydrograph_bc.ui" line="192"/>
        <source>Dry weather</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/hydrograph_bc.ui" line="243"/>
        <source>Runoff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_connect_bc.ui" line="123"/>
        <source>Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_connect_bc.ui" line="205"/>
        <source>Ad4 (npp/100mL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_connect_bc.ui" line="195"/>
        <source>Ad3 (npp/100mL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_connect_bc.ui" line="175"/>
        <source>E. Coli (npp/100mL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_connect_bc.ui" line="141"/>
        <source>NH4 (mg/L)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_connect_bc.ui" line="185"/>
        <source>Ent. I (npp/100mL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_connect_bc.ui" line="151"/>
        <source>NO3 (mg/L)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_connect_bc.ui" line="161"/>
        <source>O2 (mg/L)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/hydrograph_bc.ui" line="275"/>
        <source>Constant dry flow (m3/s):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/hydrograph_bc.ui" line="288"/>
        <source>Dry inflow defined by sector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/hydrograph_bc.ui" line="311"/>
        <source>Sector #</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/hydrograph_bc.ui" line="318"/>
        <source>Storage area (m²)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tz_bc.ui" line="70"/>
        <source>External file data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tz_bc.ui" line="78"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tz_bc.ui" line="83"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tank_bc.ui" line="201"/>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tank_bc.ui" line="204"/>
        <source>Notes editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/routing_hydrology_link.ui" line="26"/>
        <source>Routing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/routing_hydrology_link.ui" line="38"/>
        <source>Split weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="74"/>
        <source>Length (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="777"/>
        <source>Average slope (m/m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/routing_hydrology_link.ui" line="102"/>
        <source>Split coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/routing_hydrology_link.ui" line="109"/>
        <source>Average section (m²)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/river_node.ui" line="26"/>
        <source>River node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/river_node.ui" line="32"/>
        <source>pk (km)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/river_node.ui" line="59"/>
        <source>Z invert (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/manhole_hydrology_node.ui" line="56"/>
        <source>Ground elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/manhole_hydrology_node.ui" line="46"/>
        <source>Area (m²)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/mkb.ui" line="26"/>
        <source>Branch marker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/mkb.ui" line="35"/>
        <source>dx (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/mkb.ui" line="71"/>
        <source>pk0 (km)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/overflow_link.ui" line="26"/>
        <source>River-2D link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/network_overflow_link.ui" line="48"/>
        <source>Z overflow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/hydraulic_cut_singularity.ui" line="26"/>
        <source>Hydraulic cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/gate.ui" line="52"/>
        <source>Discharge for headloss computation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/gate.ui" line="58"/>
        <source>Full section</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/gate.ui" line="65"/>
        <source>River bed only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pump_link.ui" line="793"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/param_headloss_singularity.ui" line="78"/>
        <source>Discharge (m3/s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tz_bc.ui" line="121"/>
        <source>Elevation (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/hydrology_bc.ui" line="26"/>
        <source>Hydrology BC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/weir_link.ui" line="26"/>
        <source>Weir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/gate.ui" line="118"/>
        <source>V max (cm/s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/weir_bc.ui" line="69"/>
        <source>Weir coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/weir_bc.ui" line="57"/>
        <source>Z weir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/mesh_2d_link.ui" line="75"/>
        <source>Z invert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/weir_bc.ui" line="100"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/param_headloss_singularity.ui" line="26"/>
        <source>Parametric headloss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/param_headloss_singularity.ui" line="83"/>
        <source>dH (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/strickler_bc.ui" line="26"/>
        <source>Strickler BC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/strickler_link.ui" line="71"/>
        <source>Strickler coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/strickler_bc.ui" line="58"/>
        <source>Slope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/street.ui" line="85"/>
        <source>Width (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/zregul_weir_singularity.ui" line="26"/>
        <source>Regulated weir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/zregul_weir_singularity.ui" line="77"/>
        <source>Weir type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/zregul_weir_singularity.ui" line="99"/>
        <source>Fixed Weir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/zregul_weir_singularity.ui" line="106"/>
        <source>Regulated Weir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/zregul_weir_singularity.ui" line="129"/>
        <source>Water reoxygenation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/zregul_weir_singularity.ui" line="147"/>
        <source>Reoxygenation law</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/zregul_weir_singularity.ui" line="173"/>
        <source>Temperature correction coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/zregul_weir_singularity.ui" line="186"/>
        <source>Reoxygenation coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/zregul_weir_singularity.ui" line="212"/>
        <source>Adjustment coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/zregul_weir_singularity.ui" line="228"/>
        <source>Waterfall high coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/zregul_weir_singularity.ui" line="241"/>
        <source>Water pollution coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/zregul_weir_singularity.ui" line="281"/>
        <source>Z regul</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="226"/>
        <source>Regulation mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/bridge_headloss_singularity.ui" line="26"/>
        <source>Bridge headloss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/bridge_headloss_singularity.ui" line="49"/>
        <source>Road length (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/bridge_headloss_singularity.ui" line="72"/>
        <source>Z road</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="26"/>
        <source>Borda headloss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="63"/>
        <source>Borda headloss type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="163"/>
        <source>Reference cross-section</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="193"/>
        <source>Curve radius (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="203"/>
        <source>Angle (°)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="213"/>
        <source>Half-angle (°)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="223"/>
        <source>Curve angle (°)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="233"/>
        <source>Vertical angle (°)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="173"/>
        <source>Middle option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_singularity.ui" line="159"/>
        <source>Middle cs area (m²)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_singularity.ui" line="169"/>
        <source>Transition option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="293"/>
        <source>Wall friction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="303"/>
        <source>Number of segment in bend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="317"/>
        <source>Segment length/diameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="327"/>
        <source>Deposition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="347"/>
        <source>Grid shape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="357"/>
        <source>Bend shape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_singularity.ui" line="253"/>
        <source>Blocage coefficient (1-a/s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="397"/>
        <source>KQ² coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="407"/>
        <source>KV coefficient (1 -&gt; 2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="417"/>
        <source>KV coefficient (1 &lt;- 2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="427"/>
        <source>param1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="437"/>
        <source>param2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="447"/>
        <source>param3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="457"/>
        <source>param4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="467"/>
        <source>H(Q)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="26"/>
        <source>Regulated gate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="64"/>
        <source>Z ceiling stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/bradley_headloss_singularity.ui" line="86"/>
        <source>Z ceiling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="90"/>
        <source>Z invert stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/gate.ui" line="132"/>
        <source>Action mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/gate.ui" line="138"/>
        <source>Upward opening</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/gate.ui" line="145"/>
        <source>Downward opening</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/gate.ui" line="167"/>
        <source>Gate coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/gate.ui" line="177"/>
        <source>Submerged coef.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="184"/>
        <source>Regulation time step (s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="234"/>
        <source>No regulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="241"/>
        <source>Elevation (Z)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="248"/>
        <source>Discharge (Q)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="255"/>
        <source>Relief</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="262"/>
        <source>Head</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="269"/>
        <source>Q(h)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="285"/>
        <source>Control node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="298"/>
        <source>Select node...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="312"/>
        <source>Z critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/gate.ui" line="42"/>
        <source>Z gate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="398"/>
        <source>Closing head (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="405"/>
        <source>Opening head  (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="439"/>
        <source>Regulation parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="448"/>
        <source>Proportional term (m²/s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="458"/>
        <source>Integral term (m²/s²)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/regul_gate.ui" line="471"/>
        <source>Derivative term (m²)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="26"/>
        <source>Catchment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="42"/>
        <source>Contour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="52"/>
        <source>Net flow production function</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="74"/>
        <source>Runoff coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="94"/>
        <source>Initial loss coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="110"/>
        <source>Refill coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="130"/>
        <source>Saturation infiltration rate (mm/h)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="143"/>
        <source>Dry infiltration rate (mm/h)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="156"/>
        <source>Soil storage capacity (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="176"/>
        <source>Soil storage J (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="189"/>
        <source>Surface storage RFUJ (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="196"/>
        <source>Soil drainage time (days)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="228"/>
        <source>Aquifer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="234"/>
        <source>Catchment connect coef. (0-1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="244"/>
        <source>Max infiltration rate (mm/day)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="264"/>
        <source>Surface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="273"/>
        <source>Storage RFU (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="286"/>
        <source>Maximum infiltration rate f0 (mm/h)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="296"/>
        <source>Soil</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="303"/>
        <source>rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="308"/>
        <source>split</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="316"/>
        <source>Storage J (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="323"/>
        <source>Drainage time QRES (day)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="333"/>
        <source>Infiltration type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="353"/>
        <source>Split coef. (0-1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="380"/>
        <source>Production reservoir K1 (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="390"/>
        <source>Aquifer exchange rate K2 (mm/day)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="400"/>
        <source>Routing reservoir K3 (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="410"/>
        <source>Runoff concentration time K4 (days)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="473"/>
        <source>Auto-fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tank_bc.ui" line="54"/>
        <source>Pollution parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="486"/>
        <source>Network type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="496"/>
        <source>Land use occupation (%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="506"/>
        <source>Rural</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="516"/>
        <source>Industrial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="526"/>
        <source>Suburban housing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="536"/>
        <source>Dense housing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="546"/>
        <source>Total %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="553"/>
        <source>#TOTAL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="579"/>
        <source>Runoff production function</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="598"/>
        <source>Shape coefficient beta (2-6)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="608"/>
        <source>Socose unit hydrograph (rural)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="618"/>
        <source>Linear reservoir (urban)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="648"/>
        <source>Lag time (mn)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="658"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="665"/>
        <source>Concentration time (mn)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="679"/>
        <source>Define K</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="684"/>
        <source>Desbordes 1 Cr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="689"/>
        <source>Desbordes 1 Cimp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="694"/>
        <source>Desbordes 2 Cimp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="699"/>
        <source>Krajewski</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="711"/>
        <source>Define Tc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="716"/>
        <source>Giandotti</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="721"/>
        <source>Passini</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="726"/>
        <source>Turraza</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="731"/>
        <source>Ventura</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="736"/>
        <source>Kirpich</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="741"/>
        <source>Cemagref</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="746"/>
        <source>Mockus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="767"/>
        <source>Area (ha)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/deriv_pump_link.ui" line="42"/>
        <source>Maximum discharge (m3/s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_node.ui" line="804"/>
        <source>Impervious soil coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/constant_inflow_bc.ui" line="65"/>
        <source>Constant inflow (m3/s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/mkd.ui" line="26"/>
        <source>2D domain marker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="257"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/storage.ui" line="99"/>
        <source>Coverage id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/mkd.ui" line="76"/>
        <source>2D domain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/mkc.ui" line="26"/>
        <source>Coverage marker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/mkc.ui" line="59"/>
        <source>Protected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/deriv_pump_link.ui" line="26"/>
        <source>Pump derivation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/jet_fan_link.ui" line="26"/>
        <source>Jet fan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/jet_fan_link.ui" line="35"/>
        <source>Unit thrust [N]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/jet_fan_link.ui" line="45"/>
        <source>Efficiency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/jet_fan_link.ui" line="52"/>
        <source>Number of units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/jet_fan_link.ui" line="65"/>
        <source>Jet fan ejection velocity (m/s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/jet_fan_link.ui" line="75"/>
        <source>Pipe section area (m²)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/ventilator_link.ui" line="42"/>
        <source>Direction is up to down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_contour.ui" line="26"/>
        <source>Catchment contour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/catchment_contour.ui" line="55"/>
        <source>Hyetogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/air_flow_bc_singularity.ui" line="26"/>
        <source>Q(z) BC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/connector_link.ui" line="26"/>
        <source>Connector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/connector_link.ui" line="32"/>
        <source>Main branch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/select_in_combo.ui" line="26"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/select_in_combo.ui" line="42"/>
        <source>label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/elem_2d.ui" line="26"/>
        <source>2D Element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/elem_2d.ui" line="48"/>
        <source>Domain 2D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/elem_2d.ui" line="102"/>
        <source>Z bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/station_node.ui" line="26"/>
        <source>Station node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/station.ui" line="26"/>
        <source>Station</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/storage.ui" line="26"/>
        <source>Storage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/storage.ui" line="35"/>
        <source>Filling curve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tank_bc.ui" line="181"/>
        <source>Z init</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/storage.ui" line="116"/>
        <source>Generate filling curve from terrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/storage.ui" line="126"/>
        <source>Porosity coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/gate.ui" line="26"/>
        <source>Gate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/gate.ui" line="35"/>
        <source>Mode valve type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/dry_inflow_sector.ui" line="26"/>
        <source>Dry inflow sector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/froude_bc.ui" line="26"/>
        <source>Froude BC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/air_pressure_bc_singularity.ui" line="49"/>
        <source>Relative pressure [Pa]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/street.ui" line="26"/>
        <source>Street</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/street.ui" line="39"/>
        <source>Width invert (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/street.ui" line="75"/>
        <source>Associated mesh element length (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tz_bc.ui" line="26"/>
        <source>Z(t) BC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tz_bc.ui" line="61"/>
        <source>Cyclic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tz_bc.ui" line="116"/>
        <source>Time (h)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/wind_gage.ui" line="26"/>
        <source>Anemometer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/rain_gage.ui" line="32"/>
        <source>espg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/rain_gage.ui" line="66"/>
        <source>Y coordinate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/rain_gage.ui" line="79"/>
        <source>X coordinate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="73"/>
        <source>Upstream geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="83"/>
        <source>Downstream geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="93"/>
        <source>Up: width (m) if canal / section area (m²) if circular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="103"/>
        <source>Down: width (m) if canal / section area (m²) if circular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="113"/>
        <source>Middle cross-section area (m²)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="123"/>
        <source>Up: Z invert (m) if canal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="133"/>
        <source>Down: Z invert (m) if canal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="143"/>
        <source>Outlet cross-section area (m²)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="153"/>
        <source>Inlet cross-section area (m²)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="243"/>
        <source>Entrance shape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="253"/>
        <source>Outlet Shape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="263"/>
        <source>Inlet shape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="273"/>
        <source>Diffuser geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="283"/>
        <source>Transition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="367"/>
        <source>Obstruction rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/borda_headloss_link.ui" line="377"/>
        <source>Blocage coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/manhole_node.ui" line="26"/>
        <source>Manhole</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/manhole_node.ui" line="102"/>
        <source>Cover diameter (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/manhole_node.ui" line="109"/>
        <source>Cover critical pressure (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/manhole_node.ui" line="132"/>
        <source>Inlet Width (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/manhole_node.ui" line="142"/>
        <source>Inlet Height (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/constant_inflow_bc.ui" line="26"/>
        <source>Constant inflow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_connect_bc.ui" line="26"/>
        <source>Model connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/model_connect_bc.ui" line="74"/>
        <source>Cascade mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/bradley_headloss_singularity.ui" line="26"/>
        <source>Bradley headloss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/bradley_headloss_singularity.ui" line="49"/>
        <source>Left abutment width (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/bradley_headloss_singularity.ui" line="56"/>
        <source>Right abutment width (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/bradley_headloss_singularity.ui" line="79"/>
        <source>Abutment type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="38"/>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/strickler_link.ui" line="135"/>
        <source>Z crest down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/strickler_link.ui" line="168"/>
        <source>Width down (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/strickler_link.ui" line="158"/>
        <source>Width up (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/strickler_link.ui" line="125"/>
        <source>Z crest up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/mesh_2d_link.ui" line="35"/>
        <source>Lateral contraction coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/overflow_link.ui" line="159"/>
        <source>Break mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/overflow_link.ui" line="172"/>
        <source>Break parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/overflow_link.ui" line="181"/>
        <source>Breach width (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/overflow_link.ui" line="228"/>
        <source>Z break</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/overflow_link.ui" line="242"/>
        <source>Group number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/overflow_link.ui" line="249"/>
        <source>T break (h)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/qq_split_hydrology_singularity.ui" line="26"/>
        <source>Discharge split singularity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/reservoir_rs_hydrology_singularity.ui" line="241"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/qq_split_hydrology_singularity.ui" line="49"/>
        <source>Split 2 link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/qq_split_hydrology_singularity.ui" line="32"/>
        <source>  Split 1 link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/qq_split_hydrology_singularity.ui" line="42"/>
        <source>Downstream link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/zq_split_hydrology_singularity.ui" line="109"/>
        <source>Downstream law</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/zq_split_hydrology_singularity.ui" line="119"/>
        <source>Split 1 law</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/zq_split_hydrology_singularity.ui" line="129"/>
        <source>Split 2 law</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/connector_hydrology_link.ui" line="26"/>
        <source>Hydrology connector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/river_reach.ui" line="26"/>
        <source>Reach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/river_reach.ui" line="39"/>
        <source>pk0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/river_reach.ui" line="46"/>
        <source>dx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/crossroad_node.ui" line="26"/>
        <source>Crossroad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/crossroad_node.ui" line="52"/>
        <source>h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/mkp.ui" line="26"/>
        <source>Point marker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/porous_link.ui" line="26"/>
        <source>Porous link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/porous_link.ui" line="87"/>
        <source>Transmitivity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/rain_gage.ui" line="26"/>
        <source>Rain gage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/reservoir_rsp_hydrology_singularity.ui" line="26"/>
        <source>Reservoir RSP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/reservoir_rs_hydrology_singularity.ui" line="59"/>
        <source>Overflow link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/reservoir_rs_hydrology_singularity.ui" line="218"/>
        <source>Z initial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tank_bc.ui" line="70"/>
        <source>MES concentration (mg/L)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tank_bc.ui" line="80"/>
        <source>NTK concentration (mg/L)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tank_bc.ui" line="93"/>
        <source>DBO5 concentration (mg/L)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tank_bc.ui" line="100"/>
        <source>DCO concentration (mg/L)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tank_bc.ui" line="120"/>
        <source>NTK efficiency factor (%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tank_bc.ui" line="130"/>
        <source>DBO5 efficiency factor (%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tank_bc.ui" line="140"/>
        <source>DCO efficiency factor (%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tank_bc.ui" line="150"/>
        <source>MES efficiency factor (%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/reservoir_rs_hydrology_singularity.ui" line="248"/>
        <source>Drainage link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/mesh_2d_link.ui" line="26"/>
        <source>Meshing link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/strickler_link.ui" line="26"/>
        <source>Strickler link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/strickler_link.ui" line="37"/>
        <source>(distance between the nodes)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/strickler_link.ui" line="54"/>
        <source>Link length (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/weir_bc.ui" line="26"/>
        <source>Weir BC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/air_headloss_link.ui" line="26"/>
        <source>Headloss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/air_headloss_link.ui" line="32"/>
        <source>End to start headloss coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/air_duct_link.ui" line="78"/>
        <source>Area [m2]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/air_headloss_link.ui" line="56"/>
        <source>Start to end headloss coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/air_flow_bc_singularity.ui" line="49"/>
        <source>Is inwards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/air_flow_bc_singularity.ui" line="69"/>
        <source>Air flow [m3/s]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/ventilator_link.ui" line="26"/>
        <source>Ventilator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pump_link.ui" line="26"/>
        <source>Pump</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pump_link.ui" line="47"/>
        <source>Number of pumps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pump_link.ui" line="73"/>
        <source>Pump 1:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pump_link.ui" line="184"/>
        <source>Pump 2:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pump_link.ui" line="253"/>
        <source>Pump 3:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pump_link.ui" line="322"/>
        <source>Pump 4:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pump_link.ui" line="391"/>
        <source>Pump 5:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pump_link.ui" line="460"/>
        <source>Pump 6:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pump_link.ui" line="529"/>
        <source>Pump 7:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pump_link.ui" line="598"/>
        <source>Pump 8:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pump_link.ui" line="677"/>
        <source>Pump 9:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pump_link.ui" line="746"/>
        <source>Pump 10:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pump_link.ui" line="799"/>
        <source>Z start (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pump_link.ui" line="804"/>
        <source>Z stop (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/reservoir_rs_hydrology_singularity.ui" line="26"/>
        <source>Reservoir RS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/reservoir_rs_hydrology_singularity.ui" line="52"/>
        <source>Drainage flow rate (m3/s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/air_duct_link.ui" line="26"/>
        <source>Air duct</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/air_duct_link.ui" line="35"/>
        <source>Friction coefficient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/air_duct_link.ui" line="42"/>
        <source>End invert altitude [m]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/air_duct_link.ui" line="49"/>
        <source>Start invert altitude [m]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/air_duct_link.ui" line="92"/>
        <source>Perimeter [m2]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/air_duct_link.ui" line="102"/>
        <source>Custom length [m]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="32"/>
        <source>Pipe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="44"/>
        <source>Upstream elevation (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="54"/>
        <source>Silting height (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="64"/>
        <source>Downstream elevation (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="99"/>
        <source>Exclude from branches</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="106"/>
        <source>Slope (m/m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="247"/>
        <source>Geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="141"/>
        <source>Cross section type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="151"/>
        <source>Rk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="161"/>
        <source>Diameter (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="171"/>
        <source>Top diameter (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="181"/>
        <source>Invert diameter (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="191"/>
        <source>Height (m)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="204"/>
        <source>Flood plain Rk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="215"/>
        <source>Edit selected geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/pipe_link.ui" line="222"/>
        <source>Create new geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/manhole_hydrology_node.ui" line="26"/>
        <source>Node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/tank_bc.ui" line="26"/>
        <source>Tank BC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/river_cross_section_profile.ui" line="26"/>
        <source>Cross section</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/river_cross_section_profile.ui" line="78"/>
        <source>Upstream</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/river_cross_section_profile.ui" line="94"/>
        <source>Downstream</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/river_cross_section_profile.ui" line="119"/>
        <source>&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/river_cross_section_profile.ui" line="132"/>
        <source>&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/forms/river_cross_section_profile.ui" line="177"/>
        <source>Cancel and zoom to feature</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModelMenu</name>
    <message>
        <location filename="../gui/project_tree.py" line="44"/>
        <source>Set current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="45"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MoveOutletsToStreams</name>
    <message>
        <location filename="../processing/taudem/moveoutletstostreams.py" line="53"/>
        <source>Move outlets to Streams</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/moveoutletstostreams.py" line="56"/>
        <source>Stream network analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/moveoutletstostreams.py" line="62"/>
        <source>dem,hydrology,stream,outlet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/moveoutletstostreams.py" line="65"/>
        <source>Moves outlet points that are not aligned with a stream cell from a stream raster grid, downslope along the D8 flow direction until a stream raster cell is encountered.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/moveoutletstostreams.py" line="77"/>
        <source>D8 flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/moveoutletstostreams.py" line="79"/>
        <source>Stream raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/moveoutletstostreams.py" line="81"/>
        <source>Outlets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/moveoutletstostreams.py" line="84"/>
        <source>Maximum number of grid cells to traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/moveoutletstostreams.py" line="90"/>
        <source>Moved outlets</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PeukerDouglas</name>
    <message>
        <location filename="../processing/taudem/peukerdouglas.py" line="50"/>
        <source>Peuker Douglas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglas.py" line="53"/>
        <source>Stream network analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglas.py" line="59"/>
        <source>dem,hydrology,smooth,peuker,douglas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglas.py" line="62"/>
        <source>Creates an indicator grid (1, 0) of valley form grid cells according to the Peuker and Douglas algorithm.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglas.py" line="72"/>
        <source>Pit filled elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglas.py" line="74"/>
        <source>Center smoothing weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglas.py" line="79"/>
        <source>Side smoothing weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglas.py" line="84"/>
        <source>Diagonal smoothing weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglas.py" line="90"/>
        <source>Stream source</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PeukerDouglasStreamDef</name>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="71"/>
        <source>Peuker Douglas stream definition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="74"/>
        <source>Stream network analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="80"/>
        <source>dem,hydrology,stream,indicator,d8,threshold,drop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="83"/>
        <source>Generates a stream indicator grid (1,0) where the streams are located using a DEM curvature-based method.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="93"/>
        <source>Logarithmic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="94"/>
        <source>Arithmetic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="97"/>
        <source>Pit filled elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="99"/>
        <source>D8 flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="101"/>
        <source>Outlets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="106"/>
        <source>Mask grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="110"/>
        <source>D8 contributing area for drop analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="114"/>
        <source>Center smoothing weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="119"/>
        <source>Side smoothing weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="124"/>
        <source>Diagonal smoothing weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="129"/>
        <source>Accumulation threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="133"/>
        <source>Minimum threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="137"/>
        <source>Maximum threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="141"/>
        <source>Number of drop thresholds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="145"/>
        <source>Type of threshold step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="149"/>
        <source>Check for edge contamination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="152"/>
        <source>Select threshold by drop analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="156"/>
        <source>Stream source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="158"/>
        <source>Accumulated stream source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="160"/>
        <source>Stream raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="162"/>
        <source>Drop analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="162"/>
        <source>Text files (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/peukerdouglasstreamdef.py" line="181"/>
        <source>Specify drop analysis output file or uncheck &quot;Select threshold by drop analysis&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PitRemove</name>
    <message>
        <location filename="../processing/taudem/pitremove.py" line="49"/>
        <source>Pit remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/pitremove.py" line="52"/>
        <source>Basic grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/pitremove.py" line="58"/>
        <source>dem,hydrology,pit,remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/pitremove.py" line="61"/>
        <source>Identifies all pits in the DEM and raises their elevation to the level of the lowest pour point around their edge.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/pitremove.py" line="72"/>
        <source>Elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/pitremove.py" line="74"/>
        <source>Depression mask </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/pitremove.py" line="77"/>
        <source>Consider only 4 way neighbors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/pitremove.py" line="81"/>
        <source>Pit removed elevation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProjectManagerForm</name>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="26"/>
        <source>Project manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="32"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="74"/>
        <source>Project name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="79"/>
        <source>SRID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="84"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="89"/>
        <source>Creation date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="94"/>
        <source>Workspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="99"/>
        <source>Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="113"/>
        <source>%VERSION%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="120"/>
        <source>Current version :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="129"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="136"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="143"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="150"/>
        <source>Duplicate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="157"/>
        <source>Export as file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="164"/>
        <source>Import from file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="171"/>
        <source>Update project...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="178"/>
        <source>Update all projects...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="198"/>
        <source>Export with data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_manager_dialog.ui" line="205"/>
        <source>Import with data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RainMenu</name>
    <message>
        <location filename="../gui/project_tree.py" line="114"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="115"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RenderingMesh</name>
    <message>
        <location filename="../processing/rendering_mesh.py" line="45"/>
        <source>selafin Rendering mesh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/rendering_mesh.py" line="48"/>
        <source>River and free surface flow mapping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/rendering_mesh.py" line="54"/>
        <source>Creates model rendering mesh (triangulation) for post-processing.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScenarioMenu</name>
    <message>
        <location filename="../gui/project_tree.py" line="58"/>
        <source>Set current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="59"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="60"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="61"/>
        <source>Duplicate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SerieMenu</name>
    <message>
        <location filename="../gui/project_tree.py" line="92"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/project_tree.py" line="93"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Simulate</name>
    <message>
        <location filename="../processing/simulate.py" line="87"/>
        <source>Simulate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/simulate.py" line="96"/>
        <source>Run hydra simulation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/simulate.py" line="170"/>
        <source>invalidities remain in </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SlfToTif</name>
    <message>
        <location filename="../processing/slf_to_tif.py" line="79"/>
        <source>selafin Rasterize mesh results at dem resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/slf_to_tif.py" line="82"/>
        <source>River and free surface flow mapping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/slf_to_tif.py" line="88"/>
        <source>Converts selafin result file to tif raster at dem resolution.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SlopeArea</name>
    <message>
        <location filename="../processing/taudem/slopearea.py" line="51"/>
        <source>Slope area combination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopearea.py" line="54"/>
        <source>Stream network analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopearea.py" line="60"/>
        <source>dem,hydrology,dem,threshold,compare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopearea.py" line="63"/>
        <source>Creates a grid of slope-area values = (S^m)·(A^n) based on slope and specific catchment area grid inputs, and parameters m and n.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopearea.py" line="74"/>
        <source>Slope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopearea.py" line="76"/>
        <source>Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopearea.py" line="78"/>
        <source>Slope exponent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopearea.py" line="82"/>
        <source>Area exponent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopearea.py" line="87"/>
        <source>Slope area</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SlopeAreaRatio</name>
    <message>
        <location filename="../processing/taudem/slopearearatio.py" line="47"/>
        <source>Slope over area ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopearearatio.py" line="50"/>
        <source>Specialized grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopearearatio.py" line="56"/>
        <source>dem,hydrology,slope,catchment,area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopearearatio.py" line="59"/>
        <source>Calculates the ratio of the slope to the specific catchment (contributing) area.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopearearatio.py" line="69"/>
        <source>Slope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopearearatio.py" line="71"/>
        <source>Specific catchment area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopearearatio.py" line="74"/>
        <source>Slope area ratio</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SlopeAreaStreamDef</name>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="71"/>
        <source>Slope area stream definition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="74"/>
        <source>Stream network analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="80"/>
        <source>dem,hydrology,stream,indicator,slope,area,upslope,threshold,drop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="83"/>
        <source>Generates a stream indicator grid (1,0) using the area and slope threshold method suggested by Montgomery and Dietrich (1992) to determine the location of the streams.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="95"/>
        <source>Logarithmic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="96"/>
        <source>Arithmetic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="99"/>
        <source>D8 flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="101"/>
        <source>D-infinity Contributing Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="103"/>
        <source>Slope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="105"/>
        <source>Mask grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="109"/>
        <source>Outlets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="114"/>
        <source>Pit-filled grid for drop analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="118"/>
        <source>D8 contributing area for drop analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="122"/>
        <source>Slope exponent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="126"/>
        <source>Area exponent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="130"/>
        <source>Accumulation threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="134"/>
        <source>Minimum threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="138"/>
        <source>Maximum threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="142"/>
        <source>Number of drop thresholds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="146"/>
        <source>Type of threshold step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="150"/>
        <source>Check for edge contamination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="153"/>
        <source>Select threshold by drop analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="157"/>
        <source>Stream raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="159"/>
        <source>Slope area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="161"/>
        <source>Maximum upslope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="163"/>
        <source>Drop analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="163"/>
        <source>Text files (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeareastreamdef.py" line="186"/>
        <source>Specify drop analysis output file or uncheck &quot;Select threshold by drop analysis&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SlopeAveDown</name>
    <message>
        <location filename="../processing/taudem/slopeavedown.py" line="81"/>
        <source>Slope average down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeavedown.py" line="52"/>
        <source>Specialized grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeavedown.py" line="58"/>
        <source>dem,hydrology,d8,slope,downslope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeavedown.py" line="61"/>
        <source>Computes slope in a D8 downslope direction averaged over a user selected distance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeavedown.py" line="71"/>
        <source>Pit filled elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeavedown.py" line="73"/>
        <source>D8 flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/slopeavedown.py" line="75"/>
        <source>Downslope distance</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StreamDefDropAnalysis</name>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="63"/>
        <source>Stream definition with drop analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="66"/>
        <source>Stream network analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="72"/>
        <source>dem,hydrology,stream,drop,threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="75"/>
        <source>Generates a stream indicator grid (1,0) using optimal threshold as determined from the stream drop statistics.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="86"/>
        <source>Logarithmic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="87"/>
        <source>Arithmetic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="90"/>
        <source>Pit filled elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="92"/>
        <source>D8 flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="94"/>
        <source>D8 contributing area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="96"/>
        <source>Accumulated stream source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="98"/>
        <source>Outlets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="101"/>
        <source>Mask grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="105"/>
        <source>Minimum threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="109"/>
        <source>Maximum threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="113"/>
        <source>Number of drop thresholds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="117"/>
        <source>Type of threshold step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="122"/>
        <source>Stream raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="124"/>
        <source>Drop analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamdefdropanalysis.py" line="124"/>
        <source>Text files (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StreamNet</name>
    <message>
        <location filename="../processing/taudem/streamnet.py" line="61"/>
        <source>Stream reach and watershed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamnet.py" line="64"/>
        <source>Stream network analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamnet.py" line="70"/>
        <source>dem,hydrology,dem,stream,network,watershed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamnet.py" line="73"/>
        <source>Produces a vector network from the Stream Raster grid by tracing down from each source grid cell.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamnet.py" line="83"/>
        <source>Pit filled elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamnet.py" line="85"/>
        <source>D8 flow directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamnet.py" line="87"/>
        <source>D8 contributing area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamnet.py" line="89"/>
        <source>Stream raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamnet.py" line="91"/>
        <source>Outlets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamnet.py" line="95"/>
        <source>Delineate single watershed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamnet.py" line="99"/>
        <source>Stream order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamnet.py" line="101"/>
        <source>Watershed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamnet.py" line="103"/>
        <source>Channel network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamnet.py" line="106"/>
        <source>Network connectivity tree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamnet.py" line="109"/>
        <source>Data files (*.dat)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/streamnet.py" line="109"/>
        <source>Network coordinates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TauDemProvider</name>
    <message>
        <location filename="../processing/taudem/taudemProvider.py" line="92"/>
        <source>Activate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/taudemProvider.py" line="95"/>
        <source>TauDEM directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/taudemProvider.py" line="104"/>
        <source>MPICH2/OpenMPI bin directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/taudemProvider.py" line="113"/>
        <source>MPI processes to use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/taudemProvider.py" line="122"/>
        <source>Log commands output</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Threshold</name>
    <message>
        <location filename="../processing/taudem/threshold.py" line="49"/>
        <source>Stream definition by threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/threshold.py" line="52"/>
        <source>Stream network analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/threshold.py" line="58"/>
        <source>dem,hydrology,dem,threshold,compare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/threshold.py" line="61"/>
        <source>Operates on any grid and outputs an indicator (1, 0) grid identifing cells with input values &gt;= the threshold value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/threshold.py" line="72"/>
        <source>Accumulated stream source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/threshold.py" line="74"/>
        <source>Mask grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/threshold.py" line="77"/>
        <source>Threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/threshold.py" line="82"/>
        <source>Stream raster</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Twi</name>
    <message>
        <location filename="../processing/taudem/twi.py" line="47"/>
        <source>Topographic wetness index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/twi.py" line="50"/>
        <source>Specialized grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/twi.py" line="56"/>
        <source>dem,hydrology,twi,index,topographic,wetness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/twi.py" line="59"/>
        <source>Calculates the ratio of the natural log of the specific catchment area (contributing area) to slope, ln(a/S), or ln(a/tan (beta)).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/twi.py" line="70"/>
        <source>Slope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/twi.py" line="72"/>
        <source>Specific catchment area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/twi.py" line="75"/>
        <source>Wetness index</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VelocityEnvelopeRaster</name>
    <message>
        <location filename="../processing/envelope.py" line="163"/>
        <source>Maximum velocity of a group of scenarios</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/envelope.py" line="160"/>
        <source>crgeng Velocity raster envelope</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VelocityRaster</name>
    <message>
        <location filename="../processing/velocity.py" line="76"/>
        <source>Velocity Raster Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/velocity.py" line="84"/>
        <source>Terrain Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/velocity.py" line="67"/>
        <source>crgeng Velocity raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/velocity.py" line="70"/>
        <source>River and free surface flow mapping</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>W15ToSlf</name>
    <message>
        <location filename="../processing/w15_to_slf.py" line="48"/>
        <source>selafin Results to mesh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/w15_to_slf.py" line="51"/>
        <source>River and free surface flow mapping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/w15_to_slf.py" line="57"/>
        <source>Creates selefin result file from hydra results.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>catchhydrogeo</name>
    <message>
        <location filename="../processing/taudem/catchhydrogeo.py" line="53"/>
        <source>Catchment hydraulic properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/catchhydrogeo.py" line="56"/>
        <source>Basic grid analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/catchhydrogeo.py" line="62"/>
        <source>dem,hydrology,d-infinity,catchment area,HAND,properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/catchhydrogeo.py" line="65"/>
        <source>Calculates channel hydraulic properties based on HAND (Height Above Nearest Drainage).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/catchhydrogeo.py" line="75"/>
        <source>HAND raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/catchhydrogeo.py" line="77"/>
        <source>Catchment area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/catchhydrogeo.py" line="79"/>
        <source>D-infinity slope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/catchhydrogeo.py" line="90"/>
        <source>Hydraulic property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/taudem/catchhydrogeo.py" line="90"/>
        <source>Text files (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>longprofil_dock</name>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="14"/>
        <source>Longitudinal profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="76"/>
        <source>Show min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="93"/>
        <source>Show max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="125"/>
        <source>Copy data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="139"/>
        <source>Copy image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="153"/>
        <source>Export settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="167"/>
        <source>Import settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="187"/>
        <source>Display graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="204"/>
        <source>Geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="210"/>
        <source>Show right bank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="213"/>
        <source>Reach right bank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="220"/>
        <source>Show links</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="223"/>
        <source>Links</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="230"/>
        <source>Show left bank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="233"/>
        <source>Reach left Bank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="240"/>
        <source>Show singularities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="243"/>
        <source>Singularities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="250"/>
        <source>Show cross sections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="253"/>
        <source>Cross sections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="269"/>
        <source>Results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="305"/>
        <source>Show water min max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="308"/>
        <source>Water min max Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="315"/>
        <source>Show water speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="318"/>
        <source>Water speed V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="325"/>
        <source>Show water level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="328"/>
        <source>Water level Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="335"/>
        <source>Show water flow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="338"/>
        <source>Water flow Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="366"/>
        <source>Show quality parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="369"/>
        <source>Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="394"/>
        <source>Show solid flow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="397"/>
        <source>Solid flow qs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="404"/>
        <source>Show critical Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="407"/>
        <source>Critical Zc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="414"/>
        <source>Show energy Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="417"/>
        <source>Energy E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="424"/>
        <source>Show silting depth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/long_profil_dock.ui" line="427"/>
        <source>Silting hight Hs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>mainform</name>
    <message>
        <location filename="../gui/scenario_manager.ui" line="14"/>
        <source>Scenario manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.ui" line="39"/>
        <source>List of scenarios</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.ui" line="45"/>
        <source>Clone scenario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.ui" line="68"/>
        <source>Scenario settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_manager.ui" line="241"/>
        <source>Computation settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_manager.ui" line="255"/>
        <source>Hydrology settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_manager.ui" line="269"/>
        <source>Model ordering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_manager.ui" line="283"/>
        <source>Regulation and configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.ui" line="139"/>
        <source>Computation options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_manager.ui" line="307"/>
        <source>[PREMIUM] Transport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.ui" line="167"/>
        <source>[PREMIUM] Aeraulics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.ui" line="186"/>
        <source>Scn1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.ui" line="207"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.ui" line="224"/>
        <source>Import from file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_manager.ui" line="14"/>
        <source>Serie manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_manager.ui" line="22"/>
        <source>Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_manager.ui" line="60"/>
        <source>Time serie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_manager.ui" line="68"/>
        <source>Add serie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_manager.ui" line="75"/>
        <source>Add bloc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_manager.ui" line="88"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_manager.ui" line="138"/>
        <source>#Active_scn_label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_manager.ui" line="189"/>
        <source>Copy data for Extract &quot;$SET&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_manager.ui" line="209"/>
        <source>Serie settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_manager.ui" line="221"/>
        <source>Serie1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_manager.ui" line="293"/>
        <source>[PREMIUM] Computation options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model_dock</name>
    <message>
        <location filename="../gui/model_dock.ui" line="14"/>
        <source>Model dock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="42"/>
        <source>Station</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="54"/>
        <source>Station node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="80"/>
        <source>Station contour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="109"/>
        <source>Links</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1014"/>
        <source>Weir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="147"/>
        <source>Strickler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="173"/>
        <source>Pump</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="199"/>
        <source>Reverse link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="225"/>
        <source>Air duct</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1040"/>
        <source>Borda headloss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="988"/>
        <source>Gate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1118"/>
        <source>Regulated gate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="323"/>
        <source>Pump derivation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="349"/>
        <source>Ventilator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1488"/>
        <source>Connector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="395"/>
        <source>Overflow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="421"/>
        <source>Porous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="447"/>
        <source>Network overflow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="473"/>
        <source>Air headloss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="493"/>
        <source>Jet fan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="516"/>
        <source>Boundary conditions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="528"/>
        <source>Constant inflow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="554"/>
        <source>TZ BC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="580"/>
        <source>Tank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="606"/>
        <source>ZQ BC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="632"/>
        <source>Froude BC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="658"/>
        <source>RACC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="684"/>
        <source>Weir BC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="710"/>
        <source>Strikler BC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="736"/>
        <source>Hydrograph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="805"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="817"/>
        <source>Manhole</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="843"/>
        <source>Pipe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="869"/>
        <source>Branch marker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="895"/>
        <source>Generate network overflow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="921"/>
        <source>Insert manhole on pipe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="947"/>
        <source>Delete manhole</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="976"/>
        <source>Structures and headlosses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1066"/>
        <source>Bradley</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1092"/>
        <source>Hydraulic cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1144"/>
        <source>Parametric headloss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1170"/>
        <source>Point marker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1196"/>
        <source>Bridge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1225"/>
        <source>Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1243"/>
        <source>Dry inflow sector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1269"/>
        <source>Rain gage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1295"/>
        <source>Wind gage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1317"/>
        <source>Terrain points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1346"/>
        <source>Hydrology</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1358"/>
        <source>QQ split</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1384"/>
        <source>Hydrology node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1410"/>
        <source>Reservoir RS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1436"/>
        <source>Catchment contour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1462"/>
        <source>ZQ split</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1514"/>
        <source>Reservoir RSP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1540"/>
        <source>Hydrology BC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1566"/>
        <source>Routing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1592"/>
        <source>Catchment node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1618"/>
        <source>Convert hydrology</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1647"/>
        <source>River and free surface flow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1659"/>
        <source>Merge reachs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1685"/>
        <source>River node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1711"/>
        <source>Create links</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1737"/>
        <source>Constrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1763"/>
        <source>Mesh unmeshed coverages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1789"/>
        <source>Split constrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1815"/>
        <source>Crossroad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1841"/>
        <source>Re-mesh everything</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1867"/>
        <source>Manage 2D domains</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1893"/>
        <source>Street</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1919"/>
        <source>Reach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1945"/>
        <source>Split reach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1971"/>
        <source>Delete mesh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="1997"/>
        <source>Storage coverage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="2023"/>
        <source>Null coverage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="2049"/>
        <source>Cross section</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="2075"/>
        <source>Mesh coverage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="2101"/>
        <source>2D domain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="2127"/>
        <source>Generate interlinks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="2156"/>
        <source>Triggers handling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="2168"/>
        <source>Regenerate branchs from pipes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="2171"/>
        <source>Update branchs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="2184"/>
        <source>Activate auto coverage regeneration from constrains</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="2219"/>
        <source>Auto regeneration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="2200"/>
        <source>Regenerates coverages from constrain lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="2203"/>
        <source>Update coverages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/model_dock.ui" line="2216"/>
        <source>Activate auto branchs regeneration from pipes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>new_project_dialog</name>
    <message>
        <location filename="../gui/new_model_dialog.ui" line="14"/>
        <source>New model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/new_model_dialog.ui" line="20"/>
        <source>Model name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/name_srid_dialog.ui" line="14"/>
        <source>New project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/new_project_dialog.ui" line="20"/>
        <source>Project name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/name_srid_dialog.ui" line="27"/>
        <source>EPSG:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/name_srid_dialog.ui" line="53"/>
        <source>2154</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/name_srid_dialog.ui" line="37"/>
        <source>Select EPSG...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/new_project_dialog.ui" line="57"/>
        <source>Working directory :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/new_project_dialog.ui" line="70"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/name_srid_dialog.ui" line="20"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>scenario_grouping</name>
    <message>
        <location filename="../gui/scenario_grouping.ui" line="14"/>
        <source>Scenario grouping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_group_run.ui" line="26"/>
        <source>Group name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_group_run.ui" line="33"/>
        <source>Run all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_group_run.ui" line="40"/>
        <source>Calculation of the group:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_group_run.ui" line="47"/>
        <source>Run not calculated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_group_run.ui" line="95"/>
        <source>Scenario name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_group_run.ui" line="100"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_group_run.ui" line="105"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_group_run.ui" line="115"/>
        <source>scn_1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_group_run.ui" line="120"/>
        <source>calculated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_group_run.ui" line="125"/>
        <source>30/03/2017</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_grouping.ui" line="113"/>
        <source>Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_grouping.ui" line="139"/>
        <source>Groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_grouping.ui" line="39"/>
        <source>&lt;---</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_grouping.ui" line="59"/>
        <source>---&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_grouping.ui" line="66"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_grouping.ui" line="73"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/scenario_grouping.ui" line="126"/>
        <source>Scenarios</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>section_exporter</name>
    <message>
        <location filename="../gui/section_export.ui" line="14"/>
        <source>Section Exporter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_export.ui" line="26"/>
        <source>Select scenarios :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_export.ui" line="39"/>
        <source>Select reachs :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_export.ui" line="62"/>
        <source>Create PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_export.ui" line="69"/>
        <source>Create images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_export.ui" line="76"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_export.ui" line="85"/>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_export.ui" line="97"/>
        <source>Terrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_export.ui" line="113"/>
        <source>Hydra section</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_export.ui" line="129"/>
        <source>Combined topo (Terrain and points)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_export.ui" line="145"/>
        <source>Z points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/section_export.ui" line="170"/>
        <source>graphs per page :</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>serie_run</name>
    <message>
        <location filename="../gui/serie_run.ui" line="14"/>
        <source>Time serie runner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_run.ui" line="20"/>
        <source>Run time serie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_run.ui" line="60"/>
        <source>Time serie scenario</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_run.ui" line="65"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_run.ui" line="70"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_run.ui" line="75"/>
        <source>Relaunch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_run.ui" line="80"/>
        <source>scn_1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_run.ui" line="85"/>
        <source>calculated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_run.ui" line="90"/>
        <source>30/03/2017</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gui/serie_run.ui" line="98"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
