# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..base_dialog import BaseDialog, tr
import hydra.utility.string as string
from qgis.PyQt import uic
import os

class Elem2DEditor(BaseDialog):
    def __init__(self, project, contour, id=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "elem_2d.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.__model = self.project.get_current_model()

        assert not self.__model == None
        self.__contour = contour
        if id==None:
            self.__id = self.__model.add_elem_2d_node(self.__contour)
        else:
            self.__id = id
            if self.__contour is None:
                contour,= self.project.execute("""
                select contour
                from {}.elem_2d_node
                where id={}""".format(self.__model.name, self.__id)).fetchone()

        name, area, zb, rk, domain_2d, comment = self.project.execute("""
            select name, area, zb, rk, domain_2d, comment
            from {}.elem_2d_node
            where id={}""".format(self.__model.name, self.__id)).fetchone()

        self.name.setText(name)
        self.area.setText(string.get_str(area))
        self.zb.setText(string.get_str(zb))
        self.rk.setText(string.get_str(rk))
        self.notes.setText(comment)

        if domain_2d is not None:
            domain_name, = self.project.execute("""
                select name from {}.domain_2d where id={}""".format(self.__model.name, domain_2d)).fetchone()
            self.domain_2d.setText(domain_name)

    def get_id(self):
        return self.__id

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.__model.update_elem_2d_node(self.__id, self.__contour,
                                string.get_sql_float(self.zb.text()), string.get_sql_float(self.rk.text()),
                                None, string.get_sql_float(self.area.text()),
                                self.name.text(), comment=self.notes.text())
        BaseDialog.save(self)
        self.close()
