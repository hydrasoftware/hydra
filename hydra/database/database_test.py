# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run database tests

USAGE

   database_test.py [-dh]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -k, --keep
        keep database after test


"""

import os
import sys
import tempfile
import getopt
import shlex
import psycopg2
from subprocess import Popen, PIPE
from hydra.project import Project
from hydra.database.database import TestProject, project_exists, remove_project
from hydra.utility.settings_properties import SettingsProperties
from hydra.utility.string import read_file
from qgis.PyQt.QtCore import QCoreApplication

hydra_dir = os.path.join(os.path.expanduser("~"), ".hydra")

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdk",
            ["help", "debug", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

# test model creation

debug = "-d" in optlist or "--debug" in optlist
keep = "-k" in optlist or "--keep" in optlist

dbname = "database_test"

if project_exists(dbname):
    remove_project(dbname)

tes_project = TestProject(dbname, keep, debug, with_model=True)
obj_project = Project.load_project(dbname)

obj_project.add_new_model('modele1')

obj_project.execute("drop schema if exists brut cascade")
obj_project.execute("create schema brut")
obj_project.commit()

obj_project.log.notice("load test shapefiles")

current_dir = os.path.abspath(os.path.dirname(__file__))

test_data = os.path.join(current_dir, "test_data", "sketchy_model")

for table in [f[:-4] for f in os.listdir(test_data) if f.endswith(".shp")]:
    cmd = ['ogr2ogr', 
        f'PG:service={SettingsProperties.get_service()} dbname={dbname}',
        '-lco', 'GEOMETRY_NAME=geom', 
        '-a_srs', f'EPSG:2154', '-nln', "brut."+table, os.path.join(test_data, table+'.shp')] 
    env = dict(os.environ)
    env['PGCLIENTENCODING'] = 'latin1'
    p = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, env=env)
    out, err = p.communicate()
    if err :
        obj_project.log.error(" ".join(cmd)+'\n')
        obj_project.log.error(err)
        exit(1)

obj_project.log.notice("run database_test.sql and compare output")

ref_output = os.path.join(test_data, "database_test.sql.ref")
test_output = os.path.join(tempfile.gettempdir(), "database_test.sql.out")
cmd = ["psql", "-tXA", '-f', os.path.join(test_data, "database_test.sql"), f"service={SettingsProperties.get_service()} dbname={dbname}"]
with open(test_output, "w") as o:
    out, err = Popen(cmd, stdout=o, stderr=PIPE).communicate()
if err :
    obj_project.log.error(err)
    obj_project.log.error("error while running SQL command from {}\n".format(os.path.join(current_dir, "database_test.sql")))
    exit(1)

if read_file(test_output).replace('\n','').replace('\nt','') != read_file(ref_output).replace('\n','').replace('\nt',''):
    obj_project.log.error("diff found between {} and {}\n".format(ref_output, test_output))
    exit(1)

os.remove(test_output)

sys.stdout.write("ok\n")
