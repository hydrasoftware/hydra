# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.dry_inflow_sector_settings_guitest [-dhs] [-g project_name]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name
        run in gui mode

   -s  --solo
        run test in solo mode (create database)
"""

import sys
import getopt
from hydra.utility.string import normalized_name
from hydra.project import Project
from qgis.PyQt.QtWidgets import QApplication, QTableWidgetItem
from qgis.PyQt.QtCore import QCoreApplication, QTranslator
from hydra.gui.dry_inflow_sector_settings import DryInflowSectorSettings
from ..database.database import TestProject, remove_project, project_exists
from qgis.PyQt import QtCore

def gui_mode(project_name):
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    obj_project = Project.load_project(project_name)

    test_dialog = DryInflowSectorSettings(obj_project)
    test_dialog.exec_()


def test():
    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)
    obj_project.add_new_model(model_name)
    obj_project.set_current_model(model_name)


    obj_project.execute("""insert into project.dry_inflow_sector(name,geom)
    values('sector_1','SRID=2154; POLYGON((5034.74868256 1244835.8256,6239.35294856 1241081.94253,12332.4094103 1244093.4532,8830.65282309 1244821.81857,5034.74868256 1244835.8256))'::geometry);""")

    obj_project.execute("""insert into project.dry_inflow_sector(name,geom)
    values('sector_2','SRID=2154; POLYGON((2233.3434128 1248435.63137,1336.89372647 1246894.85847,4614.5378921 1247006.91468,5104.7838143 1248113.46976,2233.3434128 1248435.63137))'::geometry);""")

    obj_project.execute("""insert into model.manhole_node(geom)
    values('SRID=2154; POINTZ (6730 1247206 9999)'::geometry);""")
    obj_project.execute("""insert into model.manhole_node(geom)
    values('SRID=2154; POINTZ (2376 1247444 9999)'::geometry);""")
    obj_project.execute("""insert into model.manhole_node(geom)
    values('SRID=2154; POINTZ (4378 1247254 9999)'::geometry);""")
    obj_project.execute("""insert into model.manhole_node(geom)
    values('SRID=2154; POINTZ (2149.30125471 1247959.39247 9999)'::geometry);""")

    obj_project.execute("""insert into model.hydrograph_bc_singularity(geom)
    values('SRID=2154; POINTZ (6730 1247206 9999)'::geometry);""")
    obj_project.execute("""insert into model.hydrograph_bc_singularity(geom)
    values('SRID=2154; POINTZ (2376 1247444 9999)'::geometry);""")
    obj_project.execute("""insert into model.hydrograph_bc_singularity(geom)
    values('SRID=2154; POINTZ (4378 1247254 9999)'::geometry);""")
    obj_project.execute("""insert into model.hydrograph_bc_singularity(geom)
    values('SRID=2154; POINTZ (2149.30125471 1247959.39247 9999)'::geometry);""")

    test_dialog = DryInflowSectorSettings(obj_project)
    test_dialog.table_sectors.selectRow(0)
    assert test_dialog.sector_name.text()=='sector_1'
    test_dialog.table_sectors.selectRow(1)
    assert test_dialog.sector_name.text()=='sector_2'
    assert test_dialog.table_sector_hydrographs.rowCount()==3

    test_dialog.new_modulation()
    test_dialog.new_modulation()
    test_dialog.new_modulation()
    last_index = test_dialog.table_modulation.table.rowCount() -1
    test_dialog.table_modulation.table.selectRow(last_index)
    test_dialog.delete_modulation()

    test_dialog.table_sector_hydrographs.item(2,3).setCheckState(QtCore.Qt.Checked)
    test_dialog.table_sector_hydrographs.item(2,4).setText("2")
    test_dialog.table_sector_hydrographs.item(2,6).setText("3")
    test_dialog.table_sector_hydrographs.item(2,7).setText("4")
    test_dialog.table_sectors.selectRow(0)
    assert test_dialog.table_sector_hydrographs.rowCount()==0
    test_dialog.table_sectors.selectRow(1)
    assert test_dialog.table_sector_hydrographs.item(2,3).checkState()==QtCore.Qt.Checked
    assert test_dialog.table_sector_hydrographs.item(2,4).text()=="2.0"
    assert test_dialog.table_sector_hydrographs.item(2,6).text()=="3.0"
    assert test_dialog.table_sector_hydrographs.item(2,7).text()=="4.0"

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name)
    test()
    # fix_print_with_import
    print("ok")
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    project_name = normalized_name(project_name)
    gui_mode(project_name)
    exit(0)

test()
# fix_print_with_import

# fix_print_with_import
print("ok")
exit(0)

