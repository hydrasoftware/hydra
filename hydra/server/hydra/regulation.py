# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
hydra server-side module mesh specific functions

"""

import os
import re
import tempfile

__dico_reg_sql__ = {
    "l_gate":           "gate_link",
    "qmv":              "gate_link",
    "l_rgate":          "regul_gate_link",
    "rgz":              "regul_gate_link",
    "rgq":              "regul_gate_link",
    "l_weir":           "weir_link",
    "qms":              "weir_link",
    "l_pump":           "pump_link",
    "qmp":              "pump_link",
    "l_pump_deriv":     "deriv_pump_link",
    "qdp":              "deriv_pump_link",
    "s_gate":           "gate_singularity",
    "va":               "gate_singularity",
    "s_rgate":          "regul_sluice_gate_singularity",
    "rgb":              "regul_sluice_gate_singularity",
    "s_rweir":          "zregul_weir_singularity",
    "de":               "zregul_weir_singularity",
    "cl_zt":            "tz_bc_singularity",
    "clzt":             "tz_bc_singularity",
    "cl_dlat":          "zq_bc_singularity",
    "qdl1":             "zq_bc_singularity",
    "clq0":             "constant_inflow_bc_singularity",
    "cl_q0":            "constant_inflow_bc_singularity",
    "cas":              "storage_node",
    "storage":          "storage_node",
    "dh":               "parametric_headloss_singularity"

}

def regulated_items(plpy):
    res = plpy.execute("""select name as scenario, control_file as file
                                from project.scenario join project.param_regulation on param_scenario=scenario.id""")

    reg_files_dict = {}
    for file_scn_dict in res:
        file = file_scn_dict['file']
        scn = file_scn_dict['scenario']
        if file in reg_files_dict.keys():
            reg_files_dict[file].append(scn)
        else:
            reg_files_dict[file] = [scn]

    models = plpy.execute("select name from project.model order by UPPER(name) asc;")

    regulated_items_dict = []
    for file in reg_files_dict.keys():
        if os.path.isfile(file):
            with open(file, 'r') as f:
                for line_nb, line in enumerate(f):
                    match = re.search(r"(\w*)\s+'(\w*)'", line)
                    if match:
                        name_item = match.groups()[1]
                        type_brut = match.groups()[0]
                        if type_brut.lower() in __dico_reg_sql__.keys():
                            subtype = __dico_reg_sql__[type_brut.lower()]
                            type = subtype.split('_')[-1]
                            file_name = file
                            for model in [model['name'] for model in models]:
                                match = plpy.execute("""select id as i, ST_Force3D(ST_Centroid(geom)) as g from {}.{} where UPPER(name) = UPPER('{}')""".format(model, subtype, name_item))
                                if match:
                                    id = match[0]['i']
                                    geom = match[0]['g']
                                    for scn in reg_files_dict[file]:
                                        regulated_items_dict.append({'id': id, 'scenario': scn, 'file': file_name, 'iline': line_nb, 'model': model, 'name': name_item, 'type': type, 'subtype': subtype, 'geom': geom})

    return regulated_items_dict