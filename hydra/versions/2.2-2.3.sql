/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update hydra.metadata set version = '2.3';
;;

create or replace function project.model_instead_fct()
returns trigger
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_model
    for statement in create_model(TD['new']['name'], $srid, '$version'):
        plpy.execute(statement)
$$$$
;;

do
$$$$
    begin
    if exists (
        select 1 from pg_catalog.pg_namespace where nspname = 'csv'
    )
    then
      execute 'insert into csv.hydra_compatible_versions(hydra_version) values (''2.3'')';
    end if;
end
$$$$;
;;

/* ********************************************** */
/*                Cahtchment autofill             */
/* ********************************************** */

create type public.hydra_catchment_param as (area real, rl real, slope real, c_imp real);
;;

/* ********************************************** */
/*  Constrain update                              */
/* ********************************************** */

--isolated--
alter type hydra_constrain_type add value if not exists 'ignored_for_coverages';
;;

insert into hydra.constrain_type (id, name) values (0, 'ignored_for_coverages');
;;

/* ********************************************** */
/*  Scenario kernel version                       */
/* ********************************************** */

--isolated--
alter type hydra_kernel_version add value if not exists '20190125';
;;

--isolated--
alter type hydra_kernel_version add value if not exists '20190309';
;;

insert into hydra.kernel_version(name, id, description)
values ('20190125', 7, 'Loi bridge : amelioration du calcul des termes cinetiques'),
       ('20190309', 8, 'Loi bridge : amelioration subsequente du calcul des termes cinetiques');
;;

/* ********************************************** */
/*              Regulated items                   */
/* ********************************************** */

create type public.regulated_item as (
    id integer,
    scenario text,
    file  text,
    iline integer,
    model text,
    type text,
    subtype text,
    name text,
    geom geometry
)
;;

drop function if exists project.regulated
;;

create function project.regulated()
returns setof regulated_item
language plpython3u
as
$$$$
    import plpy
    from $hydra import regulated_items
    return regulated_items(plpy)
$$$$
;;
/* ********************************************** */
/*              Interlinks                        */
/* ********************************************** */

alter table project.model_config add constraint model_config_name_key unique (name);
;;

create table project._interlink(
    id serial primary key,
    name varchar(256) unique not null default 'INT_'||currval('project._interlink_id_seq')::varchar,
    model_up varchar not null references project.model_config(name) on delete cascade,
    node_up integer not null, --references model_up._node(id) but checked in trigger as it's a dynamic reference
    model_down varchar not null references project.model_config(name) on delete cascade,
    node_down integer not null, --references model_down._node(id) but checked in trigger as it's a dynamic reference
        unique(model_up, node_up, model_down, node_down),
    geom geometry('LINESTRINGZ',$srid) not null check(ST_IsValid(geom))
)
;;

 create view project.interlink as
    select id, name, model_up, node_up, model_down, node_down, geom
    from project._interlink
;;

create or replace function project.interlink_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        down_ integer;
    begin
        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            execute 'select id from ' || new.model_up || '._node where ST_DWithin(geom, ST_StartPoint(''' || ST_AsEWKT(new.geom) || '''::geometry), 0.01) and node_type in (''elem_2d'', ''crossroad'', ''storage'')' into up_;
            execute 'select id from ' || new.model_down || '._node where ST_DWithin(geom, ST_EndPoint(''' || ST_AsEWKT(new.geom) || '''::geometry), 0.01) and node_type = ''manhole''' into down_;
            if (up_ is null or down_ is null) then
                raise exception 'link % not touching nodes from model % to model %', ST_AsText(new.geom), new.model_up, new.model_down;
            end if;
        end if;
        if tg_op = 'INSERT' then
            insert into project._interlink(name, model_up, node_up, model_down, node_down, geom)
                values(coalesce(new.name, 'define_later'), new.model_up, up_, new.model_down, down_, new.geom) returning id into id_;
            update project._interlink set name = 'INT_'||id_::varchar where name = 'define_later' and id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update project._interlink set name=new.name, model_up=new.model_up, up=up_, model_down=new.model_down, down=down_, geom=new.geom where id=old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from project._interlink where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger project_interlink_trig
    instead of insert or update or delete on project.interlink
       for each row execute procedure project.interlink_fct()
;;

/* ********************************************** */
/* Scenario flag_hydrol_auto_dimensioning         */
/* ********************************************** */

alter table project.scenario add column if not exists flag_hydrology_auto_dimensioning bool not null default 'f'
;;

alter table project.scenario add column if not exists domain_2d_param bool not null default 'f'
;;

create table project.domain_2d_param(
    id serial primary key,
    scenario integer not null references project.scenario(id) on delete cascade,
    model varchar(24) not null,
    domain_2d integer not null,
        unique(scenario, model, domain_2d)
)
;;

alter table project.serie add column if not exists domain_2d_param bool not null default 'f'
;;

create table project.domain_2d_param_serie(
    id serial primary key,
    serie integer not null references project.serie(id) on delete cascade,
    model varchar(24) not null,
    domain_2d integer not null,
        unique(serie, model, domain_2d)
)
;;

drop view project.serie_scenario
;;

create or replace view project.serie_scenario as (
with
    tfin_bloc as (
        select min(b2.t_till_start) as tfin, b1.id
        from project.serie_bloc as b1, project.serie_bloc as b2
        where b2.t_till_start>b1.t_till_start
        and b2.id_cs = b1.id_cs
        group by b1.id
            union
        select s.tfin as tfin, b.id
        from project.serie as s, project.serie_bloc as b
        where b.id not in (
            select b1.id
            from project.serie_bloc as b1, project.serie_bloc as b2
            where b2.t_till_start>b1.t_till_start
            and b2.id_cs = b1.id_cs
            group by b1.id)
        and b.id_cs = s.id
    ),
    serie_bloc as (
        select s.id as id_cs,
            s.dt_days,
            s.tfin as tfin_cs,
            s.date0 as date0_cs,
            b.t_till_start as t_till_start_b,
            tfb.tfin as tfin_b,
            DATE_PART('day',tfb.tfin - b.t_till_start) as duration_days,
            DATE_PART('day',tfb.tfin - b.t_till_start)::int % s.dt_days as last_scn_duration,
            CEIL(DATE_PART('day',tfb.tfin - b.t_till_start)/s.dt_days)::int as nb_scn,
            b.id as id_b
        from project.serie as s, project.serie_bloc as b, tfin_bloc as tfb
        where s.id = b.id_cs and tfb.id = b.id
    ),
    serie_bloc_prev as (
        select
            sb1.*,
            sb2.id_b as id_prev,
            sb2.nb_scn as nb_prev,
            sb2.last_scn_duration as last_scn_prev
            from
            serie_bloc as sb1
            left join serie_bloc as sb2
            on sb1.t_till_start_b = sb2.tfin_b
            and sb1.id_cs = sb2.id_cs
	),
    gen as (
        select generate_series(1, nb_scn) as i, id_b from serie_bloc
    ),
    scn_gen as (
        select gen.i,
            (gen.i-1)*sb.dt_days as t_start,
            (gen.i)*sb.dt_days as t_fin,
            sb.dt_days as scn_duration,
            sb.id_b,
            sb.last_scn_duration,
            sb.nb_scn,
            sb.id_prev,
            sb.nb_prev,
            sb.last_scn_prev
        from serie_bloc_prev as sb, gen
        where gen.id_b = sb.id_b
        order by id_b,i
    ),
    scn_data as (
        select
            s.id as id_cs,
            b.id as id_b,
            scn_gen.i as index,
            scn_gen.t_start *'1 days'::interval + b.t_till_start as _tb_start,
            s.name || '_' || b.name || '_SCN' || to_char(scn_gen.i,'FM0000') as name,
            '' as comment,
            s.comput_mode as comput_mode,
            b.dry_inflow as dry_inflow,
            b.rainfall as rainfall,
            s.dt_hydrol_mn as dt_hydrol_mn,
            b.soil_moisture_coef as soil_moisture_coef,
            b.runoff_adjust_coef as runoff_adjust_coef,
            b.option_dim_hydrol_network as option_dim_hydrol_network,
            scn_gen.t_start *'1 days'::interval + b.t_till_start + s.date0 as date0,
            scn_gen.scn_duration * '24 hours'::interval as tfin_hr,
            s.tini_hydrau_hr as tini_hydrau_hr,
            s.dtmin_hydrau_hr as dtmin_hydrau_hr,
            s.dtmax_hydrau_hr as dtmax_hydrau_hr,
            s.dzmin_hydrau as dzmin_hydrau,
            s.dzmax_hydrau as dzmax_hydrau,
            s.dt_output_hr as dt_output_hr,
            't'::bool as flag_save,
            scn_gen.scn_duration * '24 hours'::interval as tsave_hr,
            case
                when scn_gen.i = 1 and scn_gen.id_prev is null then 'f'::bool
                else 't'::bool
            end as flag_rstart,
            b.flag_gfx_control as flag_gfx_control,
            case
                when scn_gen.i = 1 and scn_gen.id_prev is null then ARRAY[]::integer[]
                when scn_gen.i = 1 and scn_gen.id_prev is not null then ARRAY[s.id, scn_gen.id_prev, scn_gen.nb_prev]
                else ARRAY[s.id, b.id, scn_gen.i-1]
            end as scenario_rstart_array,
            'f'::bool as scenario_rstart,
            b.trstart_hr as trstart_hr,
            b.flag_hydrology_rstart as flag_hydrology_rstart,
            b.scenario_hydrology_rstart as scenario_hydrology_rstart,
           'f' as flag_hydrology_auto_dimensioning,
            'f'::bool as graphic_control,
            s.model_connect_settings as model_connect_settings,
            '00:00:00'::interval as tbegin_output_hr,
            scn_gen.scn_duration * '24 hours'::interval as tend_output_hr,
            s.c_affin_param as c_affin_param,
            s.domain_2d_param as domain_2d_param,
            s.t_affin_start_hr as t_affin_start_hr,
            s.t_affin_min_surf_ddomains as t_affin_min_surf_ddomains,
            s.strickler_param as strickler_param,
            s.option_file as option_file,
            s.option_file_path as option_file_path,
            s.output_option as output_option,
            s.sor1_mode as sor1_mode,
            s.dt_sor1_hr as dt_sor1_hr,
            s.transport_type as transport_type,
            s.iflag_mes as iflag_mes,
            s.iflag_dbo5 as iflag_dbo5,
            s.iflag_dco as iflag_dco,
            s.iflag_ntk as iflag_ntk,
            s.quality_type as quality_type,
            s.diffusion as diffusion,
            s.dispersion_coef as dispersion_coef,
            s.longit_diffusion_coef as longit_diffusion_coef,
            s.lateral_diff_coef as lateral_diff_coef,
            s.water_temperature_c as water_temperature_c,
            s.min_o2_mgl as min_o2_mgl,
            s.kdbo5_j_minus_1 as kdbo5_j_minus_1,
            s.koxygen_j_minus_1 as koxygen_j_minus_1,
            s.knr_j_minus_1 as knr_j_minus_1,
            s.kn_j_minus_1 as kn_j_minus_1,
            s.bdf_mgl as bdf_mgl,
            s.o2sat_mgl as o2sat_mgl,
            s.iflag_ecoli as iflag_ecoli,
            s.iflag_enti as iflag_enti,
            s.iflag_ad1 as iflag_ad1,
            s.iflag_ad2 as iflag_ad2,
            s.ecoli_decay_rate_jminus_one as ecoli_decay_rate_jminus_one,
            s.enti_decay_rate_jminus_one as enti_decay_rate_jminus_one,
            s.ad1_decay_rate_jminus_one as ad1_decay_rate_jminus_one,
            s.ad2_decay_rate_jminus_one as ad2_decay_rate_jminus_one,
            b.sst_mgl as sst_mgl,
            s.sediment_file as sediment_file,
            s.suspended_sediment_fall_velocity_mhr as suspended_sediment_fall_velocity_mhr,
            s.scenario_ref as scenario_ref ,
            b.scenario_hydrol as scenario_hydrol,
            s.kernel_version as kernel_version
        from
            project.serie as s,
            project.serie_bloc as b,
            scn_gen
        where
            s.id = b.id_cs and
            scn_gen.id_b = b.id and
            scn_gen.t_fin != ((scn_gen.nb_scn) * scn_gen.scn_duration)
                union
        select
            s.id as id_cs,
            b.id as id_b,
            scn_gen.i+1 as index,
            scn_gen.t_fin *'1 days'::interval + b.t_till_start as _tb_start,
            s.name || '_' || b.name || '_SCN' || to_char(scn_gen.i+1,'FM0000') as name,
            '' as comment,
            s.comput_mode as comput_mode,
            b.dry_inflow as dry_inflow,
            b.rainfall as rainfall,
            s.dt_hydrol_mn as dt_hydrol_mn,
            b.soil_moisture_coef as soil_moisture_coef,
            b.runoff_adjust_coef as runoff_adjust_coef,
            b.option_dim_hydrol_network as option_dim_hydrol_network,
            scn_gen.t_fin *'1 days'::interval + b.t_till_start + s.date0 as date0,
            case
                when scn_gen.last_scn_duration = 0 then scn_gen.scn_duration * '24 hours'::interval
                else scn_gen.last_scn_duration * '24 hours'::interval
            end as tfin_hr,
            s.tini_hydrau_hr as tini_hydrau_hr,
            s.dtmin_hydrau_hr as dtmin_hydrau_hr,
            s.dtmax_hydrau_hr as dtmax_hydrau_hr,
            s.dzmin_hydrau as dzmin_hydrau,
            s.dzmax_hydrau as dzmax_hydrau,
            s.dt_output_hr as dt_output_hr,
            't'::bool as flag_save,
            case
                when scn_gen.last_scn_duration = 0 then scn_gen.scn_duration * '24 hours'::interval
                else scn_gen.last_scn_duration * '24 hours'::interval
            end as tsave_hr,
            case
                when scn_gen.i = 1 and scn_gen.id_prev is null then 'f'::bool
                else 't'::bool
            end as flag_rstart,
            b.flag_gfx_control as flag_gfx_control,
            case
                when scn_gen.i = 1 and scn_gen.id_prev is null then ARRAY[]::integer[]
                when scn_gen.i = 1 and scn_gen.id_prev is not null then ARRAY[s.id, scn_gen.id_prev, scn_gen.nb_prev]
                else ARRAY[s.id, b.id, scn_gen.i]
            end as scenario_rstart_array,
            'f'::bool as scenario_rstart,
            b.trstart_hr as trstart_hr,
            b.flag_hydrology_rstart as flag_hydrology_rstart,
            b.scenario_hydrology_rstart as scenario_hydrology_rstart,
           'f' as flag_hydrology_auto_dimensioning,
            'f'::bool as graphic_control,
            s.model_connect_settings as model_connect_settings,
            '00:00:00'::interval as tbegin_output_hr,
            scn_gen.scn_duration * '24 hours'::interval as tend_output_hr,
            s.c_affin_param as c_affin_param,
            s.domain_2d_param as domain_2d_param,
            s.t_affin_start_hr as t_affin_start_hr,
            s.t_affin_min_surf_ddomains as t_affin_min_surf_ddomains,
            s.strickler_param as strickler_param,
            s.option_file as option_file,
            s.option_file_path as option_file_path,
            s.output_option as output_option,
            s.sor1_mode as sor1_mode,
            s.dt_sor1_hr as dt_sor1_hr,
            s.transport_type as transport_type,
            s.iflag_mes as iflag_mes,
            s.iflag_dbo5 as iflag_dbo5,
            s.iflag_dco as iflag_dco,
            s.iflag_ntk as iflag_ntk,
            s.quality_type as quality_type,
            s.diffusion as diffusion,
            s.dispersion_coef as dispersion_coef,
            s.longit_diffusion_coef as longit_diffusion_coef,
            s.lateral_diff_coef as lateral_diff_coef,
            s.water_temperature_c as water_temperature_c,
            s.min_o2_mgl as min_o2_mgl,
            s.kdbo5_j_minus_1 as kdbo5_j_minus_1,
            s.koxygen_j_minus_1 as koxygen_j_minus_1,
            s.knr_j_minus_1 as knr_j_minus_1,
            s.kn_j_minus_1 as kn_j_minus_1,
            s.bdf_mgl as bdf_mgl,
            s.o2sat_mgl as o2sat_mgl,
            s.iflag_ecoli as iflag_ecoli,
            s.iflag_enti as iflag_enti,
            s.iflag_ad1 as iflag_ad1,
            s.iflag_ad2 as iflag_ad2,
            s.ecoli_decay_rate_jminus_one as ecoli_decay_rate_jminus_one,
            s.enti_decay_rate_jminus_one as enti_decay_rate_jminus_one,
            s.ad1_decay_rate_jminus_one as ad1_decay_rate_jminus_one,
            s.ad2_decay_rate_jminus_one as ad2_decay_rate_jminus_one,
            b.sst_mgl as sst_mgl,
            s.sediment_file as sediment_file,
            s.suspended_sediment_fall_velocity_mhr as suspended_sediment_fall_velocity_mhr,
            s.scenario_ref as scenario_ref,
            b.scenario_hydrol as scenario_hydrol,
            s.kernel_version as kernel_version
        from
            project.serie as s,
            project.serie_bloc as b,
            scn_gen
        where
            s.id = b.id_cs and
            scn_gen.id_b = b.id and
            scn_gen.t_fin = ((scn_gen.nb_scn-1) * scn_gen.scn_duration)
                union
        select s.id as id_cs,
            b.id as id_b,
            scn_gen.i + 1 as index,
            scn_gen.t_fin::double precision * '1 day'::interval + b.t_till_start - scn_gen.scn_duration * '1 day'::interval as _tb_start,
            (((s.name::text || '_'::text) || b.name::text) || '_SCN'::text) || to_char(scn_gen.i, 'FM0000'::text) as name,
            ''::text as comment,
            s.comput_mode,
            b.dry_inflow,
            b.rainfall,
            s.dt_hydrol_mn,
            b.soil_moisture_coef,
            b.runoff_adjust_coef,
            b.option_dim_hydrol_network,
            scn_gen.t_fin::double precision * '1 day'::interval + b.t_till_start + s.date0 - scn_gen.scn_duration * '1 day'::interval as date0,
            case
                when scn_gen.last_scn_duration = 0 then scn_gen.scn_duration::double precision * '24:00:00'::interval
                else scn_gen.last_scn_duration::double precision * '24:00:00'::interval
            end as tfin_hr,
            s.tini_hydrau_hr,
            s.dtmin_hydrau_hr,
            s.dtmax_hydrau_hr,
            s.dzmin_hydrau,
            s.dzmax_hydrau,
            s.dt_output_hr,
            true as flag_save,
            case
                when scn_gen.last_scn_duration = 0 then scn_gen.scn_duration::double precision * '24:00:00'::interval
                else scn_gen.last_scn_duration::double precision * '24:00:00'::interval
            end as tsave_hr,
            case
                when scn_gen.i = 1 and scn_gen.id_prev is null then false
                else true
            end as flag_rstart,
            b.flag_gfx_control,
            case
                when scn_gen.i = 1 and scn_gen.id_prev is null then array[]::integer[]
                when scn_gen.i = 1 and scn_gen.id_prev is not null then array[s.id, scn_gen.id_prev, scn_gen.nb_prev]
                else array[s.id, b.id, scn_gen.i]
            end as scenario_rstart_array,
            false as scenario_rstart,
            b.trstart_hr,
            b.flag_hydrology_rstart,
            b.scenario_hydrology_rstart,
           'f' as flag_hydrology_auto_dimensioning,
            false as graphic_control,
            s.model_connect_settings,
            '00:00:00'::interval as tbegin_output_hr,
            scn_gen.scn_duration::double precision * '24:00:00'::interval as tend_output_hr,
            s.c_affin_param,
            s.domain_2d_param,
            s.t_affin_start_hr,
            s.t_affin_min_surf_ddomains,
            s.strickler_param,
            s.option_file,
            s.option_file_path,
            s.output_option,
            s.sor1_mode,
            s.dt_sor1_hr,
            s.transport_type,
            s.iflag_mes,
            s.iflag_dbo5,
            s.iflag_dco,
            s.iflag_ntk,
            s.quality_type,
            s.diffusion,
            s.dispersion_coef,
            s.longit_diffusion_coef,
            s.lateral_diff_coef,
            s.water_temperature_c,
            s.min_o2_mgl,
            s.kdbo5_j_minus_1,
            s.koxygen_j_minus_1,
            s.knr_j_minus_1,
            s.kn_j_minus_1,
            s.bdf_mgl,
            s.o2sat_mgl,
            s.iflag_ecoli,
            s.iflag_enti,
            s.iflag_ad1,
            s.iflag_ad2,
            s.ecoli_decay_rate_jminus_one,
            s.enti_decay_rate_jminus_one,
            s.ad1_decay_rate_jminus_one,
            s.ad2_decay_rate_jminus_one,
            b.sst_mgl,
            s.sediment_file,
            s.suspended_sediment_fall_velocity_mhr,
            s.scenario_ref,
            b.scenario_hydrol,
            s.kernel_version as kernel_version
           from project.serie s,
            project.serie_bloc b,
            scn_gen
          where s.id = b.id_cs and scn_gen.id_b = b.id and scn_gen.nb_scn = 1
    )
    select
        row_number() over(order by id_cs, id_b, index) as id,
        scn_data.*
    from scn_data
    )
;;

/* ********************************************** */
/*  Soil occupation                               */
/* ********************************************** */

create table project.land_type(
    id serial primary key,
    land_type varchar not null default project.unique_name('LAND_TYPE_') unique check(land_type!=''),
    c_imp real not null default 0.5 check(0<=c_imp and c_imp<=1),
    netflow_type hydra_netflow_type default 'constant_runoff' check(netflow_type in ('constant_runoff', 'scs', 'holtan')),
    c_runoff real check(0<=c_runoff and c_runoff<=1),
    curve real check(0<=curve),
    f0_mm_h real check(0<=f0_mm_h),
    fc_mm_h real check(0<=fc_mm_h),
    soil_cap_mm real check(0<=soil_cap_mm)
)
;;

create table project._land_occupation(
    id serial primary key,
    geom geometry('POLYGON',$srid) not null check(ST_IsValid(geom)),
    land_type_id integer references project.land_type(id) on delete set null
)
;;

create or replace view project.land_occupation as
    select o.id, o.geom, t.land_type, t.c_imp, t.netflow_type
    from project._land_occupation as o
    left join project.land_type as t on t.id=o.land_type_id
;;

create function project.land_occupation_after_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        land_type_id_ integer;
    begin
        if tg_op = 'INSERT' then
            -- Creates polygon with geom
            insert into project._land_occupation(geom) values(new.geom) returning id into id_;
            -- Check if new land_type_occupation exists, creates if needed
            if nullif(new.land_type, '') is not null then
                select id from project.land_type where land_type=new.land_type into land_type_id_;
                if land_type_id_ is null then
                    insert into project.land_type(land_type, c_imp, netflow_type) values (new.land_type, coalesce(new.c_imp, 0.5), coalesce(new.netflow_type, 'constant_runoff')) returning id into land_type_id_;
                end if;
                -- Update polygon with land type foreign key
                update project._land_occupation set land_type_id=land_type_id_ where id=id_;
            end if;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Update polygon geometry
            update project._land_occupation set geom=new.geom where id=old.id;
            -- If updated land_type is null or ''
            if nullif(new.land_type, '') is null then
                -- Update polygon land type foreign key to null
                update project._land_occupation set land_type_id=null where id=old.id;
            -- Check if updated land_type exists, creates if needed
            elsif nullif(new.land_type, '') is not null then
                select id from project.land_type where land_type=new.land_type into land_type_id_;
                if land_type_id_ is null then
                    insert into project.land_type(land_type, c_imp, netflow_type) values (new.land_type, coalesce(new.c_imp, 0.5), coalesce(new.netflow_type, 'constant_runoff')) returning id into land_type_id_;
                end if;
                -- Update polygon land type foreign key
                update project._land_occupation set land_type_id=land_type_id_ where id=old.id;
            end if;
            return new;
        elsif tg_op = 'DELETE' then
            delete from project._land_occupation where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger project_land_occupation_after_trig
    instead of insert or update or delete on project.land_occupation
    for each row execute procedure project.land_occupation_after_fct()
;;
