# -*- coding: utf-8 -*-

"""
***************************************************************************
    dinftranslimaccum.py
    ---------------------
    Date                 : June 2012
    Copyright            : (C) 2012-2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'June 2012'
__copyright__ = '(C) 2012-2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.core import (QgsVectorFileWriter,
                       QgsProcessing,
                       QgsProcessingException,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterBoolean,
                       QgsProcessingParameterRasterDestination
                      )
from .taudemAlgorithm import TauDemAlgorithm
from . import taudemUtils


class DinfTransLimAccum(TauDemAlgorithm):

    DINF_FLOWDIR = 'DINF_FLOWDIR'
    TRANSPORT_SUPPLY = 'TRANSPORT_SUPPLY'
    TRANSPORT_CAPACITY = 'TRANSPORT_CAPACITY'
    INPUT_CONCENTRATION = 'INPUT_CONCENTRATION'
    OUTLETS = 'OUTLETS'
    EDGE_CONTAMINATION = 'EDGE_CONTAMINATION'
    TRANLIM_ACCUM = 'TRANSLIM_ACCUM'
    DEPOSITION = 'DEPOSITION'
    OUTPUT_CONCENTRATION = 'OUTPUT_CONCENTRATION'

    def name(self):
        return 'dinftranslimaccum'

    def displayName(self):
        return self.tr('D-infinity transport limited accumulation')

    def group(self):
        return self.tr('Specialized grid analysis')

    def groupId(self):
        return 'specializedanalysis'

    def tags(self):
        return self.tr('dem,hydrology,d-infinity,transport,deposition').split(',')

    def shortHelpString(self):
        return self.tr('Calculates the transport and deposition of a substance '
                       '(e.g. sediment) that may be limited by both supply and '
                       'the capacity of the flow field to transport it.')

    def helpUrl(self):
        return 'http://hydrology.usu.edu/taudem/taudem5/help53/DInfinityTransportLimitedAccumulation.html'

    def __init__(self):
        super().__init__()

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterRasterLayer(self.DINF_FLOWDIR,
                                                            self.tr('D-infinity flow directions')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.TRANSPORT_SUPPLY,
                                                            self.tr('Transport supply')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.TRANSPORT_CAPACITY,
                                                            self.tr('Transport capacity')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.INPUT_CONCENTRATION,
                                                            self.tr('Input concentration'),
                                                            defaultValue=None,
                                                            optional=True))
        self.addParameter(QgsProcessingParameterFeatureSource(self.OUTLETS,
                                                              self.tr('Outlets'),
                                                              types=[QgsProcessing.TypeVectorPoint],
                                                              defaultValue=None,
                                                              optional=True))
        self.addParameter(QgsProcessingParameterBoolean(self.EDGE_CONTAMINATION,
                                                        self.tr('Check for edge contamination'),
                                                        defaultValue=True))

        self.addParameter(QgsProcessingParameterRasterDestination(self.TRANLIM_ACCUM,
                                                                  self.tr('Transport limited accumulation')))
        self.addParameter(QgsProcessingParameterRasterDestination(self.DEPOSITION,
                                                                  self.tr('Deposition')))
        self.addParameter(QgsProcessingParameterRasterDestination(self.OUTPUT_CONCENTRATION,
                                                                  self.tr('Output concentration'),
                                                                  optional=True,
                                                                  createByDefault=False))

    def processAlgorithm(self, parameters, context, feedback):
        flowLayer = self.parameterAsRasterLayer(parameters, self.DINF_FLOWDIR, context)
        if flowLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.DINF_FLOWDIR))

        supplyLayer = self.parameterAsRasterLayer(parameters, self.TRANSPORT_SUPPLY, context)
        if supplyLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.TRANSPORT_SUPPLY))

        capacityLayer = self.parameterAsRasterLayer(parameters, self.TRANSPORT_CAPACITY, context)
        if capacityLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.TRANSPORT_CAPACITY))

        arguments = []
        arguments.append(self.command())

        arguments.append('-ang')
        arguments.append(flowLayer.source())
        arguments.append('-tsup')
        arguments.append(supplyLayer.source())
        arguments.append('-tc')
        arguments.append(capacityLayer.source())

        if self.INPUT_CONCENTRATION in parameters and parameters[self.INPUT_CONCENTRATION] is not None:
            inConcentration = self.parameterAsRasterLayer(parameters, self.INPUT_CONCENTRATION, context)
            arguments.append('-cs')
            arguments.append(inConcentration.source())

            outConcentration = self.parameterAsOutputLayer(parameters, self.OUTPUT_CONCENTRATION, context)
            if outConcentration:
                arguments.append('-ctpt')
                arguments.append(outConcentration.source())
            else:
                raise QgsProcessingException(self.tr('Output concentration is not set.'))

        if self.OUTLETS in parameters and parameters[self.OUTLETS] is not None:
            outlets = self.parameterAsCompatibleSourceLayerPath(parameters, self.OUTLETS, context,
                                                                QgsVectorFileWriter.supportedFormatExtensions(),
                                                                feedback=feedback)
            arguments.append('-o')
            arguments.append(outlets)

        if not self.parameterAsBool(parameters, self.EDGE_CONTAMINATION, context):
            arguments.append('-nc')

        arguments.append('-tla')
        arguments.append(self.parameterAsOutputLayer(parameters, self.TRANLIM_ACCUM, context))

        arguments.append('-tdep')
        arguments.append(self.parameterAsOutputLayer(parameters, self.DEPOSITION, context))

        taudemUtils.execute(arguments, feedback)

        return self.algorithmResults(parameters)
