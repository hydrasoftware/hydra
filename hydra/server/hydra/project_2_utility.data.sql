/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */


/* ********************************************** */
/*                      Models                    */
/* ********************************************** */

create table project.model_config(
    id serial primary key,
    name varchar(16) unique not null,
    config integer not null default 1,
        unique(name, config),
    cascade_order integer not null default currval('project.model_config_id_seq'),
    comment varchar(256)
)
;;

create table project.grp(
    id serial primary key,
    name varchar(256) unique not null default 'GROUP_'||currval('project.grp_id_seq')::varchar
)
;;

create table project.grp_model(
    grp integer not null references project.grp(id) on delete cascade,
    model integer not null references project.model_config(id) on delete cascade,
        unique(grp, model)
)
;;

create table project._interlink(
    id serial primary key,
    name varchar(256) unique not null default 'INT_'||currval('project._interlink_id_seq')::varchar,
    model_up varchar not null references project.model_config(name) on delete cascade,
    node_up integer not null, --references model_up._node(id) but checked in trigger as it's a dynamic reference
    model_down varchar not null references project.model_config(name) on delete cascade,
    node_down integer not null, --references model_down._node(id) but checked in trigger as it's a dynamic reference
        unique(model_up, node_up, model_down, node_down),
    geom geometry('LINESTRINGZ',$srid) not null check(ST_IsValid(geom))
)
;;

/* ********************************************** */
/*              Terrain & altitude                */
/* ********************************************** */

-- Source files

create table project.dem(
    id serial primary key,
    priority integer not null default currval('project.dem_id_seq'),
    source varchar(256) unique not null
)
;;

create table project.points_type(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('TYPE_'),
    comment varchar(256)
    )
;;

create table project.points_xyz(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('P_XYZ_'),
    source varchar(24) default null,
    comment varchar(256) default null,
    geom geometry('POINTZ',$srid) not null,
    z_ground real not null,
    points_id integer references project.points_type(id) on delete set null
)
;;

create index project_points_xyz_geom_idx on project.points_xyz using gist(geom)
;;

create table project.line_xyz(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('L_XYZ_'),
    source varchar(24) default null,
    comment varchar(256) default null,
    geom geometry('LINESTRINGZ',$srid) not null
)
;;

create index project_line_xyz_geom_idx on project.line_xyz using gist(geom)
;;

/* ********************************************** */
/*              Utilities                         */
/* ********************************************** */

create table project.extract(
    id serial primary key,
    ctrl_file varchar(256) not null default ''
)
;;

create table project.animeau(
    id serial primary key,
    param_file varchar(256) not null default ''
)
;;

create table project.crgeng(
    id serial primary key,
    ctrl_file varchar(256) not null default ''
)
;;

/* ********************************************** */
/*              Land use                          */
/* ********************************************** */

create table project.pollution_land_accumulation(
    id integer not null default 1 unique check (id=1),
    sewage_storage_kgha real[4][4] not null
        check(array_length(sewage_storage_kgha, 1)=4 and array_length(sewage_storage_kgha, 2)=4)
        default '{{0,0,0,0},{175,17.5,87.5,3.25},{50,5,25,1},{112.5,11,56,2.2}}'::real[],
    unitary_storage_kgha real[4][4] not null
        check(array_length(unitary_storage_kgha, 1)=4 and array_length(unitary_storage_kgha, 2)=4)
        default '{{0,0,0,0},{280,35,210,6.2},{80,10,50,2},{180,22,135,4.5}}'::real[],
    regeneration_time_days real not null default 5,
    a_extraction_coef real not null default 0.06,
    b_extraction_coef real not null default 1.5
)
;;

insert into project.pollution_land_accumulation default values;
;;

create table project.land_type(
    id serial primary key,
    land_type varchar not null default project.unique_name('LAND_TYPE_') unique check(land_type!=''),
    c_imp real not null default 0.5 check(0<=c_imp and c_imp<=1),
    netflow_type hydra_netflow_type default 'constant_runoff' check(netflow_type in ('constant_runoff', 'scs', 'holtan')),
    c_runoff real check(0<=c_runoff and c_runoff<=1),
    curve real check(0<=curve),
    f0_mm_h real check(0<=f0_mm_h),
    fc_mm_h real check(0<=fc_mm_h),
    soil_cap_mm real check(0<=soil_cap_mm)
)
;;

create table project._land_occupation(
    id serial primary key,
    geom geometry('POLYGON',$srid) not null check(ST_IsValid(geom)),
    land_type_id integer references project.land_type(id) on delete set null
)
;;
