# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


'''
uploads a test project (MAGES), updates it, then tests the SQL functions to export to DB

USAGE

   export_csv_test.py [-hk] [projet_name] / [-w] [projet_name] [foldername]

OPTIONS

   -h, --help
        writes this help

   -w, --write
        writes to specified folder

   -k, --keep
        does not delete test database after test

'''

import sys
import os
import re
import json
import string
import getopt
from hydra.project import Project
from hydra.utility.settings_properties import SettingsProperties
from hydra.utility.string import read_file

_currendir = os.path.dirname(__file__)

_csv_files = [os.path.join(_currendir, "..", "server", "hydra", "csv.data.sql"),
              os.path.join(_currendir, "..", "server", "hydra", "csv.api.sql")]

def _create_statements(fileName, substitutions={}):
    '''parse .sql file into queries and makes necessary substitutions'''
    return [string.Template(statement).substitute(substitutions)
        for statement in read_file(fileName).split(";;")[:-1]
        ]

class ExportCsv(object):
    def __init__(self, project):
        '''prepares to export.'''
        self.project = project

        with open(os.path.join(_currendir, 'csv_properties.json')) as j:
            self.csv_columns = json.load(j)

    def write_csv_to_folder(self, folder_name):
        '''calls all functions of csv_properties.json from csv schema, then writes retsult to corresponding csv files'''
        filenames = []
        types = list(self.csv_columns.keys())

        log_progress = self.project.log.progress("Exporting CSV files")
        ntables = 0
        for type in types:
            ntables += len(self.csv_columns[type].keys())

        itables=0
        for type in types:
            tables = list(self.csv_columns[type].keys())
            tables.sort()
            for table in tables:
                itables+=1
                code_hydra = self.csv_columns[type][table]['code_hydra']
                if self.__table_has_data(table):
                    filename = os.path.join(folder_name, (self.project.name.upper() + '_' + code_hydra + '.csv').replace(' ', '_'))
                    with open(filename, 'w') as file:
                        file.write('; '.join(self.__format(self.__table_header(type, table))))
                        file.write('\n')
                        for line in self.__table_data(type, table):
                            file.write(';'.join(line))
                            file.write('\n')
                    self.project.log.silent_notice("CSV export: Successful export to {}".format(os.path.abspath(filename)))
                    filenames.append(filename)
                else:
                    self.project.log.silent_notice("CSV export: No data to export for {}".format(code_hydra.upper()))
                log_progress.set_ratio(float(itables)/ntables)

        filefinal = os.path.join(folder_name, self.project.name.upper() + '.csv')
        if filenames:
            with open(filefinal, 'w') as outfile:
                for fname in filenames:
                    with open(fname) as infile:
                        for line in infile:
                            outfile.write(line)
        log_progress.set_ratio(1)
        self.project.log.clear_progress()
        self.project.log.notice("CSV export to {} complete".format(folder_name))

    def __format(self, enum):
        '''recursive. capitalizes all strings in enum, replaces '_' with spaces'''
        for child in enum:
            if isinstance(child, list):
                yield format(child)
            else:
                yield child.capitalize().replace('_', ' ')

    def __table_header(self, type, table):
        '''gets header of csv file for table'''
        # special cases first
        if table=='pump_link':
            return self.__pump_link_header()

        header = []
        for column in self.csv_columns[type][table]['columns']:
            array_match = re.search(r"\[(\d)\]", column.split(' ')[1])
            if array_match: # array
                header.append("n_points")
                for array_header in column.split(' ')[0].split('%'):
                    header.append(array_header)
            else:
                header.append(column.split(' ')[0])
        else:
            return header

    def __table_has_data(self, table):
        '''checks if function of table returns data or nothing'''
        return self.project.execute("""select exists(select 1 from csv.{}() limit 1)""".format(table)).fetchone()[0]

    def __table_data(self, type, table):
        '''gets content of csv file for table'''
        # special cases first
        if table=='pump_link':
            return self.__pump_link_data()

        data = self.project.execute("""select * from csv.{}()""".format(table)).fetchall()

        arrays={}
        for column in self.csv_columns[type][table]['columns']:
            array_match = re.search(r"\[(\d)\]", column.split(' ')[1])
            if array_match: # array
                arrays[self.csv_columns[type][table]['columns'].index(column)] = int(array_match.group(1))

        data_ordered = []
        for record in data:
            # find number of lines needed
            nlines = 1
            if any([isinstance(value, list) for value in record]):
                nlines = max([len(l) for l in record if isinstance(l, list)]+[1])
            for iline in range(nlines):
                record_ordered = []
                for irec in range(len(record)):
                    if irec not in arrays.keys(): #if not isinstance(value, list):
                        record_ordered.append(str(record[irec]) if record[irec] is not None else '-')
                    else: # we have a list to write !
                        if record[irec] is not None:
                            npoints = len(record[irec])
                            # N points in array
                            record_ordered.append(str(npoints))
                            if iline<npoints: # write values of array at index
                                for x in record[irec][iline]:
                                    record_ordered.append(str(x))
                            else: # pad with right number of '-'
                                for x in range(arrays[irec]):
                                    record_ordered.append('-')
                        else:
                            record_ordered.append('0')
                            for x in range(arrays[irec]):
                                record_ordered.append('-')

                data_ordered.append(record_ordered)
        return data_ordered

    def __pump_link_header(self):
        '''gets header of csv file for pumps (specific format)'''
        return ["modele", "element", "identifiant", "noeud_amont", "noeud_aval",
                "nb_pompes", "no_pompe", "z_demarrage_(m)", "z_arret_(m)",
                "nb_points", "cote_d_eau_(m)", "debit_(m3/s)"]

    def __pump_link_data(self):
        '''gets header of csv file for pumps (specific format)'''
        data = self.project.execute("""select * from csv.pump_link()""").fetchall()

        arrays={}
        for column in self.csv_columns['links']['pump_link']['columns']:
            array_match = re.search(r"\[(\d)\]", column.split(' ')[1])
            if array_match: # array
                arrays[self.csv_columns['links']['pump_link']['columns'].index(column)] = int(array_match.group(1))

        data_ordered = []
        for record in data:
            npump = record[5]
            for ipump in range(npump):
                lcarac = len([hq for hq in record[7][ipump] if hq != [None,None]])
                for icarac in range(lcarac):
                    record_ordered = [str(record[0]),                      # modele varchar,
                                      str(record[1]),                      # element varchar,
                                      str(record[2]),                      # identifiant varchar,
                                      str(record[3]),                      # noeud_amont varchar,
                                      str(record[4]),                      # noeud_aval varchar,
                                      str(npump),
                                      str(ipump+1),
                                      str(record[6][ipump][0]),            # zz_arr[Zstart],
                                      str(record[6][ipump][1]),            # zz_arr[Zstop],
                                      str(lcarac),
                                      str(record[7][ipump][icarac][0]),     # hq_arr[H],
                                      str(record[7][ipump][icarac][1])]     # hq_arr[Q],

                    data_ordered.append(record_ordered)
        return data_ordered

if __name__=='__main__':

    from ..project import Project
    import sys
    project = Project(sys.argv[1])
    exporter = ExportCsv(project)
    exporter.write_csv_to_folder(sys.argv[2])