# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..base_dialog import BaseDialog, tr
from qgis.PyQt import uic
import os

class DryInflowSectorEditor(BaseDialog):
    def __init__(self, project, contour, id=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "dry_inflow_sector.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.__contour = contour

        if id==None:
            self.__id, = self.project.execute("insert into project.dry_inflow_sector (geom) values ({}) returning id;".format(
                self.make_polygon(contour))).fetchone();
        else:
            self.__id = id
            if self.__contour is None:
                contour,= self.project.execute("""
                select geom
                from project.dry_inflow_sector
                where id={}""".format(self.__id)).fetchone()

        name, comment = self.project.execute("""
            select name, comment
            from project.dry_inflow_sector
            where id={}""".format(self.__id)).fetchone()
        self.name.setText(name)
        self.comment.setText(comment)

    def make_polygon(self, geom):
        return "'srid={}; POLYGON(({}))'::geometry".format(
            str(self.project.srid),
            ",".join([str(p[0])+" "+str(p[1])
                for p in geom[0]])) if geom else None

    def get_id(self):
        return self.__id

    def save(self):
        self.project.execute("update project.dry_inflow_sector set name='{}', comment='{}' where id={};".format(
                self.name.text(), self.comment.toPlainText(), self.__id))
        BaseDialog.save(self)
        self.close()
