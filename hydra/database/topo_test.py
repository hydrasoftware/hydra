# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
run topo tests

USAGE

   python -m hydra.server.topo_test [-hk]

OPTIONS

   -h, --help
        print this help

   -k, --keep
        keep database after test

"""

assert __name__ == "__main__"

from hydra.project import Project
from hydra.database.import_model import import_model_csv
from hydra.database.database import TestProject, project_exists, remove_project
from hydra.utility.timer import Timer
from hydra.database.terrain import Terrain
from shutil import rmtree
import os, sys
import getopt

hydra_dir = os.path.join(os.path.expanduser("~"), ".hydra")

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hk",
            ["help", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

keep =  "-k" in optlist or "--keep" in optlist


if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

project_name='topo_test'

if project_exists(project_name):
    remove_project(project_name)
if os.path.exists(os.path.join(hydra_dir, project_name)):
    rmtree(os.path.join(hydra_dir, project_name))

test_project = TestProject(project_name, keep)
project = Project.load_project(project_name)
project.add_new_model('model')
data_dir = os.path.join(os.path.dirname(__file__), 'test_data')

project.execute("""insert into project.dem(source) values ('{}')""".format(os.path.join(data_dir, 'terrain','terrain_1.vrt')))

project.execute("""
    create view dataR as
    with jdata as (
        select '{}'::json as data
    )
    select
        row_number() over() as id,
        st_setsrid(st_geomfromgeojson((d->'geometry')::varchar), 2154) as geom,
        (d->'properties'->'type')::varchar as type
    from (select json_array_elements(data->'features') as d from jdata) as t
    """.format(open(os.path.join(data_dir, 'topo_test_reach.geojson')).read()))

project.execute("""
    create view dataCS as
    with jdata as (
        select '{}'::json as data
    )
    select
        row_number() over() as id,
        st_setsrid(st_geomfromgeojson((d->'geometry')::varchar), 2154) as geom,
        (d->'properties'->'type')::varchar as type
    from (select json_array_elements(data->'features') as d from jdata) as t
    """.format(open(os.path.join(data_dir, 'topo_test_cross_section.geojson')).read()))

project.execute("""
    create view dataXYZ as
    with jdata as (
        select '{}'::json as data
    )
    select
        row_number() over() as id,
        st_setsrid(st_geomfromgeojson((d->'geometry')::varchar), 2154) as geom,
        (d->'properties'->'type')::varchar as type,
        (d->'properties'->'z_ground')::varchar as z_ground
    from (select json_array_elements(data->'features') as d from jdata) as t
    """.format(open(os.path.join(data_dir, 'topo_test_points_xyz.geojson')).read()))

project.execute("""
    create view data2 as
    with jdata as (
        select '{}'::json as data
    )
    select
        row_number() over() as id,
        st_setsrid(st_geomfromgeojson((d->'geometry')::varchar), 2154) as geom,
        (d->'properties'->'type')::varchar as type,
        (d->'properties'->'elem_length')::varchar as elem_length,
        (d->'properties'->'constrain_type')::varchar as constrain_type
    from (select json_array_elements(data->'features') as d from jdata) as t
    """.format(open(os.path.join(data_dir, 'topo_test_constrain.geojson')).read()))


statements = """ insert into model.reach(geom) select geom from dataR where type='"reach"'
;;
insert into model.river_node(geom) select geom from dataCS
;;
insert into model.river_cross_section_profile(geom) select geom from dataCS
;;
insert into project.points_xyz(geom, z_ground) select geom, z_ground::real from dataXYZ
;;
insert into model.constrain(geom, constrain_type, elem_length) select geom, replace(constrain_type,'"','')::hydra_constrain_type, elem_length::real from data2
;;
"""


for statement in statements.split(';;')[:-1]:
    project.execute(statement)

project.commit()

statement_profils = """select model.profile_from_transect(constrain.geom ,false,150,false,10) from model.constrain where constrain_type = 'flood_plain_transect'"""

tabresult = project.execute(statement_profils).fetchall()

profil_clean = [profil for (profil,) in tabresult]

profil_ref = [
        [[751.76, 0.0], [754.7, 4.13], [755.36, 29.99], [760.72, 37.07],
         [750.64, 0.0], [750.65, 0.05], [750.88, 2.345], [750.95, 2.786],
         [751.39, 4.74], [751.76, 5.932], [751.84, 0.364], [752.78, 9.194],
         [753.73, 12.484], [756.83, 18.784]
        ],
        [[751.69, 0.0], [753.63, 10.62], [754.38, 29.97], [756.33, 42.88],
         [749.27, 0.0], [749.33, 0.511], [749.74, 1.404], [749.76, 2.366],
         [751.67, 5.815], [751.69, 5.895], [752.47, 0.704], [755.05, 4.104],
         [756.07, 4.864], [758.2, 7.89]
        ],
        [[750.32, 0.0], [751.59, 11.45], [751.7, 21.14], [754.36, 27.41],
         [746.5, 0.0], [746.56, 1.716], [746.88, 2.902], [748.07, 4.229],
         [749.0, 6.792], [750.32, 11.131], [750.35, 0.087], [752.64, 4.217],
         [753.66, 7.657], [756.06, 12.817]
        ],
        [[746.09, 0.0], [747.37, 13.19], [748.83, 21.45], [750.82, 25.469],
         [744.3, 0.0], [744.79, 5.098], [744.98, 6.718], [745.4, 7.192],
         [745.76, 9.483], [746.09, 10.639], [746.55, 0.382], [747.8, 5.982],
         [749.39, 8.802], [752.45, 14.142]
        ],
        [[751.28, 2.396], [753.12, 9.786], [753.45, 26.826], [756.21, 38.686],
         [747.39, 0.0], [747.73, 1.01], [748.18, 2.241], [748.89, 3.622],
         [749.19, 4.725], [749.19, 4.725], [749.19, 0.0], [750.09, 0.35],
         [754.3, 6.7], [756.47, 9.53]
        ],
        [[749.56, 1.672], [750.86, 11.532], [751.05, 21.392], [751.97, 25.822],
         [744.8, 0.0], [744.85, 0.745], [746.0, 1.812], [746.06, 2.59],
         [747.58, 4.735], [748.66, 7.314], [748.66, 0.0], [748.66, 2.0],
         [750.02, 5.45], [754.5, 18.05]
        ]
    ]

for i in range(6):
    for res, ref in zip(profil_clean[i], profil_ref[i]):
        if ref[1] != res[1]:
            print("{: 5.2f}".format(100*(res[1]-ref[1])/ref[1]), res, ref)
    assert(res[0] == ref[0] )
    assert(res[1] - ref[1] <  .01)



assert len(profil_clean) == 6


print('ok')
