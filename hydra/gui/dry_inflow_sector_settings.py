# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
from functools import partial
from qgis.PyQt import uic
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QTableWidgetItem, QComboBox, QAbstractItemView
from hydra.gui.base_dialog import BaseDialog, tr
from hydra.gui.widgets.array_widget import ArrayWidget
from hydra.gui.widgets.graph_widget import GraphWidget
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from hydra.utility.string import normalized_name
import hydra.utility.string as string

class DryInflowSectorSettings(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "dry_inflow_sector_settings.ui"), self)

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.active_sector_id = None
        self.active_modulation_id=None

        self.set_sectors_table()

        self.table_sector_hydrographs.itemChanged.connect(self.table_sector_hydrographs_itemChanged)

        self.table_modulation = HydraTableWidget(project,
            ("id", "name", "comment"),
            (tr("Id"), tr("Name"), tr("Comment")),
            "project", "dry_inflow_hourly_modulation",
            (self.new_modulation, self.delete_modulation), "", "id",
            self.table_modulation_placeholder)
        self.table_modulation.set_editable(True)
        self.table_modulation.data_edited.connect(self.save_modulation)
        self.table_modulation.table.itemSelectionChanged.connect(self.set_active_modulation)
        self.table_modulation.table.resizeColumnsToContents()

        self.graph_modulation = GraphWidget(self.graph_modulation_placeholder)

        self.array_modulation = ArrayWidget(["Time\n(h)", "Weekday\nV(t)/V(24h)", "Weekend\nV(t)/V(24h)"], [25,3], [[0,0,0],[24,1,1]],
                draw_graph_func=self.__draw_graph, help_func=None, parent=self.array_modulation_placeholder)
        self.array_modulation.setEnabled(False)

    def set_sectors_table(self):
        '''populates sectors table'''
        sectors = self.project.execute("select id, name, vol_curve, comment from project.dry_inflow_sector order by name asc").fetchall()
        modulations = self.project.execute("select id, name from project.dry_inflow_hourly_modulation order by name").fetchall()

        self.table_sectors.setRowCount(0)

        for sector in sectors:
            row = self.table_sectors.rowCount()
            self.table_sectors.insertRow(row)
            self.table_sectors.setItem(row,0,QTableWidgetItem(string.get_str(sector[0])))
            self.table_sectors.setItem(row,1,QTableWidgetItem(string.get_str(sector[1])))

            self.table_sectors.setItem(row,2,QTableWidgetItem(string.get_str(sector[2])))
            cbox = self.get_new_modulation_combo(modulations, sector[2])
            cbox.currentIndexChanged.connect(partial(self.combo_modulation_index_changed, row))
            self.table_sectors.setCellWidget(row,2,cbox)

            self.table_sectors.setItem(row,3,QTableWidgetItem(string.get_str(sector[3])))

            self.table_sectors.item(row,0).setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            self.table_sectors.item(row,1).setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            self.table_sectors.item(row,2).setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)

        self.table_sectors.resizeColumnsToContents()
        self.table_sectors.horizontalHeader().setStretchLastSection(True)
        self.table_sectors.itemSelectionChanged.connect(self.set_active_sector)
        self.table_sectors.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.set_active_sector()

    def set_active_sector(self):
        '''set active sector and populates hydrographs table based on selected sector'''
        if not self.active_modulation_id is None:
            self.save_modulation()

        items = self.table_sectors.selectedItems()
        if len(items)>0:
            selected_row = self.table_sectors.row(items[0])
            if not self.active_sector_id is None:
                self.save_sector_hydrographs()

            self.active_sector_id = self.table_sectors.item(selected_row,0).text()
            self.sector_name.setText(self.table_sectors.item(selected_row,1).text())
            self.set_sector_hydrographs(selected_row)
            self.set_distribution_coef()
        else:
            self.sector_name.setText('')

    def save_sectors_table(self):
        '''save sectors table '''
        n = self.table_sectors.rowCount()
        for i in range(0, n):
            self.project.execute("""update project.dry_inflow_sector
                                    set vol_curve={}, comment='{}'
                                    where id={}""".format(
                                        string.get_nullable_sql_float(self.table_sectors.item(i,2).text()),
                                        self.table_sectors.item(i,3).text(),
                                        self.table_sectors.item(i,0).text()))

    def set_sector_hydrographs(self, selected_row):
        '''set hydrographs in hy table for selected sector'''
        self.table_sector_hydrographs.setRowCount(0)
        if self.active_sector_id is not None:
            models = self.project.get_models()
            for model in models:
                hydrographs = self.project.execute("""
                    with n as (select geom from project.dry_inflow_sector where id={})
                    select bc.id, bc.name, bc.distrib_coef, bc.constant_dry_flow, bc.sector,
                    bc.lag_time_hr
                    from {}.hydrograph_bc_singularity as bc, n
                    where st_contains(n.geom,bc.geom)
                    order by bc.name;""".format(self.active_sector_id, model)).fetchall()

                n = len(hydrographs)
                previous_row = self.table_sector_hydrographs.rowCount()
                self.table_sector_hydrographs.setRowCount(n + previous_row)

                for row in range(0, n):
                    index = row + previous_row
                    self.table_sector_hydrographs.setItem(index,0,QTableWidgetItem(string.get_str(hydrographs[row][0])))
                    self.table_sector_hydrographs.setItem(index,1,QTableWidgetItem(model))
                    self.table_sector_hydrographs.setItem(index,2,QTableWidgetItem(string.get_str(hydrographs[row][1])))

                    self.table_sector_hydrographs.setItem(index,3,QTableWidgetItem(""))
                    if string.get_sql_float(hydrographs[row][4])==None:
                        self.table_sector_hydrographs.item(index,3).setCheckState(Qt.Unchecked)
                    else:
                        self.table_sector_hydrographs.item(index,3).setCheckState(Qt.Checked)
                    self.table_sector_hydrographs.item(index,3).setFlags(Qt.ItemIsEnabled | Qt.ItemIsUserCheckable )

                    self.table_sector_hydrographs.setItem(index,4,QTableWidgetItem(string.get_str(hydrographs[row][2])))
                    self.table_sector_hydrographs.setItem(index,6,QTableWidgetItem(string.get_str(hydrographs[row][3])))
                    self.table_sector_hydrographs.setItem(index,7,QTableWidgetItem(string.get_str(hydrographs[row][5])))

            self.table_sector_hydrographs.resizeColumnsToContents()
            self.table_sector_hydrographs.horizontalHeader().setStretchLastSection(True)

    def get_sector_hydrographs(self):
        '''returns hydrographs table items'''
        n = self.table_sector_hydrographs.rowCount()
        result = list()
        index_list=0
        for i in range(0, n):
            result.append(list())
            result[index_list].append(self.table_sector_hydrographs.item(i,1).text())
            result[index_list].append(string.get_nullable_sql_float(self.table_sector_hydrographs.item(i,6).text()))
            result[index_list].append(string.get_nullable_sql_float(self.table_sector_hydrographs.item(i,4).text()))
            result[index_list].append(string.get_nullable_sql_float(self.table_sector_hydrographs.item(i,7).text()))
            if self.table_sector_hydrographs.item(i,3).checkState()==Qt.Checked:
                result[index_list].append(self.active_sector_id)
            else:
                result[index_list].append("null")
            result[index_list].append(self.table_sector_hydrographs.item(i,0).text())

            index_list = index_list + 1
        return result

    def save_sector_hydrographs(self):
        '''save hydrographs table '''
        if self.active_sector_id is None:
            return
        items = self.get_sector_hydrographs()
        if len(items)==0:
            return
        values = list()
        for item in items:
            sql= """update {}.hydrograph_bc_singularity
                set constant_dry_flow={}, distrib_coef={},
                lag_time_hr={},
                sector={}
                where id={};
                """.format(item[0], item[1], item[2],item[3],
                item[4],item[5])
            self.project.execute(sql)

    def table_sector_hydrographs_itemChanged(self, item):
        '''triggerred at changes in hydrographs table column 3 or 4 to refresh distribution coefs'''
        # compute distrib coefs when value in column 3 (def by sector) or 4 (contribution coef) is changed
        if item.column()==3 or item.column()==4:
            self.set_distribution_coef()

    def set_distribution_coef(self):
        '''computes and display coefs for column 5 based on ponderation from column 4'''
        row_count = self.table_sector_hydrographs.rowCount()
        # sum total active hydrographs contributions
        total_contribution = 0.0
        for row in range(row_count):
            if self.table_sector_hydrographs.item(row, 3) is not None and self.table_sector_hydrographs.item(row, 3).checkState()==Qt.Checked \
            and self.table_sector_hydrographs.item(row, 4) is not None and string.get_sql_forced_float(self.table_sector_hydrographs.item(row, 4).text()) >= 0:
                total_contribution += string.get_sql_forced_float(self.table_sector_hydrographs.item(row, 4).text())
        if total_contribution==0:
            total_contribution = 1
        # set each hydrograph distribution coef
        for row in range(row_count):
            if self.table_sector_hydrographs.item(row, 3) is not None and self.table_sector_hydrographs.item(row, 3).checkState()==Qt.Checked \
            and self.table_sector_hydrographs.item(row, 4) is not None and string.get_sql_forced_float(self.table_sector_hydrographs.item(row, 4).text()) >= 0:
                self.table_sector_hydrographs.setItem(row,5,QTableWidgetItem(string.get_str(round(string.get_sql_forced_float(self.table_sector_hydrographs.item(row, 4).text())/total_contribution,3))))
            else:
                self.table_sector_hydrographs.setItem(row,5,QTableWidgetItem(""))
            self.table_sector_hydrographs.item(row,5).setFlags(Qt.ItemIsEnabled)

    def get_new_modulation_combo(self, list_modulation, id_modulation):
        '''creates combobox with "constant" option + all modulation curves'''
        combobox = QComboBox()
        combobox.addItem(tr("Constant"))
        for i in range(0, len(list_modulation)):
            modulation = list_modulation[i]
            combobox.addItem("{} - {}".format(modulation[0], modulation[1]))
            if (str(id_modulation) == str(modulation[0])):
                combobox.setCurrentIndex(i+1)
        return combobox

    def combo_modulation_index_changed(self, irow):
        '''triggerred at modulation curve changed in combo'''
        icol=2
        self.table_sectors.selectRow(irow)
        id_modulation=self.table_sectors.cellWidget(irow, icol).currentText().split("-")[0] if self.table_sectors.cellWidget(irow, icol).currentText()!=tr("Constant") else "null"
        self.table_sectors.item(irow, icol).setText(id_modulation)

    def new_modulation(self):
        '''add a modulation curve'''
        self.save_sectors_table()
        self.table_modulation.add_row(['default','null', '{{0,0,0},{24,1,1}}'], ["hv_array"])
        self.set_sectors_table()

    def delete_modulation(self):
        '''delete selected modulation curve'''
        self.table_modulation.del_selected_row()

    def save_modulation(self):
        '''save selected modulation curve'''
        if self.active_modulation_id is not None:
            self.table_modulation.save_selected_row()
            items = self.array_modulation.get_table_items()
            if items is not None:
                self.project.execute("""update project.dry_inflow_hourly_modulation set hv_array='{}' where id={}""".format(string.list_to_sql_array(items), self.active_modulation_id))

    def set_active_modulation(self):
        '''set active modulation curve and refresh array data'''
        if not self.active_sector_id is None:
            self.save_sector_hydrographs()
        items = self.table_modulation.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_modulation.table.row(items[0])
            if not self.active_modulation_id is None:
                self.save_modulation()

            self.active_modulation_id = self.table_modulation.table.item(selected_row,0).text()
            hv_array, = self.project.execute("""select hv_array from project.dry_inflow_hourly_modulation where id={};""".format(self.active_modulation_id)).fetchone()
            self.array_modulation.set_table_items(hv_array)
            self.array_modulation.setEnabled(True)

    def __draw_graph(self, datas):
        '''refresh modulation curve graph'''
        title = ''
        current_row = self.table_modulation.table.currentRow()
        if current_row != -1:
            title = self.table_modulation.table.item(current_row,1).text()
        if len(datas)>0:
            self.graph_modulation.clear()
            self.graph_modulation.add_line(
                [r[0] for r in datas],
                [r[1] for r in datas],
                "r",
                title,
                tr("t (h)"), tr("V/Vtot"))
            self.graph_modulation.add_line(
                [r[0] for r in datas],
                [r[2] for r in datas],
                "r", linestyle='--')
        else:
            self.graph_modulation.canvas.setVisible(False)

    def save(self):
        '''saves sectors, hydrographs table and modulation curves'''
        self.save_sectors_table()
        if not self.active_sector_id is None:
            self.save_sector_hydrographs()
        if not self.active_modulation_id is None:
            self.save_modulation()
        BaseDialog.save(self)
        self.close()
