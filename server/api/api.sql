create schema ___
;

create table ___.user_project(
    project varchar not null,
    username varchar not null,
    unique(username, project))
;

create schema api
;

create view api.user_project as
select * from ___.user_project
;