def style(gpkg, classes, field, units):
    if units=='m':
        col_min = [216,231,245]
        col_max = [8,48,107]
    else:
        col_min = [255,200,200]
        col_max = [255,0,0]
    with open(gpkg.rsplit('.', 1)[0] + '.qml', 'w') as qml :
        qml.write('\n'.join(
            ('<!DOCTYPE qgis PUBLIC "http://mrcc.com/qgis.dtd" "SYSTEM">',
            '<qgis styleCategories="Symbology">',
           f'  <renderer-v2 attr="{field}" type="categorizedSymbol" symbollevels="0" enableorderby="0" forceraster="0">',
            '    <categories>\n'))
            )
        i = 0
        for i, c in enumerate(classes):
            qml.write(f'''      <category symbol="{i}" label="{c} {units}" value="{c}" render="true"/>\n''')
        qml.write('    </categories>\n')
        qml.write('    <symbols>\n')
        i = 0
        for i, c in enumerate(classes):
            qml.write('\n'.join(
                ('     <symbol type="fill" force_rhr="0" name="{}" clip_to_extent="1" alpha="1">',
                '       <layer locked="0" enabled="1" class="SimpleFill" pass="0">',
                '         <prop k="color" v="{},{},{},255"/>',
                '         <prop k="outline_style" v="no"/>',
                '           <data_defined_properties>',
                '             <Option type="Map">',
                '               <Option type="QString" value="" name="name"/>',
                '               <Option name="properties"/>',
                '               <Option type="QString" value="collection" name="type"/>',
                '             </Option>',
                '           </data_defined_properties>',
                '       </layer>',
                '     </symbol>\n')).format(i,
                    int(col_min[0]-(col_min[0]-col_max[0])*(i)/len(classes)),
                    int(col_min[1]-(col_min[1]-col_max[1])*(i)/len(classes)),
                    int(col_min[2]-(col_min[2]-col_max[2])*(i)/len(classes))))
        qml.write('\n'.join(
            ('    </symbols>',
            '  <rotation/>',
            '  <sizescale/>',
            ' </renderer-v2>',
            ' <blendMode>0</blendMode>',
            ' <featureBlendMode>0</featureBlendMode>',
            ' <layerGeometryType>2</layerGeometryType>',
            '</qgis>\n')))