# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.forms.zq_split_hydrology_singularity_test [-dh] [-g project_name model_name x y]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name model_name x y

"""

import sys
import getopt
from hydra.database.database import TestProject, remove_project, project_exists
from hydra.utility.string import normalized_name, normalized_model_name
from hydra.project import Project

def gui_mode(project_name, model_name, geom, id=None):

    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.zq_split_hydrology_singularity import ZqSplitHydrologySingularityEditor
    from hydra.utility.string import normalized_name, normalized_model_name

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)

    test_dialog = ZqSplitHydrologySingularityEditor(obj_project, geom, id=id)
    test_dialog.exec_()


def test():
    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.zq_split_hydrology_singularity import ZqSplitHydrologySingularityEditor
    from hydra.database.database import TestProject

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)
    model = obj_project.get_current_model()

    model.add_manhole_hydrology([59,61])
    model.add_manhole_hydrology([61,59])
    model.add_manhole_hydrology([85,69])
    model.add_manhole_hydrology([39,39])

    model.add_simple_link('connector_hydrology_link', [[39,39], [59,61]], 'connector_7')
    model.add_simple_link('connector_hydrology_link', [[61,59], [39,39]], 'connector_8')
    model.add_simple_link('connector_hydrology_link', [[39,39], [85,69]], 'connector_9')


    test_dialog = ZqSplitHydrologySingularityEditor(obj_project, [39,39])
    test_dialog.name.setText('zq_split_test')
    test_dialog.downstream.setText('connector_7')
    test_dialog.downstream_law_widget.law_type_combo.setCurrentIndex(test_dialog.downstream_law_widget.law_type_combo.findText('1: weir'))
    test_dialog.downstream_law_widget.weir_cc.setText("1.1")
    test_dialog.downstream_law_widget.weir_z_invert.setText("1.2")
    test_dialog.downstream_law_widget.weir_width.setText("1.3")
    test_dialog.split1.setText('connector_8')
    test_dialog.split1_law_widget.law_type_combo.setCurrentIndex(test_dialog.split1_law_widget.law_type_combo.findText('2: gate'))
    test_dialog.split1_law_widget.gate_cc.setText("2.1")
    test_dialog.split1_law_widget.gate_z_invert.setText("2.2")
    test_dialog.split1_law_widget.gate_diameter.setText("2.3")
    test_dialog.check_split2.setChecked(True)
    test_dialog.set_check_split2()
    test_dialog.split2.setText('connector_9')
    test_dialog.split2_law_widget.law_type_combo.setCurrentIndex(test_dialog.split2_law_widget.law_type_combo.findText('4: zq'))
    test_dialog.split2_law_widget.zq_array.set_table_items([[1,2],[3,4],[4,5],[6,7]])
    id_zq = test_dialog.get_id()
    test_dialog.save()

    test_dialog = ZqSplitHydrologySingularityEditor(obj_project, None, id_zq)
    assert test_dialog.name.text() =='zq_split_test'
    assert test_dialog.downstream.text() == 'connector_7'
    assert test_dialog.split1.text() == 'connector_8'
    assert test_dialog.check_split2.isChecked()
    assert test_dialog.split2.text() == 'connector_9'
    assert test_dialog.split2_law_widget.law_type_combo.currentText() == '4: zq'

    def list_equals(list1, list2):
        for val in list1:
            if not val in list2:
                return False
        return True

    assert list_equals(test_dialog.split2_law_widget.zq_array.get_table_items(),[[1,2],[3,4],[4,5],[6,7]])
    test_dialog.check_split2.setChecked(False)
    test_dialog.set_check_split2()
    test_dialog.save()

    test_dialog = ZqSplitHydrologySingularityEditor(obj_project, None, id_zq)
    assert test_dialog.downstream.text() == 'connector_7'
    assert test_dialog.downstream_law_widget.law_type_combo.currentText() == '1: weir'
    assert float(test_dialog.downstream_law_widget.weir_cc.text()) == 1.1
    assert float(test_dialog.downstream_law_widget.weir_z_invert.text()) == 1.2
    assert float(test_dialog.downstream_law_widget.weir_width.text()) == 1.3
    assert test_dialog.split1.text() == 'connector_8'
    assert test_dialog.split1_law_widget.law_type_combo.currentText() == '2: gate'
    assert float(test_dialog.split1_law_widget.gate_cc.text()) == 2.1
    assert float(test_dialog.split1_law_widget.gate_z_invert.text()) == 2.2
    assert float(test_dialog.split1_law_widget.gate_diameter.text()) == 2.3
    assert not test_dialog.check_split2.isChecked()
    assert test_dialog.split2.text() == ''
    test_dialog.reject()

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])

except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name, with_model=True)
    test()
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    model_name = args[1]
    geom = [float(a) for a in args[2:]]
    id =geom[2] if len(geom)==3 else None
    project_name = normalized_name(project_name)
    model_name = normalized_model_name(model_name)
    gui_mode(project_name, model_name, geom, id)
    exit(0)

test()
# fix_print_with_import
print("ok")
