/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

create table $model.closed_parametric_geometry(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('CP_'),
    zbmin_array real[20][2] check(array_length(zbmin_array, 1)<=20 and array_length(zbmin_array, 1)>=1 and array_length(zbmin_array, 2)=2)
)
;;

create table $model.open_parametric_geometry(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('OP_'),
    zbmin_array real[20][2] check(array_length(zbmin_array, 1)<=20 and array_length(zbmin_array, 1)>=1 and array_length(zbmin_array, 2)=2),
    with_flood_plain boolean default False,
    flood_plain_width real not null default 0,
    flood_plain_lateral_slope real not null default 0
)
;;

create table $model.valley_cross_section_geometry(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('VA_'),
    zbmin_array real[6][2] not null check(array_length(zbmin_array, 1)<=6 and array_length(zbmin_array, 1)>=1 and array_length(zbmin_array, 2)=2),
    zbmaj_lbank_array real[4][2] check(array_length(zbmaj_lbank_array, 1)<=8 and array_length(zbmaj_lbank_array, 1)>=1 and array_length(zbmaj_lbank_array, 2)=2),
    zbmaj_rbank_array real[4][2] check(array_length(zbmaj_rbank_array, 1)<=8 and array_length(zbmaj_rbank_array, 1)>=1 and array_length(zbmaj_rbank_array, 2)=2),
    rlambda real not null default 1 check(rlambda > 0 and rlambda <= 1),
    rmu1 real not null default -999,
    rmu2 real not null default -999,
    zlevee_lb real not null default -999,
    zlevee_rb real not null default -999,
    transect integer references $model._constrain(id) on update cascade on delete set null,
    t_ignore_pt boolean not null default False,
    t_discret int not null default 150,
    t_distance_pt int not null default 10,
    sz_array real[]
)
;;

create table $model.valley_cross_section_topo_geometry(
    id serial primary key,
    name varchar(24) unique not null default project.unique_name('VAT_'),
    xz_array real[150][2] check(array_length(xz_array, 1)<=150 and array_length(xz_array, 1)>=1 and array_length(xz_array, 2)=2)
)
;;

