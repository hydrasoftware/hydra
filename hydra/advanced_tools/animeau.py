# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
import webbrowser
from operator import itemgetter
from qgis.PyQt import uic, QtGui, QtCore
from qgis.PyQt.QtWidgets import QTableWidgetItem, QFileDialog, QMessageBox
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from hydra.gui.base_dialog import BaseDialog, tr
from hydra.gui.widgets.combo_with_values import ComboWithValues
from hydra.utility.process import run_cmd
import hydra.utility.string as string
from hydra.utility.system import open_external


class AnimeauError(Exception):
    pass

class AnimEau(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        self.current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(self.current_dir, "animeau.ui"), self)

        self.combo_scenarios = ComboWithValues(self.placeholder_combo_scenarios)
        scn_id_name = self.project.get_scenarios()
        for scn in sorted(scn_id_name, key=itemgetter(1)):
            self.combo_scenarios.addItem(scn[1],scn[0])
        if self.project.get_current_scenario():
            self.combo_scenarios.set_selected_value(self.project.get_current_scenario()[0])
        else:
            self.combo_scenarios.setCurrentIndex(0)

        models = self.project.get_models()
        for model in sorted(models, key=unicode.lower):
            self.combo_models.addItem(model)
        if self.project.get_current_model():
            self.combo_models.setCurrentIndex(self.combo_models.findText(self.project.get_current_model().name))
        else:
            self.combo_models.setCurrentIndex(0)

        self.table_control_files = HydraTableWidget(project,
            ("id", "param_file"),
            (tr("Id"), tr("Parameters File")),
            "project", "animeau",
            (self.new_control_file, self.delete_control_file, self.edit_control_file, self.help_control_file), "", "id",
            self.table_control_files_placeholder, showEdit=True, showHelp=True, file_columns=[1])
        self.table_control_files.set_editable(False)
        self.table_control_files.setEnabled(False)

        # deactivate parametric file adding
        self.param_file.setEnabled(False)

        self.param_file.stateChanged.connect(self.param_file_changed)
        self.go.clicked.connect(self.run)
        self.buttonBox.accepted.connect(self.save)

    def param_file_changed(self):
        if self.param_file.isChecked():
            self.table_control_files.setEnabled(True)
        elif not self.param_file.isChecked():
            self.table_control_files.setEnabled(False)

    def new_control_file(self):
        file_url, __ = QFileDialog.getSaveFileName(self, tr('Select animeau control file'), self.project.data_dir, options=QFileDialog.DontConfirmOverwrite)
        if file_url:
            # creates file empty if not exists
            open(file_url, 'a').close()
            self.project.execute("""insert into project.animeau(param_file) values('{}');""".format(self.project.pack_path(file_url)))
            self.table_control_files.update_data()

    def delete_control_file(self):
        items = self.table_control_files.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_control_files.table.row(items[0])
            id = int(self.table_control_files.table.item(selected_row,0).text())
            self.project.execute("""delete from project.animeau where id={};""".format(id))
            self.table_control_files.update_data()

    def edit_control_file(self):
        items = self.table_control_files.table.selectedItems()
        if len(items)>0:
            file = items[1].text()
            open_external(file)

    def help_control_file(self):
        webbrowser.open('http://hydra-software.net/docs/notes_techniques/NT32.pdf', new=0)

    def run(self):
        exe = os.path.abspath(os.path.join(self.current_dir, "wanimolt.exe"))

        if not os.path.isfile(exe):
            raise AnimeauError("Executable not found: {}".format(exe))

        if self.combo_scenarios.currentIndex()==-1 or self.combo_models.currentIndex()==-1:
            return

        scn = self.combo_scenarios.currentText()
        model = self.combo_models.currentText()
        command = [exe, scn, model]
        dir = os.path.join(self.project.directory, scn, 'hydraulique')
        if self.param_file.isChecked():
            items = self.table_control_files.table.selectedItems()
            if len(items)>0:
                selected_row = self.table_control_files.table.row(items[0])
                file = self.table_control_files.table.item(selected_row,1).text()
                command.insert(-1, file)
            else:
                return
        run_cmd(command, log=self.project.log, directory=dir)

    def save(self):
        BaseDialog.save(self)
        self.close()