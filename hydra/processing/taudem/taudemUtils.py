# -*- coding: utf-8 -*-

"""
***************************************************************************
    taudemUtils.py
    ---------------------
    Date                 : May 2012
    Copyright            : (C) 2012-2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'May 2012'
__copyright__ = '(C) 2012-2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

import os
import sys
import pipes
import subprocess
import pathlib

from qgis.core import Qgis, QgsMessageLog, QgsProcessingFeedback, QgsProcessingException
from processing.core.ProcessingConfig import ProcessingConfig

TAUDEM_ACTIVE = 'TAUDEM_ACTIVE'
TAUDEM_DIRECTORY = 'TAUDEM_DIRECTORY'
TAUDEM_MPICH = 'TAUDEM_MPICH'
TAUDEM_PROCESSES = 'TAUDEM_PROCESSES'
TAUDEM_VERBOSE = 'TAUDEM_VERBOSE'

TAUDEM_PATH = pathlib.Path(__file__).parent.parent.parent / "advanced_tools" / "taudem"


def taudemDirectory():
    filePath = ProcessingConfig.getSetting(TAUDEM_DIRECTORY)
    if not filePath and TAUDEM_PATH.exists():
        return str(TAUDEM_PATH)
    return filePath


def mpichDirectory():
    filePath = ProcessingConfig.getSetting(TAUDEM_MPICH)
    if not filePath and TAUDEM_PATH.exists():
        return str(TAUDEM_PATH)
    return filePath


def descriptionPath():
    return os.path.normpath(os.path.join(os.path.dirname(__file__), 'descriptions'))


def prepareArguments(arguments):
    if sys.platform == 'win32':
        return subprocess.list2cmdline(arguments)
    else:
        prepared = []
        for a in arguments:
            prepared.append(pipes.quote(a))
        return ' '.join(prepared)


def execute(commands, feedback=None):
    cmds = []
    cmds.append(os.path.join(mpichDirectory(), 'mpiexec'))

    processes = ProcessingConfig.getSetting(TAUDEM_PROCESSES)
    if int(processes) <= 0:
        processes = 1

    cmds.append('-n')
    cmds.append(processes)
    cmds.extend(commands)

    if feedback is None:
        feedback = QgsProcessingFeedback()

    QgsMessageLog.logMessage(' '.join(cmds), 'Processing', Qgis.Info)
    feedback.pushInfo('TauDEM command:')
    feedback.pushCommandInfo(' '.join(cmds))
    feedback.pushInfo('TauDEM command output:')

    with subprocess.Popen(
            cmds,
            stdout=subprocess.PIPE,
            stdin=subprocess.DEVNULL,
            stderr=subprocess.STDOUT,
            encoding='utf-8',
            bufsize=1,
            errors='replace') as proc:

        while True:
            output = proc.stdout.readline().strip()
            if proc.poll() is not None and not output:
                break
            if output:
                feedback.pushCommandInfo(output)

    if proc.returncode != 0:
        raise QgsProcessingException(f"Error in Taudem execution, code {proc.returncode}")
