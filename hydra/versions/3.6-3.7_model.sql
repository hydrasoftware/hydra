update $model.metadata set version = '3.7'
;;

alter table $model._catchment_node
drop column permeable_soil_j_mm,
drop column permeable_soil_rfu_mm,
drop column permeable_soil_ground_max_inf_rate,
drop column permeable_soil_ground_lag_time_day,
drop column permeable_soil_coef_river_exchange
;;

update $model._catchment_node set
hydra_surface_soil_storage_rfu_mm = hydra_permeable_surface_soil_storage_rfu_mm,
hydra_soil_drainage_time_qres_day = hydra_permeable_soil_drainage_time_qres_day
where netflow_type='hydra_permeable';

alter table $model._catchment_node
drop column hydra_permeable_surface_soil_storage_rfu_mm,
drop column hydra_permeable_soil_drainage_time_qres_day
;;

alter table $model._catchment_node
rename column hydra_permeable_split_coefficient to hydra_split_coefficient
;;

alter table $model._catchment_node
rename column hydra_permeable_catchment_connect_coef to hydra_catchment_connect_coef
;;

alter table $model._catchment_node
add column hydra_aquifer_infiltration_rate real,
add column hydra_soil_infiltration_type hydra_soil_infiltration_type,
add column hydra_aquifer_infiltration_type hydra_aquifer_infiltration_type
;;

update $model._catchment_node
set
hydra_soil_infiltration_type='split',
hydra_aquifer_infiltration_type='split'
where netflow_type='hydra_permeable'
;;

update $model._catchment_node
set
hydra_soil_infiltration_type='rate',
hydra_aquifer_infiltration_type='rate'
where netflow_type='hydra'
;;

update $model._catchment_node
set netflow_type='hydra'
where netflow_type='hydra_permeable'
;;

update $model._catchment_node
set define_k_mn = 5.07 * area_ha^(0.18) * (slope * 100)^(-0.36) * (1 + c_imp)^(-1.9) * rl^(0.15) * 30^(0.21) * 10^(-0.07)
where runoff_type = 'Desbordes 1 Cr' and slope > 0 and c_imp > 0
;;

update $model._catchment_node
set define_k_mn = 0.14 * (area_ha * rl)^(1./3.) / sqrt(slope),
runoff_type='Define K'
where runoff_type = 'Passini'
;;

update $model._catchment_node
set define_k_mn = 60 * ( 0.4 * sqrt(area_ha) + 0.0015 * rl ) / ( 0.8 * sqrt(slope * rl) ),
runoff_type='Define K'
where runoff_type = 'Giandotti' and slope > 0 and rl > 0
;;

alter table $model.open_parametric_geometry
add column with_flood_plain boolean default False,
add column flood_plain_width real not null default 0,
add column flood_plain_lateral_slope real not null default 0
;;

alter table $model._pipe_link
add column rk_maj real default 0
;;





