from qgis.core import (
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingParameterRasterDestination,
    QgsProcessingParameterCrs,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterRasterLayer,
    QgsProcessingException,
    QgsProcessingParameterBand
)
from pathlib import Path
import re
import os
from osgeo import gdal
from qgis import processing
from multiprocessing import cpu_count


class Blurred(QgsProcessingAlgorithm):
    INPUT = 'INPUT'
    OUTPUT = 'OUTPUT'
    IGNORE_NODATA = 'IGNORE_NODATA'
    BAND = 'BAND'

    def name(self):
        return "blurred"

    def displayName(self):
        return "Blurring raster"

    def group(self):
        return "Raster tools"

    def groupId(self):
        return "rastertools"

    def createInstance(self):
        return Blurred()

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterRasterLayer(self.INPUT, "raster input", QgsProcessing.TypeRaster)
        )
        self.addParameter(
            QgsProcessingParameterBand(self.BAND, "raster band", parentLayerParameterName=self.INPUT, optional=True )
        )
        self.addParameter(
            QgsProcessingParameterRasterDestination(self.OUTPUT, "Output file name (vrt is lighter)")
        )
        self.addParameter(
            QgsProcessingParameterBoolean( self.IGNORE_NODATA, "Remove NODATA tag on output", False )
        )


    def processAlgorithm(self, parameters, context, feedback):
        input_ = self.parameterAsRasterLayer(parameters, self.INPUT, context)
        output_ = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        ignore_nodata = self.parameterAsBoolean(parameters, self.IGNORE_NODATA, context)
        band = self.parameterAsInt(parameters, self.BAND, context)

        feedback.pushInfo(str(input_.source()))

        if not band:
            if hasattr(input_.renderer(), 'band'):
                band = input_.renderer().band() 
            elif hasattr(input_.renderer(), 'inputBand'):
                band = input_.renderer().inputBand()
            else:
                band = 1

        gdal.UseExceptions()
        datasource = gdal.Open(input_.source(), gdal.GA_ReadOnly)
        sdb = datasource.GetRasterBand(band)

        if output_.endswith('.vrt'):
            output_vrt = output_
        else:
            output_vrt = output_+'.vrt'

        with open(output_vrt, "w") as f:
            content = """
            <VRTDataset rasterXSize="{RasterXSize}" rasterYSize="{RasterYSize}">
              <SRS dataAxisToSRSAxisMapping="1,2">{ogc_scr}</SRS>
              <GeoTransform>{geoTransform}</GeoTransform>
              <VRTRasterBand dataType="Float32" band="1">
                <NoDataValue>{nodata}</NoDataValue>
                <KernelFilteredSource>
                    <SourceFilename relativeToVRT="1" shared="0">{input_}</SourceFilename>
                    <SourceBand>{band}</SourceBand>
                    <SourceProperties RasterXSize="{RasterXSize}" RasterYSize="{RasterYSize}" DataType="Float32" BlockXSize="128" BlockYSize="128" />
                    <Kernel normalized="1">
                        <Size>5</Size>
                        <Coefs>0.0036630037 0.014652015 0.025641026 0.014652015 0.0036630037 0.014652015 0.058608059 0.095238095 0.058608059 0.014652015 0.025641026 0.095238095 0.15018315 0.095238095 0.025641026 0.014652015 0.058608059 0.095238095 0.058608059 0.014652015 0.0036630037 0.014652015 0.025641026 0.014652015 0.0036630037 </Coefs>
                    </Kernel>
                </KernelFilteredSource>
              </VRTRasterBand>
            </VRTDataset>
            """.format(
                    input_=Path(os.path.relpath(Path(input_.source()).resolve(), start=Path(output_vrt).resolve().parent)).as_posix(),
                    RasterXSize=datasource.RasterXSize,
                    RasterYSize=datasource.RasterYSize,
                    band=band,
                    ogc_scr=datasource.GetProjectionRef(),
                    geoTransform=', '.join(str(v) for v in datasource.GetGeoTransform()),
                    nodata=sdb.GetNoDataValue() or -9999
                )
            if ignore_nodata:
                content = re.sub(r'<NoDataValue>.*</NoDataValue>', '', content)
            f.write(content)

        if output_.endswith('.vrt'):
            pass
        elif output_.endswith('.tif'):
            processing.run(
                "gdal:translate",
                {
                    "INPUT": output_vrt,
                    "OUTPUT": output_,
                    'EXTRA' : f"-co PREDICTOR=3 -co COMPRESS=DEFLATE -co NUM_THREADS={cpu_count()}" 
                },
                feedback=feedback,
            )
        else:
            processing.run(
                "gdal:translate",
                {
                    "INPUT": output_vrt,
                    "OUTPUT": output_
                },
                feedback=feedback,
            )
            
            

        return {self.OUTPUT: output_vrt}
