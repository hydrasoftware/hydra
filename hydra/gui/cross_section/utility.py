# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from numpy import array, dot
from numpy.linalg import norm
from shapely.geometry import Polygon, LineString, Point, MultiPoint, MultiLineString, GeometryCollection, MultiPolygon
from shapely.errors import TopologicalError

from scipy.optimize import least_squares, minimize


def hydraulic_profile(points, zlim, xlim=None):
    minz = min(points[:,1]) - .1
    maxz = min(zlim[1] , max(points[:,1]) + .1)
    dz = (maxz - minz)/50
    hp = []
    z = minz
    if len(points) <= 3:
        return None

    if tuple(points[-1]) == tuple(points[0]): # closed profile
        ring = [list(p) for i, p in enumerate(points) if i==0 or norm(points[i-1]-p)>1e-6]
        if len(ring) < 3:
            return None
        section = Polygon(ring)
    else:
        ring = [[points[0][0], 9999]]+[list(p) for i, p in enumerate(points) if i==0 or norm(points[i-1]-p)>1e-6]+[[points[-1][0], 9999]]
        if len(ring) < 4:
            return None
        section = Polygon(ring)

    if not section.is_valid:
        return None
    if xlim is not None:
        section = section.intersection(Polygon([[xlim[0], -9999], [xlim[1], -9999], [xlim[1], 9999], [xlim[0], 9999]]))

    while z < zlim[1] and z < maxz :

        z += dz
        water = Polygon([
            [-9999, minz],
            [ 9999, minz],
            [ 9999, z],
            [-9999, z]])
        try:
            s = section.intersection(water)
            if s.geom_type in ('Polygon', 'MultiPolygon'):
                S = s.area
                if S > 0:
                    P = sum([p.exterior.difference(LineString([[-9999, z],[9999, z]])).length
                            for p in ([s] if s.geom_type == 'Polygon' else s.geoms)])
                    hp.append([S**(5/3)*P**(-2/3), z])
                    #hp.append([P, z])

        except TopologicalError as e:
            continue
    return array(hp) if len(hp) else None


def first_crossing(line, s, is_reversed=False, horizontal=False):
    """returns the point where the line first crosses
    the vertical defines by s (abscisse)"""
    xmin, ymin, xmax, ymax = line.bounds
    i = line.intersection(LineString([[s, ymin-1], [s, ymax+1]]) if not horizontal else LineString([[xmin, s], [xmax, s]]))
    #i = line.intersection(Polygon([[s-.1, ymin], [s-.1, ymax], [s+.1, ymax], [s+.1, ymin]]) if not horizontal else LineString([[xmin, s], [xmax, s]]))
    p = []
    if isinstance(i, GeometryCollection) or isinstance(i, MultiLineString)  or isinstance(i, MultiPoint):
        for g in i:
            p += list(g.coords)
    else:
        p += list(i.coords)
    p = sorted(p, key= lambda x: line.project(Point(x)))
    return Point(p[-1] if is_reversed else p[0])


def trim_or_extend(line, s_start, s_end):
    """trims or extend linestring such that its extremities
    are at the given coordinates, line must be oriented left to right
    """
    if line.coords[0][0] > s_start: # extend
        line = LineString([[s_start, line.coords[0][1]]] + list(line.coords))
    else:
        line = substring(line, line.project(first_crossing(line, s_start)), line.length)

    if line.coords[-1][0] < s_end: # extend
        line = LineString(list(line.coords) + [[s_end, line.coords[0][1]]])
    else:
        line = substring(line, 0, line.project(first_crossing(line, s_end, is_reversed=True)))
    return line

def sz_from_zb(left, minor, right, z_offset=0, s_offset=0, center_coef=0.5):
    pt = []
    s = left[-1][1] if len(left) else 0
    for z, b in reversed(left):
        pt.append([s - b, z])
    s += minor[-1][1]*center_coef
    for z, b in reversed(minor):
        pt.append([s - b*center_coef, z])
    for z, b in minor:
        pt.append([s + b*(1-center_coef), z])
    s += minor[-1][1]*(1-center_coef)
    for z, b in right:
        pt.append([s + b, z])
    return array(pt) + array([s_offset, z_offset])

def simplify_major(line, nbpt, is_ascending):
    """simplify a line to best fit zb,
    b must be strictly ascending,
    end points are preserved,
    line must be oriented left to right.
    """
    assert(nbpt >= 2)
    xmin, zmin, xmax, zmax = line.coords[0][0], min(line.coords[0][1], line.coords[-1][1]), line.coords[-1][0], max(line.coords[0][1], line.coords[-1][1])

    if xmin == xmax or len(line.coords) == 2:
        return LineString([line.coords[0], line.coords[-1]])

    is_increasing = (line.coords[0][1] < line.coords[-1][1])

    if is_increasing and not is_ascending:
        return LineString([[line.coords[0][0], line.coords[-1][1]], list(line.coords[-1])])
    if not is_increasing and is_ascending:
        return LineString([list(line.coords[0]), [line.coords[-1][0], line.coords[0][1]]])

    # detect vertical lines on both ends to avoid invalid polygons
    s = 0
    e = len(line.coords) - 1
    while s < e and line.coords[s][0] == line.coords[s+1][0]:
        s += 1
    while s < e and line.coords[e][0] == line.coords[e-1][0]:
        e -= 1

    # create polygon
    ztop = line.bounds[3]
    poly = Polygon([[line.coords[s][0], ztop+1]] + list(line.coords[s:e+1]) + [[line.coords[e][0], ztop+1]])
    if not poly.is_valid:
        return LineString([line.coords[0], line.coords[-1]])
    pt = array(line.coords)
    zb = [[zmin, 0]]
    for z in pt[pt[:,1].argsort(), 1]:
        if z >= zmin and z <= zmax:
            it = poly.intersection(LineString([[xmin, z],[xmax, z]]))
            b = 0 if it.is_empty else it.length
            zb.append([z, b])
            if z == zb[-1][0]: # repeated point in z, we check for horizontal piece
                bm = line.intersection(LineString([[xmin, z],[xmax, z]])).length
                if bm > 0 and zb[-1][1] != b - bm:
                    zb.append([zb[-1][0], zb[-1][1]-bm])
    # force zb[-1] to full width
    zb[-1][1] = xmax - xmin
    zb_fit = LineString([zb[0], zb[-1]])
    for p in range(nbpt-2):
        d = array([zb_fit.distance(Point(z, b)) for z, b in zb])
        i = d.argmax()
        if d[i] == 0:
            break
        l = [tuple(zb[i])] + list(zb_fit.coords)
        zb_fit = LineString(sorted(l))
    zb_fit = list(zb_fit.coords)

    # b must be strictly increasing
    # and filter out duplicates
    zb = [zb_fit[0]]
    for i in range(1, len(zb_fit)):
        p = (zb_fit[i][0], max(zb[-1][1], zb_fit[i][1]))
        if p != zb[-1]:
            zb.append(p)

    if is_increasing:
        return LineString([(line.coords[0][0] + b, z) for z, b in zb]).simplify(0)
    else:
        return LineString([(line.coords[-1][0] - b, z) for z, b in reversed(zb)]).simplify(0)

def is_stupid(left, minor, right, sz):
    """returns true if sz can be obtained by
    offsetting the symetric profile defined by left, minor and right"""
    if minor is None or sz is None:
        return False
    sz_from_zb_ = sz_from_zb(left, minor, right)
    if len(sz_from_zb_) != len(sz):
        return False
    sz_from_zb_[:,0] += sz[0][0]
    # checks for floodplains fit, they must fit exactly
    for i in list(range(len(left))) + list(range(len(left) + 2*len(minor), len(left) + 2*len(minor) + len(right))):
        if abs(sz_from_zb_[i,0] - sz[i][0]) > 1e-2 or abs(sz_from_zb_[i,1] - sz[i][1]) > 1e-2:
            return False
    # checks for riverbed fit, (z,b) must fit
    for i in range(len(minor)):
        if abs(sz_from_zb_[i + len(left), 1] - sz[i + len(left)][1]) > 1e-2 \
                or abs(minor[-1-i][1] - (sz[len(left) + 2*len(minor) - 1 - i][0] - sz[i + len(left)][0])) > 1e-2:
            return False
    return True

def project_topo(terrain, transect, topo_points, transect_topo, is_centered=False):
    if transect is None:
        return None, None, None

    # for topo, doubles first and last line segments
    coords_topo = array(transect.coords)[:,:2]
    offset = norm(coords_topo[0] - coords_topo[1]) if not is_centered else norm(coords_topo[0] - coords_topo[1])*1.5
    coords_topo[0], coords_topo[-1]  = coords_topo[0] + coords_topo[0] - coords_topo[1], coords_topo[-1] + coords_topo[-1] - coords_topo[-2]
    topo = terrain.line_elevation(coords_topo)
    # get the two geom points enclosing intersection with riverbed
    if len(topo.shape)>1 and topo.shape[1] > 2:
        tp = [[-offset, topo[0,2]]]
        for s, e in zip(topo[:-1], topo[1:]):
            tp.append([tp[-1][0] + norm(e[:2] - s[:2]), e[2]])
    else:
        tp = []
    dem = tp
    pt = []

    projector = LineString(coords_topo)
    if transect_topo is not None:
        pt = [[projector.project(Point(p)) - offset, p[2]] for p in transect_topo.coords]
    elif topo_points is not None and len(topo_points.geoms):
        pt =  [p for p in sorted([(projector.project(p) - offset, p.coords[0][2]) for p in topo_points.geoms]) if p[0] > 0 and p[0]  < projector.length]

    if len(pt):
        xl, xr = min([s for s, z in pt]), max([s for s, z in pt])
        tp = [p for p in tp if p[0] < xl] + [[xl, pt[0][1]]] + pt + [[xr, pt[-1][1]]]+ [p for p in tp if p[0] > xr]

    return array(tp) if len(tp) else None , array(pt) if len(pt) else None, array(dem) if len(dem) else None

def snap(pos, dx, dy, target):
    t = pos
    if target is not None:
        d = float('inf')
        for i, pt in enumerate(target):
            dp = pos-pt
            dp2 = dot(dp, dp)
            if abs(dp.x()) < abs(dx) and abs(dp.y()) < abs(dy) and dp2 < d:
                d = dp2
                t = pt
    return t

def left_of(geom):
    "left coordinate of the leftmost line in geom"
    #print("left_of", geom)
    if geom.geom_type in ('LineString', 'Point'):
        return geom.bounds[0]
    else:
        return max([(g.length, g.bounds[0]) for g in geom.geoms if g.geom_type == 'LineString'])[1]


def simplified(left, minor, right):
    coords = array(minor.coords)
    xmin, xbottom, xmax = coords[0, 0], coords[coords[:,1].argmin(), 0], coords[-1, 0]
    assert(xmin < xmax)
    zmin, zlow, zmax = min(coords[:,1]), min(coords[0, 1], coords[-1, 1]), max(coords[0, 1], coords[-1, 1])

    # flattens coordinates
    i_zmin = coords[:,1].argmin()
    for i in range(i_zmin):
        coords[i, 1] = min(coords[0, 1], coords[i, 1])
    for i in range(-1, i_zmin-len(coords), -1):
        coords[i, 1] = min(coords[-1, 1], coords[i, 1])

    minor = LineString(coords).simplify(0) # simplify fixes invalidity causes by flattened overhangs

    # simplify the profile to have ascending zb
    poly = Polygon([[minor.coords[0][0], 9999]] + list(minor.coords) + [[minor.coords[-1][0], 9999]])

    if not poly.is_valid:
        return [[[coords[0,1], 0]], [[zmin, coords[-1,0] - coords[0,0]]], [[coords[-1,1], 0]]], array([coords[0], coords[0], coords[-1], coords[-1]])

    szb = [[coords[0, 0], zmax, coords[-1, 0] - coords[0, 0]]]
    for z in coords[coords[:,1].argsort()[::-1], 1]:
        if z <= zlow and szb[-1][1] != z:
            i = poly.intersection(LineString([[xmin, z],[xmax, z]]))
            b = i.length
            if b == 0:
                continue
            x = left_of(i)
            if x + b > szb[-1][0]+szb[-1][2]:
                x = szb[-1][0] + szb[-1][2] - b
            if x < szb[-1][0]:
                x = szb[-1][0]
            szb.append([x, z, min(b, szb[-1][2])])  # + 0.5*max(b - szb[-1][2], 0) #+ .5*(i.bounds[2] - i.bounds[0] - min(b, szb[-1][2]))
            im = minor.intersection(LineString([[xmin, z],[xmax, z]]))
            bm = b - im.length
            if bm > 0:
                xm = left_of(i.difference(im))
                if xm + b > szb[-1][0]+szb[-1][2]:
                    xm = szb[-1][0]+szb[-1][2] - bm
                if xm < szb[-1][0]:
                    xm = szb[-1][0]
                szb.append([xm, z, min(bm, szb[-1][2])]) #+ 0.5*max(bm - szb[-1][2] , 0) #  + .5*(im.bounds[2] - im.bounds[0] - min(bm, szb[-1][2]))

    simplified = array([coords[0].tolist()] + [[s, z] for s, z, b in szb if z <= coords[0,1]] + [[s + b, z] for s, z, b in reversed(szb)  if z <= coords[-1,1]] + [coords[-1].tolist()])

    # remove first szb
    szb = szb[2:]#[[s, z, b] for s, z, b in szb if z < zlow]

    if len(szb) == 0:
        return [[[coords[0,1], 0]], [[zmin, coords[-1,0] - coords[0,0]]], [[coords[-1,1], 0]]], array([coords[0], coords[0], coords[-1], coords[-1]])

    sz_fit = LineString([simplified[0], simplified[-1]])
    ct = 0
    minor = []
    idx = []
    for p in range(6):
        dl = array([sz_fit.distance(Point(s, z)) for s, z, b in szb])
        dr = array([sz_fit.distance(Point(s+b, z)) for s, z, b in szb])
        i, j = dl.argmax(), dr.argmax()
        if dl[i] < 0.01 and dr[j] < 0.01:
            break
        if dl[i] > dr[j]:
            minor.append((szb[i][1], szb[i][2]))
            idx.append(i)
        else:
            minor.append((szb[j][1], szb[j][2]))
            idx.append(j)

        sz_fit = LineString(
            [coords[0].tolist()]
            + [[szb[k][0], szb[k][1]] for k in sorted(idx)]
            + [[szb[k][0] + szb[k][2], szb[k][1]] for k in sorted(idx, reverse=True)]
            + [coords[-1].tolist()])
        ct += 1

    simplified = array([[s, z] for s, z in sz_fit.coords])

    m = [list(zb) for zb in sorted(minor)]
    l = [[simplified[0, 1], simplified[1, 0] - simplified[0, 0]]]
    r = [[simplified[-1, 1], simplified[-1, 0] - simplified[-2, 0]]]

    sl = simplify_major(left, 8, is_ascending=False)
    sr = simplify_major(right, 8, is_ascending=True)


    if sl.length > 1e-6:
        sl = list(sl.coords)
        l += [[z, sz_fit.coords[1][0] - s] for s, z in reversed(sl[:-1])]
    else:
        sl = []

    if sr.length > 1e-6:
        sr = list(sr.coords)
        r += [[z, s - sz_fit.coords[-2][0]] for s, z in sr[1:]]
    else:
        sr = []

    sm = array([[s, z] for s, z in sz_fit.coords])
    simplified = array(sl[:-1] + [[s, z] for s, z in sz_fit.coords] + sr[1:])
    assert(len(simplified) == len(l) + 2*len(m) + len(r))
    assert(is_stupid(l, m, r, sz_from_zb(l, m, r)))
    return [l, m, r], simplified
