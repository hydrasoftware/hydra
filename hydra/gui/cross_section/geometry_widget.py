# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from .cross_section_canvas import CrossSectionCanvas
from .minimap import MiniMap
from qgis.PyQt.QtWidgets import QWidget, QHBoxLayout, QLabel, QVBoxLayout, QSpacerItem, QSizePolicy
from qgis.PyQt.QtCore import pyqtSignal
from .valley_geometry import ValleyGeometry
from .zb_arrays_widget import ZbArraysWidget

class GeometryWidget(QWidget):
    geometry_changed = pyqtSignal(dict)
    transect_changed = pyqtSignal(list)

    def __init__(self, parent=None, iface=None):
        super().__init__(parent)
        self.__layout = QHBoxLayout(self)
        self.__layout.setContentsMargins(0,0,0,0)
        self.__canvas = CrossSectionCanvas()
        self.__zb_widget = None
        self.__vlayout = QVBoxLayout()
        self.__vlayout.addWidget(self.__canvas)
        self.__layout.addLayout(self.__vlayout)
        self.__right_layout = QVBoxLayout()
        self.__layout.addLayout(self.__right_layout)

        w = QWidget()
        w.setMaximumWidth(270)
        self.__right_layout.addWidget(w)
        self.__minimap = None

        self.__map_settings = None if iface is None else iface.mapCanvas().mapSettings()

    def set_geometry(self, geometry):
        while self.__right_layout.count():
            w = self.__right_layout.takeAt(0)
            if isinstance(w.widget(), QWidget):
                w.widget().setParent(None)
        self.__minimap = None

        if self.__zb_widget is not None:
            self.__zb_widget.setParent(None)
            self.__zb_widget = None

        if geometry is not None:
            w = geometry
            if hasattr(geometry, 'set_zb_widget'):
                self.__zb_widget = ZbArraysWidget()
                self.__vlayout.addWidget(self.__zb_widget)
                geometry.set_zb_widget(self.__zb_widget)
            geometry.transect_changed.connect(self.__transect_changed)
            geometry.geometry_changed.connect(self.__geometry_changed)

            # self.geometry_changed.emit(geometry.param)
            # if geometry.transect is not None:
            #     self.transect_changed.emit(geometry.transect)

        else:
            w = QWidget()

        w.setMaximumWidth(270)
        self.__right_layout.addWidget(w)
        self.__right_layout.addItem(QSpacerItem(1,1,vPolicy=QSizePolicy.Expanding))

        if isinstance(w, ValleyGeometry):
            self.__minimap = MiniMap(self.__map_settings)
            self.__minimap.setMinimumHeight(270)
            self.__minimap.setMaximumHeight(270)
            self.__minimap.setMaximumWidth(270)
            self.__right_layout.addWidget(self.__minimap)
            self.__minimap.set_transect(w.transect)
            w.transect_changed.connect(self.__minimap.set_transect)

        self.__canvas.set_profile(geometry)
        #self.__canvas.draw()

    def __geometry_changed(self, param):
        self.geometry_changed.emit(param)
        self.__canvas.draw()

    def __transect_changed(self, l):
        self.transect_changed.emit(l)
        self.__canvas.draw()



