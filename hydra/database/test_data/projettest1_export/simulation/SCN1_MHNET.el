$ Element EORIF
   25         1     1
    1         3        68    'Vanne1           '
   54.550    55.550     0.500     0.600   -1.000   1    2    55.550     0.200   0

$ Element ESMK
   32        12     1
    1         8        69    'borda1           '
    1     0       headloss type: q
  1.500000         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0
    2         9        70    'borda2           '
    2     0       headloss type: v
           1  0.700000  0.600000         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0
    3        12        71    'borda5           '
    9     0       headloss type: sharp_transition_geometry
           2  1.510000         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0
    4        13        72    'borda6           '
   10     0       headloss type: contraction_with_transition
           1  15.000000         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0
    5        14        73    'borda7           '
   11     0       headloss type: enlargment_with_transition
           4  45.000000         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0
    6        15        74    'borda8           '
   12     0       headloss type: circular_bend
  12.000000  10.000000           2         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0
    7        16        75    'borda9           '
   13     0       headloss type: sharp_angle_bend
  35.000000           2  15.500000           1         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0
    8        17        76    'borda10          '
   14     0       headloss type: screen_normal_to_flow
  0.500000  30.000000           1           1         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0
    9        18        77    'borda11          '
   15     0       headloss type: screen_oblique_to_flow
  0.330000  32.000000           1         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0
   10        19        78    'borda12          '
   16     0       headloss type: friction
  111.000000  25.000000         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0
   11        20        79    'borda13          '
   17     0       headloss type: parametric
       0.000       0.000       0.100       0.060       0.200       0.300         0.0         0.0         0.0         0.0         0.0         0.0
   12        21        80    'borda14          '
   18     0       headloss type: sharp_bend_rectangular
           2  0.230000  0.330000         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0         0.0

$ Element EACTA
   31         1     1
    1         2        81    'Acta1            '
    56.550    57.450     0.800     0.600   -1.000   2    56.550    57.450     2.000    0.250    1    0
         5    10.000    11.100    12.200     3
     0.000   56.6000
     1.500   56.6500
     2.200   56.7000

