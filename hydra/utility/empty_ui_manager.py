# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
EmptyUIManager is an empty ui manager used for testing purposes
"""

class EmptyUIManager(object):
    def __init__(self):
        pass
    def new_project(self):
        pass
    def save_project(self, project_filename):
        pass
    def open_project(self, project_filename):
        pass
    def add_work_layer(self, table_name, layer_name, project_name, column_id, column_geom):
        pass
    def add_vector_layers(self, project_name, model_name, json_file, srid):
        pass
    def add_terrain_layers(self, project_name, files):
        pass
    def add_rain_layers(self, vrt_rains):
        pass
    def add_result_layers(self, project_path, project_name, model_name, scn_name, layer_names):
        pass
    def add_raster_layer(self, raster_file):
        pass
    def remove_model_layers(self, model_name):
        pass
    def remove_group_layers(self, model_name):
        pass
    def set_model_layers_expanded(self, model_name, expanded):
        pass
    def error(self):
        pass
    def cleanup(self):
        pass
    def group_project_exists(self):
        return True
    def layer_group_exists(self, group_name='project'):
        return False

