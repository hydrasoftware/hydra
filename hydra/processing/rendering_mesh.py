from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterString,
    QgsProcessingException
)
from ..project import Project
from .utility import ProjectWidget, ModelWidget
from shapely import wkb
from collections import defaultdict

# maillage de chaque domaine avec connectivité et pondération
# - street: pas de contraintes internes, comme on a plusieurs rues et que l'interpolation n'est pas simplement par proximité, on intègre les street_link aux contraintes de maillage, on peut ensuite interpoler la valeur d'un noeuds du bord sur les link connecté
#  IL Y A plus malin à faire: pas besoin des carefours dans le maillage, ni des links, on peut récupéré les links intersecctés par les triangles
# - 2d: il faut soustraire aux contraintes tous les anneaux du polygones, il faut post-traiter avec le plugin Crack pour traiter les discontinuités. Attention: les constraintes null et connector sont prises en compte pour le maillage, mais ne créent pas de discontinuités
# - reach: TODO
# - storage: juste un polygone, pas de contraintes, pas de noeud de casier, tous les noeuds ont la même valeur


# offset des numeroo de noeud pour assembler le maillage
# lissage sur les frontière de type null et connector

class RenderingMesh(QgsProcessingAlgorithm):
    """
    Generate rendering mesh for post-processing
    """

    PROJECT = "project"
    MODEL = "model"
    SKIP_MAJOR = "skip_major"

    def tr(self, string):
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return RenderingMesh()

    def name(self):
        return "rendering_mesh"

    def displayName(self):
        return self.tr("Rendering mesh")

    def group(self):
        return self.tr("River and free surface flow mapping")

    def groupId(self):
        return "surfaceflowmapping"

    def shortHelpString(self):
        return self.tr("Creates model rendering mesh (triangulation) for post-processing.")

    def initAlgorithm(self, config=None):

        param = QgsProcessingParameterString(self.PROJECT, "Project")
        param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.MODEL, "Model")
        param.setMetadata({ 'widget_wrapper': {'class': ModelWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterBoolean(self.SKIP_MAJOR, "Skip flood plain triangulation", defaultValue=True)
        self.addParameter(param)

    def processAlgorithm(self, parameters, context, feedback):
        feedback.pushInfo('meshing')
        project = Project(self.parameterAsString(parameters, self.PROJECT, context))
        model = self.parameterAsString(parameters, self.MODEL, context)
        skip_major = self.parameterAsBoolean(parameters, self.SKIP_MAJOR, context)

        project.execute(f"""
        select {model}.coverage_update();
        """)

        if not skip_major:
            project.execute(f"""
            refresh materialized view {model}.riverbank;
            refresh materialized view {model}.flowline;
            """)

        project.execute(f"""

        drop table if exists {model}.vertex cascade;
        drop table if exists {model}.triangle cascade;
        drop table if exists {model}.tmp_reach cascade;
        drop table if exists {model}.tmp_street cascade;
        drop table if exists {model}.tmp_crossroad cascade;
        drop table if exists {model}.tmp_simple_street cascade;
        drop table if exists {model}.tmp_triangle_street cascade;

        create table {model}.vertex(
            id integer,
            name varchar(24),
            neighbors integer[],
            interpolation_nodes varchar[],
            elevation_columns integer[], -- 0 based index in w14
            elevation_weights float[],
            velocity_u_columns integer[], -- 0 based index in w14
            velocity_u_weights float[],
            velocity_v_columns integer[], -- 0 based index in w14
            velocity_v_weights float[],
            coverage integer,
            domain_type hydra_coverage_type,
            altitude float,
            lowest_altitude float,
            geom geometry(POINT, {project.srid})
            )
        ;
        create table {model}.triangle(
            id integer,
            a integer,
            b integer,
            c integer,
            coverage integer,
            geom geometry(POLYGON, {project.srid})
            )
        ;
        """)
        project.commit()
        
        errors = project.fetchall(f"""
            with minor as (
                select
                    coalesce(st_difference(c.geom, st_collect(r.major)), c.geom) as polygon,
                    null::geometry as constrains,
                    null::geometry as discontinuities,
                    null::geometry as points,
                    null::varchar[] as names,
                    c.domain_type,
                    c.id
                from {model}.coverage c
                left join {model}.riverbank r on r.coverage = c.id and {'true' if not skip_major else 'false'}
                where c.domain_type = 'reach'
                group by c.id, c.geom, c.domain_type
            )
            select st_astext(polygon) from minor where st_geometrytype(polygon) != 'ST_Polygon'

        """)
        if len(errors):
            raise QgsProcessingException("toppological error in riverbed "+errors[0][0])

        meshables = project.fetchall(f"""
            with renderable as (
            select
                c.geom as polygon,
                null::geometry as constrains,
                null::geometry as discontinuities,
                null::geometry as points,
                null::varchar[] as names,
                c.domain_type,
                c.id
            from {model}.coverage c
            where c.domain_type in ('street', 'storage')

            union all

            select
                coalesce(st_difference(c.geom, st_collect(r.major)), c.geom) as polygon,
                null::geometry as constrains,
                null::geometry as discontinuities,
                null::geometry as points,
                null::varchar[] as names,
                c.domain_type,
                c.id
            from {model}.coverage c
            left join {model}.riverbank r on r.coverage = c.id and {'true' if not skip_major else 'false'}
            where c.domain_type = 'reach'
            group by c.id, c.geom, c.domain_type

            union all

            select
                r.major as polygon,
                st_collect(f.geom) as constrains,
                null::geometry as discontinuities,
                --st_collect(s.geom) as points,
                null::geometry as points,
                null::varchar[] as names,
                'reach' as domain_type,
                r.coverage as id
            from {model}.riverbank r
            left join {model}.flowline f on f.bank=r.id
            where {'true' if not skip_major else 'false'}
            group by r.major, r.coverage

            union all

            select
                p.geom as polygon,
                st_multi(st_difference(ct.geom, rg.geom)) as constraints,
                st_multi(st_difference(ds.geom, rg.geom)) as discontinuities,
                n.geom as nodes,
                n.names,
                p.domain_type,
                p.id as coverage
            from {model}.coverage p
            cross join lateral (
                select st_union(geom) as geom from (
                    select st_intersection(discretized, p.geom) as geom
                    from {model}.constrain c
                    where st_intersects(p.geom, c.discretized)
                    and (c.constrain_type is null or c.constrain_type = 'connector')
                    --union all
                    --select st_intersection(cb.geom, p.geom) as geom
                    --from {model}.channel_banks cb
                    --where st_intersects(p.geom, cb.geom)
                    ) t
                ) ct
            cross join lateral (
                select st_union(st_intersection(discretized, p.geom)) as geom from {model}.constrain c where st_intersects(p.geom, c.discretized) and c.constrain_type in ('overflow', 'porous', 'strickler', 'boundary')
                ) ds
            cross join lateral (
                select st_collect(st_exteriorring(r.geom)) as geom from st_dumprings(p.geom) r
                ) as rg
            cross join lateral (
                select st_collect(n.geom order by n.name) geom, array_agg(n.name order by n.name) as names
                from {model}._node n
                where st_intersects(p.geom, n.geom)
                and n.node_type='elem_2d') n
            where domain_type='2d'
            )
            select p.* from renderable p
            """)


        class empty:
            def __init__(self):
                self.geoms = []

        P = len(meshables)
        p = 0
        npoin = 0
        ntri = 0
        for polygon, constrains, discontinuities, points, names, domain_type, coverage in meshables:
            if feedback.isCanceled():
                raise QgsProcessingException('cancelled')

            p += 1
            feedback.setProgress(100*p/P)

            node_data = []
            triangle_data = []
            vertex, face = project.execute("""
                select t.vertex, t.face
                from project.tesselate(%s, %s, %s, %s, has_quads=>false) t""",
                (polygon, constrains, discontinuities, points)).fetchone()
            vertex = wkb.loads(vertex, True)

            names = names or []
            names = [None]*(len(vertex.geoms)-len(names)) + names

            neighbors = defaultdict(set)
            for t in face:
                ntri += 1
                triangle_data.append(
                    [ntri, t[0]+npoin, t[1]+npoin, t[2]+npoin, coverage] + [vertex.geoms[i-1].coords[0][j] for i in t for j in (0,1)])
                for i in range(3):
                    if names[t[i]-1] is None:
                        neighbors[t[i]+npoin] |= ({t[(i+1)%3]+npoin} if names[t[(i+1)%3] - 1] is not None else set()) | ({t[(i+2)%3]+npoin} if names[t[(i+2)%3] - 1] is not None else set())

            for v, name in zip(vertex.geoms, names):
                npoin += 1
                node_data.append(
                    [npoin, name, list(neighbors[npoin]), coverage, domain_type, v.coords[0][0], v.coords[0][1]])

            project.executemany(f"""
            insert into {model}.vertex(id, name, neighbors, coverage, domain_type, geom) values (%s, %s, %s, %s, %s, st_setsrid(st_makepoint(%s, %s), {project.srid}))
            ;
            """, node_data)

            project.executemany(f"""
            insert into {model}.triangle(id, a, b, c, coverage, geom)
            values (%s, %s, %s, %s, %s, st_setsrid(st_makepolygon(st_makeline(ARRAY[st_makepoint(%s,%s), st_makepoint(%s,%s), st_makepoint(%s,%s), st_makepoint(%s,%s)])), {project.srid}))
            """, triangle_data)

            project.commit()

        feedback.pushInfo('indexing')
        project.execute(f"""
        alter table {model}.vertex add constraint node_pkey primary key(id);
        alter table {model}.triangle add constraint triangle_pkey primary key(id);
        create index node_domain_type_idx on {model}.vertex(domain_type);
        create index node_domain_id_idx on {model}.vertex(coverage);
        create index triangle_domain_id_idx on {model}.triangle(coverage);
        create index node_geom_idx on {model}.vertex using gist(geom);
        create index triangle_geom_idx on {model}.triangle using gist(geom);
        """)

        project.commit()

        feedback.pushInfo('street interpolation')

        project.execute(f"""
        create table {model}.tmp_street as select id, up, down, geom from {model}.street_link;
        create index tmp_street_geom_idx on {model}.tmp_street using gist(geom);

        create table {model}.tmp_crossroad as
        with m as (
            select t.id, t.geom, st_linemerge(st_collect(st_intersection(s.geom, t.geom))) sgeom
            from {model}.triangle t
            join {model}.tmp_street s on st_intersects(t.geom, s.geom)
            group by t.id
        )
        select row_number() over() as id, r.geom
        from (select st_union(m.geom) as geom from m where st_numgeometries(sgeom) > 1) t
        cross join lateral st_dump(t.geom) as r
        ;

        create index tmp_crossroad_geom_idx on {model}.tmp_crossroad using gist(geom);

        create table {model}.tmp_simple_street as
        with ass as (
            select st_union(t.geom) as geom
            from {model}.triangle t
			join {model}.coverage cc on cc.id=t.coverage
            where t.id not in (select tt.id from {model}.triangle tt join {model}.tmp_crossroad c on st_covers(c.geom, tt.geom))
			and cc.domain_type='street'
        )
        select row_number() over() as id, d.geom from ass cross join lateral st_dump(ass.geom) d
        ;

        create table {model}.tmp_triangle_street as
        select t.id, t.a, t.b, t.c, st_linemerge(st_collect(s.geom)) geom, array_agg(s.id) as segments
        from {model}.triangle t
        join {model}.tmp_street s on st_intersects(t.geom, s.geom)
        group by t.id, t.a, t.b, t.c
        ;

        create index tmp_triangle_street_a_idx on {model}.tmp_triangle_street(a);
        create index tmp_triangle_street_b_idx on {model}.tmp_triangle_street(b);
        create index tmp_triangle_street_c_idx on {model}.tmp_triangle_street(c);

        with loc as (
            select v.id, st_linelocatepoint(st_makeline(up.geom, down.geom), v.geom) as alpha, up.name as up_name, down.name as down_name
            from {model}.vertex v
            cross join lateral (
                select ss.id, ss.up, ss.down
                from {model}.tmp_triangle_street m
                join {model}.tmp_street ss on ss.id=ANY(m.segments)
                where v.id in (m.a, m.b, m.c) and st_numgeometries(m.geom) = 1 order by st_distance(ss.geom, v.geom) limit 1
                ) s
            join {model}._node up on up.id=s.up
            join {model}._node down on down.id=s.down
            where v.domain_type='street'
        ),
        interp as (
            select id,
                array_agg(name) as interpolation_nodes ,
                array_agg(w) as elevation_weights,
                array_agg(1) as elevation_columns,
                array_agg(w) as velocity_u_weights,
                array_agg(2) as velocity_u_columns,
                array_agg(w) as velocity_v_weights,
                array_agg(3) as velocity_v_columns
            from (
                select id, alpha as w, up_name as name from loc
                union all
                select id, (1-alpha) as w, down_name as name from loc
                ) t
            group by id
        )
        update {model}.vertex v set
            interpolation_nodes=interp.interpolation_nodes,
            elevation_weights=interp.elevation_weights,
            elevation_columns=interp.elevation_columns,
            velocity_u_weights=interp.velocity_u_weights,
            velocity_u_columns=interp.velocity_u_columns,
            velocity_v_weights=interp.velocity_v_weights,
            velocity_v_columns=interp.velocity_v_columns
        from interp where interp.id=v.id
        ;

        -- crossroads nodes are simmply averaged
        with u as (
            select v.id, array_agg(n.name) as interpolation_nodes
            from {model}.vertex v
            join {model}.tmp_crossroad c on st_intersects(v.geom, c.geom)
            join {model}.crossroad_node n on st_intersects(c.geom, n.geom)
            where v.domain_type = 'street'
            group by v.id
        )
        update {model}.vertex v set
            interpolation_nodes=u.interpolation_nodes,
            elevation_weights=array_fill(1./array_length(u.interpolation_nodes,1), ARRAY[array_length(u.interpolation_nodes,1)]),
            elevation_columns=array_fill(1, ARRAY[array_length(u.interpolation_nodes,1)]),
            velocity_u_weights=array_fill(1./array_length(u.interpolation_nodes,1), ARRAY[array_length(u.interpolation_nodes,1)]),
            velocity_u_columns=array_fill(2, ARRAY[array_length(u.interpolation_nodes,1)]),
            velocity_v_weights=array_fill(1./array_length(u.interpolation_nodes,1), ARRAY[array_length(u.interpolation_nodes,1)]),
            velocity_v_columns=array_fill(3, ARRAY[array_length(u.interpolation_nodes,1)])
        from u where u.id = v.id
        ;

        -- isolated point at the end of the street
        with upt as (
            select v.id, nn.name
            from {model}.vertex v
            join {model}.tmp_simple_street s on st_intersects(v.geom, s.geom)
            cross join lateral (
                select n.name
                from {model}.crossroad_node n
                where st_intersects(s.geom, n.geom)
                order by v.geom <-> n.geom limit 1
            ) nn
            where v.domain_type = 'street'
            and v.interpolation_nodes is null
        )
        update {model}.vertex v set
            interpolation_nodes=ARRAY[upt.name],
            elevation_weights=ARRAY[1],
            elevation_columns=ARRAY[1],
            velocity_u_weights=ARRAY[1],
            velocity_u_columns=ARRAY[2],
            velocity_v_weights=ARRAY[1],
            velocity_v_columns=ARRAY[3]
        from upt where upt.id=v.id
        ;

        update {model}.vertex v set
            altitude=(select sum(c.z_ground*v.elevation_weights[i])
                from generate_series(1, array_length(v.interpolation_nodes, 1)) i
                join {model}.crossroad_node c on c.name = v.interpolation_nodes[i])
        where v.domain_type = 'street'
        ;
        """)

        feedback.pushInfo('elem_2d altitude interpolation')

        # les noeuds nommé on des poids définis
        project.execute(f"""
        update {model}.vertex set
            interpolation_nodes=ARRAY[name],
            elevation_weights=ARRAY[1],
            elevation_columns=ARRAY[1],
            velocity_u_weights=ARRAY[1],
            velocity_u_columns=ARRAY[2],
            velocity_v_weights=ARRAY[1],
            velocity_v_columns=ARRAY[3]
        where name is not null
        and domain_type='2d' -- normalement c'est inutile, mais au cas où
        ;

        update {model}.vertex v set altitude=n.zb
        from {model}.elem_2d_node n
        where v.name = n.name
        ;

        update {model}.vertex v set lowest_altitude=(
            select min(l.z_invert)
            from {model}.elem_2d_node e
            join {model}.mesh_2d_link l on l.up = e.id or l.down = e.id
            where e.name = v.name
            group by e.id
            )
        ;
        """)

        # les noeuds des casiers on tous le nom du noeud casier
        feedback.pushInfo('storage interpolation')
        project.execute(f"""
        update {model}.vertex n set
            name=s.name,
            interpolation_nodes=ARRAY[s.name],
            elevation_weights=ARRAY[1],
            elevation_columns=ARRAY[1],
            velocity_u_weights=ARRAY[0],
            velocity_u_columns=ARRAY[0],
            velocity_v_weights=ARRAY[0],
            velocity_v_columns=ARRAY[0],
            altitude=s.zs_array[1][1]
        from {model}.coverage c
        join {model}.storage_node s on st_intersects(s.geom, c.geom)
        where c.id=n.coverage
        ;
        """)

        # les noeuds des reach sont interpolés entre amont et aval,
        # les singularités sont prises en compte ensuite
        feedback.pushInfo('reach interpolation')

        project.execute(f"""
        create table {model}.tmp_reach as
        select * from (
            select r.id as reach, st_linesubstring(r.geom, lag(st_linelocatepoint(r.geom, n.geom)) over w, st_linelocatepoint(r.geom, n.geom)) as geom, lag(n.name) over w as up, n.name as down --, st_force2d(st_endpoint(r.geom)) = st_force2d(n.geom) as is_end
            from {model}.open_reach r
            join {model}._river_node nr on nr.reach=r.reach
            join {model}._node n on n.id=nr.id
            window w as (partition by r.id order by st_linelocatepoint(r.geom, n.geom) asc)
        ) t
        where geom is not null and st_length(geom) > 0.01
        --and not is_end -- TODO remove this patch
        ;

        create index tmp_reach_geom_idx on {model}.tmp_reach using gist(geom);

        with i as (
            select
                n.id,
                ARRAY[r.up, r.down] interpolation_nodes,
                ARRAY[1-r.alpha, r.alpha] elevation_weights,
                ARRAY[1, 1] elevation_columns,
                ARRAY[r.cos_*(1-r.alpha), r.cos_*r.alpha] velocity_u_weights,
                ARRAY[2, 2] velocity_u_columns,
                ARRAY[r.sin_*(1-r.alpha), r.sin_*r.alpha] velocity_v_weights,
                ARRAY[2, 2] velocity_v_columns
            from {model}.vertex n
            join {model}.coverage c on c.id=n.coverage
            cross join lateral (
                select
                    r.reach,
                    r.up,
                    r.down,
                    st_linelocatepoint(r.geom, n.geom) as alpha,
                    (st_x(st_endpoint(t.ds)) - st_x(st_startpoint(t.ds)))/st_length(t.ds) as cos_,
                    (st_y(st_endpoint(t.ds)) - st_y(st_startpoint(t.ds)))/st_length(t.ds) as sin_
                from {model}.tmp_reach r
                cross join lateral (
                    select st_linesubstring(
                        r.geom,
                        greatest(0., st_linelocatepoint(r.geom, n.geom)-st_length(r.geom)/1000.),
                        least(1., st_linelocatepoint(r.geom, n.geom)+st_length(r.geom)/1000.) ) as ds
                    ) as t
                where st_intersects(r.geom, c.geom)
                order by n.geom <-> r.geom limit 1) r
            where n.domain_type='reach'
        )
        update {model}.vertex n set
            interpolation_nodes=i.interpolation_nodes,
            elevation_weights=i.elevation_weights,
            elevation_columns=i.elevation_columns,
            velocity_u_weights=i.velocity_u_weights,
            velocity_u_columns=i.velocity_u_columns,
            velocity_v_weights=i.velocity_v_weights,
            velocity_v_columns=i.velocity_v_columns
        from i where i.id=n.id
        ;

        update {model}.vertex v set
            altitude=(select sum(st_z(c.geom)*v.elevation_weights[i])
                from generate_series(1, array_length(v.interpolation_nodes, 1)) i
                join {model}.river_node c on c.name = v.interpolation_nodes[i])
        where v.domain_type = 'reach'
        ;
        """)

        if not skip_major:
            project.execute(f"""
            with mt as (
                select a, b, c, side, b.start_half_transect, b.end_half_transect
                from {model}.triangle t
                join {model}.riverbank b on b.major && t.geom and st_covers(b.major, t.geom)
            ),
            mn as (
                select a as id, side, start_half_transect, end_half_transect from mt
                union
                select b as id, side, start_half_transect, end_half_transect from mt
                union
                select c as id, side, start_half_transect, end_half_transect from mt
            ),
            tr as (
                select v.id, mn.side,
                    -t.y as du,
                    t.x as dv
                from {model}.vertex v
                join mn on mn.id=v.id
                cross join lateral interpolate_direction(mn.start_half_transect, mn.end_half_transect, v.geom) t
            )
            update {model}.vertex v set
                velocity_u_columns=case when side='right' then ARRAY[7, 7] else  ARRAY[6, 6] end,
                velocity_v_columns=case when side='right' then ARRAY[7, 7] else  ARRAY[6, 6] end,
                -- TODO altitude et directions
                velocity_u_weights=ARRAY[elevation_weights[1]*du, elevation_weights[2]*du],
                velocity_v_weights=ARRAY[elevation_weights[1]*dv, elevation_weights[2]*dv],
                altitude=project.altitude(v.geom),
                lowest_altitude=v.altitude
            from tr where tr.id=v.id and v.domain_type = 'reach'
            ;

            """)



        # prise en compte des singularités sur les reach
        # TODO
            #select c.id, c.geom
            #from {model}.constrain c
            #join {model}._node n on st_dwithin(c.geom, n.geom, 10) and n.node_type='river'
            #join {model}._singularity s on s.node=n.id
            #where c.constrain_type='flood_plain_transect'


        # lissage des noeuds sur les contraintes null et connector
        # on ne prends pas les noeuds qui sont aussi sur d'autres contraintes
        # on moyenne seulement en 2d
        # TODO

        # interpolation des noeuds restants sur leur voisin (discontinuités)
        feedback.pushInfo('remaining interpolation')
        project.execute(f"""
            with interp as (
                select
                    n.id,
                    array_agg(ng.name) as nodes,
                    array_agg(1./array_length(n.neighbors,1)) as elevation_weights,
                    array_agg(1) as elevation_columns,
                    array_agg(1./array_length(n.neighbors,1)) as velocity_u_weights,
                    array_agg(2) as velocity_u_columns,
                    array_agg(1./array_length(n.neighbors,1)) as velocity_v_weights,
                    array_agg(3) as velocity_v_columns
                from {model}.vertex n
                cross join lateral unnest(n.neighbors) nid
                join {model}.vertex ng on ng.id=nid
                where n.interpolation_nodes is null
                and ng.domain_type = '2d'
                and ng.name is not null
                and n.domain_type = '2d'
                group by n.id
            )
            update {model}.vertex n set
                interpolation_nodes=i.nodes,
                elevation_weights=i.elevation_weights,
                elevation_columns=i.elevation_columns,
                velocity_u_weights=i.velocity_u_weights,
                velocity_u_columns=i.velocity_u_columns,
                velocity_v_weights=i.velocity_v_weights,
                velocity_v_columns=i.velocity_v_columns
            from interp i where i.id=n.id
            ;

            -- cas particulier où les voisin sont tous sur des contraintes, on prend le noeud le plus proche
            update {model}.vertex n set
                interpolation_nodes=(select ARRAY[name] from {model}.vertex v where v.coverage=n.coverage and v.name is not null order by v.geom <-> n.geom limit 1),
                elevation_weights=ARRAY[1],
                elevation_columns=ARRAY[1],
                velocity_u_weights=ARRAY[1],
                velocity_u_columns=ARRAY[2],
                velocity_v_weights=ARRAY[1],
                velocity_v_columns=ARRAY[3]
            where domain_type='2d' and interpolation_nodes is null
        ;

        update {model}.vertex v set
            altitude=(select sum(c.zb*v.elevation_weights[i])
                from generate_series(1, array_length(v.interpolation_nodes, 1)) i
                join {model}.elem_2d_node c on c.name = v.interpolation_nodes[i])
        where v.domain_type = '2d' and v.altitude is null
        ;

        update {model}.vertex set lowest_altitude = altitude where lowest_altitude is null
        ;

        """)

        project.commit()

        errors = project.fetchall(f"select st_astext(geom) from {model}.vertex where interpolation_nodes is null")
        if len(errors):
            raise QgsProcessingException("topological error at "+' '.join([e[0] for e in errors]))

        return {}
