# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
hydra module to handle project
"""
import os
import re
import csv
import psycopg2
import threading
from qgis.PyQt.QtCore import QCoreApplication
from hydra.database import database as dbhydra
from hydra.model import Model
from hydra.database.terrain import Terrain
from hydra.database.radar_rain import RadarRain
from hydra.utility import empty_ui_manager
from hydra.utility.settings_properties import SettingsProperties
from hydra.utility.log import LogManager, ConsoleLogger
from hydra.utility.string import normalized_name, isint, isfloat
from hydra.database.radar_rain import RainVrt

def tr(msg):
    return str(QCoreApplication.translate("Hydra", msg))

class Project(object):

    def __del__(self):
        self.close()

    @staticmethod
    def create_new_project(project_name, srid,
                            working_dir = os.path.join(os.path.expanduser('~'), ".hydra"),
                            log_manager = LogManager(ConsoleLogger(), "Hydra"),
                            ui_manager = empty_ui_manager.EmptyUIManager()):
        dbhydra.create_project(project_name, srid, working_dir)
        project = Project(project_name, log_manager, ui_manager)
        log_manager.cleanup()
        log_manager.notice(tr("Project created."))
        return project

    # TODO vmo: remove this useless method
    @staticmethod
    def load_project(project_name, log_manager = LogManager(ConsoleLogger(), "Hydra"), ui_manager = empty_ui_manager.EmptyUIManager()):
        project = Project(project_name, log_manager, ui_manager)
        log_manager.cleanup()
        log_manager.notice(tr("Project loaded."))
        return project

    def __getattr__(self, att):
        if att=='name':
            return self.__current_project
        elif att=='qgs':
            return self.__project_file
        elif att=='srid':
            return self.__srid
        elif att == 'version':
            return self.get_version()
        elif att == 'directory':
            return dbhydra.project_dir(self.__current_project)
        elif att == 'data_dir':
            if not os.path.exists(os.path.join(self.directory, 'data')):
                os.makedirs(os.path.join(self.directory, 'data'))
            return os.path.join(self.directory, 'data')
        elif att == 'carto_dir':
            if not os.path.exists(os.path.join(self.directory, 'carto')):
                os.makedirs(os.path.join(self.directory, 'carto'))
            return os.path.join(self.directory, 'carto')
        elif att == 'save_dir':
            if not os.path.exists(os.path.join(self.directory, 'autosave')):
                os.makedirs(os.path.join(self.directory, 'autosave'))
            return os.path.join(self.directory, 'autosave')
        elif att=='terrain':
            return self.__terrain
        elif att=='radar_rain':
            return self.__radar_rain
        elif att=='models':
            return {m: Model(self, self.__srid, m ) for m in self.get_models()}
        elif att=='scenarios':
            return [s for s, in self.fetchall("select name from project.scenario union select name from project.serie_scenario")]
        elif att=='current_scenario':
            return self.get_current_scenario()[1]
        elif att=='current_model':
            m = self.get_current_model()
            return m.name if m is not None else None
        elif att=='scenario_groups':
            return [g for g, in self.execute("select name from project.scenario_group").fetchall()]
        else:
            raise AttributeError(att)

    @property
    def pgconn(self):
        return self.__conn

    def __init__(self, project_name, log_manager = LogManager(ConsoleLogger(), "Hydra"), ui_manager = empty_ui_manager.EmptyUIManager(), debug=False):
        self.log = log_manager
        self.log.cleanup()
        self.debug = debug
        self.__ui_manager = ui_manager
        self.__current_project =  project_name
        self.__current_model =  None
        # for methods that are not thread safe
        self.lock = threading.Lock()
        db_name = project_name
        try:
            self.__conn = psycopg2.connect(
                database=db_name, service=SettingsProperties.get_service(),
                connect_timeout=3,
                keepalives=1,
                keepalives_idle=5,
                keepalives_interval=2,
                keepalives_count=2)
            self.__cur = self.__conn.cursor()
        except Exception:
            self.__conn = None
            self.log.error(tr('Database ') + project_name + tr(' not found. You should use create_new_projet/open_project instead of constructor.'))
            raise

        # Project file
        self.__project_file = os.path.abspath(os.path.join(self.directory, self.__current_project + ".qgs"))
        # SRID
        query = self.fetchone("""select srid from hydra.metadata""")
        if query is not None:
            self.__srid = query[0]
        else:
            self.__srid = 0
        # Terrain and Radar_rain modules
        if not os.path.exists(self.directory):
            os.makedirs(self.directory)
        self.__terrain = Terrain(os.path.join(self.directory, 'terrain'), self)
        self.__radar_rain = RadarRain(os.path.join(self.directory, 'rain'), self)
        # Data directory
        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)
        # Carto directory
        if not os.path.exists(self.carto_dir):
            os.makedirs(self.carto_dir)

    def open_in_ui(self, qgs_already_opened = False):
        # Check project file
        if not(os.path.isfile(self.__project_file)):
            self.log.notice(str(tr("New project file created.")))
            self.__ui_manager.new_project()
            self.save()

        if not qgs_already_opened:
            self.__ui_manager.open_project(self.__project_file)
        else:
            # Check version
            if self.get_version() != dbhydra.data_version():
                raise RuntimeError(
                        tr('Data model in %s is version %s while the database module is version %s')%(
                        self.__current_project, self.get_version(), dbhydra.data_version()))
            # Load layers if not there
            if not self.__ui_manager.layer_group_exists("project"):
                    self.__ui_manager.add_vector_layers(self.__current_project, "project", "layers_project.json", self.srid)
            for model_name in self.get_models():
                if not self.__ui_manager.layer_group_exists(model_name):
                    self.__ui_manager.add_vector_layers(self.__current_project, model_name, "layers_model.json", self.srid)
            # Check terrain
            self.get_terrain_errors()

            self.log.notice(str(tr("Project {0} opened.")).format(self.__project_file))

    def build_vrt(self, file, data_srid=None, name="terrain"):
        vrt = self.__terrain.build_vrt(file, data_srid, normalized_name(name, 48))
        return vrt

    def build_rain_multivrt(self, name, file, template, data_srid=None):
        vrt = self.__radar_rain.build_vrt(name, file, template, data_srid)
        return vrt

    def get_terrain_errors(self):
        for id, file in self.__terrain.error_files():
            self.log.warning(tr("Error with terrain file {}: {}".format(id, self.unpack_path(file))))

    def save(self):
        self.__ui_manager.save_project(self.__project_file)
        self.log.notice(str(tr("File {filename} saved.")).format(filename = self.__project_file))

    def add_new_model(self, model_name, empty=False):
        if self.get_current_project() is None:
            raise Exception(tr('Project needed. You should use create_new_projet to create a new project.'))
        else:
            dbhydra.remove_model(self.__cur, str(model_name))
            self.__conn.commit()
            dbhydra.create_model(self.__cur, str(model_name), self.__srid, empty)
            self.__conn.commit()
            self.__ui_manager.add_vector_layers(self.__current_project, model_name, "layers_model.json", self.srid)
            self.log.notice(str(tr("Model {0} created.")).format(model_name))

    def delete_model(self, model_name):
        self.__ui_manager.remove_group_layers(model_name)
        dbhydra.remove_model(self.__cur, str(model_name))
        self.__conn.commit()

    def reload_project_layers(self):
        self.__ui_manager.remove_group_layers('project')
        self.__ui_manager.add_vector_layers(self.__current_project, "project", "layers_project.json", self.srid)

    def reload_model_layers(self, model_name):
        if self.get_current_model() is not None:
            self.__ui_manager.remove_group_layers(model_name)
            self.__ui_manager.add_vector_layers(self.__current_project, model_name, "layers_model.json", self.srid)

    def reload_terrain_layers(self):
        self.__ui_manager.remove_group_layers('terrain')
        terrain_files = self.execute("""select source from project.dem order by priority""").fetchall()
        self.__ui_manager.add_terrain_layers(self.__current_project, [self.unpack_path(f) for f, in terrain_files])

    def reload_rain_layers(self):
        self.__ui_manager.remove_group_layers('rain')
        vrt_rains = [RainVrt(r[0], self) for r in self.fetchall("""select file,  unit_correction_coef from project.radar_rainfall""")]
        self.__ui_manager.add_rain_layers(vrt_rains)

    def load_work_layer(self, table_name, layer_name, column_id="id", column_geom="geom"):
        is_valid = self.__ui_manager.add_work_layer(table_name, layer_name, self.name, column_id, column_geom)
        if not is_valid:
            self.log.warning(tr("Layer is not valid"))

    def load_synthetic_results_layers(self):
        pass


    def load_csv_results_layers(self):
        model_name = self.get_current_model().name
        scn = self.get_current_scenario()
        if scn is not None:
            scn_name = scn[1]
        else:
            scn_name = ''

        mat_views = ['river', 'manhole', 'pipe', 'pipe_hydrol', 'link', 'catchment', 'surface', 'mesh_1d_coted']
        views_to_display=[]

        for mat_view in mat_views:
            table_name = 'results_'+mat_view if mat_view!='mesh_1d_coted' else 'mesh_1d_coted'
            populated = self.fetchone("""select ispopulated from pg_matviews where schemaname='{}' and matviewname='{}'""".format(model_name, table_name))
            if populated and populated[0]:
                item_count, = self.fetchone("""select count(1) from {}.{}""".format(model_name, table_name))
                if item_count>0:
                    views_to_display.append(mat_view)

        if 'mesh_1d_coted' in views_to_display:
            self.__create_mesh_1d_csv()

        if views_to_display:
            self.__ui_manager.add_result_layers(self.directory, self.__current_project, model_name, scn_name, views_to_display)
            return len(views_to_display)
        else:
            return 0

    def __create_mesh_1d_csv(self):
        model_name = self.get_current_model().name
        scn = self.get_current_scenario()
        if scn is not None:
            scn_name = scn[1]
        else:
            scn_name = ''

        file_mesh = os.path.join(self.directory, scn_name, 'hydraulique', '{}_{}_mesh_1d.csv'.format(scn_name.upper(), model_name.upper()))
        file_nodes = os.path.join(self.directory, scn_name, 'hydraulique', '{}_{}_river.csv'.format(scn_name.upper(), model_name.upper()))

        dic_z_nodes = {}
        triangles = []
        if os.path.isfile(file_nodes):
            with open(file_nodes, "r") as fn:
                csv_reader_nodes = csv.reader(fn, delimiter=";")
                # This skips the header of the CSV file.
                next(csv_reader_nodes)
                for line in csv_reader_nodes:
                    if isint(line[2]) and isfloat(line[7]):
                        dic_z_nodes[int(line[2])] = float(line[7]) #key = "node" (id), value="z(m)" (ngf)

            triangles = self.fetchall("""select id, z_a, z_b, z_c,
                                               a_strt, a_strt_w, a_nd, a_nd_w,
                                               b_strt, b_strt_w, b_nd, b_nd_w,
                                               c_strt, c_strt_w, c_nd, c_nd_w
                                               from {}.mesh_1d_coted""".format(model_name))

        with open(file_mesh, "w") as fm:
            csv_writer_mesh = csv.writer(fm, delimiter=";")
            csv_writer_mesh.writerow(['MODEL', 'id', 'za(m)', 'zb(m)', 'zc(m)', 'zf(m)', 'zw(m)', 'h(m)'])

            for triangle in triangles:
                id, z_a, z_b, z_c, a_strt, a_strt_w, a_nd, a_nd_w, \
                b_strt, b_strt_w, b_nd, b_nd_w, c_strt, c_strt_w, c_nd, c_nd_w = triangle

                if all(id in list(dic_z_nodes.keys()) for id in [a_strt, a_nd, b_strt, b_nd, c_strt, c_nd]):
                    a_result = a_strt_w * dic_z_nodes[a_strt] + a_nd_w * dic_z_nodes[a_nd]
                    b_result = b_strt_w * dic_z_nodes[b_strt] + b_nd_w * dic_z_nodes[b_nd]
                    c_result = c_strt_w * dic_z_nodes[c_strt] + c_nd_w * dic_z_nodes[c_nd]
                else:
                    a_result, b_result, c_result = z_a, z_b, z_c

                csv_writer_mesh.writerow([model_name, id, a_result, b_result, c_result, min(z_a, z_b, z_c), max(a_result, b_result, c_result), max(a_result, b_result, c_result) - min(z_a, z_b, z_c)])

    def unload_csv_results_layers(self, scenario_name=None):
        if scenario_name:
            self.__ui_manager.remove_group_layers('res: ' + scenario_name)
        else:
            for id, name in self.get_scenarios():
                self.__ui_manager.remove_group_layers('res: ' + name)

    def reload_w15_layer(self, w14_result, w15_result):
       self.unload_w15_layer()
       layer = self.__ui_manager.add_w15_layer(self, w14_result, w15_result)
       return layer

    def unload_w15_layer(self):
        self.__ui_manager.remove_w15_layer()

    def get_models(self):
        return [m for m, in self.fetchall("select name from project.model order by UPPER(name) asc")]

    def get_current_project(self):
        return self.__current_project

    def get_current_model(self):
        return self.__current_model

    def set_current_model(self, model_name):
        if model_name is not None:
            self.__current_model = Model(self, self.__srid, model_name)
        else :
            self.__current_model = None

    def get_current_scenario(self):
        return self.fetchone("""select id, name from project.scenario where id=(select current_scenario from project.metadata);""")

    def get_scenarios(self):
        return self.fetchall("""select id, name from project.scenario order by name asc;""")

    def get_serie_scenarios(self, id_serie):
        return self.fetchall("""select id, name from project.serie_scenario {};""".format('where id_cs={}'.format(id_serie) if id_serie else ''))

    def get_serie_scenario_path(self, scn_id):
        name_scenario, =  self.fetchone("""
            select name
            from project.serie_scenario
            where id=%s
            """, (int(scn_id),))
        return self.get_senario_path_from_name(name_scenario)

    def get_senario_path(self, id_scenario):
        name_scenario, =  self.fetchone("""
            select name
            from project.scenario
            where id=%s
            union all
            select name
            from project.serie_scenario
            where id=%s
            """, (int(id_scenario), int(id_scenario)))
        return self.get_senario_path_from_name(name_scenario)

    def get_senario_path_from_name(self, name_scenario):
        return os.path.join(self.directory, name_scenario.upper())

    def get_models_from_scenario(self, scenario):
        """Returns a list of models associated with a scenario.
        Uses model connection method to retrieve models
        """
        with self.lock:
            # not thread safe since self.__cursor is on the main thread
            results = self.fetchone("""
                select s.name, c.name as mode,
                case
                    when c.name = 'mixed' then array_agg(distinct mixed.name)
                    else array_agg(distinct models.name) end as models
                from project.scenario s
                left join hydra.model_connection_settings c on c.name = s.model_connect_settings
                left join lateral (
                    select mc.name
                    from project.mixed m
                    join project.grp_model gm on gm.grp = m.grp
                    join project.model_config mc on mc.id = gm.model
                    where m.scenario = s.id
                ) mixed on true
                left join lateral (select name::varchar from project.model) as models on true
                where s.name = %s
                group by s.name, c.name""", [scenario])

        if results:
            return results[2]

    def set_current_scenario(self, scn_id):
        self.execute("update project.metadata set current_scenario = %s", (scn_id,))
        self.__conn.commit()

    def scn_has_run(self, scn_id):
        id, scn_name = self.fetchone(f"""
            select id, name from project.scenario where id = {scn_id}
            union all
            select id, name from project.serie_scenario where id = {scn_id}
            """)
        return self.scn_has_run_from_name(scn_name)

    def scn_has_run_from_name(self, scn_name):
        '''usefull to get state of a scenario run (or serie scenario)'''
        fname = os.path.join(self.get_senario_path_from_name(scn_name), 'hydraulique', 'whydram24.err')
        if os.path.isfile(fname):
            with open(fname) as f:
                content = f.readlines()
                if isint(content[0]) and int(content[0])==0:
                    return True
        return False

    def add_new_scenario(self):
        return dbhydra.create_scenario(self.__cur)

    def get_version(self):
        query = self.fetchone("""select version from hydra.metadata;""")
        if query:
            return query[0]
        else:
            self.log.error("No project version found")
            return None

    def get_version_to_update(self):
        return dbhydra.target_version(self.get_version())

    def pack_path(self, path):
        """when we pack path we use posix separators: /"""
        if path is None or path =='':
            return path
        else:
            p = os.path.normpath(path)
            w = os.path.normpath(dbhydra.project_dir(self.name))
            if p.startswith(w):
                p = p.replace(w, '$workspace').replace(os.sep, '/')
            return p

    def unpack_path(self, path):
        """if we are on windows we replace posix separators by \\"""
        if path is None or path =='':
            return path
        else:
            p = path.replace('/', os.sep)
            if p.startswith('$workspace'):
                p = os.path.normpath(p.replace('$workspace', dbhydra.project_dir(self.name)))
            return p

    # def __sql_wait(self):
    #     while 1:
    #         state = self.__conn.poll()
    #         if state == psycopg2.extensions.POLL_OK:
    #             break
    #         elif state == psycopg2.extensions.POLL_WRITE:
    #             select.select([], [self.__conn.fileno()], [])
    #         elif state == psycopg2.extensions.POLL_READ:
    #             select.select([self.__conn.fileno()], [], [])
    #         else:
    #             raise psycopg2.OperationalError("poll() returned %s" % state)

    def vacuum_analyse(self, table=''):
        old_isolation_level = self.__conn.isolation_level
        self.__conn.set_isolation_level(0)
        self.__cur.execute("Vacuum analyse "+table)
        self.__conn.set_isolation_level(old_isolation_level)

    def try_execute(self, sql, param=None):
        success=True
        try:
            if param is None:
                self.__cur.execute(sql)
            else:
                self.__cur.execute(sql, param)
        except:
            self.__conn.rollback()
            success=False
        return success, self.__cur

    def execute(self, sql, param=None):
        try:
            if param is None:
                if self.debug:
                    self.log.notice("Query: "+sql)
                self.__cur.execute(sql)
            else:
                if self.debug:
                    self.log.notice("Query: "+sql+"\n\nParameters: "+"\n".join([str(p) for p in param]))
                self.__cur.execute(sql, param)

            if self.debug:
                for notice in self.__conn.notices:
                    self.log.notice(notice)
                self.__conn.notices = []

        except Exception as error:
            # Rollback in case there is any error
            self.__conn.rollback()
            self.log.notice('Rollback')
            self.log.error("Error in query: {}".format(re.sub(' +', ' ', sql)))
            if 'GetGeoTransform' in str(error):
                self.log.error("Error with terrain")
                self.get_terrain_errors()
            raise
        return self.__cur

    def execute_isolated(self, sql, param=None):
        try:
            if param is None:
                old_isolation_level = self.__conn.isolation_level
                self.__conn.set_isolation_level(0)
                self.__cur.execute(sql)
                if self.debug:
                    self.log.notice("Query [isolated]: "+sql)
                self.__conn.set_isolation_level(old_isolation_level)
            else:
                old_isolation_level = self.__conn.isolation_level
                self.__conn.set_isolation_level(0)
                self.__cur.execute(sql, param)
                if self.debug:
                    self.log.notice("Query [isolated]: "+sql+"\n\nParameters: "+"\n".join([str(p) for p in param]))
                self.__conn.set_isolation_level(old_isolation_level)
        except Exception as error:
            # Rollback in case there is any error
            self.__conn.rollback()
            self.log.notice('Rollback')
            self.log.error("Error in query: {}".format(re.sub(' +', ' ', sql)))
            if 'GetGeoTransform' in str(error):
                self.log.error("Error with terrain")
                self.get_terrain_errors()
            raise
        return self.__cur

    def executemany(self, sql, values):
        try:
            self.__cur.executemany(sql, values)
        except:
            # Rollback in case there is any error
            self.__conn.rollback()
            self.log.notice('Rollback')
            raise
        return self.__cur

    def commit(self):
        try:
            self.__conn.commit()
            #self.log.notice('Commit')
        except:
            # Rollback in case there is any error
            self.__conn.rollback()
            self.log.notice('Rollback')
            raise

    def rollback(self):
        self.__conn.rollback()
        self.log.notice('Rollback')

    def close(self):
        if self.__conn:
            self.__conn.close()


    # New way of accessing project without risk of locking tables
    def fetchone(self, query, param=None):
        with dbhydra.autoconnection(self.name) as con, con.cursor() as cur:
            if self.debug:
                self.log.notice("Query: "+query+("\n\nParameters: "+"\n".join([str(p) for p in param]) if param else ""))
            cur.execute(query, param)
            return cur.fetchone()

    def fetchall(self, query, param=None):
        with dbhydra.autoconnection(self.name) as con, con.cursor() as cur:
            if self.debug:
                self.log.notice("Query: "+query+("\n\nParameters: "+"\n".join([str(p) for p in param]) if param else ""))
            cur.execute(query, param)
            return cur.fetchall()
