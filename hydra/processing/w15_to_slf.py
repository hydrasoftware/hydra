import os
from pathlib import Path
from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterString,
    QgsProcessingParameterNumber,
    QgsProcessingParameterEnum,
    QgsMeshLayer
)
from qgis.core import QgsProject
from qgis.utils import iface
from ..project import Project
from .utility import ProjectWidget, ModelWidget, ScenarioWidget
from wstar.w15_to_slf import process as wstar_process


class W15ToSlf(QgsProcessingAlgorithm):
    """
    Converts hydra results to selafin format for use with mesh layers
    """

    PROJECT = "project"
    MODEL = "model"
    SCENARIO = "scenario"
    SUBMERSION_TIME_UNITS = "submersion_time_units"
    TIME_UNITS = ['', 's', 'min', 'h', 'day']
    LOAD_RESULT = "load_result"
    SUBMERSION_HEIGHT = "submersion_height"

    def tr(self, string):
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return W15ToSlf()

    def name(self):
        return "w15_to_slf"

    def displayName(self):
        return self.tr("Results to mesh")

    def group(self):
        return self.tr("River and free surface flow mapping")

    def groupId(self):
        return "surfaceflowmapping"

    def shortHelpString(self):
        return self.tr("Creates selafin result file from hydra results.")

    def initAlgorithm(self, config=None):

        param = QgsProcessingParameterString(self.PROJECT, "Project")
        param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.MODEL, "Model")
        param.setMetadata({ 'widget_wrapper': {'class': ModelWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.SCENARIO, "Scenario")
        param.setMetadata({ 'widget_wrapper': {'class': ScenarioWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterEnum(self.SUBMERSION_TIME_UNITS, "Submersion time units", self.TIME_UNITS, defaultValue=0, optional=True)
        self.addParameter(param)

        param = QgsProcessingParameterNumber(self.SUBMERSION_HEIGHT, "Submersion height", defaultValue=0.001, optional=True, minValue=0, type=QgsProcessingParameterNumber.Double)
        self.addParameter(param)

        param = QgsProcessingParameterBoolean(self.LOAD_RESULT, "Load result in project", defaultValue=True, optional=True)
        self.addParameter(param)


    def processAlgorithm(self, parameters, context, feedback):

        project = Project(self.parameterAsString(parameters, self.PROJECT, context))
        model = self.parameterAsString(parameters, self.MODEL, context)
        scenario = self.parameterAsString(parameters, self.SCENARIO, context)
        submersion_height = self.parameterAsDouble(parameters, self.SUBMERSION_HEIGHT, context)
        submersion_time_units = self.TIME_UNITS[self.parameterAsEnum(parameters, self.SUBMERSION_TIME_UNITS, context)]
        self.load_result = self.parameterAsBoolean(parameters, self.LOAD_RESULT, context)

        comment, date0 = project.execute("""
            select comment, date0
            from project.scenario
            where name ilike %s""", (scenario,)).fetchone()

        w15_path = Path(project.directory) / scenario.upper() / 'hydraulique' / f"{scenario.upper()}_{model.upper()}.w15"
        self.slf_path = w15_path.with_suffix('.slf')
        self.scenario = scenario
        self.model = model

        feedback.pushInfo(f'date0 = {date0.isoformat()}')
        wstar_process(
            w15_path,
            self.slf_path,
            model,
            scenario,
            date0,
            project.pgconn,
            submersion_height=submersion_height,
            submersion_time_units=submersion_time_units,
            comment=comment,
            feedback=feedback,
        )

        return {}

    def postProcessAlgorithm(self, context, feedback):
        if iface is not None and self.load_result:
            root = QgsProject.instance().layerTreeRoot()
            grp = 'results'
            subgrp = self.scenario
            group = root.findGroup(grp) or root.insertGroup(0, grp)
            subgroup = group.findGroup(subgrp) or group.insertGroup(0, subgrp)
            layer = QgsMeshLayer(str(self.slf_path), f"{self.scenario}_{self.model}", 'mdal')
            layer.loadNamedStyle(os.path.join(os.path.dirname(__file__), 'depth_velocity.qml'))
            layer.temporalProperties().setDefaultsFromDataProviderTemporalCapabilities(layer.dataProvider().temporalCapabilities())# setAlwaysLoadReferenceTimeFromSource(True)
            QgsProject.instance().addMapLayer(layer, False)
            subgroup.addLayer(layer)

        return {}
