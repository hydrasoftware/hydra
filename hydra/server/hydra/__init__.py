# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
hydra server-side module

"""

from shapely import geos

# @note: this makes the imported functions visible from import hydra
from .altitude import altitude, altitudes, set_altitude, set_link_altitude, set_link_altitude_topo, crest_line, filling_curve, line_elevation
from .build import build, create_project, create_model, drop_project_api, drop_model_api, create_model_api, create_project_api
from .export import geom_dat, ctrz_dat
from .mesh import tesselate, discretize_line, create_elem2d_links, create_links, create_boundary_links, create_network_overflow_links, crossed_border
from .topo import profile_from_transect
from .regulation import regulated_items
from .wk_hydra import wkb_loads













