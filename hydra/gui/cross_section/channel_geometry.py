# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import re
from qgis.PyQt.QtWidgets import QWidget, QHBoxLayout
from qgis.PyQt import uic

from numpy import array
from ..widgets.array_widget import ArrayWidget
from .geometry import Geometry

def sz(zbmin_array):
    return [[-.5*b,  z] for z, b in reversed(zbmin_array)] + [[.5*b,  z] for z, b in zbmin_array]

class ChannelGeometry(Geometry):
    def __init__(self, zbmin_array, dz=0, transect=None, terrain=None, parent=None, with_flood_plain=False, flood_plain_width=0, flood_plain_lateral_slope=0):
        super().__init__(sz(zbmin_array), dz, transect, terrain, parent=parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "channel_geometry.ui"), self)
        self.__zbmin_array = zbmin_array
        self.zbmin_array = ArrayWidget(["h[m]", "b [m]"], [20,2], zbmin_array, parent=self.array_placeholder)
        self.zbmin_array.data_edited.connect(self.__zb_changed)
        self.with_flood_plain.setChecked(with_flood_plain)
        self.flood_plain_width.setText(str(flood_plain_width))
        self.flood_plain_lateral_slope.setText(str(flood_plain_lateral_slope))

        self.with_flood_plain.toggled.connect(self.__param_changed)
        self.flood_plain_width.textChanged.connect(self.__param_changed)
        self.flood_plain_lateral_slope.textChanged.connect(self.__param_changed)

    def __zb_changed(self, zbmin_array):
        self.set_sz(sz(zbmin_array))
        self.__zbmin_array = zbmin_array
        self.geometry_changed.emit(self.param)

    def __param_changed(self, dummy=None):
        self.geometry_changed.emit(self.param)

    def __getattr__(self, name):
        if name == 'geom':
            return self.__geometry
        elif name == 'param':
            return {
            "zbmin_array": self.__zbmin_array,
            "with_flood_plain": self.with_flood_plain.isChecked(),
            "flood_plain_width": float(self.flood_plain_width.text()),
            "flood_plain_lateral_slope": float(self.flood_plain_lateral_slope.text())
            }
        else:
            return super().__getattr__(name)
