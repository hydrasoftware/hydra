# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import re
from collections import defaultdict
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QWidget, QGridLayout, QMessageBox, QLabel, QHBoxLayout
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtCore import Qt, pyqtSignal, QObject
from numpy import around, float32, array, dot, concatenate, all as npall, where as npwhere, copy as npcopy, sum as npsum, unique, array_equal, int64, sort as npsort, inf as npinf, logical_and
from numpy.linalg import norm
from shapely.geometry import Polygon, LineString, Point, MultiPoint, MultiLineString, GeometryCollection
from shapely import wkb, geos
from shapely.errors import TopologicalError
from shapely.ops import substring, nearest_points
from shapely import validation
from osgeo import gdal
from scipy.optimize import least_squares, minimize
from functools import reduce

from ..widgets.array_widget import ArrayWidget
from .geometry import Geometry
from .utility import project_topo, sz_from_zb, is_stupid, first_crossing, simplified


class Fitter:
    def __init__(self, left, minor, right, target_sz):
        self.__target_line = LineString(target_sz)
        self.__left = left
        self.__minor = minor
        self.__right = right
        sz = self.sz(0, .5)
        self.__segments = [max(1, int(norm(u)/0.5)) for u in sz[:-1] - sz[1:]]

    def sz(self, s_offset, center_coef):
        return sz_from_zb(self.__left, self.__minor, self.__right, s_offset=s_offset, center_coef=center_coef)

    def residual(self, x):
        if len(x) == 1:
            x = [x[0], .5]

        # segmentize line
        sz = self.sz(x[0], x[1])
        l = []
        for s, e, n in zip(sz[:-1], sz[1:], self.__segments):
            d = e - s
            for i in range(n):
                l.append(s + (i/n)*d)
        l.append(e)

        d = array([min(1, self.__target_line.distance(Point(pt))) for pt in l])
        return d

def invalidity(zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array):
    reason = ''
    if [len(zbmin_array),len(zbmaj_lbank_array),len(zbmaj_rbank_array)] == [0,0,0] :
        return 'all zb arrays are empty'
    elif any(zbmin_array[i][0]>zbmin_array[i+1][0] for i in range(0,len(zbmin_array)-1)) :
        return 'z is not ascending in riverbed zb array'
    elif any(zbmin_array[i][1]>zbmin_array[i+1][1] for i in range(0,len(zbmin_array)-1)) :
        return 'b is not ascending in riverbed zb array'
    elif any(zbmaj_lbank_array[i][0]>zbmaj_lbank_array[i+1][0] for i in range(0,len(zbmaj_lbank_array)-1)) :
        return 'z is not ascending in left floodplain zb array'
    elif any(zbmaj_lbank_array[i][1]>zbmaj_lbank_array[i+1][1] for i in range(0,len(zbmaj_lbank_array)-1)) :
        return 'b is not ascending in left floodplain zb array'
    elif any(zbmaj_rbank_array[i][0]>zbmaj_rbank_array[i+1][0] for i in range(0,len(zbmaj_rbank_array)-1)) :
        return 'z is not ascending in right floodplain zb array'
    elif any(zbmaj_rbank_array[i][1]>zbmaj_rbank_array[i+1][1] for i in range(0,len(zbmaj_rbank_array)-1)) :
        return 'b is not ascending in right floodplain zb array'
    elif len(zbmaj_lbank_array) != 0 and zbmaj_lbank_array[0][0] < zbmin_array[-1][0] :
        return 'first point of left floodplain is below riverbed'
    elif len(zbmaj_rbank_array) != 0 and zbmaj_rbank_array[0][0] < zbmin_array[-1][0] :
        return 'first point of right floodplain is below riverbed'
    elif len(zbmaj_lbank_array) > 8 :
        return 'too many points on left floodplain'
    elif len(zbmaj_rbank_array) > 8 :
        return 'too many points on right floodplain'
    elif len(zbmin_array) > 8 :
        return 'too many points on riverbed'
    return reason if reason != '' else None

class StupidValleyGeometry(Geometry):
    """class to edit former hydra profiles while
    maintaining the constrains"""
    bottom_changed = pyqtSignal(float)
    transect_changed = pyqtSignal(list)
    zb_changed = pyqtSignal(list)

    def __init__(self, sz, left, minor, right, zlevee_lb, zlevee_rb, transect_geom, transect_topo, topo_points, terrain, dz, fake_transect=None, parent=None):
        super().__init__(sz, dz, None, None, parent)

        self.__topo, self.__ref, self.__dem = project_topo(terrain, transect_geom, topo_points, transect_topo)
        self.__zlevee_lb = zlevee_lb
        self.__zlevee_rb = zlevee_rb
        self.__dz = dz
        self.__transect_geom = transect_geom
        self.__fake_transect = fake_transect
        if sz is None: # old hydra profile, we fit the terrain + topo_points if any and create sz accordingly
            if self.__topo is not None:
                target = self.__topo
                fitter = Fitter(left, minor, right, target)
                sz = fitter.sz(0, 0.5)
                x0 = target[target[:,1].argmin(), 0] - sz[sz[:,1].argmin(), 0]
                res = least_squares(fitter.residual, [x0])
                res = least_squares(fitter.residual, [res.x[0], .5], bounds=([-npinf, 0.001], [npinf, .999]))
                self.__sz = fitter.sz(res.x[0], res.x[1])
            else:

                self.__sz = sz_from_zb(left, minor, right)
        else:
            self.__sz = array(sz)

        if self.__transect_geom is None:
            self.__topo, self.__ref, self.__dem = project_topo(terrain, fake_transect, topo_points, transect_topo)
            if self.__topo is not None:
                dx = left[-1][1] + minor[-1][1]*0.5 - 0.5*(self.__topo[0, 0] + self.__topo[-1, 0])
                self.__topo += array([dx, 0])
                self.__dem += array([dx, 0])

        self.__leftBankIdx = max(len(left) - 1, 0)
        self.__rightBankIdx = min(len(left) + 2*len(minor), len(self.__sz)-1)
        self.__minorStartIdx = len(left)
        self.__minorEndIdx = len(left) + 2*len(minor) - 1

    def set_zlevee(self, left, right):
        self.__zlevee_lb, self.__zlevee_rb = left, right

    def __getattr__(self, name):
        if name == 'simplified':
            return self.__sz + array([0, self.__dz])
        elif name == 'left_levee':
            p = self.sz[self.__leftBankIdx]
            return array([p, array([p[0], max(self.__zlevee_lb + self.__dz, p[1])])])
        elif name == 'right_levee':
            p = self.sz[self.__rightBankIdx]
            return array([p, array([p[0], max(self.__zlevee_rb + self.__dz, p[1])])])
        elif name == 'z_invert':
            return self.zb[1][0][0] + self.__dz
            return self.__topo
        elif name == 'ref':
            return self.__ref
        elif name == 'transect':
            if self.__transect_geom is not None:
                return list(self.__transect_geom.coords)
            elif self.__fake_transect is not None:
                return list(self.__fake_transect.coords)
            else:
                return None
        elif name == 'sz':
            return self.__sz + array([0, self.__dz]) if self.__sz is not None else None
        elif name == 'dz':
            return self.__dz
        elif name == 'zb':
            return [
                [ [z, round(self.__sz[self.__minorStartIdx, 0] - s, 3)] for s, z in self.__sz[self.__leftBankIdx::-1]],
                [ [self.__sz[i + self.__minorStartIdx, 1], round(self.__sz[self.__minorEndIdx - i, 0] - self.__sz[i + self.__minorStartIdx, 0], 3)]
                    for i in reversed(range((self.__minorEndIdx - self.__minorStartIdx + 1)//2))],
                [ [z, round(s - self.__sz[self.__minorEndIdx, 0], 3)] for s, z in self.__sz[self.__rightBankIdx:] ] ]
        elif name == 'hydra_profile':
            return self.zb + [self.__sz.tolist()] #+ [self.__zlevee_lb, self.__zlevee_rb]
        elif name == 'limits':
            return (
                self.__sz[0, 0],
                self.__sz[self.__leftBankIdx, 0],
                self.__sz[self.__rightBankIdx, 0],
                self.__sz[-1, 0])
        elif name == 'center':
            xl, xr = self.limits[1:3]
            return 0.5*(xl + xr)
        elif name == 'riverbed':
            xlm, xl, xr, xrm = self.limits
            return array([
                [xlm, -9999],
                [xlm, 9999],
                [xl, 9999],
                [xl, -9999],
                [xr, -9999],
                [xr, 9999],
                [xrm, 9999],
                [xrm, -9999]])
        elif name == 'topo':
            return self.__topo
        elif name == 'dem':
            return self.__dem
        elif name == 'invalidity':
            minor, left, right = self.zb
            return invalidity(minor, left, right)
        elif name == 'water':
            return self.__sz[self.__minorStartIdx: self.__minorEndIdx+1] + array([0, self.__dz])
        else:
            raise AttributeError(name)

    def set_dz(self, dz):
        self.__dz = dz

    def move_point(self, p_orig, p):

        idx, = npwhere((self.sz == p_orig[0]).all(axis=1))
        if len(idx)>0:
            idx = max(idx)

            if idx >= self.__minorStartIdx and idx <= self.__minorEndIdx:
                sidx = self.__minorEndIdx - (idx - self.__minorStartIdx)
                b = abs(self.__sz[sidx, 0] - self.__sz[idx, 0])
                self.__sz[idx] = p[0], p[1] - self.__dz
                self.__sz[sidx, 1] = p[1] - self.__dz
                if idx < sidx:
                    self.__sz[sidx, 0] = self.__sz[idx, 0] + b #max(self.__sz[idx, 0], self.__sz[sidx, 0] )
                else:
                    self.__sz[sidx, 0] = min(self.__sz[idx, 0], self.__sz[sidx, 0] )
                if abs(idx - sidx) == 1:
                    self.bottom_changed.emit(p[1])
                #self.__sz[min(idx, sidx), 0] = min(self.__sz[idx, 0], self.__sz[idx, 0] )

            else:
                self.__sz[idx] = p[0], p[1] - self.__dz

            self.zb_changed.emit(self.hydra_profile)
            return array([self.sz[idx]])

        elif len(p_orig) == 2:
            xlm, xl, xr, xrm = self.limits
            if p_orig[0, 0] in (xlm, xl, xr, xrm):
                if p_orig[0, 0] == xlm:
                    dx = p[0] - self.__sz[0, 0]
                    self.__sz += array([dx, 0])
                elif p_orig[0, 0] == xl:
                    self.__sz[self.__leftBankIdx, 0] = p[0]
                elif p_orig[0, 0] == xr:
                    self.__sz[self.__rightBankIdx, 0] = p[0]
                elif p_orig[0,0] == xrm:
                    self.__sz[-1, 0] = p[0]

                self.zb_changed.emit(self.hydra_profile)
                #assert(is_stupid(*self.hydra_profile[:4]))
                return array([[p[0], -9999],[p[0], 9999]])
        else:
            return p_orig

    def add_point(self, p):
        # find future point idx
        idx = 0
        for i, x in enumerate(self.sz[:,0]):
            if p[0] > x:
                idx = i+1

        self.__sz = concatenate((self.__sz[:idx], array([[p[0], p[1] - self.__dz]]), self.__sz[idx:]))

        if idx <= self.__leftBankIdx:
            self.__minorStartIdx += 1
            self.__leftBankIdx = self.__minorStartIdx - 1
            self.__rightBankIdx += 1
            self.__minorEndIdx += 1
        elif idx > self.__minorEndIdx:
            self.__rightBankIdx = self.__minorEndIdx + 1
        elif idx >= self.__minorStartIdx and idx <= self.__minorEndIdx +1:
            sidx = self.__minorEndIdx - (idx - self.__minorStartIdx) + 1
            if sidx >= idx:
                sidx += 1
            # interpolate symetric point on line
            ps, pe = self.__sz[sidx-1], self.__sz[sidx]
            a = ((p[1] - self.__dz - ps[1])/(pe[1] - ps[1])) if (pe[1] - ps[1]) !=0 else 0.5
            self.__sz = concatenate((self.__sz[:sidx], [ps + a*(pe-ps)], self.__sz[sidx:]))
            self.__rightBankIdx += 2
            self.__minorEndIdx += 2

        #assert(is_stupid(*self.hydra_profile))
        self.zb_changed.emit(self.hydra_profile)

    def remove_point(self, point):
        if self.__minorStartIdx == self.__minorEndIdx - 1: # would remove riverbed
            pass
        else:
            point = point[0]

        if isinstance(point, int) or isinstance(point, int64):
            idx = point
        else:
            idx, = npwhere(npall(self.sz == point, axis=1))
            if len(idx)>0:
                idx = idx[0]
            else:
                return
        if idx >= self.__minorStartIdx and idx <= self.__minorEndIdx:
            if self.__minorEndIdx - self.__minorStartIdx <= 2:
                return
            self.__sz = concatenate((self.__sz[0:idx], self.__sz[idx+1:]))
            sidx = self.__minorEndIdx - (idx - self.__minorStartIdx)
            if sidx >= idx:
                sidx -= 1
            self.__sz = concatenate((self.__sz[0:sidx], self.__sz[sidx+1:]))
            self.__rightBankIdx -= 2
            self.__minorEndIdx -= 2
            if abs(sidx - idx) == 1:
                self.bottom_changed.emit(self.__sz[idx-1, 1])
        elif idx < self.__minorStartIdx:
            self.__sz = concatenate((self.__sz[0:idx], self.__sz[idx+1:]))
            self.__leftBankIdx = max(self.__leftBankIdx - 1, 0)
            self.__minorStartIdx = max(self.__minorStartIdx - 1, 0)
            self.__rightBankIdx -= 1
            self.__minorEndIdx -= 1
        else:
            self.__sz = concatenate((self.__sz[0:idx], self.__sz[idx+1:]))
            self.__rightBankIdx = min(len(self.__sz) - 1, self.__rightBankIdx)

        #assert(is_stupid(*self.hydra_profile))

        self.zb_changed.emit(self.hydra_profile)


    def select_point(self, pos, dx, dy):
        res = []
        for i, pt in enumerate(self.sz):
            dp = pos-pt
            if abs(dp[0]) < dx and abs(dp[1]) < dy :
                return array([pt])

        for x in self.limits :
            if (pos[0]-x)**2 < dx:
                return array([[x, 9999],[x, -9999]])

        # allow selection of topo
        if self.topo is not None:
            for i, pt in enumerate(self.topo):
                dp = pos-pt
                if abs(dp[0]) < dx and abs(dp[1]) < dy :
                    return array([pt])

        return None



class SmartValleyGeometry(Geometry):
    transect_changed = pyqtSignal(list)
    zb_changed = pyqtSignal(list)

    def __init__(self, sz, left, minor, right, zlevee_lb, zlevee_rb, transect_geom, transect_topo, topo_points, reach_point, terrain, dz, parent=None):
        # in un-coupled case, the first point of sz profile is the left limit of the floodplain, the
        # left bank, right bank and right limit of the floodplain are deduced from left, minor and right zb arrays

        super().__init__(sz, dz, None, None, parent)
        assert(minor is None or (len(minor) > 0 and left is not None and right is not None))

        self.__terrain = terrain
        self.__topo_points = topo_points
        self.__transect_topo = transect_topo
        self.__inter = reach_point
        self.__transect_geom = transect_geom
        self.__topo, self.__ref, self.__dem = project_topo(terrain, transect_geom, topo_points, transect_topo)

        self.__zlevee_lb = zlevee_lb
        self.__zlevee_rb = zlevee_rb

        self.__sz = array([]) # the target profile that the user can edit before offset by __dz
        self.__dz = dz
        self.__zb = [left, minor, right]

        if sz is not None:
            self.__sz = array(sz)
        else:
            if self.__topo is not None:
                self.__sz = self.__topo - self.__dz
            else:
                self.__sz = array([[-1,1],[0,0],[1,1]]) + array([self.center,0])

        self.fit()

    def set_zlevee(self, left, right):
        self.__zlevee_lb, self.__zlevee_rb = left, right

    def simplify(self):
        # add points to avoid becoming stupid
        self.__sz = concatenate([ [2*self.__simplified[0] - self.__simplified[1]], self.__simplified, [2*self.__simplified[-1] - self.__simplified[-2]]])
        self.zb_changed.emit(self.hydra_profile)

    def __getattr__(self, name):
        if name == 'simplified':
            return self.__simplified + array([0, self.__dz]) if self.__simplified is not None else None
        elif name == 'sz':
            return self.__sz + array([0, self.__dz])
        elif name == 'points':
            return self.__sz + array([0, self.__dz])
        elif name == 'dz':
            return self.__dz
        elif name == 'z_invert':
            return self.__zb[1][0][0] + self.__dz
        elif name == 'topo':
            return self.__topo
        elif name == 'dem':
            return self.__dem
        elif name == 'ref':
            return self.__ref
        elif name == 'hydra_profile':
            return self.__zb + [self.__sz.tolist()] #+ [self.__zlevee_lb, self.__zlevee_rb]
        elif name == 'limits':
            c = self.__transect_geom.project(self.__inter)
            p = [self.__transect_geom.project(Point(c)) for c in self.__transect_geom.coords]
            xlm = 0
            xrm = p[-1]
            for i, x in enumerate(p):
                if x >= c:
                    break
            xl = xlm if p[1] > c else p[i-1] # no left major
            xr = xrm if p[-2] < c else p[i] # no right major
            return xlm, xl, xr, xrm, c, i
        elif name == 'left_levee':
            #return None # TODO : remove this line
            if self.__simplified is None:
                return None
            p = self.__simplified[len(self.__zb[0])-1] + array([0, self.__dz])
            return array([p, array([p[0], max(self.__zlevee_lb  + self.__dz, p[1])])])
        elif name == 'right_levee':
            #return None # TODO : remove this line
            if self.__simplified is None:
                return None
            p = self.__simplified[-len(self.__zb[2])] + array([0, self.__dz])
            return array([p, array([p[0], max(self.__zlevee_rb + self.__dz, p[1])])])
        elif name == 'center':
            xl, xr = self.limits[1:3]
            return 0.5*(xl + xr)
        elif name == 'riverbed':
            xlm, xl, xr, xrm, c = self.limits[:5]
            return array([
                [xlm, -9999],
                [xlm, 9999],
                [xl, 9999],
                [xl, -9999],
                [c, -9999],
                [c, 9999],
                [xr, 9999],
                [xr, -9999],
                [xrm, -9999],
                [xrm, 9999]])
        elif name == 'transect':
            return list(self.__transect_geom.coords) if self.__transect_geom is not None else None
        elif name == 'water':
            #return self.__sz
            s = max(len(self.__zb[0]), 1)
            e = max(len(self.__zb[2]), 1)
            return self.__simplified[s:-e] + array([0, self.__dz])
        else:
            raise AttributeError(name)

    def set_dz(self, dz):
        self.__dz = dz

    def fit(self):
        # computes hydra profile from simplified geometry and banks
        sz = around(self.__sz, 3)
        l = LineString(sz)
        xlm, xl, xr, xrm = (round(x, 3) for x in self.limits[:4])

        plm = first_crossing(l, xlm) if xlm >= sz[0, 0] and xlm <= sz[-1, 0] else Point(sz[0])
        pl =  first_crossing(l, xl) if xl >= sz[0,0] and xl <= sz[-1, 0] else Point(sz[0])
        pr =  first_crossing(l, xr, is_reversed=True) if xr >= sz[0,0] and xr <= sz[-1, 0] else Point(sz[-1])
        prm = first_crossing(l, xrm, is_reversed=True) if xrm >= sz[0,0] and xrm <= sz[-1, 0] else Point(sz[-1])

        minor = substring(l, l.project(pl), l.project(pr))
        left = substring(l, l.project(plm), l.project(pl))
        right = substring(l, l.project(pr), l.project(prm))
        self.__zb, self.__simplified = simplified(left, minor, right)
        self.zb_changed.emit(self.hydra_profile)

    def move_point(self, p_orig, p):
        if len(p_orig) == 2: # we move banks
            xlm, xl, xr, xrm, c, i = self.limits
            l = array(self.__transect_geom.coords)
            ds = p[0] - p_orig[0,0]
            idx = None
            if p_orig[0,0] == xlm:
                d = l[1][:2] - l[0][:2]
                l[0,:2] += ds*(d/norm(d))
                self.__sz[:,0] -= ds
                idx = 0
            elif p_orig[0,0] == xrm:
                d = l[-1][:2] - l[-2][:2]
                l[-1,:2] += ds*(d/norm(d))
                idx = 8
            elif p_orig[0,0] == xl:
                if p[0] >= c or p[0] <= xlm: # remove major left
                    l = concatenate([l[0:i-1],l[i:]])
                    idx = None
                else:
                    d = l[i][:2] - l[i-1][:2]
                    l[i-1,:2] += ds*(d/norm(d))
                    idx = 2
            elif p_orig[0,0] == xr:
                if p[0] <= c or p[0] >= xrm: # remove major right
                    l = concatenate([l[0:i],l[i+1:]])
                    idx = None
                else:
                    d = l[i][:2] - l[i-1][:2]
                    l[i,:2] += ds*(d/norm(d))
                    idx = 6
            elif p_orig[0,0] == c:
                if xl == xlm and ds < 0: # create left major
                    d = l[1][:2] - l[0][:2]
                    l = concatenate([l[0:1], l])
                    l[1,:2] = (1-c/xr)*l[0,:2] + (c/xr)*l[i+1,:2] + ds*(d/norm(d))
                    idx = 2
                elif xr == xrm and ds > 0: # create left major
                    d = l[-1][:2] - l[-2][:2]
                    l = concatenate([l, l[-1:]])
                    l[-2,:2] = (1-c/xr)*l[0,:2] + (c/xr)*l[i,:2] + ds*(d/norm(d))
                    idx = 6
            self.__transect_geom = LineString(l)
            self.__topo, self.__ref, self.__dem = project_topo(self.__terrain, self.__transect_geom, self.__topo_points, self.__transect_topo)
            self.transect_changed.emit(list(l))
            self.fit()
            if idx is None:
                return None
            else:
                return self.riverbed[idx:idx+2]

        idx, = npwhere((self.sz == p_orig[0]).all(axis=1))
        if len(idx)>0:
            idx = idx[0]
        else:
            return p_orig
        self.__sz[idx] = p
        self.fit()
        return array([self.sz[idx]])

    def add_point(self, p):
        # find future point idx
        idx = 0
        for i, x in enumerate(self.sz[:,0]):
            if p[0] > x:
                idx = i+1
        self.__sz = concatenate((self.__sz[:idx], array([[p[0], p[1] - self.__dz]]), self.__sz[idx:]))
        self.fit()

    def is_minor(self, idx):
        return idx >= self.__leftBankIdx and idx <= self.__rightBankIdx

    def symetric_point(self, idx):
        return self.points[self.__bottomIdx - (idx - self.__bottomIdx)]

    def remove_point(self, point):
        if len(point) == 2: # remove riverbed
            l = array(self.__transect_geom.coords)
            xlm, xl, xr, xrm, c, i = self.limits
            if point[0][0] == xlm and xlm != xl: # remove major left
                l = l[1:]
                # offset all points
                self.__sz -= array([xl, 0])
            elif point[0][0] == xrm and xrm != xr: # remove major right
                l = l[:-1]
            elif point[0][0] == xl and xl != xlm: # remove major left
                l = concatenate([l[0:i-1],l[i:]])
            elif point[0][0] == xr and xr != xrm: # remove major right
                l = concatenate([l[0:i],l[i+1:]])
            self.__transect_geom = LineString(l)
            self.__topo, self.__ref, self.__dem = project_topo(self.__terrain, self.__transect_geom, self.__topo_points, self.__transect_topo)
            self.transect_changed.emit(list(l))
            self.fit()
            return
        else:
            point = point[0]

        if isinstance(point, int) or isinstance(point, int64):
            idx = point
        else:
            idx, = npwhere(npall(self.sz == point, axis=1))
            if len(idx)>0:
                idx = idx[0]
            else:
                return
        self.__sz = concatenate((self.__sz[0:idx], self.__sz[idx+1:]))
        self.fit()
        return

    def select_point(self, pos, dx, dy):
        for i, pt in enumerate(self.sz):
            dp = pos-pt
            if abs(dp[0]) < dx and abs(dp[1]) < dy:
                return array([pt])

        for x in self.limits[:5] :
            if (pos[0]-x)**2 < dx:
                return array([[x, 9999],[x, -9999]])

        # allow selection of topo
        for i, pt in enumerate(self.topo):
            dp = pos-pt
            if abs(dp[0]) < dx and abs(dp[1]) < dy:
                return array([pt])
        return None


class ValleyGeometry(Geometry):
    # this class is implemented as a decorator over SmartValleyGeometry and StupidValleyGeometry
    geometry_changed = pyqtSignal(dict)
    transect_changed = pyqtSignal(list)
    is_advanced = False

    def __init__(self, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, rlambda, rmu1, rmu2, zlevee_lb, zlevee_rb, sz_array, transect,
                transects, terrain, dz=0, fake_transect=None, parent=None):
        super().__init__(sz_array, dz, None, None, parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "valley_geometry.ui"), self)
        self.__decorated = None
        self.__fake_transect = fake_transect
        self.advanced.setChecked(ValleyGeometry.is_advanced)
        self.advanced.toggled.connect(self.__advanced_changed)

        self.rlambda.setValue(rlambda if rlambda is not None else .99)
        self.rmu1.setValue(rmu1 if rmu1 is not None else -999)
        self.rmu2.setValue(rmu2 if rmu2 is not None else -999)
        self.zlevee_lb.setValue(zlevee_lb if zlevee_lb is not None else -999)
        self.zlevee_rb.setValue(zlevee_rb if zlevee_rb is not None else -999)

        self.__zbmin_array = zbmin_array
        self.__zbmaj_lbank_array = zbmaj_lbank_array
        self.__zbmaj_rbank_array = zbmaj_rbank_array
        self.__sz_array = sz_array
        self.__dz = dz

        self.__zb_widget = None

        # self.minor_array = ArrayWidget(["z[m]", "b [m]"], [6,2], self.__zbmin_array, parent=self.minor_widget)
        # self.left_array = ArrayWidget(["z[m]", "b [m]"], [8,2], self.__zbmaj_lbank_array, parent=self.left_widget)
        # self.right_array = ArrayWidget(["z[m]", "b [m]"], [8,2], self.__zbmaj_rbank_array, parent=self.right_widget)
        #self.q_dp_array.data_edited.connect(self.__zb_changed)

        self.__terrain = terrain
        self.__transects = transects
        idx = None
        for i, t in transects.items():
            self.transect_combo.addItem(t['name'], userData=i)
            self.transect_combo.setItemData(self.transect_combo.count()-1, f"pk = {t['pk']:.3f}km", Qt.ToolTipRole)
            if transect == i:
                idx = self.transect_combo.count()-1
        self.transect_combo.addItem('')
        idx = idx if idx is not None else self.transect_combo.count()-1
        self.transect_combo.setCurrentIndex(idx)
        self.transect_combo.currentIndexChanged.connect(self.__transect_changed)
        self.__transect_changed(idx)

        self.autofit.toggled.connect(self.__autofit)
        self.simplify.clicked.connect(self.__simplify)
        self.reset.clicked.connect(self.__reset)

        self.rlambda.valueChanged.connect(self.__geometry_changed)
        self.rmu1.valueChanged.connect(self.__geometry_changed)
        self.rmu2.valueChanged.connect(self.__geometry_changed)
        self.zlevee_lb.valueChanged.connect(self.__geometry_changed)
        self.zlevee_rb.valueChanged.connect(self.__geometry_changed)

    def __advanced_changed(self, flag):
        ValleyGeometry.is_advanced = flag
        self.__set_arrays()

    def set_sz(self, sz):
        return self.__decorated.set_sz(sz)

    def set_dz(self, dz):
        return self.__decorated.set_dz(dz)

    def __getattr__(self, name):
        if name == 'param':
            return {
            'zbmin_array': self.__zbmin_array,
            'zbmaj_lbank_array': self.__zbmaj_lbank_array,
            'zbmaj_rbank_array': self.__zbmaj_rbank_array,
            'rlambda': self.rlambda.value(),
            'rmu1': self.rmu1.value(),
            'rmu2': self.rmu2.value(),
            'zlevee_lb': self.zlevee_lb.value(),
            'zlevee_rb': self.zlevee_rb.value(),
            'transect': self.transect_combo.itemData(self.transect_combo.currentIndex()),
            'sz_array': self.__sz_array
            }
        elif name == "invalidity":
            return invalidity(self.__zbmin_array, self.__zbmaj_lbank_array, self.__zbmaj_rbank_array)
        else:
            return getattr(self.__decorated, name)

    def add_point(self, p):
        return self.__decorated.add_point(p)

    def remove_point(self, idx):
        return self.__decorated.remove_point(idx)

    def select_point(self, pos, dx, dy):
        return self.__decorated.select_point(pos, dx, dy)

    def move_point(self, orig, dest):
        return self.__decorated.move_point(orig, dest)

    def __transect_changed(self, idx):
        t_id = self.transect_combo.itemData(idx)

        t = self.__transects[t_id] if t_id is not None else None
        transect_geom = wkb.loads(t['geom'], True) if t is not None and t['geom'] is not None else None
        transect_topo = wkb.loads(t['topo'], True) if t is not None and t['topo'] is not None else None
        points = wkb.loads(t['points'], True) if t is not None and t['points'] is not None else None
        reach_point = wkb.loads(t['inter'], True) if t is not None and t['inter'] is not None else None

        if transect_geom is None:
            self.autofit.hide()
            #self.reset.hide()
        else:
            self.autofit.show()
            #self.reset.show()

        left, minor, right = self.__zbmaj_lbank_array, self.__zbmin_array, self.__zbmaj_rbank_array
        sz, dz = self.__sz_array, self.__dz

        if (sz is None and str(minor) != '[[0, 0]]') or is_stupid(left, minor, right, sz) or transect_geom is None:
            if not is_stupid(left, minor, right, sz):
                sz = None
            self.__decorated = StupidValleyGeometry(
                sz, left, minor, right,
                self.zlevee_lb.value(), self.zlevee_rb.value(),
                transect_geom, transect_topo, points, self.__terrain, dz, self.__fake_transect, parent=self)
            self.simplify.hide()
            #self.reset.hide()
            self.autofit.blockSignals(True)
            self.autofit.setChecked(False)
            self.autofit.blockSignals(False)
        else:
            self.__decorated = SmartValleyGeometry(
                sz, left, minor, right,
                self.zlevee_lb.value(), self.zlevee_rb.value(),
                transect_geom, transect_topo, points, reach_point, self.__terrain, dz, parent=self)
            self.simplify.show()
            #self.reset.show()
            self.autofit.blockSignals(True)
            self.autofit.setChecked(True)
            self.autofit.blockSignals(False)

        self.__geometry_changed()

        if transect_geom is not None:
            self.transect_changed.emit(list(transect_geom.coords))
        elif self.__fake_transect is not None:
            self.transect_changed.emit(list(self.__fake_transect.coords))
        else:
            self.transect_changed.emit(list())

        self.__decorated.transect_changed.connect(self.transect_changed.emit)
        self.__decorated.zb_changed.connect(self.__geometry_changed)


    def __reset(self):
        self.__dz = 0
        self.__sz_array = None
        self.__zbmin_array = [[0,0]]
        self.__zbmaj_lbank_array = [[0,0]]
        self.__zbmaj_rbank_array = [[0,0]]
        self.__transect_changed(self.transect_combo.currentIndex())

    def __manual(self):
        self.__dz = 0
        self.__sz_array = None
        self.__transect_changed(self.transect_combo.currentIndex())

    def __simplify(self):
        self.__decorated.simplify()

    def __autofit(self, flag):
        if flag:
            self.__reset()
        else:
            self.__manual()

        self.__set_arrays()


    def set_zb_widget(self, w):
        self.__zb_widget = w
        self.__set_arrays()
        w.set_arrays(self.__zbmaj_lbank_array, self.__zbmin_array, self.__zbmaj_rbank_array)
        w.arrays_changed.connect(self.__set_geom_from_array)

    def __set_geom_from_array(self, a):
        self.__dz = 0
        self.__sz_array = None
        self.__zbmaj_lbank_array, self.__zbmin_array, self.__zbmaj_rbank_array = a
        self.__transect_changed(self.transect_combo.currentIndex())

    def __set_arrays(self):
        if self.autofit.isChecked() or not ValleyGeometry.is_advanced:
            if self.__zb_widget is not None:
                self.__zb_widget.setMaximumHeight(0)
            txt = '<table cellpadding="3" order="1">\n'
            for title, array in (('Riverbed', self.__zbmin_array), ('Left bank', self.__zbmaj_lbank_array), ('Right bank', self.__zbmaj_rbank_array)):
                txt+= f'<tr><th>{title}</th><th>z [m]</th><th>b [m]</th></tr>\n'
                for z, b in array:
                    txt += f'<tr><td></td><td style="text-align: right;">{z: 8.2f}</td><td style="text-align: right;">{b: 8.2f}</td></tr>\n'
            txt += '</table>'
        else:
            if self.__zb_widget is not None:
                self.__zb_widget.setMaximumHeight(200)
            txt = ""

        self.arrays.setText(txt)

    def __geometry_changed(self):
        self.__decorated.set_zlevee(self.zlevee_lb.value(), self.zlevee_rb.value())
        zbmaj_lbank_array, zbmin_array, zbmaj_rbank_array, sz_array = self.__decorated.hydra_profile
        self.__zbmin_array = zbmin_array
        self.__zbmaj_lbank_array = zbmaj_lbank_array
        self.__zbmaj_rbank_array = zbmaj_rbank_array
        self.__sz_array = sz_array
        # self.minor_array.set_table_items(self.__zbmin_array)
        # self.left_array.set_table_items(self.__zbmaj_lbank_array)
        # self.right_array.set_table_items(self.__zbmaj_rbank_array)
        self.__set_arrays()

        self.geometry_changed.emit(self.param)
