Developpement plugin quick install
==================================

Add the directory containing the hydra package to your PYTHONPATH. On \*nix::

    export PYTHONPATH=hydra_directory/..

For windows users::

    set PYTHONPATH hydra_directory\..

Then run::

    python -m hydra.utility.package -i








..
    modules
    hydra.mainlib
    hydra.gui.main
    hydra.gui
    hydra.mainlib.classes
    hydra.mainlib.database
    hydra.mainlib.utility

..
    REMARQUES LIEES A L'IMPLEMENTATION
    ==================================

    Le contenu de ce fichier sera parsé avec le module string.Template de python
    (lib standard), on préfixe donc d'un $ les variables à remplacer.

    On suppose que ce script permet de définir un modèle.  Un modèle est représenté
    par un schema.

    Le système de coordonné pour un modèle est supposé constant et noté $srid pour
    l'ensemble des éléments.

    Certaines tables sont communes à un ensemble de modèle, pour ne pas duppliquer
    l'information, on les stocke dans le schema commun "hydra" que l'on crée si il n'existe
    pas.

    /!\ la solution a base de check sur fonction n'empèche pas la modification
        du type d'un noeud existant, ça peut être un frein au typage des réseau 
        hydraulique/hydrologiques.
        
        Une solution serait de faire un trigger sur le on update qui ne permet
        que le passage hydraulique <-> hydrologique

    Finalement la solution a base de double clef primaire semble plus satisfaisant

    Il faut une vue pour grouper les containeurs particuliers et la géométrie du containeur 
    on peut rendre ces vues editables et mettre la mécanique de création dedans

    On utilise des vues éditables pour avoir accès à la géométrie et protéger
    des champs (e.g. "id" et "type" ne sont pas éditables).

    note: les noms sont en anglais, des abbréviations comme "pt" pour un profil en
    travers ne sont pas assez parlantes IMO, les noms de tables peuvent être plus
    long et plus explicite e.g. "transverse_profile"


    note: le nom des tables "privée" sont préfixées d'un underscore '_'
    il est possible de révoquer les droit d'édition de ces tables pour l'utilisateur
    tout en conservant les privilèges pour le trigger d'édition
    http://www.postgresql.org/docs/current/interactive/sql-createfunction.html#SQL-CREATEFUNCTION-SECURITY

               voilà un exemple:
               CREATE OR REPLACE FUNCTION public.trig_update_test()
                RETURNS trigger
                LANGUAGE plpgsql
                 SECURITY DEFINER
               AS $function$
               BEGIN
                 UPDATE test SET i = NEW.i;
                 RETURN NEW;
               END;
               $function$;
               CREATE TRIGGER trg_test_update INSTEAD OF UPDATE ON vtest FOR EACH ROW EXECUTE PROCEDURE trig_update_test();
               \c - moneypenny
               postgres=> update vtest set i = -1 ;
               avec un utilisateur moneypenny qui n'a pas le droit d'écrire sur la table test

Linux Install
=============

    # clone repo and cd top dir
    python3 -m venv venv
    . venv/bin/activate
    python3 -m pip install wheel # for psycopg2 and gdal
    python3 -m pip install -r hydra/requirements.txt
    history | grep ln
    ln -s /usr/lib/python3/dist-packages/PyQt5 venv/lib/python3.6/site-packages/ # qgis, PyQt and sip are a pain to install in venv
    ln -s /usr/lib/python3/dist-packages/sip* venv/lib/python3.6/site-packages/
