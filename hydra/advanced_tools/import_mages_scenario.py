# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
import MAGES scenario
"""

import re
import os
import string
import shutil
import datetime
import subprocess
from configparser import ConfigParser
from hydra.project import Project
from hydra.utility.string import get_sql_forced_float

settings_template = """date0 = '$debut',
                       model_connect_settings = 'cascade',
                       tfin_hr = (interval '00:01' * $tfin),
                       tsave_hr = (interval '00:01' * $tsave),
                       tini_hydrau_hr = (interval '00:01' * $tini),
                       dt_output_hr = (interval '00:01' * $dtsor),
                       tend_output_hr = (interval '00:01' * $tfin)"""

rstart_template = """
*RSTART
2
$rep_ini
$scenario_ini
"""

parametrage_template = """
*F_QTS
$qts_file

*F_PLUVIO
$plu_file

*F_DEROUT
$der_file

*F_REGUL
$dft_file
$ctl_file
$rtc_file
$zcrue_file
$crue_file
$cfg_file

*F_MES_DEB
$mes_deb_file

*OPTION_LECW14
$ioption_lecw14

*PINI3_CR
biev  5.  20  1.2  3.0  0.
d93  5.  20  1.2  3.0  0.
vl  5.  20  1.2  3.0  0.
sav  5.  20  1.2  3.0  0.
"""

plu_template = """prad2
$crb_txt
$plu
"""

qts_template = """$qts
$secteurs_txt
$cts_txt
"""

def _copy_file(src, dst, log=None):
    '''if dst is a dir, copy to file with same name as src in dst
    else copy file src to dst file'''
    if os.path.isdir(dst):
        dst = os.path.join(dst, os.path.basename(src))

    if os.path.isfile(src):
        shutil.copy(src, dst)
        if log:
            log.notice(f"Copied {os.path.basename(src)} to {dst}")
    elif src != 'nul':
        if log:
            log.error(f"File not found {src}")
            raise FileNotFoundError(f"File not found {src}")

def _make_hydra_dir(path):
    hydrol_dir = os.path.join(path, 'hydrol')
    hydrau_dir = os.path.join(path, 'hydraulique')

    if not os.path.exists(hydrol_dir):
        os.makedirs(hydrol_dir)
    if not os.path.exists(hydrau_dir):
        os.makedirs(hydrau_dir)

    return hydrol_dir, hydrau_dir

def import_mages_scenario(project, scenario_dir):
    scenario_name = os.path.basename(scenario_dir)
    scenario_path = project.get_senario_path_from_name(scenario_name)

    cmd_file = os.path.join(scenario_dir, f"{scenario_name}.cmd")
    if not os.path.isfile(cmd_file):
        project.log.error(f"Directory {scenario_dir} does not contain expected {scenario_name}.cmd file")
        raise ValueError(f"Directory {scenario_dir} does not contain expected {scenario_name}.cmd file")

    hydrol_dir, hydrau_dir = _make_hydra_dir(scenario_path)

    data_dir = os.path.join(project.data_dir, scenario_name)
    config_dir = os.path.join(data_dir, 'config')
    if not os.path.isdir(config_dir):
        os.makedirs(config_dir)


    project.execute(f"""insert into project.scenario(name) values ('{scenario_name}');""")

    cmd = ConfigParser()
    cmd.read(os.path.join(scenario_dir, f"{scenario_name}.cmd"))

    # SETTINGS
    parametrage_file = os.path.join(data_dir, 'parametrage.dat')
    project.execute(f"""update project.scenario
                        set {string.Template(settings_template).substitute(cmd['Commun'])},
                            flag_save = {'True' if cmd['Commun']['ioption_sav'] == '1' else 'False'},
                            option_file = True,
                            option_file_path = '{project.pack_path(parametrage_file)}'
                        where name = '{scenario_name}';""")

    # PARAMETRAGE
    substitutions = dict(cmd['Commun'])

    if cmd['Commun']['rep_ini'] != 'nul':
        substitutions['scenario_ini'] = f"{cmd['Commun']['scenario_ini']}R"
        substitutions['rep_ini'] = os.path.join(project.directory, f"{cmd['Commun']['scenario_ini']}R")

    for key in cmd['Fichiers_Hydrol']:
        if key in ['plu', 'qts']:
            substitutions[f'{key}_file'] = os.path.join(data_dir, f"fich_{'pluvio' if key =='plu' else 'qts'}")
        if key == 'der':
            substitutions[f'{key}_file'] = os.path.join(data_dir, os.path.basename(cmd['Fichiers_Hydrol'][key]))
        # Copy file from .cmd file's dir if possible else  from path in .cmd file
        if os.path.isfile(os.path.join(scenario_dir, os.path.basename(cmd['Fichiers_Hydrol'][key]))):
            _copy_file(os.path.join(scenario_dir, os.path.basename(cmd['Fichiers_Hydrol'][key])), os.path.join(data_dir, os.path.basename(cmd['Fichiers_Hydrol'][key])), log=project.log)
        else:
            _copy_file(cmd['Fichiers_Hydrol'][key], os.path.join(data_dir, os.path.basename(cmd['Fichiers_Hydrol'][key])), log=project.log)

    for key in cmd['Fichiers_Hydrau']:
        if  key in ['dft', 'ctl']:
            substitutions[f'{key}_file'] = os.path.join(config_dir, os.path.basename(cmd['Fichiers_Hydrau'][key])) if cmd['Fichiers_Hydrau'][key] != 'nul' else 'nul'
        else:
            substitutions[f'{key}_file'] = os.path.join(data_dir, os.path.basename(cmd['Fichiers_Hydrau'][key])) if cmd['Fichiers_Hydrau'][key] != 'nul' else 'nul'
        # Copy file from .cmd file's dir if possible else  from path in .cmd file
        if os.path.isfile(os.path.join(scenario_dir, os.path.basename(cmd['Fichiers_Hydrau'][key]))):
            _copy_file(os.path.join(scenario_dir, os.path.basename(cmd['Fichiers_Hydrau'][key])), substitutions[f'{key}_file'], log=project.log)
        else:
            _copy_file(cmd['Fichiers_Hydrau'][key], substitutions[f'{key}_file'], log=project.log)

    with open(parametrage_file, 'w') as f:
        if cmd['Commun']['rep_ini'] != 'nul':
            rstart_text = string.Template(rstart_template).substitute(substitutions)
        else:
            rstart_text = ''
        parametrage_text = string.Template(parametrage_template).substitute(substitutions)
        f.write(f"{rstart_text}\n{parametrage_text}")
        project.log.notice(f"File {parametrage_file}:\n\n{parametrage_text}")

    # REP INI
    if cmd['Commun']['rep_ini'] != 'nul':
        rep_ini_hydrol_dir, rep_ini_hydrau_dir = _make_hydra_dir(substitutions['rep_ini'])
        for file in os.listdir(cmd['Commun']['rep_ini']):
            file_path = os.path.join(cmd['Commun']['rep_ini'], file)
            if os.path.isfile(file_path):
                if '_hydrol' in file:
                    _copy_file(file_path, os.path.join(rep_ini_hydrol_dir, file.replace(cmd['Commun']['scenario_ini'], substitutions['scenario_ini'])), log=project.log)
                else:
                    _copy_file(file_path, os.path.join(rep_ini_hydrau_dir, file.replace(cmd['Commun']['scenario_ini'], substitutions['scenario_ini'])), log=project.log)

    # Fichier PLU
    plu_file = substitutions['plu_file']
    if os.path.isfile(cmd['Fichiers_Hydrol']['plu']):
        with open(cmd['Fichiers_Hydrol']['plu'], 'r') as f:
            lines = f.readlines()
            if len(lines) > 2:
                sub_file = lines[2].strip()
                # Copy file from .cmd file's dir if possible else  from path in .cmd file
                if os.path.isfile(os.path.join(scenario_dir, os.path.basename(sub_file))):
                    _copy_file(os.path.join(scenario_dir, os.path.basename(sub_file)), data_dir, log=project.log)
                else:
                    _copy_file(sub_file, data_dir, log=project.log)
            else:
                project.log.error(f"Error reading content of file {cmd['Fichiers_Hydrol']['plu']}")
                raise ValueError(f"Error reading content of file {cmd['Fichiers_Hydrol']['plu']}")
    else:
        project.log.error(f"File not found {cmd['Fichiers_Hydrol']['plu']}")
        raise FileNotFoundError(f"File not found {cmd['Fichiers_Hydrol']['plu']}")

    plu_subs = {'crb_txt': os.path.join(config_dir, 'crb.txt'),
                     'plu': os.path.join(data_dir, os.path.basename(sub_file))}
    with open(plu_file, 'w') as f:
        plu_text = string.Template(plu_template).substitute(plu_subs)
        f.write(plu_text)
        project.log.notice(f"File {plu_file}:\n{plu_text}")

    # Fichier QTS
    qts_file = substitutions['qts_file']
    qts_subs = {'qts': os.path.join(data_dir, os.path.basename(cmd['Fichiers_Hydrol']['qts'])),
                'secteurs_txt': os.path.join(config_dir, 'secteurs.txt'),
                'cts_txt': os.path.join(config_dir, 'cts.txt')}
    with open(qts_file, 'w') as f:
        qts_text = string.Template(qts_template).substitute(qts_subs)
        f.write(qts_text)
        project.log.notice(f"File {qts_file}:\n{qts_text}")

    # Long / short format
    if cmd['Commun']['ioption_lecW14'] == '1':
        res_dir = os.path.join(scenario_dir, 'res')
        long_dir = os.path.join(scenario_dir, 'res_long')
        if not os.path.exists(long_dir):
            os.makedirs(long_dir)
        environ = os.environ.copy()
        environ['PATH'] = f"{os.path.dirname(__file__)};{environ['PATH']};"
        for file in os.listdir(res_dir):
            file_path = os.path.join(res_dir, file)
            if os.path.isfile(file_path):
                if file.split('.')[-1].lower() in ['w13', 'w14', 'w15']:
                    if os.name == 'nt' and subprocess.call("where convert_w14w15_to_long", env=environ) == 1:
                        project.log.error("convert_w14w15_to_long.exe not found")
                        raise FileNotFoundError("convert_w14w15_to_long.exe not found")
                    params = ["convert_w14w15_to_long", file_path, os.path.join(scenario_dir, 'res_long', os.path.basename(file))]
                    subprocess.run(params, env=environ, shell=True)
                    project.log.notice(f"Command: {' '.join(params)}")

    # COPY RESULT FILES
    res_long_dir = os.path.join(scenario_dir, 'res_long')
    for file in os.listdir(res_long_dir):
        file_path = os.path.join(res_long_dir, file)
        if os.path.isfile(file_path):
            if file.split('.')[-1].lower() in ['app', 'w13']:
                _copy_file(file_path, hydrol_dir, log=project.log)
            elif file.split('.')[-1].lower() in ['w14', 'w15']:
                _copy_file(file_path, hydrau_dir, log=project.log)

    # CONFIG FILES
    old_config_dir = os.path.join(scenario_dir, '..', '..', 'Config')
    for file in os.listdir(old_config_dir):
        file_path = os.path.join(old_config_dir, file)
        # Copy file from .cmd file's dir if possible else  from path in .cmd file
        if os.path.isfile(os.path.join(scenario_dir, os.path.basename(file_path))):
            _copy_file(os.path.join(scenario_dir, os.path.basename(file_path)), config_dir, log=project.log)
        else:
            _copy_file(file_path, config_dir, log=project.log)


if __name__ == '__main__':
    import sys
    import getopt
    from shutil import rmtree
    from hydra.utility import empty_ui_manager
    from hydra.utility.log import LogManager, ConsoleLogger

    try:
        optlist, args = getopt.getopt(sys.argv[1:],
                "hd",
                ["help", "debug"])
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        exit(1)

    optlist = dict(optlist)

    if "-h" in optlist or "--help" in optlist:
        help(sys.modules[__name__])
        exit(0)

    if "-d" in optlist or "--debug" in optlist:
        debug=True
    else:
        debug=False


    project_name = args[0]
    scenario_dir = args[1]

    project = Project(project_name, debug=debug)

    scenario_name = os.path.basename(scenario_dir)
    project.execute(f"delete from project.scenario where name='{scenario_name}';")
    rmtree(project.get_senario_path_from_name(scenario_name))
    rmtree(project.data_dir)

    import_mages_scenario(project, scenario_dir)

    project.commit()
