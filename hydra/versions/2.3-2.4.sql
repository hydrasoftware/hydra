/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update hydra.metadata set version = '2.4';
;;

create or replace function project.model_instead_fct()
returns trigger
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_model
    for statement in create_model(TD['new']['name'], $srid, '$version'):
        plpy.execute(statement)
$$$$
;;

do
$$$$
    begin
    if exists (
        select 1 from pg_catalog.pg_namespace where nspname = 'csv'
    )
    then
      execute 'insert into csv.hydra_compatible_versions(hydra_version) values (''2.4'')';
    end if;
end
$$$$;
;;

/* ********************************************** */
/*  Thiessen                                      */
/* ********************************************** */

--isolated--
alter type hydra_rainfall_interpolation_type add value if not exists 'thiessen';
;;

insert into hydra.rainfall_interpolation_type (id, name) values (3, 'thiessen');
;;

/* ********************************************** */
/*  Affin option                                  */
/* ********************************************** */

create type public.hydra_affin_option as enum('affin', 'affin_dry', 'inertia_only');
;;

create table hydra.affin_option(
    name hydra_affin_option primary key,
    id integer unique,
    description varchar
);

insert into hydra.affin_option(name, id, description)
values ('affin', 1, 'Affin (inertia convective terms)'),
       ('affin_dry', 2, 'Affin with dry land'),
       ('inertia_only', 3, 'Inertia terms only');
;;

alter table project.c_affin_param add column affin_option hydra_affin_option not null default 'affin';
;;

alter table project.c_affin_param_serie add column affin_option hydra_affin_option not null default 'affin';
;;

/* ********************************************** */
/*  Scenario hydrol ini conditions                */
/* ********************************************** */

alter table project.scenario add column soil_cr_alp real default 1 check(soil_cr_alp >= 0);
alter table project.scenario add column soil_horner_sat real default 0 check(soil_horner_sat >= 0 and soil_horner_sat <=1);
alter table project.scenario add column soil_holtan_sat real default 0 check(soil_holtan_sat >= 0 and soil_holtan_sat <=1);
alter table project.scenario add column soil_scs_sat real default 0 check(soil_scs_sat >= 0 and soil_scs_sat <=1);
alter table project.scenario add column soil_hydra_psat real default 0 check(soil_hydra_psat >= 0 and soil_hydra_psat <=1);
alter table project.scenario add column soil_hydra_rsat real default 0 check(soil_hydra_rsat >= 0 and soil_hydra_rsat <=1);
alter table project.scenario add column soil_gr4_psat real default 0 check(soil_gr4_psat >= 0 and soil_gr4_psat <=1);
alter table project.scenario add column soil_gr4_rsat real default 0 check(soil_gr4_rsat >= 0 and soil_gr4_rsat <=1);
;;

update project.scenario set
    soil_cr_alp = GREATEST(runoff_adjust_coef, 0),
    soil_horner_sat = GREATEST(LEAST(soil_moisture_coef, 1), 0),
    soil_holtan_sat = GREATEST(LEAST(soil_moisture_coef, 1), 0),
    soil_scs_sat = GREATEST(LEAST(soil_moisture_coef, 1), 0),
    soil_hydra_psat = GREATEST(LEAST((FLOOR(soil_moisture_coef*100))::float/100, 1), 0),
    soil_hydra_rsat = GREATEST(LEAST((FLOOR((soil_moisture_coef*100 - FLOOR(soil_moisture_coef*100))*100))::float/100, 1), 0),
    soil_gr4_psat = GREATEST(LEAST((FLOOR(soil_moisture_coef*100))::float/100, 1), 0),
    soil_gr4_rsat = GREATEST(LEAST((FLOOR((soil_moisture_coef*100 - FLOOR(soil_moisture_coef*100))*100))::float/100, 1), 0);
;;

alter table project.scenario drop column if exists soil_moisture_coef;
alter table project.scenario drop column if exists runoff_adjust_coef;
;;

/* ********************************************** */
/*  Serie hydrol ini conditions                   */
/* ********************************************** */

drop view project.serie_scenario
;;

alter table project.serie_bloc add column soil_cr_alp real default 1 check(soil_cr_alp >= 0);
alter table project.serie_bloc add column soil_horner_sat real default 0 check(soil_horner_sat >= 0);
alter table project.serie_bloc add column soil_holtan_sat real default 0 check(soil_holtan_sat >= 0 and soil_holtan_sat <=1);
alter table project.serie_bloc add column soil_scs_sat real default 0 check(soil_scs_sat >= 0 and soil_scs_sat <=1);
alter table project.serie_bloc add column soil_hydra_psat real default 0 check(soil_hydra_psat >= 0 and soil_hydra_psat <=1);
alter table project.serie_bloc add column soil_hydra_rsat real default 0 check(soil_hydra_rsat >= 0 and soil_hydra_rsat <=1);
alter table project.serie_bloc add column soil_gr4_psat real default 0 check(soil_gr4_psat >= 0 and soil_gr4_psat <=1);
alter table project.serie_bloc add column soil_gr4_rsat real default 0 check(soil_gr4_rsat >= 0 and soil_gr4_rsat <=1);
;;

update project.serie_bloc set
    soil_cr_alp = GREATEST(runoff_adjust_coef, 0),
    soil_horner_sat = GREATEST(runoff_adjust_coef, 0),
    soil_holtan_sat = GREATEST(LEAST(soil_moisture_coef, 1), 0),
    soil_scs_sat = GREATEST(LEAST(soil_moisture_coef, 1), 0),
    soil_hydra_psat = GREATEST(LEAST((FLOOR(soil_moisture_coef*100))::float/100, 1), 0),
    soil_hydra_rsat = GREATEST(LEAST((FLOOR((soil_moisture_coef*100 - FLOOR(soil_moisture_coef*100))*100))::float/100, 1), 0),
    soil_gr4_psat = GREATEST(LEAST((FLOOR(soil_moisture_coef*100))::float/100, 1), 0),
    soil_gr4_rsat = GREATEST(LEAST((FLOOR((soil_moisture_coef*100 - FLOOR(soil_moisture_coef*100))*100))::float/100, 1), 0);
;;

alter table project.serie_bloc drop column if exists soil_moisture_coef;
alter table project.serie_bloc drop column if exists runoff_adjust_coef;
;;

create or replace view project.serie_scenario as (
with
    tfin_bloc as (
        select min(b2.t_till_start) as tfin, b1.id
        from project.serie_bloc as b1, project.serie_bloc as b2
        where b2.t_till_start>b1.t_till_start
        and b2.id_cs = b1.id_cs
        group by b1.id
            union
        select s.tfin as tfin, b.id
        from project.serie as s, project.serie_bloc as b
        where b.id not in (
            select b1.id
            from project.serie_bloc as b1, project.serie_bloc as b2
            where b2.t_till_start>b1.t_till_start
            and b2.id_cs = b1.id_cs
            group by b1.id)
        and b.id_cs = s.id
    ),
    serie_bloc as (
        select s.id as id_cs,
            s.dt_days,
            s.tfin as tfin_cs,
            s.date0 as date0_cs,
            b.t_till_start as t_till_start_b,
            tfb.tfin as tfin_b,
            DATE_PART('day',tfb.tfin - b.t_till_start) as duration_days,
            DATE_PART('day',tfb.tfin - b.t_till_start)::int % s.dt_days as last_scn_duration,
            CEIL(DATE_PART('day',tfb.tfin - b.t_till_start)/s.dt_days)::int as nb_scn,
            b.id as id_b
        from project.serie as s, project.serie_bloc as b, tfin_bloc as tfb
        where s.id = b.id_cs and tfb.id = b.id
    ),
    serie_bloc_prev as (
        select
            sb1.*,
            sb2.id_b as id_prev,
            sb2.nb_scn as nb_prev,
            sb2.last_scn_duration as last_scn_prev
            from
            serie_bloc as sb1
            left join serie_bloc as sb2
            on sb1.t_till_start_b = sb2.tfin_b
            and sb1.id_cs = sb2.id_cs
	),
    gen as (
        select generate_series(1, nb_scn) as i, id_b from serie_bloc
    ),
    scn_gen as (
        select gen.i,
            (gen.i-1)*sb.dt_days as t_start,
            (gen.i)*sb.dt_days as t_fin,
            sb.dt_days as scn_duration,
            sb.id_b,
            sb.last_scn_duration,
            sb.nb_scn,
            sb.id_prev,
            sb.nb_prev,
            sb.last_scn_prev
        from serie_bloc_prev as sb, gen
        where gen.id_b = sb.id_b
        order by id_b,i
    ),
    scn_data as (
        select
            s.id as id_cs,
            b.id as id_b,
            scn_gen.i as index,
            scn_gen.t_start *'1 days'::interval + b.t_till_start as _tb_start,
            s.name || '_' || b.name || '_SCN' || to_char(scn_gen.i,'FM0000') as name,
            '' as comment,
            s.comput_mode as comput_mode,
            b.dry_inflow as dry_inflow,
            b.rainfall as rainfall,
            s.dt_hydrol_mn as dt_hydrol_mn,
            b.soil_cr_alp as soil_cr_alp,
            b.soil_horner_sat as soil_horner_sat,
            b.soil_holtan_sat as soil_holtan_sat,
            b.soil_scs_sat as soil_scs_sat,
            b.soil_hydra_psat as soil_hydra_psat,
            b.soil_hydra_rsat as soil_hydra_rsat,
            b.soil_gr4_psat as soil_gr4_psat,
            b.soil_gr4_rsat as soil_gr4_rsat,
            b.option_dim_hydrol_network as option_dim_hydrol_network,
            scn_gen.t_start *'1 days'::interval + b.t_till_start + s.date0 as date0,
            scn_gen.scn_duration * '24 hours'::interval as tfin_hr,
            s.tini_hydrau_hr as tini_hydrau_hr,
            s.dtmin_hydrau_hr as dtmin_hydrau_hr,
            s.dtmax_hydrau_hr as dtmax_hydrau_hr,
            s.dzmin_hydrau as dzmin_hydrau,
            s.dzmax_hydrau as dzmax_hydrau,
            s.dt_output_hr as dt_output_hr,
            't'::bool as flag_save,
            scn_gen.scn_duration * '24 hours'::interval as tsave_hr,
            case
                when scn_gen.i = 1 and scn_gen.id_prev is null then 'f'::bool
                else 't'::bool
            end as flag_rstart,
            b.flag_gfx_control as flag_gfx_control,
            case
                when scn_gen.i = 1 and scn_gen.id_prev is null then ARRAY[]::integer[]
                when scn_gen.i = 1 and scn_gen.id_prev is not null then ARRAY[s.id, scn_gen.id_prev, scn_gen.nb_prev]
                else ARRAY[s.id, b.id, scn_gen.i-1]
            end as scenario_rstart_array,
            'f'::bool as scenario_rstart,
            b.trstart_hr as trstart_hr,
            b.flag_hydrology_rstart as flag_hydrology_rstart,
            b.scenario_hydrology_rstart as scenario_hydrology_rstart,
           'f' as flag_hydrology_auto_dimensioning,
            'f'::bool as graphic_control,
            s.model_connect_settings as model_connect_settings,
            '00:00:00'::interval as tbegin_output_hr,
            scn_gen.scn_duration * '24 hours'::interval as tend_output_hr,
            s.c_affin_param as c_affin_param,
            s.domain_2d_param as domain_2d_param,
            s.t_affin_start_hr as t_affin_start_hr,
            s.t_affin_min_surf_ddomains as t_affin_min_surf_ddomains,
            s.strickler_param as strickler_param,
            s.option_file as option_file,
            s.option_file_path as option_file_path,
            s.output_option as output_option,
            s.sor1_mode as sor1_mode,
            s.dt_sor1_hr as dt_sor1_hr,
            s.transport_type as transport_type,
            s.iflag_mes as iflag_mes,
            s.iflag_dbo5 as iflag_dbo5,
            s.iflag_dco as iflag_dco,
            s.iflag_ntk as iflag_ntk,
            s.quality_type as quality_type,
            s.diffusion as diffusion,
            s.dispersion_coef as dispersion_coef,
            s.longit_diffusion_coef as longit_diffusion_coef,
            s.lateral_diff_coef as lateral_diff_coef,
            s.water_temperature_c as water_temperature_c,
            s.min_o2_mgl as min_o2_mgl,
            s.kdbo5_j_minus_1 as kdbo5_j_minus_1,
            s.koxygen_j_minus_1 as koxygen_j_minus_1,
            s.knr_j_minus_1 as knr_j_minus_1,
            s.kn_j_minus_1 as kn_j_minus_1,
            s.bdf_mgl as bdf_mgl,
            s.o2sat_mgl as o2sat_mgl,
            s.iflag_ecoli as iflag_ecoli,
            s.iflag_enti as iflag_enti,
            s.iflag_ad1 as iflag_ad1,
            s.iflag_ad2 as iflag_ad2,
            s.ecoli_decay_rate_jminus_one as ecoli_decay_rate_jminus_one,
            s.enti_decay_rate_jminus_one as enti_decay_rate_jminus_one,
            s.ad1_decay_rate_jminus_one as ad1_decay_rate_jminus_one,
            s.ad2_decay_rate_jminus_one as ad2_decay_rate_jminus_one,
            b.sst_mgl as sst_mgl,
            s.sediment_file as sediment_file,
            s.suspended_sediment_fall_velocity_mhr as suspended_sediment_fall_velocity_mhr,
            s.scenario_ref as scenario_ref ,
            b.scenario_hydrol as scenario_hydrol,
            s.kernel_version as kernel_version
        from
            project.serie as s,
            project.serie_bloc as b,
            scn_gen
        where
            s.id = b.id_cs and
            scn_gen.id_b = b.id and
            scn_gen.t_fin != ((scn_gen.nb_scn) * scn_gen.scn_duration)
                union
        select
            s.id as id_cs,
            b.id as id_b,
            scn_gen.i+1 as index,
            scn_gen.t_fin *'1 days'::interval + b.t_till_start as _tb_start,
            s.name || '_' || b.name || '_SCN' || to_char(scn_gen.i+1,'FM0000') as name,
            '' as comment,
            s.comput_mode as comput_mode,
            b.dry_inflow as dry_inflow,
            b.rainfall as rainfall,
            s.dt_hydrol_mn as dt_hydrol_mn,
            b.soil_cr_alp as soil_cr_alp,
            b.soil_horner_sat as soil_horner_sat,
            b.soil_holtan_sat as soil_holtan_sat,
            b.soil_scs_sat as soil_scs_sat,
            b.soil_hydra_psat as soil_hydra_psat,
            b.soil_hydra_rsat as soil_hydra_rsat,
            b.soil_gr4_psat as soil_gr4_psat,
            b.soil_gr4_rsat as soil_gr4_rsat,
            b.option_dim_hydrol_network as option_dim_hydrol_network,
            scn_gen.t_fin *'1 days'::interval + b.t_till_start + s.date0 as date0,
            case
                when scn_gen.last_scn_duration = 0 then scn_gen.scn_duration * '24 hours'::interval
                else scn_gen.last_scn_duration * '24 hours'::interval
            end as tfin_hr,
            s.tini_hydrau_hr as tini_hydrau_hr,
            s.dtmin_hydrau_hr as dtmin_hydrau_hr,
            s.dtmax_hydrau_hr as dtmax_hydrau_hr,
            s.dzmin_hydrau as dzmin_hydrau,
            s.dzmax_hydrau as dzmax_hydrau,
            s.dt_output_hr as dt_output_hr,
            't'::bool as flag_save,
            case
                when scn_gen.last_scn_duration = 0 then scn_gen.scn_duration * '24 hours'::interval
                else scn_gen.last_scn_duration * '24 hours'::interval
            end as tsave_hr,
            case
                when scn_gen.i = 1 and scn_gen.id_prev is null then 'f'::bool
                else 't'::bool
            end as flag_rstart,
            b.flag_gfx_control as flag_gfx_control,
            case
                when scn_gen.i = 1 and scn_gen.id_prev is null then ARRAY[]::integer[]
                when scn_gen.i = 1 and scn_gen.id_prev is not null then ARRAY[s.id, scn_gen.id_prev, scn_gen.nb_prev]
                else ARRAY[s.id, b.id, scn_gen.i]
            end as scenario_rstart_array,
            'f'::bool as scenario_rstart,
            b.trstart_hr as trstart_hr,
            b.flag_hydrology_rstart as flag_hydrology_rstart,
            b.scenario_hydrology_rstart as scenario_hydrology_rstart,
           'f' as flag_hydrology_auto_dimensioning,
            'f'::bool as graphic_control,
            s.model_connect_settings as model_connect_settings,
            '00:00:00'::interval as tbegin_output_hr,
            scn_gen.scn_duration * '24 hours'::interval as tend_output_hr,
            s.c_affin_param as c_affin_param,
            s.domain_2d_param as domain_2d_param,
            s.t_affin_start_hr as t_affin_start_hr,
            s.t_affin_min_surf_ddomains as t_affin_min_surf_ddomains,
            s.strickler_param as strickler_param,
            s.option_file as option_file,
            s.option_file_path as option_file_path,
            s.output_option as output_option,
            s.sor1_mode as sor1_mode,
            s.dt_sor1_hr as dt_sor1_hr,
            s.transport_type as transport_type,
            s.iflag_mes as iflag_mes,
            s.iflag_dbo5 as iflag_dbo5,
            s.iflag_dco as iflag_dco,
            s.iflag_ntk as iflag_ntk,
            s.quality_type as quality_type,
            s.diffusion as diffusion,
            s.dispersion_coef as dispersion_coef,
            s.longit_diffusion_coef as longit_diffusion_coef,
            s.lateral_diff_coef as lateral_diff_coef,
            s.water_temperature_c as water_temperature_c,
            s.min_o2_mgl as min_o2_mgl,
            s.kdbo5_j_minus_1 as kdbo5_j_minus_1,
            s.koxygen_j_minus_1 as koxygen_j_minus_1,
            s.knr_j_minus_1 as knr_j_minus_1,
            s.kn_j_minus_1 as kn_j_minus_1,
            s.bdf_mgl as bdf_mgl,
            s.o2sat_mgl as o2sat_mgl,
            s.iflag_ecoli as iflag_ecoli,
            s.iflag_enti as iflag_enti,
            s.iflag_ad1 as iflag_ad1,
            s.iflag_ad2 as iflag_ad2,
            s.ecoli_decay_rate_jminus_one as ecoli_decay_rate_jminus_one,
            s.enti_decay_rate_jminus_one as enti_decay_rate_jminus_one,
            s.ad1_decay_rate_jminus_one as ad1_decay_rate_jminus_one,
            s.ad2_decay_rate_jminus_one as ad2_decay_rate_jminus_one,
            b.sst_mgl as sst_mgl,
            s.sediment_file as sediment_file,
            s.suspended_sediment_fall_velocity_mhr as suspended_sediment_fall_velocity_mhr,
            s.scenario_ref as scenario_ref,
            b.scenario_hydrol as scenario_hydrol,
            s.kernel_version as kernel_version
        from
            project.serie as s,
            project.serie_bloc as b,
            scn_gen
        where
            s.id = b.id_cs and
            scn_gen.id_b = b.id and
            scn_gen.t_fin = ((scn_gen.nb_scn-1) * scn_gen.scn_duration)
                union
        select s.id as id_cs,
            b.id as id_b,
            scn_gen.i + 1 as index,
            scn_gen.t_fin::double precision * '1 day'::interval + b.t_till_start - scn_gen.scn_duration * '1 day'::interval as _tb_start,
            (((s.name::text || '_'::text) || b.name::text) || '_SCN'::text) || to_char(scn_gen.i, 'FM0000'::text) as name,
            ''::text as comment,
            s.comput_mode,
            b.dry_inflow,
            b.rainfall,
            s.dt_hydrol_mn,
            b.soil_cr_alp,
            b.soil_horner_sat,
            b.soil_holtan_sat,
            b.soil_scs_sat,
            b.soil_hydra_psat,
            b.soil_hydra_rsat,
            b.soil_gr4_psat,
            b.soil_gr4_rsat,
            b.option_dim_hydrol_network,
            scn_gen.t_fin::double precision * '1 day'::interval + b.t_till_start + s.date0 - scn_gen.scn_duration * '1 day'::interval as date0,
            case
                when scn_gen.last_scn_duration = 0 then scn_gen.scn_duration::double precision * '24:00:00'::interval
                else scn_gen.last_scn_duration::double precision * '24:00:00'::interval
            end as tfin_hr,
            s.tini_hydrau_hr,
            s.dtmin_hydrau_hr,
            s.dtmax_hydrau_hr,
            s.dzmin_hydrau,
            s.dzmax_hydrau,
            s.dt_output_hr,
            true as flag_save,
            case
                when scn_gen.last_scn_duration = 0 then scn_gen.scn_duration::double precision * '24:00:00'::interval
                else scn_gen.last_scn_duration::double precision * '24:00:00'::interval
            end as tsave_hr,
            case
                when scn_gen.i = 1 and scn_gen.id_prev is null then false
                else true
            end as flag_rstart,
            b.flag_gfx_control,
            case
                when scn_gen.i = 1 and scn_gen.id_prev is null then array[]::integer[]
                when scn_gen.i = 1 and scn_gen.id_prev is not null then array[s.id, scn_gen.id_prev, scn_gen.nb_prev]
                else array[s.id, b.id, scn_gen.i]
            end as scenario_rstart_array,
            false as scenario_rstart,
            b.trstart_hr,
            b.flag_hydrology_rstart,
            b.scenario_hydrology_rstart,
           'f' as flag_hydrology_auto_dimensioning,
            false as graphic_control,
            s.model_connect_settings,
            '00:00:00'::interval as tbegin_output_hr,
            scn_gen.scn_duration::double precision * '24:00:00'::interval as tend_output_hr,
            s.c_affin_param,
            s.domain_2d_param,
            s.t_affin_start_hr,
            s.t_affin_min_surf_ddomains,
            s.strickler_param,
            s.option_file,
            s.option_file_path,
            s.output_option,
            s.sor1_mode,
            s.dt_sor1_hr,
            s.transport_type,
            s.iflag_mes,
            s.iflag_dbo5,
            s.iflag_dco,
            s.iflag_ntk,
            s.quality_type,
            s.diffusion,
            s.dispersion_coef,
            s.longit_diffusion_coef,
            s.lateral_diff_coef,
            s.water_temperature_c,
            s.min_o2_mgl,
            s.kdbo5_j_minus_1,
            s.koxygen_j_minus_1,
            s.knr_j_minus_1,
            s.kn_j_minus_1,
            s.bdf_mgl,
            s.o2sat_mgl,
            s.iflag_ecoli,
            s.iflag_enti,
            s.iflag_ad1,
            s.iflag_ad2,
            s.ecoli_decay_rate_jminus_one,
            s.enti_decay_rate_jminus_one,
            s.ad1_decay_rate_jminus_one,
            s.ad2_decay_rate_jminus_one,
            b.sst_mgl,
            s.sediment_file,
            s.suspended_sediment_fall_velocity_mhr,
            s.scenario_ref,
            b.scenario_hydrol,
            s.kernel_version as kernel_version
           from project.serie s,
            project.serie_bloc b,
            scn_gen
          where s.id = b.id_cs and scn_gen.id_b = b.id and scn_gen.nb_scn = 1
    )
    select
        row_number() over(order by id_cs, id_b, index) as id,
        scn_data.*
    from scn_data
    )
;;