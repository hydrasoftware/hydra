# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QMainWindow, QGridLayout
from hydra.gui.base_dialog import BaseDialog, tr
from hydra.gui.widgets.graph_widget import GraphWidget
from hydra.gui.visu_graph import VisuGraph
from hydra.database.radar_rain import RainVrt

class CatchmentContourEditor(BaseDialog):
    def __init__(self, project, contour, id=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "catchment_contour.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.__model = self.project.get_current_model()
        assert not self.__model == None
        self.__contour = contour
        if id==None:
            self.__id = self.__model.add_catchment_contour(self.__contour, name=None)
        else:
            self.__id = id
            if self.__contour is None:
                contour,= self.project.execute("""
                select geom
                from {}.catchment
                where id={}""".format(self.__model.name, self.__id)).fetchone()

        name = self.project.execute("""
            select name
            from {}.catchment
            where id={}""".format(self.__model.name, self.__id)).fetchone()[0]
        self.name.setText(name)

        radar_rains = self.project.execute("""select id, name
                                            from project.radar_rainfall
                                            order by id asc""").fetchall()

        self.btn_hyeto.clicked.connect(self.display_hyeto)
        if len(radar_rains)>0:
            for radar_rain in radar_rains:
                self.radar_rain_combo.addItem('{}: {}'.format(radar_rain[0], radar_rain[1]))
            self.radar_rain_combo.setCurrentIndex(0)
        else:
            self.radar_rain_combo.setEnabled(False)
            self.btn_hyeto.setEnabled(False)

    def get_id(self):
        return self.__id

    def display_hyeto(self):
        if self.radar_rain_combo.currentIndex() >-1:
            rain_id = self.radar_rain_combo.currentText().split(': ')[0]
            rain_file, rain_name, unit_cc = self.project.execute("""select file, name, unit_correction_coef from project.radar_rainfall where id={}""".format(rain_id)).fetchone()
            rad_file = os.path.join(self.project.radar_rain.data_directory, rain_name+"_"+self.__model.name+".rad")
            if os.path.exists(rad_file):
                vg = VisuGraph(self.project, file=rad_file, time_format='date', column=self.name.text(), shape='step', parent=self)
                vg.show()
            else:
                self.label.setText(self.tr("hyetogram file ")+rad_file+self.tr(" not found"))

            #rain_id = self.radar_rain_combo.currentText().split(': ')[0]
            #rain_file, rain_name, unit_cc = self.project.execute("""select file, name, unit_correction_coef from project.radar_rainfall where id={}""".format(rain_id)).fetchone()
            #rain = RainVrt(self.project.unpack_path(rain_file), self.project)
            #hyeto = rain.hyetogram(self.__id, self.__model.name, unit_cc)
            #self.graph_window=QMainWindow(self)
            #self.graph_window.setWindowTitle('Hyetogram')
            #graph=GraphWidget(self.graph_window)
            #print('debug', hyeto)
            #graph.add_filled_line(
            #    [t for [t,i] in hyeto],
            #    [i*3600/rain.dt for [t,i] in hyeto],
            #    "b",
            #    tr('Hyetogram of {} in {}'.format(rain_name, self.name.text())),
            #    tr('Date ()'), tr('Rain intensity (mm/h)'))
            #self.graph_window.setCentralWidget(graph)
            #self.graph_window.setFixedSize(int(graph.get_size()[0]), int(graph.get_size()[1]))
            #self.graph_window.show()

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.__model.update_catchment_contour(self.__id, self.__contour, self.name.text())
        BaseDialog.save(self)
        self.close()
