# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
hydra module to handle database

USAGE
    hydra.py [-h, -t, -d] project_name SRID [model]

    where conninfo are options to be passed to connect to the database
    and model is the model name

OPTIONS
    -d, --debug
        ouputs sql statements

    -t, --timing
        ouputs timing

    -h, --help
        ouputs this help

"""

import os
import logging
import shutil
import psycopg2
import requests
import atexit
from contextlib import contextmanager
from configparser import ConfigParser
from psycopg2.extras import LoggingConnection
from ..utility.string import normalized_name
from ..utility.settings_properties import SettingsProperties
from ..versions import __version__, __version_history__

# this is a hack to circunvent a bug in python3
@atexit.register
def goodbye():
    for project in PROJECT_TO_DELETE:
        remove_project(project)
PROJECT_TO_DELETE = []

__hydra_dir__ = os.path.join(os.path.expanduser('~'), ".hydra")


@contextmanager
def autoconnection(
        database: str,
        service: str = None,
        connection_factory=None):
    """Context manager for connection in autocommit mode without transaction.
    Usefull for DDL statements.
    @see https://github.com/psycopg/psycopg2/issues/941 to see why this is needed.
    """
    service = service or SettingsProperties.get_service()
    conn = psycopg2.connect(database=database, service=service, connection_factory=connection_factory)
    conn.autocommit = True
    try:
        yield conn
    finally:
        conn.close()


# this is the database version the plugin is meant to work with
def data_version():
    return __version__

def target_version(version):
    assert version in __version_history__.keys()
    return __version_history__[version]["target"]

class ProjectCreationError(Exception):
    pass

class TestProject(object):
    '''create a project from template the project contains one model named model'''

    NAME = "template_project"

    @staticmethod
    def reset_template(debug=False):
        if project_exists(TestProject.NAME):
            remove_project(TestProject.NAME)
        create_project(TestProject.NAME, 2154, debug=debug)
        server = public_server()
        if server:
            # TODO use template in project creation here too
            return

        with autoconnection(database="postgres") as con, con.cursor() as cur:
            cur.execute("select pg_terminate_backend(pg_stat_activity.pid) \
                        from pg_stat_activity \
                        where pg_stat_activity.datname = '%s'"%(TestProject.NAME))

    def __init__(self, name, keep=False, debug=False, with_model=False):
        if not project_exists(TestProject.NAME):
            self.reset_template(debug)
        assert name[-5:] == "_test"
        self.__name = name
        self.__debug = debug
        pd = project_dir(name)

        if os.path.exists(pd):
            clean_project_dir(name)

        if project_exists(name):
            remove_project(name)

        server = public_server()
        if server:
            # TODO use template in project creation here too
            create_project(name, 2154)
        else:
            with autoconnection(database="postgres") as con, con.cursor() as cur:
                cur.execute("create database {} with template {}".format(name, TestProject.NAME))

        if not keep:
            PROJECT_TO_DELETE.append(self.__name)

        if with_model:
            with autoconnection(database=name) as con:
                with con.cursor() as cur:
                    create_model(cur, "model")

        if not os.path.exists(pd):
            os.mkdir(pd)

def project_dir(project_name):
    '''returns directory associated with project'''
    assert project_name == normalized_name(project_name)
    dir_ = os.path.join(os.path.expanduser('~'), ".hydra", project_name)
    if os.path.exists(dir_+'.path'):
        with open(dir_+'.path') as f:
            dir_ = f.read().strip()
    return dir_

def __project_file_path(project_name):
    '''returns path of the project file'''
    assert project_name == normalized_name(project_name)
    return os.path.join(project_dir(project_name), project_name+".qgs")

def create_project(project_name, srid, working_dir=__hydra_dir__, debug=False, version=__version__):
    '''create project database and directory'''
    # @todo add user identification
    assert project_name == normalized_name(project_name)

    if os.path.exists(__project_file_path(project_name)):
        raise ProjectCreationError("File {} already exists. Cannot create project {}".format(__project_file_path(project_name), project_name))
    if project_exists(project_name):
        raise ProjectCreationError("A database named {} already exists. Use another name for the project.".format(project_name))
    if working_dir != __hydra_dir__:
        with open(os.path.join(__hydra_dir__, project_name)+'.path', 'w') as path_file:
            path_file.write(os.path.join(working_dir, project_name))

    # if we use a public server, we use it's api
    server = public_server()
    if server:
        res = requests.post(
            server['url']+'/create_project?name={}&srid={}'.format(project_name, srid),
            headers={'authorization': '{token_type} {access_token}'.format(**server['token'])})
        if res.status_code != 200:
            raise RuntimeError('code: {} msg:{}'.format(res.status_code, res.text))
        return

    # connects to postgres DB
    with autoconnection(database="postgres") as con, con.cursor() as cur:
        # creates project DB
        cur.execute("create database %s"%(project_name))
        if not os.path.exists(project_dir(project_name)) and SettingsProperties.local():
            os.makedirs(project_dir(project_name))
        # /!\ change connection to project DB

    with autoconnection(database=project_name, connection_factory=LoggingConnection) as con:
        logging.basicConfig(level=logging.ERROR if not debug else logging.DEBUG)
        logger = logging.getLogger(__name__)
        con.initialize(logger)
        with con.cursor() as cur:
            cur.execute("create extension postgis")
            cur.execute("create extension postgis_sfcgal")
            cur.execute("create extension plpython3u")
            cur.execute(f"create extension hydra with version '{version}'")
            cur.execute(f"insert into hydra.metadata(srid) values({srid})")

def is_hydra_db(dbname):
    if not dbname:
        return False

    try:
        with autoconnection(database=dbname) as con, con.cursor() as cur:
            cur.execute("select count(1) from pg_catalog.pg_available_extensions where name='hydra' and installed_version is not null")
            res = cur.fetchone()[0] == 1
            if res:
                try:
                    cur.execute("select srid, version, creation_date from hydra.metadata")
                except:
                    raise RuntimeError(f"database {dbname} has extension hydra, but you don't have permission to access hydra.metadata")
        return res
    except psycopg2.OperationalError:
        return False

def get_projects_list(all_db=False):
    server = public_server()
    if server:
        res = requests.get(
            server['url']+'/get_projects_list',
            headers={'authorization': '{token_type} {access_token}'.format(**server['token'])})
        if res.status_code != 200:
            raise RuntimeError('code: {} msg:{}'.format(res.status_code, res.text))
        return res.json()

    with autoconnection(database="postgres") as con, con.cursor() as cur:
        cur.execute("select datname from pg_database where datistemplate=false")
        databases = cur.fetchall()
        result = []

    for database in databases:
        if is_hydra_db(database[0]) or all_db:
            result.append(database[0])
    return result

def get_available_hydra_versions():
    server = public_server()
    if server:
        res = requests.get(
            server['url']+'/get_available_hydra_versions',
            headers={'authorization': '{token_type} {access_token}'.format(**server['token'])})
        if res.status_code != 200:
            raise RuntimeError('code: {} msg:{}'.format(res.status_code, res.text))
        return res.json()

    with autoconnection(database="postgres") as con, con.cursor() as cur:
        cur.execute("select version from pg_available_extension_versions where name='hydra'")
        versions = cur.fetchall()
    result = []
    for version in versions:
        result.append(version[0])
    return result

def project_exists(project_name):
    server = public_server()
    if server:
        res = requests.get(
        server['url']+'/project_exists?name={}'.format(project_name),
        headers={'authorization': '{token_type} {access_token}'.format(**server['token'])})
        if res.status_code != 200:
            raise RuntimeError('code: {} msg:{}'.format(res.status_code, res.text))
        return res.json()

    with autoconnection(database="postgres") as con, con.cursor() as cur:
        cur.execute("select count(1) from pg_database where datname='{}'".format(project_name))
        res = cur.fetchone()[0] == 1
    return res


def get_project_metadata(project_name, metadata_name=None):
    # @todo add user identification
    with autoconnection(database=project_name) as con, con.cursor() as cur:
        cur.execute("select srid, version, creation_date from hydra.metadata")
        srid, version, creation_date = cur.fetchone() or [None, None, None]

    index={'srid':0, 'version':1, 'creation_date':2}
    if metadata_name is None:
        return [srid, version, creation_date]
    elif metadata_name in index.keys():
        return [srid, version, creation_date][index[metadata_name]]
    else:
        raise AttributeError("{} not in {}".format(metadata_name, ', '.join(index.keys())))

def insert_project_metadata(project_name, srid):
    with autoconnection(database=project_name) as con, con.cursor() as cur:
        cur.execute(f"""alter table hydra.metadata disable trigger hydra_metadata_after_insert_trig;
                        insert into hydra.metadata(srid) values ({srid});
                        alter table hydra.metadata enable trigger hydra_metadata_after_insert_trig;""")

def remove_project(project_name, clean_dir=True):
    # @todo add user identification
    server = public_server()
    if server:
        res = requests.delete(
            server['url']+'/remove_project?name={}'.format(project_name),
            headers={'authorization': '{token_type} {access_token}'.format(**server['token'])})
        if res.status_code != 200:
            raise RuntimeError('code: {} msg:{}'.format(res.status_code, res.text))
        return res.json()

    if clean_dir and os.path.isdir(project_dir(project_name)):
        shutil.rmtree(project_dir(project_name))

    with autoconnection(database="postgres") as con, con.cursor() as cur:
        # close all remaining opened connection to this database if any
        cur.execute("select pg_terminate_backend(pg_stat_activity.pid) \
                    from pg_stat_activity \
                    where pg_stat_activity.datname='"+project_name+"';")
        cur.execute("drop database {}".format(project_name))


def clean_project_dir(project_name):
    # if project file still exists, remove it
    project_file = __project_file_path(project_name)
    if os.path.exists(project_file):
        os.remove(project_file)
    if os.path.exists(project_file+"~"):
        os.remove(project_file+"~")

    # remove all scenarios paths
    if os.path.isdir(project_dir(project_name)):
        for item in os.listdir(project_dir(project_name)):
            scn_path = os.path.join(item, 'scenario.nom')
            if os.path.exists(scn_path):
                shutil.rmtree(item)

    terrain_path = os.path.join(project_dir(project_name), "terrain")
    if os.path.exists(terrain_path) and not os.listdir(terrain_path):
        shutil.rmtree(terrain_path)
    project_dir_path = project_dir(project_name)
    if os.path.exists(project_dir_path) and not os.listdir(project_dir_path):
        shutil.rmtree(project_dir_path)

def create_model(cur, model, srid=None, empty=False):
    '''create model schema and related tables'''
    cur.execute("insert into project.model(name) values('{}')".format(model.lower()))
    cur.execute("insert into project.model_config(name,config) values('%s', 1);"%(model.lower()))

def remove_model(cur, name):
    cur.execute("delete from project.grp_model where model=(select id from project.model_config where name='%s');"%(name))
    cur.execute("delete from project.model_config where name ='%s';"%(name))
    cur.execute("drop schema if exists {} cascade".format(name))

def create_scenario(cur):
    '''create scenario, returns its id'''
    cur.execute("""insert into project.scenario default values returning id;""")
    return cur.fetchone()[0]

def public_server():
    "returns base url and token to public server in a dict, None if not a public server"
    services = ConfigParser()
    services.read(os.environ['PGSERVICEFILE'] if 'PGSERVICEFILE' in os.environ else os.path.join(os.path.expanduser('~'), '.pg_service.conf'))
    current_service = SettingsProperties.get_service()
    if services[current_service]['host'] in ('database.hydra-software.net'):
        url = "https://"+services[current_service]['host']+'/api'
        res = requests.post(url+'/token',
            data={'username': services[current_service]['user'], 'password': services[current_service]['password']})
        if res.status_code != 200:
            raise RuntimeError('code: {} msg:{}'.format(res.status_code, res.text))
        return {'url':url, 'token':res.json()}
    else:
        return None


if __name__ == "__main__":
    import sys
    from psycopg2.extras import LoggingConnection
    import logging
    import time
    import getopt

    try:
        optlist, args = getopt.getopt(sys.argv[1:],
                "htd",
                ["help", "timing", "debug"])
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        exit(1)

    optlist = dict(optlist)

    if "-h" in optlist or "--help" in optlist:
        help(sys.modules[__name__])
        exit(0)

    timing = "-t" in optlist or "--timing" in optlist
    debug = "-d" in optlist or "--debug" in optlist

    start = time.time()
    create_project(args[0], int(args[1]), debug=debug)
    if timing:
        sys.stdout.write("hydra %.2f sec"%(time.time()-start))

    start = time.time()
    with autoconnection(database=args[0]) as con, con.cursor() as cur:
        remove_model(cur, args[2])
        create_model(cur, args[2])
    if timing:
        sys.stdout.write(" %s %.2f sec"%(args[0], time.time()-start))
