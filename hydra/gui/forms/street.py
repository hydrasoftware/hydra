# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..base_dialog import BaseDialog, tr
import hydra.utility.string as string
from qgis.PyQt import uic, QtGui
import os

class StreetEditor(BaseDialog):
    def __init__(self, project, geom, id=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "street.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.__model = self.project.get_current_model()
        assert not self.__model == None
        self.__geom = geom

        if id==None:
            self.__id = self.__model.add_street(self.__geom)
        else:
            self.__id = id
            if self.__geom is None:
                geom,= self.project.execute("""
                select geom
                from {}.street
                where id={}""".format(self.__model.name, self.__id)).fetchone()

        name, width, width_invert, elem_length, rk, comment = self.project.execute("""
            select name, width, width_invert, elem_length, rk, comment
            from {}.street
            where id={}""".format(self.__model.name, self.__id)).fetchone()

        self.name.setText(name)
        self.notes.setText(comment)
        self.width.setText(string.get_str(width))
        self.width_invert.setText(string.get_str(width_invert) if width_invert else string.get_str(width))
        self.elem_length.setText(string.get_str(elem_length))
        self.rk.setText(string.get_str(rk))
        # stop width/elem length edition if width !=0 (if constraint already created)
        if width !=0:
            self.width.setEnabled(False)
            self.elem_length.setEnabled(False)

    def get_id(self):
        return self.__id

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.__model.update_street(self.__id, self.__geom,
                                        string.get_sql_float(self.width.text()),
                                        string.get_sql_float(self.width_invert.text()),
                                        string.get_sql_float(self.rk.text()),
                                        string.get_sql_float(self.elem_length.text()),
                                        self.name.text(),
                                        comment=self.notes.text())
        BaseDialog.save(self)
        self.close()







