# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
hydra server-side module mesh specific functions

"""

import os
import re
import numpy
import tempfile
import time
import json
import shutil
from subprocess import run, DEVNULL
from shapely import wkt
from shapely import wkb

import shapely
if int(shapely.__version__.split('.')[0]) >=2:
    from shapely import get_srid, set_srid
else:
    from shapely import geos
    def set_srid(geom, srid):
        geos.lgeos.GEOSSetSRID(geom._geom, srid)
        return geom
    def get_srid(geom):
        return geos.lgeos.GEOSGetSRID(geom._geom)


from shapely.geometry import Point, LineString, MultiPoint, MultiLineString

from shapely import speedups
speedups.disable()

from .altitude import altitude, altitudes

# Auto generation implemented:
#   At mesh, generate links:
#       2d node -> immersed channel reach
#       2d node -> valley reach
#       2d node -> reach end
#       2d node -> storage
#       2d node -> street
#   Create links available for link generation:
#       reach   -> reach
#       reach   -> street
#       reach   -> storage
#       storage -> storage
#       cst line-> station node

def tesselate(polygon, constrains, discontinuities, points, has_quads):
    class mesh:
        def __init__(self, vtx, tri):
            self.vertex = vtx
            self.face = tri

    poly = wkb.loads(polygon, True)
    constr = wkb.loads(constrains, True) if constrains is not None else MultiLineString()
    disc= wkb.loads(discontinuities, True) if discontinuities is not None else MultiLineString()
    pts = wkb.loads(points, True) if points is not None else MultiPoint()

    node_data = []
    triangle_data = []

    node_map = {}
    node_name_map = {}
    node_coord_map = {}
    line_set = set()
    current_id = 0
    tempdir = tempfile.mkdtemp()
    os.chmod(tempdir, 0o777)
    tmp_in_file = os.path.join(tempdir, 'tmp_mesh.geo')
    tmp_in_file_post = os.path.join(tempdir, 'tmp_mesh_post.geo')
    line_ids = []
    with open(tmp_in_file, "w") as geo:
        surface_idx = []
        for ring in [poly.exterior] + list(poly.interiors):
            ring_idx = []
            for coord in ring.coords:
                c = (f"{coord[0]:.3f}", f"{coord[1]:.3f}")
                if c in node_map:
                    ring_idx.append(node_map[c]['id'])
                else:
                    current_id += 1
                    ring_idx.append(current_id)
                    node_map[c] = {'id': current_id, 'original': coord[:2]}
                    geo.write("Point(%d) = {%.3f, %.3f, 0, 999999};\n"%(current_id, coord[0], coord[1]))
            loop_idx = []
            for i, j in zip(ring_idx[:-1], ring_idx[1:]):
                current_id += 1
                geo.write("Line(%d) = {%d, %d};\n"%(current_id, i, j))
                loop_idx.append(current_id)
            current_id += 1
            geo.write("Line Loop(%d) = {%s};\n"%(current_id, ", ".join((str(i) for i in loop_idx))))
            surface_idx.append(current_id)
        current_id += 1
        geo.write("Plane Surface(%d) = {%s};\n"%(current_id, ", ".join((str(i) for i in surface_idx))))
        if has_quads:
            geo.write("Recombine Surface {%s};\n"%(current_id))
        geo.write("Physical Surface(1) = {%s};\n"%(current_id))

        current_surface = current_id
        line_ids = []
        point_ids = []
        for lines in [disc, constr]:
            for line in lines.geoms:
                line_idx = []
                for coord in line.coords:
                    c = (f"{coord[0]:.3f}", f"{coord[1]:.3f}")
                    if c in node_map:
                        line_idx.append(node_map[c]['id'])
                    else:
                        current_id += 1
                        line_idx.append(current_id)
                        node_map[c] = {'id': current_id, 'original': coord[:2]}
                        geo.write("Point(%d) = {%.3f, %.3f, 0, 999999};\n"%(current_id, coord[0], coord[1]))

                point_ids += [line_idx[0], line_idx[-1]]

                for i, j in zip(line_idx[:-1], line_idx[1:]):
                    current_id += 1
                    geo.write("Line(%d) = {%d, %d};\n"%(current_id, i, j))
                    geo.write("Line {%d} In Surface {%d};\n"%(current_id, current_surface))
                    if lines == disc:
                        line_ids.append(current_id)

        if len(point_ids):
            geo.write("Physical Point (1) = {%s};\n"%(', '.join((str(l) for l in point_ids))))
        if len(line_ids):
            geo.write("Physical Line (1) = {%s};\n"%(', '.join((str(l) for l in line_ids))))

        for point in pts.geoms:
            coord = point.coords[0]
            c = (f"{coord[0]:.3f}", f"{coord[1]:.3f}")
            if c in node_map:
                geo.write("Point {%d} In Surface {%d};\n"%(node_map[c]["id"], current_surface))
            else:
                current_id += 1
                node_map[c] = {'id': current_id, 'original': coord[:2]}
                geo.write("Point(%d) = {%.3f, %.3f, 0, 999999};\n"%(current_id, coord[0], coord[1]))
                geo.write("Point {%d} In Surface {%d};\n"%(current_id, current_surface))

        if not has_quads:
            geo.write("Mesh.Algorithm = 3;\n")
        #geo.write("Mesh.RecombineAll = 1;\n")

    with open(tmp_in_file_post, "w") as geo:
        geo.write("Plugin(Crack).Dimension = 1;\n")
        geo.write("Plugin(Crack).PhysicalGroup = 1;\n")
        geo.write("Plugin(Crack).OpenBoundaryPhysicalGroup = 1;\n")
        geo.write("Plugin(Crack).NormalX = 0.000000;\n")
        geo.write("Plugin(Crack).NormalY = 0.000000;\n")
        geo.write("Plugin(Crack).NormalZ = 1.000000;\n")
        geo.write("Plugin(Crack).Run;\n")

    tmp_out_file = os.path.join(tempdir, 'tmp_mesh.msh')
    cmd = ['gmsh', '-2', tmp_in_file, '-o', tmp_out_file, '-format', 'msh2']
    run(cmd, stdout=DEVNULL)

    if len(disc.geoms):
        tmp_out_file_post = os.path.join(tempdir, 'tmp_mesh_post.msh')
        cmd = ['gmsh', '-2', tmp_out_file, tmp_in_file_post, '-o', tmp_out_file_post, '-format', 'msh2']
        #print(' '.join(cmd))
        run(cmd, stdout=DEVNULL)
    else:
        tmp_out_file_post = os.path.join(tempdir, 'tmp_mesh.msh')

    with open(tmp_out_file_post) as out:
        while not re.match("^\$Nodes", out.readline(), re.IGNORECASE):
            pass
        nb_nodes = int(out.readline())
        id_map = {}
        coord_map = {}
        nd = {}
        for i in range(nb_nodes):
            id_, x, y, z = out.readline().split()
            c = (f"{float(x):.3f}", f"{float(y):.3f}")
            coord = node_map[c]['original'] if c in node_map else (float(x), float(y))
            node_data.append((coord[0], coord[1]))
            id_map[id_] = len(node_data)
            coord_map[id_] = coord
        assert re.match("^\$EndNodes", out.readline(), re.IGNORECASE)

        assert re.match("^\$Elements", out.readline(), re.IGNORECASE)
        nb_elem = int(out.readline())
        for i in range(nb_elem):
            spl = out.readline().split()
            wkt = None
            if spl[1] == '2':
                if has_quads:
                    triangle_data.append([id_map[n] for n in reversed(spl[-3:])]+[spl[-1], None])
                else:
                    triangle_data.append([id_map[n] for n in reversed(spl[-3:])] + [spl[-1]])
            elif spl[1] == '3':
                assert has_quads
                triangle_data.append([id_map[n] for n in reversed(spl[-4:])] + [spl[-1]])
        assert re.match("^\$EndElements", out.readline(), re.IGNORECASE)

    shutil.rmtree(tempdir)

    vtx = MultiPoint(node_data)
    vtx = set_srid(vtx, get_srid(poly))
    return [mesh(wkb.dumps(vtx, hex=True, include_srid=True), triangle_data)]

def create_elem2d_links(plpy, elem_id, border, model, srid, generation_step):

    # find the domains that borders the element
    res = plpy.execute("""
        select c.domain_type, c.id
        from {model}.coverage as c
        where st_intersects(c.geom, '{border}'::geometry)
        and st_length(st_collectionextract(st_intersection(c.geom, '{border}'::geometry), 2)) > 0
        """.format(
            model=model,
            border=border
        ))
    domains = {r['domain_type']: r['id'] for r in res}
    cst = plpy.execute("""
        select c.constrain_type, c.link_attributes, c.elem_length, c.geom
        from {model}.constrain as c
        where st_covers(c.discretized, '{border}'::geometry)
        and c.constrain_type is distinct from 'ignored_for_coverages'
        """.format(
            model=model,
            border=border
        ))

    link_type = (cst[0]['constrain_type']) if cst else None
    link_attributes = json.loads(cst[0]['link_attributes']) if cst and cst[0]['link_attributes'] is not None else {}

    #plpy.warning(domains, link_type)
    if "reach" in domains:
        if link_type == 'flood_plain_transect':
            plpy.execute("""
                with river_node as (
                    select
                        distinct ST_ClosestPoint(o.geom, ST_Centroid(ST_Intersection(ST_Intersection(c.geom, ST_Buffer('{transect}'::geometry, {elem_length}::real/2)), o.geom))) as geom,
                        ST_Distance(e.geom, o.geom)/10 as dist
                    from {model}.open_reach as o, {model}.coverage as c, {model}.elem_2d_node as e
                    where ST_Intersects(o.geom, c.geom)
                    and c.id={coverage_id}
                    and e.id={elem_id}
                ),
                new_node as (
                    select project.set_altitude(ST_Force3D(geom)) as geom
                    from river_node as n
                    where (select count(1) from {model}.river_node as r where ST_DWithin(r.geom, n.geom, n.dist)) = 0
                ),
                ins_node as (
                    insert into {model}.river_node(geom, generated)
                    select geom, {generation_step}
                    from new_node
                    returning id, geom
                ),
                ins_link as (
                    insert into {model}.{link_type}_link(geom, border, generated{attribute_colums})
                    select ST_MakeLine(ARRAY[(
                        select geom from (
                            select n.geom
                            from {model}.river_node as n join {model}.coverage as c on st_intersects(c.geom, n.geom)
                            where c.id={coverage_id}
                            union
                            select geom from ins_node
                        ) as t order by ST_Distance(geom, {model}.interpolate_transect_at(ST_LineInterpolatePoint('{border}'::geometry, .5))) limit 1),
                        ST_LineInterpolatePoint('{border}'::geometry, .5),
                        ST_Centroid(st_union(e.geom, (ST_LineInterpolatePoint('{border}'::geometry, .5))))]), '{border}'::geometry, {generation_step}{attribute_values}
                    from {model}.elem_2d_node as e, river_node as tg
                    where e.id={elem_id}
                    returning id
                )
                select {model}.set_link_z_and_altitude(id) from ins_link
            """.format(
                model=model,
                border=border,
                elem_id=elem_id,
                transect=cst[0]['geom'],
                elem_length=cst[0]['elem_length'],
                coverage_id=domains["reach"],
                generation_step=generation_step,
                attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
                attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
                link_type = 'overflow'
            ))

        else:
            plpy.execute("""
                with river_node as (
                    select
                        distinct ST_ClosestPoint(o.geom, {model}.interpolate_transect_at(ST_LineInterpolatePoint('{border}'::geometry, .5))) as geom,
                        ST_Distance(e.geom, o.geom)/10 as dist
                    from {model}.open_reach as o, {model}.coverage as c, {model}.elem_2d_node as e
                    where ST_Intersects(o.geom, c.geom)
                    and c.id={coverage_id}
                    and e.id={elem_id}
                ),
                new_node as (
                    select project.set_altitude(ST_Force3D(geom)) as geom
                    from river_node as n
                    where (select count(1) from {model}.river_node as r where st_dwithin(r.geom, n.geom, n.dist)) = 0
                ),
                ins_node as (
                    insert into {model}.river_node(geom, generated)
                    select geom, {generation_step}
                    from new_node
                    returning id, geom
                ),
                ins_link as (
                    insert into {model}.{link_type}_link(geom, border, generated{attribute_colums})
                    select ST_MakeLine(ARRAY[(
                        select geom from (
                            select n.geom
                            from {model}.river_node as n join {model}.coverage as c on ST_Intersects(c.geom, n.geom)
                            where c.id={coverage_id}
                            union
                            select geom from ins_node
                        ) as t order by ST_Distance(geom, {model}.interpolate_transect_at(ST_LineInterpolatePoint('{border}'::geometry, .5))) limit 1),
                        ST_LineInterpolatePoint('{border}'::geometry, .5),
                        ST_Centroid(st_union(e.geom, (ST_LineInterpolatePoint('{border}'::geometry, .5))))]), '{border}'::geometry, {generation_step}{attribute_values}
                    from {model}.elem_2d_node as e
                    where e.id={elem_id}
                    returning id
                )
                select {model}.set_link_z_and_altitude(id) from ins_link
            """.format(
                model=model,
                border=border,
                elem_id=elem_id,
                coverage_id=domains["reach"],
                generation_step=generation_step,
                attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
                attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
                link_type = link_type or 'overflow'
            ))

    elif "storage" in domains:
        plpy.execute("""
            with storage_node as (
                select n.geom
                from {model}.storage_node as n, {model}.coverage as c
                where st_intersects(c.geom, n.geom)
                and c.id={coverage_id}
            ),
            ins_link as (
                insert into {model}.{link_type}_link(geom, border, generated{attribute_colums})
                select st_makeline(ARRAY[
                     st_centroid(st_union(e.geom, (st_lineinterpolatepoint('{border}'::geometry, .5)))),
                     st_lineinterpolatepoint('{border}'::geometry, .5),
                     (st_centroid(st_union((select geom from {model}.storage_node as t where contour = {coverage_id}), st_lineinterpolatepoint('{border}'::geometry, .5))))
                     ]), '{border}'::geometry, {generation_step}{attribute_values}
                from {model}.elem_2d_node as e
                where e.id={elem_id}
                returning id
            )
            select {model}.set_link_z_and_altitude(id) from ins_link
        """.format(
            model=model,
            border=border,
            elem_id=elem_id,
            coverage_id=domains["storage"],
            generation_step=generation_step,
            attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
            attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
            link_type = link_type or 'overflow'
        ))

    elif "street" in domains:
        plpy.execute("""
            with street_node as (
                select
                    distinct st_lineinterpolatepoint(r.geom, st_linelocatepoint(r.geom, st_lineinterpolatepoint('{border}'::geometry, .5))) as geom,
                    st_distance(st_centroid('{border}'::geometry), r.geom)/3 as dist
                from {model}.street as r, {model}.coverage as c, {model}.elem_2d_node as e
                where st_intersects(r.geom, c.geom)
                and c.id={coverage_id}
                and e.id={elem_id}
                order by dist
                limit 1
            ),
            new_node as (
                select project.set_altitude(st_force3d(geom)) as geom
                from street_node as n
                where (select count(1) from {model}.crossroad_node as r where st_dwithin(r.geom, n.geom, n.dist)) = 0
            ),
            ins_node as (
                insert into {model}.crossroad_node(geom, area, generated)
                select geom, 1, {generation_step}
                from new_node
                returning id, geom
            ),
            ins_link as (
                insert into {model}.{link_type}_link(geom, border, generated{attribute_colums})
                select st_makeline(ARRAY[(
                    select geom from (
                    select r.geom from {model}.crossroad_node as r, street_node as n where st_dwithin(r.geom, n.geom, n.dist)
                        union
                    select geom from ins_node
                    ) as t order by geom <-> st_lineinterpolatepoint('{border}'::geometry, .5) limit 1),
                    st_lineinterpolatepoint('{border}'::geometry, .5),
                    st_centroid(st_union(e.geom, (st_lineinterpolatepoint('{border}'::geometry, .5))))]) , '{border}'::geometry, {generation_step}{attribute_values}
                from {model}.elem_2d_node as e
                where e.id={elem_id}
                returning id
            )
            select {model}.set_link_z_and_altitude(id) from ins_link
        """.format(
            model=model,
            border=border,
            elem_id=elem_id,
            coverage_id=domains["street"],
            generation_step=generation_step,
            attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
            attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
            link_type = link_type or 'overflow'
        ))

    elif None in domains:
        pass
    else: # 2D/2D
        assert(len(domains) == 1)

        plpy.execute("""
            with ins_link as (
                insert into {model}.{link_type}_link(geom, border, generated{attribute_colums})
                select st_makeline(e1.geom, e2.geom), '{border}'::geometry, {generation_step}{attribute_values}
                from {model}.elem_2d_node as e1, {model}.elem_2d_node as e2
                where e1.id={elem_id}
                and e2.id!=e1.id
                and st_covers(e2.contour, '{border}'::geometry)
                returning id
            )
            select {model}.set_link_z_and_altitude(id) from ins_link
        """.format(
            model=model,
            border=border,
            elem_id=elem_id,
            generation_step=generation_step,
            attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
            attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
            link_type = link_type if link_type is not None and link_type not in ('flood_plain_transect', 'boundary') else 'mesh_2d'
        ))



    return 1

def create_links(plpy, up_domain_id, down_domain_id, model, srid):

    domains = plpy.execute("""
        select du.geom as ugeom, dd.geom as dgeom, du.domain_type as utype, dd.domain_type as dtype, st_linemerge(st_intersection(du.geom, dd.geom)) as border
        from {model}.coverage as du, {model}.coverage as dd
        where dd.id={down_domain_id}
        and du.id={up_domain_id}
        and st_length(st_collectionextract(st_intersection(du.geom, dd.geom), 2)) > 0
        """.format(
        down_domain_id=down_domain_id,
        up_domain_id=up_domain_id,
        model=model
    ))

    # Check if domains are adjacent
    if not domains:
        plpy.warning("domains {} and {} have no common boundary".format(up_domain_id, down_domain_id))
        return 0

    domains_properties, = domains

    # Check domains are in right order, switches them if needed

    #         |  REACH  |  STORAGE  |  STREET  |  <= Second selected
    # --------+---------+-----------+----------+
    # REACH   |    V    |     V     |    V     |
    # STORAGE |    X    |     V     |    X     |
    # STREET  |    X    |     V     |    V     |
    # --------+---------+-----------+----------+
    #    ^ First selected

    if (domains_properties['utype'] == 'storage') or (domains_properties['utype'] == 'street' and domains_properties['dtype'] == 'reach'):
        temp_id = up_domain_id
        up_domain_id = down_domain_id
        down_domain_id = temp_id

        domains = plpy.execute("""
            select du.geom as ugeom, dd.geom as dgeom, du.domain_type as utype, dd.domain_type as dtype, st_linemerge(st_intersection(du.geom, dd.geom)) as border
            from {model}.coverage as du, {model}.coverage as dd
            where dd.id={down_domain_id}
            and du.id={up_domain_id}
            and st_length(st_collectionextract(st_intersection(du.geom, dd.geom), 2)) > 0
            """.format(
            down_domain_id=down_domain_id,
            up_domain_id=up_domain_id,
            model=model
        ))

        domains_properties, = domains

    generation_step = plpy.execute("""
        insert into {model}.generation_step default values returning id
        """.format(
            model=model
            ))[0]['id']

    # Get constrain properties
    constraints = plpy.execute("""
        with cst as (
            select c.constrain_type, c.link_attributes, c.elem_length, ST_LineMerge(ST_Intersection(c.discretized, '{border}'::geometry)) as geom
            from {model}.constrain as c
            where st_intersects(c.discretized, '{border}'::geometry)
            and c.constrain_type is distinct from 'ignored_for_coverages'
        ) select * from cst where not ST_IsEmpty(geom) and ST_Dimension(geom)=1
        """.format(
            model=model,
            border=domains_properties['border']
        ))

    # Saves current config and switches to default config
    cfg = plpy.execute("""select configuration from {model}.metadata""".format(model=model))[0]['configuration']
    if cfg != 1:
        plpy.execute("""update {model}.metadata set configuration=1""".format(model=model))

    errors = 0

    for cst in constraints:
        link_type = (cst['constrain_type']) if (cst and cst['constrain_type'] not in ['boundary', 'ignored_for_coverages']) else None
        link_attributes = json.loads(cst['link_attributes']) if cst and cst['link_attributes'] is not None else {}

        # Check if links of good type are already generated on that border
        if link_type == 'flood_plain_transect':
            check_links_generated = plpy.execute("""
                select exists (select 1 from {model}.overflow_link as l where ST_Intersects(l.border, '{border}'::geometry) and ST_Dimension(ST_Intersection(l.border, '{border}'::geometry))=1)
                """.format(model=model, border=cst['geom']))
        else:
            check_links_generated = plpy.execute("""
                select exists (select 1 from {model}.{link_type}_link as l where ST_Intersects(l.border, '{border}'::geometry) and ST_Dimension(ST_Intersection(l.border, '{border}'::geometry))=1)
                """.format(model=model, link_type=link_type or 'overflow', border=cst['geom']))
        # If links already exists => Warning + Error
        if check_links_generated[0]['exists']:
            plpy.warning("links {} already generated between domains {} and {} (on {})".format(link_type, up_domain_id, down_domain_id, cst['geom']))
            errors += 1

        # Else => generates links
        elif domains_properties['utype'] == 'reach' and domains_properties['dtype'] == 'reach':
            if link_type == 'flood_plain_transect':
                plpy.execute("""
                    with blade as (
                        select (st_dumppoints('{border}'::geometry)).geom as geom
                    ),
                    border as (
                        select (st_dump(st_split('{border}'::geometry, b.geom))).geom as  geom
                        from (select st_collect(geom) as geom from blade) as b
                    ),
                    up_river_node as (
                        select
                            ST_ClosestPoint(r.geom, ST_Centroid(ST_Intersection(ST_Intersection(c.geom, ST_Buffer('{transect}'::geometry, {elem_length}::real/2)), r.geom))) as geom,
                            ST_Distance(ST_LineInterpolatePoint('{border}'::geometry, .5), r.geom)/10 as dist
                        from {model}.reach as r, {model}.coverage as c
                        where ST_Intersects(r.geom, c.geom)
                        and c.id={uid}
                    ),
                    down_river_node as (
                        select
                            ST_ClosestPoint(r.geom, ST_Centroid(ST_Intersection(ST_Intersection(c.geom, ST_Buffer('{transect}'::geometry, {elem_length}::real/2)), r.geom))) as geom,
                            ST_Distance(ST_LineInterpolatePoint('{border}'::geometry, .5), r.geom)/10 as dist
                        from {model}.reach as r, {model}.coverage as c
                        where ST_Intersects(r.geom, c.geom)
                        and c.id={did}
                    ),
                    new_node as (
                        select project.set_altitude(ST_Force3D(n.geom)) as geom
                        from (select geom, dist from up_river_node union select geom, dist from down_river_node) as n
                        where not exists (select 1 from {model}.river_node as r where ST_DWithin(r.geom, n.geom, n.dist))
                    ),
                    ins_node as (
                        insert into {model}.river_node(geom, generated)
                        select geom, {generation_step}
                        from new_node
                        returning id, geom
                    ),
                    ins_link as (
                        insert into {model}.overflow_link(geom, border,generated{attribute_colums})
                        select st_makeline(ARRAY[
                             (select geom from (select n.geom from {model}.river_node as n
                                                    join {model}.coverage as c on ST_Intersects(c.geom, n.geom)
                                                    where c.id={uid}
                                                    union
                                                select n.geom from ins_node as n
                                                    join {model}.coverage as c on ST_Intersects(c.geom, n.geom)
                                                    where c.id={uid}) as t
                             order by ST_Distance(geom, ST_LineInterpolatePoint(b.geom, .5)) limit 1),
                             st_lineinterpolatepoint(b.geom, .5),
                             (select geom from (select n.geom from {model}.river_node as n
                                                    join {model}.coverage as c on ST_Intersects(c.geom, n.geom)
                                                    where c.id={did}
                                                    union
                                                select n.geom from ins_node as n
                                                    join {model}.coverage as c on ST_Intersects(c.geom, n.geom)
                                                    where c.id={did}) as t
                             order by ST_Distance(geom, ST_LineInterpolatePoint(b.geom, .5)) limit 1)
                             ]), b.geom, {generation_step}{attribute_values}
                        from border as b
                        returning id
                    )
                    select {model}.set_link_z_and_altitude(id) from ins_link
                """.format(
                    model=model,
                    uid=up_domain_id,
                    did=down_domain_id,
                    transect=cst['geom'],
                    elem_length=cst['elem_length'],
                    generation_step=generation_step,
                    attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
                    attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
                    border=cst['geom']
                ))
            else:
                plpy.execute("""
                    with blade as
                    (
                        select (st_dumppoints('{border}'::geometry)).geom as geom
                    ),
                    border as (
                        select (st_dump(st_split('{border}'::geometry, b.geom))).geom as  geom
                        from (select st_collect(geom) as geom from blade) as b
                    ),
                    up_river_node as (
                        select st_lineinterpolatepoint(r.geom, st_linelocatepoint(r.geom, st_centroid(st_intersection(r.geom, {model}.interpolate_transect_at(st_lineinterpolatepoint(b.geom,.5), {uid}))))) as geom,
                        st_distance(st_lineinterpolatepoint(b.geom, .5), r.geom)/10 as dist
                        from {model}.reach as r, {model}.coverage as c, border as b
                        where st_intersects(r.geom, c.geom)
                        and c.id={uid}
                    ),
                    down_river_node as (
                        select st_lineinterpolatepoint(r.geom, st_linelocatepoint(r.geom, st_centroid(st_intersection(r.geom, {model}.interpolate_transect_at(st_lineinterpolatepoint(b.geom, .5), {did}))))) as geom,
                        st_distance(st_lineinterpolatepoint(b.geom, .5), r.geom)/10 as dist
                        from {model}.reach as r, {model}.coverage as c, border as b
                        where st_intersects(r.geom, c.geom)
                        and c.id={did}
                    ),
                    new_node as (
                        select project.set_altitude(st_force3d(geom)) as geom
                        from (select geom, dist from up_river_node union select geom, dist from down_river_node) as n
                        where not exists (select 1 from {model}.river_node as r where st_dwithin(r.geom, n.geom, n.dist))
                    ),
                    ins_node as (
                        insert into {model}.river_node(geom, generated)
                        select geom, {generation_step}
                        from new_node
                        returning id, geom
                    ),
                    ins_link as (
                        insert into {model}.{link_type}_link(geom, border,generated{attribute_colums})
                        select st_makeline(ARRAY[
                             (select geom from (select n.geom from {model}.river_node as n
                                                    join {model}.coverage as c on st_intersects(c.geom, n.geom)
                                                    where c.id={uid}
                                                    union
                                                select geom from ins_node) as t
                             order by geom <-> {model}.interpolate_transect_at(st_lineinterpolatepoint(b.geom, .5), {uid}) limit 1),
                             st_lineinterpolatepoint(b.geom, .5),
                             (select geom from (select n.geom from {model}.river_node as n
                                                    join {model}.coverage as c on st_intersects(c.geom, n.geom)
                                                    where c.id={did}
                                                    union
                                                select geom from ins_node) as t
                             order by geom <-> {model}.interpolate_transect_at(st_lineinterpolatepoint(b.geom, .5), {did}) limit 1)
                             ]), b.geom, {generation_step}{attribute_values}
                        from border as b
                        returning id
                    )
                    select {model}.set_link_z_and_altitude(id) from ins_link
                """.format(
                    model=model,
                    uid=up_domain_id,
                    did=down_domain_id,
                    generation_step=generation_step,
                    attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
                    attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
                    link_type = link_type or 'overflow',
                    border=cst['geom']
                ))

        elif domains_properties['utype'] == 'reach' and domains_properties['dtype'] == 'street':
            if link_type == 'flood_plain_transect':
                plpy.execute("""
                    with blade as
                    (
                        select (st_dumppoints('{border}'::geometry)).geom as geom
                    ),
                    border as (
                        select (st_dump(st_split('{border}'::geometry, b.geom))).geom as  geom
                        from (select st_collect(geom) as geom from blade) as b
                    ),
                    river_node as (
                        select
                            ST_ClosestPoint(r.geom, ST_Centroid(ST_Intersection(ST_Intersection(c.geom, ST_Buffer('{transect}'::geometry, {elem_length}::real/2)), r.geom))) as geom,
                            ST_Distance(ST_LineInterpolatePoint('{border}'::geometry, .5), r.geom)/10 as dist
                        from {model}.reach as r, {model}.coverage as c
                        where ST_Intersects(r.geom, c.geom)
                        and c.id={uid}
                    ),
                    new_river_node as (
                        select project.set_altitude(st_force3d(geom)) as geom
                        from river_node as n
                        where not exists (select 1 from {model}.river_node as r where st_dwithin(r.geom, n.geom, n.dist))
                    ),
                    ins_river_node as (
                        insert into {model}.river_node(geom, generated)
                        select geom, {generation_step}
                        from new_river_node
                        returning id, geom
                    ),
                    crossroad_node as (
                        select
                            ST_ClosestPoint(s.geom, ST_LineInterpolatePoint(b.geom, .5)) as geom,
                            ST_Distance(ST_LineInterpolatePoint(b.geom, .5), s.geom)/10 as dist
                        from {model}.coverage as c, border as b
                        join lateral (
                            select geom from {model}.street
                            where ST_Intersects(street.geom, c.geom)
                            order by ST_LineInterpolatePoint(b.geom, .5) <-> street.geom
                            limit 1
                        ) as s
                        on true
                        where c.id={did}
                    ),
                    new_crossroad_node as (
                        select project.set_altitude(st_force3d(geom)) as geom
                        from crossroad_node as n
                        where not exists (select 1 from {model}.crossroad_node as c where st_dwithin(c.geom, n.geom, n.dist))
                    ),
                    ins_crossroad_node as (
                        insert into {model}.crossroad_node(geom, area, generated)
                        select geom, 1, {generation_step}
                        from new_crossroad_node
                        returning id, geom
                    ),
                    ins_link as (
                        insert into {model}.overflow_link(geom, border,generated{attribute_colums})
                        select st_makeline(ARRAY[
                             (select geom from (select n.geom from {model}.river_node as n
                                                    join {model}.coverage as c on ST_Intersects(c.geom, n.geom)
                                                    where c.id={uid}
                                                    union
                                                select n.geom from ins_river_node as n
                                                    join {model}.coverage as c on ST_Intersects(c.geom, n.geom)
                                                    where c.id={uid}) as t
                             order by ST_Distance(geom, ST_LineInterpolatePoint(b.geom, .5)) limit 1),
                             st_lineinterpolatepoint(b.geom, .5),
                             (select geom from (select r.geom from {model}.crossroad_node as r, crossroad_node as n where st_dwithin(r.geom, n.geom, n.dist)
                                                    union
                                                select n.geom from ins_crossroad_node as n
                                                    join {model}.coverage as c on ST_Intersects(c.geom, n.geom)
                                                    where c.id={did}) as t
                             order by ST_Distance(geom, ST_LineInterpolatePoint(b.geom, .5)) limit 1)
                             ]), b.geom, {generation_step}{attribute_values}
                        from border as b
                        returning id
                    )
                    select {model}.set_link_z_and_altitude(id) from ins_link
                """.format(
                    model=model,
                    uid=up_domain_id,
                    did=down_domain_id,
                    transect=cst['geom'],
                    elem_length=cst['elem_length'],
                    generation_step=generation_step,
                    attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
                    attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
                    border=cst['geom']
                ))
            else:
                plpy.execute("""
                    with blade as
                    (
                        select (st_dumppoints('{border}'::geometry)).geom as geom
                    ),
                    border as (
                        select (st_dump(st_split('{border}'::geometry, b.geom))).geom as  geom
                        from (select st_collect(geom) as geom from blade) as b
                    ),
                    river_node as (
                        select
                            st_lineinterpolatepoint(r.geom, st_linelocatepoint(r.geom, st_centroid(st_intersection(r.geom, {model}.interpolate_transect_at(st_lineinterpolatepoint(b.geom,.5), {uid}))))) as geom,
                            st_distance(st_lineinterpolatepoint(b.geom, .5), r.geom)/10 as dist
                        from {model}.reach as r, {model}.coverage as c, border as b
                        where st_intersects(r.geom, c.geom)
                        and c.id={uid}
                    ),
                    street_node as (
                        select
                            st_lineinterpolatepoint(s.geom, st_linelocatepoint(s.geom, st_lineinterpolatepoint(b.geom, .5))) as geom,
                            st_distance(st_lineinterpolatepoint(b.geom, .5), s.geom)/3 as dist
                        from {model}.coverage as c, border as b
                        join lateral (
                            select geom from {model}.street
                            where st_intersects(street.geom, c.geom)
                            order by st_lineinterpolatepoint(b.geom, .5) <-> street.geom
                            limit 1
                        ) as s
                        on true
                        where c.id={did}
                    ),
                    new_river_node as (
                        select project.set_altitude(st_force3d(geom)) as geom
                        from river_node as n
                        where not exists (select 1 from {model}.river_node as r where st_dwithin(r.geom, n.geom, n.dist))
                    ),
                    new_street_node as (
                        select project.set_altitude(st_force3d(geom)) as geom
                        from street_node as n
                        where not exists (select 1 from {model}.crossroad_node as s where st_dwithin(s.geom, n.geom, n.dist))
                    ),
                    ins_river_node as (
                        insert into {model}.river_node(geom, generated)
                        select geom, {generation_step}
                        from new_river_node
                        returning id, geom
                    ),
                    ins_street_node as (
                        insert into {model}.crossroad_node(geom, area, generated)
                        select geom, 1, {generation_step}
                        from new_street_node
                        returning id, geom
                    ),
                    ins_link as (
                        insert into {model}.{link_type}_link(geom, border,generated{attribute_colums})
                        select st_makeline(ARRAY[
                             (select geom from (select n.geom from {model}.river_node as n
                                                    join {model}.coverage as c on st_intersects(c.geom, n.geom)
                                                    where c.id={uid}
                                                    union
                                                select geom from ins_river_node) as t
                             order by geom <-> {model}.interpolate_transect_at(st_lineinterpolatepoint(b.geom, .5), {uid}) limit 1),
                             st_lineinterpolatepoint(b.geom, .5),
                             (select geom from (select r.geom from {model}.crossroad_node as r, street_node as n where st_dwithin(r.geom, n.geom, n.dist)
                                                    union
                                                select geom from ins_street_node) as t
                             order by geom <-> st_lineinterpolatepoint(b.geom, .5) limit 1)
                             ]), b.geom, {generation_step}{attribute_values}
                        from border as b
                        returning id
                    )
                    select {model}.set_link_z_and_altitude(id) from ins_link
                """.format(
                    model=model,
                    uid=up_domain_id,
                    did=down_domain_id,
                    generation_step=generation_step,
                    attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
                    attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
                    link_type = link_type or 'overflow',
                    border=cst['geom']
                ))

        elif domains_properties['utype'] == 'reach' and domains_properties['dtype'] == 'storage':
            if link_type == 'flood_plain_transect':
                plpy.execute("""
                    with blade as
                    (
                        select (st_dumppoints('{border}'::geometry)).geom as geom
                    ),
                    border as (
                        select (st_dump(st_split('{border}'::geometry, b.geom))).geom as  geom
                        from (select st_collect(geom) as geom from blade) as b
                    ),
                    river_node as (
                        select
                            ST_ClosestPoint(r.geom, ST_Centroid(ST_Intersection(ST_Intersection(c.geom, ST_Buffer('{transect}'::geometry, {elem_length}::real/2)), r.geom))) as geom,
                            ST_Distance(ST_LineInterpolatePoint('{border}'::geometry, .5), r.geom)/10 as dist
                        from {model}.reach as r, {model}.coverage as c
                        where ST_Intersects(r.geom, c.geom)
                        and c.id={uid}
                    ),
                    new_river_node as (
                        select project.set_altitude(st_force3d(geom)) as geom
                        from river_node as n
                        where not exists (select 1 from {model}.river_node as r where st_dwithin(r.geom, n.geom, n.dist))
                    ),
                    ins_river_node as (
                        insert into {model}.river_node(geom, generated)
                        select geom, {generation_step}
                        from new_river_node
                        returning id, geom
                    ),
                    ins_link as (
                        insert into {model}.overflow_link(geom, border,generated{attribute_colums})
                        select st_makeline(ARRAY[
                             (select geom from (select n.geom from {model}.river_node as n
                                                    join {model}.coverage as c on ST_Intersects(c.geom, n.geom)
                                                    where c.id={uid}
                                                    union
                                                select n.geom from ins_river_node as n
                                                    join {model}.coverage as c on ST_Intersects(c.geom, n.geom)
                                                    where c.id={uid}) as t
                             order by ST_Distance(geom, ST_LineInterpolatePoint(b.geom, .5)) limit 1),
                             st_lineinterpolatepoint(b.geom, .5),
                             (select geom from {model}.storage_node as t where contour = {did})
                             ]), b.geom, {generation_step}{attribute_values}
                        from border as b
                        returning id
                    )
                    select {model}.set_link_z_and_altitude(id) from ins_link
                """.format(
                    model=model,
                    uid=up_domain_id,
                    did=down_domain_id,
                    transect=cst['geom'],
                    elem_length=cst['elem_length'],
                    generation_step=generation_step,
                    attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
                    attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
                    border=cst['geom']
                ))
            else:
                plpy.execute("""
                    with blade as
                    (
                        select (st_dumppoints('{border}'::geometry)).geom as geom
                    ),
                    border as (
                        select (st_dump(st_split('{border}'::geometry, b.geom))).geom as  geom
                        from (select st_collect(geom) as geom from blade) as b
                    ),
                    river_node as (
                        select
                            st_lineinterpolatepoint(r.geom, st_linelocatepoint(r.geom, st_centroid(st_intersection(r.geom, {model}.interpolate_transect_at(st_lineinterpolatepoint(b.geom,.5), {uid}))))) as geom,
                            st_distance(st_lineinterpolatepoint(b.geom, .5), r.geom)/10 as dist
                        from {model}.reach as r, {model}.coverage as c, border as b
                        where st_intersects(r.geom, c.geom)
                        and c.id={uid}
                    ),
                    new_river_node as (
                        select project.set_altitude(st_force3d(geom)) as geom
                        from river_node as n
                        where not exists (select 1 from {model}.river_node as r where st_dwithin(r.geom, n.geom, n.dist))
                    ),
                    ins_river_node as (
                        insert into {model}.river_node(geom, generated)
                        select geom, {generation_step}
                        from new_river_node
                        returning id, geom
                    ),
                    ins_link as (
                        insert into {model}.{link_type}_link(geom, border,generated{attribute_colums})
                        select st_makeline(ARRAY[
                             (select geom from (select n.geom from {model}.river_node as n
                                                    join {model}.coverage as c on st_intersects(c.geom, n.geom)
                                                    where c.id={uid}
                                                    union
                                                select geom from ins_river_node) as t
                             order by geom <-> st_lineinterpolatepoint(b.geom, .5) limit 1),
                             st_lineinterpolatepoint(b.geom, .5),
                             (select geom from {model}.storage_node as t where contour = {did})
                             ]), b.geom, {generation_step}{attribute_values}
                        from border as b
                        returning id
                    )
                    select {model}.set_link_z_and_altitude(id) from ins_link
                """.format(
                    model=model,
                    uid=up_domain_id,
                    did=down_domain_id,
                    generation_step=generation_step,
                    attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
                    attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
                    link_type = link_type or 'overflow',
                    border=cst['geom']
                ))

        elif domains_properties['utype'] == 'street' and domains_properties['dtype'] == 'storage':
            plpy.execute("""
                with blade as
                (
                    select (st_dumppoints('{border}'::geometry)).geom as geom
                ),
                border as (
                    select (st_dump(st_split('{border}'::geometry, b.geom))).geom as  geom
                    from (select st_collect(geom) as geom from blade) as b
                ),
                street_node as (
                    select
                        st_lineinterpolatepoint(s.geom, st_linelocatepoint(s.geom, st_lineinterpolatepoint(b.geom, .5))) as geom,
                        st_distance(st_lineinterpolatepoint(b.geom, .5), s.geom)/3 as dist
                    from {model}.coverage as c, border as b
                    join lateral (
                        select geom from {model}.street
                        where st_intersects(street.geom, c.geom)
                        order by st_lineinterpolatepoint(b.geom, .5) <-> street.geom
                        limit 1
                    ) as s
                    on true
                    where c.id={uid}
                ),
                new_street_node as (
                    select project.set_altitude(st_force3d(geom)) as geom
                    from street_node as n
                    where not exists (select 1 from {model}.crossroad_node as s where st_dwithin(s.geom, n.geom, n.dist))
                ),
                ins_street_node as (
                    insert into {model}.crossroad_node(geom, area, generated)
                    select geom, 1, {generation_step}
                    from new_street_node
                    returning id, geom
                ),
                ins_link as (
                    insert into {model}.{link_type}_link(geom, border,generated{attribute_colums})
                    select st_makeline(ARRAY[
                         (select geom from (select r.geom from {model}.crossroad_node as r, street_node as n where st_dwithin(r.geom, n.geom, n.dist)
                                                union
                                            select geom from ins_street_node) as t
                         order by geom <-> st_lineinterpolatepoint(b.geom, .5) limit 1),
                         st_lineinterpolatepoint(b.geom, .5),
                         (st_centroid(st_union((select geom from {model}.storage_node as t where contour = {did}), st_lineinterpolatepoint(b.geom, .5))))
                         ]), b.geom, {generation_step}{attribute_values}
                    from border as b
                    returning id
                )
                select {model}.set_link_z_and_altitude(id) from ins_link
            """.format(
                model=model,
                uid=up_domain_id,
                did=down_domain_id,
                generation_step=generation_step,
                attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
                attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
                link_type = link_type or 'overflow',
                border=cst['geom']
            ))

        elif domains_properties['utype'] == 'street' and domains_properties['dtype'] == 'street':
            plpy.execute("""
                with blade as
                (
                    select (st_dumppoints('{border}'::geometry)).geom as geom
                ),
                border as (
                    select (st_dump(st_split('{border}'::geometry, b.geom))).geom as  geom
                    from (select st_collect(geom) as geom from blade) as b
                ),
                up_street_node as (
                    select
                        st_lineinterpolatepoint(s.geom, st_linelocatepoint(s.geom, st_lineinterpolatepoint(b.geom, .5))) as geom,
                        st_distance(st_lineinterpolatepoint(b.geom, .5), s.geom)/3 as dist
                    from {model}.coverage as c, border as b
                    join lateral (
                        select geom from {model}.street
                        where st_intersects(street.geom, c.geom)
                        order by st_lineinterpolatepoint(b.geom, .5) <-> street.geom
                        limit 1
                    ) as s
                    on true
                    where c.id={uid}
                ),
                new_up_street_node as (
                    select project.set_altitude(st_force3d(geom)) as geom
                    from up_street_node as n
                    where not exists (select 1 from {model}.crossroad_node as s where st_dwithin(s.geom, n.geom, n.dist))
                ),
                ins_up_street_node as (
                    insert into {model}.crossroad_node(geom, area, generated)
                    select geom, 1, {generation_step}
                    from new_up_street_node
                    returning id, geom
                ),
                down_street_node as (
                    select
                        st_lineinterpolatepoint(s.geom, st_linelocatepoint(s.geom, st_lineinterpolatepoint(b.geom, .5))) as geom,
                        st_distance(st_lineinterpolatepoint(b.geom, .5), s.geom)/3 as dist
                    from {model}.coverage as c, border as b
                    join lateral (
                        select geom from {model}.street
                        where st_intersects(street.geom, c.geom)
                        order by st_lineinterpolatepoint(b.geom, .5) <-> street.geom
                        limit 1
                    ) as s
                    on true
                    where c.id={did}
                ),
                new_down_street_node as (
                    select project.set_altitude(st_force3d(geom)) as geom
                    from down_street_node as n
                    where not exists (select 1 from {model}.crossroad_node as s where st_dwithin(s.geom, n.geom, n.dist))
                ),
                ins_down_street_node as (
                    insert into {model}.crossroad_node(geom, area, generated)
                    select geom, 1, {generation_step}
                    from new_down_street_node
                    returning id, geom
                ),
                ins_link as (
                    insert into {model}.{link_type}_link(geom, border,generated{attribute_colums})
                    select st_makeline(ARRAY[
                         (select geom from (select r.geom from {model}.crossroad_node as r, up_street_node as n where st_dwithin(r.geom, n.geom, n.dist)
                                                union
                                            select geom from ins_up_street_node) as t
                         order by geom <-> st_lineinterpolatepoint(b.geom, .5) limit 1),
                         st_lineinterpolatepoint(b.geom, .5),
                         (select geom from (select r.geom from {model}.crossroad_node as r, down_street_node as n where st_dwithin(r.geom, n.geom, n.dist)
                                                    union
                                                select geom from ins_down_street_node) as t
                             order by geom <-> st_lineinterpolatepoint(b.geom, .5) limit 1)
                         ]), b.geom, {generation_step}{attribute_values}
                    from border as b
                    returning id
                )
                select {model}.set_link_z_and_altitude(id) from ins_link
            """.format(
                model=model,
                uid=up_domain_id,
                did=down_domain_id,
                generation_step=generation_step,
                attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
                attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
                link_type = link_type or 'overflow',
                border=cst['geom']
            ))

        elif domains_properties['utype'] == 'storage' and domains_properties['dtype'] == 'storage':
            plpy.execute("""
                with blade as
                (
                    select (st_dumppoints('{border}'::geometry)).geom as geom
                ),
                border as (
                    select (st_dump(st_split('{border}'::geometry, b.geom))).geom as  geom
                    from (select st_collect(geom) as geom from blade) as b
                ),
                ins_link as (
                    insert into {model}.{link_type}_link(geom, border,generated{attribute_colums})
                    select st_makeline(ARRAY[
                         (st_centroid(st_union((select geom from {model}.storage_node as t where contour = {uid}), st_lineinterpolatepoint(b.geom, .5)))),
                         st_lineinterpolatepoint(b.geom, .5),
                         (st_centroid(st_union((select geom from {model}.storage_node as t where contour = {did}), st_lineinterpolatepoint(b.geom, .5))))
                         ]), b.geom, {generation_step}{attribute_values}
                    from border as b
                    returning id
                )
                select {model}.set_link_z_and_altitude(id) from ins_link
            """.format(
                model=model,
                uid=up_domain_id,
                did=down_domain_id,
                generation_step=generation_step,
                attribute_colums = (', '+', '.join(link_attributes.keys())) if link_attributes else '',
                attribute_values = (', '+', '.join(str(v) for v in link_attributes.values())) if link_attributes else '',
                link_type = link_type or 'overflow',
                border=cst['geom']
            ))

        else:
            plpy.warning("link generation from {} to {} domains is not implemented".format(domains_properties['utype'], domains_properties['dtype']))
            errors += 1

    # Restores initial configuration
    if cfg != 1:
        plpy.execute("""update {m}.metadata set configuration={c}""".format(m=model,c=cfg))
    return int(errors==0)

def create_boundary_links(plpy, up_type, up_id, station_node_id, model, srid):
    generation_step = plpy.execute("""
        insert into {model}.generation_step default values returning id
        """.format(model=model))[0]['id']

    if up_type == 'constrain':
        constrain_id = up_id
        res = plpy.execute("""
                with mesh_node as (
                    select ST_Force2D(n.geom) as node, ST_ClosestPoint(c.discretized, ST_Centroid(ST_Intersection(n.contour, ST_Buffer(c.discretized, 0.1)))) as border_point
                    from {model}.elem_2d_node as n, {model}.constrain as c
                    where c.id={cid}
                    and ST_Intersects(n.contour, ST_Buffer(c.geom, 0.1))
                    and ST_Length(ST_CollectionExtract(ST_Intersection(n.contour, c.discretized), 2)) > 0
                    and c.constrain_type is distinct from 'ignored_for_coverages'
                )
                insert into {model}.connector_link(geom, generated)
                    select ST_MakeLine(ARRAY[ST_Centroid(ST_Collect(m.node, m.border_point)), m.border_point, n.geom]), {generation_step}
                    from mesh_node as m, {model}.station_node as n
                    where n.id={nid}
            """.format(model=model, cid=constrain_id, nid=station_node_id, generation_step=generation_step))

    elif up_type == 'street':
        street_id = up_id
        res = plpy.execute("""
                with street_node as (
                    select ST_Force2D(n.geom) as node
                    from {model}.crossroad_node as n, {model}.street as s
                    where s.id={sid}
                    and ST_Contains(ST_Buffer(s.geom, 0.1), n.geom)
                )
                insert into {model}.connector_link(geom, generated)
                    select ST_MakeLine(ARRAY[m.node, n.geom]), {generation_step}
                    from street_node as m, {model}.station_node as n
                    where n.id={nid}
            """.format(model=model, sid=street_id, nid=station_node_id, generation_step=generation_step))

def create_network_overflow_links(plpy, domain_id, model, srid):
    res = plpy.execute("""
        select d.geom as geom, d.domain_type as type
        from {model}.coverage as d
        where d.id={domain_id}
        """.format(domain_id=domain_id, model=model))

    if not res:
        plpy.warning("error identifying domain {}".format(domain_id))
        return 0

    domain, = res

    # delete existing network overflows
    plpy.execute("""
        delete from {model}.network_overflow_link as l
        where ST_Contains('{geom}', l.geom)
        """.format(model=model, geom=domain['geom']))

    generation_step = plpy.execute("""
        insert into {model}.generation_step default values returning id
        """.format(model=model))[0]['id']

    if domain['type'] == 'street':
        plpy.execute("""
            with manholes as (
                select n.geom
                from {model}.manhole_node as n, {model}.coverage as c
                where ST_Contains(c.geom, n.geom)
                and c.id={id}
            ),
            crossroads as (
                select c.geom, c.z_ground
                from {model}.crossroad_node as c, {model}.coverage as cov
                where ST_Contains(cov.geom, c.geom)
                and cov.id={id}
            ),
            nodes as (
                select m.geom as up_geom,
                      (select c.geom from crossroads as c order by m.geom <-> c.geom limit 1) as down_geom,
                      (select c.z_ground from crossroads as c order by m.geom <-> c.geom limit 1) as z
                from manholes as m
            )
            insert into {model}.network_overflow_link(geom, z_overflow, generated)
            select st_makeline(ARRAY[n.up_geom,n.down_geom]), n.z, {generation_step}
            from nodes as n
        """.format(model=model, id=domain_id, generation_step=generation_step))
        return 1

    elif domain['type'] == 'storage':
        plpy.execute("""
            with manholes as (
                select n.geom
                from {model}.manhole_node as n, {model}.coverage as c
                where ST_Contains(c.geom, n.geom)
                and c.id={id}
            ),
            nodes as (
                select m.geom as up_geom, s.geom as down_geom, s.zs_array[0][0] as z
                from manholes as m, {model}.storage_node as s
                where s.contour={id}
            )
            insert into {model}.network_overflow_link(geom, z_overflow, generated)
            select st_makeline(ARRAY[n.up_geom,st_centroid(st_union(n.up_geom, n.down_geom))]), n.z, {generation_step}
            from nodes as n
        """.format(model=model, id=domain_id, generation_step=generation_step))
        return 1

    elif domain['type'] == '2d':
        plpy.execute("""
            with manholes as (
                select n.geom
                from {model}.manhole_node as n, {model}.coverage as c
                where ST_Contains(c.geom, n.geom)
                and c.id={id}
            ),
            nodes as (
                select m.geom as up_geom, e.geom as down_geom, e.zb as z
                from manholes as m
                join {model}.elem_2d_node as e on ST_contains(e.contour, m.geom)
            )
            insert into {model}.network_overflow_link(geom, z_overflow, generated)
            select st_makeline(ARRAY[n.up_geom,st_centroid(st_union(n.up_geom, n.down_geom))]), n.z, {generation_step}
            from nodes as n
        """.format(model=model, id=domain_id, generation_step=generation_step))
        return 1

    else:
        plpy.warning("network overflow generation on {} is not implemented".format(domain['type']))
        return 0


def discretize_line(geom, elem_length):
    """split line if needed"""
    from math import ceil

    def split_line(l, nb_elem):
        return LineString((l.interpolate(x, True).coords[0] for x in (float(i)/nb_elem for i in range(nb_elem+1))))

    line = wkb.loads(geom, True)
    points = [line.coords[0]]
    for point in line.coords[1:]:
        d = Point(points[-1]).distance(Point(point))
        if d > elem_length:
            points += list(split_line(LineString([points[-1], point]), int(ceil(d/elem_length))).coords[1:])
        else:
            points.append(point)
    out_line = LineString(points)
    out_line = set_srid(out_line, get_srid(line))
    return out_line.wkb_hex

def crossed_border(line, polygon):
    """return border (segment of exterior ring) crossed by link
    the returned segment direction makes a positive angle with tthe link lines
    """
    link = wkb.loads(line, True)
    contour = wkb.loads(polygon, True).exterior
    # find intersecting edge
    border = [LineString([n1, n2])
            for n1, n2 in zip(contour.coords[:-1], contour.coords[1:])
            if link.intersects(LineString([n1, n2]))]
    if not len(border):
        return None
    border = border[0]

    # compute angle
    u = numpy.array(contour.coords[0]) - numpy.array(contour.coords[1])
    v = numpy.array(border.coords[0]) - numpy.array(border.coords[1])
    if u[0]*v[1] - u[1]*v[0] < 0:
        border = LineString(reversed(border.coords))

    border = set_srid(border, get_srid(border))

    return border.wkb_hex
