# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
exposes functions that produce pandas dataframes from various file formats

the first two columns are "time(h)" and "date()"
"""

import csv
import os
import re


import pandas as pd

def add_time(df, date0):
    "adds a 'time(h)' column after the 'date()' column that is in first position"
    df['date()'] = pd.to_datetime(df['date()'], dayfirst=True )
    df.insert(1, 'time(h)', None)
    df['time(h)'] = (df['date()'] - date0).dt.total_seconds()/3600

def add_date(df, date0):
    "adds a 'date()' column after the 'time(h)' column that is in first position"
    df['time(h)'] = pd.to_numeric(df['time(h)'])
    df.insert(1, 'date()', None)
    df['date()'] = date0 + pd.to_timedelta(df['time(h)'], unit='h')

def __set_time_and_date(df, time_format, date0):
    if time_format == 'date':
        df.rename(columns={ df.columns[0]: 'date()' }, inplace=True)
        add_time(df, date0)
    elif time_format == 'time':
        df.rename(columns={ df.columns[0]: 'time(h)' }, inplace=True)
        add_date(df, date0)
    else:
        raise RuntimeError('time_format must be time or date and is {time_format}')

    # also force other columns to numeric
    for c in df.columns[2:]:
        df[c] = pd.to_numeric(df[c])

    # finaly sort according to time
    return df.groupby(by=['date()'], sort=True).first().reset_index()

def read_csv(path, time_format, date0):
    with open(path) as f:
       dialect = csv.Sniffer().sniff(f.read(10000))
    df = pd.read_csv(path, dialect=dialect)
    df.rename(columns={c: c.strip() for c in df.columns}, inplace=True)
    return __set_time_and_date(df, time_format, date0)

def read_xls(path, time_format, date0):
    df = pd.read_excel(path)
    df.rename(columns={c: c.strip() for c in df.columns}, inplace=True)
    return __set_time_and_date(df, time_format, date0)


def read_ten_char_column(path, time_format, date0):
    '''symbol : '!', parser for format of ten varchar : first row / ten varchar : second row , no separator'''
    # format, no separator
    #  0----------10----------20
    # | header with no limit    |
    # | object1                 |
    # | xxxxxxxx    yyyyyyyy    |
    # | xxxxxxxx    yyyyyyyy    |
    # | object2                 |
    # | xxxxxxxx    yyyyyyyy    |

    parse_token = 0
    levels = []
    values = []
    if time_format == 'time':
        time_column = 'time(h)'
    elif time_format=='date':
        time_column = 'date()'
    else:
        raise RuntimeError('time_format must be time or date and is {time_format}')

    df = pd.DataFrame({time_column : []})

    with open(path) as advanced_data:
        next(advanced_data)
        for ligne in advanced_data:
            if ligne[0] == "'" :
                if parse_token == 0 :
                    name_curve = re.sub(r"^'(.*)'$", r"\1", ligne.strip())
                parse_token = parse_token + 1
                if parse_token == 2 :
                    df = df.merge(pd.DataFrame({time_column: levels, name_curve: values}), how='outer', on=time_column)
                    name_curve = re.sub(r"^'(.*)'$", r"\1", ligne.strip())
                    levels = []
                    values = []
                    parse_token = 1
            else :
                levels.append(ligne[0:10].strip())
                values.append(ligne[10:20].strip())

        if parse_token == 1 :
            df = df.merge(pd.DataFrame({time_column: levels, name_curve: values}), how='outer', on=time_column)

        return __set_time_and_date(df, time_format, date0)

def read_one_param(path, time_format, date0):
    ''' symbol : '*', parser for format of 2 simple column but with several object of 1 colums and 1 empty column'''

    # format, separator (,/;)
    # | Header              |
    # | object1 ;           |
    # | xxxxx   ;   yyyyyy  |
    # | xxxxx   ;   yyyyyy  |
    # | object2 ;           |
    # | xxxxx   ;   yyyyyy  |
    # | xxxxx   ;   yyyyyy  |


    parse_token = 0
    levels = []
    values = []
    if time_format == 'time':
        time_column = 'time(h)'
    elif time_format=='date':
        time_column = 'date()'
    else:
        raise RuntimeError('time_format must be time or date and is {time_format}')

    df = pd.DataFrame({time_column : []})

    with open(path) as advanced_data:
        dialect = csv.Sniffer().sniff(advanced_data.read(2048), delimiters=[";",','])
        advanced_data.seek(0)
        advanced_data_reader = csv.reader(advanced_data,dialect=dialect)
        next(advanced_data_reader)
        for row in advanced_data_reader :
            if row[1] == "" :
                if parse_token == 0 :
                    name_curve = row[0]
                parse_token = parse_token + 1
                if parse_token == 2 :
                    df = df.merge(pd.DataFrame({time_column: levels, name_curve: values}), how='outer', on=time_column)
                    name_curve = row[0]
                    levels = []
                    values = []
                    parse_token = 1
            else :
                levels.append(row[0])
                values.append(row[1])

        if parse_token == 1 :
            df = df.merge(pd.DataFrame({time_column: levels, name_curve: values}), how='outer', on=time_column)

        return __set_time_and_date(df, time_format, date0)

def read_rad(path, time_format, date0):
    "read .rad date/intensity or time/intensity hydrogramm"

    #  date format:
    # | 'object1'           |
    # | xxxxx   ;   yyyyyy  | xxxx iso date
    # | xxxxx   ;   yyyyyy  |
    # |                     |
    # | 'object2'           |
    # | xxxxx   ;   yyyyyy  |
    # | xxxxx   ;   yyyyyy  |

    #  time format:
    # | 'object1'          |
    # | xxxxx      yyyyyy  | xxxx time in seconds
    # | xxxxx      yyyyyy  |
    # |                    |
    # | 'object2'          |
    # | xxxxx      yyyyyy  |
    # | xxxxx      yyyyyy  |

    columns = [None]
    data = [[]]
    with open(path) as f:
        for ll in f:
            ll = ll.strip()
            if ll == '' and columns[-1] is not None:
                columns.append(None)
                data.append([])
            elif columns[-1] is None:
                columns[-1] = ll[1:-1] if ll.startswith("'") else ll
            else:
                data[-1].append([v.strip() for v in ll.split(';')] if ';' in ll else ll.split())

    if columns[-1] is None:
        columns.pop()
        data.pop()

    if time_format == 'time':
        time_column = 'time(h)'
    elif time_format=='date':
        time_column = 'date()'
    else:
        raise RuntimeError('time_format must be time or date and is {time_format}')

    df = pd.DataFrame({time_column : []})
    for c, d in zip(columns, data):
        df = df.merge(pd.DataFrame({
            time_column: [int(v[0])/3600 for v in d] if time_format=='time' else [v[0] for v in d],
            c: [v[1] for v in d]
            }), how='outer', on=time_column)

    df.fillna(0.0, inplace=True)
    return __set_time_and_date(df, time_format, date0)

def read_multi_param(path, time_format, date0):
    ''' symbol : '$', parser for format of multiple row but 1 object'''

    # format, separator (,/;)
    # | Header                              |
    # | object ;  value1 ; value2 ; value3  |
    # | xxxxx  ;  yyyy11 ; yyyy22 ; yyy333  |
    # | xxxxx  ;  yyyy11 ; yyyy22 ; yyy333  |
    # | xxxxx  ;  yyyy11 ; yyyy22 ; yyy333  |
    # | xxxxx  ;  yyyy11 ; yyyy22 ; yyy333  |

    parse_token = 0
    levels = []
    table_values = []
    if time_format == 'time':
        time_column = 'time(h)'
    elif time_format=='date':
        time_column = 'date()'
    else:
        raise RuntimeError('time_format must be time or date and is {time_format}')

    df = pd.DataFrame({time_column : []})

    with open(path) as advanced_data:
        dialect = csv.Sniffer().sniff(advanced_data.read(2048), delimiters=[";",','])
        advanced_data.seek(0)
        advanced_data_reader = csv.reader(advanced_data,dialect=dialect)
        next(advanced_data_reader)
        header = next(advanced_data_reader)[1:]
        for row in advanced_data_reader :
            if len(row) == 2 and row[1]=="" :
                if parse_token == 0 :
                    name_curve = row[0].strip()
                parse_token = parse_token + 1
                if parse_token == 2 :
                    for i in range(len(header)) :
                        df = df.merge(pd.DataFrame({time_column: levels,
                            name_curve + '_'+ header[i].strip() : [values[i] for values in table_values]}),
                            how='outer', on=time_column)

                    name_curve = row[0]
                    levels = []
                    table_values = []
                    parse_token = 1
            else :
                levels.append(row[0])
                table_values.append([elem for elem in row[1:]])

        if parse_token == 1 :
            for i in range(len(header)) :
                df = df.merge(pd.DataFrame({time_column: levels,
                    name_curve + '_'+ header[i].strip() : [values[i] for values in table_values]}),
                    how='outer', on=time_column)

        return __set_time_and_date(df, time_format, date0)


def read_multi_column(path, time_format, date0):
    ''' symbol '%', format of a lot of row with multiple object '''

    # format, separator (,/;)
    # | Header                                  |
    # | object1 ;  value11 ; value12 ; value13  |
    # | xxxxx   ;  yyyy11  ; yyyy2   ; yyy333   |
    # | xxxxx   ;  yyyy11  ; yyyy2   ; yyy333   |
    # | object2 ;  value21 ; value22 ; value23  |
    # | xxxxx   ;  yyyy11  ; yyyy2   ; yyy333   |

    levels = []
    table_values = []
    if time_format == 'time':
        time_column = 'time(h)'
    elif time_format=='date':
        time_column = 'date()'
    else:
        raise RuntimeError('time_format must be time or date and is {time_format}')

    df = pd.DataFrame({time_column : []})

    with open(path) as advanced_data:
        dialect = csv.Sniffer().sniff(advanced_data.read(2048),delimiters=[";",','])
        advanced_data.seek(0)
        advanced_data_reader = csv.reader(advanced_data,dialect=dialect)
        next(advanced_data_reader)
        header = next(advanced_data_reader)[1:]
        for row in advanced_data_reader :
            levels.append(row[0])
            table_values.append(row[1:])
        for i in range(len(header)) :
            df = df.merge(pd.DataFrame({time_column: levels,
                header[i].strip() : [values[i] for values in table_values]}),
                how='outer', on=time_column)

        return __set_time_and_date(df, time_format, date0)

def parse(path, time_format, date0) :
    ''' use extension or the first line of the file to find a parser for this format'''

    if not os.path.exists(path):
        raise RuntimeError(f'{path} not found')

    if path.endswith('.csv'):
        return read_csv(path, time_format, date0)
    elif path.endswith('.xls'):
        return read_xls(path, time_format, date0)
    elif path.endswith('.rad'):
        return read_rad(path, time_format, date0)

    with open(path) as f:
        first_line = f.readline()

    if "%" in first_line[0] :
        # multi colonnes / mono paramètre
        return read_multi_column(path, time_format, date0)
    elif "$" in first_line[0] :
        # multi parametre / mono colonne
        return read_multi_param(path, time_format, date0)
    elif "!" in first_line[0] :
        # mono 10/10
        return read_ten_char_column(path, time_format, date0)
    elif "*" in first_line[0] :
        # mono sep
        return read_one_param(path, time_format, date0)
    else :
        raise RuntimeError(f'unknown file format {path}')

class FileParser(object) :
    def __init__(self, filepath, auto_load = False) :
        self.filepath = filepath
        self.firstline = None
        self.content = None
        self.loaded = False
        self.filename = os.path.basename(filepath)

        if auto_load :
            with open(self.filepath) as data:
                self.firstline = data.readline()
                data.seek(0)
                self.content = data.readlines()
            self.loaded = True


class SectionParser(FileParser) :
    ''' parser for format of 2 simple column  with several section object of 1 colums and 1 empty column'''
    def __init__(self, filepath, section_type) :
        FileParser.__init__(self, filepath)
        self.sections = []
        self.section_type = section_type

        # format, separator (,/;)           |
        # | object1 ;           |
        # | xxxxx   ;   yyyyyy  |
        # | xxxxx   ;   yyyyyy  |
        # | object2 ;           |
        # | xxxxx   ;   yyyyyy  |
        # | xxxxx   ;   yyyyyy  |


        parse_token = 0
        widths = []
        heigths = []
        with open(self.filepath) as advanced_data:
            dialect = csv.Sniffer().sniff(advanced_data.read(2048),delimiters=[";",','])
            advanced_data.seek(0)
            self.content = advanced_data.readlines()
            advanced_data.seek(0)
            advanced_data_reader = csv.reader(advanced_data,dialect=dialect)
            for row in advanced_data_reader :
                if row[1] == "" :
                    if parse_token == 0 :
                        name_section = row[0]
                    parse_token = parse_token + 1
                    if parse_token == 2 :
                        section = Section(widths, heigths, name_section, type = self.section_type, table = self.section_type + "_parametric_geometry")
                        if section.is_valid():
                            self.sections.append(section)
                        name_section = row[0]
                        widths = []
                        heigths = []
                        parse_token = 1
                else :
                    widths.append(float(row[0]))
                    heigths.append(float(row[1]))
            if parse_token == 1 :
                section = Section(widths, heigths, name_section, type = self.section_type, table = self.section_type + "_parametric_geometry")
                if section.is_valid():
                    self.sections.append(section)

class Section(object):
    ''' class which represent a section in geometry_manager'''
    def __init__(self, widths, heigths, name, table, type=None):
        assert len(widths) == len(heigths)

        self.__array = []

        for i in range(len(widths)):
            self.__array.append([heigths[i], widths[i]])

        self.name = name
        self.type = type

    def is_valid(self):
        for i in range(1, len(self.__array)):
            if self.__array[i-1][0] > self.__array[i][0]:
                return False
        return True

    def get_array(self):
        return self.__array


if __name__ == '__main__':
    import sys
    import dateutil

    path = sys.argv[1]
    time_format = sys.argv[2]
    date0 = dateutil.parser.parse(sys.argv[3], dayfirst=True)

    df = parse(path, time_format, date0)

    #print(df)

