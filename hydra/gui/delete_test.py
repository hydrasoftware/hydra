# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
tests that automated deletion functions are working correctly

USAGE

   python -m hydra.gui.delete_test [-h]

OPTIONS

   -h, --help
        print this help
"""

import os
import sys
import getopt
from hydra.project import Project
from hydra.utility.log import LogManager, ConsoleLogger
from hydra.utility import empty_ui_manager as ui_manager
from hydra.database.database import TestProject, project_exists, remove_project
from hydra.database.import_model import import_model_csv
from hydra.gui.delete import DeleteTool

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "h",
            ["help"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

data_dir = os.path.join(os.path.dirname(__file__),'../database', 'test_data', 'light_projects')

project_name = "delete_test"

if project_exists(project_name):
    remove_project(project_name)

model_name = "model1"
srid = 2154

test_project = TestProject(project_name)

obj_project = Project.load_project(project_name)

models = [os.path.join(data_dir, f)
    for f in os.listdir(data_dir) if f.endswith(".csv")]

for model in models:
    model_name = os.path.split(model)[1][:-4]
    if model_name in obj_project.get_models():
        obj_project.delete_model(model_name)
    obj_project.add_new_model(model_name)
    obj_project.set_current_model(model_name)
    # fix_print_with_import
    print("Import model: " + model_name)
    import_model_csv(model, obj_project, obj_project.get_current_model(), model_name)

# DELETE FUNCTIONS
# fix_print_with_import
print('Delete tests :')
# fix_print_with_import
print('HY BC...')
DeleteTool.delete_hy_bc(None, obj_project, 'reso', 3)
DeleteTool.delete_hy_bc(None, obj_project, 'riv', 1)
# fix_print_with_import
print('Links...')
DeleteTool.delete_link(None, obj_project, 'reso', 'pipe_link', 47)
DeleteTool.delete_link(None, obj_project, 'reso', 'pipe_link', 50)
# fix_print_with_import
print('Nodes...')
DeleteTool.delete_node(None, obj_project, 'riv', 'river_node', 5)
DeleteTool.delete_node(None, obj_project, 'riv', 'storage_node', 12)
DeleteTool.delete_node(None, obj_project, 'reso', 'manhole_node', 10)
DeleteTool.delete_node(None, obj_project, 'reso', 'manhole_node', 40)
DeleteTool.delete_node(None, obj_project, 'riv', 'manhole_hydrology_node', 12)
# fix_print_with_import
print('Reachs...')
DeleteTool.delete_reach(None, obj_project, 'riv', 3)
DeleteTool.delete_reach(None, obj_project, 'riv', 2)
# fix_print_with_import
print('Stations...')
DeleteTool.delete_station(None, obj_project, 'riv', 1)
DeleteTool.delete_station(None, obj_project, 'riv', 2)

obj_project.commit()
# fix_print_with_import
print('ok')

