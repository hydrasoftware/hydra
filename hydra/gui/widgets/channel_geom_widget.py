from __future__ import absolute_import

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import webbrowser
from qgis.PyQt.QtWidgets import QApplication, QWidget, QGridLayout
from qgis.PyQt import uic
from ..widgets.array_widget import ArrayWidget
from .graph_widget import GraphWidget
from hydra.gui.base_dialog import tr


class ChannelGeomWidget(QWidget):

    def __init__(self, project, parent=None, channel_geom_table=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "channel_geom_widget.ui"), self)
        self.project = project
        self.model = self.project.get_current_model()

        self.op_geom_xyarray = ArrayWidget(["H (m)", "Width (m)"], [20,2], None,
                draw_graph_func=self.__draw_op_geom, help_func=self.help_hyd_file, hide_headers=False, parent=self.array_op_geom_placeholder)
        self.graph_op_geom = GraphWidget(self.graph_op_geom_placeholder, True, True)
        self.channel_geom_table = channel_geom_table

        self.op_geom_name.textChanged.connect(self.__op_geom_update_name)

        self.set_enabled(False)

        self.tmplayout = QGridLayout()
        self.tmplayout.addWidget(self, 0, 0)

    def get_layout(self):
        return self.tmplayout

    def set_items(self, id_channel_geom):
        if id_channel_geom is not None:
            name, zbmin_array, \
                with_flood_plain, flood_plain_width, flood_plain_lateral_slope = \
                self.project.execute("""
                select name, zbmin_array,
                    with_flood_plain, flood_plain_width, flood_plain_lateral_slope
                    from {}.open_parametric_geometry where id={}
                """.format(self.model.name, id_channel_geom)).fetchone()
            self.op_geom_name.setText(name)
            self.op_geom_xyarray.set_table_items(zbmin_array)
            self.set_enabled(True)

            self.with_flood_plain.setChecked(with_flood_plain)
            self.flood_plain_width.setText(str(flood_plain_width))
            self.flood_plain_lateral_slope.setText(str(flood_plain_lateral_slope))

    def set_enabled(self, bool=True):
        self.op_geom_name.setEnabled(bool)
        self.op_geom_xyarray.setEnabled(bool)
        self.graph_op_geom.canvas.setVisible(bool)

    def __op_geom_update_name(self):
        if self.channel_geom_table:
            self.channel_geom_table.table.item(self.channel_geom_table.table.row(self.channel_geom_table.table.selectedItems()[0]),1).setText(self.op_geom_name.text())

    def save_geom(self, id_channel_geom):
        if id_channel_geom:
            name = self.op_geom_name.text()
            zbmin_array = self.op_geom_xyarray.get_table_items()

            self.model.update_pipe_channel_open_section_geometry(id_channel_geom, zbmin_array, name,
                self.with_flood_plain.isChecked(),
                float(self.flood_plain_width.text()),
                float(self.flood_plain_lateral_slope.text()))

    def __draw_op_geom(self, datas):
        title = self.op_geom_name.text()
        if len(datas)>0:
            datas.insert(0,[datas[0][0], 0])
            self.graph_op_geom.clear()
            self.graph_op_geom.add_symetric_line(
                [r[1]/2. for r in datas],
                [r[0] for r in datas],
                "k",
                title,
                tr("Width (m)"), tr("H (m)"))
        else:
            self.graph_op_geom.canvas.setVisible(False)

    def help_hyd_file(self):
        webbrowser.open('http://hydra-software.net/docs/notes_techniques/notice_import_section.pdf', new=0)
