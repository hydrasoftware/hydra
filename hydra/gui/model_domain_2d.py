# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from qgis.PyQt import uic
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QWidget
import os

class ModelDomain2dWidget(QWidget):
    def __init__(self, project, id_=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "model_domain_2d.ui"), self)
        self.project = project
        self.model = project.get_current_model()

        self.id, name = \
            project.execute("""
                insert into {}.domain_2d default values returning id, name
                """.format(self.model.name, self.model.make_line(line))).fetchone() \
            if id_ is None else \
            project.execute("""
                select id, name from {}.domain_2d where id={}
                """.format(self.model.name, str(id_))).fetchone()

        self.lineEdit.setText(name)

    def save(self):
        self.project.execute("""
            update {}.constrain set name=%s where id={}
            """.format(self.model.name, self.id),
            (self.lineEdit.text(),))
