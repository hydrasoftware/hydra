/* ********************************************************************************** */
/* Delete crossroad_node which are not:                                               */
/*     + intersecting a street                                                        */
/*     + Connected to a link                                                          */
/* ********************************************************************************** */

delete from $model.crossroad_node as c
where c.id not in (select l.up from $model._link as l where up is not null)
    and c.id not in (select l.down from $model._link as l  where down is not null)
	and c.geom not in (select c.geom from $model.street as s where st_intersects(c.geom,s.geom))
;;
