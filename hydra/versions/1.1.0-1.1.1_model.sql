/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  Fix of view L2D                               */
/* ********************************************** */

create or replace view ${model}.l2d
as
with link as (
    select l.name, project.crossed_border(l.geom, n.contour) as geom
    from ${model}._link as l, ${model}.elem_2d_node as n
    where (n.id = l.up or n.id=l.down)
    and l.link_type!='mesh_2d'
    and l.link_type!='network_overflow'
    and st_intersects(l.geom, st_exteriorring(n.contour)))
select name, st_x(st_startpoint(geom)) as xb, st_y(st_startpoint(geom)) as yb, st_x(st_endpoint(geom)) as xa, st_y(st_endpoint(geom)) as ya from link
;;

/* ********************************************** */
/*  Fix pipes trigger                             */
/* ********************************************** */

/* note: id and link_type are not updatable */
create or replace function ${model}.pipe_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from ${model}.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('pipe', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'pipe') where name = 'define_later' and id = id_;
            insert into $model._pipe_link
                values (id_, 'pipe', new.comment, new.z_invert_up, new.z_invert_down, coalesce(new.cross_section_type, 'circular'), coalesce(new.h_sable, 0), new.branch, new.rk, coalesce(new.custom_length, null), new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom);
            if 'pipe' = 'pipe' then
                perform $model.branch_update_fct();
            end if;
            --perform model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'pipe_link');
            update $model._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (h_sable is not null) and (cross_section_type is not null) and (cross_section_type not in ('valley')) and (rk is not null) and (rk >0) and (cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 )) and (cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)) and (cross_section_type!='pipe' or cp_geom is not null) and (cross_section_type!='channel' or op_geom is not null) from  $model._pipe_link where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' then
                update $model.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if row(new.*)::varchar is distinct from row(old.*)::varchar then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.comment, old.z_invert_up, old.z_invert_down, old.cross_section_type, old.h_sable, old.branch, old.rk, old.custom_length, old.circular_diameter, old.ovoid_height, old.ovoid_top_diameter, old.ovoid_invert_diameter, old.cp_geom, old.op_geom) as o, (select new.comment, new.z_invert_up, new.z_invert_down, new.cross_section_type, new.h_sable, new.branch, new.rk, new.custom_length, new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.comment, new.z_invert_up, new.z_invert_down, new.cross_section_type, new.h_sable, new.branch, new.rk, new.custom_length, new.circular_diameter, new.ovoid_height, new.ovoid_top_diameter, new.ovoid_invert_diameter, new.cp_geom, new.op_geom) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from ${model}.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._pipe_link set comment=new.comment, z_invert_up=new.z_invert_up, z_invert_down=new.z_invert_down, cross_section_type=new.cross_section_type, h_sable=new.h_sable, branch=new.branch, rk=new.rk, custom_length=new.custom_length, circular_diameter=new.circular_diameter, ovoid_height=new.ovoid_height, ovoid_top_diameter=new.ovoid_top_diameter, ovoid_invert_diameter=new.ovoid_invert_diameter, cp_geom=new.cp_geom, op_geom=new.op_geom where id=old.id;

            if 'pipe' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
               perform $model.branch_update_fct();
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'pipe_link');

            -- Update of connected manholes for connectivity check if pipe
            if 'pipe' = 'pipe' and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update $model.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select (z_invert_up is not null) and (z_invert_down is not null) and (h_sable is not null) and (cross_section_type is not null) and (cross_section_type not in ('valley')) and (rk is not null) and (rk >0) and (cross_section_type!='circular' or (circular_diameter is not null and circular_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)) and (cross_section_type!='ovoid' or (ovoid_height is not null and ovoid_height >0 )) and (cross_section_type!='ovoid' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)) and (cross_section_type!='pipe' or cp_geom is not null) and (cross_section_type!='channel' or op_geom is not null) from  $model._pipe_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._pipe_link where id=old.id;
            delete from $model._link where id=old.id;
            if 'pipe' = 'pipe' then
               perform $model.branch_update_fct();

                -- Update of connected manholes for connectivity check if pipe
                if 'pipe' = 'pipe' then
                    update $model.manhole_node set geom=geom where id=old.up or id=old.down;
                end if;
            end if;
            return old;
        end if;
    end;
$$$$;;


/* ********************************************** */
/*  Fix configs                                   */
/* ********************************************** */

alter table ${model}.metadata drop column if exists is_switching;
;;

alter table ${model}.metadata drop column if exists switching;
;;

create or replace function ${model}.metadata_configuration_after_update_fct()
returns trigger
language plpgsql
as $$$$
    declare
        node record;
        singularity record;
        link record;
        update_fields varchar;
        update_arrays varchar;
        json_fields varchar;
        config_name varchar;
    begin
        update $model.config_switch set is_switching=true;

        -- Update nodes that have a configuration
        for node in select * from $model._node where configuration is not null loop
            select name from $model.configuration where id=new.configuration into config_name;
            if config_name not in (select k from json_object_keys(node.configuration) as k) then
                select 'default' into config_name;
            end if;

            -- json switch must be done first to handle trigger on other fields with correct values
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||node.node_type::varchar||'_node'
            and data_type = 'json'
            and column_name not in ('id', 'node_type', 'name', 'geom', 'configuration')
            into json_fields;

            if json_fields is not null then
                execute 'update $model.'||node.node_type::varchar||'_node '||
                             'set '||json_fields||' where id='||node.id::varchar||';';
            end if;

            -- user defined types
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||udt_name, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||node.node_type::varchar||'_node'
            and data_type = 'USER-DEFINED'
            and column_name not in ('id', 'node_type', 'name', 'geom', 'configuration')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||node.node_type::varchar||'_node '||
                             'set '||update_fields||' where id='||node.id::varchar||';';
            end if;

            -- common fields
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||node.node_type::varchar||'_node'
            and data_type != 'USER-DEFINED'
            and data_type != 'ARRAY'
            and data_type != 'json'
            and column_name not in ('id', 'node_type', 'name', 'geom', 'configuration')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||node.node_type::varchar||'_node '||
                             'set '||update_fields||' where id='||node.id::varchar||';';
            end if;

            -- assume that all arrays are real[] (it is the case in v1.0.0)
            select string_agg(column_name||'=replace(replace(configuration_json->'''||config_name||'''->>'''||column_name||''', ''['' , ''{''), '']'', ''}'')::real[]', ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||node.node_type::varchar||'_node'
            and data_type = 'ARRAY'
            and column_name not in ('id', 'node_type', 'name', 'geom', 'configuration')
            into update_arrays;

            if update_arrays is not null then
                execute 'update $model.'||node.node_type::varchar||'_node '||
                             'set '||update_arrays||' where id='||node.id::varchar||';';
            end if;
        end loop;

        -- Update singularities that have a configuration
        for singularity in select * from $model._singularity where configuration is not null loop
            select name from $model.configuration where id=new.configuration into config_name;
            if config_name not in (select k from json_object_keys(singularity.configuration) as k) then
                select 'default' into config_name;
            end if;

            -- json switch must be done first to handle trigger on other fields with correct values
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||singularity.singularity_type::varchar||'_singularity'
            and data_type = 'json'
            and column_name not in ('id', 'singularity_type', 'name', 'geom', 'configuration')
            into json_fields;

            if json_fields is not null then
                execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                             'set '||json_fields||' where id='||singularity.id::varchar||';';
            end if;

            -- user defined types
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||udt_name, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||singularity.singularity_type::varchar||'_singularity'
            and data_type = 'USER-DEFINED'
            and column_name not in ('id', 'singularity_type', 'name', 'geom', 'configuration')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                             'set '||update_fields||' where id='||singularity.id::varchar||';';
            end if;

            -- common fields
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||singularity.singularity_type::varchar||'_singularity'
            and data_type != 'USER-DEFINED'
            and data_type != 'ARRAY'
            and data_type != 'json'
            and column_name not in ('id', 'singularity_type', 'name', 'geom', 'configuration')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                             'set '||update_fields||' where id='||singularity.id::varchar||';';
            end if;

            -- assume that all arrays are real[] (it is the case in v1.0.0)
            select string_agg(column_name||'=replace(replace(configuration_json->'''||config_name||'''->>'''||column_name||''', ''['' , ''{''), '']'', ''}'')::real[]', ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||singularity.singularity_type::varchar||'_singularity'
            and data_type = 'ARRAY'
            and column_name not in ('id', 'singularity_type', 'name', 'geom', 'configuration')
            into update_arrays;

            if update_arrays is not null then
                execute 'update $model.'||singularity.singularity_type::varchar||'_singularity '||
                             'set '||update_arrays||' where id='||singularity.id::varchar||';';
            end if;
        end loop;

        -- Update links that have a configuration
        for link in select * from $model._link where configuration is not null loop
            select name from $model.configuration where id=new.configuration into config_name;
            if config_name not in (select k from json_object_keys(link.configuration) as k) then
                select 'default' into config_name;
            end if;

            -- json switch must be done first to handle trigger on other fields with correct values
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||link.link_type::varchar||'_link'
            and data_type = 'json'
            and column_name not in ('id', 'link_type', 'name', 'geom', 'configuration', 'branch')
            into json_fields;

            if json_fields is not null then
                execute 'update $model.'||link.link_type::varchar||'_link '||
                             'set '||json_fields||' where id='||node.id::varchar||';';
            end if;

            -- user defined types
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||udt_name, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||link.link_type::varchar||'_link'
            and data_type = 'USER-DEFINED'
            and column_name not in ('id', 'link_type', 'name', 'geom', 'configuration', 'branch')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||link.link_type::varchar||'_link '||
                             'set '||update_fields||' where id='||link.id::varchar||';';
            end if;

            -- common fields
            select string_agg(column_name||'=(configuration_json->'''||config_name||'''->>'''||column_name||''')::'||data_type, ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||link.link_type::varchar||'_link'
            and data_type != 'USER-DEFINED'
            and data_type != 'ARRAY'
            and data_type != 'json'
            and column_name not in ('id', 'link_type', 'name', 'geom', 'configuration', 'branch')
            into update_fields;

            if update_fields is not null then
                execute 'update $model.'||link.link_type::varchar||'_link '||
                             'set '||update_fields||' where id='||link.id::varchar||';';
            end if;

            -- assume that all arrays are real[] (it is the case in v1.0.0)
            select string_agg(column_name||'=replace(replace(configuration_json->'''||config_name||'''->>'''||column_name||''', ''['' , ''{''), '']'', ''}'')::real[]', ', ')
            from information_schema.columns
            where table_schema='$model'
            and table_name='_'||link.link_type::varchar||'_link'
            and data_type = 'ARRAY'
            and column_name not in ('id', 'link_type', 'name', 'geom', 'configuration', 'branch')
            into update_arrays;

            if update_arrays is not null then
                execute 'update $model.'||link.link_type::varchar||'_link '||
                             'set '||update_arrays||' where id='||link.id::varchar||';';
            end if;
        end loop;

        update $model.config_switch set is_switching=false;
        return new;
    end;
$$$$
;;

create or replace view $model.configured as
    select CONCAT('node:', n.id) as id,
        configuration,
        name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom
        from $model._node as n
        where configuration is not null
    union all
    select CONCAT('link:', l.id) as id,
        configuration,
        name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom
        from $model._link as l
        where configuration is not null
    union all
    select CONCAT('singularity:', s.id) as id,
        s.configuration,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom
        from $model._singularity as s join $model._node as n on s.node = n.id
        where s.configuration is not null
;;

