# -*- coding: utf-8 -*-

"""
***************************************************************************
    slopearea.py
    ---------------------
    Date                 : June 2012
    Copyright            : (C) 2012-2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'June 2012'
__copyright__ = '(C) 2012-2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.core import (QgsProcessingException,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterRasterDestination
                      )


from .taudemAlgorithm import TauDemAlgorithm
from . import taudemUtils


class SlopeArea(TauDemAlgorithm):

    SLOPE = 'SLOPE'
    AREA = 'AREA'
    SLOPE_EXPONENT = 'SLOPE_EXPONENT'
    AREA_EXPONENT = 'AREA_EXPONENT'
    SLOPE_AREA = 'SLOPE_AREA'

    def name(self):
        return 'slopearea'

    def displayName(self):
        return self.tr('Slope area combination')

    def group(self):
        return self.tr('Stream network analysis')

    def groupId(self):
        return 'streamanalysis'

    def tags(self):
        return self.tr('dem,hydrology,dem,threshold,compare').split(',')

    def shortHelpString(self):
        return self.tr('Creates a grid of slope-area values = (S^m)·(A^n) '
                       'based on slope and specific catchment area grid '
                       'inputs, and parameters m and n.')

    def helpUrl(self):
        return 'http://hydrology.usu.edu/taudem/taudem5/help53/SlopeAreaCombination.html'

    def __init__(self):
        super().__init__()

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterRasterLayer(self.SLOPE,
                                                            self.tr('Slope')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.AREA,
                                                            self.tr('Area')))
        self.addParameter(QgsProcessingParameterNumber(self.SLOPE_EXPONENT,
                                                       self.tr('Slope exponent'),
                                                       QgsProcessingParameterNumber.Double,
                                                       defaultValue=2.0))
        self.addParameter(QgsProcessingParameterNumber(self.AREA_EXPONENT,
                                                       self.tr('Area exponent'),
                                                       QgsProcessingParameterNumber.Double,
                                                       defaultValue=1.0))

        self.addParameter(QgsProcessingParameterRasterDestination(self.SLOPE_AREA,
                                                                  self.tr('Slope area')))

    def processAlgorithm(self, parameters, context, feedback):
        slopeLayer = self.parameterAsRasterLayer(parameters, self.SLOPE, context)
        if slopeLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.SLOPE))

        areaLayer = self.parameterAsRasterLayer(parameters, self.AREA, context)
        if areaLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.AREA))

        arguments = []
        arguments.append(self.command())

        arguments.append('-slp')
        arguments.append(slopeLayer.source())
        arguments.append('-sca')
        arguments.append(areaLayer.source())

        arguments.append('-par')
        arguments.append('{}'.format(self.parameterAsDouble(parameters, self.SLOPE_EXPONENT, context)))
        arguments.append('{}'.format(self.parameterAsDouble(parameters, self.AREA_EXPONENT, context)))

        arguments.append('-sa')
        arguments.append(self.parameterAsOutputLayer(parameters, self.SLOPE_AREA, context))

        taudemUtils.execute(arguments, feedback)

        return self.algorithmResults(parameters)
