# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
from builtins import str
from qgis.PyQt.QtWidgets import QWidget, QGridLayout, QApplication
from qgis.PyQt.QtGui import QStandardItem, QStandardItemModel
from qgis.PyQt import uic
from qgis.PyQt.QtCore import Qt
from hydra.gui.base_dialog import BaseDialog, tr
from hydra.gui.scenario_group_run import ScenarioGroupRun
import os

class ScenarioGrouping(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "scenario_grouping.ui"), self)

        self.project = project

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.btnRunGroup.clicked.connect(self.run)
        self.btnAddGroup.clicked.connect(self.add_group)
        self.btnDelGroup.clicked.connect(self.delete_group)
        self.btnAddScnGroup.clicked.connect(self.add_scn_to_group)
        self.btnRemoveScnGroup.clicked.connect(self.remove_scn_from_group)

        self.__list_scenario = QStandardItemModel(self.listScenarios)
        self.__tree_groups = QStandardItemModel(self.treeGroups)
        self.listScenarios.setModel(self.__list_scenario)
        self.treeGroups.setModel(self.__tree_groups)
        self.__refresh_lists_groups()
        self.treeGroups.setCurrentIndex(self.__tree_groups.index(0, 0))
        self.show()

    def run(self):
        id = self.get_selected_group()
        if not id is None:
            run_dlg = ScenarioGroupRun(self.project, id, self)
            run_dlg.exec_()

    def add_group(self):
        self.project.execute("""insert into project.scenario_group default values""")
        self.__refresh_lists_groups()

    def remove_scn_from_group(self):
        id_group, id_scn = self.get_selected_group_scn()
        if id_scn is None:
            return
        self.project.execute("""delete from project.param_scenario_group where grp=%i and scenario=%i"""%(id_group, id_scn))
        self.__refresh_lists_groups()

    def add_scn_to_group(self):
        id_group = self.get_selected_group()
        if id_group is None:
            return
        id_scn = self.get_selected_scn()
        if id_scn is None:
            return
        self.project.execute("""insert into project.param_scenario_group(grp, scenario) values (%i, %i)"""%(id_group, id_scn))
        self.__refresh_lists_groups()

    def get_selected_scn(self):
        items = self.listScenarios.selectedIndexes()
        if len(items)>0:
            name = self.__list_scenario.item(items[0].row(),0).text()
            id, = self.project.execute("""select id from project.scenario where name='%s'"""%(name)).fetchone()
            return int(id)
        return None

    def get_selected_group_scn(self):
        items = self.treeGroups.selectedIndexes()
        if len(items)>0:
            scn_index = items[0]
            group_index = scn_index.parent()
            if self.__tree_groups.data(group_index) is None:
                return None, None

            scn_name = str(self.__tree_groups.data(scn_index))
            id_scn, = self.project.execute("""select id from project.scenario where name='%s'"""%(scn_name)).fetchone()
            group_name = str(self.__tree_groups.data(group_index))
            id_group, = self.project.execute("""select id from project.scenario_group where name='%s'"""%(group_name)).fetchone()
            return int(id_group), int(id_scn)
        return None, None

    def get_selected_group(self):
        items = self.treeGroups.selectedIndexes()
        if len(items)>0:
            selected_item = items[0]
            if self.__tree_groups.data(selected_item.parent()) is not None:
                return None # A scenario is selected, not a group
            name = self.__tree_groups.item(items[0].row(),0).text()
            id, = self.project.execute("""select id from project.scenario_group where name='%s'"""%(name)).fetchone()
            return int(id)
        return None

    def delete_group(self):
        id = self.get_selected_group()
        if not id is None:
            self.project.execute("""delete from project.scenario_group where id=%i"""%(id))
            self.__refresh_lists_groups()

    def __refresh_lists_groups(self):
        self.__list_scenario.setRowCount(0)
        self.__tree_groups.setRowCount(0)
        available_scenarios = self.project.execute("""
            select name
            from project.scenario as scn
            where scn.id not in (
                select scenario
                from project.param_scenario_group
                )""").fetchall()

        for scn in available_scenarios:
            item = QStandardItem(scn[0])
            item.setEditable(False)
            self.__list_scenario.appendRow(item)

        groups = self.project.execute("""
            select id, name
            from project.scenario_group""").fetchall()

        for group in groups:
            node_group = QStandardItem(group[1])
            node_group.setEditable(False)
            self.__tree_groups.appendRow(node_group)
            scenario_groups = self.project.execute("""
                select name
                from project.scenario as scn
                where scn.id in (
                    select scenario from project.param_scenario_group
                    where grp = %i)
                """%(int(group[0]))).fetchall()
            for scn in scenario_groups:
                item = QStandardItem(scn[0])
                item.setEditable(False)
                node_group.appendRow(item)

        self.treeGroups.expandAll()

    def save(self):
        BaseDialog.save(self)
        self.close()
