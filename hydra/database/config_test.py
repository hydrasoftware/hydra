# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if config mechanics are working

USAGE

   python -m hydra.config_test [-hk]

OPTIONS

   -h, --help
        print this help

   -k, --keep
        does not delete DB at the end
"""

from __future__ import absolute_import # important to read the doc !
import os
import getopt
import sys, traceback
from hydra.project import Project
from hydra.model import Model
from hydra.database.database import remove_project, TestProject, project_exists

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hk",
            ["help" "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

keep = True if "-k" in optlist or "--keep" in optlist else False

if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

project_name = "config_test"

if project_exists(project_name):
    remove_project(project_name)

test_project = TestProject(project_name, keep)
project = Project.load_project(project_name)

project.add_new_model("mod")
model = Model(project, 2154, 'mod')

project.execute("""insert into mod.manhole_node (area, z_ground, geom) values (2, 60.00, 'SRID=2154; POINTZ(987690 6560670 9999)'::geometry);""")
project.execute("""insert into mod.manhole_node (area, z_ground, geom) values (2, 57.50, 'SRID=2154; POINTZ(987860 6560480 9999)'::geometry);""")
project.execute("""insert into mod.manhole_node (area, z_ground, geom) values (2, 57.50, 'SRID=2154; POINTZ(988860 6560480 9999)'::geometry);""")
project.execute("""insert into mod.configuration default values;""")
project.execute("""insert into mod.configuration default values;""")

project.execute("""update mod.metadata set configuration=2;""")

#### test Node
# check select when there is no configuration for object
manhole = project.execute("""select area,z_ground from mod.manhole_node where id=1;""").fetchone()
assert(manhole[0]==2.0)
assert(manhole[1]==60.0)

# check update when not in default configuration
project.execute("""update mod.manhole_node set area=3, z_ground=65 where id=1;""")

manhole = project.execute("""select area,z_ground from mod.manhole_node where id=1;""").fetchone()
assert(manhole[0]==3.0)
assert(manhole[1]==65.0)

# switch back to default configuration and check that default values are retrieved
project.execute("""update mod.metadata set configuration=1;""")

manhole = project.execute("""select area,z_ground from mod.manhole_node where id=1;""").fetchone()
assert(manhole[0]==2.0)
assert(manhole[1]==60.0)

# switch again to configuration 1 and check that configuration values are retrieved
project.execute("""update mod.metadata set configuration=2;""")

manhole = project.execute("""select area,z_ground from mod.manhole_node where id=1;""").fetchone()
assert(manhole[0]==3.0)
assert(manhole[1]==65.0)

#### test Singularity
# test froude in configuration 2 because froude have no extra field
project.execute("""insert into mod.froude_bc_singularity (geom) values ('SRID=2154; POINTZ(987690 6560670 9999)'::geometry);""")
project.execute("""update mod.froude_bc_singularity set name='new_name' where id=1;""")
froude = project.execute("""select name from mod.froude_bc_singularity where id=1;""").fetchone()
assert(froude[0]=='new_name')
# switch to default configuration
project.execute("""update mod.metadata set configuration=1;""")

#switch back to configuration 1
project.execute("""update mod.metadata set configuration=2;""")

# test singularity with extra field, without configuration in configuration 1
project.execute("""insert into mod.weir_bc_singularity (z_weir, width, cc, geom) values (2, 3, 4, 'SRID=2154; POINTZ(987860 6560480 9999)'::geometry);""")
weir_bc = project.execute("""select z_weir, width, cc from mod.weir_bc_singularity where id=2;""").fetchone()
assert(weir_bc[0]==2.0)
assert(weir_bc[1]==3.0)
assert(weir_bc[2]==4.0)

# update weir_bc in configuration 1
project.execute("""update mod.weir_bc_singularity set z_weir=3, width=4, cc=5 where id=2;""")
weir_bc = project.execute("""select z_weir, width, cc from mod.weir_bc_singularity where id=2;""").fetchone()
assert(weir_bc[0]==3.0)
assert(weir_bc[1]==4.0)
assert(weir_bc[2]==5.0)

# switch to default configuration and check that default values are retrieved
project.execute("""update mod.metadata set configuration=1;""")
weir_bc = project.execute("""select z_weir, width, cc from mod.weir_bc_singularity where id=2;""").fetchone()
assert(weir_bc[0]==2.0)
assert(weir_bc[1]==3.0)
assert(weir_bc[2]==4.0)

# test singularity with array field
project.execute("""insert into mod.tz_bc_singularity (tz_array, geom) values ('{{1, 2}, {3, 4}}'::real[], 'SRID=2154; POINTZ(988860 6560480 9999)'::geometry);""")
tz_bc = project.execute("""select tz_array from mod.tz_bc_singularity where id=3;""").fetchone()
assert(tz_bc[0][0][0]==1.0)
assert(tz_bc[0][0][1]==2.0)
assert(tz_bc[0][1][0]==3.0)
assert(tz_bc[0][1][1]==4.0)

# switch to configuration 1
project.execute("""update mod.metadata set configuration=2;""")

# update tz_bc_singularity in configuration 1
project.execute("""update mod.tz_bc_singularity set tz_array='{{5, 6}, {7, 8}}'::real[] where id=3;""")
tz_bc = project.execute("""select tz_array from mod.tz_bc_singularity where id=3;""").fetchone()
assert(tz_bc[0][0][0]==5.0)
assert(tz_bc[0][0][1]==6.0)
assert(tz_bc[0][1][0]==7.0)
assert(tz_bc[0][1][1]==8.0)

# switch back to default configuration
project.execute("""update mod.metadata set configuration=1;""")
tz_bc = project.execute("""select tz_array from mod.tz_bc_singularity where id=3;""").fetchone()
assert(tz_bc[0][0][0]==1.0)
assert(tz_bc[0][0][1]==2.0)
assert(tz_bc[0][1][0]==3.0)
assert(tz_bc[0][1][1]==4.0)

#### test Link
cp_geom_1 = project.execute("""insert into mod.closed_parametric_geometry(zbmin_array) VALUES ('{{50.0999985,10.5},{51,11},{51.2000008,11.3000002},{51.5,11.8000002},{52,12},{53,13}}') returning id;""").fetchone()[0]
cp_geom_2 = project.execute("""insert into mod.closed_parametric_geometry(zbmin_array) VALUES ('{{51.0999985,11.5},{52,12},{52.2000008,12.3000002},{52.5,12.8000002},{54,14},{58,18}}') returning id;""").fetchone()[0]

project.execute("""insert into mod.pipe_link (h_sable, rk, geom, cp_geom, cross_section_type) values (1, 2, 'SRID=2154; LINESTRINGZ(987690 6560670 9999, 987860 6560480 9999)'::geometry, {}, 'pipe');""".format(cp_geom_1))
pipe = project.execute("""select h_sable, rk from mod.pipe_link where id=1;""").fetchone()
assert(pipe[0]==1.0)
assert(pipe[1]==2.0)

project.commit()

#switch to configuration 1
project.execute("""update mod.metadata set configuration=2;""")
pipe = project.execute("""select h_sable, rk from mod.pipe_link where id=1;""").fetchone()
assert(pipe[0]==1.0)
assert(pipe[1]==2.0)

# update pipe link
project.execute("""update mod.pipe_link set h_sable=2, rk=3, cp_geom={} where id=1""".format(cp_geom_2))
pipe = project.execute("""select h_sable, rk, cp_geom from mod.pipe_link where id=1;""").fetchone()
assert(pipe[0]==2.0)
assert(pipe[1]==3.0)
assert(pipe[2]==cp_geom_2)

project.commit()

# switch back to default configuration
project.execute("""update mod.metadata set configuration=1;""")
pipe = project.execute("""select h_sable, rk, cp_geom from mod.pipe_link where id=1;""").fetchone()
assert(pipe[0]==1.0)
assert(pipe[1]==2.0)
assert(pipe[2]==cp_geom_1)

project.commit()

project.execute("""delete from mod.closed_parametric_geometry where id={};""".format(cp_geom_2))

#switch to configuration 1
project.execute("""update mod.metadata set configuration=2;""")
pipe = project.execute("""select h_sable, rk, cp_geom from mod.pipe_link where id=1;""").fetchone()
assert(pipe[0]==2.0)
assert(pipe[1]==3.0)
assert(pipe[2] is None)

project.commit()

# switch back to default configuration
project.execute("""update mod.metadata set configuration=1;""")
# create catchment (hydra_types test) and storage (arrays test)
id_catchment =project.execute("""insert into mod.catchment_node (area_ha, rl, slope, c_imp, netflow_type, constant_runoff, runoff_type, socose_tc_mn, socose_shape_param_beta, q_limit, q0, geom)
                    values (1, 2, 0.5, 0.75, 'constant_runoff', 5, 'Define Tc', 60, 4, 999, 1, 'SRID=2154; POINTZ(987695 6560675 9999)'::geometry) returning id;""").fetchone()[0]
id_storage = project.execute("""insert into mod.storage_node (zs_array, zini, geom) values ('{{10, 11}, {12, 13}}'::real[], 10, 'SRID=2154; POINTZ(987700 6560680 9999)'::geometry) returning id;""").fetchone()[0]

project.commit()

#switch to configuration 1
project.execute("""update mod.metadata set configuration=2;""")
project.execute("""update mod.catchment_node set netflow_type='scs', scs_j_mm=34, scs_soil_drainage_time_day=4, scs_rfu_mm=99 , runoff_type='Define K', define_k_mn=30 where id={};""".format(id_catchment))

project.commit()

#switch to configuration 2
project.execute("""update mod.metadata set configuration=3;""")
project.execute("""update mod.catchment_node set netflow_type='horner', horner_ini_loss_coef=0.8, horner_recharge_coef=0.15, runoff_type='Define K', define_k_mn=30 where id={};""".format(id_catchment))
project.execute("""update mod.storage_node set zs_array='{{20, 21}, {22, 23}, {24, 25}}'::real[] where id="""+str(id_storage))

# test config deletion on items
model.drop_config_on_object(3, 'catchment_node', id_catchment)
model.drop_config_on_object(3, 'storage_node', id_storage)
catchment = project.execute("""select area_ha, rl, slope, c_imp, netflow_type, constant_runoff, horner_ini_loss_coef, horner_recharge_coef, runoff_type, socose_tc_mn, socose_shape_param_beta, define_k_mn from mod.catchment_node where id={};""".format(id_catchment)).fetchone()
storage = project.execute("""select zs_array, zini from mod.storage_node where id={};""".format(id_storage)).fetchone()

assert(catchment[0]==1)
assert(catchment[1]==2)
assert(catchment[2]==0.5)
assert(catchment[3]==0.75)
assert(catchment[4]=='constant_runoff')
assert(catchment[5]==5)
assert(catchment[6]==0.8)
assert(catchment[7]==0.15)
assert(catchment[8]=='Define Tc')
assert(catchment[9]==60)
assert(catchment[10]==4)
assert(catchment[11]==30)
assert(storage[0]==[[10, 11], [12, 13]])
assert(storage[1]==10)

project.commit()

# test of PTR config (and specifically up_vcs_geom)

project.execute("""update mod.metadata set configuration=1;""")
valley_1 = project.execute("""insert into mod.valley_cross_section_geometry(zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, rlambda, rmu1, rmu2) VALUES ('{{50.0999985,10.5},{51,11},{51.2000008,11.3000002},{51.5,11.8000002},{52,12},{53,13}}', '{{53.0999985,13.5},{54,14},{55,15},{56,16}}', '{{53.0999985,13.5},{54,14},{55,15},{56,16}}', 0.899999976, -1, -1) returning id;""").fetchone()[0]
valley_2 = project.execute("""insert into mod.valley_cross_section_geometry(zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, rlambda, rmu1, rmu2) VALUES ('{{51.0999985,11.5},{52,12},{52.2000008,12.3000002},{52.5,12.8000002},{54,14},{58,18}}', '{{56.0999985,16.5},{58,18},{59,19}}', '{{54.0999985,14.5},{55,15},{56,16},{58,18}}', 0.299999976, -1, -1) returning id;""").fetchone()[0]

project.execute("""insert into mod.reach (name, pk0_km, dx, geom) values ('101', 100, 50, 'SRID=2154; LINESTRINGZ(987700 6560700 9999, 987750 6560780 9999)'::geometry);""")
ptr_id = project.execute("""insert into mod.river_cross_section_profile(geom, name, z_invert_up, type_cross_section_up, up_rk, up_rk_maj, up_sinuosity, up_vcs_geom)
                        values('SRID=2154; POINTZ(987700 6560700 9999)'::geometry, 'SEC1', 99, 'valley', 20, 40, 10, {}) returning id;""".format(valley_1)).fetchone()[0]

project.commit()

# switch to config to set config on foreign key up_vcs_geom
project.execute("""update mod.metadata set configuration=3;""")
project.execute("""update mod.river_cross_section_profile
                    set (z_invert_up, up_rk, up_rk_maj, up_sinuosity, up_vcs_geom)=(101, 25, 35, 66, {}) where id={};""".format(valley_2, ptr_id))

project.commit()

# back to default
project.execute("""update mod.metadata set configuration=1;""")
z_up, up_rk, up_rk_maj, sinuosity, vcs_key = project.execute("""select z_invert_up, up_rk, up_rk_maj, up_sinuosity, up_vcs_geom from mod.river_cross_section_profile where id={};""".format(ptr_id)).fetchone()
assert z_up ==99
assert up_rk==20
assert up_rk_maj==40
assert sinuosity==10
assert vcs_key==valley_1
project.execute("""delete from mod.valley_cross_section_geometry where id={};""".format(valley_2))

project.commit()

# back to config 3
project.execute("""update mod.metadata set configuration=3;""")
z_up, up_rk, up_rk_maj, sinuosity, vcs_key = project.execute("""select z_invert_up, up_rk, up_rk_maj, up_sinuosity, up_vcs_geom from mod.river_cross_section_profile where id={};""".format(ptr_id)).fetchone()
assert z_up ==101
assert up_rk==25
assert up_rk_maj==35
assert sinuosity==66
assert vcs_key is None

# back to default, change a config name
project.execute("""update mod.metadata set configuration=1;""")
project.execute("""update mod.configuration set name='name_changed' where id=3;""")
project.execute("""update mod.metadata set configuration=3;""")
z_up, up_rk, up_rk_maj, sinuosity, vcs_key = project.execute("""select z_invert_up, up_rk, up_rk_maj, up_sinuosity, up_vcs_geom from mod.river_cross_section_profile where id={};""".format(ptr_id)).fetchone()
assert z_up ==101
assert up_rk==25
assert up_rk_maj==35
assert sinuosity==66
assert vcs_key is None

project.commit()

# delete config
project.execute("""delete from mod.configuration where id=2;""")

project.commit()

print('ok')
