# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if config mechanics are working

USAGE

   python -m hydra.settings_trigger_test [-dhk]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -k, --keep
        does not delete DB at the end
"""

import sys
import os
import getopt
from hydra.project import Project
from hydra.database.database import remove_project, TestProject, project_exists

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdk",
            ["help", "debug", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

keep = True if "-k" in optlist or "--keep" in optlist else False

if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

project_name = "trigger_test"

if project_exists(project_name):
    remove_project(project_name)

test_project = TestProject(project_name, keep)
project = Project.load_project(project_name)

project.add_new_model("mod")

project.execute("""insert into mod.constrain (geom) values ('SRID=2154; LINESTRINGZ(1 0 9999, 1 1 9999, 2 1 9999)'::geometry);""")
project.execute("""insert into mod.constrain (geom) values ('SRID=2154; LINESTRINGZ(2 1 9999, 2 0 9999, 1 0 9999)'::geometry);""")

project.execute("""insert into mod.manhole_node (geom) values ('SRID=2154; POINTZ(0 0 9999)'::geometry);""")
project.execute("""insert into mod.manhole_node (geom) values ('SRID=2154; POINTZ(0 1 9999)'::geometry);""")
project.execute("""insert into mod.manhole_node (geom) values ('SRID=2154; POINTZ(0 2 9999)'::geometry);""")
project.execute("""insert into mod.pipe_link (geom) values ('SRID=2154; LINESTRINGZ(0 0 9999, 0 1 9999)'::geometry);""")
project.execute("""insert into mod.pipe_link (geom) values ('SRID=2154; LINESTRINGZ(0 1 9999, 0 2 9999)'::geometry);""")

project.commit()

assert(project.execute("select count(1) from mod.coverage").fetchone()[0] == 1)
assert(project.execute("select count(1) from mod.branch").fetchone()[0] == 1)

project.execute("""update mod.metadata set(trigger_coverage, trigger_branch) = ('f', 'f')""")
project.commit()

project.execute("""insert into mod.constrain (geom) values ('SRID=2154; LINESTRINGZ(2 0 9999, 3 0 9999, 3 1 9999, 2 1 9999)'::geometry);""")

project.execute("""insert into mod.manhole_node (geom) values ('SRID=2154; POINTZ(-1 1 9999)'::geometry);""")
project.execute("""insert into mod.pipe_link (geom) values ('SRID=2154; LINESTRINGZ(0 1 9999, -1 1 9999)'::geometry);""")

project.commit()

assert(project.execute("select count(1) from mod.coverage").fetchone()[0] == 1)
assert(project.execute("select count(1) from mod.branch").fetchone()[0] == 1)

project.execute("select mod.coverage_update();")
project.execute("select mod.branch_update_fct();")

project.commit()

assert(project.execute("select count(1) from mod.coverage").fetchone()[0] == 2)
assert(project.execute("select count(1) from mod.branch").fetchone()[0] == 3)

print('ok')
