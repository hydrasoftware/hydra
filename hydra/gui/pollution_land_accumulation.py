# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from qgis.PyQt import uic, QtGui, QtCore
from qgis.PyQt.QtWidgets import QTableWidgetItem
from hydra.gui.base_dialog import BaseDialog, tr
import hydra.utility.string as string

class PollutionLandAccumulationManager(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "pollution_land_accumulation.ui"), self)

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.project = project

        sewage_storage_kgha, unitary_storage_kgha, regeneration_time_days,a_extraction_coef, b_extraction_coef = self.project.execute("""
            select sewage_storage_kgha, unitary_storage_kgha, regeneration_time_days,a_extraction_coef, b_extraction_coef
            from project.pollution_land_accumulation
            where id=1;
            """).fetchone()

        for i in range(4):
            for j in range(4):
                self.sewage_storage_kgha.setItem(i,j,QTableWidgetItem(string.get_str(sewage_storage_kgha[i][j])))
                self.unitary_storage_kgha.setItem(i,j,QTableWidgetItem(string.get_str(unitary_storage_kgha[i][j])))

        self.regeneration_time_days.setText(string.get_str(regeneration_time_days))
        self.a_extraction_coef.setText(string.get_str(a_extraction_coef))
        self.b_extraction_coef.setText(string.get_str(b_extraction_coef))

    def save(self):
        sewage_storage_kgha = [[0 for y in range(4)]for x in range(4)]
        unitary_storage_kgha = [[0 for y in range(4)]for x in range(4)]
        for i in range(4):
            for j in range(4):
                sewage_storage_kgha[i][j] = string.get_sql_forced_float(self.sewage_storage_kgha.item(i,j).text())
                unitary_storage_kgha[i][j] = string.get_sql_forced_float(self.unitary_storage_kgha.item(i,j).text())

        self.project.execute("""
            update project.pollution_land_accumulation set
            sewage_storage_kgha = '{}',
            unitary_storage_kgha = '{}',
            regeneration_time_days = {},
            a_extraction_coef = {},
            b_extraction_coef = {}
            where id=1;
            """.format(
                string.list_to_sql_array(sewage_storage_kgha),
                string.list_to_sql_array(unitary_storage_kgha),
                string.get_sql_float(self.regeneration_time_days.text()),
                string.get_sql_float(self.a_extraction_coef.text()),
                string.get_sql_float(self.b_extraction_coef.text())
                )
            )

        BaseDialog.save(self)
        self.close()
