# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
from shapely import wkb
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QWidget, QInputDialog, QLineEdit
from qgis.PyQt.QtCore import pyqtSignal
from .channel_geometry import ChannelGeometry

class ChannelSection(QWidget):

    geometry_changed = pyqtSignal(dict)
    transect_changed = pyqtSignal(list)
    section_changed = pyqtSignal(dict)
    new_geometry = pyqtSignal()

    def __init__(self, z_invert, rk, rk_maj, geometry, geometries, transect, terrain, parent=None):
        QWidget.__init__(self, parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "channel_section.ui"), self)
        self.__geometry = None
        self.geometry_layout.addWidget(QWidget())

        self.z_invert.setValue(z_invert if z_invert is not None else -999)
        self.rk.setValue(rk if rk is not None else  0)
        self.rk_maj.setValue(rk_maj if rk_maj is not None else  0)

        self.z_invert.valueChanged.connect(self.__section_parameters_changed)
        self.rk.valueChanged.connect(self.__section_parameters_changed)
        self.rk_maj.valueChanged.connect(self.__section_parameters_changed)

        self.new_geom.clicked.connect(self.new_geometry.emit)
        self.rename_geom.clicked.connect(self.__rename_geometry)

        self.__geometries = geometries
        self.__transect = wkb.loads(transect, True) if transect is not None else None
        self.__terrain = terrain

        assert(None not in geometries)
        idx = None
        for i, g in geometries.items():
            self.geometry_combo.addItem(g['name'], userData=i)
            if geometry == i:
                idx = self.geometry_combo.count()-1
        self.geometry_combo.addItem('')
        if idx is None:
            idx = self.geometry_combo.count()-1
        else:
            self.__set_geometry(idx)
        self.geometry_combo.setCurrentIndex(idx)
        self.geometry_combo.currentIndexChanged.connect(self.__set_geometry)

    def __rename_geometry(self):
        g_id = self.geometry_combo.itemData(self.geometry_combo.currentIndex())
        if g_id is not None:
            t = QInputDialog.getText(self, 'Rename', "New name", QLineEdit.Normal, self.__geometries[g_id]['name'])
            if t[1]:
                self.__geometries[g_id]['name'] = t[0]
                self.geometry_combo.setItemText(self.geometry_combo.currentIndex(), t[0])

    def __getattr__(self, name):
        if name == 'geom':
            return self.__geometry
        elif name == 'param':
            return {
            "z_invert": self.z_invert.value(),
            "rk": self.rk.value(),
            "rk_maj": self.rk_maj.value(),
            "op_geom": self.geometry_combo.itemData(self.geometry_combo.currentIndex())
            }
        else:
            raise AttributeError(name)

    def __set_geometry(self, idx):
        w = self.geometry_layout.takeAt(0)
        w and w.widget().setParent(None)
        g_id = self.geometry_combo.itemData(idx)
        cp_geom = self.__geometries[g_id]
        self.__geometry = ChannelGeometry(cp_geom['zbmin_array'], self.z_invert.value(), self.__transect, self.__terrain, parent=self)
        self.__geometry.geometry_changed.connect(self.geometry_changed.emit)
        self.geometry_layout.addWidget(self.__geometry)
        self.__section_parameters_changed()

    def __section_parameters_changed(self):
        self.__geometry.set_dz(self.z_invert.value())
        self.section_changed.emit(self.param)
        self.geometry_changed.emit(self.geom.param)
