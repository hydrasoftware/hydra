# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
import json
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QCoreApplication, Qt
from qgis.PyQt.QtWidgets import QMessageBox
from qgis.PyQt.QtGui import QColor
import hydra.utility.string as string
from hydra.utility.sql_json import instances as INSTANCES
from hydra.gui.widgets.array_widget import ArrayWidget
from hydra.gui.simple_link_dialog import SimpleLinkDialog, tr

# list all possible names for json params
fields=[]
combos={}
for law in INSTANCES['borda_headloss_link_type']:
    for field in list(INSTANCES['borda_headloss_link_type'][law]['params'].keys()):
        if field not in fields:
            fields.append(field)
        if INSTANCES['borda_headloss_link_type'][law]['params'][field]['type'][0:4] == 'enum':
            m = re.match(r'enum\((.*)\)', INSTANCES['borda_headloss_link_type'][law]['params'][field]['type'])
            enum = {v: i+1 for i, v in enumerate(m.group(1).replace("'","").split(', '))}
            if field not in list(combos.keys()):
                combos[field] = sorted([[str(i), str(k.replace('_', ' ').capitalize())] for k, i in enum.items() if k!='placeholder_do_not_display'], key = lambda x: int(x[0]))


class BordaHeadlossLinkEditor(SimpleLinkDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleLinkDialog.__init__(self, project, geom, "borda_headloss_link", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "borda_headloss_link.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.connector_no.setMinimum(0)
        self.connector_no.setMaximum(99)

        for combo_name, combo_values in combos.items():
            getattr(self, combo_name).addItems([': '.join(v) for v in combo_values])
            if 'computed' in [v[1].replace('_', ' ').lower() for v in combo_values]:
                getattr(self, combo_name).activated.connect(self.user_defined_changed)

        name, law_type, param_json, comment = self.project.execute("""
            select name, law_type, param::varchar, comment
            from {}.borda_headloss_link
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.name.setText(name)
        self.notes.setText(comment)
        self.populate_combo_from_sqlenum(self.law_type, "borda_headloss_link_type", law_type, show_id=True)
        for index in range(self.law_type.count()):
            if self.law_type.itemText(index).split(': ')[1] not in [INSTANCES['borda_headloss_link_type'][law_key]['description'] for law_key in list(INSTANCES['borda_headloss_link_type'].keys())]:
                    self.law_type.setItemData(index,QColor(255,0,0),Qt.ForegroundRole)

        qh_array = None

        if param_json is not None and law_type is not None:
            self.param_dict = json.loads(param_json)
            for param_name, param_value in self.param_dict[law_type].items():
                if law_type in list(INSTANCES['borda_headloss_link_type'].keys()):
                    if param_name in list(INSTANCES['borda_headloss_link_type'][self.__law_type()]['params'].keys()):
                        if INSTANCES['borda_headloss_link_type'][self.__law_type()]['params'][param_name]['type'] == 'real':
                            getattr(self, param_name).setText(string.get_str(param_value))
                        elif INSTANCES['borda_headloss_link_type'][self.__law_type()]['params'][param_name]['type'][0:4] == 'enum':
                            m = re.match(r'enum\((.*)\)', INSTANCES['borda_headloss_link_type'][self.__law_type()]['params'][param_name]['type'])
                            enum = {v: i+1 for i, v in enumerate(m.group(1).replace("'","").split(', '))}
                            getattr(self, param_name).setCurrentIndex(getattr(self, param_name).findText(': '.join([str(enum[param_value]), param_value.replace('_', ' ').capitalize()])))
                        elif INSTANCES['borda_headloss_link_type'][self.__law_type()]['params'][param_name]['type'] == 'integer':
                            getattr(self, param_name).setValue(int(string.get_sql_float(param_value)))
                        elif param_name == 'qh_array':
                            qh_array = param_value

        self.array = ArrayWidget(["Discharge (m3/s)", "Headloss (m)"], [10,2], qh_array,
                draw_graph_func=self.__draw_graph, help_func=None, parent=self.qh_array)

        self.law_type.activated.connect(self.law_changed)
        self.law_changed()

        self.bend_shape.activated.connect(self.bend_shape_changed)
        self.bend_shape_changed()

        self.middle_option.activated.connect(self.middle_option_changed)
        self.middle_option_changed()
        self.connector_no.valueChanged.connect(self.connector_no_changed)
        self.connector_no_changed()

        self.coef_kv_12.textChanged.connect(self.__coef_kv_12_changed)
        self.coef_kv_21.textChanged.connect(self.__coef_kv_21_changed)
        if self.coef_kv_21.text() == '' or self.coef_kv_21.text() == self.coef_kv_12.text():
            self.coef_kv_copy = True
        else:
            self.coef_kv_copy = False


    def __draw_graph(self, datas):
        pass

    def law_changed(self):
        if self.__law_type() is not None and self.__law_type() not in list(INSTANCES['borda_headloss_link_type'].keys()):
            for field in fields:
                getattr(self, "label_"+field).hide()
                getattr(self, field).hide()
            QMessageBox(QMessageBox.Warning, tr('Incorrect law'), tr('{} law is not available any more. Please select another one.').format(self.__law_type().title()), QMessageBox.Ok).exec_()
        else:
            for field in fields:
                if field in self.__law_params():
                    getattr(self, "label_"+field).show()
                    getattr(self, field).show()
                    if field in list(combos.keys()) and field not in ['diffus_geom']:
                        if 'computed' in [v[1].replace('_', ' ').lower() for v in combos[field]]:
                            if getattr(self, field).currentText().split(': ')[1].replace('_', ' ').lower()=='computed':
                                getattr(self, field.replace('geom', 'cs_area')).setEnabled(False)
                                getattr(self, field.replace('geom', 'cs_zinvert')).setEnabled(False)
                            elif getattr(self, field).currentText().split(': ')[1].replace('_', ' ').lower()=='circular':
                                getattr(self, field.replace('geom', 'cs_area')).setEnabled(True)
                                getattr(self, field.replace('geom', 'cs_zinvert')).setEnabled(False)
                            else:
                                getattr(self, field.replace('geom', 'cs_area')).setEnabled(True)
                                getattr(self, field.replace('geom', 'cs_zinvert')).setEnabled(True)
                else:
                    getattr(self, "label_"+field).hide()
                    getattr(self, field).hide()
        self.bend_shape_changed()

    def bend_shape_changed(self):
        params = {'param1': {'elbow_90_deg': 'A0/B0', 'z_shaped_elbow': 'A0/B0', 'u_shaped_elbow': 'A0/B0', 'two_elbows_in_distinct_planes': 'A0/B0'},
                  'param2': {'elbow_90_deg': 'B1/B0', 'z_shaped_elbow': 'L0/B0', 'u_shaped_elbow': 'L0/B0', 'two_elbows_in_distinct_planes': 'L0/B0'},
                  'param3': {'elbow_90_deg': None, 'z_shaped_elbow': None, 'u_shaped_elbow': 'Bk/B0', 'two_elbows_in_distinct_planes': None},
                  'param4': {'elbow_90_deg': None, 'z_shaped_elbow': None, 'u_shaped_elbow': 'B1/B0', 'two_elbows_in_distinct_planes': None}}
        for param in list(params.keys()):
            bend_shape = self.bend_shape.currentText().split(': ')[1].replace(' ', '_').lower()
            if self.__law_type() == 'sharp_bend_rectangular' and bend_shape in ['elbow_90_deg', 'z_shaped_elbow', 'u_shaped_elbow', 'two_elbows_in_distinct_planes']:
                if params[param][bend_shape] is not None:
                    getattr(self, 'label_{}'.format(param)).setText(params[param][bend_shape])
                    getattr(self, 'label_{}'.format(param)).show()
                    getattr(self, param).show()
                elif params[param][bend_shape] is None:
                    getattr(self, 'label_{}'.format(param)).hide()
                    getattr(self, param).hide()
            else:
                getattr(self, 'label_{}'.format(param)).hide()
                getattr(self, param).hide()


    def user_defined_changed(self, combo_name):
        combo_name=self.sender().objectName()
        if self.sender().currentText().split(': ')[1].replace('_', ' ').lower()=='computed':
            getattr(self, combo_name.replace('geom', 'cs_area')).setEnabled(False)
            getattr(self, combo_name.replace('geom', 'cs_zinvert')).setEnabled(False)
        elif self.sender().currentText().split(': ')[1].replace('_', ' ').lower()=='circular':
            getattr(self, combo_name.replace('geom', 'cs_area')).setEnabled(True)
            getattr(self, combo_name.replace('geom', 'cs_zinvert')).setEnabled(False)
        else:
            getattr(self, combo_name.replace('geom', 'cs_area')).setEnabled(True)
            getattr(self, combo_name.replace('geom', 'cs_zinvert')).setEnabled(True)

    def middle_option_changed(self):
        if self.middle_option.currentText().split(': ')[1].replace(' ', '_').lower()=='no':
            self.middle_cs_area.setEnabled(False)
        else:
            self.middle_cs_area.setEnabled(True)

    def connector_no_changed(self):
        if self.connector_no.value() == 0:
            self.ls.setEnabled(False)
        else:
            self.ls.setEnabled(True)

    def __coef_kv_12_changed(self):
        if self.coef_kv_copy:
            self.coef_kv_21.setText(self.coef_kv_12.text())

    def __coef_kv_21_changed(self):
        if self.coef_kv_21.text() != self.coef_kv_12.text():
            self.coef_kv_copy = False

    def __law_type(self):
        if self.law_type.currentIndex() != -1:
            return self.project.execute("""
                select name
                from hydra.borda_headloss_link_type
                where description='{}'""".format(self.law_type.currentText().split(': ')[1])).fetchone()[0]
        else:
            return None

    def __law_params(self):
        if self.__law_type() != None:
            return list(INSTANCES['borda_headloss_link_type'][self.__law_type()]['params'].keys())
        else:
            return []

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))

        law_type=self.__law_type()

        params_dict = {}
        for field in self.__law_params():
            if INSTANCES['borda_headloss_link_type'][self.__law_type()]['params'][field]['type'] == 'real':
                params_dict[field] = string.get_sql_float(getattr(self, field).text())
            elif INSTANCES['borda_headloss_link_type'][self.__law_type()]['params'][field]['type'][0:4] == 'enum':
                params_dict[field] = getattr(self, field).currentText().split(': ')[1].replace(' ', '_').lower()
            elif INSTANCES['borda_headloss_link_type'][self.__law_type()]['params'][field]['type'] == 'integer':
                params_dict[field] = getattr(self, field).value()
            elif field == 'qh_array':
                params_dict[field] = self.array.get_table_items()

        params_json = json.dumps({law_type:params_dict})

        self.model.update_borda_link(self.id, self.geom, law_type, params_json, self.name.text(), comment=self.notes.text())
        SimpleLinkDialog.save(self)
        self.close()
