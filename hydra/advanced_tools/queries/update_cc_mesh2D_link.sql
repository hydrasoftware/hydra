/* **************************************************** */
/* Update lateral_contraction_coef for mesh_2d_link     */
/* Warning : you must select a table from work schema   */
/* as a "bati" table and choose a geometry column       */
/* **************************************************** */

 with cc as(
    select link.id as id, link.border as border, 1-sum(st_length(st_intersection(link.border, bati.$work_table_geom_column)))/st_length(link.border) as lateral_contraction_coef
    from $model.mesh_2d_link as link, work.$work_table as bati
	where st_intersects(link.border, bati.$work_table_geom_column)
    group by link.id, link.border
    )
update $model.mesh_2d_link
set lateral_contraction_coef=cc.lateral_contraction_coef from cc as cc
where $model.mesh_2d_link.id=cc.id
  and cc.lateral_contraction_coef>0.02
;;

with cc as(
    select link.id as id, link.border as border, 1-sum(st_length(st_intersection(link.border, bati.$work_table_geom_column)))/st_length(link.border) as lateral_contraction_coef
    from $model.mesh_2d_link as link, work.$work_table as bati
	where st_intersects(link.border, bati.$work_table_geom_column)
    group by link.id, link.border
    )
update $model.mesh_2d_link
set lateral_contraction_coef=0.02 from cc as cc
where $model.mesh_2d_link.id=cc.id
  and cc.lateral_contraction_coef<=0.02
;;