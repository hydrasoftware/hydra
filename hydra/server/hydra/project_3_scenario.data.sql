/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*              Scenario management               */
/* ********************************************** */

create table project.scenario(
    id serial primary key,
    name varchar(24) not null default project.unique_name('SCN_'),
    comment varchar,
    comput_mode hydra_computation_mode not null default 'hydrology_and_hydraulics',
    dry_inflow integer references project.dry_inflow_scenario(id),
    rainfall integer references project._rainfall(id),
    dt_hydrol_mn interval default '00:05:00',
    soil_cr_alp real default 1 check(soil_cr_alp >= 0),
    soil_horner_sat real default 0 check(soil_horner_sat >= 0 and soil_horner_sat <=1),
    soil_holtan_sat real default 0 check(soil_holtan_sat >= 0 and soil_holtan_sat <=1),
    soil_scs_sat real default 0 check(soil_scs_sat >= 0 and soil_scs_sat <=1),
    soil_hydra_psat real default 0 check(soil_hydra_psat >= 0 and soil_hydra_psat <=1),
    soil_hydra_rsat real default 0 check(soil_hydra_rsat >= 0 and soil_hydra_rsat <=1),
    soil_gr4_psat real default 0 check(soil_gr4_psat >= 0 and soil_gr4_psat <=1),
    soil_gr4_rsat real default 0 check(soil_gr4_rsat >= 0 and soil_gr4_rsat <=1),
    option_dim_hydrol_network bool not null default false,
    date0 timestamp not null default '2000-01-01 00:00:00',
    tfin_hr interval default '12:00:00' check(extract(seconds from tfin_hr)=0),
    tini_hydrau_hr interval default '12:00:00' check(extract(seconds from tini_hydrau_hr)=0),
    dtmin_hydrau_hr interval default '00:00:15',
    dtmax_hydrau_hr interval default '00:05:00',
    dzmin_hydrau real not null default 0.05,
    dzmax_hydrau real not null default 0.1,
    flag_save bool not null default 'f',
    tsave_hr interval default '00:00:00' check(extract(seconds from tsave_hr)=0),
    flag_rstart bool not null default 'f',
    flag_gfx_control bool not null default 'f',
    scenario_rstart integer references project.scenario(id),
    flag_hydrology_rstart bool not null default 'f',
    scenario_hydrology_rstart integer references project.scenario(id),
    flag_hydrology_auto_dimensioning bool not null default 'f',
    flag_wind_option bool not null default 'f',
    air_density real not null default 1.2 check(air_density >0),
    skin_friction_coef real not null default 0.008 check(skin_friction_coef > 0),
    wind_data_file varchar(256),
    trstart_hr interval default '00:00:00' check(extract(seconds from trstart_hr)=0),
    graphic_control bool not null default 'f',
    model_connect_settings hydra_model_connection_settings not null default 'cascade',
    tbegin_output_hr interval default '00:00:00' check(extract(seconds from tbegin_output_hr)=0),
    tend_output_hr interval default '12:00:00' check(extract(seconds from tend_output_hr)=0),
    dt_output_hr interval default '00:05:00' check(extract(seconds from dt_output_hr)=0),
    c_affin_param bool not null default 'f',
    domain_2d_param bool not null default 'f',
    t_affin_start_hr interval default '00:00:00' check(extract(seconds from t_affin_start_hr)=0),
    t_affin_min_surf_ddomains real not null default 0,
    strickler_param bool not null default 'f',
    option_file bool not null default 'f',
    option_file_path varchar(256),
    output_option bool not null default 'f',
    sor1_mode hydra_sor1_mode not null default 'two_files_extended',
    dt_sor1_hr interval default '00:05:00' check(extract(seconds from dt_sor1_hr)=0),
    transport_type hydra_transport_type not null default 'no_transport',
    iflag_mes bool not null default 'f',
    iflag_dbo5 bool not null default 'f',
    iflag_dco bool not null default 'f',
    iflag_ntk bool not null default 'f',
    quality_type hydra_quality_type not null default 'physico_chemical',
    diffusion bool not null default 'f',
    dispersion_coef real not null default 20,
    longit_diffusion_coef real not null default 41,
    lateral_diff_coef real not null default .45,
    water_temperature_c real not null default 20,
    min_o2_mgl real not null default 1.5,
    kdbo5_j_minus_1 real not null default 0.2,
    koxygen_j_minus_1 real not null default 0.45,
    knr_j_minus_1 real not null default 0.09,
    kn_j_minus_1 real not null default 0.035,
    bdf_mgl real not null default 1.5,
    o2sat_mgl real not null default 9.07,
    iflag_ecoli bool not null default 'f',
    iflag_enti bool not null default 'f',
    iflag_ad1 bool not null default 'f',
    iflag_ad2 bool not null default 'f',
    ecoli_decay_rate_jminus_one real not null default 0,
    enti_decay_rate_jminus_one real not null default 0,
    ad1_decay_rate_jminus_one real not null default 0,
    ad2_decay_rate_jminus_one real not null default 0,
    sst_mgl real not null default 0,
    sediment_file varchar(256) not null default '',
    suspended_sediment_fall_velocity_mhr real not null default 0,
    scenario_ref integer references project.scenario(id),
    scenario_hydrol integer references project.scenario(id),
    kernel_version varchar default null,
    has_aeraulics boolean not null default false,
    speed_of_sound_in_air real not null default 340,
    absolute_ambiant_air_pressure real null default 1.e5,
    average_altitude_msl real not null default 0,
    air_water_skin_friction_coefficient real not null default .01,
    air_pipe_friction_coefficient real not null default .01,
    air_temperature_in_network_celcius real not null default 15,
    ambiant_air_temperature_celcius real not null default 15,
    leak_pressure real not null default 20000
)
;;

create table project.metadata(
    current_scenario integer references project.scenario(id) on delete set null,
    id integer unique not null default 1 check (id=1) -- Only one row
)
;;

insert into project.metadata values (null, 1)
;;

create table project.mixed(
    ord integer not null,
    scenario integer not null references project.scenario(id) on delete cascade,
    grp integer not null references project.grp(id),
        unique(scenario, grp),
        unique(scenario, ord)
)
;;

create table project.config_scenario(
    id serial primary key,
    scenario integer not null references project.scenario(id) on delete cascade,
    model varchar(24) not null,
    configuration integer not null,
    priority integer not null default currval('project.config_scenario_id_seq')
)
;;

create table project.param_hydrograph(
    id serial primary key,
    param_scenario integer not null references project.scenario(id) on delete cascade,
    model varchar(24) not null,
    hydrograph_from varchar(24) not null,
    hydrograph_to varchar(256) not null
)
;;

create table project.param_external_hydrograph(
    id serial primary key,
    param_scenario integer not null references project.scenario(id) on delete cascade,
    external_file varchar(256) not null,
    rank_ integer not null default 1
)
;;

create table project.param_regulation(
    id serial primary key,
    param_scenario integer not null references project.scenario(id) on delete cascade,
    control_file varchar(256) not null,
    rank_ integer not null default 1
)
;;

create table project.param_measure(
    id serial primary key,
    param_scenario integer not null references project.scenario(id) on delete cascade,
    measure_file varchar(256) not null,
    rank_ integer not null default 1
)
;;

create table project.inflow_rerouting(
    id serial primary key,
    scenario integer not null references project.scenario(id) on delete cascade,
    model_from varchar(24) not null,
    hydrograph_from integer not null,
    model_to varchar(24),
    hydrograph_to integer,
    option hydra_rerouting_option not null default 'fraction',
    parameter real not null default 0,
        unique(scenario, model_from, hydrograph_from, model_to, hydrograph_to)
)
;;

create table project.c_affin_param(
    id serial primary key,
    scenario integer not null references project.scenario(id) on delete cascade,
    model varchar(24) not null,
    type_domain varchar(24) not null check(type_domain in ('reach', 'domain_2d', 'branch')),
    element varchar(24) not null,
        unique(scenario, model, type_domain, element),
    affin_option hydra_affin_option not null default 'affin'
)
;;

create table project.domain_2d_param(
    id serial primary key,
    scenario integer not null references project.scenario(id) on delete cascade,
    model varchar(24) not null,
    domain_2d integer not null,
        unique(scenario, model, domain_2d)
)
;;

create table project.strickler_param(
    id serial primary key ,
    scenario integer not null references project.scenario(id) on delete cascade,
    model varchar(24) not null,
    type_domain varchar(24) not null check(type_domain in ('reach', 'domain_2d', 'branch')),
    element varchar(24) not null,
    rkmin real not null default 0,
    rkmaj real not null default 0,
    pk1 real not null default 0,
    pk2 real not null default 0,
    unique(scenario, model, type_domain, element)
)
;;

create table project.output_option(
    id serial primary key ,
    scenario integer not null references project.scenario(id) on delete cascade,
    model varchar(24) not null,
    type_element varchar(48) not null,
    element varchar(24) not null
)
;;

/* ********************************************** */
/*              Grouping                          */
/* ********************************************** */

create table project.scenario_group(
    id serial primary key,
    name varchar(24) unique not null default 'GROUP_'||currval('project.scenario_group_id_seq')::varchar
)
;;

create table project.param_scenario_group(
    scenario integer not null references project.scenario(id) on delete cascade,
    grp integer not null references project.scenario_group(id) on delete cascade,
        unique(scenario, grp)
)
;;

/* ********************************************** */
/*              External files                    */
/* ********************************************** */

create table project.bc_tz_data(
    id serial primary key,
    filename varchar(256) not null
)
;;

/* ********************************************** */
/*              Chronological series              */
/* ********************************************** */

create function project.get_tmax_serie (id_serie integer)
returns interval
language plpgsql
as
$$$$
DECLARE tmax interval;
BEGIN
    select serie.tfin
    from project.serie
    where id = id_serie
    into tmax;
    return tmax;
END;
$$$$
;;

create function project.get_tmax_bloc_serie (id_serie integer)
returns interval
language plpgsql
as
$$$$
DECLARE tmin interval;
BEGIN
    select max(serie_bloc.t_till_start)
    from project.serie_bloc, project.serie
    where serie.id = serie_bloc.id_cs and serie.id = id_serie
    into tmin;
    return tmin;
END;
$$$$
;;

create table project.serie(
    id serial primary key,
    name varchar(24) not null default project.unique_name('CS'),
    comment varchar,
    comput_mode hydra_computation_mode not null default 'hydrology_and_hydraulics',
    dt_hydrol_mn interval default '00:05:00',
    dt_days int default 4 check(dt_days>0 and dt_days<5),
    date0 timestamp not null default '2000-01-01 00:00:00',
    tfin interval default '365 day' check(extract(seconds from tfin)=0 and tfin>project.get_tmax_bloc_serie(id)),
    tini_hydrau_hr interval default '12:00:00' check(extract(seconds from tini_hydrau_hr)=0),
    dtmin_hydrau_hr interval default '00:00:15',
    dtmax_hydrau_hr interval default '00:05:00',
    dzmin_hydrau real not null default 0.05,
    dzmax_hydrau real not null default 0.1,
    model_connect_settings hydra_model_connection_settings not null default 'cascade',
    dt_output_hr interval default '00:05:00' check(extract(seconds from dt_output_hr)=0),
    c_affin_param bool not null default 'f',
    domain_2d_param bool not null default 'f',
    t_affin_start_hr interval default '00:00:00' check(extract(seconds from t_affin_start_hr)=0),
    t_affin_min_surf_ddomains real not null default 0,
    strickler_param bool not null default 'f',
    option_file bool not null default 'f',
    option_file_path varchar(256),
    output_option bool not null default 'f',
    sor1_mode hydra_sor1_mode not null default 'two_files_extended',
    dt_sor1_hr interval default '01:00:00' check(extract(seconds from dt_sor1_hr)=0),
    transport_type hydra_transport_type not null default 'no_transport',
    iflag_mes bool not null default 'f',
    iflag_dbo5 bool not null default 'f',
    iflag_dco bool not null default 'f',
    iflag_ntk bool not null default 'f',
    quality_type hydra_quality_type not null default 'physico_chemical',
    diffusion bool not null default 'f',
    dispersion_coef real not null default 20,
    longit_diffusion_coef real not null default 41,
    lateral_diff_coef real not null default .45,
    water_temperature_c real not null default 20,
    min_o2_mgl real not null default 1.5,
    kdbo5_j_minus_1 real not null default 0.2,
    koxygen_j_minus_1 real not null default 0.45,
    knr_j_minus_1 real not null default 0.09,
    kn_j_minus_1 real not null default 0.035,
    bdf_mgl real not null default 1.5,
    o2sat_mgl real not null default 9.07,
    iflag_ecoli bool not null default 'f',
    iflag_enti bool not null default 'f',
    iflag_ad1 bool not null default 'f',
    iflag_ad2 bool not null default 'f',
    ecoli_decay_rate_jminus_one real not null default 0,
    enti_decay_rate_jminus_one real not null default 0,
    ad1_decay_rate_jminus_one real not null default 0,
    ad2_decay_rate_jminus_one real not null default 0,
    sediment_file varchar(256) not null default '',
    suspended_sediment_fall_velocity_mhr real not null default 0,
    scenario_ref integer default 0,
    kernel_version varchar default null
)
;;

create table project.serie_bloc(
    id serial primary key,
    id_cs integer not null references project.serie(id) on delete cascade,
    name varchar(24) not null default project.unique_name('CB'),
    t_till_start interval check(date_trunc('day', t_till_start) = t_till_start and t_till_start<project.get_tmax_serie(id_cs)),
        unique(id_cs, t_till_start),
    dry_inflow integer references project.dry_inflow_scenario(id),
    rainfall integer references project._rainfall(id),
    soil_cr_alp real default 1 check(soil_cr_alp >= 0),
    soil_horner_sat real default 0 check(soil_horner_sat >= 0 and soil_horner_sat <=1),
    soil_holtan_sat real default 0 check(soil_holtan_sat >= 0 and soil_holtan_sat <=1),
    soil_scs_sat real default 0 check(soil_scs_sat >= 0 and soil_scs_sat <=1),
    soil_hydra_psat real default 0 check(soil_hydra_psat >= 0 and soil_hydra_psat <=1),
    soil_hydra_rsat real default 0 check(soil_hydra_rsat >= 0 and soil_hydra_rsat <=1),
    soil_gr4_psat real default 0 check(soil_gr4_psat >= 0 and soil_gr4_psat <=1),
    soil_gr4_rsat real default 0 check(soil_gr4_rsat >= 0 and soil_gr4_rsat <=1),
    option_dim_hydrol_network bool not null default false,
    flag_gfx_control bool not null default 'f',
    flag_hydrology_rstart bool not null default 'f',
    scenario_hydrology_rstart integer references project.scenario(id),
    trstart_hr interval default '00:00:00' check(extract(seconds from trstart_hr)=0),
    sst_mgl real not null default 0,
    scenario_hydrol integer
)
;;

create table project.mixed_serie(
    ord integer not null,
    serie integer not null references project.serie(id) on delete cascade,
    grp integer not null references project.grp(id),
        unique(serie, grp),
        unique(serie, ord)
)
;;

create table project.config_serie(
    id serial primary key,
    serie integer not null references project.serie(id) on delete cascade,
    model varchar(24) not null,
    configuration integer not null,
    priority integer not null default currval('project.config_serie_id_seq')
)
;;

create table project.param_external_hydrograph_bloc(
    id serial primary key,
    param_bloc integer not null references project.serie_bloc(id) on delete cascade,
    external_file varchar(256) not null
)
;;

create table project.param_regulation_bloc(
    id serial primary key,
    param_bloc integer not null references project.serie_bloc(id) on delete cascade,
    control_file varchar(256) not null
)
;;

create table project.param_measure_bloc(
    id serial primary key,
    param_bloc integer not null references project.serie_bloc(id) on delete cascade,
    measure_file varchar(256) not null
)
;;

create table project.inflow_rerouting_bloc(
    id serial primary key,
    bloc integer not null references project.serie_bloc(id) on delete cascade,
    model_from varchar(24) not null,
    hydrograph_from integer not null,
    model_to varchar(24),
    hydrograph_to integer,
    option hydra_rerouting_option not null default 'fraction',
    parameter real not null default 0,
        unique(bloc, model_from, hydrograph_from, model_to, hydrograph_to)
)
;;

create table project.c_affin_param_serie(
    id serial primary key,
    serie integer not null references project.serie(id) on delete cascade,
    model varchar(24) not null,
    type_domain varchar(24) not null check(type_domain in ('reach', 'domain_2d', 'branch')),
    element varchar(24) not null,
        unique(serie, model, type_domain, element),
    affin_option hydra_affin_option not null default 'affin'
)
;;

create table project.domain_2d_param_serie(
    id serial primary key,
    serie integer not null references project.serie(id) on delete cascade,
    model varchar(24) not null,
    domain_2d integer not null,
        unique(serie, model, domain_2d)
)
;;

create table project.strickler_param_serie(
    id serial primary key ,
    serie integer not null references project.serie(id) on delete cascade,
    model varchar(24) not null,
    type_domain varchar(24) not null check(type_domain in ('reach', 'domain_2d', 'branch')),
    element varchar(24) not null,
    rkmin real not null default 0,
    rkmaj real not null default 0,
    pk1 real not null default 0,
    pk2 real not null default 0,
        unique(serie, model, type_domain, element)
)
;;

create table project.output_option_serie(
    id serial primary key ,
    serie integer not null references project.serie(id) on delete cascade,
    model varchar(24) not null,
    type_element varchar(48) not null,
    element varchar(24) not null
)
;;
