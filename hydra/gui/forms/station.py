# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..base_dialog import BaseDialog, tr
from qgis.PyQt import uic
import os

class StationEditor(BaseDialog):
    def __init__(self, project, contour, id=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "station.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.__model = self.project.get_current_model()
        assert not self.__model == None
        self.__contour = contour
        if id==None:
            self.__id = self.__model.add_station(self.__contour, name=None)
        else:
            self.__id = id
            if self.__contour is None:
                contour,= self.project.execute("""
                select geom
                from {}.station
                where id={}""".format(self.__model.name, self.__id)).fetchone()

        name, comment = self.project.execute("""
            select name, comment
            from {}.station
            where id={}""".format(self.__model.name, self.__id)).fetchone()
        self.name.setText(name)
        self.notes.setText(comment)

    def get_id(self):
        return self.__id

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.__model.update_station(self.__id, self.__contour, self.name.text(), comment=self.notes.text())
        BaseDialog.save(self)
        self.close()
