################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
run test to check if shp import in database

USAGE

   python -m hydra.gui.work_manager_guitest [-dhs] [-g project_name]

OPTIONS

   -h, --help
        print this help

   -g  --gui project_name shapefile_name
        run in gui mode

   -g  --gui project_name
        run in gui mode

   -s  --solo
        run test in solo mode (create database)

"""


import inspect
import sys
import os
import getopt
from subprocess import Popen, PIPE, DEVNULL

from qgis.PyQt.QtWidgets import QApplication
from qgis.PyQt.QtCore import QCoreApplication, QTranslator, Qt
from hydra.project import Project
from hydra.database.database import TestProject, remove_project, project_exists
from hydra.gui.work_manager import WorkManager
from hydra.utility.log import LogManager, ConsoleLogger
from hydra.utility.settings_properties import SettingsProperties
from hydra.utility.string import normalized_name, normalized_model_name

def gui_mode(project_name):
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)
    project = Project.load_project(project_name)
    test_dialog = WorkManager(project)
    test_dialog.exec_()


def execute(argslist, install_env):
    print(' '.join(argslist))
    process = Popen(argslist, stdin=PIPE, stdout=PIPE, stderr=PIPE, env=install_env)
    output, error = process.communicate()
    if process.returncode != 0:
        raise RuntimeError(error.decode('utf8'))
    return

def test(debug, keep):

    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.street import StreetEditor
    from hydra.utility.string import normalized_name, normalized_model_name
    import sys

    log_manager = LogManager(ConsoleLogger(), "Hydra")

    current_dir = os.path.abspath(os.path.dirname(__file__))
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "work_manager_test"
    model_name = "model"

    if project_exists(project_name):
        remove_project(project_name)

    test_project = TestProject(project_name, keep, debug, with_model=True)


    project = Project.load_project(project_name)

    for filename, layer in [
            ("manholetest.shp", "worktestmanhole"),
            ("pipetest.shp", "worktestpipe"),
            ("pointxyztest.shp", "workpointerrain"),
            ("point_topo2d_test.shp", "work_point_terrain_2"),
            ("centroid_sous_bv.shp", "centroid_sous_bv"),
            ("sous_bv_vf.shp", "sous_bv"),
            ("hydro_constant.shp", "hydro_constant"),
            ("test_street.shp", "test_street"),
            ("constrain_extern.shp", "constrain_extern"),
            ("constrain_demo.shp", "constrain_demo"),
            ("reach_test.shp", "reach_test"),
            ("mos_2017_extract.shp", "mos_2017_test"),
            ("station_polygon.shp", "station_polygon"),
            ("station_node.shp", "station_node"),
            ("constrain_marker.shp", "constrain_marker"),
            ("null_marker_points.shp", "null_marker_points"),
            ("null_marker_polygon.shp", "null_marker_polygon"),
            ("hydrology_connectors_test.shp", "hydrology_connectors_test"),
            ("catchment_test.shp", "catchment_test"),
            ("manhole_hydrology_test.shp", "manhole_hydrology_test"),
            ("routing_test.shp", "routing_test")
            ]:
        filepath = os.path.join(current_dir, 'test_data', filename)
        env = dict(os.environ)
        env['PGCLIENTENCODING'] = 'latin1'
        cmd = ['ogr2ogr', f'PG:service=hydra dbname={project.name}', '-lco', 'GEOMETRY_NAME=geom', '-a_srs', f'EPSG:{project.srid}', '-nln', 'work.'+layer, filepath] 
        print(' '.join(cmd))
        p = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, env=env)
        output, error = p.communicate()
        if error:
            raise RuntimeError(error.decode('utf8'))
    print("imported")


    test_dialog = WorkManager(project, source='worktestmanhole', dest='model.manhole')
    test_dialog.import_from_work()
    test_dialog.save()

    test_dialog = WorkManager(project, source='worktestpipe', dest='model.pipe')
    test_dialog.set_default_value('Lenght', '42')
    test_dialog.import_from_work()
    test_dialog.save()

    test_dialog = WorkManager(project, source='workpointerrain', dest='project.points_xyz')
    test_dialog.set_default_value('Source', 'Nocifer')
    test_dialog.import_from_work()
    test_dialog.save()

    test_dialog = WorkManager(project, source='work_point_terrain_2', dest='project.points_xyz')
    test_dialog.set_connection('Name', 'name_2')
    test_dialog.set_default_value('Comment', 'test_123')
    test_dialog.set_default_value('Source', 'Nassima')
    test_dialog.import_from_work()
    test_dialog.save()

    test_dialog = WorkManager(project, source='centroid_sous_bv', dest='model.catchment_node')
    test_dialog.set_default_value('Slope', '0.8')
    test_dialog.import_from_work()
    test_dialog.save()

    test_dialog = WorkManager(project, source='sous_bv', dest='model.catchment')
    test_dialog.import_from_work()
    test_dialog.save()

    test_dialog = WorkManager(project, source='hydro_constant', dest='model.constant_inflow_bc_singularity')
    test_dialog.set_default_value('Q0', '38')
    test_dialog.run_import_from_work(True, "river")
    test_dialog.save()

    test_dialog = WorkManager(project, source='test_street', dest='model.street')
    test_dialog.import_from_work()
    test_dialog.save()

    test_dialog = WorkManager(project, source='constrain_demo', dest='model.constrain')
    test_dialog.set_connection('Name', '')
    test_dialog.import_from_work()
    test_dialog.save()

    test_dialog = WorkManager(project, source='constrain_extern', dest='model.constrain')
    test_dialog.set_connection('Name', '')
    test_dialog.import_from_work()
    test_dialog.save()

    test_dialog = WorkManager(project, source='reach_test', dest='model.reach')
    test_dialog.import_from_work()
    test_dialog.save()

    #mos specific land_type creation test
    project.execute("""delete from project.land_type;""") # cleanup table
    project.execute("""insert into project.land_type (land_type, c_imp) values ('equipments',0.4);""")
    project.execute("""insert into project.land_type (land_type, c_imp) values ('test_type',0.85);""")

    test_dialog = WorkManager(project, source='mos_2017_test', dest='project.land_occupation')
    test_dialog.set_connection('Land type', 'mos')
    test_dialog.import_from_work()
    test_dialog.save()

    test_dialog = WorkManager(project, source='station_polygon', dest='model.station')
    test_dialog.import_from_work()
    test_dialog.save()

    test_dialog = WorkManager(project, source='station_node', dest='model.station_node')
    test_dialog.import_from_work()
    test_dialog.save()

    test_dialog = WorkManager(project, source='constrain_marker', dest='model.constrain')
    test_dialog.set_connection('Name', '')
    test_dialog.import_from_work()
    test_dialog.save()

    test_dialog = WorkManager(project, source='null_marker_points', dest='model.coverage_marker')
    test_dialog.import_from_work()
    test_dialog.save()

    project.execute("""delete from model.coverage_marker""")

    test_dialog = WorkManager(project, source='null_marker_polygon', dest='model.coverage_marker')
    test_dialog.import_from_work()
    test_dialog.save()

    test_dialog = WorkManager(project, source='manhole_hydrology_test', dest='model.manhole_hydrology')
    test_dialog.import_from_work()
    test_dialog.save()

    test_dialog = WorkManager(project, source='hydrology_connectors_test', dest='model.connector_hydrology')
    test_dialog.import_from_work()
    test_dialog.save()

    test_dialog = WorkManager(project, source='catchment_test', dest='model.catchment_node')
    test_dialog.import_from_work()
    test_dialog.save()

    test_dialog = WorkManager(project, source='routing_test', dest='model.routing_hydrology')
    test_dialog.import_from_work()
    test_dialog.save()

    manhole = project.execute("""select area,z_ground from model.manhole_node where name='NODA5018';""").fetchone()
    assert(float(manhole[0]) == 15)
    assert(float(manhole[1]) == 700)
    manhole2 = project.execute("""select area,z_ground from model.manhole_node where name='NODA5023';""").fetchone()
    assert(float(manhole2[0]) == 1.0)
    assert(float(manhole2[1]) == 9999)
    pipe = project.execute("""select comment, h_sable, rk, custom_length from model.pipe_link where z_invert_down=20 and z_invert_up=25""").fetchone()
    assert(pipe[0] == 'honor')
    assert(pipe[1] == 1.2)
    assert(pipe[2] == 70)
    assert(pipe[3] == 42)
    pointxyz = project.execute("""select name,comment,source from project.points_xyz where name= 'P_XYZ_5537';""").fetchone()
    assert(pointxyz[0] == 'P_XYZ_5537')
    assert(pointxyz[1] == 'vador')
    assert(pointxyz[2] == 'Nocifer')
    numberpipe = project.execute("""select COUNT(*) from model.pipe_link""").fetchone()[0]
    assert(numberpipe == 6)
    numberpointxyz = project.execute("""select COUNT(*) from project.points_xyz""").fetchone()[0]
    assert(numberpointxyz == 1051)
    numberpointxyz_2D = project.execute("""select COUNT(*) from project.points_xyz where source = 'Nassima' and comment = 'test_123'""").fetchone()[0]
    assert(numberpointxyz_2D == 1047)
    number_branch = project.execute("""select model.branch_update_fct()""").fetchone()[0]
    assert(number_branch)
    number_catchment = project.execute("""select count(*) from model.catchment""").fetchone()[0]
    assert(number_catchment == 89)
    number_catchment = project.execute("""select count(*) from model.catchment_node""").fetchone()[0]
    assert(number_catchment == 91)
    number_catchment = project.execute("""select max(slope) from model.catchment_node""").fetchone()[0]
    assert(number_catchment == 0.8)
    # check trigger status
    trigger = project.execute("""select trigger_branch from model.metadata""").fetchone()[0]
    assert(trigger)
    nb_constant_inflow = project.execute("""select count(*) from model.constant_inflow_bc_singularity""").fetchone()[0]
    assert(nb_constant_inflow == 96)
    valu_inflow = project.execute("""select * from model.constant_inflow_bc_singularity limit 1""").fetchone()[3]
    assert(valu_inflow == 38)
    nb_street = project.execute("""select count(*) from model.street""").fetchone()[0]
    assert(nb_street == 8)
    cov = project.execute("""select count(*) from model.coverage where domain_type = 'street' """).fetchone()[0]
    assert(cov == 1)
    number_constrain_rk = project.execute("""select count(*) from model.constrain where constrain_type = 'strickler' """).fetchone()[0]
    assert(number_constrain_rk == 1)
    value_const_strickler = project.execute("""select elem_length from model.constrain where constrain_type = 'strickler' """).fetchone()[0]
    assert(value_const_strickler == 75)
    number_reach = project.execute("""select count(*) from model.reach""").fetchone()[0]
    assert(number_reach == 10)
    len_reach = project.execute("""select CEIL(SUM(St_length(geom))) from model.reach""").fetchone()[0]
    assert(len_reach == 5392.0)
    nb_land_type = project.execute("""select count(*) from project.land_type""").fetchone()[0]
    assert(nb_land_type == 11)
    land_types = [land_type for (land_type,) in project.execute("""select land_type from project.land_type order by land_type""").fetchall()]
    assert(land_types == ['activities', 'collective_housing', 'equipments', 'fields', 'individual_housing',
    'open_artificial_spaces', 'pits_landfills_construction_sites', 'public_transports', 'test_type', 'water', 'woods_and_forests'])
    nb_water_occupation_type = project.execute("""select count(*) from project.land_occupation where land_type = 'water'""").fetchone()[0]
    assert(nb_water_occupation_type == 55)
    nb_stations = project.execute("""select count(*) from model.station""").fetchone()[0]
    assert(nb_stations == 61)
    nb_stations_node = project.execute("""select count(*) from model.station_node""").fetchone()[0]
    assert(nb_stations_node == 77)
    nb_coverage_null = project.execute(""" select count(*) from model.coverage_marker""").fetchone()[0]
    assert(nb_coverage_null == 5)
    nb_hydro_manhole = project.execute(""" select count(*) from model.manhole_hydrology_node""").fetchone()[0]
    assert(nb_hydro_manhole == 4)
    nb_routing = project.execute(""" select count(*) from model.routing_hydrology_link""").fetchone()[0]
    assert(nb_routing == 2)
    number_constrain = project.execute("""select count(*) from model.constrain """).fetchone()[0]
    assert(number_constrain == 157 or number_constrain==158)


try:
    optlist, args = getopt.getopt(sys.argv[1:],
                    "hdgk",
                    ["help", "debug", "gui", "keep"])
except Exception as e:
    sys.stderr.write(str(e) + "\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist
keep = "-k" in optlist or "--keep" in optlist

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    project_name = normalized_name(project_name)
    gui_mode(project_name)
    exit(0)

test(debug, keep)
# fix_print_with_import

# fix_print_with_import
print("ok")
