--isolated--
alter type hydra_dry_inflow_period add value 'auto'
;;

insert into hydra.dry_inflow_period(name, id)
select 'auto', 0
;;

update hydra.dry_inflow_period set id=1 where name='weekday'
;;

update hydra.dry_inflow_period set id=2 where name='weekend'
;;

