# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from shapely import wkt
from shapely.geometry import Polygon
from qgis.PyQt import uic, QtGui, QtCore
from qgis.PyQt.QtWidgets import QTableWidgetItem
from hydra.gui.base_dialog import BaseDialog, tr
import hydra.utility.string as string

class RadarGridManager(BaseDialog):
    def __init__(self, project, parent=None):
        '''init the GUI'''
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "radar_grid.ui"), self)

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.export_crb.clicked.connect(self.__export_crb_file)

        self.project = project

        x0, y0, dx, dy, nx, ny = self.project.execute("""
            select ST_X(origin_point), ST_Y(origin_point), dx, dy, nx, ny
            from project.radar_grid
            where id=1;
            """).fetchone()

        self.x0.setText(string.get_str(x0))
        self.y0.setText(string.get_str(y0))
        self.dx.setText(string.get_str(dx))
        self.dy.setText(string.get_str(dy))
        self.nx.setValue(nx)
        self.ny.setValue(ny)

        self.dx.textChanged.connect(self.__refresh_crb_btn)
        self.dy.textChanged.connect(self.__refresh_crb_btn)
        self.nx.valueChanged.connect(self.__refresh_crb_btn)
        self.ny.valueChanged.connect(self.__refresh_crb_btn)

        self.__refresh_crb_btn()

    def save(self):
        self.project.execute("""
            update project.radar_grid set
            (origin_point, dx, dy, nx, ny)=({})
            where id=1;
            """.format(', '.join([
                    "'srid={}; POINT({} {})'::geometry".format(
                        str(self.project.srid),
                        str(string.get_sql_float(self.x0.text())),
                        str(string.get_sql_float(self.y0.text()))
                        ),
                    str(string.get_sql_float(self.dx.text())),
                    str(string.get_sql_float(self.dy.text())),
                    str(string.get_sql_float(self.nx.value())),
                    str(string.get_sql_float(self.ny.value()))
                    ])
                )
            )

        BaseDialog.save(self)
        self.close()

    def __refresh_crb_btn(self):
        self.export_crb.setEnabled(all(string.get_sql_float(btn.text()) > 0 for btn in [self.dx, self.dy]) and
                                   all(string.get_sql_float(btn.value()) > 0 for btn in [self.nx, self.ny]))

    def __export_crb_file(self):
        '''writes a project_name.crb file in workspace/project/data
           contains 1 block for each catchment witrh the area of
           intersection of each pixel of the grid'''
        filename = os.path.join(self.project.data_dir, self.project.name+".crb")
        if not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))

        x0 = string.get_sql_float(self.x0.text())
        y0 = string.get_sql_float(self.y0.text())
        dx = string.get_sql_float(self.dx.text())
        dy = string.get_sql_float(self.dy.text())
        nx = string.get_sql_float(self.nx.value())
        ny = string.get_sql_float(self.ny.value())

        self.project.log.notice(tr("Starting CRB file generation"))

        with open(filename, 'w') as f:
            for model in self.project.get_models():
                progress = self.project.log.progress(tr("Generating CRB file: model {}".format(model)))
                pix = {}
                for i in range(nx):
                    for j in range(ny):
                        (x,y) = [x0 + i * dx, y0 - j * dy]
                        catchments = self.project.execute("""
                            with pixel as (select ST_SetSRID('{}'::geometry, {}) as geom)
                            select name, ST_Area(ST_Intersection(c.geom, p.geom)) / ST_Area(c.geom)
                            from {}.catchment as c, pixel as p
                            where ST_Intersects(c.geom, p.geom) and ST_Area(ST_Intersection(c.geom, p.geom)) / ST_Area(c.geom) > 0.001
                            order by UPPER(c.name) asc;""".format(Polygon([(x,y),(x+dx,y),(x+dx,y-dy),(x,y-dy)]), self.project.srid, model)).fetchall()
                        for name, surf in catchments:
                            if name not in list(pix.keys()):
                                pix[name] = {}
                            pix[name][i+1, j+1] = surf
                        progress.set_ratio(float(i*ny+j)/float(nx*ny))
                for name, fractions in pix.items():
                    f.write("%s %s %i\n" % (model, name, len(fractions)))
                    for pixel, frac in fractions.items():
                        f.write("%i %i %10.3f\n" % (pixel[1], pixel[0], frac)) # historically, line written as "j i %"
                    f.write("\n")
                progress.set_ratio(1)
                self.project.log.clear_progress()
                self.project.log.notice(tr("Model {} written in CRB file".format(model)))
        self.project.log.notice(tr("CRB file generated"))
