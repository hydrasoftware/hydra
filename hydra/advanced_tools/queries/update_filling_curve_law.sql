/* ********************************************************************************************* */
/* Will update the zs_array to get the filling_curve law of a coverage                           */
/*     + you will need a DEM to use this query                                                   */
/*     + TIP : will run on the whole model, but can edit query to use on selection of storage    */
/* ********************************************************************************************* */

-- Option (use a selection of storage)

update $model.storage_node
set zs_array = project.filling_curve(cov.geom)
from $model.coverage as cov
where cov.id = storage_node.contour
-- and storage_node.id in ({list})
;;

update $model.storage_node
set zini = zs_array[1][1]
-- where storage_node.id in ({list})
;;
