# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
QGIS plugin
"""

import os
import traceback
import re
import subprocess
import webbrowser
from datetime import datetime
from functools import partial
from qgis.core import QgsProject, QgsCoordinateReferenceSystem, QgsSettings, QgsApplication
from qgis.PyQt.QtCore import QTranslator, QCoreApplication, QObject, Qt, QSettings
from qgis.PyQt.QtWidgets import QMenu, QAction, QWidget, QFileDialog, QInputDialog, QLabel, QDockWidget, QMessageBox, QApplication, QVBoxLayout, QScrollArea
from qgis.PyQt.QtGui import QIcon, QPixmap
from .project import Project
from .database import database as dbhydra
from .gui import new_project_dialog as npdialog
from .gui import new_model_dialog as nmdialog
from .gui import project_manager_dialog as pjmanagerdialog
from .gui.settings import HydraSettings
from .gui.project_tree import ProjectTree
from .gui.section_export import SectionExporter
from .gui.time_control import TimeControl
from .gui.work_manager import WorkManager
from .utility.log import LogManager
from .utility.licence import check_licence, activate_licence, save_key, get_serial, get_build, build_file
from .utility.system import deploy_dependencies
from .utility.qgis_utilities import QGisUIManager, QGisLogger
from .utility.tables_properties import TablesProperties
from .utility.settings_properties import SettingsProperties
from .utility.string import isfloat, normalized_name
from .advanced_tools.pre_composed_queries import PreComposedQueriesManager
from .versions.dump_restore import srid_from_dump, version_from_dump, run_psql
from .processing.provider import HydraProvider
from .processing.taudem.taudemProvider import TauDemProvider
from .versions.update import update_project

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

_plugin_dir = os.path.dirname(__file__)
_hydra_dir = os.path.join(os.path.expanduser('~'), ".hydra")
_layer_icons_dir = os.path.join(_plugin_dir, "ressources", "layer_icons")
_log_file = os.path.join(_hydra_dir, 'hydra.log')
_desktop_dir = os.path.join(os.path.expanduser('~'), "Desktop")
_svg_dir = os.path.normpath(os.path.join(os.path.dirname(__file__), 'ressources', 'svg'))

class Hydra(QObject):
    '''QGIS Plugin Implementation.'''

    def __init__(self, iface):
        '''Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        '''
        QObject.__init__(self)
        self.__iface = iface

        if _svg_dir not in QSettings().value('svg/searchPathsForSVG', []):
            path = QSettings().value('svg/searchPathsForSVG', []) + [_svg_dir]
            QSettings().setValue('svg/searchPathsForSVG', path)

        # initialize locale
        locale = QgsSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            _plugin_dir,
            'i18n',
            '{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.__translator = QTranslator()
            self.__translator.load(locale_path)
            QCoreApplication.installTranslator(self.__translator)

        self.__project_menu = None
        self.__model_menu = None
        self.__hydro_menu = None
        self.__scenario_menu = None

        self.__toolbar = None
        self.__project_tree = None
        self.__model_dock = None
        self.__long_profil_dock = None
        self.__longi_profile_interface = None
        self.__time_control_profil_dock = None

        self.__currentproject = None

        self.__show_w15 = False

        self.__notebook_process = None

        self.__ui_manager = QGisUIManager(self.__iface)
        self.__log_manager = LogManager(QGisLogger(self.__iface), tr("Hydra"))

        # check settings file
        SettingsProperties.check_settings_file()

        if os.name == 'nt':
            deploy_dependencies(_plugin_dir, self.__log_manager)

        QgsProject.instance().readProject.connect(self.project_loaded)
        #self.__iface.projectRead.connect(self.project_loaded)
        self.__iface.newProjectCreated.connect(self.project_loaded)

        self.processing_instance = HydraProvider()
        self.processing_taudem = TauDemProvider()

        # create notepad++ syntax file (on expresseau's basis, ne fonctionne que pour Windows)
        if os.name == "nt":
            notepad_user_defines_lang = os.path.join(os.path.expanduser('~'), 'AppData', 'Roaming', 'Notepad++', 'userDefineLangs')
            notepad_format = [
                (os.path.join(_plugin_dir, 'hydra_ctl.xml'), os.path.join(notepad_user_defines_lang, 'hydra_ctl.xml')),
                (os.path.join(_plugin_dir, 'hydra_extract.xml'), os.path.join(notepad_user_defines_lang, 'hydra_extract.xml'))
            ]
            if os.path.exists(notepad_user_defines_lang):
                for source, destination in notepad_format:
                    os.system('copy src destination')

    @property
    def project(self):
        """return current project instance"""
        return self.__currentproject

    def initGui(self):
        '''create the menu entries and toolbar icons inside the QGIS GUI'''
        self.__iface.mainWindow().menuBar().addMenu(self.project_menu())
        self.__currentproject = None

        self.project_loaded() # initialize hydra on plugin reload

        # register processing algorithms only for hydra projects
        if not QgsApplication.processingRegistry().providerById('taudem'):
            QgsApplication.processingRegistry().addProvider(self.processing_taudem)
        if not QgsApplication.processingRegistry().providerById('hydra'):
            QgsApplication.processingRegistry().addProvider(self.processing_instance)

    def activate_licence(self):
        licence, ok = QInputDialog.getText(self.__iface.mainWindow(), tr('Hydra licence activation'), tr('Enter your licence number:'))
        if ok:
            try:
                key = activate_licence(licence)
                save_key(key)
                self.__log_manager.notice(tr("Licence activated"))
                QMessageBox.information(self.__iface.mainWindow(), tr('Activation successfull'),
                        tr('Your licence has successfully been activated!'), QMessageBox.Ok)
            except Exception as e:
                self.__log_manager.error(str(e))
                QMessageBox.critical(self.__iface.mainWindow(), tr('Error'),
                        tr('There has been an error during your licence activation. Please contact technical support.'), QMessageBox.Ok)
        if not check_licence():
            self.__log_manager.warning(tr("No licence activated"))

    def project_loaded(self, dom=None):
        project_path = QgsProject.instance().readPath("./")
        project_name = project_path.split("/")[-1]
        self.__currentproject = None
        if dbhydra.is_hydra_db(project_name):
            self.__currentproject = Project(project_name, self.__log_manager, self.__ui_manager)
            if dbhydra.data_version() != self.__currentproject.version:
                confirm = QMessageBox(QMessageBox.Warning,
                    tr('Update project'), tr('Do you want to update project {} from version {} to version {} ?'
                    ).format(self.__selected_project, self.__currentproject.version, self.ui_version), QMessageBox.Ok | QMessageBox.Cancel).exec_()
                if confirm == QMessageBox.Ok:
                    update_project(self.__currentproject)
                else:
                    self.__currentproject = None

        if self.__currentproject:
            self.open_project_in_ui(True)
            if not check_licence():
                self.activate_licence()
        else:
            self.__remove_dependant_menus()

    def __create_dependant_menus(self):
        for action in self.__project_menu.actions():
            if action.text() == tr("&Terrain"):
                for menu in [self.model_menu(), self.hydro_menu(), self.scenario_menu()]:
                    self.__project_menu.insertMenu(action, menu)

        tb = self.toolbar()
        self.__iface.addToolBar(tb)
        tb.show()

        self.__iface.mainWindow().addDockWidget(Qt.LeftDockWidgetArea, self.project_tree())
        self.__iface.mainWindow().tabifyDockWidget(self.__iface.mainWindow().findChildren(QDockWidget, "Layers")[0], self.project_tree())

        self.__iface.mainWindow().addDockWidget(Qt.LeftDockWidgetArea, self.model_dock())
        self.__iface.mainWindow().tabifyDockWidget(self.__iface.mainWindow().findChildren(QDockWidget, "Layers")[0], self.model_dock())

        self.__iface.mainWindow().addDockWidget(Qt.LeftDockWidgetArea, self.long_profil_dock())
        self.__iface.mainWindow().tabifyDockWidget(self.__iface.mainWindow().findChildren(QDockWidget, "Layers")[0], self.long_profil_dock())

    def project_menu(self):
        '''returns the project menu, creates it if needed'''
        if not self.__project_menu:
            self.__project_menu = QMenu(tr("&Hydra"))
            self.__project_menu.setIcon(QIcon(os.path.join(_plugin_dir, 'ressources', 'images', 'hydra_logo.png')))
            self.__project_menu.addAction(tr("&New project")).triggered.connect(self.__project_new)
            self.__project_menu.addAction(tr("Manage &projects")).triggered.connect(self.__project_manage)
            self.__project_menu.addSeparator()
            self.__project_menu.addAction(tr("&Terrain")).triggered.connect(self.__terrain_manager)
            self.__project_menu.addAction(tr("&Configurations")).triggered.connect(self.__config_manager)

            self.__advanced_tools_menu = self.__project_menu.addMenu(tr("&Advanced tools"))
            self.__advanced_tools_menu.addAction(tr("&Extract")).triggered.connect(self.__extract)
            self.__advanced_tools_menu.addAction(tr("&Anim-Eau")).triggered.connect(self.__animeau)
            self.__advanced_tools_menu.addAction(tr("&Crgeng")).triggered.connect(self.__crgeng)
            self.__advanced_tools_menu.addSeparator()
            self.__advanced_tools_menu.addAction(tr("Manage external &tables")).triggered.connect(self.__work_manager)
            self.__advanced_tools_menu.addAction(tr("&Pre-composed SQL queries")).triggered.connect(self.__pre_composed_queries)
            self.__advanced_tools_menu.addAction(tr("Create &river profile album")).triggered.connect(self.__section_exporter)
            self.__advanced_tools_menu.addAction(tr("Export as CSV &files")).triggered.connect(self.__export_csv)
            self.__advanced_tools_menu.addAction(tr("Launch notebook")).triggered.connect(self.__launch_notebook)
            self.__advanced_tools_menu.addAction(tr("Export Live")).triggered.connect(self.__export_live)
            self.__advanced_tools_menu.addAction(tr("Import Live")).triggered.connect(self.__import_live)

            self.__project_menu.addAction(tr("&Reload project layers")).triggered.connect(self.__project_layers_reload)
            self.__project_menu.addSeparator()

            self.__documentation_menu = self.__project_menu.addMenu(tr("&Documentation"))
            self.__documentation_menu.addAction(tr("&Users manuel (online)")).triggered.connect(self.__doc)
            self.__documentation_menu.addAction(tr("&Analysis: hydraulics")).triggered.connect(self.__doc_hydraulics)
            self.__documentation_menu.addAction(tr("Analysis: &hydrology")).triggered.connect(self.__doc_hydrology)
            self.__documentation_menu.addAction(tr("Analysis: &quality")).triggered.connect(self.__doc_quality)
            self.__documentation_menu.addAction(tr("Analysis: &sedimentation")).triggered.connect(self.__doc_sedimentation)

            self.__project_menu.addAction(tr("Change&log")).triggered.connect(self.__changelog)
            self.__project_menu.addAction(tr("Settings")).triggered.connect(self.__settings)
            self.__project_menu.addAction(tr("Licence activation")).triggered.connect(self.__licence_activation)
            self.__project_menu.addAction(tr("About Hydra")).triggered.connect(self.__about)

            #Technical notes
            self.__documentation_menu.addSeparator()
            titles = {'NT22.pdf': 'Control and regulation file',
                      'NT24.pdf': 'External data file',
                      'NT25.pdf': 'Computation option and output file',
                      'NT27.pdf': 'Sedimentation input file',
                      'NT31.pdf': 'Extract control file',
                      'NT32.pdf': 'Animeau',
                      'NT51.pdf': 'Crgeng',
                      'NT53.pdf': 'External rain data file format',
                      'NT56.pdf': '*HYDRA_BV command',
                      'NT59.pdf': '*Correct_hydrau command',
                      'NT60.pdf': 'Wind file format'}
            for note, title in titles.items():
                self.__documentation_menu.addAction('{}: {}'.format(note, title)).triggered.connect(partial(self.__doc_nt, note))

        return self.__project_menu

    def model_menu(self):
        '''returns the model menu, creates it if needed'''
        if not self.__model_menu:
            self.__model_menu = QMenu(tr("&Models"))
            self.__models_menu = self.__model_menu.addMenu(tr("&Set current"))
            models = self.__currentproject.get_models()
            for model in models:
                action = self.__models_menu.addAction(model)
                action.triggered.connect(self.__set_current_model)
                if self.__currentproject.get_current_model() is None \
                        or self.__currentproject.get_current_model().name == model:
                    self.__currentproject.set_current_model(model)
                    ft = action.font()
                    ft.setBold(True)
                    action.setFont(ft)
            self.__model_menu.addAction(tr("&Add model")).triggered.connect(self.__model_new)
            self.__model_menu.addAction(tr("&Delete current model")).triggered.connect(self.__model_delete)
            self.__model_menu.addAction(tr("&Import model (sql)")).triggered.connect(self.__import_model_sql)
            self.__model_menu.addAction(tr("&Export model (sql)")).triggered.connect(self.__export_model_sql)
            self.__model_menu.addAction(tr("Import model (&csv)")).triggered.connect(self.__import_model_csv)
            self.__model_menu.addAction(tr("&Reload current model")).triggered.connect(self.__model_layers_reload)
        return self.__model_menu

    def hydro_menu(self):
        '''returns the hydrology menu, creates it if needed'''
        if not self.__hydro_menu:
            self.__hydro_menu = QMenu(tr("&Hydrology"))
            self.__hydro_menu.addAction(tr("&Dryflow sector")).triggered.connect(self.__dry_inflow_sectors_settings)
            self.__hydro_menu.addAction(tr("Dryflow &scenario")).triggered.connect(self.__dry_inflow_manager)
            self.__hydro_menu.addAction(tr("&Rain scenario")).triggered.connect(self.__rain_scenario)
            self.__hydro_menu.addAction(tr("Radar &grid")).triggered.connect(self.__radar_grid)
            self.__hydro_menu.addAction(tr("&Pollution land accumulation")).triggered.connect(self.__pollution_land_accumulation)
            self.__hydro_menu.addAction(tr("&Land occupation")).triggered.connect(self.__land_types)
        return self.__hydro_menu

    def scenario_menu(self):
        '''returns the scenario menu, creates it if needed'''
        if not self.__scenario_menu:
            self.__scenario_menu = QMenu(tr("&Scenarios"))
            self.__scenario_menu.addAction(tr("&Settings")).triggered.connect(self.__scenario_manage)
            self.__scenario_menu.addAction(tr("&Grouping")).triggered.connect(self.__scenario_grouping)
            self.__scenario_menu.addAction(tr("&Time series")).triggered.connect(self.__serie_manage)
            self.__scenario_menu.addAction(tr("&Import scenario (MAGES)")).triggered.connect(self.__import_scenario_mages)
            # self.__scenario_menu.addAction(tr("&Import data (csv)")).triggered.connect(self.__import_project_csv)
        return self.__scenario_menu

    def project_tree(self):
        '''returns the project_tree, creates it if needed'''
        if not self.__project_tree:
            self.__project_tree = ProjectTree(tr("Project items"), self.__currentproject, self.__refresh_ui_menus)
        return self.__project_tree

    def model_dock(self):
        '''returns the modelisation dock, creates it if needed'''
        from .gui.model_dock import ModelDock
        if not self.__model_dock:
            self.__model_dock = ModelDock(self.__currentproject, self.__iface)
            self.__model_dock.show()
            self.__model_dock.layers_changed_signal.connect(self.__refresh_layers)
        return self.__model_dock

    def long_profil_dock(self):
        '''returns the long profil dock, creates it if needed'''
        from .gui.long_profil_dock import LongProfilDock
        if not self.__time_control_profil_dock:
            self.__time_control_profil_dock=TimeControl(blocker=None)
        if not self.__long_profil_dock:
            self.__long_profil_dock = LongProfilDock(tr("Longitudinal profile"), self.__currentproject, self.__iface,self.__time_control_profil_dock)
        return self.__long_profil_dock

    def toolbar(self):
        '''returns the toolbar, creates it if needed'''
        from .gui.toolbar import ToolBar
        if not self.__toolbar:
            self.__toolbar = ToolBar(tr("Hydra toolbar"), self.__currentproject, self.__iface)
            self.__toolbar.mod_changed_signal.connect(self.__model_changed_fct)
            self.__toolbar.scn_changed_signal.connect(self.__scn_changed_fct)
            self.__toolbar.layers_changed_signal.connect(self.__refresh_layers)
        return self.__toolbar

    def __refresh_ui_menus(self):
        if self.__currentproject is not None:
            if not self.__model_menu:
                self.__create_dependant_menus()
            self.__refresh_models_menu()
            if self.__project_tree:
                self.__project_tree.refresh_content(self.__currentproject)
            if self.__toolbar:
                self.__toolbar.refresh_content(self.__currentproject)
            if self.__model_dock:
                self.__model_dock.refresh()

    def __refresh_models_menu(self):
        self.__models_menu.clear()
        if self.__currentproject is not None:
            models = self.__currentproject.get_models()
            for model in models:
                action = self.__models_menu.addAction(model)
                action.triggered.connect(self.__set_current_model)
                if self.__currentproject.get_current_model() is None \
                        or self.__currentproject.get_current_model().name == model:
                    self.__currentproject.set_current_model(model)
                    ft = action.font()
                    ft.setBold(True)
                    action.setFont(ft)

    def __model_changed_fct(self):
        self.__refresh_ui_menus()
        self.__refresh_layers()

    def __scn_changed_fct(self):
        self.__refresh_ui_menus()
        self.__refresh_layers()

    def __refresh_layers(self, table_names=[]):
        layer_names = [TablesProperties.get_properties()[table_name]['name'] for table_name in table_names+['invalid', 'configured_current']]
        for layer in self.__iface.mapCanvas().layers():
            if not table_names:
                layer.reload()
            else:
                if (layer.name() in layer_names) or (layer.name() in table_names):
                    layer.reload()

    def __remove_dependant_menus(self):
        if self.__project_tree:
            self.__iface.mainWindow().removeDockWidget(self.__project_tree)
            self.__project_tree.setParent(None)
            self.__project_tree = None
        if self.__model_dock:
            self.__iface.mainWindow().removeDockWidget(self.__model_dock)
            self.__model_dock.setParent(None)
            self.__model_dock = None
        if self.__time_control_profil_dock:
            self.__time_control_profil_dock.setParent(None)
            self.__time_control_profil_dock = None
        if self.__long_profil_dock:
            self.__iface.mainWindow().removeDockWidget(self.__long_profil_dock)
            self.__long_profil_dock.setParent(None)
            self.__long_profil_dock = None
        if self.__model_menu:
            self.__model_menu.setParent(None)
            self.__model_menu = None
        if self.__hydro_menu:
            self.__hydro_menu.setParent(None)
            self.__hydro_menu = None
        if self.__scenario_menu:
            self.__scenario_menu.setParent(None)
            self.__scenario_menu = None
        if self.__toolbar:
            self.__toolbar.setParent(None)
            self.__toolbar = None

    def unload(self):
        '''Removes the plugin menu item and icon from QGIS GUI.'''
        if QgsApplication.processingRegistry().providerById('hydra'):
            QgsApplication.processingRegistry().removeProvider(self.processing_instance)
        if QgsApplication.processingRegistry().providerById('taudem'):
            QgsApplication.processingRegistry().removeProvider(self.processing_taudem)

        self.__remove_dependant_menus()
        self.__project_menu.setParent(None)
        self.__project_menu = None
        QgsProject.instance().readProject.disconnect(self.project_loaded)
        #self.__iface.projectRead.disconnect(self.project_loaded)
        self.__iface.newProjectCreated.disconnect(self.project_loaded)
        #self.__iface.newProject()
        self.__notebook_process and self.__notebook_process.terminate()

    def open_project_in_ui(self, already_opened=False):
        self.__currentproject.open_in_ui(already_opened)
        if not already_opened:
            self.__currentproject.save()
            target_crs = QgsCoordinateReferenceSystem.fromEpsgId(self.__currentproject.srid)
            self.__iface.mapCanvas().setDestinationCrs(target_crs)
            QgsProject.instance().setCrs(target_crs)
            self.__remove_dependant_menus()
        self.__refresh_ui_menus()

    def __open_project(self):
        self.__currentproject = Project.load_project(self.sender().text(), self.__log_manager, self.__ui_manager)
        self.open_project_in_ui()

    def __project_new(self):
        new_project_dialog = npdialog.NewProjectDialog(self.__iface.mainWindow())
        if new_project_dialog.exec_():
            name, srid, workspace = new_project_dialog.get_result()
            if name != '':
                self.__currentproject = Project.create_new_project(name, srid, workspace, self.__log_manager, self.__ui_manager)
                self.open_project_in_ui()

    def __project_manage(self):
        project_manager_dialog = pjmanagerdialog.ProjectManagerDialog(self.__log_manager, self.__ui_manager,
                    self.__currentproject.name if self.__currentproject is not None else None, self.__iface.mainWindow())
        project_manager_dialog.exec_()
        if project_manager_dialog.new_current_project is not None and self.__currentproject!=project_manager_dialog.new_current_project:
            if self.__currentproject is not None:
                self.__currentproject.close()
            self.__currentproject=project_manager_dialog.new_current_project
            self.open_project_in_ui()
        if project_manager_dialog.qgis_current_project_deleted:
            self.__currentproject.close()
            self.__remove_dependant_menus()
            self.__currentproject=None
        self.__refresh_ui_menus()

    def __model_new(self):
        if self.__currentproject is not None:
            new_model_dialog = nmdialog.NewModelDialog(self.__currentproject)
            new_model_dialog.exec_()
            self.__currentproject.save()
            self.__refresh_ui_menus()
        else:
            self.__log_manager.warning(tr("You need a new a project before adding a new model."))

    def __set_current_model(self):
        if self.__currentproject.get_current_model() is None\
                or self.sender().text() != self.__currentproject.get_current_model().name:
            self.__currentproject.set_current_model(self.sender().text())
            for act in self.__model_menu.findChildren(QAction):
                ft = act.font()
                ft.setBold(act == self.sender())
                act.setFont(ft)
        self.__refresh_ui_menus()

    def __model_delete(self):
        if self.__currentproject is not None and self.__currentproject.get_current_model() is not None:
            confirm = QMessageBox(QMessageBox.Question, tr('Delete model'), tr('This will delete model {}. Proceed?').format(self.__currentproject.get_current_model().name), QMessageBox.Ok | QMessageBox.Cancel).exec_()
            if confirm == QMessageBox.Ok:
                self.__currentproject.delete_model(self.__currentproject.get_current_model().name)
                self.__currentproject.set_current_model(self.__currentproject.get_models()[0] if self.__currentproject.get_models() else None)
                # Bug where model layer of active model are unloaded after deletion of another model
                if self.__currentproject.get_current_model():
                    self.__currentproject.reload_model_layers(self.__currentproject.get_current_model().name)
                self.__refresh_ui_menus()
                self.__currentproject.save()
        else:
            self.__log_manager.notice(tr("No model to delete found."))

    def __project_layers_reload(self):
        if self.__currentproject is not None:
            Project.reload_project_layers(self.__currentproject)
            Project.reload_terrain_layers(self.__currentproject)
            Project.reload_rain_layers(self.__currentproject)
            self.__currentproject.save()
            self.__refresh_ui_menus()
        else:
            self.__log_manager.warning(tr("You need an open project to reload its layers."))

    def __model_layers_reload(self):
        if self.__currentproject is not None and Project.get_current_model(self.__currentproject) is not None:
            current_model = Project.get_current_model(self.__currentproject).name
            Project.reload_model_layers(self.__currentproject, Project.get_current_model(self.__currentproject).name)
            Project.set_current_model(self.__currentproject, current_model)
            self.__currentproject.save()
            self.__refresh_ui_menus()
        else:
            self.__log_manager.warning(tr("You need a project and a model to reload a model."))

    def __terrain_manager(self):
        from .gui.terrain_manager import TerrainManager
        if self.__currentproject is not None:
            terrain_manager = TerrainManager(self.__currentproject, self.__iface.mainWindow())
            terrain_manager.exec_()
            self.__currentproject.get_terrain_errors()
            Project.reload_terrain_layers(self.__currentproject)
            self.__currentproject.save()

    def __config_manager(self):
        from .gui.config_manager import ConfigManager
        if self.__currentproject is not None:
            config_manager = ConfigManager(self.__currentproject, self.__iface.mainWindow())
            config_manager.exec_()
            self.__refresh_layers()
            self.__refresh_ui_menus()

    def __extract(self):
        from .advanced_tools.extract import Extract
        if self.__currentproject is not None:
            extract = Extract(self.__currentproject, self.__iface.mainWindow())
            extract.exec_()

    def __animeau(self):
        from .advanced_tools.animeau import AnimEau
        if self.__currentproject is not None:
            animeau = AnimEau(self.__currentproject, self.__iface.mainWindow())
            animeau.exec_()

    def __crgeng(self):
        from .advanced_tools.crgeng import Crgeng
        if self.__currentproject is not None:
            crgeng = Crgeng(self.__currentproject, self.__iface.mainWindow())
            crgeng.exec_()

    def __section_exporter(self):
        if self.__currentproject is not None:
            SectionExporter(self.__currentproject).exec_()
            self.__refresh_layers()

    def __pre_composed_queries(self):
        if self.__currentproject is not None:
            PreComposedQueriesManager(self.__currentproject).exec_()
            self.__refresh_layers()

    def __export_csv(self):
        from .database.export_csv import ExportCsv
        if self.__currentproject is not None:
            dirName = QFileDialog.getExistingDirectory(self.__iface.mainWindow(), tr('Select directory'), _desktop_dir)
            if dirName:
                exporter = ExportCsv(self.__currentproject)
                exporter.write_csv_to_folder(dirName)

    def __export_live(self):
        from .export_rt import export
        if self.__currentproject is not None:
            export(self.__currentproject.name)
            QMessageBox.information(self.__iface.mainWindow(), tr('Export successfull'),
                tr(f'project {self.__currentproject.name} successfully exported into {os.path.join(self.__currentproject.directory, "REF")}.'), QMessageBox.Ok)

    def __import_live(self):
        from .import_rt import import_, ReferenceScenarioMismatch
        if self.__currentproject is not None:
            zip_file, __ = QFileDialog.getOpenFileName(None, tr('Import live scenario file'), _desktop_dir, tr('Zip files (*.zip)'))
            if zip_file:
                try:
                    scn, scn_id = import_(self.__currentproject.name, zip_file)
                except ReferenceScenarioMismatch as e:
                    if QMessageBox.Ok == QMessageBox(QMessageBox.Question, tr('Import Live'),
                            f'{e.message}\n'+tr("do you want to overwrite REF scenario ?"), QMessageBox.Ok | QMessageBox.Cancel).exec_():
                        scn, scn_id = import_(self.__currentproject.name, zip_file, overwrite_ref=True)

                self.__currentproject.set_current_scenario(scn_id)
                self.__refresh_ui_menus()

    def __launch_notebook(self):
        ''' start jupyter notebook or install it if not already installed'''
        osgeo_root = os.environ['OSGEO4W_ROOT']
        try :
            self.__notebook_process = subprocess.Popen(['jupyter', 'notebook'], cwd=_hydra_dir)
        except:
            QMessageBox.information(self.__iface.mainWindow(), tr('We need to install Jupyter'),
                        tr('we will try to perform auto install of Jupyter!'), QMessageBox.Ok)
            try:
                subprocess.Popen(['runas','/noprofile','/user:Administrator','NeedsAdminPrivilege.exe',os.path.join(osgeo_root,'OSGeo4W.bat'),'python -m pip install jupyter'], stdin=subprocess.PIPE)
            except:
                QMessageBox.information(self.__iface.mainWindow(), tr('We did not install jupyter'),
                        tr('Please do a manual installation of Jupyter'), QMessageBox.Ok)

            QMessageBox.information(self.__iface.mainWindow(), tr('Jupyter installed!'),
                        tr('Jupyter has been installed, please restart Qgis'), QMessageBox.Ok)

    def __dry_inflow_sectors_settings(self):
        from .gui.dry_inflow_sector_settings import DryInflowSectorSettings
        if self.__currentproject is not None:
            dry_inflow = DryInflowSectorSettings(self.__currentproject)
            dry_inflow.exec_()
        self.__refresh_ui_menus()

    def __dry_inflow_manager(self):
        from .gui.dry_inflow_manager import DryInflowManager
        if self.__currentproject is not None:
            dry_inflow = DryInflowManager(self.__currentproject)
            dry_inflow.exec_()
        self.__refresh_ui_menus()

    def __rain_scenario(self):
        from .gui.rain_manager import RainManager
        if self.__currentproject is not None:
            rain = RainManager(self.__currentproject)
            rain.exec_()
            Project.reload_rain_layers(self.__currentproject)
            self.__currentproject.save()
        self.__refresh_ui_menus()

    def __radar_grid(self):
        from .gui.radar_grid import RadarGridManager
        if self.__currentproject is not None:
            rg = RadarGridManager(self.__currentproject)
            rg.exec_()

    def __pollution_land_accumulation(self):
        from .gui.pollution_land_accumulation import PollutionLandAccumulationManager
        if self.__currentproject is not None:
            pollution = PollutionLandAccumulationManager(self.__currentproject)
            pollution.exec_()

    def __land_types(self):
        from .gui.land_type import LandTypeManager
        if self.__currentproject is not None:
            land = LandTypeManager(self.__currentproject, self.__iface.mainWindow())
            land.reload_style_signal.connect(self.__refresh_layers)
            land.exec_()

    def __serie_manage(self):
        from .gui.serie_manager import SerieManager
        if self.__currentproject is not None:
            serie_manager = SerieManager(self.__currentproject)
            serie_manager.exec_()
        self.__refresh_ui_menus()

    def __scenario_manage(self):
        from .gui.scenario_manager import ScenarioManager
        if self.__currentproject is not None:
            scenario = ScenarioManager(self.__currentproject)
            current_scn = self.__currentproject.get_current_scenario()
            if current_scn:
                scenario.select_scenario_by_name(current_scn[1])
            scenario.exec_()
        self.__refresh_ui_menus()

    def __scenario_grouping(self):
        from .gui.scenario_grouping import ScenarioGrouping
        if self.__currentproject is not None:
            dialog = ScenarioGrouping(self.__currentproject)
            dialog.exec_()

    def __import_model_sql(self):
        file_name, __ = QFileDialog.getOpenFileName(None, tr('Import hydra dump file'), _desktop_dir, tr('Sql files (*.sql)'))
        if file_name:
            # Control file is a valid hydra export file
            file_srid = srid_from_dump(file_name)
            file_version = version_from_dump(file_name)
            if file_srid == '' or file_version == '':
                self.__log_manager.warning("File {} is not a valid hydra sql dump file".format(file_name))
                return
            # Control file is compatible with current project
            if file_srid != self.__currentproject.srid:
                self.__log_manager.warning("File {} and project {} SRIDs do not match".format(file_name, self.__currentproject.name))
                return
            if file_version != self.__currentproject.get_version():
                self.__log_manager.warning("File {} and project {} versions do not match".format(file_name, self.__currentproject.name))
                return
            # Find models in file
            models=[]
            with open(file_name, 'r') as fil:
                for line in fil:
                    match = re.search(r"CREATE SCHEMA (\w+);", line)
                    if match:
                        name = match.group(1)
                        models.append(name)
            # Control dump of model only
            if not set(models).isdisjoint(['hydra', 'project', 'public', 'work']):
                self.__log_manager.warning("File {} is not a model-only export. Use 'Export model command' to get a correct file.".format(file_name))
                return
            # Control only one model to import
            if len(models)!=1:
                self.__log_manager.warning("File {} has more than one model. Use 'Export model command'.".format(file_name))
                return
            # chose name for the imported model, defaulting to actual name
            name_model, ok = QInputDialog.getText(None, "Import model", "Give a name to imported model", text=name)

            # check if given name is lower case and 1-24 alphanumeric name
            if (not ok) or normalized_name(name_model) != name_model:
                self.__log_manager.warning("input model name is not valid : '{}'. Please chose another name.".format(name_model))
                return

            # change model in file (load text, then rewrite it)
            if name_model != name:
                with open(file_name) as f:
                    newsqlfilecontent=f.read().replace(name, name_model)
                with open(file_name, "w") as f:
                    f.write(newsqlfilecontent)
            # Check no existing model with same name
            if name_model in self.__currentproject.get_models():
                self.__log_manager.warning("Project {} already has a model named '{}'. Please delete it beforehand.".format(self.__currentproject.name, name_model))
                return
            # Process import
            confirm = QMessageBox(QMessageBox.Question, tr('Import model'), tr('This will import model {} from file {} into project {}. Proceed?'.format(name_model, os.path.basename(file_name), self.__currentproject.name)), QMessageBox.Ok | QMessageBox.Cancel).exec_()
            if confirm == QMessageBox.Ok:
                QApplication.setOverrideCursor(Qt.WaitCursor)
                QApplication.processEvents()
                try:
                    run_psql(self.__currentproject.log, file_name, self.__currentproject.name)
                except Exception:
                    self.__currentproject.delete_model(name_model)
                    QApplication.restoreOverrideCursor()
                    QMessageBox.critical(self.__iface.mainWindow(), tr('Import error'),
                                tr('Error importing model {} into {}. See hydra.log file.').format(name_model, self.__currentproject.name), QMessageBox.Ok)
                    return
                self.__currentproject.execute("""select pg_catalog.setval('project.model_config_id_seq', (select MAX(id)+1 from project.model_config), true);""")
                self.__currentproject.execute("""insert into project.model_config(name) values ('{}');""".format(name_model))
                self.__currentproject.set_current_model(name_model)
                self.__currentproject.reload_model_layers(name_model)
                self.__currentproject.commit()
                self.__refresh_ui_menus()
                QApplication.restoreOverrideCursor()
                QMessageBox.information(self.__iface.mainWindow(), tr('Import successfull'),
                            tr('Model {} successfully imported into {}.').format(name_model, self.__currentproject.name), QMessageBox.Ok)

    def __export_model_sql(self):
        from .versions.dump_restore import dump_project
        # Choose one model to import
        models = self.__currentproject.get_models()
        model=None
        if len(models)==1:
            model=models[0]
        elif len(models)>1:
            m, ok = QInputDialog.getItem(self.__iface.mainWindow(), "Model selection", "Select a model export", models, 0, False)
            if ok and m:
                model=m
        # Process export
        if model:
            file_name, __ = QFileDialog.getSaveFileName(self.__iface.mainWindow(), tr('Export model'), _desktop_dir, tr('Sql files (*.sql)'))
            if file_name:
                QApplication.setOverrideCursor(Qt.WaitCursor)
                QApplication.processEvents()
                QApplication.restoreOverrideCursor()
                try:
                    dump_project(self.__currentproject.log, file_name, self.__currentproject.name, model)
                except Exception:
                    QApplication.restoreOverrideCursor()
                    QMessageBox.critical(self.__iface.mainWindow(), tr('Export error'),
                                tr('Error exporting model {}. See hydra.log file.').format(model), QMessageBox.Ok)
                    return
                QApplication.restoreOverrideCursor()
                QMessageBox.information(self.__iface.mainWindow(), tr('Export successfull'),
                            tr('Model {} successfully exported in file {}.').format(model, file_name), QMessageBox.Ok)

    def __import_model_csv(self):
        from .database.import_model import import_model_csv
        from .gui.text_info import TextInfo
        fileName, __ = QFileDialog.getOpenFileName(self.__iface.mainWindow(), tr('Import csv file'), _desktop_dir, tr('Csv files (*.csv)'))
        if fileName:
            model_name = os.path.split(fileName)[1][:-4]
            try:
                def log_info(info, info_form):
                    if info_form is not None:
                        info_form.textInfos.append(info)
                        QApplication.processEvents()

                info_form = TextInfo(self.__iface.mainWindow())
                info_form.show()
                info_form.textInfos.clear()
                QApplication.setOverrideCursor(Qt.WaitCursor)
                QApplication.processEvents()
                if model_name in self.__currentproject.get_models():
                    self.__currentproject.delete_model(model_name)
                log_info("Creating model...", info_form)
                self.__currentproject.add_new_model(model_name)
                self.__currentproject.set_current_model(model_name)
                log_info("Import model...", info_form)
                import_model_csv(fileName, self.__currentproject, self.__currentproject.get_current_model(), model_name, self.__log_manager, info_form)
                log_info("Save imported model...", info_form)
                self.__currentproject.commit()
                self.__refresh_layers()
                self.__refresh_ui_menus()
                if info_form:
                    info_form.close()
                self.__log_manager.notice("{f} imported in model {m}".format(f=fileName,m=model_name))
                QApplication.restoreOverrideCursor()
                QMessageBox(QMessageBox.Information, tr('Import model'), tr('Importation of model {} finished.'.format(model_name)), QMessageBox.Ok).exec_()
            except Exception as e:
                self.__currentproject.rollback()
                QApplication.restoreOverrideCursor()
                traceback.print_exc()
                raise e

    def __import_scenario_mages(self):
        from .advanced_tools.import_mages_scenario import import_mages_scenario
        path = QFileDialog.getExistingDirectory(self.__iface.mainWindow(), "Select a directory")
        if path:
            import_mages_scenario(self.__currentproject, path)
            self.__currentproject.commit()

    def __import_project_csv(self):
        from .database.import_project import import_project_csv
        from .gui.text_info import TextInfo
        fileName, __ = QFileDialog.getOpenFileName(self.__iface.mainWindow(), tr('Import csv file'), _desktop_dir, tr('Csv files (*.csv)'))
        if fileName:
            try:
                info_form = TextInfo(self.__iface.mainWindow())
                info_form.show()
                info_form.textInfos.clear()
                QApplication.setOverrideCursor(Qt.WaitCursor)
                QApplication.processEvents()
                import_project_csv(fileName, self.__currentproject, self.__log_manager, info_form)
                self.__currentproject.commit()
                self.__refresh_ui_menus()
                if info_form:
                    info_form.close()
                self.__log_manager.notice("{f} imported as project data".format(f=fileName))
                QApplication.restoreOverrideCursor()
                QMessageBox(QMessageBox.Information, tr('Import project data'), tr('Importation of datas from file {} finished.'.format(fileName)), QMessageBox.Ok).exec_()
            except Exception as e:
                self.__currentproject.rollback()
                QApplication.restoreOverrideCursor()
                traceback.print_exc()
                raise e

    def __work_manager(self):
        if self.__currentproject is not None:
            WorkManager(self.__currentproject, parent=None).exec_()
        else:
            self.__log_manager.warning(tr("You need an open project to access work schema."))

    def __changelog(self):
        metadata_file = os.path.join(_plugin_dir, 'metadata.txt')
        with open(metadata_file, 'r') as f:
            changelog_list = f.readlines()[31:-1]
        changelog_box = QMessageBox()
        changelog_box.setWindowTitle(tr('Changelog'))
        scroll = QScrollArea(changelog_box)
        scroll.setWidgetResizable(True)
        changelog_box.content = QWidget()
        scroll.setWidget(changelog_box.content)
        lay = QVBoxLayout(changelog_box.content)
        lay.setSpacing(0)
        lay.setMargin(0)
        for item in changelog_list:
            widget = QLabel(item[:-1], changelog_box)
            if isfloat(item):
                widget.setStyleSheet("font-weight: bold")
            lay.addWidget(widget)
        changelog_box.layout().addWidget(scroll, 0, 0, 1, changelog_box.layout().columnCount())
        changelog_box.setStyleSheet("QScrollArea{min-width:800 px; min-height: 494px}")
        changelog_box.exec_()

    def __settings(self):
        hydra_settings = HydraSettings(project=self.__currentproject)
        hydra_settings.exec_()
        if os.name == 'nt':
            deploy_dependencies(_plugin_dir, self.__log_manager)

    def __licence_activation(self):
        if not check_licence():
            self.activate_licence()
        else:
            confirm = QMessageBox.information(self.__iface.mainWindow(), tr('Licence already activated'),
                tr('A licence has already been activated. Do you want to activate it again?'), QMessageBox.Ok)
            if confirm == QMessageBox.Ok:
                self.activate_licence()

    def __doc(self):
        webbrowser.open('https://wiki.hydra-software.net', new=0)

    def __doc_hydraulics(self):
        webbrowser.open('http://hydra-software.net/docs/manuels/analyse_hydraulique.pdf', new=0)

    def __doc_hydrology(self):
        webbrowser.open('http://hydra-software.net/docs/manuels/analyse_hydrologie.pdf', new=0)

    def __doc_quality(self):
        webbrowser.open('http://hydra-software.net/docs/manuels/analyse_qualite.pdf', new=0)

    def __doc_sedimentation(self):
        webbrowser.open('http://hydra-software.net/docs/manuels/analyse_transport_sedimentaire.pdf', new=0)

    def __doc_nt(self, nt_name):
        webbrowser.open('http://hydra-software.net/docs/notes_techniques/'+nt_name, new=0)

    def __about(self):
        build_info = get_build().split('\n')
        version =  build_info[1].split(':')[1]
        date_ = datetime.fromtimestamp(os.stat(build_file).st_ctime).strftime("%Y-%m-%d")

        about_pic = QPixmap(os.path.join(_plugin_dir, 'ressources', 'images', 'hydra_logo.png')).scaledToHeight(32, Qt.SmoothTransformation)

        about_text = f"<b>Hydra version {version} {date_}</b><br><br>"
        about_file = os.path.join(_plugin_dir, 'license_short.txt')
        with open(about_file, 'r') as f:
            about_text += f.read()
        about_text += f"<br><br><b>Build info</b><br>{'<br>'.join([line.replace('    ', '- ') for line in build_info])}"
        about_text += f"<br><b>Serial number</b><br>{get_serial()}"

        about_box = QMessageBox()
        about_box.setWindowTitle(tr('About Hydra'))
        about_box.setIconPixmap(about_pic)
        about_box.setTextFormat(Qt.RichText)
        about_box.setText(about_text)
        about_box.exec_()
