/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update hydra.metadata set version = '3.5';
;;

create or replace function project.model_instead_fct()
returns trigger
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_model
    for statement in create_model(TD['new']['name'], $srid, '$version'):
        plpy.execute(statement)
$$$$
;;

create or replace function project.drop_api() returns void language plpython3u volatile as
$$$$
    from $hydra import drop_model_api, drop_project_api, create_project_api, create_model_api
    r = list(plpy.execute('select srid, version from hydra.metadata'))[0]
    srid, version = r['srid'], r['version']
    for r in plpy.execute('select name from project.model'):
        for statement in drop_model_api(r['name']):
            plpy.notice(statement)
            plpy.execute(statement)
    for statement in drop_project_api():
        plpy.notice(statement)
        plpy.execute(statement)
$$$$
;;

create or replace function project.refresh_api() returns void language plpython3u volatile as
$$$$
    from $hydra import drop_model_api, drop_project_api, create_project_api, create_model_api
    r = list(plpy.execute('select srid, version from hydra.metadata'))[0]
    srid, version = r['srid'], r['version']
    for r in plpy.execute('select name from project.model'):
        for statement in drop_model_api(r['name']):
            plpy.notice(statement)
            plpy.execute(statement)
    for statement in drop_project_api():
        plpy.notice(statement)
        plpy.execute(statement)
    for statement in create_project_api(srid, version):
        plpy.notice(statement)
        plpy.execute(statement)
    for r in plpy.execute('select name from project.model'):
        for statement in create_model_api(r['name'], srid, version):
            plpy.notice(statement)
            plpy.execute(statement)
$$$$
;;

--isolated--
alter type hydra_gate_mode_regul add value if not exists 'relief'
;;
--isolated--
alter type hydra_gate_mode_regul add value if not exists 'head'
;;
--isolated--
alter type hydra_gate_mode_regul add value if not exists 'q(h)'
;;

insert into hydra.gate_mode_regul(name, id, description) values
 ('q(h)', 5, 'Q(h)'),
 ('head', 4, 'Head'),
 ('relief', 3, 'Relief')
;;

do
$$$$
    begin
    if exists (
        select 1 from pg_catalog.pg_namespace where nspname = 'csv'
    )
    then
      execute 'insert into csv.hydra_compatible_versions(hydra_version) values (''3.5'')';
    end if;
end
$$$$;
;;


