/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*              Rainfalls                         */
/* ********************************************** */

create view project.radar_grid_display as(
    with grid as (
            select ST_X(origin_point) as x0, ST_Y(origin_point) as y0, nx, ny, dx, dy from project.radar_grid
            )
    select
        row_number() OVER () AS id,
        i + 1 as row,
        j + 1 as col,
        ST_SetSRID(ST_Translate(cell, x0 + i * dx, y0 - j * dy), $srid) as geom
	from grid,
		generate_series(0, nx - 1) AS i,
	    generate_series(0, ny - 1) AS j,
	    (select ST_SetSRID(('POLYGON((0 0, 0 -'||dy||', '||dx||' -'||dy||', '||dx||' 0,0 0))')::geometry, $srid) as cell from grid) as poly
)
;;


