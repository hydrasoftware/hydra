create schema hydra;
create type hydra_pollution_quality_mode as enum ('pollution', 'quality1', 'quality2', 'quality3');
create table hydra.pollution_quality_mode(
    name hydra_pollution_quality_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.pollution_quality_mode(name,id) values ('pollution',1);
insert into hydra.pollution_quality_mode(name,id) values ('quality1',2);
insert into hydra.pollution_quality_mode(name,id) values ('quality2',3);
insert into hydra.pollution_quality_mode(name,id) values ('quality3',4);
create type hydra_valve_mode as enum ('one_way_downstream', 'one_way_upstream', 'no_valve');
create table hydra.valve_mode(
    name hydra_valve_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.valve_mode(name,description,id) values ('one_way_downstream','one way valve (toward downstream)',2);
insert into hydra.valve_mode(name,description,id) values ('one_way_upstream','one way valve (toward upstream)',3);
insert into hydra.valve_mode(name,description,id) values ('no_valve','no valve',1);
create type hydra_borda_headloss_singularity_type as enum ('v', 'screen_oblique_to_flow', 'q', 'sharp_bend_rectangular', 'parametric', 'sharp_transition_geometry', 'circular_bend', 'sharp_angle_bend', 'transition_edge_inlet', 'enlargment_with_transition', 'screen_normal_to_flow', 'friction', 'diffuser', 'contraction_with_transition');
create table hydra.borda_headloss_singularity_type(
    name hydra_borda_headloss_singularity_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('v','Law dE=(K/2g).(Q2/S2)',2);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('screen_oblique_to_flow','Screen oblique to flow',11);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('q','Law dz=KQ2',1);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('sharp_bend_rectangular','Sharp bend rectangular',14);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('parametric','Parametric',13);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('sharp_transition_geometry','Sharp geometry transition',5);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('circular_bend','Circular bend',8);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('sharp_angle_bend','Sharp angle bend',9);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('transition_edge_inlet','Transition edge inlet',3);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('enlargment_with_transition','Enlargment with transition',7);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('screen_normal_to_flow','Screen normal to flow',10);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('friction','Friction',12);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('diffuser','Diffuser',4);
insert into hydra.borda_headloss_singularity_type(name,description,id) values ('contraction_with_transition','Contraction with transition',6);
create type hydra_transport_type as enum ('sediment', 'pollution', 'quality', 'no_transport');
create table hydra.transport_type(
    name hydra_transport_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.transport_type(name,description,id) values ('sediment','Sediment transport',4);
insert into hydra.transport_type(name,description,id) values ('pollution','Pollution generation and network transport',2);
insert into hydra.transport_type(name,description,id) values ('quality','Quality',3);
insert into hydra.transport_type(name,description,id) values ('no_transport','None',1);
create type hydra_type_domain as enum ('network', '2d_domain', 'river');
create table hydra.type_domain(
    name hydra_type_domain primary key,
    id integer unique,
    description varchar
    );
insert into hydra.type_domain(name,id) values ('network',1);
insert into hydra.type_domain(name,id) values ('2d_domain',3);
insert into hydra.type_domain(name,id) values ('river',2);
create type hydra_cross_section_type as enum ('circular', 'ovoid', 'pipe', 'valley', 'channel');
create table hydra.cross_section_type(
    name hydra_cross_section_type primary key,
    id integer unique,
    description varchar,
    abbreviation varchar unique
    );
insert into hydra.cross_section_type(name,id,abbreviation) values ('circular',6,'CI');
insert into hydra.cross_section_type(name,id,abbreviation) values ('ovoid',5,'OV');
insert into hydra.cross_section_type(name,id,abbreviation) values ('pipe',3,'PF');
insert into hydra.cross_section_type(name,id,abbreviation) values ('valley',2,'VL');
insert into hydra.cross_section_type(name,id,abbreviation) values ('channel',4,'PO');
create type hydra_model_connect_mode as enum ('hydrograph', 'zq_downstream_condition');
create table hydra.model_connect_mode(
    name hydra_model_connect_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.model_connect_mode(name,description,id) values ('hydrograph','Hydrograph',1);
insert into hydra.model_connect_mode(name,description,id) values ('zq_downstream_condition','Downstream condition: Q(Z)',2);
create type hydra_runoff_type as enum ('k_desbordes', 'passini', 'socose', 'giandotti');
create table hydra.runoff_type(
    name hydra_runoff_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.runoff_type(name,description,id) values ('k_desbordes','k-Desbordes  and linear reservoir',1);
insert into hydra.runoff_type(name,description,id) values ('passini','k-Passini  and linear reservoir',2);
insert into hydra.runoff_type(name,description,id) values ('socose','Tc imposed and Socose',4);
insert into hydra.runoff_type(name,description,id) values ('giandotti','k-Giandotti and linear reservoir',3);
create type hydra_weir_mode_regul as enum ('elevation', 'width');
create table hydra.weir_mode_regul(
    name hydra_weir_mode_regul primary key,
    id integer unique,
    description varchar
    );
insert into hydra.weir_mode_regul(name,description,id) values ('elevation','Elevation',1);
insert into hydra.weir_mode_regul(name,description,id) values ('width','Width',2);
create type hydra_rainfall_type as enum ('caquot', 'gage', 'single_triangular', 'radar', 'double_triangular', 'intensity_curve');
create table hydra.rainfall_type(
    name hydra_rainfall_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.rainfall_type(name,id) values ('caquot',1);
insert into hydra.rainfall_type(name,id) values ('gage',5);
insert into hydra.rainfall_type(name,id) values ('single_triangular',2);
insert into hydra.rainfall_type(name,id) values ('radar',6);
insert into hydra.rainfall_type(name,id) values ('double_triangular',3);
insert into hydra.rainfall_type(name,id) values ('intensity_curve',4);
create type hydra_abutment_type as enum ('angle_45', 'rounded', 'angle_90');
create table hydra.abutment_type(
    name hydra_abutment_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.abutment_type(name,description,id) values ('angle_45','Angle 45',2);
insert into hydra.abutment_type(name,description,id) values ('rounded','Rounded edge',3);
insert into hydra.abutment_type(name,description,id) values ('angle_90','Angle 90',1);
create type hydra_singularity_type as enum ('pipe_branch_marker', 'strickler_bc', 'zq_split_hydrology', 'gate', 'tz_bc', 'qq_split_hydrology', 'borda_headloss', 'bridge_headloss', 'param_headloss', 'marker', 'reservoir_rs_hydrology', 'zregul_weir', 'hydraulic_cut', 'zq_bc', 'reservoir_rsp_hydrology', 'froude_bc', 'constant_inflow_bc', 'hydrograph_bc', 'model_connect_bc', 'weir_bc', 'bradley_headloss', 'regul_sluice_gate', 'hydrology_bc', 'tank_bc');
create table hydra.singularity_type(
    name hydra_singularity_type primary key,
    id integer unique,
    description varchar,
    abbreviation varchar unique
    );
insert into hydra.singularity_type(name,id,abbreviation) values ('pipe_branch_marker',9,'MRKB');
insert into hydra.singularity_type(name,id,abbreviation) values ('strickler_bc',12,'RK');
insert into hydra.singularity_type(name,id,abbreviation) values ('zq_split_hydrology',21,'DZH');
insert into hydra.singularity_type(name,id,abbreviation) values ('gate',1,'VA');
insert into hydra.singularity_type(name,id,abbreviation) values ('tz_bc',14,'ZT');
insert into hydra.singularity_type(name,id,abbreviation) values ('qq_split_hydrology',20,'DQH');
insert into hydra.singularity_type(name,id,abbreviation) values ('borda_headloss',3,'SMK');
insert into hydra.singularity_type(name,id,abbreviation) values ('bridge_headloss',25,'BRDG');
insert into hydra.singularity_type(name,id,abbreviation) values ('param_headloss',4,'DH');
insert into hydra.singularity_type(name,id,abbreviation) values ('marker',7,'MRKP');
insert into hydra.singularity_type(name,id,abbreviation) values ('reservoir_rs_hydrology',22,'RSH');
insert into hydra.singularity_type(name,id,abbreviation) values ('zregul_weir',5,'DE');
insert into hydra.singularity_type(name,id,abbreviation) values ('hydraulic_cut',2,'CPR');
insert into hydra.singularity_type(name,id,abbreviation) values ('zq_bc',13,'ZQ');
insert into hydra.singularity_type(name,id,abbreviation) values ('reservoir_rsp_hydrology',23,'RSPH');
insert into hydra.singularity_type(name,id,abbreviation) values ('froude_bc',15,'ZF');
insert into hydra.singularity_type(name,id,abbreviation) values ('constant_inflow_bc',18,'HC');
insert into hydra.singularity_type(name,id,abbreviation) values ('hydrograph_bc',17,'HY');
insert into hydra.singularity_type(name,id,abbreviation) values ('model_connect_bc',19,'RACC');
insert into hydra.singularity_type(name,id,abbreviation) values ('weir_bc',11,'DL');
insert into hydra.singularity_type(name,id,abbreviation) values ('bradley_headloss',8,'BRD');
insert into hydra.singularity_type(name,id,abbreviation) values ('regul_sluice_gate',6,'ACTA');
insert into hydra.singularity_type(name,id,abbreviation) values ('hydrology_bc',24,'CLH');
insert into hydra.singularity_type(name,id,abbreviation) values ('tank_bc',16,'BO');
create type hydra_weir_mode_reoxy as enum ('r15', 'gameson');
create table hydra.weir_mode_reoxy(
    name hydra_weir_mode_reoxy primary key,
    id integer unique,
    description varchar
    );
insert into hydra.weir_mode_reoxy(name,description,id) values ('r15','R15 law',2);
insert into hydra.weir_mode_reoxy(name,description,id) values ('gameson','Gameson law',1);
create type hydra_constrain_type as enum ('porous', 'connector', 'strickler', 'flood_plain_transect', 'overflow');
create table hydra.constrain_type(
    name hydra_constrain_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.constrain_type(name,id) values ('porous',2);
insert into hydra.constrain_type(name,id) values ('connector',6);
insert into hydra.constrain_type(name,id) values ('strickler',5);
insert into hydra.constrain_type(name,id) values ('flood_plain_transect',7);
insert into hydra.constrain_type(name,id) values ('overflow',1);
create type hydra_type_sor1 as enum ('hys_file', 'deb_or_lim_file');
create table hydra.type_sor1(
    name hydra_type_sor1 primary key,
    id integer unique,
    description varchar
    );
insert into hydra.type_sor1(name,description,id) values ('hys_file','hys file',1);
insert into hydra.type_sor1(name,description,id) values ('deb_or_lim_file','deb/lim file',2);
create type hydra_coverage_type as enum ('crossroad', '2d', 'storage', 'reach', 'reach_end', 'left_flood_plain', 'right_flood_plain', 'street');
create table hydra.coverage_type(
    name hydra_coverage_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.coverage_type(name,id) values ('crossroad',4);
insert into hydra.coverage_type(name,id) values ('2d',1);
insert into hydra.coverage_type(name,id) values ('storage',7);
insert into hydra.coverage_type(name,id) values ('reach',2);
insert into hydra.coverage_type(name,id) values ('reach_end',8);
insert into hydra.coverage_type(name,id) values ('left_flood_plain',5);
insert into hydra.coverage_type(name,id) values ('right_flood_plain',6);
insert into hydra.coverage_type(name,id) values ('street',3);
create type hydra_catchment_pollution_mode as enum ('fixed_concentration', 'variable_concentration');
create table hydra.catchment_pollution_mode(
    name hydra_catchment_pollution_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.catchment_pollution_mode(name,id) values ('fixed_concentration',1);
insert into hydra.catchment_pollution_mode(name,id) values ('variable_concentration',2);
create type hydra_quality_type as enum ('quality_parameter_selection', 'physico_chemical', 'suspended sediment transport');
create table hydra.quality_type(
    name hydra_quality_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.quality_type(name,description,id) values ('quality_parameter_selection','Quality parameter selection: no chemical interaction betwwen parameters',2);
insert into hydra.quality_type(name,description,id) values ('physico_chemical','Physico chemical: 4 inter related parameters: DBO, NH4, NO3, O2',1);
insert into hydra.quality_type(name,description,id) values ('suspended sediment transport','Suspended sediment transport',3);
create type hydra_model_connection_settings as enum ('cascade', 'global', 'mixed');
create table hydra.model_connection_settings(
    name hydra_model_connection_settings primary key,
    id integer unique,
    description varchar
    );
insert into hydra.model_connection_settings(name,id) values ('cascade',1);
insert into hydra.model_connection_settings(name,id) values ('global',2);
insert into hydra.model_connection_settings(name,id) values ('mixed',3);
create type hydra_gate_mode_regul as enum ('no_regulation', 'elevation', 'discharge');
create table hydra.gate_mode_regul(
    name hydra_gate_mode_regul primary key,
    id integer unique,
    description varchar
    );
insert into hydra.gate_mode_regul(name,description,id) values ('no_regulation','No regulation',0);
insert into hydra.gate_mode_regul(name,description,id) values ('elevation','Elevation',1);
insert into hydra.gate_mode_regul(name,description,id) values ('discharge','Discharge',2);
create type hydra_borda_headloss_link_type as enum ('v', 'screen_oblique_to_flow', 't_shape_bifurcation', 't_shape_junction', 'q', 'junction', 'sharp_edge_outlet', 'sharp_bend_rectangular', 'parametric', 'sharp_transition_geometry', 'sharp_edge_inlet', 'circular_bend', 'sharp_angle_bend', 'bifurcation', 'enlargment_with_transition', 'screen_normal_to_flow', 'transition_edge_inlet', 'friction', 'diffuser', 'contraction_with_transition');
create table hydra.borda_headloss_link_type(
    name hydra_borda_headloss_link_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.borda_headloss_link_type(name,description,id) values ('v','Law dE=(K/2g).(Q2/S2)',2);
insert into hydra.borda_headloss_link_type(name,description,id) values ('screen_oblique_to_flow','Screen oblique to flow',15);
insert into hydra.borda_headloss_link_type(name,description,id) values ('t_shape_bifurcation','T shape bifurcation',19);
insert into hydra.borda_headloss_link_type(name,description,id) values ('t_shape_junction','T shape junction',20);
insert into hydra.borda_headloss_link_type(name,description,id) values ('q','Law dz=KQ2',1);
insert into hydra.borda_headloss_link_type(name,description,id) values ('junction','Junction',6);
insert into hydra.borda_headloss_link_type(name,description,id) values ('sharp_edge_outlet','Sharp edge outlet',3);
insert into hydra.borda_headloss_link_type(name,description,id) values ('sharp_bend_rectangular','Sharp rectangular bend',18);
insert into hydra.borda_headloss_link_type(name,description,id) values ('parametric','Parametric',17);
insert into hydra.borda_headloss_link_type(name,description,id) values ('sharp_transition_geometry','Sharp geometry transition',9);
insert into hydra.borda_headloss_link_type(name,description,id) values ('sharp_edge_inlet','Sharp edge inlet',4);
insert into hydra.borda_headloss_link_type(name,description,id) values ('circular_bend','Circular bend',12);
insert into hydra.borda_headloss_link_type(name,description,id) values ('sharp_angle_bend','Sharp angle bend',13);
insert into hydra.borda_headloss_link_type(name,description,id) values ('bifurcation','Bifurcation',5);
insert into hydra.borda_headloss_link_type(name,description,id) values ('enlargment_with_transition','Enlargment with transition',11);
insert into hydra.borda_headloss_link_type(name,description,id) values ('screen_normal_to_flow','Screen normal to flow',14);
insert into hydra.borda_headloss_link_type(name,description,id) values ('transition_edge_inlet','Transition edge inlet',7);
insert into hydra.borda_headloss_link_type(name,description,id) values ('friction','Friction',16);
insert into hydra.borda_headloss_link_type(name,description,id) values ('diffuser','Diffuser',8);
insert into hydra.borda_headloss_link_type(name,description,id) values ('contraction_with_transition','Contraction with transition',10);
create type hydra_computation_mode as enum ('hydrology_and_hydraulics', 'hydrology', 'hydraulics');
create table hydra.computation_mode(
    name hydra_computation_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.computation_mode(name,id) values ('hydrology_and_hydraulics',3);
insert into hydra.computation_mode(name,id) values ('hydrology',1);
insert into hydra.computation_mode(name,id) values ('hydraulics',2);
create type hydra_netflow_type as enum ('scs', 'constant_runoff', 'permeable_soil', 'horner', 'holtan');
create table hydra.netflow_type(
    name hydra_netflow_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.netflow_type(name,description,id) values ('scs','SCS',4);
insert into hydra.netflow_type(name,description,id) values ('constant_runoff','Constant runoff',1);
insert into hydra.netflow_type(name,description,id) values ('permeable_soil','Permeable soil',5);
insert into hydra.netflow_type(name,description,id) values ('horner','Horner',2);
insert into hydra.netflow_type(name,description,id) values ('holtan','Holtan',3);
create type hydra_param_sor1_type as enum ('upstream_downstream_singularity', 'downstream_singularity', 'downstream_upstream_singularity', 'upstream_singularity', 'hydrologic_element', 'link');
create table hydra.param_sor1_type(
    name hydra_param_sor1_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.param_sor1_type(name) values ('upstream_downstream_singularity');
insert into hydra.param_sor1_type(name) values ('downstream_singularity');
insert into hydra.param_sor1_type(name) values ('downstream_upstream_singularity');
insert into hydra.param_sor1_type(name) values ('upstream_singularity');
insert into hydra.param_sor1_type(name) values ('hydrologic_element');
insert into hydra.param_sor1_type(name) values ('link');
create type hydra_rerouting_option as enum ('derout_fraction', 'cut_flow_rate');
create table hydra.rerouting_option(
    name hydra_rerouting_option primary key,
    id integer unique,
    description varchar
    );
insert into hydra.rerouting_option(name,description,id) values ('derout_fraction','(between 0 et 1)',1);
insert into hydra.rerouting_option(name,description,id) values ('cut_flow_rate','(m3/s)',2);
create type hydra_pollution_treatment_mode as enum ('efficiency', 'residual_concentration');
create table hydra.pollution_treatment_mode(
    name hydra_pollution_treatment_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.pollution_treatment_mode(name,description,id) values ('efficiency','Efficiency',2);
insert into hydra.pollution_treatment_mode(name,description,id) values ('residual_concentration','Residual concentration',1);
create type hydra_rainfall_interpolation_type as enum ('thyssen', 'distance_ponderation');
create table hydra.rainfall_interpolation_type(
    name hydra_rainfall_interpolation_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.rainfall_interpolation_type(name) values ('thyssen');
insert into hydra.rainfall_interpolation_type(name) values ('distance_ponderation');
create type hydra_node_type as enum ('crossroad', 'station', 'manhole', 'storage', 'river', 'catchment', 'elem_2d', 'manhole_hydrology');
create table hydra.node_type(
    name hydra_node_type primary key,
    id integer unique,
    description varchar,
    abbreviation varchar unique
    );
insert into hydra.node_type(name,id,abbreviation) values ('crossroad',6,'CAR');
insert into hydra.node_type(name,id,abbreviation) values ('station',3,'NODS');
insert into hydra.node_type(name,id,abbreviation) values ('manhole',1,'NODA');
insert into hydra.node_type(name,id,abbreviation) values ('storage',4,'CAS');
insert into hydra.node_type(name,id,abbreviation) values ('river',2,'NODR');
insert into hydra.node_type(name,id,abbreviation) values ('catchment',9,'BC');
insert into hydra.node_type(name,id,abbreviation) values ('elem_2d',5,'PAV');
insert into hydra.node_type(name,id,abbreviation) values ('manhole_hydrology',8,'NODH');
create type hydra_split_law_type as enum ('gate', 'weir', 'zq', 'stickler');
create table hydra.split_law_type(
    name hydra_split_law_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.split_law_type(name,id) values ('gate',2);
insert into hydra.split_law_type(name,id) values ('weir',1);
insert into hydra.split_law_type(name,id) values ('zq',4);
insert into hydra.split_law_type(name,id) values ('stickler',3);
create type hydra_land_use_type as enum ('industrial', 'rural', 'individual_housing', 'group_housing');
create table hydra.land_use_type(
    name hydra_land_use_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.land_use_type(name,id) values ('industrial',2);
insert into hydra.land_use_type(name,id) values ('rural',1);
insert into hydra.land_use_type(name,id) values ('individual_housing',3);
insert into hydra.land_use_type(name,id) values ('group_housing',4);
create type hydra_link_type as enum ('strickler', 'gate', 'borda_headloss', 'weir', 'overflow', 'fuse_spillway', 'pump', 'connector', 'mesh_2d', 'street', 'connector_hydrology', 'pipe', 'porous', 'routing_hydrology', 'regul_gate', 'network_overflow', 'deriv_pump');
create table hydra.link_type(
    name hydra_link_type primary key,
    id integer unique,
    description varchar,
    abbreviation varchar unique
    );
insert into hydra.link_type(name,id,abbreviation) values ('strickler',12,'LSTK');
insert into hydra.link_type(name,id,abbreviation) values ('gate',5,'LORF');
insert into hydra.link_type(name,id,abbreviation) values ('borda_headloss',10,'QMK');
insert into hydra.link_type(name,id,abbreviation) values ('weir',4,'LDEV');
insert into hydra.link_type(name,id,abbreviation) values ('overflow',21,'OFL');
insert into hydra.link_type(name,id,abbreviation) values ('fuse_spillway',15,'LDVF');
insert into hydra.link_type(name,id,abbreviation) values ('pump',6,'QMP');
insert into hydra.link_type(name,id,abbreviation) values ('connector',9,'QMTR');
insert into hydra.link_type(name,id,abbreviation) values ('mesh_2d',11,'LPAV');
insert into hydra.link_type(name,id,abbreviation) values ('street',16,'LRUE');
insert into hydra.link_type(name,id,abbreviation) values ('connector_hydrology',19,'TRH');
insert into hydra.link_type(name,id,abbreviation) values ('pipe',20,'TRC');
insert into hydra.link_type(name,id,abbreviation) values ('porous',13,'LPOR');
insert into hydra.link_type(name,id,abbreviation) values ('routing_hydrology',18,'ROUT');
insert into hydra.link_type(name,id,abbreviation) values ('regul_gate',7,'RG');
insert into hydra.link_type(name,id,abbreviation) values ('network_overflow',22,'NOV');
insert into hydra.link_type(name,id,abbreviation) values ('deriv_pump',3,'QDP');
create type hydra_action_gate_type as enum ('downward_opening', 'upward_opening');
create table hydra.action_gate_type(
    name hydra_action_gate_type primary key,
    id integer unique,
    description varchar
    );
insert into hydra.action_gate_type(name,description,id) values ('downward_opening','downward opening',1);
insert into hydra.action_gate_type(name,description,id) values ('upward_opening','upward opening',2);
create type hydra_fuse_spillway_break_mode as enum ('time_critical', 'zw_critical', 'none');
create table hydra.fuse_spillway_break_mode(
    name hydra_fuse_spillway_break_mode primary key,
    id integer unique,
    description varchar
    );
insert into hydra.fuse_spillway_break_mode(name,description,id) values ('time_critical','Break on time',2);
insert into hydra.fuse_spillway_break_mode(name,description,id) values ('zw_critical','Break on water level',1);
insert into hydra.fuse_spillway_break_mode(name,description,id) values ('none','No break',0);
create table hydra.metadata(
    creation_date timestamp not null default current_date,
    srid integer references spatial_ref_sys(srid),
    precision real not null default .01,
    workspace varchar(256) not null default '',
    version varchar(24) not null default '0.0.27',
    id integer unique not null default(1) check (id=1) -- only one row
    );
create function hydra.metadata_after_insert_fct()
        returns trigger
        language plpython3u
        as $$
            from hydra_0_0_27 import create_project
            import plpy
            for statement in create_project(TD['new']['srid'], '0.0.27'):
                plpy.execute(statement)
        $$;
create trigger hydra_metadata_after_insert_trig
        after insert on hydra.metadata
        for each row execute procedure hydra.metadata_after_insert_fct()
        ;
