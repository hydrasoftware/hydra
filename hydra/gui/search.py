from __future__ import absolute_import
from builtins import str
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from .base_dialog import BaseDialog, tr
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QListWidgetItem
from qgis.core import QgsPointXY, QgsRectangle
import os
import re
from hydra.gui.edit import EditTool
from hydra.utility.sql_json import instances

class SearchTool(BaseDialog):
    def __init__(self, project, iface, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "search.ui"), self)

        self.project = project
        self.instances = instances
        self.iface = iface

        self.nodes = list(self.instances['node_type'].keys())
        self.singularities = list(self.instances['singularity_type'].keys())
        self.links = list(self.instances['link_type'].keys())
        self.model_tables = ['reach', 'station', 'branch', 'catchment', 'river_cross_section_profile']
        self.project_tables = ['dry_inflow_sector', 'rain_gage', 'wind_gage']

        self.model.addItems(self.project.get_models())
        if self.project.get_current_model() is not None :
            self.model.setCurrentIndex(self.project.get_models().index(self.project.get_current_model().name))
        else :
            self.model.setCurrentIndex(-1)

        self.model.activated.connect(self.__model_changed)
        self.field.textChanged.connect(self.__clear)
        self.edit_button.clicked.connect(self.__edit)
        self.zoom_button.clicked.connect(self.__zoom)

        self.__clear()

    def __model_changed(self):
        if self.model.currentIndex() != -1 :
            self.project.set_current_model(self.model.currentText())
        else :
            self.project.log.warning(tr("No active model"))
            self.project.set_current_model(None)

    def __clear(self):
        self.items_list.clear()
        self.items_list.hide()
        self.label_info.setText('')

    def refresh_list(self, items):
        self.__clear()
        self.items_list.show()
        for item in items:
            self.items_list.addItem(QListWidgetItem(str(item[0]) + ": " + str(item[1]) + ": " + str(item[2])))

    def search(self):
        tables = []
        tables.extend([[self.project.get_current_model().name, n+'_node'] for n in self.nodes])
        tables.extend([[self.project.get_current_model().name, s+'_singularity'] for s in self.singularities])
        tables.extend([[self.project.get_current_model().name, l+'_link'] for l in self.links])
        tables.extend([[self.project.get_current_model().name, 'street']])
        tables.extend([[self.project.get_current_model().name, m] for m in self.model_tables])
        tables.extend([['project', p] for p in self.project_tables])

        results = []

        if self.field.text():
            for table in tables:
                hits = self.project.execute("""select id, name from {s}.{t} where UPPER(name) like UPPER('%{n}%')
                                            """.format(s=table[0], t=table[1], n=self.field.text())).fetchall()
                if len(hits):
                    results.extend([[table[1], id, name] for [id, name] in hits])
        return results

    def __edit(self):
        if self.items_list.currentItem() is not None:
            table, id, name = self.items_list.currentItem().text().split(': ')
            EditTool.edit(self.project, table, id)
        else:
            results = self.search()
            if len(results)==0:
                self.label_info.setText('No object found. Please refine your search.')
            elif len(results)==1:
                EditTool.edit(self.project, results[0][0], results[0][1])
            elif len(results)>1:
                self.refresh_list(results)
                self.label_info.setText('Multiples objects found. Please either:\no Select an item\no Refine your search')

    def __zoom(self):
        if self.items_list.currentItem() is not None:
            table, id, name = self.items_list.currentItem().text().split(': ')
            type, geom = self.project.execute("""select ST_GeometryType(ST_Envelope(geom)), ST_AsText(ST_Envelope(geom))  from {s}.{t} where id={i}
                                            """.format(s=self.project.get_current_model().name, t=table, i=id)).fetchone()
            self.__zoom_canvas(type, geom)
        else:
            results = self.search()
            if len(results)==0:
                self.label_info.setText('No object found. Please refine your search.')
            elif len(results)==1:
                type, geom = self.project.execute("""select ST_GeometryType(ST_Envelope(geom)), ST_AsText(ST_Envelope(geom)) from {s}.{t} where id={i}
                                                """.format(s=self.project.get_current_model().name, t=results[0][0], i=results[0][1])).fetchone()
                self.__zoom_canvas(type, geom)
            elif len(results)>1:
                self.refresh_list(results)
                self.label_info.setText('Multiples objects found. Please either:\no Select an item\no Refine your search')

    def __zoom_canvas(self, type, geom):
        if type == 'ST_Point':
            # geom = 'POINT(X Y)'
            x, y = geom[6:-1].split(' ')
            point1 = QgsPointXY(float(x)-10, float(y)-10)
            point2 = QgsPointXY(float(x)+10, float(y)+10)
        elif type == 'ST_Polygon':
            # geom = 'POLYGON((X1 Y1,X2 Y2,X3 Y3,X4 Y4))'
            x1, y1 = geom[9:-2].split(',')[0].split(' ')
            x2, y2 = geom[9:-2].split(',')[2].split(' ')
            point1 = QgsPointXY(float(x1), float(y1))
            point2 = QgsPointXY(float(x2), float(y2))
        if point1:
            extent = QgsRectangle(point1, point2)
            self.iface.mapCanvas().setExtent(extent)
            self.iface.mapCanvas().refresh()
            self.close()
        else:
            return

