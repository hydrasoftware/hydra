from encodings.aliases import aliases
from processing.gui.wrappers import WidgetWrapper
from qgis.PyQt.QtWidgets import QComboBox, QLineEdit
from qgis.PyQt.QtCore  import Qt
#from qgis.gui import QgsCrsSelectionWidget only from QGIS 3.24
from qgis.gui import QgsCheckableComboBox
from qgis.core import QgsProject

import json

from ..database.database import get_projects_list, is_hydra_db
from ..project import Project

class ProjectCombo(QComboBox):
    def __init__(self, parent=None):
        super().__init__(parent)
        project_path = QgsProject.instance().readPath("./")
        project_name = project_path.split("/")[-1]
        if is_hydra_db(project_name):
            self.addItem(project_name)
        else:
            self.addItems(sorted(get_projects_list()))

    def mousePressEvent(self, e):
        if self.count() == 1:
            self.clear()
            self.addItems(sorted(get_projects_list()))
        return QComboBox.mousePressEvent(self, e)

class ProjectWidget(WidgetWrapper):
    last_combo = None
    def createWidget(self):
        self.__combo = ProjectCombo()
        self.__combo.setEditable(True)
        ProjectWidget.last_combo = self.__combo
        return self.__combo

    def value(self):
        return self.__combo.currentText()

    def setValue(self, txt):
        self.__combo.setCurrentText(txt)


class SridWidget(WidgetWrapper):
    def createWidget(self):
        self.__widget = QLineEdit()
        c = ProjectWidget.last_combo
        if c is not None:
            self.__project_changed(c.currentText())
            c.currentTextChanged.connect(self.__project_changed)
        return self.__widget

    def value(self):
        return self.__widget.text()

    def __project_changed(self, project_name):
        self.__widget.setEnabled(True)
        if project_name and is_hydra_db(project_name):
            project = Project(project_name)
            self.__widget.setText(f"{project.srid}")
            self.__widget.setEnabled(False)


class ScenarioWidget(WidgetWrapper):
    def createWidget(self):
        self.__combo = QComboBox()
        c = ProjectWidget.last_combo
        if c is not None:
            self.__project_changed(c.currentText())
            c.currentTextChanged.connect(self.__project_changed)
        return self.__combo

    def value(self):
        return self.__combo.currentText()

    def setValue(self, txt):
        self.__combo.setCurrentText(txt)

    def __project_changed(self, project_name):
        self.__combo.clear()
        if project_name and is_hydra_db(project_name):
            project = Project(project_name)
            self.__combo.addItems(sorted(project.scenarios))
            current_scenario = project.current_scenario
            if current_scenario and current_scenario in project.scenarios:
                self.__combo.setCurrentText(current_scenario)

class ScenariosWidget(WidgetWrapper):
    def createWidget(self):
        self.__combo = QgsCheckableComboBox()
        c = ProjectWidget.last_combo
        if c is not None:
            self.__project_changed(c.currentText())
            c.currentTextChanged.connect(self.__project_changed)
        return self.__combo

    def value(self):
        return json.dumps(self.__combo.checkedItems())
        
    def setValue(self, value):
        #Set item check state to unchecked for all items
        for i in range(self.__combo.count()) :
            self.__combo.setItemCheckState(i, Qt.Unchecked)
        
        if isinstance(value, str):
            try:
                value = json.loads(value)
            except json.JSONDecodeError:
                value = value.split(",")
                
        self.__combo.setCheckedItems(value)

    def __project_changed(self, project_name):
        self.__combo.clear()
        if project_name and is_hydra_db(project_name):
            project = Project(project_name)
            current_scenario = project.current_scenario
            scenarios = project.scenarios
            for i, scn in enumerate(sorted(scenarios)):
                self.__combo.addItemWithCheckState(scn,
                    Qt.Checked if (current_scenario and current_scenario in scenarios and current_scenario == scn) else Qt.Unchecked )

class ScenarioGroupWidget(WidgetWrapper):
    def createWidget(self):
        self.__combo = QComboBox()
        c = ProjectWidget.last_combo
        if c is not None:
            self.__project_changed(c.currentText())
            c.currentTextChanged.connect(self.__project_changed)
        return self.__combo

    def value(self):
        return self.__combo.currentText()

    def __project_changed(self, project_name):
        self.__combo.clear()
        if project_name and is_hydra_db(project_name):
            project = Project(project_name)
            self.__combo.addItems(project.scenario_groups)

class ModelWidget(WidgetWrapper):
    def createWidget(self):
        self.__combo = QComboBox()
        c = ProjectWidget.last_combo
        if c is not None:
            self.__project_changed(c.currentText())
            c.currentTextChanged.connect(self.__project_changed)
        return self.__combo

    def value(self):
        return self.__combo.currentText()

    def __project_changed(self, project_name):
        self.__combo.clear()
        if project_name and is_hydra_db(project_name):
            project = Project(project_name)
            self.__combo.addItems(sorted(project.models))
            current_model = project.current_model
            if current_model and current_model in project.models:
                self.__combo.setCurrentText(current_model)

class EncodingWidget(WidgetWrapper):
    def createWidget(self):
        self.__widget = QComboBox()
        self.__widget.addItems(sorted(aliases.keys()))
        return self.__widget

    def value(self):
        return self.__widget.currentText()

    def setParameterValue(self, value):
        self.__widget.setCurrentText(value)

class Logger:
    def __init__(self, feedback):
        self.feedback = feedback

    def notice(self, msg):
        self.feedback.pushInfo(msg)

    def warning(self, msg):
        self.feedback.pushWarning(msg)

    def error(self, msg):
        self.feedback.reportError(msg)

    def cleanup(self):
        pass


if __name__ == '__main__':
    from qgis.core import QgsApplication, QgsProject
    from qgis.PyQt.QtWidgets import QDialog, QVBoxLayout
    import os
    import sys
    from qgis.core import QgsProcessingParameterString

    QgsApplication.setPrefixPath(os.environ['QGIS_PREFIX_PATH'])
    a = QgsApplication([], True)

    if len(sys.argv) > 1:
        QgsProject.instance().read(sys.argv[1])

    d = QDialog()

    param = QgsProcessingParameterString('project', "Project")
    param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
    p = ProjectWidget(param, d)

    param = QgsProcessingParameterString('scenario', "Scenario")
    param.setMetadata({ 'widget_wrapper': {'class': ScenarioWidget}})
    s = ScenarioWidget(param, d)

    d.setLayout(QVBoxLayout())
    d.layout().addWidget(p.createWidget())
    d.layout().addWidget(s.createWidget())

    d.exec_()


