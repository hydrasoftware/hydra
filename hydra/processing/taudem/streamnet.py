# -*- coding: utf-8 -*-

"""
***************************************************************************
    streamnet.py
    ---------------------
    Date                 : January 2018
    Copyright            : (C) 2018-2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'January 2018'
__copyright__ = '(C) 2018-2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.core import (QgsVectorFileWriter,
                       QgsProcessing,
                       QgsProcessingException,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterBoolean,
                       QgsProcessingParameterRasterDestination,
                       QgsProcessingParameterVectorDestination,
                       QgsProcessingParameterFileDestination
                      )

from .taudemAlgorithm import TauDemAlgorithm
from . import taudemUtils


class StreamNet(TauDemAlgorithm):

    PIT_FILLED = 'PIT_FILLED'
    D8_FLOWDIR = 'D8_FLOWDIR'
    D8_CONTRIB_AREA = 'D8_CONTRIB_AREA'
    STREAM_RASTER = 'STREAM_RASTER'
    OUTLETS = 'OUTLETS'
    SINGLE_WATERSHED = 'SINGLE_WATERSHED'
    STREAM_ORDER = 'STREAM_ORDER'
    WATERSHED = 'WATERSHED'
    STREAM_REACH = 'STREAM_REACH'
    NETWORK_CONNECTIVITY = 'NETWORK_CONNECTIVITY'
    NETWORK_COORDINATES = 'NETWORK_COORDINATES'

    def name(self):
        return 'streamnet'

    def displayName(self):
        return self.tr('Stream reach and watershed')

    def group(self):
        return self.tr('Stream network analysis')

    def groupId(self):
        return 'streamanalysis'

    def tags(self):
        return self.tr('dem,hydrology,dem,stream,network,watershed').split(',')

    def shortHelpString(self):
        return self.tr('Produces a vector network from the Stream Raster grid '
                       'by tracing down from each source grid cell.')

    def helpUrl(self):
        return 'http://hydrology.usu.edu/taudem/taudem5/help53/StreamReachAndWatershed.html'

    def __init__(self):
        super().__init__()

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterRasterLayer(self.PIT_FILLED,
                                                            self.tr('Pit filled elevation')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.D8_FLOWDIR,
                                                            self.tr('D8 flow directions')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.D8_CONTRIB_AREA,
                                                            self.tr('D8 contributing area')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.STREAM_RASTER,
                                                            self.tr('Stream raster')))
        self.addParameter(QgsProcessingParameterFeatureSource(self.OUTLETS,
                                                              self.tr('Outlets'),
                                                              types=[QgsProcessing.TypeVectorPoint],
                                                              optional=True))
        self.addParameter(QgsProcessingParameterBoolean(self.SINGLE_WATERSHED,
                                                        self.tr('Delineate single watershed'),
                                                        defaultValue=False))

        self.addParameter(QgsProcessingParameterRasterDestination(self.STREAM_ORDER,
                                                                  self.tr('Stream order')))
        self.addParameter(QgsProcessingParameterRasterDestination(self.WATERSHED,
                                                                  self.tr('Watershed')))
        self.addParameter(QgsProcessingParameterVectorDestination(self.STREAM_REACH,
                                                                  self.tr('Channel network'),
                                                                  QgsProcessing.TypeVectorLine))
        self.addParameter(QgsProcessingParameterFileDestination(self.NETWORK_CONNECTIVITY,
                                                                self.tr('Network connectivity tree'),
                                                                self.tr('Data files (*.dat)')))
        self.addParameter(QgsProcessingParameterFileDestination(self.NETWORK_COORDINATES,
                                                                self.tr('Network coordinates'),
                                                                self.tr('Data files (*.dat)')))

    def processAlgorithm(self, parameters, context, feedback):
        demLayer = self.parameterAsRasterLayer(parameters, self.PIT_FILLED, context)
        if demLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.PIT_FILLED))

        flowLayer = self.parameterAsRasterLayer(parameters, self.D8_FLOWDIR, context)
        if flowLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.D8_FLOWDIR))

        contribLayer = self.parameterAsRasterLayer(parameters, self.D8_CONTRIB_AREA, context)
        if contribLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.D8_CONTRIB_AREA))

        streamLayer = self.parameterAsRasterLayer(parameters, self.STREAM_RASTER, context)
        if streamLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.STREAM_RASTER))

        arguments = []
        arguments.append(self.command())

        arguments.append('-fel')
        arguments.append(demLayer.source())
        arguments.append('-p')
        arguments.append(flowLayer.source())
        arguments.append('-ad8')
        arguments.append(contribLayer.source())
        arguments.append('-src')
        arguments.append(streamLayer.source())

        if self.OUTLETS in parameters and parameters[self.OUTLETS] is not None:
            outlets = self.parameterAsCompatibleSourceLayerPath(parameters, self.OUTLETS, context,
                                                                QgsVectorFileWriter.supportedFormatExtensions(),
                                                                feedback=feedback)
            arguments.append('-o')
            arguments.append(outlets)

        if self.parameterAsBool(parameters, self.SINGLE_WATERSHED, context):
            arguments.append('-sw')

        arguments.append('-ord')
        arguments.append(self.parameterAsOutputLayer(parameters, self.STREAM_ORDER, context))

        arguments.append('-w')
        arguments.append(self.parameterAsOutputLayer(parameters, self.WATERSHED, context))

        arguments.append('-net')
        arguments.append(self.parameterAsOutputLayer(parameters, self.STREAM_REACH, context))

        arguments.append('-tree')
        arguments.append(self.parameterAsFileOutput(parameters, self.NETWORK_CONNECTIVITY, context))

        arguments.append('-coord')
        arguments.append(self.parameterAsFileOutput(parameters, self.NETWORK_COORDINATES, context))

        taudemUtils.execute(arguments, feedback)

        return self.algorithmResults(parameters)
