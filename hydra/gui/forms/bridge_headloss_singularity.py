# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QTableWidgetItem
import hydra.utility.string as string
from hydra.gui.simple_singularity_dialog import SimpleSingularityDialog, tr
from hydra.gui.widgets.graph_widget import GraphWidget
from hydra.gui.widgets.array_widget import ArrayWidget

class BridgeHeadlossEditor(SimpleSingularityDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleSingularityDialog.__init__(self, project, geom, "bridge_headloss_singularity", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "bridge_headloss_singularity.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        name, l_road, z_road, zw_array, full_section_discharge_for_headloss, comment = self.project.execute("""
            select name, l_road, z_road, zw_array, full_section_discharge_for_headloss, comment
            from {}.bridge_headloss_singularity
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.name.setText(name)
        self.l_road.setText(string.get_str(l_road))
        self.z_road.setText(string.get_str(z_road))

        self.z_road.textChanged.connect(self.__z_road_changed)

        self.graph = GraphWidget(self.graph_placeholder)
        self.array = ArrayWidget(["Elevation (m)", "Empty cross section (m)"], [10,2], zw_array,
                draw_graph_func=self.__draw_graph, help_func=None, parent=self.array_placeholder)

        # discharge for headloss computation
        node_type, = self.project.execute("""select node_type from {}._singularity where id={}""".format(self.model.name, self.id)).fetchone()
        if node_type != 'river':
            self.groupbox_discharge_for_headloss.setEnabled(False)
        elif full_section_discharge_for_headloss is True:
            self.radioBtn_full_section.setChecked(True)
        elif full_section_discharge_for_headloss is False:
            self.radioBtn_river_bed.setChecked(True)

        self.notes.setText(comment)

    def __draw_graph(self, datas):
        if len(datas)>0:
            self.graph.clear()
            self.graph.add_symetric_line(
                [r[1]/2 for r in datas],
                [r[0] for r in datas],
                "k", self.name.text(),
                tr("Empty width (m)"),
                tr("Z (m)"), top=True)
            if string.isfloat(self.z_road.text()):
                self.graph.add_symetric_line(
                    [r[1]/2 for r in datas],
                    [float(self.z_road.text()) for r in datas],
                    "k", top=True)
        else:
            self.graph.canvas.setVisible(False)

    def __z_road_changed(self):
        self.__draw_graph(self.array.get_table_items())

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))

        self.model.update_singularity_brdg(self.id, self.geom, self.array.get_table_items(),
                                            string.get_sql_float(self.l_road.text()), string.get_sql_float(self.z_road.text()),
                                            not self.radioBtn_river_bed.isChecked(), self.name.text(), comment=self.notes.text())
        SimpleSingularityDialog.save(self)
        self.close()
