/* ************************************************************** */
/* Updates geometries of valley cross section replacing           */
/* flood plain array_length with last point of river bed array    */
/* ************************************************************** */

update $model.valley_cross_section_geometry
set (zbmaj_lbank_array, zbmaj_rbank_array) = (ARRAY[[zbmin_array[array_length(zbmin_array, 1)][1],0]], ARRAY[[zbmin_array[array_length(zbmin_array, 1)][1],0]])
;;