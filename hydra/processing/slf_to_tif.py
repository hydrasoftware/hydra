from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterString,
    QgsProcessingParameterNumber,
    QgsProcessingParameterRasterLayer,
    QgsRasterLayer
)
from qgis.core import QgsProject
from qgis.utils import iface
from .utility import ProjectWidget, ModelWidget, ScenariosWidget
import os
from pathlib import Path
from ..database import database as dbhydra
from wstar.slf_to_tif import process as wstar_process
import json



class SlfToTif(QgsProcessingAlgorithm):
    """
    Converts hydra maximum results to selafin format for use with mesh layers
    """

    PROJECT = "project"
    MODEL = "model"
    SCENARIOS = "scenarios"
    DEM = "dem"
    LOAD_RESULT = "load_result"
    HEIGHT_THRESHOLD_NODATA = "height_threshold_nodata"
    HEIGHT_THRESHOLD_RENDER = "height_threshold_render"
    OUT_TIME = "out_time"
    #ENVELOP_MULTIPLE_SCENARIOS = "envelop_multiple_scenarios"

    def tr(self, string):
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return SlfToTif()

    def name(self):
        return "slf_to_tif"

    def displayName(self):
        return self.tr("Rasterize mesh results at maximum")

    def group(self):
        return self.tr("River and free surface flow mapping")

    def groupId(self):
        return "surfaceflowmapping"

    def shortHelpString(self):
        return self.tr("This algorithm converts Selafin result file to TIF raster at DEM resolution, conserving only the maximum values.\n\n If only one scenario is selected, results can be extracted and converted for a required compuation time.\n\n In any case, if one selects multiple scenarios, the absolute maximum value for the chosen scenarios is returned.")

    def initAlgorithm(self, config=None):

        param = QgsProcessingParameterString(self.PROJECT, "Project")
        param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.MODEL, "Model")
        param.setMetadata({ 'widget_wrapper': {'class': ModelWidget}})
        self.addParameter(param)
        
        param = QgsProcessingParameterString(self.SCENARIOS, "Scenario(s)")
        param.setMetadata({'widget_wrapper': {'class': ScenariosWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterRasterLayer(self.DEM, "Terrain")
        self.addParameter(param)

        param = QgsProcessingParameterNumber(self.HEIGHT_THRESHOLD_RENDER, "Height threshold render (triangles where max height(m) is below are nodata)", defaultValue=0.01, type=QgsProcessingParameterNumber.Double, )
        self.addParameter(param)

        param = QgsProcessingParameterNumber(self.HEIGHT_THRESHOLD_NODATA, "Height threshold nodata (pixels with less than this height(m) are set to nodata)", defaultValue=-1.0, type=QgsProcessingParameterNumber.Double, )
        self.addParameter(param)

        param = QgsProcessingParameterNumber(self.OUT_TIME, "Output time relative to date0 in hours (option working only with a unique scenario)", defaultValue=None, type=QgsProcessingParameterNumber.Double, optional=True )
        self.addParameter(param)

        param = QgsProcessingParameterBoolean(self.LOAD_RESULT, "Load result in project", defaultValue=True, optional=True)
        self.addParameter(param)


    def processAlgorithm(self, parameters, context, feedback):
        project = self.parameterAsString(parameters, self.PROJECT, context)
        self.model = self.parameterAsString(parameters, self.MODEL, context)
        self.scenarios = [s.strip() for s in json.loads(self.parameterAsString(parameters, self.SCENARIOS, context))]
        self.load_result = self.parameterAsBoolean(parameters, self.LOAD_RESULT, context)
        height_threshold_render = self.parameterAsDouble(parameters, self.HEIGHT_THRESHOLD_RENDER, context)
        height_threshold_nodata = self.parameterAsDouble(parameters, self.HEIGHT_THRESHOLD_NODATA, context)
        out_time = self.parameterAsDouble(parameters, self.OUT_TIME, context)

        dem = self.parameterAsRasterLayer(parameters, self.DEM, context).source()

        project_directory = dbhydra.project_dir(project)
        input_slf_list = [Path(project_directory) / scenario.strip().upper() / "hydraulique" / f"{scenario.upper()}_{self.model.upper()}.slf" for scenario in self.scenarios]

        self.output_time = parameters[self.OUT_TIME]
        if len(self.scenarios) > 1:
            self.tif_path = Path(project_directory) / f"scenarios_{self.model}_max.tif"
            self.output_type =  "max"
            out_time = None
        else:
            assert len(self.scenarios) == 1
            if self.output_time is None:
                self.tif_path = Path(project_directory) / self.scenarios[0].upper() / "hydraulique" / f"{self.scenarios[0]}_{self.model}_max.tif"
                self.output_type =  "max"
                out_time = None
            else:
                self.tif_path = Path(project_directory) / f"{self.scenarios[0]}_{self.model}_t{out_time}.tif"
                self.output_type = f"t={out_time}h"

        if self.tif_path.exists():
            os.remove(self.tif_path)

        wstar_process(
            input_slf_list,
            self.tif_path,
            dem,
            bands="zvh",
            time=out_time,
            scale=1,  # FIXME add parameter
            height_threshold_nodata=height_threshold_nodata,
            height_threshold_render=height_threshold_render,
            feedback=feedback
        )

        return {}

    def postProcessAlgorithm(self, context, feedback):
        if iface is not None and self.load_result:
            root = QgsProject.instance().layerTreeRoot()
            grp = 'results'
            subgrp = ', '.join(self.scenarios)
            group = root.findGroup(grp) or root.insertGroup(0, grp)
            subgroup = group.findGroup(subgrp) or group.insertGroup(0, subgrp)
            for layer_name in ('elevation', 'depth', 'velocity'):
                layer = QgsRasterLayer(str(self.tif_path), f"{self.model} {layer_name} {self.output_type}", 'gdal')
                layer.loadNamedStyle(os.path.join(os.path.dirname(__file__), f'raster_{layer_name}.qml'))
                QgsProject.instance().addMapLayer(layer, False)
                item = subgroup.addLayer(layer)
                item.setItemVisibilityChecked(layer_name != 'velocity')

        return {}
