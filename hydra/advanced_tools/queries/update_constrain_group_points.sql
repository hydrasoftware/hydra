/* **************************************************************************************************** */
/* update constrain.points_xyz and update it to the main group around the constrain using proximity     */
/*     + will not affect constrain with default proximity (1m)                                          */
/*     + you can change proximity of constrain before use this query to change the searching radius     */
/*                                                                                                      */
/* **************************************************************************************************** */

with tab as (
    select c.id as c_id, count(ptx.points_id), ptx.points_id as ptx_id
    from $model.constrain as c, project.points_xyz as ptx
    where ST_DWithin(c.geom, ptx.geom, c.points_xyz_proximity)
    group by c.id, ptx.points_id
),
max_value_tab as (
    select c_id, MAX(count) as max_count
    from tab
    group by c_id
),
couple as (
    select tab.c_id, tab.ptx_id
    from tab, max_value_tab
    where max_value_tab.c_id = tab.c_id
    and max_value_tab.max_count = tab.count
)
    update $model.constrain
    set points_xyz = couple.ptx_id
    from couple
    where couple.c_id = constrain.id
    and constrain.points_xyz_proximity <> 1
;;