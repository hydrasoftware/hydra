# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if layers loaded actually exist on the database

USAGE

   python -m hydra.utility.layer_test [-dk]

OPTIONS

   -h, --help
        print this help

   -k, --keep
        does not delete DB at the end

"""

from __future__ import absolute_import # important to read the doc !

assert __name__ == "__main__"

import json
import sys
import os
import getopt
import logging
import psycopg2
from psycopg2.extras import LoggingConnection
from hydra.project import Project
from hydra.database.database import TestProject, project_exists, remove_project
from hydra.utility.settings_properties import SettingsProperties

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hk",
            ["help", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

keep = True if "-k" in optlist or "--keep" in optlist else False

if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

project_name = "layers_test"

if project_exists(project_name):
    remove_project(project_name)

def test_object(object_, project):
    for key, value in object_.items():
        if key == "objects":
            for obj in value:
                test_object(obj, project)
        elif key == "type" and object_["type"] == "layer":
            try:
                project.execute("select * from model.{}".format(object_["table"]))
            except Exception as e:
                raise RuntimeError("Table {} does not exist in model".format(object_["table"]))

with open(os.path.join(os.path.dirname(__file__), "layers_model.json")) as j:
    layer_tree = json.load(j)

test_project = TestProject(project_name)
project = Project.load_project(project_name)
project.add_new_model('model')

test_object(layer_tree, project)

print("ok")

