create index ${model}_node_configuration_idx on $model._node((configuration is not null))
;;
create index ${model}_link_configuration_idx on $model._link((configuration is not null))
;;
create index ${model}_singularity_configuration_idx on $model._singularity((configuration is not null))
;;
create index ${model}_node_validity_idx on $model._node(validity)
;;
create index ${model}_link_validity_idx on $model._link(validity)
;;
create index ${model}_singularity_validity_idx on $model._singularity(validity)
;;


alter table ${model}._pump_link add column response_time_sec real default 300
;;

create or replace function $model.quote_text(t text)
returns text
language plpgsql immutable as
$$$$
    declare x numeric;
begin
x := t::numeric;
    return t;
    exception when others then
    return '"'||t||'"';
end;
$$$$
;;

with dumped as (
    select l.id, c.key, e.key as attr, e.value as val
    from $model._link l
    join lateral json_each_text(l.configuration::json) c on true
    join lateral json_each_text(c.value::json) e on true
    where configuration is not null 
    and link_type='pump'
),
agg1 as (
    select id, key, ('{'||string_agg('"'||
    attr ||'":'|| $model.quote_text(val)
    , ', ')||', "response_time_sec" : 300 }')::json as value
    from dumped
    group by id, key
),
agg2 as (
    select id, ('{'||string_agg('"'||key||'":'||value, ', ')||'}')::json as configuration
    from agg1
    group by id
)
update $model._link l set configuration=agg2.configuration
from agg2 where agg2.id=l.id
;;

drop function $model.quote_text
;;
