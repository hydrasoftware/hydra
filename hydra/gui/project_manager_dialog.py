# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import tempfile
import traceback
import datetime
import zipfile
import shutil
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QCoreApplication, Qt, QDate
from qgis.PyQt.QtWidgets import QApplication, QDialog, QFileDialog, QMessageBox, QTableWidgetItem
from qgis.PyQt.QtGui import QColor
import hydra.gui.new_project_dialog as npdialog
from hydra.gui.widgets.checklist_dialog import ChecklistDialog
from hydra.database import database as dbhydra
from hydra.project import Project
from hydra.utility.empty_ui_manager import EmptyUIManager
from hydra.utility.string import isint
from hydra.versions.update import update_project as updt_project
from hydra.versions.dump_restore import (
    dump_project,
    temp_dump_project,
    run_psql,
    create_db,
    copy_project,
    srid_from_dump,
    version_from_dump,
    run_pg_restore,
)


_desktop_dir = os.path.join(os.path.expanduser('~'), "Desktop")
__hydra_dir__ = os.path.join(os.path.expanduser('~'), ".hydra")

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

def unique_project_name(project_name):
    projects = dbhydra.get_projects_list()
    matches = [prj
                for prj in projects
                if project_name in prj[:len(project_name)]]
    index = [int(name.replace(project_name+'_',''))
                for name in matches
                if isint(name.replace(project_name+'_',''))]
    return str(project_name)+'_'+str(next(i for i, e in enumerate(sorted(index) + [ None ], 1) if i != e))

class ProjectManagerDialog(QDialog):
    def __init__(self, log_manager, ui_manager, qgis_current_project=None, parent=None):
        QDialog.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "project_manager_dialog.ui"), self)
        self.parent = parent

        self.tempdir = tempfile.gettempdir()

        self.__log = log_manager
        self.__ui_manager = ui_manager
        self.__empty_ui_manager = EmptyUIManager()
        self.__qgis_current_project = qgis_current_project

        self.__project_selected()
        self.new_current_project = None
        self.qgis_current_project_deleted = False

        self.table_project.sortItems(0)
        self.table_project.itemSelectionChanged.connect(self.__project_selected)
        self.table_project.cellDoubleClicked.connect(self.__cell_double_clicked)
        self.table_project.setColumnHidden(5, True)
        #self.table_project.resizeRowsToContents()
        #self.table_project.resizeColumnsToContents()
        self.table_project.horizontalHeader().setStretchLastSection(True)
        self.table_project.setHorizontalHeaderLabels(['Project name', 'SRID', 'Version', 'Creation date', 'Workspace', 'Id'])
        self.table_project.setColumnWidth(0,250)
        self.table_project.setColumnWidth(3,150)

        self.btnNewProject.clicked.connect(self.__new_project)
        self.btnOpenProject.clicked.connect(self.__open_project)
        self.btnDeleteProject.clicked.connect(self.__delete_project)
        self.btnDuplicateProject.clicked.connect(self.__copy_project)
        self.btnExport.clicked.connect(self.__export_project)
        self.btnImport.clicked.connect(self.__import_project)
        self.btnUpdate.clicked.connect(self.__update_project)
        self.btnUpdate_all.clicked.connect(self.__update_all_project)
        self.btnExportData.clicked.connect(self.__export_project_zip)
        self.btnImportData.clicked.connect(self.__import_project_zip)
        self.btnClose.clicked.connect(self.close)

        self.ui_version = dbhydra.data_version()
        self.label_version.setText(self.ui_version)
        self.label_version.setStyleSheet("font-weight: bold")

        self.data_table=[]

        self.__refresh_list_projects()

    def __project_selected(self):
        if self.table_project.currentRow()>-1:
            index = int(self.table_project.item(self.table_project.currentRow(), 5).text())-1
            self.__selected_project = self.data_table[index][0]
            self.__selected_srid = self.data_table[index][1]
            self.__selected_version = self.data_table[index][2]
        else:
            self.__selected_project = None
            self.__selected_version = None
            self.__selected_srid = None
        self.__refresh_ui()

    def __refresh_ui(self):
        if self.__selected_project is not None:
            self.__disable_ui()
            self.__enable_ui()

    def __refresh_list_projects(self):

        selected_project = self.__selected_project
        self.__disable_ui()

        self.data_table=[]
        self.table_project.setRowCount(0)
        self.table_project.setSortingEnabled(False)

        projects = sorted(dbhydra.get_projects_list())

        for project_name in projects:
            row_position = self.table_project.rowCount()
            self.table_project.insertRow(row_position)

            srid, version, date = dbhydra.get_project_metadata(project_name)
            workspace = dbhydra.project_dir(project_name)
            self.data_table.append([project_name, srid, version, date, workspace])
            # Column 0: project name
            self.table_project.setItem(row_position, 0, QTableWidgetItem(project_name))
            # Column 1: project SRID
            self.table_project.setItem(row_position, 1, QTableWidgetItem(str(srid)))
            if srid is None or srid == 0:
                self.table_project.item(row_position, 1).setForeground(QColor(255,0,0))
                self.table_project.item(row_position, 1).setToolTip(tr('Bad SRID'))
            # Column 2: project version
            self.table_project.setItem(row_position, 2, QTableWidgetItem(version))
            if version != self.ui_version:
                self.table_project.item(row_position, 2).setForeground(QColor(255,0,0))
                self.table_project.item(row_position, 2).setToolTip(tr('Not current version'))
            # Column 3: project creation date
            if isinstance(date, datetime.date):
                date_item = QDate(date.year, date.month, date.day)
                creation_date = QTableWidgetItem()
                creation_date.setData(0, date_item)
            else:
                creation_date = QTableWidgetItem(tr('No creation date found'))
                creation_date.setForeground(QColor(255,0,0))
            self.table_project.setItem(row_position, 3, creation_date)
            # Column 4: project workspace
            self.table_project.setItem(row_position, 4, QTableWidgetItem(workspace))
            if not os.path.isdir(workspace):
                self.table_project.item(row_position, 4).setForeground(QColor(255,0,0))
                self.table_project.item(row_position, 4).setToolTip(tr('Project folder not found'))
            # Column 5 (hidden): index
            self.table_project.setItem(row_position, 5, QTableWidgetItem(str(row_position+1)))

        self.table_project.setSortingEnabled(True)
        #self.table_project.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

        for r in range(self.table_project.rowCount()):
            if selected_project ==  self.table_project.item(r, 0).text():
                self.table_project.setCurrentCell(r, 0)
                break

        self.__project_selected()

        self.__refresh_ui()

    def __cell_double_clicked(self, r, c):
        self.__selected_project = self.table_project.item(r, 0).text()
        self.__selected_srid = int(self.table_project.item(r, 1).text())
        self.__selected_version = self.table_project.item(r, 2).text()
        if self.__selected_version == self.ui_version:
            self.__open_project()
        else:
            confirm = QMessageBox(QMessageBox.Warning,
                tr('Update project'), tr('Do you want to update project {} from version {} to version {} ?'
                ).format(self.__selected_project, self.__selected_version, self.ui_version), QMessageBox.Ok | QMessageBox.Cancel).exec_()
            if confirm == QMessageBox.Ok:
                self.__update_project()
                self.__open_project()

    def __new_project(self):
        new_project_dialog = npdialog.NewProjectDialog()
        if new_project_dialog.exec_():
            project_name, srid, working_dir = new_project_dialog.get_result()
            if not project_name=='':
                Project.create_new_project(project_name, srid, working_dir, self.__log, self.__empty_ui_manager)
                self.__refresh_list_projects()

    def __open_project(self):
        if (self.__selected_project is not None):
            self.new_current_project = Project.load_project(self.__selected_project, self.__log, self.__ui_manager)
            self.close()

    def __delete_project(self):
        if (self.__selected_project is not None):
            confirm = QMessageBox(QMessageBox.Warning, tr('Delete project'), tr('This will delete project {}. Proceed?').format(self.__selected_project), QMessageBox.Ok | QMessageBox.Cancel)
            confirm.addButton(tr("Also delete project workspace (project directory and all its content)"), QMessageBox.AcceptRole)
            res = confirm.exec_()
            if res in (QMessageBox.Ok, 0):
                self.__disable_ui()
                if (self.__selected_project == self.__qgis_current_project):
                    self.__ui_manager.new_project()
                    self.qgis_current_project_deleted = True
                dbhydra.remove_project(self.__selected_project, clean_dir=res==0)
                self.__log.notice(str(tr("Project {0} deleted.")).format(self.__selected_project))
                self.__refresh_list_projects()

    def __update_project(self):
        if (self.__selected_project is not None):
            project = Project(self.__selected_project, self.__log, self.__empty_ui_manager)
            updt_project(project)
            QMessageBox.information(None, tr('Successful update'), tr("""Project {} updated to version {}.""").format(project.name, project.get_version()), QMessageBox.Ok)
            self.__refresh_list_projects()

    def __update_all_project(self):
        confirm = QMessageBox(QMessageBox.Warning, tr('Update all projects'), tr('This will update all you projects to version {}. Proceed?').format(self.ui_version), QMessageBox.Ok | QMessageBox.Cancel).exec_()
        if confirm == QMessageBox.Ok:
            projects = sorted(dbhydra.get_projects_list())
            for project_name in projects:
                project = Project(project_name, self.__log, self.__empty_ui_manager)
                updt_project(project)
            QMessageBox.information(None, tr('Successful update'), tr("""All projects updated to version {}.""").format(project.get_version()), QMessageBox.Ok)
            self.__refresh_list_projects()

    def __export_project(self):
        if self.__selected_project is not None:
            file_name, __ = QFileDialog.getSaveFileName(
                self,
                tr("Export project"),
                os.path.join(_desktop_dir, self.__selected_project + ".sql"),
                tr("SQL/Dump file(*.sql *.dump)"),
            )
            if file_name:
                # Check file path has no weird character
                if not isinstance(file_name, str):
                    QMessageBox.critical(self, tr('Export error'),
                            tr('File {f} has special characters in path. Choose a simpler directory path.').format(f=file_name), QMessageBox.Ok)
                    return

                # QApplication.setOverrideCursor(Qt.WaitCursor)
                # QApplication.processEvents()
                try:
                    dump_project(self.__log, file_name, self.__selected_project)
                    # QApplication.restoreOverrideCursor()
                    QMessageBox.information(self, tr('Export successfull'), tr('Project {p} successfully exported to {f}.').format(p=self.__selected_project, f=file_name), QMessageBox.Ok)
                except:
                    self.__log.error(traceback.format_exc())
                    # QApplication.restoreOverrideCursor()
                    QMessageBox.critical(self, tr('Export error'), tr('Error exporting project {p} to {f}. See hydra.log file.').format(p=self.__selected_project, f=file_name), QMessageBox.Ok)

    def __export_project_zip(self):
        if self.__selected_project is not None:
            project = Project(self.__selected_project, self.__log, self.__empty_ui_manager)

            archive_name, __ = QFileDialog.getSaveFileName(self, tr('Export project with data'), os.path.join(_desktop_dir, self.__selected_project + '.hyp'), tr('Hydra project file(*.hyp)'))
            if archive_name:
                # Check file path has no weird character
                if not isinstance(archive_name, str):
                    QMessageBox.critical(self, tr('Export error'),
                            tr('File {f} has special characters in path. Choose a simpler directory path.').format(f=archive_name), QMessageBox.Ok)
                    return

                # Select what to add to archive
                list = ['data', 'terrain', 'rain'] + [scn for scn in os.listdir(project.directory) if os.path.isdir(os.path.join(project.directory, scn)) and scn in project.get_scenarios()]
                form = ChecklistDialog('What do you want to export?', list, checked=True)
                if form.exec_() == QDialog.Accepted:
                    # QApplication.setOverrideCursor(Qt.WaitCursor)
                    # QApplication.processEvents()
                    try:
                        if 'data' in form.choices:
                            data_dir = project.data_dir
                            with zipfile.ZipFile(archive_name, 'a', zipfile.ZIP_DEFLATED) as zipw:
                                for root, dirs, files in os.walk(data_dir):
                                    for file in files:
                                        self.__log.notice("Archiving file {} in {}".format(file, archive_name))
                                        zipw.write(os.path.join(root, file), os.path.relpath(os.path.join(root, file), os.path.join(data_dir, '..')))
                        if 'terrain' in form.choices:
                            terrain_dir = os.path.join(project.directory, 'terrain')
                            with zipfile.ZipFile(archive_name, 'a', zipfile.ZIP_DEFLATED) as zipw:
                                for root, dirs, files in os.walk(terrain_dir):
                                    for file in files:
                                        self.__log.notice("Archiving file {} in {}".format(file, archive_name))
                                        zipw.write(os.path.join(root, file), os.path.relpath(os.path.join(root, file), os.path.join(terrain_dir, '..')))
                        if 'rain' in form.choices:
                            rain_dir = os.path.join(project.directory, 'rain')
                            with zipfile.ZipFile(archive_name, 'a', zipfile.ZIP_DEFLATED) as zipw:
                                for root, dirs, files in os.walk(rain_dir):
                                    for file in files:
                                        self.__log.notice("Archiving file {} in {}".format(file, archive_name))
                                        zipw.write(os.path.join(root, file), os.path.relpath(os.path.join(root, file), os.path.join(rain_dir, '..')))
                        scenarios = [s for s in form.choices if s not in ('data', 'terrain', 'rain')]
                        for scenario in scenarios:
                            dir = os.path.join(project.directory, scenario)
                            with zipfile.ZipFile(archive_name, 'a', zipfile.ZIP_DEFLATED) as zipw:
                                for root, dirs, files in os.walk(dir):
                                    for file in files:
                                        self.__log.notice("Archiving file {} in {}".format(file, archive_name))
                                        zipw.write(os.path.join(root, file), os.path.relpath(os.path.join(root, file), os.path.join(dir, '..')))

                        project_file = temp_dump_project(self.__log, self.__selected_project)
                        with zipfile.ZipFile(archive_name,'a') as zipa:
                            zipa.write(project_file, os.path.relpath(project_file, tempfile.gettempdir()))

                        # QApplication.restoreOverrideCursor()
                        QMessageBox.information(self, tr('Export with data successfull'), tr('Project {p} successfully exported to {f} with folder {d}.').format(p=self.__selected_project, f=archive_name, d=data_dir), QMessageBox.Ok)
                    except:
                        # QApplication.restoreOverrideCursor()
                        QMessageBox.critical(self, tr('Export with data error'), tr('Error exporting project {p} to {f} with folder {d}. See hydra.log file.').format(p=self.__selected_project, f=archive_name, d=data_dir), QMessageBox.Ok)

    def __import_project(self):
        file_url, __ = QFileDialog.getOpenFileName(self, tr('Import project'), _desktop_dir, tr('SQL files(*.sql)'))
        if file_url:
            # Check file path has no weird character
            if not isinstance(file_url, str):
                QMessageBox.critical(self, tr('Import error'),
                        tr('File {f} has special characters in path. Move it to a simpler directory path.').format(f=file_url), QMessageBox.Ok)
                return

            # Import project
            try:
                result = self.__import_project_file(file_url)
            except HydraVersionError as e:
                QMessageBox.critical(self, tr('Import error'),
                        tr('File {f} data version {v} do not match available versions ({av}).').format(f=file_url, v=e.version, av=', '.join(dbhydra.get_available_hydra_versions())), QMessageBox.Ok)
                return
            except HydraImportError:
                QMessageBox.critical(self, tr('Import error'), tr('Error importing file {f}. See hydra.log file.').format(f=file_url), QMessageBox.Ok)
                return

            if result is not None:
                QMessageBox.information(self, tr('Import successfull'), tr('Project {p} successfully imported from {f}.').format(p=result, f=file_url), QMessageBox.Ok)
                self.__refresh_list_projects()

    def __import_project_zip(self):
        archive_url,  __ = QFileDialog.getOpenFileName(self, tr('Import project with data'), _desktop_dir, tr('Hydra project file(*.hyp)'))
        if archive_url:
            # Check file path has no weird character
            if not isinstance(archive_url, str):
                QMessageBox.critical(self, tr('Import error'),
                        tr('File {f} has special characters in path. Move it to a simpler directory path.').format(f=archive_url), QMessageBox.Ok)
                return

            # Extract zip
            with zipfile.ZipFile(archive_url, 'r') as zipr:
                self.__log.notice("Extracting files from {}".format(archive_url))
                files_list = zipr.namelist()
                zipr.extractall(tempfile.gettempdir())
            sql_file_name = [f for f in files_list if f[-9:]=='.save.sql'][0]
            sql_file = os.path.join(tempfile.gettempdir(), sql_file_name)
            if not os.path.isfile(sql_file):
                QMessageBox.critical(self, tr('Import error'),
                        tr('Error handling file {f}. No project found.').format(f=archive_url), QMessageBox.Ok)
                return

            # Import project
            try:
                result = self.__import_project_file(sql_file)
            except HydraVersionError as e:
                QMessageBox.critical(self, tr('Import error'),
                        tr('File {f} data version {v} do not match available versions ({av}).').format(f=archive_url, v=e.version, av=', '.join(dbhydra.get_available_hydra_versions())), QMessageBox.Ok)
                return
            except HydraImportError:
                QMessageBox.critical(self, tr('Import error'), tr('Error importing file {f}. See hydra.log file.').format(f=archive_url), QMessageBox.Ok)
                return

            if result is not None:
                # Restore files
                project = Project(result, self.__log, self.__empty_ui_manager)
                for file in files_list:
                    self.__log.notice("Restoring file {} in {}".format(file, project.data_dir))
                    # Restores files with relative path from archive to same path into data_dir
                    file_path = os.path.join(tempfile.gettempdir(), file)
                    dest_dir = os.path.dirname(os.path.join(project.data_dir, '..', file))
                    if not os.path.isdir(dest_dir):
                        os.makedirs(dest_dir)
                    shutil.copy(file_path, dest_dir)

                # Update terrain path if needed
                for file in os.listdir(os.path.join(project.directory, 'terrain')):
                    file_path = os.path.join(project.directory, 'terrain', file)
                    if os.path.isfile(file_path) and file[-4:]=='.vrt':
                        project.execute(f"""update project.dem
                                            set source='{file_path}'
                                            where source like '%{file}'""")

                project.commit()
                QMessageBox.information(self, tr('Import successfull'), tr('Project {p} with data successfully imported from {f}.').format(p=result, f=archive_url), QMessageBox.Ok)
                self.__refresh_list_projects()

    def __import_project_file(self, file):
        if not file.endswith(".dump"):
            file_version = version_from_dump(file)
            # Check file version vs. current database version
            if file_version not in dbhydra.get_available_hydra_versions():
                raise HydraVersionError(file_version)
        # Get project name
        project_name = os.path.splitext(os.path.basename(file))[0]
        if project_name[-5:] == ".save":
            project_name = project_name[:-5]
        if project_name[0] == "_":
            project_name = project_name[1:]

        if file.endswith(".dump"):
            srid = "2154"  # FIXME
        else:
            srid = srid_from_dump(file)
        new_project_dialog = npdialog.NewProjectDialog(
            project_name=project_name, srid=srid
        )
        # Disable ways to change SRID of imported project
        new_project_dialog.srid.setEnabled(False)
        new_project_dialog.btn_epsg.setEnabled(False)
        if new_project_dialog.exec_():
            name, srid, working_dir = new_project_dialog.get_result()
            if name != '':
                # QApplication.setOverrideCursor(Qt.WaitCursor)
                # QApplication.processEvents()
                try:
                    create_db(self.__log, name)
                    if file.endswith(".dump"):
                        run_pg_restore(self.__log, file, name)
                    else:
                        run_psql(self.__log, file, name, strict=False)

                    # QApplication.restoreOverrideCursor()
                    # after create extension Hydra owner is set to hydra_writer
                    # reassign extension owner to hydra
                    #TODO in 3.11
                    #args = ["psql", "-c",
                    #        "set role hydra_writer; reassign owned by hydra_writer to hydra",
                    #        f"service={SettingsProperties.get_service()} dbname={name}"]
                    #run_cmd(args, log=self.__log)
                except:
                    self.__log.error(traceback.format_exc())
                    # QApplication.restoreOverrideCursor()
                    raise HydraImportError

                # Metadatas (SRID, workspace)
                dbhydra.insert_project_metadata(name, srid)
                if not file.endswith(".dump"):
                    assert dbhydra.get_project_metadata(name, "version") == file_version
                if working_dir != __hydra_dir__:
                    with open(os.path.join(__hydra_dir__, name)+'.path', 'w') as path_file:
                        path_file.write(os.path.join(working_dir, name))
                return name

    def __copy_project(self):
        if self.__selected_project is not None:
            new_name = unique_project_name(self.__selected_project+'_copy')
            copy_project_dialog = npdialog.NewProjectDialog(project_name=new_name, srid=self.__selected_srid)
            if copy_project_dialog.exec_():
                name, srid, workspace = copy_project_dialog.get_result()
                if name != '':
                    QApplication.setOverrideCursor(Qt.WaitCursor)
                    QApplication.processEvents()
                    try:
                        copy_project(self.__log, self.__selected_project, name)
                        QApplication.restoreOverrideCursor()
                    except:
                        QApplication.restoreOverrideCursor()
                        QMessageBox.critical(self, tr('Import error'), tr('Error while duplicating project {}. See hydra.log file.').format(self.__selected_project), QMessageBox.Ok)
                        return

                    dbhydra.insert_project_metadata(name, srid)
                    if workspace != __hydra_dir__:
                        with open(os.path.join(__hydra_dir__, name)+'.path', 'w') as path_file:
                            path_file.write(os.path.join(workspace, name))

                    QMessageBox.information(self, tr('Import successfull'), tr('Project {} successfully copied into project {}.').format(self.__selected_project, name), QMessageBox.Ok)
                    self.__refresh_list_projects()

    def __browse(self):
        dir_name = QFileDialog.getExistingDirectory(self, tr('Select workspace path:'), _desktop_dir)
        if dir_name:
            self.workspace.setText(dir_name)

    def __enable_ui(self):
        self.btnDeleteProject.setEnabled(True)
        self.btnExport.setEnabled(True)
        self.btnOpenProject.setEnabled(self.__selected_version == self.ui_version)
        self.btnUpdate.setEnabled(self.__selected_version != self.ui_version)

    def __disable_ui(self):
        self.btnOpenProject.setEnabled(False)
        self.btnDeleteProject.setEnabled(False)
        self.btnExport.setEnabled(False)
        self.btnUpdate.setEnabled(False)

class HydraVersionError(Exception):
    def __init__(self, version):
        self.version = version

class HydraImportError(Exception):
    pass
