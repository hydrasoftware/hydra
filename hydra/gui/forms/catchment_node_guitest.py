# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.forms.catchment_node_guitest [-dhs] [-g project_name model_name x y]

OPTIONS
   -s  --solo
        run test in solo mode (create database)

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name model_name x y
        run in gui mode

"""

from hydra.database.database import TestProject, remove_project, project_exists
from hydra.utility.string import normalized_name, normalized_model_name
import sys
import getopt

from hydra.project import Project

def gui_mode(project_name, model_name, geom, id):

    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.catchment_node import CatchmentNodeEditor
    from hydra.utility.string import normalized_name, normalized_model_name

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)

    test_dialog = CatchmentNodeEditor(obj_project, geom, id)
    test_dialog.exec_()


def test():
    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.catchment_node import CatchmentNodeEditor
    from hydra.database.database import TestProject

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)
    model = obj_project.get_current_model()


    # check cancellation and creation on the same point
    test_dialog = CatchmentNodeEditor(obj_project, [-9, -11])
    test_dialog.reject()

    test_dialog = CatchmentNodeEditor(obj_project, [-9, -11])
    test_dialog.name.setText('test_catchment')
    test_dialog.area_ha.setText('197')
    test_dialog.rl.setText('120')
    test_dialog.slope.setText('0.05')
    test_dialog.c_imp.setText('0.9')

    test_dialog.q0.setText('5')
    test_dialog.q_limit.setText('50')

    test_dialog.netflow_type.setCurrentIndex(test_dialog.netflow_type.findText('Horner'))
    test_dialog.horner_ini_loss_coef.setText('89')
    test_dialog.horner_recharge_coef.setText('0.04')

    test_dialog.socose.setChecked(True)
    test_dialog.socose_tc_mn.setText('66')
    test_dialog.socose_shape_param_beta.setText('12')
    id_catchment = test_dialog.get_id()
    test_dialog.save()

    test_dialog = CatchmentNodeEditor(obj_project, None, id_catchment)
    assert test_dialog.name.text() =='test_catchment'
    assert float(test_dialog.area_ha.text()) == 197
    assert float(test_dialog.rl.text()) == 120
    assert float(test_dialog.slope.text()) == 0.05
    assert float(test_dialog.c_imp.text()) == 0.9

    assert float(test_dialog.q0.text()) == 5
    assert float(test_dialog.q_limit.text()) == 50

    assert test_dialog.netflow_type.currentText() == 'Horner'
    assert float(test_dialog.horner_ini_loss_coef.text()) == 89
    assert float(test_dialog.horner_recharge_coef.text()) == 0.04

    assert test_dialog.socose.isChecked()
    assert float(test_dialog.socose_tc_mn.text()) == 66
    assert float(test_dialog.socose_shape_param_beta.text()) == 12

    test_dialog.autofill()
    test_dialog.reject()


try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])

except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name, with_model=True)
    test()
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    model_name = args[1]
    geom = [float(a) for a in args[2:]]
    id =geom[2] if len(geom)==3 else None
    project_name = normalized_name(project_name)
    model_name = normalized_model_name(model_name)
    gui_mode(project_name, model_name, geom, id)
    exit(0)

test()
# fix_print_with_import
print("ok")
