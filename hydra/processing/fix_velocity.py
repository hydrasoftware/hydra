from qgis.core import (QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingMultiStepFeedback,
    QgsProcessingParameterVectorLayer,
    QgsProcessingParameterString,
    QgsProcessingParameterDistance,
    QgsProcessingParameterVectorDestination,
    QgsProject)
from osgeo import ogr
import os
from qgis import processing
#from . import style

class FixVelocity(QgsProcessingAlgorithm):
    def initAlgorithm(self, config=None):
        proj = QgsProject.instance()

        self.addParameter(QgsProcessingParameterVectorLayer('risk_classes_polygons', 'Depth',
            defaultValue=proj.readEntry('depth_velocity_risk', 'risk_classes_polygons', None)[0]))
        self.addParameter(QgsProcessingParameterVectorLayer('velocity_classes_polygons', 'Velocity',
            defaultValue=proj.readEntry('depth_velocity_risk', 'velocity_classes_polygons', None)[0]))

        self.addParameter(QgsProcessingParameterDistance('too_small_a_surface', 'Too small a surface (m²)',
            defaultValue=proj.readEntry('depth_velocity_risk', 'too_small_a_surface', '0')[0]))


        self.addParameter(QgsProcessingParameterVectorDestination('fixed_velocity_classes_polygons', 'Output velocity file name', QgsProcessing.TypeVectorPolygon))

    def processAlgorithm(self, parameters, context, model_feedback):
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(5, model_feedback)
        results = {}
        outputs = {}

        # Save parameters
        proj = QgsProject.instance()
        for param in ['risk_classes_polygons', 'velocity_classes_polygons', 'too_small_a_surface']:
            proj.writeEntry("depth_velocity_risk", param, parameters[param])


        fixed_velocity_poly = self.parameterAsOutputLayer(parameters, 'fixed_velocity_classes_polygons', context)
        velocity_ext = '.'+fixed_velocity_poly.rsplit(".", 1)[1]

        parameters['risk_classes_polygons'] = self.parameterAsVectorLayer(parameters, 'risk_classes_polygons', context).dataProvider().dataSourceUri()
        parameters['velocity_classes_polygons'] = self.parameterAsVectorLayer(parameters, 'velocity_classes_polygons', context).dataProvider().dataSourceUri()

        work_dir = os.path.dirname(fixed_velocity_poly)
        # get risk small polygons
        feedback.pushInfo('Extract small risk polygons')
        param = {
            'EXPRESSION' : f'area($geometry) < {parameters['too_small_a_surface']}',
            'INPUT' : parameters['risk_classes_polygons'],
            'OUTPUT' : os.path.join(work_dir, 'risk_small_polygons'+velocity_ext)
        }
        processing.run("qgis:extractbyexpression", param)
        feedback.setCurrentStep(1)




        results = {'fixed_velocity_classes_polygons': fixed_velocity_poly}
        feedback.setCurrentStep(4)

        return results

    def name(self):
        return 'risk'

    def displayName(self):
        return 'risk'

    def group(self):
        return ''

    def groupId(self):
        return ''

    def createInstance(self):
        return Risk()

def style(gpkg, classes, field, units):
    if units=='m':
        col_min = [216,231,245]
        col_max = [8,48,107]
    else:
        col_min = [255,200,200]
        col_max = [255,0,0]
    with open(gpkg.rsplit('.', 1)[0] + '.qml', 'w') as qml :
        qml.write('\n'.join(
            ('<!DOCTYPE qgis PUBLIC "http://mrcc.com/qgis.dtd" "SYSTEM">',
            '<qgis styleCategories="Symbology">',
           f'  <renderer-v2 attr="{field}" type="categorizedSymbol" symbollevels="0" enableorderby="0" forceraster="0">',
            '    <categories>\n'))
            )
        i = 0
        for i, c in enumerate(classes):
            qml.write(f'''      <category symbol="{i}" label="{c} {units}" value="{c}" render="true"/>\n''')
        qml.write('    </categories>\n')
        qml.write('    <symbols>\n')
        i = 0
        for i, c in enumerate(classes):
            qml.write('\n'.join(
                ('     <symbol type="fill" force_rhr="0" name="{}" clip_to_extent="1" alpha="1">',
                '       <layer locked="0" enabled="1" class="SimpleFill" pass="0">',
                '         <prop k="color" v="{},{},{},255"/>',
                '         <prop k="outline_style" v="no"/>',
                '           <data_defined_properties>',
                '             <Option type="Map">',
                '               <Option type="QString" value="" name="name"/>',
                '               <Option name="properties"/>',
                '               <Option type="QString" value="collection" name="type"/>',
                '             </Option>',
                '           </data_defined_properties>',
                '       </layer>',
                '     </symbol>\n')).format(i,
                    int(col_min[0]-(col_min[0]-col_max[0])*(i)/len(classes)),
                    int(col_min[1]-(col_min[1]-col_max[1])*(i)/len(classes)),
                    int(col_min[2]-(col_min[2]-col_max[2])*(i)/len(classes))))
        qml.write('\n'.join(
            ('    </symbols>',
            '  <rotation/>',
            '  <sizescale/>',
            ' </renderer-v2>',
            ' <blendMode>0</blendMode>',
            ' <featureBlendMode>0</featureBlendMode>',
            ' <layerGeometryType>2</layerGeometryType>',
            '</qgis>\n')))