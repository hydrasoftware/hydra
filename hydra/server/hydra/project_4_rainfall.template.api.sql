/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

create view project.$tablename as
    select p.id, $tableview, p.name
    from project._$tablename as c, project._rainfall as p
    where p.id = c.id

;;

/* note: id and rainfall_type are not updatable */
create function project.${tablename}_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
    begin
        if tg_op = 'INSERT' then
            insert into project._rainfall(rainfall_type, name)
                values($tableid, coalesce(new.name, 'define_later')) returning id into id_;
            update project._rainfall set name = 'RAIN_'||id_::varchar
                where name = 'define_later' and id = id_;
            insert into project._$tablename
                values (id_, $tableid, $tableinsert);
            update project._rainfall set validity = (select $validity from  project._$tablename where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            update project._rainfall set name=new.name where id=old.id;
            ${commentupdate}update project._$tablename set $tableupdate where id=old.id;
            update project._rainfall set validity = (select $validity from  project._$tablename where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from project._$tablename where id=old.id;
            delete from project._rainfall where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

create trigger project_${tablename}_trig
    instead of insert or update or delete on project.$tablename
       for each row execute procedure project.${tablename}_fct()
;;

