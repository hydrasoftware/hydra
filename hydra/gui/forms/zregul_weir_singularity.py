# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from qgis.PyQt import uic
from qgis.gui import QgsCollapsibleGroupBox
import hydra.utility.string as string
import hydra.utility.sql_json as sql_json
from hydra.gui.simple_singularity_dialog import SimpleSingularityDialog, tr

class ZregulWeirSingularityEditor(SimpleSingularityDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleSingularityDialog.__init__(self, project, geom, "zregul_weir_singularity", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "zregul_weir_singularity.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        name, z_invert, z_regul, width, cc, mode_regul, reoxy_law, reoxy_param_json, full_section_discharge_for_headloss, comment = self.project.execute("""
            select name, z_invert, z_regul, width, cc, mode_regul, reoxy_law, reoxy_param::varchar, full_section_discharge_for_headloss, comment
            from {}.zregul_weir_singularity
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.name.setText(name)
        self.notes.setText(comment)
        self.z_regul.setText(string.get_str(z_regul))
        self.z_invert.setText(string.get_str(z_invert))
        self.z_weir.setText(string.get_str(z_invert))
        self.width.setText(string.get_str(width))
        self.cc.setText(string.get_str(cc))

        # initialize regul type
        self.populate_combo_from_sqlenum(self.mode_regul, "weir_mode_regul", mode_regul)

        # initialize reoxy type
        self.populate_combo_from_sqlenum(self.reoxy_law, "weir_mode_reoxy", reoxy_law)

        self.reoxy_law.activated.connect(self.reoxychanged)
        self.stacked_reoxy.setVisible(False)
        self.reoxychanged()

        # initialize law parameters
        sql_json.set_fields_to_json(self, 'weir_mode_reoxy', reoxy_param_json)

        # discharge for headloss computation
        node_type, = self.project.execute("""select node_type from {}._singularity where id={}""".format(self.model.name, self.id)).fetchone()
        if node_type != 'river':
            self.groupbox_discharge_for_headloss.setEnabled(False)
        elif full_section_discharge_for_headloss is True:
            self.radioBtn_full_section.setChecked(True)
        elif full_section_discharge_for_headloss is False:
            self.radioBtn_river_bed.setChecked(True)

        # set weir type
        if z_regul == z_invert :
            self.radioBtn_fixed_weir.setChecked(True)
            self.labelimage_weir_simple.setVisible(True)
            self.z_regul.setVisible(False)
            self.z_invert.setVisible(False)
            self.z_weir.setVisible(True)
            self.labelImage_weir_regul.setVisible(False)
        elif z_regul != z_invert :
            self.radioBtn_regulated_weir.setChecked(True)
            self.labelimage_weir_simple.setVisible(False)
            self.z_regul.setVisible(True)
            self.z_invert.setVisible(True)
            self.z_weir.setVisible(False)
            self.labelImage_weir_regul.setVisible(True)
        self.radioBtn_fixed_weir.clicked.connect(self.__weirtypechanged)
        self.radioBtn_regulated_weir.clicked.connect(self.__weirtypechanged)

        # reoxy connection
        self.btn_reoxy.clicked.connect(self.__reoxy_clicked)
        self.reoxy_groupbox.setVisible(False)

    def __weirtypechanged(self):
        if self.radioBtn_fixed_weir.isChecked():
            self.z_regul.setVisible(False)
            self.z_invert.setVisible(False)
            self.z_weir.setVisible(True)
            self.labelImage_weir_regul.setVisible(False)
            self.labelimage_weir_simple.setVisible(True)
        elif self.radioBtn_regulated_weir.isChecked():
            self.z_weir.setVisible(False)
            self.z_regul.setVisible(True)
            self.z_invert.setVisible(True)
            self.labelImage_weir_regul.setVisible(True)
            self.labelimage_weir_simple.setVisible(False)

    def reoxychanged(self):
        if self.reoxy_law.currentIndex() != -1:
            if self.reoxy_law.currentText() == 'R15 law':
                    self.stacked_reoxy.setVisible(True)
                    self.stacked_reoxy.setCurrentIndex(0)
            elif self.reoxy_law.currentText() == 'Gameson law':
                    self.stacked_reoxy.setVisible(True)
                    self.stacked_reoxy.setCurrentIndex(1)

    def __reoxy_clicked(self):
        self.reoxy_groupbox.setVisible(not self.reoxy_groupbox.isVisible())

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))

        reoxy_law = self.project.execute("""
            select name
            from hydra.weir_mode_reoxy
            where description='{}'""".format(self.reoxy_law.currentText())).fetchone()[0]

        mode_regul = self.project.execute("""
            select name
            from hydra.weir_mode_regul
            where description='{}'""".format(self.mode_regul.currentText())).fetchone()[0]

        #get reoxy params
        params = sql_json.build_json_from_fields(self, 'weir_mode_reoxy')

        if self.radioBtn_fixed_weir.isChecked():

            self.model.update_singularity_de(self.id, self.geom,
                                                string.get_sql_float(self.z_weir.text()), string.get_sql_float(self.z_weir.text()),
                                                string.get_sql_float(self.width.text()), string.get_sql_float(self.cc.text()),
                                                mode_regul, reoxy_law, params, not self.radioBtn_river_bed.isChecked(), self.name.text(), comment=self.notes.text())

        elif self.radioBtn_regulated_weir.isChecked():
            self.model.update_singularity_de(self.id, self.geom,
                                                string.get_sql_float(self.z_invert.text()), string.get_sql_float(self.z_regul.text()),
                                                string.get_sql_float(self.width.text()), string.get_sql_float(self.cc.text()),
                                                mode_regul, reoxy_law, params, not self.radioBtn_river_bed.isChecked(), self.name.text(), comment=self.notes.text())

        SimpleSingularityDialog.save(self)
        self.close()