--returning query--

/* ********************************************************************************** */
/* Extract cross section hydra :                                                      */
/*      -  give information about geometry up/down on each cross section of a reach.  */
/*                                                                                    */
/* ********************************************************************************** */

-- header of the dataset --
select 'id', 'name', 'z_invert_up','z_invert_down','type_cross_section_up','type_cross_section_down','up_rk','up_rk_maj','up_sinuosity',
      'up_circular_diameter','up_ovoid_height','up_ovoid_top_diameter','up_ovoid_invert_diameter','up_cp_geom','up_op_geom','up_vcs_geom',
      'up_vcs_topo_geom','down_rk','down_rk_maj','down_sinuosity','down_circular_diameter','down_ovoid_height','down_ovoid_top_diameter',
      'down_ovoid_invert_diameter','down_cp_geom','down_op_geom','down_vcs_geom','down_vcs_topo_geom','configuration','z_tn_up','z_tn_down',
      'z_lbank_up','z_lbank_down','z_rbank_up','z_rbank_down','z_ceiling_up','z_ceiling_down','validity','geom', 'name_up', 'sm_max_up',
      'pm_max_up', 'rh_max_up', 'name_down', 'sm_max_down', 'pm_max_down', 'rh_max_up', 'reach', 'pk_km'

;;
--returning query--
with section_table as (
    select rc.id as id, rc.name as cpname, up_vcs_geom as vcs_up, down_vcs_geom as vcs_down, n.pk_km as pk_km, n.reach as reach
    from $model.river_cross_section_profile as rc, $model.river_node as n
    where rc.id = n.id
    order by reach , pk_km),
section_geom as (
    select section_table.id as id, section_table.cpname as cpname, vcs_up.name as name_up, vcs_up.zbmin_array as zbmin_array_up,
    vcs_down.name as name_down, vcs_down.zbmin_array as zbmin_array_down, section_table.pk_km, section_table.reach
    from section_table
    left outer join $model.valley_cross_section_geometry as vcs_up on section_table.vcs_up = vcs_up.id
    left outer join $model.valley_cross_section_geometry as vcs_down on section_table.vcs_down = vcs_down.id
    order by reach, pk_km),
section_sm_pm as (
    select section_geom.id as id, section_geom.cpname as cpname, section_geom.name_up,
    ((section_geom.zbmin_array_up[1][2]+section_geom.zbmin_array_up[2][2])*(section_geom.zbmin_array_up[2][1]-section_geom.zbmin_array_up[1][1]))/2 +
    ((section_geom.zbmin_array_up[2][2]+section_geom.zbmin_array_up[3][2])*(section_geom.zbmin_array_up[3][1]-section_geom.zbmin_array_up[2][1]))/2 +
    ((section_geom.zbmin_array_up[3][2]+section_geom.zbmin_array_up[4][2])*(section_geom.zbmin_array_up[4][1]-section_geom.zbmin_array_up[3][1]))/2 +
    ((section_geom.zbmin_array_up[4][2]+section_geom.zbmin_array_up[5][2])*(section_geom.zbmin_array_up[5][1]-section_geom.zbmin_array_up[4][1]))/2 +
    ((section_geom.zbmin_array_up[5][2]+section_geom.zbmin_array_up[6][2])*(section_geom.zbmin_array_up[6][1]-section_geom.zbmin_array_up[5][1]))/2 as Sm_max_up,
    (sqrt(power(((section_geom.zbmin_array_up[1][2]-section_geom.zbmin_array_up[2][2])/2),2)+power((section_geom.zbmin_array_up[1][1]-section_geom.zbmin_array_up[2][1]),2)) +
    sqrt(power(((section_geom.zbmin_array_up[2][2]-section_geom.zbmin_array_up[3][2])/2),2)+power((section_geom.zbmin_array_up[2][1]-section_geom.zbmin_array_up[3][1]),2)) +
    sqrt(power(((section_geom.zbmin_array_up[3][2]-section_geom.zbmin_array_up[4][2])/2),2)+power((section_geom.zbmin_array_up[3][1]-section_geom.zbmin_array_up[4][1]),2)) +
    sqrt(power(((section_geom.zbmin_array_up[4][2]-section_geom.zbmin_array_up[5][2])/2),2)+power((section_geom.zbmin_array_up[4][1]-section_geom.zbmin_array_up[5][1]),2)) +
    sqrt(power(((section_geom.zbmin_array_up[5][2]-section_geom.zbmin_array_up[6][2])/2),2)+power((section_geom.zbmin_array_up[5][1]-section_geom.zbmin_array_up[6][1]),2))*2
    + section_geom.zbmin_array_up[1][2] ) as Pm_max_up,
    section_geom.name_down,
    ((section_geom.zbmin_array_down[1][2]+section_geom.zbmin_array_down[2][2])*(section_geom.zbmin_array_down[2][1]-section_geom.zbmin_array_down[1][1]))/2 +
    ((section_geom.zbmin_array_down[2][2]+section_geom.zbmin_array_down[3][2])*(section_geom.zbmin_array_down[3][1]-section_geom.zbmin_array_down[2][1]))/2 +
    ((section_geom.zbmin_array_down[3][2]+section_geom.zbmin_array_down[4][2])*(section_geom.zbmin_array_down[4][1]-section_geom.zbmin_array_down[3][1]))/2 +
    ((section_geom.zbmin_array_down[4][2]+section_geom.zbmin_array_down[5][2])*(section_geom.zbmin_array_down[5][1]-section_geom.zbmin_array_down[4][1]))/2 +
    ((section_geom.zbmin_array_down[5][2]+section_geom.zbmin_array_down[6][2])*(section_geom.zbmin_array_down[6][1]-section_geom.zbmin_array_down[5][1]))/2 as Sm_max_down,
    (sqrt(power(((section_geom.zbmin_array_down[1][2]-section_geom.zbmin_array_down[2][2])/2),2)+power((section_geom.zbmin_array_down[1][1]-section_geom.zbmin_array_down[2][1]),2)) +
    sqrt(power(((section_geom.zbmin_array_down[2][2]-section_geom.zbmin_array_down[3][2])/2),2)+power((section_geom.zbmin_array_down[2][1]-section_geom.zbmin_array_down[3][1]),2)) +
    sqrt(power(((section_geom.zbmin_array_down[3][2]-section_geom.zbmin_array_down[4][2])/2),2)+power((section_geom.zbmin_array_down[3][1]-section_geom.zbmin_array_down[4][1]),2)) +
    sqrt(power(((section_geom.zbmin_array_down[4][2]-section_geom.zbmin_array_down[5][2])/2),2)+power((section_geom.zbmin_array_down[4][1]-section_geom.zbmin_array_down[5][1]),2)) +
    sqrt(power(((section_geom.zbmin_array_down[5][2]-section_geom.zbmin_array_down[6][2])/2),2)+power((section_geom.zbmin_array_down[5][1]-section_geom.zbmin_array_down[6][1]),2))*2
    + section_geom.zbmin_array_down[1][2] ) as Pm_max_down,
    section_geom.pk_km as pk_km, section_geom.reach as reach
    from section_geom)
select rc.*, sm_pm.name_up as name_up, round(sm_pm.Sm_max_up::numeric,3) as sm_max_up, round(sm_pm.Pm_max_up::numeric,3) as pm_max_up,
    round((sm_pm.Sm_max_up/sm_pm.Pm_max_up)::numeric,3) as rh_max_up,
    sm_pm.name_down as name_down, round(sm_pm.Sm_max_down::numeric,3) as sm_max_down, round(sm_pm.Pm_max_down::numeric,3) as pm_max_down,
    round((sm_pm.Sm_max_down/sm_pm.Pm_max_down)::numeric,3) as rh_max_down,
    sm_pm.reach as reach, sm_pm.pk_km
    from $model.river_cross_section_profile as rc
    join section_sm_pm as sm_pm on sm_pm.id = rc.id
    order by sm_pm.reach, sm_pm.pk_km

;;