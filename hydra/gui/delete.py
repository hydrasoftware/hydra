# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtWidgets import QMessageBox
from hydra.gui.select_menu import SelectTool
from hydra.utility.tables_properties import TablesProperties

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)


class DeleteTool(SelectTool):
    def __init__(self, project, point, size=10, parent=None):
        SelectTool.__init__(self, project, point, size, parent, excluded_layers = ['regulated'])
        self.cascade = False
        if self.table is not None:
            key_id = self.list_objects[self.schema, self.table][1]
            self.cascade = DeleteTool.delete(self, self.currentproject, self.schema, self.table, key_id, self.id)

    def is_cascade(self):
        return self.cascade

    @staticmethod
    def delete(parent, currentproject, schema, table, key_id, id):
        cascade=False
        if table is None:
            return cascade
        name, = currentproject.execute("select name from {}.{} where {}={};".format(schema, table, key_id, id)).fetchone()

        reply = QMessageBox.question(parent, tr('Confirm'), tr("Are you sure you want to delete {} ({})?".format(name, TablesProperties.get_properties()[table]['name'])), QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

        tn = TablesProperties.get_properties()[table]['name']
        if TablesProperties.get_properties()[table]['name'] in ('Reach', 'Street'):
            reply = QMessageBox.warning(parent, tr('Warning'), tr("/!\ This is a {}, removing it will remove linked object or create domain issue, are you sure ?".format(tn)), QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.Yes:
            success, ret = currentproject.try_execute("delete from {}.{} where {}={};".format(
                                                schema, table, key_id, id))
            if not success:
                reply = QMessageBox.question(parent, tr('Confirm'),
                    tr("Multiple objects will be deleted or modified, continue?"), QMessageBox.Yes |
                    QMessageBox.No, QMessageBox.No)

                if reply == QMessageBox.Yes:
                    cascade=True
                    if table == 'hydrograph_bc_singularity':
                        DeleteTool.delete_hy_bc(parent, currentproject, schema, id)
                    elif table[-5:] == '_link':
                        DeleteTool.delete_link(parent, currentproject, schema, table, id)
                    elif table[-5:]=='_node':
                        DeleteTool.delete_node(parent, currentproject, schema, table, id)
                    elif table =='reach':
                        DeleteTool.delete_reach(parent, currentproject, schema, id)
                    elif table =='station':
                        DeleteTool.delete_station(parent, currentproject, schema, id)
                    elif table =='domain_2d':
                        DeleteTool.delete_domain_2d(parent, currentproject, schema, id)
            currentproject.commit()
        return cascade

    @staticmethod
    def delete_hy_bc(parent, currentproject, schema, id):
        routings = [i for i, in currentproject.execute("""select id
                                                from {model}.routing_hydrology_link
                                                where hydrograph={id}""".format(model=schema, id=id)).fetchall()]
        connectors = [i for i, in currentproject.execute("""select id
                                                from {model}.connector_hydrology_link
                                                where hydrograph={id}""".format(model=schema, id=id)).fetchall()]
        for routing in routings:
            currentproject.execute("""update {model}.routing_hydrology_link set hydrograph=null
                                        where id={id}""".format(model=schema,id=routing))
        for connector in connectors:
            currentproject.execute("""update {model}.connector_hydrology_link set hydrograph=null
                                        where id={id}""".format(model=schema,id=connector))
        currentproject.execute("""delete from {model}.hydrograph_bc_singularity
                                    where id={id}""".format(model=schema, id=id))

    @staticmethod
    def delete_link(parent, currentproject, schema, type, id):
        tables = {'qq_split_hydrology_singularity':['downstream', 'split1', 'split2'],
                  'zq_split_hydrology_singularity':['downstream', 'split1', 'split2'],
                  'reservoir_rs_hydrology_singularity':['drainage', 'overflow'],
                  'reservoir_rsp_hydrology_singularity':['drainage', 'overflow']}
        for table, columns in tables.items():
            for column in columns:
                currentproject.execute("""update {model}.{table} set {column}=null, {column}_type=null where {column}={link}""".format(model=schema, table=table, column=column, link=id))
        currentproject.execute("""delete from {model}.{type}
                                    where id={id}""".format(model=schema, type=type, id=id))

    @staticmethod
    def delete_node(parent, currentproject, schema, table, id):
        links = currentproject.execute("""select id, link_type
                                        from {model}._link
                                        where up={id} or down={id}""".format(model=schema, id=id)).fetchall()
        for link in links:
            DeleteTool.delete_link(parent, currentproject, schema, link[1]+'_link', link[0])
        singularities = currentproject.execute("""select id, singularity_type
                                        from {model}._singularity
                                        where node={id}""".format(model=schema, id=id)).fetchall()
        for singularity in singularities:
            if singularity[1]=='hydrograph_bc':
                DeleteTool.delete_hy_bc(parent, currentproject, schema, singularity[0])
            else:
                currentproject.execute("""delete from {model}.{type}_singularity
                                        where id={id}""".format(model=schema,type=singularity[1], id=singularity[0]))
        if table=='river_node':
            currentproject.execute("""delete from {model}.river_cross_section_profile
                                        where id={id}""".format(model=schema, id=id))
            currentproject.execute("""delete from {model}.river_cross_section_pl1d
                                        where id={id}""".format(model=schema, id=id))
        currentproject.execute("""delete from {model}.{table}
                                    where id={id}""".format(model=schema, table=table, id=id))

    @staticmethod
    def delete_reach(parent, currentproject, schema, id):
        river_node_ids = [i for i, in currentproject.execute("""select id
                                                from {model}.river_node
                                                where reach={id}""".format(model=schema, id=id)).fetchall()]
        for river_node_id in river_node_ids:
            DeleteTool.delete_node(parent, currentproject, schema, 'river_node', river_node_id)
        currentproject.execute("""delete from {model}.reach
                                        where id={id}""".format(model=schema, id=id))

    @staticmethod
    def delete_station(parent, currentproject, schema, id):
        station_node_ids = [i for i, in currentproject.execute("""select id
                                                from {model}.station_node
                                                where station={id}""".format(model=schema, id=id)).fetchall()]
        for station_node_id in station_node_ids:
            DeleteTool.delete_node(parent, currentproject, schema, 'station_node', station_node_id)
        currentproject.execute("""delete from {model}.station
                                        where id={id}""".format(model=schema, id=id))

    @staticmethod
    def delete_domain_2d(parent, currentproject, schema, id):
        elem2d_node_ids = [i for i, in currentproject.execute("""select id
                                                from {model}.elem_2d_node
                                                where domain_2d={id}""".format(model=schema, id=id)).fetchall()]
        for elem2d_node_id in elem2d_node_ids:
            DeleteTool.delete_node(parent, currentproject, schema, 'elem_2d_node', elem2d_node_id)
        currentproject.execute("""delete from {model}.domain_2d
                                        where id={id}""".format(model=schema, id=id))
