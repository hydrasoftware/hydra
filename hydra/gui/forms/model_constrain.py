# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import json
from qgis.PyQt import uic
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QWidget, QLineEdit, QComboBox, QHBoxLayout, QDoubleSpinBox, QLabel
from hydra.gui.base_dialog import BaseDialog, tr
from hydra.gui.widgets.combo_with_values import ComboWithValues
from hydra.gui.widgets.graph_widget import GraphWidget
from shapely.geometry import LineString, Point
from numpy import array

with open(os.path.join(os.path.dirname(__file__), "model_constrain_param.json")) as j:
    __param_constrain__ = json.load(j)

class ModelConstrainWidget(BaseDialog):
    def __init__(self, project, line, id_=None, parent=None):
        BaseDialog.__init__(self, project, parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "model_constrain.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.project = project
        model = project.get_current_model()

        self.graph_mnt = GraphWidget(self.graph_placeholder)

        self.create_cross_section.clicked.connect(self.__create_cross_section)

        self.links_gb.exclusive = True
        self.set_link_btn.clicked.connect(self.__set_links)

        self.list_constrain_type = [type for (type,) in self.project.execute("""select name from hydra.constrain_type""").fetchall()]
        self.list_constrain_type.append("vide")
        for type in self.list_constrain_type :
            vars(self)[type+"_radio"].clicked.connect(self.__change_constrain_type)

        self.combo_type_points = ComboWithValues(self.point_type_placeholder)
        type_points = self.project.execute("""select name, id from project.points_type order by name""").fetchall()
        self.combo_type_points.addItem("",-1)
        for type in type_points :
            self.combo_type_points.addItem(type[0],type[1])

        # @todo remove the snap if we can fix the snapper to have exact coordinates
        self.id, name, elem_length, constrain_type, link_attributes, points_xyz, points_xyz_proximity, self.line, line_xyz, comment = \
            project.execute("""
                insert into {model}.constrain(geom)
                values({geom})
                returning id, name, elem_length, constrain_type, link_attributes,  points_xyz, points_xyz_proximity, ST_AsText(geom), line_xyz, comment
                """.format(model=model.name, geom=model.make_line(line))).fetchone() \
            if id_ is None else \
            project.execute("""
                select id, name, elem_length, constrain_type, link_attributes, points_xyz, points_xyz_proximity, ST_AsText(geom), line_xyz, comment
                from {}.constrain where id={}
                """.format(model.name, str(id_))).fetchone()
        self.nameEdit.setText(name)
        self.notes.setText(comment)
        self.constrain_type = constrain_type
        if self.constrain_type:
            vars(self)[self.constrain_type + "_radio"].setChecked(True)
        else:
            self.vide_radio.setChecked(True)
        self.elem_length_spinbox.setValue(elem_length)
        self.combo_type_points.set_selected_value(points_xyz)
        self.proximity_spinbox.setValue(points_xyz_proximity)

        self.proximity_spinbox.valueChanged.connect(self.__updt_constrain_table)
        self.combo_type_points.currentIndexChanged.connect(self.__updt_constrain_table)

        idx = None
        self.terrain_line.addItem('', userData=None)
        for id_, name_, dist_ in project.execute(f"""
                select l.id, l.name, st_distance(c.geom, l.geom)
                from project.line_xyz l, {model.name}._constrain c
                where c.id = {self.id}
                order by l.geom <-> c.geom asc
                """).fetchall():
            self.terrain_line.addItem(name_, userData=id_)
            self.terrain_line.setItemData(self.terrain_line.count()-1, f"distance = {dist_:.0f}m", Qt.ToolTipRole)
            if line_xyz == id_:
                idx = self.terrain_line.count()-1

        self.terrain_line.setCurrentIndex(idx if idx is not None else 0)
        self.terrain_line.currentIndexChanged.connect(self.__render_mnt_graph)

        self.__generate_param_ui()
        self.__init_json_attribute(link_attributes)

        self.__change_constrain_type()
        self.__reload_3d_points_tab()

    def __reload_3d_points_tab(self):
        ''' reload the second tab widget if it's active tab'''
        self.__updt_constrain_table()
        self.__render_mnt_graph()

    def __init_json_attribute(self, json):
        if json:
            for box in json:
                vars(self)[box+"_layout"].itemAt(1).widget().setValue(json[box])

    def __create_cross_section(self):
        model = self.project.get_current_model().name
        line_id = self.terrain_line.itemData(self.terrain_line.currentIndex())
        if line_id is not None:
            z_invert, = self.project.execute(f"""
                select min(st_z(p.geom))
                from {model}.flood_plain_transect_candidate t
                join lateral st_dumppoints(t.topo) p on true
                where t.id = {line_id}
                """).fetchone()

            if z_invert is None:
                # the constrain has already been created
                return

            self.project.execute(f"""
                update {model}.constrain set line_xyz={line_id} where id={self.id}
                """)

            self.project.execute(f"""
                insert into {model}.valley_cross_section_geometry(name, zbmin_array, zbmaj_lbank_array, zbmaj_rbank_array, transect, sz_array)
                select t.name, '{{{{{z_invert}, 0}}}}'::real[], '{{{{{z_invert}, 0}}}}'::real[], '{{{{{z_invert}, 0}}}}'::real[],
                    c.id, array_agg(ARRAY[st_linelocatepoint(c.geom, p.geom)*st_length(c.geom), st_z(p.geom)] order by p.path)
                from {model}.flood_plain_transect_candidate t
                join {model}.constrain c on c.id = {self.id}
                join lateral st_dumppoints(c.topo) p on true
                where t.id = {line_id} and not exists (select 1 from {model}.valley_cross_section_geometry g where g.name=t.name)
                group by t.name, c.id
                --on conflict on constraint
                """)

            self.project.execute(f"""
                insert into {model}.river_node(geom)
                select distinct t.reach_intersection
                from {model}.flood_plain_transect_candidate t
                where t.id = {line_id} and not exists (select 1 from {model}.river_node n where st_dwithin(t.reach_intersection, n.geom, 1.))
                """)

            self.project.execute(f"""
                insert into {model}._river_cross_section_profile(name, id, type_cross_section_up, type_cross_section_down, down_vcs_geom, z_invert_down)
                select t.name, (select n.id from {model}.river_node n order by n.geom <-> t.reach_intersection limit 1) id, null, 'valley', g.id down_vcs_geom, {z_invert}
                from {model}.flood_plain_transect_candidate t
                join {model}.valley_cross_section_geometry g on g.name = t.name
                where not exists (select 1 from {model}.river_cross_section_profile p where st_dwithin(t.reach_intersection, p.geom, 1.))
                and t.id = {line_id}
                """)

            self.create_cross_section.setEnabled(False)


    def __set_links(self) :
        tab_links = self.project.execute("""select _link.id
        from {model}._link, {model}._constrain
        where St_intersects(_link.geom, _constrain.geom)
        and _constrain.id = {id}
        and _link.generated is not null""".format(model=self.project.get_current_model().name,id=self.id)).fetchall()

        for id in tab_links :
            self.project.execute("""select {model}.set_link_altitude_topo({id})""".format(model=self.project.get_current_model().name,id=id[0]))

    def __get_number_points_constrain(self):
        distance = self.proximity_spinbox.value()
        model = self.project.get_current_model().name
        type_point = self.combo_type_points.get_selected_value()
        nb_points, = self.project.execute(f"""
            select count(1)
            from {model}._constrain c
            join project.points_xyz p on st_dwithin(p.geom, c.geom, {distance}) and  {type_point or 'null'} = p.points_id
            where c.id = {self.id}
            """).fetchone()

        return nb_points

    def __render_mnt_graph(self):
        #self.graph_mnt.hide()
        self.graph_mnt.clear()
        model = self.project.get_current_model().name

        if self.project.terrain.has_data():
            cstr, = self.project.execute(f"""
                select array_agg(ARRAY[st_x(d.geom), st_y(d.geom)])
                from {model}._constrain c
                join lateral st_dumppoints(c.geom) d on true
                where c.id = {self.id}
                """).fetchone()
            elevation = self.project.terrain.line_elevation(cstr)
            line = LineString(cstr)
            pt = array([(line.project(Point(p)), p[2]) for p in elevation])
            self.graph_mnt.add_line(pt[:,0], pt[:,1],"#c3c3c3")

        line_id = self.terrain_line.itemData(self.terrain_line.currentIndex())
        self.create_cross_section.setEnabled(False)
        if line_id is not None:
            # test if candidate cross section exists
            ct, = self.project.execute(f"""
                select count(1) from {model}.flood_plain_transect_candidate where id = {line_id}
                """).fetchone()
            self.create_cross_section.setEnabled(ct != 0)
            line_profil = self.project.execute(f"""
                select  St_LineLocatePoint(c.geom, d.geom)*St_length(c.geom), st_z(d.geom)
                from project.line_xyz l
                join {model}._constrain c on c.id = {self.id}
                join lateral st_dumppoints(l.geom) d on true
                where l.id = {line_id}
                """).fetchall()
            self.graph_mnt.add_line([point[0] for point in line_profil],[point[1] for point in line_profil],"blue")
        elif self.__get_number_points_constrain() > 0:
            point_profil = self.project.execute(f"""
                select St_LineLocatePoint(c.geom, p.geom)*St_length(c.geom) s, p.z_ground
                from {model}._constrain c
                join project.points_xyz p on st_dwithin(p.geom, c.geom, c.points_xyz_proximity) and c.points_xyz = p.points_id
                where c.id = {self.id}
                order by s
                """).fetchall()

            self.graph_mnt.add_line([point[0] for point in point_profil],[point[1] for point in point_profil],"green",marker="|")

        if self.project.terrain.has_data() and self.__get_number_points_constrain()>0:
            #self.graph_mnt.show()
            self.graph_mnt.extend_zone()
            self.graph_mnt.render()

    def __generate_param_ui (self):
        for box in __param_constrain__ :
            cur_param = __param_constrain__[box]
            vars(self)[box +"_layout"] = QHBoxLayout()
            vars(self)[box +"_layout"].addWidget(QLabel(cur_param["name"]))
            spinbox = QDoubleSpinBox()
            spinbox.setRange(float(cur_param["min"]),float(cur_param["max"]))
            spinbox.setSingleStep(float(cur_param["step"]))
            spinbox.setValue(float(cur_param["default"]))
            spinbox.setDecimals(int(cur_param["prec"]))
            vars(self)[box +"_layout"].addWidget(spinbox)
            self.layout_param.addLayout(vars(self)[box +"_layout"])

    def __updt_constrain_table(self) :
        self.label_nb_points.setText(f"{self.__get_number_points_constrain()} points")

        type_point = self.combo_type_points.get_selected_value()
        if type_point is None or type_point < 1:
            type_point = 'null'

        self.project.execute("""update {model}.constrain
        set points_xyz = {points_value}, points_xyz_proximity = {proximity_value}
        where id ={id}""".format(model=self.project.get_current_model().name,
        points_value=type_point, proximity_value=self.proximity_spinbox.value(),id=self.id))

        self.set_link_btn.setEnabled(self.__get_number_points_constrain() > 0)
        self.__render_mnt_graph()

    def __change_constrain_type(self) :
        for type in self.list_constrain_type :
            if vars(self)[type+"_radio"].isChecked() :
                self.constrain_type = type if type != "vide" else None

        if self.constrain_type in ('flood_plain_transect', 'ignored_for_coverages'):
            self.create_cross_section.show()
            self.terrain_line.show()
            self.terrain_line_label.show()
        else:
            self.create_cross_section.hide()
            self.terrain_line.hide()
            self.terrain_line_label.hide()
            self.terrain_line.setCurrentIndex(0)

        for box in __param_constrain__ :
            show = False
            for type in __param_constrain__[box]["type"] :
                if self.constrain_type == type :
                    for i in range (vars(self)[box +"_layout"].count()) :
                        vars(self)[box +"_layout"].itemAt(i).widget().setVisible(True)
                    show = True
            if not show :
                for i in range (vars(self)[box +"_layout"].count()) :
                    vars(self)[box +"_layout"].itemAt(i).widget().setVisible(False)

    def __json_dico_attribute(self) :
        dico_json = {}
        for box in __param_constrain__ :
            for type in __param_constrain__[box]["type"] :
                if type == self.constrain_type :
                    dico_json[box] = vars(self)[box+"_layout"].itemAt(1).widget().value()
        return dico_json

    def save(self):
        dico_json = self.__json_dico_attribute()

        type_point = self.combo_type_points.get_selected_value()
        if (type_point is not None and type_point < 1):
            type_point = None

        self.project.execute("""
            update {}.constrain set
            name = %s,
            elem_length = %s,
            constrain_type = %s,
            link_attributes = %s,
            points_xyz = %s,
            points_xyz_proximity = %s,
            line_xyz = %s,
            comment = %s
            where id = {}
            """.format(self.project.get_current_model().name, self.id), (
                self.nameEdit.text(),
                self.elem_length_spinbox.value(),
                self.constrain_type or None,
                json.dumps(dico_json) or None,
                type_point,
                self.proximity_spinbox.value(),
                self.terrain_line.itemData(self.terrain_line.currentIndex()),
                self.notes.text()
                ))

        BaseDialog.save(self)
        self.close()
