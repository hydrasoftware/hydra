# -*- coding: utf-8 -*-

"""
***************************************************************************
    d8flowpathextremeup.py
    ---------------------
    Date                 : January 2018
    Copyright            : (C) 2018-2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'January 2018'
__copyright__ = '(C) 2018-2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.core import (QgsVectorFileWriter,
                       QgsProcessing,
                       QgsProcessingException,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterBoolean,
                       QgsProcessingParameterRasterDestination
                      )

from .taudemAlgorithm import TauDemAlgorithm
from . import taudemUtils


class D8FlowPathExtremeUp(TauDemAlgorithm):

    D8_FLOWDIR = 'D8_FLOWDIR'
    SLOPE_AREA = 'SLOPE_AREA'
    OUTLETS = 'OUTLETS'
    MIN_UPSLOPE = 'MIN_UPSLOPE'
    EDGE_CONTAMINATION = 'EDGE_CONTAMINATION'
    EXTREME_VALUE = 'EXTREME_VALUE'

    def name(self):
        return 'd8flowpathextremeup'

    def displayName(self):
        return self.tr('D8 Extreme Upslope Value')

    def group(self):
        return self.tr('Stream network analysis')

    def groupId(self):
        return 'streamanalysis'

    def tags(self):
        return self.tr('dem,hydrology,d8,upslope,extreme').split(',')

    def shortHelpString(self):
        return self.tr('Evaluates the extreme (either maximum or minimum) '
                       'upslope value from an input grid based on the D8 flow '
                       'model.')

    def helpUrl(self):
        return 'http://hydrology.usu.edu/taudem/taudem5/help53/D8ExtremeUpslopeValue.html'

    def __init__(self):
        super().__init__()

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterRasterLayer(self.D8_FLOWDIR,
                                                            self.tr('D8 flow directions')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.SLOPE_AREA,
                                                            self.tr('Slope area')))
        self.addParameter(QgsProcessingParameterFeatureSource(self.OUTLETS,
                                                              self.tr('Outlets'),
                                                              types=[QgsProcessing.TypeVectorPoint],
                                                              defaultValue=None,
                                                              optional=True))
        self.addParameter(QgsProcessingParameterBoolean(self.MIN_UPSLOPE,
                                                        self.tr('Calculate minimum upslope value'),
                                                        defaultValue=False))
        self.addParameter(QgsProcessingParameterBoolean(self.EDGE_CONTAMINATION,
                                                        self.tr('Check for edge contamination'),
                                                        defaultValue=True))

        self.addParameter(QgsProcessingParameterRasterDestination(self.EXTREME_VALUE,
                                                                  self.tr('Extreme value')))

    def processAlgorithm(self, parameters, context, feedback):
        d8Layer = self.parameterAsRasterLayer(parameters, self.D8_FLOWDIR, context)
        if d8Layer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.D8_FLOWDIR))

        slopeLayer = self.parameterAsRasterLayer(parameters, self.SLOPE_AREA, context)
        if slopeLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.SLOPE_AREA))

        arguments = []
        arguments.append(self.command())

        arguments.append('-p')
        arguments.append(d8Layer.source())

        arguments.append('-sa')
        arguments.append(slopeLayer.source())

        if self.OUTLETS in parameters and parameters[self.OUTLETS] is not None:
            outlets = self.parameterAsCompatibleSourceLayerPath(parameters, self.OUTLETS, context,
                                                                QgsVectorFileWriter.supportedFormatExtensions(),
                                                                feedback=feedback)
            arguments.append('-o')
            arguments.append(outlets)

        if self.parameterAsBool(parameters, self.MIN_UPSLOPE, context):
            arguments.append('-min')

        if not self.parameterAsBool(parameters, self.EDGE_CONTAMINATION, context):
            arguments.append('-nc')

        arguments.append('-ssa')
        arguments.append(self.parameterAsOutputLayer(parameters, self.EXTREME_VALUE, context))

        taudemUtils.execute(arguments, feedback)

        return self.algorithmResults(parameters)
