# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if no queries make an error for empty project

USAGE

   python -m hydra.advanced_tools.pre_composed_queries_test [-dh]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name
        run in gui mode

"""

from hydra.utility.string import normalized_name, normalized_model_name
from hydra.project import Project
from hydra.model import Model
from hydra.advanced_tools.pre_composed_queries import PreComposedQueriesManager
from hydra.database.database import remove_project, TestProject, project_exists
from qgis.PyQt.QtCore import QCoreApplication, QTranslator
from qgis.PyQt.QtWidgets import QApplication
from hydra.utility.log import LogManager, ConsoleLogger
from os import listdir
from os.path import isfile, join
import getopt
import string
import sys, traceback
import os

current_dir = os.path.abspath(os.path.dirname(__file__))

def parse_query(fileName):
    with open(os.path.join(current_dir, 'queries', fileName)) as f:
        return f.read().split(";;")[:-1]

def gui_mode(project_name):
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)
    obj_project = Project.load_project(project_name)
    test_queries = PreComposedQueriesManager(obj_project)
    test_queries.exec_()

def test() :

    log_manager = LogManager(ConsoleLogger(), "Hydra")

    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)


    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
    if project_exists("queries_test"):
        remove_project("queries_test")

    substitutions = ['model', 'work_table', 'work_table_geom_column']

    project_name = "queries_test"
    test_project = TestProject(project_name)
    project = Project.load_project(project_name)

    project.add_new_model("model1")
    project.add_new_model("model2")

    project.execute(""" create table work.table_test as ( select 1234 as c1, 'try' as c2, ST_SetSRID(ST_MakePoint(1,2),2154) as geom) """)
    project.execute(""" insert into work.table_test (c1,c2,geom) values (12345,'trymore', ST_SetSRID(ST_MakePoint(5,3),2154))""")
    project.execute(""" insert into work.table_test (c1,c2,geom) values (666,'notry', ST_SetSRID(ST_MakePoint(1,2),2154)) """)
    project.execute(""" insert into model1.coverage (id,geom,domain_type) values (44, ST_GeomFromText('POLYGONZ((75.15 29.53 4,77 29 4,77.6 29.5 4,75.15 29.53 4))',2154), 'storage') """)
    project.execute(""" insert into model2.coverage (id,geom,domain_type) values (44, ST_GeomFromText('POLYGONZ((75.15 29.53 4,77 29 4,77.6 29.5 4,75.15 29.53 4))',2154), 'storage') """)
    project.execute(""" insert into model1.storage_node (id,geom,name,contour,zs_array) values (12, ST_SetSRID(ST_MakePoint(1,2,3),2154),'test_storage', 44, ARRAY[[1,2]]) """)
    project.execute(""" insert into model2.storage_node (id,geom,name,contour,zs_array) values (12, ST_SetSRID(ST_MakePoint(1,2,3),2154),'test_storage', 44, ARRAY[[1,2]]) """)


    sql_files = [f for f in listdir(join(current_dir,'queries')) if isfile(join(current_dir,'queries', f))]

    for f in sql_files :
        query_template = parse_query(f)
        substitutions = {"model":"model1","srid":project.srid,"work_table":"table_test","work_table_geom_column":"geom"}
        for query in query_template:
            project.execute(string.Template(query).substitute(substitutions))
        project.log.notice("Query {} run.".format(f))

    no_duplicate = project.execute("""select count(*) from work.table_test where geom = ST_SetSRID(ST_MakePoint(1,2),2154) ;""").fetchone()[0]
    # test to remove duplicate, NB : only remove one of two row with same geometry, if there is 3 row then there is still two duplicate in the table
    assert no_duplicate==1

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdg",
            ["help", "debug", "gui"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    project_name = normalized_name(project_name)
    gui_mode(project_name)
    exit(0)

debug = "-d" in optlist or "--debug" in optlist


test()

print("ok")
