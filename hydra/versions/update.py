################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
handles the maj on databases from version to version
"""

import os
import tempfile
import string
import json
import math
import string
from qgis.PyQt.QtCore import QCoreApplication, Qt
from qgis.PyQt.QtWidgets import QMessageBox, QProgressDialog
from hydra.database import database as dbhydra
from hydra.versions.dump_restore import temp_dump_project, autosave_project
from hydra.utility.sql_json import instances
from hydra.versions import version_tuple

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

class VersionError(Exception):
    pass


model_list_sql = "SELECT tables.table_schema AS name FROM information_schema.tables WHERE tables.table_name::name = '_singularity'::name"

def update_project(project):
    save_file = temp_dump_project(project.log, project.name)
    autosave_file = autosave_project(project.log, project.name, project.save_dir, project.version)

    current_version = project.get_version()
    target_version = dbhydra.data_version()

    while project.get_version() != target_version:
        __check_version(project)
        if version_tuple(project.get_version()) >= (3, 5):
            project.execute(f"""
            create or replace function project.drop_api()
            returns void language plpython3u security definer volatile as
            $$
                from hydra_{'_'.join(target_version.split('.'))} import drop_model_api, drop_project_api
                r = list(plpy.execute('select srid, version from hydra.metadata'))[0]
                srid, version = r['srid'], r['version']
                for r in plpy.execute("{model_list_sql}"):
                    for statement in drop_model_api(r['name']):
                        plpy.execute(statement)
                for statement in drop_project_api():
                    plpy.execute(statement)
            $$;
            select project.drop_api();
            """)
            project.commit()
        one_step_update(project)

    if version_tuple(target_version) >= (3,5):
        project.execute(f"""
        create or replace function project.refresh_api()
        returns void language plpython3u security definer volatile as
        $$
            from hydra_{'_'.join(target_version.split('.'))} import drop_model_api, drop_project_api, create_project_api, create_model_api
            r = list(plpy.execute('select srid, version from hydra.metadata'))[0]
            srid, version = r['srid'], r['version']
            for r in plpy.execute("{model_list_sql}"):
                for statement in drop_model_api(r['name']):
                    plpy.execute(statement)
            for statement in drop_project_api():
                plpy.execute(statement)
            for statement in create_project_api(srid, version):
                plpy.execute(statement)
            for r in plpy.execute("{model_list_sql}"):
                for statement in create_model_api(r['name'], srid, version):
                    plpy.execute(statement)
        $$;
        select project.refresh_api();
        """)
        project.commit()


def one_step_update(project):
    current_version = project.get_version()
    target_version = project.get_version_to_update()

    statements = []

    project.log.notice("Updating project {} from version {} to version {}".format(project.name, current_version, target_version))

    srid = project.srid
    substitutions = {"srid":str(srid), "version":target_version, "hydra":'hydra_'+'_'.join(target_version.split('.')), "workspace":'$workspace'}

    project_file = f"{current_version}-{target_version}.sql"
    model_file = f"{current_version}-{target_version}_model.sql"

    statements += __create_statements(project_file, substitutions)
    statements += [f"update hydra.metadata set version='{target_version}'"]

    for model, in project.execute(model_list_sql).fetchall():
        substitutions["model"] = model
        statements += __create_statements(model_file, substitutions)
        statements += [f"update {model}.metadata set version='{target_version}'"]

    __execute_statements(project, statements)

    project.log.notice("Updated project {} from version {} to version {}".format(project.name, current_version, target_version))


def __execute_statements(project, statements):
    # rearrange queries to run isolated ones first as they need specific transaction blocs and commits
    statements.sort(key=lambda x:(0 if '--isolated--' in x else 1))

    for i in range(len(statements)):
        if "--isolated--" in statements[i]:
            # carefull with use of this feature (commits previous queries. must be harmless for DB !)
            project.commit()
            project.execute_isolated(statements[i])
            project.commit()
        else:
            project.execute(statements[i])
    project.commit()

def __create_statements(file, subs={}):
    with open(os.path.join(os.path.dirname(__file__), file)) as f:
        # a = []
        # for s in f.read().split(";;")[:-1]:
        #     print(s)
        #     a.append(string.Template(s).substitute(subs))
        # return a
        return [string.Template(s).substitute(subs) for s in f.read().split(";;")[:-1]]

def __check_version(project):
    if project.get_version() == dbhydra.data_version():
        project.log.error(tr("""Selected project is in version {}.\nThis is already the last available data model version.\nNo update is needed.""".format(project.get_version())))
        raise VersionError("See hydra.log file for details")
    elif project.get_version_to_update() == "null":
        project.log.error(tr("""Selected project is in version {}.\nThis version does not support automatic update.\nPlease contact Hydra support.""".format(project.get_version())))
        raise VersionError("See hydra.log file for details")
    elif project.get_version_to_update() == 'unsupported':
        project.log.error(tr("""Selected project seems to be in version {}.\nThis version is not compatible with automatic update.\nPlease contact Hydra support.""".format(project.get_version())))
        raise VersionError("See hydra.log file for details")

if __name__ == '__main__':
    import sys
    from hydra.project import Project

    project = Project.load_project(sys.argv[1])
    update_project(project)
