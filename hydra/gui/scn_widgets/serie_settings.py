# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
from builtins import str
import os
import datetime
from operator import itemgetter
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QWidget, QGridLayout, QMessageBox
from qgis.PyQt.QtCore import QRegExp
from hydra.gui.widgets.combo_with_values import ComboWithValues
from hydra.gui.widgets.time_input_widget import TimeInputWidget
from hydra.gui.base_dialog import tr

class SerieSettingsWidget(QWidget):

    def __init__(self, current_project, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "serie_settings.ui"), self)
        tmplayout = QGridLayout()
        tmplayout.addWidget(self,0,0)
        parent.setLayout(tmplayout)

        self.project = current_project
        self.__current_id_serie = None
        self.__current_id_bloc = None
        self.__loading_chk = False

        self.bloc_item_list = [self.bloc_active_label,self.bloc_start_date]

        self.dt_hydrol_mn = TimeInputWidget(self.dt_hydrol_mn_placeholder)
        self.dt_hydrol_mn.set_format(hr=99, min=59, sec=59)

        self.dtmin_hydrau_hr = TimeInputWidget(self.dtmin_hydrau_hr_placeholder)
        self.dtmin_hydrau_hr.set_format(hr=99, min=59, sec=59)
        self.dtmin_hydrau_hr.setEnabled(False)
        self.dtmax_hydrau_hr = TimeInputWidget(self.dtmax_hydrau_hr_placeholder)
        self.dtmax_hydrau_hr.set_format(hr=99, min=59, sec=59)

        self.tini_hydrau_hr = TimeInputWidget(self.tini_hydrau_hr_placeholder)
        self.tini_hydrau_hr.set_format(hr=99, min=59, sec=-1)

        self.dt_output_hr = TimeInputWidget(self.dt_output_hr_placeholder)
        self.dt_output_hr.set_format(hr=99999, min=59, sec=-1)

        self.show()

    def set_serie(self, id_serie):
        if id_serie:
            self.__current_id_serie = id_serie
            comput_mode, dt_days, date0, tfin, dt_hydrol_mn, dtmin_hydrau_hr, dtmax_hydrau_hr, dzmin_hydrau, \
            dzmax_hydrau, dt_output_hr, scenario_ref, tini_hydrau_hr = self.project.execute("""
            select comput_mode, dt_days, date0, tfin, dt_hydrol_mn, dtmin_hydrau_hr, dtmax_hydrau_hr, dzmin_hydrau,
                dzmax_hydrau, dt_output_hr, scenario_ref, tini_hydrau_hr
            from project.serie
            where id={} limit 1""".format(str(id_serie))).fetchone()

            self.date0.setDate(date0)
            self.dt_days.setValue(dt_days)
            self.tfin.setValue(int(tfin.total_seconds() // 86400))
            self.dt_hydrol_mn.set_time(dt_hydrol_mn)
            self.dtmin_hydrau_hr.set_time(dtmin_hydrau_hr)
            self.dtmax_hydrau_hr.set_time(dtmax_hydrau_hr)
            self.dzmin_hydrau.setValue(dzmin_hydrau)
            self.dzmax_hydrau.setValue(dzmax_hydrau)
            self.dt_output_hr.set_time(dt_output_hr)
            self.tini_hydrau_hr.set_time(tini_hydrau_hr)

            self.set_comput_mode(comput_mode)

    def set_serie_duration_min(self, id_serie):
        date_min, = self.project.execute("""select max(t_till_start)
        from project.serie_bloc as b, project.serie as s
        where b.id_cs=s.id and s.id={id}""".format(id=id_serie)).fetchone()

        if date_min :
            self.min_date_label.setText("Last bloc start: {} days".format(date_min.days))
            self.tfin.setMinimum(date_min.days+1)
        else :
            self.min_date_label.setText("")
            self.tfin.setMinimum(0)

    def set_bloc(self, id_bloc):
        for item in self.bloc_item_list :
            item.setEnabled(True)
        if id_bloc :
            self.__current_id_bloc = id_bloc

            date0, tfin, t_till_start, name = self.project.execute(
            """ select s.date0, s.tfin, b.t_till_start, b.name
            from project.serie_bloc as b, project.serie as s
            where b.id ={id} and b.id_cs = s.id""".format(id=id_bloc)).fetchone()

            self.bloc_active_label.setText(name)
            self.bloc_start_date.setMinimumDate(date0)
            self.bloc_start_date.setMaximumDate(date0+tfin)
            self.bloc_start_date.setDate(date0+t_till_start)
            self.__current_id_bloc = id_bloc

            self.date0.setEnabled(False)
            self.tfin.setEnabled(False)


        else :
            self.__current_id_bloc = None
            self.bloc_active_label.setText('')
            for item in self.bloc_item_list :
                item.setEnabled(False)

            self.date0.setEnabled(True)
            self.tfin.setEnabled(True)

    def save(self):
        if self.__current_id_serie:
            if self.hydrology.isChecked():
                comput_mode = "hydrology"
            elif self.hydraulic.isChecked():
                comput_mode = "hydraulics"
            elif self.hydrologic_and_hydraulic.isChecked():
                comput_mode = "hydrology_and_hydraulics"

            self.project.execute("""
            update project.serie set
                comput_mode='{}',
                dt_days='{}',
                date0='{}', tfin='{}', dt_hydrol_mn='{}', dtmin_hydrau_hr='{}', dtmax_hydrau_hr='{}',
                dzmin_hydrau='{}', dzmax_hydrau='{}',
                dt_output_hr='{}', tini_hydrau_hr='{}'
            where id={}""".format(comput_mode, int(self.dt_days.value()), str(self.date0.date().toString("yyyy-MM-dd")), str(self.tfin.value())+" days", \
                self.dt_hydrol_mn.get_time(), self.dtmin_hydrau_hr.get_time(), self.dtmax_hydrau_hr.get_time(), \
                str(self.dzmin_hydrau.value()), str(self.dzmax_hydrau.value()), \
                self.dt_output_hr.get_time(), self.tini_hydrau_hr.get_time(),
                str(self.__current_id_serie)))

        if self.__current_id_bloc:
            try :
                self.project.execute("""
                update project.serie_bloc set
                    t_till_start='{}'
                where id={}""".format(str((self.bloc_start_date.date().toPyDate()-self.date0.date().toPyDate()).days)+" days",self.__current_id_bloc))
            except :
                QMessageBox.information(self, tr('Bloc starting at same date'), tr('there is already a bloc starting in : {} , please check your input.'.format(self.bloc_start_date.date())), QMessageBox.Ok)



    def set_comput_mode(self, comput_mode):
        if comput_mode=="hydrology":
            self.hydrology.setChecked(True)
        elif comput_mode=="hydraulics":
            self.hydraulic.setChecked(True)
        elif comput_mode=="hydrology_and_hydraulics":
            self.hydrologic_and_hydraulic.setChecked(True)
        else:
            raise "Unknown comput mode"