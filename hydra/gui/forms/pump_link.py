# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_link_dialog import SimpleLinkDialog, tr
import hydra.utility.string as string
from ..widgets.graph_widget import GraphWidget
from ..widgets.array_widget import ArrayWidget
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QTableWidgetItem
from functools import partial
import os

class PumpLinkEditor(SimpleLinkDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleLinkDialog.__init__(self, project, geom, "pump_link", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "pump_link.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        name, npump, zregul_array, hq_array, comment = self.project.execute("""
            select name, coalesce(npump, 0), zregul_array, hq_array, comment
            from {}.pump_link
            where id={}""".format(self.model.name, self.id)).fetchone()
        self.name.setText(name)
        self.notes.setText(comment)

        if npump is not None:
            self.npump.setValue(int(string.get_sql_float(npump)))

        if not zregul_array is None:
            self.set_table_items(zregul_array, self.table_zregul)

        self.table_zregul.currentCellChanged.connect(self.__zregul_current_cell_changed)

        self.npump.valueChanged.connect(self.__npump_changed)
        self.__npump_changed()

        self.graph = []
        self.array = []
        hq_array = [] if hq_array==None else hq_array

        for i in range(10):
            self.graph.append(GraphWidget(getattr(self, 'graph_placeholder_{}'.format(i))))
            if i >= npump:
                hq_array.append([])
            self.array.append(ArrayWidget(["H (m)", "Q (m3/s)"], [10,2], hq_array[i],
                draw_graph_func=partial(self.__draw_graph, i), help_func=None, parent=getattr(self, 'array_placeholder_{}'.format(i)), padding=True))

        for i in range(10):
            self.array[i].setDelegate("DoubleSpinBox", {'decimals': 6})

    def __draw_graph(self, i, datas):
        if len(datas)>0:
            self.graph[i].clear()
            self.graph[i].add_line(
                [r[1] for r in datas],
                [r[0] for r in datas],
                "r",
                "PUMP_{}".format(i+1),
                tr("Q (m3/s)"), tr("H (m)"))
        else:
            self.graph[i].canvas.setVisible(False)

    def __npump_changed(self):
        self.table_zregul.setRowCount(self.npump.value())

    def __zregul_current_cell_changed(self, row, column):
        self.stacked_pumps.setCurrentIndex(row)

    def set_table_items(self, array, table):
        n = len(array)
        table.setRowCount(n+1)
        effective_rows = 0
        for row in range(0, n):
            if array[row] != [None,None]:
                table.setItem(effective_rows,0,QTableWidgetItem(string.get_str(array[row][0])))
                table.setItem(effective_rows,1,QTableWidgetItem(string.get_str(array[row][1])))
                effective_rows = effective_rows +1
        table.setRowCount(effective_rows+1)
        table.resizeColumnsToContents()

    def get_table_items(self, table):
        n = table.rowCount()
        result = list()
        index_list=0
        for i in range(0, n):
            if table.item(i,0) is not None and table.item(i,1) is not None:
                result.append(list())
                m = table.columnCount()
                result[index_list].append(string.get_sql_float(table.item(i,0).text()))
                result[index_list].append(string.get_sql_float(table.item(i,1).text()))
                index_list = index_list + 1
        return result

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))
        hq_array = []
        for i in range(self.npump.value()):
            hq_array.append(self.array[i].get_table_items())
        self.model.update_pump_link(self.id, self.geom, self.npump.value(), self.get_table_items(self.table_zregul), hq_array, self.name.text(), comment=self.notes.text())
        SimpleLinkDialog.save(self)
        self.close()
