/* *************************************************** */
/* Move river_nodes closer than 1m on reach            */
/* *************************************************** */

 with node_reach as (
        select n.id as node, r.id as reach, r.geom
        from $model.river_node as n, $model.reach as r
        where st_intersects(st_buffer(n.geom, 1), r.geom)
          and n.reach is null
        )
update $model.river_node as r
  set geom = st_snap(r.geom, node_reach.geom, 1), reach=node_reach.reach
  from node_reach
  where r.reach is null
  and node_reach.node=r.id
;;