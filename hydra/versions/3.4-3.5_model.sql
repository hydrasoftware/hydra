/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update ${model}.metadata set version = '3.5';
;;

-- Adds attributes for freg/hydrass regulation modes

alter table ${model}._regul_gate_link add column hq_array real[10][2]
;;
alter table ${model}._regul_gate_link add column h_open real
;;
alter table ${model}._regul_gate_link add column h_close real
;;

alter table ${model}._regul_sluice_gate_singularity add column hq_array real[10][2]
;;
alter table ${model}._regul_sluice_gate_singularity add column h_open real
;;
alter table ${model}._regul_sluice_gate_singularity add column h_close real
;;

-- Adds comment to all entities

alter table $model._node add column comment varchar
;;
alter table $model._link add column comment varchar
;;
alter table $model._singularity add column comment varchar
;;
alter table $model._river_cross_section_profile add column comment varchar
;;
alter table $model._constrain add column comment varchar
;;
alter table $model.station add column comment varchar
;;
alter table $model.street add column comment varchar
;;
alter table $model.reach add column comment varchar
;;



update $model._link l set comment=p.comment from $model._pipe_link p where p.id = l.id
;;
drop materialized view if exists $model.results_pipe_hydrol
;;
drop materialized view if exists $model.results_pipe
;;
drop view if exists $model.pipe_link
;;
alter table $model._pipe_link drop column comment
;;
update $model._link l set comment=p.comment from $model._jet_fan_link p where p.id = l.id
;;
drop view if exists $model.jet_fan_link
;;
alter table $model._jet_fan_link drop column comment
;;
update $model._link l set comment=p.comment from $model._air_duct_link p where p.id = l.id
;;
drop view if exists $model.air_duct_link
;;
alter table $model._air_duct_link drop column comment
;;
update $model._link l set comment=p.comment from $model._air_headloss_link p where p.id = l.id
;;
drop view if exists $model.air_headloss_link
;;
alter table $model._air_headloss_link drop column comment
;;
update $model._link l set comment=p.comment from $model._ventilator_link p where p.id = l.id
;;
drop view if exists $model.ventilator_link
;;
alter table $model._ventilator_link drop column comment
;;


update $model._singularity l set comment=p.comment from $model._marker_singularity p where p.id = l.id
;;
drop view if exists $model.marker_singularity
;;
alter table $model._marker_singularity drop column comment
;;

