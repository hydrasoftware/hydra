# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
import os
import re
import unicodedata
import webbrowser
from qgis.PyQt import uic, QtGui, QtCore
from qgis.PyQt.QtWidgets import QTableWidgetItem, QFileDialog, QMessageBox
import hydra.utility.string as string
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from hydra.gui.base_dialog import BaseDialog, tr
from hydra.gui.widgets.scroll_log_widget import ScrollLogger
from hydra.advanced_tools.writer_crgeng_file import WriterCrgeng
from hydra.utility.process import ExecRunner, HydraExecError
from hydra.utility.system import open_external


class CrgengError(Exception):
    pass

class Crgeng(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        self.current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(self.current_dir, "crgeng.ui"), self)
        self.project = project

        self.table_control_files = HydraTableWidget(project,
            ("id", "ctrl_file"),
            (tr("Id"), tr("Control File")),
            "project", "crgeng",
            (self.new_control_file, self.delete_control_file, self.edit_control_file, self.help_control_file), "", "id",
            self.table_control_files_placeholder, showEdit=True, showHelp=True, file_columns=[1])
        self.table_control_files.set_editable(False)
        self.table_control_files.table.itemSelectionChanged.connect(self.__selection_changed)

        self.logger = ScrollLogger(self.logger_placeholder, night_mode=True)
        self.logger.setFixedSize(400, 400)

        self.go.clicked.connect(self.run)
        self.generate_file_btn.clicked.connect(self.__generate_file)
        self.log_btn.clicked.connect(self.open_log_file)
        self.buttonBox.accepted.connect(self.save)

    def __selection_changed(self):
        self.log_btn.setEnabled(False)
        items = self.table_control_files.table.selectedItems()
        if len(items) > 0:
            selected_row = self.table_control_files.table.row(items[0])
            file = self.table_control_files.table.item(selected_row, 1).text()
            directory = os.path.dirname(file)
            log_file = os.path.abspath(os.path.join(directory, 'crgeng.log'))

            self.log_btn.setEnabled(os.path.isfile(log_file))

    def __add_file(self, file_url):
        if file_url:
            self.project.execute("""delete from project.crgeng where ctrl_file = '{}';""".format(self.project.pack_path(file_url)))
            self.project.execute("""insert into project.crgeng(ctrl_file) values('{}');""".format(self.project.pack_path(file_url)))
            self.table_control_files.update_data()

    def __generate_file(self):
        dialog = WriterCrgeng(self.project)
        dialog.signal_file.connect(self.__add_file)
        dialog.exec_()

    def new_control_file(self):
        file_url, __ = QFileDialog.getSaveFileName(self, tr('Select crgeng control file'), self.project.data_dir, options=QFileDialog.DontConfirmOverwrite)
        if file_url:
            # creates file empty if not exists
            open(file_url, 'a').close()
            self.__add_file(file_url)

    def delete_control_file(self):
        items = self.table_control_files.table.selectedItems()
        if len(items) > 0:
            selected_row = self.table_control_files.table.row(items[0])
            id = int(self.table_control_files.table.item(selected_row, 0).text())
            self.project.execute("""delete from project.crgeng where id={};""".format(id))
            self.table_control_files.update_data()

    def edit_control_file(self):
        items = self.table_control_files.table.selectedItems()
        if len(items) > 0:
            file = items[1].text()
            open_external(file)

    def help_control_file(self):
        webbrowser.open('http://hydra-software.net/docs/notes_techniques/NT51.pdf', new=0)

    def __output_log(self, text):
        ''' add line for crgeng output logging'''
        text = '\n'.join(re.split('\n+', text.strip()))
        keep_words = ['traitement', 'generation', '*']
        if text:
            if "output:" in text:
                self.logger.new_label(20)
                self.logger.add_line(text, None, color='#ffffff')
            elif any(words in text for words in keep_words):
                self.logger.reset_current_label()
                self.logger.new_label()
                self.logger.add_line(text, 'bold', color='#ffffff')
            else:
                self.logger.add_line(text, None, color='#ffffff')

    def run(self):
        exe = os.path.abspath(os.path.join(self.current_dir, "crgeng.exe"))

        if not os.path.isfile(exe):
            raise CrgengError("Executable not found: {}".format(exe))

        items = self.table_control_files.table.selectedItems()
        if len(items) > 0:
            self.logger.clear()
            selected_row = self.table_control_files.table.row(items[0])
            file = self.table_control_files.table.item(selected_row, 1).text()
            outfile = os.path.join(os.path.dirname(file), 'crgeng')
            runner = ExecRunner([exe], log=self.project.log)
            runner.output.connect(self.__output_log)

            try:
                runner.run_cmd_outfile(arguments=[file], file=outfile, directory=os.path.dirname(file))
                QMessageBox.information(self, tr('Crgeng successful'), tr("""Crgeng run successfully with control file {}.""").format(file), QMessageBox.Ok)
            except HydraExecError as error:
                QMessageBox.critical(self, tr('Crgeng error'), tr("""An error occured during crgeng with file {}.\nSee hydra.log file for crgeng logs.\n""").format(file), QMessageBox.Ok)

    def open_log_file(self):
        items = self.table_control_files.table.selectedItems()
        if len(items) > 0:
            selected_row = self.table_control_files.table.row(items[0])
            file = self.table_control_files.table.item(selected_row, 1).text()
            directory = os.path.dirname(file)
            log_file = os.path.abspath(os.path.join(directory, 'crgeng.log'))
            if os.path.isfile(log_file):
                open_external(log_file)

    def save(self):
        BaseDialog.save(self)
        self.close()