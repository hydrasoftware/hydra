from builtins import str
from builtins import range
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from qgis.core import QgsPointXY, QgsRectangle
from operator import itemgetter
from qgis.PyQt.QtWidgets import QDialog, QTableWidgetItem, QApplication
from hydra.gui.widgets.combo_with_values import ComboWithValues
from hydra.utility.system import open_external
from qgis.PyQt import uic
import os
import re


class RegulationTool(QDialog):
    def __init__(self, current_project, iface, parent=None):
        QDialog.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "regulation.ui"), self)
        self.project = current_project
        self.iface = iface

        # Scenarios combobox
        self.scn_combo = ComboWithValues(self.scn_combo_placeholder)
        self.scn_combo.clear()
        scn_id_name = self.project.get_scenarios()
        for scn in sorted(scn_id_name, key=itemgetter(1)):
            self.scn_combo.addItem(scn[1], scn[0])
        self.scn_combo.addItem("All Scenarios", 0)
        if self.project.get_current_scenario():
            self.scn_combo.set_selected_value(self.project.get_current_scenario()[0])
        else:
            self.scn_combo.set_selected_value(0)

        self.scn_combo.currentIndexChanged.connect(self.refresh)

        self.buttonBox.accepted.connect(self.sav)
        self.buttonBox.rejected.connect(self.close)
        self.open_file_button.clicked.connect(self.open_file)
        self.zoom_button.clicked.connect(self.zoom)
        self.copy_button.clicked.connect(self.__copy)

        self.refresh()

    def open_file(self):
        ''' open file for the selected item in the list'''
        if self.regulation_table.currentRow() != -1:
            file = self.regulation_table.item(self.regulation_table.currentRow(), 2).text()

            for reg_file in self.reg_files:
                if file in reg_file and os.path.isfile(self.project.unpack_path(reg_file)):
                    open_external(self.project.unpack_path(reg_file))

    def refresh(self):
        self.regulation_table.setRowCount(0)
        self.reg_files = []

        name_item_list = []
        self.regulation_table.setColumnCount(6)

        where_clause = "where scenario ='{}'".format(self.scn_combo.currentText()) if self.scn_combo.get_selected_value() != 0 else ""

        for model in self.project.get_models():

            fetch = self.project.execute("""select file, iline, subtype, name, scenario from
            {model}.regulated_detailed {where} order by name, iline""".format(
                model=model,
                where=where_clause
            )).fetchall()

            for row in fetch:
                filename = os.path.basename(row[0])
                nb_line = row[1]
                type_item = row[2]
                name_item = row[3]
                scenario = row[4]
                if row[0] not in self.reg_files:
                    self.reg_files.append(row[0])

                row_position = self.regulation_table.rowCount()
                self.regulation_table.insertRow(row_position)
                self.regulation_table.setItem(row_position, 0, QTableWidgetItem(name_item))
                self.regulation_table.setItem(row_position, 1, QTableWidgetItem(type_item))
                self.regulation_table.setItem(row_position, 2, QTableWidgetItem(filename))
                self.regulation_table.setItem(row_position, 3, QTableWidgetItem(str(nb_line)))
                self.regulation_table.setItem(row_position, 4, QTableWidgetItem(model))
                self.regulation_table.setItem(row_position, 5, QTableWidgetItem(scenario))
                name_item_list.append(name_item)

        regulated_elements = list(set(name_item_list))

        self.number_regulated.setText("Number of regulated object : {}".format(len(regulated_elements)))

    def zoom(self):
        if self.regulation_table.currentItem() is not None:
            type, geom = self.project.execute("""select ST_GeometryType(ST_Envelope(geom)), ST_AsText(ST_Envelope(geom))  from {s}.{t} where name='{n}'
                                            """.format(s=self.regulation_table.item(self.regulation_table.currentRow(), 4).text(),
                                                       t=self.regulation_table.item(self.regulation_table.currentRow(), 1).text().lower(),
                                                       n=self.regulation_table.item(self.regulation_table.currentRow(), 0).text())).fetchone()
            self.__zoom_canvas(type, geom)

    def __zoom_canvas(self, type, geom):
        if type == 'ST_Point':
            # geom = 'POINT(X Y)'
            x, y = geom[6:-1].split(' ')
            point1 = QgsPointXY(float(x) - 10, float(y) - 10)
            point2 = QgsPointXY(float(x) + 10, float(y) + 10)
        elif type == 'ST_Polygon':
            # geom = 'POLYGON((X1 Y1,X2 Y2,X3 Y3,X4 Y4))'
            x1, y1 = geom[9:-2].split(',')[0].split(' ')
            x2, y2 = geom[9:-2].split(',')[2].split(' ')
            point1 = QgsPointXY(float(x1), float(y1))
            point2 = QgsPointXY(float(x2), float(y2))
        if point1:
            extent = QgsRectangle(point1, point2)
            self.iface.mapCanvas().setExtent(extent)
            self.iface.mapCanvas().refresh()
            self.close()
        else:
            return

    def __copy(self):
        copied_text = "\t".join([str(self.regulation_table.horizontalHeaderItem(i).text()) for i in range(self.regulation_table.columnCount())]) + "\n"
        for i in range(self.regulation_table.rowCount()):
            for j in range(self.regulation_table.columnCount()):
                try:
                    copied_text += self.regulation_table.item(i, j).text() + "\t"
                except AttributeError:
                    # quand une case n'a jamais été initialisée
                    copied_text += "\t"
            copied_text = copied_text[:-1] + "\n"  # le [:-1] élimine le '\t' en trop
        copied_text = copied_text[:-1]  # le [:-1] élimine le '\n' en trop
        # enregistrement dans le clipboard
        QApplication.clipboard().setText(copied_text)

    def sav(self):
        self.close()