from fastapi import Header, FastAPI, HTTPException, Depends, Form, File, UploadFile
from fastapi.responses import FileResponse
from typing import List
from pydantic import BaseModel
import jwt
import requests
import psycopg2
import datetime
import os
from zipfile import ZipFile
import tempfile
import subprocess

import hydra.database.database
from .api_config import HYDRA_LICENSE_API_KEY, JWT_KEY, HYDRA_PUBLIC_SERVICE

app = FastAPI()

@app.post("/token")
def _token(username = Form(...), password = Form(...)):
    """validate user on licence.hydra-software.net and
    returns jwt token"""
    res = requests.post("https://licence.hydra-software.net/api/rpc/valid_user",
        #headers={'Accept': 'application/vnd.pgrst.object'},
        data={'username': username, 'password': password, 'key_': HYDRA_LICENSE_API_KEY})
    if res.status_code != 200:
        raise HTTPException(status_code=res.status_code, detail='licence server :'+res.text)
    #if not res.json()['valid_user']:
    if not res.json():#['valid_user']:
        raise HTTPException(status_code=400, detail="Incorrect username or password")

    # create database user if necessary
    with psycopg2.connect(service=HYDRA_PUBLIC_SERVICE, dbname='postgres') as con:
        cur = con.cursor()
        cur.execute("select count(1) from pg_catalog.pg_roles where rolname=%s", (username,))
        if cur.fetchone()[0] == 0:
            cur.execute(f'create user "{username}" with password %s', (password,))
        else:
            cur.execute("select count(1) from pg_authid where rolpassword='md5'||md5(%s||%s) and rolname=%s", (password, username, username))
            if cur.fetchone()[0] == 0:
                cur.execute(f'alter user "{username}" password %s', (password,))
        con.commit()

    encoded = jwt.encode({
        "user": username,
        "creation_time": datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')},
        JWT_KEY, algorithm="HS256")
    return {"access_token": encoded, "token_type": "bearer"}

def _user_from_jwt(authorization = Header(...)):
    """parses hearder to find authorization token
    returns identified user"""
    if authorization is None or not authorization.startswith('bearer'):
        raise HTTPException(status_code=400, detail="Invalid token")
    return jwt.decode(authorization.replace('bearer', '').strip(), JWT_KEY, algorithms="HS256")['user']

def _password_from_jwt(authorization = Header(...)):
    """parses hearder to find authorization token
    returns identified user"""
    if authorization is None or not authorization.startswith('bearer'):
        raise HTTPException(status_code=400, detail="Invalid token")
    return jwt.decode(authorization.replace('bearer', '').strip(), JWT_KEY, algorithms="HS256")


@app.get("/get_projects_list")
def _get_projects_list(user : str = Depends(_user_from_jwt)):
    with psycopg2.connect(service=HYDRA_PUBLIC_SERVICE) as con:
        cur = con.cursor()
        cur.execute("select up.project from api.user_project up where up.username = %s", (user,))
        projects = [p for p, in cur.fetchall()]
        return projects

@app.post("/create_project")
def _create_project(name: str, srid: int,
         debug: bool = False, version: str = hydra.database.database.data_version(),
         user : str = Depends(_user_from_jwt), template : str = None):
    with psycopg2.connect(service=HYDRA_PUBLIC_SERVICE) as con:
        con.set_isolation_level(0)
        cur = con.cursor()
        cur.execute("select 1 from api.user_project up where up.project = %s", (name,))
        res = cur.fetchone()
        if cur.fetchone() is not None:
            raise HTTPException(status_code=400, detail="project {} aready exists".format(name))
        cur.execute('create database "{}"'.format(name))
        cur.execute("insert into api.user_project(username, project) values (%s, %s)", (user, name))
        con.commit()

    with psycopg2.connect(service=HYDRA_PUBLIC_SERVICE, dbname=name) as con:
        cur = con.cursor()
        cur.execute("create extension postgis")
        cur.execute("create extension postgis_sfcgal")
        cur.execute("create extension plpython3u")
        cur.execute(f"create extension hydra with version '{version}'")
        cur.execute(f"insert into hydra.metadata(srid) values({srid})")
        #cur.execute('reassign owned by session_user to "{}"'.format(user))
        cur.execute('grant all on database {} to "{}"'.format(name, user))
        for schema in ('hydra', 'project', 'work'):
            cur.execute('grant all on schema {} to "{}"'.format(schema, user))
            cur.execute('grant all privileges on all tables in schema {} to "{}"'.format(schema, user))
            cur.execute('grant usage on all sequences in schema {} to "{}"'.format(schema, user))
            cur.execute('grant execute on all functions in schema {} to "{}"'.format(schema, user))
        con.commit()

    return {'message': '{} successfully created with EPSG:{}'.format(name, srid)}

@app.delete("/remove_project")
def _remove_project(name: str, user : str = Depends(_user_from_jwt)):
    with psycopg2.connect(service=HYDRA_PUBLIC_SERVICE) as con:
        con.set_isolation_level(0)
        cur = con.cursor()
        cur.execute("select 1 from api.user_project up where up.project = %s", (name,))
        if cur.fetchone() is None:
            raise HTTPException(status_code=400, detail="project {} does not exists".format(name))
        cur.execute("select pg_terminate_backend(pg_stat_activity.pid) \
            from pg_stat_activity \
            where pg_stat_activity.datname='"+name+"';")
        cur.execute("delete from api.user_project where username=%s and project=%s", (user, name))
        cur.execute('drop database if exists "{}"'.format(name))
        con.commit()

    return {'message': '{} successfully dropped'.format(name)}

@app.get("/project_exists")
def _project_exists(name: str, user : str = Depends(_user_from_jwt)):
    with psycopg2.connect(service=HYDRA_PUBLIC_SERVICE) as con:
        con.set_isolation_level(0)
        cur = con.cursor()
        cur.execute("select 1 from api.user_project up where up.project = %s", (name,))
        return cur.fetchone() is not None

@app.get("/get_available_hydra_versions")
def _get_available_hydra_versions(user : str = Depends(_user_from_jwt)):
    with psycopg2.connect(service=HYDRA_PUBLIC_SERVICE) as con:
        cur = con.cursor()
        cur.execute("select version from pg_available_extension_versions where name='hydra'")
        return [v for v, in cur.fetchall()]

@app.post("/create_db")
def _create_db(name: str, user : str = Depends(_user_from_jwt)):
    with psycopg2.connect(service=HYDRA_PUBLIC_SERVICE) as con:
        con.set_isolation_level(0)
        cur = con.cursor()
        cur.execute("select 1 from api.user_project up where up.project = %s", (name,))
        res = cur.fetchone()
        if cur.fetchone() is not None:
            raise HTTPException(status_code=400, detail="project {} aready exists".format(name))
        cur.execute('create database "{}"'.format(name))
        cur.execute("insert into api.user_project(username, project) values (%s, %s)", (user, name))
        con.commit()

    return {'message': '{} successfully created'.format(name)}

@app.post("/add_dem")
def _add_dem(name: str, zip_: UploadFile = File(...), user : str = Depends(_user_from_jwt)):
    dir_ = os.path.join(hydra.database.database.project_dir(name), 'terrain')
    if not os.path.exists(dir_):
        os.makedirs(dir_)
    saved_zip = os.path.join(dir_, 'uploaded.zip')
    open(saved_zip, 'wb').write(zip_.file.read())
    ZipFile(saved_zip, 'r').extractall(dir_)
    os.remove(saved_zip)
    return {'message': 'dem successfully added to {}'.format(name)}

@app.get("/dump_project")
async def _dump_project(name: str, user : str = Depends(_user_from_jwt)):
    dumpfile = os.path.join(tempfile.gettempdir(), name+'.sql')
    subprocess.run(['pg_dump', '-f', dumpfile, '-d', name])
    return FileResponse(dumpfile)
