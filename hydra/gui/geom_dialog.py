# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
base dialog with buttons
"""

from qgis.PyQt.QtWidgets import QDialog, QDialogButtonBox, QVBoxLayout, QMessageBox
from qgis.PyQt.QtCore import Qt
from hydra.gui.widgets.valley_geom_widget import ValleyGeomWidget
from hydra.gui.widgets.valley_geom_widget import dico_code_error as dico_error
from hydra.gui.widgets.channel_geom_widget import ChannelGeomWidget
from hydra.gui.widgets.pipe_geom_widget import PipeGeomWidget
from hydra.gui.base_dialog import tr

class GeomDialog(QDialog):
    def __init__(self, project, geom_id, geom_type):
        QDialog.__init__(self)

        self.project = project
        self.model = self.project.get_current_model()
        self.geom_id = geom_id
        self.canceled = False
        self.geom_type = geom_type

        self.setLayout(QVBoxLayout())
        if geom_type == 'pipe':
            self.widget = PipeGeomWidget(self.project)
            name, = self.project.execute("""select name
                                                from {}.closed_parametric_geometry
                                                where id={}""".format(self.model.name, self.geom_id)).fetchone()
        elif geom_type == 'channel':
            self.widget = ChannelGeomWidget(self.project)
            name, = self.project.execute("""select name
                                                from {}.open_parametric_geometry
                                                where id={}""".format(self.model.name, self.geom_id)).fetchone()
        elif geom_type == 'valley':
            self.widget = ValleyGeomWidget(self.project)
            name, = self.project.execute("""select name
                                                from {}.valley_cross_section_geometry
                                                where id={}""".format(self.model.name, self.geom_id)).fetchone()
        self.widget.set_items(self.geom_id)
        self.layout().addWidget(self.widget)

        box = QDialogButtonBox(QDialogButtonBox.Save, Qt.Horizontal)
        box.accepted.connect(self.click_ok)
        self.layout().addWidget(box)

        self.resize(712, 662)
        self.setWindowTitle(name)
        self.finished.connect(self.__finished)

    def click_ok(self):
        if self.geom_type == 'valley':
            error_code = self.widget.check_geom_validity()
            if error_code != 0:
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Information)
                msg.setWindowTitle(tr("Geometry is not valid"))
                msg.setText(tr("Entered valley geometry is not valid, click details below to show more information."))
                msg.setDetailedText(tr(dico_error[error_code]))
                msg.setStandardButtons(QMessageBox.Ok)
                msg.exec_()
                return
        self.accept()

    def __finished(self, ret=None):
        if ret == QDialog.Accepted:
            self.widget.save_geom(self.geom_id)
            if self.geom_type == 'valley':
                if self.widget.init_trigger_status :
                    self.project.execute("""select {model}.coverage_update()""".format(model=self.project.get_current_model().name))
                self.project.execute("""update {model}.metadata set trigger_coverage = {bool}""".format(model=self.project.get_current_model().name,bool=self.widget.init_trigger_status))
        else:
            if self.geom_type == 'valley':
                self.project.execute("""update {model}.metadata set trigger_coverage = {bool}""".format(model=self.project.get_current_model().name,bool=self.widget.init_trigger_status))
            self.canceled = True

