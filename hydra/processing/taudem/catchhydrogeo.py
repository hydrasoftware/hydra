# -*- coding: utf-8 -*-

"""
***************************************************************************
    catchhydrogeo.py
    ---------------------
    Date                 : April 2019
    Copyright            : (C) 2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'April 2019'
__copyright__ = '(C) 2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.core import (QgsProcessing,
                       QgsProcessingException,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterBoolean,
                       QgsProcessingParameterRasterDestination
                      )

from .taudemAlgorithm import TauDemAlgorithm
from . import taudemUtils


class catchhydrogeo(TauDemAlgorithm):

    HAND = 'HAND'
    CATCHMENT = 'CATCHMENT'
    CATCH_ID = 'CATCH_ID'
    DINF_SLOPE = 'DINF_SLOPE'
    STAGE = 'STAGE'
    HYDRO_PROPERTY = 'HYDRO_PROPERTY'

    def name(self):
        return 'catchhydrogeo'

    def displayName(self):
        return self.tr('Catchment hydraulic properties')

    def group(self):
        return self.tr('Basic grid analysis')

    def groupId(self):
        return 'basicanalysis'

    def tags(self):
        return self.tr('dem,hydrology,d-infinity,catchment area,HAND,properties').split(',')

    def shortHelpString(self):
        return self.tr('Calculates channel hydraulic properties based '
                       'on HAND (Height Above Nearest Drainage).')

    def helpUrl(self):
        return 'http://hydrology.usu.edu/taudem/taudem5/help53/D8ContributingArea.html'

    def __init__(self):
        super().__init__()

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterRasterLayer(self.HAND,
                                                            self.tr('HAND raster')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.CATCHMENT,
                                                            self.tr('Catchment area')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.DINF_SLOPE,
                                                            self.tr('D-infinity slope')))
        self.addParameter(QgsProcessingParameterFile(self.CATCH_ID,
                                                     'Catchment ID list',
                                                     QgsProcessingParameterFile.File,
                                                     'txt'))
        self.addParameter(QgsProcessingParameterFile(self.STAGE,
                                                     'Stage table',
                                                     QgsProcessingParameterFile.File,
                                                     'txt'))

        self.addParameter(QgsProcessingParameterFileDestination(self.HYDRO_PROPERTY,
                                                                self.tr('Hydraulic property'),
                                                                self.tr('Text files (*.txt)')))

    def processAlgorithm(self, parameters, context, feedback):
        handLayer = self.parameterAsRasterLayer(parameters, self.HAND, context)
        if handLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.HAND))

        catchmentLayer = self.parameterAsRasterLayer(parameters, self.CATCHMENT, context)
        if catchmentLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.CATCHMENT))

        slopeLayer = self.parameterAsRasterLayer(parameters, self.DINF_SLOPE, context)
        if slopeLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.DINF_SLOPE))

        arguments = []
        arguments.append(self.command())

        arguments.append('-hand')
        arguments.append(handLayer.source())
        arguments.append('-catch')
        arguments.append(catchmentLayer.source())
        arguments.append('-slp')
        arguments.append(slopeLayer.source())
        arguments.append('-catchlist')
        arguments.append(self.parameterAsFile(parameters, self.CATCH_ID, context))
        arguments.append('-h')
        arguments.append(self.parameterAsFile(parameters, self.STAGE, context))
        arguments.append('-table')
        arguments.append(self.parameterAsFileOutput(parameters, self.HYDRO_PROPERTY, context))

        taudemUtils.execute(arguments, feedback)

        return self.algorithmResults(parameters)
