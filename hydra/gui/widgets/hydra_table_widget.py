# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
from builtins import str
from builtins import range
from qgis.PyQt.QtWidgets import QTableWidgetItem, QWidget, QGridLayout, QItemDelegate, QLineEdit, QAbstractItemView
from qgis.PyQt import uic
from qgis.PyQt.QtCore import pyqtSignal, Qt
import os
import hydra.utility.string as string

class HydraTableWidget(QWidget):
    data_edited = pyqtSignal()
    data_updated = pyqtSignal()

    class DelegateLineEdit(QItemDelegate):
        def __init__(self, parent=None, maxlen=24):
            QItemDelegate.__init__(self, parent)
            self.maxlen = maxlen

        def createEditor(self, parent, option, index):
            line_edit = QLineEdit(parent)
            line_edit.setMaxLength(self.maxlen)
            return line_edit

    @staticmethod
    def __quote(value):
        if ((isinstance(value, str) or isinstance(value, str)) and len(value)==0):
            return "null"

        return "'"+value+"'" \
            if (isinstance(value, str) or isinstance(value, str))\
            and value != "null" and value != "default" and value[0] != "'"  else value


    def __init__(self, project, data_columns,
            title_columns, dbname, table_name, callbacks, filter="",
            key_id="id", parent=None, autoInit=True, showEdit=False, showHelp=False, showCopy=False, file_columns=[], ui_file="hydra_table_widget.ui", order_column=None):

        self.__loading = True
        QWidget.__init__(self, parent)

        if parent:
            tmplayout = QGridLayout(parent)
            tmplayout.addWidget(self)

        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, ui_file), self)

        self.__editable = False
        self.set_editable(self.__editable)

        self.__fixed_first_row = False
        self.set_fixed_first_row(self.__fixed_first_row)

        self.project = project
        self.data_cols = data_columns
        self.__dbname = dbname
        self.__table_name = table_name
        self.__filter = filter
        self.__key_id = key_id
        self.__file_columns = file_columns

        self.table.setColumnCount(0)
        self.table.setColumnCount(len(self.data_cols))
        self.table.setHorizontalHeaderLabels(title_columns)
        self.table.setSelectionMode(QAbstractItemView.SingleSelection)

        self.btnAdd.clicked.connect(callbacks[0])
        self.btnDel.clicked.connect(callbacks[1])

        if showEdit:
            self.btnEdit.clicked.connect(callbacks[2])
        self.btnEdit.setVisible(showEdit)

        if showHelp:
            self.btnHelp.clicked.connect(callbacks[3])
        self.btnHelp.setVisible(showHelp)

        if showCopy:
            self.btnCopy.clicked.connect(callbacks[4])
        self.btnCopy.setVisible(showCopy)

        self.table.resizeColumnsToContents()
        self.table.setColumnWidth(1, self.table.columnWidth(1) + 50)
        self.table.resizeRowsToContents()
        self.table.horizontalHeader().setStretchLastSection(True)
        self.table.itemChanged.connect(self.on_data_edited)

        self.__order_column = order_column
        if autoInit:
            self.update_data()

        #self.show()
        self.__loading = False

    def set_dbname(self, dbname):
        self.__dbname = dbname
        self.update_data()

    def set_tablename(self, tablename):
        self.__table_name = tablename
        self.update_data()

    def on_data_edited(self, new_value):
        if not self.__loading:
            #self.table.resizeColumnsToContents()
            self.table.horizontalHeader().setStretchLastSection(True)
            self.data_edited.emit()

    def set_editable(self, value):
        self.__editable = value
        if value:
            self.table.setSelectionBehavior(QAbstractItemView.SelectRows)
            self.table.setEditTriggers(QAbstractItemView.AllEditTriggers)
            row = self.table.rowCount()
            for i in range(row):
                for j in range(self.table.columnCount()):
                    item = self.table.item(i, j)
                    if item is None:
                        item = QTableWidgetItem("")
                        self.table.setItem(i, j, item)
                    item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsEditable)
            self.data_edited.connect(self.save_row)
        else:
            self.table.setSelectionBehavior(QAbstractItemView.SelectRows)
            self.table.setEditTriggers(QAbstractItemView.NoEditTriggers)


    def set_fixed_first_row(self, value):
        self.__fixed_first_row = value
        if self.table.rowCount() > 0:
            for j in range(self.table.columnCount()):
                if self.table.item(0,j) is not None:
                    if value:
                        self.table.item(0,j).setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
                    else:
                        self.table.item(0,j).setFlags(Qt.ItemIsEditable | Qt.ItemIsSelectable | Qt.ItemIsEnabled)

    @property
    def filter(self):
        return self.__filter

    @property
    def rowCount(self):
        return self.table.rowCount()

    def set_filter(self, filter):
        if (filter != self.__filter):
            self.__filter = filter
            self.update_data()

    def setDelegate(self, delegate, column, argmap={}):
        if delegate == "LineEdit":
            if column is not None:
                self.table.setItemDelegateForColumn(column, HydraTableWidget.DelegateLineEdit(parent=None, **argmap))
            else:
                self.table.setItemDelegate(HydraTableWidget.DelegateLineEdit(parent=None, **argmap))

    def add_row(self, new_row_data=(), additional_rows=None):
        additional_rows = "," + ",".join(additional_rows) if additional_rows is not None else ""

        if len(new_row_data)<len(self.data_cols):
            new_row_data = list(new_row_data)
            while len(new_row_data)<len(self.data_cols)-1:
                new_row_data.append('null')

        new_row_formated = []
        for i in range(len(new_row_data)):
            if i+1 in self.__file_columns:
                new_row_formated.append(self.project.pack_path(str(new_row_data[i])))
            else:
                new_row_formated.append(str(new_row_data[i]))

        self.project.execute("insert into {}.{} ({}) values ({});".format(self.__dbname, self.__table_name,
            ",".join([x for x in self.data_cols if x != self.__key_id]) + additional_rows,
            ",".join(HydraTableWidget.__quote(str(value)) for value in new_row_formated)))
        self.update_data()
        self.table.selectRow(self.table.rowCount()-1)

    def save_row(self):
        self.save_selected_row()

    def save_selected_row(self):
        items = self.table.selectedItems()
        if len(items)>0:
            selected_row = self.table.row(items[0])
            id = self.table.item(selected_row,0).text()
            data = list()
            for i in range(0, len(self.data_cols)):
                if not self.data_cols[i]==self.__key_id:
                    if string.isfloat(self.table.item(selected_row,i).text()):
                        value = string.get_sql_float(self.table.item(selected_row,i).text())
                    else:
                        value = self.table.item(selected_row,i).text()
                    if value is not None:
                        data.append("{}={}".format(self.data_cols[i], HydraTableWidget.__quote(self.project.pack_path(value) if i in self.__file_columns else str(value))))
            to_insert = ",".join(data)
            self.project.execute("update {}.{} set {} where {}={};".format(self.__dbname, self.__table_name, to_insert, self.__key_id, id))

    def del_selected_row(self):
        items = self.table.selectedItems()
        if len(items)>0:
            if self.table.currentRow() != 0 or not self.__fixed_first_row:
                for item in items:
                    selected_row = self.table.row(item)
                    id = self.table.item(selected_row,0).text()
                    self.project.execute("delete from {}.{} where {}={};".format(self.__dbname,
                        self.__table_name, self.__key_id, id))
                self.update_data()

    def to_str(self, value):
        if value is None:
            return ""
        elif isinstance(value, int):
            return str(value)
        elif isinstance(value, float):
            return str(value)
        elif type(value)!=str:
            return value.decode('utf8')
        else:
            return value

    def update_data(self, order_column=None):
        order_column = order_column or self.__order_column
        self.__loading = True
        datas = self.project.execute("select {} from {}.{} {} {};".format(",".join(self.data_cols), self.__dbname, self.__table_name, self.__filter, "order by {}".format(order_column if order_column else self.__key_id))).fetchall()

        self.table.setRowCount(0)
        for item in datas:
            row = self.table.rowCount()
            self.table.setRowCount(row + 1)
            for i in range(0, len(item)):
                new_item = QTableWidgetItem(self.to_str(self.project.unpack_path(item[i]) if i in self.__file_columns else item[i]))
                if i in self.__file_columns:
                    new_item.setToolTip(self.to_str(self.project.unpack_path(item[i])))
                self.table.setItem(row, i, new_item)
        #self.table.resizeColumnsToContents()
        #self.table.resizeRowsToContents()
        #self.table.horizontalHeader().setStretchLastSection(True)
        #self.set_fixed_first_row(self.__fixed_first_row)
        self.set_editable(self.__editable)
        self.data_updated.emit()
        self.__loading = False
