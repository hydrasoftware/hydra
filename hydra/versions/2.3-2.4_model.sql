/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update $model.metadata set version = '2.4';
;;

/* ********************************************** */
/*              Regulated items                   */
/* ********************************************** */

drop view if exists $model.regulated;
;;

drop view if exists $model.regulated_detailed;
;;

create or replace view $model.regulated as
    select distinct on (name)
        row_number() over () AS id,
        CONCAT(type, ':', r.id) as orig_id,
        r.type,
        r.subtype,
        r.name,
        r.geom
    from project.regulated() as r
    where model = '$model';
;;

create or replace view $model.regulated_detailed as
    select
        row_number() over() as id,
        r.scenario,
        r.file,
        r.iline,
        CONCAT(type, ':', r.id) as orig_id,
        r.type,
        r.subtype,
        r.name,
        r.geom
    from project.regulated() as r
    where model = '$model';
;;

/* ********************************************** */
/*              Boundary links update             */
/* ********************************************** */

create or replace function ${model}.create_boundary_links(up_type varchar, up_id integer, station_node_id integer)
returns integer
language plpython3u
as
$$$$
    import plpy
    from $hydra import create_boundary_links
    return create_boundary_links(plpy, up_type, up_id, station_node_id, '$model', $srid)
$$$$
;;

/* ********************************************** */
/*              Thiessen                          */
/* ********************************************** */

create or replace view ${model}.thiessen_rain_gage as
    with
        vertices as (select ST_Union(geom) as geom from project.rain_gage),
        extend as (select ST_Union(geom) as geom from ${model}.catchment),
        voronoi as (select ST_VoronoiPolygons(v.geom, 0, e.geom) as geom from vertices as v, extend as e),
        polygons as (select (ST_Dump(geom)).geom from voronoi)
    select rg.id, rg.name, ST_Intersection(p.geom, e.geom) as geom
    from project.rain_gage as rg, polygons as p, extend as e
    where ST_Contains(p.geom, rg.geom) and ST_intersects(p.geom, e.geom)
;;

/* ********************************************** */
/*  Street add width invert                       */
/* ********************************************** */

alter table ${model}.street add column if not exists width_invert real default null
;;

create or replace function ${model}.street_after_fct()
returns trigger
language plpgsql
as $$$$
    declare
        res integer;
    begin
        if new.width != 0 then
            if tg_op = 'INSERT' or (tg_op = 'UPDATE' and old.width=0) then
                perform $model.gen_cst_street(new.id);
            end if;
        end if;
        if tg_op != 'DELETE' then
            if new.width_invert = new.width then
                update $model.street
                set width_invert = null
                where id = old.id;
            end if;
        end if;

        perform $model.crossroad_update();

        return new;
    end;
$$$$
;;

drop view if exists $model.street_link;
;;

create or replace view $model.street_link as (
    with p as (select precision as prec from hydra.metadata),
         links as (
                select s.rk,
                    s.width,
                    s.width_invert,
                    ST_MakeLine(
                        lag(n.geom) over (partition by s.id order by ST_LineLocatePoint(s.geom, n.geom)),
                        n.geom
                        ) as geom
                from $model.street as s, $model.crossroad_node as n, p
                where ST_DWithin(s.geom, n.geom, p.prec)
                )
    select
        (row_number() OVER ())::integer + (( SELECT max(_link.id) FROM $model._link)) AS id,
        'LRUE'::text || ((row_number() OVER ())::integer + ( SELECT max(_link.id) FROM $model._link))::character varying::text AS name,
        u.id as up,
        d.id as down,
        width,
        width_invert,
        ST_Length(l.geom)::real as length,
        rk,
        l.geom
    from links as l
        join $model.crossroad_node as u on ST_Equals(ST_StartPoint(l.geom), u.geom)
        join $model.crossroad_node as d on ST_Equals(ST_EndPoint(l.geom), d.geom)
    where l.geom is not null and ST_IsValid(l.geom)
    order by id asc
    );
;;

/* ********************************************** */
/*  Storage add contraction_coef                  */
/* ********************************************** */

alter table ${model}._storage_node add column if not exists contraction_coef real default 1;
;;

drop view if exists ${model}.storage_node cascade;
;;

create or replace view ${model}.storage_node as
    select
        p.id,
        p.name,
        c.zs_array,
        c.zini,
        c.contour,
        c.contraction_coef,
        p.geom,
        p.configuration::character varying as configuration,
        p.generated,
        p.validity,
        p.configuration as configuration_json
    from ${model}._storage_node c, ${model}._node p
    where p.id = c.id;
;;

create or replace function ${model}.storage_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into ${model}._node(node_type, name, geom, configuration, generated)
                values('storage', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update ${model}._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'storage') where name = 'define_later' and id = id_;
            insert into ${model}._storage_node(id, node_type, zs_array, zini, contour, contraction_coef)
                values (id_, 'storage', new.zs_array, new.zini, coalesce(new.contour, (select id from ${model}.coverage where st_intersects(geom, new.geom))), coalesce(new.contraction_coef, 1));
            perform ${model}.add_configuration_fct(new.configuration::json, id_, 'storage_node');

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' then
                update ${model}._storage_node set contour=(select id from ${model}.coverage as c where ST_intersects(new.geom, c.geom)) where id=id_;
                if (select trigger_coverage from ${model}.metadata) then
                    update ${model}.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            update ${model}._node set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1) <= 10 ) and (array_length(zs_array, 1) >= 1) and (array_length(zs_array, 2) = 2) and (contraction_coef is not null) and (contraction_coef<=1) and (contraction_coef>=0) from  ${model}._storage_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.zs_array, new.zini, new.contraction_coef) is distinct from (old.zs_array, old.zini, old.contraction_coef)) then
                select is_switching from ${model}.config_switch into switching;
                if switching=false then
                    select name from ${model}.configuration as c, ${model}.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.zs_array, old.zini, old.contraction_coef) as o, (select new.zs_array, new.zini, new.contraction_coef) as n into new_config;
                        update ${model}._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.zs_array, new.zini, new.contraction_coef) n into new_config;
                        update ${model}._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update ${model}._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update ${model}._storage_node set zs_array=new.zs_array, zini=new.zini, contour=new.contour, contraction_coef=new.contraction_coef where id=old.id;
            perform ${model}.add_configuration_fct(new.configuration::json, old.id, 'storage_node');

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' then
                update ${model}._storage_node set contour=(select id from ${model}.coverage as c where ST_intersects(new.geom, c.geom)) where id=old.id;
                if (select trigger_coverage from ${model}.metadata) then
                    update ${model}.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
                    update ${model}.coverage as c set domain_type='storage' where st_intersects(new.geom, c.geom);
                end if;
            end if;

            update ${model}._node set validity = (select (zs_array is not null ) and (zini is not null) and (array_length(zs_array, 1) <= 10 ) and (array_length(zs_array, 1) >= 1) and (array_length(zs_array, 2) = 2) and (contraction_coef is not null) and (contraction_coef<=1) and (contraction_coef>=0) from  ${model}._storage_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            -- Lines to update specific nodes that works with associated contours
            if 'storage' = 'storage' and (select trigger_coverage from ${model}.metadata) then
                update ${model}.coverage as c set domain_type='2d' where st_intersects(old.geom, c.geom);
            end if;

            delete from project.interlink where (model_up='${model}' and node_up=old.id) or (model_down='${model}' and node_down=old.id);

            delete from ${model}._storage_node where id=old.id;
            delete from ${model}._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

create trigger ${model}_storage_node_trig
    instead of insert or update or delete on $model.storage_node
       for each row execute procedure ${model}.storage_node_fct()
;;

/* ********************************************** */
/*  Utility layers                                */
/* ********************************************** */

--invalid view
create or replace view ${model}.invalid as
 with node_invalidity_reason as (
         select new_1.id,
            (
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   (area > 0)   '::text
                end) ||
                case
                    when new_1.z_ground is not null then ''::text
                    else '   z_ground is not null   '::text
                end as reason
           from $model.manhole_hydrology_node new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((((((((((((((((((((((((
                case
                    when new_1.area_ha is not null then ''::text
                    else '   area_ha is not null   '::text
                end ||
                case
                    when new_1.area_ha > 0::double precision then ''::text
                    else '   area_ha>0   '::text
                end) ||
                case
                    when new_1.rl is not null then ''::text
                    else '   rl is not null   '::text
                end) ||
                case
                    when new_1.rl > 0::double precision then ''::text
                    else '   rl>0   '::text
                end) ||
                case
                    when new_1.slope is not null then ''::text
                    else '   slope is not null   '::text
                end) ||
                case
                    when new_1.c_imp is not null then ''::text
                    else '   c_imp is not null   '::text
                end) ||
                case
                    when new_1.c_imp >= 0::double precision then ''::text
                    else '   c_imp>=0   '::text
                end) ||
                case
                    when new_1.c_imp <= 1::double precision then ''::text
                    else '   c_imp<=1   '::text
                end) ||
                case
                    when new_1.netflow_type is not null then ''::text
                    else '   netflow_type is not null   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'constant_runoff'::hydra_netflow_type or new_1.constant_runoff is not null and new_1.constant_runoff >= 0::double precision and new_1.constant_runoff <= 1::double precision then ''::text
                    else '   netflow_type!=''constant_runoff'' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'horner'::hydra_netflow_type or new_1.horner_ini_loss_coef is not null and new_1.horner_ini_loss_coef >= 0::double precision then ''::text
                    else '   netflow_type!=''horner'' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'horner'::hydra_netflow_type or new_1.horner_recharge_coef is not null and new_1.horner_recharge_coef >= 0::double precision then ''::text
                    else '   netflow_type!=''horner'' or (horner_recharge_coef is not null and horner_recharge_coef>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'holtan'::hydra_netflow_type or new_1.holtan_sat_inf_rate_mmh is not null and new_1.holtan_sat_inf_rate_mmh >= 0::double precision then ''::text
                    else '   netflow_type!=''holtan'' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'holtan'::hydra_netflow_type or new_1.holtan_dry_inf_rate_mmh is not null and new_1.holtan_dry_inf_rate_mmh >= 0::double precision then ''::text
                    else '   netflow_type!=''holtan'' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'holtan'::hydra_netflow_type or new_1.holtan_soil_storage_cap_mm is not null and new_1.holtan_soil_storage_cap_mm >= 0::double precision then ''::text
                    else '   netflow_type!=''holtan'' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'scs'::hydra_netflow_type or new_1.scs_j_mm is not null and new_1.scs_j_mm >= 0::double precision then ''::text
                    else '   netflow_type!=''scs'' or (scs_j_mm is not null and scs_j_mm>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'scs'::hydra_netflow_type or new_1.scs_soil_drainage_time_day is not null and new_1.scs_soil_drainage_time_day >= 0::double precision then ''::text
                    else '   netflow_type!=''scs'' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'scs'::hydra_netflow_type or new_1.scs_rfu_mm is not null and new_1.scs_rfu_mm >= 0::double precision then ''::text
                    else '   netflow_type!=''scs'' or (scs_rfu_mm is not null and scs_rfu_mm>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'hydra'::hydra_netflow_type or new_1.hydra_surface_soil_storage_rfu_mm is not null and new_1.hydra_surface_soil_storage_rfu_mm >= 0::double precision then ''::text
                    else '   netflow_type!=''hydra'' or (hydra_surface_soil_storage_rfu_mm is not null and hydra_surface_soil_storage_rfu_mm>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'hydra'::hydra_netflow_type or new_1.hydra_inf_rate_f0_mm_day is not null and new_1.hydra_inf_rate_f0_mm_day >= 0::double precision then ''::text
                    else '   netflow_type!=''hydra'' or (hydra_inf_rate_f0_mm_day is not null and hydra_inf_rate_f0_mm_day>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'hydra'::hydra_netflow_type or new_1.hydra_int_soil_storage_j_mm is not null and new_1.hydra_int_soil_storage_j_mm >= 0::double precision then ''::text
                    else '   netflow_type!=''hydra'' or (hydra_int_soil_storage_j_mm is not null and hydra_int_soil_storage_j_mm>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'hydra'::hydra_netflow_type or new_1.hydra_soil_drainage_time_qres_day is not null and new_1.hydra_soil_drainage_time_qres_day >= 0::double precision then ''::text
                    else '   netflow_type!=''hydra'' or (hydra_soil_drainage_time_qres_day is not null and hydra_soil_drainage_time_qres_day>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'gr4'::hydra_netflow_type or new_1.gr4_k1 is not null and new_1.gr4_k1 >= 0::double precision then ''::text
                    else '   netflow_type!=''gr4'' or (gr4_k1 is not null and gr4_k1>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'gr4'::hydra_netflow_type or new_1.gr4_k2 is not null then ''::text
                    else '   netflow_type!=''gr4'' or (gr4_k2 is not null)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'gr4'::hydra_netflow_type or new_1.gr4_k3 is not null and new_1.gr4_k3 >= 0::double precision then ''::text
                    else '   netflow_type!=''gr4'' or (gr4_k3 is not null and gr4_k3>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'gr4'::hydra_netflow_type or new_1.gr4_k4 is not null and new_1.gr4_k4 >= 0::double precision then ''::text
                    else '   netflow_type!=''gr4'' or (gr4_k4 is not null and gr4_k4>=0)   '::text
                end) ||
                case
                    when new_1.runoff_type is not null or new_1.netflow_type = 'gr4'::hydra_netflow_type then ''::text
                    else '   runoff_type is not null or netflow_type=''gr4''   '::text
                end) ||
                case
                    when new_1.runoff_type <> 'socose'::hydra_runoff_type or new_1.socose_tc_mn is not null and new_1.socose_tc_mn > 0::double precision or new_1.netflow_type = 'gr4'::hydra_netflow_type then ''::text
                    else '   runoff_type!=''socose'' or (socose_tc_mn is not null and socose_tc_mn>0) or netflow_type=''gr4''   '::text
                end) ||
                case
                    when new_1.runoff_type <> 'socose'::hydra_runoff_type or new_1.socose_shape_param_beta is not null and new_1.socose_shape_param_beta >= 1::double precision and new_1.socose_shape_param_beta <= 6::double precision or new_1.netflow_type = 'gr4'::hydra_netflow_type then ''::text
                    else '   runoff_type!=''socose'' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6) or netflow_type=''gr4''   '::text
                end) ||
                case
                    when new_1.runoff_type <> 'define_k'::hydra_runoff_type or new_1.define_k_mn is not null and new_1.define_k_mn > 0::double precision or new_1.netflow_type = 'gr4'::hydra_netflow_type then ''::text
                    else '   runoff_type!=''define_k'' or (define_k_mn is not null and define_k_mn>0) or netflow_type=''gr4''   '::text
                end) ||
                case
                    when new_1.q_limit is not null then ''::text
                    else '   q_limit is not null   '::text
                end) ||
                case
                    when new_1.q0 is not null then ''::text
                    else '   q0 is not null   '::text
                end) ||
                case
                    when new_1.network_type is not null then ''::text
                    else '   network_type is not null   '::text
                end) ||
                case
                    when new_1.rural_land_use is null or new_1.rural_land_use >= 0::double precision then ''::text
                    else '   rural_land_use is null or rural_land_use>=0   '::text
                end) ||
                case
                    when new_1.industrial_land_use is null or new_1.industrial_land_use >= 0::double precision then ''::text
                    else '   industrial_land_use is null or industrial_land_use>=0   '::text
                end) ||
                case
                    when new_1.suburban_housing_land_use is null or new_1.suburban_housing_land_use >= 0::double precision then ''::text
                    else '   suburban_housing_land_use is null or suburban_housing_land_use>=0   '::text
                end) ||
                case
                    when new_1.dense_housing_land_use is null or new_1.dense_housing_land_use >= 0::double precision then ''::text
                    else '   dense_housing_land_use is null or dense_housing_land_use>=0   '::text
                end as reason
           from $model.catchment_node new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   area>0   '::text
                end) ||
                case
                    when new_1.zb is not null then ''::text
                    else '   zb is not null   '::text
                end) ||
                case
                    when new_1.rk is not null then ''::text
                    else '   rk is not null   '::text
                end) ||
                case
                    when new_1.rk > 0::double precision then ''::text
                    else '   rk>0   '::text
                end as reason
           from $model.elem_2d_node new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.z_ground is not null then ''::text
                    else '   z_ground is not null   '::text
                end ||
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end) ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   area>0   '::text
                end) ||
                case
                    when new_1.reach is not null then ''::text
                    else '   reach is not null   '::text
                end) ||
                case
                    when ( select not (exists ( select 1
                               from $model.station
                              where st_intersects(station.geom, new_1.geom)))) then ''::text
                    else '   (select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))   '::text
                end as reason
           from $model.river_node new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((((
                case
                    when new_1.connection_law is not null then ''::text
                    else '   connection_law is not null   '::text
                end ||
                case
                    when new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type or new_1.cover_diameter is not null then ''::text
                    else '   connection_law != ''manhole_cover'' or cover_diameter is not null   '::text
                end) ||
                case
                    when new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type or new_1.cover_diameter > 0::double precision then ''::text
                    else '   connection_law != ''manhole_cover'' or cover_diameter > 0   '::text
                end) ||
                case
                    when new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type or new_1.cover_critical_pressure is not null then ''::text
                    else '   connection_law != ''manhole_cover'' or cover_critical_pressure is not null   '::text
                end) ||
                case
                    when new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type or new_1.cover_critical_pressure > 0::double precision then ''::text
                    else '   connection_law != ''manhole_cover'' or cover_critical_pressure >0   '::text
                end) ||
                case
                    when new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type or new_1.inlet_width is not null then ''::text
                    else '   connection_law != ''drainage_inlet'' or inlet_width is not null   '::text
                end) ||
                case
                    when new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type or new_1.inlet_width > 0::double precision then ''::text
                    else '   connection_law != ''drainage_inlet'' or inlet_width >0   '::text
                end) ||
                case
                    when new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type or new_1.inlet_height is not null then ''::text
                    else '   connection_law != ''drainage_inlet'' or inlet_height is not null   '::text
                end) ||
                case
                    when new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type or new_1.inlet_height > 0::double precision then ''::text
                    else '   connection_law != ''drainage_inlet'' or inlet_height >0   '::text
                end) ||
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end) ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   area>0   '::text
                end) ||
                case
                    when new_1.z_ground is not null then ''::text
                    else '   z_ground is not null   '::text
                end) ||
                case
                    when ( select not (exists ( select 1
                               from $model.station
                              where st_intersects(station.geom, new_1.geom)))) then ''::text
                    else '   (select not exists(select 1 from $model.station where st_intersects(geom, new.geom)))   '::text
                end) ||
                case
                    when ( select (exists ( select 1
                               from $model.pipe_link
                              where pipe_link.up = new_1.id or pipe_link.down = new_1.id)) as "exists") then ''::text
                    else '   (select exists (select 1 from $model.pipe_link where up=new.id or down=new.id))   '::text
                end as reason
           from $model.manhole_node new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   area>0   '::text
                end) ||
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end) ||
                case
                    when new_1.station is not null then ''::text
                    else '   station is not null    '::text
                end) ||
                case
                    when ( select st_intersects(new_1.geom, station.geom) as st_intersects
                       from $model.station
                      where station.id = new_1.station) then ''::text
                    else '   (select st_intersects(new.geom, geom) from $model.station where id=new.station)   '::text
                end as reason
           from $model.station_node new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   (area > 0)   '::text
                end) ||
                case
                    when new_1.z_ground is not null then ''::text
                    else '   z_ground is not null   '::text
                end) ||
                case
                    when new_1.h is not null then ''::text
                    else '   h is not null   '::text
                end) ||
                case
                    when new_1.h >= 0::double precision then ''::text
                    else '   h>=0   '::text
                end as reason
           from $model.crossroad_node new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((
                case
                    when new_1.zs_array is not null then ''::text
                    else '   zs_array is not null    '::text
                end ||
                case
                    when new_1.zini is not null then ''::text
                    else '   zini is not null   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) <= 10 then ''::text
                    else '   array_length(zs_array, 1) <= 10    '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) >= 1 then ''::text
                    else '   array_length(zs_array, 1) >= 1   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 2) = 2 then ''::text
                    else '   array_length(zs_array, 2) = 2   '::text
                end) ||
                case
                    when new_1.contraction_coef is not null then ''::text
                    else '   contraction_coef is not null   '::text
                end) ||
                case
                    when new_1.contraction_coef <= 1::double precision then ''::text
                    else '   contraction_coef<=1   '::text
                end) ||
                case
                    when new_1.contraction_coef >= 0::double precision then ''::text
                    else '   contraction_coef>=0   '::text
                end as reason
           from $model.storage_node new_1
          where not new_1.validity
        ), link_invalidity_reason as (
         select new_1.id,
            (((
                case
                    when new_1.down_type = 'manhole_hydrology'::hydra_node_type or (new_1.down_type = any (array['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'storage'::hydra_node_type, 'crossroad'::hydra_node_type, 'elem_2d'::hydra_node_type])) and new_1.hydrograph is not null then ''::text
                    else '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'', ''storage'', ''crossroad'', ''elem_2d'') and (hydrograph is not null))   '::text
                end ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from $model.connector_hydrology_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((((((((((((((((
                case
                    when new_1.z_crest1 is not null then ''::text
                    else '   z_crest1 is not null   '::text
                end ||
                case
                    when new_1.width1 is not null then ''::text
                    else '   width1 is not null   '::text
                end) ||
                case
                    when new_1.width1 >= 0::double precision then ''::text
                    else '   width1>=0   '::text
                end) ||
                case
                    when new_1.z_crest2 is not null then ''::text
                    else '   z_crest2 is not null   '::text
                end) ||
                case
                    when new_1.z_crest2 >= new_1.z_crest1 then ''::text
                    else '   z_crest2>=z_crest1   '::text
                end) ||
                case
                    when new_1.width2 is not null then ''::text
                    else '   width2 is not null   '::text
                end) ||
                case
                    when new_1.width2 >= 0::double precision then ''::text
                    else '   width2>=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc>=0   '::text
                end) ||
                case
                    when new_1.lateral_contraction_coef is not null then ''::text
                    else '   lateral_contraction_coef is not null   '::text
                end) ||
                case
                    when new_1.lateral_contraction_coef <= 1::double precision then ''::text
                    else '   lateral_contraction_coef<=1   '::text
                end) ||
                case
                    when new_1.lateral_contraction_coef >= 0::double precision then ''::text
                    else '   lateral_contraction_coef>=0   '::text
                end) ||
                case
                    when new_1.break_mode is not null then ''::text
                    else '   break_mode is not null   '::text
                end) ||
                case
                    when new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode or new_1.z_break is not null then ''::text
                    else '   break_mode!=''zw_critical'' or z_break is not null   '::text
                end) ||
                case
                    when new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode or new_1.t_break is not null then ''::text
                    else '   break_mode!=''time_critical'' or t_break is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.width_breach is not null then ''::text
                    else '   break_mode=''none'' or width_breach is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.z_invert is not null then ''::text
                    else '   break_mode=''none'' or z_invert is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp is not null then ''::text
                    else '   break_mode=''none'' or grp is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp > 0 then ''::text
                    else '   break_mode=''none'' or grp>0   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp < 100 then ''::text
                    else '   break_mode=''none'' or grp<100   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.dt_fracw_array is not null then ''::text
                    else '   break_mode=''none'' or dt_fracw_array is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 1) <= 10 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 1) >= 1 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 2) = 2 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from $model.overflow_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((
                case
                    when new_1.z_overflow is not null then ''::text
                    else '   z_overflow is not null   '::text
                end ||
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end) ||
                case
                    when new_1.area >= 0::double precision then ''::text
                    else '   area>=0   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from $model.network_overflow_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((((((((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.z_ceiling is not null then ''::text
                    else '   z_ceiling is not null   '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc>=0   '::text
                end) ||
                case
                    when new_1.action_gate_type is not null then ''::text
                    else '   action_gate_type is not null   '::text
                end) ||
                case
                    when new_1.z_invert_stop is not null then ''::text
                    else '   z_invert_stop is not null   '::text
                end) ||
                case
                    when new_1.z_ceiling_stop is not null then ''::text
                    else '   z_ceiling_stop is not null   '::text
                end) ||
                case
                    when new_1.v_max_cms is not null then ''::text
                    else '   v_max_cms is not null   '::text
                end) ||
                case
                    when new_1.v_max_cms >= 0::double precision then ''::text
                    else '   v_max_cms>=0   '::text
                end) ||
                case
                    when new_1.dt_regul_hr is not null then ''::text
                    else '   dt_regul_hr is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_control_node is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_pid_array is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_tz_array is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 1) <= 10 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 1) >= 1 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 2) = 2 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or new_1.q_z_crit is not null then ''::text
                    else '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 1) <= 10 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 1) >= 1 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 2) = 2 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul or new_1.nr_z_gate is not null then ''::text
                    else '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                end) ||
                case
                    when new_1.cc_submerged > 0::double precision then ''::text
                    else '   cc_submerged >0   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from $model.regul_gate_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end) ||
                case
                    when new_1.transmitivity is not null then ''::text
                    else '   transmitivity is not null   '::text
                end) ||
                case
                    when new_1.transmitivity >= 0::double precision then ''::text
                    else '   transmitivity>=0   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from $model.porous_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((
                case
                    when new_1.z_crest1 is not null then ''::text
                    else '   z_crest1 is not null   '::text
                end ||
                case
                    when new_1.width1 is not null then ''::text
                    else '   width1 is not null   '::text
                end) ||
                case
                    when new_1.width1 > 0::double precision then ''::text
                    else '   width1>0   '::text
                end) ||
                case
                    when new_1.length is not null then ''::text
                    else '   length is not null   '::text
                end) ||
                case
                    when new_1.length > 0::double precision then ''::text
                    else '   length>0   '::text
                end) ||
                case
                    when new_1.rk is not null then ''::text
                    else '   rk is not null   '::text
                end) ||
                case
                    when new_1.rk >= 0::double precision then ''::text
                    else '   rk>=0   '::text
                end) ||
                case
                    when new_1.z_crest2 is not null then ''::text
                    else '   z_crest2 is not null   '::text
                end) ||
                case
                    when new_1.z_crest2 > new_1.z_crest1 then ''::text
                    else '   z_crest2>z_crest1   '::text
                end) ||
                case
                    when new_1.width2 is not null then ''::text
                    else '   width2 is not null   '::text
                end) ||
                case
                    when new_1.width2 > new_1.width1 then ''::text
                    else '   width2>width1   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from $model.strickler_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((
                case
                    when new_1.npump is not null then ''::text
                    else '   npump is not null   '::text
                end ||
                case
                    when new_1.npump <= 10 then ''::text
                    else '   npump<=10   '::text
                end) ||
                case
                    when new_1.npump >= 1 then ''::text
                    else '   npump>=1   '::text
                end) ||
                case
                    when new_1.zregul_array is not null then ''::text
                    else '   zregul_array is not null   '::text
                end) ||
                case
                    when new_1.hq_array is not null then ''::text
                    else '   hq_array is not null   '::text
                end) ||
                case
                    when array_length(new_1.zregul_array, 1) = new_1.npump then ''::text
                    else '   array_length(zregul_array, 1)=npump   '::text
                end) ||
                case
                    when array_length(new_1.zregul_array, 2) = 2 then ''::text
                    else '   array_length(zregul_array, 2)=2   '::text
                end) ||
                case
                    when array_length(new_1.hq_array, 1) = new_1.npump then ''::text
                    else '   array_length(hq_array, 1)=npump   '::text
                end) ||
                case
                    when array_length(new_1.hq_array, 2) <= 10 then ''::text
                    else '   array_length(hq_array, 2)<=10   '::text
                end) ||
                case
                    when array_length(new_1.hq_array, 2) >= 1 then ''::text
                    else '   array_length(hq_array, 2)>=1   '::text
                end) ||
                case
                    when array_length(new_1.hq_array, 3) = 2 then ''::text
                    else '   array_length(hq_array, 3)=2   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from $model.pump_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.lateral_contraction_coef is not null then ''::text
                    else '   lateral_contraction_coef is not null   '::text
                end) ||
                case
                    when new_1.lateral_contraction_coef <= 1::double precision then ''::text
                    else '   lateral_contraction_coef<=1   '::text
                end) ||
                case
                    when new_1.lateral_contraction_coef >= 0::double precision then ''::text
                    else '   lateral_contraction_coef>=0   '::text
                end) ||
                case
                    when st_npoints(new_1.border) = 2 then ''::text
                    else '   st_npoints(border)=2   '::text
                end) ||
                case
                    when st_isvalid(new_1.border) then ''::text
                    else '   st_isvalid(border)   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from $model.mesh_2d_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.z_ceiling is not null then ''::text
                    else '   z_ceiling is not null   '::text
                end) ||
                case
                    when new_1.z_ceiling > new_1.z_invert then ''::text
                    else '   z_ceiling>z_invert   '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc <=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc >=0   '::text
                end) ||
                case
                    when new_1.action_gate_type is not null then ''::text
                    else '   action_gate_type is not null   '::text
                end) ||
                case
                    when new_1.mode_valve is not null then ''::text
                    else '   mode_valve is not null   '::text
                end) ||
                case
                    when new_1.z_gate is not null then ''::text
                    else '   z_gate is not null   '::text
                end) ||
                case
                    when new_1.z_gate >= new_1.z_invert then ''::text
                    else '   z_gate>=z_invert   '::text
                end) ||
                case
                    when new_1.z_gate <= new_1.z_ceiling then ''::text
                    else '   z_gate<=z_ceiling   '::text
                end) ||
                case
                    when new_1.v_max_cms is not null then ''::text
                    else '   v_max_cms is not null   '::text
                end) ||
                case
                    when new_1.cc_submerged > 0::double precision then ''::text
                    else '   cc_submerged >0   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from $model.gate_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((
                case
                    when new_1.law_type is not null then ''::text
                    else '   law_type is not null   '::text
                end ||
                case
                    when new_1.param is not null then ''::text
                    else '   param is not null   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from $model.borda_headloss_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((((((((
                case
                    when new_1.z_invert_up is not null then ''::text
                    else '   z_invert_up is not null   '::text
                end ||
                case
                    when new_1.z_invert_down is not null then ''::text
                    else '   z_invert_down is not null   '::text
                end) ||
                case
                    when new_1.h_sable is not null then ''::text
                    else '   h_sable is not null   '::text
                end) ||
                case
                    when new_1.cross_section_type is not null then ''::text
                    else '   cross_section_type is not null   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'valley'::hydra_cross_section_type then ''::text
                    else '   cross_section_type not in (''valley'')   '::text
                end) ||
                case
                    when new_1.rk is not null then ''::text
                    else '   rk is not null   '::text
                end) ||
                case
                    when new_1.rk > 0::double precision then ''::text
                    else '   rk >0   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'circular'::hydra_cross_section_type or new_1.circular_diameter is not null and new_1.circular_diameter > 0::double precision then ''::text
                    else '   cross_section_type!=''circular'' or (circular_diameter is not null and circular_diameter > 0)   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type or new_1.ovoid_top_diameter is not null and new_1.ovoid_top_diameter > 0::double precision then ''::text
                    else '   cross_section_type!=''ovoid'' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type or new_1.ovoid_invert_diameter is not null and new_1.ovoid_invert_diameter > 0::double precision then ''::text
                    else '   cross_section_type!=''ovoid'' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type or new_1.ovoid_height is not null and new_1.ovoid_height > 0::double precision then ''::text
                    else '   cross_section_type!=''ovoid'' or (ovoid_height is not null and ovoid_height >0 )   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type or new_1.ovoid_height > ((new_1.ovoid_top_diameter + new_1.ovoid_invert_diameter) / 2::double precision) then ''::text
                    else '   cross_section_type!=''ovoid'' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'pipe'::hydra_cross_section_type or new_1.cp_geom is not null then ''::text
                    else '   cross_section_type!=''pipe'' or cp_geom is not null   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'channel'::hydra_cross_section_type or new_1.op_geom is not null then ''::text
                    else '   cross_section_type!=''channel'' or op_geom is not null   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from $model.pipe_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null    '::text
                end ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc>=0   '::text
                end) ||
                case
                    when new_1.v_max_cms is not null then ''::text
                    else '   v_max_cms is not null   '::text
                end) ||
                case
                    when new_1.v_max_cms >= 0::double precision then ''::text
                    else '   v_max_cms>=0   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from $model.weir_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((
                case
                    when new_1.cross_section is not null then ''::text
                    else '   cross_section is not null   '::text
                end ||
                case
                    when new_1.cross_section >= 0::double precision then ''::text
                    else '   cross_section>=0   '::text
                end) ||
                case
                    when new_1.length is not null then ''::text
                    else '   length is not null   '::text
                end) ||
                case
                    when new_1.length >= 0::double precision then ''::text
                    else '   length>=0   '::text
                end) ||
                case
                    when new_1.slope is not null then ''::text
                    else '   slope is not null   '::text
                end) ||
                case
                    when new_1.down_type = 'manhole_hydrology'::hydra_node_type or (new_1.down_type = any (array['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'crossroad'::hydra_node_type, 'storage'::hydra_node_type, 'elem_2d'::hydra_node_type])) and new_1.hydrograph is not null then ''::text
                    else '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'', ''crossroad'', ''storage'', ''elem_2d'') and (hydrograph is not null))   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from $model.routing_hydrology_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((
                case
                    when new_1.q_pump is not null then ''::text
                    else '   q_pump is not null   '::text
                end ||
                case
                    when new_1.q_pump >= 0::double precision then ''::text
                    else '   q_pump>= 0   '::text
                end) ||
                case
                    when new_1.qz_array is not null then ''::text
                    else '   qz_array is not null   '::text
                end) ||
                case
                    when array_length(new_1.qz_array, 1) <= 10 then ''::text
                    else '   array_length(qz_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.qz_array, 1) >= 1 then ''::text
                    else '   array_length(qz_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.qz_array, 2) = 2 then ''::text
                    else '   array_length(qz_array, 2)=2   '::text
                end) ||
                case
                    when ( select bool_and(data_set.bool) as bool_and
                       from ( select 0::double precision <= unnest(new_1.qz_array[:][1:1]) as bool) data_set) then ''::text
                    else '   (/*   qz_array: 0<=q   */ select bool_and(bool) from (select 0<=unnest(new.qz_array[:][1]) as bool) as data_set)   '::text
                end) ||
                case
                    when ( select bool_and(data_set.bool) as bool_and
                       from ( select unnest(new_1.qz_array[:][1:1]) <= 1::double precision as bool) data_set) then ''::text
                    else '   (/*   qz_array: q<=1   */ select bool_and(bool) from (select unnest(new.qz_array[:][1])<=1 as bool) as data_set)   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from $model.deriv_pump_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= '0'::numeric::double precision then ''::text
                    else '   width>=0.   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= '0'::numeric::double precision then ''::text
                    else '   cc>=0.   '::text
                end) ||
                case
                    when new_1.break_mode is not null then ''::text
                    else '   break_mode is not null   '::text
                end) ||
                case
                    when new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode or new_1.z_break is not null then ''::text
                    else '   break_mode!=''zw_critical'' or z_break is not null   '::text
                end) ||
                case
                    when new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode or new_1.t_break is not null then ''::text
                    else '   break_mode!=''time_critical'' or t_break is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp is not null then ''::text
                    else '   break_mode=''none'' or grp is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp > 0 then ''::text
                    else '   break_mode=''none'' or grp>0   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp < 100 then ''::text
                    else '   break_mode=''none'' or grp<100   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.dt_fracw_array is not null then ''::text
                    else '   break_mode=''none'' or dt_fracw_array is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 1) <= 10 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 1) >= 1 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 2) = 2 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from $model.fuse_spillway_link new_1
          where not new_1.validity
        ), singularity_invalidity_reason as (
         select new_1.id,
            (((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null    '::text
                end ||
                case
                    when new_1.z_regul is not null then ''::text
                    else '   z_regul is not null    '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc>=0   '::text
                end) ||
                case
                    when new_1.mode_regul is not null then ''::text
                    else '   mode_regul is not null   '::text
                end) ||
                case
                    when new_1.reoxy_law is not null then ''::text
                    else '   reoxy_law is not null   '::text
                end) ||
                case
                    when new_1.reoxy_param is not null then ''::text
                    else '   reoxy_param is not null   '::text
                end) ||
                case
                    when not $model.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from $model.zregul_weir_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((
                case
                    when new_1.z_weir is not null then ''::text
                    else '   z_weir is not null   '::text
                end ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= '0'::numeric::double precision then ''::text
                    else '   cc>=0.   '::text
                end as reason
           from $model.weir_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((
                case
                    when new_1.d_abutment_l is not null then ''::text
                    else '   d_abutment_l is not null   '::text
                end ||
                case
                    when new_1.d_abutment_r is not null then ''::text
                    else '   d_abutment_r is not null   '::text
                end) ||
                case
                    when new_1.abutment_type is not null then ''::text
                    else '   abutment_type is not null   '::text
                end) ||
                case
                    when new_1.zw_array is not null then ''::text
                    else '   zw_array is not null    '::text
                end) ||
                case
                    when new_1.z_ceiling is not null then ''::text
                    else '   z_ceiling is not null   '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 1) <= 10 then ''::text
                    else '   array_length(zw_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 1) >= 1 then ''::text
                    else '   array_length(zw_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 2) = 2 then ''::text
                    else '   array_length(zw_array, 2)=2   '::text
                end) ||
                case
                    when not $model.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from $model.bradley_headloss_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((
                case
                    when new_1.zq_array is not null then ''::text
                    else '   zq_array is not null   '::text
                end ||
                case
                    when array_length(new_1.zq_array, 1) <= 20 then ''::text
                    else '   array_length(zq_array, 1)<=20   '::text
                end) ||
                case
                    when array_length(new_1.zq_array, 1) >= 1 then ''::text
                    else '   array_length(zq_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zq_array, 2) = 2 then ''::text
                    else '   array_length(zq_array, 2)=2   '::text
                end as reason
           from $model.zq_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((
                case
                    when new_1.zs_array is not null then ''::text
                    else '   zs_array is not null    '::text
                end ||
                case
                    when new_1.zini is not null then ''::text
                    else '   zini is not null   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) <= 10 then ''::text
                    else '   array_length(zs_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) >= 1 then ''::text
                    else '   array_length(zs_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 2) = 2 then ''::text
                    else '   array_length(zs_array, 2)=2   '::text
                end) ||
                case
                    when new_1.treatment_mode is not null then ''::text
                    else '   treatment_mode is not null   '::text
                end as reason
           from $model.tank_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
                case
                    when new_1.q0 is not null then ''::text
                    else '   q0 is not null   '::text
                end as reason
           from $model.constant_inflow_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.z_ceiling is not null then ''::text
                    else '   z_ceiling is not null   '::text
                end) ||
                case
                    when new_1.z_ceiling > new_1.z_invert then ''::text
                    else '   z_ceiling>z_invert   '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc <=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc >=0   '::text
                end) ||
                case
                    when new_1.action_gate_type is not null then ''::text
                    else '   action_gate_type is not null   '::text
                end) ||
                case
                    when new_1.mode_valve is not null then ''::text
                    else '   mode_valve is not null   '::text
                end) ||
                case
                    when new_1.z_gate is not null then ''::text
                    else '   z_gate is not null   '::text
                end) ||
                case
                    when new_1.z_gate >= new_1.z_invert then ''::text
                    else '   z_gate>=z_invert   '::text
                end) ||
                case
                    when new_1.z_gate <= new_1.z_ceiling then ''::text
                    else '   z_gate<=z_ceiling   '::text
                end) ||
                case
                    when new_1.v_max_cms is not null then ''::text
                    else '   v_max_cms is not null   '::text
                end) ||
                case
                    when not $model.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end) ||
                case
                    when new_1.cc_submerged > 0::double precision then ''::text
                    else '   cc_submerged >0   '::text
                end as reason
           from $model.gate_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (
                case
                    when new_1.law_type is not null then ''::text
                    else '   law_type is not null   '::text
                end ||
                case
                    when new_1.param is not null then ''::text
                    else '   param is not null   '::text
                end) ||
                case
                    when not $model.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from $model.borda_headloss_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((
                case
                    when new_1.downstream is not null then ''::text
                    else '   downstream is not null   '::text
                end ||
                case
                    when new_1.split1 is not null then ''::text
                    else '   split1 is not null   '::text
                end) ||
                case
                    when new_1.downstream_param is not null then ''::text
                    else '   downstream_param is not null   '::text
                end) ||
                case
                    when new_1.split1_law is not null then ''::text
                    else '   split1_law is not null   '::text
                end) ||
                case
                    when new_1.split1_param is not null then ''::text
                    else '   split1_param is not null   '::text
                end) ||
                case
                    when new_1.split2 is null or new_1.split2_law is not null then ''::text
                    else '   split2 is null or split2_law is not null   '::text
                end) ||
                case
                    when new_1.split2 is null or new_1.split2_param is not null then ''::text
                    else '   split2 is null or split2_param is not null   '::text
                end as reason
           from $model.zq_split_hydrology_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((
                case
                    when new_1.cascade_mode is not null then ''::text
                    else '   cascade_mode is not null   '::text
                end ||
                case
                    when new_1.cascade_mode = 'hydrograph'::hydra_model_connect_mode or new_1.zq_array is not null then ''::text
                    else '   cascade_mode=''hydrograph'' or zq_array is not null   '::text
                end) ||
                case
                    when array_length(new_1.zq_array, 1) <= 20 then ''::text
                    else '   array_length(zq_array, 1)<=20   '::text
                end) ||
                case
                    when array_length(new_1.zq_array, 1) >= 1 then ''::text
                    else '   array_length(zq_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zq_array, 2) = 2 then ''::text
                    else '   array_length(zq_array, 2)=2   '::text
                end) ||
                case
                    when array_length(new_1.quality, 1) = 9 then ''::text
                    else '   array_length(quality, 1)=9   '::text
                end as reason
           from $model.model_connect_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((
                case
                    when new_1.storage_area is not null then ''::text
                    else '   storage_area is not null   '::text
                end ||
                case
                    when new_1.storage_area >= 0::double precision then ''::text
                    else '   storage_area>=0   '::text
                end) ||
                case
                    when new_1.constant_dry_flow is not null then ''::text
                    else '   constant_dry_flow is not null   '::text
                end) ||
                case
                    when new_1.constant_dry_flow >= 0::double precision then ''::text
                    else '   constant_dry_flow>=0   '::text
                end) ||
                case
                    when new_1.distrib_coef is null or new_1.distrib_coef >= 0::double precision then ''::text
                    else '   distrib_coef is null or distrib_coef>=0   '::text
                end) ||
                case
                    when new_1.lag_time_hr is null or new_1.lag_time_hr >= 0::double precision then ''::text
                    else '   lag_time_hr is null or (lag_time_hr>=0)   '::text
                end) ||
                case
                    when new_1.sector is null or new_1.distrib_coef is not null and new_1.lag_time_hr is not null then ''::text
                    else '   sector is null or (distrib_coef is not null and lag_time_hr is not null)   '::text
                end) ||
                case
                    when array_length(new_1.pollution_dryweather_runoff, 1) = 2 then ''::text
                    else '   array_length(pollution_dryweather_runoff, 1)=2   '::text
                end) ||
                case
                    when array_length(new_1.pollution_dryweather_runoff, 2) = 4 then ''::text
                    else '   array_length(pollution_dryweather_runoff, 2)=4   '::text
                end) ||
                case
                    when array_length(new_1.quality_dryweather_runoff, 1) = 2 then ''::text
                    else '   array_length(quality_dryweather_runoff, 1)=2   '::text
                end) ||
                case
                    when array_length(new_1.quality_dryweather_runoff, 2) = 9 then ''::text
                    else '   array_length(quality_dryweather_runoff, 2)=9   '::text
                end) ||
                case
                    when new_1.external_file_data or new_1.tq_array is not null then ''::text
                    else '   external_file_data or tq_array is not null   '::text
                end) ||
                case
                    when new_1.external_file_data or array_length(new_1.tq_array, 1) <= 10 then ''::text
                    else '   external_file_data or array_length(tq_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.external_file_data or array_length(new_1.tq_array, 1) >= 1 then ''::text
                    else '   external_file_data or array_length(tq_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.external_file_data or array_length(new_1.tq_array, 2) = 2 then ''::text
                    else '   external_file_data or array_length(tq_array, 2)=2   '::text
                end as reason
           from $model.hydrograph_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((
                case
                    when new_1.external_file_data or new_1.tz_array is not null then ''::text
                    else '   external_file_data or tz_array is not null   '::text
                end ||
                case
                    when new_1.external_file_data or array_length(new_1.tz_array, 1) <= 10 then ''::text
                    else '   external_file_data or array_length(tz_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.external_file_data or array_length(new_1.tz_array, 1) >= 1 then ''::text
                    else '   external_file_data or array_length(tz_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.external_file_data or array_length(new_1.tz_array, 2) = 2 then ''::text
                    else '   external_file_data or array_length(tz_array, 2)=2   '::text
                end as reason
           from $model.tz_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (
                case
                    when new_1.pk0_km is not null then ''::text
                    else '   pk0_km is not null   '::text
                end ||
                case
                    when new_1.dx is not null then ''::text
                    else '   dx is not null   '::text
                end) ||
                case
                    when new_1.dx >= 0.1::double precision then ''::text
                    else '   dx>=0.1   '::text
                end as reason
           from $model.pipe_branch_marker_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((
                case
                    when new_1.qq_array is not null then ''::text
                    else '   qq_array is not null   '::text
                end ||
                case
                    when new_1.downstream is not null then ''::text
                    else '   downstream is not null   '::text
                end) ||
                case
                    when new_1.split1 is not null then ''::text
                    else '   split1 is not null   '::text
                end) ||
                case
                    when array_length(new_1.qq_array, 1) <= 10 then ''::text
                    else '   array_length(qq_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.qq_array, 1) >= 1 then ''::text
                    else '   array_length(qq_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.split2 is not null and array_length(new_1.qq_array, 2) = 3 or new_1.split2 is null and array_length(new_1.qq_array, 2) = 2 then ''::text
                    else '   (split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2)   '::text
                end as reason
           from $model.qq_split_hydrology_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.z_ceiling is not null then ''::text
                    else '   z_ceiling is not null   '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width >=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc <=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc >=0   '::text
                end) ||
                case
                    when new_1.action_gate_type is not null then ''::text
                    else '   action_gate_type is not null   '::text
                end) ||
                case
                    when new_1.z_invert_stop is not null then ''::text
                    else '   z_invert_stop is not null   '::text
                end) ||
                case
                    when new_1.z_ceiling_stop is not null then ''::text
                    else '   z_ceiling_stop is not null   '::text
                end) ||
                case
                    when new_1.v_max_cms is not null then ''::text
                    else '   v_max_cms is not null   '::text
                end) ||
                case
                    when new_1.v_max_cms >= 0::double precision then ''::text
                    else '   v_max_cms>=0   '::text
                end) ||
                case
                    when new_1.dt_regul_hr is not null then ''::text
                    else '   dt_regul_hr is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_control_node is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_pid_array is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_tz_array is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 1) <= 10 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 1) >= 1 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 2) = 2 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or new_1.q_z_crit is not null then ''::text
                    else '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 1) <= 10 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 1) >= 1 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 2) = 2 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul or new_1.nr_z_gate is not null then ''::text
                    else '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                end) ||
                case
                    when new_1.cc_submerged > 0::double precision then ''::text
                    else '   cc_submerged >0   '::text
                end as reason
           from $model.regul_sluice_gate_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((
                case
                    when new_1.drainage is not null then ''::text
                    else '   drainage is not null   '::text
                end ||
                case
                    when new_1.overflow is not null then ''::text
                    else '   overflow is not null   '::text
                end) ||
                case
                    when new_1.q_drainage is not null then ''::text
                    else '   q_drainage is not null   '::text
                end) ||
                case
                    when new_1.q_drainage >= 0::double precision then ''::text
                    else '   q_drainage>=0   '::text
                end) ||
                case
                    when new_1.z_ini is not null then ''::text
                    else '   z_ini is not null   '::text
                end) ||
                case
                    when new_1.zs_array is not null then ''::text
                    else '   zs_array is not null    '::text
                end) ||
                case
                    when new_1.treatment_mode is not null then ''::text
                    else '   treatment_mode is not null   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) <= 10 then ''::text
                    else '   array_length(zs_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) >= 1 then ''::text
                    else '   array_length(zs_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 2) = 2 then ''::text
                    else '   array_length(zs_array, 2)=2   '::text
                end as reason
           from $model.reservoir_rs_hydrology_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((
                case
                    when new_1.drainage is not null then ''::text
                    else '   drainage is not null   '::text
                end ||
                case
                    when new_1.overflow is not null then ''::text
                    else '   overflow is not null   '::text
                end) ||
                case
                    when new_1.z_ini is not null then ''::text
                    else '   z_ini is not null   '::text
                end) ||
                case
                    when new_1.zr_sr_qf_qs_array is not null then ''::text
                    else '   zr_sr_qf_qs_array is not null    '::text
                end) ||
                case
                    when new_1.treatment_mode is not null then ''::text
                    else '   treatment_mode is not null   '::text
                end) ||
                case
                    when new_1.treatment_param is not null then ''::text
                    else '   treatment_param is not null   '::text
                end) ||
                case
                    when array_length(new_1.zr_sr_qf_qs_array, 1) <= 10 then ''::text
                    else '   array_length(zr_sr_qf_qs_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.zr_sr_qf_qs_array, 1) >= 1 then ''::text
                    else '   array_length(zr_sr_qf_qs_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zr_sr_qf_qs_array, 2) = 4 then ''::text
                    else '   array_length(zr_sr_qf_qs_array, 2)=4   '::text
                end as reason
           from $model.reservoir_rsp_hydrology_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((
                case
                    when new_1.l_road is not null then ''::text
                    else '   l_road is not null   '::text
                end ||
                case
                    when new_1.l_road >= 0::double precision then ''::text
                    else '   l_road>=0   '::text
                end) ||
                case
                    when new_1.z_road is not null then ''::text
                    else '   z_road is not null   '::text
                end) ||
                case
                    when new_1.zw_array is not null then ''::text
                    else '   zw_array is not null    '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 1) <= 10 then ''::text
                    else '   array_length(zw_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 1) >= 1 then ''::text
                    else '   array_length(zw_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 2) = 2 then ''::text
                    else '   array_length(zw_array, 2)=2   '::text
                end) ||
                case
                    when not $model.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from $model.bridge_headloss_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((
                case
                    when new_1.q_dz_array is not null then ''::text
                    else '   q_dz_array is not null   '::text
                end ||
                case
                    when array_length(new_1.q_dz_array, 1) <= 10 then ''::text
                    else '   array_length(q_dz_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.q_dz_array, 1) >= 1 then ''::text
                    else '   array_length(q_dz_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.q_dz_array, 2) = 2 then ''::text
                    else '   array_length(q_dz_array, 2)=2   '::text
                end) ||
                case
                    when new_1.q_dz_array[1][1] = 0::double precision then ''::text
                    else '   q_dz_array[1][1]=0   '::text
                end) ||
                case
                    when new_1.q_dz_array[1][2] = 0::double precision then ''::text
                    else '   q_dz_array[1][2]=0   '::text
                end) ||
                case
                    when not $model.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from $model.param_headloss_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.qz_array is not null then ''::text
                    else '   qz_array is not null   '::text
                end ||
                case
                    when array_length(new_1.qz_array, 1) <= 10 then ''::text
                    else '   array_length(qz_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.qz_array, 1) >= 1 then ''::text
                    else '   array_length(qz_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.qz_array, 2) = 2 then ''::text
                    else '   array_length(qz_array, 2)=2   '::text
                end) ||
                case
                    when not $model.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not $model.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from $model.hydraulic_cut_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.slope is not null then ''::text
                    else '   slope is not null   '::text
                end ||
                case
                    when new_1.k is not null then ''::text
                    else '   k is not null   '::text
                end) ||
                case
                    when new_1.k > 0::double precision then ''::text
                    else '   k>0   '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end as reason
           from $model.strickler_bc_singularity new_1
          where not new_1.validity
        )
    select CONCAT('node:', n.id) as
        id,
        validity,
        name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom,
        reason from $model._node as n join node_invalidity_reason as r on r.id=n.id
        where validity=FALSE
    union
    select CONCAT('link:', l.id) as
        id,
        validity,
        name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom,
        reason from $model._link as l join link_invalidity_reason as r on  r.id=l.id
        where validity=FALSE
    union
    select CONCAT('singularity:', s.id) as
        id,
        s.validity,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        reason from $model._singularity as s join $model._node as n on s.node = n.id join singularity_invalidity_reason as r on r.id=s.id
        where s.validity=FALSE
    union
    select CONCAT('profile:', p.id) as
        id,
        p.validity,
        p.name,
        'profile'::varchar as type,
        ''::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        ''::varchar as reason from $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where p.validity=FALSE
;;

-- Surface elements results
drop materialized view if exists ${model}.results_surface;
;;

create materialized view ${model}.results_surface as
    with
        dump as (
            select st_collect(n.geom) as points, s.geom as envelope, s.id
            from ${model}.coverage as s, ${model}.crossroad_node as n
            where s.domain_type='street' and st_intersects(s.geom, n.geom)
            group by s.id
        ),
        voronoi as (
            select st_intersection(st_setsrid((st_dump(st_VoronoiPolygons(dump.points, 0, dump.envelope))).geom, ${srid}), dump.envelope) as geom
            from dump
        )
    select e.id as id, UPPER(e.name)::varchar(24) as name, e.contour as geom, now() as last_refresh from ${model}.elem_2d_node as e
        union
    select n.id as id, UPPER(n.name)::varchar(24) as name, c.geom as geom, now() as last_refresh from ${model}.storage_node as n join ${model}.coverage as c on n.contour=c.id
        union
    select n.id as id, UPPER(n.name)::varchar(24) as name, voronoi.geom as geom, now() as last_refresh from voronoi, ${model}.crossroad_node as n where st_intersects(voronoi.geom, n.geom)
    order by id asc
with no data;
;;

/* ********************************************** */
/*          Hydrology functions to hydraulics     */
/* ********************************************** */

create or replace function ${model}.find_nodes_network(node_array integer[])
returns integer[]
language plpgsql
stable
as
$$$$
    declare
        up_node integer;
        down_node integer;
        result_array integer array;
    begin
        for up_node in select unnest(node_array)
        loop
            with link as (
            select id from ${model}._link where up = up_node)
            select l.down into down_node from ${model}._link as l, link where l.id = link.id;
            if (select exists(select 1 from ${model}.manhole_hydrology_node where id = down_node)) then
                select array_append(result_array, down_node) into result_array;
            end if;
        end loop;
        select array_cat(result_array, node_array) into result_array;
        select array(select distinct unnest(result_array) order by 1) into result_array;

        return result_array;
    end;
$$$$
;;

create or replace function ${model}.find_minimal_hydrology_network(node_array integer[])
returns integer[]
language plpgsql
stable
as
$$$$
    declare
        temp_array integer ARRAY;
    begin
	select ${model}.find_nodes_network(node_array) into temp_array;
	while temp_array != node_array loop
		select ${model}.find_nodes_network(node_array) into node_array;
		select ${model}.find_nodes_network(temp_array) into temp_array;
	end loop;
	return temp_array;
    end;
$$$$
;;

/* ********************************************** */
/*          Hotfix for 3D constrain generation    */
/* ********************************************** */

create or replace view $model.constrain_points as(
    with points_from_constrain as (
        select (ST_DumpPoints(c.geom)).path[1] as p_id,
            c.points_xyz as points_id,
            (ST_DumpPoints(c.geom)).geom as geom,
            9999 as z_ground,
            ST_LineLocatePoint(c.geom, (ST_DumpPoints(c.geom)).geom) as loc,
            (ST_DumpPoints(c.geom)).geom as geom_on_line,
            c.id as id_line
        from $model.constrain c
        where c.constrain_type is distinct from 'ignored_for_coverages'::hydra_constrain_type
        order by c.id, (ST_LineLocatePoint(c.geom, (ST_DumpPoints(c.geom)).geom))
        ),
    z_points as (
        select p.id,
            p.points_id,
            p.geom,
            p.z_ground,
            ST_LineLocatePoint(c.geom, p.geom) as loc,
            ST_LineInterpolatePoint(c.geom, ST_LineLocatePoint(c.geom, p.geom)) as geom_on_line,
            c.id as id_line
        from $model.constrain c,
            project.points_xyz p
        where ST_Distance(c.geom, p.geom) < c.points_xyz_proximity and
            c.points_xyz = p.points_id and
            c.constrain_type is distinct from 'ignored_for_coverages'::hydra_constrain_type
        order by c.id, (ST_LineLocatePoint(c.geom, p.geom))
        ),
    dist_comp as (
        select pfc.id_line as c_id,
            pfc.p_id,
            pfc.geom,
            pfc.points_id,
            pfc.loc,
            closest_zp.id as zp_id,
            closest_zp.dist,
            closest_zp.z_ground
        from points_from_constrain as pfc
        cross join lateral (
            select id,
                z_ground,
                ST_Distance(z_points.geom, pfc.geom) as dist
            from z_points
            where pfc.points_id = z_points.points_id
            order by pfc.geom <-> z_points.geom
            limit 2
            ) as closest_zp
        ),
    weighted as (
        select sum(dist) as weight,
            dist_comp.p_id,
            dist_comp.c_id
        from dist_comp
        group by p_id, c_id
        ),
    contrib as (
        select dist_comp.c_id as c_id,
            dist_comp.p_id,
            dist_comp.z_ground * (dist_comp.dist/ weighted.weight) as contrib_z,
            dist_comp.geom,
            dist_comp.loc
        from dist_comp, weighted
        where dist_comp.p_id = weighted.p_id and
        dist_comp.c_id = weighted.c_id
        ),
    vertex_with_z as (
        select ST_SetSrid(ST_MakePoint(st_x(contrib.geom),
            st_y(contrib.geom),
            sum(contrib_z)),$srid) as geom,
            c_id,
            p_id,
            loc
        from contrib
        group by c_id, p_id, geom, loc
        ),
    all_points as (
        select vz.geom,
            vz.c_id,
            vz.loc
        from vertex_with_z as vz
            union
        select ST_SetSrid(ST_MakePoint(st_x(zp.geom_on_line),
            st_y(zp.geom_on_line),
            zp.z_ground),$srid) as geom,
            zp.id_line as c_id,
            zp.loc
        from z_points as zp
        ),
    ord_points as (
        select ap.c_id,
            ap.geom,
            ap.loc
        from all_points as ap
        order by ap.c_id, ap.loc
        )
    select ST_SetSrid(ST_MakeLine(points.geom)::geometry(LinestringZ), $srid) as geom,
        points.c_id as id
    from ord_points points
    group by points.c_id
    )
;;
