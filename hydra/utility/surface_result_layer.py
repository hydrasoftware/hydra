# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
from qgis.core import edit, QgsVectorLayer
from hydra.utility.result_decoder import W15Result
from numpy import array, zeros_like, logical_and, zeros, sqrt as npsqrt, arcsin as npasin, arccos as npacos, abs as npabs
from math import sin, cos, asin, acos, sqrt, pi
from hydra.utility.settings_properties import SettingsProperties

_plugin_dir = os.path.dirname(os.path.dirname(__file__))
_qml_dir = os.path.join(_plugin_dir, "ressources", "qml")

class SurfaceResultLayer(QgsVectorLayer):
    def __init__(self, project, w14, w15):
        model = project.get_current_model().name
        srid = project.srid
        scn = project.get_current_scenario()

        sql = f"""
        with dump as (
                select st_collect(n.geom) as points, s.geom as envelope, s.id
                from {model}.coverage as s, {model}.crossroad_node as n
                where s.domain_type='street' and st_intersects(s.geom, n.geom)
                group by s.id
            ),
            voronoi as (
                select st_intersection(st_setsrid((st_dump(st_VoronoiPolygons(dump.points, 0, dump.envelope))).geom, {srid}), dump.envelope) as geom
                from dump
            ),
            result as (
                select e.id as id, UPPER(e.name)::varchar(24) as name, zb as z, e.contour as geom, 'elem_2d'::varchar as type
                from {model}.elem_2d_node as e
                    union all
                select n.id as id, UPPER(n.name)::varchar(24) as name, n.zs_array[1][1] as z, c.geom as geom, 'storage'::varchar as type
                from {model}.storage_node as n join {model}.coverage as c on n.contour=c.id
                    union all
                select n.id as id, UPPER(n.name)::varchar(24) as name, z_ground as z, voronoi.geom as geom, 'crossroad'::varchar as type
                from voronoi, {model}.crossroad_node as n where st_intersects(voronoi.geom, n.geom)
            )
            select *, 0. as \\"surface_h1(m)\\", 0. as \\"surface_v(m/s)\\", 0. as \\"surface_alp_v(degre)\\" from result order by id asc
            """

        uri = (f"dbname='{project.name}' "
            f"service='{SettingsProperties.get_service()}' "
            f"key=id "
            f"srid={srid} "
            f"type=Polygon "
            f"checkPrimaryKeyUnicity='0' "
            f'table="({sql})" (geom)')
        src_layer = QgsVectorLayer(uri, f"surface tmp", "postgres")
        assert(src_layer.isValid())
        feats = [feat for feat in src_layer.getFeatures()]
        self.__basename = f"{scn[1]} {model} surface"
        super().__init__(f"Polygon?crs=epsg:{project.srid}", self.__basename, "memory")
        pr = self.dataProvider()
        pr.addAttributes(src_layer.dataProvider().fields().toList())
        self.updateFields()
        pr.addFeatures(feats)
        self.loadNamedStyle(os.path.join(_qml_dir, "results_surface.qml"))


        self.__w15 = w15
        feats = [feat for feat in self.getFeatures()]
        self.__value_idx = array([[
            w15.offset(f['name'], 'z(m)') if f['type']!='elem_2d' else w15.offset(f['name'], 'z2(m)') , 
            w15.offset(f['name'], 'ux(m/s)'), 
            w15.offset(f['name'], 'uy(m/s)'), 
            w15.offset(f['name'], 'vol(m3)')] for f in feats])
        self.__fid = array([f.id() for f in feats])
        self.__z = array([f['z'] for f in feats])
        self.__area = array([f.geometry().area() for f in feats])

    def setStep(self, step):
        step = self.__w15.step(step)
        self.__update(step.values)
        self.setName(self.__basename)

    def __update(self, res):
        if not len(self.__z):
            return

        h = res[self.__value_idx[:,0,0], self.__value_idx[:,0,1]] - self.__z
        ux = res[self.__value_idx[:,1,0], self.__value_idx[:,1,1]]
        uy = res[self.__value_idx[:,2,0], self.__value_idx[:,2,1]]
        vol = res[self.__value_idx[:,3,0], self.__value_idx[:,3,1]]
        mask = (vol > 0)
        h[mask] = vol[mask] / self.__area[mask] # pour les casier, la hauteur h_avg = v(z)/s(z_max)
        v = npsqrt(ux**2 + uy**2)
        a = zeros_like(v)
        mask = (v > 0)
        c = zeros_like(v)
        c[mask] = ux[mask]/v[mask]
        s = zeros_like(v)
        s[mask] = uy[mask]/v[mask]
        m2 = (c>0)
        a[m2] = npasin(s[m2])
        m3 = logical_and(s>0, c<0)
        a[m3] = npacos(c[m3])
        a[logical_and(~m2, ~m3)] = npabs(npasin(s[logical_and(~m2, ~m3)])) - pi
        a *= 180./pi
        features = { fid : {4: float(h_), 5: float(v_), 6: float(a_)} for fid, h_, v_, a_ in zip(self.__fid, h, v, a)}
        self.dataProvider().changeAttributeValues(features)
        self.triggerRepaint()

    def hMax(self):
        """for each element
            max of h
            v for hmax
            max of v
            angle for vmax
        """
        res_for_z_max = zeros((len(self.__w15.names), 8))
        z = zeros((len(self.__w15.names),))
        zm = zeros((len(self.__w15.names),))
        for s in range(self.__w15.nb_steps):
            step = self.__w15.step(s)
            res = step.values
            z[self.__value_idx[:,0,0]] = res[self.__value_idx[:,0,0], self.__value_idx[:,0,1]]
            zm[self.__value_idx[:,0,0]] = res_for_z_max[self.__value_idx[:,0,0], self.__value_idx[:,0,1]]
            mask = z > zm
            res_for_z_max[mask,:] = res[mask,:]
        self.__update(res_for_z_max)
        self.setName(f"{self.__basename} h max")
