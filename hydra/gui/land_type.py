# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import json
import os
from functools import partial
from qgis.PyQt import uic, QtGui, QtCore
from qgis.PyQt.QtCore import pyqtSignal
from qgis.PyQt.QtWidgets import QInputDialog, QMessageBox, QAbstractItemView
from qgis.PyQt.QtGui import QColor
from qgis.core import QgsProject, QgsRendererCategory, QgsSymbol, QgsProject
from hydra.utility.string import get_nullable_sql_float
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from hydra.gui.widgets.combo_with_values import ComboWithValues
from hydra.gui.base_dialog import BaseDialog, tr

with open(os.path.join(os.path.dirname(__file__), "land_type.json")) as j:
    __json_classification__ = json.load(j)


class LandTypeManager(BaseDialog):
    reload_style_signal = pyqtSignal(list)

    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        #self.iface = iface
        uic.loadUi(os.path.join(current_dir, "land_type.ui"), self)

        self.project = project
        self.table_land_type = None

        self.table_land_type = HydraTableWidget(project,
            ("id", "land_type", "c_imp", "netflow_type", "c_runoff", "curve", "f0_mm_h", "fc_mm_h", "soil_cap_mm"),
            (tr("Id"), tr("Land type"), tr("Impervious\nCoefficient"), tr("Production\nLaw type"),
            tr("Runoff\nCoefficient"), tr("SCS\nSoil storage J (mm)"), tr("Holtan\nDry infiltration rate (mm/h)"), tr("Holtan\nSaturation infiltration rate (mm/h)"), tr("Holtan\nSoil storage capacity (mm)")),
            "project", "land_type",
            (self.new_land_type, self.delete_land_type), "", "id",
            self.table_land_type_placeholder)

        self.table_land_type.set_editable(True)
        self.table_land_type.data_edited.connect(self.__save_land_type)
        self.table_land_type.table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.table_land_type.table.setSelectionMode(QAbstractItemView.ContiguousSelection)
        self.table_land_type.data_updated.connect(self.__update_netflow_type_combos)
        self.btn_load.clicked.connect(self.__load_land_type)

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.__update_netflow_type_combos()

    def new_land_type(self):
        '''Insert row with default values'''
        self.project.execute("""insert into project.land_type default values;""")
        self.table_land_type.update_data()

    def delete_land_type(self):
        '''Delete selected rows'''
        items = self.table_land_type.table.selectedItems()
        if len(items) > 0:
            self.table_land_type.del_selected_row()

    def __update_netflow_type_combos(self):
        '''For all rows, replace content of netflow_type column with a combobox with possible values'''
        netflow_types = self.project.execute("""select id, name, description from hydra.netflow_type where id in (1, 3, 4)""").fetchall()
        for row in range(self.table_land_type.table.rowCount()):
            combobox = ComboWithValues()
            for i in range(len(netflow_types)):
                id, name, description = netflow_types[i]
                combobox.addItem(name, id)
                if name == self.table_land_type.table.item(row, 3).text():
                    combobox.set_selected_value(id)
            combobox.currentIndexChanged.connect(partial(self.__update_text_behind_combo, row))
            self.table_land_type.table.setCellWidget(row,3,combobox)

    def __update_text_behind_combo(self, combo_row):
        '''Updates item text in the cells of netflow_type comboBoxes when comboBox selection is changed'''
        self.table_land_type.table.item(combo_row, 3).setText(self.table_land_type.table.cellWidget(combo_row, 3).currentText())
        self.table_land_type.table.selectRow(combo_row)
        self.__save_land_type()

    def keyPressEvent(self, event):
        '''Catches key pressing events. Then copy/pastes data if Ctrl + C/V'''
        # if self.table_land_type.table.hasFocus():
        if event.key() == QtCore.Qt.Key_V and  (event.modifiers() & QtCore.Qt.ControlModifier):
            self.__paste()
            event.accept()
        elif event.key() == QtCore.Qt.Key_C and  (event.modifiers() & QtCore.Qt.ControlModifier):
            self.__copy()
            event.accept()
        else:
            event.ignore()

    def __paste(self):
        '''Parse clipboard, insert into DB table if possible, then refresh UI table'''
        text = QtGui.QApplication.clipboard().text()
        for i, textline in enumerate(text.split('\n')):
            try:
                land_type, c_imp, netflow_type, c_runoff, curve, f0_mm_h, fc_mm_h, soil_cap_mm = textline.split('\t')
                self.project.execute("""insert into project.land_type(land_type, c_imp, netflow_type, c_runoff, curve, f0_mm_h, fc_mm_h, soil_cap_mm)
                                        values ('{}', {}, '{}', {}, {}, {}, {}, {}) on conflict do nothing
                                     """.format(land_type, c_imp, netflow_type,
                                                get_nullable_sql_float(c_runoff), get_nullable_sql_float(curve),
                                                get_nullable_sql_float(f0_mm_h), get_nullable_sql_float(fc_mm_h),
                                                get_nullable_sql_float(soil_cap_mm)))
            except:
                pass
        self.table_land_type.update_data()

    def __copy(self):
        '''Copy selected rows to clipboard'''
        selected = self.table_land_type.table.selectedRanges()
        copied_text = ""
        for i in range(selected[0].topRow(), selected[0].bottomRow() + 1):
            for j in range(selected[0].leftColumn()+1, selected[0].rightColumn() + 1):
                try:
                    copied_text += self.table_land_type.table.item(i, j).text() + "\t"
                except AttributeError:
                    # quand une case n'a jamais été initialisée
                    copied_text += "\t"
            copied_text = copied_text[:-1] + "\n"  # le [:-1] élimine le '\t' en trop
        # enregistrement dans le clipboard
        QtGui.QApplication.clipboard().setText(copied_text)

    def __save_land_type(self):
        '''Saves selected row'''
        self.table_land_type.save_selected_row()

    def __load_land_type(self):
        '''Check if there is data in land_type layers, if so,
        inform the user that the current style will be lost and give choice to cancel'''

        has_data = self.project.execute("""select 1 from project.land_type""").fetchone()

        if has_data:
            confirm = QMessageBox(QMessageBox.Question, tr('Delete current classification'),
            tr('This will delete the current classification of land occupation layer. Proceed?'), QMessageBox.Ok | QMessageBox.Cancel).exec_()
            if confirm == QMessageBox.Ok:
                self.__load_land_type_from_json()
        else:
            self.__load_land_type_from_json()

    def __load_land_type_from_json(self):
        '''Clears table, then loads a preset classification from land_type.json'''
        classifications = list(__json_classification__.keys())
        # classifications = classifications + [classif + " + style" for classif in classifications] ## Temporarily removed the "style option"

        raw_classification, ok = QInputDialog.getItem(None, tr('Preset land type'), tr('Choose a preset classification'), classifications, 0, False)
        if raw_classification and ok:
            classification_name = raw_classification.replace(" + style", "")
            self.load_classification(classification_name)
            if "style" in raw_classification:
                self.__load_preset_style(__json_classification__[classification_name])

    def load_classification(self, classification_name):
        assert classification_name in list(__json_classification__.keys())
        self.project.execute("""delete from project.land_type""")
        for cat in __json_classification__[classification_name]:
            self.project.execute(""" insert into project.land_type(land_type, c_imp) values ('{land_type}',{c_imp})
                                 """.format(**__json_classification__[classification_name][cat]))

        self.table_land_type.update_data()

    def __load_preset_style(self, dict_class):
        '''Change land_occupation layer style to a predefined classification on land type'''
        root = QgsProject.instance().layerTreeRoot()
        if root.findGroup('project'):
            project_group = root.findGroup('project')
            for layertree in project_group.children():
                if layertree.layerName() == "Land occupation":
                    land_occupation_layer = layertree.layer()
            renderer = land_occupation_layer.rendererV2()
            renderer.deleteAllCategories()

            for cat in dict_class:

                id = self.project.execute("""select id from project.land_type where land_type = '{land_type}'
                                          """.format(land_type=dict_class[cat]['land_type'])).fetchone()[0]

                mySymbol = QgsSymbol.defaultSymbol(land_occupation_layer.geometryType())
                mySymbol.setColor(QColor(dict_class[cat]['color']))

                renderer.addCategory(QgsRendererCategory(dict_class[cat]['land_type'], mySymbol, dict_class[cat]['land_type']))

            land_occupation_layer.setRendererV2(renderer)
            self.reload_style_signal.emit(['land_occupation'])

            for l in QgsProject.instance().layers():
                l.reload()

            self.iface.layerTreeView().refreshLayerSymbology(land_occupation_layer.id())

    def save(self):
        BaseDialog.save(self)
        self.close()
