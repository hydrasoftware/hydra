# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import io
import os
import numpy
import copy
import matplotlib.patheffects as path_effects
from random import randint
from datetime import timedelta
from qgis.PyQt.QtWidgets import QApplication, QVBoxLayout, QWidget, QLabel
from qgis.PyQt.QtGui import QImage
from qgis.PyQt.QtCore import Qt, QCoreApplication, pyqtSignal, QMargins
from matplotlib.figure import Figure
from matplotlib.gridspec import GridSpec
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT, FigureCanvasQTAgg as FigureCanvas
from hydra.utility.result_decoder import W16Result

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)

_plugin_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
_layer_icons_dir = os.path.join(_plugin_dir, "ressources", "layer_icons")
_icons_dir = os.path.join(_plugin_dir, "ressources", "images")
_color = ['royalblue','coral','aquamarine','powderblue','steelblue','limegreen','crimson','olivedrab','darkcyan','slategray']
_linestyle = ['-','--','-.',':',(5,2),(7,3),(4,1)]

# checkboxs de l'interface

_list_checkbox_ = {
    "links":            ["Links",               "Show links"],
    "singularities":    ["Singularities",       "Show singularities"],
    "cross_sections":   ["Cross sections",      "Show cross sections"],
    "r_bank":           ["Reach right bank",    "Show right bank"],
    "l_bank":           ["Reach left bank",     "Show left bank"],
    "water_line":       ["Water level Z",       "Show water level"],
    "water_flow":       ["Water flow Q",        "Show water's debit"],
    "water_minmax":     ["Water min max Z",     "Show water min/max z"],
    "water_speed":      ["Water speed V",       "Show water speed"],
    "energy":           ["Energy E",            "Show equivalent Z energy"],
    "critical":         ["Critical Zc",         "Show critical Z"],
    "solid_flow":       ["Solid flow Qs",       "Show solid flow Qs"],
    "h_sable":          ["Silting hight Hs",     "Show silting hight"],
    "quality":          ["Quality",             "Show quality_param"]
    }

# valeurs résultats mappées par rapport au format .w16

_dicograph = {  'water_line'  : { 'graph' : 11,    'ind':0,   'label':'waterline Z',      'type':'collection',   'color':'royalblue',    'param':None,       'unit':None       },
                'water_flow'  : { 'graph' : 21,    'ind':1,   'label':'waterflow Q',      'type':'line',         'color':'m',            'param':None,       'unit':None       },
                'water_speed' : { 'graph' : 31,    'ind':3,   'label':'waterspeed V',     'type':'line',         'color':'green',        'param':None,       'unit':None       },
                'h_sable'     : { 'graph' : 41,    'ind':6,   'label':'Silting hight Hs', 'type':'line',         'color':'olive',        'param':None,       'unit':None       },
                'solid_flow'  : { 'graph' : 22,    'ind':7,   'label':'Solid flow Qs',    'type':'line',         'color':'peru',         'param':None,       'unit':None       },
                'energy'      : { 'graph' : 11,    'ind':4,   'label':'Energy E',         'type':'collection',   'color':'purple',       'param':None,       'unit':None       },
                'critical'    : { 'graph' : 11,    'ind':5,   'label':'Critical Zc',      'type':'collection',   'color':'red',          'param':None,       'unit':None       },
                'MES'         : { 'graph' : 51,    'ind':999, 'label':'Pollution MES',    'type':'line',         'color':'blue',         'param':'MES ',     'unit':'mg/l'     },
                'DBO5'        : { 'graph' : 51,    'ind':999, 'label':'Pollution DBO5',   'type':'line',         'color':'orange',       'param':'DBO5',     'unit':'mg/l'     },
                'DC0'         : { 'graph' : 51,    'ind':999, 'label':'Pollution DCO',    'type':'line',         'color':'crimson',      'param':'DCO ',     'unit':'mg/l'     },
                'NTK'         : { 'graph' : 51,    'ind':999, 'label':'Pollution NTK',    'type':'line',         'color':'green',        'param':'NTK ',     'unit':'mg/l'     },
                'O2'          : { 'graph' : 51,    'ind':999, 'label':'Pollution O2',     'type':'line',         'color':'skyblue',      'param':'O2  ',     'unit':'mg/l'     },
                'NH4'         : { 'graph' : 51,    'ind':999, 'label':'Pollution NH4',    'type':'line',         'color':'m',            'param':'NH4 ',     'unit':'mg/l'     },
                'N03'         : { 'graph' : 51,    'ind':999, 'label':'Pollution NO3',    'type':'line',         'color':'k',            'param':'NO3 ',     'unit':'mg/l'     },
                'EI'          : { 'graph' : 52,    'ind':999, 'label':'Pollution EI',     'type':'line',         'color':'salmon',       'param':'EI  ',     'unit':'npp/100ml'},
                'EC'          : { 'graph' : 52,    'ind':999, 'label':'Pollution EC',     'type':'line',         'color':'limegreen',    'param':'EC  ',     'unit':'npp/100ml'},
                'AD1'         : { 'graph' : 52,    'ind':999, 'label':'Pollution AD1',    'type':'line',         'color':'teal',         'param':'AD1 ',     'unit':'npp/100ml'},
                'AD2'         : { 'graph' : 52,    'ind':999, 'label':'Pollution AD2',    'type':'line',         'color':'brown',        'param':'AD2 ',     'unit':'npp/100ml'}}

def solve_non_continu(x_data_incont,y_data_incont,x_data_max,y_data_max) :
    ''' this function allow a discretisation of the dataset to make a fill between possible'''
    if x_data_incont is None or y_data_incont is None :
        return(x_data_incont,y_data_incont,x_data_max,y_data_max)

    listsaved = []

    for i in range(len(x_data_incont)-1) :
        if x_data_incont[i] == x_data_incont[i+1]:
            listsaved.append([x_data_incont[i],y_data_incont[i]])

    if x_data_max is not None:
        interp_y_data_incont = numpy.interp(x_data_max,x_data_incont,y_data_incont)
    else :
        interp_y_data_incont = []

    if len(x_data_max) == len(interp_y_data_incont) :
        for save in listsaved :
            for i in range (len(x_data_max)) :
                if x_data_max[i] == save[0] :
                    x_data_max = numpy.insert(x_data_max,i,save[0])
                    interp_y_data_incont = numpy.insert(interp_y_data_incont,i,save[1])
                    y_data_max = numpy.insert(y_data_max,i,y_data_max[i])
                    save = [-1,-999]

    return(x_data_max,interp_y_data_incont,x_data_max,y_data_max)


class LongProfilInterface(QWidget):

    def __getattr__(self, att):
        if att=='display_r_bank':
            return  self.__parent.display_r_bank if (self.__parent and hasattr(self.__parent,'display_r_bank')) else self.__display_r_bank
        elif att=='display_l_bank':
            return  self.__parent.display_l_bank if (self.__parent and hasattr(self.__parent,'display_l_bank')) else self.__display_l_bank
        elif att=='display_singularities':
            return  self.__parent.display_singularities if (self.__parent and hasattr(self.__parent,'display_singularities')) else self.__display_singularities
        elif att=='display_links':
            return  self.__parent.display_links if (self.__parent and hasattr(self.__parent,'display_links')) else self.__display_links
        elif att=='display_cross_sections':
            return  self.__parent.display_cross_sections if (self.__parent and hasattr(self.__parent,'display_cross_sections')) else self.__display_cross_sections
        elif att=='display_water_line':
            return  self.__parent.display_water_line if (self.__parent and hasattr(self.__parent,'display_water_line')) else self.__display_water_line
        elif att=='display_water_flow':
            return  self.__parent.display_water_flow if (self.__parent and hasattr(self.__parent,'display_water_flow')) else self.__display_water_flow
        elif att=='display_water_minmax':
            return  self.__parent.display_water_minmax if (self.__parent and hasattr(self.__parent,'display_water_minmax')) else self.__display_water_minmax
        elif att=='display_energy':
            return  self.__parent.display_energy if (self.__parent and hasattr(self.__parent,'display_energy')) else self.__display_energy
        elif att=='display_critical':
            return  self.__parent.display_critical if (self.__parent and hasattr(self.__parent,'display_critical')) else self.__display_critical
        elif att=='display_solid_flow':
            return  self.__parent.display_solid_flow if (self.__parent and hasattr(self.__parent,'display_solid_flow')) else self.__display_solid_flow
        elif att=='display_water_speed':
            return  self.__parent.display_water_speed if (self.__parent and hasattr(self.__parent,'display_water_speed')) else self.__display_water_speed
        elif att=='display_h_sable':
            return  self.__parent.display_h_sable if (self.__parent and hasattr(self.__parent,'display_h_sable')) else self.__display_h_sable
        elif att=='display_min_water':
            return  self.__parent.display_min_water if (self.__parent and hasattr(self.__parent,'display_min_water')) else self.__display_min_water
        elif att=='display_max_water':
            return  self.__parent.display_max_water if (self.__parent and hasattr(self.__parent,'display_max_water')) else self.__display_max_water
        elif att=='display_quality':
            return  self.__parent.display_quality if (self.__parent and hasattr(self.__parent,'display_quality')) else self.__display_quality
        elif att=='branchs':
            return  self.__parent.branchs if (self.__parent and hasattr(self.__parent,'branchs')) else self.__branchs
        elif att=='list_scenarios':
            return  self.__parent.list_scenarios if (self.__parent and hasattr(self.__parent,'list_scenarios')) else self.__list_scenarios
        else:
            raise AttributeError

    def __setattr__(self, name, value):
        if name=='display_r_bank':
            self.__display_r_bank=value
        elif name=='display_l_bank':
            self.__display_l_bank=value
        elif name=='display_singularities':
            self.__display_singularities=value
        elif name=='display_links':
            self.__display_links=value
        elif name=='display_cross_sections':
            self.__display_cross_sections=value
        elif name=='display_water_line':
            self.__display_water_line=value
        elif name=='display_water_flow':
            self.__display_water_flow=value
        elif name=='display_water_minmax':
            self.__display_water_minmax=value
        elif name=='display_energy':
            self.__display_energy=value
        elif name=='display_critical':
            self.__display_critical=value
        elif name=='display_solid_flow':
            self.__display_solid_flow=value
        elif name=='display_water_speed':
            self.__display_water_speed=value
        elif name=='display_h_sable':
            self.__display_h_sable=value
        elif name=='display_quality':
            self.__display_quality=value
        elif name=='branchs':
            self.__branchs=value
        elif name=='list_scenarios':
            self.__list_scenarios=value
        else:
            super(LongProfilInterface, self).__setattr__(name, value)

    def __init__ (self, iface, project, time_control, parent=None):
        QWidget.__init__(self)

        self.__isreloading = True
        self.setVisible(False)

        self.__iface = iface
        self.__project = project
        self.__time_control = time_control
        self.__time_control.timeChanged.connect(self.time_changed)
        self.__parent = parent

        self.setWindowModality(Qt.NonModal)
        self.setWindowTitle("Graph window")
        self.resize(900,500)
        self.graph_window = None
        self.graph_window = GraphWindow(self.__project,self)

        # short_cut to hide and uncheck all checkbox
        for check_box in list(_list_checkbox_.items()) :
            vars(self)['__'+check_box[0]] = False

        self.__display_max_water = False
        self.__display_min_water = False

        self.__show_struct = False
        self.__show_links = False
        self.__show_cross = False

        self.__list_scenarios = []
        self.__branchs = []

        self.__idx = 0

        self.has_result = False
        self.param_header = None
        self.init_locals()
        self.__isreloading = False

    def closeEvent(self, event):
        ''' override of the close event to handle and destroy properly the hihglighter in the Qgis canvas'''
        self.__parent.reload_branchmanager()
        super(LongProfilInterface, self).closeEvent(event)

    def change_title(self):
        branch = ' - '.join([branch.name for branch in self.branchs])
        scn = " / " + ",".join([scn[1] for scn in self.list_scenarios]) if self.list_scenarios else ''
        self.setWindowTitle("Longitudinal profile: {} {}".format(branch, scn))

    def init_locals(self):
        ''' init of the property that should be reseted after each show of the graph window'''
        self.__values_list=[]
        self.__pks_results=numpy.array([])
        self.__pks_geom=numpy.array([])
        self.__baseline=numpy.array([])
        self.__vaultline=numpy.array([])
        self.__rbankline=numpy.array([])
        self.__lbankline=numpy.array([])
        self.__topo=numpy.array([])
        self.__mainline_min=numpy.array([])
        self.__mainline_max=numpy.array([])
        self.__speedline=numpy.array([])
        self.__labelshover_point_Z=numpy.array([])
        self.__labelshover_point_pk=numpy.array([])
        self.__labels_marker = []
        self.__labelslink = []
        self.__labels = []
        self.__branch_limit=[]
        self.__export_data=[]
        self.__dict_side_style_links_overflow=[]

        self.__struct_rpr_collection = {'weir':[],'gate':[],'various':[],'tank_bc':[],'zq_bc':[]}

        list_collection_geom = ['baselinecollection','rbankcollection','lbankcollection','vaultcollection','groundcollection']

        list_collection_result = [ key[0] for key in _dicograph.items() if key[1]['type'] == 'collection']

        self.init_collection(list_collection_geom + list_collection_result)
        self.init_scn()

    def init_collection(self,listcollection):
        ''' init the main collection for the graph'''
        for c in listcollection:
            if hasattr(self.graph_window, c):
                vars(self.graph_window)[c].reset()
                vars(self.graph_window)[c] = GraphWindow.LineCollection([])
            else:
                vars(self.graph_window)[c] = GraphWindow.LineCollection([])

    def init_scn(self):
        ''' init the collection for the scn list'''

        for i in range(len(self.list_scenarios)):
            scn = self.list_scenarios[i]
            for key in _dicograph.items():
                if key[1]['type'] == 'collection' :
                    if hasattr(self.graph_window, key[0]+'_'+scn[1]):
                        vars(self.graph_window)[key[0]+'_'+scn[1]].reset()
                        vars(self.graph_window)[key[0]+'_'+scn[1]] = GraphWindow.LineCollection([])
                    else:
                        vars(self.graph_window)[key[0]+'_'+scn[1]] = GraphWindow.LineCollection([])
            self.graph_window.init_lines_for_scn(scn,_linestyle[i])

    def display(self):
        ''' display the graph'''
        self.reload(override = True)
        self.show()

    def copy_image(self):
        ''' create an image and put in clipboard'''
        self.graph_window.export_image()

    def copy_data(self):
        ''' copy the data for the current graph and put in clipboard'''
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard)

        data_to_export = ""
        data_structures = ""

        tab_export =   [["Geometry","-","-","-","-","-","-","-","Results"],
                        ["-",tr("pk (km)"), tr("baseline (m)"), tr("topo (m)"), tr("vault (m)"), tr("left bank (m)"), tr("rigth bank (m)")]]

        if self.param_header is None :
            tab_export[1]   +=   [  tr(""),
                                    tr("pk (km)"),
                                    tr("waterline z {minmax} (m)"),
                                    tr("flow minor riverbed Q {minmax} (m3/s)"),
                                    tr("flow major river bed Q {minmax} (m3/s)"),
                                    tr("speed V {minmax} (m3/s)"),
                                    tr("energy E {minmax} (m)"),
                                    tr("criticalZ Zc {minmax} (m/s)"),
                                    tr("silting hight Hs {minmax} (m)"),
                                    tr("solid flow Qs {minmax} (m3/s)")]

        else :
            tab_export[1]   +=   [  tr(""),
                                    tr("pk (km)"),
                                    tr("waterline z {minmax} (m)"),
                                    tr("flow minor riverbed Q {minmax} (m3/s)"),
                                    tr("flow major river bed Q {minmax} (m3/s)"),
                                    tr("speed V {minmax} (m3/s)"),
                                    tr(self.param_header[0]),
                                    tr(self.param_header[1]),
                                    tr(self.param_header[2]),
                                    tr(self.param_header[3])]

        nb_export = 0
        len_reach = 0
        len_branch = 0

        for i in range(0, max(len(self.__pks_geom),len(self.__pks_results))):
            if i >= (self.__export_data[nb_export][0]+len_reach+len_branch) :
                if self.__export_data[nb_export][1] == 'branch' :
                    len_branch += self.__export_data[nb_export][0]
                elif self.__export_data[nb_export][1] == 'reach' :
                    len_reach += self.__export_data[nb_export][0]
                if nb_export < len(self.__export_data)-1 :
                    nb_export += 1

            row = []
            if self.__export_data[nb_export][1] == 'branch' :
                row.append(str(self.__export_data[nb_export][2] if i<len(self.__pks_geom) else "-"))
                row.append(str(self.__pks_geom[i] if i<len(self.__pks_geom) else "-"))
                row.append((str(self.__baseline[i]) if i<len(self.__baseline) else "-"))
                row.append((str(self.__topo[i]) if i<len(self.__topo) else "-"))
                row.append((str(self.__vaultline[i-len_reach]) if (i-len_reach)<len(self.__vaultline) else "-"))
                row.append('-')
                row.append('-')

            elif self.__export_data[nb_export][1] == 'reach' :
                row.append(str(self.__export_data[nb_export][2] if i<len(self.__pks_geom) else "-"))
                row.append(str(self.__pks_geom[i] if i<len(self.__pks_geom) else "-"))
                row.append((str(self.__baseline[i]) if i<len(self.__baseline) else "-"))
                row.append((str(self.__topo[i]) if i<len(self.__topo) else "-"))
                row.append('-')
                row.append((str(self.__lbankline[i-len_branch]) if (i-len_branch)<len(self.__lbankline) else "-"))
                row.append((str(self.__rbankline[i-len_branch]) if (i-len_branch)<len(self.__rbankline) else "-"))

            row.append("")
            row.append(str(self.__pks_results[i] if i<len(self.__pks_results) else "-"))
            for k in range (0,8) :
                row.append((str(self.__values_list[0][2][i][k]) if (self.__values_list != []) and (i<len(self.__values_list[0][2]) and len(self.__values_list[0][2][i])>0) else "-"))
            tab_export.append(row)
        tab_export.append([])

        for k in range(len(tab_export)) :
            for i in range(len(tab_export[k])) :
                if self.display_min_water :
                    tab_export[k][i] = tab_export[k][i].format(minmax = "min")
                elif self.display_max_water :
                    tab_export[k][i] = tab_export[k][i].format(minmax = "max")
                else :
                    tab_export[k][i] = tab_export[k][i].format(minmax = 'time')

        for row in tab_export :
            data_to_export += '\t'.join(row) +'\r'

        if (self.display_singularities or self.display_links or self.display_cross_sections) :
            header_struct = []
            if self.display_singularities :
                header_struct.append("Structures")
            if self.display_links :
                header_struct.append("Links")
            if self.display_cross_sections :
                header_struct.append("Cross sections")

            data_structures = tr("Objects ( " + ', '.join(header_struct) +" )")

            data_structures_header = '\t'.join([ tr(" : Name"), tr("pk (km)"), tr("Cumulated pk (km)"), tr("Z (m)"), tr("Direction")])

            data_structures += ((data_structures_header) + '\r\n')

            data_struct_values = ""
            #pk_cum = 0
            #compt = 1
            data_struct_values += '\tBranch\t'+self.branchs[0].name+'\t-\r\n'
            current_reach = self.__labelslink[0][4]

            for object in self.__labelslink :
                if object[4] != current_reach :
                    data_struct_values += '\tBranch\t'+object[4]+'\t-\r\n'
                    current_reach = object[4]
                data_struct_values +=  (object[1] + '\t')
                data_struct_values +=  (str(object[3]) + '\t')
                data_struct_values +=  (str(round(object[0],3) if object[0] else None) + '\t')
                data_struct_values +=  (str(round(object[2],3) if object[2] else None) + '\t')
                data_struct_values +=  (object[5] or '-') + '\t'
                data_struct_values += '\r\n'

            data_structures += data_struct_values

        data_to_export += data_structures

        cb.setText(data_to_export, mode=cb.Clipboard)

    def remove_secondary_scn (self,scn):
        ''' remove the lines and collections for the comparator'''
        if hasattr(self.graph_window,'water_line_' + scn[1]):
            for key in _dicograph.items() :
                vars(self.graph_window)[key[0]+'_'+scn[1]].reset()

    def time_changed_collection(self, collection, color, index_data, legend) :
        ''' specific function to change the time of a collection to make the program lighter'''
        lenght_collection,format = collection.get_collection_format()
        total_lines = 0
        collection.reset()
        for i in range (lenght_collection):
            total_lines = total_lines + format[i]
            collection.append_line(self.graph_window.new_line(11,color, '-', self.__pks_results[total_lines:(total_lines+format[i+1])],self.__values[:,index_data][total_lines:(total_lines+format[i+1])]),legend)
            collection.show_collection(True,legend)

    def time_changed(self, idx):
        self.__values_list = self.result_at_time_collection(idx)
        self.__idx = idx
        #colors=[]

        time = float(self.__time_control.getTime(idx))
        scn = self.list_scenarios[0]
        date, = self.__project.fetchone(f"""
            select date0 from project.scenario where id = {scn[0]}
            union all
            select date0 from project.serie_scenario where id = {scn[0]}
            """)

        self.graph_window.label_time.setText("DATE: {} , SCENARIO TIME : {}, SCENARIO : {}".format(str(date+timedelta(hours=time)), str(timedelta(hours=time)), scn[1]))

        for key in _dicograph.items() :
            if key[1]['type'] == 'line' and key[1]['param'] is None :
                if getattr(self,'display_'+key[0]):
                    for scn_values in self.__values_list:
                        vars(self.graph_window)[key[0]+'_'+scn_values[1]].set_data(self.__pks_results,scn_values[2][:,key[1]['ind']])
                        vars(self.graph_window)[key[0]+'_'+scn_values[1]].set_legend(key[1]['label']+'_'+scn_values[1])

            if key[1]['param'] is not None :
                if self.display_quality and self.param_header is not None:
                    ind = [i for i,x in enumerate(self.param_header) if x == key[1]['param']]
                    if len(ind) == 1 :
                        for scn_values in self.__values_list:
                            vars(self.graph_window)[key[0]+'_' + scn_values[1]].set_data(self.__pks_results,scn_values[2][:,4+ind[0]])
                            vars(self.graph_window)[key[0]+'_' + scn_values[1]].set_legend(key[1]['label'] +'_'+ scn_values[1]+' '+key[1]['unit'])

            elif key[1]['type'] == 'collection':
                if getattr(self,'display_'+key[0]):
                    style_compteur = 0
                    for scn_values in self.__values_list:
                        if vars(self.graph_window)[key[0]+'_'+scn_values[1]].get_collection():
                            color =(vars(self.graph_window)[key[0]+'_'+scn_values[1]].get_collection()[0].get_color())
                            style =(vars(self.graph_window)[key[0]+'_'+scn_values[1]].get_collection()[0].get_style())
                        else :
                            color = key[1]['color']
                            style = _linestyle[style_compteur]
                        lenght_collection,format = vars(self.graph_window)[key[0]+'_'+scn_values[1]].get_collection_format()
                        total_lines = 0
                        vars(self.graph_window)[key[0]+'_'+scn_values[1]].reset()
                        style_compteur += 1
                        for i in range (lenght_collection):
                            total_lines = total_lines + format[i]
                            vars(self.graph_window)[key[0]+'_'+scn_values[1]].append_line(self.graph_window.new_line(key[1]['graph'],color, style ,self.__pks_results[total_lines:(total_lines+format[i+1])],scn_values[2][:,key[1]['ind']][total_lines:(total_lines+format[i+1])]),key[1]['label']+'_'+scn_values[1])
                            vars(self.graph_window)[key[0]+'_'+scn_values[1]].show_collection(True,key[1]['label']+'_'+scn_values[1])

        if self.display_water_line:
            for p in self.graph_window.listfillbetween:
                p.remove()
            self.graph_window.listfillbetween = []
            if self.list_scenarios :
                self.graph_window.listfillbetween = vars(self.graph_window)['water_line_' + self.__values_list[0][1]].fill_between_collection(self.graph_window.baselinecollection,'lightblue',maxmode=True,max_secondary_collection=self.graph_window.vaultcollection)

        self.__time_control.nextFrame()

    def calcul_max_extent(self) :
        """ extent resolved on the first scenario of the list, we will look for the maximal extent of each curve"""
        #to get the value refers to _dico graph and the "ind" of each values
        ymaxQ=0
        ymaxV=0
        ymaxQs=0
        ymaxHs=0
        ymaxQuality51=0
        ymaxQuality52=0
        if self.param_header is not None and self.__mainline_max is not None and len(self.__pks_results) != 0 :

            for i in range(len(self.param_header)) :
                quality_item = self.param_header[i]
                for key in _dicograph.items() :
                        if key[1]['param'] == quality_item :
                            if key[1]['graph'] == 51 :
                                for p in range(0, len(self.__pks_results)):
                                    if self.__mainline_max[p][4+i] > ymaxQuality51 :
                                        ymaxQuality51 = self.__mainline_max[p][4+i]
                            elif key[1]['graph'] == 52 :
                                for p in range(0, len(self.__pks_results)):
                                    if self.__mainline_max[p][4+i] > ymaxQuality52 :
                                        ymaxQuality52 = self.__mainline_max[p][4+i]

        if self.__mainline_max is not None and len(self.__pks_results) != 0:
            for i in range(0, len(self.__pks_results)):
                if self.__mainline_max[i][1] > ymaxQ :
                    ymaxQ = self.__mainline_max[i][1]
                if self.__mainline_max[i][3] > ymaxV :
                    ymaxV = self.__mainline_max[i][3]
                if self.__mainline_max[i][7] > ymaxQs :
                    ymaxQs = self.__mainline_max[i][7]
                if self.__mainline_max[i][6] > ymaxHs :
                    ymaxHs = self.__mainline_max[i][6]

            return(ymaxQ + float(0.1*ymaxQ), self.__pks_results[-1], ymaxV + float(0.1*ymaxV), self.__pks_results[-1], ymaxQs + float(0.1*ymaxQs), ymaxHs + float(0.1*ymaxHs),
                ymaxQuality51 + float(0.1*ymaxQuality51), ymaxQuality52 + float(0.1*ymaxQuality52))

        return(0, 0, 0, 0, 0, 0, 0, 0)

    def init_profile(self):
        ''' function that check if checkboxes are checked and then set visible the right part of the graph'''

        #reset fillbetween
        for j in self.graph_window.listfillbetween:
            j.remove()
        self.graph_window.listfillbetween = []

        self.graph_window.label_points.reset()

        self.graph_window.rbankcollection.show_collection(False)
        self.graph_window.lbankcollection.show_collection(False)

        for i in range(len(self.__values_list)):
            scn_values =  self.__values_list[i]
            self.graph_window.init_lines_for_scn([scn_values[0],scn_values[1]],_linestyle[i])

        for key in _dicograph.items() :
            if key[1]['type'] == 'collection':
                vars(self.graph_window)[key[0]].show_collection(False)
                for scn_values in self.__values_list:
                    vars(self.graph_window)[key[0]+'_'+scn_values[1]].show_collection(False)

        self.graph_window.label_points_collection.reset()
        self.graph_window.label_points_collection.show_collection(False)

        #reset singularities
        for struct in self.graph_window.structure_collection:
            struct.reset()
        self.graph_window.structure_collection = []

        for p in self.graph_window.listgroundlevel:
            p.remove()
        self.graph_window.listgroundlevel = []

        self.graph_window.baselinecollection.show_collection(True,'baseline')
        self.graph_window.groundcollection.show_collection(True,'groundline')

        self.graph_window.vaultcollection.show_collection(True,'vaultline')
        self.graph_window.listgroundlevel = self.graph_window.groundcollection.fill_between_collection(self.graph_window.vaultcollection,'lightgrey')

        #draw separator
        for sep in self.graph_window.limits :
            sep.remove()
        self.graph_window.limits = []

        for i in range(len(self.__branch_limit)-1) :
            pk = self.__branch_limit[i]
            self.graph_window.limits.append(self.graph_window.axes11.axvline(pk, color='k', linewidth=1))
            self.graph_window.limits.append(self.graph_window.axes21.axvline(pk, color='k', linewidth=1))
            self.graph_window.limits.append(self.graph_window.axes31.axvline(pk, color='k', linewidth=1))
            self.graph_window.limits.append(self.graph_window.axes41.axvline(pk, color='k', linewidth=1))

        if self.display_r_bank:
            self.graph_window.rbankcollection.show_collection(True,'rbank')
        if self.display_l_bank:
            self.graph_window.lbankcollection.show_collection(True,'lbank')
        if self.display_links:
            self.make_style_links_labels()
            self.graph_window.label_points_collection.show_collection(True)
        if self.display_singularities:
            for elem in self.__struct_rpr_collection['weir']:
                self.graph_window.structure_collection.append(self.graph_window.new_line(11,'black', '-',elem[0],elem[1]))
            for elem in self.__struct_rpr_collection['gate']:
                self.graph_window.structure_collection.append(self.graph_window.new_line(11,'black', '-',elem[0][0],elem[0][1],linewidth=6))
                self.graph_window.structure_collection.append(self.graph_window.new_line(11,'white', '-',elem[1][0],elem[1][1],linewidth=3,patheffect='black'))
                self.graph_window.structure_collection.append(self.graph_window.new_line(11,'black', '-',elem[2][0],elem[2][1],linewidth=6))
            for elem in self.__struct_rpr_collection['various']:
                self.graph_window.structure_collection.append(self.graph_window.new_line(11,'black', '',elem[0],elem[1],marker='*'))
            for elem in self.__struct_rpr_collection['tank_bc']:
                self.graph_window.structure_collection.append(self.graph_window.new_line(11,'black', '-',elem[0],elem[1]))
            for elem in self.__struct_rpr_collection['zq_bc']:
                self.graph_window.structure_collection.append(self.graph_window.new_line(11,'steelblue', '-',elem[0],elem[1],linewidth=0.25))
        if self.display_cross_sections:
            self.make_style_links_labels()
            self.graph_window.label_points_collection.show_collection(True)

        # Parcours l'ensemble des types de données de _dicograph et déclenche leurs actualisations si necessaire
        for key in _dicograph.items() :
            if key[1]['type'] == 'line' and key[1]['param'] is None :
                for scn_values in self.__values_list:
                    vars(self.graph_window)[key[0]+'_' + scn_values[1]].set_data(self.__pks_results,scn_values[2][:,key[1]['ind']])
                    vars(self.graph_window)[key[0]+'_' + scn_values[1]].set_legend(key[1]['label'] +'_'+ scn_values[1])

            if key[1]['param'] is not None :
                if self.display_quality and self.param_header is not None :
                    ind = [i for i,x in enumerate(self.param_header) if x == key[1]['param']]
                    if len(ind) == 1 :
                        for scn_values in self.__values_list:
                            vars(self.graph_window)[key[0]+'_' + scn_values[1]].set_data(self.__pks_results,scn_values[2][:,4+ind[0]])
                            vars(self.graph_window)[key[0]+'_' + scn_values[1]].set_legend(key[1]['label'] +'_'+ scn_values[1]+' '+key[1]['unit'])

            elif key[1]['type'] == 'collection':
                if getattr(self,'display_'+key[0]):
                    for scn_values in self.__values_list:
                        vars(self.graph_window)[key[0]+'_'+scn_values[1]].show_collection(True,key[1]['label']+'_'+scn_values[1])

        # affiche le fill_between bleu pour la zone entre la ligne d'eau et la cote de fond
        if self.display_water_line:
            if self.__values_list :
                self.graph_window.listfillbetween = vars(self.graph_window)['water_line_' + self.__values_list[0][1]].fill_between_collection(self.graph_window.baselinecollection,'lightblue',maxmode=True,max_secondary_collection=self.graph_window.vaultcollection)

        # affiche les valeurs maximales et minimales de la ligne d'eau
        if self.display_water_minmax:
            for scn_values in self.__values_list:
                vars(self.graph_window)['waterline_min_' + scn_values[1]].set_data(self.__pks_results,vars(self)['__waterline_min_' + scn_values[1]][:,0])
                vars(self.graph_window)['waterline_max_' + scn_values[1]].set_data(self.__pks_results,vars(self)['__waterline_max_'+scn_values[1]][:,0])
                vars(self.graph_window)['waterline_max_' + scn_values[1]].set_legend('Water max z ' + scn_values[1])
                vars(self.graph_window)['waterline_min_' + scn_values[1]].set_legend('Water min z ' + scn_values[1])

        self.graph_window.label_points.set_data(self.__labelshover_point_pk,self.__labelshover_point_Z)
        self.graph_window.label_points.set_line_visible(False)
        self.graph_window.labels_link.set_data(self.__labelslink)

        # flag ? refaire les fonctions de zoom.
        ymaxQ, xmaxQ, ymaxV, xmaxV, ymaxQs, ymaxHs, ymax51, ymax52 = self.calcul_max_extent()
        self.graph_window.set_extent(ymaxQ, xmaxQ, ymaxV, xmaxV, ymaxQs, ymaxHs, ymax51, ymax52)
        self.graph_window.reload_legend()

    def result_at_time(self, istep, result_path):
        ''' create the result at a certain time, and return the values'''
        values = None
        for branch in self.branchs:
            w16result = W16Result(result_path)
            results = w16result[branch.name][istep][0]
            tmpres = numpy.array([r[1] for r in results])
            values = numpy.concatenate([values,tmpres]) if values is not None else tmpres
        return values

    def result_at_time_collection(self,istep):
        ''' create an array of results associated with a scn from the scenario comparator '''
        # values_collection = [scn_id,scn_name,result]
        values_collection = []
        model = self.__project.get_current_model().name

        for scn in self.list_scenarios :
            if self.__project.scn_has_run_from_name(scn[1]):
                values_collection.append([scn[0],scn[1], self.result_at_time(istep, os.path.join(self.__project.get_senario_path(scn[0]), "hydraulique", scn[1].upper() + "_" + model.upper() + ".w16"))])
        return values_collection

    def result_minmax_collection(self, minmax) :
        ''' create array of result with min and max of each scn '''
        values_collection = []
        model = self.__project.get_current_model().name

        for scn in self.list_scenarios :
            if self.__project.scn_has_run_from_name(scn[1]) :
                temp_res = W16Result(os.path.join(self.__project.get_senario_path(scn[0]), "hydraulique", scn[1].upper() + "_" + model.upper() + ".w16"))
                if minmax == "max" :
                    values_collection.append([scn[0],scn[1], self.result_at_time(temp_res.nstep+2, os.path.join(self.__project.get_senario_path(scn[0]), "hydraulique", scn[1].upper() + "_" + model.upper() + ".w16"))])
                else :
                    values_collection.append([scn[0],scn[1], self.result_at_time(temp_res.nstep+1, os.path.join(self.__project.get_senario_path(scn[0]), "hydraulique", scn[1].upper() + "_" + model.upper() + ".w16"))])

        return values_collection

    def proceed_branch(self, tool, hover_label, singularities_draw):
        ''' function that take a branch and proceed to every calcul on this branch'''

        line_wkt, = self.__project.execute("""
            select ST_AsText(geom) from {model}.{table}
            where id={id}
            """.format(
                model=tool.schema,
                table=tool.table,
                id=tool.id)).fetchone()

        self.__load_from_geom(hover_label, singularities_draw, line_wkt, tool)

        # change this ?
        if self.list_scenarios :
            self.has_result = True
            self.__load_from_results(line_wkt, tool)

        self.init_profile()
        self.graph_window.plot_graph(self.has_result, self.display_water_flow, self.display_solid_flow, self.display_water_speed, self.display_h_sable, self.display_quality)

    def reload(self, override = False):
        ''' handle the event of reloading the graphwindow'''
        if (not self.__isreloading and self.isVisible()) or override:
            self.param_header = None
            # get the header, needed for quality and silting hight. Let None as default param header result in disabling quality.

            # self.param_header = None
            # if self.__project.get_current_scenario() is not None:
                # id_scn, name_scn = self.__project.get_current_scenario()
                # model = self.__project.get_current_model().name
                # self.__result_path = os.path.join(self.__project.get_senario_path(id_scn), "hydraulique", name_scn + "_" + model + ".w16")
                # if self.__project.scn_has_run(id_scn) and os.path.exists(self.__result_path):
                    # w16result = W16Result(self.__result_path)
                    # self.param_header = w16result.param_header

            self.__isreloading = True
            self.init_locals()
            for num, branch in enumerate(self.branchs):
                hover_label, singularities_draw = self.reload_new_labels(branch)
                self.proceed_branch(branch, hover_label, singularities_draw)
                self.graph_window.pk_range_changed.connect(self.__parent.highlighters[num].set_extent_km)
            self.change_title()
            self.__isreloading = False

    def reload_new_labels(self, tool):
        ''' create the labels setup trough SQL queries on the database, you have labels for structures (singularity), links and cross section'''
        #declaration des requetes sql

        #-------------- singularities label & draw -----------------------------------------------------------------------------------------------------------------

        #                               table,                              z1,                              z2,                                  z3,          rpr
        singularities_dico = [['zregul_weir_singularity'               , 't.z_regul'         , 0                                           , 0               ,  1  ],
                              ['gate_singularity'                      , 't.z_invert'        , 't.z_ceiling'                               ,'t.z_ceiling+0.5',  2  ],
                              ['hydraulic_cut_singularity'             , 'n.z_ground'        , 0                                           , 0               ,  3  ],
                              ['borda_headloss_singularity'            , 'n.z_ground'        , 0                                           , 0               ,  3  ],
                              ['bradley_headloss_singularity'          , 't.zw_array[1][1]'  , 't.z_ceiling'                               ,'t.z_ceiling+0.5',  2  ],
                              ['bridge_headloss_singularity'           , 't.zw_array[1][1]'  , 't.zw_array[array_length(zw_array,1)][1]'   ,'t.z_road'       ,  2  ],
                              ['param_headloss_singularity'            , 'n.z_ground'        , 0                                           ,0                ,  3  ],
                              ['marker_singularity'                    , 'n.z_ground'        , 0                                           ,0                ,  3  ],
                              ['hydrograph_bc_singularity'             , 'n.z_ground'        , 0                                           ,0                ,  3  ],
                              ['constant_inflow_bc_singularity'        , 'n.z_ground'        , 0                                           ,0                ,  3  ],
                              ['zq_bc_singularity'                     , 't.zq_array[1][1]'  , 't.zq_array[array_length(zq_array,1)][1]'   ,0                ,  5  ],
                              ['tz_bc_singularity'                     , 'n.z_ground'        , 0                                           ,0                ,  3  ],
                              ['froude_bc_singularity'                 , 'n.z_ground'        , 0                                           ,0                ,  3  ],
                              ['strickler_bc_singularity'              , 'n.z_ground'        , 0                                           ,0                ,  3  ],
                              ['tank_bc_singularity'                   , 't.zs_array[1][1]'  , 't.zs_array[array_length(zs_array,1)][1]'   ,0                ,  4  ],
                              ['model_connect_bc_singularity'          , 'n.z_ground'        , 0                                           ,0                ,  3  ]]

        # reach singularities

        sql_singularity_hover_label_reach = ' UNION '.join([ """SELECT n.pk_km, t.name as name, {z1} as z_print, '  None' as symbol
                FROM {model}.{t} as t,{model}.river_node as n
                WHERE n.reach={reach} and t.node = n.id """.replace('n.z_ground','{model}.river_node_z_invert(n.geom)').format(reach=tool.id, model=tool.schema, t=table, z1=z1) for (table,z1,z2,z3,rpr) in singularities_dico])


        sql_singularity_draw_reach = ' UNION '.join([ """SELECT t.name as name, n.pk_km,{rpr} as rpr, {z1} as z1, {z2} as z2, {z3} as z3
                FROM {model}.{t} as t,{model}.river_node as n
                WHERE n.reach={reach} and t.node = n.id""".format(reach=tool.id, model=tool.schema, t=table, z1=z1, z2=z2, z3=z3, rpr=rpr) for (table,z1,z2,z3,rpr) in singularities_dico])

        # branch singularities

        sql_singularity_hover_label_branch = ' UNION '.join(["""SELECT DISTINCT st_linelocatepoint(b.geom, n.geom)*st_length(b.geom)/1000 as pk_km, t.name as name, {z1} as z_print, '  None' as symbol
                FROM {model}.{t} as t,
                    {model}.manhole_node as n,
                    {model}.pipe_link as p
                JOIN {model}.branch as b on b.id = p.branch
                WHERE b.id = {reach}
                    and t.node = n.id
                    and n.id in
                        (	    select t.up as node from {model}.pipe_link as t where t.branch = {reach}
                        union   select t.down as node from {model}.pipe_link as t where t.branch = {reach})""".format(reach=tool.id, model=tool.schema, t=table, z1=z1) for (table,z1,z2,z3,rpr) in singularities_dico])

        sql_singularity_draw_branch = ' UNION '.join(["""SELECT DISTINCT t.name as name, st_linelocatepoint(b.geom, n.geom)*st_length(b.geom)/1000 as pk_km,{rpr} as rpr, {z1} as z1, {z2} as z2, {z3} as z3
                FROM {model}.{t} as t,
                    {model}.manhole_node as n,
                    {model}.pipe_link as p
                JOIN {model}.branch as b on b.id = p.branch
                WHERE b.id = {reach}
                    and t.node = n.id
                    and n.id in
                        (	    select t.up as node from {model}.pipe_link as t where t.branch = {reach}
                        union   select t.down as node from {model}.pipe_link as t where t.branch = {reach})""".format(reach=tool.id, model=tool.schema, t=table, z1=z1, z2=z2, z3=z3, rpr=rpr) for (table,z1,z2,z3,rpr) in singularities_dico])

        # ---------------  label hover for link --------------------------------------------------------

        #                       link,                z_print,           symbol,         conditions
        linkhover_dico = [[ 'overflow_link'      , 'z.z_crest1'   , "'. #009e00'" , 'and z.width1 <> 0' ],
                          [ 'overflow_link'      , 'z.z_crest2'   , "'. #009e00'" , 'and z.width1 = 0'  ],
                          [ 'strickler_link'     , 'z.z_crest1'   , "'. #ff7f00'" , ' '                 ],
                          [ 'gate_link'          , 'z.z_invert'   , "'x black'"   , ' '                 ],
                          [ 'regul_gate_link'    , 'z.z_invert'   , "'x red'"     , ' '                 ],
                          [ 'porous_link'        , 'z.z_invert'   , "'_ #e31a1c'" , ' '                 ],
                          [ 'weir_link'          , 'z.z_weir'     , "'_ black'"   , ' '                 ]]

        sql_linkhover_label_reach = ' UNION '.join(["""select n.pk_km, z.name as name, {zprint} as z_print, {symbol} as symbol
                from {model}.river_node as n, {model}.{t} as z
                where n.reach={reach} and z.up = n.id {cdt}
                union
                select n.pk_km, z.name as name, {zprint} as z_print, {symbol} as symbol
                from {model}.river_node as n, {model}.{t} as z
                where n.reach={reach} and z.down = n.id {cdt}""".format(reach=tool.id, model=tool.schema, t=table, zprint=zprint, symbol=symbol, cdt=cdt) for (table,zprint,symbol,cdt) in linkhover_dico])

        with_sql_branch = """with nodes as (select n.name, n.id, n.z_ground, st_linelocatepoint(b.geom, n.geom)*st_length(b.geom)/1000 as pk_km
                    from {model}.manhole_node as n, {model}.branch as b
                    where st_dwithin(b.geom, n.geom, .1)
                    and b.id={reach})
                    """.format(reach=tool.id, model=tool.schema)

        sql_linkhover_label_branch = """select nodes.pk_km, nodes.name as name, nodes.z_ground, 'o black' as symbol
                from nodes
                union
                select nodes.pk_km, nodes.name as name, nodes.z_ground, 'o black' as symbol
                from nodes """.format()

        # ------------- label hover for cross section ----------------------------------------------------

        sql_crosssection_hover_label_reach = """select n.pk_km, s.name,
                CASE when s.z_invert_up is null
                    then s.z_invert_down
                    else s.z_invert_up
                    end
                    as z_print,
                '| #e31a1c' as symbol
                from {model}.river_cross_section_profile as s join {model}.river_node as n on s.id=n.id
                where n.reach={reach}""".format(reach=tool.id, model=tool.schema)

        #☺ --------------- label hover for pipe slope branch -----------------------------------------------------

        sql_pipe_slope_hover_label_branch ="""
            SELECT DISTINCT st_linelocatepoint(b.geom, ST_Centroid(p.geom))*st_length(b.geom)/1000 as pk_km,
            p.name || ', slope : ' || ROUND(cast((p.z_invert_up - p.z_invert_down)/coalesce(custom_length, st_length(b.geom)) as numeric),4) as name,
            round(cast((p.z_invert_up + p.z_invert_down)/2 as numeric),2) as z_print,
            '  None' as symbol
            FROM
                {model}.pipe_link as p
            JOIN {model}.branch as b on b.id = p.branch
            WHERE b.id = {branch}""".format(branch=tool.id, model=tool.schema)

        # construction des requetes
        # NB : reach = branch dans la formulation des requêtes pour simplicité

        sql_hover_label =""
        sql_singularities_draw =""

        hover_label = []
        singularities_draw = []

        # construct a dict for style in  overflow links
        if tool.table == 'reach' :
            dict_side_style_links = self.__project.execute('''with node_2d as (
            select m.up as node, m.name from {model}.overflow_link as m where m.up_type != 'river' and m.down_type = 'river'
            union
            select m.down as node, m.name from {model}.overflow_link as m where m.down_type != 'river' and m.up_type = 'river'
            ),
            node_river as (
            select m.up as node, m.name from {model}.overflow_link as m where m.up_type = 'river'
            union
            select m.down as node, m.name from {model}.overflow_link as m where m.down_type = 'river'
            ),
            node_2d_geom as (
            select _node.geom as geom, node_2d.name from {model}._node, node_2d where _node.id = node_2d.node
            ),
            node_river_geom as (
            select river_node.geom as geom, node_river.name from {model}.river_node, node_river where river_node.id = node_river.node and river_node.reach = {reach}
            ),
            river_on_reach as (
            select node_river_geom.name, st_linelocatepoint(r.geom, node_river_geom.geom) as locate, r.geom as reach from {model}.reach as r, node_river_geom where r.id={reach} and st_linelocatepoint(r.geom, node_river_geom.geom) != 1
            )
            select node_2d_geom.name,
            case
            when degrees(St_azimuth(node_river_geom.geom, node_2d_geom.geom))- degrees(St_azimuth(node_river_geom.geom, St_LineInterpolatePoint(river_on_reach.reach, river_on_reach.locate + (1-river_on_reach.locate)*0.1))) + 360 > 540 then 'left'
            when degrees(St_azimuth(node_river_geom.geom, node_2d_geom.geom))- degrees(St_azimuth(node_river_geom.geom, St_LineInterpolatePoint(river_on_reach.reach, river_on_reach.locate + (1-river_on_reach.locate)*0.1))) + 360 < 180 then 'right'
            when degrees(St_azimuth(node_river_geom.geom, node_2d_geom.geom))- degrees(St_azimuth(node_river_geom.geom, St_LineInterpolatePoint(river_on_reach.reach, river_on_reach.locate + (1-river_on_reach.locate)*0.1))) + 360 < 360 then 'left'
            when degrees(St_azimuth(node_river_geom.geom, node_2d_geom.geom))- degrees(St_azimuth(node_river_geom.geom, St_LineInterpolatePoint(river_on_reach.reach, river_on_reach.locate + (1-river_on_reach.locate)*0.1))) + 360 > 360 then 'right'
            end as side
            from node_2d_geom, node_river_geom, river_on_reach
            where node_2d_geom.name = node_river_geom.name
            and node_river_geom.name = river_on_reach.name'''.format(reach=tool.id, model=tool.schema)).fetchall()
            self.__dict_side_style_links_overflow += dict_side_style_links

        if tool.table == 'reach' :

            if self.display_singularities:
                sql_singularities_draw += sql_singularity_draw_reach
                sql_hover_label += sql_singularity_hover_label_reach
            if self.display_cross_sections:
                if not sql_hover_label=="":
                    sql_hover_label += """ union """
                sql_hover_label += sql_crosssection_hover_label_reach
            if self.display_links:
                if not sql_hover_label=="" :
                    sql_hover_label += """ union """
                sql_hover_label += sql_linkhover_label_reach


        if tool.table == 'branch' :

            if self.display_singularities:
                sql_singularities_draw += sql_singularity_draw_branch
                sql_hover_label += sql_singularity_hover_label_branch
            if self.display_links:
                if not sql_hover_label =="" :
                    sql_hover_label = with_sql_branch + sql_linkhover_label_branch +""" union """ + sql_hover_label
                else:
                    sql_hover_label = with_sql_branch + sql_linkhover_label_branch
            if not sql_hover_label=="":
                sql_hover_label += """ union """
            sql_hover_label += sql_pipe_slope_hover_label_branch

        # execution des requêtes

        if not sql_hover_label=="":
            sql_hover_label = sql_hover_label +""" order by pk_km;"""
            hover_label = self.__project.execute(sql_hover_label).fetchall()
        if not sql_singularities_draw=="":
            sql_singularities_draw = sql_singularities_draw +""" order by pk_km;"""
            singularities_draw = self.__project.execute(sql_singularities_draw).fetchall()

        return hover_label, singularities_draw


    def __load_from_geom(self, hover_label, singularities_draw, line_wkt, tool):
        ''' load the geometry of the area from the database (vaultline,groundline,bank,baseline) '''
        pk_geom=None
        if tool.table == 'reach':
            baseline = []
            topo_line = []
            z_vault_line = []
            pk_geom = []
            l_bank = []
            r_bank = []
            pk0, name = self.__project.execute("""
                    select pk0_km, name
                    from {model}.reach
                    where id={reach};
                    """.format(model=tool.schema, reach=tool.id)).fetchone()
            for pk_km, z_invert_up, z_invert_down, z_tn_up, z_tn_down, \
                z_lbank_up, z_lbank_down, z_rbank_up, z_rbank_down in \
                self.__project.execute("""
                    select n.pk_km, p.z_invert_up, p.z_invert_down, p.z_tn_up, p.z_tn_down,
                        p.z_lbank_up, p.z_lbank_down, p.z_rbank_up, p.z_rbank_down
                    from {model}.river_cross_section_profile p
                    join {model}.river_node n on n.id=p.id
                    where n.reach={reach}
                    order by n.pk_km
                    """.format(model=tool.schema, reach=tool.id)).fetchall():
                if z_invert_up is not None and z_invert_up!=0:
                    baseline.append(z_invert_up)
                    pk_geom.append(pk_km-pk0)
                    topo_line.append(z_tn_up)
                    r_bank.append(z_rbank_up)
                    l_bank.append(z_lbank_up)
                if z_invert_down is not None and z_invert_down!=0:
                    baseline.append(z_invert_down)
                    pk_geom.append(pk_km-pk0)
                    topo_line.append(z_tn_down)
                    r_bank.append(z_rbank_down)
                    l_bank.append(z_lbank_down)

        elif tool.table == 'branch':
            baseline = []
            topo_line = []
            z_vault_line = []
            pk_geom = []
            l_bank = []
            r_bank = []
            pk0, name = self.__project.execute("""
                    select pk0_km, name
                    from {model}.branch
                    where id={branch};
                    """.format(model=tool.schema, branch=tool.id)).fetchone()
            for pk_km_up, z_invert_up, pk_km_down, z_invert_down, z_tn_up, z_tn_down, z_vault_up, z_vault_down in \
                self.__project.execute("""
                    select
                        st_linelocatepoint(b.geom, st_startpoint(p.geom))*st_length(b.geom)/1000 as pk_km_up,
                        z_invert_up,
                        st_linelocatepoint(b.geom, st_endpoint(p.geom))*st_length(b.geom)/1000 as pk_km_down,
                        z_invert_down,
                        z_tn_up,
                        z_tn_down,
                        z_vault_up,
                        z_vault_down
                    from {model}.pipe_link p
                    join {model}.branch b on b.id=p.branch
                    where b.id={branch}
                    order by pk_km_up
                    """.format(model=tool.schema, branch=tool.id)).fetchall():
                if z_invert_up is not None and z_invert_up!=0:
                    baseline.append(z_invert_up)
                    topo_line.append(z_tn_up)
                    z_vault_line.append(z_vault_up)
                    pk_geom.append(pk_km_up-pk0)
                if z_invert_down is not None and z_invert_down!=0:
                    baseline.append(z_invert_down)
                    topo_line.append(z_tn_down)
                    z_vault_line.append(z_vault_down)
                    pk_geom.append(pk_km_down-pk0)

        assert pk_geom is not None

        pk_offset = 0 if len(self.__pks_geom)==0 else self.__pks_geom[-1]

        hover_label_pk = []
        hover_label_z = []

        for ll in hover_label:
            # links is list of links corrected of the pk, it has this format :
            # [pk, name, z_latitude, rounded pk, reach, direction]

            marker = None
            links = [ll[0]+pk_offset- pk0,ll[1],ll[2],round(ll[0],3), tool.name, None]
            hover_label_pk.append(ll[0]+pk_offset- pk0)
            hover_label_z.append(ll[2])
            overflow_list = [elem[0] for elem in self.__dict_side_style_links_overflow]
            if ll[1] in overflow_list:
                index = overflow_list.index(ll[1])
                if self.__dict_side_style_links_overflow[index][1] == 'right':
                    marker = ". #009e00"
                    links[5] = 'right'
                if self.__dict_side_style_links_overflow[index][1] == 'left':
                    marker = ". #ff5000"
                    links[5] = 'left'
            self.__labels_marker.append(marker or ll[3])
            self.__labelslink.append(links)

        self.__labelshover_point_Z = numpy.append(self.__labelshover_point_Z,hover_label_z)
        self.__labelshover_point_pk = numpy.append(self.__labelshover_point_pk,hover_label_pk)

        pks = numpy.array([r + pk_offset for r in pk_geom])

        #singularities_draw : arraylike [name,pk,type representation,z1,z2,z3]
        #Zbaseline is interpolated and may fail !
        offset_sing = 0
        if len(self.__pks_geom)!= 0 :
            offset_sing = self.__pks_geom[-1]

        for ll in singularities_draw:
            if ll[2] == 1 :
                zbaseline = numpy.interp(offset_sing+ll[1]-pk0,pks,numpy.array(topo_line,dtype='float64'))
                line_rpr1=[[offset_sing+ll[1]-pk0,offset_sing+ll[1]-pk0],[zbaseline,ll[3]]]
                self.__struct_rpr_collection['weir'].append(line_rpr1)
            if ll[2] == 2 :
                zbaseline = numpy.interp(offset_sing+ll[1]-pk0,pks,numpy.array(topo_line,dtype='float64'))
                line1_rpr2=[[offset_sing+ll[1]-pk0,offset_sing+ll[1]-pk0],[zbaseline,ll[3]]]
                line2_rpr2=[[offset_sing+ll[1]-pk0,offset_sing+ll[1]-pk0],[ll[3],ll[4]]]
                line3_rpr2=[[offset_sing+ll[1]-pk0,offset_sing+ll[1]-pk0],[ll[4],ll[5]]]
                self.__struct_rpr_collection['gate'].append([line1_rpr2,line2_rpr2,line3_rpr2])
            if ll[2] == 3 :
                point_rpr3 = [[offset_sing+ll[1]-pk0],[ll[3]]]
                self.__struct_rpr_collection['various'].append(point_rpr3)
            if ll[2] == 4 :
                line_rpr4=[[offset_sing+ll[1]-pk0,offset_sing+ll[1]-pk0],[ll[3],ll[4]]]
                self.__struct_rpr_collection['tank_bc'].append(line_rpr4)
            if ll[2] == 5 :
                line_rpr5=[[offset_sing+ll[1]-pk0,offset_sing+ll[1]-pk0],[ll[3],ll[4]]]
                self.__struct_rpr_collection['zq_bc'].append(line_rpr5)

        self.__pks_geom = numpy.append(self.__pks_geom, pks)

        self.__branch_limit.append(self.__pks_geom[-1])

        baseline = numpy.array(baseline)
        self.__baseline = numpy.append(self.__baseline, baseline) # zf
        self.__topo = numpy.append(self.__topo, topo_line) # z_tn
        self.__vaultline = numpy.append(self.__vaultline, z_vault_line) # z_vault
        self.__rbankline = numpy.append(self.__rbankline, r_bank) # r_bank
        self.__lbankline = numpy.append(self.__lbankline, l_bank) # l_bank
        self.__export_data.append([len(pks),tool.table,name])

        # CollectionLine, append lignes for a branch
        self.graph_window.baselinecollection.append_line(self.graph_window.new_line(11,'grey', '-',pks,baseline),'baseline')
        self.graph_window.rbankcollection.append_line(self.graph_window.new_line(11,'k', '--',pks,r_bank),"rbank")
        self.graph_window.lbankcollection.append_line(self.graph_window.new_line(11,'k', ':',pks,l_bank),"lbank")
        self.graph_window.vaultcollection.append_line(self.graph_window.new_line(11,'k', '-',pks,z_vault_line),"vaultline")
        self.graph_window.groundcollection.append_line(self.graph_window.new_line(11,'#DC7633', '-',pks,topo_line),"groundline")



    def __load_from_results(self, line_wkt, tool):
        ''' load part of the graph which are in the result W16 data file , water level, water flow , water speed , etc'''
        w16resultcollection = []
        model = self.__project.get_current_model().name

        for scn in self.list_scenarios :
            if scn[2] is not None :
                w16resultcollection.append([scn[0], scn[1] ,W16Result(os.path.join(self.__project.get_senario_path(scn[0]), "hydraulique", scn[1].upper() + "_" + model.upper() + ".w16"))])

        res_geom = w16resultcollection[0][2].get_geom(tool.name)

        if tool.table == 'reach':
            pk0, = self.__project.execute("""
                    select pk0_km
                    from {model}.reach
                    where id={reach};
                    """.format(model=tool.schema, reach=tool.id)).fetchone()
        elif tool.table == 'branch':
            pk0, = self.__project.execute("""
                    select pk0_km
                    from {model}.branch
                    where id={branch};
                    """.format(model=tool.schema, branch=tool.id)).fetchone()

        pk_offset = 0 if len(self.__pks_results)==0 else self.__pks_results[-1]

        pks = numpy.array([r[0] - pk0 + pk_offset for r in res_geom])

        if self.display_min_water:
            self.__values_list = self.result_minmax_collection("min")
        elif self.display_max_water:
            self.__values_list = self.result_minmax_collection("max")
        else:
            self.__values_list = self.result_at_time_collection(0)

        for key in _dicograph.items() :
            compteur_style = 0
            if key[1]['type'] == 'collection' :
                for scn_values in self.__values_list:
                    vars(self.graph_window)[key[0]+'_'+scn_values[1]].append_line(self.graph_window.new_line(key[1]['graph'],key[1]['color'] , _linestyle[compteur_style] ,pks ,scn_values[2][:,key[1]['ind']][len(self.__pks_results):(len(self.__pks_results)+len(pks))]) ,key[0]+'_'+scn_values[1])
                    compteur_style += 1

        self.__pks_results = numpy.append(self.__pks_results, pks)

        for cnt, scn_result in enumerate(w16resultcollection):
            vars(self)['__waterline_min_'+scn_result[1]] = self.result_at_time(scn_result[2].nstep +1,os.path.join(self.__project.get_senario_path(scn_result[0]), "hydraulique", scn_result[1].upper() + "_" + model.upper() + ".w16"))
            vars(self)['__waterline_max_'+scn_result[1]] = self.result_at_time(scn_result[2].nstep +2,os.path.join(self.__project.get_senario_path(scn_result[0]), "hydraulique", scn_result[1].upper() + "_" + model.upper() + ".w16"))
            if cnt == 0 :
                self.__mainline_min = self.result_at_time(scn_result[2].nstep +1,os.path.join(self.__project.get_senario_path(scn_result[0]), "hydraulique", scn_result[1].upper() + "_" + model.upper() + ".w16"))
                self.__mainline_max = self.result_at_time(scn_result[2].nstep +2,os.path.join(self.__project.get_senario_path(scn_result[0]), "hydraulique", scn_result[1].upper() + "_" + model.upper() + ".w16"))

        #ici gérer le multi temps (on prend le temps sur le 1er scn 2ème temps, calendaire ? )
        times = w16resultcollection[0][2].get_step_times()
        self.__time_control.setTimes(["%.2f"%(d) for d in times])

    def make_style_links_labels(self):
        ''' use the result of the SQL request to set properly the style into the graph '''
        liststyle = ['. #ff5000','. #009e00','. #ff7f00','x black','x red','. #e31a1c','| black','o black','| #e31a1c','  None']
        for i in range (len(liststyle)):
            indices = [j for j, mrk in enumerate(self.__labels_marker) if mrk == liststyle[i]]
            labels_z = [self.__labelshover_point_Z[k] for k in indices]
            labels_pk = [self.__labelshover_point_pk[ll] for ll in indices]
            self.graph_window.label_points_collection.append_line(self.graph_window.new_line(11,liststyle[i][2:],':',labels_pk,labels_z,marker=liststyle[i][0]), None)


class GraphWindow(QWidget):
    "Graph window Widget"
    class NavigationToolbar(NavigationToolbar2QT):
        # only display the buttons we need
        toolitems = [t for t in NavigationToolbar2QT.toolitems if
                     t[0] in ('Home', 'Back', 'Forward', 'Pan', 'Zoom', 'Subplots')]

    pk_range_changed = pyqtSignal(float, float)

    class HoverLabels(object):
        ''' class that allow a pop-up to pop when you hover a certain elements of the graph. '''
        def __init__(self, canvas, axes, symbol=None):
            self.__data = None
            self.__canvas = canvas
            self.__axes = axes
            self.__labels = []
            self.annot = self.__axes.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                    bbox=dict(boxstyle="round", fc="w"),
                    arrowprops=dict(arrowstyle="->"))
            self.annot.set_visible(False)

        def reset_labels(self):
            self.__labels = []

        def set_data(self,labels):
            self.__labels = labels

        def set_annot(self,ind,line):
            ''' called to show the label with the right text in it ( actually : Z_level,name of the elements, pk in the branch)'''
            x,y = line.get_data()
            self.annot.xy = (x[ind["ind"][0]], y[ind["ind"][0]])
            text = "{}, {},pk : {}".format([self.__labels[n][1] for n in ind["ind"]][0], [self.__labels[n][2] for n in ind["ind"]][0], [self.__labels[n][3] for n in ind["ind"]][0])
            self.annot.set_text(text)
            self.annot.get_bbox_patch().set_alpha(0.4)

    class LineCollection (object):
        '''class to regroup lines for a given data and allow a better control of them'''
        def __init__(self, collection ):
            self.__LineCollection = collection

        def reset(self):
            for ll in self.__LineCollection:
                ll.reset()
            self.__LineCollection = []

        def append_line(self,line,legend):
            if len(self.__LineCollection) == 0:
                line.set_legend(legend)
            line.set_line_visible(False)
            self.__LineCollection.append(line)

        def show_collection(self, bool, legend = None):
            for ll in self.__LineCollection:
                ll.set_line_visible(bool)
            onelegend = False
            if bool:
                for ll in self.__LineCollection:
                    if ll.get_line() is not None and not onelegend:
                        ll.set_legend(legend)
                        onelegend = True
            else:
                for ll in self.__LineCollection:
                    if ll.get_line() is not None and not onelegend:
                        ll.set_legend('_nolegend_')
                        onelegend = True

        def get_collection_format(self):
            ''' function used to get information about the collection it gives the number of line and the lenght of each one'''
            listformat = [0]
            for ll in self.__LineCollection:
                listformat.append(ll.get_len_line())
            return(len(self.__LineCollection),listformat)

        def get_collection(self):
            ''' getter to get acces to the line into the collection '''
            return(self.__LineCollection)

        def fill_between_collection(self,baselinecollection,color,maxmode=False,max_secondary_collection=None):
            ''' function that take two collection and return a colored area for the graph beetween these collection, you can also add a secondary collection to create a merged collection'''
            listfillbetween =[]
            xdatabaseline=None
            baseline_collection = baselinecollection.get_collection()
            if max_secondary_collection is not None:
                secondary_collection = max_secondary_collection.get_collection()
            if len(self.__LineCollection) == len(baseline_collection):
                for i in range (len(self.__LineCollection)):
                    if maxmode:
                        if secondary_collection[i].get_xline() is not None:
                            xdata = self.__LineCollection[i].get_xline()
                            xdatabaseline = baseline_collection[i].get_xline()
                            zvaultline = secondary_collection[i].get_yline()
                            zwater = copy.deepcopy(self.__LineCollection[i].get_yline())
                            zbaseline = baseline_collection[i].get_yline()
                            zvaultlineinterp = numpy.interp(xdata,xdatabaseline,zvaultline)
                            for ll in range(len(zwater)):
                                if zwater[ll] > zvaultlineinterp[ll]:
                                    zwater[ll] = zvaultlineinterp[ll]
                            xdatabaseline,zbaseline,xdata,zwater = solve_non_continu(xdatabaseline,zbaseline,xdata,zwater)
                        else :
                            xdata = self.__LineCollection[i].get_xline()
                            xdatabaseline = baseline_collection[i].get_xline()
                            zwater = self.__LineCollection[i].get_yline()
                            zbaseline = baseline_collection[i].get_yline()
                            xdatabaseline,zbaseline,xdata,zwater = solve_non_continu(xdatabaseline,zbaseline,xdata,zwater)
                    else :
                        xdata = self.__LineCollection[i].get_xline()
                        xdatabaseline = baseline_collection[i].get_xline()
                        zwater = self.__LineCollection[i].get_yline()
                        zbaseline = baseline_collection[i].get_yline()
                        xdatabaseline,zbaseline,xdata,zwater = solve_non_continu(xdatabaseline,zbaseline,xdata,zwater)

                    if xdatabaseline is not None:
                        if len(xdatabaseline) == len(zbaseline):

                            listfillbetween.append(self.__LineCollection[i].get_axes().fill_between(xdata,zbaseline,zwater,where = zwater>zbaseline, interpolate=True, facecolor = color))

                    self.__LineCollection[i].get_canvas().draw()
            return listfillbetween

    class Line(object):
        '''utility class to encapsulate data and show/hide'''
        def __init__(self, canvas, axes, color, style,legend=None,marker=None):
            self.__xdata = None
            self.__ydata = None
            self.__canvas = canvas
            self.__axes = axes
            self.__line = None
            self.__color = color
            self.__style= style
            self.__legend = legend
            self.__marker = marker

        def __update(self):
            '''update the line graph (show/hide)'''
            self.__remove()
            if self.__marker is None and self.__xdata is not None and self.__ydata is not None :
                self.__line, = self.__axes.plot(
                    self.__xdata,
                    self.__ydata,
                    linestyle=self.__style,
                    marker="",
                    color=self.__color,
                    label=self.__legend)
            elif self.__marker is not None and self.__xdata is not None and self.__ydata is not None:
                self.__line, = self.__axes.plot(
                    self.__xdata,
                    self.__ydata,
                    linestyle='None',
                    marker=self.__marker,
                    markerfacecolor='None',
                    markeredgecolor=self.__color,
                    label=self.__legend)

            if self.__axes.get_legend_handles_labels()[1]:
                self.__axes.legend()
            self.__axes.get_xaxis().get_major_formatter().set_useOffset(False)
            self.__axes.get_yaxis().get_major_formatter().set_useOffset(False)
            self.__canvas.draw()

        def __remove(self):
            if self.__line:
                self.__line.remove()
                self.__line = None

        def reset(self):
            self.set_legend(None)
            self.__remove()
            self.__xdata = None
            self.__ydata = None

        def set_data(self, x_data, y_data):
            if len(x_data)==len(y_data):
                "set/reset line data"
                self.__xdata = x_data
                self.__ydata = y_data
                self.__update()

        def get_canvas(self):
            return self.__canvas

        def get_line(self):
            return self.__line

        def get_style(self):
            return self.__style

        def get_color(self):
            return self.__color

        def get_axes(self):
            return self.__axes

        def get_xline(self):
            return self.__xdata

        def get_yline(self):
            return self.__ydata

        def get_len_line(self):
            if self.__line is not None:
                return len(self.__xdata)

        def set_line_visible(self,bool):
            if self.__line is not None:
                self.__line.set_visible(bool)

        def set_legend(self,legend):
            self.__legend = legend
            if self.__line is not None:
                self.__line.set_label(self.__legend)

    def __init__(self, project, parent=None):
        QWidget.__init__(self, parent)

        self.__project = project
        self.__dpi = 75
        self.has_result = False
        self.__fig = Figure(dpi=self.__dpi)
        self.graph_xy = [11]
        self.graph_x = [21,31,41,51]
        self.axes11 = self.__fig.add_subplot(511)
        self.axes21 = self.__fig.add_subplot(512)
        self.axes31 = self.__fig.add_subplot(513)
        self.axes41 = self.__fig.add_subplot(514)
        self.axes51 = self.__fig.add_subplot(515)
        self.axes22 = self.axes21.twinx()
        self.axes52 = self.axes51.twinx()

        self.label_time = QLabel("TIME : Initial")
        self.label_time.setFixedSize(600,25)

        self.__canvas = FigureCanvas(self.__fig)

        self.vLayout = QVBoxLayout()
        self.vLayout.setContentsMargins(QMargins(0,0,0,0))

        self.vLayout.addWidget(self.__canvas)
        self.vLayout.addWidget(self.label_time)
        self.setLayout(self.vLayout)
        parent.setLayout(self.vLayout)

        self.mpl_toolbar = GraphWindow.NavigationToolbar(self.__canvas, parent)

        self.__canvas.mpl_connect('scroll_event', self.__on_scroll)
        self.__canvas.mpl_connect('button_press_event', self.__on_press)
        self.__canvas.mpl_connect('motion_notify_event', self.__on_move)
        self.__canvas.mpl_connect('motion_notify_event', self.__hover)

        self.listfillbetween = []
        self.listgroundlevel = []

        for i in self.graph_xy :
            setattr(self,'start_x'+str(i), None)
            setattr(self,'start_xlim'+str(i), None)
            setattr(self,'start_ylim'+str(i), None)
            setattr(self,'xratio'+str(i), None)

        for key in _dicograph.items() :
            if key[1]['type'] == 'line' :
                setattr(self,key[0],GraphWindow.Line(self.__canvas, vars(self)['axes'+str(key[1]['graph'])], key[1]['color'], '-', key[1]['label']))

        self.waterline_min = GraphWindow.Line(self.__canvas, self.axes11, 'lightgreen', '-',"Min waterline")
        self.waterline_max = GraphWindow.Line(self.__canvas, self.axes11, 'cornflowerblue', '-',"Max waterline")

        self.label_points = GraphWindow.Line(self.__canvas, self.axes11, 'purple', None, marker ="o")
        self.labels_link = GraphWindow.HoverLabels(self.__canvas, self.axes11)

        self.label_points_collection = GraphWindow.LineCollection([])
        self.structure_collection = []
        self.limits = []

        self.plot_graph()

    def reload_legend(self):
        if self.axes11.get_legend_handles_labels()[1]:
            self.axes11.legend()
        if self.axes21.get_legend_handles_labels()[1]:
            self.axes21.legend()
        if self.axes31.get_legend_handles_labels()[1]:
            self.axes31.legend()
        if self.axes41.get_legend_handles_labels()[1]:
            self.axes41.legend()
        if self.axes22.get_legend_handles_labels()[1]:
            self.axes22.legend()
        if self.axes51.get_legend_handles_labels()[1]:
            self.axes51.legend()
        if self.axes52.get_legend_handles_labels()[1]:
            self.axes52.legend()

    def add_filled_line(self, x_data, ydata, axes, color):
        ''' fonction to add a line filled between the zero level and the line'''
        self.listfillbetween.append(vars(self)['axes'+str(axes)].fill_between(x_data, ydata, color=color))

    def init_lines_for_scn(self,scn,linestyle):

        for key in _dicograph.items() :
            if key[1]['type'] == 'line' :
                if hasattr(self, key[0]+'_'+scn[1]):
                    vars(self)[key[0]+'_'+scn[1]].reset()
                else:
                    vars(self)[key[0]+'_'+scn[1]] = GraphWindow.Line(self.__canvas, getattr(self,'axes'+str(key[1]['graph'])), key[1]['color'], linestyle ,key[1]['label']+'_'+ scn[1])


        if hasattr(self, 'waterline_min_' + scn[1]):
            vars(self)['waterline_min_'+scn[1]].reset()
            vars(self)['waterline_max_'+scn[1]].reset()
        else:
            vars(self)['waterline_min_'+scn[1]] = GraphWindow.Line(self.__canvas, self.axes11, _color[randint(0,len(_color)-1)], ':',"Min waterline "+ scn[1])
            vars(self)['waterline_max_'+scn[1]] = GraphWindow.Line(self.__canvas, self.axes11, _color[randint(0,len(_color)-1)], ':',"Max waterline "+ scn[1])

    def new_line(self,axes,color,linestyle,xdata,ydata,marker=None,linewidth=None,patheffect=None):
        ''' used to create a line from outside the main class'''
        if axes == 11:
            axetemp = self.axes11
        elif axes == 21:
            axetemp = self.axes21
        elif axes == 31:
            axetemp = self.axes31
        elif axes == 41:
            axetemp = self.axes41
        elif axes == 51:
            axetemp = self.axes51
        elif axes == 52:
            axetemp = self.axes52
        elif axes == 22:
            axetemp = self.axes22
        Line = GraphWindow.Line(self.__canvas,axetemp,color,linestyle,marker=marker)
        Line.set_data(xdata,ydata)

        if linewidth is not None :
            Line.get_line().set_linewidth(linewidth)
        if patheffect is not None :
            Line.get_line().set_path_effects([path_effects.Stroke(linewidth=6, foreground=patheffect),path_effects.Normal()])

        return Line

    def plot_graph(self, result=False, Qflow=False, Qsolidflow=False, speedline=False, h_sable=False, quality=False):
        ''' function which handle the plot of the graph, size, position and what should be shown according to the checked checkboxes'''

        gs=GridSpec(5,1, height_ratios=[(1+999*int(bool)) for bool in [True, (Qflow or Qsolidflow) and result, speedline and result, h_sable and result, quality and result]])

        self.axes11.set_position(gs[0,0].get_position(self.__fig))
        self.axes21.set_position(gs[1,0].get_position(self.__fig))
        self.axes31.set_position(gs[2,0].get_position(self.__fig))
        self.axes22.set_position(gs[1,0].get_position(self.__fig))
        self.axes41.set_position(gs[3,0].get_position(self.__fig))
        self.axes51.set_position(gs[4,0].get_position(self.__fig))
        self.axes52.set_position(gs[4,0].get_position(self.__fig))

        self.axes21.set_visible(False)
        self.axes31.set_visible(False)
        self.axes22.set_visible(False)
        self.axes41.set_visible(False)
        self.axes51.set_visible(False)
        self.axes52.set_visible(False)

        self.axes52.legend(bbox_to_anchor=(0., 0.95))
        self.axes22.legend(bbox_to_anchor=(0., 0.95))

        if Qflow and result :
            self.axes21.set_visible(True)
        if Qsolidflow and result :
            self.axes22.set_visible(True)
        if speedline and result:
            self.axes31.set_visible(True)
        if h_sable and result:
            self.axes41.set_visible(True)
        if quality and result:
            self.axes51.set_visible(True)
            self.axes52.set_visible(True)

        self.__canvas.draw()

    def __emit_range_change(self):
            x_min_graph = 0
            if self.axes11.get_xlim()[0] > 0 :
                x_min_graph = self.axes11.get_xlim()[0]
            x_max_graph = self.__x_max_graph
            if self.axes11.get_xlim()[1] < x_max_graph :
                x_max_graph = self.axes11.get_xlim()[1]
            self.pk_range_changed.emit(
                    x_min_graph,
                    x_max_graph)

    def __hover(self, event):
        ''' check if an element of the hidden label line is hovered and if so, launch the sequence to show label'''
        # there is a pb with inaxes if 2 axes overlap
        line = self.label_points.get_line()
        vis = self.labels_link.annot.get_visible()
        if event.inaxes == self.axes11 and line is not None:
            cont, ind = line.contains(event)
            if cont:
                self.labels_link.set_annot(ind,line)
                self.labels_link.annot.set_visible(True)
                self.__canvas.draw()
            else:
                if vis:
                    self.labels_link.annot.set_visible(False)
                    self.__canvas.draw()

    def __on_press(self, event):
        '''initialise data for drag'''

        if (hasattr(self.mpl_toolbar,"_activate") and self.mpl_toolbar._activate is None) or len(self.mpl_toolbar.mode.value) == 0:

            if event.button == 1:
                for i in self.graph_xy :

                    setattr(self,'start_x'+str(i), event.x)
                    setattr(self,'start_y'+str(i), event.y)

                    setattr(self,'start_xlim'+str(i),numpy.array(getattr(self,'axes'+str(i)).get_xlim()))
                    setattr(self,'start_ylim'+str(i),numpy.array(getattr(self,'axes'+str(i)).get_ylim()))

                    setattr(self,'xratio'+str(i),(getattr(self,'start_xlim'+str(i))[1] -  getattr(self,'start_xlim'+str(i))[0])\
                             / self.__canvas.width())
                    setattr(self,'yratio'+str(i),(getattr(self,'start_ylim'+str(i))[1] -  getattr(self,'start_ylim'+str(i))[0])\
                             / self.__canvas.height())

                for i in self.graph_x :

                    setattr(self,'start_x'+str(i), event.x)
                    setattr(self,'start_y'+str(i), event.y)

                    setattr(self,'start_xlim'+str(i),numpy.array(getattr(self,'axes'+str(i)).get_xlim()))
                    setattr(self,'start_ylim'+str(i),numpy.array(getattr(self,'axes'+str(i)).get_ylim()))

                    setattr(self,'xratio'+str(i),(getattr(self,'start_xlim'+str(i))[1] -  getattr(self,'start_xlim'+str(i))[0])\
                             / self.__canvas.width())
                    setattr(self,'yratio'+str(i),(getattr(self,'start_ylim'+str(i))[1] -  getattr(self,'start_ylim'+str(i))[0])\
                             / self.__canvas.height())

    def __on_move(self, event):

        if (hasattr(self.mpl_toolbar.mode,"value") and len(self.mpl_toolbar.mode.value)==0) or self.mpl_toolbar._activate is None:

            if event.button == 1:
                for i in self.graph_xy :

                    if event.x is not None :
                        getattr(self,'axes'+str(i)).set_xlim(getattr(self,'start_xlim'+str(i)) + (getattr(self,'start_x'+str(i)) - event.x)*getattr(self,'xratio'+str(i)))
                    if event.y is not None :
                        getattr(self,'axes'+str(i)).set_ylim(getattr(self,'start_ylim'+str(i)) + (getattr(self,'start_y'+str(i)) - event.y)*getattr(self,'yratio'+str(i)))

                for i in self.graph_x :

                    if event.x is not None :
                        getattr(self,'axes'+str(i)).set_xlim(getattr(self,'start_xlim'+str(i)) + (getattr(self,'start_x'+str(i)) - event.x)*getattr(self,'xratio'+str(i)))

                self.__canvas.draw()
                self.__emit_range_change()

    def __on_scroll(self, event):
        '''zoom plot on scroll'''
        for i in self.graph_xy:
            self.__scrolling(event,getattr(self,'axes'+str(i)))
        for i in self.graph_x:
            self.__scrolling(event,getattr(self,'axes'+str(i)))
        self.__scrolling(event,self.axes52)
        self.__scrolling(event,self.axes51)

        self.__canvas.draw()
        self.__emit_range_change()

    def __scrolling(self,event,axes):
        ''' function to handle the scroll zoom not used when navigation toolbar is active'''

        if (hasattr(self.mpl_toolbar,"_activate") and self.mpl_toolbar._activate is None) or len(self.mpl_toolbar.mode.value) == 0:

            xmin,xmax = axes.get_xlim()
            if event.xdata < (xmin+(0.1*(xmax-xmin))):
                if event.inaxes == axes :
                    y_min, y_max = axes.get_ylim()
                    new_height = (y_max - y_min)*(1.1 if event.step < 0 else .9)
                    axes.set_ylim(
                            [(y_max+y_min)*.5 - new_height*.5,
                             (y_max+y_min)*.5 + new_height*.5])
            else:
                assert event.step
                x_min, x_max = axes.get_xlim()
                new_width = (x_max - x_min)*(1.1 if event.step < 0 else .9)
                relx = (x_max - event.xdata) / (x_max - x_min)
                axes.set_xlim(
                        [event.xdata - new_width*(1-relx),
                         event.xdata + new_width*(relx)])

    def set_extent(self, ymaxQ, xmaxQ, ymaxV, xmaxV, ymaxQs, ymaxHs, ymax51, ymax52):
        ''' functino which handle how the graph is render for the first time with extent properlly set'''

        graph_axes_titles_xy = ['Altitude(Z)(m)']

        graph_axes_titles_x = [ 'Flow(Q)(m3/s)','Speed(V)(m/s)','Height(Z)(m)','Concentration(C)(mg/l)']

        for i in range(len(self.graph_xy)) :
            getattr(self,'axes'+str(self.graph_xy[i])).get_xaxis().set_visible(True)
            getattr(self,'axes'+str(self.graph_xy[i])).get_yaxis().set_visible(True)
            getattr(self,'axes'+str(self.graph_xy[i])).grid(True)
            getattr(self,'axes'+str(self.graph_xy[i])).set_ylabel(tr(graph_axes_titles_xy[i]))
            getattr(self,'axes'+str(self.graph_xy[i])).set_xlabel(tr('pk(km)'))
            getattr(self,'axes'+str(self.graph_xy[i])).relim()
            getattr(self,'axes'+str(self.graph_xy[i])).autoscale(enable=True, axis='both', tight=True)

        for i in range(len(self.graph_x)) :
            getattr(self,'axes'+str(self.graph_x[i])).get_xaxis().set_visible(True)
            getattr(self,'axes'+str(self.graph_x[i])).get_yaxis().set_visible(True)
            getattr(self,'axes'+str(self.graph_x[i])).grid(True)
            getattr(self,'axes'+str(self.graph_x[i])).set_ylabel(tr(graph_axes_titles_x[i]))
            getattr(self,'axes'+str(self.graph_x[i])).set_xlabel(tr('pk(km)'))
            getattr(self,'axes'+str(self.graph_x[i])).relim()
            getattr(self,'axes'+str(self.graph_x[i])).autoscale(enable=True, axis='both', tight=True)

        # twinx axes for 22
        self.axes22.grid(True)
        self.axes22.set_xlabel(tr('pk(km)'))
        self.axes22.get_xaxis().set_visible(True)
        self.axes22.get_yaxis().set_visible(True)
        self.axes22.grid(True)
        self.axes22.set_xlabel(tr('pk(km)'))
        self.axes22.set_ylabel(tr('Flow(Qs)(m3/s)'))
        self.axes22.relim()

        # twinx axes for 52
        self.axes52.grid(True)
        self.axes52.set_xlabel(tr('pk(km)'))
        self.axes52.get_xaxis().set_visible(True)
        self.axes52.get_yaxis().set_visible(True)
        self.axes52.grid(True)
        self.axes52.set_xlabel(tr('pk(km)'))
        self.axes52.set_ylabel(tr('Concentration(C)(npp/100ml)'))
        self.axes52.relim()

        self.__x_max_graph = self.axes11.get_xlim()[1]

        self.axes21.set_ylim(0,ymaxQ)
        self.axes21.set_xlim(0,xmaxQ)

        self.axes22.set_ylim(0,ymaxQs)
        self.axes41.set_ylim(0,ymaxHs)

        self.axes51.set_ylim(0,ymax51)
        self.axes52.set_ylim(0,ymax52)

        self.axes31.set_ylim(0,ymaxV)
        self.axes31.set_xlim(0,xmaxV)

        self.__canvas.draw()
        self.__emit_range_change()

    def export_image(self):
        ''' export an image '''
        buf = io.BytesIO()
        self.__fig.savefig(buf)
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard )
        cb.setImage(QImage.fromData(buf.getvalue()), mode=cb.Clipboard)
        buf.close()
