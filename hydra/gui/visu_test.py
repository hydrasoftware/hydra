# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from __future__ import absolute_import # important to read the doc !
from __future__ import print_function
from qgis.PyQt.QtWidgets import QApplication
from qgis.PyQt.QtCore import QCoreApplication
from hydra.project import Project
from hydra.gui import visu_tool
from hydra.database.database import TestProject, remove_project, project_exists
import tempfile
import os, sys
import re
import difflib

if not project_exists("template_project"):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()

project_name = "visu_test"
testproject = TestProject(project_name, with_model=True)

obj_project = Project.load_project(project_name)
obj_project.set_current_model("model")

app = QApplication(sys.argv)
visutool = visu_tool.VisuTool(obj_project, [0,0], 10)

tempdir = tempfile.gettempdir()

doc = "visu tool elements\n"
doc +="------------------\n\n\n"
doc += "========================================  =====  =========  =====================  =====================  =====================  =====================\n"
doc += "table                                     file   abrev      param1                 param2                 param3                 param4 \n"
doc += "========================================  =====  =========  =====================  =====================  =====================  =====================\n"

elements = sorted(visu_tool.elements)

for name in elements:
    element = visu_tool.elements[name]
    results=""
    for n in element["results"]:
        if n is None:
            n = "None"
        results += "  " + n + (21-len(n))*" "
    doc += name + (40-len(name))*" " \
            + "  " + element["file"] + (5-len(element["file"]))*" " \
            + "  " + element["abbreviation"] + (9-len(element["abbreviation"]))*" " \
            + results \
            + "\n"

doc += "========================================  =====  =========  =====================  =====================  ===============  =====================n"

gen_filename = os.path.join(tempdir, 'visu_elements.rst')
ref_filename = os.path.join(os.path.dirname(__file__), 'visu_elements.rst')

open(gen_filename, 'w').write(doc)

ref = [line for line in open(ref_filename).readlines() if not re.match(r'^(=+ +)+=+$', line)]
gen = [line for line in open(gen_filename).readlines() if not re.match(r'^(=+ +)+=+$', line)]
diff = difflib.unified_diff(ref, gen, fromfile=ref_filename, tofile=gen_filename)
nodiff = True

for line in diff:
    nodiff &= False

if not nodiff:
    sys.stderr.write("found diff {} {}\n".format(ref_filename, gen_filename))
    sys.stderr.write("failed: differences found between generated documentation and specs\n")
    exit(1)

# fix_print_with_import
print("ok")

