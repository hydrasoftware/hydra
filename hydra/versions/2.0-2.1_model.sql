/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update $model.metadata set version = '2.1';
;;

/* ********************************************** */
/*            River node Z_INVERT                 */
/* ********************************************** */

drop view if exists ${model}.river_node cascade;

create or replace view ${model}.river_node as
    select
        p.id,
        p.name,
        c.reach,
        c.z_ground,
        c.area,
        ${model}.river_node_pk(p.id) as pk_km,
        p.geom,
        p.configuration::character varying AS configuration,
        p.generated,
        p.validity,
        p.configuration AS configuration_json
    from ${model}._river_node c,
        ${model}._node p
    where p.id = c.id
;;

create or replace function ${model}.river_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into ${model}._node(node_type, name, geom, configuration, generated)
                values('river', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update ${model}._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'river') where name = 'define_later' and id = id_;
            insert into ${model}._river_node(id, node_type, reach, z_ground, area)
                values (id_, 'river', coalesce(new.reach, (select id from ${model}.reach where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) asc limit 1)), coalesce(new.z_ground, (select project.altitude(new.geom))), coalesce(new.area, 1));
            perform ${model}.add_configuration_fct(new.configuration::json, id_, 'river_node');

            update ${model}._node set validity = (select (z_ground is not null) and (area is not null) and (area>0) and (reach is not null) and ((select not exists(select 1 from ${model}.station where st_intersects(geom, new.geom)))) from  ${model}._river_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.z_ground, new.area) is distinct from (old.z_ground, old.area)) then
                select is_switching from ${model}.config_switch into switching;
                if switching=false then
                    select name from ${model}.configuration as c, ${model}.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_ground, old.area) as o, (select new.z_ground, new.area) as n into new_config;
                        update ${model}._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_ground, new.area) n into new_config;
                        update ${model}._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update ${model}._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update ${model}._river_node set reach=new.reach, z_ground=new.z_ground, area=new.area where id=old.id;
            perform ${model}.add_configuration_fct(new.configuration::json, old.id, 'river_node');

            update ${model}._node set validity = (select (z_ground is not null) and (area is not null) and (area>0) and (reach is not null) and ((select not exists(select 1 from ${model}.station where st_intersects(geom, new.geom)))) from  ${model}._river_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from ${model}._river_node where id=old.id;
            delete from ${model}._node where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_river_node_trig
    instead of insert or update or delete on ${model}.river_node
    for each row execute procedure ${model}.river_node_fct();
;;

--results view
create or replace view ${model}.results_river as
    select
        n.id,
        UPPER(n.name)::varchar(24) as name,
        n.geom,
        case when v.id is not null then 'valley'
             when o.id is not null then 'channel'
             else 'closed'
        end as section_type
    from ${model}.river_node as n
    left join ${model}.open_reach as v on ST_DWithin(v.geom, n.geom, 0.001)
    left join ${model}.channel_reach as o on ST_DWithin(o.geom, n.geom, 0.001);
;;

--invalid view
create or replace view ${model}.invalid as
 with node_invalidity_reason as (
         select new_1.id,
            (
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   (area > 0)   '::text
                end) ||
                case
                    when new_1.z_ground is not null then ''::text
                    else '   z_ground is not null   '::text
                end as reason
           from ${model}.manhole_hydrology_node new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((((((((((((((((((((((((
                case
                    when new_1.area_ha is not null then ''::text
                    else '   area_ha is not null   '::text
                end ||
                case
                    when new_1.area_ha > 0::double precision then ''::text
                    else '   area_ha>0   '::text
                end) ||
                case
                    when new_1.rl is not null then ''::text
                    else '   rl is not null   '::text
                end) ||
                case
                    when new_1.rl > 0::double precision then ''::text
                    else '   rl>0   '::text
                end) ||
                case
                    when new_1.slope is not null then ''::text
                    else '   slope is not null   '::text
                end) ||
                case
                    when new_1.c_imp is not null then ''::text
                    else '   c_imp is not null   '::text
                end) ||
                case
                    when new_1.c_imp >= 0::double precision then ''::text
                    else '   c_imp>=0   '::text
                end) ||
                case
                    when new_1.c_imp <= 1::double precision then ''::text
                    else '   c_imp<=1   '::text
                end) ||
                case
                    when new_1.netflow_type is not null then ''::text
                    else '   netflow_type is not null   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'constant_runoff'::hydra_netflow_type or new_1.constant_runoff is not null and new_1.constant_runoff >= 0::double precision and new_1.constant_runoff <= 1::double precision then ''::text
                    else '   netflow_type!=''constant_runoff'' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'horner'::hydra_netflow_type or new_1.horner_ini_loss_coef is not null and new_1.horner_ini_loss_coef >= 0::double precision then ''::text
                    else '   netflow_type!=''horner'' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'horner'::hydra_netflow_type or new_1.horner_recharge_coef is not null and new_1.horner_recharge_coef >= 0::double precision then ''::text
                    else '   netflow_type!=''horner'' or (horner_recharge_coef is not null and horner_recharge_coef>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'holtan'::hydra_netflow_type or new_1.holtan_sat_inf_rate_mmh is not null and new_1.holtan_sat_inf_rate_mmh >= 0::double precision then ''::text
                    else '   netflow_type!=''holtan'' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'holtan'::hydra_netflow_type or new_1.holtan_dry_inf_rate_mmh is not null and new_1.holtan_dry_inf_rate_mmh >= 0::double precision then ''::text
                    else '   netflow_type!=''holtan'' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'holtan'::hydra_netflow_type or new_1.holtan_soil_storage_cap_mm is not null and new_1.holtan_soil_storage_cap_mm >= 0::double precision then ''::text
                    else '   netflow_type!=''holtan'' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'scs'::hydra_netflow_type or new_1.scs_j_mm is not null and new_1.scs_j_mm >= 0::double precision then ''::text
                    else '   netflow_type!=''scs'' or (scs_j_mm is not null and scs_j_mm>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'scs'::hydra_netflow_type or new_1.scs_soil_drainage_time_day is not null and new_1.scs_soil_drainage_time_day >= 0::double precision then ''::text
                    else '   netflow_type!=''scs'' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'scs'::hydra_netflow_type or new_1.scs_rfu_mm is not null and new_1.scs_rfu_mm >= 0::double precision then ''::text
                    else '   netflow_type!=''scs'' or (scs_rfu_mm is not null and scs_rfu_mm>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'hydra'::hydra_netflow_type or new_1.hydra_surface_soil_storage_rfu_mm is not null and new_1.hydra_surface_soil_storage_rfu_mm >= 0::double precision then ''::text
                    else '   netflow_type!=''hydra'' or (hydra_surface_soil_storage_rfu_mm is not null and hydra_surface_soil_storage_rfu_mm>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'hydra'::hydra_netflow_type or new_1.hydra_inf_rate_f0_mm_day is not null and new_1.hydra_inf_rate_f0_mm_day >= 0::double precision then ''::text
                    else '   netflow_type!=''hydra'' or (hydra_inf_rate_f0_mm_day is not null and hydra_inf_rate_f0_mm_day>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'hydra'::hydra_netflow_type or new_1.hydra_int_soil_storage_j_mm is not null and new_1.hydra_int_soil_storage_j_mm >= 0::double precision then ''::text
                    else '   netflow_type!=''hydra'' or (hydra_int_soil_storage_j_mm is not null and hydra_int_soil_storage_j_mm>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'hydra'::hydra_netflow_type or new_1.hydra_soil_drainage_time_qres_day is not null and new_1.hydra_soil_drainage_time_qres_day >= 0::double precision then ''::text
                    else '   netflow_type!=''hydra'' or (hydra_soil_drainage_time_qres_day is not null and hydra_soil_drainage_time_qres_day>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'gr4'::hydra_netflow_type or new_1.gr4_k1 is not null and new_1.gr4_k1 >= 0::double precision then ''::text
                    else '   netflow_type!=''gr4'' or (gr4_k1 is not null and gr4_k1>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'gr4'::hydra_netflow_type or new_1.gr4_k2 is not null then ''::text
                    else '   netflow_type!=''gr4'' or (gr4_k2 is not null)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'gr4'::hydra_netflow_type or new_1.gr4_k3 is not null and new_1.gr4_k3 >= 0::double precision then ''::text
                    else '   netflow_type!=''gr4'' or (gr4_k3 is not null and gr4_k3>=0)   '::text
                end) ||
                case
                    when new_1.netflow_type <> 'gr4'::hydra_netflow_type or new_1.gr4_k4 is not null and new_1.gr4_k4 >= 0::double precision then ''::text
                    else '   netflow_type!=''gr4'' or (gr4_k4 is not null and gr4_k4>=0)   '::text
                end) ||
                case
                    when new_1.runoff_type is not null or new_1.netflow_type = 'gr4'::hydra_netflow_type then ''::text
                    else '   runoff_type is not null or netflow_type=''gr4''   '::text
                end) ||
                case
                    when new_1.runoff_type <> 'socose'::hydra_runoff_type or new_1.socose_tc_mn is not null and new_1.socose_tc_mn > 0::double precision or new_1.netflow_type = 'gr4'::hydra_netflow_type then ''::text
                    else '   runoff_type!=''socose'' or (socose_tc_mn is not null and socose_tc_mn>0) or netflow_type=''gr4''   '::text
                end) ||
                case
                    when new_1.runoff_type <> 'socose'::hydra_runoff_type or new_1.socose_shape_param_beta is not null and new_1.socose_shape_param_beta >= 1::double precision and new_1.socose_shape_param_beta <= 6::double precision or new_1.netflow_type = 'gr4'::hydra_netflow_type then ''::text
                    else '   runoff_type!=''socose'' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6) or netflow_type=''gr4''   '::text
                end) ||
                case
                    when new_1.runoff_type <> 'define_k'::hydra_runoff_type or new_1.define_k_mn is not null and new_1.define_k_mn > 0::double precision or new_1.netflow_type = 'gr4'::hydra_netflow_type then ''::text
                    else '   runoff_type!=''define_k'' or (define_k_mn is not null and define_k_mn>0) or netflow_type=''gr4''   '::text
                end) ||
                case
                    when new_1.q_limit is not null then ''::text
                    else '   q_limit is not null   '::text
                end) ||
                case
                    when new_1.q0 is not null then ''::text
                    else '   q0 is not null   '::text
                end) ||
                case
                    when new_1.network_type is not null then ''::text
                    else '   network_type is not null   '::text
                end) ||
                case
                    when new_1.rural_land_use is null or new_1.rural_land_use >= 0::double precision then ''::text
                    else '   rural_land_use is null or rural_land_use>=0   '::text
                end) ||
                case
                    when new_1.industrial_land_use is null or new_1.industrial_land_use >= 0::double precision then ''::text
                    else '   industrial_land_use is null or industrial_land_use>=0   '::text
                end) ||
                case
                    when new_1.suburban_housing_land_use is null or new_1.suburban_housing_land_use >= 0::double precision then ''::text
                    else '   suburban_housing_land_use is null or suburban_housing_land_use>=0   '::text
                end) ||
                case
                    when new_1.dense_housing_land_use is null or new_1.dense_housing_land_use >= 0::double precision then ''::text
                    else '   dense_housing_land_use is null or dense_housing_land_use>=0   '::text
                end as reason
           from ${model}.catchment_node new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   area>0   '::text
                end) ||
                case
                    when new_1.zb is not null then ''::text
                    else '   zb is not null   '::text
                end) ||
                case
                    when new_1.rk is not null then ''::text
                    else '   rk is not null   '::text
                end) ||
                case
                    when new_1.rk > 0::double precision then ''::text
                    else '   rk>0   '::text
                end as reason
           from ${model}.elem_2d_node new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   (area > 0)   '::text
                end) ||
                case
                    when new_1.z_ground is not null then ''::text
                    else '   z_ground is not null   '::text
                end) ||
                case
                    when new_1.h is not null then ''::text
                    else '   h is not null   '::text
                end) ||
                case
                    when new_1.h >= 0::double precision then ''::text
                    else '   h>=0   '::text
                end as reason
           from ${model}.crossroad_node new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.zs_array is not null then ''::text
                    else '   zs_array is not null    '::text
                end ||
                case
                    when new_1.zini is not null then ''::text
                    else '   zini is not null   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) <= 10 then ''::text
                    else '   array_length(zs_array, 1) <= 10    '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) >= 1 then ''::text
                    else '   array_length(zs_array, 1) >= 1   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 2) = 2 then ''::text
                    else '   array_length(zs_array, 2) = 2   '::text
                end as reason
           from ${model}.storage_node new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((((
                case
                    when new_1.connection_law is not null then ''::text
                    else '   connection_law is not null   '::text
                end ||
                case
                    when new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type or new_1.cover_diameter is not null then ''::text
                    else '   connection_law != ''manhole_cover'' or cover_diameter is not null   '::text
                end) ||
                case
                    when new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type or new_1.cover_diameter > 0::double precision then ''::text
                    else '   connection_law != ''manhole_cover'' or cover_diameter > 0   '::text
                end) ||
                case
                    when new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type or new_1.cover_critical_pressure is not null then ''::text
                    else '   connection_law != ''manhole_cover'' or cover_critical_pressure is not null   '::text
                end) ||
                case
                    when new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type or new_1.cover_critical_pressure > 0::double precision then ''::text
                    else '   connection_law != ''manhole_cover'' or cover_critical_pressure >0   '::text
                end) ||
                case
                    when new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type or new_1.inlet_width is not null then ''::text
                    else '   connection_law != ''drainage_inlet'' or inlet_width is not null   '::text
                end) ||
                case
                    when new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type or new_1.inlet_width > 0::double precision then ''::text
                    else '   connection_law != ''drainage_inlet'' or inlet_width >0   '::text
                end) ||
                case
                    when new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type or new_1.inlet_height is not null then ''::text
                    else '   connection_law != ''drainage_inlet'' or inlet_height is not null   '::text
                end) ||
                case
                    when new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type or new_1.inlet_height > 0::double precision then ''::text
                    else '   connection_law != ''drainage_inlet'' or inlet_height >0   '::text
                end) ||
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end) ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   area>0   '::text
                end) ||
                case
                    when new_1.z_ground is not null then ''::text
                    else '   z_ground is not null   '::text
                end) ||
                case
                    when ( select not (exists ( select 1
                               from ${model}.station
                              where st_intersects(station.geom, new_1.geom)))) then ''::text
                    else '   (select not exists(select 1 from ${model}.station where st_intersects(geom, new.geom)))   '::text
                end) ||
                case
                    when ( select (exists ( select 1
                               from ${model}.pipe_link
                              where pipe_link.up = new_1.id or pipe_link.down = new_1.id)) as "exists") then ''::text
                    else '   (select exists (select 1 from ${model}.pipe_link where up=new.id or down=new.id))   '::text
                end as reason
           from ${model}.manhole_node new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   area>0   '::text
                end) ||
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end) ||
                case
                    when new_1.station is not null then ''::text
                    else '   station is not null    '::text
                end) ||
                case
                    when ( select st_intersects(new_1.geom, station.geom) as st_intersects
                       from ${model}.station
                      where station.id = new_1.station) then ''::text
                    else '   (select st_intersects(new.geom, geom) from ${model}.station where id=new.station)   '::text
                end as reason
           from ${model}.station_node new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.z_ground is not null then ''::text
                    else '   z_ground is not null   '::text
                end ||
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end) ||
                case
                    when new_1.area > 0::double precision then ''::text
                    else '   area>0   '::text
                end) ||
                case
                    when new_1.reach is not null then ''::text
                    else '   reach is not null   '::text
                end) ||
                case
                    when ( select not (exists ( select 1
                               from ${model}.station
                              where st_intersects(station.geom, new_1.geom)))) then ''::text
                    else '   (select not exists(select 1 from ${model}.station where st_intersects(geom, new.geom)))   '::text
                end as reason
           from ${model}.river_node new_1
          where not new_1.validity
        ), link_invalidity_reason as (
         select new_1.id,
            ((((
                case
                    when new_1.law_type is not null then ''::text
                    else '   law_type is not null   '::text
                end ||
                case
                    when new_1.param is not null then ''::text
                    else '   param is not null   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.borda_headloss_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.lateral_contraction_coef is not null then ''::text
                    else '   lateral_contraction_coef is not null   '::text
                end) ||
                case
                    when new_1.lateral_contraction_coef <= 1::double precision then ''::text
                    else '   lateral_contraction_coef<=1   '::text
                end) ||
                case
                    when new_1.lateral_contraction_coef >= 0::double precision then ''::text
                    else '   lateral_contraction_coef>=0   '::text
                end) ||
                case
                    when st_npoints(new_1.border) = 2 then ''::text
                    else '   st_npoints(border)=2   '::text
                end) ||
                case
                    when st_isvalid(new_1.border) then ''::text
                    else '   st_isvalid(border)   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.mesh_2d_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.z_ceiling is not null then ''::text
                    else '   z_ceiling is not null   '::text
                end) ||
                case
                    when new_1.z_ceiling > new_1.z_invert then ''::text
                    else '   z_ceiling>z_invert   '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc <=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc >=0   '::text
                end) ||
                case
                    when new_1.action_gate_type is not null then ''::text
                    else '   action_gate_type is not null   '::text
                end) ||
                case
                    when new_1.mode_valve is not null then ''::text
                    else '   mode_valve is not null   '::text
                end) ||
                case
                    when new_1.z_gate is not null then ''::text
                    else '   z_gate is not null   '::text
                end) ||
                case
                    when new_1.z_gate >= new_1.z_invert then ''::text
                    else '   z_gate>=z_invert   '::text
                end) ||
                case
                    when new_1.z_gate <= new_1.z_ceiling then ''::text
                    else '   z_gate<=z_ceiling   '::text
                end) ||
                case
                    when new_1.v_max_cms is not null then ''::text
                    else '   v_max_cms is not null   '::text
                end) ||
                case
                    when new_1.cc_submerged > 0::double precision then ''::text
                    else '   cc_submerged >0   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.gate_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((((((((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.z_ceiling is not null then ''::text
                    else '   z_ceiling is not null   '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc>=0   '::text
                end) ||
                case
                    when new_1.action_gate_type is not null then ''::text
                    else '   action_gate_type is not null   '::text
                end) ||
                case
                    when new_1.z_invert_stop is not null then ''::text
                    else '   z_invert_stop is not null   '::text
                end) ||
                case
                    when new_1.z_ceiling_stop is not null then ''::text
                    else '   z_ceiling_stop is not null   '::text
                end) ||
                case
                    when new_1.v_max_cms is not null then ''::text
                    else '   v_max_cms is not null   '::text
                end) ||
                case
                    when new_1.v_max_cms >= 0::double precision then ''::text
                    else '   v_max_cms>=0   '::text
                end) ||
                case
                    when new_1.dt_regul_hr is not null then ''::text
                    else '   dt_regul_hr is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_control_node is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_pid_array is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_tz_array is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 1) <= 10 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 1) >= 1 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 2) = 2 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or new_1.q_z_crit is not null then ''::text
                    else '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 1) <= 10 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 1) >= 1 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 2) = 2 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul or new_1.nr_z_gate is not null then ''::text
                    else '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                end) ||
                case
                    when new_1.cc_submerged > 0::double precision then ''::text
                    else '   cc_submerged >0   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.regul_gate_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.down_type = 'manhole_hydrology'::hydra_node_type or (new_1.down_type = any (array['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'storage'::hydra_node_type, 'crossroad'::hydra_node_type, 'elem_2d'::hydra_node_type])) and new_1.hydrograph is not null then ''::text
                    else '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'', ''storage'', ''crossroad'', ''elem_2d'') and (hydrograph is not null))   '::text
                end ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.connector_hydrology_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((((((((
                case
                    when new_1.z_invert_up is not null then ''::text
                    else '   z_invert_up is not null   '::text
                end ||
                case
                    when new_1.z_invert_down is not null then ''::text
                    else '   z_invert_down is not null   '::text
                end) ||
                case
                    when new_1.h_sable is not null then ''::text
                    else '   h_sable is not null   '::text
                end) ||
                case
                    when new_1.cross_section_type is not null then ''::text
                    else '   cross_section_type is not null   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'valley'::hydra_cross_section_type then ''::text
                    else '   cross_section_type not in (''valley'')   '::text
                end) ||
                case
                    when new_1.rk is not null then ''::text
                    else '   rk is not null   '::text
                end) ||
                case
                    when new_1.rk > 0::double precision then ''::text
                    else '   rk >0   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'circular'::hydra_cross_section_type or new_1.circular_diameter is not null and new_1.circular_diameter > 0::double precision then ''::text
                    else '   cross_section_type!=''circular'' or (circular_diameter is not null and circular_diameter > 0)   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type or new_1.ovoid_top_diameter is not null and new_1.ovoid_top_diameter > 0::double precision then ''::text
                    else '   cross_section_type!=''ovoid'' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type or new_1.ovoid_invert_diameter is not null and new_1.ovoid_invert_diameter > 0::double precision then ''::text
                    else '   cross_section_type!=''ovoid'' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type or new_1.ovoid_height is not null and new_1.ovoid_height > 0::double precision then ''::text
                    else '   cross_section_type!=''ovoid'' or (ovoid_height is not null and ovoid_height >0 )   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type or new_1.ovoid_height > ((new_1.ovoid_top_diameter + new_1.ovoid_invert_diameter) / 2::double precision) then ''::text
                    else '   cross_section_type!=''ovoid'' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'pipe'::hydra_cross_section_type or new_1.cp_geom is not null then ''::text
                    else '   cross_section_type!=''pipe'' or cp_geom is not null   '::text
                end) ||
                case
                    when new_1.cross_section_type <> 'channel'::hydra_cross_section_type or new_1.op_geom is not null then ''::text
                    else '   cross_section_type!=''channel'' or op_geom is not null   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.pipe_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((((((((((((((((
                case
                    when new_1.z_crest1 is not null then ''::text
                    else '   z_crest1 is not null   '::text
                end ||
                case
                    when new_1.width1 is not null then ''::text
                    else '   width1 is not null   '::text
                end) ||
                case
                    when new_1.width1 >= 0::double precision then ''::text
                    else '   width1>=0   '::text
                end) ||
                case
                    when new_1.z_crest2 is not null then ''::text
                    else '   z_crest2 is not null   '::text
                end) ||
                case
                    when new_1.z_crest2 >= new_1.z_crest1 then ''::text
                    else '   z_crest2>=z_crest1   '::text
                end) ||
                case
                    when new_1.width2 is not null then ''::text
                    else '   width2 is not null   '::text
                end) ||
                case
                    when new_1.width2 >= 0::double precision then ''::text
                    else '   width2>=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc>=0   '::text
                end) ||
                case
                    when new_1.lateral_contraction_coef is not null then ''::text
                    else '   lateral_contraction_coef is not null   '::text
                end) ||
                case
                    when new_1.lateral_contraction_coef <= 1::double precision then ''::text
                    else '   lateral_contraction_coef<=1   '::text
                end) ||
                case
                    when new_1.lateral_contraction_coef >= 0::double precision then ''::text
                    else '   lateral_contraction_coef>=0   '::text
                end) ||
                case
                    when new_1.break_mode is not null then ''::text
                    else '   break_mode is not null   '::text
                end) ||
                case
                    when new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode or new_1.z_break is not null then ''::text
                    else '   break_mode!=''zw_critical'' or z_break is not null   '::text
                end) ||
                case
                    when new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode or new_1.t_break is not null then ''::text
                    else '   break_mode!=''time_critical'' or t_break is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.width_breach is not null then ''::text
                    else '   break_mode=''none'' or width_breach is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.z_invert is not null then ''::text
                    else '   break_mode=''none'' or z_invert is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp is not null then ''::text
                    else '   break_mode=''none'' or grp is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp > 0 then ''::text
                    else '   break_mode=''none'' or grp>0   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp < 100 then ''::text
                    else '   break_mode=''none'' or grp<100   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.dt_fracw_array is not null then ''::text
                    else '   break_mode=''none'' or dt_fracw_array is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 1) <= 10 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 1) >= 1 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 2) = 2 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.overflow_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((
                case
                    when new_1.q_pump is not null then ''::text
                    else '   q_pump is not null   '::text
                end ||
                case
                    when new_1.q_pump >= 0::double precision then ''::text
                    else '   q_pump>= 0   '::text
                end) ||
                case
                    when new_1.qz_array is not null then ''::text
                    else '   qz_array is not null   '::text
                end) ||
                case
                    when array_length(new_1.qz_array, 1) <= 10 then ''::text
                    else '   array_length(qz_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.qz_array, 1) >= 1 then ''::text
                    else '   array_length(qz_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.qz_array, 2) = 2 then ''::text
                    else '   array_length(qz_array, 2)=2   '::text
                end) ||
                case
                    when ( select bool_and(data_set.bool) as bool_and
                       from ( select 0::double precision <= unnest(new_1.qz_array[:][1:1]) as bool) data_set) then ''::text
                    else '   (/*   qz_array: 0<=q   */ select bool_and(bool) from (select 0<=unnest(new.qz_array[:][1]) as bool) as data_set)   '::text
                end) ||
                case
                    when ( select bool_and(data_set.bool) as bool_and
                       from ( select unnest(new_1.qz_array[:][1:1]) <= 1::double precision as bool) data_set) then ''::text
                    else '   (/*   qz_array: q<=1   */ select bool_and(bool) from (select unnest(new.qz_array[:][1])<=1 as bool) as data_set)   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.deriv_pump_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null    '::text
                end ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc>=0   '::text
                end) ||
                case
                    when new_1.v_max_cms is not null then ''::text
                    else '   v_max_cms is not null   '::text
                end) ||
                case
                    when new_1.v_max_cms >= 0::double precision then ''::text
                    else '   v_max_cms>=0   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.weir_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((
                case
                    when new_1.cross_section is not null then ''::text
                    else '   cross_section is not null   '::text
                end ||
                case
                    when new_1.cross_section >= 0::double precision then ''::text
                    else '   cross_section>=0   '::text
                end) ||
                case
                    when new_1.length is not null then ''::text
                    else '   length is not null   '::text
                end) ||
                case
                    when new_1.length >= 0::double precision then ''::text
                    else '   length>=0   '::text
                end) ||
                case
                    when new_1.slope is not null then ''::text
                    else '   slope is not null   '::text
                end) ||
                case
                    when new_1.down_type = 'manhole_hydrology'::hydra_node_type or (new_1.down_type = any (array['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'crossroad'::hydra_node_type, 'storage'::hydra_node_type, 'elem_2d'::hydra_node_type])) and new_1.hydrograph is not null then ''::text
                    else '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'', ''crossroad'', ''storage'', ''elem_2d'') and (hydrograph is not null))   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.routing_hydrology_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end) ||
                case
                    when new_1.transmitivity is not null then ''::text
                    else '   transmitivity is not null   '::text
                end) ||
                case
                    when new_1.transmitivity >= 0::double precision then ''::text
                    else '   transmitivity>=0   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.porous_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((
                case
                    when new_1.npump is not null then ''::text
                    else '   npump is not null   '::text
                end ||
                case
                    when new_1.npump <= 10 then ''::text
                    else '   npump<=10   '::text
                end) ||
                case
                    when new_1.npump >= 1 then ''::text
                    else '   npump>=1   '::text
                end) ||
                case
                    when new_1.zregul_array is not null then ''::text
                    else '   zregul_array is not null   '::text
                end) ||
                case
                    when new_1.hq_array is not null then ''::text
                    else '   hq_array is not null   '::text
                end) ||
                case
                    when array_length(new_1.zregul_array, 1) = new_1.npump then ''::text
                    else '   array_length(zregul_array, 1)=npump   '::text
                end) ||
                case
                    when array_length(new_1.zregul_array, 2) = 2 then ''::text
                    else '   array_length(zregul_array, 2)=2   '::text
                end) ||
                case
                    when array_length(new_1.hq_array, 1) = new_1.npump then ''::text
                    else '   array_length(hq_array, 1)=npump   '::text
                end) ||
                case
                    when array_length(new_1.hq_array, 2) <= 10 then ''::text
                    else '   array_length(hq_array, 2)<=10   '::text
                end) ||
                case
                    when array_length(new_1.hq_array, 2) >= 1 then ''::text
                    else '   array_length(hq_array, 2)>=1   '::text
                end) ||
                case
                    when array_length(new_1.hq_array, 3) = 2 then ''::text
                    else '   array_length(hq_array, 3)=2   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.pump_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((
                case
                    when new_1.z_crest1 is not null then ''::text
                    else '   z_crest1 is not null   '::text
                end ||
                case
                    when new_1.width1 is not null then ''::text
                    else '   width1 is not null   '::text
                end) ||
                case
                    when new_1.width1 > 0::double precision then ''::text
                    else '   width1>0   '::text
                end) ||
                case
                    when new_1.length is not null then ''::text
                    else '   length is not null   '::text
                end) ||
                case
                    when new_1.length > 0::double precision then ''::text
                    else '   length>0   '::text
                end) ||
                case
                    when new_1.rk is not null then ''::text
                    else '   rk is not null   '::text
                end) ||
                case
                    when new_1.rk >= 0::double precision then ''::text
                    else '   rk>=0   '::text
                end) ||
                case
                    when new_1.z_crest2 is not null then ''::text
                    else '   z_crest2 is not null   '::text
                end) ||
                case
                    when new_1.z_crest2 > new_1.z_crest1 then ''::text
                    else '   z_crest2>z_crest1   '::text
                end) ||
                case
                    when new_1.width2 is not null then ''::text
                    else '   width2 is not null   '::text
                end) ||
                case
                    when new_1.width2 > new_1.width1 then ''::text
                    else '   width2>width1   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.strickler_link new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= '0'::numeric::double precision then ''::text
                    else '   width>=0.   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= '0'::numeric::double precision then ''::text
                    else '   cc>=0.   '::text
                end) ||
                case
                    when new_1.break_mode is not null then ''::text
                    else '   break_mode is not null   '::text
                end) ||
                case
                    when new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode or new_1.z_break is not null then ''::text
                    else '   break_mode!=''zw_critical'' or z_break is not null   '::text
                end) ||
                case
                    when new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode or new_1.t_break is not null then ''::text
                    else '   break_mode!=''time_critical'' or t_break is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp is not null then ''::text
                    else '   break_mode=''none'' or grp is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp > 0 then ''::text
                    else '   break_mode=''none'' or grp>0   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.grp < 100 then ''::text
                    else '   break_mode=''none'' or grp<100   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or new_1.dt_fracw_array is not null then ''::text
                    else '   break_mode=''none'' or dt_fracw_array is not null   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 1) <= 10 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 1) >= 1 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode or array_length(new_1.dt_fracw_array, 2) = 2 then ''::text
                    else '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.fuse_spillway_link new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((
                case
                    when new_1.z_overflow is not null then ''::text
                    else '   z_overflow is not null   '::text
                end ||
                case
                    when new_1.area is not null then ''::text
                    else '   area is not null   '::text
                end) ||
                case
                    when new_1.area >= 0::double precision then ''::text
                    else '   area>=0   '::text
                end) ||
                case
                    when new_1.up is not null then ''::text
                    else '   up is not null   '::text
                end) ||
                case
                    when new_1.down is not null then ''::text
                    else '   down is not null   '::text
                end) ||
                case
                    when new_1.up_type is not null then ''::text
                    else '   up_type is not null   '::text
                end) ||
                case
                    when new_1.down_type is not null then ''::text
                    else '   down_type is not null   '::text
                end as reason
           from ${model}.network_overflow_link new_1
          where not new_1.validity
        ), singularity_invalidity_reason as (
         select new_1.id,
            (
                case
                    when new_1.law_type is not null then ''::text
                    else '   law_type is not null   '::text
                end ||
                case
                    when new_1.param is not null then ''::text
                    else '   param is not null   '::text
                end) ||
                case
                    when not ${model}.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from ${model}.borda_headloss_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (
                case
                    when new_1.pk0_km is not null then ''::text
                    else '   pk0_km is not null   '::text
                end ||
                case
                    when new_1.dx is not null then ''::text
                    else '   dx is not null   '::text
                end) ||
                case
                    when new_1.dx >= 0.1::double precision then ''::text
                    else '   dx>=0.1   '::text
                end as reason
           from ${model}.pipe_branch_marker_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.z_ceiling is not null then ''::text
                    else '   z_ceiling is not null   '::text
                end) ||
                case
                    when new_1.z_ceiling > new_1.z_invert then ''::text
                    else '   z_ceiling>z_invert   '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc <=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc >=0   '::text
                end) ||
                case
                    when new_1.action_gate_type is not null then ''::text
                    else '   action_gate_type is not null   '::text
                end) ||
                case
                    when new_1.mode_valve is not null then ''::text
                    else '   mode_valve is not null   '::text
                end) ||
                case
                    when new_1.z_gate is not null then ''::text
                    else '   z_gate is not null   '::text
                end) ||
                case
                    when new_1.z_gate >= new_1.z_invert then ''::text
                    else '   z_gate>=z_invert   '::text
                end) ||
                case
                    when new_1.z_gate <= new_1.z_ceiling then ''::text
                    else '   z_gate<=z_ceiling   '::text
                end) ||
                case
                    when new_1.v_max_cms is not null then ''::text
                    else '   v_max_cms is not null   '::text
                end) ||
                case
                    when not ${model}.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end) ||
                case
                    when new_1.cc_submerged > 0::double precision then ''::text
                    else '   cc_submerged >0   '::text
                end as reason
           from ${model}.gate_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((
                case
                    when new_1.l_road is not null then ''::text
                    else '   l_road is not null   '::text
                end ||
                case
                    when new_1.l_road >= 0::double precision then ''::text
                    else '   l_road>=0   '::text
                end) ||
                case
                    when new_1.z_road is not null then ''::text
                    else '   z_road is not null   '::text
                end) ||
                case
                    when new_1.zw_array is not null then ''::text
                    else '   zw_array is not null    '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 1) <= 10 then ''::text
                    else '   array_length(zw_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 1) >= 1 then ''::text
                    else '   array_length(zw_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 2) = 2 then ''::text
                    else '   array_length(zw_array, 2)=2   '::text
                end) ||
                case
                    when not ${model}.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from ${model}.bridge_headloss_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((
                case
                    when new_1.storage_area is not null then ''::text
                    else '   storage_area is not null   '::text
                end ||
                case
                    when new_1.storage_area >= 0::double precision then ''::text
                    else '   storage_area>=0   '::text
                end) ||
                case
                    when new_1.constant_dry_flow is not null then ''::text
                    else '   constant_dry_flow is not null   '::text
                end) ||
                case
                    when new_1.constant_dry_flow >= 0::double precision then ''::text
                    else '   constant_dry_flow>=0   '::text
                end) ||
                case
                    when new_1.distrib_coef is null or new_1.distrib_coef >= 0::double precision then ''::text
                    else '   distrib_coef is null or distrib_coef>=0   '::text
                end) ||
                case
                    when new_1.lag_time_hr is null or new_1.lag_time_hr >= 0::double precision then ''::text
                    else '   lag_time_hr is null or (lag_time_hr>=0)   '::text
                end) ||
                case
                    when new_1.sector is null or new_1.distrib_coef is not null and new_1.lag_time_hr is not null then ''::text
                    else '   sector is null or (distrib_coef is not null and lag_time_hr is not null)   '::text
                end) ||
                case
                    when array_length(new_1.pollution_dryweather_runoff, 1) = 2 then ''::text
                    else '   array_length(pollution_dryweather_runoff, 1)=2   '::text
                end) ||
                case
                    when array_length(new_1.pollution_dryweather_runoff, 2) = 4 then ''::text
                    else '   array_length(pollution_dryweather_runoff, 2)=4   '::text
                end) ||
                case
                    when array_length(new_1.quality_dryweather_runoff, 1) = 2 then ''::text
                    else '   array_length(quality_dryweather_runoff, 1)=2   '::text
                end) ||
                case
                    when array_length(new_1.quality_dryweather_runoff, 2) = 9 then ''::text
                    else '   array_length(quality_dryweather_runoff, 2)=9   '::text
                end) ||
                case
                    when new_1.external_file_data or new_1.tq_array is not null then ''::text
                    else '   external_file_data or tq_array is not null   '::text
                end) ||
                case
                    when new_1.external_file_data or array_length(new_1.tq_array, 1) <= 10 then ''::text
                    else '   external_file_data or array_length(tq_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.external_file_data or array_length(new_1.tq_array, 1) >= 1 then ''::text
                    else '   external_file_data or array_length(tq_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.external_file_data or array_length(new_1.tq_array, 2) = 2 then ''::text
                    else '   external_file_data or array_length(tq_array, 2)=2   '::text
                end as reason
           from ${model}.hydrograph_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((
                case
                    when new_1.d_abutment_l is not null then ''::text
                    else '   d_abutment_l is not null   '::text
                end ||
                case
                    when new_1.d_abutment_r is not null then ''::text
                    else '   d_abutment_r is not null   '::text
                end) ||
                case
                    when new_1.abutment_type is not null then ''::text
                    else '   abutment_type is not null   '::text
                end) ||
                case
                    when new_1.zw_array is not null then ''::text
                    else '   zw_array is not null    '::text
                end) ||
                case
                    when new_1.z_ceiling is not null then ''::text
                    else '   z_ceiling is not null   '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 1) <= 10 then ''::text
                    else '   array_length(zw_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 1) >= 1 then ''::text
                    else '   array_length(zw_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zw_array, 2) = 2 then ''::text
                    else '   array_length(zw_array, 2)=2   '::text
                end) ||
                case
                    when not ${model}.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from ${model}.bradley_headloss_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((
                case
                    when new_1.zq_array is not null then ''::text
                    else '   zq_array is not null   '::text
                end ||
                case
                    when array_length(new_1.zq_array, 1) <= 10 then ''::text
                    else '   array_length(zq_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.zq_array, 1) >= 1 then ''::text
                    else '   array_length(zq_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zq_array, 2) = 2 then ''::text
                    else '   array_length(zq_array, 2)=2   '::text
                end as reason
           from ${model}.zq_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null    '::text
                end ||
                case
                    when new_1.z_regul is not null then ''::text
                    else '   z_regul is not null    '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc>=0   '::text
                end) ||
                case
                    when new_1.mode_regul is not null then ''::text
                    else '   mode_regul is not null   '::text
                end) ||
                case
                    when new_1.reoxy_law is not null then ''::text
                    else '   reoxy_law is not null   '::text
                end) ||
                case
                    when new_1.reoxy_param is not null then ''::text
                    else '   reoxy_param is not null   '::text
                end) ||
                case
                    when not ${model}.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from ${model}.zregul_weir_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((
                case
                    when new_1.z_weir is not null then ''::text
                    else '   z_weir is not null   '::text
                end ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc<=1   '::text
                end) ||
                case
                    when new_1.cc >= '0'::numeric::double precision then ''::text
                    else '   cc>=0.   '::text
                end as reason
           from ${model}.weir_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((
                case
                    when new_1.zs_array is not null then ''::text
                    else '   zs_array is not null    '::text
                end ||
                case
                    when new_1.zini is not null then ''::text
                    else '   zini is not null   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) <= 10 then ''::text
                    else '   array_length(zs_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) >= 1 then ''::text
                    else '   array_length(zs_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 2) = 2 then ''::text
                    else '   array_length(zs_array, 2)=2   '::text
                end) ||
                case
                    when new_1.treatment_mode is not null then ''::text
                    else '   treatment_mode is not null   '::text
                end as reason
           from ${model}.tank_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((
                case
                    when new_1.external_file_data or new_1.tz_array is not null then ''::text
                    else '   external_file_data or tz_array is not null   '::text
                end ||
                case
                    when new_1.external_file_data or array_length(new_1.tz_array, 1) <= 10 then ''::text
                    else '   external_file_data or array_length(tz_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.external_file_data or array_length(new_1.tz_array, 1) >= 1 then ''::text
                    else '   external_file_data or array_length(tz_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.external_file_data or array_length(new_1.tz_array, 2) = 2 then ''::text
                    else '   external_file_data or array_length(tz_array, 2)=2   '::text
                end as reason
           from ${model}.tz_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((((((
                case
                    when new_1.drainage is not null then ''::text
                    else '   drainage is not null   '::text
                end ||
                case
                    when new_1.overflow is not null then ''::text
                    else '   overflow is not null   '::text
                end) ||
                case
                    when new_1.q_drainage is not null then ''::text
                    else '   q_drainage is not null   '::text
                end) ||
                case
                    when new_1.q_drainage >= 0::double precision then ''::text
                    else '   q_drainage>=0   '::text
                end) ||
                case
                    when new_1.z_ini is not null then ''::text
                    else '   z_ini is not null   '::text
                end) ||
                case
                    when new_1.zs_array is not null then ''::text
                    else '   zs_array is not null    '::text
                end) ||
                case
                    when new_1.treatment_mode is not null then ''::text
                    else '   treatment_mode is not null   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) <= 10 then ''::text
                    else '   array_length(zs_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 1) >= 1 then ''::text
                    else '   array_length(zs_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zs_array, 2) = 2 then ''::text
                    else '   array_length(zs_array, 2)=2   '::text
                end as reason
           from ${model}.reservoir_rs_hydrology_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.qz_array is not null then ''::text
                    else '   qz_array is not null   '::text
                end ||
                case
                    when array_length(new_1.qz_array, 1) <= 10 then ''::text
                    else '   array_length(qz_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.qz_array, 1) >= 1 then ''::text
                    else '   array_length(qz_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.qz_array, 2) = 2 then ''::text
                    else '   array_length(qz_array, 2)=2   '::text
                end) ||
                case
                    when not ${model}.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from ${model}.hydraulic_cut_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((((((((((((((((((
                case
                    when new_1.z_invert is not null then ''::text
                    else '   z_invert is not null   '::text
                end ||
                case
                    when new_1.z_ceiling is not null then ''::text
                    else '   z_ceiling is not null   '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width >=0   '::text
                end) ||
                case
                    when new_1.cc is not null then ''::text
                    else '   cc is not null   '::text
                end) ||
                case
                    when new_1.cc <= 1::double precision then ''::text
                    else '   cc <=1   '::text
                end) ||
                case
                    when new_1.cc >= 0::double precision then ''::text
                    else '   cc >=0   '::text
                end) ||
                case
                    when new_1.action_gate_type is not null then ''::text
                    else '   action_gate_type is not null   '::text
                end) ||
                case
                    when new_1.z_invert_stop is not null then ''::text
                    else '   z_invert_stop is not null   '::text
                end) ||
                case
                    when new_1.z_ceiling_stop is not null then ''::text
                    else '   z_ceiling_stop is not null   '::text
                end) ||
                case
                    when new_1.v_max_cms is not null then ''::text
                    else '   v_max_cms is not null   '::text
                end) ||
                case
                    when new_1.v_max_cms >= 0::double precision then ''::text
                    else '   v_max_cms>=0   '::text
                end) ||
                case
                    when new_1.dt_regul_hr is not null then ''::text
                    else '   dt_regul_hr is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_control_node is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_pid_array is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or new_1.z_tz_array is not null then ''::text
                    else '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 1) <= 10 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 1) >= 1 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul or array_length(new_1.z_tz_array, 2) = 2 then ''::text
                    else '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or new_1.q_z_crit is not null then ''::text
                    else '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 1) <= 10 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 1) >= 1 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul or array_length(new_1.q_tq_array, 2) = 2 then ''::text
                    else '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                end) ||
                case
                    when new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul or new_1.nr_z_gate is not null then ''::text
                    else '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                end) ||
                case
                    when new_1.cc_submerged > 0::double precision then ''::text
                    else '   cc_submerged >0   '::text
                end as reason
           from ${model}.regul_sluice_gate_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
                case
                    when new_1.q0 is not null then ''::text
                    else '   q0 is not null   '::text
                end as reason
           from ${model}.constant_inflow_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((
                case
                    when new_1.downstream is not null then ''::text
                    else '   downstream is not null   '::text
                end ||
                case
                    when new_1.split1 is not null then ''::text
                    else '   split1 is not null   '::text
                end) ||
                case
                    when new_1.downstream_param is not null then ''::text
                    else '   downstream_param is not null   '::text
                end) ||
                case
                    when new_1.split1_law is not null then ''::text
                    else '   split1_law is not null   '::text
                end) ||
                case
                    when new_1.split1_param is not null then ''::text
                    else '   split1_param is not null   '::text
                end) ||
                case
                    when new_1.split2 is null or new_1.split2_law is not null then ''::text
                    else '   split2 is null or split2_law is not null   '::text
                end) ||
                case
                    when new_1.split2 is null or new_1.split2_param is not null then ''::text
                    else '   split2 is null or split2_param is not null   '::text
                end as reason
           from ${model}.zq_split_hydrology_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((
                case
                    when new_1.slope is not null then ''::text
                    else '   slope is not null   '::text
                end ||
                case
                    when new_1.k is not null then ''::text
                    else '   k is not null   '::text
                end) ||
                case
                    when new_1.k > 0::double precision then ''::text
                    else '   k>0   '::text
                end) ||
                case
                    when new_1.width is not null then ''::text
                    else '   width is not null   '::text
                end) ||
                case
                    when new_1.width >= 0::double precision then ''::text
                    else '   width>=0   '::text
                end as reason
           from ${model}.strickler_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((
                case
                    when new_1.q_dz_array is not null then ''::text
                    else '   q_dz_array is not null   '::text
                end ||
                case
                    when array_length(new_1.q_dz_array, 1) <= 10 then ''::text
                    else '   array_length(q_dz_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.q_dz_array, 1) >= 1 then ''::text
                    else '   array_length(q_dz_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.q_dz_array, 2) = 2 then ''::text
                    else '   array_length(q_dz_array, 2)=2   '::text
                end) ||
                case
                    when new_1.q_dz_array[1][1] = 0::double precision then ''::text
                    else '   q_dz_array[1][1]=0   '::text
                end) ||
                case
                    when new_1.q_dz_array[1][2] = 0::double precision then ''::text
                    else '   q_dz_array[1][2]=0   '::text
                end) ||
                case
                    when not ${model}.check_on_branch_or_reach_endpoint(new_1.geom) then ''::text
                    else '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                end as reason
           from ${model}.param_headloss_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            (((((((
                case
                    when new_1.drainage is not null then ''::text
                    else '   drainage is not null   '::text
                end ||
                case
                    when new_1.overflow is not null then ''::text
                    else '   overflow is not null   '::text
                end) ||
                case
                    when new_1.z_ini is not null then ''::text
                    else '   z_ini is not null   '::text
                end) ||
                case
                    when new_1.zr_sr_qf_qs_array is not null then ''::text
                    else '   zr_sr_qf_qs_array is not null    '::text
                end) ||
                case
                    when new_1.treatment_mode is not null then ''::text
                    else '   treatment_mode is not null   '::text
                end) ||
                case
                    when new_1.treatment_param is not null then ''::text
                    else '   treatment_param is not null   '::text
                end) ||
                case
                    when array_length(new_1.zr_sr_qf_qs_array, 1) <= 10 then ''::text
                    else '   array_length(zr_sr_qf_qs_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.zr_sr_qf_qs_array, 1) >= 1 then ''::text
                    else '   array_length(zr_sr_qf_qs_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zr_sr_qf_qs_array, 2) = 4 then ''::text
                    else '   array_length(zr_sr_qf_qs_array, 2)=4   '::text
                end as reason
           from ${model}.reservoir_rsp_hydrology_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((
                case
                    when new_1.cascade_mode is not null then ''::text
                    else '   cascade_mode is not null   '::text
                end ||
                case
                    when new_1.cascade_mode = 'hydrograph'::hydra_model_connect_mode or new_1.zq_array is not null then ''::text
                    else '   cascade_mode=''hydrograph'' or zq_array is not null   '::text
                end) ||
                case
                    when array_length(new_1.zq_array, 1) <= 100 then ''::text
                    else '   array_length(zq_array, 1)<=100   '::text
                end) ||
                case
                    when array_length(new_1.zq_array, 1) >= 1 then ''::text
                    else '   array_length(zq_array, 1)>=1   '::text
                end) ||
                case
                    when array_length(new_1.zq_array, 2) = 2 then ''::text
                    else '   array_length(zq_array, 2)=2   '::text
                end) ||
                case
                    when array_length(new_1.quality, 1) = 9 then ''::text
                    else '   array_length(quality, 1)=9   '::text
                end as reason
           from ${model}.model_connect_bc_singularity new_1
          where not new_1.validity
        union
         select new_1.id,
            ((((
                case
                    when new_1.qq_array is not null then ''::text
                    else '   qq_array is not null   '::text
                end ||
                case
                    when new_1.downstream is not null then ''::text
                    else '   downstream is not null   '::text
                end) ||
                case
                    when new_1.split1 is not null then ''::text
                    else '   split1 is not null   '::text
                end) ||
                case
                    when array_length(new_1.qq_array, 1) <= 10 then ''::text
                    else '   array_length(qq_array, 1)<=10   '::text
                end) ||
                case
                    when array_length(new_1.qq_array, 1) >= 1 then ''::text
                    else '   array_length(qq_array, 1)>=1   '::text
                end) ||
                case
                    when new_1.split2 is not null and array_length(new_1.qq_array, 2) = 3 or new_1.split2 is null and array_length(new_1.qq_array, 2) = 2 then ''::text
                    else '   (split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2)   '::text
                end as reason
           from ${model}.qq_split_hydrology_singularity new_1
          where not new_1.validity
        )
    select CONCAT('node:', n.id) as
        id,
        validity,
        name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom,
        reason from $model._node as n join node_invalidity_reason as r on r.id=n.id
        where validity=FALSE
    union
    select CONCAT('link:', l.id) as
        id,
        validity,
        name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom,
        reason from $model._link as l join link_invalidity_reason as r on  r.id=l.id
        where validity=FALSE
    union
    select CONCAT('singularity:', s.id) as
        id,
        s.validity,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        reason from $model._singularity as s join $model._node as n on s.node = n.id join singularity_invalidity_reason as r on r.id=s.id
        where s.validity=FALSE
    union
    select CONCAT('profile:', p.id) as
        id,
        p.validity,
        p.name,
        'profile'::varchar as type,
        ''::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        ''::varchar as reason from $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where p.validity=FALSE
;;

--mesh tools



create materialized view ${model}.coted_point_with_interp as
    with easy as (
        select distinct on (cp.id) cp.id, cp.geom, ft.reach, ft.reach_rel_abs
        from ${model}.coted_point as cp join ${model}.flood_plain_bluff_point_transect as ft on st_dwithin(cp.geom, ft.geom, .1)
    ),
    hard as (
        select id, geom from ${model}.coted_point
        except
        select id, geom from easy
    ),
    hard_section as (
        select id, geom, ${model}.interpolate_transect_at(geom) as transect
        from hard
    ),
    interp as (
        select id, geom, reach, reach_rel_abs from easy
        union all
        select distinct on (h.id) h.id, h.geom, r.id as reach, st_linelocatepoint(r.geom, st_closestpoint(r.geom, h.transect)) as reach_rel_abs
        from hard_section as h join ${model}.reach as r on st_intersects(r.geom, h.transect)
    ),
    reach_segment as (
        select n.reach, n.id as nd, st_linelocatepoint(r.geom, n.geom) as reach_rel_abs_nd,
        lag(n.id) over(w) as strt, lag(st_linelocatepoint(r.geom, n.geom)) over(w) as reach_rel_abs_strt,
        n.geom as nd_geom,
        lag(n.geom) over(w) as strt_geom
        from ${model}.river_node as n join ${model}.reach as r on r.id=n.reach
        window w as (partition by n.reach order by st_linelocatepoint(r.geom, n.geom))
    )
    select distinct on (i.id) i.id, i.geom, s.strt, s.nd, s.reach_rel_abs_strt, i.reach_rel_abs, s.reach_rel_abs_nd, st_makeline(array[s.strt_geom, i.geom, nd_geom]) as lnk, 1. - (i.reach_rel_abs - s.reach_rel_abs_strt)/(s.reach_rel_abs_nd - s.reach_rel_abs_strt) as weight_strt, (i.reach_rel_abs - s.reach_rel_abs_strt)/(s.reach_rel_abs_nd - s.reach_rel_abs_strt) as weight_nd
    from interp as i join reach_segment as s on (s.reach=i.reach and s.reach_rel_abs_strt <= i.reach_rel_abs and s.reach_rel_abs_nd >= i.reach_rel_abs)
    with no data
;;

create index ${model}_coted_point_with_interp_geom_idx on  ${model}.coted_point_with_interp using gist(geom)
;;

/* ********************************************** */
/*                     Mesh                       */
/* ********************************************** */



create materialized view ${model}.mesh_1d_coted as
    select row_number() over() as id,
    a.id as a, a.strt as a_strt, a.weight_strt as a_strt_w, a.nd as a_nd, a.weight_nd as a_nd_w,
    b.id as b, b.strt as b_strt, b.weight_strt as b_strt_w, b.nd as b_nd, b.weight_nd as b_nd_w,
    c.id as c, c.strt as c_strt, c.weight_strt as c_strt_w, c.nd as c_nd, c.weight_nd as c_nd_w,
    (st_z(a.geom) + st_z(b.geom) + st_z(c.geom))/3. z_mean,
    st_z(a.geom) as z_a, st_z(b.geom) as z_b, st_z(c.geom) as z_c,
    st_makepolygon(st_makeline(array[a.geom, b.geom, c.geom, a.geom])) as geom,
    st_makeline(array[st_startpoint(a.lnk), st_centroid(st_makepolygon(st_makeline(array[a.geom, b.geom, c.geom, a.geom]))), st_endpoint(a.lnk)]) as lnk
    from ${model}.mesh_1d as e
    join ${model}.coted_point_with_interp as a on st_dwithin(st_pointn(st_exteriorring(e.geom), 1), a.geom, .1)
    join ${model}.coted_point_with_interp as b on st_dwithin(st_pointn(st_exteriorring(e.geom), 2), b.geom, .1)
    join ${model}.coted_point_with_interp as c on st_dwithin(st_pointn(st_exteriorring(e.geom), 3), c.geom, .1)
    where a.id!=b.id and b.id!= c.id and c.id!=a.id
    with no data
;;