update $model.metadata set version = '3.8'
;;

create or replace function $model.quote_text(t text)
returns text
language plpgsql immutable as
$$$$
    declare x numeric;
begin
    x := t::numeric;
    return t;
    exception when others then
    return '"'||t||'"';
end;
$$$$
;;

with dumped as (
    select l.id, c.key, e.key as attr, e.value as val
    from $model._pipe_link l
    join $model._link ll on ll.id=l.id
    join lateral json_each_text(ll.configuration::json) c on true
    join lateral json_each_text(c.value::json) e on true
    where configuration is not null
),
agg1 as (
    select id, key, ('{'||string_agg('"'|| attr ||'":'|| $model.quote_text(val), ', ')||', "rk_maj":0}')::json as value
    from dumped
    where attr != 'rl_maj'
    group by id, key
),
agg2 as (
    select id, ('{'||string_agg('"'||key||'":'||value, ', ')||'}')::json as configuration
    from agg1
    group by id
)
update $model._link l set configuration=agg2.configuration
from agg2 where agg2.id=l.id
;;

with dumped as (
    select n.id, c.key, e.key as attr, e.value as val, n.area_ha, n.rl, n.slope, n.c_imp
    from $model._catchment_node n
    join $model._node nn on nn.id=n.id
    join lateral json_each_text(nn.configuration::json) c on true
    join lateral json_each_text(c.value::json) e on true
    where configuration is not null and (netflow_type='hydra_permeable' or nn.configuration::varchar like '%"hydra_permeable"%')
),
agg1 as (
    select id, key, ('{'||string_agg('"'||
    case
    when attr = 'hydra_permeable_surface_soil_storage_rfu_mm' then 'hydra_surface_soil_storage_rfu_mm'
    when attr = 'hydra_permeable_soil_drainage_time_qres_day' then 'hydra_soil_drainage_time_qres_day'
    when attr = 'hydra_permeable_split_coefficient' then 'hydra_split_coefficient'
    when attr = 'hydra_permeable_catchment_connect_coef' then 'hydra_catchment_connect_coef'
    else attr
    end
    ||'":'||
    $model.quote_text(val)
    , ', ')||', "hydra_soil_infiltration_type":"split", "netflow_type": "hydra"}')::json as value
    from dumped
    where attr != 'hydra_surface_soil_storage_rfu_mm' and attr != 'hydra_soil_drainage_time_qres_day' and attr != 'netflow_type'
    group by id, key
),
agg2 as (
    select id, ('{'||string_agg('"'||key||'":'||value, ', ')||'}')::json as configuration
    from agg1
    group by id
)
update $model._node n set configuration=agg2.configuration
from agg2 where agg2.id=n.id
;;

with dumped as (
    select n.id, c.key, e.key as attr, e.value as val, n.area_ha, n.rl, n.slope, n.c_imp
    from $model._catchment_node n
    join $model._node nn on nn.id=n.id
    join lateral json_each_text(nn.configuration::json) c on true
    join lateral json_each_text(c.value::json) e on true
    where configuration is not null
),
agg1 as (
    select id, key, ('{'||string_agg('"'||
    attr
    ||'":'||
    case
    when attr = 'runoff_type' and val = 'k_desbordes' then '"Desbordes 1 Cr"'
    when attr = 'runoff_type' and val = 'passini' then '"Passini"'
    when attr = 'runoff_type' and val = 'giandotti' then '"Giandotti"'
    when attr = 'runoff_type' and val = 'socose' then '"Define Tc"'
    when attr = 'runoff_type' and val = 'define_k' then '"Define K"'
    else $model.quote_text(val)
    end
    , ', ')||'}')::json as value
    from dumped
    group by id, key
),
agg2 as (
    select id, ('{'||string_agg('"'||key||'":'||value, ', ')||'}')::json as configuration
    from agg1
    group by id
)

update $model._node n set configuration=agg2.configuration
from agg2 where agg2.id=n.id
;;

with dumped as (
    select n.id, c.key, e.key as attr, e.value as val, n.area_ha, n.rl, n.slope, n.c_imp, (nn.configuration->>c.key)::json->>'runoff_type' as runoff_type
    from $model._catchment_node n
    join $model._node nn on nn.id=n.id
    join lateral json_each_text(nn.configuration::json) c on true
    join lateral json_each_text(c.value::json) e on true
    where configuration is not null
),
agg1 as (
    select id, key, ('{'||string_agg('"'||
    attr
    ||'":'||
    $model.quote_text(val)
    , ', ')||
    case
    when runoff_type = 'Desbordes 1 Cr' then ', "define_k_mn":'||(5.07 * area_ha^(0.18) * (slope * 100)^(-0.36) * (1 + c_imp)^(-1.9) * rl^(0.15) * 30^(0.21) * 10^(-0.07))::text
    when runoff_type = 'Passini' then ', "define_k_mn":'||(0.14 * (area_ha * rl)^(1./3.) / sqrt(slope))::text
    when runoff_type = 'Giandotti' then ', "define_k_mn":'||(60 * ( 0.4 * sqrt(area_ha) + 0.0015 * rl ) / ( 0.8 * sqrt(slope * rl) ))::text
    else ''
    end
    ||'}')::json as value
    from dumped
    group by id, key, runoff_type, area_ha, rl, slope, c_imp
),
agg2 as (
    select id, ('{'||string_agg('"'||key||'":'||value, ', ')||'}')::json as configuration
    from agg1
    group by id
)
update $model._node n set configuration=agg2.configuration
from agg2 where agg2.id=n.id
;;

drop function $model.quote_text
;;
