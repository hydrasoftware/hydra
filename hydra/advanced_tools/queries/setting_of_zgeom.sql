/* ************************************************************************************  */
/* Update of a whole model to set Zgeom of object to z of calcul :                       */
/*    <WARNING> This query will impact the geom for the whole model, use it with caution */
/*     + project constrain to MNT values                                                 */
/*     + zgeom river_node <-> z_invert                                                   */
/*     + zgeom storage_node <-> z of first point of zs_array                             */
/*     + zgeom elem_2d_node (mesh) <-> zb                                                */
/*     + zgeom crossroad_node <-> z_ground                                               */
/*     + zgeom manhole_node <-> z_ground                                                 */
/*     + zgeom station node <-> z_invert                                                 */
/*     + TIP : If you only need one of these, you can edit the query and pick one        */
/*                                                                                       */
/* ************************************************************************************* */

------------------------------------------------------------------------------------------------
-- MISE A JOUR SUR MNT SOUS-JACENT

-- mise a jours des vertex de lignes de contrainte sur la cote MNT
update $model.constrain set geom = project.set_altitude(geom);; -- where id in ({list})

----------------------------------------------------------------------------------------

-- MISE A JOUR DIFFERENCIE DES ZGEOM POUR PRENDRE LES MEMES ATTRIBUTS QUE LE CALCUL

-- river node
update $model.river_node
set geom = St_SetSrid(St_MakePoint(St_X(geom), St_Y(geom), $model.river_node_z_invert(river_node.geom)), metadata.srid)
from hydra.metadata;;

-- storage node
update $model.storage_node
set geom = St_SetSrid(St_MakePoint(St_X(geom), St_Y(geom), zs_array[1][1] ), metadata.srid)
from hydra.metadata;;

-- mailles 2d
update $model.elem_2d_node
set geom = St_SetSrid(St_MakePoint(St_X(geom), St_Y(geom), zb), metadata.srid)
from hydra.metadata;;

-- crossroad
update $model.crossroad_node
set geom = St_SetSrid(St_MakePoint( St_X(geom), St_Y(geom), z_ground), metadata.srid)
from hydra.metadata;;

-- manhole
update $model.manhole_node
set geom = St_SetSrid(St_MakePoint( St_X(geom), St_Y(geom), z_ground), metadata.srid)
from hydra.metadata;;

-- station node
update $model.station_node
set geom = St_SetSrid(St_MakePoint( St_X(geom), St_Y(geom), z_invert), metadata.srid)
from hydra.metadata;;




