from builtins import str
from builtins import object

# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from hydra.utility.string import matrix_to_html

class HydrologyConverter(object):
    def __init__(self, proj, model_name):
        self.__proj = proj
        self.name = model_name
        self.inserted_manhole_list = []
        self.inserted_connectors_list = []
        self.inserted_hydro_manhole_list = []
        self.inserted_hydro_link_list = []
        self.inserted_hydrograph_list = []
        self.node_network_id = []

        self.deleted_manhole_list = []
        self.deleted_connectors_list = []
        self.deleted_hydro_manhole_list = []
        self.deleted_hydro_link_list = []
        self.deleted_hydrograph_list = []

    def check_for_singularity(self, node_network_id):

        check_sing, = self.__proj.execute("""select ARRAY_AGG (node) from {model}._singularity where node in ({list})""".format(
            model=self.name, list=",".join([str(pt) for pt in node_network_id]))).fetchone()

        if check_sing:
            return check_sing

    def convert_to_hydraulic_network(self, node_network_id):
        ''' take a list of ids that represent a complete hydrologic network,
        run the treatment to convert it in an hydraulic network '''

        self.node_network_id = node_network_id

        # find hydrologic connectors inside the network ( need to replace them when manhole are inserted)

        query_hyd_connector_id = self.__proj.execute("""
        select id from {model}.connector_hydrology_link
        where up in ({list}) and down in ({list})""".format(
            model=self.name, list=",".join([str(pt) for pt in node_network_id]))).fetchall()

        hydro_connector_ids = [res for (res,) in query_hyd_connector_id]

        #print(" hydro connect id", hydro_connector_ids)

        # look for hydrologic connectors in the lower part of the network (need to remove them)

        connectors_down = self.__proj.execute("""
        select id, St_asText(hl.geom) from {model}.connector_hydrology_link as hl
        where up in ({list})""".format(model=self.name, list=",".join([str(pt) for pt in node_network_id]))).fetchall()

        #print(" hydro down id", connectors_down)

        pipe_not_in_network = self.__proj.execute("""
        select id, St_asText(hl.geom) from {model}.pipe_link as hl
        where down in ({list}) and up not in ({list})""".format(model=self.name, list=",".join([str(pt) for pt in node_network_id]))).fetchall()

        #print(" connectors not in network", pipe_not_in_network)

        if pipe_not_in_network:
            self.transform_pipe_to_pipe_and_connector_hydrology(pipe_not_in_network)

        root = self.find_root_hydro_network(node_network_id)

        points_network = [pt for pt in node_network_id if pt!= root]

        hydrology_node = self.__proj.execute(""" with deleted as (
            delete from {model}.manhole_hydrology_node where id in ({list}) returning St_asText(geom) as textgeom, name, area, id)
            select textgeom, name, area, id from deleted""".format(
                model=self.name, list=",".join([str(pt) for pt in points_network]))).fetchall()

        #print(hydrology_node, "Deleted hydrology manhole")

        if hydro_connector_ids:

            geom_connectors_list = self.__proj.execute(""" with deleted as (
                delete from {model}.connector_hydrology_link where id in ({list}) returning St_asText(geom) as textgeom, id)
            select textgeom, id from deleted""".format(model=self.name, list=",".join([str(pt) for pt in hydro_connector_ids]))).fetchall()

            for geom in geom_connectors_list:
                self.deleted_hydro_link_list.append(geom[1])

        for node_geom in hydrology_node:
            inserted_manhole = self.__proj.execute("""insert into {model}.manhole_node (geom, name, area) values (St_setSrid(ST_GeomFromText('{geom}'),{srid}), '{name}', {area})
                returning id, name""".format(
                model=self.name, geom=node_geom[0], name=node_geom[1], area=node_geom[2], srid=self.__proj.srid)).fetchone()

            self.inserted_manhole_list.append(inserted_manhole)
            self.deleted_hydro_manhole_list.append(node_geom[3])

        if hydro_connector_ids:
            for geom_connector in geom_connectors_list:
                inserted_connector = self.__proj.execute("""insert into {model}.connector_link (geom)
                                        select (St_setSrid(ST_GeomFromText('{geom}'),{srid})) returning id, name""".format(
                                        model=self.name, geom=geom_connector[0], srid=self.__proj.srid)).fetchone()

                self.inserted_connectors_list.append(inserted_connector)

        #print("inserted conenctor list", self.inserted_connectors_list)
        # print("inserted manhole list", self.inserted_manhole_list)
        # replace the last connector hydrologie => connector
        self.replace_down_hydro_connector([id for [id,geom] in connectors_down])

        # target the hydrology node that will support the interface between hydrology and hydraulics, create manhole ,
        # new hydro node link then and add hydrograph
        self.transform_hydrology_in_manhole_interface(root)

        # self.update_data_hydro()

    def transform_pipe_to_pipe_and_connector_hydrology(self, pipe_link_ids):
        ''' function to handle pipe linked to the network but not in the network,
        i.e other incoming network'''

        for link_geom in pipe_link_ids:
            self.__proj.execute("""insert into {model}.manhole_hydrology_node (geom)
            select ST_LineInterpolatePoint(St_setSrid(ST_GeomFromText('{link_geom}'),{srid}),0.90)""".format(
                link_geom=link_geom[1], model=self.name, srid=self.__proj.srid))

            self.__proj.execute("""update {model}.pipe_link
            set geom = ST_LineSubstring(St_setSrid(ST_GeomFromText('{link_geom}'),{srid}),0,0.90) where id = {id} """.format(
                link_geom=link_geom[1], model=self.name, id=link_geom[0], srid=self.__proj.srid))

            self.__proj.execute("""insert into {model}.connector_hydrology_link (geom)
            select ST_LineSubstring(St_setSrid(ST_GeomFromText('{link_geom}'),{srid}),0.90,1)""".format(
                model=self.name, link_geom=link_geom[1], srid=self.__proj.srid))

    def transform_hydrology_in_manhole_interface(self, id):
        '''target the hydrology node that will support the interface between hydrology and hydraulics, create manhole ,
           new hydro node. And then link them and add hydrograph'''

        if not id:
            #print("manhole transform id is None")
            return

        geom_node = self.__proj.execute("""select St_asText(geom) from {model}.manhole_hydrology_node where id = {id}""".format(
            model=self.name, id=id)).fetchone()[0]

        pipe_up_id = self.__proj.execute("""select id from {model}.pipe_link as pl
            where down={id}""".format(
            model=self.name, id=id)).fetchone()

        if pipe_up_id:
            pipe_up_id = pipe_up_id[0]

        # add hydro node if need because the node is not the top node (i.e root is not the deepest node)
        if pipe_up_id:
            new_hydro_node = self.__proj.execute("""
                insert into {model}.manhole_hydrology_node (geom) values ((select ST_LineInterpolatePoint((select geom from {model}.pipe_link where id={pipe_up_id}),0.95) as new_hydro)) returning id, geom""".format(
                model=self.name, pipe_up_id=pipe_up_id)).fetchone()[0]

            new_link = self.__proj.execute("""with connect as (
                select geom, ST_LineInterpolatePoint((select geom from {model}.pipe_link where id={pipe_up_id}),0.95) as new_hydro from {model}.manhole_hydrology_node where id = {id})
                insert into {model}.connector_hydrology_link (geom) values ((select St_Makeline(new_hydro,geom) from connect)) returning id, geom""".format(
                model=self.name, id=id, pipe_up_id=pipe_up_id)).fetchone()[0]

            self.inserted_hydro_manhole_list.append(new_hydro_node)
            self.inserted_hydro_link_list.append(new_link)

        self.__proj.execute("""delete from {model}.manhole_hydrology_node where id = {id}""".format(
            model=self.name, id=id))

        self.deleted_hydro_manhole_list.append(id)

        new_manhole = self.__proj.execute("""insert into {model}.manhole_node (geom) values
            (St_setSrid(St_GeomFromText('{geom_node}'),{srid})) returning id, name""".format(
            model=self.name, geom_node=geom_node, srid=self.__proj.srid)).fetchone()

        self.inserted_manhole_list.append(new_manhole)

        if pipe_up_id:
            #translate link top
            self.__proj.execute("""update {model}.pipe_link
                set geom = St_MakeLine(St_StartPoint(geom),ST_LineInterpolatePoint((select geom from {model}.pipe_link where id={id}),0.95))
                where id = {id}""".format(model=self.name, id=pipe_up_id))

            #add hydrograph
            hydrograph = self.__proj.execute("""insert into {model}.hydrograph_bc_singularity
                (geom) values(
                St_setSrid(St_GeomFromText('{geom_node}'),{srid}))
                returning id, geom""".format(
                model=self.name, geom_node=geom_node, srid=self.__proj.srid))

            self.inserted_hydrograph_list.append(hydrograph)

        #print("inserted hydro manhole", self.inserted_hydro_manhole_list)
        #print("inserted hydro link", self.inserted_hydro_link_list)

    def find_root_hydro_network(self, ids):
        ''' find the root node among the selection, root node is the deepest node in the network'''

        for num in ids:
            tree_for_id = self.__proj.execute('''select {model}.find_minimal_hydrology_network('{points}')'''.format(
                model=self.name, points="{"+ str(num) +"}")).fetchall()[0][0]
            if tree_for_id == ids:
                #print("root : ", num)
                return num

    def replace_down_hydro_connector(self, connectors):
        ''' arg is [id] from hydrology connectors '''
        if connectors:

            for c in connectors:
                self.deleted_hydro_link_list.append(c)

            connectors = self.__proj.execute("""delete from {model}.connector_hydrology_link where id in ({list}) returning St_asText(geom)""".format(
                model=self.name, list=",".join([str(c) for c in connectors]))).fetchall()

            connector_geom = connectors[0]

            #print("connector", connector_geom)

            for c_geom in connector_geom:

                self.__proj.execute("""insert into {model}.connector_link
                (geom) values (St_setSrid(St_geomFromText('{geom}'),{srid}))""".format(
                    model=self.name, geom=c_geom, srid=self.__proj.srid))

    def add_hydrograph_to_hydraulics_routing(self):
        ''' add hydrograph to all created manhole node connected to a routing or a connector by geom, but not in attribute '''

        # print("list manhole id", ",".join([str(m[0]) for m in self.inserted_manhole_list]))


        added_hydrograph_routing = self.__proj.execute("""
        with manhole_hydrograph as (
        select id, geom from {model}.manhole_node
        where id in ({list})
        and st_force2d(geom) in (select st_force2d(St_endpoint(geom)) from {model}.routing_hydrology_link)
        and id not in (select node from {model}._singularity))
        insert into {model}.hydrograph_bc_singularity (geom, tq_array)
        select geom, ARRAY[ARRAY[0,0]] from manhole_hydrograph
        returning geom, id
        """.format(
        model=self.name, list=",".join([str(m[0]) for m in self.inserted_manhole_list]))).fetchall()

        added_hydrograph_connector = self.__proj.execute("""
        with manhole_hydrograph as (
        select id, geom from {model}.manhole_node
        where id in ({list})
        and st_force2d(geom) in (select st_force2d(St_endpoint(geom)) from {model}.connector_hydrology_link)
        and id not in (select node from {model}._singularity))
        insert into {model}.hydrograph_bc_singularity (geom, tq_array)
        select geom, ARRAY[ARRAY[0,0]] from manhole_hydrograph
        returning geom, id
        """.format(
        model=self.name, list=",".join([str(m[0]) for m in self.inserted_manhole_list]))).fetchall()

        #print("added hydrograph routing", added_hydrograph_routing)
        #print("added hydrograph connector", added_hydrograph_connector)

        for elem in added_hydrograph_routing + added_hydrograph_connector:
            self.inserted_hydrograph_list.append(elem)

        #print("inserted hydrograph", self.inserted_hydrograph_list)

    def update_data_hydro(self):
        ''' update all changed data to reconnect node and links'''

        for table in ['routing_hydrology_link', 'pipe_link', 'connector_link', 'manhole_node', 'hydrograph_bc_singularity', 'connector_hydrology_link']:

            self.__proj.execute("""do
                        $$
                        DECLARE
                            t_row {model}.{table}%rowtype;
                        BEGIN
                            FOR t_row in SELECT * FROM {model}.{table} LOOP
                                RAISE NOTICE 'id : (%)',  t_row.id;
                                update {model}.{table}
                                    set geom = geom
                                where id = t_row.id; --<<< !!! important !!!
                            END LOOP;
                        END
                        $$;
                        ;;""".format(model=self.name, table=table))

        self.__proj.commit()

    def message(self):
        tab_data = []
        v = []
        h = ["Item type", "Created count"]#, "id created"]

        if self.inserted_manhole_list:
            manhole = ["Manhole", str(len(self.inserted_manhole_list))] #," , ".join([str(ids[0]) for ids in self.inserted_manhole_list])]
            tab_data.append(manhole)
            v.append("")
        if self.inserted_connectors_list:
            connectors = ["Connectors", str(len(self.inserted_connectors_list))] #," , ".join([str(ids[0]) for ids in self.inserted_connectors_list])]
            tab_data.append(connectors)
            v.append("")
        if self.inserted_hydrograph_list:
            hydrographs = ["Hydrographs", str(len(self.inserted_hydrograph_list))] #," , ".join([str(ids[1]) for ids in self.inserted_hydrograph_list])]
            tab_data.append(hydrographs)
            v.append("")
        if self.inserted_hydro_manhole_list:
            hydro_manhole = ["hydroManhole", str(len(self.inserted_hydro_manhole_list))] #," , ".join([str(ids[0]) for ids in self.inserted_hydro_manhole_list])]
            tab_data.append(hydro_manhole)
            v.append("")

        created = matrix_to_html(tab_data, h=h, v=v, style=["padding: 15px"])

        tab_data = []
        v = []
        h = ["Item type", "Deleted count"]#, "id deleted"]

        if self.deleted_manhole_list:
            manhole = ["Manhole", str(len(self.deleted_manhole_list))] #," , ".join([str(ids[0]) for ids in self.deleted_manhole_list])]
            tab_data.append(manhole)
            v.append("")
        if self.deleted_connectors_list:
            connectors = ["Connectors", str(len(self.deleted_connectors_list))] #," , ".join([str(ids[0]) for ids in self.deleted_connectors_list])]
            tab_data.append(connectors)
            v.append("")
        if self.deleted_hydrograph_list:
            hydrographs = ["Hydrographs", str(len(self.deleted_hydrograph_list))] #," , ".join([str(ids[1]) for ids in self.deleted_hydrograph_list])]
            tab_data.append(hydrographs)
            v.append("")
        if self.deleted_hydro_manhole_list:
            hydro_manhole = ["hydrologic Manhole", str(len(self.deleted_hydro_manhole_list))] #," , ".join([str(ids) for ids in self.deleted_hydro_manhole_list])]
            tab_data.append(hydro_manhole)
            v.append("")
        if self.deleted_hydro_link_list:
            hydro_link = ["hydrologic Link", str(len(self.deleted_hydro_link_list))] #," , ".join([str(ids) for ids in self.deleted_hydro_link_list])]
            tab_data.append(hydro_link)
            v.append("")

        deleted = matrix_to_html(tab_data, h=h, v=v, style=["padding: 5px"])

        return("created items during process : \n"+ created +"\n" + "deleted items during process : \n"+ deleted)
