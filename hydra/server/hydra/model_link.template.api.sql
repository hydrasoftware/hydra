/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

create view $model.$tablename as
    select p.id, p.name, p.up, p.down, $tableview, p.geom, p.configuration::varchar as configuration, p.generated, p.validity, p.up_type, p.down_type, p.configuration as  configuration_json, p.comment
    from $model._$tablename as c, $model._link as p
    where p.id = c.id
;;

/* note: id and link_type are not updatable */
create function ${model}.${tablename}_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from ${model}.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;

            -- Creation of HY singularity if type = routing_hydrology_link or type = connector_hydrology_link
            if $tableid = $routing_hydrology_link_type or $tableid = $connector_hydrology_link_type then
                if down_type_ != 'manhole_hydrology' and not (select exists (select 1 from $model.hydrograph_bc_singularity where node=down_)) then
                    insert into $model.hydrograph_bc_singularity(geom) select geom from $model._node where id=down_;
                end if;
            end if;

            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated, comment)
                values($tableid, up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated, new.comment) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = $tableid) where name = 'define_later' and id = id_;
            insert into $model._$tablename(id, link_type, $tablecolumns)
                values (id_, $tableid, $tableinsert);
            if $tableid = $pipe_link_type and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, '$tablename');
            update $model._link set validity = (select $validity from  $model._$tablename where id = id_) where id = id_;
            new.id = id_;

            -- Update of connected manholes for connectivity check if pipe
            if $tableid = $pipe_link_type then
                update $model.manhole_node set geom=geom where id=up_ or id=down_;
            end if;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if (($newconfigfields) is distinct from ($oldconfigfields)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select $oldconfigfields) as o, (select $newconfigfields) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select $newconfigfields) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from ${model}.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated, comment=new.comment where id=old.id;
            ${commentupdate}update $model._$tablename set $tableupdate where id=old.id;

            if $tableid = $pipe_link_type and (select trigger_branch from $model.metadata) then
                if not ST_Equals(old.geom, new.geom) or (new.exclude_from_branch != old.exclude_from_branch) then
                    perform $model.branch_update_fct();
                end if;
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, '$tablename');

            -- Update of connected manholes for connectivity check if pipe
            if $tableid = $pipe_link_type and (ST_Equals(ST_StartPoint(new.geom), ST_StartPoint(old.geom))=false or ST_Equals(ST_EndPoint(new.geom), ST_EndPoint(old.geom))=false) then
                update $model.manhole_node set geom=geom where id=up_ or id=down_ or id=old.up or id=old.down;
            end if;

            update $model._link set validity = (select $validity from  $model._$tablename where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._$tablename where id=old.id;
            delete from $model._link where id=old.id;
            if $tableid = $pipe_link_type and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;

            -- Update of connected manholes for connectivity check if pipe
            if $tableid = $pipe_link_type then
                update $model.manhole_node set geom=geom where id=old.up or id=old.down;
            end if;
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_${tablename}_trig
    instead of insert or update or delete on $model.$tablename
       for each row execute procedure ${model}.${tablename}_fct()
;;
