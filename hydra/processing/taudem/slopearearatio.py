# -*- coding: utf-8 -*-

"""
***************************************************************************
    slopearearatio.py
    ---------------------
    Date                 : January 2018
    Copyright            : (C) 2018-2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'January 2018'
__copyright__ = '(C) 2018-2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.core import (QgsProcessingException,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterRasterDestination
                      )

from .taudemAlgorithm import TauDemAlgorithm
from . import taudemUtils


class SlopeAreaRatio(TauDemAlgorithm):

    SLOPE = 'SLOPE'
    AREA = 'AREA'
    SLOPE_AREA_RATIO = 'SLOPE_AREA_RATIO'

    def name(self):
        return 'slopearearatio'

    def displayName(self):
        return self.tr('Slope over area ratio')

    def group(self):
        return self.tr('Specialized grid analysis')

    def groupId(self):
        return 'specializedanalysis'

    def tags(self):
        return self.tr('dem,hydrology,slope,catchment,area').split(',')

    def shortHelpString(self):
        return self.tr('Calculates the ratio of the slope to the specific '
                       'catchment (contributing) area.')

    def helpUrl(self):
        return 'http://hydrology.usu.edu/taudem/taudem5/help53/SlopeOverAreaRatio.html'

    def __init__(self):
        super().__init__()

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterRasterLayer(self.SLOPE,
                                                            self.tr('Slope')))
        self.addParameter(QgsProcessingParameterRasterLayer(self.AREA,
                                                            self.tr('Specific catchment area')))

        self.addParameter(QgsProcessingParameterRasterDestination(self.SLOPE_AREA_RATIO,
                                                                  self.tr('Slope area ratio')))

    def processAlgorithm(self, parameters, context, feedback):
        slopeLayer = self.parameterAsRasterLayer(parameters, self.SLOPE, context)
        if slopeLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.SLOPE))

        areaLayer = self.parameterAsRasterLayer(parameters, self.AREA, context)
        if areaLayer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.AREA))

        arguments = []
        arguments.append(self.command())

        arguments.append('-slp')
        arguments.append(slopeLayer.source())
        arguments.append('-sca')
        arguments.append(areaLayer.source())

        arguments.append('-sar')
        arguments.append(self.parameterAsOutputLayer(parameters, self.SLOPE_AREA_RATIO, context))

        taudemUtils.execute(arguments, feedback)

        return self.algorithmResults(parameters)
