# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.land_type_guitest [-dhs] [-g project_name]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name
        run in gui mode

   -s  --solo
        run test in solo mode (create database)
"""

from hydra.utility.string import normalized_name, normalized_model_name
import sys
import getopt
from hydra.project import Project
from qgis.PyQt.QtWidgets import QApplication
from qgis.PyQt.QtCore import QCoreApplication, QTranslator
from hydra.gui.land_type import LandTypeManager
from hydra.database.database import TestProject, remove_project, project_exists

def gui_mode(project_name):
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    obj_project = Project.load_project(project_name)

    test_dialog = LandTypeManager(obj_project, None)
    test_dialog.exec_()


def test():
    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)

    obj_project.execute("""delete from project.land_type;""")

    test_dialog = LandTypeManager(obj_project, None)
    test_dialog.load_classification("Corine land cover")
    test_dialog.new_land_type()
    test_dialog.load_classification("MOS Ile-de-France")
    test_dialog.new_land_type()

    rows_to_delete = []
    for row in range(test_dialog.table_land_type.table.rowCount()):
        test_dialog.table_land_type.table.selectRow(row)
        if test_dialog.table_land_type.table.item(row, 1).text() in ["pits_landfills_construction_sites"]:
            test_dialog.table_land_type.table.item(row, 2).setText("0.06")
        elif test_dialog.table_land_type.table.item(row, 1).text() in ["water", "transports"]:
            test_dialog.table_land_type.table.item(row, 4).setText("0.99")
        elif test_dialog.table_land_type.table.item(row, 1).text() in ["woods_and_forests", "cultivated_fields", "semi_natural_spaces"]:
            test_dialog.table_land_type.table.cellWidget(row, 3).setCurrentIndex(test_dialog.table_land_type.table.cellWidget(row, 3).findText('holtan'))
        test_dialog.table_land_type.save_selected_row()

    test_dialog.new_land_type()
    test_dialog.save()

    test_dialog = LandTypeManager(obj_project, None)
    assert(obj_project.execute("select count(*) from project.land_type").fetchone()[0] == 13)
    assert(obj_project.execute("select count(*) from project.land_type where c_imp=0.06::real").fetchone()[0] == 1)
    assert(obj_project.execute("select count(*) from project.land_type where netflow_type='holtan'").fetchone()[0] == 3)
    assert(obj_project.execute("select count(*) from project.land_type where c_runoff=0.99::real").fetchone()[0] == 2)


try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name, debug=True)
    test()
    # fix_print_with_import
    print("ok")
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    project_name = normalized_name(project_name)
    gui_mode(project_name)
    exit(0)

test()
# fix_print_with_import

# fix_print_with_import
print("ok")
exit(0)
