# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_link_dialog import SimpleLinkDialog, tr
from qgis.PyQt import uic
import os

class JetFanLinkEditor(SimpleLinkDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleLinkDialog.__init__(self, project, geom, "jet_fan_link", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "jet_fan_link.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        unit_thrust_newton, number_of_units, efficiency, flow_velocity, pipe_area, is_up_to_down, comment = self.project.execute("""
            select unit_thrust_newton, number_of_units, efficiency, flow_velocity, pipe_area, is_up_to_down, comment
            from {}.jet_fan_link
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.unit_thrust_newton.setText(f"{unit_thrust_newton:.2f}" if unit_thrust_newton is not None else '')
        self.number_of_units.setValue(number_of_units)
        self.efficiency.setText(f"{efficiency:.3f}" if efficiency is not None else '')
        self.flow_velocity.setText(f"{flow_velocity:.3f}" if flow_velocity is not None else '')
        self.pipe_area.setText(f"{pipe_area:.3f}" if pipe_area is not None else '')
        self.is_up_to_down.setChecked(is_up_to_down)
        self.notes.setText(comment)

    def get_id(self):
        return self.id

    def save(self):
        float_or_none = lambda s: float(s) if s else None
        self.project.log.notice(tr("Saved"))
        self.model.update_jet_fan_link(self.id, self.geom,
            float_or_none(self.unit_thrust_newton.text()),
            self.number_of_units.value(),
            float_or_none(self.efficiency.text()),
            float_or_none(self.flow_velocity.text()),
            float_or_none(self.pipe_area.text()),
            self.is_up_to_down.isChecked(),
            comment=self.notes.text())
        SimpleLinkDialog.save(self)
        self.close()
