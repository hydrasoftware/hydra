from builtins import str
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import json
import hydra.utility.string as string

with open(os.path.normpath(os.path.join(os.path.dirname(__file__), '..', 'server', 'hydra', 'hydra.instances.json'))) as instance_file:
    instances = json.load(instance_file)

def pack(dic, keep_text=False):
    '''creates a json dict from a python dict
       keep_text=False removes all values not convertible to int/float'''
    __check_dict_value(dic, keep_text)
    return json.dumps(dic)

def unpack(param_json):
    '''creates a python dict from a json dict
       all values are converted to string'''
    param = json.loads(param_json)
    __recursive_dict_text(param)
    return param

def __check_dict_value(dic, keep_text=False):
    '''modifies dict removing values not convertible to float/int (keep_text=False)'''
    for key, value in dic.items():
        if isinstance(value, dict):
            __check_dict_value(value, keep_text)
        elif isinstance(value, bool) or isinstance(value, list):
            dic[key]=value
        elif keep_text and isinstance(value, str) and not string.isfloat(value):
            dic[key]=value
        else:
            dic[key]=string.get_sql_float(value)

def __recursive_dict_text(dic):
    '''change all values from dict to string'''
    for key, value in dic.items():
        if isinstance(value, dict):
            __recursive_dict_text(value)
        else:
            dic[key]=__text_from_json(value)

def __text_from_json(value):
    '''force value to str'''
    if string.isfloat(value):
        return str(value)
    elif isinstance(value, str):
        return value
    else:
        return ''

def set_fields_to_json(dialog, param_name, param_json):
    '''sets all QTextEdit from dialog with json dict
       QTextEdit named key set to value'''
    param_dict = None
    if param_name in instances.keys():
        param_properties = instances[param_name]
        for type in param_properties.keys():
            for parameter in param_properties[type]['params']:
                # for each parameter, if default value in json + field with name in .ui, set to default
                if 'default' in param_properties[type]['params'][parameter].keys():
                    if hasattr(dialog, parameter):
                        try:
                            getattr(dialog, parameter).setText(string.get_str(param_properties[type]['params'][parameter]['default']))
                        except:
                            pass

                # then, if value in param_json defined (from previous writting) set to it
                if param_json is not None:
                    param_dict = unpack(param_json)
                    if type in param_dict.keys():
                        parameters = param_dict[type]
                        if parameter in parameters.keys():
                            if hasattr(dialog, parameter):
                                try:
                                    getattr(dialog, parameter).setText(string.get_str(parameters[parameter]))
                                except:
                                    pass
    return param_dict

def build_json_from_fields(dialog, param_name):
    '''packs all values in QTextEdit of dialog to a json dic'''
    dic = dict()
    if param_name in instances.keys():
        param_properties = instances[param_name]
        for type in param_properties.keys():
            dic[type] = dict()
            for parameter in param_properties[type]['params']:
                # for parameter, if exists a field in form with same name, puts his value in dic
                if hasattr(dialog, parameter):
                    try:
                        dic[type][parameter] = getattr(dialog, parameter).text()
                    except:
                        dic[type][parameter] = None
    dic_json = pack(dic)
    return dic_json