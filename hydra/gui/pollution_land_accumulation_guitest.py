# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.pollution_land_accumulation_guitest [-dhs] [-g project_name]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name
        run in gui mode

   -s  --solo
        run test in solo mode (create database)
"""

import sys
import getopt
from hydra.utility.string import normalized_name, normalized_model_name
from hydra.project import Project
from qgis.PyQt.QtWidgets import QApplication, QTableWidgetItem
from qgis.PyQt.QtCore import QCoreApplication, QTranslator
from hydra.gui.pollution_land_accumulation import PollutionLandAccumulationManager
from hydra.database.database import TestProject, remove_project, project_exists
import hydra.utility.string as string

def gui_mode(project_name):
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    obj_project = Project.load_project(project_name)

    test_dialog = PollutionLandAccumulationManager(obj_project)
    test_dialog.exec_()


def test():
    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)

    test_dialog = PollutionLandAccumulationManager(obj_project)

    # check default values are loaded
    sewage_storage_kgha = [[0 for y in range(4)]for x in range(4)]
    unitary_storage_kgha = [[0 for y in range(4)]for x in range(4)]
    for i in range(4):
        for j in range(4):
            sewage_storage_kgha[i][j] = string.get_sql_forced_float(test_dialog.sewage_storage_kgha.item(i,j).text())
            unitary_storage_kgha[i][j] = string.get_sql_forced_float(test_dialog.unitary_storage_kgha.item(i,j).text())

    assert sewage_storage_kgha == [[0,0,0,0],[175,17.5,87.5,3.25],[50,5,25,1],[112.5,11,56,2.2]]
    assert unitary_storage_kgha == [[0,0,0,0],[280,35,210,6.2],[80,10,50,2],[180,22,135,4.5]]
    assert float(test_dialog.regeneration_time_days.text()) == 5
    assert float(test_dialog.a_extraction_coef.text()) == 0.06
    assert float(test_dialog.b_extraction_coef.text()) == 1.5

    # change values
    for i in range(4):
        for j in range(4):
            test_dialog.sewage_storage_kgha.setItem(i,j,QTableWidgetItem(str(i*j*j)))
            test_dialog.unitary_storage_kgha.setItem(i,j,QTableWidgetItem(str(i*i*j)))
    test_dialog.regeneration_time_days.setText('10')
    test_dialog.a_extraction_coef.setText('1.69')
    test_dialog.b_extraction_coef.setText('4.2297')
    test_dialog.save()

    # check values were saved
    sewage_storage_kgha = [[0 for y in range(4)]for x in range(4)]
    unitary_storage_kgha = [[0 for y in range(4)]for x in range(4)]
    for i in range(4):
        for j in range(4):
            sewage_storage_kgha[i][j] = string.get_sql_forced_float(test_dialog.sewage_storage_kgha.item(i,j).text())
            unitary_storage_kgha[i][j] = string.get_sql_forced_float(test_dialog.unitary_storage_kgha.item(i,j).text())

    assert sewage_storage_kgha == [[0, 0, 0, 0], [0, 1, 4, 9], [0, 2, 8, 18], [0, 3, 12, 27]]
    assert unitary_storage_kgha == [[0, 0, 0, 0], [0, 1, 2, 3], [0, 4, 8, 12], [0, 9, 18, 27]]
    assert float(test_dialog.regeneration_time_days.text()) == 10
    assert float(test_dialog.a_extraction_coef.text()) == 1.69
    assert float(test_dialog.b_extraction_coef.text()) == 4.2297

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name)
    test()
    # fix_print_with_import
    print("ok")
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    project_name = normalized_name(project_name)
    gui_mode(project_name)
    exit(0)

test()
# fix_print_with_import

# fix_print_with_import
print("ok")
exit(0)
