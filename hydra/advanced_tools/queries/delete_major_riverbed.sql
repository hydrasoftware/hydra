/* ********************************************************************************** */
/* Delete part of major riverbed and keep first point:                                */
/*     + set zbmaj_lbank_array and zbmaj_rbank_array to their first point             */
/*                                                                                    */
/* ********************************************************************************** */

update $model.valley_cross_section_geometry
set zbmaj_lbank_array = ARRAY[zbmaj_lbank_array[1][1],zbmaj_lbank_array[1][2]],
zbmaj_rbank_array = ARRAY[zbmaj_rbank_array[1][1],zbmaj_rbank_array[1][2]]
;;