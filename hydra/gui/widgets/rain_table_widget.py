################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from qgis.PyQt.QtCore import pyqtSignal
from qgis.PyQt.QtWidgets import QComboBox
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from functools import partial

class RainTableWidget(HydraTableWidget):
    def __init__(self, project, data_columns,
            title_columns, dbname, table_name, callbacks, filter="",
            key_id="id", parent=None, autoInit=True):

        HydraTableWidget.__init__(self, project, data_columns,
            title_columns, dbname, table_name, callbacks, filter,
            key_id, parent, autoInit)

    def update_data(self):
        super(RainTableWidget, self).update_data()
        self.__loading = True
        list_montana = self.project.execute("select id, name from project.coef_montana").fetchall()
        #add montana combobox selectors
        for icol in range(0, len(self.data_cols)):
            if "montana" in self.data_cols[icol]:
                for irow in range(0,self.table.rowCount()):
                    id_montana = self.table.item(irow,icol).text()
                    cbox = self.get_new_montana_combo(list_montana, id_montana)
                    cbox.currentIndexChanged.connect(partial(self.combo_index_changed,irow, icol))
                    self.table.setCellWidget(irow,icol, cbox)

        self.__loading = False

    def get_new_montana_combo(self, list_montana, id_montana):
        combobox = QComboBox()
        combobox.addItem("")
        for i in range(0, len(list_montana)):
            montana = list_montana[i]
            combobox.addItem("{} - {}".format(montana[0], montana[1]))
            if (id_montana == str(montana[0])):
                combobox.setCurrentIndex(i+1)
        return combobox

    def combo_index_changed(self, irow, icol, comboBoxIndex):
        self.table.selectRow(irow)
        id_montana=self.table.cellWidget(irow,icol).currentText().split("-")[0] if self.table.cellWidget(irow,icol).currentText()!="" else ""
        self.table.item(irow,icol).setText(id_montana)

    def save_selected_row(self):
        super(RainTableWidget, self).save_selected_row()
