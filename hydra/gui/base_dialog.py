# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from qgis.PyQt.QtWidgets import QDialog, QLineEdit, QDialogButtonBox, QHBoxLayout
from qgis.PyQt.QtCore import QCoreApplication

def tr(msg):
    return QCoreApplication.translate("Hydra", msg)


class BaseDialog(QDialog):
    """Base class that commit or rollback the actions on cancel
    the child class must call the save() function to commit the
    changes

    Never use Basedialog to make child dialog box.
    The rollback of the child __finished event
    will rollback the parent sql request too if the child
    dialog is cancelled.
    """
    def __init__(self, project, parent=None):
        QDialog.__init__(self, parent)
        self.project = project
        self.__saved = False
        self.ignore_rollback = False
        self.finished.connect(self.__finished)

    def __finished(self):
        if not self.__saved and not self.ignore_rollback:
            self.project.log.notice(tr("Canceled"))
            self.project.rollback()

    def save(self):
        self.__saved = True
        self.project.commit()
        self.project.log.notice(tr("Saved"))

    def populate_combo_from_sqlenum(self, combox, enum_table, selected_value=None, excluded_values=[], show_id=False):
        enum_values = self.project.execute("""
            select description, name, id
            from hydra.{}
            order by id""".format(enum_table)).fetchall()
        for value in enum_values:
            corrected_value = value if value[0] is not None else [value[1],value[1],value[2]]
            if not corrected_value[1] in excluded_values:
                if not show_id:
                    combox.addItem(corrected_value[0])
                else:
                    combox.addItem(str(corrected_value[2])+': '+corrected_value[0])
                if selected_value is not None:
                    if corrected_value[1] == selected_value:
                        if not show_id:
                            combox.setCurrentIndex(combox.findText(corrected_value[0]))
                        else:
                            combox.setCurrentIndex(combox.findText(str(corrected_value[2])+': '+corrected_value[0]))
                else:
                    combox.setCurrentIndex(-1)
        return enum_values

    def add_notes(self):
        self.buttonBox.layout().insertWidget(0, QLineEdit())
        self.buttonBox.layout().setStretch(0,1)

    def update(self):
        super().update()
