/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

create view project.model as
select table_schema as name from information_schema.tables where table_name='_singularity'
;;

create function project.model_instead_fct()
returns trigger
language plpython3u security definer as
$$$$
    from $hydra import create_model
    import plpy
    for statement in create_model(TD['new']['name'], $srid, '$version'):
        plpy.notice(statement)
        plpy.execute(statement)
    plpy.execute('alter schema "'+TD['new']['name']+'" owner to session_user')
    plpy.execute('grant all privileges on all tables in schema "'+TD['new']['name']+'" to session_user')
    plpy.execute('grant usage on all sequences in schema "'+TD['new']['name']+'" to session_user')
    plpy.execute('grant execute on all functions in schema "'+TD['new']['name']+'" to session_user')
$$$$
;;

create trigger model_intead_trig
instead of insert on project.model
for each row execute procedure project.model_instead_fct()
;;

create function project.drop_api()
returns void language plpython3u security definer volatile as
$$$$
    from $hydra import drop_model_api, drop_project_api
    r = list(plpy.execute('select srid, version from hydra.metadata'))[0]
    srid, version = r['srid'], r['version']
    for r in plpy.execute('select name from project.model'):
        for statement in drop_model_api(r['name']):
            plpy.execute(statement)
    for statement in drop_project_api():
        plpy.execute(statement)
$$$$
;;

create function project.refresh_api()
returns void language plpython3u security definer volatile as
$$$$
    from $hydra import drop_model_api, drop_project_api, create_project_api, create_model_api
    r = list(plpy.execute('select srid, version from hydra.metadata'))[0]
    srid, version = r['srid'], r['version']
    for r in plpy.execute('select name from project.model'):
        for statement in drop_model_api(r['name']):
            plpy.execute(statement)
    for statement in drop_project_api():
        plpy.execute(statement)
    for statement in create_project_api(srid, version):
        plpy.execute(statement)
    for r in plpy.execute('select name from project.model'):
        for statement in create_model_api(r['name'], srid, version):
            plpy.execute(statement)
$$$$
;;
