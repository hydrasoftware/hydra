from builtins import str
from builtins import range
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import json
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QSize
from qgis.PyQt.QtWidgets import QPushButton, QTableWidgetItem, QFileDialog
from qgis.PyQt.QtGui import QIcon
from hydra.gui.base_dialog import BaseDialog, tr
from hydra.gui.name_srid_dialog import NameSridDialog
from hydra.gui.work_manager import WorkManager
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget

with open(os.path.join(os.path.dirname(__file__), "import_points.json")) as j:
    __json_keys__ = json.load(j)


class TerrainManager(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "terrain_manager.ui"), self)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.btnAdd.clicked.connect(self.add)
        self.btnDel.clicked.connect(self.delete)
        self.btnUp.clicked.connect(self.up)
        self.btnDown.clicked.connect(self.down)
        self.btnAscToVrt.clicked.connect(self.asc_to_vrt)
        self.btnImportPoints = QPushButton(QIcon(os.path.join(current_dir, '..', 'ressources', 'images', 'import_settings.svg')), '')
        self.btnImportPoints.setFixedSize(QSize(37, 36))
        self.btnImportPoints.setIconSize(QSize(30, 30))
        self.btnImportPoints.setToolTip(self.tr("Caution: does not load the points imported from vector layers into the groups of terrain points."))
        self.btnImportPoints.clicked.connect(self.import_points)

        self.table_terrain.resizeColumnsToContents()
        self.table_terrain.horizontalHeader().setStretchLastSection(True)
        self.__refresh_table_terrain()
        self.table_terrain.itemChanged.connect(self.on_data_edited)

        self.type_points_table = HydraTableWidget(self.project, ("id", "name", "comment"), (tr("Id"), tr("Name"), tr("Comment")),
                                "project", "points_type",
                                (self.new_type_points, self.delete_type_points), "", "id",
                                self.place_holder_table)

        #tmplayout = QGridLayout(self.type_points_table.btnPlaceholder)
        #tmplayout.addWidget(self.btnImportPoints)
        self.type_points_table.btnPlaceholder.addWidget(self.btnImportPoints)

        self.type_points_table.set_editable(True)
        self.type_points_table.data_edited.connect(self.save_row)

    def save_row(self):
        self.type_points_table.save_selected_row()

    def new_type_points(self):
        self.type_points_table.add_row(['default', ''])

    def delete_type_points(self):
        self.type_points_table.del_selected_row()

    def on_data_edited(self, new_value):
        if not self.__loading:
            self.table_terrain.resizeColumnsToContents()
            self.table_terrain.horizontalHeader().setStretchLastSection(True)
            current_index = self.table_terrain.currentRow()
            self.project.execute("""update project.dem set source='{}' where id={};""".format(
                self.project.pack_path(self.table_terrain.item(current_index,2).text()),
                self.table_terrain.item(current_index,0).text()))
            self.__refresh_table_terrain()

    def __refresh_table_terrain(self):
        self.__loading = True
        terrains = self.project.execute("""
                    select id, priority, source
                    from project.dem
                    order by priority asc
                    """).fetchall()
        self.table_terrain.setRowCount(0)
        for t in terrains:
            rowPosition = self.table_terrain.rowCount()
            self.table_terrain.insertRow(rowPosition)
            self.table_terrain.setItem(rowPosition , 0, QTableWidgetItem(str(t[0])))
            self.table_terrain.setItem(rowPosition , 1, QTableWidgetItem(str(t[1])))
            self.table_terrain.setItem(rowPosition , 2, QTableWidgetItem(self.project.unpack_path(t[2])))
        self.__loading = False

    def up(self):
        current_index = self.table_terrain.currentRow()
        if current_index==-1:
            return
        self.__loading = True
        item1 = self.table_terrain.takeItem(current_index, 0)
        item2 = self.table_terrain.takeItem(current_index, 1)
        item3 = self.table_terrain.takeItem(current_index, 2)
        rowPosition = current_index - 1
        self.table_terrain.insertRow(rowPosition)
        self.table_terrain.setItem(rowPosition , 0, item1)
        self.table_terrain.setItem(rowPosition , 1, item2)
        self.table_terrain.setItem(rowPosition , 2, item3)
        self.table_terrain.removeRow(current_index+1)
        self.update_priorities()
        self.__refresh_table_terrain()

    def down(self):
        current_index = self.table_terrain.currentRow()
        if current_index==-1:
            return
        if current_index==self.table_terrain.rowCount()-1:
            return
        self.__loading = True
        item1 = self.table_terrain.takeItem(current_index, 0)
        item2 = self.table_terrain.takeItem(current_index, 1)
        item3 = self.table_terrain.takeItem(current_index, 2)
        rowPosition = current_index + 2
        self.table_terrain.insertRow(rowPosition)
        self.table_terrain.setItem(rowPosition , 0, item1)
        self.table_terrain.setItem(rowPosition , 1, item2)
        self.table_terrain.setItem(rowPosition , 2, item3)
        self.table_terrain.removeRow(current_index)
        self.update_priorities()
        self.__refresh_table_terrain()

    def update_priorities(self):
        for i in range(0, self.table_terrain.rowCount()):
            self.project.execute("""update project.dem set priority={} where id={};""".format(
                i+1, self.table_terrain.item(i,0).text()))

    def add(self):
        title = tr('Add file')
        file, __ = QFileDialog.getOpenFileName(self, title, self.project.directory, tr("Terrain files (*.vrt)"))
        if file is not None and os.path.exists(file):
            self.project.execute(f"""insert into project.dem(source) values('{file}')""")
            self.__refresh_table_terrain()
            self.update_priorities()
            self.__refresh_table_terrain()

    def delete(self):
        current_index = self.table_terrain.currentRow()
        if current_index==-1:
            return
        self.project.execute("""delete from project.dem where id={0}""".format(self.table_terrain.item(current_index,0).text()))
        self.__refresh_table_terrain()
        self.update_priorities()
        self.__refresh_table_terrain()

    def asc_to_vrt(self):
        files, __ = QFileDialog.getOpenFileNames(self, tr("Select one or more files to open"), self.project.directory, tr("Images (*.asc *.tiff *.tif)"))
        if files:
            name_srid_dialog = NameSridDialog(self, os.path.splitext(os.path.basename(files[0]))[0], self.project.srid)
            name_srid_dialog.label_name.setText('Terrain name:')
            name_srid_dialog.label_srid.setText('Source files ESPG:')
            if name_srid_dialog.exec_():
                name, srid = name_srid_dialog.get_result()
                if name and srid:
                    self.project.build_vrt(files, srid, name)
                    self.__refresh_table_terrain()
                    self.update_priorities()
                    self.__refresh_table_terrain()
                    self.project.save()

    def import_points(self):
        WorkManager(self.project, json_keys=__json_keys__, parent=self).exec_()

    def save(self):
        BaseDialog.save(self)
        self.close()
