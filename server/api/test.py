import requests
import sys

#url = 'https://database.hydra-software.net/api'
url = 'http://127.0.0.1:8000'

print("Test invalid user")
res = requests.post(url+'/token',
    data={'username': 'invalid.user@setec.com', 'password': 'whatever'})
print(res.status_code, res.text)
assert(res.status_code == 400)

print(  )
print("Test user dentification")
res = requests.post(url+'/token',
    data={'username': sys.argv[1], 'password': sys.argv[2]})
if res.status_code != 200:
    raise RuntimeError('code: {} msg:{}'.format(res.status_code, res.text))
print(res.status_code, res.text)
assert(res.status_code == 200)
token = res.json()

print()
print("Test list projects")
res = requests.get(url+'/get_projects_list',
    headers={'authorization': '{token_type} {access_token}'.format(**token)})
if res.status_code != 200:
    raise RuntimeError('code: {} msg:{}'.format(res.status_code, res.text))
print(res, res.json())
assert(res.status_code == 200)

print()
print("Test user database drop")
res = requests.delete(url+'/remove_project?name=test_public',
    headers={'authorization': '{token_type} {access_token}'.format(**token)})
if res.status_code != 200:
    raise RuntimeError('code: {} msg:{}'.format(res.status_code, res.text))
print(res, res.json())
assert(res.status_code == 200)

print()
print("Test user database creation")
res = requests.post(url+'/create_project?name=test_public&srid=2154&working_dir=~/hydra/public_test',
    headers={'authorization': '{token_type} {access_token}'.format(**token)})
if res.status_code != 200:
    raise RuntimeError('code: {} msg:{}'.format(res.status_code, res.text))
print(res, res.json())
assert(res.status_code == 200)


print()
print("Test list projects")
res = requests.get(url+'/get_projects_list',
    headers={'authorization': '{token_type} {access_token}'.format(**token)})
if res.status_code != 200:
    raise RuntimeError('code: {} msg:{}'.format(res.status_code, res.text))
print(res, res.json())
assert(res.status_code == 200)

print()
print("Test project exists")
res = requests.get(url+'/project_exists?name=test_public',
    headers={'authorization': '{token_type} {access_token}'.format(**token)})
if res.status_code != 200:
    raise RuntimeError('code: {} msg:{}'.format(res.status_code, res.text))
assert(res.status_code == 200)
assert(res.json() == True)
print(res, res.json())

print()
print("Test project that does not exists")
res = requests.get(url+'/project_exists?name=a_project_that_does_not_exist',
    headers={'authorization': '{token_type} {access_token}'.format(**token)})
if res.status_code != 200:
    raise RuntimeError('code: {} msg:{}'.format(res.status_code, res.text))
print(res, res.json())
assert(res.status_code == 200)
assert(res.json() == False)

print()
print("Test adding dem")
res = requests.post(url+'/add_dem?name=test_public',
    files={'zip_': open('dem.zip','rb')},
    headers={'authorization': '{token_type} {access_token}'.format(**token)})
if res.status_code != 200:
    raise RuntimeError('code: {} msg:{}'.format(res.status_code, res.text))
print(res, res.json())
assert(res.status_code == 200)

print()
print("Test dump")
res = requests.get(url+'/dump_project?name=test_public',
    headers={'authorization': '{token_type} {access_token}'.format(**token)})
if res.status_code != 200:
    raise RuntimeError('code: {} msg:{}'.format(res.status_code, res.text))
open('/tmp/dump.sql', 'wb').write(res.content)