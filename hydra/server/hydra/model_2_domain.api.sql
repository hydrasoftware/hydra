/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*                Catchment                       */
/* ********************************************** */

/* add rule affect catchemnt to catchment_node at creation */
create function ${model}.catchment_after_fct()
returns trigger
language plpgsql
as $$$$
    begin
        if tg_op = 'INSERT' then
            update $model.catchment_node set contour=new.id where ST_Intersects($model.catchment_node.geom, new.geom);
            return new;
        else
            if tg_op = 'UPDATE' then
                update $model.catchment_node set contour=null where contour=old.id;
                update $model.catchment_node set contour=new.id where ST_Intersects($model.catchment_node.geom, new.geom);
                return new;
            end if;
        end if;
    end;
$$$$
;;

create trigger ${model}_catchment_after_trig
    after insert or update on $model.catchment
       for each row execute procedure ${model}.catchment_after_fct()
;;

create view ${model}.thiessen_rain_gage as
    with
        vertices as (select ST_Union(geom) as geom from project.rain_gage),
        extend as (select ST_Buffer(ST_Union(ST_Buffer(geom, 1)), -1) as geom from ${model}.catchment),
        voronoi as (select ST_VoronoiPolygons(v.geom, 0, e.geom) as geom from vertices as v, extend as e),
        polygons as (select (ST_Dump(geom)).geom from voronoi)
    select rg.id, rg.name, ST_Intersection(p.geom, e.geom) as geom
    from project.rain_gage as rg, polygons as p, extend as e
    where ST_Contains(p.geom, rg.geom) and ST_intersects(p.geom, e.geom)
;;

/* ********************************************** */
/*                  Constrain lines               */
/* ********************************************** */

create view $model.constrain as
select c.id, c.name, c.geom, discretized, elem_length, constrain_type, link_attributes, points_xyz, points_xyz_proximity,
    exists (select 1
       from project.points_xyz as points
       where c.points_xyz = points.points_id
       and St_DWithin(points.geom, c.geom, c.points_xyz_proximity))  as _is_3d, line_xyz, l.geom as topo, c.comment
from $model._constrain as c
left join project.line_xyz l on l.id = c.line_xyz
;;

create view $model.constrain_3d as
with projected as (
    select c.id, st_linelocatepoint(c.geom, p.geom) as loc, c.geom,
        st_distance(c.geom, p.geom) as dist, p.z_ground as z
    from $model._constrain c
    join project.points_xyz p on st_dwithin(p.geom, c.geom, c.points_xyz_proximity) and c.points_xyz = p.points_id
),
distinct_projected as(
    select distinct on (loc) id, st_lineinterpolatepoint(geom, loc) as geom, loc, z
            from projected
            order by loc, dist -- dictin on + order by makes sure that if two poinst project
                            -- on the same spot (e.g. extremities) we keep only the closest
),
all_points as (
    select c.id, d.geom, st_linelocatepoint(c.geom, (d).geom) as loc, null as z
    from $model._constrain c
    cross join lateral st_dumppoints(c.geom) d
    where c.id in (select distinct id from projected)
    union all
    select * from distinct_projected
),
lz as (
    select id, array_agg(ARRAY[loc, z]) as a
    from distinct_projected
    where z is not null
    group by id
)
select i.id, st_setsrid(st_makeline(st_makepoint(
    st_x(i.geom), st_y(i.geom), coalesce(i.z, project.interpolate_z(i.loc, lz.a))) order by i.loc), $srid) as geom
from all_points i
join lz on lz.id = i.id
group by i.id
;;

create view $model.close_point as
    with allpoints as (
        select id, (st_dumppoints(geom)).geom as geom from $model.constrain where constrain_type is distinct from 'ignored_for_coverages'
    ),
    ends as (
        select id, st_startpoint(geom) as geom from $model.constrain where constrain_type is distinct from 'ignored_for_coverages'
        union
        select id, st_endpoint(geom) as geom from $model.constrain where constrain_type is distinct from 'ignored_for_coverages'
    ),
    cpoints as (
        select id, st_collect(geom) as geom from allpoints group by id
    ),
    clust as (
        select
          sqrt(ST_Area(ST_MinimumBoundingCircle(gc)) / pi()) as radius,
          ST_NumGeometries(gc) as nb,
          ST_Centroid(gc) as geom
        from (
          select unnest(ST_ClusterWithin(geom, 5)) gc
          from allpoints
        ) f
        union
        select
            1 as radius,
            0 as nb,
            st_force2d(e.geom) as geom
        from $model.constrain as c, cpoints as cp, ends as e
        where st_dwithin(c.geom, e.geom, 1)
        and not st_intersects(cp.geom, e.geom)
        and e.id != c.id
        and c.id = cp.id
        and c.constrain_type is distinct from 'ignored_for_coverages'
    )
select row_number() over () AS id, radius, nb, geom::geometry('POINT', $srid) from clust where radius > 0
;;

-- Note that the discretized is not editable
create function ${model}.constrain_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        prec_ real;
        snapped_ geometry;
        other_cst_ geometry;
        differenced_ geometry;
        oned_ geometry;
        discretized_ geometry;
    begin
        select precision from hydra.metadata into prec_;
        if tg_op = 'INSERT' then
            select st_snaptogrid(new.geom, prec_) into snapped_;
            if new.constrain_type != 'ignored_for_coverages' then
                select ST_Linemerge(ST_Collect(geom)) from $model._constrain where ST_Intersects(snapped_, geom) and constrain_type != 'ignored_for_coverages' into other_cst_;
                select ST_LineMerge(coalesce(ST_Difference(snapped_, other_cst_), snapped_)) into differenced_;
            else
                select snapped_ into differenced_;
            end if;
            if not ST_IsEmpty(differenced_) then
                select ST_GeometryN(differenced_, 1) into oned_;
                select st_snaptogrid(project.discretize_line(oned_, coalesce(new.elem_length, 100)), prec_) into discretized_;
                if not (select St_IsEmpty(st_snaptogrid(new.geom, prec_))) then
                    insert into $model._constrain(name, geom, discretized, elem_length, constrain_type, link_attributes, points_xyz, points_xyz_proximity, line_xyz, comment)
                    values (coalesce(new.name, 'define_later'), oned_, discretized_, coalesce(new.elem_length, 100), new.constrain_type, new.link_attributes, new.points_xyz, coalesce(new.points_xyz_proximity, 1), new.line_xyz, new.comment)
                    returning id, elem_length, points_xyz, points_xyz_proximity into new.id, new.elem_length, new.points_xyz, new.points_xyz_proximity ;
                    update $model._constrain set name = 'CONSTR'||new.id::varchar where name = 'define_later' and id = new.id
                    returning name into new.name;
                    return new;
                end if;
                return null;
            end if;
            return null;
        elsif tg_op = 'UPDATE' then
            select st_snaptogrid(new.geom, prec_) into snapped_;
            if new.constrain_type != 'ignored_for_coverages' then
                select ST_Linemerge(ST_Collect(geom)) from $model._constrain where ST_Intersects(snapped_, geom) and id!=old.id and constrain_type != 'ignored_for_coverages' into other_cst_;
                select ST_LineMerge(coalesce(ST_Difference(snapped_, other_cst_), snapped_)) into differenced_;
            else
                select snapped_ into differenced_;
            end if;
            if not ST_IsEmpty(differenced_) then
                select ST_GeometryN(differenced_, 1) into oned_;
                select st_snaptogrid(project.discretize_line(differenced_, coalesce(new.elem_length, 100)), prec_) into discretized_;
                update $model._constrain set name=new.name, geom=differenced_, discretized=discretized_,
                    elem_length=new.elem_length, constrain_type=new.constrain_type, link_attributes=new.link_attributes,
                    points_xyz=new.points_xyz, points_xyz_proximity=coalesce(new.points_xyz_proximity, 1),
                    line_xyz=new.line_xyz, comment=new.comment
                    where id = old.id;
                return new;
            end if;
            return old;
        elsif tg_op = 'DELETE' then
            delete from $model._constrain where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_constrain_trig
    instead of insert or update or delete on $model.constrain
       for each row execute procedure ${model}.constrain_fct()
;;

create function ${model}.constrain_after_fct()
returns trigger
language plpgsql
as $$$$
    declare
        cov integer;
    begin
        if (select trigger_coverage from $model.metadata) then
            select $model.coverage_update() into cov;
        end if;

        if tg_op = 'INSERT' or tg_op = 'UPDATE'  then
            return new;
        else
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_constrain_after_trig
    after insert or update of geom, elem_length or delete on $model._constrain
       execute procedure ${model}.constrain_after_fct()
;;

/* ********************************************** */
/*                    Coverages                   */
/* ********************************************** */

create function ${model}.coverage_after_fct()
returns trigger
language plpgsql
as $$$$
    begin
        if tg_op = 'INSERT' then
            update $model.storage_node set
                contour=new.id
                where ST_Intersects($model.storage_node.geom, new.geom);
            return new;
        else
            if tg_op = 'UPDATE' then
                update $model.storage_node set contour=null where contour=old.id;

                update $model.storage_node set
                    contour=new.id
                    where ST_Intersects($model.storage_node.geom, new.geom);
                    return new;
            end if;
        end if;
    end;
$$$$
;;

create trigger ${model}_coverage_after_trig
    after insert or update of geom on $model.coverage
       for each row execute procedure ${model}.coverage_after_fct()
;;

create function ${model}.mkc_after_fct()
returns trigger
language plpgsql
as $$$$
    declare
        cov_ integer;
    begin
        if (select trigger_coverage from $model.metadata) then
            select $model.coverage_update() into cov_;
        end if;

        if tg_op = 'INSERT' or tg_op = 'UPDATE'  then
            return new;
        else
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_mkc_trig
    after insert or update of geom or delete on $model.coverage_marker
       for each row execute procedure ${model}.mkc_after_fct()
;;

/* ********************************************** */
/*                 2D domain                      */
/* ********************************************** */

-- Add rule to update elem 2d after mkd modification
create function ${model}.mkd_after_fct()
returns trigger
language plpgsql
as $$$$
    declare
        old_cov integer;
        new_cov integer;
    begin
        if tg_op = 'INSERT' then
            select id from $model.coverage as c where st_intersects(c.geom, new.geom) into new_cov;
            update $model.elem_2d_node as n set domain_2d = new.domain_2d from $model.coverage as c where st_intersects(n.geom, c.geom) and c.id = new_cov;
            return new;
        else
            if tg_op = 'UPDATE' then
                select id from $model.coverage as c where st_intersects(c.geom, old.geom) into old_cov;
                update $model.elem_2d_node as n set domain_2d = new.domain_2d from $model.coverage as c where st_intersects(n.geom, c.geom) and c.id = old_cov;
                select id from $model.coverage as c where st_intersects(c.geom, new.geom) into new_cov;
                update $model.elem_2d_node as n set domain_2d = new.domain_2d from $model.coverage as c where st_intersects(n.geom, c.geom) and c.id = new_cov;
                return new;
            else
                select id from $model.coverage as c where st_intersects(c.geom, old.geom) into old_cov;
                update $model.elem_2d_node as n set domain_2d = null from $model.coverage as c where st_intersects(n.geom, c.geom) and c.id = old_cov;
                return old;
            end if;
        end if;
    end;
$$$$
;;

create trigger ${model}_mkd_after_trig
    after insert or update of geom, domain_2d or delete on $model.domain_2d_marker
       for each row execute procedure ${model}.mkd_after_fct()
;;

/* ********************************************** */
/*                     Reach                      */
/* ********************************************** */

/* add rule to create end points when reach is created, the pk are set automagically */
create function ${model}.reach_after_insert_fct()
returns trigger
language plpgsql
as $$$$
    declare
        _up boolean;
        _down boolean;
    begin
        select exists(select 1 from $model.river_node as n where ST_Intersects(n.geom, ST_Buffer(ST_StartPoint(new.geom), 0.1))) into _up;
        select exists(select 1 from $model.river_node as n where ST_Intersects(n.geom, ST_Buffer(ST_EndPoint(new.geom), 0.1))) into _down;

        if not _up then
            insert into $model.river_node(geom) values(ST_StartPoint(new.geom));
        end if;
        if not _down then
            insert into $model.river_node(geom) values(ST_EndPoint(new.geom));
        end if;
        return new;
    end;
$$$$
;;

create trigger ${model}_reach_after_insert_trig
    after insert on $model.reach
       for each row execute procedure ${model}.reach_after_insert_fct()
;;

/* add trigger on reach geom update to recompute the position */
/* shall we update the pk according to the new position ? */
create function ${model}.reach_after_update_fct()
returns trigger
language plpgsql
as $$$$
    begin
        update $model.river_node set
                -- il faut valider cette mise à jour du pk
                geom = (select ST_LineInterpolatePoint(new.geom, ST_LineLocatePoint(new.geom, geom)))
                where reach=new.id
                    and id not in (select id from $model.river_node as n where ST_Intersects(n.geom, ST_LineInterpolatePoint(new.geom, ST_LineLocatePoint(new.geom, n.geom))));
        return new;
    end;
$$$$
;;

create trigger ${model}_reach_after_update_trig
    after update on $model.reach
    for each row execute procedure ${model}.reach_after_update_fct()
;;

/* add trigger on reach to update singularities validities */
create function ${model}.reach_after_fct()
returns trigger
language plpgsql
as $$$$
    begin
        if tg_op = 'INSERT' then
            update $model.bradley_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.bridge_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.hydraulic_cut_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.zregul_weir_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.param_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.regul_sluice_gate_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.gate_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.borda_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            return new;
        else
            if tg_op = 'UPDATE' then
                update $model.bradley_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.bridge_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.hydraulic_cut_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.zregul_weir_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.param_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.regul_sluice_gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.borda_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                return new;
            else
                update $model.bradley_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.bridge_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.hydraulic_cut_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.zregul_weir_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.param_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.regul_sluice_gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.borda_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                return old;
            end if;
        end if;
    end;
$$$$
;;

create trigger ${model}_reach_trig
    after insert or update or delete on $model.reach
       for each row execute procedure ${model}.reach_after_fct()
;;

/* ********************************************** */
/*                     Branch                     */
/* ********************************************** */

create function $model.branch_update_fct()
returns boolean
language plpgsql
as $$$$
    begin
        update $model._pipe_link set branch = null;
        delete from $model.branch;

        -- count up and down node connectivity on pipes
        -- select pass_through manhole nodes that have one up and one down
        -- line merge pipes that have pass_through at both ends
        -- add pipes that are touching

        with up as (
            select n.id, count(1) as ct, n.geom
            from $model.pipe_link as l join $model.manhole_node as n on n.id=l.up
            where not l.exclude_from_branch
            group by n.id, n.geom
        ),
        down as (
            select n.id, count(1) as ct
            from $model.pipe_link as l join $model.manhole_node as n on n.id=l.down
            where not l.exclude_from_branch
            group by n.id
        ),
        blade as (
            select st_collect(n.geom) as geom
            from $model.manhole_node as n
            where id in (
            select id from $model.manhole_node
            except
            select up.id
            from up join down on up.id=down.id
            where up.ct = 1
            and down.ct = 1)
        ),
        branch as (
            select (st_dump(st_split(st_linemerge(st_collect(l.geom)), b.geom))).geom as geom
            from $model.pipe_link as l, blade as b
            where l.up_type='manhole' and not l.exclude_from_branch
            group by b.geom
        ),
        oriented as (
            select (st_dump(st_linemerge(st_collect(p.geom)))).geom as geom, min(p.id) as min_pipe_id
            from branch as b,  $model.pipe_link as p
            where st_covers(b.geom, p.geom) and not p.exclude_from_branch
            group by b.geom
        )
        insert into $model.branch(id, name, geom)
        select row_number() over w, 'BRANCH_'||(row_number() over w)::varchar, geom
        from oriented
        window w as (order by st_length(geom) asc, min_pipe_id asc)
        ;

        update $model.branch as b set name=m.name, dx=m.dx, pk0_km=m.pk0_km
        from $model.pipe_branch_marker_singularity as m where st_intersects(m.geom, b.geom);

        update $model._pipe_link as p set branch = b.id
        from $model._link as l, $model.branch as b
        where p.id = l.id and st_covers(b.geom, l.geom) and not p.exclude_from_branch;

        return 't';
    end;
$$$$
;;

/* add trigger on branch to update singularities validities */
create function ${model}.branch_after_fct()
returns trigger
language plpgsql
as $$$$
    begin
        if tg_op = 'INSERT' then
            update $model.hydraulic_cut_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.zregul_weir_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.param_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.regul_sluice_gate_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.gate_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            update $model.borda_headloss_singularity
                set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom));
            return new;
        else
            if tg_op = 'UPDATE' then
                update $model.hydraulic_cut_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.zregul_weir_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.param_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.regul_sluice_gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.borda_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(new.geom)) or st_intersects(geom, st_endpoint(new.geom))
                    or st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                return new;
            else
                update $model.hydraulic_cut_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.zregul_weir_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.param_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.regul_sluice_gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.gate_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                update $model.borda_headloss_singularity
                    set geom=geom
                where st_intersects(geom, st_startpoint(old.geom)) or st_intersects(geom, st_endpoint(old.geom));
                return old;
            end if;
        end if;
    end;
$$$$
;;

create trigger ${model}_branch_trig
    after insert or update or delete on $model.branch
       for each row execute procedure ${model}.branch_after_fct()
;;

/* ********************************************** */
/*                      Street                    */
/* ********************************************** */

create function ${model}.street_after_fct()
returns trigger
language plpgsql
as $$$$
    declare
        res integer;
    begin
        if new.width != 0 then
            if tg_op = 'INSERT' or (tg_op = 'UPDATE' and old.width=0) then
                perform $model.gen_cst_street(new.id);
            end if;
        end if;
        if tg_op != 'DELETE' then
            if new.width_invert = new.width then
                update $model.street
                set width_invert = null
                where id = old.id;
            end if;
        end if;

        perform $model.crossroad_update();

        return new;
    end;
$$$$
;;

create trigger ${model}_street_after_trig
    after insert or update of width, geom on $model.street
       for each row execute procedure ${model}.street_after_fct();
;;

/* ********************************************** */
/*                      Station                   */
/* ********************************************** */


create function ${model}.station_after_fct()
returns trigger
language plpgsql
as $$$$
    begin
        if tg_op = 'INSERT' then
            update $model.river_node
                set geom=geom
                where st_intersects(geom, new.geom);
            update $model.manhole_node
                set geom=geom
                where st_intersects(geom, new.geom);
            return new;
        else
            if tg_op = 'UPDATE' then
                update $model.river_node
                    set geom=geom
                    where st_intersects(geom, old.geom) or st_intersects(geom, new.geom);
                update $model.manhole_node
                    set geom=geom
                    where st_intersects(geom, old.geom) or st_intersects(geom, new.geom);
                return new;
            else
                update $model.river_node
                    set geom=geom
                    where st_intersects(geom, old.geom);
                update $model.manhole_node
                    set geom=geom
                    where st_intersects(geom, old.geom);
                return old;
            end if;
        end if;
    end;
$$$$
;;

create trigger ${model}_station_trig
    after insert or update or delete on $model.station
       for each row execute procedure ${model}.station_after_fct()
;;
