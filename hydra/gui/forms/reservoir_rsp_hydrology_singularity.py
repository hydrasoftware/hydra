# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_singularity_dialog import SimpleSingularityDialog, tr
from hydra.utility.settings_properties import SettingsProperties
import hydra.utility.string as string
import hydra.utility.sql_json as sql_json
from ..widgets.graph_widget import GraphWidget
from ..widgets.array_widget import ArrayWidget
from hydra.utility.log import LogManager, ConsoleLogger
from qgis.PyQt import uic
import os

class ReservoirRspHydrologySingularityEditor(SimpleSingularityDialog):
    def __init__(self, project, geom, id=None, parent=None, iface=None, size=10):
        SimpleSingularityDialog.__init__(self, project, geom, "reservoir_rsp_hydrology_singularity", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "reservoir_rsp_hydrology_singularity.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.dir = current_dir
        self.__size = size
        self.__iface = iface
        self.__log_manager = LogManager(ConsoleLogger(), "Hydra")

        self.treatment_values = self.project.execute("""
            select description, name
            from hydra.pollution_treatment_mode""").fetchall()

        self.btn_drainage.clicked.connect(self.__select_drainage)
        self.btn_overflow.clicked.connect(self.__select_overflow)

        if iface==None:
            self.btn_drainage.setEnabled(False)
            self.btn_overflow.setEnabled(False)

        self.node, name, zr_sr_qf_qs_array, drainage, drainage_type, overflow, overflow_type, z_ini, treatment_mode, treatment_param, comment = self.project.execute("""
            select node, name, zr_sr_qf_qs_array, drainage, drainage_type, overflow, overflow_type, z_ini, treatment_mode, treatment_param::varchar, comment
            from {}.reservoir_rsp_hydrology_singularity
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.name.setText(name)
        self.notes.setText(comment)
        self.z_ini.setText(string.get_str(z_ini))

        # drainage
        if drainage:
            drainage_name, = self.project.execute("""select name from {}._link where id={}""".format(self.model.name, drainage)).fetchone()
            self.drainage.setText(drainage_name)
        # overflow
        if overflow:
            overflow_name, = self.project.execute("""select name from {}._link where id={}""".format(self.model.name, overflow)).fetchone()
            self.overflow.setText(overflow_name)

        self.array = ArrayWidget(["Elevation (m)", "Surface (m2)", "Drainage flow rate (m3/s)", "Overflow flow rate (m3/s)"], [10,4], zr_sr_qf_qs_array,
                draw_graph_func=self.__draw_graph, help_func=None, parent=self.array_placeholder)

        # initialize treatment type
        self.populate_combo_from_sqlenum(self.treatment_mode, "pollution_treatment_mode", treatment_mode)
        self.treatment_mode.activated.connect(self.treatmentchanged)
        self.treatment_param.setVisible(False)
        self.treatmentchanged()
        # initialize law parameters
        sql_json.set_fields_to_json(self, 'pollution_treatment_mode', treatment_param)

        self.btn_pollution.clicked.connect(self.__pollution_clicked)
        self.pollution_groupbox.setVisible(False)

        self.show()

    def __pollution_clicked(self):
        self.pollution_groupbox.setVisible(not self.pollution_groupbox.isVisible())

    def treatmentchanged(self):
        for value in self.treatment_values:
            corrected_value = value if value[0] is not None else [value[1],value[1]]
            if self.treatment_mode.currentIndex() != -1:
                if self.treatment_mode.currentText() == corrected_value[0]:
                    if corrected_value[1] == 'residual_concentration':
                        self.treatment_param.setVisible(True)
                        self.treatment_param.setCurrentIndex(0)
                    elif corrected_value[1] == 'efficiency':
                        self.treatment_param.setVisible(True)
                        self.treatment_param.setCurrentIndex(1)

    def __draw_graph(self, datas):
        #: nothing to draw here
        pass

    def __select_drainage(self):
        self.__select_link('drainage')

    def __select_overflow(self):
        self.__select_link('overflow')

    def __select_link(self, field):
        from hydra.utility.item_selector import ItemSelector

        i_selector = ItemSelector(self.project, self.__iface)
        self.hide()

        i_selector.select(['pipe_link', 'connector_hydrology_link'], " and (up={n} or down={n})".format(n=self.node))
        name, id, table = i_selector.get_selected_item_infos()

        if name:
            getattr(self, field).setText(name)
        self.stash()
        ReservoirRspHydrologySingularityEditor(self.project, None, self.id, None, self.__iface, self.__size).exec_()
        self.ignore_rollback = True
        self.close()

    def get_id(self):
        return self.id

    def stash(self):
        # zr_sr_qf_qs_array
        zr_sr_qf_qs_array = self.array.get_table_items()

        # treatment mode
        treatment_mode = None
        for value in self.treatment_values:
            corrected_value = value if value[0] is not None else [value[1],value[1]]
            if self.treatment_mode.currentIndex() != -1:
                if self.treatment_mode.currentText() == corrected_value[0]:
                    treatment_mode = corrected_value[1]

        # treatment param
        treatment_param = sql_json.build_json_from_fields(self, 'pollution_treatment_mode')

        #drainage/overflow
        drainage = None
        overflow = None
        if self.drainage.text():
            drainage, = self.project.execute("""select id from {}._link where name='{}'""".format(self.model.name, self.drainage.text())).fetchone()
        if self.overflow.text():
            overflow, = self.project.execute("""select id from {}._link where name='{}'""".format(self.model.name, self.overflow.text())).fetchone()

        self.model.update_singularity_rsph(self.id, self.geom,
                            drainage, overflow, string.get_sql_float(self.z_ini.text()),
                            zr_sr_qf_qs_array, treatment_mode, treatment_param, self.name.text(), comment=self.notes.text())

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.stash()
        SimpleSingularityDialog.save(self)
        self.close()





