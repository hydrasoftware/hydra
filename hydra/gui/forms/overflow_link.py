# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_link_dialog import SimpleLinkDialog, tr
import hydra.utility.string as string
from qgis.PyQt import uic, QtGui
from ..widgets.graph_widget import GraphWidget
from ..widgets.array_widget import ArrayWidget
import os
import re

class OverflowLinkEditor(SimpleLinkDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleLinkDialog.__init__(self, project, geom, "overflow_link", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "overflow_link.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        name, z_crest1, z_crest2, width1, width2, cc, lateral_contraction_coef, break_mode, z_break, t_break, z_invert, width_breach, grp, dt_fracw_array, comment = self.project.execute("""
            select name, z_crest1, z_crest2, width1, width2, cc, lateral_contraction_coef, break_mode,
            z_break, t_break, z_invert, width_breach, grp, dt_fracw_array, comment
            from {}.overflow_link
            where id={}""".format(self.model.name, self.id)).fetchone()

        self.name.setText(name)
        self.notes.setText(comment)
        self.z_crest1.setText(string.get_str(z_crest1))
        self.z_crest2.setText(string.get_str(z_crest2))
        self.width1.setText(string.get_str(width1))
        self.width2.setText(string.get_str(width2))
        self.cc.setText(string.get_str(cc))
        self.lateral_contraction_coef.setText(string.get_str(lateral_contraction_coef))
        self.z_break.setText(string.get_str(z_break))
        self.t_break.setText(string.get_str(t_break))
        self.z_invert.setText(string.get_str(z_invert))
        self.width_breach.setText(string.get_str(width_breach))
        if grp is not None:
            self.grp.setValue(int(string.get_sql_float(grp)))
        else:
            self.grp.setValue(1)

        if break_mode == None:
            break_mode = 'none'

        self.break_modes = self.populate_combo_from_sqlenum(self.break_mode,
            "fuse_spillway_break_mode", break_mode)
        self.break_mode.activated.connect(self.__break_mode_changed)
        self.__break_mode_changed()

        self.graph = GraphWidget(self.graph_placeholder)
        self.array = ArrayWidget([tr("dt (h)"), tr("Fract. ratio (0-1)")], [10,2], dt_fracw_array,
                draw_graph_func=self.__draw_graph, help_func=None, parent=self.array_placeholder)

    def __draw_graph(self, datas):
        if len(datas)>0:
            self.graph.clear()
            self.graph.add_line(
                [r[0] for r in datas],
                [r[1] for r in datas],
                "k",
                self.name.text(),
                tr("dt (h)"), tr("Fractured width (m)"))
        else:
            self.graph.canvas.setVisible(False)

    def __break_mode_changed(self):
        self.z_break.hide()
        self.label_z_break.hide()
        self.t_break.hide()
        self.label_t_break.hide()
        self.tab.setTabEnabled(1, True)
        for break_mode in self.break_modes:
            if break_mode[0] == self.break_mode.currentText():
                if break_mode[1] == 'none':
                    self.tab.setTabEnabled(1, False)
                elif break_mode[1] == 'zw_critical':
                    self.z_break.show()
                    self.label_z_break.show()
                elif break_mode[1] == 'time_critical':
                    self.t_break.show()
                    self.label_t_break.show()
    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))

        break_mode = self.project.execute("""
            select name
            from hydra.fuse_spillway_break_mode
            where description='{}'""".format(self.break_mode.currentText())).fetchone()[0]

        self.model.update_overflow_link(self.id, self.geom,
                                        string.get_sql_float(self.z_crest1.text()), string.get_sql_float(self.z_crest2.text()),
                                        string.get_sql_float(self.width1.text()), string.get_sql_float(self.width2.text()),
                                        string.get_sql_float(self.cc.text()), string.get_sql_float(self.lateral_contraction_coef.text()),
                                        break_mode,
                                        string.get_sql_float(self.z_break.text()), string.get_sql_float(self.t_break.text()),
                                        string.get_sql_float(self.z_invert.text()), string.get_sql_float(self.width_breach.text()),
                                        self.grp.value(), self.array.get_table_items(), self.name.text(), comment=self.notes.text())

        SimpleLinkDialog.save(self)
        self.close()







