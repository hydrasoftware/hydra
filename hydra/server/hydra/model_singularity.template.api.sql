/* ****************************************************************************************** */
/*                                                                                            */
/*     This file is part of HYDRA, a QGIS plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      */
/*     of Setec Hydratec, Paris.                                                              */
/*                                                                                            */
/*     Contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     You can use this program under the terms of the GNU General Public                     */
/*     License as published by the Free Software Foundation, version 3 of                     */
/*     the License.                                                                           */
/*                                                                                            */
/*     You should have received a copy of the GNU General Public License                      */
/*     along with this program. If not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

create view $model.$tablename as
    select p.id, p.name, n.id as node, $tableview, n.geom, p.configuration::varchar as configuration, p.validity, p.configuration as configuration_json, p.comment
    from $model._$tablename as c, $model._singularity as p, $model._node as n
    where p.id = c.id and n.id = p.node

;;

create function ${model}.${tablename}_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration, comment)
                values (nid_, node_type_, $tableid, coalesce(new.name, 'define_later'), new.configuration::json, new.comment) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = $tableid) where name = 'define_later' and id = id_;

            insert into $model._$tablename(id, singularity_type, $tablecolumns)
                values (id_, $tableid, $tableinsert);
            if $tableid = $pipe_branch_marker_singularity_type and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            if $tableid = $hydrograph_bc_singularity_type then
                update $model.routing_hydrology_link set hydrograph=id_ where down=nid_;
                update $model.connector_hydrology_link set hydrograph=id_ where down=nid_;
            end if;
            perform $model.add_configuration_fct(new.configuration::json, id_, '$tablename');
            update $model._singularity set validity = (select $validity from  $model._$tablename where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if (($newconfigfields) is distinct from ($oldconfigfields)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select $oldconfigfields) as o, (select $newconfigfields) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select $newconfigfields) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name, comment=new.comment where id=old.id;
            ${commentupdate}update $model._$tablename set $tableupdate where id=old.id;
            if $tableid = $pipe_branch_marker_singularity_type and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            if $tableid = $hydrograph_bc_singularity_type then
                update $model.routing_hydrology_link set hydrograph=null where down=old.node;
                update $model.connector_hydrology_link set hydrograph=null where down=old.node;
                update $model.routing_hydrology_link set hydrograph=new.id where down=new.node;
                update $model.connector_hydrology_link set hydrograph=new.id where down=new.node;
            end if;
            perform $model.add_configuration_fct(new.configuration::json, old.id, '$tablename');
            update $model._singularity set validity = (select $validity from  $model._$tablename where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            if $tableid = $hydrograph_bc_singularity_type then
                update $model.routing_hydrology_link set hydrograph=null where down=old.node;
                update $model.connector_hydrology_link set hydrograph=null where down=old.node;
            end if;
            delete from $model._$tablename where id=old.id;
            delete from $model._singularity where id=old.id;
            if $tableid = $pipe_branch_marker_singularity_type and (select trigger_branch from $model.metadata) then
                perform $model.branch_update_fct();
            end if;
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_${tablename}_trig
    instead of insert or update or delete on $model.$tablename
       for each row execute procedure ${model}.${tablename}_fct()
;;


