# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.forms.gate_link_guitest [-dhs] [-g project_name model_name x y x y]

OPTIONS
   -s  --solo
        run test in solo mode (create database)

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name model_name x y
        run in gui mode

"""

from hydra.database.database import TestProject, remove_project, project_exists
from hydra.utility.string import normalized_name, normalized_model_name
import sys
import getopt

from hydra.project import Project

def gui_mode(project_name, model_name, geom, id):

    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.gate_link import GateLinkEditor
    from hydra.utility.string import normalized_name, normalized_model_name

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    obj_project = Project.load_project(project_name)
    obj_project.set_current_model(model_name)

    test_dialog = GateLinkEditor(obj_project, geom, id)
    test_dialog.exec_()


def test():
    from qgis.PyQt.QtWidgets import QApplication
    from qgis.PyQt.QtCore import QCoreApplication, QTranslator
    from hydra.gui.forms.gate_link import GateLinkEditor
    from hydra.database.database import TestProject

    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)

    obj_project.set_current_model(model_name)
    model = obj_project.get_current_model()

    model.add_manhole([-3, 1])
    model.add_manhole([4, 2])

    test_dialog = GateLinkEditor(obj_project, [[-3,1],[4,2]])
    test_dialog.name.setText("Test_gate_link")
    test_dialog.z_invert.setText(str(10))
    test_dialog.z_ceiling.setText(str(12))
    test_dialog.width.setText(str(98))
    test_dialog.cc.setText(str(0.8))
    test_dialog.cc_submerged.setText(str(0.555))
    test_dialog.chk_cc_submerged.setChecked(True)
    test_dialog.z_gate.setText(str(11))
    test_dialog.v_max_cms.setText(str(100))
    test_dialog.upward_opening_gate.setChecked(True)
    test_dialog.downward_opening_gate.setChecked(True)
    test_dialog.mode_valve.setCurrentIndex(test_dialog.mode_valve.findText('one way valve (toward downstream)'))
    id = test_dialog.get_id()
    test_dialog.save()

    test_dialog = GateLinkEditor(obj_project, None, id)
    assert test_dialog.name.text()=="Test_gate_link"
    assert float(test_dialog.z_invert.text())==10
    assert float(test_dialog.z_ceiling.text())==12
    assert float(test_dialog.width.text())==98
    assert float(test_dialog.cc.text())==0.8
    assert float(test_dialog.cc_submerged.text())==0.555
    assert test_dialog.chk_cc_submerged.isChecked()
    assert float(test_dialog.z_gate.text())==11
    assert float(test_dialog.v_max_cms.text())==100
    assert test_dialog.downward_opening_gate.isChecked()
    assert test_dialog.mode_valve.currentText()=='one way valve (toward downstream)'
    test_dialog.reject()

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])

except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name)
    test()
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    model_name = args[1]
    geom = [float(a) for a in args[2:]]
    id =geom[2] if len(geom)==3 else None
    project_name = normalized_name(project_name)
    model_name = normalized_model_name(model_name)
    gui_mode(project_name, model_name, geom, id)
    exit(0)

test()
# fix_print_with_import
print("ok")


