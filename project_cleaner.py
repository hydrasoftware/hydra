# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""get rid of useless test projects

USAGE
    scripts.project_cleaner [-t, -h]
        deletes all prjects ending with "_test"

OPTIONS
    -t, --template
        also delete template_project

    -h, --help
        print this help

    -a, --all
        delete all databases

"""

from __future__ import absolute_import # important to read the doc !
import os
import sys
import getopt
from hydra.database import database
from hydra.utility.settings_properties import SettingsProperties

current_dir = os.path.dirname(__file__)
hydra_dir = os.path.join(os.path.expanduser("~"), ".hydra")

def get_directories(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

def remove_project(project_name):
    if project_name in database.get_projects_list():
        database.remove_project(project_name)
        print('Project "', project_name, '" removed')

def clean(all=False):
    for project in database.get_projects_list():
        if (project[-5:] == '_test' or all):
            remove_project(project)
    for directory in get_directories(hydra_dir):
        if (directory[-5:] == '_test' or directory not in database.get_projects_list() or all):
            remove_project(directory)

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hta",
            ["help", "template", "all"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)
if 'PGSERVICEFILE' not in os.environ:
    os.environ['PGSERVICEFILE'] = os.path.join(os.path.expanduser('~'), '.pg_service.conf')

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

if "-t" in optlist or "--template" in optlist:
    remove_project('template_project')

clean()

if "-a" in optlist or "--all" in optlist:
    valid = {"yes": True, "y": True, "ye": True,"no": False, "n": False, '':False}
    sys.stdout.write(f"Are you sure you want to delete all databases on service {SettingsProperties.get_service()} from {os.environ['PGSERVICEFILE']}? [y/N]")
    choice = input().lower()
    if choice in valid:
        if valid[choice]:
            clean(all=True)
    else:
        sys.stdout.write("Answer not clear.\n")



