/* ********************************************************************************** */
/* Update river_node and set Z geom to z_invert :                                     */
/*     + set Z geom to z_invert of a river_node                                       */
/*     + will update only river_node on a reach                                       */
/*                                                                                    */
/* ********************************************************************************** */

update $model.river_node
set geom = St_setSrid(St_MakePoint(St_X(geom), St_Y(geom), $model.river_node_z_invert(river_node.geom)),St_Srid(geom))
where reach is not null
;;