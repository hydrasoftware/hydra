# -*- coding: utf-8 -*-

"""
***************************************************************************
    gagewatershed.py
    ---------------------
    Date                 : January 2018
    Copyright            : (C) 2018-2019 by Alexander Bruy
    Email                : alexander dot bruy at gmail dot com
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Alexander Bruy'
__date__ = 'January 2018'
__copyright__ = '(C) 2018-2019, Alexander Bruy'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.core import (QgsVectorFileWriter,
                       QgsProcessing,
                       QgsProcessingException,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterRasterDestination,
                       QgsProcessingParameterFileDestination
                      )

from .taudemAlgorithm import TauDemAlgorithm
from . import taudemUtils


class GageWatershed(TauDemAlgorithm):

    D8_FLOWDIR = 'D8_FLOWDIR'
    OUTLETS = 'OUTLETS'
    GAGE_WATERSHED = 'GAGE_WATERSHED'
    WATERSHED_CONNECTIVITY = 'WATERSHED_CONNECTIVITY'

    def name(self):
        return 'gagewatershed'

    def displayName(self):
        return self.tr('Gage watershed')

    def group(self):
        return self.tr('Stream network analysis')

    def groupId(self):
        return 'streamanalysis'

    def tags(self):
        return self.tr('dem,hydrology,gage,watershed').split(',')

    def shortHelpString(self):
        return self.tr('Calculates Gage watersheds grid.')

    def helpUrl(self):
        return 'http://hydrology.usu.edu/taudem/taudem5/help53/GageWatershed.html'

    def __init__(self):
        super().__init__()

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterRasterLayer(self.D8_FLOWDIR,
                                                            self.tr('D8 flow directions')))
        self.addParameter(QgsProcessingParameterFeatureSource(self.OUTLETS,
                                                              self.tr('Outlets'),
                                                              types=[QgsProcessing.TypeVectorPoint]))
        self.addParameter(QgsProcessingParameterRasterDestination(self.GAGE_WATERSHED,
                                                                  self.tr('Gage watershed')))
        self.addParameter(QgsProcessingParameterFileDestination(self.WATERSHED_CONNECTIVITY,
                                                                self.tr('Watershed downslope connectivity'),
                                                                self.tr('Text files (*.txt)'),
                                                                optional=True,
                                                                createByDefault=False))

    def processAlgorithm(self, parameters, context, feedback):
        d8Layer = self.parameterAsRasterLayer(parameters, self.D8_FLOWDIR, context)
        if d8Layer is None:
            raise QgsProcessingException(self.invalidRasterError(parameters, self.D8_FLOWDIR))

        outlets = self.parameterAsSource(parameters, self.OUTLETS, context)
        if outlets is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.OUTLETS))

        arguments = []
        arguments.append(self.command())

        arguments.append('-p')
        arguments.append(d8Layer.source())

        outlets = self.parameterAsCompatibleSourceLayerPath(parameters, self.OUTLETS, context,
                                                            QgsVectorFileWriter.supportedFormatExtensions(),
                                                            feedback=feedback)
        arguments.append('-o')
        arguments.append(outlets)

        arguments.append('-gw')
        arguments.append(self.parameterAsOutputLayer(parameters, self.GAGE_WATERSHED, context))

        outputFile = self.parameterAsFileOutput(parameters, self.WATERSHED_CONNECTIVITY, context)
        if outputFile:
            arguments.append('-id')
            arguments.append(outputFile)

        taudemUtils.execute(arguments, feedback)

        return self.algorithmResults(parameters)
