from builtins import str
# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QWidget, QGridLayout
from qgis.PyQt.QtCore import QRect

class DefaultValueWidget(QWidget):
    def __init__(self, parent):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "default_value_widget.ui"), self)
        self.tmplayout = QGridLayout()
        self.tmplayout.addWidget(self,0,0)
        self.activate_checkbox.stateChanged.connect(self.__activate)
        self.default_value.setEnabled(False)

    def __activate(self) :
        if self.activate_checkbox.isChecked() :
            self.default_value.setEnabled(True)
        else :
            self.default_value.setEnabled(False)
            self.default_value.setText('')

    def get_default_value(self) :
        if self.activate_checkbox.isChecked() :
            return str(self.default_value.text())
        else :
            return None