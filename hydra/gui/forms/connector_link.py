# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from ..simple_link_dialog import SimpleLinkDialog, tr
from qgis.PyQt import uic
import os

class ConnectorLinkEditor(SimpleLinkDialog):
    def __init__(self, project, geom, id=None, parent=None):
        SimpleLinkDialog.__init__(self, project, geom, "connector_link", id, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "connector_link.ui"), self)
        title = self.windowTitle()
        self.setWindowTitle(title + self.get_info())
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        name, main_branch, comment = self.project.execute("""
            select name, main_branch, comment
            from {}.connector_link
            where id={}""".format(self.model.name, self.id)).fetchone()
        self.name.setText(name)
        self.main_branch.setChecked(main_branch)
        self.notes.setText(comment)

    def get_id(self):
        return self.id

    def save(self):
        self.project.log.notice(tr("Saved"))
        self.model.update_connector_link(self.id, self.geom, self.main_branch.isChecked(), self.name.text(), comment=self.notes.text())
        SimpleLinkDialog.save(self)
        self.close()
