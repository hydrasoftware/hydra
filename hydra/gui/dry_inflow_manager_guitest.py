# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m hydra.gui.dry_inflow_manager_guitest [-dhs] [-g project_name]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -g  --gui project_name
        run in gui mode

   -s  --solo
        run test in solo mode (create database)
"""

import os
import sys
import getopt
from hydra.utility.string import normalized_name, normalized_model_name
from hydra.project import Project
from qgis.PyQt.QtWidgets import QApplication, QTableWidgetItem
from qgis.PyQt.QtCore import QCoreApplication, QTranslator
from hydra.gui.dry_inflow_manager import DryInflowManager
from ..database.database import TestProject, remove_project, project_exists

def gui_mode(project_name):
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    obj_project = Project.load_project(project_name)

    test_dialog = DryInflowManager(obj_project)
    test_dialog.exec_()


def test():
    import sys
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("i18n/fr", "../..")
    QCoreApplication.installTranslator(translator)
    guitranslator = QTranslator()
    guitranslator.load("/usr/share/qt4/translations/qt_fr")
    QCoreApplication.installTranslator(guitranslator)

    current_dir = os.path.abspath(os.path.dirname(__file__))

    project_name = "gui_test"
    model_name = "model"

    obj_project = Project.load_project(project_name)

    obj_project.execute("insert into project.dry_inflow_sector(name,geom) values('sect_app_ts_1','SRID=2154; POLYGON((988700 6561800, 991000 6561800, 991000 6559500, 988700 6559500, 988700 6561800))'::geometry);")
    obj_project.execute("insert into project.dry_inflow_sector(name,geom) values('sect_app_ts_2','SRID=2154; POLYGON((988700 6561800, 991000 6561800, 991000 6559500, 988700 6559500, 988700 6561800))'::geometry);")
    obj_project.execute("insert into project.dry_inflow_sector(name,geom) values('sect_app_ts_3','SRID=2154; POLYGON((988700 6561800, 991000 6561800, 991000 6559500, 988700 6559500, 988700 6561800))'::geometry);")

    test_dialog = DryInflowManager(obj_project)

    test_dialog.new_dry_scenario()
    test_dialog.new_dry_scenario()
    test_dialog.new_dry_scenario()
    last_index = test_dialog.table_dry_inflow_scenario.table.rowCount() -1
    test_dialog.table_dry_inflow_scenario.table.selectRow(last_index)
    test_dialog.delete_dry_scenario()

    test_dialog.table_dry_inflow_scenario.table.selectRow(0)

    for i in range(2, 6):
        test_dialog.table_dry_inflow_scenario_settings.setItem(0, i, QTableWidgetItem("1"))
        test_dialog.table_dry_inflow_scenario_settings.setItem(1, i, QTableWidgetItem("2"))

    test_dialog.table_dry_inflow_scenario.table.selectRow(1)
    test_dialog.table_dry_inflow_scenario.table.selectRow(0)

    settings = test_dialog.get_dry_inflow_scenario_settings_table_items()
    for i in range(2, 6):
        assert float(settings[0][i])==1
        assert float(settings[1][i])==2

    missing,valid = test_dialog.import_file(os.path.join(current_dir,"test_data","test_dry_inflow.csv"))

    assert len(valid) == 3
    assert len(missing) == 2

    assert valid[0][0] == 'sect_app_ts_1'
    assert valid[1][2] == str(124500)

    assert [name for (name,) in obj_project.execute("""select name from project.dry_inflow_scenario where name = 'test_scen_dry'""").fetchall()][0] == 'test_scen_dry'
    assert obj_project.execute("""select count(*) from project.dry_inflow_scenario_sector_setting""").fetchone()[0] == 9

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdgs",
            ["help", "debug", "gui", "solo"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-s" in optlist or "--solo" in optlist:
    if not project_exists("template_project"):
        sys.stdout.write("creating template database... ")
        TestProject.reset_template()
        sys.stdout.write("ok\n")
    project_name = "gui_test"
    testproject = TestProject(project_name)
    test()
    # fix_print_with_import
    print("ok")
    exit(0)

if "-g" in optlist or "--gui" in optlist:
    project_name = args[0]
    project_name = normalized_name(project_name)
    gui_mode(project_name)
    exit(0)

test()
# fix_print_with_import

# fix_print_with_import
print("ok")
exit(0)
