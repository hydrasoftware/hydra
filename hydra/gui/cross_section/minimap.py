# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from qgis.PyQt.QtWidgets import QLabel
from qgis.PyQt.QtGui import QImage, QPainter, QColor, QPixmap
from qgis.core import QgsMapSettings, QgsRectangle, QgsMapRendererCustomPainterJob, QgsProject, QgsVectorLayer, QgsFeature, QgsGeometry, QgsPoint
import os
from hydra.utility.timer import Timer


_current_dir = os.path.dirname(__file__)
_qml_dir = os.path.join(_current_dir, "..", "..", "ressources", "qml")

class MiniMap(QLabel):
    def __init__(self, map_settings=None, parent=None):
        super().__init__(parent)
        self.__map_setting = map_settings

    def set_transect(self, transect):
        timer = Timer()

        if transect is None:
            img = QImage(self.size(), QImage.Format_ARGB32_Premultiplied)
            img.fill(QColor(255,255,255))
            self.setPixmap(QPixmap.fromImage(img))
            return

        if len(transect):
            dynamic_layer = QgsVectorLayer("LineString", "transect", "memory")
            feat = QgsFeature()
            feat.setGeometry(QgsGeometry.fromPolyline([QgsPoint(x[0], x[1]) for x in transect]))
            dynamic_layer.dataProvider().addFeatures([feat])
            dynamic_layer.loadNamedStyle(os.path.join(_qml_dir, "transect.qml"))
        else:
            dynamic_layer = None

        ms = None # self.__map_setting
        if ms is None:
            #print("not using qgis map settings")
            ms = QgsMapSettings()
            ms.setBackgroundColor(QColor(255,255,255))
            ms.setLayers([dynamic_layer] + [l for l in QgsProject.instance().layerTreeRoot().layerOrder() if l.name() in ('Reach', 'Terrain points', 'Constrain', 'Cross section', 'Coverage')])
        # else:
        #     ms.setLayers(list(ms.layers()) + [dynamic_layer])


        if dynamic_layer is not None:
            rect = QgsRectangle(dynamic_layer.extent())
            rect.scale(1.1)
            ms.setExtent(rect)
        # else:
        #     ms.setExtent(self.extent)
        # TODO handle the case of no transect (zoom on PT)

        img = QImage(self.size(), QImage.Format_ARGB32_Premultiplied)

        ms.setOutputSize(img.size())

        p = QPainter()
        p.begin(img)
        p.setRenderHint(QPainter.Antialiasing)
        render = QgsMapRendererCustomPainterJob(ms, p)
        render.start()
        render.waitForFinished()

        p.end()
        self.setPixmap(QPixmap.fromImage(img))
        #print(timer.reset("render minimap"))