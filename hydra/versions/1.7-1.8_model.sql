/* ****************************************************************************************** */
/*                                                                                            */
/*     this file is part of hydra, a qgis plugin for hydraulics                               */
/*     (see <http://hydra-software.net/>).                                                    */
/*                                                                                            */
/*     copyright (c) 2017 by hydra-software, which is a commercial brand                      */
/*     of setec hydratec, paris.                                                              */
/*                                                                                            */
/*     contact: <contact@hydra-software.net>                                                  */
/*                                                                                            */
/*     you can use this program under the terms of the gnu general public                     */
/*     license as published by the free software foundation, version 3 of                     */
/*     the license.                                                                           */
/*                                                                                            */
/*     you should have received a copy of the gnu general public license                      */
/*     along with this program. if not, see <http://www.gnu.org/licenses/>.                   */
/*                                                                                            */
/* ****************************************************************************************** */

/* ********************************************** */
/*  update version                                */
/* ********************************************** */

update $model.metadata set version = '1.8';
;;

/* ********************************************** */
/*  CC denoye on gates                            */
/* ********************************************** */

alter table ${model}._gate_singularity add column if not exists cc_submerged real;
alter table ${model}._gate_link add column if not exists cc_submerged real;
alter table ${model}._regul_sluice_gate_singularity add column if not exists cc_submerged real;
alter table ${model}._regul_gate_link add column if not exists cc_submerged real;
;;


drop view $model.gate_singularity cascade;
;;

create or replace view $model.gate_singularity as
    select p.id,
        p.name,
        n.id as node,
        c.z_invert,
        c.z_ceiling,
        c.width,
        c.cc,
        c.action_gate_type,
        c.mode_valve,
        c.z_gate,
        c.v_max_cms,
        c.full_section_discharge_for_headloss,
        c.cc_submerged,
        n.geom,
        p.configuration::character varying AS configuration,
        p.validity,
        p.configuration AS configuration_json
    from $model._gate_singularity c,
        $model._singularity p,
        $model._node n
    where p.id = c.id and n.id = p.node;
;;

create or replace function ${model}.gate_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'gate', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'gate') where name = 'define_later' and id = id_;

            insert into $model._gate_singularity(id, singularity_type, z_invert, z_ceiling, width, cc, action_gate_type, mode_valve, z_gate, v_max_cms, full_section_discharge_for_headloss, cc_submerged)
                values (id_, 'gate', new.z_invert, new.z_ceiling, new.width, coalesce(new.cc, .6), coalesce(new.action_gate_type, 'upward_opening'), coalesce(new.mode_valve, 'no_valve'), coalesce(new.z_gate, new.z_ceiling), coalesce(new.v_max_cms, .2), coalesce(new.full_section_discharge_for_headloss, 't'), coalesce(new.cc_submerged, 1));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'gate_singularity');
            update $model._singularity set validity = (select (z_invert is not null) and (z_ceiling is not null) and (z_ceiling>z_invert) and (width is not null) and (cc is not null) and (cc <=1) and (cc >=0) and (action_gate_type is not null) and (mode_valve is not null) and (z_gate is not null) and (z_gate>=z_invert) and (z_gate<=z_ceiling) and (v_max_cms is not null) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) and (cc_submerged >0) from  $model._gate_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.mode_valve, new.z_gate, new.v_max_cms, new.full_section_discharge_for_headloss, new.cc_submerged) is distinct from (old.z_invert, old.z_ceiling, old.width, old.cc, old.action_gate_type, old.mode_valve, old.z_gate, old.v_max_cms, old.full_section_discharge_for_headloss, old.cc_submerged)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.z_ceiling, old.width, old.cc, old.action_gate_type, old.mode_valve, old.z_gate, old.v_max_cms, old.full_section_discharge_for_headloss, old.cc_submerged) as o, (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.mode_valve, new.z_gate, new.v_max_cms, new.full_section_discharge_for_headloss, new.cc_submerged) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.mode_valve, new.z_gate, new.v_max_cms, new.full_section_discharge_for_headloss, new.cc_submerged) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._gate_singularity set z_invert=new.z_invert, z_ceiling=new.z_ceiling, width=new.width, cc=new.cc, action_gate_type=new.action_gate_type, mode_valve=new.mode_valve, z_gate=new.z_gate, v_max_cms=new.v_max_cms, full_section_discharge_for_headloss=new.full_section_discharge_for_headloss, cc_submerged=new.cc_submerged where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'gate_singularity');
            update $model._singularity set validity = (select (z_invert is not null) and (z_ceiling is not null) and (z_ceiling>z_invert) and (width is not null) and (cc is not null) and (cc <=1) and (cc >=0) and (action_gate_type is not null) and (mode_valve is not null) and (z_gate is not null) and (z_gate>=z_invert) and (z_gate<=z_ceiling) and (v_max_cms is not null) and (not $model.check_on_branch_or_reach_endpoint(new.geom)) and (cc_submerged >0) from  $model._gate_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._gate_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_gate_singularity_trig
    instead of insert or update or delete on $model.gate_singularity
       for each row execute procedure ${model}.gate_singularity_fct()
;;




drop view $model.gate_link cascade;
;;

create or replace view $model.gate_link as
    select p.id,
        p.name,
        p.up,
        p.down,
        c.z_invert,
        c.z_ceiling,
        c.width,
        c.cc,
        c.action_gate_type,
        c.mode_valve,
        c.z_gate,
        c.v_max_cms,
        c.cc_submerged,
        p.geom,
        p.configuration::character varying AS configuration,
        p.generated,
        p.validity,
        p.up_type,
        p.down_type,
        p.configuration AS configuration_json
    from $model._gate_link c,
        $model._link p
    where p.id = c.id;
;;

create or replace function ${model}.gate_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('gate', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'gate') where name = 'define_later' and id = id_;
            insert into $model._gate_link(id, link_type, z_invert, z_ceiling, width, cc, action_gate_type, mode_valve, z_gate, v_max_cms, cc_submerged)
                values (id_, 'gate', new.z_invert, new.z_ceiling, new.width, coalesce(new.cc, .6), coalesce(new.action_gate_type, 'upward_opening'), coalesce(new.mode_valve, 'no_valve'), coalesce(new.z_gate, new.z_ceiling), coalesce(new.v_max_cms, .2), coalesce(new.cc_submerged, 1));
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'gate_link');
            update $model._link set validity = (select (z_invert is not null) and (z_ceiling is not null) and (z_ceiling>z_invert) and (width is not null) and (cc is not null) and (cc <=1) and (cc >=0) and (action_gate_type is not null) and (mode_valve is not null) and (z_gate is not null) and (z_gate>=z_invert) and (z_gate<=z_ceiling) and (v_max_cms is not null) and (cc_submerged >0) from  $model._gate_link where id = id_) where id = id_;
            new.id = id_;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.mode_valve, new.z_gate, new.v_max_cms, new.cc_submerged) is distinct from (old.z_invert, old.z_ceiling, old.width, old.cc, old.action_gate_type, old.mode_valve, old.z_gate, old.v_max_cms, old.cc_submerged)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.z_ceiling, old.width, old.cc, old.action_gate_type, old.mode_valve, old.z_gate, old.v_max_cms, old.cc_submerged) as o, (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.mode_valve, new.z_gate, new.v_max_cms, new.cc_submerged) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.mode_valve, new.z_gate, new.v_max_cms, new.cc_submerged) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._gate_link set z_invert=new.z_invert, z_ceiling=new.z_ceiling, width=new.width, cc=new.cc, action_gate_type=new.action_gate_type, mode_valve=new.mode_valve, z_gate=new.z_gate, v_max_cms=new.v_max_cms, cc_submerged=new.cc_submerged where id=old.id;

            perform $model.add_configuration_fct(new.configuration::json, old.id, 'gate_link');

            update $model._link set validity = (select (z_invert is not null) and (z_ceiling is not null) and (z_ceiling>z_invert) and (width is not null) and (cc is not null) and (cc <=1) and (cc >=0) and (action_gate_type is not null) and (mode_valve is not null) and (z_gate is not null) and (z_gate>=z_invert) and (z_gate<=z_ceiling) and (v_max_cms is not null) and (cc_submerged >0) from  $model._gate_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._gate_link where id=old.id;
            delete from $model._link where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_gate_link_trig
    instead of insert or update or delete on $model.gate_link
       for each row execute procedure ${model}.gate_link_fct()
;;





drop view $model.regul_sluice_gate_singularity cascade;
;;

create or replace view $model.regul_sluice_gate_singularity as
    select p.id,
        p.name,
        n.id AS node,
        c.z_invert,
        c.z_ceiling,
        c.width,
        c.cc,
        c.action_gate_type,
        c.z_invert_stop,
        c.z_ceiling_stop,
        c.v_max_cms,
        c.dt_regul_hr,
        c.mode_regul,
        c.z_control_node,
        c.z_pid_array,
        c.z_tz_array,
        c.q_z_crit,
        c.q_tq_array,
        c.nr_z_gate,
        c.full_section_discharge_for_headloss,
        c.cc_submerged,
        n.geom,
        p.configuration::character varying AS configuration,
        p.validity,
        p.configuration AS configuration_json
    from $model._regul_sluice_gate_singularity c,
        $model._singularity p,
        $model._node n
    where p.id = c.id and n.id = p.node;
;;

create or replace function ${model}.regul_sluice_gate_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'regul_sluice_gate', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'regul_sluice_gate') where name = 'define_later' and id = id_;

            insert into $model._regul_sluice_gate_singularity(id, singularity_type, z_invert, z_ceiling, width, cc, action_gate_type, z_invert_stop, z_ceiling_stop, v_max_cms, dt_regul_hr, mode_regul, z_control_node, z_pid_array, z_tz_array, q_z_crit, q_tq_array, nr_z_gate, full_section_discharge_for_headloss, cc_submerged)
                values (id_, 'regul_sluice_gate', new.z_invert, new.z_ceiling, new.width, coalesce(new.cc, .6), coalesce(new.action_gate_type, 'upward_opening'), coalesce(new.z_invert_stop, new.z_invert), coalesce(new.z_ceiling_stop, new.z_ceiling), coalesce(new.v_max_cms, .5), coalesce(new.dt_regul_hr, 0), coalesce(new.mode_regul, 'elevation'), new.z_control_node, coalesce(new.z_pid_array, '{1, 0, 0}'::real[]), new.z_tz_array, new.q_z_crit, new.q_tq_array, coalesce(new.nr_z_gate, new.z_ceiling), coalesce(new.full_section_discharge_for_headloss, 't'), coalesce(new.cc_submerged, 1));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'regul_sluice_gate_singularity');
            update $model._singularity set validity = (select (z_invert is not null) and (z_ceiling is not null) and (width is not null) and (width >=0) and (cc is not null) and (cc <=1) and (cc >=0) and (action_gate_type is not null) and (z_invert_stop is not null) and (z_ceiling_stop is not null) and (v_max_cms is not null) and (v_max_cms>=0) and (dt_regul_hr is not null) and (mode_regul!='elevation' or z_control_node is not null) and (mode_regul!='elevation' or z_pid_array is not null) and (mode_regul!='elevation' or z_tz_array is not null) and (mode_regul!='elevation' or array_length(z_tz_array, 1)<=10) and (mode_regul!='elevation' or array_length(z_tz_array, 1)>=1) and (mode_regul!='elevation' or array_length(z_tz_array, 2)=2) and (mode_regul!='discharge' or q_z_crit is not null) and (mode_regul!='discharge' or array_length(q_tq_array, 1)<=10) and (mode_regul!='discharge' or array_length(q_tq_array, 1)>=1) and (mode_regul!='discharge' or array_length(q_tq_array, 2)=2) and (mode_regul!='no_regulation' or nr_z_gate is not null) and (cc_submerged >0) from  $model._regul_sluice_gate_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.z_invert_stop, new.z_ceiling_stop, new.v_max_cms, new.dt_regul_hr, new.mode_regul, new.z_control_node, new.z_pid_array, new.z_tz_array, new.q_z_crit, new.q_tq_array, new.nr_z_gate, new.full_section_discharge_for_headloss, new.cc_submerged) is distinct from (old.z_invert, old.z_ceiling, old.width, old.cc, old.action_gate_type, old.z_invert_stop, old.z_ceiling_stop, old.v_max_cms, old.dt_regul_hr, old.mode_regul, old.z_control_node, old.z_pid_array, old.z_tz_array, old.q_z_crit, old.q_tq_array, old.nr_z_gate, old.full_section_discharge_for_headloss, old.cc_submerged)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.z_ceiling, old.width, old.cc, old.action_gate_type, old.z_invert_stop, old.z_ceiling_stop, old.v_max_cms, old.dt_regul_hr, old.mode_regul, old.z_control_node, old.z_pid_array, old.z_tz_array, old.q_z_crit, old.q_tq_array, old.nr_z_gate, old.full_section_discharge_for_headloss, old.cc_submerged) as o, (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.z_invert_stop, new.z_ceiling_stop, new.v_max_cms, new.dt_regul_hr, new.mode_regul, new.z_control_node, new.z_pid_array, new.z_tz_array, new.q_z_crit, new.q_tq_array, new.nr_z_gate, new.full_section_discharge_for_headloss, new.cc_submerged) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.z_invert_stop, new.z_ceiling_stop, new.v_max_cms, new.dt_regul_hr, new.mode_regul, new.z_control_node, new.z_pid_array, new.z_tz_array, new.q_z_crit, new.q_tq_array, new.nr_z_gate, new.full_section_discharge_for_headloss, new.cc_submerged) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._regul_sluice_gate_singularity set z_invert=new.z_invert, z_ceiling=new.z_ceiling, width=new.width, cc=new.cc, action_gate_type=new.action_gate_type, z_invert_stop=new.z_invert_stop, z_ceiling_stop=new.z_ceiling_stop, v_max_cms=new.v_max_cms, dt_regul_hr=new.dt_regul_hr, mode_regul=new.mode_regul, z_control_node=new.z_control_node, z_pid_array=new.z_pid_array, z_tz_array=new.z_tz_array, q_z_crit=new.q_z_crit, q_tq_array=new.q_tq_array, nr_z_gate=new.nr_z_gate, full_section_discharge_for_headloss=new.full_section_discharge_for_headloss, cc_submerged=new.cc_submerged where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'regul_sluice_gate_singularity');
            update $model._singularity set validity = (select (z_invert is not null) and (z_ceiling is not null) and (width is not null) and (width >=0) and (cc is not null) and (cc <=1) and (cc >=0) and (action_gate_type is not null) and (z_invert_stop is not null) and (z_ceiling_stop is not null) and (v_max_cms is not null) and (v_max_cms>=0) and (dt_regul_hr is not null) and (mode_regul!='elevation' or z_control_node is not null) and (mode_regul!='elevation' or z_pid_array is not null) and (mode_regul!='elevation' or z_tz_array is not null) and (mode_regul!='elevation' or array_length(z_tz_array, 1)<=10) and (mode_regul!='elevation' or array_length(z_tz_array, 1)>=1) and (mode_regul!='elevation' or array_length(z_tz_array, 2)=2) and (mode_regul!='discharge' or q_z_crit is not null) and (mode_regul!='discharge' or array_length(q_tq_array, 1)<=10) and (mode_regul!='discharge' or array_length(q_tq_array, 1)>=1) and (mode_regul!='discharge' or array_length(q_tq_array, 2)=2) and (mode_regul!='no_regulation' or nr_z_gate is not null) and (cc_submerged >0) from  $model._regul_sluice_gate_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._regul_sluice_gate_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_regul_sluice_gate_singularity_trig
    instead of insert or update or delete on $model.regul_sluice_gate_singularity
       for each row execute procedure ${model}.regul_sluice_gate_singularity_fct()
;;





drop view $model.regul_gate_link cascade;
;;

create or replace view $model.regul_gate_link as
    select p.id,
        p.name,
        p.up,
        p.down,
        c.z_invert,
        c.z_ceiling,
        c.width,
        c.cc,
        c.action_gate_type,
        c.z_invert_stop,
        c.z_ceiling_stop,
        c.v_max_cms,
        c.dt_regul_hr,
        c.mode_regul,
        c.z_control_node,
        c.z_pid_array,
        c.z_tz_array,
        c.q_z_crit,
        c.q_tq_array,
        c.nr_z_gate,
        c.cc_submerged,
        p.geom,
        p.configuration::character varying AS configuration,
        p.generated,
        p.validity,
        p.up_type,
        p.down_type,
        p.configuration AS configuration_json
    from $model._regul_gate_link c,
        $model._link p
    where p.id = c.id;
;;

create or replace function ${model}.regul_gate_link_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        up_ integer;
        up_type_ hydra_node_type;
        down_ integer;
        down_type_ hydra_node_type;
        dump_ boolean;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            insert into $model._link(link_type, up, up_type, down, down_type, name, geom, configuration, generated)
                values('regul_gate', up_, up_type_, down_, down_type_, coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._link set name = (select abbreviation||id_::varchar
                from hydra.link_type where name = 'regul_gate') where name = 'define_later' and id = id_;
            insert into $model._regul_gate_link(id, link_type, z_invert, z_ceiling, width, cc, action_gate_type, z_invert_stop, z_ceiling_stop, v_max_cms, dt_regul_hr, mode_regul, z_control_node, z_pid_array, z_tz_array, q_z_crit, q_tq_array, nr_z_gate, cc_submerged)
                values (id_, 'regul_gate', new.z_invert, new.z_ceiling, new.width, coalesce(new.cc, .6), coalesce(new.action_gate_type, 'upward_opening'), coalesce(new.z_invert_stop, new.z_invert), coalesce(new.z_ceiling_stop, new.z_ceiling), coalesce(new.v_max_cms, .5), coalesce(new.dt_regul_hr, 0), coalesce(new.mode_regul, 'elevation'), new.z_control_node, coalesce(new.z_pid_array, '{1, 0, 0}'::real[]), new.z_tz_array, new.q_z_crit, new.q_tq_array, coalesce(new.nr_z_gate, new.z_ceiling), coalesce(new.cc_submerged, 1));
            --perform $model.set_link_altitude(id_);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'regul_gate_link');
            update $model._link set validity = (select (z_invert is not null) and (z_ceiling is not null) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (action_gate_type is not null) and (z_invert_stop is not null) and (z_ceiling_stop is not null) and (v_max_cms is not null) and (v_max_cms>=0) and (dt_regul_hr is not null) and (mode_regul!='elevation' or z_control_node is not null) and (mode_regul!='elevation' or z_pid_array is not null) and (mode_regul!='elevation' or z_tz_array is not null) and (mode_regul!='elevation' or array_length(z_tz_array, 1)<=10) and (mode_regul!='elevation' or array_length(z_tz_array, 1)>=1) and (mode_regul!='elevation' or array_length(z_tz_array, 2)=2) and (mode_regul!='discharge' or q_z_crit is not null) and (mode_regul!='discharge' or array_length(q_tq_array, 1)<=10) and (mode_regul!='discharge' or array_length(q_tq_array, 1)>=1) and (mode_regul!='discharge' or array_length(q_tq_array, 2)=2) and (mode_regul!='no_regulation' or nr_z_gate is not null) and (cc_submerged>0) from  $model._regul_gate_link where id = id_) where id = id_;
            new.id = id_;

            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.z_invert_stop, new.z_ceiling_stop, new.v_max_cms, new.dt_regul_hr, new.mode_regul, new.z_control_node, new.z_pid_array, new.z_tz_array, new.q_z_crit, new.q_tq_array, new.nr_z_gate, new.cc_submerged) is distinct from (old.z_invert, old.z_ceiling, old.width, old.cc, old.action_gate_type, old.z_invert_stop, old.z_ceiling_stop, old.v_max_cms, old.dt_regul_hr, old.mode_regul, old.z_control_node, old.z_pid_array, old.z_tz_array, old.q_z_crit, old.q_tq_array, old.nr_z_gate, old.cc_submerged)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.z_invert, old.z_ceiling, old.width, old.cc, old.action_gate_type, old.z_invert_stop, old.z_ceiling_stop, old.v_max_cms, old.dt_regul_hr, old.mode_regul, old.z_control_node, old.z_pid_array, old.z_tz_array, old.q_z_crit, old.q_tq_array, old.nr_z_gate, old.cc_submerged) as o, (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.z_invert_stop, new.z_ceiling_stop, new.v_max_cms, new.dt_regul_hr, new.mode_regul, new.z_control_node, new.z_pid_array, new.z_tz_array, new.q_z_crit, new.q_tq_array, new.nr_z_gate, new.cc_submerged) as n into new_config;
                        update $model._link set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.z_invert, new.z_ceiling, new.width, new.cc, new.action_gate_type, new.z_invert_stop, new.z_ceiling_stop, new.v_max_cms, new.dt_regul_hr, new.mode_regul, new.z_control_node, new.z_pid_array, new.z_tz_array, new.q_z_crit, new.q_tq_array, new.nr_z_gate, new.cc_submerged) n into new_config;
                        update $model._link set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            select * from $model.find_link_fct(new.geom) into up_, up_type_, down_, down_type_;
            update $model._link set up=up_, up_type=up_type_, down=down_, down_type=down_type_, name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._regul_gate_link set z_invert=new.z_invert, z_ceiling=new.z_ceiling, width=new.width, cc=new.cc, action_gate_type=new.action_gate_type, z_invert_stop=new.z_invert_stop, z_ceiling_stop=new.z_ceiling_stop, v_max_cms=new.v_max_cms, dt_regul_hr=new.dt_regul_hr, mode_regul=new.mode_regul, z_control_node=new.z_control_node, z_pid_array=new.z_pid_array, z_tz_array=new.z_tz_array, q_z_crit=new.q_z_crit, q_tq_array=new.q_tq_array, nr_z_gate=new.nr_z_gate, cc_submerged=new.cc_submerged where id=old.id;

            update $model._link set validity = (select (z_invert is not null) and (z_ceiling is not null) and (width is not null) and (width>=0) and (cc is not null) and (cc<=1) and (cc>=0) and (action_gate_type is not null) and (z_invert_stop is not null) and (z_ceiling_stop is not null) and (v_max_cms is not null) and (v_max_cms>=0) and (dt_regul_hr is not null) and (mode_regul!='elevation' or z_control_node is not null) and (mode_regul!='elevation' or z_pid_array is not null) and (mode_regul!='elevation' or z_tz_array is not null) and (mode_regul!='elevation' or array_length(z_tz_array, 1)<=10) and (mode_regul!='elevation' or array_length(z_tz_array, 1)>=1) and (mode_regul!='elevation' or array_length(z_tz_array, 2)=2) and (mode_regul!='discharge' or q_z_crit is not null) and (mode_regul!='discharge' or array_length(q_tq_array, 1)<=10) and (mode_regul!='discharge' or array_length(q_tq_array, 1)>=1) and (mode_regul!='discharge' or array_length(q_tq_array, 2)=2) and (mode_regul!='no_regulation' or nr_z_gate is not null) and (cc_submerged>0) from  $model._regul_gate_link where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._regul_gate_link where id=old.id;
            delete from $model._link where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_regul_gate_link_trig
    instead of insert or update or delete on $model.regul_gate_link
       for each row execute procedure ${model}.regul_gate_link_fct()
;;

/* ********************************************** */
/*  Catchment evolution                           */
/* ********************************************** */

drop view if exists $model.catchment_node cascade;
;;

update $model._catchment_node set netflow_type='hydra' where netflow_type='permeable_soil';
;;

alter table $model._catchment_node add column hydra_surface_soil_storage_rfu_mm real;
alter table $model._catchment_node add column hydra_inf_rate_f0_mm_day real;
alter table $model._catchment_node add column hydra_int_soil_storage_j_mm real;
alter table $model._catchment_node add column hydra_soil_drainage_time_qres_day real;
;;

alter table $model._catchment_node add column gr4_k1 real;
alter table $model._catchment_node add column gr4_k2 real;
alter table $model._catchment_node add column gr4_k3 real;
alter table $model._catchment_node add column gr4_k4 real;
;;

create or replace view $model.catchment_node as
    select p.id,
        p.name,
        c.area_ha,
        c.rl,
        c.slope,
        c.c_imp,
        c.netflow_type,
        c.constant_runoff,
        c.horner_ini_loss_coef,
        c.horner_recharge_coef,
        c.holtan_sat_inf_rate_mmh,
        c.holtan_dry_inf_rate_mmh,
        c.holtan_soil_storage_cap_mm,
        c.scs_j_mm,
        c.scs_soil_drainage_time_day,
        c.scs_rfu_mm,
        c.permeable_soil_j_mm,
        c.permeable_soil_rfu_mm,
        c.permeable_soil_ground_max_inf_rate,
        c.permeable_soil_ground_lag_time_day,
        c.permeable_soil_coef_river_exchange,
        c.hydra_surface_soil_storage_rfu_mm,
        c.hydra_inf_rate_f0_mm_day,
        c.hydra_int_soil_storage_j_mm,
        c.hydra_soil_drainage_time_qres_day,
        c.gr4_k1,
        c.gr4_k2,
        c.gr4_k3,
        c.gr4_k4,
        c.runoff_type,
        c.socose_tc_mn,
        c.socose_shape_param_beta,
        c.define_k_mn,
        c.q_limit,
        c.q0,
        c.contour,
        c.network_type,
        c.rural_land_use,
        c.industrial_land_use,
        c.suburban_housing_land_use,
        c.dense_housing_land_use,
        p.geom,
        p.configuration::character varying AS configuration,
        p.generated,
        p.validity,
        p.configuration AS configuration_json
    from $model._catchment_node c,
        $model._node p
    where p.id = c.id
;;

/* note: id and node_type are not updatable */
create or replace function ${model}.catchment_node_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            insert into $model._node(node_type, name, geom, configuration, generated)
                values('catchment', coalesce(new.name, 'define_later'), new.geom, new.configuration::json, new.generated) returning id into id_;
            update $model._node set name = (select abbreviation||id_::varchar
                from hydra.node_type where name = 'catchment') where name = 'define_later' and id = id_;
            insert into $model._catchment_node(id, node_type, area_ha, rl, slope, c_imp, netflow_type, constant_runoff, horner_ini_loss_coef, horner_recharge_coef, holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm, scs_j_mm, scs_soil_drainage_time_day, scs_rfu_mm, permeable_soil_j_mm, permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange, hydra_surface_soil_storage_rfu_mm, hydra_inf_rate_f0_mm_day, hydra_int_soil_storage_j_mm, hydra_soil_drainage_time_qres_day, gr4_k1, gr4_k2, gr4_k3, gr4_k4, runoff_type, socose_tc_mn, socose_shape_param_beta, define_k_mn, q_limit, q0, contour, network_type, rural_land_use, industrial_land_use, suburban_housing_land_use, dense_housing_land_use)
                values (id_, 'catchment', new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.hydra_surface_soil_storage_rfu_mm, new.hydra_inf_rate_f0_mm_day, new.hydra_int_soil_storage_j_mm, new.hydra_soil_drainage_time_qres_day, new.gr4_k1, new.gr4_k2, new.gr4_k3, new.gr4_k4, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, coalesce(new.q_limit, 9999), coalesce(new.q0, 0), coalesce(new.contour, (select id from $model.catchment where st_intersects(geom, new.geom))), coalesce(new.network_type, 'separative'), new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use);
            perform $model.add_configuration_fct(new.configuration::json, id_, 'catchment_node');

            update $model._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_type!='constant_runoff' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)) and (netflow_type!='horner' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)) and (netflow_type!='horner' or (horner_recharge_coef is not null and horner_recharge_coef>=0)) and (netflow_type!='holtan' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)) and (netflow_type!='scs' or (scs_j_mm is not null and scs_j_mm>=0)) and (netflow_type!='scs' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)) and (netflow_type!='scs' or (scs_rfu_mm is not null and scs_rfu_mm>=0)) and (netflow_type!='hydra' or (hydra_surface_soil_storage_rfu_mm is not null and hydra_surface_soil_storage_rfu_mm>=0)) and (netflow_type!='hydra' or (hydra_inf_rate_f0_mm_day is not null and hydra_inf_rate_f0_mm_day>=0)) and (netflow_type!='hydra' or (hydra_int_soil_storage_j_mm is not null and hydra_int_soil_storage_j_mm>=0)) and (netflow_type!='hydra' or (hydra_soil_drainage_time_qres_day is not null and hydra_soil_drainage_time_qres_day>=0)) and (netflow_type!='gr4' or (gr4_k1 is not null and gr4_k1>=0)) and (netflow_type!='gr4' or (gr4_k2 is not null and gr4_k2>=0)) and (netflow_type!='gr4' or (gr4_k3 is not null and gr4_k3>=0)) and (netflow_type!='gr4' or (gr4_k4 is not null and gr4_k4>=0)) and (runoff_type is not null or netflow_type='gr4') and (runoff_type!='socose' or (socose_tc_mn is not null and socose_tc_mn>0) or netflow_type='gr4') and (runoff_type!='socose' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6) or netflow_type='gr4') and (runoff_type!='define_k' or (define_k_mn is not null and define_k_mn>0) or netflow_type='gr4') and (q_limit is not null) and (q0 is not null) and (network_type is not null) and (rural_land_use is null or rural_land_use>=0) and (industrial_land_use is null or industrial_land_use>=0) and (suburban_housing_land_use is null or suburban_housing_land_use>=0) and (dense_housing_land_use is null or dense_housing_land_use>=0) from  $model._catchment_node where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.hydra_surface_soil_storage_rfu_mm, new.hydra_inf_rate_f0_mm_day, new.hydra_int_soil_storage_j_mm, new.hydra_soil_drainage_time_qres_day, new.gr4_k1, new.gr4_k2, new.gr4_k3, new.gr4_k4, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.network_type, new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use) is distinct from (old.area_ha, old.rl, old.slope, old.c_imp, old.netflow_type, old.constant_runoff, old.horner_ini_loss_coef, old.horner_recharge_coef, old.holtan_sat_inf_rate_mmh, old.holtan_dry_inf_rate_mmh, old.holtan_soil_storage_cap_mm, old.scs_j_mm, old.scs_soil_drainage_time_day, old.scs_rfu_mm, old.permeable_soil_j_mm, old.permeable_soil_rfu_mm, old.permeable_soil_ground_max_inf_rate, old.permeable_soil_ground_lag_time_day, old.permeable_soil_coef_river_exchange, old.hydra_surface_soil_storage_rfu_mm, old.hydra_inf_rate_f0_mm_day, old.hydra_int_soil_storage_j_mm, old.hydra_soil_drainage_time_qres_day, old.gr4_k1, old.gr4_k2, old.gr4_k3, old.gr4_k4, old.runoff_type, old.socose_tc_mn, old.socose_shape_param_beta, old.define_k_mn, old.q_limit, old.q0, old.contour, old.network_type, old.rural_land_use, old.industrial_land_use, old.suburban_housing_land_use, old.dense_housing_land_use)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.area_ha, old.rl, old.slope, old.c_imp, old.netflow_type, old.constant_runoff, old.horner_ini_loss_coef, old.horner_recharge_coef, old.holtan_sat_inf_rate_mmh, old.holtan_dry_inf_rate_mmh, old.holtan_soil_storage_cap_mm, old.scs_j_mm, old.scs_soil_drainage_time_day, old.scs_rfu_mm, old.permeable_soil_j_mm, old.permeable_soil_rfu_mm, old.permeable_soil_ground_max_inf_rate, old.permeable_soil_ground_lag_time_day, old.permeable_soil_coef_river_exchange, old.hydra_surface_soil_storage_rfu_mm, old.hydra_inf_rate_f0_mm_day, old.hydra_int_soil_storage_j_mm, old.hydra_soil_drainage_time_qres_day, old.gr4_k1, old.gr4_k2, old.gr4_k3, old.gr4_k4, old.runoff_type, old.socose_tc_mn, old.socose_shape_param_beta, old.define_k_mn, old.q_limit, old.q0, old.contour, old.network_type, old.rural_land_use, old.industrial_land_use, old.suburban_housing_land_use, old.dense_housing_land_use) as o, (select new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.hydra_surface_soil_storage_rfu_mm, new.hydra_inf_rate_f0_mm_day, new.hydra_int_soil_storage_j_mm, new.hydra_soil_drainage_time_qres_day, new.gr4_k1, new.gr4_k2, new.gr4_k3, new.gr4_k4, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.network_type, new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use) as n into new_config;
                        update $model._node set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.area_ha, new.rl, new.slope, new.c_imp, new.netflow_type, new.constant_runoff, new.horner_ini_loss_coef, new.horner_recharge_coef, new.holtan_sat_inf_rate_mmh, new.holtan_dry_inf_rate_mmh, new.holtan_soil_storage_cap_mm, new.scs_j_mm, new.scs_soil_drainage_time_day, new.scs_rfu_mm, new.permeable_soil_j_mm, new.permeable_soil_rfu_mm, new.permeable_soil_ground_max_inf_rate, new.permeable_soil_ground_lag_time_day, new.permeable_soil_coef_river_exchange, new.hydra_surface_soil_storage_rfu_mm, new.hydra_inf_rate_f0_mm_day, new.hydra_int_soil_storage_j_mm, new.hydra_soil_drainage_time_qres_day, new.gr4_k1, new.gr4_k2, new.gr4_k3, new.gr4_k4, new.runoff_type, new.socose_tc_mn, new.socose_shape_param_beta, new.define_k_mn, new.q_limit, new.q0, new.contour, new.network_type, new.rural_land_use, new.industrial_land_use, new.suburban_housing_land_use, new.dense_housing_land_use) n into new_config;
                        update $model._node set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;

            update $model._node set name=new.name, geom=new.geom, generated=new.generated where id=old.id;
            update $model._catchment_node set area_ha=new.area_ha, rl=new.rl, slope=new.slope, c_imp=new.c_imp, netflow_type=new.netflow_type, constant_runoff=new.constant_runoff, horner_ini_loss_coef=new.horner_ini_loss_coef, horner_recharge_coef=new.horner_recharge_coef, holtan_sat_inf_rate_mmh=new.holtan_sat_inf_rate_mmh, holtan_dry_inf_rate_mmh=new.holtan_dry_inf_rate_mmh, holtan_soil_storage_cap_mm=new.holtan_soil_storage_cap_mm, scs_j_mm=new.scs_j_mm, scs_soil_drainage_time_day=new.scs_soil_drainage_time_day, scs_rfu_mm=new.scs_rfu_mm, permeable_soil_j_mm=new.permeable_soil_j_mm, permeable_soil_rfu_mm=new.permeable_soil_rfu_mm, permeable_soil_ground_max_inf_rate=new.permeable_soil_ground_max_inf_rate, permeable_soil_ground_lag_time_day=new.permeable_soil_ground_lag_time_day, permeable_soil_coef_river_exchange=new.permeable_soil_coef_river_exchange, hydra_surface_soil_storage_rfu_mm=new.hydra_surface_soil_storage_rfu_mm, hydra_inf_rate_f0_mm_day=new.hydra_inf_rate_f0_mm_day, hydra_int_soil_storage_j_mm=new.hydra_int_soil_storage_j_mm, hydra_soil_drainage_time_qres_day=new.hydra_soil_drainage_time_qres_day, gr4_k1=new.gr4_k1, gr4_k2=new.gr4_k2, gr4_k3=new.gr4_k3, gr4_k4=new.gr4_k4, runoff_type=new.runoff_type, socose_tc_mn=new.socose_tc_mn, socose_shape_param_beta=new.socose_shape_param_beta, define_k_mn=new.define_k_mn, q_limit=new.q_limit, q0=new.q0, contour=new.contour, network_type=new.network_type, rural_land_use=new.rural_land_use, industrial_land_use=new.industrial_land_use, suburban_housing_land_use=new.suburban_housing_land_use, dense_housing_land_use=new.dense_housing_land_use where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'catchment_node');

            -- Lines to update specific nodes that works with associated contours
            if 'catchment' = 'catchment' then
                update $model._catchment_node set contour=(select id from $model.catchment as c where ST_intersects(new.geom, c.geom)) where id=old.id;
            end if;
            update $model._node set validity = (select (area_ha is not null) and (area_ha>0) and (rl is not null) and (rl>0) and (slope is not null) and (c_imp is not null) and (c_imp>=0) and (c_imp<=1) and (netflow_type is not null) and (netflow_type!='constant_runoff' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)) and (netflow_type!='horner' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)) and (netflow_type!='horner' or (horner_recharge_coef is not null and horner_recharge_coef>=0)) and (netflow_type!='holtan' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)) and (netflow_type!='holtan' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)) and (netflow_type!='scs' or (scs_j_mm is not null and scs_j_mm>=0)) and (netflow_type!='scs' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)) and (netflow_type!='scs' or (scs_rfu_mm is not null and scs_rfu_mm>=0)) and (netflow_type!='hydra' or (hydra_surface_soil_storage_rfu_mm is not null and hydra_surface_soil_storage_rfu_mm>=0)) and (netflow_type!='hydra' or (hydra_inf_rate_f0_mm_day is not null and hydra_inf_rate_f0_mm_day>=0)) and (netflow_type!='hydra' or (hydra_int_soil_storage_j_mm is not null and hydra_int_soil_storage_j_mm>=0)) and (netflow_type!='hydra' or (hydra_soil_drainage_time_qres_day is not null and hydra_soil_drainage_time_qres_day>=0)) and (netflow_type!='gr4' or (gr4_k1 is not null and gr4_k1>=0)) and (netflow_type!='gr4' or (gr4_k2 is not null and gr4_k2>=0)) and (netflow_type!='gr4' or (gr4_k3 is not null and gr4_k3>=0)) and (netflow_type!='gr4' or (gr4_k4 is not null and gr4_k4>=0)) and (runoff_type is not null or netflow_type='gr4') and (runoff_type!='socose' or (socose_tc_mn is not null and socose_tc_mn>0) or netflow_type='gr4') and (runoff_type!='socose' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6) or netflow_type='gr4') and (runoff_type!='define_k' or (define_k_mn is not null and define_k_mn>0) or netflow_type='gr4') and (q_limit is not null) and (q0 is not null) and (network_type is not null) and (rural_land_use is null or rural_land_use>=0) and (industrial_land_use is null or industrial_land_use>=0) and (suburban_housing_land_use is null or suburban_housing_land_use>=0) and (dense_housing_land_use is null or dense_housing_land_use>=0) from  $model._catchment_node where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then

            delete from $model._catchment_node where id=old.id;
            delete from $model._node where id=old.id;
            return old;
        end if;

    end;
$$$$
;;

create trigger ${model}_catchment_node_trig
    instead of insert or update or delete on $model.catchment_node
       for each row execute procedure ${model}.catchment_node_fct()
;;

/* ********************************************** */
/*  Quality on model_connect                      */
/* ********************************************** */

drop view if exists $model.model_connect_bc_singularity cascade;
;;

alter table $model._model_connect_bc_singularity add column quality real[];
update $model._model_connect_bc_singularity set quality = '{0, 0, 0, 0, 0, 0, 0, 0, 0}'::real[];
;;

create or replace view $model.model_connect_bc_singularity as
    select p.id,
        p.name,
        n.id as node,
        c.cascade_mode,
        c.zq_array,
        c.quality,
        n.geom,
        p.configuration::character varying as configuration,
        p.validity,
        p.configuration as configuration_json
    from $model._model_connect_bc_singularity c,
        $model._singularity p,
        $model._node n
    where p.id = c.id and n.id = p.node;
;;

create or replace function ${model}.model_connect_bc_singularity_fct()
returns trigger
language plpgsql
as $$$$
    declare
        id_ integer;
        nid_ integer;
        node_type_ hydra_node_type;
        point_ geometry('POINTZ',$srid);
        config_name varchar;
        new_config varchar;
        switching boolean;
    begin
        if tg_op = 'INSERT' then
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            insert into $model._singularity(node, node_type, singularity_type, name, configuration)
                values (nid_, node_type_, 'model_connect_bc', coalesce(new.name, 'define_later'), new.configuration::json) returning id into id_;
            update $model._singularity set name = (select abbreviation||id_::varchar
                from hydra.singularity_type where name = 'model_connect_bc') where name = 'define_later' and id = id_;

            insert into $model._model_connect_bc_singularity(id, singularity_type, cascade_mode, zq_array, quality)
                values (id_, 'model_connect_bc', coalesce(new.cascade_mode, 'hydrograph'), new.zq_array, coalesce(new.quality, '{0, 0, 0, 0, 0, 0, 0, 0, 0}'::real[]));
            perform $model.add_configuration_fct(new.configuration::json, id_, 'model_connect_bc_singularity');
            update $model._singularity set validity = (select (cascade_mode is not null) and (cascade_mode='hydrograph' or zq_array is not null) and (array_length(zq_array, 1)<=100) and (array_length(zq_array, 1)>=1) and (array_length(zq_array, 2)=2) and (array_length(quality, 1)=9) from  $model._model_connect_bc_singularity where id = id_) where id = id_;
            new.id = id_;
            return new;
        elsif tg_op = 'UPDATE' then
            -- Handle configurations
            if ((new.cascade_mode, new.zq_array, new.quality) is distinct from (old.cascade_mode, old.zq_array, old.quality)) then
                select is_switching from $model.config_switch into switching;
                if switching=false then
                    select name from $model.configuration as c, $model.metadata as m where c.id = m.configuration into config_name;
                    if old.configuration is null and config_name!='default' then
                        select '{"default":' || row_to_json(o) ||', "' || config_name || '":' || row_to_json(n) ||'}' from (select old.cascade_mode, old.zq_array, old.quality) as o, (select new.cascade_mode, new.zq_array, new.quality) as n into new_config;
                        update $model._singularity set configuration=new_config::json where id=old.id;
                    elsif old.configuration is not null then
                        select '{ "' || config_name || '":' || row_to_json(n) ||'}' from (select new.cascade_mode, new.zq_array, new.quality) n into new_config;
                        update $model._singularity set configuration=(configuration::jsonb|| new_config::jsonb)::json where id=old.id;
                    end if;
                end if;
            end if;
            /* find the node if not specified */
            select id, node_type, geom from $model._node where ST_DWithin(new.geom, geom, .1) order by ST_Distance(new.geom, geom) limit 1 into nid_, node_type_, point_;
            if nid_ is null then
                raise exception 'singularity % not on node % (distance %)', ST_AsText(new.geom), (select name from $model._node ORDER BY geom <-> new.geom limit 1), (select ST_Distance(new.geom, geom) from $model._node ORDER BY geom <-> new.geom limit 1);
            end if;
            update $model._singularity set node=nid_, node_type=node_type_, name=new.name where id=old.id;
            update $model._model_connect_bc_singularity set cascade_mode=new.cascade_mode, zq_array=new.zq_array, quality=new.quality where id=old.id;
            perform $model.add_configuration_fct(new.configuration::json, old.id, 'model_connect_bc_singularity');
            update $model._singularity set validity = (select (cascade_mode is not null) and (cascade_mode='hydrograph' or zq_array is not null) and (array_length(zq_array, 1)<=100) and (array_length(zq_array, 1)>=1) and (array_length(zq_array, 2)=2) and (array_length(quality, 1)=9) from  $model._model_connect_bc_singularity where id = old.id) where id = old.id;
            return new;
        elsif tg_op = 'DELETE' then
            delete from $model._model_connect_bc_singularity where id=old.id;
            delete from $model._singularity where id=old.id;
            return old;
        end if;
    end;
$$$$
;;

create trigger ${model}_model_connect_bc_singularity_trig
    instead of insert or update or delete on $model.model_connect_bc_singularity
       for each row execute procedure ${model}.model_connect_bc_singularity_fct();
;;


/* ********************************************** */
/*  Dropped items                                 */
/* ********************************************** */

-- Catchments
create or replace view ${model}.results_catchment as
    select id, UPPER(name)::varchar(24) as name, geom from ${model}.catchment_node
;;


-- Links
create or replace view ${model}.results_link as
    select id, name, geom from ${model}.gate_link
        union
    select id, name, geom from ${model}.regul_gate_link
        union
    select id, name, geom from ${model}.weir_link
        union
    select id, name, geom from ${model}.borda_headloss_link
        union
    select id, name, geom from ${model}.pump_link
        union
    select id, name, geom from ${model}.deriv_pump_link
        union
    select id, name, geom from ${model}.fuse_spillway_link
        union
    select id, name, geom from ${model}.connector_link
        union
    select id, name, geom from ${model}.strickler_link
        union
    select id, name, geom from ${model}.porous_link
        union
    select id, name, geom from ${model}.overflow_link
        union
    select id, name, geom from ${model}.network_overflow_link
        union
    select id, name, geom from ${model}.mesh_2d_link
        union
    select id, name, geom from ${model}.street_link
;;

create or replace view $model.invalid as
 WITH node_invalidity_reason AS (
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   (area > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h IS NOT NULL THEN ''::text
                    ELSE '   h is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h >= 0::double precision THEN ''::text
                    ELSE '   h>=0   '::text
                END AS reason
           FROM ${model}.crossroad_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END ||
                CASE
                    WHEN new_1.zini IS NOT NULL THEN ''::text
                    ELSE '   zini is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10    '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1) >=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END AS reason
           FROM ${model}.storage_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((
                CASE
                    WHEN new_1.connection_law IS NOT NULL THEN ''::text
                    ELSE '   connection_law is not null   '::text
                END ||
                CASE
                    WHEN new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type OR new_1.cover_diameter IS NOT NULL THEN ''::text
                    ELSE '   connection_law != ''manhole_cover'' or cover_diameter is not null   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type OR new_1.cover_diameter > 0::double precision THEN ''::text
                    ELSE '   connection_law != ''manhole_cover'' or cover_diameter > 0   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type OR new_1.cover_critical_pressure IS NOT NULL THEN ''::text
                    ELSE '   connection_law != ''manhole_cover'' or cover_critical_pressure is not null   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'manhole_cover'::hydra_manhole_connection_type OR new_1.cover_critical_pressure > 0::double precision THEN ''::text
                    ELSE '   connection_law != ''manhole_cover'' or cover_critical_pressure >0   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type OR new_1.inlet_width IS NOT NULL THEN ''::text
                    ELSE '   connection_law != ''drainage_inlet'' or inlet_width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type OR new_1.inlet_width > 0::double precision THEN ''::text
                    ELSE '   connection_law != ''drainage_inlet'' or inlet_width >0   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type OR new_1.inlet_height IS NOT NULL THEN ''::text
                    ELSE '   connection_law != ''drainage_inlet'' or inlet_height is not null   '::text
                END) ||
                CASE
                    WHEN new_1.connection_law <> 'drainage_inlet'::hydra_manhole_connection_type OR new_1.inlet_height > 0::double precision THEN ''::text
                    ELSE '   connection_law != ''drainage_inlet'' or inlet_height >0   '::text
                END) ||
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END) ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END) ||
                CASE
                    WHEN ( SELECT NOT (EXISTS ( SELECT 1
                               FROM ${model}.station
                              WHERE st_intersects(station.geom, new_1.geom)))) THEN ''::text
                    ELSE '   (select not exists(select 1 from ${model}.station where st_intersects(geom, new.geom)))   '::text
                END) ||
                CASE
                    WHEN ( SELECT (EXISTS ( SELECT 1
                               FROM ${model}.pipe_link
                              WHERE pipe_link.up = new_1.id OR pipe_link.down = new_1.id)) AS "exists") THEN ''::text
                    ELSE '   (select exists (select 1 from ${model}.pipe_link where up=new.id or down=new.id))   '::text
                END AS reason
           FROM ${model}.manhole_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END ||
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END) ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.reach IS NOT NULL THEN ''::text
                    ELSE '   reach is not null   '::text
                END) ||
                CASE
                    WHEN ( SELECT NOT (EXISTS ( SELECT 1
                               FROM ${model}.station
                              WHERE st_intersects(station.geom, new_1.geom)))) THEN ''::text
                    ELSE '   (select not exists(select 1 from ${model}.station where st_intersects(geom, new.geom)))   '::text
                END AS reason
           FROM ${model}.river_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((((((((((((((((((((((((
                CASE
                    WHEN new_1.area_ha IS NOT NULL THEN ''::text
                    ELSE '   area_ha is not null   '::text
                END ||
                CASE
                    WHEN new_1.area_ha > 0::double precision THEN ''::text
                    ELSE '   area_ha>0   '::text
                END) ||
                CASE
                    WHEN new_1.rl IS NOT NULL THEN ''::text
                    ELSE '   rl is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rl > 0::double precision THEN ''::text
                    ELSE '   rl>0   '::text
                END) ||
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp IS NOT NULL THEN ''::text
                    ELSE '   c_imp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp >= 0::double precision THEN ''::text
                    ELSE '   c_imp>=0   '::text
                END) ||
                CASE
                    WHEN new_1.c_imp <= 1::double precision THEN ''::text
                    ELSE '   c_imp<=1   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type IS NOT NULL THEN ''::text
                    ELSE '   netflow_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'constant_runoff'::hydra_netflow_type OR new_1.constant_runoff IS NOT NULL AND new_1.constant_runoff >= 0::double precision AND new_1.constant_runoff <= 1::double precision THEN ''::text
                    ELSE '   netflow_type!=''constant_runoff'' or (constant_runoff is not null and constant_runoff>=0 and constant_runoff<=1)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'horner'::hydra_netflow_type OR new_1.horner_ini_loss_coef IS NOT NULL AND new_1.horner_ini_loss_coef >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''horner'' or (horner_ini_loss_coef is not null and horner_ini_loss_coef>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'horner'::hydra_netflow_type OR new_1.horner_recharge_coef IS NOT NULL AND new_1.horner_recharge_coef >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''horner'' or (horner_recharge_coef is not null and horner_recharge_coef>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'holtan'::hydra_netflow_type OR new_1.holtan_sat_inf_rate_mmh IS NOT NULL AND new_1.holtan_sat_inf_rate_mmh >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''holtan'' or (holtan_sat_inf_rate_mmh is not null and holtan_sat_inf_rate_mmh>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'holtan'::hydra_netflow_type OR new_1.holtan_dry_inf_rate_mmh IS NOT NULL AND new_1.holtan_dry_inf_rate_mmh >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''holtan'' or (holtan_dry_inf_rate_mmh is not null and holtan_dry_inf_rate_mmh>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'holtan'::hydra_netflow_type OR new_1.holtan_soil_storage_cap_mm IS NOT NULL AND new_1.holtan_soil_storage_cap_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''holtan'' or (holtan_soil_storage_cap_mm is not null and holtan_soil_storage_cap_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'scs'::hydra_netflow_type OR new_1.scs_j_mm IS NOT NULL AND new_1.scs_j_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''scs'' or (scs_j_mm is not null and scs_j_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'scs'::hydra_netflow_type OR new_1.scs_soil_drainage_time_day IS NOT NULL AND new_1.scs_soil_drainage_time_day >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''scs'' or (scs_soil_drainage_time_day is not null and scs_soil_drainage_time_day>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'scs'::hydra_netflow_type OR new_1.scs_rfu_mm IS NOT NULL AND new_1.scs_rfu_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''scs'' or (scs_rfu_mm is not null and scs_rfu_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'hydra'::hydra_netflow_type OR new_1.hydra_surface_soil_storage_rfu_mm IS NOT NULL AND new_1.hydra_surface_soil_storage_rfu_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''hydra'' or (hydra_surface_soil_storage_rfu_mm is not null and hydra_surface_soil_storage_rfu_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'hydra'::hydra_netflow_type OR new_1.hydra_inf_rate_f0_mm_day IS NOT NULL AND new_1.hydra_inf_rate_f0_mm_day >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''hydra'' or (hydra_inf_rate_f0_mm_day is not null and hydra_inf_rate_f0_mm_day>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'hydra'::hydra_netflow_type OR new_1.hydra_int_soil_storage_j_mm IS NOT NULL AND new_1.hydra_int_soil_storage_j_mm >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''hydra'' or (hydra_int_soil_storage_j_mm is not null and hydra_int_soil_storage_j_mm>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'hydra'::hydra_netflow_type OR new_1.hydra_soil_drainage_time_qres_day IS NOT NULL AND new_1.hydra_soil_drainage_time_qres_day >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''hydra'' or (hydra_soil_drainage_time_qres_day is not null and hydra_soil_drainage_time_qres_day>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'gr4'::hydra_netflow_type OR new_1.gr4_k1 IS NOT NULL AND new_1.gr4_k1 >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''gr4'' or (gr4_k1 is not null and gr4_k1>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'gr4'::hydra_netflow_type OR new_1.gr4_k2 IS NOT NULL AND new_1.gr4_k2 >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''gr4'' or (gr4_k2 is not null and gr4_k2>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'gr4'::hydra_netflow_type OR new_1.gr4_k3 IS NOT NULL AND new_1.gr4_k3 >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''gr4'' or (gr4_k3 is not null and gr4_k3>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.netflow_type <> 'gr4'::hydra_netflow_type OR new_1.gr4_k4 IS NOT NULL AND new_1.gr4_k4 >= 0::double precision THEN ''::text
                    ELSE '   netflow_type!=''gr4'' or (gr4_k4 is not null and gr4_k4>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type IS NOT NULL OR new_1.netflow_type = 'gr4'::hydra_netflow_type THEN ''::text
                    ELSE '   runoff_type is not null or netflow_type=''gr4''   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type <> 'socose'::hydra_runoff_type OR new_1.socose_tc_mn IS NOT NULL AND new_1.socose_tc_mn > 0::double precision OR new_1.netflow_type = 'gr4'::hydra_netflow_type THEN ''::text
                    ELSE '   runoff_type!=''socose'' or (socose_tc_mn is not null and socose_tc_mn>0) or netflow_type=''gr4''   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type <> 'socose'::hydra_runoff_type OR new_1.socose_shape_param_beta IS NOT NULL AND new_1.socose_shape_param_beta >= 1::double precision AND new_1.socose_shape_param_beta <= 6::double precision OR new_1.netflow_type = 'gr4'::hydra_netflow_type THEN ''::text
                    ELSE '   runoff_type!=''socose'' or (socose_shape_param_beta is not null and socose_shape_param_beta>=1 and socose_shape_param_beta<=6) or netflow_type=''gr4''   '::text
                END) ||
                CASE
                    WHEN new_1.runoff_type <> 'define_k'::hydra_runoff_type OR new_1.define_k_mn IS NOT NULL AND new_1.define_k_mn > 0::double precision OR new_1.netflow_type = 'gr4'::hydra_netflow_type THEN ''::text
                    ELSE '   runoff_type!=''define_k'' or (define_k_mn is not null and define_k_mn>0) or netflow_type=''gr4''   '::text
                END) ||
                CASE
                    WHEN new_1.q_limit IS NOT NULL THEN ''::text
                    ELSE '   q_limit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q0 IS NOT NULL THEN ''::text
                    ELSE '   q0 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.network_type IS NOT NULL THEN ''::text
                    ELSE '   network_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rural_land_use IS NULL OR new_1.rural_land_use >= 0::double precision THEN ''::text
                    ELSE '   rural_land_use is null or rural_land_use>=0   '::text
                END) ||
                CASE
                    WHEN new_1.industrial_land_use IS NULL OR new_1.industrial_land_use >= 0::double precision THEN ''::text
                    ELSE '   industrial_land_use is null or industrial_land_use>=0   '::text
                END) ||
                CASE
                    WHEN new_1.suburban_housing_land_use IS NULL OR new_1.suburban_housing_land_use >= 0::double precision THEN ''::text
                    ELSE '   suburban_housing_land_use is null or suburban_housing_land_use>=0   '::text
                END) ||
                CASE
                    WHEN new_1.dense_housing_land_use IS NULL OR new_1.dense_housing_land_use >= 0::double precision THEN ''::text
                    ELSE '   dense_housing_land_use is null or dense_housing_land_use>=0   '::text
                END AS reason
           FROM ${model}.catchment_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END) ||
                CASE
                    WHEN new_1.station IS NOT NULL THEN ''::text
                    ELSE '   station is not null    '::text
                END) ||
                CASE
                    WHEN ( SELECT st_intersects(new_1.geom, station.geom) AS st_intersects
                       FROM ${model}.station
                      WHERE station.id = new_1.station) THEN ''::text
                    ELSE '   (select st_intersects(new.geom, geom) from ${model}.station where id=new.station)   '::text
                END AS reason
           FROM ${model}.station_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   area>0   '::text
                END) ||
                CASE
                    WHEN new_1.zb IS NOT NULL THEN ''::text
                    ELSE '   zb is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk > 0::double precision THEN ''::text
                    ELSE '   rk>0   '::text
                END AS reason
           FROM ${model}.elem_2d_node new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END ||
                CASE
                    WHEN new_1.area > 0::double precision THEN ''::text
                    ELSE '   (area > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.z_ground IS NOT NULL THEN ''::text
                    ELSE '   z_ground is not null   '::text
                END AS reason
           FROM ${model}.manhole_hydrology_node new_1
          WHERE NOT new_1.validity
        ), link_invalidity_reason AS (
         SELECT new_1.id,
            (((((((((((((((((((((((((((
                CASE
                    WHEN new_1.z_crest1 IS NOT NULL THEN ''::text
                    ELSE '   z_crest1 is not null   '::text
                END ||
                CASE
                    WHEN new_1.width1 IS NOT NULL THEN ''::text
                    ELSE '   width1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width1 >= 0::double precision THEN ''::text
                    ELSE '   width1>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 IS NOT NULL THEN ''::text
                    ELSE '   z_crest2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 >= new_1.z_crest1 THEN ''::text
                    ELSE '   z_crest2>=z_crest1   '::text
                END) ||
                CASE
                    WHEN new_1.width2 IS NOT NULL THEN ''::text
                    ELSE '   width2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width2 >= 0::double precision THEN ''::text
                    ELSE '   width2>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef IS NOT NULL THEN ''::text
                    ELSE '   lateral_contraction_coef is not null   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef <= 1::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef<=1   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef >= 0::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef>=0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode IS NOT NULL THEN ''::text
                    ELSE '   break_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode OR new_1.z_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''zw_critical'' or z_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode OR new_1.t_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''time_critical'' or t_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.width_breach IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or width_breach is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or z_invert is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or grp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp > 0 THEN ''::text
                    ELSE '   break_mode=''none'' or grp>0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp < 100 THEN ''::text
                    ELSE '   break_mode=''none'' or grp<100   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.dt_fracw_array IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or dt_fracw_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) <= 10 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) >= 1 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 2) = 2 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM ${model}.overflow_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((
                CASE
                    WHEN new_1.z_overflow IS NOT NULL THEN ''::text
                    ELSE '   z_overflow is not null   '::text
                END ||
                CASE
                    WHEN new_1.area IS NOT NULL THEN ''::text
                    ELSE '   area is not null   '::text
                END) ||
                CASE
                    WHEN new_1.area >= 0::double precision THEN ''::text
                    ELSE '   area>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM ${model}.network_overflow_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null    '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM ${model}.weir_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= '0'::numeric::double precision THEN ''::text
                    ELSE '   width>=0.   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= '0'::numeric::double precision THEN ''::text
                    ELSE '   cc>=0.   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode IS NOT NULL THEN ''::text
                    ELSE '   break_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'zw_critical'::hydra_fuse_spillway_break_mode OR new_1.z_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''zw_critical'' or z_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode <> 'time_critical'::hydra_fuse_spillway_break_mode OR new_1.t_break IS NOT NULL THEN ''::text
                    ELSE '   break_mode!=''time_critical'' or t_break is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or grp is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp > 0 THEN ''::text
                    ELSE '   break_mode=''none'' or grp>0   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.grp < 100 THEN ''::text
                    ELSE '   break_mode=''none'' or grp<100   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR new_1.dt_fracw_array IS NOT NULL THEN ''::text
                    ELSE '   break_mode=''none'' or dt_fracw_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) <= 10 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 1) >= 1 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.break_mode = 'none'::hydra_fuse_spillway_break_mode OR array_length(new_1.dt_fracw_array, 2) = 2 THEN ''::text
                    ELSE '   break_mode=''none'' or array_length(dt_fracw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM ${model}.fuse_spillway_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN new_1.z_crest1 IS NOT NULL THEN ''::text
                    ELSE '   z_crest1 is not null   '::text
                END ||
                CASE
                    WHEN new_1.width1 IS NOT NULL THEN ''::text
                    ELSE '   width1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width1 > 0::double precision THEN ''::text
                    ELSE '   width1>0   '::text
                END) ||
                CASE
                    WHEN new_1.length IS NOT NULL THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN new_1.length > 0::double precision THEN ''::text
                    ELSE '   length>0   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk >= 0::double precision THEN ''::text
                    ELSE '   rk>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 IS NOT NULL THEN ''::text
                    ELSE '   z_crest2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_crest2 > new_1.z_crest1 THEN ''::text
                    ELSE '   z_crest2>z_crest1   '::text
                END) ||
                CASE
                    WHEN new_1.width2 IS NOT NULL THEN ''::text
                    ELSE '   width2 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width2 > new_1.width1 THEN ''::text
                    ELSE '   width2>width1   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM ${model}.strickler_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert_stop IS NOT NULL THEN ''::text
                    ELSE '   z_invert_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling_stop IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.dt_regul_hr IS NOT NULL THEN ''::text
                    ELSE '   dt_regul_hr is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_control_node IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_pid_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_tz_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR new_1.q_z_crit IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul OR new_1.nr_z_gate IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc_submerged > 0::double precision THEN ''::text
                    ELSE '   cc_submerged >0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM ${model}.regul_gate_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.lateral_contraction_coef IS NOT NULL THEN ''::text
                    ELSE '   lateral_contraction_coef is not null   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef <= 1::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef<=1   '::text
                END) ||
                CASE
                    WHEN new_1.lateral_contraction_coef >= 0::double precision THEN ''::text
                    ELSE '   lateral_contraction_coef>=0   '::text
                END) ||
                CASE
                    WHEN st_npoints(new_1.border) = 2 THEN ''::text
                    ELSE '   ST_NPoints(border)=2   '::text
                END) ||
                CASE
                    WHEN st_isvalid(new_1.border) THEN ''::text
                    ELSE '   ST_IsValid(border)   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM ${model}.mesh_2d_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling > new_1.z_invert THEN ''::text
                    ELSE '   z_ceiling>z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc <=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc >=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_valve IS NOT NULL THEN ''::text
                    ELSE '   mode_valve is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate IS NOT NULL THEN ''::text
                    ELSE '   z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate >= new_1.z_invert THEN ''::text
                    ELSE '   z_gate>=z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate <= new_1.z_ceiling THEN ''::text
                    ELSE '   z_gate<=z_ceiling   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc_submerged > 0::double precision THEN ''::text
                    ELSE '   cc_submerged >0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM ${model}.gate_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.down_type = 'manhole_hydrology'::hydra_node_type OR (new_1.down_type = ANY (ARRAY['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'storage'::hydra_node_type, 'crossroad'::hydra_node_type])) AND new_1.hydrograph IS NOT NULL THEN ''::text
                    ELSE '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'', ''storage'', ''crossroad'') and (hydrograph is not null))   '::text
                END ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM ${model}.connector_hydrology_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.law_type IS NOT NULL THEN ''::text
                    ELSE '   law_type is not null   '::text
                END ||
                CASE
                    WHEN new_1.param IS NOT NULL THEN ''::text
                    ELSE '   param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM ${model}.borda_headloss_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((((((((
                CASE
                    WHEN new_1.z_invert_up IS NOT NULL THEN ''::text
                    ELSE '   z_invert_up is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_invert_down IS NOT NULL THEN ''::text
                    ELSE '   z_invert_down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.h_sable IS NOT NULL THEN ''::text
                    ELSE '   h_sable is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'valley'::hydra_cross_section_type THEN ''::text
                    ELSE '   cross_section_type not in (''valley'')   '::text
                END) ||
                CASE
                    WHEN new_1.rk IS NOT NULL THEN ''::text
                    ELSE '   rk is not null   '::text
                END) ||
                CASE
                    WHEN new_1.rk > 0::double precision THEN ''::text
                    ELSE '   rk >0   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'circular'::hydra_cross_section_type OR new_1.circular_diameter IS NOT NULL AND new_1.circular_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''circular'' or (circular_diameter is not null and circular_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_top_diameter IS NOT NULL AND new_1.ovoid_top_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_top_diameter is not null and ovoid_top_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_invert_diameter IS NOT NULL AND new_1.ovoid_invert_diameter > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_invert_diameter is not null and ovoid_invert_diameter > 0)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_height IS NOT NULL AND new_1.ovoid_height > 0::double precision THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_height is not null and ovoid_height >0 )   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'ovoid'::hydra_cross_section_type OR new_1.ovoid_height > ((new_1.ovoid_top_diameter + new_1.ovoid_invert_diameter) / 2::double precision) THEN ''::text
                    ELSE '   cross_section_type!=''ovoid'' or (ovoid_height > (ovoid_top_diameter+ovoid_invert_diameter)/2)   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'pipe'::hydra_cross_section_type OR new_1.cp_geom IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type!=''pipe'' or cp_geom is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cross_section_type <> 'channel'::hydra_cross_section_type OR new_1.op_geom IS NOT NULL THEN ''::text
                    ELSE '   cross_section_type!=''channel'' or op_geom is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM ${model}.pipe_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.transmitivity IS NOT NULL THEN ''::text
                    ELSE '   transmitivity is not null   '::text
                END) ||
                CASE
                    WHEN new_1.transmitivity >= 0::double precision THEN ''::text
                    ELSE '   transmitivity>=0   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM ${model}.porous_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN new_1.npump IS NOT NULL THEN ''::text
                    ELSE '   npump is not null   '::text
                END ||
                CASE
                    WHEN new_1.npump <= 10 THEN ''::text
                    ELSE '   npump<=10   '::text
                END) ||
                CASE
                    WHEN new_1.npump >= 1 THEN ''::text
                    ELSE '   npump>=1   '::text
                END) ||
                CASE
                    WHEN new_1.zregul_array IS NOT NULL THEN ''::text
                    ELSE '   zregul_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.hq_array IS NOT NULL THEN ''::text
                    ELSE '   hq_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zregul_array, 1) = new_1.npump THEN ''::text
                    ELSE '   array_length(zregul_array, 1)=npump   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zregul_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zregul_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 1) = new_1.npump THEN ''::text
                    ELSE '   array_length(hq_array, 1)=npump   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 2) <= 10 THEN ''::text
                    ELSE '   array_length(hq_array, 2)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 2) >= 1 THEN ''::text
                    ELSE '   array_length(hq_array, 2)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.hq_array, 3) = 2 THEN ''::text
                    ELSE '   array_length(hq_array, 3)=2   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM ${model}.pump_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((((
                CASE
                    WHEN new_1.q_pump IS NOT NULL THEN ''::text
                    ELSE '   q_pump is not null   '::text
                END ||
                CASE
                    WHEN new_1.q_pump >= 0::double precision THEN ''::text
                    ELSE '   q_pump>= 0   '::text
                END) ||
                CASE
                    WHEN new_1.qz_array IS NOT NULL THEN ''::text
                    ELSE '   qz_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(qz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN ( SELECT bool_and(data_set.bool) AS bool_and
                       FROM ( SELECT 0::double precision <= unnest(new_1.qz_array[:][1:1]) AS bool) data_set) THEN ''::text
                    ELSE '   (/*   qz_array: 0<=q   */ select bool_and(bool) from (select 0<=unnest(new.qz_array[:][1]) as bool) as data_set)   '::text
                END) ||
                CASE
                    WHEN ( SELECT bool_and(data_set.bool) AS bool_and
                       FROM ( SELECT unnest(new_1.qz_array[:][1:1]) <= 1::double precision AS bool) data_set) THEN ''::text
                    ELSE '   (/*   qz_array: q<=1   */ select bool_and(bool) from (select unnest(new.qz_array[:][1])<=1 as bool) as data_set)   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM ${model}.deriv_pump_link new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.cross_section IS NOT NULL THEN ''::text
                    ELSE '   cross_section is not null   '::text
                END ||
                CASE
                    WHEN new_1.cross_section >= 0::double precision THEN ''::text
                    ELSE '   cross_section>=0   '::text
                END) ||
                CASE
                    WHEN new_1.length IS NOT NULL THEN ''::text
                    ELSE '   length is not null   '::text
                END) ||
                CASE
                    WHEN new_1.length >= 0::double precision THEN ''::text
                    ELSE '   length>=0   '::text
                END) ||
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type = 'manhole_hydrology'::hydra_node_type OR (new_1.down_type = ANY (ARRAY['manhole'::hydra_node_type, 'river'::hydra_node_type, 'station'::hydra_node_type, 'crossroad'::hydra_node_type, 'storage'::hydra_node_type])) AND new_1.hydrograph IS NOT NULL THEN ''::text
                    ELSE '   down_type=''manhole_hydrology'' or (down_type in (''manhole'', ''river'', ''station'', ''crossroad'', ''storage'') and (hydrograph is not null))   '::text
                END) ||
                CASE
                    WHEN new_1.up IS NOT NULL THEN ''::text
                    ELSE '   up is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down IS NOT NULL THEN ''::text
                    ELSE '   down is not null   '::text
                END) ||
                CASE
                    WHEN new_1.up_type IS NOT NULL THEN ''::text
                    ELSE '   up_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.down_type IS NOT NULL THEN ''::text
                    ELSE '   down_type is not null   '::text
                END AS reason
           FROM ${model}.routing_hydrology_link new_1
          WHERE NOT new_1.validity
        ), singularity_invalidity_reason AS (
         SELECT new_1.id,
            ((
                CASE
                    WHEN new_1.zq_array IS NOT NULL THEN ''::text
                    ELSE '   zq_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zq_array, 2)=2   '::text
                END AS reason
           FROM ${model}.zq_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.drainage IS NOT NULL THEN ''::text
                    ELSE '   drainage is not null   '::text
                END ||
                CASE
                    WHEN new_1.overflow IS NOT NULL THEN ''::text
                    ELSE '   overflow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ini IS NOT NULL THEN ''::text
                    ELSE '   z_ini is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zr_sr_qf_qs_array IS NOT NULL THEN ''::text
                    ELSE '   zr_sr_qf_qs_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.treatment_mode IS NOT NULL THEN ''::text
                    ELSE '   treatment_mode is not null   '::text
                END) ||
                CASE
                    WHEN new_1.treatment_param IS NOT NULL THEN ''::text
                    ELSE '   treatment_param is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zr_sr_qf_qs_array, 2) = 4 THEN ''::text
                    ELSE '   array_length(zr_sr_qf_qs_array, 2)=4   '::text
                END AS reason
           FROM ${model}.reservoir_rsp_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.qq_array IS NOT NULL THEN ''::text
                    ELSE '   qq_array is not null   '::text
                END ||
                CASE
                    WHEN new_1.downstream IS NOT NULL THEN ''::text
                    ELSE '   downstream is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1 IS NOT NULL THEN ''::text
                    ELSE '   split1 is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qq_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NOT NULL AND array_length(new_1.qq_array, 2) = 3 OR new_1.split2 IS NULL AND array_length(new_1.qq_array, 2) = 2 THEN ''::text
                    ELSE '   (split2 is not null and array_length(qq_array, 2)=3) or (split2 is null and array_length(qq_array, 2)=2)   '::text
                END AS reason
           FROM ${model}.qq_split_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling > new_1.z_invert THEN ''::text
                    ELSE '   z_ceiling>z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc <=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc >=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_valve IS NOT NULL THEN ''::text
                    ELSE '   mode_valve is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate IS NOT NULL THEN ''::text
                    ELSE '   z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate >= new_1.z_invert THEN ''::text
                    ELSE '   z_gate>=z_invert   '::text
                END) ||
                CASE
                    WHEN new_1.z_gate <= new_1.z_ceiling THEN ''::text
                    ELSE '   z_gate<=z_ceiling   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN NOT ${model}.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END) ||
                CASE
                    WHEN new_1.cc_submerged > 0::double precision THEN ''::text
                    ELSE '   cc_submerged >0   '::text
                END AS reason
           FROM ${model}.gate_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((
                CASE
                    WHEN new_1.q_dz_array IS NOT NULL THEN ''::text
                    ELSE '   q_dz_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(q_dz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(q_dz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.q_dz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(q_dz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.q_dz_array[1][1] = 0::double precision THEN ''::text
                    ELSE '   q_dz_array[1][1]=0   '::text
                END) ||
                CASE
                    WHEN new_1.q_dz_array[1][2] = 0::double precision THEN ''::text
                    ELSE '   q_dz_array[1][2]=0   '::text
                END) ||
                CASE
                    WHEN NOT ${model}.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM ${model}.param_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((((((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null   '::text
                END ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width >=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc <=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc >=0   '::text
                END) ||
                CASE
                    WHEN new_1.action_gate_type IS NOT NULL THEN ''::text
                    ELSE '   action_gate_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_invert_stop IS NOT NULL THEN ''::text
                    ELSE '   z_invert_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling_stop IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling_stop is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms IS NOT NULL THEN ''::text
                    ELSE '   v_max_cms is not null   '::text
                END) ||
                CASE
                    WHEN new_1.v_max_cms >= 0::double precision THEN ''::text
                    ELSE '   v_max_cms>=0   '::text
                END) ||
                CASE
                    WHEN new_1.dt_regul_hr IS NOT NULL THEN ''::text
                    ELSE '   dt_regul_hr is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_control_node IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_control_node is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_pid_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_pid_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR new_1.z_tz_array IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or z_tz_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'elevation'::hydra_gate_mode_regul OR array_length(new_1.z_tz_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''elevation'' or array_length(z_tz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR new_1.q_z_crit IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or q_z_crit is not null   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) <= 10 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 1) >= 1 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'discharge'::hydra_gate_mode_regul OR array_length(new_1.q_tq_array, 2) = 2 THEN ''::text
                    ELSE '   mode_regul!=''discharge'' or array_length(q_tq_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul <> 'no_regulation'::hydra_gate_mode_regul OR new_1.nr_z_gate IS NOT NULL THEN ''::text
                    ELSE '   mode_regul!=''no_regulation'' or nr_z_gate is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc_submerged > 0::double precision THEN ''::text
                    ELSE '   cc_submerged >0   '::text
                END AS reason
           FROM ${model}.regul_sluice_gate_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((
                CASE
                    WHEN new_1.downstream IS NOT NULL THEN ''::text
                    ELSE '   downstream is not null   '::text
                END ||
                CASE
                    WHEN new_1.split1 IS NOT NULL THEN ''::text
                    ELSE '   split1 is not null   '::text
                END) ||
                CASE
                    WHEN new_1.downstream_param IS NOT NULL THEN ''::text
                    ELSE '   downstream_param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1_law IS NOT NULL THEN ''::text
                    ELSE '   split1_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split1_param IS NOT NULL THEN ''::text
                    ELSE '   split1_param is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NULL OR new_1.split2_law IS NOT NULL THEN ''::text
                    ELSE '   split2 is null or split2_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.split2 IS NULL OR new_1.split2_param IS NOT NULL THEN ''::text
                    ELSE '   split2 is null or split2_param is not null   '::text
                END AS reason
           FROM ${model}.zq_split_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((
                CASE
                    WHEN new_1.d_abutment_l IS NOT NULL THEN ''::text
                    ELSE '   d_abutment_l is not null   '::text
                END ||
                CASE
                    WHEN new_1.d_abutment_r IS NOT NULL THEN ''::text
                    ELSE '   d_abutment_r is not null   '::text
                END) ||
                CASE
                    WHEN new_1.abutment_type IS NOT NULL THEN ''::text
                    ELSE '   abutment_type is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zw_array IS NOT NULL THEN ''::text
                    ELSE '   zw_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.z_ceiling IS NOT NULL THEN ''::text
                    ELSE '   z_ceiling is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT ${model}.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM ${model}.bradley_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((
                CASE
                    WHEN new_1.external_file_data OR new_1.tz_array IS NOT NULL THEN ''::text
                    ELSE '   external_file_data or tz_array is not null   '::text
                END ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 1) <= 10 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 1) >= 1 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tz_array, 2) = 2 THEN ''::text
                    ELSE '   external_file_data or array_length(tz_array, 2)=2   '::text
                END AS reason
           FROM ${model}.tz_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.qz_array IS NOT NULL THEN ''::text
                    ELSE '   qz_array is not null   '::text
                END ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(qz_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(qz_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.qz_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(qz_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT ${model}.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM ${model}.hydraulic_cut_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.pk0_km IS NOT NULL THEN ''::text
                    ELSE '   pk0_km is not null   '::text
                END ||
                CASE
                    WHEN new_1.dx IS NOT NULL THEN ''::text
                    ELSE '   dx is not null   '::text
                END) ||
                CASE
                    WHEN new_1.dx >= 0.1::double precision THEN ''::text
                    ELSE '   dx>=0.1   '::text
                END AS reason
           FROM ${model}.pipe_branch_marker_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((
                CASE
                    WHEN new_1.slope IS NOT NULL THEN ''::text
                    ELSE '   slope is not null   '::text
                END ||
                CASE
                    WHEN new_1.k IS NOT NULL THEN ''::text
                    ELSE '   k is not null   '::text
                END) ||
                CASE
                    WHEN new_1.k > 0::double precision THEN ''::text
                    ELSE '   k>0   '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END AS reason
           FROM ${model}.strickler_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
                CASE
                    WHEN new_1.q0 IS NOT NULL THEN ''::text
                    ELSE '   q0 is not null   '::text
                END AS reason
           FROM ${model}.constant_inflow_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.cascade_mode IS NOT NULL THEN ''::text
                    ELSE '   cascade_mode is not null   '::text
                END ||
                CASE
                    WHEN new_1.cascade_mode = 'hydrograph'::hydra_model_connect_mode OR new_1.zq_array IS NOT NULL THEN ''::text
                    ELSE '   cascade_mode=''hydrograph'' or zq_array is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) <= 100 THEN ''::text
                    ELSE '   array_length(zq_array, 1)<=100   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zq_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zq_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.quality, 1) = 9 THEN ''::text
                    ELSE '   array_length(quality, 1)=9   '::text
                END AS reason
           FROM ${model}.model_connect_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END ||
                CASE
                    WHEN new_1.zini IS NOT NULL THEN ''::text
                    ELSE '   zini is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN new_1.treatment_mode IS NOT NULL THEN ''::text
                    ELSE '   treatment_mode is not null   '::text
                END AS reason
           FROM ${model}.tank_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((
                CASE
                    WHEN new_1.z_weir IS NOT NULL THEN ''::text
                    ELSE '   z_weir is not null   '::text
                END ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= '0'::numeric::double precision THEN ''::text
                    ELSE '   cc>=0.   '::text
                END AS reason
           FROM ${model}.weir_bc_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (
                CASE
                    WHEN new_1.law_type IS NOT NULL THEN ''::text
                    ELSE '   law_type is not null   '::text
                END ||
                CASE
                    WHEN new_1.param IS NOT NULL THEN ''::text
                    ELSE '   param is not null   '::text
                END) ||
                CASE
                    WHEN NOT ${model}.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM ${model}.borda_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((
                CASE
                    WHEN new_1.l_road IS NOT NULL THEN ''::text
                    ELSE '   l_road is not null   '::text
                END ||
                CASE
                    WHEN new_1.l_road >= 0::double precision THEN ''::text
                    ELSE '   l_road>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_road IS NOT NULL THEN ''::text
                    ELSE '   z_road is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zw_array IS NOT NULL THEN ''::text
                    ELSE '   zw_array is not null    '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zw_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zw_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zw_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zw_array, 2)=2   '::text
                END) ||
                CASE
                    WHEN NOT ${model}.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM ${model}.bridge_headloss_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            ((((((((
                CASE
                    WHEN new_1.drainage IS NOT NULL THEN ''::text
                    ELSE '   drainage is not null   '::text
                END ||
                CASE
                    WHEN new_1.overflow IS NOT NULL THEN ''::text
                    ELSE '   overflow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q_drainage IS NOT NULL THEN ''::text
                    ELSE '   q_drainage is not null   '::text
                END) ||
                CASE
                    WHEN new_1.q_drainage >= 0::double precision THEN ''::text
                    ELSE '   q_drainage>=0   '::text
                END) ||
                CASE
                    WHEN new_1.z_ini IS NOT NULL THEN ''::text
                    ELSE '   z_ini is not null   '::text
                END) ||
                CASE
                    WHEN new_1.zs_array IS NOT NULL THEN ''::text
                    ELSE '   zs_array is not null    '::text
                END) ||
                CASE
                    WHEN new_1.treatment_mode IS NOT NULL THEN ''::text
                    ELSE '   treatment_mode is not null   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) <= 10 THEN ''::text
                    ELSE '   array_length(zs_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 1) >= 1 THEN ''::text
                    ELSE '   array_length(zs_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.zs_array, 2) = 2 THEN ''::text
                    ELSE '   array_length(zs_array, 2)=2   '::text
                END AS reason
           FROM ${model}.reservoir_rs_hydrology_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((
                CASE
                    WHEN new_1.z_invert IS NOT NULL THEN ''::text
                    ELSE '   z_invert is not null    '::text
                END ||
                CASE
                    WHEN new_1.z_regul IS NOT NULL THEN ''::text
                    ELSE '   z_regul is not null    '::text
                END) ||
                CASE
                    WHEN new_1.width IS NOT NULL THEN ''::text
                    ELSE '   width is not null   '::text
                END) ||
                CASE
                    WHEN new_1.width >= 0::double precision THEN ''::text
                    ELSE '   width>=0   '::text
                END) ||
                CASE
                    WHEN new_1.cc IS NOT NULL THEN ''::text
                    ELSE '   cc is not null   '::text
                END) ||
                CASE
                    WHEN new_1.cc <= 1::double precision THEN ''::text
                    ELSE '   cc<=1   '::text
                END) ||
                CASE
                    WHEN new_1.cc >= 0::double precision THEN ''::text
                    ELSE '   cc>=0   '::text
                END) ||
                CASE
                    WHEN new_1.mode_regul IS NOT NULL THEN ''::text
                    ELSE '   mode_regul is not null   '::text
                END) ||
                CASE
                    WHEN new_1.reoxy_law IS NOT NULL THEN ''::text
                    ELSE '   reoxy_law is not null   '::text
                END) ||
                CASE
                    WHEN new_1.reoxy_param IS NOT NULL THEN ''::text
                    ELSE '   reoxy_param is not null   '::text
                END) ||
                CASE
                    WHEN NOT ${model}.check_on_branch_or_reach_endpoint(new_1.geom) THEN ''::text
                    ELSE '   not ${model}.check_on_branch_or_reach_endpoint(new.geom)   '::text
                END AS reason
           FROM ${model}.zregul_weir_singularity new_1
          WHERE NOT new_1.validity
        UNION
         SELECT new_1.id,
            (((((((((((((
                CASE
                    WHEN new_1.storage_area IS NOT NULL THEN ''::text
                    ELSE '   storage_area is not null   '::text
                END ||
                CASE
                    WHEN new_1.storage_area >= 0::double precision THEN ''::text
                    ELSE '   storage_area>=0   '::text
                END) ||
                CASE
                    WHEN new_1.constant_dry_flow IS NOT NULL THEN ''::text
                    ELSE '   constant_dry_flow is not null   '::text
                END) ||
                CASE
                    WHEN new_1.constant_dry_flow >= 0::double precision THEN ''::text
                    ELSE '   constant_dry_flow>=0   '::text
                END) ||
                CASE
                    WHEN new_1.distrib_coef IS NULL OR new_1.distrib_coef >= 0::double precision THEN ''::text
                    ELSE '   distrib_coef is null or distrib_coef>=0   '::text
                END) ||
                CASE
                    WHEN new_1.lag_time_hr IS NULL OR new_1.lag_time_hr >= 0::double precision THEN ''::text
                    ELSE '   lag_time_hr is null or (lag_time_hr>=0)   '::text
                END) ||
                CASE
                    WHEN new_1.sector IS NULL OR new_1.distrib_coef IS NOT NULL AND new_1.lag_time_hr IS NOT NULL THEN ''::text
                    ELSE '   sector is null or (distrib_coef is not null and lag_time_hr is not null)   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.pollution_dryweather_runoff, 1) = 2 THEN ''::text
                    ELSE '   array_length(pollution_dryweather_runoff, 1)=2   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.pollution_dryweather_runoff, 2) = 4 THEN ''::text
                    ELSE '   array_length(pollution_dryweather_runoff, 2)=4   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.quality_dryweather_runoff, 1) = 2 THEN ''::text
                    ELSE '   array_length(quality_dryweather_runoff, 1)=2   '::text
                END) ||
                CASE
                    WHEN array_length(new_1.quality_dryweather_runoff, 2) = 9 THEN ''::text
                    ELSE '   array_length(quality_dryweather_runoff, 2)=9   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR new_1.tq_array IS NOT NULL THEN ''::text
                    ELSE '   external_file_data or tq_array is not null   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 1) <= 10 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 1)<=10   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 1) >= 1 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 1)>=1   '::text
                END) ||
                CASE
                    WHEN new_1.external_file_data OR array_length(new_1.tq_array, 2) = 2 THEN ''::text
                    ELSE '   external_file_data or array_length(tq_array, 2)=2   '::text
                END AS reason
           FROM ${model}.hydrograph_bc_singularity new_1
          WHERE NOT new_1.validity
        )
    select CONCAT('node:', n.id) as
        id,
        validity,
        name,
        'node'::varchar as type,
        node_type::varchar as subtype,
        geom::geometry('POINTZ', $srid) as geom,
        reason from $model._node as n join node_invalidity_reason as r on r.id=n.id
        where validity=FALSE
    union
    select CONCAT('link:', l.id) as
        id,
        validity,
        name,
        'link'::varchar as type,
        link_type::varchar as subtype,
        ST_Force3D(st_centroid(geom))::geometry('POINTZ', $srid) as geom,
        reason from $model._link as l join link_invalidity_reason as r on  r.id=l.id
        where validity=FALSE
    union
    select CONCAT('singularity:', s.id) as
        id,
        s.validity,
        s.name,
        'singularity'::varchar as type,
        s.singularity_type::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        reason from $model._singularity as s join $model._node as n on s.node = n.id join singularity_invalidity_reason as r on r.id=s.id
        where s.validity=FALSE
    union
    select CONCAT('profile:', p.id) as
        id,
        p.validity,
        p.name,
        'profile'::varchar as type,
        ''::varchar as subtype,
        n.geom::geometry('POINTZ', $srid) as geom,
        ''::varchar as reason from $model._river_cross_section_profile as p join $model._node as n on p.id = n.id
        where p.validity=FALSE
;;