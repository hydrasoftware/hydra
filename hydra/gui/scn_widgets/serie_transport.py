# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

from __future__ import unicode_literals
from builtins import str
from builtins import range
import os
import webbrowser
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QFileDialog, QWidget, QGridLayout
from hydra.gui.base_dialog import tr
from hydra.utility.system import open_external


class SerieTransportWidget(QWidget):

    def __init__(self, current_project, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "transport.ui"), self)
        tmplayout = QGridLayout()
        tmplayout.addWidget(self,0,0)
        parent.setLayout(tmplayout)
        self.active_serie_id = None
        self.project = current_project

        self.__load_transport_controls()
        self.current_transport_type = "no_transport"
        self.current_quality_type = "physico_chemical"

        self.add_sediment_file.clicked.connect(self.__select_file)
        self.edit_sediment_file.clicked.connect(self.__edit_file)
        self.help_sediment_file.clicked.connect(self.__help_file)

        self.diffusion.stateChanged.connect(self.diffusion_changed)
        self.diffusion_changed()

        self.show()

    def __select_file(self):
        file_url, __ = QFileDialog.getOpenFileName(self, tr('Select sediment file'), self.project.data_dir, options=QFileDialog.DontConfirmOverwrite)
        if not file_url is None and file_url!='':
            # creates file empty if not exists
            open(file_url, 'a').close()
            self.sediment_file.setText(file_url)

    def __edit_file(self):
        file = self.sediment_file.text()
        open_external(file)

    def __help_file(self):
        webbrowser.open('http://hydra-software.net/docs/notes_techniques/NT27.pdf', new=0)

    def set_serie(self, id_serie):
        self.active_serie_id = id_serie
        if self.active_serie_id :
            self.current_transport_type, self.current_quality_type = self.project.execute("""
                select transport_type, quality_type
                from project.serie
                where id={} limit 1""".format(str(self.active_serie_id))).fetchone()
            self.__init_ui_transport()

    def diffusion_changed(self):
        self.table_coef_quality.setEnabled(self.diffusion.isChecked())

    def save(self):
        if self.active_serie_id:
            self.project.execute("""
                update project.serie set
                    iflag_mes='{}', iflag_dbo5='{}', iflag_dco='{}', iflag_ntk='{}', sediment_file='{}'
                where id={}
                """.format(str(self.iflag_mes.isChecked()),
                str(self.iflag_dbo5.isChecked()),
                str(self.iflag_dco.isChecked()),
                str(self.iflag_ntk.isChecked()),
                str(self.project.pack_path(self.sediment_file.text())),
                str(self.active_serie_id)))

            self.project.execute("""
                update project.serie set
                    diffusion='{}',
                    dispersion_coef='{}',
                    longit_diffusion_coef='{}',
                    lateral_diff_coef='{}',
                    quality_type='{}'
                where id={}
                """.format(str(self.diffusion.isChecked()),
                str(self.table_coef_quality.item(0, 0).text()),
                str(self.table_coef_quality.item(1, 0).text()),
                str(self.table_coef_quality.item(2, 0).text()),
                str(self.current_quality_type),
                str(self.active_serie_id)))

            self.project.execute("""
                update project.serie set
                    water_temperature_c='{}',
                    min_o2_mgl='{}',
                    kdbo5_j_minus_1='{}',
                    koxygen_j_minus_1='{}',
                    knr_j_minus_1='{}',
                    kn_j_minus_1='{}',
                    bdf_mgl='{}',
                    o2sat_mgl='{}'
                where id={}
                """.format(
                str(self.table_class.item(0, 1).text()),
                str(self.table_class.item(1, 1).text()),
                str(self.table_class.item(2, 1).text()),
                str(self.table_class.item(3, 1).text()),
                str(self.table_class.item(4, 1).text()),
                str(self.table_class.item(5, 1).text()),
                str(self.table_class.item(6, 1).text()),
                str(self.table_class.item(7, 1).text()),
                str(self.active_serie_id)))

            self.project.execute("""
                update project.serie set
                    iflag_ecoli='{}',
                    iflag_enti='{}',
                    iflag_ad1='{}',
                    iflag_ad2='{}',
                    ecoli_decay_rate_jminus_one='{}',
                    enti_decay_rate_jminus_one='{}',
                    ad1_decay_rate_jminus_one='{}',
                    ad2_decay_rate_jminus_one='{}'
                where id={}
                """.format(str(self.iflag_ecoli.isChecked()),
                str(self.iflag_enti.isChecked()),
                str(self.iflag_ad1.isChecked()),
                str(self.iflag_ad2.isChecked()),
                str(self.ecoli_decay_rate_jminus_one.text()),
                str(self.enti_decay_rate_jminus_one.text()),
                str(self.ad1_decay_rate_jminus_one.text()),
                str(self.ad2_decay_rate_jminus_one.text()),
                str(self.active_serie_id)))

            self.project.execute("""
                update project.serie set
                    suspended_sediment_fall_velocity_mhr='{}'
                where id={}
                """.format(str(self.fall_velocity_mhr.text()),
                str(self.active_serie_id)))

            self.project.execute("""
                update project.serie set
                    transport_type='{}',
                    quality_type='{}'
                where id={}""".format(str(self.current_transport_type),
                str(self.current_quality_type),
                str(self.active_serie_id)))

    def __load_transport_controls(self):
        self.transport_types = self.project.execute("select name, id, description from hydra.transport_type where not id='3' order by id asc").fetchall()
        self.quality_types = self.project.execute("select name, id, description from hydra.quality_type order by id asc").fetchall()

        self.cmb_transport.clear()
        for i in range(0,len(self.transport_types)):
            self.cmb_transport.addItem(tr(self.transport_types[i][2]))
        for i in range(0,len(self.quality_types)):
            self.cmb_transport.addItem(tr(self.quality_types[i][2]))

        self.cmb_transport.setCurrentIndex(0)
        self.cmb_transport.currentIndexChanged.connect(self.transport_selection_change)
        self.transport_selection_change(0)

    def __init_ui_transport(self):
        diffusion, dispersion_coef, longit_diffusion_coef, \
        lateral_diff_coef = self.project.execute("""
            select diffusion, dispersion_coef, longit_diffusion_coef,
            lateral_diff_coef from project.serie
            where id='{}'
            """.format(str(self.active_serie_id))).fetchone()

        found=False
        for i in range(0,len(self.transport_types)):
            if self.transport_types[i][0]==self.current_transport_type:
                self.cmb_transport.setCurrentIndex(i)
                found=True
                break
        if not found:
            for i in range(0,len(self.quality_types)):
                if self.quality_types[i][0]==self.current_quality_type:
                    self.cmb_transport.setCurrentIndex(i+len(self.transport_types))
                    found=True
                    break
        assert found==True

        iflag_mes, iflag_dbo5, iflag_dco, iflag_ntk, sediment_file, = self.project.execute("""
            select iflag_mes, iflag_dbo5, iflag_dco, iflag_ntk, sediment_file from project.serie
            where id='{}'
            """.format(self.active_serie_id)).fetchone()
        self.iflag_dbo5.setChecked(iflag_dbo5)
        self.iflag_mes.setChecked(iflag_mes)
        self.iflag_dco.setChecked(iflag_dco)
        self.iflag_ntk.setChecked(iflag_ntk)
        self.sediment_file.setText(self.project.unpack_path(sediment_file))

        self.diffusion.setChecked(diffusion)
        self.table_coef_quality.item(0, 0).setText(str(dispersion_coef))
        self.table_coef_quality.item(1, 0).setText(str(longit_diffusion_coef))
        self.table_coef_quality.item(2, 0).setText(str(lateral_diff_coef))

        water_temperature_c, min_o2_mgl, kdbo5_j_minus_1, koxygen_j_minus_1, \
        knr_j_minus_1, kn_j_minus_1, bdf_mgl, o2sat_mgl, = self.project.execute("""
            select water_temperature_c, min_o2_mgl, kdbo5_j_minus_1, koxygen_j_minus_1,
            knr_j_minus_1, kn_j_minus_1, bdf_mgl, o2sat_mgl
            from project.serie
            where id={}
            """.format(str(self.active_serie_id))).fetchone()

        self.table_class.item(0, 1).setText(str(water_temperature_c))
        self.table_class.item(1, 1).setText(str(min_o2_mgl))
        self.table_class.item(2, 1).setText(str(kdbo5_j_minus_1))
        self.table_class.item(3, 1).setText(str(koxygen_j_minus_1))
        self.table_class.item(4, 1).setText(str(knr_j_minus_1))
        self.table_class.item(5, 1).setText(str(kn_j_minus_1))
        self.table_class.item(6, 1).setText(str(bdf_mgl))
        self.table_class.item(7, 1).setText(str(o2sat_mgl))


        iflag_ecoli, iflag_enti, iflag_ad1, iflag_ad2, \
        ecoli_decay_rate_jminus_one, enti_decay_rate_jminus_one, \
        ad1_decay_rate_jminus_one, ad2_decay_rate_jminus_one, = self.project.execute("""
            select iflag_ecoli, iflag_enti, iflag_ad1, iflag_ad2,
                ecoli_decay_rate_jminus_one, enti_decay_rate_jminus_one,
                ad1_decay_rate_jminus_one, ad2_decay_rate_jminus_one
            from project.serie
            where id={}
            """.format(str(self.active_serie_id))).fetchone()

        self.iflag_ecoli.setChecked(iflag_ecoli)
        self.iflag_enti.setChecked(iflag_enti)
        self.iflag_ad1.setChecked(iflag_ad1)
        self.iflag_ad2.setChecked(iflag_ad2)
        self.ecoli_decay_rate_jminus_one.setText(str(ecoli_decay_rate_jminus_one))
        self.enti_decay_rate_jminus_one.setText(str(enti_decay_rate_jminus_one))
        self.ad1_decay_rate_jminus_one.setText(str(ad1_decay_rate_jminus_one))
        self.ad2_decay_rate_jminus_one.setText(str(ad2_decay_rate_jminus_one))

        fall_velocity_mhr = self.project.execute("""select suspended_sediment_fall_velocity_mhr from project.serie where id={}""".format(str(self.active_serie_id))).fetchone()[0]

        self.fall_velocity_mhr.setText(str(fall_velocity_mhr))

    def transport_selection_change(self, i):
        self.current_transport_type = "no_transport"
        self.current_quality_type = "physico_chemical"

        if (i < len(self.transport_types)):
            for j in range(0,len(self.transport_types)):
                if tr(self.transport_types[j][2])==self.cmb_transport.currentText():
                    self.current_transport_type = self.transport_types[j][0]
                    break

            self.diffusion.hide()
            self.table_coef_quality.hide()
            if (self.current_transport_type=="no_transport"):
                self.stacked_transport.setCurrentIndex(4)
            elif (self.current_transport_type=="pollution"):
                self.stacked_transport.setCurrentIndex(1)
            elif (self.current_transport_type=="sediment"):
                self.stacked_transport.setCurrentIndex(0)
        else:
            self.current_transport_type = "quality"
            for j in range(0,len(self.quality_types)):
                if tr(self.quality_types[j][2])==self.cmb_transport.currentText():
                    self.current_quality_type = self.quality_types[j][0]
                    break

            self.diffusion.show()
            self.table_coef_quality.show()
            if (self.current_quality_type=="physico_chemical"):
                self.stacked_transport.setCurrentIndex(2)
            elif (self.current_quality_type=="bacteriological_quality"):
                self.stacked_transport.setCurrentIndex(3)
            elif (self.current_quality_type=="suspended_sediment_transport"):
                self.stacked_transport.setCurrentIndex(5)
            else:
                self.stacked_transport.setCurrentIndex(4)
                self.diffusion.hide()
                self.table_coef_quality.hide()