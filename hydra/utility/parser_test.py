# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
run test to check visu_graph import and loading of graph with external files

USAGE

   python -m hydra.gui.visu_graph_guitest.py [-h]

OPTIONS

   -h, --help
        print this help

"""

import os
import dateutil
from hydra.utility.parser import parse

__current_dir__ = os.path.dirname(__file__)
test_dir = os.path.join(__current_dir__,'test_data')

def test() :

    p1 = parse(os.path.join(test_dir,'mono!.hyd'), 'time', dateutil.parser.parse('2020-01-01'))
    p2 = parse(os.path.join(test_dir,'mono!.hyd'), 'time', dateutil.parser.parse('2020-01-01'))
    p3 = parse(os.path.join(test_dir,'timemulticourbestar.hyd'), 'time', dateutil.parser.parse('2020-01-01'))
    p4 = parse(os.path.join(test_dir,'multiline$.hyd'), 'time', dateutil.parser.parse('2020-01-01'))
    p5 = parse(os.path.join(test_dir,'timemulticolumn%.hyd'), 'time', dateutil.parser.parse('2020-01-01'))
    p6 = parse(os.path.join(test_dir,'datemulticolumn%.hyd'), "date", dateutil.parser.parse("01/02/1975  22:35:00"))
    p7 = parse(os.path.join(test_dir,'datehugedataset$.dat'), "date", dateutil.parser.parse("01/01/1965"))

    assert len(p1) == 891
    assert p1['time(h)'].iloc[-1] == 1449.46

    assert len(p2.columns) == 5
    assert p2.columns[2] == 'MEAUX'
    assert p2.iloc[0,2] == 86.87414

    assert len(p3.columns) == 5
    assert p3.columns[2] == 'Hober Mallow'
    assert p3.iloc[0,2] == 86.874146

    assert len(p4) == 10
    assert p4.columns[2] == 'station1_Q'
    assert p4.iloc[2,2] == 22.0

    assert len(p5.columns) == 4
    assert p5.columns[2] == 'hy_111'
    assert p5.iloc[0, 2] == 0

    assert len(p6.columns) == 4
    assert p6.columns[2] == 'hy_111'
    assert p6.iloc[0,2] == 0

    assert len(p7.columns) == 4
    assert p7.columns[2] == 'hy_loire_debit'
    assert p7.iloc[2,2] == 675.0
    assert len(p7) == 4749


    p8  = parse(os.path.join(test_dir,'date_line_multiple.csv'), 'date', dateutil.parser.parse('2021-01-01 18'))
    p9  = parse(os.path.join(test_dir,'date_line_multiple.xls'), 'date', dateutil.parser.parse('2021-01-01 18'))
    p10 = parse(os.path.join(test_dir,'date_line_multiple_coma.csv'), 'date', dateutil.parser.parse('2021-01-01 18'))

    assert all([v8 == v9 and v9 == v10 for v8, v9, v10 in zip(p8.columns, p9.columns, p10.columns)])
    assert all(p8 == p9)
    assert all(p9 == p10)

    p11 = parse(os.path.join(test_dir,'date_line_single.csv'), 'date', dateutil.parser.parse('2021-01-01 18'))
    assert len(p11.columns) == 3
    assert abs(p11.iloc[-1,1]) < 1e-5

    p11 = parse(os.path.join(test_dir,'Juillet_2017_5min_biev.rad'), 'date', dateutil.parser.parse('2021-01-01 18'))



test()

print("ok")
