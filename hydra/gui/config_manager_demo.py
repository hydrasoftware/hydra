# coding=utf-8

from qgis.PyQt.QtWidgets import QApplication
from qgis.PyQt.QtCore import QCoreApplication, QTranslator
from hydra.gui.config_manager import ConfigManager
from hydra.project import Project

import sys
app = QApplication(sys.argv)
translator = QTranslator()
translator.load("i18n/fr", "../..")
QCoreApplication.installTranslator(translator)
guitranslator = QTranslator()
guitranslator.load("/usr/share/qt4/translations/qt_fr")
QCoreApplication.installTranslator(guitranslator)

project = Project.load_project("config_test")
new_config_manager = ConfigManager(project)
new_config_manager.exec_()


