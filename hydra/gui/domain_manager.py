# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from .base_dialog import BaseDialog, tr
from hydra.gui.edit import EditTool
from hydra.gui.delete import DeleteTool
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from hydra.utility.string import normalized_name
from hydra.utility.tables_properties import TablesProperties
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QTableWidgetItem, QAbstractItemView, QTextEdit

class DomainManager(BaseDialog):
    def __init__(self, project, parent=None):
        BaseDialog.__init__(self, project, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "domain_manager.ui"), self)

        self.project = project

        self.buttonBox.accepted.connect(self.sav)

        self.model.addItems(self.project.get_models())
        if self.project.get_current_model() is not None :
            self.model.setCurrentIndex(self.project.get_models().index(self.project.get_current_model().name))
        else :
            self.model.setCurrentIndex(-1)
        self.model.activated.connect(self.model_changed)

        self.domain_table = HydraTableWidget(self.project, ("id", "name", "comment"),(tr("Id"), tr("Name"), tr("Comment")),
                                self.project.get_current_model().name, "domain_2d",
                                (self.new_domain, self.delete_domain), "", "id",
                                self.table_domain_placeholder)
        self.domain_table.set_editable(True)
        self.domain_table.data_edited.connect(self.save_active_domain)
        self.active_domain_id = None
        self.domain_table.table.itemSelectionChanged.connect(self.set_active_domain)

    def set_active_domain(self):
        items = self.domain_table.table.selectedItems()
        if len(items)>0:
            selected_row = self.domain_table.table.row(items[0])
            if not self.active_domain_id is None:
                self.save_active_domain()
                self.domain_table.table.selectRow(selected_row)
            self.active_domain_id = self.domain_table.table.item(selected_row,0).text()
        else:
            self.active_domain_id = None

    def save_active_domain(self):
        if not self.active_domain_id is None:
            new_name=None
            new_desc=None
            row=0
            for i in range(0, self.domain_table.table.rowCount()):
                if int(self.active_domain_id) == int(self.domain_table.table.item(i,0).text()):
                    new_name = normalized_name(self.domain_table.table.item(i, 1).text())
                    new_desc = self.domain_table.table.item(i, 2).text()
                    row=i

            assert new_name is not None
            assert new_desc is not None

            self.project.execute("""
            update {}.domain_2d set name='{}', comment='{}'
            where id={}""".format(self.project.get_current_model().name,str(new_name),str(new_desc),self.active_domain_id))

            if not new_name == self.domain_table.table.item(row, 1).text():
                self.domain_table.update_data()

    def new_domain(self):
        self.domain_table.add_row(['default', ''])

    def delete_domain(self):
        self.domain_table.del_selected_row()

    def model_changed(self):
        if self.model.currentIndex() != -1 :
            self.project.set_current_model(self.model.currentText())
        else :
            self.project.log.warning(tr("No active model"))
            self.project.set_current_model(None)
        self.refresh()

    def sav(self):
        self.save()
        self.close()
