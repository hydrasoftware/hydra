################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


from qgis.PyQt.QtWidgets import QComboBox
from hydra.gui.widgets.hydra_table_widget import HydraTableWidget
from functools import partial

class RainGagedTableWidget(HydraTableWidget):

    def __init__(self, project, data_columns,
            title_columns, dbname, table_name, callbacks, filter="",
            key_id="id", parent=None, autoInit=True, showEdit=False, showHelp=False, file_columns=[]):
        HydraTableWidget.__init__(self, project, data_columns,
            title_columns, dbname, table_name, callbacks, filter,
            key_id, parent, autoInit=autoInit, showEdit=showEdit, showHelp=showHelp, file_columns=file_columns)

    def update_data(self):
        super(RainGagedTableWidget, self).update_data()
        self.__loading = True

        #add montana combobox selectors
        for icol in range(0, len(self.data_cols)):
            if "interpolation" in self.data_cols[icol]:
                for irow in range(0,self.table.rowCount()):
                    interpolation = self.table.item(irow,icol).text()
                    cbox = self.get_new_interpolation_combo(interpolation)
                    cbox.currentIndexChanged.connect(partial(self.combo_index_changed,irow, icol))
                    self.table.setCellWidget(irow,icol, cbox)

        self.__loading = False

    def get_new_interpolation_combo(self, interpolation):
        list_interpolation = self.project.execute("select id, name from hydra.rainfall_interpolation_type").fetchall()
        combobox = QComboBox()
        for i in range(0, len(list_interpolation)):
            item = list_interpolation[i]
            combobox.addItem(item[1])
            if (interpolation == str(item[1])):
                combobox.setCurrentIndex(i)
        return combobox

    def combo_index_changed(self, irow, icol, comboBoxIndex):
        self.table.selectRow(irow)
        list_interpolation = self.project.execute("select id, name from hydra.rainfall_interpolation_type").fetchall()
        interpolation=self.table.cellWidget(irow,icol).currentText() if self.table.cellWidget(irow,icol).currentText()!="" else list_interpolation[1]
        self.table.item(irow,icol).setText(interpolation)

    def save_selected_row(self):
        super(RainGagedTableWidget, self).save_selected_row()